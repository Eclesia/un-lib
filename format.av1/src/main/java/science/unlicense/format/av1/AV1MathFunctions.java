
package science.unlicense.format.av1;

/**
 *
 * @author Johann Sorel
 */
public final class AV1MathFunctions {

    private AV1MathFunctions(){}

    // 4.7. Mathematical functions , page 18

    public static int abs(int x) {
        return x >= 0 ? x : -x;
    }

    public static int clip1(int x, int bitDepth) {
        return clip3(0, (int) (Math.pow(2, bitDepth))-1, x);
    }

    public static int clip3(int x, int y, int z) {
        if (z < x) return x;
        if (z > y) return y;
        return z;
    }

    public static int round2(int x, int n) {
        if (n == 0) return x;
        return (x+(1<<(n-1))) >> n;
    }

    public static int round2Signed(int x, int n) {
        if (x >= 0) return round2(x, n);
        return -round2(-x, n);
    }

    public static int floorLog2(int x) {
        int s = 0;
        while (x != 0) {
            x = x >> 1;
            s++;
        }
        return s-1;
    }

    public static int ceilLog2(int x) {
        if (x < 2) return 0;
        int i = 1;
        int p = 2;
        while (p < x) {
            i++;
            p = p << 1;
        }
        return i;
    }

}
