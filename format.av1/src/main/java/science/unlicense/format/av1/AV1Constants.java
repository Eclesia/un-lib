
package science.unlicense.format.av1;

/**
 * AV1 codec constants.
 *
 * @author Johann Sorel
 */
public final class AV1Constants {

    private AV1Constants() {}

    // 3. Symbols and abbreviated terms : page 8

    // page 7
    /** Number of reference frames that can be used for interprediction */
    public static final int REFS_PER_FRAME = 7;
    /** Number of reference frame types (including intra type) */
    public static final int TOTAL_REFS_PER_FRAME = 8;
    /** Number of contexts when decoding y_mode */
    public static final int BLOCK_SIZE_GROUPS = 4;
    /** Number of different block sizes used */
    public static final int BLOCK_SIZES = 22;
    /**Sentinel value to mark partition choices that are not allowed */
    public static final int BLOCK_INVALID = 22;
    /** Maximum size of a superblock in luma samples */
    public static final int MAX_SB_SIZE = 128;
    /** Smallest size of a mode info block in luma samples */
    public static final int MI_SIZE = 4;
    /** Base 2 logarithm of smallest size of a mode info block */
    public static final int MI_SIZE_LOG2 = 2;
    /** Maximum width of a tile in units of luma samples */
    public static final int MAX_TILE_WIDTH = 4096;
    /** Maximum area of a tile in units of luma samples */
    public static final int MAX_TILE_AREA = 4096 * 2304;
    /** Maximum number of tile rows */
    public static final int MAX_TILE_ROWS = 64;
    /** Maximum number of tile columns */
    public static final int MAX_TILE_COLS = 64;
    /** Number of horizontal luma samples before intra block copy can be used */
    public static final int INTRABC_DELAY_PIXELS = 256;

    // page 8
    /** Number of 64 by 64 blocks before intra block copy can be used */
    public static final int INTRABC_DELAY_SB64 = 4;
    /** Number of frames that can be stored for future reference */
    public static final int NUM_REF_FRAMES = 8;
    /** Number of contexts for is_inter */
    public static final int IS_INTER_CONTEXTS = 4;
    /** Number of contexts for single_ref , comp_ref , comp_bwdref , uni_comp_ref , uni_comp_ref_p1 and uni_comp_ref_p2 */
    public static final int REF_CONTEXTS = 3;
    /** Number of segments allowed in segmentation map */
    public static final int MAX_SEGMENTS = 8;
    /** Number of contexts for segment_id */
    public static final int SEGMENT_ID_CONTEXTS = 3;
    /** Index for quantizer segment feature */
    public static final int SEG_LVL_ALT_Q = 0;
    /** Index for vertical luma loop filter segment feature */
    public static final int SEG_LVL_ALT_LF_Y_V = 1;
    /** Index for reference frame segment feature */
    public static final int SEG_LVL_REF_FRAME = 5;
    /** Index for skip segment feature */
    public static final int SEG_LVL_SKIP = 6;
    /** Index for global mv feature */
    public static final int SEG_LVL_GLOBALMV = 7;
    /** Number of segment features */
    public static final int SEG_LVL_MAX = 8;
    /** Number of different plane types (luma or chroma) */
    public static final int PLANE_TYPES = 2;
    /** Number of contexts for transform size */
    public static final int TX_SIZE_CONTEXTS = 3;
    /** Number of values for interp_filter */
    public static final int INTERP_FILTERS = 3;
    /** Number of contexts for interp_filter */
    public static final int INTERP_FILTER_CONTEXTS = 16;
    /** Number of contexts for decoding skip_mode */
    public static final int SKIP_MODE_CONTEXTS = 3;
    /** Number of contexts for decoding skip */
    public static final int SKIP_CONTEXTS = 3;
    /** Number of contexts when decoding partition */
    public static final int PARTITION_CONTEXTS = 4;
    /** Number of square transform sizes */
    public static final int TX_SIZES = 5;
    /** Number of transform sizes (including non-square sizes) */
    public static final int TX_SIZES_ALL = 19;
    /** Number of values for tx_mode */
    public static final int TX_MODES = 3;
    /** Inverse transform rows with DCT and columns with DCT */
    public static final int DCT_DCT = 0;
    /** Inverse transform rows with DCT and columns with ADST */
    public static final int ADST_DCT = 1;

    // page 9
    /** Inverse transform rows with ADST and columns with DCT */
    public static final int DCT_ADST = 2;
    /** Inverse transform rows with ADST and columns with ADST */
    public static final int ADST_ADST = 3;
    /** Inverse transform rows with FLIPADST and columns with DCT */
    public static final int FLIPADST_DCT = 4;
    /** Inverse transform rows with DCT and columns with FLIPADST */
    public static final int DCT_FLIPADST = 5;
    /** Inverse transform rows with FLIPADST and columns with FLIPADST */
    public static final int FLIPADST_FLIPADST = 6;
    /** Inverse transform rows with ADST and columns with FLIPADST */
    public static final int ADST_FLIPADST = 7;
    /** Inverse transform rows with FLIPADST and columns with ADST */
    public static final int FLIPADST_ADST = 8;
    /** Inverse transform rows with identity and columns with identity */
    public static final int IDTX = 9;
    /** Inverse transform rows with identity and columns with DCT */
    public static final int V_DCT = 10;
    /** Inverse transform rows with DCT and columns with identity */
    public static final int H_DCT = 11;
    /** Inverse transform rows with identity and columns with ADST */
    public static final int V_ADST = 12;
    /** Inverse transform rows with ADST and columns with identity */
    public static final int H_ADST = 13;
    /** Inverse transform rows with identity and columns with FLIPADST */
    public static final int V_FLIPADST = 14;
    /** Inverse transform rows with FLIPADST and columns with identity */
    public static final int H_FLIPADST = 15;
    /** Number of inverse transform types */
    public static final int TX_TYPES = 16;
    /** Number of values for YMode */
    public static final int MB_MODE_COUNT = 17;
    /** Number of values for y_mode */
    public static final int INTRA_MODES = 13;
    /** Number of values for uv_mode when chroma from luma is not allowed */
    public static final int UV_INTRA_MODES_CFL_NOT_ALLOWED = 13;

    // page 10
    /** Number of values for uv_mode when chroma from luma is allowed */
    public static final int UV_INTRA_MODES_CFL_ALLOWED = 14;
    /** Number of values for compound_mode */
    public static final int COMPOUND_MODES = 8;
    /** Number of contexts for compound_mode */
    public static final int COMPOUND_MODE_CONTEXTS = 8;
    /** Number of new mv values used when constructing context for compound_mode */
    public static final int COMP_NEWMV_CTXS = 5;
    /** Number of contexts for new_mv */
    public static final int NEW_MV_CONTEXTS = 6;
    /** Number of contexts for zero_mv */
    public static final int ZERO_MV_CONTEXTS = 2;
    /** Number of contexts for ref_mv */
    public static final int REF_MV_CONTEXTS = 6;
    /** Number of contexts for drl_mode */
    public static final int DRL_MODE_CONTEXTS = 3;
    /** Number of contexts for decoding motion vectors including one for intra block copy */
    public static final int MV_CONTEXTS = 2;
    /** Motion vector context used for intra block copy */
    public static final int MV_INTRABC_CONTEXT = 1;
    /** Number of values for mv_joint */
    public static final int MV_JOINTS = 4;
    /** Number of values for mv_class */
    public static final int MV_CLASSES = 11;
    /** Number of values for mv_class0_bit */
    public static final int CLASS0_SIZE = 2;
    /** Maximum number of bits for decoding motion vectors */
    public static final int MV_OFFSET_BITS = 10;
    /** Maximum value used for loop filtering */
    public static final int MAX_LOOP_FILTER = 63;
    /** Number of bits of precision when scaling reference frames */
    public static final int REF_SCALE_SHIFT = 14;
    /** Number of bits of precision when choosing an inter prediction filter kernel */
    public static final int SUBPEL_BITS = 4;
    /** ( 1 << SUBPEL_BITS ) - 1 */
    public static final int SUBPEL_MASK = 15;
    /** Number of bits of precision when computing inter prediction locations */
    public static final int SCALE_SUBPEL_BITS = 10;
    /** Value used when clipping motion vectors */
    public static final int MV_BORDER = 128;
    /** Number of values for color contexts */
    public static final int PALETTE_COLOR_CONTEXTS = 5;
    /** Number of mappings between color context hash and color context */
    public static final int PALETTE_MAX_COLOR_CONTEXT_HASH = 8;
    /** Number of values for palette block size */
    public static final int PALETTE_BLOCK_SIZE_CONTEXTS = 7;
    /** Number of values for palette Y plane mode contexts */
    public static final int PALETTE_Y_MODE_CONTEXTS = 3;

    // page 11
    /** Number of values for palette U and V plane mode contexts */
    public static final int PALETTE_UV_MODE_CONTEXTS = 2;
    /** Number of values for palette_size */
    public static final int PALETTE_SIZES = 7;
    /** Number of values for palette_color */
    public static final int PALETTE_COLORS = 8;
    /** Number of neighbors considered within palette computation */
    public static final int PALETTE_NUM_NEIGHBORS = 3;
    /** Value indicating alternative encoding of quantizer index delta values */
    public static final int DELTA_Q_SMALL = 3;
    /** Value indicating alternative encoding of loop filter delta values */
    public static final int DELTA_LF_SMALL = 3;
    /** Number of values in the quantizer matrix */
    public static final int QM_TOTAL_SIZE = 3344;
    /** Maximum magnitude of AngleDeltaY and AngleDeltaUV */
    public static final int MAX_ANGLE_DELTA = 3;
    /** Number of directional intra modes */
    public static final int DIRECTIONAL_MODES = 8;
    /** Number of degrees of step per unit increase in AngleDeltaY or AngleDeltaUV. */
    public static final int ANGLE_STEP = 3;
    /** Number of intra transform set types */
    public static final int TX_SET_TYPES_INTRA = 3;
    /** Number of inter transform set types */
    public static final int TX_SET_TYPES_INTER = 4;
    /** Internal precision of warped motion models */
    public static final int WARPEDMODEL_PREC_BITS = 16;
    /** Warp model is just an identity transform */
    public static final int IDENTITY = 0;
    /** Warp model is a pure translation */
    public static final int TRANSLATION = 1;
    /** Warp model is a rotation + symmetric zoom + translation */
    public static final int ROTZOOM = 2;
    /** Warp model is a general affine transform */
    public static final int AFFINE = 3;
    /** Number of bits encoded for translational components of global motion models, if part of a ROTZOOM or AFFINE model */
    public static final int GM_ABS_TRANS_BITS = 12;
    /** Number of bits encoded for translational components of global motion models, if part of a TRANSLATION model */
    public static final int GM_ABS_TRANS_ONLY_BITS = 9;
    /** Number of bits encoded for non-translational components of global motion models */
    public static final int GM_ABS_ALPHA_BITS = 12;

    // page 12
    /** Number of fractional bits of entries in divisor lookup table */
    public static final int DIV_LUT_PREC_BITS = 14;
    /** Number of fractional bits for lookup in divisor lookup table */
    public static final int DIV_LUT_BITS = 8;
    /** Number of entries in divisor lookup table */
    public static final int DIV_LUT_NUM = 257;
    /** Number of values for motion modes */
    public static final int MOTION_MODES = 3;
    /** Use translation or global motion compensation */
    public static final int SIMPLE = 0;
    /** Use overlapped block motion compensation */
    public static final int OBMC = 1;
    /** Use local warp motion compensation */
    public static final int LOCALWARP = 2;
    /** Largest number of samples used when computing a local warp */
    public static final int LEAST_SQUARES_SAMPLES_MAX = 8;
    /** Largest motion vector difference to include in local warp computation */
    public static final int LS_MV_MAX = 256;
    /** Clamping value used for translation components of warp */
    public static final int WARPEDMODEL_TRANS_CLAMP = 1<<23;
    /** Clamping value used for matrix components of warp */
    public static final int WARPEDMODEL_NONDIAGAFFINE_CLAMP = 1<<13;
    /** Number of phases used in warped filtering */
    public static final int WARPEDPIXEL_PREC_SHIFTS = 1<<6;
    /** Number of extra bits of precision in warped filtering */
    public static final int WARPEDDIFF_PREC_BITS = 10;
    /** Number of fractional bits for sending non-translational warp model coefficients */
    public static final int GM_ALPHA_PREC_BITS = 15;
    /** Number of fractional bits for sending translational warp model coefficients */
    public static final int GM_TRANS_PREC_BITS = 6;
    /** Number of fractional bits used for pure translational warps */
    public static final int GM_TRANS_ONLY_PREC_BITS = 3;
    /** Number of inter intra modes */
    public static final int INTERINTRA_MODES = 4;
    /** Size of MasterMask array */
    public static final int MASK_MASTER_SIZE = 64;
    /** Number of contexts for segment_id_predicted */
    public static final int SEGMENT_ID_PREDICTED_CONTEXTS = 3;
    /** Number of contexts for is_inter */
    //public static final int IS_INTER_CONTEXTS = 4; // NOTE : declared twice
    /** Number of contexts for skip */
    //public static final int SKIP_CONTEXTS = 3; //NOTE : declared twice
    /** Number of syntax elements for forward reference frames */
    public static final int FWD_REFS = 4;

    // page 13
    /** Number of syntax elements for backward reference frames */
    public static final int BWD_REFS = 3;
    /** Number of syntax elements for single reference frames */
    public static final int SINGLE_REFS = 7;
    /** Number of syntax elements for unidirectional compound reference frames */
    public static final int UNIDIR_COMP_REFS = 4;
    /** Number of values for compound_type */
    public static final int COMPOUND_TYPES = 2;
    /** Number of values for cfl_alpha_signs */
    public static final int CFL_JOINT_SIGNS = 8;
    /** Number of values for cfl_alpha_u and cfl_alpha_v */
    public static final int CFL_ALPHABET_SIZE = 16;
    /** Number of contexts for comp_mode */
    public static final int COMP_INTER_CONTEXTS = 5;
    /** Number of contexts for comp_ref_type */
    public static final int COMP_REF_TYPE_CONTEXTS = 5;
    /** Number of contexts for cfl_alpha_u and cfl_alpha_v */
    public static final int CFL_ALPHA_CONTEXTS = 6;
    /** Number of contexts for intra_frame_y_mode */
    public static final int INTRA_MODE_CONTEXTS = 5;
    /** Number of contexts for comp_group_idx */
    public static final int COMP_GROUP_IDX_CONTEXTS = 6;
    /** Number of contexts for compound_idx */
    public static final int COMPOUND_IDX_CONTEXTS = 6;
    /** Number of filter kernels for the intra edge filter */
    public static final int INTRA_EDGE_KERNELS = 3;
    /** Number of kernel taps for the intra edge filter */
    public static final int INTRA_EDGE_TAPS = 5;
    /** Number of loop filter strength values */
    public static final int FRAME_LF_COUNT = 4;
    /** Maximum depth for variable transform trees */
    public static final int MAX_VARTX_DEPTH = 2;
    /** Number of contexts for txfm_split */
    public static final int TXFM_PARTITION_CONTEXTS = 21;
    /** Bonus weight for close motion vectors */
    public static final int REF_CAT_LEVEL = 640;
    /** Maximum number of motion vectors in the stack */
    public static final int MAX_REF_MV_STACK_SIZE = 8;
    /** Stack size for motion field motion vectors */
    public static final int MFMV_STACK_SIZE = 3;
    /** Number of contexts for tx_depth when the maximum transform size is 8x8 */
    public static final int MAX_TX_DEPTH = 2;
    /** Number of directions for the wedge mask process */
    public static final int WEDGE_TYPES = 16;
    /** Number of bits used in Wiener filter coefficients */
    public static final int FILTER_BITS = 7;
    /** Number of Wiener filter coefficients to read */
    public static final int WIENER_COEFFS = 3;

    // page 14
    /** Number of bits needed to specify self guided filter set */
    public static final int SGRPROJ_PARAMS_BITS = 4;
    /** Controls how self guided deltas are read */
    public static final int SGRPROJ_PRJ_SUBEXP_K = 4;
    /** Precision bits during self guided restoration */
    public static final int SGRPROJ_PRJ_BITS = 7;
    /** Restoration precision bits generated higher than source before projection */
    public static final int SGRPROJ_RST_BITS = 4;
    /** Precision of mtable division table */
    public static final int SGRPROJ_MTABLE_BITS = 20;
    /** Precision of division by n table */
    public static final int SGRPROJ_RECIP_BITS = 12;
    /** Internal precision bits for core selfguided_restoration */
    public static final int SGRPROJ_SGR_BITS = 8;
    /** Number of bits to reduce CDF precision during arithmetic coding */
    public static final int EC_PROB_SHIFT = 6;
    /** Minimum probability assigned to each symbol during arithmetic coding */
    public static final int EC_MIN_PROB = 4;
    /** Value that indicates the allow_screen_content_tools syntax element is coded */
    public static final int SELECT_SCREEN_CONTENT_TOOLS = 2;
    /** Value that indicates the force_integer_mv syntax element is coded */
    public static final int SELECT_INTEGER_MV = 2;
    /** Maximum size of a loop restoration tile */
    public static final int RESTORATION_TILESIZE_MAX = 256;
    /** Maximum distance when computing weighted prediction */
    public static final int MAX_FRAME_DISTANCE = 31;
    /** Maximum horizontal offset of a projected motion vector */
    public static final int MAX_OFFSET_WIDTH = 8;
    /** Maximum vertical offset of a projected motion vector */
    public static final int MAX_OFFSET_HEIGHT = 0;
    /** Rounding bitwidth for the parameters to the shear process */
    public static final int WARP_PARAM_REDUCE_BITS = 6;
    /** Number of quantizer base levels */
    public static final int NUM_BASE_LEVELS = 2;
    /** The quantizer range above NUM_BASE_LEVELS above which the Exp-Golomb coding process is activated */
    public static final int COEFF_BASE_RANGE = 12;
    /** Number of contexts for coeff_br */
    public static final int BR_CDF_SIZE = 4;
    /** Number of contexts for coeff_base_eob */
    public static final int SIG_COEF_CONTEXTS_EOB = 4;
    /** Context offset for coeff_base for horizontal-only or vertical-only transforms. */
    public static final int SIG_COEF_CONTEXTS_2D = 26;
    /** Number of contexts for coeff_base */
    public static final int SIG_COEF_CONTEXTS = 42;

    // page 15
    /** Maximum number of context samples to be used in determining the context index for coeff_base and coeff_base_eob . */
    public static final int SIG_REF_DIFF_OFFSET_NUM = 5;
    /** Numerator for upscaling ratio */
    public static final int SUPERRES_NUM = 8;
    /** Smallest denominator for upscaling ratio */
    public static final int SUPERRES_DENOM_MIN = 9;
    /** Number of bits sent to specify denominator of upscaling ratio */
    public static final int SUPERRES_DENOM_BITS = 3;
    /** Number of bits of fractional precision for upscaling filter selection */
    public static final int SUPERRES_FILTER_BITS = 6;
    /** Number of phases of upscaling filters */
    public static final int SUPERRES_FILTER_SHIFTS = 1 << SUPERRES_FILTER_BITS;
    /** Number of taps of upscaling filters */
    public static final int SUPERRES_FILTER_TAPS = 8;
    /** Sample offset for upscaling filters */
    public static final int SUPERRES_FILTER_OFFSET = 3;
    /** Number of fractional bits for computing position in upscaling */
    public static final int SUPERRES_SCALE_BITS = 14;
    /** Mask for computing position in upscaling */
    public static final int SUPERRES_SCALE_MASK = (1 << 14) - 1;
    /** Difference in precision between SUPERRES_SCALE_BITS and SUPERRES_FILTER_BITS */
    public static final int SUPERRES_EXTRA_BITS = 8;
    /** Number of contexts for all_zero */
    public static final int TXB_SKIP_CONTEXTS = 13;
    /** Number of contexts for eob_extra */
    public static final int EOB_COEF_CONTEXTS = 9;
    /** Number of contexts for dc_sign */
    public static final int DC_SIGN_CONTEXTS = 3;
    /** Number of contexts for coeff_br */
    public static final int LEVEL_CONTEXTS = 21;
    /** Transform class for transform types performing non-identity transforms in both directions */
    public static final int TX_CLASS_2D = 0;
    /** Transform class for transforms performing only a horizontal non-identity transform */
    public static final int TX_CLASS_HORIZ = 1;
    /** Transform class for transforms performing only a vertical non-identity transform */
    public static final int TX_CLASS_VERT = 2;
    /** Largest reference MV component that can be saved */
    public static final int REFMVS_LIMIT = ( 1 << 12 ) - 1;
    /** Scaling shift for intra filtering process */
    public static final int INTRA_FILTER_SCALE_BITS = 4;
    /** Number of types of intra filtering */
    public static final int INTRA_FILTER_MODES = 5;

    // page 16
    /** Number of selectable context types for the coeff( ) syntax structure */
    public static final int COEFF_CDF_Q_CTXS = 4;
    /** Value of primary_ref_frame indicating that there is no primary reference frame */
    public static final int PRIMARY_REF_NONE = 7;


    ////////////////////////////////////////////////////////////////////////////
    // 6.10.4. Decode partition semantics, page 160
    ////////////////////////////////////////////////////////////////////////////

    public static final int PARTITION_NONE = 0;
    public static final int PARTITION_HORZ = 1;
    public static final int PARTITION_VERT = 2;
    public static final int PARTITION_SPLIT = 3;
    public static final int PARTITION_HORZ_A = 4;
    public static final int PARTITION_HORZ_B = 5;
    public static final int PARTITION_VERT_A = 6;
    public static final int PARTITION_VERT_B = 7;
    public static final int PARTITION_HORZ_4 = 8;
    public static final int PARTITION_VERT_4 = 9;

    public static final int BLOCK_4X4 = 0;
    public static final int BLOCK_4X8 = 1;
    public static final int BLOCK_8X4 = 2;
    public static final int BLOCK_8X8 = 3;
    public static final int BLOCK_8X16 = 4;
    public static final int BLOCK_16X8 = 5;
    public static final int BLOCK_16X16 = 6;
    public static final int BLOCK_16X32 = 7;
    public static final int BLOCK_32X16 = 8;
    public static final int BLOCK_32X32 = 9;
    public static final int BLOCK_32X64 = 10;
    public static final int BLOCK_64X32 = 11;
    public static final int BLOCK_64X64 = 12;
    public static final int BLOCK_64X128 = 13;
    public static final int BLOCK_128X64 = 14;
    public static final int BLOCK_128X128 = 15;
    public static final int BLOCK_4X16 = 16;
    public static final int BLOCK_16X4 = 17;
    public static final int BLOCK_8X32 = 18;
    public static final int BLOCK_32X8 = 19;
    public static final int BLOCK_16X64 = 20;
    public static final int BLOCK_64X16 = 21;

    ////////////////////////////////////////////////////////////////////////////
    // 6.10.16. TX size semantics, page 166
    ////////////////////////////////////////////////////////////////////////////

    public static final int TX_4X4 = 0;
    public static final int TX_8X8 = 1;
    public static final int TX_16X16 = 2;
    public static final int TX_32X32 = 3;
    public static final int TX_64X64 = 4;
    public static final int TX_4X8 = 5;
    public static final int TX_8X4 = 6;
    public static final int TX_8X16 = 7;
    public static final int TX_16X8 = 8;
    public static final int TX_16X32 = 9;
    public static final int TX_32X16 = 10;
    public static final int TX_32X64 = 11;
    public static final int TX_64X32 = 12;
    public static final int TX_4X16 = 13;
    public static final int TX_16X4 = 14;
    public static final int TX_8X32 = 15;
    public static final int TX_32X8 = 16;
    public static final int TX_16X64 = 17;
    public static final int TX_64X16 = 18;


}
