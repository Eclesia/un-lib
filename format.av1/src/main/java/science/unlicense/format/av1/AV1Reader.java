
package science.unlicense.format.av1;

import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.av1.model.OBUHeader;
import static science.unlicense.format.av1.AV1Constants.*;

/**
 *
 * @author Johann Sorel
 */
public class AV1Reader extends AbstractReader {

    private AV1InputStream ds;

    //TODO : where does this value come from ???
    private int OperatingPointIdc = 0;

    /**
     * Specification : 5.3.1
     *
     */
    private void open_bitstream_unit(int sz) throws IOException {

//        final OBUHeader header = new OBUHeader();
//        header.read(ds);
//
//        long obu_size;
//        if ( header.obu_has_size_field ) {
//            obu_size = ds.leb128();
//        } else {
//            obu_size = sz - 1 - header.obu_extension_flag;
//        }
//
//        long startPosition = ds.get_position();
//        if ( header.obu_type != OBU_SEQUENCE_HEADER &&
//             header.obu_type != OBU_TEMPORAL_DELIMITER &&
//             OperatingPointIdc != 0 &&
//             header.obu_extension_flag == 1 ) {
//            int inTemporalLayer = (OperatingPointIdc >> header.temporal_id ) & 1;
//            int inSpatialLayer = (OperatingPointIdc >> (header.spatial_id + 8) ) & 1;
//            if ( !(inTemporalLayer!=0) || !(inSpatialLayer!=0) ) {
//                drop_obu();
//                return;
//            }
//        }
//        switch (header.obu_type) {
//            case OBU_SEQUENCE_HEADER: sequence_header_obu(); break;
//            case OBU_TEMPORAL_DELIMITER: temporal_delimiter_obu(); break;
//            case OBU_FRAME_HEADER: frame_header_obu(); break;
//            case OBU_REDUNDANT_FRAME_HEADER: frame_header_obu(); break;
//            case OBU_TILE_GROUP: tile_group_obu(obu_size); break;
//            case OBU_METADATA: metadata_obu(); break;
//            case OBU_FRAME: frame_obu(obu_size); break;
//            case OBU_TILE_LIST: tile_list_obu(); break;
//            case OBU_PADDING: padding_obu(); break;
//            default: reserved_obu(); break;
//        }
//
//        long currentPosition = ds.get_position();
//        long payloadBits = currentPosition - startPosition;
//        if (obu_size > 0 && header.obu_type != OBU_TILE_GROUP && header.obu_type != OBU_FRAME) {
//            ds.trailing_bits((int) (obu_size*8 - payloadBits));
//        }
    }

    private void drop_obu() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void sequence_header_obu() throws IOException {
        final int seq_profile = ds.readBits(3);
        final int still_picture = ds.readBits(1);
        final int reduced_still_picture_header = ds.readBits(1);

//        int timing_info_present_flag;
//        if (reduced_still_picture_header != 0) {
//            timing_info_present_flag = 0;
//            decoder_model_info_present_flag = 0;
//            initial_display_delay_present_flag = 0;
//            operating_points_cnt_minus_1 = 0;
//            operating_point_idc[ 0 ] = 0;
//            seq_level_idx[ 0 ] = ds.readBits(5);
//            seq_tier[ 0 ] = 0;
//            decoder_model_present_for_this_op[ 0 ] = 0;
//            initial_display_delay_present_for_this_op[ 0 ] = 0;
//        } else {
//
//        }


    }

    private void temporal_delimiter_obu() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void frame_header_obu() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void tile_group_obu(long obu_size) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void metadata_obu() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void frame_obu(long obu_size) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void tile_list_obu() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void padding_obu() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void reserved_obu() {
        throw new UnsupportedOperationException("Not supported yet.");
    }


}
