
package science.unlicense.format.av1.table;

/**
 *
 * @author Johann Sorel
 */
public final class Default_Coeff_Base_Eob_Cdf_Table {

    private Default_Coeff_Base_Eob_Cdf_Table(){}

    /** size : COEFF_CDF_Q_CTXS x TX_SIZES x PLANE_TYPES x SIG_COEF_CONTEXTS_EOB x 4 */
    public static final int[][][][][] Default_Coeff_Base_Eob_Cdf = {
      { { { { 17837, 29055, 32768, 0 },
            { 29600, 31446, 32768, 0 },
            { 30844, 31878, 32768, 0 },
            { 24926, 28948, 32768, 0 } },
          { { 21365, 30026, 32768, 0 },
            { 30512, 32423, 32768, 0 },
            { 31658, 32621, 32768, 0 },
            { 29630, 31881, 32768, 0 } } },
        { { { 5717, 26477, 32768, 0 },
            { 30491, 31703, 32768, 0 },
            { 31550, 32158, 32768, 0 },
            { 29648, 31491, 32768, 0 } },
          { { 12608, 27820, 32768, 0 },
            { 30680, 32225, 32768, 0 },
            { 30809, 32335, 32768, 0 },
            { 31299, 32423, 32768, 0 } } },
        { { { 1786, 12612, 32768, 0 },
            { 30663, 31625, 32768, 0 },
            { 32339, 32468, 32768, 0 },
            { 31148, 31833, 32768, 0 } },
          { { 18857, 23865, 32768, 0 },
            { 31428, 32428, 32768, 0 },
            { 31744, 32373, 32768, 0 },
            { 31775, 32526, 32768, 0 } } },
        { { { 1787, 2532, 32768, 0 },
            { 30832, 31662, 32768, 0 },
            { 31824, 32682, 32768, 0 },
            { 32133, 32569, 32768, 0 } },
          { { 13751, 22235, 32768, 0 },
            { 32089, 32409, 32768, 0 },
            { 27084, 27920, 32768, 0 },
            { 29291, 32594, 32768, 0 } } },
        { { { 1725, 3449, 32768, 0 },
            { 31102, 31935, 32768, 0 },
            { 32457, 32613, 32768, 0 },
            { 32412, 32649, 32768, 0 } },
          { { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 } } } },
      { { { { 17560, 29888, 32768, 0 },
            { 29671, 31549, 32768, 0 },
            { 31007, 32056, 32768, 0 },
            { 27286, 30006, 32768, 0 } },
          { { 26594, 31212, 32768, 0 },
            { 31208, 32582, 32768, 0 },
            { 31835, 32637, 32768, 0 },
            { 30595, 32206, 32768, 0 } } },
        { { { 15239, 29932, 32768, 0 },
            { 31315, 32095, 32768, 0 },
            { 32130, 32434, 32768, 0 },
            { 30864, 31996, 32768, 0 } },
          { { 26279, 30968, 32768, 0 },
            { 31142, 32495, 32768, 0 },
            { 31713, 32540, 32768, 0 },
            { 31929, 32594, 32768, 0 } } },
        { { { 2644, 25198, 32768, 0 },
            { 32038, 32451, 32768, 0 },
            { 32639, 32695, 32768, 0 },
            { 32166, 32518, 32768, 0 } },
          { { 17187, 27668, 32768, 0 },
            { 31714, 32550, 32768, 0 },
            { 32283, 32678, 32768, 0 },
            { 31930, 32563, 32768, 0 } } },
        { { { 1044, 2257, 32768, 0 },
            { 30755, 31923, 32768, 0 },
            { 32208, 32693, 32768, 0 },
            { 32244, 32615, 32768, 0 } },
          { { 21317, 26207, 32768, 0 },
            { 29133, 30868, 32768, 0 },
            { 29311, 31231, 32768, 0 },
            { 29657, 31087, 32768, 0 } } },
        { { { 478, 1834, 32768, 0 },
            { 31005, 31987, 32768, 0 },
            { 32317, 32724, 32768, 0 },
            { 30865, 32648, 32768, 0 } },
          { { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 } } } },
      { { { { 20092, 30774, 32768, 0 },
            { 30695, 32020, 32768, 0 },
            { 31131, 32103, 32768, 0 },
            { 28666, 30870, 32768, 0 } },
          { { 27258, 31095, 32768, 0 },
            { 31804, 32623, 32768, 0 },
            { 31763, 32528, 32768, 0 },
            { 31438, 32506, 32768, 0 } } },
        { { { 18049, 30489, 32768, 0 },
            { 31706, 32286, 32768, 0 },
            { 32163, 32473, 32768, 0 },
            { 31550, 32184, 32768, 0 } },
          { { 27116, 30842, 32768, 0 },
            { 31971, 32598, 32768, 0 },
            { 32088, 32576, 32768, 0 },
            { 32067, 32664, 32768, 0 } } },
        { { { 12854, 29093, 32768, 0 },
            { 32272, 32558, 32768, 0 },
            { 32667, 32729, 32768, 0 },
            { 32306, 32585, 32768, 0 } },
          { { 25476, 30366, 32768, 0 },
            { 32169, 32687, 32768, 0 },
            { 32479, 32689, 32768, 0 },
            { 31673, 32634, 32768, 0 } } },
        { { { 2809, 19301, 32768, 0 },
            { 32205, 32622, 32768, 0 },
            { 32338, 32730, 32768, 0 },
            { 31786, 32616, 32768, 0 } },
          { { 22737, 29105, 32768, 0 },
            { 30810, 32362, 32768, 0 },
            { 30014, 32627, 32768, 0 },
            { 30528, 32574, 32768, 0 } } },
        { { { 935, 3382, 32768, 0 },
            { 30789, 31909, 32768, 0 },
            { 32466, 32756, 32768, 0 },
            { 30860, 32513, 32768, 0 } },
          { { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 } } } },
      { { { { 22497, 31198, 32768, 0 },
            { 31715, 32495, 32768, 0 },
            { 31606, 32337, 32768, 0 },
            { 30388, 31990, 32768, 0 } },
          { { 27877, 31584, 32768, 0 },
            { 32170, 32728, 32768, 0 },
            { 32155, 32688, 32768, 0 },
            { 32219, 32702, 32768, 0 } } },
        { { { 21457, 31043, 32768, 0 },
            { 31951, 32483, 32768, 0 },
            { 32153, 32562, 32768, 0 },
            { 31473, 32215, 32768, 0 } },
          { { 27558, 31151, 32768, 0 },
            { 32020, 32640, 32768, 0 },
            { 32097, 32575, 32768, 0 },
            { 32242, 32719, 32768, 0 } } },
        { { { 19980, 30591, 32768, 0 },
            { 32219, 32597, 32768, 0 },
            { 32581, 32706, 32768, 0 },
            { 31803, 32287, 32768, 0 } },
          { { 26473, 30507, 32768, 0 },
            { 32431, 32723, 32768, 0 },
            { 32196, 32611, 32768, 0 },
            { 31588, 32528, 32768, 0 } } },
        { { { 24647, 30463, 32768, 0 },
            { 32412, 32695, 32768, 0 },
            { 32468, 32720, 32768, 0 },
            { 31269, 32523, 32768, 0 } },
          { { 28482, 31505, 32768, 0 },
            { 32152, 32701, 32768, 0 },
            { 31732, 32598, 32768, 0 },
            { 31767, 32712, 32768, 0 } } },
        { { { 12358, 24977, 32768, 0 },
            { 31331, 32385, 32768, 0 },
            { 32634, 32756, 32768, 0 },
            { 30411, 32548, 32768, 0 } },
          { { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 },
            { 10923, 21845, 32768, 0 } } } }
    };

}
