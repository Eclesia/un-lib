
package science.unlicense.format.av1.model;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.av1.AV1InputStream;

/**
 *
 * Specification : 5.3.2 OBU header syntax
 *                 5.3.3 OBU extension header syntax
 *
 * @author Johann Sorel
 */
public class OBUHeader {

    public boolean obu_forbidden_bit;
    public int obu_type;
    public int obu_extension_flag;
    public boolean obu_has_size_field;
    public boolean obu_reserved_1bit;
    //extension header
    public int temporal_id;
    public int spatial_id;
    public int extension_header_reserved_3bits;

    public void read(AV1InputStream ds) throws IOException {
        obu_forbidden_bit = ds.readBits(1) != 0;
        obu_type = ds.readBits(4);
        obu_extension_flag = ds.readBits(1);
        obu_has_size_field = ds.readBits(1) != 0;
        if (obu_extension_flag == 1) {
            temporal_id = ds.readBits(3);
            spatial_id = ds.readBits(2);
            extension_header_reserved_3bits = ds.readBits(3);
        }
    }

}
