
package science.unlicense.format.av1;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class AV1InputStream extends DataInputStream {

    public AV1InputStream(ByteInputStream in) {
        super(in);
        setBitsDirection(DataInputStream.MSB);
    }


    public long get_position() {
        return getByteOffset() * 8 + getBitOffset();
    }

    public void initSymbol(int sz) {
        throw new RuntimeException("TODO");
    }

    public void exitSymbol() {
        throw new RuntimeException("TODO");
    }

    /**
     * Specification : 4.10.3 uvlc() page. 23
     */
    public int uvlc() throws IOException {
        int leadingZeros = 0;
        while (readBits(1)==1) {
            leadingZeros++;
        }
        if (leadingZeros >= 32) {
            return (1 << 32) - 1;
        }
        int value = readBits(leadingZeros);
        return value + ( 1 << leadingZeros ) - 1 ;
    }

    /**
     * Specification : 4.10.4 le(n) page. 23
     */
    public int le(int n) throws IOException {
        int t = 0;
        for (int i=0; i<n; i++) {
            t += (readUByte() << (i*8));
        }
        return t;
    }

    /**
     * Specification : 4.10.6 le(n) page. 24
     */
    public int su(int n) throws IOException {
        int value = readBits(n);
        int signMask = 1 << (n-1);
        if ( (value & signMask) != 0 ) {
            value = value - 2*signMask;
        }
        return value;
    }

    /**
     * Specification : 4.10.7 ns(n) page. 25
     */
    public int ns(int n) throws IOException {
        int w = AV1MathFunctions.floorLog2(n) + 1;
        int m = (1 << w) - n;
        int v = readBits(w-1);
        if (v < m) return v;
        int extraBit = readBits(1);
        return (v << 1) - m + extraBit;
    }

    /**
     * Specification : 4.10.8 L(n) page. 25
     */
    public int L(int n) throws IOException {
        throw new IOException(null, "TODO");
    }

    /**
     * Specification : 4.10.9 S() page. 26
     */
    public int S() throws IOException {
        throw new IOException(null, "TODO");
    }

    /**
     * Specification : 4.10.10 NS(n) page. 26
     */
    public int NS(int n) throws IOException {
        int w = AV1MathFunctions.floorLog2(n) + 1;
        int m = (1 << w) - n;
        int v = L(w-1);
        if (v < m) return v;
        int extraBit = L(1);
        return (v << 1) - m + extraBit;
    }

    public long leb128() throws IOException {
        return readVarLengthUInt();
    }


}
