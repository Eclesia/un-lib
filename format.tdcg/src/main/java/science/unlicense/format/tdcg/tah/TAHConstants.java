
package science.unlicense.format.tdcg.tah;

/**
 *
 * @author Johann Sorel
 */
public final class TAHConstants {

    public static final byte[] SIGNATURE = new byte[]{'T','A','H','2'};

    private TAHConstants(){}

}
