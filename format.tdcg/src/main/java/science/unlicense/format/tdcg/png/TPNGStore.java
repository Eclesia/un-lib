
package science.unlicense.format.tdcg.png;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.image.api.ImageSetMetadata;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.png.PNGImageReader;
import science.unlicense.format.tdcg.png.model.TaObChunk;
import science.unlicense.format.tdcg.tso.TSOReader;

/**
 *
 * @author Johann Sorel
 */
public class TPNGStore extends AbstractModel3DStore{

    private boolean isRead = false;
    private final Sequence TaObChunks = new ArraySequence();

    public TPNGStore(Object input) {
        super(TPNGFormat.INSTANCE, input);
    }

    @Override
    public Collection getElements() throws StoreException {
        read();

        final Sequence elements = new ArraySequence();
        try{
            for (int i=0,n=TaObChunks.getSize();i<n;i++){
                Object res = convert((TaObChunk) TaObChunks.get(i));
                if (res!=null){
                    elements.add(res);
                }
            }
        }catch(IOException ex){
            throw new StoreException(ex);
        }
        return elements;
    }

    private void read() throws StoreException{
        if (isRead) return;
        isRead = true;

        try{
            final PNGImageReader reader = new PNGImageReader();
            reader.getChunkTable().add(TPNGConstants.CHUNK_TaOb, TaObChunk.class);
            reader.setInput(source);
            final ImageSetMetadata metas = (ImageSetMetadata) reader.getMetadata(new Chars("imageset"));
            final TypedNode pngMeta = (TypedNode) reader.getMetadata(new Chars("png"));

            final Iterator ite = pngMeta.getChildren().createIterator();
            while (ite.hasNext()) {
                final Object next = ite.next();
                if (next instanceof TaObChunk){
                    TaObChunks.add(next);
                }
            }
        }catch(IOException ex){
            throw new StoreException(ex);
        }
    }

    private Object convert(TaObChunk chunk) throws IOException{
        if (TPNGConstants.TYPE_TDCG.equals(chunk.type)){
            //TODO
            return null;
        } else if (TPNGConstants.TYPE_HSAV.equals(chunk.type)){
            //TODO
            return null;
        } else if (TPNGConstants.TYPE_FTSO.equals(chunk.type)){
            final TSOReader reader = new TSOReader();
            reader.setInput(new ArrayInputStream(chunk.data));
            return reader.read();
        } else {
            //unknown
            return null;
        }

    }

}
