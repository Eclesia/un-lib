
package science.unlicense.format.tdcg.tso;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.FloatSequence;
import science.unlicense.common.api.collection.primitive.IntSequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.tdcg.tso.model.TSOMaterial;
import science.unlicense.format.tdcg.tso.model.TSOMesh;
import science.unlicense.format.tdcg.tso.model.TSOShader;
import science.unlicense.format.tdcg.tso.model.TSOTexture;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * TSO reader.
 *
 * Note : the file structure is very similar to PMD.
 *
 *
 * @author Johann Sorel
 */
public class TSOReader extends AbstractReader{

    private Chars[] names;
    private Matrix4x4[] transforms;
    private TSOTexture[] textures;
    private TSOShader[] shaders;
    private TSOMaterial[] materials;
    private TSOMesh[] meshes;

    public Object read() throws IOException{
        decodeFile();
        return rebuildModel();
    }

    private void decodeFile() throws IOException{
        final DataInputStream ds = getInputAsDataStream(Endianness.LITTLE_ENDIAN);

        if (!Arrays.equals(TSOConstants.SIGNATURE, ds.readFully(new byte[4]))){
            throw new IOException(ds, "Stream is not a TSO.");
        }

        //read scene objets names/path
        names = new Chars[ds.readInt()];
        for (int i=0;i<names.length;i++){
            names[i] = ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS);
        }
        //read object transforms
        transforms = new Matrix4x4[ds.readInt()];
        for (int i=0;i<transforms.length;i++){
            transforms[i] = TSOConstants.readMatrix(ds);
        }
        //read textures
        textures = new TSOTexture[ds.readInt()];
        for (int i=0;i<textures.length;i++){
            textures[i] = new TSOTexture();
            textures[i].read(ds);
        }
        //read shaders
        shaders = new TSOShader[ds.readInt()];
        for (int i=0;i<shaders.length;i++){
            shaders[i] = new TSOShader();
            shaders[i].read(ds);
        }
        //read materials
        materials = new TSOMaterial[ds.readInt()];
        for (int i=0;i<materials.length;i++){
            materials[i] = new TSOMaterial();
            materials[i].read(ds);
        }
        //read meshes
        meshes = new TSOMesh[ds.readInt()];
        for (int i=0;i<meshes.length;i++){
            meshes[i] = new TSOMesh();
            meshes[i].read(ds);
        }
    }

    /**
     * TODO, rebuild this better.
     */
    private Object rebuildModel(){

        SceneNode node = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);

        for (int i=0;i<meshes.length;i++){
            final DefaultModel mesh = new DefaultModel();
            mesh.getTechniques().add(new SimpleBlinnPhong());
            final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
            mesh.setShape(shell);
            mesh.getNodeTransform().set(meshes[i].trs);
            node.getChildren().add(mesh);

            final FloatSequence vertices = new FloatSequence();
            final FloatSequence normals = new FloatSequence();
            final FloatSequence uvs = new FloatSequence();
            final IntSequence index = new IntSequence();
            int cnt = 0;
            for (int p=0;p<meshes[i].parts.length;p++){
                for (int v=0;v<meshes[i].parts[p].vertices.length;v++){
                    vertices.put(meshes[i].parts[p].vertices[v].coordinate);
                    normals .put(meshes[i].parts[p].vertices[v].normal);
                    uvs     .put(meshes[i].parts[p].vertices[v].uv);
                    index.add(cnt);
                    cnt++;
                }
            }

            shell.setPositions(new VBO(vertices.toArrayFloat(),3));
            shell.setNormals(new VBO(normals.toArrayFloat(),3));
            shell.setUVs(new VBO(uvs.toArrayFloat(),2));
            shell.setIndex(new IBO(index.toArrayInt()));
            shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, cnt)});

            final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
            material.setDiffuse(Color.BLUE);
            mesh.getMaterials().add(material);
        }

        return node;
    }

}
