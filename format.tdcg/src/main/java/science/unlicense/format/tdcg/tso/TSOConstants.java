
package science.unlicense.format.tdcg.tso;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.impl.Matrix4x4;

/**
 *
 * @author Johann Sorel
 */
public final class TSOConstants {

    public static final byte[] SIGNATURE = new byte[]{'T','S','O','1'};

    public static Matrix4x4 readMatrix(DataInputStream ds) throws IOException{
        final Matrix4x4 m = new Matrix4x4(
                    ds.readFloat(),ds.readFloat(),ds.readFloat(),ds.readFloat(),
                    ds.readFloat(),ds.readFloat(),ds.readFloat(),ds.readFloat(),
                    ds.readFloat(),ds.readFloat(),ds.readFloat(),ds.readFloat(),
                    ds.readFloat(),ds.readFloat(),ds.readFloat(),ds.readFloat()  );
        //matrix are stored in column order, we must transpose it since our implementation
        //was expecting row order in the constructor
        return m.transpose();
    }

    private TSOConstants(){}

}
