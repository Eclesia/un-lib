
package science.unlicense.format.tdcg.tmo;

/**
 *
 * @author Johann Sorel
 */
public final class TMOConstants {

    public static final byte[] SIGNATURE = new byte[]{'T','M','O','1'};

    private TMOConstants(){}

}
