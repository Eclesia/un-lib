
package science.unlicense.format.tdcg.png.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.zlib.ZlibInputStream;
import science.unlicense.format.png.model.Chunk;
import science.unlicense.common.api.number.Endianness;

/**
 * This PNG chunk is a deflate compressed stream containing the various tso files.
 *
 * @author Johann Sorel
 */
public class TaObChunk extends Chunk{

    //header
    public Chars type;
    public int unknown1;
    public int unknown2;
    public int uncompressedSize;
    public int compressedSize;
    //decompressed datas
    public byte[] data;


    @Override
    public void read(DataInputStream ds, int length) throws IOException {
        //read header
        type = new Chars(ds.readFully(new byte[4]));
        unknown1 = ds.readInt(Endianness.LITTLE_ENDIAN);
        unknown2 = ds.readInt(Endianness.LITTLE_ENDIAN);
        uncompressedSize = ds.readInt(Endianness.LITTLE_ENDIAN);
        compressedSize = ds.readInt(Endianness.LITTLE_ENDIAN);

        //unzip data
        final ByteInputStream clip = new ArrayInputStream(ds.readFully(new byte[compressedSize]));
        final DataInputStream deflate = new DataInputStream(new ZlibInputStream(clip));
        data = deflate.readFully(new byte[uncompressedSize]);
        //skip any remaining bytes
        ds.skipFully(length-20-compressedSize);
    }

}
