
package science.unlicense.format.tdcg.tah;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class TAHReader extends AbstractReader {

    private File[] files;
    private int version;
    private int unknown;

    public Object read() throws IOException{
        decodeFile();
        return null;
    }

    private void decodeFile() throws IOException{
        final DataInputStream ds = getInputAsDataStream(Endianness.LITTLE_ENDIAN);

        if (!Arrays.equals(TAHConstants.SIGNATURE, ds.readFully(new byte[4]))){
            throw new IOException(ds, "Stream is not a TAH.");
        }

        files = new File[ds.readInt()];
        version = ds.readInt();
        unknown = ds.readInt();
        for (int i=0;i<files.length;i++){
            files[i] = new File();
            files[i].read(ds);
        }

        //datas are compressed in a variant of LZSS
        final byte[] uncompressed = new byte[ds.readInt()];


    }

    private static class File{
        public int id;
        public int offset;

        void read(DataInputStream ds) throws IOException{
            id = ds.readInt();
            offset = ds.readInt();
        }
    }

}
