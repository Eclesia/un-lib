
package science.unlicense.format.tdcg.pose;

/**
 *
 * @author Johann Sorel
 */
public final class POSEConstants {

    public static final byte[] SIGNATURE = new byte[]{'T','A','H','2'};

    private POSEConstants(){}

}
