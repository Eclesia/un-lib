
package science.unlicense.format.tdcg.tso.model;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class TSOShader {

    public Chars name;
    public Chars code;

    public void read(DataInputStream ds) throws IOException{
        name = ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS);
        final CharBuffer cb = new CharBuffer(CharEncodings.SHIFT_JIS);
        for (int i=0,n=ds.readInt();i<n;i++) cb.append(ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS)).append('\n');
        code = cb.toChars();
    }

}
