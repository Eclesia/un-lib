
package science.unlicense.format.tdcg.png;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.format.png.PNGConstants;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * TSO Bundled model in a png chunk.
 *
 * @author Johann Sorel
 */
public class TPNGFormat extends AbstractModel3DFormat {

    public static final TPNGFormat INSTANCE = new TPNGFormat();

    public TPNGFormat() {
        super(new Chars("tsopng"));
        shortName = new Chars("TSO-PNG");
        longName = new Chars("TSO-PNG");
        extensions.add(new Chars("png"));
        signatures.add(PNGConstants.SIGNATURE);
        isSubset = true;
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    @Override
    public boolean canDecode(Object input) throws IOException {
        if (super.canDecode(input)){
            //check if this png contains an acTL chunk
            boolean isTpng = false;

            final boolean[] closeStream = new boolean[1];
            final ByteInputStream is = IOUtilities.toInputStream(input, closeStream);
            final DataInputStream ds = new DataInputStream(is, Endianness.BIG_ENDIAN);
            //skip signature
            ds.skipFully(8);
            //quick loop on chunks
            while (true){
                //read chunk header
                final int length = ds.readInt();
                final byte[] btype = ds.readFully(new byte[4]);
                final Chars type = new Chars(btype);
                //skip chunk data + crc
                ds.skipFully(length + 4);

                if (TPNGConstants.CHUNK_TaOb.equals(type)){
                    isTpng = true;
                    break;
                } else if (PNGConstants.CHUNK_IEND.equals(type)){
                    //we have finish reading file
                    break;
                }
            }
            if (closeStream[0]) is.dispose();

            return isTpng;
        }
        return false;
    }

    @Override
    public Store open(Object input) throws IOException {
        return new TPNGStore(input);
    }

}
