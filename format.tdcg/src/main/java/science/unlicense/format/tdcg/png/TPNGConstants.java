
package science.unlicense.format.tdcg.png;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class TPNGConstants {

    public static final Chars CHUNK_TaOb = Chars.constant(new byte[]{'t','a','O','b'});

    public static final Chars TYPE_TDCG = Chars.constant("TDCG");
    public static final Chars TYPE_HSAV = Chars.constant("HSAV");
    public static final Chars TYPE_FTSO = Chars.constant("FTSO");


    private TPNGConstants(){}

}
