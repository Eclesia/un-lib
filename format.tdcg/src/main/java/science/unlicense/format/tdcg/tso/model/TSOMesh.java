
package science.unlicense.format.tdcg.tso.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.format.tdcg.tso.TSOConstants;

/**
 *
 * @author Johann Sorel
 */
public class TSOMesh {

    public Chars name;
    public Matrix4x4 trs;
    public int shader;
    public Part[] parts;

    public void read(DataInputStream ds) throws IOException{
        name = ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS);
        trs = TSOConstants.readMatrix(ds);
        shader = ds.readInt();
        parts = new Part[ds.readInt()];

        for (int i=0;i<parts.length;i++){
            parts[i] = new Part();
            parts[i].read(ds);
        }
    }

    public static final class Part{

        public int unknown;
        public int[] joints;
        public TSOVertex[] vertices;

        public void read(DataInputStream ds) throws IOException{
            unknown = ds.readInt();
            joints = ds.readInt(ds.readInt());
            vertices = new TSOVertex[ds.readInt()];
            for (int i=0;i<vertices.length;i++){
                vertices[i] = new TSOVertex();
                vertices[i].read(ds);
            }
        }

    }

}
