
package science.unlicense.format.tdcg.tso.model;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 *
 * @author Johann Sorel
 */
public class TSOTexture {

    public Chars name;
    public Chars path;
    public Image image;


    public void read(DataInputStream ds) throws IOException{
        name = ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS);
        path = ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS);
        final int width = ds.readInt();
        final int height = ds.readInt();
        final int format = ds.readInt();
        final Buffer buffer = DefaultBufferFactory.wrap(ds.readFully(new byte[width*height*format]));
        final ImageModel rm;
        final ImageModel cm;
        if (format==3){
            rm = new InterleavedModel(new UndefinedSystem(3), UInt8.TYPE);
            cm = DerivateModel.create(rm, new int[]{0,1,2}, null, null, ColorSystem.RGB_8BITS);
        } else if (format==4){
            rm = new InterleavedModel(new UndefinedSystem(4), UInt8.TYPE);
            cm = DerivateModel.create(rm, new int[]{0,1,2,3}, null, null, ColorSystem.RGB_8BITS);
        } else {
            throw new IOException(ds, "Unexpected image format "+format);
        }

        image = new DefaultImage(buffer, new Extent.Long(width, height), rm, cm);
    }

}
