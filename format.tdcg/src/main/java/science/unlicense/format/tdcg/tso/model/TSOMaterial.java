
package science.unlicense.format.tdcg.tso.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class TSOMaterial {

    public Chars name;
    public Chars path;
    public Chars[] parameters;

    public void read(DataInputStream ds) throws IOException{
        name = ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS);
        path = ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS);
        parameters = new Chars[ds.readInt()];
        for (int i=0;i<parameters.length;i++){
            parameters[i] = ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS);
        }
    }

}
