
package science.unlicense.format.json;

import science.unlicense.common.api.character.Chars;

/**
 * Store various JSON constants.
 * http://tools.ietf.org/html/rfc4627
 *
 * @author Johann Sorel
 */
public final class JSONConstants {

    public static final int ARRAY_BEGIN = '[';
    public static final int ARRAY_END = ']';
    public static final int OBJECT_BEGIN = '{';
    public static final int OBJECT_END = '}';
    public static final int NAME_SEPARATOR = ':';
    public static final int VALUE_SEPARATOR = ',';

    public static final Chars NULL = Chars.constant("null");
    public static final Chars TRUE = Chars.constant("true");
    public static final Chars FALSE = Chars.constant("false");

    private JSONConstants() {}


}
