
package science.unlicense.format.json;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public class JSONFormat extends DefaultFormat {

    public JSONFormat() {
        super(new Chars("json"));
        shortName = new Chars("JSON");
        longName = new Chars("JavaScript Object Notation");
        mimeTypes.add(new Chars("application/json"));
        extensions.add(new Chars("json"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
