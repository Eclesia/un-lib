
package science.unlicense.format.json;

import science.unlicense.common.api.model.doc.Document;
import science.unlicense.encoding.api.io.IOException;

/**
 * Utility class to compare different json files.
 *
 * @author Johann Sorel
 */
public final class JSONComparator {

    public JSONComparator(){

    }

    public void compare(Object expected, Object result) throws IOException {

        Document expectedDoc = readAsDoc(expected);
        Document resultDoc = readAsDoc(result);

        if (!expectedDoc.equals(resultDoc)) {
            throw new IOException("Documents do not match");
        }

    }

    private Document readAsDoc(Object cdt) throws IOException {
        return JSONUtilities.readAsDocument(cdt, null);
    }

}
