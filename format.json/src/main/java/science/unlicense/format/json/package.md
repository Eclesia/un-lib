
# Notes

# JSON derivates

JSON is widely used and several variant exist.
Here follows a list of derivates and the reasons why they are included or not in Unlicense-Lib

## BSON

http://bsonspec.org/
Created for the needs of MongoDB, BSON is a binary version of JSON with elements proper to MongoDB.
I do not see BSON as a valid stand-alone format. The existance of many obscur types
such as javascript or various string representations (string,cstring) makes it clear that the format
is only an opportunist promotion of MongoDB inner format trying to disguise as a generic binary format.

If the format is one day needed it should be placed in the MongoDB connector module, not in the JSON one.

## YAML

Inspired by JSON and other syntaxes, the format is so different that it should live in it's own module.

## HJSON

A Humanized version of JSON more appropriate for configuration files.

## I-JSON

