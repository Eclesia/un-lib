
package science.unlicense.format.json;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.RegexTokenType;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenGroup;
import science.unlicense.syntax.api.lexer.TokenType;

/**
 *
 * @author Johann Sorel
 */
public final class JSONReader extends AbstractJSONReader {

    private static final TokenType TT_BEGIN_ARRAY       = RegexTokenType.keyword(new Chars("["), new Chars("\\["));
    private static final TokenType TT_END_ARRAY         = RegexTokenType.keyword(new Chars("]"), new Chars("\\]"));
    private static final TokenType TT_BEGIN_OBJECT      = RegexTokenType.keyword(new Chars("{"), new Chars("{"));
    private static final TokenType TT_END_OBJECT        = RegexTokenType.keyword(new Chars("}"), new Chars("}"));
    private static final TokenType TT_NAME_SEPARATOR    = RegexTokenType.keyword(new Chars(":"), new Chars(":"));
    private static final TokenType TT_VALUE_SEPARATOR   = RegexTokenType.keyword(new Chars(","), new Chars(","));
    private static final TokenType TT_WHITESPACE        = RegexTokenType.space();
    private static final TokenType TT_TRUE              = RegexTokenType.keyword(new Chars("true"), new Chars("true"));
    private static final TokenType TT_FALSE             = RegexTokenType.keyword(new Chars("false"), new Chars("false"));
    private static final TokenType TT_NULL              = RegexTokenType.keyword(new Chars("null"), new Chars("null"));
    private static final TokenType TT_NUMBER            = RegexTokenType.decimal();
    private static final TokenType TT_STRING            = RegexTokenType.keyword(new Chars("word"), new Chars("\\\"(([^\"])|(\\\\\\\"))*\\\""));

    private static final Chars ESC = Chars.constant("\\\"");
    private static final Chars ESC_R = Chars.constant("\"");

    private Lexer lexer;
    private JSONElement next;
    //contain where the cursor is : in array=1, in object=0
    private final Sequence stateStack = new ArraySequence();
    private boolean nextIsName = false;

    @Override
    public boolean hashNext() throws IOException{
        findNext();
        return next != null;
    }

    @Override
    public JSONElement next() throws IOException{
        findNext();
        if (next == null) throw new IOException(this, "No more elements");
        final JSONElement temp = next;
        next = null;
        return temp;
    }

    private void findNext() throws IOException{
        if (next!=null) return;

        if (lexer==null) {
            lexer = new Lexer();
            final TokenGroup tts = lexer.getTokenGroup();
            tts.addTokenType(TT_BEGIN_ARRAY);
            tts.addTokenType(TT_END_ARRAY);
            tts.addTokenType(TT_BEGIN_OBJECT);
            tts.addTokenType(TT_END_OBJECT);
            tts.addTokenType(TT_NAME_SEPARATOR);
            tts.addTokenType(TT_VALUE_SEPARATOR);
            tts.addTokenType(TT_WHITESPACE);
            tts.addTokenType(TT_TRUE);
            tts.addTokenType(TT_FALSE);
            tts.addTokenType(TT_NULL);
            tts.addTokenType(TT_NUMBER);
            tts.addTokenType(TT_STRING);
            lexer.setInput(getInput());
            lexer.init();
        }

        while (next == null) {
            final Token token = lexer.next();
            if (token == null) return;

            if (token.type == TT_BEGIN_ARRAY) {
                next = new JSONElement(JSONElement.TYPE_ARRAY_BEGIN, null);
                stateStack.add(1);
            } else if (token.type == TT_END_ARRAY) {
                next = new JSONElement(JSONElement.TYPE_ARRAY_END, null);
                stateStack.remove(stateStack.getSize()-1);
            } else if (token.type == TT_BEGIN_OBJECT) {
                nextIsName=true;
                next = new JSONElement(JSONElement.TYPE_OBJECT_BEGIN, null);
                stateStack.add(0);
            } else if (token.type == TT_END_OBJECT) {
                nextIsName=true;
                next = new JSONElement(JSONElement.TYPE_OBJECT_END, null);
                stateStack.remove(stateStack.getSize()-1);
            } else if (token.type == TT_NAME_SEPARATOR) {
                nextIsName=false;
                //do nothing skip it
                //TODO ensure we are after a name
            } else if (token.type == TT_VALUE_SEPARATOR) {
                nextIsName=true;
                //do nothing skip it
                //TODO ensure we are in array or object
            } else if (token.type == TT_WHITESPACE) {
                //do nothing skip it
            } else if (token.type == TT_TRUE) {
                next = new JSONElement(JSONElement.TYPE_VALUE, true);
            } else if (token.type == TT_FALSE) {
                next = new JSONElement(JSONElement.TYPE_VALUE, false);
            } else if (token.type == TT_NULL) {
                next = new JSONElement(JSONElement.TYPE_VALUE, null);
            } else if (token.type == TT_NUMBER) {
                Object value;
                try {
                    value = Int32.decode(token.value);
                } catch(RuntimeException ex) { //TODO change this exception type
                    value = Float64.decode(token.value);
                }
                next = new JSONElement(JSONElement.TYPE_VALUE, value);
            } else if (token.type == TT_STRING) {
                if (nextIsName && (Integer) stateStack.get(stateStack.getSize()-1) == 0) {
                    next = new JSONElement(JSONElement.TYPE_NAME, trimQuotes(token.value));
                } else {
                    next = new JSONElement(JSONElement.TYPE_VALUE, trimQuotes(token.value));
                }
            } else {
                throw new IOException(this, "Unexpected token : "+token);
            }
        }

    }

    private static Chars trimQuotes(Chars candidate) {
        Chars str = candidate.truncate(1, candidate.getCharLength()-1);
        str = str.replaceAll(ESC, ESC_R);
        return str;
    }

}
