
package science.unlicense.format.json;

import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;

/**
 * Common parent for JSon, HJson and other json variants.
 *
 * @author Johann Sorel
 */
public abstract class AbstractJSONReader extends AbstractReader {

    public abstract boolean hashNext() throws IOException;

    public abstract JSONElement next() throws IOException;

}
