
package science.unlicense.format.json;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.RegexTokenType;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenGroup;
import science.unlicense.syntax.api.lexer.TokenType;

/**
 *
 * @author Johann Sorel
 */
public final class HJSONReader extends AbstractJSONReader {

    private static final TokenType TT_BEGIN_ARRAY       = RegexTokenType.keyword(new Chars("["), new Chars("\\["));
    private static final TokenType TT_END_ARRAY         = RegexTokenType.keyword(new Chars("]"), new Chars("\\]"));
    private static final TokenType TT_BEGIN_OBJECT      = RegexTokenType.keyword(new Chars("{"), new Chars("{"));
    private static final TokenType TT_END_OBJECT        = RegexTokenType.keyword(new Chars("}"), new Chars("}"));
    private static final TokenType TT_NAME_SEPARATOR    = RegexTokenType.keyword(new Chars(":"), new Chars(":"));
    private static final TokenType TT_VALUE_SEPARATOR   = RegexTokenType.keyword(new Chars(","), new Chars(","));
    private static final TokenType TT_SPACE             = RegexTokenType.keyword(new Chars("sp"), new Chars("( |\\t|\\r)+"));
    private static final TokenType TT_NEWLINE           = RegexTokenType.keyword(new Chars("nl"), new Chars("\\n"));
    private static final TokenType TT_TRUE              = RegexTokenType.keyword(new Chars("true"), new Chars("true"));
    private static final TokenType TT_FALSE             = RegexTokenType.keyword(new Chars("false"), new Chars("false"));
    private static final TokenType TT_NULL              = RegexTokenType.keyword(new Chars("null"), new Chars("null"));
    private static final TokenType TT_NUMBER            = RegexTokenType.decimal();
    private static final TokenType TT_STRING            = RegexTokenType.keyword(new Chars("quoteString"), new Chars("\\\"(([^\"])|(\\\\\\\"))*\\\""));
    private static final TokenType TT_QUOTELESSS        = RegexTokenType.keyword(new Chars("quotelessString"), new Chars("[^ \\r\\n\\,\\:\\[\\]\\{\\}][^\\r\\n\\,\\:\\[\\]\\{\\}]*"));
    private static final TokenType TT_MULTILINE         = RegexTokenType.keyword(new Chars("multiString"), new Chars("\\'\\'\\'([^\\']|(\\'[^\\'])|(\\'\\'[^\\']))*\\'\\'\\'"));
    private static final TokenType TT_COMMENT1          = RegexTokenType.keyword(new Chars("comment1"), new Chars("#[^\\n]*"));
    private static final TokenType TT_COMMENT2          = RegexTokenType.keyword(new Chars("comment2"), new Chars("//[^\\n]*"));
    private static final TokenType TT_COMMENT3          = RegexTokenType.keyword(new Chars("comment3"), new Chars("/\\*([^\\*]|(\\*[^/]))*\\*/"));

    private Lexer lexer;
    private JSONElement next;
    //contain where the cursor is : in array=1, in object=0
    private final Sequence stateStack = new ArraySequence();
    private boolean nextIsName = false;

    @Override
    public boolean hashNext() throws IOException{
        findNext();
        return next != null;
    }

    @Override
    public JSONElement next() throws IOException{
        findNext();
        if (next==null) throw new IOException(this, "No more elements");
        final JSONElement temp = next;
        next = null;
        return temp;
    }

    private void findNext() throws IOException{
        if (next != null) return;

        if (lexer == null) {
            lexer = new Lexer();
            final TokenGroup tts = lexer.getTokenGroup();
            tts.addTokenType(TT_COMMENT1);
            tts.addTokenType(TT_COMMENT2);
            tts.addTokenType(TT_COMMENT3);
            tts.addTokenType(TT_BEGIN_ARRAY);
            tts.addTokenType(TT_END_ARRAY);
            tts.addTokenType(TT_BEGIN_OBJECT);
            tts.addTokenType(TT_END_OBJECT);
            tts.addTokenType(TT_NAME_SEPARATOR);
            tts.addTokenType(TT_VALUE_SEPARATOR);
            tts.addTokenType(TT_SPACE);
            tts.addTokenType(TT_NEWLINE);
            tts.addTokenType(TT_TRUE);
            tts.addTokenType(TT_FALSE);
            tts.addTokenType(TT_NULL);
            tts.addTokenType(TT_NUMBER);
            tts.addTokenType(TT_STRING);
            tts.addTokenType(TT_QUOTELESSS);
            tts.addTokenType(TT_MULTILINE);
            lexer.setInput(getInput());
            lexer.init();
        }

        while (next == null) {
            final Token token = lexer.next();
            if (token == null) return;

            if (token.type==TT_BEGIN_ARRAY) {
                next = new JSONElement(JSONElement.TYPE_ARRAY_BEGIN, null);
                stateStack.add(1);
            } else if (token.type==TT_END_ARRAY) {
                next = new JSONElement(JSONElement.TYPE_ARRAY_END, null);
                stateStack.remove(stateStack.getSize()-1);
            } else if (token.type==TT_BEGIN_OBJECT) {
                nextIsName=true;
                next = new JSONElement(JSONElement.TYPE_OBJECT_BEGIN, null);
                stateStack.add(0);
            } else if (token.type==TT_END_OBJECT) {
                nextIsName=true;
                next = new JSONElement(JSONElement.TYPE_OBJECT_END, null);
                stateStack.remove(stateStack.getSize()-1);
            } else if (token.type==TT_NAME_SEPARATOR) {
                nextIsName=false;
                //do nothing skip it
                //TODO ensure we are after a name
            } else if (token.type==TT_VALUE_SEPARATOR) {
                nextIsName=true;
                //do nothing skip it
                //TODO ensure we are in array or object
            } else if (token.type==TT_SPACE) {
                //do nothing skip it
            } else if (token.type==TT_NEWLINE) {
                nextIsName=true;
                //do nothing skip it
            } else if (token.type==TT_TRUE) {
                next = new JSONElement(JSONElement.TYPE_VALUE, true);
            } else if (token.type==TT_FALSE) {
                next = new JSONElement(JSONElement.TYPE_VALUE, false);
            } else if (token.type==TT_NULL) {
                next = new JSONElement(JSONElement.TYPE_VALUE, null);
            } else if (token.type==TT_NUMBER) {
                Object value;
                try{
                    value = Int32.decode(token.value);
                }catch(RuntimeException ex) { //TODO change this exception type
                    value = Float64.decode(token.value);
                }
                next = new JSONElement(JSONElement.TYPE_VALUE, value);
            } else if (token.type==TT_STRING) {
                if (nextIsName && (Integer) stateStack.get(stateStack.getSize()-1) == 0) {
                    next = new JSONElement(JSONElement.TYPE_NAME, trimQuotes(token.value));
                } else {
                    next = new JSONElement(JSONElement.TYPE_VALUE, trimQuotes(token.value));
                }
            } else if (token.type==TT_MULTILINE) {
                if (nextIsName && (Integer) stateStack.get(stateStack.getSize()-1) == 0) {
                    throw new IOException(this, "Multiline string can only be used as value : "+token);
                } else {
                    next = new JSONElement(JSONElement.TYPE_VALUE, token.value.truncate(3, token.value.getCharLength()-3));
                }
            } else if (token.type==TT_QUOTELESSS) {
                if (nextIsName && (Integer) stateStack.get(stateStack.getSize()-1) == 0) {
                    next = new JSONElement(JSONElement.TYPE_NAME, token.value.trim());
                } else {
                    next = new JSONElement(JSONElement.TYPE_VALUE, token.value);
                }
            } else if (token.type==TT_COMMENT1) {
                next = new JSONElement(JSONElement.TYPE_COMMENT, token.value.truncate(1, -1));
            } else if (token.type==TT_COMMENT2) {
                next = new JSONElement(JSONElement.TYPE_COMMENT, token.value.truncate(2, -1));
            } else if (token.type==TT_COMMENT3) {
                next = new JSONElement(JSONElement.TYPE_COMMENT, token.value.truncate(2, token.value.getCharLength()-2));
            } else {
                throw new IOException(this, "Unexpected token : "+token);
            }
        }

    }

    private static Chars trimQuotes(Chars candidate) {
        return candidate.truncate(1, candidate.getCharLength()-1);
    }

}
