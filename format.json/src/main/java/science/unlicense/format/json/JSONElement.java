

package science.unlicense.format.json;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;

/**
 *
 * @author Johann Sorel
 */
public class JSONElement extends CObject{

    public static final int TYPE_ARRAY_BEGIN = 1;
    public static final int TYPE_ARRAY_END = 2;
    public static final int TYPE_OBJECT_BEGIN = 3;
    public static final int TYPE_OBJECT_END = 4;
    public static final int TYPE_NAME = 5;
    public static final int TYPE_VALUE = 6;
    /**
     * This type is used by JSON variants such as HJSON.
     */
    public static final int TYPE_COMMENT = 7;


    private final int type;
    private final Object value;

    public JSONElement(int type, Object value) {
        this.type = type;
        this.value = value;
    }

    public int getType() {
        return type;
    }

    /**
     * Values can be :
     * - Boolean.TRUE
     * - Boolean.FALSE
     * - null
     * - Chars
     * - Double
     * - Integer
     */
    public Object getValue() {
        return value;
    }

    public Chars toChars() {
        return new CharBuffer()
                .append(Int32.encode(type))
                .append(' ')
                .append(CObjects.toChars(value))
                .toChars();
    }

}
