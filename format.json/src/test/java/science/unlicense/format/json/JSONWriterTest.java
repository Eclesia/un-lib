

package science.unlicense.format.json;

import science.unlicense.format.json.JSONWriter;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class JSONWriterTest {

    @Test
    public void testWrite() throws IOException{

        final ArrayOutputStream out = new ArrayOutputStream();

        final JSONWriter writer = new JSONWriter();
        writer.setOutput(out);

        writer.writeObjectBegin();

        writer.writeName(new Chars("propNull"));
        writer.writeValue(null);

        writer.writeName(new Chars("propTrue"));
        writer.writeValue(true);

        writer.writeName(new Chars("propFalse"));
        writer.writeValue(false);

        writer.writeName(new Chars("propNumber"));
        writer.writeValue(45.3);

        writer.writeName(new Chars("propArray"));
        writer.writeArrayBegin();
        writer.writeValue(new Chars("text1"));
        writer.writeValue(new Chars("text2"));
        writer.writeArrayEnd();

        writer.writeName(new Chars("propString"));
        writer.writeValue(new Chars("some text"));

        writer.writeObjectEnd();

        final Chars result = new Chars(out.getBuffer().toArrayByte(),CharEncodings.UTF_8);

        Assert.assertEquals(new Chars("{\"propNull\":null,\"propTrue\":true,\"propFalse\":false,\"propNumber\":45.3,\"propArray\":[\"text1\",\"text2\"],\"propString\":\"some text\"}"),result);

    }

    @Test
    public void testWriteFormattedObjectEmpty() throws IOException{

        final ArrayOutputStream out = new ArrayOutputStream();

        final JSONWriter writer = new JSONWriter();
        writer.setOutput(out);
        writer.setFormatted(true);

        writer.writeObjectBegin();

        writer.writeName(new Chars("user"));
        writer.writeObjectBegin();
        writer.writeObjectEnd();

        writer.writeObjectEnd();

        final Chars result = new Chars(out.getBuffer().toArrayByte(),CharEncodings.UTF_8);

        Assert.assertEquals(new Chars(
            "{\n" +
            "\t\"user\":{}\n" +
            "}"),result);
    }

    @Test
    public void testWriteFormattedArrayEmpty() throws IOException{

        final ArrayOutputStream out = new ArrayOutputStream();

        final JSONWriter writer = new JSONWriter();
        writer.setOutput(out);
        writer.setFormatted(true);

        writer.writeObjectBegin();

        writer.writeName(new Chars("vals"));
        writer.writeArrayBegin();
        writer.writeArrayEnd();

        writer.writeObjectEnd();

        final Chars result = new Chars(out.getBuffer().toArrayByte(),CharEncodings.UTF_8);

        Assert.assertEquals(new Chars(
            "{\n" +
            "\t\"vals\":[]\n" +
            "}"),result);
    }

    @Test
    public void testWriteFormattedTest2() throws IOException{

        final ArrayOutputStream out = new ArrayOutputStream();

        final JSONWriter writer = new JSONWriter();
        writer.setOutput(out);
        writer.setFormatted(true);
        writer.setIndentation(new Chars("    "));

        writer.writeObjectBegin();

        writer.writeName(new Chars("propNull"));
        writer.writeValue(null);

        writer.writeName(new Chars("propTrue"));
        writer.writeValue(true);

        writer.writeName(new Chars("propFalse"));
        writer.writeValue(false);

        writer.writeName(new Chars("propNumber"));
        writer.writeValue(45.3);

        writer.writeName(new Chars("propArray"));
        writer.writeArrayBegin();
        writer.writeValue(new Chars("text1"));
        writer.writeValue(new Chars("text2"));
        writer.writeArrayEnd();


        writer.writeName(new Chars("propString"));
        writer.writeValue(new Chars("some text"));

        writer.writeObjectEnd();

        final Chars result = new Chars(out.getBuffer().toArrayByte(),CharEncodings.UTF_8);

        Assert.assertEquals(new Chars(
            "{\n" +
            "    \"propNull\":null,\n" +
            "    \"propTrue\":true,\n" +
            "    \"propFalse\":false,\n" +
            "    \"propNumber\":45.3,\n" +
            "    \"propArray\":[\n" +
            "        \"text1\",\n" +
            "        \"text2\"\n" +
            "    ],\n" +
            "    \"propString\":\"some text\"\n" +
            "}"),result);

    }

    @Test
    public void testWriteFormattedArrayOfValues() throws IOException{

        final ArrayOutputStream out = new ArrayOutputStream();

        final JSONWriter writer = new JSONWriter();
        writer.setOutput(out);
        writer.setFormatted(true);

        writer.writeObjectBegin();

        writer.writeName(new Chars("vals"));
        writer.writeArrayBegin();

        writer.writeValue(new Chars("text1"));
        writer.writeValue(new Chars("text2"));

        writer.writeArrayEnd();
        writer.writeObjectEnd();

        final Chars result = new Chars(out.getBuffer().toArrayByte(),CharEncodings.UTF_8);

        Assert.assertEquals(new Chars(
            "{\n" +
            "\t\"vals\":[\n" +
            "\t\t\"text1\",\n" +
            "\t\t\"text2\"\n" +
            "\t]\n" +
            "}"),result);

    }

    @Test
    public void testWriteFormattedArrayOfObjects() throws IOException{

        final ArrayOutputStream out = new ArrayOutputStream();

        final JSONWriter writer = new JSONWriter();
        writer.setOutput(out);
        writer.setFormatted(true);

        writer.writeObjectBegin();

        writer.writeName(new Chars("vals"));
        writer.writeArrayBegin();

        writer.writeObjectBegin();
        writer.writeName(new Chars("name"));
        writer.writeValue(new Chars("text1"));
        writer.writeObjectEnd();

        writer.writeObjectBegin();
        writer.writeName(new Chars("name"));
        writer.writeValue(new Chars("text2"));
        writer.writeObjectEnd();

        writer.writeArrayEnd();
        writer.writeObjectEnd();

        final Chars result = new Chars(out.getBuffer().toArrayByte(),CharEncodings.UTF_8);

        Assert.assertEquals(new Chars(
            "{\n" +
            "\t\"vals\":[\n" +
            "\t\t{\n"+
            "\t\t\t\"name\":\"text1\"\n" +
            "\t\t},\n"+
            "\t\t{\n"+
            "\t\t\t\"name\":\"text2\"\n" +
            "\t\t}\n"+
            "\t]\n" +
            "}"),result);

    }

}
