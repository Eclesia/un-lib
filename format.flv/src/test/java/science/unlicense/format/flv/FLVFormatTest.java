

package science.unlicense.format.flv;

import science.unlicense.format.flv.FLVFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.media.api.MediaFormat;
import science.unlicense.media.api.Medias;

/**
 * Test FLV Format declaration.
 *
 * @author Johann Sorel
 */
public class FLVFormatTest {

    @Test
    public void testFormat(){
        final MediaFormat[] formats = Medias.getFormats();
        for (MediaFormat format : formats){
            if (format instanceof FLVFormat){
                return;
            }
        }

        Assert.fail("FLV Format not found.");
    }

}
