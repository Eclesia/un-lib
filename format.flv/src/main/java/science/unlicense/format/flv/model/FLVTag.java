

package science.unlicense.format.flv.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.flv.FLVConstants;
import science.unlicense.format.swf.SWFUtilities;

/**
 *
 * @author Johann Sorel
 */
public class FLVTag {

    public int reserved;
    public int filter;
    public int tagType;
    public int dataSize;
    public int timestamp;
    public int timestampExtended;
    public int streamId;

    //sub tags
    public FLVAudioTagHeader audioTagHeader;
    public FLVVideoTagHeader videoTagHeader;
    public FLVEncryptionHeader encryptionHeader;
    public FLVFilterParams filterParams;

    //datas
    public FLVAudioData audioData;
    public FLVVideoData videoData;
    public FLVScriptData scriptData;

    public int totalSize;

    public void read(DataInputStream ds) throws IOException {
        reserved = SWFUtilities.readUB(ds, 2);
        filter = SWFUtilities.readUB(ds, 1);
        tagType = SWFUtilities.readUB(ds, 5);
        dataSize = ds.readUInt24();
        timestamp = ds.readUInt24();
        timestampExtended = ds.readUByte();
        streamId = ds.readUInt24();

        //read sub tags
        if (FLVConstants.TAG_TYPE_AUDIO == tagType){
            audioTagHeader = new FLVAudioTagHeader();
            audioTagHeader.read(ds);
        } else if (FLVConstants.TAG_TYPE_VIDEO == tagType){
            videoTagHeader = new FLVVideoTagHeader();
            videoTagHeader.read(ds);
        }
        if (filter == 1){
            encryptionHeader = new FLVEncryptionHeader();
            encryptionHeader.read(ds);
            filterParams = new FLVFilterParams();
            filterParams.read(ds);
        }

        if (FLVConstants.TAG_TYPE_AUDIO == tagType){
            audioData = new FLVAudioData();
            audioData.read(ds);
        } else if (FLVConstants.TAG_TYPE_VIDEO == tagType){
            videoData = new FLVVideoData();
            videoData.read(ds);
        } else if (FLVConstants.TAG_TYPE_SCRIPT == tagType){
            scriptData = new FLVScriptData();
            scriptData.read(ds);
        }

        totalSize = ds.readInt();
    }

}
