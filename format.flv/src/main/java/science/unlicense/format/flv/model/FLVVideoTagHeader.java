
package science.unlicense.format.flv.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.flv.FLVConstants;
import science.unlicense.format.swf.SWFUtilities;

/**
 *
 * @author Johann Sorel
 */
public class FLVVideoTagHeader {

    public int frameType;
    public int codecId;
    public int avcPacketType;
    public int compositionTime;

    public void read(DataInputStream ds) throws IOException {
        frameType = SWFUtilities.readUB(ds, 4);
        codecId = SWFUtilities.readUB(ds, 4);
        if (codecId == FLVConstants.CODEC_AVC){
            avcPacketType = ds.readUByte();
            compositionTime = ds.readUInt24();
        }
    }

}
