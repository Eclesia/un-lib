
package science.unlicense.format.flv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.flv.FLVConstants;

/**
 *
 * @author Johann Sorel
 */
public class FLVHeader {

    public Chars signature;
    public byte version;
    public byte flags;
    public int dataOffset;

    public void read(DataInputStream ds) throws IOException{
        signature = new Chars(ds.readFully(new byte[3]));
        version = ds.readByte();
        flags = ds.readByte();
        dataOffset = ds.readInt();
    }

    public boolean hasAudio(){
        return (flags & FLVConstants.HEADER_AUDIO_MASK) != 0;
    }

    public boolean hasVideo(){
        return (flags & FLVConstants.HEADER_VIDEO_MASK) != 0;
    }

}
