

package science.unlicense.format.flv;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;

/**
 * Specification :
 * http://www.adobe.com/devnet/f4v.html
 * http://download.macromedia.com/f4v/video_file_format_spec_v10_1.pdf
 *
 * @author Johann Sorel
 */
public class FLVFormat extends AbstractMediaFormat{

    public static final FLVFormat INSTANCE = new FLVFormat();

    public FLVFormat() {
        super(new Chars("flv"));
        shortName = new Chars("FLV");
        longName = new Chars("FLV");
        mimeTypes.add(new Chars("video/x-flv"));
        extensions.add(new Chars("flv"));
        resourceTypes.add(Media.class);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public Store open(Object input) throws IOException {
        return new FLVStore((Path) input);
    }

}
