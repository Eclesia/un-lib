
package science.unlicense.format.flv;

import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.flv.model.FLVTag;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class FLVTagReader extends AbstractReader {

    private boolean start = true;

    public FLVTag next() throws IOException {
        final DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);

        if (start){
            //skip the first 'previous tag size'
            start = false;
            ds.readInt();
        }

        final FLVTag tag = new FLVTag();
        tag.read(ds);

        return tag;
    }

}
