

package science.unlicense.format.flv;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;
import science.unlicense.format.flv.model.FLVHeader;

/**
 *
 * @author Johann Sorel
 */
public class FLVStore extends AbstractStore implements Media {

    private final Path path;

    private FLVHeader header;

    public FLVStore(Path path) {
        super(FLVFormat.INSTANCE,path);
        this.path = path;
        try {
            read();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void read() throws IOException{
        final ByteInputStream bs = path.createInputStream();
        final DataInputStream ds = new DataInputStream(bs,Endianness.BIG_ENDIAN);

        header = new FLVHeader();
        header.read(ds);
    }

    @Override
    public Track[] getTracks() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
