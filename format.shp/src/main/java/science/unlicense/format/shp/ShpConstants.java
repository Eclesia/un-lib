

package science.unlicense.format.shp;

/**
 * SHP constants.
 *
 * @author Samuel Andrés
 * @author Johann Sorel
 */
public final class ShpConstants {

    private ShpConstants(){}

    public static final int TYPE_NONE = 0;

    public static final int TYPE_POINT = 1;
    public static final int TYPE_POLYLINE = 3;
    public static final int TYPE_POLYGON = 5;
    public static final int TYPE_MULTIPOINT = 8;

    public static final int TYPE_POINT_Z = 11;
    public static final int TYPE_POLYLINE_Z = 13;
    public static final int TYPE_POLYGON_Z = 15;
    public static final int TYPE_MULTIPOINT_Z = 18;

    public static final int TYPE_POINT_M = 21;
    public static final int TYPE_POLYLINE_M = 23;
    public static final int TYPE_POLYGON_M = 25;
    public static final int TYPE_MULTIPOINT_M = 28;

    public static final int TYPE_MULTIPATCH = 31;

}
