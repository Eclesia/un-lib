package science.unlicense.format.shp;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.FormatEncodingException;
import science.unlicense.geometry.api.BBox;

/**
 * ESRI Shapefile header (.shp).
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShpHeader extends CObject {

    /**
     * <div class="fr">Longueur d'une en-tête.</div>
     */
    public static final int HEADER_LENGTH = 100;

    /**
     * Total file length in 16bits words.
     */
    public int fileLength;

    /**
     * Geometric type
     */
    public int shapeType;

    /**
     * Shapefile datas bounding box, in axis order : X,Y,Z,M
     * bbox Z and M are defined even if they are not declared by the shape type.
     */
    public BBox bbox;

    /**
     * <div class="fr">Lecture de l'en-tête.</div>
     *
     * @param ds
     * @throws IOException
     */
    public void read(final DataInputStream ds) throws IOException {
        ds.setEndianness(Endianness.BIG_ENDIAN);
        //file signature check
        if ( ds.readInt() != 9994) throw new FormatEncodingException(ds, "file code must be 9994");
        //5 unused ints
        for (int i=0; i<5; i++){
            if (ds.readInt() != 0) throw new FormatEncodingException(ds, "bytes from 5 to 20 must not be used");
        }
        fileLength = ds.readInt();

        ds.setEndianness(Endianness.LITTLE_ENDIAN);
        if (ds.readInt() != 1000) throw new IllegalStateException("file version must be 1000");
        shapeType = ds.readInt();
        bbox = new BBox(4);
        bbox.getLower().set(0, ds.readDouble());
        bbox.getLower().set(1, ds.readDouble());
        bbox.getUpper().set(0, ds.readDouble());
        bbox.getUpper().set(1, ds.readDouble());
        bbox.getLower().set(2, ds.readDouble());
        bbox.getUpper().set(2, ds.readDouble());
        bbox.getLower().set(3, ds.readDouble());
        bbox.getUpper().set(3, ds.readDouble());

    }

    public void write(DataOutputStream ds) throws IOException {
        ds.setEndianness(Endianness.BIG_ENDIAN);
        ds.writeInt(9994);
        ds.skipFully(20);
        ds.writeInt(fileLength);
        ds.setEndianness(Endianness.LITTLE_ENDIAN);
        ds.writeInt(1000);
        ds.writeInt(shapeType);
        ds.writeDouble(bbox.getMin(0));
        ds.writeDouble(bbox.getMin(1));
        ds.writeDouble(bbox.getMax(0));
        ds.writeDouble(bbox.getMax(1));
        ds.writeDouble(bbox.getMin(2));
        ds.writeDouble(bbox.getMax(2));
        ds.writeDouble(bbox.getMin(3));
        ds.writeDouble(bbox.getMax(3));
    }

    @Override
    public Chars toChars() {
        return new Chars("ShpHeader{" + "length=" + fileLength + ", shapeType=" + shapeType + ", bbox=" +bbox+ '}');
    }
}
