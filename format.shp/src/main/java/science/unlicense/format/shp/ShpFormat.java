

package science.unlicense.format.shp;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * ESRI Shapefile format.
 *
 * Resource :
 *
 * @author Johann Sorel
 */
public class ShpFormat extends DefaultFormat {

    public ShpFormat() {
        super(new Chars("shp"));
        shortName = new Chars("SHP");
        longName = new Chars("ESRI shapefile");
        extensions.add(new Chars("shp"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
