package science.unlicense.format.shp;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShxWriter extends AbstractWriter {

    /**
     * Write header.
     *
     * @param header
     * @throws IOException
     */
    public void writeHeader(final ShpHeader header) throws IOException {
        final DataOutputStream ds = getOutputAsDataStream(Endianness.BIG_ENDIAN);
        header.write(ds);
    }

    public void writeData(Iterator records) throws IOException {

        final DataOutputStream ds = getOutputAsDataStream(Endianness.BIG_ENDIAN);

        while (records.hasNext()) {
            final int[] record = (int[]) records.next();
            ds.writeInt(record[0]);
            ds.writeInt(record[1]);
        }
        ds.flush();
    }
}
