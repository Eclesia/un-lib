package science.unlicense.format.shp;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.shp.ShpConstants.*;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShpWriter extends AbstractWriter {

    /**
     * Write file header.
     *
     * @param header
     * @throws IOException
     */
    public void writeHeader(final ShpHeader header) throws IOException {
        header.write(getOutputAsDataStream(Endianness.BIG_ENDIAN));
    }

    public void writeData(Iterator records) throws IOException {

        final DataOutputStream ds = getOutputAsDataStream(Endianness.BIG_ENDIAN);

        while (records.hasNext()) {
            final ShpRecordOld record = (ShpRecordOld) records.next();
            writeRecordHeaderField(ds, record.recordNumber);

            final int recordLength = record.recordLength;
            writeRecordHeaderField(ds, recordLength);

            writeRecordData(ds, record.shapeType, record.geometry, record.bbox,
                    record.zRange, record.zValues, record.measureRange, record.measureValues,
                    record.partTypes);
        }

        ds.flush();
    }

    //==================================================================================================================
    // En-tête d'enregistrement
    //==================================================================================================================

    private static void writeRecordHeaderField(final DataOutputStream ds, int value) throws IOException {
        ds.setEndianness(Endianness.BIG_ENDIAN);
        ds.writeInt(value);
    }

    //==================================================================================================================
    // Enregistrement
    //==================================================================================================================

    private static void writeRecordData(final DataOutputStream ds, int shapeType, Object data, double[] bbox,
            double[] zRange, double[] zValues, double[] measureRange, double[] measureValues, int[] partTypes) throws IOException{

        // type de forme
        ds.setEndianness(Endianness.LITTLE_ENDIAN);
        ds.writeInt(shapeType);

        writeBbox(ds, shapeType, bbox);

        writeGeometry(ds, shapeType, data, partTypes);

        if (zRange!=null){
            writeArray(ds, zRange);
        }
        if (zValues!=null){
            writeArray(ds, zValues);
        }
        if (measureRange!=null){
            writeArray(ds, measureRange);
        }
        if (measureValues!=null){
            writeArray(ds, measureValues);
        }
    }

    //==================================================================================================================
    // BBox d'enregistrement
    //==================================================================================================================

    private static void writeBbox(final DataOutputStream ds, final int shapeType, final double[] bbox) throws IOException{
        switch(shapeType){
            case TYPE_NONE: return;
            case TYPE_POINT: return;
            case TYPE_POLYLINE:
            case TYPE_POLYGON:
            case TYPE_MULTIPOINT:

            case TYPE_POINT_Z:
            case TYPE_POLYLINE_Z:
            case TYPE_POLYGON_Z:
            case TYPE_MULTIPOINT_Z:

            case TYPE_POINT_M:
            case TYPE_POLYLINE_M:
            case TYPE_POLYGON_M:
            case TYPE_MULTIPOINT_M: writeArray(ds, bbox); return;

            case TYPE_MULTIPATCH:
            default: throw new IllegalArgumentException("unknown shape type "+shapeType);
        }
    }

    //==================================================================================================================
    // Géométrie d'enregistrement
    //==================================================================================================================


    static void writeGeometry(final DataOutputStream ds, final int shapeType, final Object data, final int[] partTypes) throws IOException{
        switch(shapeType){
            case TYPE_NONE: return;
            case TYPE_POINT:
            case TYPE_POINT_M:
            case TYPE_POINT_Z: writePoint(ds, (double[]) data); return;
            case TYPE_POLYLINE:
            case TYPE_POLYLINE_M:
            case TYPE_POLYLINE_Z: writePolyLine(ds, (double[][][]) data); return;
            case TYPE_POLYGON:
            case TYPE_POLYGON_M:
            case TYPE_POLYGON_Z: writePolygon(ds, (double[][][]) data); return;
            case TYPE_MULTIPOINT:
            case TYPE_MULTIPOINT_M:
            case TYPE_MULTIPOINT_Z: writeMultiPoint(ds, (double[][]) data); return;
            case TYPE_MULTIPATCH: writeMultiPatch(ds, (double[][][]) data, partTypes); return;
            default: throw new IllegalArgumentException("unknown shape type "+shapeType);
        }
    }

    /**
     * <div class="fr">Écriture d'un polygone. La structure des enregistrements est identique à celle des polylignes.
     * On considère seulement que les anneaux extérieurs sont donnés dans le sens des aiguilles d'une montre et les
     * anneaux intérieurs dans le sens contraire. De même, les suites de point définissant les anneaux doivent
     * explicitement finir par le même vertex que leur vertex de départ.</div>
     *
     * @param ds
     * @param polygon
     * @throws IOException
     */
    private static void writePolygon(final DataOutputStream ds, double[][][] polygon) throws IOException{
        writePolyLine(ds, polygon);
    }

    /**
     * <div class="fr">Écriture d'une polyligne.</div>
     *
     * @param ds
     * @param polyLine
     * @throws IOException
     */
    private static void writePolyLine(final DataOutputStream ds, double[][][] polyLine) throws IOException{

        // Détermination des indices de départ et du nombre de points

        // on prépare un tableau pour enregistrer les indices de départ
        final int[] parts = new int[polyLine.length];

        // on prépare le décompte du nombre de points
        int nbPoints = 0;

        // on parcourt les lignes de la polyligne
        for (int i=0; i<polyLine.length; i++){
            // indice de début de la ligne dans les points de la polyligne
            parts[i]=nbPoints;
            for (int j=0; j<polyLine[i].length; j++){
                nbPoints++; // incrémentation du nombre de points
            }
        }

        ds.setEndianness(Endianness.LITTLE_ENDIAN);

        // nombre de lignes dans la polyligne
        ds.writeInt(polyLine.length);

        // nombre de points
        ds.writeInt(nbPoints);

        // écriture des indices de départ
        ds.writeInt(parts);

        // points des lignes de la polyligne
        for (int i=0; i<polyLine.length; i++){
            for (int j=0; j<polyLine[i].length; j++){
                writeArray(ds, polyLine[i][j]);
            }
        }
    }

    /**
     * <div class="fr">Écriture d'un MultiPatch.</div>
     *
     * @param ds
     * @param multipatch
     * @param partTypes <span class="fr">types des parties d'un mutipatch</span>
     * @throws IOException
     */
    private static void writeMultiPatch(final DataOutputStream ds, double[][][] multipatch, int[] partTypes) throws IOException{

        // Détermination des indices de départ et du nombre de points

        // on prépare un tableau pour enregistrer les indices de départ
        final int[] parts = new int[multipatch.length];

        // on prépare le décompte du nombre de points
        int nbPoints = 0;

        // on parcourt les lignes de la polyligne
        for (int i=0; i<multipatch.length; i++){
            // indice de début de la ligne dans les points de la polyligne
            parts[i]=nbPoints;
            for (int j=0; j<multipatch[i].length; j++){
                nbPoints++; // incrémentation du nombre de points
            }
        }

        ds.setEndianness(Endianness.LITTLE_ENDIAN);

        // nombre de lignes dans la polyligne
        ds.writeInt(multipatch.length);

        // nombre de points
        ds.writeInt(nbPoints);

        // écriture des indices de départ
        ds.writeInt(parts);

        // écriture des types de parties
        ds.writeInt(partTypes);

        // points des lignes de la polyligne
        for (int i=0; i<multipatch.length; i++){
            for (int j=0; j<multipatch[i].length; j++){
                writeArray(ds, multipatch[i][j]);
            }
        }
    }

    /**
     * <div class="fr">Écriture d'un MultiPoint.</div>
     *
     * @param ds
     * @param multiPoint
     * @throws IOException
     */
    private static void writeMultiPoint(final DataOutputStream ds, double[][] multiPoint) throws IOException{
        ds.setEndianness(Endianness.LITTLE_ENDIAN);
        // nombre de points
        ds.writeInt(multiPoint.length);

        // points du multipoint
        for (int i=0; i<multiPoint.length; i++){
            writeArray(ds, multiPoint[i]);
        }
    }

    /**
     * <div class="fr">Écriture d'un point.</div>
     *
     * @param ds
     * @param point
     */
    private static void writePoint(final DataOutputStream ds, double[] point) throws IOException{
        writeArray(ds, point);
    }

    /**
     * <div class="fr">Écriture d'un tableau de doubles.</div>
     *
     * @param ds
     * @param values <span class="fr">tableau à écrire</span>
     * @throws IOException
     */
    private static void writeArray(final DataOutputStream ds, double[] values) throws IOException {
        ds.setEndianness(Endianness.LITTLE_ENDIAN);
        ds.writeDouble(values);
    }
}
