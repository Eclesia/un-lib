
package science.unlicense.format.shp;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 *
 * <div class="fr">Représentation d'un enregistrement, incluant en-tête, type de forme et données géométriques.</div>
 *
 * @author Samuel Andrés
 * @author Johann Sorel
 */
public class ShpRecord {

    /**
     * Record number
     */
    public int recordNumber;
    /**
     * record length, in 16bits words
     */
    public int recordLength;
    /**
     * Geometry type.
     */
    public int shapeType;
    /**
     * Bounding box of the geometry
     */
    public BBox bbox;
    /**
     * Record geometry
     */
    public PlanarGeometry geometry;

    public ShpRecord() {
    }

}
