package science.unlicense.format.shp;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShpReader extends AbstractReader {

    private ShpHeader header;

    public ShpHeader getHeader() throws IOException {
        if (header == null) {
            header = new ShpHeader();
            header.read(getInputAsDataStream(Endianness.BIG_ENDIAN));
        }

        return header;
    }

    public ShpRecordOld next() throws IOException {
        final ShpHeader header = getHeader();
        final DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);

        if (ds.getByteOffset() >= header.fileLength*2) return null;

        final int recordNumber = ds.readInt();
        final int recordLength = ds.readInt();

        return ShpUtils.readRecordData(ds, recordNumber, recordLength);
    }

    public Sequence readAll() throws IOException {
        final Sequence lst = new ArraySequence();
        for (ShpRecordOld record = next(); record!=null; record=next()) {
            lst.add(record);
        }
        return lst;
    }

}
