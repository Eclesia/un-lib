
package science.unlicense.format.shp;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.ArrayMultiGeometry;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.MultiPoint;
import science.unlicense.geometry.impl.MultiPolygon;
import science.unlicense.geometry.impl.MultiPolyline;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.math.api.TupleRW;
import static science.unlicense.format.shp.ShpConstants.TYPE_MULTIPATCH;
import static science.unlicense.format.shp.ShpConstants.TYPE_MULTIPOINT;
import static science.unlicense.format.shp.ShpConstants.TYPE_MULTIPOINT_M;
import static science.unlicense.format.shp.ShpConstants.TYPE_MULTIPOINT_Z;
import static science.unlicense.format.shp.ShpConstants.TYPE_NONE;
import static science.unlicense.format.shp.ShpConstants.TYPE_POINT;
import static science.unlicense.format.shp.ShpConstants.TYPE_POINT_M;
import static science.unlicense.format.shp.ShpConstants.TYPE_POINT_Z;
import static science.unlicense.format.shp.ShpConstants.TYPE_POLYGON;
import static science.unlicense.format.shp.ShpConstants.TYPE_POLYGON_M;
import static science.unlicense.format.shp.ShpConstants.TYPE_POLYGON_Z;
import static science.unlicense.format.shp.ShpConstants.TYPE_POLYLINE;
import static science.unlicense.format.shp.ShpConstants.TYPE_POLYLINE_M;
import static science.unlicense.format.shp.ShpConstants.TYPE_POLYLINE_Z;

/**
 *
 * @author Johann Sorel
 */
public class ShpGeometryIO {


    public abstract static class ReaderWriter {

        public abstract int getDimension();

        public abstract void read(DataInputStream ds, ShpRecord record) throws IOException;

        public abstract void write(DataOutputStream ds, ShpRecord shape) throws IOException;

        protected void readBBox2D(DataInputStream ds, ShpRecord shape) throws IOException {
            shape.bbox = new BBox(getDimension());
            shape.bbox.getLower().set(0, ds.readDouble());
            shape.bbox.getLower().set(1, ds.readDouble());
            shape.bbox.getUpper().set(0, ds.readDouble());
            shape.bbox.getUpper().set(1, ds.readDouble());
        }

        protected void writeBBox2D(DataOutputStream ds, ShpRecord shape) throws IOException {
            ds.writeDouble(shape.bbox.getMin(0));
            ds.writeDouble(shape.bbox.getMin(1));
            ds.writeDouble(shape.bbox.getMax(0));
            ds.writeDouble(shape.bbox.getMax(1));
        }

        protected Sequence readLines2(DataInputStream ds, ShpRecord shape) throws IOException {
            readBBox2D(ds, shape);
            final int numParts = ds.readInt();
            final int numPoints = ds.readInt();
            final int[] offsets = ds.readInt(numParts);

            final Sequence lines = new ArraySequence(numParts);
            for (int i=0;i<numParts;i++) {
                final int nbValues = (i==numParts-1) ? numPoints - offsets[numParts] : offsets[numParts+1] - offsets[numParts];
                final double[] values = ds.readDouble(nbValues*2);
                final Polyline line = new Polyline(InterleavedTupleGrid1D.create(values, 2));
                lines.add(line);
            }
            return lines;
        }

        protected Sequence readLines3(DataInputStream ds, ShpRecord shape) throws IOException {
            readBBox2D(ds, shape);
            final int numParts = ds.readInt();
            final int numPoints = ds.readInt();
            final int[] offsets = ds.readInt(numParts);

            final Sequence lines = new ArraySequence(numParts);
            for (int i=0;i<numParts;i++) {
                final int nbValues = (i==numParts-1) ? numPoints - offsets[numParts] : offsets[numParts+1] - offsets[numParts];
                final double[] values = ds.readDouble(nbValues*3);
                for (int k=0; k<nbValues; k++) {
                    values[k*3  ] = ds.readDouble();
                    values[k*3+1] = ds.readDouble();
                }
                final Polyline line = new Polyline(InterleavedTupleGrid1D.create(values, 3));
                lines.add(line);
            }
            shape.bbox.getLower().set(2, ds.readDouble());
            shape.bbox.getUpper().set(2, ds.readDouble());
            for (int i=0;i<numParts;i++) {
                final int nbValues = (i==numParts-1) ? numPoints - offsets[numParts] : offsets[numParts+1] - offsets[numParts];
                final Polyline line = (Polyline) lines.get(i);
                final double[] values = (double[]) ((InterleavedTupleGrid) line.getCoordinates()).getPrimitiveBuffer().getBackEnd();
                for (int k=0; k<nbValues; k++) {
                    values[k*3+2] = ds.readDouble();
                }
            }
            return lines;
        }

        protected Sequence readLines4(DataInputStream ds, ShpRecord shape) throws IOException {
            readBBox2D(ds, shape);
            final int numParts = ds.readInt();
            final int numPoints = ds.readInt();
            final int[] offsets = ds.readInt(numParts);

            final Sequence lines = new ArraySequence(numParts);

            //XY
            for (int i=0;i<numParts;i++) {
                final int nbValues = (i==numParts-1) ? numPoints - offsets[numParts] : offsets[numParts+1] - offsets[numParts];
                final double[] values = ds.readDouble(nbValues*4);
                for (int k=0; k<nbValues; k++) {
                    values[k*4  ] = ds.readDouble();
                    values[k*4+1] = ds.readDouble();
                }
                final Polyline line = new Polyline(InterleavedTupleGrid1D.create(values, 4));
                lines.add(line);
            }
            //Z
            shape.bbox.getLower().set(2, ds.readDouble());
            shape.bbox.getUpper().set(2, ds.readDouble());
            for (int i=0;i<numParts;i++) {
                final int nbValues = (i==numParts-1) ? numPoints - offsets[numParts] : offsets[numParts+1] - offsets[numParts];
                final Polyline line = (Polyline) lines.get(i);
                final double[] values = (double[]) ((InterleavedTupleGrid) line.getCoordinates()).getPrimitiveBuffer().getBackEnd();
                for (int k=0; k<nbValues; k++) {
                    values[k*4+2] = ds.readDouble();
                }
            }
            //M
            shape.bbox.getLower().set(3, ds.readDouble());
            shape.bbox.getUpper().set(3, ds.readDouble());
            for (int i=0;i<numParts;i++) {
                final int nbValues = (i==numParts-1) ? numPoints - offsets[numParts] : offsets[numParts+1] - offsets[numParts];
                final Polyline line = (Polyline) lines.get(i);
                final double[] values = (double[]) ((InterleavedTupleGrid) line.getCoordinates()).getPrimitiveBuffer().getBackEnd();
                for (int k=0; k<nbValues; k++) {
                    values[k*4+3] = ds.readDouble();
                }
            }

            return lines;
        }

        protected void writeLines2(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeBBox2D(ds, shape);
            final Sequence lines = extractRings(shape.geometry);
            final int nbLines = lines.getSize();
            int[] offsets = new int[nbLines];
            int nbPts = 0;
            //first loop write offsets
            for (int i=0;i<nbLines;i++) {
                final Polyline line = (Polyline) lines.get(i);
                offsets[i] = nbPts;
                nbPts += line.getCoordinates().getExtent().getL(0);
            }
            ds.writeInt(nbLines);
            ds.writeInt(nbPts);
            ds.writeInt(offsets);

            //second loop write points
            for (int i=0;i<nbLines;i++) {
                final Polyline line = (Polyline) lines.get(i);
                final TupleGridCursor cursor = line.getCoordinates().cursor();
                while (cursor.next()) {
                    ds.writeDouble(cursor.samples().get(0));
                    ds.writeDouble(cursor.samples().get(1));
                }
            }
        }

        protected void writeLines3(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeBBox2D(ds, shape);
            final Sequence lines = extractRings(shape.geometry);
            final int nbLines = lines.getSize();
            int[] offsets = new int[nbLines];
            int nbPts = 0;
            //first loop write offsets
            for (int i=0;i<nbLines;i++) {
                final Polyline line = (Polyline) lines.get(i);
                offsets[i] = nbPts;
                nbPts += line.getCoordinates().getExtent().getL(0);
            }
            ds.writeInt(nbLines);
            ds.writeInt(nbPts);
            ds.writeInt(offsets);

            //second loop write points
            for (int i=0;i<nbLines;i++) {
                final Polyline line = (Polyline) lines.get(i);
                final TupleGridCursor cursor = line.getCoordinates().cursor();
                while (cursor.next()) {
                    ds.writeDouble(cursor.samples().get(0));
                    ds.writeDouble(cursor.samples().get(1));
                }
            }

            //third loop write M
            ds.writeDouble(shape.bbox.getMin(2));
            ds.writeDouble(shape.bbox.getMax(2));
            for (int i=0;i<nbLines;i++) {
                final Polyline line = (Polyline) lines.get(i);
                final TupleGridCursor cursor = line.getCoordinates().cursor();
                while (cursor.next()) {
                    ds.writeDouble(cursor.samples().get(2));
                }
            }
        }

        protected void writeLines4(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeBBox2D(ds, shape);
            final Sequence lines = extractRings(shape.geometry);
            final int nbLines = lines.getSize();
            int[] offsets = new int[nbLines];
            int nbPts = 0;
            //first loop write offsets
            for (int i=0;i<nbLines;i++) {
                final Polyline line = (Polyline) lines.get(i);
                offsets[i] = nbPts;
                nbPts += line.getCoordinates().getExtent().getL(0);
            }
            ds.writeInt(nbLines);
            ds.writeInt(nbPts);
            ds.writeInt(offsets);

            //second loop write points
            for (int i=0;i<nbLines;i++) {
                final Polyline line = (Polyline) lines.get(i);
                final TupleGridCursor cursor = line.getCoordinates().cursor();
                while (cursor.next()) {
                    ds.writeDouble(cursor.samples().get(0));
                    ds.writeDouble(cursor.samples().get(1));
                }
            }

            //third loop write Z
            ds.writeDouble(shape.bbox.getMin(2));
            ds.writeDouble(shape.bbox.getMax(2));
            for (int i=0;i<nbLines;i++) {
                final Polyline line = (Polyline) lines.get(i);
                final TupleGridCursor cursor = line.getCoordinates().cursor();
                while (cursor.next()) {
                    ds.writeDouble(cursor.samples().get(2));
                }
            }

            //third loop write M
            ds.writeDouble(shape.bbox.getMin(3));
            ds.writeDouble(shape.bbox.getMax(3));
            for (int i=0;i<nbLines;i++) {
                final Polyline line = (Polyline) lines.get(i);
                final TupleGridCursor cursor = line.getCoordinates().cursor();
                while (cursor.next()) {
                    ds.writeDouble(cursor.samples().get(3));
                }
            }
        }

        protected Sequence extractRings(PlanarGeometry geom) {
            final Sequence lst = new ArraySequence();
            extractRings(geom, lst);
            return lst;
        }

        private void extractRings(PlanarGeometry geom, Sequence lst) {
            if (geom instanceof ArrayMultiGeometry) {
                final Sequence children = ((ArrayMultiGeometry) geom).getGeometries();
                for (int i=0,n=children.getSize(); i<n; i++) {
                    extractRings((PlanarGeometry) children.get(i), lst);
                }
            } else if (geom instanceof Polygon) {
                final Polygon poly = (Polygon) geom;
                lst.add(poly.getExterior());
                lst.addAll(poly.getInteriors());
            } else if (geom instanceof Polyline) {
                lst.add(geom);
            } else {
                throw new RuntimeException("Unexpected geometry type "+geom);
            }
        }

        protected MultiPolygon rebuild(Sequence rings) {
            return null;
        }
    }


    private ShpGeometryIO(){}

    public static ReaderWriter getReaderWriter(int shapeType) {
        switch(shapeType) {
            //2D
            case TYPE_NONE: return new Null();
            case TYPE_POINT: return new PointXY();
            case TYPE_POLYLINE: return new PolylineXY();
            case TYPE_POLYGON: return new PolygonXY();
            case TYPE_MULTIPOINT: return new MultiPointXY();
            //2D+1
            case TYPE_POINT_M: return new PointXYM();
            case TYPE_POLYLINE_M: return new PolylineXYM();
            case TYPE_POLYGON_M: return new PolygonXYM();
            case TYPE_MULTIPOINT_M: return new MultiPointXYM();
            //3D+1
            case TYPE_POINT_Z: return new PointXYZM();
            case TYPE_POLYLINE_Z: return new PolylineXYZM();
            case TYPE_POLYGON_Z: return new PolygonXYZM();
            case TYPE_MULTIPOINT_Z: return new MultiPointXYZM();
            case TYPE_MULTIPATCH: return new MultiPatch();
            default: throw new IllegalArgumentException("unknown shape type");
        }
    }

    private static class Null extends ReaderWriter {

        @Override
        public int getDimension() {
            return 2;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
        }
    }

    private static class PointXY extends ReaderWriter {

        @Override
        public int getDimension() {
            return 2;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            shape.bbox = null;
            shape.geometry = new DefaultPoint(ds.readDouble(), ds.readDouble());
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            final Point pt = (Point) shape.geometry;
            final TupleRW coord = pt.getCoordinate();
            ds.writeDouble(coord.get(0));
            ds.writeDouble(coord.get(1));
        }
    }

    private static class PointXYM extends ReaderWriter {

        @Override
        public int getDimension() {
            return 3;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            shape.bbox = null;
            shape.geometry = new DefaultPoint(
                    ds.readDouble(),
                    ds.readDouble(),
                    ds.readDouble());
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            final Point pt = (Point) shape.geometry;
            final TupleRW coord = pt.getCoordinate();
            ds.writeDouble(coord.get(0));
            ds.writeDouble(coord.get(1));
            ds.writeDouble(coord.get(2));
        }
    }

    private static class PointXYZM extends ReaderWriter {

        @Override
        public int getDimension() {
            return 4;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            shape.bbox = null;
            shape.geometry = new DefaultPoint(
                    ds.readDouble(),
                    ds.readDouble(),
                    ds.readDouble(),
                    ds.readDouble());
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            final Point pt = (Point) shape.geometry;
            final TupleRW coord = pt.getCoordinate();
            ds.writeDouble(coord.get(0));
            ds.writeDouble(coord.get(1));
            ds.writeDouble(coord.get(2));
            ds.writeDouble(coord.get(3));
        }
    }

    private static class MultiPointXY extends ReaderWriter {

        @Override
        public int getDimension() {
            return 2;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            readBBox2D(ds, shape);
            int nbPt = ds.readInt();
            final Sequence points = new ArraySequence(nbPt);
            for (int i=0;i<nbPt;i++) {
                points.add(new DefaultPoint(ds.readDouble(), ds.readDouble()));
            }
            shape.geometry = new MultiPoint(points);
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeBBox2D(ds, shape);
            final MultiPoint geometry = (MultiPoint) shape.geometry;
            final Sequence points = geometry.getGeometries();
            final int nbPts = points.getSize();
            ds.writeInt(nbPts);
            for (int i=0; i<nbPts; i++) {
                final Point pt = (Point) points.get(i);
                final TupleRW coord = pt.getCoordinate();
                ds.writeDouble(coord.get(0));
                ds.writeDouble(coord.get(1));
            }
        }
    }

    private static class MultiPointXYM extends ReaderWriter {

        @Override
        public int getDimension() {
            return 3;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            readBBox2D(ds, shape);
            int nbPt = ds.readInt();
            final Sequence points = new ArraySequence(nbPt);
            for (int i=0;i<nbPt;i++) {
                points.add(new DefaultPoint(ds.readDouble(), ds.readDouble(), 0.0));
            }
            shape.bbox.getLower().set(2, ds.readDouble());
            shape.bbox.getUpper().set(2, ds.readDouble());
            for (int i=0;i<nbPt;i++) {
                ((Point) points.get(i)).getCoordinate().set(2, ds.readDouble());
            }
            shape.geometry = new MultiPoint(points);
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeBBox2D(ds, shape);
            final MultiPoint geometry = (MultiPoint) shape.geometry;
            final Sequence points = geometry.getGeometries();
            final int nbPts = points.getSize();
            ds.writeInt(nbPts);
            for (int i=0; i<nbPts; i++) {
                final Point pt = (Point) points.get(i);
                final TupleRW coord = pt.getCoordinate();
                ds.writeDouble(coord.get(0));
                ds.writeDouble(coord.get(1));
            }
            ds.writeDouble(shape.bbox.getMin(2));
            ds.writeDouble(shape.bbox.getMax(2));
            for (int i=0; i<nbPts; i++) {
                final Point pt = (Point) points.get(i);
                ds.writeDouble(pt.getCoordinate().get(2));
            }
        }
    }

    private static class MultiPointXYZM extends ReaderWriter {

        @Override
        public int getDimension() {
            return 4;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            readBBox2D(ds, shape);
            int nbPt = ds.readInt();
            final Sequence points = new ArraySequence(nbPt);
            for (int i=0;i<nbPt;i++) {
                points.add(new DefaultPoint(ds.readDouble(), ds.readDouble(), 0.0, 0.0));
            }
            shape.bbox.getLower().set(2, ds.readDouble());
            shape.bbox.getUpper().set(2, ds.readDouble());
            for (int i=0;i<nbPt;i++) {
                ((Point) points.get(i)).getCoordinate().set(2,ds.readDouble());
            }
            shape.bbox.getLower().set(3, ds.readDouble());
            shape.bbox.getUpper().set(3, ds.readDouble());
            for (int i=0;i<nbPt;i++) {
                ((Point) points.get(i)).getCoordinate().set(3,ds.readDouble());
            }
            shape.geometry = new MultiPoint(points);
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeBBox2D(ds, shape);
            final MultiPoint geometry = (MultiPoint) shape.geometry;
            final Sequence points = geometry.getGeometries();
            final int nbPts = points.getSize();
            ds.writeInt(nbPts);
            for (int i=0; i<nbPts; i++) {
                final Point pt = (Point) points.get(i);
                final TupleRW coord = pt.getCoordinate();
                ds.writeDouble(coord.get(0));
                ds.writeDouble(coord.get(1));
            }
            ds.writeDouble(shape.bbox.getMin(2));
            ds.writeDouble(shape.bbox.getMax(2));
            for (int i=0; i<nbPts; i++) {
                final Point pt = (Point) points.get(i);
                ds.writeDouble(pt.getCoordinate().get(2));
            }
            ds.writeDouble(shape.bbox.getMin(3));
            ds.writeDouble(shape.bbox.getMax(3));
            for (int i=0; i<nbPts; i++) {
                final Point pt = (Point) points.get(i);
                ds.writeDouble(pt.getCoordinate().get(3));
            }
        }
    }

    private static class PolylineXY extends ReaderWriter {

        @Override
        public int getDimension() {
            return 2;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            final Sequence lines = readLines2(ds, shape);
            shape.geometry = new MultiPolyline(lines);
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeLines2(ds, shape);
        }
    }

    private static class PolylineXYM extends ReaderWriter {

        @Override
        public int getDimension() {
            return 3;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            final Sequence lines = readLines3(ds, shape);
            shape.geometry = new MultiPolyline(lines);
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeLines3(ds, shape);
        }
    }

    private static class PolylineXYZM extends ReaderWriter {

        @Override
        public int getDimension() {
            return 4;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            final Sequence lines = readLines4(ds, shape);
            shape.geometry = new MultiPolyline(lines);
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeLines4(ds, shape);
        }
    }

    private static class PolygonXY extends ReaderWriter {

        @Override
        public int getDimension() {
            return 2;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            final Sequence rings = readLines2(ds, shape);
            shape.geometry = rebuild(rings);
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeLines2(ds, shape);
        }
    }

    private static class PolygonXYM extends ReaderWriter {

        @Override
        public int getDimension() {
            return 3;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            final Sequence rings = readLines3(ds, shape);
            shape.geometry = rebuild(rings);
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeLines3(ds, shape);
        }
    }

    private static class PolygonXYZM extends ReaderWriter {

        @Override
        public int getDimension() {
            return 4;
        }
        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            final Sequence rings = readLines4(ds, shape);
            shape.geometry = rebuild(rings);
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            writeLines4(ds, shape);
        }
    }

    private static class MultiPatch extends ReaderWriter {

        @Override
        public int getDimension() {
            return 4;
        }

        @Override
        public void read(DataInputStream ds, ShpRecord shape) throws IOException {
            readBBox2D(ds, shape);
            final int numParts = ds.readInt();
            final int numPoints = ds.readInt();
            final int[] offsets = ds.readInt(numParts);
            final int[] types = ds.readInt(numParts);

            final Sequence lines = new ArraySequence(numParts);

            //XY
            for (int i=0;i<numParts;i++) {
                final int nbValues = (i==numParts-1) ? numPoints - offsets[numParts] : offsets[numParts+1] - offsets[numParts];
                final double[] values = ds.readDouble(nbValues*4);
                for (int k=0; k<nbValues; k++) {
                    values[k*4  ] = ds.readDouble();
                    values[k*4+1] = ds.readDouble();
                }
                final Polyline line = new Polyline(InterleavedTupleGrid1D.create(values, 4));
                lines.add(line);
            }
            //Z
            shape.bbox.getLower().set(2, ds.readDouble());
            shape.bbox.getUpper().set(2, ds.readDouble());
            for (int i=0;i<numParts;i++) {
                final int nbValues = (i==numParts-1) ? numPoints - offsets[numParts] : offsets[numParts+1] - offsets[numParts];
                final Polyline line = (Polyline) lines.get(i);
                final double[] values = (double[]) ((InterleavedTupleGrid) line.getCoordinates()).getPrimitiveBuffer().getBackEnd();
                for (int k=0; k<nbValues; k++) {
                    values[k*4+2] = ds.readDouble();
                }
            }
            //M
            shape.bbox.getLower().set(3, ds.readDouble());
            shape.bbox.getUpper().set(3, ds.readDouble());
            for (int i=0;i<numParts;i++) {
                final int nbValues = (i==numParts-1) ? numPoints - offsets[numParts] : offsets[numParts+1] - offsets[numParts];
                final Polyline line = (Polyline) lines.get(i);
                final double[] values = (double[]) ((InterleavedTupleGrid) line.getCoordinates()).getPrimitiveBuffer().getBackEnd();
                for (int k=0; k<nbValues; k++) {
                    values[k*4+3] = ds.readDouble();
                }
            }

            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void write(DataOutputStream ds, ShpRecord shape) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}
