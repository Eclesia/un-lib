package science.unlicense.format.shp;

import java.util.Arrays;
import java.util.Objects;

/**
 * <div class="fr">Représentation d'un enregistrement, incluant en-tête, type de forme et données géométriques.</div>
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShpRecordOld {


    /**
     * <div class="fr">Numéro d'enregistrement.</div>
     */
    public int recordNumber;
    /**
     * <div class="fr">Taille de l'enregistrement en nombre de mots de 16 bits.</div>
     */
    public int recordLength;
    /**
     * <div class="fr">Type de forme.</div>
     */
    public int shapeType;

    /**
     * <div class="fr">Emprise spatiale des données géométriques.</div>
     */
    public double[] bbox;
    /**
     * <div class="fr">Données géométriques brutes telles que lues dans le fichier, structurées en fonction du type de forme.</div>
     */
    public Object geometry;
    public double[] measureRange;
    public double[] measureValues;
    public double[] zRange;
    public double[] zValues;
    public int[] partTypes;

    public ShpRecordOld() {
    }

    public ShpRecordOld(final int recordNumber, final int recordLength, final int shapeType, final double[] bbox, final Object geometry,
            final double[] measureRange, final double[] measureValues, final double[] zRange, final double[] zValues, final int[] partTypes){
        this.recordNumber = recordNumber;
        this.recordLength = recordLength;
        this.shapeType = shapeType;
        this.bbox = bbox;
        this.geometry = geometry;
        this.measureRange = measureRange;
        this.measureValues = measureValues;
        this.zRange = zRange;
        this.zValues = zValues;
        this.partTypes = partTypes;
    }

    @Override
    public String toString() {
        return "ShpRecord{" + "recordNumber=" + recordNumber + ", recordLength=" + recordLength +
                ", shapeType=" + shapeType + ", bbox=" + Arrays.toString(bbox) + ", geometry=" + geometry +
                ", measureRange=" + Arrays.toString(measureRange) + ", measureValues=" + Arrays.toString(measureValues) +
                ", zRange=" + Arrays.toString(zRange) + ", zValues=" + Arrays.toString(zValues) +
                ", partTypes=" + Arrays.toString(partTypes) + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + this.recordNumber;
        hash = 11 * hash + this.recordLength;
        hash = 11 * hash + this.shapeType;
        hash = 11 * hash + Arrays.hashCode(this.bbox);
        hash = 11 * hash + Objects.hashCode(this.geometry);
        hash = 11 * hash + Arrays.hashCode(this.measureRange);
        hash = 11 * hash + Arrays.hashCode(this.measureValues);
        hash = 11 * hash + Arrays.hashCode(this.zRange);
        hash = 11 * hash + Arrays.hashCode(this.zValues);
        hash = 11 * hash + Arrays.hashCode(this.partTypes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ShpRecordOld other = (ShpRecordOld) obj;
        if (this.recordNumber != other.recordNumber) {
            return false;
        }
        if (this.recordLength != other.recordLength) {
            return false;
        }
        if (this.shapeType != other.shapeType) {
            return false;
        }
        if (!Arrays.equals(this.bbox, other.bbox)) {
            return false;
        }
        if (!Objects.equals(this.geometry, other.geometry)) {
            return false;
        }
        if (!Arrays.equals(this.measureRange, other.measureRange)) {
            return false;
        }
        if (!Arrays.equals(this.measureValues, other.measureValues)) {
            return false;
        }
        if (!Arrays.equals(this.zRange, other.zRange)) {
            return false;
        }
        if (!Arrays.equals(this.zValues, other.zValues)) {
            return false;
        }
        if (!Arrays.equals(this.partTypes, other.partTypes)) {
            return false;
        }
        return true;
    }
}
