package science.unlicense.format.shp;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShxReader extends AbstractReader {

    private ShpHeader header = null;

    public ShpHeader getHeader() throws IOException {
        if (header == null) {
            DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);
            header = new ShpHeader();
            header.read(ds);
        }
        return header;
    }

    /**
     *
     * @return int array, first is offset, second is content length
     * @throws IOException
     */
    public int[] next() throws IOException {
        //ensure header has been skipped
        final ShpHeader header = getHeader();
        final DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);

        if (ds.getByteOffset() >= header.fileLength*2) {
            return null;
        } else {
            return new int[]{ds.readInt(), ds.readInt()};
        }
    }

    public Sequence readAll() throws IOException {
        final Sequence lst = new ArraySequence();
        for (int[] record = next(); record!=null; record=next()) {
            lst.add(record);
        }
        return lst;
    }

}
