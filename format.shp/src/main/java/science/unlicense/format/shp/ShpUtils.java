package science.unlicense.format.shp;

import java.util.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.shp.ShpConstants.*;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public final class ShpUtils {

    private ShpUtils(){}

    //==================================================================================================================
    // Enregistrement
    //==================================================================================================================

    protected static ShpRecordOld readRecordData(final DataInputStream ds, int recordNumber, int recordLength) throws IOException {

        final ShpRecordOld record = new ShpRecordOld();
        record.recordNumber = recordNumber;
        record.recordLength = recordLength;
        ds.setEndianness(Endianness.LITTLE_ENDIAN);
        record.shapeType = ds.readInt();
        record.bbox = readBbox(ds, record.shapeType);

        final int[] parameters = recomputeNbPoints(ds, record.shapeType);
        record.geometry = readGeometry(ds, record.shapeType, parameters);
        record.zRange = record.shapeType >= TYPE_POINT_Z ? readZRange(ds, record.shapeType) : null;
        record.zValues = record.shapeType >= TYPE_POINT_Z ? readZ(ds, record.shapeType, parameters[1]) : null;
        record.measureRange = record.shapeType >= TYPE_POINT_M ? readMeasureRange(ds, record.shapeType) : null;
        record.measureValues = record.shapeType >= TYPE_POINT_M ? readMeasure(ds, record.shapeType, parameters[1]) : null;
        record.partTypes = record.shapeType == TYPE_MULTIPATCH ? Arrays.copyOfRange(parameters, ((parameters.length-2)/2)+2, parameters.length) : null;

        return record;
    }

    //==================================================================================================================
    // calcul du nombre de points
    //==================================================================================================================

    private static int[] recomputeNbPoints(final DataInputStream ds, final int shapeType) throws IOException {
        switch(shapeType){
            case TYPE_NONE: return null;
            case TYPE_POINT: return null;
            case TYPE_POLYLINE:
            case TYPE_POLYGON: return readPolyLineParameters(ds);
            case TYPE_MULTIPOINT: return readMultiPointParamter(ds);

            case TYPE_POINT_Z: return null;
            case TYPE_POLYLINE_Z:
            case TYPE_POLYGON_Z: return readPolyLineParameters(ds);
            case TYPE_MULTIPOINT_Z: return readMultiPointParamter(ds);

            case TYPE_POINT_M: return null;
            case TYPE_POLYLINE_M:
            case TYPE_POLYGON_M: return readPolyLineParameters(ds);
            case TYPE_MULTIPOINT_M: return readMultiPointParamter(ds);

            case TYPE_MULTIPATCH: return readMultiPatchParameters(ds);
            default: throw new IllegalArgumentException("unknown shape type");
        }
    }

    //==================================================================================================================
    // intervalle des valeurs de mesure
    //==================================================================================================================

    private static double[] readMeasureRange(final DataInputStream ds, final int shapeType) throws IOException {
        switch(shapeType){
            case TYPE_NONE:
            case TYPE_POINT:
            case TYPE_POLYLINE:
            case TYPE_POLYGON:
            case TYPE_MULTIPOINT:

            case TYPE_POINT_Z: return null;
            case TYPE_POLYLINE_Z:
            case TYPE_POLYGON_Z:
            case TYPE_MULTIPOINT_Z: return readArray(ds, 2);

            case TYPE_POINT_M: return null;
            case TYPE_POLYLINE_M:
            case TYPE_POLYGON_M:
            case TYPE_MULTIPOINT_M:

            case TYPE_MULTIPATCH: return readArray(ds, 2);
            default: throw new IllegalArgumentException("unknown shape type");
        }
    }

    //==================================================================================================================
    // valeurs de mesure
    //==================================================================================================================

    private static double[] readMeasure(final DataInputStream ds, final int shapeType, final int nbPoints) throws IOException {
        switch(shapeType){
            case TYPE_NONE:
            case TYPE_POINT:
            case TYPE_POLYLINE:
            case TYPE_POLYGON:
            case TYPE_MULTIPOINT:

            case TYPE_POINT_Z: return null;
            case TYPE_POLYLINE_Z:
            case TYPE_POLYGON_Z:
            case TYPE_MULTIPOINT_Z: return readArray(ds, nbPoints);

            case TYPE_POINT_M: return null;
            case TYPE_POLYLINE_M:
            case TYPE_POLYGON_M:
            case TYPE_MULTIPOINT_M:

            case TYPE_MULTIPATCH: return readArray(ds, nbPoints);
            default: throw new IllegalArgumentException("unknown shape type");
        }
    }

    //==================================================================================================================
    // intervalle des valeurs de la composante en Z
    //==================================================================================================================

    private static double[] readZRange(final DataInputStream ds, final int shapeType) throws IOException {
        switch(shapeType){
            case TYPE_NONE:
            case TYPE_POINT:
            case TYPE_POLYLINE:
            case TYPE_POLYGON:
            case TYPE_MULTIPOINT:

            case TYPE_POINT_Z: return null;
            case TYPE_POLYLINE_Z:
            case TYPE_POLYGON_Z:
            case TYPE_MULTIPOINT_Z: return readArray(ds, 2);

            case TYPE_POINT_M:
            case TYPE_POLYLINE_M:
            case TYPE_POLYGON_M:
            case TYPE_MULTIPOINT_M: return null;

            case TYPE_MULTIPATCH: return readArray(ds, 2);
            default: throw new IllegalArgumentException("unknown shape type");
        }
    }

    //==================================================================================================================
    // valeurs de la composante en z
    //==================================================================================================================

    private static double[] readZ(final DataInputStream ds, final int shapeType, final int nbPoints) throws IOException {
        switch(shapeType){
            case TYPE_NONE:
            case TYPE_POINT:
            case TYPE_POLYLINE:
            case TYPE_POLYGON:
            case TYPE_MULTIPOINT:

            case TYPE_POINT_Z: return null;
            case TYPE_POLYLINE_Z:
            case TYPE_POLYGON_Z:
            case TYPE_MULTIPOINT_Z: return readArray(ds, nbPoints);

            case TYPE_POINT_M:
            case TYPE_POLYLINE_M:
            case TYPE_POLYGON_M:
            case TYPE_MULTIPOINT_M: return null;

            case TYPE_MULTIPATCH: return readArray(ds, nbPoints);
            default: throw new IllegalArgumentException("unknown shape type");
        }
    }

    //==================================================================================================================
    // BBox d'enregistrement
    //==================================================================================================================

    private static double[] readBbox(final DataInputStream ds, final int shapeType) throws IOException {
        switch(shapeType){
            case TYPE_NONE: return null;
            case TYPE_POINT: return null;
            case TYPE_POLYLINE:
            case TYPE_POLYGON:
            case TYPE_MULTIPOINT: return readArray(ds, 4);

            case TYPE_POINT_Z: return null;
            case TYPE_POLYLINE_Z:
            case TYPE_POLYGON_Z:
            case TYPE_MULTIPOINT_Z: return readArray(ds, 4);

            case TYPE_POINT_M: return null;
            case TYPE_POLYLINE_M:
            case TYPE_POLYGON_M:
            case TYPE_MULTIPOINT_M: return readArray(ds, 4);

            case TYPE_MULTIPATCH: return readArray(ds, 4);
            default: throw new IllegalArgumentException("unknown shape type");
        }
    }

    //==================================================================================================================
    // Géométrie d'enregistrement
    //==================================================================================================================

    private static Object readGeometry(final DataInputStream ds, final int shapeType, final int[] parameters) throws IOException {
        switch(shapeType){
            case TYPE_NONE: return null;
            case TYPE_POINT:
            case TYPE_POINT_M:
            case TYPE_POINT_Z: return readPoint(ds);
            case TYPE_POLYLINE:
            case TYPE_POLYLINE_M:
            case TYPE_POLYLINE_Z: return readPolyLine(ds, parameters);
            case TYPE_POLYGON:
            case TYPE_POLYGON_M:
            case TYPE_POLYGON_Z: return readPolygon(ds, parameters);
            case TYPE_MULTIPOINT:
            case TYPE_MULTIPOINT_M:
            case TYPE_MULTIPOINT_Z: return readMultiPoint(ds, parameters[0]);
            case TYPE_MULTIPATCH: return readMultiPatch(ds, parameters);
            default: throw new IllegalArgumentException("unknown shape type");
        }
    }

    /**
     * <div class="fr">Lecture d'un polygone 2D. La structure des enregistrements est identique à celle des polylignes.
     * On considère seulement que les anneaux extérieurs sont donnés dans le sens des aiguilles d'une montre et les
     * anneaux intérieurs dans le sens contraire. De même, les suites de point définissant les anneaux doivent
     * explicitement finir par le même vertex que leur vertex de départ.</div>
     *
     * @param ds
     * @param parameters
     * @return <span class="fr">tableau de trois dimensions contentant, dans l'ordre des indices, 1) les anneaux du polygone
     * 2) les points de chaque anneau 3) les coordonnées x et y de chaque point</span>
     * @throws IOException
     */
    private static double[][][] readPolygon(final DataInputStream ds, final int[] parameters) throws IOException {
        return readPolyLine(ds, parameters);
    }

    /**
     * <div class="fr">Lecture des paramètres des polylignes et des polygones. Les paramètres sont :
     * <ol>
     * <li>le nombre de parties ;</li>
     * <li>le nombre total de points ;</li>
     * <li>une liste de longueur égale au nombre de parties contenant les indices de départ de chaque partie.</li>
     * </ol>
     * </div>
     *
     * @param ds
     * @return
     * @throws IOException
     */
    private static int[] readPolyLineParameters(final DataInputStream ds) throws IOException {
        ds.setEndianness(Endianness.LITTLE_ENDIAN);

        final int nbParts = ds.readInt();
        final int nbPoints = ds.readInt();

        final int[] parameters = new int[nbParts+2];
        parameters[0] = nbParts;
        parameters[1] = nbPoints;

        for (int i=2; i<nbParts+2; i++){
            parameters[i] = ds.readInt();
        }
        return parameters;
    }

    /**
     * <div class="fr">Lecture d'une polyLigne.</div>
     *
     * @param ds
     * @param parameters
     * @return <span class="fr">tableau de trois dimensions contentant, dans l'ordre des indices, 1) les lignes de la polyligne
     * 2) les points de chaque ligne 3) les coordonnées x et y de chaque point</span>
     * @throws IOException
     */
    private static double[][][] readPolyLine(final DataInputStream ds, final int[] parameters) throws IOException {

        final int nbParts = parameters[0];
        final int nbPoints = parameters[1];

        // lecture des points de chaque ligne
        final double[][][] result = new double[nbParts][][];
        for (int i=0; i<nbParts-1; i++){

            // le nombre de point de la ligne est donné par l'index du premier point de la ligne suivante
            // dont on retranche l'index du premier point de la ligne courante
            result[i] = readPoints(ds, parameters[2+i+1]-parameters[2+i]);
        }

        // le nombre de point de la dernière ligne doit être calculé à l'aide du nombre de points dont on retranche
        // l'index du premier point de la dernière ligne.
        result[result.length-1] = readPoints(ds, nbPoints-parameters[2+nbParts-1]);
        return result;
    }

    /**
     * <div class="fr">Lecture des paramètres des Multipatchs. Les paramètres sont :
     * <ol>
     * <li>le nombre de parties ;</li>
     * <li>le nombre total de points ;</li>
     * <li>une liste de longueur égale au nombre de parties contenant les indices de départ de chaque partie ;</li>
     * <li>une liste de longueur égale au nombre de parties contenant le type correspondant à chaque partie.</li>
     * </ol>
     * </div>
     *
     * @param ds
     * @return
     * @throws IOException
     */
    private static int[] readMultiPatchParameters(final DataInputStream ds) throws IOException {
        ds.setEndianness(Endianness.LITTLE_ENDIAN);

        final int nbParts = ds.readInt();
        final int nbPoints = ds.readInt();

        final int[] parameters = new int[nbParts * 2 + 2];
        parameters[0] = nbParts;
        parameters[1] = nbPoints;

        for (int i=2; i<2+nbParts*2; i++){
            parameters[i] = ds.readInt();
        }

        return parameters;
    }

    /**
     * <div class="fr">Lecture d'un multipatch.</div>
     *
     * @param ds
     * @param parameters
     * @return <span class="fr">tableau de trois dimensions contentant, dans l'ordre des indices, 1) les parties du multipatch
     * 2) les points de chaque partie 3) les coordonnées x et y de chaque point</span>
     * @throws IOException
     */
    private static double[][][] readMultiPatch(final DataInputStream ds, final int[] parameters) throws IOException {

        final int nbParts = parameters[0];
        final int nbPoints = parameters[1];


        // lecture des points de chaque ligne
        final double[][][] result = new double[nbParts][][];
        for (int i=2; i<nbParts+2-1; i++){

            // le nombre de point de la ligne est donné par l'index du premier point de la ligne suivante
            // dont on retranche l'index du premier point de la ligne courante
            result[i] = readPoints(ds, parameters[i+1]-parameters[i]);
        }

        // le nombre de point de la dernière ligne doit être calculé à l'aide du nombre de points dont on retranche
        // l'index du premier point de la dernière ligne.
        result[result.length-1] = readPoints(ds, nbPoints-parameters[2+nbParts-1]);
        return result;
    }

    /**
     * <div class="fr">Lecture de l'unique paramètre des multipoints : le nombre de points.</div>
     *
     * @param ds
     * @return
     * @throws IOException
     */
    private static int[] readMultiPointParamter(final DataInputStream ds) throws IOException {
        ds.setEndianness(Endianness.LITTLE_ENDIAN);
        return new int[]{ds.readInt()};
    }

    /**
     * <div class="fr">Lecture d'un multipoint.</div>
     *
     * @param ds
     * @param nbPoints
     * @return <span class="fr">tableau de deux dimensions contentant, dans l'ordre des indices, 1) les points du multipoint
     * 2) les coordonnées x et y de chaque point</span>
     * @throws IOException
     */
    private static double[][] readMultiPoint(final DataInputStream ds, final int nbPoints) throws IOException {
        return readPoints(ds, nbPoints);
    }

    /**
     * <div class="fr">Lecture des coordonnées d'une série de points.</div>
     *
     * @param ds
     * @param nbPoints <span class="fr">nombre de points à lire</div>
     * @param dim <span class="fr">dimension des points</div>
     * @return <span class="fr">coordonnées d'une série de point dans un tableau à deux indices dont le premier
     * est le nombre de points de la série et le second la dimension des points</div>
     * @throws IOException
     */
    private static double[][] readPoints(final DataInputStream ds, int nbPoints) throws IOException {

        final double[][] result = new double[nbPoints][];
        for (int i=0; i<nbPoints; i++){
            result[i] = readArray(ds, 2);
        }
        return result;
    }

    /**
     * <div class="fr">Lecture d'un point.</div>
     *
     * @param ds
     * @return <span class="fr">tableau d'une dimension de longueur 2 contenant les coordonnées x et y du point</span>
     * @throws IOException
     */
    private static double[] readPoint(final DataInputStream ds) throws IOException {
        return readArray(ds, 2);
    }

    /**
     * <div class="fr">Lecture d'un tableau de doubles.</div>
     *
     * @param ds
     * @param length <span class="fr">longueur du tableau à lire</span>
     * @return
     * @throws IOException
     */
    private static double[] readArray(final DataInputStream ds, int length) throws IOException {
        ds.setEndianness(Endianness.LITTLE_ENDIAN);
        return ds.readDouble(length);
    }
}
