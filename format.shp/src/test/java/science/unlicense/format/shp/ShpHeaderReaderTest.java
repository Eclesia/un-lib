package science.unlicense.format.shp;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShpHeaderReaderTest {

    @Test
    public void pointHeaderTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/point.shp"));
        final ShpHeader header = new ShpHeader();
        header.read(new DataInputStream(path.createInputStream()));
        ShpTestGeometries.pointHeaderTest(header);
    }

    @Test
    public void polyLineHeaderTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/polyLine.shp"));
        final ShpHeader header = new ShpHeader();
        header.read(new DataInputStream(path.createInputStream()));
        ShpTestGeometries.polyLineHeaderTest(header);
    }

    @Test
    public void polygonHeaderTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/polygon.shp"));
        final ShpHeader header = new ShpHeader();
        header.read(new DataInputStream(path.createInputStream()));
        ShpTestGeometries.polygonHeaderTest(header);
    }

    @Test
    public void polygon2HeaderTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/polygon2.shp"));
        final ShpHeader header = new ShpHeader();
        header.read(new DataInputStream(path.createInputStream()));
        ShpTestGeometries.polygon2HeaderTest(header);
    }
}
