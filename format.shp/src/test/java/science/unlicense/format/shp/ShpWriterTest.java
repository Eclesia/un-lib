package science.unlicense.format.shp;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.concurrent.api.VirtualPath;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import static science.unlicense.format.shp.ShpConstants.*;
import static science.unlicense.format.shp.ShpTestGeometries.MULTIPOINTS;
import static science.unlicense.format.shp.ShpTestGeometries.MULTIPOINTS_BBOXES;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.impl.Vector4f64;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShpWriterTest {

    @Test
    public void pointTest() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/point.shp"));
        final ShpReader reader1 = new ShpReader();
        reader1.setInput(path);
        final ShpHeader header1 = reader1.getHeader();
        final Sequence records1 = reader1.readAll();

        final Path fileW = VirtualPath.onRam(new Chars("test"));
        final ShpWriter writer = new ShpWriter();
        writer.setOutput(fileW);
        writer.writeHeader(header1);
        writer.writeData(records1.createIterator());

        final ShpReader reader2 = new ShpReader();
        reader2.setInput(fileW);
        ShpTestGeometries.pointHeaderTest(reader2.getHeader());

        ShpTestGeometries.pointDataTest(fileW);
    }

    @Test
    public void multiPointTest() throws IOException {

        // lecture du fichier
        final Sequence records1 = new ArraySequence();
        records1.add(new ShpRecordOld(1, 88/2, TYPE_MULTIPOINT, MULTIPOINTS_BBOXES[0], MULTIPOINTS[0], null, null, null, null, null));
        records1.add(new ShpRecordOld(2, 88/2, TYPE_MULTIPOINT, MULTIPOINTS_BBOXES[1], MULTIPOINTS[1], null, null, null, null, null));

        // fichier à écrire
        final Path fileW = VirtualPath.onRam(new Chars("test"));

        final ShpHeader header = new ShpHeader();
        header.fileLength = (100+96+96)/2;
        header.shapeType = TYPE_MULTIPOINT;
        header.bbox = new BBox(new Vector4f64(2.319942485021687, 42.1236994094, 0, 0), new Vector4f64(3.7894734278072297, 49.89021323552048, 0, 0));

        final ShpWriter writer = new ShpWriter();
        writer.setOutput(fileW);
        writer.writeHeader(header);
        writer.writeData(records1.createIterator());

        // contrôle en lecture du fichier écrit
        final ShpReader reader2 = new ShpReader();
        reader2.setInput(fileW);
        ShpTestGeometries.multiPointHeaderTest(reader2.getHeader());

        ShpTestGeometries.multiPointDataTest(fileW);
    }

    @Test
    public void polyLineTest() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/polyLine.shp"));
        final ShpReader reader1 = new ShpReader();
        reader1.setInput(path);
        final ShpHeader header1 = reader1.getHeader();
        final Sequence records1 = reader1.readAll();

        // fichier à écrire
        final Path fileW = VirtualPath.onRam(new Chars("test"));
        final ShpWriter writer = new ShpWriter();
        writer.setOutput(fileW);
        writer.writeHeader(header1);
        writer.writeData(records1.createIterator());

        // contrôle en lecture du fichier écrit
        final ShpReader reader2 = new ShpReader();
        reader2.setInput(fileW);
        ShpTestGeometries.polyLineHeaderTest(reader2.getHeader());

        ShpTestGeometries.polyLineDataTest(fileW);
    }

    @Test
    public void polygonTest() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/polygon.shp"));
        final ShpReader reader1 = new ShpReader();
        reader1.setInput(path);
        final ShpHeader header1 = reader1.getHeader();
        final Sequence records1 = reader1.readAll();

        // fichier à écrire
        final Path fileW = VirtualPath.onRam(new Chars("test"));
        final ShpWriter writer = new ShpWriter();
        writer.setOutput(fileW);
        writer.writeHeader(header1);
        writer.writeData(records1.createIterator());

        // contrôle en lecture du fichier écrit
        final ShpReader reader2 = new ShpReader();
        reader2.setInput(fileW);
        ShpTestGeometries.polygonHeaderTest(reader2.getHeader());

        ShpTestGeometries.polygonDataTest(fileW);
    }

    @Test
    public void polygon2Test() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/polygon2.shp"));
        final ShpReader reader1 = new ShpReader();
        reader1.setInput(path);
        final ShpHeader header1 = reader1.getHeader();
        final Sequence records1 = reader1.readAll();

        // fichier à écrire
        final Path fileW = VirtualPath.onRam(new Chars("test"));
        final ShpWriter writer = new ShpWriter();
        writer.setOutput(fileW);
        writer.writeHeader(header1);
        writer.writeData(records1.createIterator());

        // contrôle en lecture du fichier écrit
        final ShpReader reader2 = new ShpReader();
        reader2.setInput(fileW);
        ShpTestGeometries.polygon2HeaderTest(reader2.getHeader());

        ShpTestGeometries.polygon2DataTest(fileW);
    }
}
