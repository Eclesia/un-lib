package science.unlicense.format.shp;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShxHeaderReaderTest {

    @Test
    public void pointHeaderTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/point.shx"));
        final ShpHeader header = new ShpHeader();
        header.read(new DataInputStream(path.createInputStream()));
        ShxTestIndex.pointHeaderTest(header);
    }

    @Test
    public void polyLineHeaderTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/polyLine.shx"));
        final ShpHeader header = new ShpHeader();
        header.read(new DataInputStream(path.createInputStream()));
        ShxTestIndex.polyLineHeaderTest(header);
    }

    @Test
    public void polygonHeaderTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/polygon.shx"));
        final ShpHeader header = new ShpHeader();
        header.read(new DataInputStream(path.createInputStream()));
        ShxTestIndex.polygonHeaderTest(header);
    }

    @Test
    public void polygon2HeaderTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/polygon2.shx"));
        final ShpHeader header = new ShpHeader();
        header.read(new DataInputStream(path.createInputStream()));
        ShxTestIndex.polygon2HeaderTest(header);
    }
}
