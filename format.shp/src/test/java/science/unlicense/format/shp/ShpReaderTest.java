package science.unlicense.format.shp;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShpReaderTest {

    @Test
    public void pointDataTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/point.shp"));
        ShpTestGeometries.pointDataTest(path);
    }

    @Test
    public void polyLineDataTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/polyLine.shp"));
        ShpTestGeometries.polyLineDataTest(path);
    }

    @Test
    public void polygonDataTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/polygon.shp"));
        ShpTestGeometries.polygonDataTest(path);
    }

    @Test
    public void polygon2DataTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shp/polygon2.shp"));
        ShpTestGeometries.polygon2DataTest(path);
    }
}
