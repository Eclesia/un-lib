package science.unlicense.format.shp;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.concurrent.api.VirtualPath;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShxWriterTest {

    @Test
    public void pointTest() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/point.shx"));

        // lecture du fichier
        final ShxReader reader1 = new ShxReader();
        reader1.setInput(path);
        final ShpHeader header1 = reader1.getHeader();
        final Sequence records1 = reader1.readAll();

        // fichier à écrire
        final Path fileW = VirtualPath.onRam(new Chars("test"));

        final ShxWriter writer = new ShxWriter();
        writer.setOutput(fileW);
        writer.writeHeader(header1);
        writer.writeData(records1.createIterator());
        writer.dispose();


        // contrôle en lecture du fichier écrit
        final ShxReader reader2 = new ShxReader();
        reader2.setInput(fileW);
        ShxTestIndex.pointHeaderTest(reader2.getHeader());
        ShxTestIndex.pointIndexTest(fileW);
    }

    @Test
    public void polyLineTest() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/polyLine.shx"));

        // lecture du fichier
        final ShxReader reader1 = new ShxReader();
        reader1.setInput(path);
        final ShpHeader header1 = reader1.getHeader();
        final Sequence records1 = reader1.readAll();

        // fichier à écrire
        final Path fileW = VirtualPath.onRam(new Chars("test"));

        final ShxWriter writer = new ShxWriter();
        writer.setOutput(fileW);
        writer.writeHeader(header1);
        writer.writeData(records1.createIterator());
        writer.dispose();

        // contrôle en lecture du fichier écrit
        final ShxReader reader2 = new ShxReader();
        reader2.setInput(fileW);
        ShxTestIndex.polyLineHeaderTest(reader2.getHeader());
        ShxTestIndex.polyLineIndexTest(fileW);
    }

    @Test
    public void polygonTest() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/polygon.shx"));

        // lecture du fichier
        final ShxReader reader1 = new ShxReader();
        reader1.setInput(path);
        final ShpHeader header1 = reader1.getHeader();
        final Sequence records1 = reader1.readAll();

        // fichier à écrire
        final Path fileW = VirtualPath.onRam(new Chars("test"));

        final ShxWriter writer = new ShxWriter();
        writer.setOutput(fileW);
        writer.writeHeader(header1);
        writer.writeData(records1.createIterator());
        writer.dispose();

        // contrôle en lecture du fichier écrit
        final ShxReader reader2 = new ShxReader();
        reader2.setInput(fileW);
        ShxTestIndex.polygonHeaderTest(reader2.getHeader());
        ShxTestIndex.polygonIndexTest(fileW);
    }

    @Test
    public void polygon2Test() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/polygon2.shx"));

        // lecture du fichier
        final ShxReader reader1 = new ShxReader();
        reader1.setInput(path);
        final ShpHeader header1 = reader1.getHeader();
        final Sequence records1 = reader1.readAll();

        // fichier à écrire
        final Path fileW = VirtualPath.onRam(new Chars("test"));

        final ShxWriter writer = new ShxWriter();
        writer.setOutput(fileW);
        writer.writeHeader(header1);
        writer.writeData(records1.createIterator());
        writer.dispose();

        // contrôle en lecture du fichier écrit
        final ShxReader reader2 = new ShxReader();
        reader2.setInput(fileW);
        ShxTestIndex.polygon2HeaderTest(reader2.getHeader());
        ShxTestIndex.polygon2IndexTest(fileW);
    }
}
