package science.unlicense.format.shp;

import org.junit.Assert;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import static science.unlicense.format.shp.ShpConstants.*;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShxTestIndex {

    private static final int[][] POINTS = new int[][]{
            {50, 10},
            {64, 10},
            {78, 10}};

    public static void pointHeaderTest(ShpHeader header){

        Assert.assertEquals((100+3*4*2)/2, header.fileLength);
        Assert.assertEquals(TYPE_POINT, header.shapeType);
        Assert.assertEquals(2.319942485021687, header.bbox.getMin(0), 0.);
        Assert.assertEquals(43.1236994094, header.bbox.getMin(1), 0.);
        Assert.assertEquals(3.7894734278072297, header.bbox.getMax(0), 0.);
        Assert.assertEquals(48.89021323552048, header.bbox.getMax(1), 0.);
        Assert.assertEquals(0., header.bbox.getMin(2), 0.);
        Assert.assertEquals(0., header.bbox.getMax(2), 0.);
        Assert.assertEquals(0., header.bbox.getMin(3), 0.);
        Assert.assertEquals(0., header.bbox.getMax(3), 0.);
    }

    public static void pointIndexTest(Path file) throws IOException{
        final ShxReader reader = new ShxReader();
        reader.setInput(file);
        final ShpHeader header = reader.getHeader();

        int i=0;
        for (int[] record = reader.next(); record!=null; record=reader.next()){
            pointIndexTest_0(record, i);
            i++;
        }
        Assert.assertEquals(3, i);
    }

    private static void pointIndexTest_0(final int[] record, int i){
        Assert.assertArrayEquals(POINTS[i], record);
    }

    private static final int[][] POLYLINE = new int[][]{
            {50, 88},
            {142, 80}};

    public static void polyLineHeaderTest(ShpHeader header){

        Assert.assertEquals((100+2*4*2)/2, header.fileLength);
        Assert.assertEquals(TYPE_POLYLINE, header.shapeType);
        Assert.assertEquals(-0.2098829354698788, header.bbox.getMin(0), 0.);
        Assert.assertEquals(42.54704802678795, header.bbox.getMin(1), 0.);
        Assert.assertEquals(4.477734755440964, header.bbox.getMax(0), 0.);
        Assert.assertEquals(48.648391687973486, header.bbox.getMax(1), 0.);
        Assert.assertEquals(0., header.bbox.getMin(2), 0.);
        Assert.assertEquals(0., header.bbox.getMax(2), 0.);
        Assert.assertEquals(0., header.bbox.getMin(3), 0.);
        Assert.assertEquals(0., header.bbox.getMax(3), 0.);
    }

    public static void polyLineIndexTest(Path file) throws IOException{
        final ShxReader reader = new ShxReader();
        reader.setInput(file);
        final ShpHeader header = reader.getHeader();

        int i=0;
        for (int[] record = reader.next(); record!=null; record=reader.next()){
            polyLineIndexTest_0(record, i);
            i++;
        }
        Assert.assertEquals(2, i);
    }

    private static void polyLineIndexTest_0(int[] record, int i){
        Assert.assertArrayEquals(POLYLINE[i], record);
    }

    private static final int[][] POLYGON = new int[][]{
            {50, 80},
            {134, 64}};

    public static void polygonHeaderTest(ShpHeader header){

        Assert.assertEquals((100+2*4*2)/2, header.fileLength);
        Assert.assertEquals(TYPE_POLYGON, header.shapeType);
        Assert.assertEquals(-4.934703941387951, header.bbox.getMin(0), 0.);
        Assert.assertEquals(41.375143604060234, header.bbox.getMin(1), 0.);
        Assert.assertEquals(9.388572336395182, header.bbox.getMax(0), 0.);
        Assert.assertEquals(51.19681876596867, header.bbox.getMax(1), 0.);
        Assert.assertEquals(0., header.bbox.getMin(2), 0.);
        Assert.assertEquals(0., header.bbox.getMax(2), 0.);
        Assert.assertEquals(0., header.bbox.getMin(3), 0.);
        Assert.assertEquals(0., header.bbox.getMax(3), 0.);
    }

    public static void polygonIndexTest(Path file) throws IOException{
        final ShxReader reader = new ShxReader();
        reader.setInput(file);
        final ShpHeader header = reader.getHeader();

        int i=0;
        for (int[] record = reader.next(); record!=null; record=reader.next()){
            polygonIndexTest_0(record, i);
            i++;
        }
        Assert.assertEquals(2, i);
    }

    private static void polygonIndexTest_0(int[] record, int i){
        Assert.assertArrayEquals(POLYGON[i], record);
    }

    private static final int[][] POLYGON2 = new int[][]{
            {50, 114},
            {168, 64}};

    public static void polygon2HeaderTest(ShpHeader header){

        Assert.assertEquals((100+2*4*2)/2, header.fileLength);
        Assert.assertEquals(TYPE_POLYGON, header.shapeType);
        Assert.assertEquals(-4.934703941387951, header.bbox.getMin(0), 0.);
        Assert.assertEquals(41.375143604060234, header.bbox.getMin(1), 0.);
        Assert.assertEquals(9.388572336395182, header.bbox.getMax(0), 0.);
        Assert.assertEquals(51.19681876596867, header.bbox.getMax(1), 0.);
        Assert.assertEquals(0., header.bbox.getMin(2), 0.);
        Assert.assertEquals(0., header.bbox.getMax(2), 0.);
        Assert.assertEquals(0., header.bbox.getMin(3), 0.);
        Assert.assertEquals(0., header.bbox.getMax(3), 0.);
    }

    public static void polygon2IndexTest(Path file) throws IOException{
        final ShxReader reader = new ShxReader();
        reader.setInput(file);
        final ShpHeader header = reader.getHeader();

        int i=0;
        for (int[] record = reader.next(); record!=null; record=reader.next()){
            polygon2IndexTest_0(record, i);
            i++;
        }
        Assert.assertEquals(2, i);
    }

    private static void polygon2IndexTest_0(int[] record, int i){
        Assert.assertArrayEquals(POLYGON2[i++], record);
    }

    private static final int[][] GADM36_FRA_1 = new int[][]{{50, 19602},
            {19656, 10320},
            {29980, 636600},
            {666584, 8032},
            {674620, 216676},
            {891300, 35176},
            {926480, 51682},
            {978166, 4728},
            {982898, 168624},
            {1151526, 220456},
            {1371986, 107346},
            {1479336, 130646},
            {1609986, 194566}};

    public static void gadm36_fra_1_IndexTest(Path file) throws IOException{
        final ShxReader reader = new ShxReader();
        reader.setInput(file);
        final ShpHeader header = reader.getHeader();

        int i=0;
        for (int[] record = reader.next(); record!=null; record=reader.next()){
            gadm36_fra_1_IndexTest_0(record, i);
            i++;
        }
        Assert.assertEquals(13, i);
    }

    private static void gadm36_fra_1_IndexTest_0(int[] record, int i){
        Assert.assertArrayEquals(GADM36_FRA_1[i++], record);
    }
}
