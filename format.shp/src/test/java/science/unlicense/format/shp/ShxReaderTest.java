package science.unlicense.format.shp;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel (Adapted to Unlicense-Lib API)
 */
public class ShxReaderTest {

    @Test
    public void pointIndexTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/point.shx"));
        ShxTestIndex.pointIndexTest(path);
    }

    @Test
    public void polyLineIndexTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/polyLine.shx"));
        ShxTestIndex.polyLineIndexTest(path);
    }

    @Test
    public void polygonIndexTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/polygon.shx"));
        ShxTestIndex.polygonIndexTest(path);
    }

    @Test
    public void polygon2IndexTest() throws IOException {
        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/shx/polygon2.shx"));
        ShxTestIndex.polygon2IndexTest(path);
    }
}
