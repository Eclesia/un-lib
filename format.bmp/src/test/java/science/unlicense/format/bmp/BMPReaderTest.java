package science.unlicense.format.bmp;

import science.unlicense.format.bmp.BMPReader;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;

/**
 *
 * @author Johann Sorel
 */
public class BMPReaderTest {

    private static Vector2i32 b00 = new Vector2i32(0,0);
    private static Vector2i32 b10 = new Vector2i32(1,0);
    private static Vector2i32 b01 = new Vector2i32(0,1);
    private static Vector2i32 b11 = new Vector2i32(1,1);

    @Test
    public void testSample() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/bmp/Sample.bmp")).createInputStream();

        final ImageReader reader = new BMPReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final ImageModel sm = image.getRawModel();
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor tb = image.getTupleBuffer(cm).cursor();

        //values are in BGR
        Assert.assertEquals(new Vector3f64(0,0,255), image.getTuple(b00, sm)); //RED
        Assert.assertEquals(new Vector3f64(0,255,255), image.getTuple(b10, sm)); //YELLOW
        Assert.assertEquals(new Vector3f64(255,255,0), image.getTuple(b01, sm)); //CYAN
        Assert.assertEquals(new Vector3f64(255,0,0), image.getTuple(b11, sm)); //BLUE

        Assert.assertEquals(new ColorRGB(255,   0,   0, 255).toColorSystem(cs), image.getColor(b00));
        Assert.assertEquals(new ColorRGB(255, 255,   0, 255).toColorSystem(cs), image.getColor(b10));
        Assert.assertEquals(new ColorRGB(  0, 255, 255, 255).toColorSystem(cs), image.getColor(b01));
        Assert.assertEquals(new ColorRGB(  0,   0, 255, 255).toColorSystem(cs), image.getColor(b11));

    }

    @Test
    public void testSampleRGBA() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/bmp/SampleRGBA.bmp")).createInputStream();

        final ImageReader reader = new BMPReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final ImageModel sm = image.getRawModel();
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();

        //values are in BGRA
        Assert.assertEquals(new Vector4f64(167,174, 26,175), image.getTuple(b00, sm));
        Assert.assertEquals(new Vector4f64(156,163, 50,137), image.getTuple(b10, sm));
        Assert.assertEquals(new Vector4f64(157,163, 48,143), image.getTuple(b01, sm));
        Assert.assertEquals(new Vector4f64(142,148, 80,111), image.getTuple(b11, sm));

        Assert.assertEquals(new ColorRGB( 26, 174, 167, 175).toColorSystem(cs), image.getColor(b00));
        Assert.assertEquals(new ColorRGB( 50, 163, 156, 137).toColorSystem(cs), image.getColor(b10));
        Assert.assertEquals(new ColorRGB( 48, 163, 157, 143).toColorSystem(cs), image.getColor(b01));
        Assert.assertEquals(new ColorRGB( 80, 148, 142, 111).toColorSystem(cs), image.getColor(b11));

    }

    //TODO
    @Ignore
    @Test
    public void testSampleRGBX() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/bmp/SampleRGBX.bmp")).createInputStream();

        final ImageReader reader = new BMPReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final ImageModel sm = image.getRawModel();

        //values are in BGRA
        Assert.assertEquals(new Vector4f64(167,174, 26,175), image.getTuple(b00, sm));
        Assert.assertEquals(new Vector4f64(156,163, 50,137), image.getTuple(b10, sm));
        Assert.assertEquals(new Vector4f64(157,163, 48,143), image.getTuple(b01, sm));
        Assert.assertEquals(new Vector4f64(142,148, 80,111), image.getTuple(b11, sm));

        Assert.assertEquals(new ColorRGB( 26, 174, 167, 175), image.getColor(b00));
        Assert.assertEquals(new ColorRGB( 50, 163, 156, 137), image.getColor(b10));
        Assert.assertEquals(new ColorRGB( 48, 163, 157, 143), image.getColor(b01));
        Assert.assertEquals(new ColorRGB( 80, 148, 142, 111), image.getColor(b11));

    }
}
