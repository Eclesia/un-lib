

package science.unlicense.format.bmp;

import science.unlicense.format.bmp.BMPWriter;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageWriter;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorRW;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class BMPWriterTest {

    @Test
    public void testWriteRGBA() throws IOException{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/bmp/SampleRGBA.bmp")).createInputStream();
        final byte[] expected = IOUtilities.readAll(bs);

        final ArrayOutputStream out = new ArrayOutputStream();
        final ImageWriter writer = new BMPWriter();
        writer.setOutput(out);

        final Image image = Images.create(new Extent.Long(2, 2),Images.IMAGE_TYPE_RGBA);
        final ImageModel colorModel = image.getColorModel();
        final ColorSystem cs = (ColorSystem) colorModel.getSampleSystem();

        final TupleGridCursor tb = colorModel.asTupleBuffer(image).cursor();
        final ColorRW pixel = Colors.castOrWrap(tb.samples(), cs);
        tb.moveTo(new Vector2i32(0,0)); pixel.set(new ColorRGB(26, 174, 167, 175));
        tb.moveTo(new Vector2i32(1,0)); pixel.set(new ColorRGB(50, 163, 156, 137));
        tb.moveTo(new Vector2i32(0,1)); pixel.set(new ColorRGB(48, 163, 157, 143));
        tb.moveTo(new Vector2i32(1,1)); pixel.set(new ColorRGB(80, 148, 142, 111));

        writer.write(image, null);
        final byte[] result = out.getBuffer().toArrayByte();

        Assert.assertArrayEquals(expected, result);

    }

}
