

package science.unlicense.format.bmp;

import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.AbstractImageWriter;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageWriteParameters;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class BMPWriter extends AbstractImageWriter{


    protected void write(Image image, ImageWriteParameters params, ByteOutputStream stream) throws IOException {

        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);

        final int datasize = width*height*4;

        //TODO handle various image cases
        final BMPInfoHeader infoHeader = new BMPInfoHeader();
        infoHeader.width = width;
        infoHeader.height = height;
        infoHeader.planes = 1;
        infoHeader.bitPerPixel = 32;
        infoHeader.dibSize = 40;
        // 40bytes DIB
        infoHeader.compression = BMPConstants.COMPRESSION_NONE;
        infoHeader.imageSize = datasize;
        infoHeader.xPixelPerMeter = 3780;
        infoHeader.yPixelPerMeter = 3780;
        infoHeader.colorsInColorTable = 0;
        infoHeader.importantColorCount = 0;

        final BMPFileHeader fileHeader = new BMPFileHeader();
        fileHeader.signature = BMPConstants.SIGNATURE_BM;
        fileHeader.reserved1 = 0;
        fileHeader.reserved2 = 0;
        fileHeader.offset = 14+infoHeader.dibSize;
        fileHeader.fileSize = 14+infoHeader.dibSize+datasize;

        //images are in BVRA starting from last line
        final byte[] array = new byte[datasize];
        int k = 0;
        final TupleGridCursor cursor = image.getColorModel().asTupleBuffer(image).cursor();
        final ColorSystem cs = (ColorSystem) image.getColorModel().getSampleSystem();
        final Vector2i32 coord = new Vector2i32();
        final int[] argb = new int[4];
        for (coord.y=height-1;coord.y>=0;coord.y--){
            for (coord.x=0;coord.x<width;coord.x++){
                cursor.moveTo(coord);
                final int color = new ColorRGB(cursor.samples(),cs).toARGB();
                Colors.toARGB(color, argb);
                array[k++] = (byte) argb[3];
                array[k++] = (byte) argb[2];
                array[k++] = (byte) argb[1];
                array[k++] = (byte) argb[0];
            }
        }

        //now write everything
        final DataOutputStream ds = getOutputAsDataStream(Endianness.LITTLE_ENDIAN);
        fileHeader.write(ds);
        infoHeader.write(ds);

        ds.write(array);

    }

}
