
package science.unlicense.format.bmp;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * @author Johann Sorel
 */
public class BMPFormat extends AbstractImageFormat {

    public static final BMPFormat INSTANCE = new BMPFormat();

    private BMPFormat() {
        super(new Chars("bmp"));
        shortName = new Chars("BMP");
        longName = new Chars("Bitmap");
        mimeTypes.add(new Chars("image/bmp"));
        mimeTypes.add(new Chars("image/x-bmp"));
        extensions.add(new Chars("bmp"));
        extensions.add(new Chars("dib"));
        signatures.add(BMPConstants.SIGNATURE_BM.toBytes());
        signatures.add(BMPConstants.SIGNATURE_BA.toBytes());
        signatures.add(BMPConstants.SIGNATURE_CI.toBytes());
        signatures.add(BMPConstants.SIGNATURE_CP.toBytes());
        signatures.add(BMPConstants.SIGNATURE_IC.toBytes());
        signatures.add(BMPConstants.SIGNATURE_PT.toBytes());
    }

    @Override
    public boolean supportReading() {
        return true;
}

    @Override
    public boolean supportWriting() {
        return true;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new BmpStore(this, source);
    }

}
