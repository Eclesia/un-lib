package science.unlicense.format.bmp;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import science.unlicense.image.api.ImageSetMetadata;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 * BMP image format defined here :
 * http://en.wikipedia.org/wiki/BMP_file_format
 *
 * @author Johann Sorel
 */
public class BMPReader extends AbstractImageReader{

    private BMPFileHeader fileHeader;
    private BMPInfoHeader header;

    /**
     * Used by ICO,CUR formats.
     * @param header
     */
    public void setHeader(BMPInfoHeader header) {
        this.header = header;
    }

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        final DataInputStream ds = new DataInputStream(stream, Endianness.LITTLE_ENDIAN);

        //Bitmap file header ---------------------------------------------------
        fileHeader = new BMPFileHeader();
        fileHeader.read(ds);

        //DIB header -----------------------------------------------------------
        header = new BMPInfoHeader();
        header.read(ds);

        if (header.planes != 1) throw new IOException(stream, "Only handle 1 plane, found "+ header.planes);
        if (header.bitPerPixel == 1) throw new IOException(stream, "1Bit per pixel BMP not supported yet");

        final ImageSetMetadata meta = new ImageSetMetadata();
        meta.getChildren().add(new ImageSetMetadata.Image(header.width, header.height));

        final Dictionary metadatas = new HashDictionary();
        metadatas.add(meta.getType().getId(), meta);
        return metadatas;
    }

    protected Image read(ImageReadParameters params, BacktrackInputStream stream) throws IOException {
        stream.mark();
        readMetadatas(stream);
        stream.rewind();
        stream.skip(14+header.dibSize);
        return readNoSkip(params, stream);
    }

    /**
     * Used by BMP,ICO,CUR formats.
     * ICO and CUR do not contains a fileHeader.
     *
     * @param params
     * @param stream
     * @return
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public Image readNoSkip(ImageReadParameters params, BacktrackInputStream stream) throws IOException {

        final DataInputStream ds = new DataInputStream(stream, Endianness.LITTLE_ENDIAN);

        //start reading image
        if (header.bitPerPixel != 24 && header.bitPerPixel != 32){
            throw new IOException(ds, "BMP different from 24bit or 32bit RGB.A not supported yet.");
        }

        final int lineLength = (header.bitPerPixel/8)*header.width;
        final Buffer datas = params.getBufferFactory().createInt8(lineLength*header.height).getBuffer();

        //lines must be a multiple of 4 bytes, skip any other bytes.
        int padding = lineLength % 4 ;
        if (padding != 0) padding = 4-padding;

        //line stored in reverse order
        for (int y=header.height-1; y>=0 ;y--){
            //from left to right
            ds.readFully(datas, y*lineLength, lineLength);
            if (padding != 0) ds.skip(padding);
        }

        //images are in BVR starting from last line
        final ImageModel sm;
        final ImageModel cm;

        if (header.bitPerPixel == 24){
            sm = new InterleavedModel(new UndefinedSystem(3),UInt8.TYPE);
            cm = DerivateModel.create(sm, new int[]{2,1,0},null,null,ColorSystem.RGB_8BITS);
        } else {
            sm = new InterleavedModel(new UndefinedSystem(4),UInt8.TYPE);
            cm = DerivateModel.create(sm,new int[]{2,1,0,3},null,null,ColorSystem.RGBA_8BITS);
        }

        return new DefaultImage(datas, new Extent.Long(header.width, header.height), sm, cm);
    }

}
