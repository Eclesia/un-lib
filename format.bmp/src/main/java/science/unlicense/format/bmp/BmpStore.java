
package science.unlicense.format.bmp;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.ImageResource;
import science.unlicense.image.api.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class BmpStore extends AbstractStore implements ImageResource {

    public BmpStore(ImageFormat format, Object input) {
        super(format, input);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return true;
    }

    @Override
    public ImageReader createReader() throws IOException {
        final ImageReader reader = new BMPReader();
        reader.setInput(source);
        return reader;
    }

    @Override
    public ImageWriter createWriter() throws IOException {
        final ImageWriter writer = new BMPWriter();
        writer.setOutput(source);
        return writer;
    }

    @Override
    public Chars getId() {
        return null;
    }

}
