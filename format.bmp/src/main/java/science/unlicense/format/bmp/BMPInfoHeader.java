

package science.unlicense.format.bmp;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class BMPInfoHeader {


    public int dibSize;

    //available in DIB 12/40/56/108
    public int width;
    public int height;
    public int planes;
    public int bitPerPixel;
    //available in DIB 40/56/108
    public int compression = 0;
    public int imageSize = 0;
    public int xPixelPerMeter = 0;
    public int yPixelPerMeter = 0;
    public int colorsInColorTable = 0;
    public int importantColorCount = 0;
    //optional in DIB 40, mandatory in 56/108
    public int maskRed = 0;
    public int maskGreen = 0;
    public int maskBlue = 0;
    public int maskAlpha = 0;
    //available in DIB 108
    public int colorSpaceType = 0;
    public int[] colorSpaceParams;


    public void read(DataInputStream ds) throws IOException{
        dibSize = ds.readInt();
        if (dibSize != 12 && dibSize != 40 && dibSize != 56 && dibSize != 108){
            throw new IOException(ds, "Unexpected header size :" +dibSize);
        }

        if (dibSize == 12){
            width = ds.readUShort();
            height = ds.readUShort();
        } else {
            width = ds.readInt();
            height = ds.readInt();
        }

        planes = ds.readUShort();
        bitPerPixel = ds.readUShort();

        if (dibSize == 12) {
        } else if (dibSize == 40) {
            readExtended(ds);
            readOptionalMask(ds);
        } else if (dibSize == 56) {
            readExtended(ds);
            readMandatoryMask(ds);
            readOptionalMask(ds);
        } else if (dibSize == 108) {
            readExtended(ds);
            readMandatoryMask(ds);
            colorSpaceType = ds.readInt();
            colorSpaceParams = ds.readInt(12);
        }
    }

    private void readExtended(DataInputStream ds) throws IOException{
        compression = ds.readInt();
        imageSize = ds.readInt();
        xPixelPerMeter = ds.readInt();
        yPixelPerMeter = ds.readInt();
        colorsInColorTable = ds.readInt();
        importantColorCount = ds.readInt();
    }

    private void readMandatoryMask(DataInputStream ds) throws IOException{
        maskRed = ds.readInt();
        maskGreen = ds.readInt();
        maskBlue = ds.readInt();
        maskAlpha = ds.readInt();
    }

    private void readOptionalMask(DataInputStream ds) throws IOException{
        if (bitPerPixel == 16 || bitPerPixel == 32) {
            if (compression == 0) {
                if (bitPerPixel == 32) {
                    maskRed = 0xff << 16;
                    maskGreen = 0xff << 8;
                    maskBlue = 0xff << 0;
                    maskAlpha = -1;
                } else {
                    maskRed = 0x1f << 10;
                    maskGreen = 0x1f << 5;
                    maskBlue = 0x1f << 0;
                    maskAlpha = -1;
                }
            } else if (compression == 3) {
                maskRed = ds.readInt();
                maskGreen = ds.readInt();
                maskBlue = ds.readInt();
                maskAlpha = -1;
            }
        }
    }

    public void write(DataOutputStream ds) throws IOException {

        ds.writeInt(dibSize);
        if (dibSize == 12){
            ds.writeUShort(width);
            ds.writeUShort(height);
        } else {
            ds.writeInt(width);
            ds.writeInt(height);;
        }

        ds.writeUShort(planes);
        ds.writeUShort(bitPerPixel);

        if (dibSize==12){
            //nothing to do
        } else if (dibSize==40){
            writeExtended(ds);
            writeOptionalMask(ds);
        } else if (dibSize==56){
            writeExtended(ds);
            writeMandatoryMask(ds);
            writeOptionalMask(ds);
        } else if (dibSize==108){
            writeExtended(ds);
            writeMandatoryMask(ds);
            ds.writeInt(colorSpaceType);
            for (int i=0;i<colorSpaceParams.length;i++){
                ds.writeInt(colorSpaceParams[i]);
            }
        } else {
            throw new IOException(ds, "Unexpected DIB size : "+dibSize);
        }
    }

    private void writeExtended(DataOutputStream ds) throws IOException{
        ds.writeInt(compression);
        ds.writeInt(imageSize);
        ds.writeInt(xPixelPerMeter);
        ds.writeInt(yPixelPerMeter);
        ds.writeInt(colorsInColorTable);
        ds.writeInt(importantColorCount);
    }

    private void writeMandatoryMask(DataOutputStream ds) throws IOException{
        ds.writeInt(maskRed);
        ds.writeInt(maskGreen);
        ds.writeInt(maskBlue);
        ds.writeInt(maskAlpha);
    }

    private void writeOptionalMask(DataOutputStream ds) throws IOException{
        if (bitPerPixel == 16 || bitPerPixel == 32) {
            if (compression == 3) {
                ds.writeInt(maskRed);
                ds.writeInt(maskGreen);
                ds.writeInt(maskBlue);
            }
        }
    }

}
