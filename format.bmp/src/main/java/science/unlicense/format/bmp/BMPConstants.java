
package science.unlicense.format.bmp;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class BMPConstants {

    /** Windows 3.1x, 95, NT, ... etc. */
    public static final Chars SIGNATURE_BM = Chars.constant(new byte[]{'B','M'});
    /** OS/2 struct Bitmap Array */
    public static final Chars SIGNATURE_BA = Chars.constant(new byte[]{'B','A'});
    /** OS/2 struct Color Icon */
    public static final Chars SIGNATURE_CI = Chars.constant(new byte[]{'C','I'});
    /** OS/2 const Color Pointer */
    public static final Chars SIGNATURE_CP = Chars.constant(new byte[]{'C','P'});
    /** OS/2 struct Icon */
    public static final Chars SIGNATURE_IC = Chars.constant(new byte[]{'I','C'});
    /** OS/2 Pointer */
    public static final Chars SIGNATURE_PT = Chars.constant(new byte[]{'P','T'});

    public static final int COMPRESSION_NONE = 0;
    public static final int COMPRESSION_RLE8 = 1;
    public static final int COMPRESSION_RLE4 = 2;
    public static final int COMPRESSION_BITFIELDS = 3;
    public static final int COMPRESSION_JPEG = 4;
    public static final int COMPRESSION_PNG = 5;
    public static final int COMPRESSION_ALPHABITFIELDS = 6;

    private BMPConstants(){}

}
