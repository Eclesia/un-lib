
package science.unlicense.common.api.array;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultInt32Buffer;

/**
 *
 * @author Johann Sorel
 */
public class DefaultIntBufferTest extends AbstractBufferTest{

    @Override
    public Buffer create(int nbByte) {
        return new DefaultInt32Buffer(new int[nbByte/4]);
    }

}
