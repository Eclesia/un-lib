
package science.unlicense.common.api.array;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultInt8Buffer;

/**
 *
 * @author Johann Sorel
 */
public class DefaultByteBufferTest extends AbstractBufferTest{

    @Override
    public Buffer create(int nbByte) {
        return new DefaultInt8Buffer(new byte[nbByte]);
    }

}
