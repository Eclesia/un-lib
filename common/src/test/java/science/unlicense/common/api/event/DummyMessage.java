
package science.unlicense.common.api.event;

import science.unlicense.common.api.event.DefaultEventMessage;
import science.unlicense.common.api.event.MessagePredicate;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Dummy event, used for various tests.
 *
 * @author Johann Sorel
 */
public class DummyMessage extends DefaultEventMessage {

    public static final Predicate PREDICATE = new MessagePredicate(DummyMessage.class);

    public DummyMessage() {
        super(true);
    }

    public boolean equals(Object obj) {
        return obj == this;
    }

}
