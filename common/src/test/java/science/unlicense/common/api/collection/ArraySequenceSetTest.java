
package science.unlicense.common.api.collection;

import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 * Test sequence.
 *
 * @author Johann Sorel
 */
public class ArraySequenceSetTest extends SequenceTest {

    @Override
    protected Sequence createNew() {
        return new ArraySequenceSet();
    }

    @Override
    protected Object[] createDifferentValues(int nb) {
        final Object[] values = new Object[nb];
        for (int i=0;i<nb;i++) values[i] = new String(""+i);
        return values;
    }

    /**
     * Constructors test.
     */
    @Test
    public void testConstructor() {

        Sequence seq = new ArraySequence();
        Assert.assertTrue(seq.isEmpty());
        Assert.assertEquals(0, seq.getSize());

        seq = new ArraySequence(new Object[0]);
        Assert.assertTrue(seq.isEmpty());
        Assert.assertEquals(0, seq.getSize());

        final Object[] array = new Object[]{"a","b","c"};
        seq = new ArraySequence(array);
        Assert.assertFalse(seq.isEmpty());
        Assert.assertEquals(3, seq.getSize());
        Assert.assertEquals("a", seq.get(0));
        Assert.assertEquals("b", seq.get(1));
        Assert.assertEquals("c", seq.get(2));

    }

}
