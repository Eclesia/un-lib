
package science.unlicense.common.api.character;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.Arrays;

/**
 * CharSequence class tests.
 *
 * @author Johann Sorel
 */
public class CharsTest {

    @Test
    public void testEquals() {
        final Chars str1 = new Chars(new byte[]{'a','B','c','1'}, CharEncodings.US_ASCII);
        final Chars str2 = new Chars(new byte[]{'a','B','c','1'}, CharEncodings.UTF_8);
        final Chars str3 = new Chars(new byte[]{'A','b','C','1'}, CharEncodings.US_ASCII);
        final Chars str4 = new Chars(new byte[]{'A','b','C','1'}, CharEncodings.UTF_8);
        final Chars str5 = new Chars(new byte[]{'A','z','C','1'}, CharEncodings.US_ASCII);
        final Chars str6 = new Chars(new byte[]{'a','B','c','z','1'}, CharEncodings.UTF_8);

        //test strict
        Assert.assertFalse(str1.equals(str2, false, false));
        Assert.assertFalse(str1.equals(str3, false, false));
        Assert.assertFalse(str1.equals(str4, false, false));
        Assert.assertFalse(str1.equals(str5, false, false));
        Assert.assertFalse(str1.equals(str6, false, false));

        //test without case
        Assert.assertFalse(str1.equals(str2, true, false));
        Assert.assertTrue(str1.equals(str3, true, false));
        Assert.assertFalse(str1.equals(str4, true, false));
        Assert.assertFalse(str1.equals(str5, true, false));
        Assert.assertFalse(str1.equals(str6, true, false));

        //test without encoding, this is the default equals
        Assert.assertTrue(str1.equals(str2, false, true));
        Assert.assertFalse(str1.equals(str3, false, true));
        Assert.assertFalse(str1.equals(str4, false, true));
        Assert.assertFalse(str1.equals(str5, false, true));
        Assert.assertFalse(str1.equals(str6, false, true));
        Assert.assertTrue(str1.equals(str2));
        Assert.assertFalse(str1.equals(str3));
        Assert.assertFalse(str1.equals(str4));
        Assert.assertFalse(str1.equals(str5));
        Assert.assertFalse(str1.equals(str6));

        //test without case and encoding
        Assert.assertTrue(str1.equals(str2, true, true));
        Assert.assertTrue(str1.equals(str3, true, true));
        Assert.assertTrue(str1.equals(str4, true, true));
        Assert.assertFalse(str1.equals(str5, true, true));
        Assert.assertFalse(str1.equals(str6, true, true));

    }

    @Test
    public void testIterator() {
        final Chars str = new Chars(new byte[]{'a','B','c','1'}, CharEncodings.UTF_8);

        CharIterator ite = str.createIterator();
        Assert.assertTrue(ite.hasNext());
        Assert.assertEquals('a', ite.nextToUnicode());
        Assert.assertTrue(ite.hasNext());
        Assert.assertEquals('B', ite.nextToUnicode());
        Assert.assertTrue(ite.hasNext());
        Assert.assertEquals('c', ite.nextToUnicode());
        Assert.assertTrue(ite.hasNext());
        Assert.assertEquals('1', ite.nextToUnicode());
        Assert.assertFalse(ite.hasNext());

        ite = str.createIterator(1,3);
        Assert.assertTrue(ite.hasNext());
        Assert.assertEquals('B', ite.nextToUnicode());
        Assert.assertTrue(ite.hasNext());
        Assert.assertEquals('c', ite.nextToUnicode());
        Assert.assertFalse(ite.hasNext());
    }

    @Test
    public void testTrim() {

        Chars text = new Chars(new byte[]{});

        text = text.trim();
        Assert.assertTrue(text.isEmpty());

        text = new Chars(new byte[]{'a','\n','b',' ','c','d'});
        text = text.trim();
        Assert.assertEquals(6, text.getCharLength());
        Assert.assertEquals('a', text.getUnicode(0));
        Assert.assertEquals('\n', text.getUnicode(1));
        Assert.assertEquals('b', text.getUnicode(2));
        Assert.assertEquals(' ', text.getUnicode(3));
        Assert.assertEquals('c', text.getUnicode(4));
        Assert.assertEquals('d', text.getUnicode(5));

        text = new Chars(new byte[]{' ','\r','a','\n','b',' ','c','d',' ','\n'});
        text = text.trim();
        Assert.assertEquals(6, text.getCharLength());
        Assert.assertEquals('a', text.getUnicode(0));
        Assert.assertEquals('\n', text.getUnicode(1));
        Assert.assertEquals('b', text.getUnicode(2));
        Assert.assertEquals(' ', text.getUnicode(3));
        Assert.assertEquals('c', text.getUnicode(4));
        Assert.assertEquals('d', text.getUnicode(5));

        text = new Chars(new byte[]{' ',' ',' '});
        text = text.trim();
        Assert.assertTrue(text.isEmpty());

    }

    @Test
    public void testTrimStart() {

        Chars text = new Chars(new byte[]{});

        text = text.trimStart();
        Assert.assertTrue(text.isEmpty());

        text = new Chars(new byte[]{'a','\n','b',' ','c','d'});
        text = text.trimStart();
        Assert.assertEquals(6, text.getCharLength());
        Assert.assertEquals('a', text.getUnicode(0));
        Assert.assertEquals('\n', text.getUnicode(1));
        Assert.assertEquals('b', text.getUnicode(2));
        Assert.assertEquals(' ', text.getUnicode(3));
        Assert.assertEquals('c', text.getUnicode(4));
        Assert.assertEquals('d', text.getUnicode(5));

        text = new Chars(new byte[]{' ','\r','a','\n','b',' ','c','d',' ','\n'});
        text = text.trimStart();
        Assert.assertEquals(8, text.getCharLength());
        Assert.assertEquals('a', text.getUnicode(0));
        Assert.assertEquals('\n', text.getUnicode(1));
        Assert.assertEquals('b', text.getUnicode(2));
        Assert.assertEquals(' ', text.getUnicode(3));
        Assert.assertEquals('c', text.getUnicode(4));
        Assert.assertEquals('d', text.getUnicode(5));
        Assert.assertEquals(' ', text.getUnicode(6));
        Assert.assertEquals('\n', text.getUnicode(7));


    }

    @Test
    public void testTrimEnd() {

        Chars text = new Chars(new byte[]{});

        text = text.trimEnd();
        Assert.assertTrue(text.isEmpty());

        text = new Chars(new byte[]{'a','\n','b',' ','c','d'});
        text = text.trimEnd();
        Assert.assertEquals(6, text.getCharLength());
        Assert.assertEquals('a', text.getUnicode(0));
        Assert.assertEquals('\n', text.getUnicode(1));
        Assert.assertEquals('b', text.getUnicode(2));
        Assert.assertEquals(' ', text.getUnicode(3));
        Assert.assertEquals('c', text.getUnicode(4));
        Assert.assertEquals('d', text.getUnicode(5));

        text = new Chars(new byte[]{' ','\r','a','\n','b',' ','c','d',' ','\n'});
        text = text.trimEnd();
        Assert.assertEquals(8, text.getCharLength());
        Assert.assertEquals(' ', text.getUnicode(0));
        Assert.assertEquals('\r', text.getUnicode(1));
        Assert.assertEquals('a', text.getUnicode(2));
        Assert.assertEquals('\n', text.getUnicode(3));
        Assert.assertEquals('b', text.getUnicode(4));
        Assert.assertEquals(' ', text.getUnicode(5));
        Assert.assertEquals('c', text.getUnicode(6));
        Assert.assertEquals('d', text.getUnicode(7));


    }

    @Test
    public void testTrucate() {
        final Chars text = new Chars("$test");
        final Chars value = text.truncate(1, text.getCharLength());
        Assert.assertEquals(new Chars("test"), value);
    }

    @Test
    public void testSplit() {

        Chars text = Chars.EMPTY;
        Chars[] parts = text.split(' ');
        Assert.assertEquals(1, parts.length);
        Assert.assertEquals(Chars.EMPTY, parts[0]);

        text = new Chars("hello");
        parts = text.split(' ');
        Assert.assertEquals(1, parts.length);
        Assert.assertEquals(new Chars("hello"), parts[0]);

        text = new Chars("hello great world");
        parts = text.split(' ');
        Assert.assertEquals(3, parts.length);
        Assert.assertEquals(new Chars("hello"), parts[0]);
        Assert.assertEquals(new Chars("great"), parts[1]);
        Assert.assertEquals(new Chars("world"), parts[2]);

        text = new Chars(" a b c ");
        parts = text.split(' ');
        Assert.assertEquals(5, parts.length);
        Assert.assertEquals(Chars.EMPTY, parts[0]);
        Assert.assertEquals(new Chars("a"), parts[1]);
        Assert.assertEquals(new Chars("b"), parts[2]);
        Assert.assertEquals(new Chars("c"), parts[3]);
        Assert.assertEquals(Chars.EMPTY, parts[4]);

        text = new Chars("   ");
        parts = text.split(' ');
        Assert.assertEquals(4, parts.length);
        Assert.assertEquals(Chars.EMPTY, parts[0]);
        Assert.assertEquals(Chars.EMPTY, parts[1]);
        Assert.assertEquals(Chars.EMPTY, parts[2]);
        Assert.assertEquals(Chars.EMPTY, parts[3]);

    }

    @Test
    public void testReplaceAll() {

        Chars text = Chars.EMPTY;
        Chars result = text.replaceAll(new Chars("hello"), new Chars("bye"));
        Assert.assertEquals(Chars.EMPTY, result);

        text = new Chars("hello");
        result = text.replaceAll(new Chars("hello"), new Chars("bye"));
        Assert.assertEquals(new Chars("bye"), result);

        text = new Chars("great hello world hello");
        result = text.replaceAll(new Chars("hello"), new Chars("bye"));
        Assert.assertEquals(new Chars("great bye world bye"), result);

        text = new Chars("great hello world hello");
        result = text.replaceAll(new Chars("hello"), new Chars("bye"),12);
        Assert.assertEquals(new Chars("great hello world bye"), result);

        text = new Chars("f_color = vec4($value);");
        result = text.replaceAll(new Chars("$value"), new Chars("1,0,0,1"));
        Assert.assertEquals(new Chars("f_color = vec4(1,0,0,1);"), result);


    }

    @Test
    public void testLowercase() {
        Chars str = new Chars("Hello World!");
        Chars res = str.toLowerCase();
        Assert.assertEquals(new Chars("hello world!"), res);

    }

    @Test
    public void testUppercase() {
        Chars str = new Chars("Hello World!");
        Chars res = str.toUpperCase();
        Assert.assertEquals(new Chars("HELLO WORLD!"), res);

    }

    @Test
    public void testOrder() {

        final Object[] array = new Object[]{
            new Chars("b"),
            new Chars("G"),
            new Chars("aaaaa"),
            new Chars("bc"),
            new Chars("b"),
            new Chars("Gj"),
            new Chars("ba"),
            new Chars("Gjk"),
        };

        Arrays.sort(array);

        Assert.assertEquals(new Chars("G"),    array[0]);
        Assert.assertEquals(new Chars("Gj"),   array[1]);
        Assert.assertEquals(new Chars("Gjk"),  array[2]);
        Assert.assertEquals(new Chars("aaaaa"),array[3]);
        Assert.assertEquals(new Chars("b"),    array[4]);
        Assert.assertEquals(new Chars("b"),    array[5]);
        Assert.assertEquals(new Chars("ba"),   array[6]);
        Assert.assertEquals(new Chars("bc"),   array[7]);
    }

}
