
package science.unlicense.common.api.array;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.primitive.ByteSequence;

/**
 *
 * @author Johann Sorel
 */
public class ByteSequenceTest {

    @Test
    public void testGrow() {
        final ByteSequence buffer = new ByteSequence();
        Assert.assertEquals(0, buffer.getSize());

        buffer.put((byte) 1);
        Assert.assertEquals(1, buffer.getSize());

        buffer.skip();
        Assert.assertEquals(2, buffer.getSize());

        buffer.put(new byte[50]);
        Assert.assertEquals(52, buffer.getSize());

        buffer.put(new byte[1200]);
        Assert.assertEquals(1252, buffer.getSize());

    }

}
