package science.unlicense.common.api.character;

import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 *
 * @author Johann Sorel
 */
public class LanguageTest {

    @Test
    public void testCase() {
        final int lcp = 107; // k
        final int ucp = 75; // K
        Assert.assertEquals(ucp,Languages.UNSET.toUpperCase(lcp));
        Assert.assertEquals(lcp,Languages.UNSET.toLowerCase(ucp));

        final Char lc = new Char(lcp, CharEncodings.US_ASCII);
        final Char uc = new Char(ucp, CharEncodings.US_ASCII);
        Assert.assertEquals(uc,Languages.UNSET.toUpperCase(lc));
        Assert.assertEquals(lc,Languages.UNSET.toLowerCase(uc));

        final Chars lcs = new Chars(new byte[]{'-','a','b','c','z','*'}, CharEncodings.US_ASCII);
        final Chars ucs = new Chars(new byte[]{'-','A','B','C','Z','*'}, CharEncodings.US_ASCII);
        Assert.assertEquals(ucs,Languages.UNSET.toUpperCase(lcs));
        Assert.assertEquals(lcs,Languages.UNSET.toLowerCase(ucs));

    }
}
