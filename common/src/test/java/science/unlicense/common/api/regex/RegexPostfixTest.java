
package science.unlicense.common.api.regex;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;

/**
 * Regular expression using postfix syntax.
 *
 * @author Johann Sorel
 */
public class RegexPostfixTest {

    @Test
    public void testSingleChar() {

        final Chars exp = new Chars("a");
        final RegexExec exec = Regex.compilePostfix(exp);

        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
    }

    @Test
    public void testZeroOne() {

        final Chars exp = new Chars("a?");
        final RegexExec exec = Regex.compilePostfix(exp);

        Assert.assertTrue(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
    }

    @Test
    public void testZeroMany() {

        final Chars exp = new Chars("a*");
        final RegexExec exec = Regex.compilePostfix(exp);

        Assert.assertTrue(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertTrue(exec.match(new Chars("aa")));
        Assert.assertTrue(exec.match(new Chars("aaaaaaa")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }

    @Test
    public void testOneMany() {

        final Chars exp = new Chars("a+");
        final RegexExec exec = Regex.compilePostfix(exp);

        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertTrue(exec.match(new Chars("aa")));
        Assert.assertTrue(exec.match(new Chars("aaaaaaa")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }

    @Test
    public void testConcatenation() {

        final Chars exp = new Chars("ab>c>d>");
        final RegexExec exec = Regex.compilePostfix(exp);

        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("abc")));
        Assert.assertFalse(exec.match(new Chars("bcd")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertTrue(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("abcdz")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }

}
