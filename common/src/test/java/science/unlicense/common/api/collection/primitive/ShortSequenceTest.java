
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.collection.primitive.ShortSequence;
import science.unlicense.common.api.collection.primitive.PrimitiveSequence;

/**
 *
 * @author Johann Sorel
 */
public class ShortSequenceTest extends PrimitiveSequenceTest {

    @Override
    protected PrimitiveSequence createNew() {
        return new ShortSequence();
    }
@Override
    protected Object[] createDifferentValues(int nb) {
        final Object[] values = new Object[nb];
        for (int i=0;i<nb;i++) values[i] = (short) i;
        return values;
    }

}
