
package science.unlicense.common.api.collection;

import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.collection.HashSet;


/**
 * Test sequence.
 *
 * @author Johann Sorel
 */
public class HashSetTest extends AbstractCollectionTest {

    @Override
    protected Set createNew() {
        return new HashSet();
    }

    @Override
    protected Object[] createDifferentValues(int nb) {
        final Object[] values = new Object[nb];
        for (int i=0;i<nb;i++) values[i] = new String(""+i);
        return values;
    }

}
