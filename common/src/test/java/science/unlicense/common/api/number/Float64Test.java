
package science.unlicense.common.api.number;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class Float64Test {

    private static final double DELTA = 0.0000001;


    //TODO
    public void skipDoubleEncoding() {
        Chars cs;
        double d;

        d = 1234560789;
        cs = Float64.encode(d);
        Assert.assertEquals(CharEncodings.US_ASCII, cs.getEncoding());
        Assert.assertArrayEquals(new byte[]{'1','2','3','4','5','6','0','7','8','9'}, cs.toBytes());

    }

    @Test
    public void testDoubleDecoding() {
        Chars cs;
        double d;

        cs = new Chars(new byte[]{'1','2','3','4','5','6','0','7','8','9'});
        d = Float64.decode(cs);
        Assert.assertEquals(1234560789d, d,DELTA);

        cs = new Chars(new byte[]{'+','1','2','3','4','5','6','.','0','7','8','9'});
        d = Float64.decode(cs);
        Assert.assertEquals(+123456.0789d, d,DELTA);

        cs = new Chars(new byte[]{'-','1','2','3','4','5','6','.','0','7','8','9'});
        d = Float64.decode(cs);
        Assert.assertEquals(-123456.0789d, d,DELTA);

        cs = new Chars(new byte[]{'-','1','2','3','4','5','6','.','0','7','8','9','E','7'});
        d = Float64.decode(cs);
        Assert.assertEquals(-123456.0789E7d, d,DELTA);

        cs = new Chars(new byte[]{'-','1','2','3','4','5','6','.','0','7','8','9','e','-','7'});
        d = Float64.decode(cs);
        Assert.assertEquals(-123456.0789e-7d, d,DELTA);

        cs = new Chars(new byte[]{'0','.','2','3','4','5','6','6','0','7','8','9','1','2','2','2','4','5','6','7','8','9'});
        d = Float64.decode(cs);
        Assert.assertEquals(0.23456607891222456789, d,DELTA);
    }

}
