
package science.unlicense.common.api.collection;

import org.junit.Test;
import static science.unlicense.common.api.Assert.*;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractSetTest extends AbstractCollectionTest {

    @Override
    protected abstract Set createNew();


    @Test
    public void testAdd() {
        final Object[] values = createDifferentValues(3);

        final Set set = createNew();
        assertEquals(0, set.getSize());
        set.add(values[0]);
        assertEquals(1, set.getSize());
        set.add(values[1]);
        assertEquals(2, set.getSize());
        set.add(values[2]);
        assertEquals(3, set.getSize());
        set.add(values[2]);
        assertEquals(3, set.getSize());
        set.add(values[1]);
        assertEquals(3, set.getSize());

    }

}
