
package science.unlicense.common.api.array;

import science.unlicense.common.api.Arrays;
import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 * TODO : reorganize properly ArrayTest and ArraysTest
 *
 * @author Johann Sorel
 */
public class ArraysTest {

    @Test
    public void testCopyInt() {

        //forward order copy
        {
        final int[] array = new int[]{0,1,2,3,4,5,6};
        Arrays.copy(array, 2, 3, array, 0);
        Assert.assertArrayEquals(new int[]{2,3,4,3,4,5,6}, array);
        }

        //reverse order copy
        {
        final int[] array = new int[]{0,1,2,3,4,5,6};
        Arrays.copy(array, 2, 3, array, 3);
        Assert.assertArrayEquals(new int[]{0,1,2,2,3,4,6}, array);
        }
    }

}
