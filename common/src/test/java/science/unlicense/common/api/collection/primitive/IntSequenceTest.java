
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.collection.primitive.PrimitiveSequence;
import science.unlicense.common.api.collection.primitive.IntSequence;

/**
 *
 * @author Johann Sorel
 */
public class IntSequenceTest extends PrimitiveSequenceTest {

    @Override
    protected PrimitiveSequence createNew() {
        return new IntSequence();
    }

    @Override
    protected Object[] createDifferentValues(int nb) {
        final Object[] values = new Object[nb];
        for (int i=0;i<nb;i++) values[i] = (int) i;
        return values;
    }

}
