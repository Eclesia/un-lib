
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.collection.primitive.PrimitiveSequence;
import science.unlicense.common.api.collection.primitive.ByteSequence;


/**
 *
 * @author Johann Sorel
 */
public class ByteSequenceTest extends PrimitiveSequenceTest {

    @Override
    protected PrimitiveSequence createNew() {
        return new ByteSequence();
    }

    @Override
    protected Object[] createDifferentValues(int nb) {
        final Object[] values = new Object[nb];
        for (int i=0;i<nb;i++) values[i] = (byte) i;
        return values;
    }

}
