
package science.unlicense.common.api.collection.primitive;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.SequenceTest;
import science.unlicense.common.api.exception.InvalidArgumentException;

/**
 *
 * @author Johann Sorel
 */
public abstract class PrimitiveSequenceTest extends SequenceTest {

    protected abstract PrimitiveSequence createNew();

    private void put(PrimitiveSequence seq, int val) {
        if (seq instanceof ByteSequence) ((ByteSequence) seq).put((byte) val);
        else if (seq instanceof ShortSequence) ((ShortSequence) seq).put((short) val);
        else if (seq instanceof IntSequence) ((IntSequence) seq).put(val);
        else if (seq instanceof LongSequence) ((LongSequence) seq).put(val);
        else if (seq instanceof FloatSequence) ((FloatSequence) seq).put(val);
        else if (seq instanceof DoubleSequence) ((DoubleSequence) seq).put(val);
        else throw new InvalidArgumentException("Unknown type : "+seq.getClass());
    }

    private void insert(PrimitiveSequence seq, int val) {
        if (seq instanceof ByteSequence) ((ByteSequence) seq).insert((byte) val);
        else if (seq instanceof ShortSequence) ((ShortSequence) seq).insert((short) val);
        else if (seq instanceof IntSequence) ((IntSequence) seq).insert(val);
        else if (seq instanceof LongSequence) ((LongSequence) seq).insert(val);
        else if (seq instanceof FloatSequence) ((FloatSequence) seq).insert(val);
        else if (seq instanceof DoubleSequence) ((DoubleSequence) seq).insert(val);
        else throw new InvalidArgumentException("Unknown type : "+seq.getClass());
    }

    private void insert(PrimitiveSequence seq, int[] val) {
        if (seq instanceof ByteSequence) ((ByteSequence) seq).insert(Arrays.reformatToByte(val));
        else if (seq instanceof ShortSequence) ((ShortSequence) seq).insert(Arrays.reformatToShort(val));
        else if (seq instanceof IntSequence) ((IntSequence) seq).insert(val);
        else if (seq instanceof LongSequence) ((LongSequence) seq).insert(Arrays.reformatToLong(val));
        else if (seq instanceof FloatSequence) ((FloatSequence) seq).insert(Arrays.reformatToFloat(val));
        else if (seq instanceof DoubleSequence) ((DoubleSequence) seq).insert(Arrays.reformatToDouble(val));
        else throw new InvalidArgumentException("Unknown type : "+seq.getClass());
    }

    private int[] toArrayInt(PrimitiveSequence seq) {
        if (seq instanceof ByteSequence) return Arrays.reformatToInt(((ByteSequence) seq).toArrayByte());
        if (seq instanceof ShortSequence) return Arrays.reformatToInt(((ShortSequence) seq).toArrayShort());
        if (seq instanceof IntSequence) return ((IntSequence) seq).toArrayInt();
        if (seq instanceof LongSequence) return Arrays.reformatToInt(((LongSequence) seq).toArrayLong());
        if (seq instanceof FloatSequence) return Arrays.reformatToInt(((FloatSequence) seq).toArrayFloat());
        if (seq instanceof DoubleSequence) return Arrays.reformatToInt(((DoubleSequence) seq).toArrayDouble());
        else throw new InvalidArgumentException("Unknown type : "+seq.getClass());
    }

    @Test
    public void testSequence() {

        final PrimitiveSequence seq = createNew();
        Assert.assertArrayEquals(new int[]{},toArrayInt(seq));

        put(seq,1);
        put(seq,2);
        put(seq,3);
        Assert.assertArrayEquals(new int[]{1,2,3},toArrayInt(seq));

        seq.moveTo(1);
        put(seq,25);
        Assert.assertArrayEquals(new int[]{1,25,3},toArrayInt(seq));

        seq.moveTo(1);
        insert(seq,15);
        Assert.assertArrayEquals(new int[]{1,15,25,3},toArrayInt(seq));
        insert(seq,16);
        Assert.assertArrayEquals(new int[]{1,15,16,25,3},toArrayInt(seq));

        seq.moveTo(0);
        insert(seq,new int[]{101,-54,47});
        Assert.assertArrayEquals(new int[]{101,-54,47,1,15,16,25,3},toArrayInt(seq));

        seq.moveTo(2);
        seq.removeNext(4);
        Assert.assertArrayEquals(new int[]{101,-54,25,3},toArrayInt(seq));

        seq.moveTo(2);
        seq.removeNext(2);
        Assert.assertArrayEquals(new int[]{101,-54},toArrayInt(seq));

    }

}
