
package science.unlicense.common.api.array;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultFloat32Buffer;

/**
 *
 * @author Johann Sorel
 */
public class DefaultFloatBufferTest extends AbstractBufferTest{

    @Override
    public Buffer create(int nbByte) {
        return new DefaultFloat32Buffer(new float[nbByte/4]);
    }

}
