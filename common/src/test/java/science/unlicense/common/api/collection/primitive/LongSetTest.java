
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.collection.primitive.LongSet;
import science.unlicense.common.api.collection.AbstractSetTest;
import science.unlicense.common.api.collection.Set;

/**
 *
 * @author Johann Sorel
 */
public class LongSetTest extends AbstractSetTest{

    @Override
    protected boolean canSendEvents() {
        return false;
    }

    @Override
    protected Set createNew() {
        return new LongSet();
    }

    @Override
    protected Object[] createDifferentValues(int nb) {
        final Object[] values = new Object[nb];
        for (int i=0;i<nb;i++) values[i] = (long) i;
        return values;
    }

}
