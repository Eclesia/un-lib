
package science.unlicense.common.api.predicate;

import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 *
 * @author Johann Sorel
 */
public class VariableTest {

    @Test
    public void testGetSetValue() {

        final Variable var1 = new DefaultVariable("test1");
        Assert.assertEquals(var1.getValue(), "test1");

        var1.setValue("test2");
        Assert.assertEquals(var1.getValue(), "test2");

    }

    @Test
    public void testSyncRead() {

        final Variable var1 = new DefaultVariable("test1");
        final Variable var2 = new DefaultVariable("test2");

        var1.sync(var2, true);
        //var1 value should have been set to var2 value
        Assert.assertEquals(var1.getValue(), "test2");

        //check read link
        var2.setValue("test3");
        Assert.assertEquals(var1.getValue(), "test3");

        //check write link fails
        try{
            var1.setValue("test4");
            Assert.fail("Setting value on synced value should have failed.");
        }catch(VariableSyncException ex) {
            //ok
        }
        //NOTE : value has been modified, do we consider it normal ?
        Assert.assertEquals(var1.getValue(), "test4");
        Assert.assertEquals(var2.getValue(), "test3");

    }

    @Test
    public void testSyncReadWrite() {

        final Variable var1 = new DefaultVariable("test1");
        final Variable var2 = new DefaultVariable("test2");

        var1.sync(var2);
        //var1 value should have been set to var2 value
        Assert.assertEquals(var1.getValue(), "test2");

        //check read link
        var2.setValue("test3");
        Assert.assertEquals(var1.getValue(), "test3");

        //check write link active
        var1.setValue("test4");
        Assert.assertEquals(var1.getValue(), "test4");
        Assert.assertEquals(var2.getValue(), "test4");

    }

}
