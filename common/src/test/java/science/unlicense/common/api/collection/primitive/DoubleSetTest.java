
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.collection.primitive.DoubleSet;
import science.unlicense.common.api.collection.AbstractSetTest;
import science.unlicense.common.api.collection.Set;

/**
 *
 * @author Johann Sorel
 */
public class DoubleSetTest extends AbstractSetTest{

    @Override
    protected boolean canSendEvents() {
        return false;
    }

    @Override
    protected Set createNew() {
        return new DoubleSet();
    }

    @Override
    protected Object[] createDifferentValues(int nb) {
        final Object[] values = new Object[nb];
        for (int i=0;i<nb;i++) values[i] = (double) i;
        return values;
    }

}
