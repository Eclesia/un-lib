
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.collection.primitive.PrimitiveSequence;
import science.unlicense.common.api.collection.primitive.FloatSequence;

/**
 *
 * @author Johann Sorel
 */
public class FloatSequenceTest extends PrimitiveSequenceTest {

    @Override
    protected PrimitiveSequence createNew() {
        return new FloatSequence();
    }

    @Override
    protected Object[] createDifferentValues(int nb) {
        final Object[] values = new Object[nb];
        for (int i=0;i<nb;i++) values[i] = (float) i;
        return values;
    }

}
