
package science.unlicense.common.api.collection;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class HashDictionnaryTest {

    private Dictionary createNew() {
        //TODO preparing abstract dictionary test case
        return new HashDictionary();
    }

    @Test
    public void testNullSearch() {
        final Dictionary dico = createNew();
        final Object res = dico.getValue(null);
        Assert.assertNull(res);
    }

    @Test
    public void testNullKey() {
        final Chars value1 = new Chars("hello1");
        final Chars value2 = new Chars("hello2");
        Object res;
        final Dictionary dico = createNew();

        //first add
        dico.add(null, value1);
        res = dico.getValue(null);
        Assert.assertEquals(value1, res);
        //replace value
        dico.add(null, value2);
        res = dico.getValue(null);
        Assert.assertEquals(value2, res);
        //remove value
        dico.remove(null);
        res = dico.getValue(null);
        Assert.assertNull(res);
    }

    @Test
    public void testChars() {
        final Chars key1 = new Chars("a");
        final Chars key2 = new Chars("b");
        final Chars value1 = new Chars("hello1");
        final Chars value2 = new Chars("hello2");
        final Dictionary dico = createNew();
        Object res;
        dico.add(key1, value1);
        dico.add(key2, value2);

        Assert.assertEquals(value1, dico.getValue(key1));
        Assert.assertEquals(value2, dico.getValue(key2));

    }

}
