
package science.unlicense.common.api.array;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultFloat64Buffer;

/**
 *
 * @author Johann Sorel
 */
public class DefaultDoubleBufferTest extends AbstractBufferTest{

    @Override
    public Buffer create(int nbByte) {
        return new DefaultFloat64Buffer(new double[nbByte/8]);
    }

}
