
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.collection.primitive.LongSequence;
import science.unlicense.common.api.collection.primitive.PrimitiveSequence;

/**
 *
 * @author Johann Sorel
 */
public class LongSequenceTest extends PrimitiveSequenceTest {

    @Override
    protected PrimitiveSequence createNew() {
        return new LongSequence();
    }
@Override
    protected Object[] createDifferentValues(int nb) {
        final Object[] values = new Object[nb];
        for (int i=0;i<nb;i++) values[i] = (long) i;
        return values;
    }

}
