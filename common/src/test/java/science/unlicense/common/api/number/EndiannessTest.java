
package science.unlicense.common.api.number;

import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 *
 * @author Johann Sorel
 */
public class EndiannessTest {

    @Test
    public void testReverseOrder() {

        byte[] array = new byte[]{0,1,2,3,4,5,6,7};
        Endianness.reverseOrder(array, 0);
        Assert.assertArrayEquals(new byte[]{0,1,2,3,4,5,6,7}, array);

        array = new byte[]{0,1,2,3,4,5,6,7};
        Endianness.reverseOrder(array, 1);
        Assert.assertArrayEquals(new byte[]{0,1,2,3,4,5,6,7}, array);

        array = new byte[]{0,1,2,3,4,5,6,7};
        Endianness.reverseOrder(array, 2);
        Assert.assertArrayEquals(new byte[]{1,0,3,2,5,4,7,6}, array);

        array = new byte[]{0,1,2,3,4,5,6,7,8};
        Endianness.reverseOrder(array, 3);
        Assert.assertArrayEquals(new byte[]{2,1,0,5,4,3,8,7,6}, array);

        array = new byte[]{0,1,2,3,4,5,6,7};
        Endianness.reverseOrder(array, 4);
        Assert.assertArrayEquals(new byte[]{3,2,1,0,7,6,5,4}, array);

        array = new byte[]{0,1,2,3,4,5,6,7};
        Endianness.reverseOrder(array, 8);
        Assert.assertArrayEquals(new byte[]{7,6,5,4,3,2,1,0}, array);

    }

}
