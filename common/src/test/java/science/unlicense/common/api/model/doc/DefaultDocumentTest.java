
package science.unlicense.common.api.model.doc;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DefaultDocumentTest {

    @Test
    public void testDefaultValueUsed() {

        final FieldType ft = new FieldTypeBuilder(new Chars("att")).valueClass(Chars.class).defaultValue(new Chars("hello")).build();
        final DocumentType dt = new DefaultDocumentType(new Chars("dt"), null, null, true, new FieldType[]{ft}, null);

        final Document doc = new DefaultDocument(dt);
        Assert.assertEquals(new Chars("hello"), doc.getPropertyValue(new Chars("att")));

    }

}
