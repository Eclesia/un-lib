
package science.unlicense.common.api.doc;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.DocMessage;
import science.unlicense.common.api.model.doc.Document;

/**
 * Test Document events.
 *
 * @author Johann Sorel
 */
public class DocumentEventTest {

    private static final Chars NAME = Chars.constant("testField");

    @Test
    public void testSingleValue() {

        final EventCollector collector = new EventCollector();

        final DefaultDocument doc = new DefaultDocument();
        doc.addEventListener(DocMessage.PREDICATE, collector);

        //event setting value
        doc.getProperty(NAME).setValue(15);
        Assert.assertEquals(1,collector.events.getSize());
        DocMessage event = (DocMessage) ((Event) collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        DocMessage.FieldChange change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(null, change.getOldValue());
        Assert.assertEquals(15, change.getNewValue());
        collector.reset();

        //no event setting the same value again
        doc.getProperty(NAME).setValue(15);
        Assert.assertEquals(0,collector.events.getSize());
        collector.reset();

        //change value
        doc.getProperty(NAME).setValue(22);
        Assert.assertEquals(1,collector.events.getSize());
        event = (DocMessage) ((Event) collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(15, change.getOldValue());
        Assert.assertEquals(22, change.getNewValue());
        collector.reset();

        //set to null
        doc.getProperty(NAME).setValue(null);
        Assert.assertEquals(1,collector.events.getSize());
        event = (DocMessage) ((Event) collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(22, change.getOldValue());
        Assert.assertEquals(null, change.getNewValue());
        collector.reset();
    }

    @Test
    public void testDocValue() {

        final EventCollector collector = new EventCollector();

        final Document val1 = new DefaultDocument();
        val1.getProperty(NAME).setValue(10);
        final Document val2 = new DefaultDocument();
        val2.getProperty(NAME).setValue(10);
        final Document val3 = new DefaultDocument();
        val3.getProperty(NAME).setValue(20);

        final DefaultDocument doc = new DefaultDocument();
        doc.addEventListener(DocMessage.PREDICATE, collector);

        //event setting value
        doc.getProperty(NAME).setValue(val1);
        Assert.assertEquals(1,collector.events.getSize());
        DocMessage event = (DocMessage) ((Event) collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        DocMessage.FieldChange change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(null, change.getOldValue());
        Assert.assertEquals(val1, change.getNewValue());
        collector.reset();

        //no event setting the equal value again
        doc.getProperty(NAME).setValue(val2);
        Assert.assertEquals(0,collector.events.getSize());
        collector.reset();

        //change value
        doc.getProperty(NAME).setValue(val3);
        Assert.assertEquals(1,collector.events.getSize());
        event = (DocMessage) ((Event) collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(val1, change.getOldValue());
        Assert.assertEquals(val3, change.getNewValue());
        collector.reset();

        //set to null
        doc.getProperty(NAME).setValue(null);
        Assert.assertEquals(1,collector.events.getSize());
        event = (DocMessage) ((Event) collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(val3, change.getOldValue());
        Assert.assertEquals(null, change.getNewValue());
        collector.reset();
    }

    private static class EventCollector implements EventListener{

        private final Sequence events = new ArraySequence();

        public void reset() {
            events.removeAll();
        }

        @Override
        public void receiveEvent(Event event) {
            events.add(event);
        }

    }

}
