
package science.unlicense.common.api.buffer;

import org.junit.Test;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class DefaultCursorTest {

    /**
     * Test cursor correctly end writing on the last buffer bits.
     * This test ensure the cursor to not try to read the next byte
     * when he is on the last one.
     */
    @Test
    public void testWriteLastBits() {

        final Buffer buffer = new DefaultInt8Buffer(new byte[2]);
        final DefaultCursor cursor = new DefaultCursor(buffer, Endianness.BIG_ENDIAN);
        cursor.writeBit(0, 4);
        cursor.writeBit(0, 4);
        cursor.writeBit(0, 4);
        cursor.writeBit(0, 4);


    }

}
