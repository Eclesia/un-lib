
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.collection.primitive.PrimitiveSequence;
import science.unlicense.common.api.collection.primitive.DoubleSequence;

/**
 *
 * @author Johann Sorel
 */
public class DoubleSequenceTest extends PrimitiveSequenceTest {

    @Override
    protected PrimitiveSequence createNew() {
        return new DoubleSequence();
    }

    @Override
    protected Object[] createDifferentValues(int nb) {
        final Object[] values = new Object[nb];
        for (int i=0;i<nb;i++) values[i] = (double) i;
        return values;
    }

}
