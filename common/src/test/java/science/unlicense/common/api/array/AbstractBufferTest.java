
package science.unlicense.common.api.array;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DataCursor;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractBufferTest {

    private static final double DELTA = 0.000001;

    public abstract Buffer create(int nbByte);

    @Test
    public void testRWByte() {
        final Buffer buffer = create(16);

        final byte[] expected = new byte[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

        buffer.writeInt8(expected,0);

        final byte[] res = new byte[16];
        buffer.readInt8(res, 0);

        Assert.assertArrayEquals(expected, res);
    }

    @Test
    public void testRWCursor() {
        final Buffer buffer = create(16);
        final DataCursor cursor = buffer.dataCursor();
        cursor.writeFloat(-1);
        cursor.writeFloat(12);
        cursor.writeFloat(21);
        cursor.writeFloat(-46);

        Assert.assertEquals(-1, buffer.readFloat32(0), DELTA);
        Assert.assertEquals(12, buffer.readFloat32(4), DELTA);
        Assert.assertEquals(21, buffer.readFloat32(8), DELTA);
        Assert.assertEquals(-46, buffer.readFloat32(12), DELTA);

    }

}
