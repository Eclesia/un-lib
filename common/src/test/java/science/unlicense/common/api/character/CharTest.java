
package science.unlicense.common.api.character;

import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 * Char class tests.
 *
 * @author Johann Sorel
 */
public class CharTest {

    @Test
    public void testCase() {
        final Char c1 = new Char('a');
        final Char c2 = new Char('z');
        final Char c3 = new Char('A');
        final Char c4 = new Char('Z');
        final Char c5 = new Char('%');

        //test isUpperCase
        Assert.assertFalse(c1.isUpperCase());
        Assert.assertFalse(c2.isUpperCase());
        Assert.assertTrue(c3.isUpperCase());
        Assert.assertTrue(c4.isUpperCase());
        Assert.assertTrue(c5.isUpperCase());
        //test toUpperCase
        Assert.assertEquals(new Char('A'), c1.toUpperCase());
        Assert.assertEquals(new Char('Z'), c2.toUpperCase());
        Assert.assertEquals(new Char('A'), c3.toUpperCase());
        Assert.assertEquals(new Char('Z'), c4.toUpperCase());
        Assert.assertEquals(new Char('%'), c5.toUpperCase());

        //test isLowerCase
        Assert.assertTrue(c1.isLowerCase());
        Assert.assertTrue(c2.isLowerCase());
        Assert.assertFalse(c3.isLowerCase());
        Assert.assertFalse(c4.isLowerCase());
        Assert.assertTrue(c5.isLowerCase());
        //test toLowerCase
        Assert.assertEquals(new Char('a'), c1.toLowerCase());
        Assert.assertEquals(new Char('z'), c2.toLowerCase());
        Assert.assertEquals(new Char('a'), c3.toLowerCase());
        Assert.assertEquals(new Char('z'), c4.toLowerCase());
        Assert.assertEquals(new Char('%'), c5.toLowerCase());


    }

    @Test
    public void testEquals() {
        final Char c1 = new Char(new byte[]{'h'}, CharEncodings.US_ASCII);
        final Char c2 = new Char(new byte[]{'h'}, CharEncodings.UTF_8);
        final Char c3 = new Char(new byte[]{'H'}, CharEncodings.US_ASCII);
        final Char c4 = new Char(new byte[]{'H'}, CharEncodings.UTF_8);
        final Char c5 = new Char(new byte[]{'z'}, CharEncodings.US_ASCII);

        //test strict
        Assert.assertFalse(c1.equals(c2, false, false));
        Assert.assertFalse(c1.equals(c3, false, false));
        Assert.assertFalse(c1.equals(c4, false, false));
        Assert.assertFalse(c1.equals(c5, false, false));

        //test without case
        Assert.assertFalse(c1.equals(c2, true, false));
        Assert.assertTrue(c1.equals(c3, true, false));
        Assert.assertFalse(c1.equals(c4, true, false));
        Assert.assertFalse(c1.equals(c5, true, false));

        //test without encoding, this is the default equals
        Assert.assertTrue(c1.equals(c2, false, true));
        Assert.assertFalse(c1.equals(c3, false, true));
        Assert.assertFalse(c1.equals(c4, false, true));
        Assert.assertFalse(c1.equals(c5, false, true));
        Assert.assertTrue(c1.equals(c2));
        Assert.assertFalse(c1.equals(c3));
        Assert.assertFalse(c1.equals(c4));
        Assert.assertFalse(c1.equals(c5));

        //test without case and encoding
        Assert.assertTrue(c1.equals(c2, true, true));
        Assert.assertTrue(c1.equals(c3, true, true));
        Assert.assertTrue(c1.equals(c4, true, true));
        Assert.assertFalse(c1.equals(c5, true, true));

    }

}
