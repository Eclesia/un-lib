
package science.unlicense.common.api.model.tree;

import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 *
 * @author Johann Sorel
 */
public class NodeSequenceTest {

    @Test
    public void testNodeSequence() {

        {//test including the root node
            final DefaultNode root = new DefaultNode(true);
            final NodeSequence seq = new NodeSequence(root, true);
            Assert.assertEquals(1,seq.getSize());

            //add one direct child
            final DefaultNode child1 = new DefaultNode(true);
            root.getChildren().add(child1);
            Assert.assertEquals(2,seq.getSize());

            //add one undirect child
            final DefaultNode child2 = new DefaultNode(true);
            child1.getChildren().add(child2);
            Assert.assertEquals(3,seq.getSize());

            //remove undirect child
            child1.getChildren().remove(0);
            Assert.assertEquals(2,seq.getSize());

            //remove direct child
            root.getChildren().remove(0);
            Assert.assertEquals(1,seq.getSize());
        }

        {//test excluding the root node
            final DefaultNode root = new DefaultNode(true);
            final NodeSequence seq = new NodeSequence(root, false);
            Assert.assertEquals(0,seq.getSize());

            //add one direct child
            final DefaultNode child1 = new DefaultNode(true);
            root.getChildren().add(child1);
            Assert.assertEquals(1,seq.getSize());

            //add one undirect child
            final DefaultNode child2 = new DefaultNode(true);
            child1.getChildren().add(child2);
            Assert.assertEquals(2,seq.getSize());

            //remove undirect child
            child1.getChildren().remove(0);
            Assert.assertEquals(1,seq.getSize());

            //remove direct child
            root.getChildren().remove(0);
            Assert.assertEquals(0,seq.getSize());
        }
    }

}
