
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractCursor implements Cursor {

    protected Endianness encoding;

    public AbstractCursor(Endianness encoding) {
        this.encoding = encoding;
    }

    public Endianness getEndianness() {
        return encoding;
    }

}
