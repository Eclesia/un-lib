
package science.unlicense.common.api.country;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.character.DefaultLanguage;
import science.unlicense.common.api.character.Language;
import science.unlicense.common.api.character.Language.Division;
import science.unlicense.common.api.character.Languages;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;

/**
 * List of all countries.
 *
 * @author Johann Sorel
 */
public class Country extends CObject implements Division {


    public static final Chars COUNTRY = Chars.constant("country");

    private final Chars name;
    private final Chars code2;
    private final Chars code3;
    private final int num;

    //cache language
    private Language language;

    /**
     *
     * @param name country name
     * @param code2 ISO Code 2
     * @param code3 ISO Code 3
     * @param num ISO number
     */
    public Country(Chars name, Chars code2, Chars code3, int num) {
        this.name = name;
        this.code2 = code2;
        this.code3 = code3;
        this.num = num;
    }

    /**
     * Division type.
     *
     * @return "country"
     */
    public Chars getType() {
        return COUNTRY;
    }

    /**
     * Get Unique code.
     *
     * @return ISO Code 3
     */
    public Chars getCode() {
        return code3;
    }

    /**
     * Get country name.
     *
     * @return Chars, never null
     */
    public Chars getName() {
        return name;
    }

    /**
     * Get country ISO Code 2.
     *
     * @return Chars, never null
     */
    public Chars getISOCode2() {
        return code2;
    }

    /**
     * Get country ISO Code 3.
     *
     * @return Chars, never null
     */
    public Chars getISOCode3() {
        return code3;
    }

    /**
     * Get country ISO number.
     *
     * @return int ISO number
     */
    public int getISONum() {
        return num;
    }

    public Chars toChars() {
        return code3;
    }

    /**
     * Create a single division language.
     *
     * @return Language
     */
    public synchronized Language asLanguage() {
        if (language==null) {
            language = new DefaultLanguage(this, Languages.UNSET);
        }
        return language;
    }

    public static final Country AFG = new Country(new Chars("Afghanistan"), new Chars("AF"), new Chars("AFG"), 4);
    public static final Country ALA = new Country(new Chars("Åland Islands"), new Chars("AX"), new Chars("ALA"), 248);
    public static final Country ALB = new Country(new Chars("Albania"), new Chars("AL"), new Chars("ALB"), 8);
    public static final Country DZA = new Country(new Chars("Algeria"), new Chars("DZ"), new Chars("DZA"), 12);
    public static final Country ASM = new Country(new Chars("American Samoa"), new Chars("AS"), new Chars("ASM"), 16);
    public static final Country AND = new Country(new Chars("Andorra"), new Chars("AD"), new Chars("AND"), 20);
    public static final Country AGO = new Country(new Chars("Angola"), new Chars("AO"), new Chars("AGO"), 24);
    public static final Country AIA = new Country(new Chars("Anguilla"), new Chars("AI"), new Chars("AIA"), 660);
    public static final Country ATA = new Country(new Chars("Antarctica"), new Chars("AQ"), new Chars("ATA"), 10);
    public static final Country ATG = new Country(new Chars("Antigua and Barbuda"), new Chars("AG"), new Chars("ATG"), 28);
    public static final Country ARG = new Country(new Chars("Argentina"), new Chars("AR"), new Chars("ARG"), 32);
    public static final Country ARM = new Country(new Chars("Armenia"), new Chars("AM"), new Chars("ARM"), 51);
    public static final Country ABW = new Country(new Chars("Aruba"), new Chars("AW"), new Chars("ABW"), 533);
    public static final Country AUS = new Country(new Chars("Australia"), new Chars("AU"), new Chars("AUS"), 36);
    public static final Country AUT = new Country(new Chars("Austria"), new Chars("AT"), new Chars("AUT"), 40);
    public static final Country AZE = new Country(new Chars("Azerbaijan"), new Chars("AZ"), new Chars("AZE"), 31);
    public static final Country BHS = new Country(new Chars("Bahamas"), new Chars("BS"), new Chars("BHS"), 44);
    public static final Country BHR = new Country(new Chars("Bahrain"), new Chars("BH"), new Chars("BHR"), 48);
    public static final Country BGD = new Country(new Chars("Bangladesh"), new Chars("BD"), new Chars("BGD"), 50);
    public static final Country BRB = new Country(new Chars("Barbados"), new Chars("BB"), new Chars("BRB"), 52);
    public static final Country BLR = new Country(new Chars("Belarus"), new Chars("BY"), new Chars("BLR"), 112);
    public static final Country BEL = new Country(new Chars("Belgium"), new Chars("BE"), new Chars("BEL"), 56);
    public static final Country BLZ = new Country(new Chars("Belize"), new Chars("BZ"), new Chars("BLZ"), 84);
    public static final Country BEN = new Country(new Chars("Benin"), new Chars("BJ"), new Chars("BEN"), 204);
    public static final Country BMU = new Country(new Chars("Bermuda"), new Chars("BM"), new Chars("BMU"), 60);
    public static final Country BTN = new Country(new Chars("Bhutan"), new Chars("BT"), new Chars("BTN"), 64);
    public static final Country BOL = new Country(new Chars("Plurinational State of Bolivia"), new Chars("BO"), new Chars("BOL"), 68);
    public static final Country BES = new Country(new Chars("Sint Eustatius and Saba Bonaire"), new Chars("BQ"), new Chars("BES"), 535);
    public static final Country BIH = new Country(new Chars("Bosnia and Herzegovina"), new Chars("BA"), new Chars("BIH"), 70);
    public static final Country BWA = new Country(new Chars("Botswana"), new Chars("BW"), new Chars("BWA"), 72);
    public static final Country BVT = new Country(new Chars("Bouvet Island"), new Chars("BV"), new Chars("BVT"), 74);
    public static final Country BRA = new Country(new Chars("Brazil"), new Chars("BR"), new Chars("BRA"), 76);
    public static final Country IOT = new Country(new Chars("British Indian Ocean Territory"), new Chars("IO"), new Chars("IOT"), 86);
    public static final Country BRN = new Country(new Chars("Brunei Darussalam"), new Chars("BN"), new Chars("BRN"), 96);
    public static final Country BGR = new Country(new Chars("Bulgaria"), new Chars("BG"), new Chars("BGR"), 100);
    public static final Country BFA = new Country(new Chars("Burkina Faso"), new Chars("BF"), new Chars("BFA"), 854);
    public static final Country BDI = new Country(new Chars("Burundi"), new Chars("BI"), new Chars("BDI"), 108);
    public static final Country KHM = new Country(new Chars("Cambodia"), new Chars("KH"), new Chars("KHM"), 116);
    public static final Country CMR = new Country(new Chars("Cameroon"), new Chars("CM"), new Chars("CMR"), 120);
    public static final Country CAN = new Country(new Chars("Canada"), new Chars("CA"), new Chars("CAN"), 124);
    public static final Country CPV = new Country(new Chars("Cape Verde"), new Chars("CV"), new Chars("CPV"), 132);
    public static final Country CYM = new Country(new Chars("Cayman Islands"), new Chars("KY"), new Chars("CYM"), 136);
    public static final Country CAF = new Country(new Chars("Central African Republic"), new Chars("CF"), new Chars("CAF"), 140);
    public static final Country TCD = new Country(new Chars("Chad"), new Chars("TD"), new Chars("TCD"), 148);
    public static final Country CHL = new Country(new Chars("Chile"), new Chars("CL"), new Chars("CHL"), 152);
    public static final Country CHN = new Country(new Chars("China"), new Chars("CN"), new Chars("CHN"), 156);
    public static final Country CXR = new Country(new Chars("Christmas Island"), new Chars("CX"), new Chars("CXR"), 162);
    public static final Country CCK = new Country(new Chars("Cocos (Keeling) Islands"), new Chars("CC"), new Chars("CCK"), 166);
    public static final Country COL = new Country(new Chars("Colombia"), new Chars("CO"), new Chars("COL"), 170);
    public static final Country COM = new Country(new Chars("Comoros"), new Chars("KM"), new Chars("COM"), 174);
    public static final Country COG = new Country(new Chars("Congo"), new Chars("CG"), new Chars("COG"), 178);
    public static final Country COD = new Country(new Chars("Congo, the Democratic Republic of the"), new Chars("CD"), new Chars("COD"), 180);
    public static final Country COK = new Country(new Chars("Cook Islands"), new Chars("CK"), new Chars("COK"), 184);
    public static final Country CRI = new Country(new Chars("Costa Rica"), new Chars("CR"), new Chars("CRI"), 188);
    public static final Country CIV = new Country(new Chars("Côte d'Ivoire"), new Chars("CI"), new Chars("CIV"), 384);
    public static final Country HRV = new Country(new Chars("Croatia"), new Chars("HR"), new Chars("HRV"), 191);
    public static final Country CUB = new Country(new Chars("Cuba"), new Chars("CU"), new Chars("CUB"), 192);
    public static final Country CUW = new Country(new Chars("Curaçao"), new Chars("CW"), new Chars("CUW"), 531);
    public static final Country CYP = new Country(new Chars("Cyprus"), new Chars("CY"), new Chars("CYP"), 196);
    public static final Country CZE = new Country(new Chars("Czech Republic"), new Chars("CZ"), new Chars("CZE"), 203);
    public static final Country DNK = new Country(new Chars("Denmark"), new Chars("DK"), new Chars("DNK"), 208);
    public static final Country DJI = new Country(new Chars("Djibouti"), new Chars("DJ"), new Chars("DJI"), 262);
    public static final Country DMA = new Country(new Chars("Dominica"), new Chars("DM"), new Chars("DMA"), 212);
    public static final Country DOM = new Country(new Chars("Dominican Republic"), new Chars("DO"), new Chars("DOM"), 214);
    public static final Country ECU = new Country(new Chars("Ecuador"), new Chars("EC"), new Chars("ECU"), 218);
    public static final Country EGY = new Country(new Chars("Egypt"), new Chars("EG"), new Chars("EGY"), 818);
    public static final Country SLV = new Country(new Chars("El Salvador"), new Chars("SV"), new Chars("SLV"), 222);
    public static final Country GNQ = new Country(new Chars("Equatorial Guinea"), new Chars("GQ"), new Chars("GNQ"), 226);
    public static final Country ERI = new Country(new Chars("Eritrea"), new Chars("ER"), new Chars("ERI"), 232);
    public static final Country EST = new Country(new Chars("Estonia"), new Chars("EE"), new Chars("EST"), 233);
    public static final Country ETH = new Country(new Chars("Ethiopia"), new Chars("ET"), new Chars("ETH"), 231);
    public static final Country FLK = new Country(new Chars("Falkland Islands (Malvinas)"), new Chars("FK"), new Chars("FLK"), 238);
    public static final Country FRO = new Country(new Chars("Faroe Islands"), new Chars("FO"), new Chars("FRO"), 234);
    public static final Country FJI = new Country(new Chars("Fiji"), new Chars("FJ"), new Chars("FJI"), 242);
    public static final Country FIN = new Country(new Chars("Finland"), new Chars("FI"), new Chars("FIN"), 246);
    public static final Country FRA = new Country(new Chars("France"), new Chars("FR"), new Chars("FRA"), 250);
    public static final Country GUF = new Country(new Chars("French Guiana"), new Chars("GF"), new Chars("GUF"), 254);
    public static final Country PYF = new Country(new Chars("French Polynesia"), new Chars("PF"), new Chars("PYF"), 258);
    public static final Country ATF = new Country(new Chars("French Southern Territories"), new Chars("TF"), new Chars("ATF"), 260);
    public static final Country GAB = new Country(new Chars("Gabon"), new Chars("GA"), new Chars("GAB"), 266);
    public static final Country GMB = new Country(new Chars("Gambia"), new Chars("GM"), new Chars("GMB"), 270);
    public static final Country GEO = new Country(new Chars("Georgia"), new Chars("GE"), new Chars("GEO"), 268);
    public static final Country DEU = new Country(new Chars("Germany"), new Chars("DE"), new Chars("DEU"), 276);
    public static final Country GHA = new Country(new Chars("Ghana"), new Chars("GH"), new Chars("GHA"), 288);
    public static final Country GIB = new Country(new Chars("Gibraltar"), new Chars("GI"), new Chars("GIB"), 292);
    public static final Country GRC = new Country(new Chars("Greece"), new Chars("GR"), new Chars("GRC"), 300);
    public static final Country GRL = new Country(new Chars("Greenland"), new Chars("GL"), new Chars("GRL"), 304);
    public static final Country GRD = new Country(new Chars("Grenada"), new Chars("GD"), new Chars("GRD"), 308);
    public static final Country GLP = new Country(new Chars("Guadeloupe"), new Chars("GP"), new Chars("GLP"), 312);
    public static final Country GUM = new Country(new Chars("Guam"), new Chars("GU"), new Chars("GUM"), 316);
    public static final Country GTM = new Country(new Chars("Guatemala"), new Chars("GT"), new Chars("GTM"), 320);
    public static final Country GGY = new Country(new Chars("Guernsey"), new Chars("GG"), new Chars("GGY"), 831);
    public static final Country GIN = new Country(new Chars("Guinea"), new Chars("GN"), new Chars("GIN"), 324);
    public static final Country GNB = new Country(new Chars("Guinea-Bissau"), new Chars("GW"), new Chars("GNB"), 624);
    public static final Country GUY = new Country(new Chars("Guyana"), new Chars("GY"), new Chars("GUY"), 328);
    public static final Country HTI = new Country(new Chars("Haiti"), new Chars("HT"), new Chars("HTI"), 332);
    public static final Country HMD = new Country(new Chars("Heard Island and McDonald Islands"), new Chars("HM"), new Chars("HMD"), 334);
    public static final Country VAT = new Country(new Chars("Holy See (Vatican City State)"), new Chars("VA"), new Chars("VAT"), 336);
    public static final Country HND = new Country(new Chars("Honduras"), new Chars("HN"), new Chars("HND"), 340);
    public static final Country HKG = new Country(new Chars("Hong Kong"), new Chars("HK"), new Chars("HKG"), 344);
    public static final Country HUN = new Country(new Chars("Hungary"), new Chars("HU"), new Chars("HUN"), 348);
    public static final Country ISL = new Country(new Chars("Iceland"), new Chars("IS"), new Chars("ISL"), 352);
    public static final Country IND = new Country(new Chars("India"), new Chars("IN"), new Chars("IND"), 356);
    public static final Country IDN = new Country(new Chars("Indonesia"), new Chars("ID"), new Chars("IDN"), 360);
    public static final Country IRN = new Country(new Chars("Iran, Islamic Republic of"), new Chars("IR"), new Chars("IRN"), 364);
    public static final Country IRQ = new Country(new Chars("Iraq"), new Chars("IQ"), new Chars("IRQ"), 368);
    public static final Country IRL = new Country(new Chars("Ireland"), new Chars("IE"), new Chars("IRL"), 372);
    public static final Country IMN = new Country(new Chars("Isle of Man"), new Chars("IM"), new Chars("IMN"), 833);
    public static final Country ISR = new Country(new Chars("Israel"), new Chars("IL"), new Chars("ISR"), 376);
    public static final Country ITA = new Country(new Chars("Italy"), new Chars("IT"), new Chars("ITA"), 380);
    public static final Country JAM = new Country(new Chars("Jamaica"), new Chars("JM"), new Chars("JAM"), 388);
    public static final Country JPN = new Country(new Chars("Japan"), new Chars("JP"), new Chars("JPN"), 392);
    public static final Country JEY = new Country(new Chars("Jersey"), new Chars("JE"), new Chars("JEY"), 832);
    public static final Country JOR = new Country(new Chars("Jordan"), new Chars("JO"), new Chars("JOR"), 400);
    public static final Country KAZ = new Country(new Chars("Kazakhstan"), new Chars("KZ"), new Chars("KAZ"), 398);
    public static final Country KEN = new Country(new Chars("Kenya"), new Chars("KE"), new Chars("KEN"), 404);
    public static final Country KIR = new Country(new Chars("Kiribati"), new Chars("KI"), new Chars("KIR"), 296);
    public static final Country PRK = new Country(new Chars("Korea, Democratic People's Republic of"), new Chars("KP"), new Chars("PRK"), 408);
    public static final Country KOR = new Country(new Chars("Korea, Republic of"), new Chars("KR"), new Chars("KOR"), 410);
    public static final Country KWT = new Country(new Chars("Kuwait"), new Chars("KW"), new Chars("KWT"), 414);
    public static final Country KGZ = new Country(new Chars("Kyrgyzstan"), new Chars("KG"), new Chars("KGZ"), 417);
    public static final Country LAO = new Country(new Chars("Lao People's Democratic Republic"), new Chars("LA"), new Chars("LAO"), 418);
    public static final Country LVA = new Country(new Chars("Latvia"), new Chars("LV"), new Chars("LVA"), 428);
    public static final Country LBN = new Country(new Chars("Lebanon"), new Chars("LB"), new Chars("LBN"), 422);
    public static final Country LSO = new Country(new Chars("Lesotho"), new Chars("LS"), new Chars("LSO"), 426);
    public static final Country LBR = new Country(new Chars("Liberia"), new Chars("LR"), new Chars("LBR"), 430);
    public static final Country LBY = new Country(new Chars("Libya"), new Chars("LY"), new Chars("LBY"), 434);
    public static final Country LIE = new Country(new Chars("Liechtenstein"), new Chars("LI"), new Chars("LIE"), 438);
    public static final Country LTU = new Country(new Chars("Lithuania"), new Chars("LT"), new Chars("LTU"), 440);
    public static final Country LUX = new Country(new Chars("Luxembourg"), new Chars("LU"), new Chars("LUX"), 442);
    public static final Country MAC = new Country(new Chars("Macao"), new Chars("MO"), new Chars("MAC"), 446);
    public static final Country MKD = new Country(new Chars("Macedonia, the former Yugoslav Republic of"), new Chars("MK"), new Chars("MKD"), 807);
    public static final Country MDG = new Country(new Chars("Madagascar"), new Chars("MG"), new Chars("MDG"), 450);
    public static final Country MWI = new Country(new Chars("Malawi"), new Chars("MW"), new Chars("MWI"), 454);
    public static final Country MYS = new Country(new Chars("Malaysia"), new Chars("MY"), new Chars("MYS"), 458);
    public static final Country MDV = new Country(new Chars("Maldives"), new Chars("MV"), new Chars("MDV"), 462);
    public static final Country MLI = new Country(new Chars("Mali"), new Chars("ML"), new Chars("MLI"), 466);
    public static final Country MLT = new Country(new Chars("Malta"), new Chars("MT"), new Chars("MLT"), 470);
    public static final Country MHL = new Country(new Chars("Marshall Islands"), new Chars("MH"), new Chars("MHL"), 584);
    public static final Country MTQ = new Country(new Chars("Martinique"), new Chars("MQ"), new Chars("MTQ"), 474);
    public static final Country MRT = new Country(new Chars("Mauritania"), new Chars("MR"), new Chars("MRT"), 478);
    public static final Country MUS = new Country(new Chars("Mauritius"), new Chars("MU"), new Chars("MUS"), 480);
    public static final Country MYT = new Country(new Chars("Mayotte"), new Chars("YT"), new Chars("MYT"), 175);
    public static final Country MEX = new Country(new Chars("Mexico"), new Chars("MX"), new Chars("MEX"), 484);
    public static final Country FSM = new Country(new Chars("Micronesia, Federated States of"), new Chars("FM"), new Chars("FSM"), 583);
    public static final Country MDA = new Country(new Chars("Moldova, Republic of"), new Chars("MD"), new Chars("MDA"), 498);
    public static final Country MCO = new Country(new Chars("Monaco"), new Chars("MC"), new Chars("MCO"), 492);
    public static final Country MNG = new Country(new Chars("Mongolia"), new Chars("MN"), new Chars("MNG"), 496);
    public static final Country MNE = new Country(new Chars("Montenegro"), new Chars("ME"), new Chars("MNE"), 499);
    public static final Country MSR = new Country(new Chars("Montserrat"), new Chars("MS"), new Chars("MSR"), 500);
    public static final Country MAR = new Country(new Chars("Morocco"), new Chars("MA"), new Chars("MAR"), 504);
    public static final Country MOZ = new Country(new Chars("Mozambique"), new Chars("MZ"), new Chars("MOZ"), 508);
    public static final Country MMR = new Country(new Chars("Myanmar"), new Chars("MM"), new Chars("MMR"), 104);
    public static final Country NAM = new Country(new Chars("Namibia"), new Chars("NA"), new Chars("NAM"), 516);
    public static final Country NRU = new Country(new Chars("Nauru"), new Chars("NR"), new Chars("NRU"), 520);
    public static final Country NPL = new Country(new Chars("Nepal"), new Chars("NP"), new Chars("NPL"), 524);
    public static final Country NLD = new Country(new Chars("Netherlands"), new Chars("NL"), new Chars("NLD"), 528);
    public static final Country NCL = new Country(new Chars("New Caledonia"), new Chars("NC"), new Chars("NCL"), 540);
    public static final Country NZL = new Country(new Chars("New Zealand"), new Chars("NZ"), new Chars("NZL"), 554);
    public static final Country NIC = new Country(new Chars("Nicaragua"), new Chars("NI"), new Chars("NIC"), 558);
    public static final Country NER = new Country(new Chars("Niger"), new Chars("NE"), new Chars("NER"), 562);
    public static final Country NGA = new Country(new Chars("Nigeria"), new Chars("NG"), new Chars("NGA"), 566);
    public static final Country NIU = new Country(new Chars("Niue"), new Chars("NU"), new Chars("NIU"), 570);
    public static final Country NFK = new Country(new Chars("Norfolk Island"), new Chars("NF"), new Chars("NFK"), 574);
    public static final Country MNP = new Country(new Chars("Northern Mariana Islands"), new Chars("MP"), new Chars("MNP"), 580);
    public static final Country NOR = new Country(new Chars("Norway"), new Chars("NO"), new Chars("NOR"), 578);
    public static final Country OMN = new Country(new Chars("Oman"), new Chars("OM"), new Chars("OMN"), 512);
    public static final Country PAK = new Country(new Chars("Pakistan"), new Chars("PK"), new Chars("PAK"), 586);
    public static final Country PLW = new Country(new Chars("Palau"), new Chars("PW"), new Chars("PLW"), 585);
    public static final Country PSE = new Country(new Chars("Palestine, State of"), new Chars("PS"), new Chars("PSE"), 275);
    public static final Country PAN = new Country(new Chars("Panama"), new Chars("PA"), new Chars("PAN"), 591);
    public static final Country PNG = new Country(new Chars("Papua New Guinea"), new Chars("PG"), new Chars("PNG"), 598);
    public static final Country PRY = new Country(new Chars("Paraguay"), new Chars("PY"), new Chars("PRY"), 600);
    public static final Country PER = new Country(new Chars("Peru"), new Chars("PE"), new Chars("PER"), 604);
    public static final Country PHL = new Country(new Chars("Philippines"), new Chars("PH"), new Chars("PHL"), 608);
    public static final Country PCN = new Country(new Chars("Pitcairn"), new Chars("PN"), new Chars("PCN"), 612);
    public static final Country POL = new Country(new Chars("Poland"), new Chars("PL"), new Chars("POL"), 616);
    public static final Country PRT = new Country(new Chars("Portugal"), new Chars("PT"), new Chars("PRT"), 620);
    public static final Country PRI = new Country(new Chars("Puerto Rico"), new Chars("PR"), new Chars("PRI"), 630);
    public static final Country QAT = new Country(new Chars("Qatar"), new Chars("QA"), new Chars("QAT"), 634);
    public static final Country REU = new Country(new Chars("Réunion"), new Chars("RE"), new Chars("REU"), 638);
    public static final Country ROU = new Country(new Chars("Romania"), new Chars("RO"), new Chars("ROU"), 642);
    public static final Country RUS = new Country(new Chars("Russian Federation"), new Chars("RU"), new Chars("RUS"), 643);
    public static final Country RWA = new Country(new Chars("Rwanda"), new Chars("RW"), new Chars("RWA"), 646);
    public static final Country BLM = new Country(new Chars("Saint Barthélemy"), new Chars("BL"), new Chars("BLM"), 652);
    public static final Country SHN = new Country(new Chars("Saint Helena, Ascension and Tristan da Cunha"), new Chars("SH"), new Chars("SHN"), 654);
    public static final Country KNA = new Country(new Chars("Saint Kitts and Nevis"), new Chars("KN"), new Chars("KNA"), 659);
    public static final Country LCA = new Country(new Chars("Saint Lucia"), new Chars("LC"), new Chars("LCA"), 662);
    public static final Country MAF = new Country(new Chars("Saint Martin (French part)"), new Chars("MF"), new Chars("MAF"), 663);
    public static final Country SPM = new Country(new Chars("Saint Pierre and Miquelon"), new Chars("PM"), new Chars("SPM"), 666);
    public static final Country VCT = new Country(new Chars("Saint Vincent and the Grenadines"), new Chars("VC"), new Chars("VCT"), 670);
    public static final Country WSM = new Country(new Chars("Samoa"), new Chars("WS"), new Chars("WSM"), 882);
    public static final Country SMR = new Country(new Chars("San Marino"), new Chars("SM"), new Chars("SMR"), 674);
    public static final Country STP = new Country(new Chars("Sao Tome and Principe"), new Chars("ST"), new Chars("STP"), 678);
    public static final Country SAU = new Country(new Chars("Saudi Arabia"), new Chars("SA"), new Chars("SAU"), 682);
    public static final Country SEN = new Country(new Chars("Senegal"), new Chars("SN"), new Chars("SEN"), 686);
    public static final Country SRB = new Country(new Chars("Serbia"), new Chars("RS"), new Chars("SRB"), 688);
    public static final Country SYC = new Country(new Chars("Seychelles"), new Chars("SC"), new Chars("SYC"), 690);
    public static final Country SLE = new Country(new Chars("Sierra Leone"), new Chars("SL"), new Chars("SLE"), 694);
    public static final Country SGP = new Country(new Chars("Singapore"), new Chars("SG"), new Chars("SGP"), 702);
    public static final Country SXM = new Country(new Chars("Sint Maarten (Dutch part)"), new Chars("SX"), new Chars("SXM"), 534);
    public static final Country SVK = new Country(new Chars("Slovakia"), new Chars("SK"), new Chars("SVK"), 703);
    public static final Country SVN = new Country(new Chars("Slovenia"), new Chars("SI"), new Chars("SVN"), 705);
    public static final Country SLB = new Country(new Chars("Solomon Islands"), new Chars("SB"), new Chars("SLB"), 90);
    public static final Country SOM = new Country(new Chars("Somalia"), new Chars("SO"), new Chars("SOM"), 706);
    public static final Country ZAF = new Country(new Chars("South Africa"), new Chars("ZA"), new Chars("ZAF"), 710);
    public static final Country SGS = new Country(new Chars("South Georgia and the South Sandwich Islands"), new Chars("GS"), new Chars("SGS"), 239);
    public static final Country SSD = new Country(new Chars("South Sudan"), new Chars("SS"), new Chars("SSD"), 728);
    public static final Country ESP = new Country(new Chars("Spain"), new Chars("ES"), new Chars("ESP"), 724);
    public static final Country LKA = new Country(new Chars("Sri Lanka"), new Chars("LK"), new Chars("LKA"), 144);
    public static final Country SDN = new Country(new Chars("Sudan"), new Chars("SD"), new Chars("SDN"), 729);
    public static final Country SUR = new Country(new Chars("Suriname"), new Chars("SR"), new Chars("SUR"), 740);
    public static final Country SJM = new Country(new Chars("Svalbard and Jan Mayen"), new Chars("SJ"), new Chars("SJM"), 744);
    public static final Country SWZ = new Country(new Chars("Swaziland"), new Chars("SZ"), new Chars("SWZ"), 748);
    public static final Country SWE = new Country(new Chars("Sweden"), new Chars("SE"), new Chars("SWE"), 752);
    public static final Country CHE = new Country(new Chars("Switzerland"), new Chars("CH"), new Chars("CHE"), 756);
    public static final Country SYR = new Country(new Chars("Syrian Arab Republic"), new Chars("SY"), new Chars("SYR"), 760);
    public static final Country TWN = new Country(new Chars("Taiwan, Province of China"), new Chars("TW"), new Chars("TWN"), 158);
    public static final Country TJK = new Country(new Chars("Tajikistan"), new Chars("TJ"), new Chars("TJK"), 762);
    public static final Country TZA = new Country(new Chars("Tanzania, United Republic of"), new Chars("TZ"), new Chars("TZA"), 834);
    public static final Country THA = new Country(new Chars("Thailand"), new Chars("TH"), new Chars("THA"), 764);
    public static final Country TLS = new Country(new Chars("Timor-Leste"), new Chars("TL"), new Chars("TLS"), 626);
    public static final Country TGO = new Country(new Chars("Togo"), new Chars("TG"), new Chars("TGO"), 768);
    public static final Country TKL = new Country(new Chars("Tokelau"), new Chars("TK"), new Chars("TKL"), 772);
    public static final Country TON = new Country(new Chars("Tonga"), new Chars("TO"), new Chars("TON"), 776);
    public static final Country TTO = new Country(new Chars("Trinidad and Tobago"), new Chars("TT"), new Chars("TTO"), 780);
    public static final Country TUN = new Country(new Chars("Tunisia"), new Chars("TN"), new Chars("TUN"), 788);
    public static final Country TUR = new Country(new Chars("Turkey"), new Chars("TR"), new Chars("TUR"), 792);
    public static final Country TKM = new Country(new Chars("Turkmenistan"), new Chars("TM"), new Chars("TKM"), 795);
    public static final Country TCA = new Country(new Chars("Turks and Caicos Islands"), new Chars("TC"), new Chars("TCA"), 796);
    public static final Country TUV = new Country(new Chars("Tuvalu"), new Chars("TV"), new Chars("TUV"), 798);
    public static final Country UGA = new Country(new Chars("Uganda"), new Chars("UG"), new Chars("UGA"), 800);
    public static final Country UKR = new Country(new Chars("Ukraine"), new Chars("UA"), new Chars("UKR"), 804);
    public static final Country ARE = new Country(new Chars("United Arab Emirates"), new Chars("AE"), new Chars("ARE"), 784);
    public static final Country GBR = new Country(new Chars("United Kingdom"), new Chars("GB"), new Chars("GBR"), 826);
    public static final Country USA = new Country(new Chars("United States"), new Chars("US"), new Chars("USA"), 840);
    public static final Country UMI = new Country(new Chars("United States Minor Outlying Islands"), new Chars("UM"), new Chars("UMI"), 581);
    public static final Country URY = new Country(new Chars("Uruguay"), new Chars("UY"), new Chars("URY"), 858);
    public static final Country UZB = new Country(new Chars("Uzbekistan"), new Chars("UZ"), new Chars("UZB"), 860);
    public static final Country VUT = new Country(new Chars("Vanuatu"), new Chars("VU"), new Chars("VUT"), 548);
    public static final Country VEN = new Country(new Chars("Venezuela, Bolivarian Republic of"), new Chars("VE"), new Chars("VEN"), 862);
    public static final Country VNM = new Country(new Chars("Viet Nam"), new Chars("VN"), new Chars("VNM"), 704);
    public static final Country VGB = new Country(new Chars("Virgin Islands, British"), new Chars("VG"), new Chars("VGB"), 92);
    public static final Country VIR = new Country(new Chars("Virgin Islands, U.S."), new Chars("VI"), new Chars("VIR"), 850);
    public static final Country WLF = new Country(new Chars("Wallis and Futuna"), new Chars("WF"), new Chars("WLF"), 876);
    public static final Country ESH = new Country(new Chars("Western Sahara"), new Chars("EH"), new Chars("ESH"), 732);
    public static final Country YEM = new Country(new Chars("Yemen"), new Chars("YE"), new Chars("YEM"), 887);
    public static final Country ZMB = new Country(new Chars("Zambia"), new Chars("ZM"), new Chars("ZMB"), 894);
    public static final Country ZWE = new Country(new Chars("Zimbabwe"), new Chars("ZW"), new Chars("ZWE"), 716);

    private static final Sequence INDEX = new ArraySequence();
    static {
        INDEX.add(AFG);
        INDEX.add(ALA);
        INDEX.add(ALB);
        INDEX.add(DZA);
        INDEX.add(ASM);
        INDEX.add(AND);
        INDEX.add(AGO);
        INDEX.add(AIA);
        INDEX.add(ATA);
        INDEX.add(ATG);
        INDEX.add(ARG);
        INDEX.add(ARM);
        INDEX.add(ABW);
        INDEX.add(AUS);
        INDEX.add(AUT);
        INDEX.add(AZE);
        INDEX.add(BHS);
        INDEX.add(BHR);
        INDEX.add(BGD);
        INDEX.add(BRB);
        INDEX.add(BLR);
        INDEX.add(BEL);
        INDEX.add(BLZ);
        INDEX.add(BEN);
        INDEX.add(BMU);
        INDEX.add(BTN);
        INDEX.add(BOL);
        INDEX.add(BES);
        INDEX.add(BIH);
        INDEX.add(BWA);
        INDEX.add(BVT);
        INDEX.add(BRA);
        INDEX.add(IOT);
        INDEX.add(BRN);
        INDEX.add(BGR);
        INDEX.add(BFA);
        INDEX.add(BDI);
        INDEX.add(KHM);
        INDEX.add(CMR);
        INDEX.add(CAN);
        INDEX.add(CPV);
        INDEX.add(CYM);
        INDEX.add(CAF);
        INDEX.add(TCD);
        INDEX.add(CHL);
        INDEX.add(CHN);
        INDEX.add(CXR);
        INDEX.add(CCK);
        INDEX.add(COL);
        INDEX.add(COM);
        INDEX.add(COG);
        INDEX.add(COD);
        INDEX.add(COK);
        INDEX.add(CRI);
        INDEX.add(CIV);
        INDEX.add(HRV);
        INDEX.add(CUB);
        INDEX.add(CUW);
        INDEX.add(CYP);
        INDEX.add(CZE);
        INDEX.add(DNK);
        INDEX.add(DJI);
        INDEX.add(DMA);
        INDEX.add(DOM);
        INDEX.add(ECU);
        INDEX.add(EGY);
        INDEX.add(SLV);
        INDEX.add(GNQ);
        INDEX.add(ERI);
        INDEX.add(EST);
        INDEX.add(ETH);
        INDEX.add(FLK);
        INDEX.add(FRO);
        INDEX.add(FJI);
        INDEX.add(FIN);
        INDEX.add(FRA);
        INDEX.add(GUF);
        INDEX.add(PYF);
        INDEX.add(ATF);
        INDEX.add(GAB);
        INDEX.add(GMB);
        INDEX.add(GEO);
        INDEX.add(DEU);
        INDEX.add(GHA);
        INDEX.add(GIB);
        INDEX.add(GRC);
        INDEX.add(GRL);
        INDEX.add(GRD);
        INDEX.add(GLP);
        INDEX.add(GUM);
        INDEX.add(GTM);
        INDEX.add(GGY);
        INDEX.add(GIN);
        INDEX.add(GNB);
        INDEX.add(GUY);
        INDEX.add(HTI);
        INDEX.add(HMD);
        INDEX.add(VAT);
        INDEX.add(HND);
        INDEX.add(HKG);
        INDEX.add(HUN);
        INDEX.add(ISL);
        INDEX.add(IND);
        INDEX.add(IDN);
        INDEX.add(IRN);
        INDEX.add(IRQ);
        INDEX.add(IRL);
        INDEX.add(IMN);
        INDEX.add(ISR);
        INDEX.add(ITA);
        INDEX.add(JAM);
        INDEX.add(JPN);
        INDEX.add(JEY);
        INDEX.add(JOR);
        INDEX.add(KAZ);
        INDEX.add(KEN);
        INDEX.add(KIR);
        INDEX.add(PRK);
        INDEX.add(KOR);
        INDEX.add(KWT);
        INDEX.add(KGZ);
        INDEX.add(LAO);
        INDEX.add(LVA);
        INDEX.add(LBN);
        INDEX.add(LSO);
        INDEX.add(LBR);
        INDEX.add(LBY);
        INDEX.add(LIE);
        INDEX.add(LTU);
        INDEX.add(LUX);
        INDEX.add(MAC);
        INDEX.add(MKD);
        INDEX.add(MDG);
        INDEX.add(MWI);
        INDEX.add(MYS);
        INDEX.add(MDV);
        INDEX.add(MLI);
        INDEX.add(MLT);
        INDEX.add(MHL);
        INDEX.add(MTQ);
        INDEX.add(MRT);
        INDEX.add(MUS);
        INDEX.add(MYT);
        INDEX.add(MEX);
        INDEX.add(FSM);
        INDEX.add(MDA);
        INDEX.add(MCO);
        INDEX.add(MNG);
        INDEX.add(MNE);
        INDEX.add(MSR);
        INDEX.add(MAR);
        INDEX.add(MOZ);
        INDEX.add(MMR);
        INDEX.add(NAM);
        INDEX.add(NRU);
        INDEX.add(NPL);
        INDEX.add(NLD);
        INDEX.add(NCL);
        INDEX.add(NZL);
        INDEX.add(NIC);
        INDEX.add(NER);
        INDEX.add(NGA);
        INDEX.add(NIU);
        INDEX.add(NFK);
        INDEX.add(MNP);
        INDEX.add(NOR);
        INDEX.add(OMN);
        INDEX.add(PAK);
        INDEX.add(PLW);
        INDEX.add(PSE);
        INDEX.add(PAN);
        INDEX.add(PNG);
        INDEX.add(PRY);
        INDEX.add(PER);
        INDEX.add(PHL);
        INDEX.add(PCN);
        INDEX.add(POL);
        INDEX.add(PRT);
        INDEX.add(PRI);
        INDEX.add(QAT);
        INDEX.add(REU);
        INDEX.add(ROU);
        INDEX.add(RUS);
        INDEX.add(RWA);
        INDEX.add(BLM);
        INDEX.add(SHN);
        INDEX.add(KNA);
        INDEX.add(LCA);
        INDEX.add(MAF);
        INDEX.add(SPM);
        INDEX.add(VCT);
        INDEX.add(WSM);
        INDEX.add(SMR);
        INDEX.add(STP);
        INDEX.add(SAU);
        INDEX.add(SEN);
        INDEX.add(SRB);
        INDEX.add(SYC);
        INDEX.add(SLE);
        INDEX.add(SGP);
        INDEX.add(SXM);
        INDEX.add(SVK);
        INDEX.add(SVN);
        INDEX.add(SLB);
        INDEX.add(SOM);
        INDEX.add(ZAF);
        INDEX.add(SGS);
        INDEX.add(SSD);
        INDEX.add(ESP);
        INDEX.add(LKA);
        INDEX.add(SDN);
        INDEX.add(SUR);
        INDEX.add(SJM);
        INDEX.add(SWZ);
        INDEX.add(SWE);
        INDEX.add(CHE);
        INDEX.add(SYR);
        INDEX.add(TWN);
        INDEX.add(TJK);
        INDEX.add(TZA);
        INDEX.add(THA);
        INDEX.add(TLS);
        INDEX.add(TGO);
        INDEX.add(TKL);
        INDEX.add(TON);
        INDEX.add(TTO);
        INDEX.add(TUN);
        INDEX.add(TUR);
        INDEX.add(TKM);
        INDEX.add(TCA);
        INDEX.add(TUV);
        INDEX.add(UGA);
        INDEX.add(UKR);
        INDEX.add(ARE);
        INDEX.add(GBR);
        INDEX.add(USA);
        INDEX.add(UMI);
        INDEX.add(URY);
        INDEX.add(UZB);
        INDEX.add(VUT);
        INDEX.add(VEN);
        INDEX.add(VNM);
        INDEX.add(VGB);
        INDEX.add(VIR);
        INDEX.add(WLF);
        INDEX.add(ESH);
        INDEX.add(YEM);
        INDEX.add(ZMB);
        INDEX.add(ZWE);
    }


    public static Country getForIsoCode3(Chars code) {
        for (int i=0,n=INDEX.getSize();i<n;i++) {
            final Country cnt = (Country) INDEX.get(i);
            if (cnt.getISOCode3().equals(code)) {
                return cnt;
            }
        }
        return null;
    }

    public static Sequence getAll() {
        return new ArraySequence(INDEX);
    }

}
