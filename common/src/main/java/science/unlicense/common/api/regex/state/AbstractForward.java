
package science.unlicense.common.api.regex.state;

import science.unlicense.common.api.CObject;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractForward extends CObject implements NFAState {

    public final NextRef next = new NextRef(null);

    public NextRef getNextRef() {
        return next;
    }

    public final NFAState getNextState() {
        return getNextRef().state;
    }

}
