
package science.unlicense.common.api.number;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public final class UInt8 extends AbstractNumber {

    public static final NumberType TYPE = new NumberType() {

        @Override
        public Number create(Arithmetic value) {
            if (!(value instanceof Number)) throw new UnsupportedOperationException();
            return new UInt8((byte) ((Number) value).toInteger());
        }

        @Override
        public Class getPrimitiveClass() {
            return byte.class;
        }

        @Override
        public Class getValueClass() {
            return UInt8.class;
        }

        @Override
        public int getSizeInBits() {
            return 8;
        }

        @Override
        public int getSizeInBytes() {
            return 1;
        }

        @Override
        public int getPrimitiveCode() {
            return Primitive.UINT8;
        }
    };

    private final byte value;

    public UInt8(byte value) {
        this.value = value;
    }

    @Override
    public NumberType getType() {
        return TYPE;
    }

    @Override
    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public int toInteger() {
        return value;
    }

    @Override
    public long toLong() {
        return value;
    }

    @Override
    public float toFloat() {
        return value;
    }

    @Override
    public double toDouble() {
        return value;
    }

    @Override
    public Chars toChars() {
        return Int32.encode(toInteger());
    }

    @Override
    public int order(Object other) {
        int of = ((Number) other).toInteger();
        int v = value & 0xFF;
        return v < of ? -1 : (v > of ? +1 : 0) ;
    }

}
