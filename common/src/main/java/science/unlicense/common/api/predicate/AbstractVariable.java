
package science.unlicense.common.api.predicate;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.event.PropertyPredicate;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractVariable extends AbstractEventSource implements Variable {

    private static final Chars VALUE = Chars.constant("Value");
    private static final PropertyPredicate PREDICATE = new PropertyPredicate(VALUE);

    private Variables.VarSync sync;

    @Override
    public EventSource getHolder() {
        return this;
    }

    @Override
    public boolean isReadable() {
        return true;
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public void sync(Variable target) {
        sync(target, false);
    }

    @Override
    public void sync(Variable target, boolean readOnly) {
        if (sync != null) throw new VariableSyncException("Variable is already synchronized");
        sync = Variables.syncInternal(this, target, readOnly);
    }

    @Override
    public void unsync() {
        if (sync != null) {
            sync.release();
            sync = null;
        }
    }

    @Override
    public void addListener(EventListener listener) {
        addEventListener(PREDICATE, listener);
    }

    @Override
    public void removeListener(EventListener listener) {
        removeEventListener(PREDICATE, listener);
    }

    protected void fireValueChanged(Object oldVal, Object newVal) {
        sendPropertyEvent(VALUE, oldVal, newVal);
    }

}
