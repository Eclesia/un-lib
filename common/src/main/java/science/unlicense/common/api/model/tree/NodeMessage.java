
package science.unlicense.common.api.model.tree;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.event.DefaultEventMessage;
import science.unlicense.common.api.event.MessagePredicate;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Node event.
 *
 * @author Johann Sorel
 */
public class NodeMessage extends DefaultEventMessage{

    public static final Predicate PREDICATE = new MessagePredicate(NodeMessage.class);

    private static final Node[] EMPTY = new Node[0];

    public static final int TYPE_ADD = 1;
    public static final int TYPE_REMOVE = 2;
    public static final int TYPE_REPLACE = 3;
    public static final int TYPE_REPLACEALL = 4;

    private final int type;
    private final int startIndex;
    private final int endIndex;
    private final Node[] oldElements;
    private final Node[] newElements;

    public NodeMessage(int type,
            int startIndex, int endIndex,
            Node[] oldElements, Node[] newElements) {
        super(false);
        this.type = type;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.oldElements = (oldElements==null) ? EMPTY : oldElements;
        this.newElements = (newElements==null) ? EMPTY : newElements;
    }

    /**
     * Node event type.
     *
     * @return int, one of NodeEvent.TYPE_X
     */
    public int getType() {
        return type;
    }

    /**
     * Get the start index of the event.
     *
     * @return int
     */
    public int getStartIndex() {
        return startIndex;
    }

    /**
     * Get the end index of the event.
     *
     * @return int
     */
    public int getEndIndex() {
        return endIndex;
    }

    /**
     * Get the old nodes, removed or replaced.
     *
     * @return Node[] never null
     */
    public Node[] getOldElements() {
        return oldElements;
    }

    /**
     * Get the new nodes, inserted or replacing.
     *
     * @return Object[] never null
     */
    public Node[] getNewElements() {
        return newElements;
    }

    @Override
    public int getHash() {
        int hash = 7;
        hash = 53 * hash + this.type;
        hash = 53 * hash + this.startIndex;
        hash = 53 * hash + this.endIndex;
        hash = 53 * hash + Arrays.computeHash(this.oldElements);
        hash = 53 * hash + Arrays.computeHash(this.newElements);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NodeMessage other = (NodeMessage) obj;
        if (this.type != other.type) {
            return false;
        }
        if (this.startIndex != other.startIndex) {
            return false;
        }
        if (this.endIndex != other.endIndex) {
            return false;
        }
        if (!Arrays.equals(this.oldElements, other.oldElements)) {
            return false;
        }
        if (!Arrays.equals(this.newElements, other.newElements)) {
            return false;
        }
        return true;
    }



}
