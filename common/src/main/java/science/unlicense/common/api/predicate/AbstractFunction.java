
package science.unlicense.common.api.predicate;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;

/**
 * Abstract Function.
 *
 * @author Johann Sorel
 */
public abstract class AbstractFunction extends CObject implements Function{

    protected final Chars name;
    protected final Expression[] parameters;

    public AbstractFunction(Chars name, Expression[] parameters) {
        this.name = name;
        this.parameters = parameters;
    }

    @Override
    public Chars getName() {
        return name;
    }

    /**
     * Get function parameters.
     * Returned array is a copy.
     *
     * @return expression array
     */
    @Override
    public Expression[] getParameters() {
        return Arrays.copy(parameters,new Expression[parameters.length]);
    }

    protected Object[] evaluateParameters(Object candidate) {
        final Object[] values = new Object[parameters.length];
        for (int i=0;i<values.length;i++) {
            values[i] = parameters[i].evaluate(candidate);
        }
        return values;
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(name);
        cb.append('(');
        for (int i=0;i<parameters.length;i++) {
            if (i>0) cb.append(',');
            cb.append(CObjects.toChars(parameters[i]));
        }
        cb.append(')');
        return cb.toChars();
    }

    @Override
    public int getHash() {
        int hash = 5;
        hash = 13 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 13 * hash + Arrays.computeHash(this.parameters);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractFunction other = (AbstractFunction) obj;
        if (!this.name.equals(other.name)) {
            return false;
        }
        return Arrays.equals(this.parameters, other.parameters);
    }


}
