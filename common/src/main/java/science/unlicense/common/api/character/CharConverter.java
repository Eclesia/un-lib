
package science.unlicense.common.api.character;

/**
 * Converter from a character encoding to another.
 *
 * Char converters are not thread safe.
 *
 * @author Johann Sorel
 */
public final class CharConverter {

    private final CharEncoding source;
    private final CharEncoding target;
    private final int[] cp = new int[2];

    public CharConverter(CharEncoding source, CharEncoding target) {
        this.source = source;
        this.target = target;
    }

    /**
     *
     * @param input array to read from
     * @param inOffset position to read from
     * @param output array to write to
     * @param outOffset position to write to
     * @param size result [0] number of byte read, [1] number of byte written
     */
    public void convert(final byte[] input, int inOffset, final byte[] output, int outOffset, int[] size) {
        source.toUnicode(input, cp, inOffset);
        size[0] = cp[1];
        target.toBytes(cp, output, outOffset);
        size[1] = cp[1];
    }


}
