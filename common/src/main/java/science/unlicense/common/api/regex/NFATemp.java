

package science.unlicense.common.api.regex;

import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.regex.state.NextRef;

/**
 * Temporary object used when rebuilding NFA state graph.
 *
 * @author Johann Sorel
 */
public final class NFATemp {
    public NFAState state;
    public final Sequence nexts;

    public NFATemp(NFAState state, NextRef next) {
        this.state = state;
        this.nexts = new ArraySequence();
        this.nexts.add(next);
    }

    public NFATemp(NFAState start, Sequence nexts) {
        this.state = start;
        this.nexts = nexts;
    }

    public void setNext(NFAState state) {
        for (int i = 0, n = nexts.getSize(); i < n; i++) {
            ((NextRef) nexts.get(i)).state = state;
        }
    }

}
