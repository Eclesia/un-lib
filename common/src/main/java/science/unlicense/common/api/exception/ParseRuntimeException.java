
package science.unlicense.common.api.exception;

/**
 *
 * @author Johann Sorel
 */
public class ParseRuntimeException extends RuntimeException {

    public ParseRuntimeException() {
    }

    public ParseRuntimeException(String s) {
        super(s);
    }

    public ParseRuntimeException(Throwable cause) {
        super(cause);
    }

    public ParseRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

}
