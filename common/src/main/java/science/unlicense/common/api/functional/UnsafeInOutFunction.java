
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface UnsafeInOutFunction {

    Object perform(Object input) throws Exception;

}
