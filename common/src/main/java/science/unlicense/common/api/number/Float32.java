
package science.unlicense.common.api.number;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharIterator;
import science.unlicense.common.api.exception.ParseRuntimeException;
import science.unlicense.common.api.exception.UnimplementedException;

/**
 * 32bit decimal, called float.
 * Encoded in IEEE754
 * http://en.wikipedia.org/wiki/Single-precision_floating-point_format
 *
 * @author Johann Sorel
 */
public final class Float32 extends AbstractNumber {

    public static final NumberType TYPE = new NumberType() {

        @Override
        public Number create(Arithmetic value) {
            if (!(value instanceof Number)) throw new UnsupportedOperationException();
            return new Float32(((Number) value).toFloat());
        }

        @Override
        public Class getPrimitiveClass() {
            return float.class;
        }

        @Override
        public Class getValueClass() {
            return Float32.class;
        }

        @Override
        public int getSizeInBits() {
            return 32;
        }

        @Override
        public int getSizeInBytes() {
            return 4;
        }

        @Override
        public int getPrimitiveCode() {
            return Primitive.FLOAT32;
        }
    };

    public static final int MASK_FRACTION = 0x007FFFFFF;
    public static final int MASK_EXPONENT = 0x7F800000;
    public static final int MASK_SIGN = 1 >> 31;

    public static final int FRACTION_SIZE = 23;
    public static final int EXPONENT_SIZE = 8;
    public static final int EXP_BIAS = 127;


    /**
     * Test float equality with a tolerance.
     * Returns true if both values are NaN.
     * Returns false if either value is NaN.
     *
     * @param v1
     * @param v2
     * @param tolerance
     * @return
     */
    public static boolean equals(float v1, float v2, float tolerance) {
        if (Float.isNaN(v1)) {
            return Float.isNaN(v2);
        } else {
            return !Float.isNaN(v2)
                && (Math.abs(v1 - v2) <= tolerance);
        }
    }

    public static IEEE754 decompose(float value) {
        return decompose(value, null);
    }

    public static IEEE754 decompose(float value, IEEE754 buffer) {
        if (buffer == null) buffer = new IEEE754();
        final int bits = Float.floatToRawIntBits(value);
        buffer.fraction = bits & MASK_FRACTION;
        buffer.exponent = ((bits & MASK_EXPONENT) >> FRACTION_SIZE);
        buffer.sign = bits >> 31;

        return buffer;
    }

    private final float value;

    public Float32(float value) {
        this.value = value;
    }

    @Override
    public NumberType getType() {
        return TYPE;
    }

    @Override
    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public int toInteger() {
        return (int) value;
    }

    @Override
    public long toLong() {
        return (long) value;
    }

    @Override
    public float toFloat() {
        return value;
    }

    @Override
    public double toDouble() {
        return value;
    }

    @Override
    public int order(Object other) {
        float of = ((Number) other).toFloat();
        return value < of ? -1 : (value > of ? +1 : 0) ;
    }


    public static float decode(CharArray candidate) throws ParseRuntimeException {
        return decode(candidate, 0, candidate.getCharLength());
    }

    /**
     *
     * @param candidate text de decode
     * @param fromIndex start offset
     * @param toIndex last character excluded,
     *          or use -1 to stop on sequence end or invalid character.
     * @return decoded value
     */
    public static float decode(CharArray candidate, int fromIndex, int toIndex) throws ParseRuntimeException {
        try{
            return decode(candidate.createIterator(fromIndex,toIndex),true);
        }catch(RuntimeException ex) {
            throw new ParseRuntimeException("Not an decimal : "+candidate);
        }
    }


    public static float decode(CharIterator ite, boolean strict) throws ParseRuntimeException {
        if (!ite.hasNext()) {
            throw new ParseRuntimeException("Not an decimal");
        }

        boolean isNegative = false;
        boolean hasDecimals = false;
        boolean hasExpo = false;

        float number = 0;
        int dec = 1;
        int expo = 0;

        int c = ite.peekToUnicode();
        if (c == 43) { // +
            ite.skip();
        } else if (c == 45) { // -
            isNegative = true;
            ite.skip();
        }

        boolean requestDigit = true;
        while (ite.hasNext()) {
            c = ite.peekToUnicode();
            if (requestDigit) {
                //firt char must be a digit
                if (c > 47 && c < 58) { // digit
                    ite.skip();
                    c -= 48;
                    number = number*10 + c;
                    if (hasDecimals) {
                        dec *= 10;
                    }
                } else {
                    throw new ParseRuntimeException("Not an decimal");
                }
                requestDigit = false;
                continue;
            }

            if (c == 46) { // .
                ite.skip();
                if (hasDecimals) {
                    throw new ParseRuntimeException("Not an decimal");
                } else {
                    hasDecimals = true;
                }
            } else if (c ==69 || c == 101) { // exponentiel E,e
                if (hasExpo) {
                    throw new ParseRuntimeException("Not an decimal");
                } else {
                    hasExpo = true;
                    ite.skip();
                    expo = Int32.decode(ite,false);
                    break;
                }
            } else if (c > 47 && c < 58) { // digit
                ite.skip();
                c -= 48;
                number = number*10 + c;
                if (hasDecimals) {
                    dec *= 10;
                }
            } else if (strict) {
                throw new ParseRuntimeException("Not an decimal");
            } else {
                //not valid, stop
                break;
            }
        }

        if (isNegative) {
            number *= -1;
        }
        if (hasDecimals) {
            number /= dec;
        }
        if (hasExpo) {
            number *= Math.pow(10, expo);
        }

        return number;
    }

}
