
package science.unlicense.common.api.model.tree;

import science.unlicense.common.api.collection.AbstractSequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Read only sequence of the nodes in a node tree.
 *
 * @author Johann Sorel
 */
public class NodeSequence extends AbstractSequence implements EventListener {

    private final Node root;
    private final boolean includeThisNode;
    private final Predicate filter;
    private final EventListener changeTracker = new EventListener() {
        @Override
        public void receiveEvent(Event event) {
            receiveNodeEvent(event);
        }
    };

    private Sequence cache = null;

    public NodeSequence(Node root, boolean includeThisNode) {
        this(root,includeThisNode,Predicate.TRUE);
    }

    public NodeSequence(Node root, boolean includeThisNode, Predicate filter) {
        this.root = root;
        this.includeThisNode = includeThisNode;
        this.filter = filter;
        nodeAdded(root);
    }

    private synchronized void clearCache() {
        cache = null;
    }

    private synchronized Sequence getCache() {
        if (cache==null) {
            cache = Nodes.flatten(root,includeThisNode,filter);
        }
        return cache;
    }

    @Override
    public Object get(int index) {
        return getCache().get(index);
    }

    @Override
    public int getSize() {
        return getCache().getSize();
    }

    @Override
    public Iterator createIterator() {
        return getCache().createIterator();
    }

    @Override
    public Iterator createReverseIterator() {
        return getCache().createReverseIterator();
    }

    @Override
    public boolean add(int index, Object value) {
        throw new UnimplementedException("Node sequence is read-only.");
    }

    @Override
    public boolean remove(int index) {
        throw new UnimplementedException("Node sequence is read-only.");
    }

    @Override
    public void receiveEvent(Event event) {
        clearCache();
    }

    protected void receiveNodeEvent(Event event) {
        final NodeMessage msg = (NodeMessage) event.getMessage();
        for (Node n : msg.getNewElements()) {
            nodeAdded(n);
        }
        for (Node n : msg.getOldElements()) {
            nodeRemoved(n);
        }
        clearCache();
    }

    protected void nodeAdded(Node n) {
        n.addEventListener(NodeMessage.PREDICATE, changeTracker);
    }

    protected void nodeRemoved(Node n) {
        n.removeEventListener(NodeMessage.PREDICATE, changeTracker);
    }

}
