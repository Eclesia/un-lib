
package science.unlicense.common.api.functional;


/**
 *
 * @author Johann Sorel
 */
public final class Functions {

    private Functions(){}

    /**
     * In handler doing nothing.
     */
    public static final InFunction NOOP_IN = new InFunction() {
        public void perform(Object input) {}
    };

    /**
     * Void handler doing nothing.
     */
    public static final VoidFunction NOOP_VOID = new VoidFunction() {
        public void perform() {}
    };

    /**
     * Out handler returning a null value.
     */
    public static final OutFunction NULL_OUT = new OutFunction() {
        public Object perform() {
            return null;
        }
    };

    /**
     * InOut handler returning a null value.
     */
    public static final InOutFunction NULL_INOUT = new InOutFunction() {
        public Object perform(Object input) {
            return null;
        }
    };

    /**
     * Callback used to convert Exceptions to RuntimeExceptions.
     */
    private static final InOutFunction EXP_TO_RUNTIME = new InOutFunction() {
        @Override
        public Object perform(Object input) {
            final Exception ex = (Exception) input;
            throw new RuntimeException(ex.getMessage(), ex);
        }
    };


    /**
     * Create a BinaryOut handler which return a constant value.
     *
     * @param value constant value
     * @return SafeBinaryOut
     */
    public static BinaryOutFunction constantBinaryOut(final Object value) {
        return new BinaryOutFunction() {
            public Object perform(Object in1, Object in2) {
                return value;
            }
        };
    }

    /**
     * Create a InOut handler which return a constant value.
     *
     * @param value constant value
     * @return SafeInOut
     */
    public static InOutFunction constantInOut(final Object value) {
        return new InOutFunction() {
            public Object perform(Object input) {
                return value;
            }
        };
    }

    /**
     * Create a Out handler which return a constant value.
     *
     * @param value constant value
     * @return SafeOut
     */
    public static OutFunction constantOut(final Object value) {
        return new OutFunction() {
            public Object perform() {
                return value;
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static BinaryOutFunction safe(final UnsafeBinaryOutFunction handler) {
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static BinaryFunction safe(final UnsafeBinaryFunction handler) {
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static InOutFunction safe(final UnsafeInOutFunction handler) {
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static InFunction safe(final UnsafeInFunction handler) {
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static OutFunction safe(final OutFunction handler) {
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static VoidFunction safe(final UnsafeVoidFunction handler) {
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static BinaryOutFunction safe(final UnsafeBinaryOutFunction handler, final InOutFunction exceptionCallback) {
        return new BinaryOutFunction() {
            public Object perform(Object inValue1, Object inValue2) {
                try {
                    return handler.perform(inValue1,inValue2);
                } catch (Exception ex) {
                    return exceptionCallback.perform(ex);
                }
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static BinaryFunction safe(final UnsafeBinaryFunction handler, final InOutFunction exceptionCallback) {
        return new BinaryFunction() {
            public void perform(Object inValue1, Object inValue2) {
                try {
                    handler.perform(inValue1,inValue2);
                } catch (Exception ex) {
                    exceptionCallback.perform(ex);
                }
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static InOutFunction safe(final UnsafeInOutFunction handler, final InOutFunction exceptionCallback) {
        return new InOutFunction() {
            public Object perform(Object input) {
                try {
                    return handler.perform(input);
                } catch (Exception ex) {
                    return exceptionCallback.perform(ex);
                }
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static InFunction safe(final UnsafeInFunction handler, final InOutFunction exceptionCallback) {
        return new InFunction() {
            public void perform(Object inValue) {
                try {
                    handler.perform(inValue);
                } catch (Exception ex) {
                    exceptionCallback.perform(ex);
                }
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static OutFunction safe(final OutFunction handler, final InOutFunction exceptionCallback) {
        return new OutFunction() {
            public Object perform() {
                try {
                    return handler.perform();
                } catch (Exception ex) {
                    return exceptionCallback.perform(ex);
                }
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static VoidFunction safe(final UnsafeVoidFunction handler, final InOutFunction exceptionCallback) {
        return new VoidFunction() {
            public void perform() {
                try {
                    handler.perform();
                } catch (Exception ex) {
                    exceptionCallback.perform(ex);
                }
            }
        };
    }

    public static UnsafeInOutFunction asInOut(final UnsafeVoidFunction handler, final Object outValue) {
        return new UnsafeInOutFunction() {
            public Object perform(Object input) throws Exception {
                handler.perform();
                return outValue;
            }
        };
    }

    public static UnsafeInOutFunction asInOut(final UnsafeOutFunction handler) {
        return new UnsafeInOutFunction() {
            public Object perform(Object input) throws Exception {
                return handler.perform();
            }
        };
    }

    public static UnsafeInOutFunction asInOut(final UnsafeInFunction handler, final Object outValue) {
        return new UnsafeInOutFunction() {
            public Object perform(Object input) throws Exception {
                handler.perform(input);
                return outValue;
            }
        };
    }

    public static UnsafeInFunction asIn(final UnsafeVoidFunction handler) {
        return new UnsafeInFunction() {
            public void perform(Object input) throws Exception {
                handler.perform();
            }
        };
    }

    public static UnsafeInFunction asIn(final UnsafeOutFunction handler) {
        return new UnsafeInFunction() {
            public void perform(Object input) throws Exception {
                handler.perform();
            }
        };
    }

    public static UnsafeInFunction asIn(final UnsafeInOutFunction handler) {
        return new UnsafeInFunction() {
            public void perform(Object input) throws Exception {
                handler.perform(input);
            }
        };
    }

    public static UnsafeOutFunction asOut(final UnsafeVoidFunction handler, final Object outValue) {
        return new UnsafeOutFunction() {
            public Object perform() throws Exception {
                handler.perform();
                return outValue;
            }
        };
    }

    public static UnsafeOutFunction asOut(final UnsafeInFunction handler, final Object inValue, final Object outValue) {
        return new UnsafeOutFunction() {
            public Object perform() throws Exception {
                handler.perform(inValue);
                return outValue;
            }
        };
    }

    public static UnsafeOutFunction asOut(final UnsafeInOutFunction handler, final Object inValue) {
        return new UnsafeOutFunction() {
            public Object perform() throws Exception {
                return handler.perform(inValue);
            }
        };
    }

    public static UnsafeVoidFunction asVoid(final UnsafeOutFunction handler, final Object outValue) {
        return new UnsafeVoidFunction() {
            public void perform() throws Exception {
                handler.perform();
            }
        };
    }

    public static UnsafeVoidFunction asVoid(final UnsafeInFunction handler, final Object inValue) {
        return new UnsafeVoidFunction() {
            public void perform() throws Exception {
                handler.perform(inValue);
            }
        };
    }

    public static UnsafeVoidFunction asVoid(final UnsafeInOutFunction handler, final Object inValue) {
        return new UnsafeVoidFunction() {
            public void perform() throws Exception {
                handler.perform(inValue);
            }
        };
    }

    public static InOutFunction asInOut(final VoidFunction handler, final Object outValue) {
        return new InOutFunction() {
            public Object perform(Object input) {
                handler.perform();
                return outValue;
            }
        };
    }

    public static InOutFunction asInOut(final OutFunction handler) {
        return new InOutFunction() {
            public Object perform(Object input) {
                return handler.perform();
            }
        };
    }

    public static InOutFunction asInOut(final InFunction handler, final Object outValue) {
        return new InOutFunction() {
            public Object perform(Object input) {
                handler.perform(input);
                return outValue;
            }
        };
    }

    public static InFunction asIn(final VoidFunction handler) {
        return new InFunction() {
            public void perform(Object input) {
                handler.perform();
            }
        };
    }

    public static InFunction asIn(final OutFunction handler) {
        return new InFunction() {
            public void perform(Object input) {
                handler.perform();
            }
        };
    }

    public static InFunction asIn(final InOutFunction handler) {
        return new InFunction() {
            public void perform(Object input) {
                handler.perform(input);
            }
        };
    }

    public static OutFunction asOut(final VoidFunction handler, final Object outValue) {
        return new OutFunction() {
            public Object perform() {
                handler.perform();
                return outValue;
            }
        };
    }

    public static OutFunction asOut(final InFunction handler, final Object inValue, final Object outValue) {
        return new OutFunction() {
            public Object perform() {
                handler.perform(inValue);
                return outValue;
            }
        };
    }

    public static OutFunction asOut(final InOutFunction handler, final Object inValue) {
        return new OutFunction() {
            public Object perform() {
                return handler.perform(inValue);
            }
        };
    }

    public static VoidFunction asVoid(final OutFunction handler, final Object outValue) {
        return new VoidFunction() {
            public void perform() {
                handler.perform();
            }
        };
    }

    public static VoidFunction asVoid(final InFunction handler, final Object inValue) {
        return new VoidFunction() {
            public void perform() {
                handler.perform(inValue);
            }
        };
    }

    public static VoidFunction asVoid(final InOutFunction handler, final Object inValue) {
        return new VoidFunction() {
            public void perform()  {
                handler.perform(inValue);
            }
        };
    }

}
