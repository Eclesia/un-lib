
package science.unlicense.common.api.predicate;

/**
 * Predicate utilities.
 *
 * @author Johann Sorel
 */
public final class Predicates {

    private Predicates() {}

    public static Predicate instanceOf(final Class expected) {
        return new Predicate() {
            public Boolean evaluate(Object candidate) {
                return expected.isInstance(candidate);
            }
        };
    }

    /**
     * Creates a predicate which evaluation result is a XOR operator between the
     * two specified predicates.
     *
     * @param p1 the first predicate.
     * @param p2 the second predicate.
     * @return a new predicate.
     */
    public static Predicate xor(final Predicate p1, final Predicate p2) {
        return new AbstractPredicate() {
            public Boolean evaluate(Object candidate) {
                return p1.evaluate(candidate) ^ p2.evaluate(candidate);
            }
        };
    }

}
