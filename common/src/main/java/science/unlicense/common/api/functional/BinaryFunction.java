
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface BinaryFunction extends UnsafeBinaryFunction {

    void perform(Object in1, Object in2);

}
