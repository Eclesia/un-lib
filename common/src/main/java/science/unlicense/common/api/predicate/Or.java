
package science.unlicense.common.api.predicate;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;

/**
 * Logic OR of several predicates.
 *
 * @author Johann Sorel
 */
public class Or extends CObject implements Predicate, Function{

    public static final Chars NAME = Chars.constant("OR");
    private final Predicate[] predicates;

    /**
     * Predicates to concatenate.
     *
     * @param predicates input predicates
     */
    public Or(Predicate[] predicates) {
        CObjects.ensureNotNull(predicates);
        this.predicates = predicates;
    }

    public Chars getName() {
        return NAME;
    }

    /**
     * Or predicates.
     * Returned array is a copy.
     *
     * @return Predicate[] not null
     */
    public Predicate[] getPrecidates() {
        return Arrays.copy(predicates, new Predicate[predicates.length]);
    }

    public Expression[] getParameters() {
        return getPrecidates();
    }

    public Boolean evaluate(Object candidate) {
        for (int i=0;i<predicates.length;i++) {
            if (predicates[i].evaluate(candidate)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Or other = (Or) obj;
        return Arrays.equals(this.predicates, other.predicates);
    }

    public int getHI() {
        return 79 * 5 + Arrays.computeHash(this.predicates);
    }
}
