
package science.unlicense.common.api.character.encoding;

import science.unlicense.common.api.character.AbstractCharEncoding;
import science.unlicense.common.api.character.CharEncodingException;

/**
 *
 * @author Johann Sorel
 */
public class Utf8 extends AbstractCharEncoding{

    private static final byte nomatch = (byte) 95; //underscore used to match unsupported characters

    public Utf8() {
        super(new byte[]{'U','T','F','-','8'},false,4);
    }

    public int charlength(byte[] array, int offset) {
        final int b1 = array[offset] & 0xff;
        if (b1 < 0x80) {
            return 1;
        } else if ((b1 & 0xe0) == 0xc0) {
            return 2;
        } else if ((b1 & 0xf0) == 0xe0) {
            return 3;
        } else if ((b1 & 0xf8) == 0xf0) {
            return 4;
        } else {
            //unvalid char
            return 1;
        }
    }

    public void toUnicode(byte[] array, int[] codePoint, int offset) {
        final int b1 = array[offset] & 0xff;
        if (b1 < 0x80) {
            codePoint[0] = b1;
            codePoint[1] = 1;
            return;
        }
        final int b2 = array[offset+1] & 0xff;
        if ((b1 & 0xe0) == 0xc0) {
            codePoint[0] = ((b1 & 0x1f) << 6 )
                         | ((b2 & 0x3f) << 0 ) ;
            codePoint[1] = 2;
            return;
        }
        final int b3 = array[offset+2] & 0xff;
        if ((b1 & 0xf0) == 0xe0) {
            codePoint[0] = ((b1 & 0x0f) << 12)
                         | ((b2 & 0x3f) << 6 )
                         | ((b3 & 0x3f) << 0 ) ;
            codePoint[1] = 3;
            return;
        }
        final int b4 = array[offset+3] & 0xff;
        if ((b1 & 0xf8) == 0xf0) {
            codePoint[0] = ((b1 & 0x07) << 18)
                         | ((b2 & 0x3f) << 12)
                         | ((b3 & 0x3f) << 6 )
                         | ((b4 & 0x3f) << 0 ) ;
            codePoint[1] = 4;
            return;
        }

        throw new CharEncodingException("Unvalid UTF-8 character");

    }

    public byte[] toBytes(final int u) {
        final byte[] array;
        if (u < 0x80) {
            array = new byte[1];
            array[0] = (byte) ( ((u >> 0)       )        );
        } else if (u < 0x800) {
            array = new byte[2];
            array[0] = (byte) ( ((u >> 6) & 0x1f) | 0xc0 );
            array[1] = (byte) ( ((u >> 0) & 0x3f) | 0x80 );
        } else if (u < 0x10000) {
            array = new byte[3];
            array[0] = (byte) ( ((u >> 12) & 0x0f) | 0xe0 );
            array[1] = (byte) ( ((u >> 6 ) & 0x3f) | 0x80 );
            array[2] = (byte) ( ((u >> 0 ) & 0x3f) | 0x80 );
        } else if (u < 0x110000) {
            array = new byte[4];
            array[0] = (byte) ( ((u >> 18) & 0x07) | 0xf0 );
            array[1] = (byte) ( ((u >> 12) & 0x3f) | 0x80 );
            array[2] = (byte) ( ((u >> 6 ) & 0x3f) | 0x80 );
            array[3] = (byte) ( ((u >> 0 ) & 0x3f) | 0x80 );
        } else {
            //invalide char, use replacement
            array = toBytes(nomatch);
        }
        return array;
    }

    public void toBytes(int[] codePoint, byte[] array, int offset) {
        final int u = codePoint[0];
        if (u < 0x80) {
            codePoint[1] = 1;
            array[offset+0] = (byte) ( ((u >> 0 )       )        );
        } else if (u < 0x800) {
            codePoint[1] = 2;
            array[offset+0] = (byte) ( ((u >> 6 ) & 0x1f) | 0xc0 );
            array[offset+1] = (byte) ( ((u >> 0 ) & 0x3f) | 0x80 );
        } else if (u < 0x10000) {
            codePoint[1] = 3;
            array[offset+0] = (byte) ( ((u >> 12) & 0x0f) | 0xe0 );
            array[offset+1] = (byte) ( ((u >> 6 ) & 0x3f) | 0x80 );
            array[offset+2] = (byte) ( ((u >> 0 ) & 0x3f) | 0x80 );
        } else if (u < 0x110000) {
            codePoint[1] = 4;
            array[offset+0] = (byte) ( ((u >> 18) & 0x07) | 0xf0 );
            array[offset+1] = (byte) ( ((u >> 12) & 0x3f) | 0x80 );
            array[offset+2] = (byte) ( ((u >> 6 ) & 0x3f) | 0x80 );
            array[offset+3] = (byte) ( ((u >> 0 ) & 0x3f) | 0x80 );
        } else {
            //invalid, use fallback
            codePoint[0] = nomatch;
            toBytes(codePoint, array, offset);
        }
    }

    public boolean isComplete(byte[] array, int offset, int length) {
        final int b1 = array[offset] & 0xff;
        if (b1 < 0x80) {
            return length == 1;
        } else if ((b1 & 0xe0) == 0xc0) {
            return length == 2;
        } else if ((b1 & 0xf0) == 0xe0) {
            return length == 3;
        } else if ((b1 & 0xf8) == 0xf0) {
            return length == 4;
        } else {
            //unvalid char
            return length == 1;
        }
    }

}
