package science.unlicense.common.api.model.tree;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.ValueType;

/**
 *
 * @author Johann Sorel
 */
public interface NodeType extends ValueType{

    boolean isNullable();

    boolean isComplexe();

    NodeCardinality[] getChildrenTypes();

    NodeCardinality getChild(Chars name);

    Chars toChars();

    /**
     * Get a char sequence representation.
     * @param depth : maximum sub nodes depth to print.
     * @return CharSequence
     */
    Chars toCharsTree(int depth);
}
