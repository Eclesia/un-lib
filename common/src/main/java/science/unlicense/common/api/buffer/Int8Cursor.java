
package science.unlicense.common.api.buffer;

/**
 *
 * @author Johann Sorel
 */
public interface Int8Cursor extends Cursor {

    long getOffset();

    Int8Cursor offset(long position);

    byte read();

    void read(byte[] array);

    void read(byte[] array, int arrayOffset, int length);

    Int8Cursor write(byte value);

    Int8Cursor write(byte[] array);

    Int8Cursor write(byte[] array, int arrayOffset, int length);

}
