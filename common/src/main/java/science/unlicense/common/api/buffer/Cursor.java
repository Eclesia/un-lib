
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.number.Endianness;

/**
 * Cursors are not thread safe.
 *
 * @author Johann Sorel
 */
public interface Cursor {

    /**
     * Get buffer this cursor moves on.
     * @return Buffer
     */
    Buffer getBuffer();

    /**
     * Get cursor number encoding.
     * @return Endianness
     */
    Endianness getEndianness();

    /**
     * @return cursor offset in bytes.
     */
    long getByteOffset();

}
