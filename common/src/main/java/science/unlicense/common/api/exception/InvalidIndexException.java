
package science.unlicense.common.api.exception;

/**
 * Special case of invalid argument exception for incorrect indexes.
 *
 * @author Johann Sorel
 */
public class InvalidIndexException extends InvalidArgumentException {

    public InvalidIndexException() {
    }

    public InvalidIndexException(String s) {
        super(s);
    }

    public InvalidIndexException(Throwable cause) {
        super(cause);
    }

    public InvalidIndexException(String message, Throwable cause) {
        super(message, cause);
    }

}
