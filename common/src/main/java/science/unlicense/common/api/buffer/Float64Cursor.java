
package science.unlicense.common.api.buffer;

/**
 *
 * @author Johann Sorel
 */
public interface Float64Cursor extends Cursor {

    long getOffset();

    Float64Cursor offset(long position);

    double read();

    void read(double[] array);

    void read(double[] array, int arrayOffset, int length);

    Float64Cursor write(double value);

    Float64Cursor write(double[] array);

    Float64Cursor write(double[] array, int arrayOffset, int length);

}
