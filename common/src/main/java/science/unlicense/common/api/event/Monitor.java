
package science.unlicense.common.api.event;

/**
 * A monitor is an object informed of status events.
 * Monitors are often given as parameter for long process.
 * This pattern allows to display progress informations and cancel request.
 *
 * This interface expect events of type MonitorEvent.
 *
 * @author Johann Sorel
 */
public interface Monitor extends EventListener {

    boolean isCancelRequested();

    boolean isPauseRequested();

    boolean isResumeRequested();

    /**
     * Request to stop the ongoing work.
     * Canceling might not be supported by all monitored objects.
     */
    void cancel();

    /**
     * Request to pause the ongoing work.
     * Pausing/Resuming might not be supported by all monitored objects.
     */
    void pause();

    /**
     * Request to resume the ongoing work.
     * Pausing/Resuming might not be supported by all monitored objects.
     */
    void resume();

}
