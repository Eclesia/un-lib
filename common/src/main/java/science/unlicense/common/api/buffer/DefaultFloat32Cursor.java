
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class DefaultFloat32Cursor extends AbstractCursor implements Float32Cursor, Buffer.Float32{

    final Buffer buffer;

    //current offset from start.
    private long byteOffset = 0;

    public DefaultFloat32Cursor(Buffer buffer, Endianness encoding) {
        super(encoding);
        this.buffer = buffer;
    }

    @Override
    public long getSize() {
        return buffer.getByteSize() / 4;
    }

    @Override
    public Buffer getBuffer() {
        return buffer;
    }

    @Override
    public long getByteOffset() {
        return byteOffset;
    }

    @Override
    public long getOffset() {
        return byteOffset/4;
    }

    @Override
    public DefaultFloat32Cursor offset(long position) {
        byteOffset = position*4;
        return this;
    }

    @Override
    public Float32Cursor cursor() {
        return new DefaultFloat32Cursor(buffer, encoding);
    }

    @Override
    public float read(long position) {
        return buffer.readFloat32(position*4);
    }

    @Override
    public void read(long position, float[] array) {
        buffer.readFloat32(array,position*4);
    }

    @Override
    public void read(long position, float[] array, int arrayOffset, int length) {
        buffer.readFloat32(array,arrayOffset,length,position*4);
    }

    @Override
    public void write(long position, float value) {
        buffer.writeFloat32(value, position*4);
    }

    @Override
    public void write(long position, float[] array) {
        write(position, array, 0, array.length);
    }

    @Override
    public void write(long position, float[] array, int arrayOffset, int length) {
        buffer.writeFloat32(array,arrayOffset,length, position*4);
    }

    @Override
    public float read() {
        float v = buffer.readFloat32(byteOffset);
        byteOffset +=4;
        return v;
    }

    @Override
    public void read(float[] array) {
        read(array,0,array.length);
    }

    @Override
    public void read(float[] array, int arrayOffset, int length) {
        buffer.readFloat32(array,byteOffset);
        byteOffset+=length*4;
    }

    @Override
    public Float32Cursor write(float value) {
        buffer.writeFloat32(value, byteOffset);
        byteOffset += 4;
        return this;
    }

    @Override
    public Float32Cursor write(float[] array) {
        return write(array, 0, array.length);
    }

    @Override
    public Float32Cursor write(float[] array, int arrayOffset, int length) {
        buffer.writeFloat32(array,arrayOffset,length, byteOffset);
        byteOffset += length*4;
        return this;
    }

    @Override
    public float[] toFloatArray() {
        final float[] array = new float[(int) getSize()];
        read(array);
        return array;
    }

}
