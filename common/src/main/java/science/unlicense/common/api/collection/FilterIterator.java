
package science.unlicense.common.api.collection;

import science.unlicense.common.api.exception.MishandleException;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Filter iterator elements based on a predicate.
 *
 * @author Johann Sorel
 */
final class FilterIterator implements Iterator {

    private final Iterator sub;
    private final Predicate predicate;
    private Object next = null;

    FilterIterator(Iterator sub, Predicate predicate) {
        this.sub = sub;
        this.predicate = predicate;
    }

    public boolean hasNext() {
        findNext();
        return next != null;
    }

    public Object next() {
        findNext();
        if (next == null) {
            throw new MishandleException("No more elements");
        }
        Object ret = next;
        next = null;
        return ret;
    }

    private void findNext() {
        if (next != null) {
            return;
        }
        while (next == null) {
            if (sub.hasNext()) {
                final Object candidate = sub.next();
                if (predicate.evaluate(candidate)) {
                    next = candidate;
                }
            } else {
                break;
            }
        }
    }

    public boolean remove() {
        return sub.remove();
    }

    public void close() {
        sub.close();
    }

}
