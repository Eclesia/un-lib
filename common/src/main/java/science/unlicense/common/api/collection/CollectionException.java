
package science.unlicense.common.api.collection;

/**
 *
 * @author Johann Sorel
 */
public class CollectionException extends RuntimeException {

    public CollectionException() {
    }

    public CollectionException(String s) {
        super(s);
    }

    public CollectionException(Throwable cause) {
        super(cause);
    }

    public CollectionException(String message, Throwable cause) {
        super(message, cause);
    }


}
