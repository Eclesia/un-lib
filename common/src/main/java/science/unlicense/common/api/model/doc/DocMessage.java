
package science.unlicense.common.api.model.doc;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.DefaultEventMessage;
import science.unlicense.common.api.event.MessagePredicate;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Document events list the document field changes.
 *
 * @author Johann Sorel
 */
public class DocMessage extends DefaultEventMessage {

    public static final Predicate PREDICATE = new MessagePredicate(DocMessage.class);

    private final Collection changes;

    public DocMessage(boolean consumable, Sequence changes) {
        super(consumable);
        this.changes = changes;
    }

    public Collection getChanges() {
        return changes;
    }

    public Chars toChars() {
        return Nodes.toChars(new Chars("DocEvent"), changes);
    }

    public static class FieldChange extends CObject{

        private final Chars id;
        private final Object oldValue;
        private final Object newValue;

        public FieldChange(Chars id, Object oldValue, Object newValue) {
            this.id = id;
            this.oldValue = oldValue;
            this.newValue = newValue;

            if (CObjects.equals(oldValue, newValue)) {
                throw new RuntimeException("Uncoherent event, new and old values are equal");
            }
        }

        public Chars getId() {
            return id;
        }

        public Object getNewValue() {
            return newValue;
        }

        public Object getOldValue() {
            return oldValue;
        }

        public Chars toChars() {
            return id.concat(':').concat(CObjects.toChars(oldValue)).concat('>').concat(CObjects.toChars(newValue));
        }

    }
}
