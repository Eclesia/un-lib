
package science.unlicense.common.api.graph;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractEdge implements Edge{

    public boolean isLoop() {
        final Vertex v1 = getVertex1();
        final Vertex v2 = getVertex2();
        if (v1 == null || v2 == null) {
            return false;
        }
        return v1.equals(v2);
    }

    public int getAdjacentVertices(Edge edge) {
        int res = 0;
        if (getVertex1().equals(edge.getVertex1())) {
            res++;
        } else if (getVertex1().equals(edge.getVertex2())) {
            res++;
        }
        if (getVertex2().equals(edge.getVertex1())) {
            res +=2;
        } else if (getVertex2().equals(edge.getVertex2())) {
            res +=2;
        }
        return res;
    }

    @Override
    public boolean isParallale(Edge edge) {
        return (getVertex1().equals(edge.getVertex1())||getVertex1().equals(edge.getVertex2()))
            && (getVertex2().equals(edge.getVertex1())||getVertex2().equals(edge.getVertex2()));
    }

}
