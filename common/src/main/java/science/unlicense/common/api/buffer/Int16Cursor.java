
package science.unlicense.common.api.buffer;

/**
 *
 * @author Johann Sorel
 */
public interface Int16Cursor extends Cursor {

    long getOffset();

    Int16Cursor offset(long position);

    short read();

    void read(short[] array);

    void read(short[] array, int arrayOffset, int length);

    Int16Cursor write(short value);

    Int16Cursor write(short[] array);

    Int16Cursor write(short[] array, int arrayOffset, int length);

}
