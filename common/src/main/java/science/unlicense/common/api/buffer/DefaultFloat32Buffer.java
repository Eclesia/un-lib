
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class DefaultFloat32Buffer extends AbstractBuffer {

    private static final int[] MASKS = {
        0xFF000000,
        0x00FF0000,
        0x0000FF00,
        0x000000FF};
    private static final int[] OFFSET = {
        24,16,8,0};

    private final float[] buffer;

    public DefaultFloat32Buffer(float[] buffer) {
        super(science.unlicense.common.api.number.Float32.TYPE, Endianness.BIG_ENDIAN, DefaultBufferFactory.INSTANCE);
        this.buffer = buffer;
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public Object getBackEnd() {
        return buffer;
    }

    @Override
    public long getByteSize() {
        return buffer.length*4;
    }

    @Override
    public byte readInt8(long offset) {
        final int index = (int) (offset/4);
        final int mod = (int) (offset%4);
        final float f = buffer[index];
        final int i = java.lang.Float.floatToRawIntBits(f);
        return (byte) ( (i & MASKS[mod]) >> OFFSET[mod] );
    }

    @Override
    public float readFloat32(long offset) {
        if (offset%4==0) {
            return buffer[(int) (offset/4)];
        } else {
            return super.readFloat32(offset);
        }
    }

    @Override
    public void readFloat32(float[] array, int arrayOffset, int length, long offset) {
        if (offset%4==0) {
            Arrays.copy(buffer, (int) (offset/4), length, array, arrayOffset);
        } else {
            super.readFloat32(array, arrayOffset, length, offset);
        }
    }

    @Override
    public void writeInt8(byte value, long offset) {
        final int index = (int) (offset/4);
        final int mod = (int) (offset%4);
        final float f = buffer[index];
        int i = java.lang.Float.floatToRawIntBits(f);
        i &= ~MASKS[mod];
        i |= (value&0xFF) << OFFSET[mod];
        buffer[index] = java.lang.Float.intBitsToFloat(i);
    }

    @Override
    public void writeFloat32(float value, long offset) {
        if (offset%4==0) {
            buffer[(int) (offset/4)] = value;
        } else {
            super.writeFloat32(value, offset);
        }
    }

    @Override
    public void writeFloat32(float[] array, int arrayOffset, int length, long offset) {
        if (offset%4==0) {
            Arrays.copy(array, arrayOffset, length, buffer, (int) (offset/4));
        } else {
            super.writeFloat32(array, arrayOffset, length, offset);
        }
    }

    @Override
    public Buffer copy(BufferFactory factory, NumberType primitive, Endianness encoding) {
        if (  (factory == null || this.factory == factory)
           && (primitive == null || this.numericType == primitive)
           && (encoding == null || this.endianness == encoding)) {
            return new DefaultFloat32Buffer(Arrays.copy(buffer));
        }
        return super.copy(factory, primitive, encoding);
    }

}
