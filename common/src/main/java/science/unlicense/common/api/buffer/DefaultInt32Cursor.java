
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.number.Endianness;

/**
 * Both Int cursor and int buffer view.
 *
 * @author Johann Sorel
 */
public class DefaultInt32Cursor extends AbstractCursor implements Int32Cursor, Buffer.Int32 {

    final Buffer buffer;

    //current offset from start.
    private long byteOffset = 0;

    public DefaultInt32Cursor(Buffer buffer, Endianness encoding) {
        super(encoding);
        this.buffer = buffer;
    }

    @Override
    public Buffer getBuffer() {
        return buffer;
    }

    @Override
    public long getByteOffset() {
        return byteOffset;
    }

    @Override
    public long getOffset() {
        return byteOffset/4;
    }

    @Override
    public long getSize() {
        return buffer.getByteSize() / 4;
    }

    public Int32Cursor cursor() {
        return new DefaultInt32Cursor(buffer, encoding);
    }

    @Override
    public DefaultInt32Cursor offset(long position) {
        byteOffset = position*4;
        return this;
    }

    @Override
    public int read(long position) {
        return buffer.readInt32(position*4);
    }

    @Override
    public void read(long position, int[] array) {
        buffer.readInt32(array,position*4);
    }

    @Override
    public void read(long position, int[] array, int arrayOffset, int length) {
        buffer.readInt32(array,arrayOffset,length,position*4);
    }

    @Override
    public void write(long position, int value) {
        buffer.writeInt32(value, position*4);
    }

    @Override
    public void write(long position, int[] array) {
        write(position, array, 0, array.length);
    }

    @Override
    public void write(long position, int[] array, int arrayOffset, int length) {
        buffer.writeInt32(array,arrayOffset,length, position*4);
    }

    @Override
    public int read() {
        int v = buffer.readInt32(byteOffset);
        byteOffset +=4;
        return v;
    }

    @Override
    public void read(int[] array) {
        read(array,0,array.length);
    }

    @Override
    public void read(int[] array, int arrayOffset, int length) {
        buffer.readInt32(array,byteOffset);
        byteOffset+=length*4;
    }

    @Override
    public Int32Cursor write(int value) {
        buffer.writeInt32(value, byteOffset);
        byteOffset += 4;
        return this;
    }

    @Override
    public Int32Cursor write(int[] array) {
        return write(array, 0, array.length);
    }

    @Override
    public Int32Cursor write(int[] array, int arrayOffset, int length) {
        buffer.writeInt32(array,arrayOffset,length, byteOffset);
        byteOffset += length*4;
        return this;
    }

    @Override
    public int[] toIntArray() {
        final int[] array = new int[(int) getSize()];
        read(array);
        return array;
    }
}
