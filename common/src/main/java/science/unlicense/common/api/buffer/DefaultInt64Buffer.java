
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class DefaultInt64Buffer extends AbstractBuffer{

    private static final long[] MASKS = {
        0xFF00000000000000l,
        0x00FF000000000000l,
        0x0000FF0000000000l,
        0x000000FF00000000l,
        0x00000000FF000000l,
        0x0000000000FF0000l,
        0x000000000000FF00l,
        0x00000000000000FFl};
    private static final int[] OFFSET = {
        56,48,40,32,24,16,8,0};

    private final long[] buffer;

    public DefaultInt64Buffer(long[] buffer) {
        super(science.unlicense.common.api.number.Int64.TYPE, Endianness.BIG_ENDIAN, DefaultBufferFactory.INSTANCE);
        this.buffer = buffer;
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public Object getBackEnd() {
        return buffer;
    }

    @Override
    public long getByteSize() {
        return buffer.length*8;
    }

    @Override
    public byte readInt8(long offset) {
        final int index = (int) (offset/8);
        final int mod = (int) (offset%8);
        final long i = buffer[index];
        return (byte) ( (i & MASKS[mod]) >> OFFSET[mod] );
    }

    @Override
    public long readInt64(long offset) {
        if (offset%8==0) {
            return buffer[(int) (offset/8)];
        } else {
            return super.readInt64(offset);
        }
    }

    @Override
    public void readInt64(long[] array, int arrayOffset, int length, long offset) {
        if (offset%8==0) {
            Arrays.copy(buffer, (int) (offset/8), length, array, arrayOffset);
        } else {
            super.readInt64(array, arrayOffset, length, offset);
        }
    }

    @Override
    public void writeInt8(byte value, long offset) {
        final int index = (int) (offset/8);
        final int mod = (int) (offset%8);
        long i = buffer[index];
        i &= ~MASKS[mod];
        i |= (long) (value&0xFF) << OFFSET[mod];
        buffer[index] = i;
    }

    @Override
    public void writeInt64(long value, long offset) {
        if (offset%8==0) {
            buffer[(int) (offset/8)] = value;
        } else {
            super.writeInt64(value, offset);
        }
    }

    @Override
    public void writeInt64(long[] array, int arrayOffset, int length, long offset) {
        if (offset%8==0) {
            Arrays.copy(array, arrayOffset, length, buffer, (int) (offset/8));
        } else {
            super.writeInt64(array, arrayOffset, length, offset);
        }
    }

    @Override
    public Buffer copy(BufferFactory factory, NumberType primitive, Endianness encoding) {
        if (  (factory == null || this.factory == factory)
           && (primitive == null || this.numericType == primitive)
           && (encoding == null || this.endianness == encoding)) {
            return new DefaultInt64Buffer(Arrays.copy(buffer));
        }
        return super.copy(factory, primitive, encoding);
    }
}
