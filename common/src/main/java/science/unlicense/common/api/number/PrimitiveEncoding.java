
package science.unlicense.common.api.number;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class PrimitiveEncoding extends CObject {

    private final int type;
    private final Endianness enc;

    public PrimitiveEncoding(int type, Endianness enc) {
        this.type = type;
        this.enc = enc;
    }

    public int getPrimitive() {
        return type;
    }

    public Endianness getEndianness() {
        return enc;
    }

    public int getBitSize() {
        return Primitive.getSizeInBits(type);
    }

    public Object read(byte[] buffer, int offset) {
        Object candidate = new Chars("");
        switch(type) {
//            case TYPE_1_BIT     : candidate = ds.readBits(1); break;
//            case TYPE_2_BIT     : candidate = ds.readBits(2); break;
//            case TYPE_4_BIT     : candidate = ds.readBits(4); break;
            case Primitive.INT8      : candidate = enc.readByte(buffer, offset); break;
            case Primitive.UINT8     : candidate = enc.readUByte(buffer, offset); break;
            case Primitive.INT16     : candidate = enc.readShort(buffer, offset); break;
            case Primitive.UINT16    : candidate = enc.readUShort(buffer, offset); break;
            case Primitive.INT24     : candidate = enc.readInt24(buffer, offset); break;
            case Primitive.UINT24    : candidate = enc.readUInt24(buffer, offset); break;
            case Primitive.INT32       : candidate = enc.readInt(buffer, offset); break;
            case Primitive.UINT32      : candidate = enc.readUInt(buffer, offset); break;
            case Primitive.INT64      : candidate = enc.readLong(buffer, offset); break;
            case Primitive.UINT64     : candidate = enc.readLong(buffer, offset); break;
            case Primitive.FLOAT32     : candidate = enc.readFloat(buffer, offset); break;
            case Primitive.FLOAT64    : candidate = enc.readDouble(buffer, offset); break;
        }
        return candidate;
    }

    public Chars toChars() {
        Chars candidate = new Chars("");
        switch(type) {
            case Primitive.BITS1     : candidate = new Chars("1Bit"); break;
            case Primitive.BITS2     : candidate = new Chars("2Bit"); break;
            case Primitive.BITS4     : candidate = new Chars("4Bit"); break;
            case Primitive.INT8      : candidate = new Chars("Byte"); break;
            case Primitive.UINT8     : candidate = new Chars("UByte"); break;
            case Primitive.INT16     : candidate = new Chars("Short"); break;
            case Primitive.UINT16    : candidate = new Chars("UShort"); break;
            case Primitive.INT24     : candidate = new Chars("Int24"); break;
            case Primitive.UINT24    : candidate = new Chars("UInt24"); break;
            case Primitive.INT32       : candidate = new Chars("Int"); break;
            case Primitive.UINT32      : candidate = new Chars("UInt"); break;
            case Primitive.INT64      : candidate = new Chars("Long"); break;
            case Primitive.UINT64     : candidate = new Chars("Ulong"); break;
            case Primitive.FLOAT32     : candidate = new Chars("Float"); break;
            case Primitive.FLOAT64    : candidate = new Chars("Double"); break;
        }
        if (type>5) {
            if (enc==Endianness.BIG_ENDIAN) {
                candidate = candidate.concat(new Chars(" BE"));
            } else {
                candidate = candidate.concat(new Chars(" LE"));
            }
        }
        return candidate;
    }

}
