
package science.unlicense.common.api.collection;

/**
 * Ordered dictionnary. inserted values are oredered base on values returned
 * by order method.
 * Keys passed in must be instances of Orderable.
 *
 * @author Johann Sorel
 */
public interface OrderedDictionnary extends Dictionary{

}
