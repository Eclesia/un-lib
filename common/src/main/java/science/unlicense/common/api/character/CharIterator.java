package science.unlicense.common.api.character;

import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.collection.Iterator;

/**
 * Character iterator.
 *
 * @author Johann Sorel
 */
public interface CharIterator extends Iterator {

    /**
     * Get this iterator character encoding.
     *
     * @return CharEncoding
     */
    CharEncoding getEncoding();

    /**
     * Returns next char and move cursor forward.
     *
     * @return next character
     */
    Char next();

    /**
     * returns next char as bytes and move cursor forward.
     *
     * @return next character as byte array
     */
    byte[] nextToBytes();

    /**
     * write next char in buffer and move cursor forward.
     *
     * @param buffer copy next character bytes in this buffer
     */
    void nextToBuffer(ByteSequence buffer);

    /**
     * returns next char as unicode codepoint and move cursor forward.
     *
     * @return next character as unicode codepoint.
     */
    int nextToUnicode();

    /**
     * Returns all informations from the next char.
     * character bytes are written in the bytebuffer and character codepoint / byte length
     * are return in the int array.
     *
     * Caution, the same array is always returned.
     *
     * This method moves the cursor forward.
     *
     * @param bytebuffer byte buffer
     * @return codePoint used to store result,
     *    int array : [0] unicode code point, [1] character byte length
     */
    int[] nextBulk(ByteSequence bytebuffer);

    /**
     * Move cursor on character forward.
     */
    void skip();

    /**
     * Returns next char, do not move cursor forward.
     *
     * @return next character
     */
    Char peek();

    /**
     * returns next char as bytes, do not move cursor forward.
     *
     * @return next character as byte array
     */
    byte[] peekToBytes();

    /**
     * write next char in buffer, do not move cursor forward.
     *
     * @param buffer copy next character bytes in this buffer
     */
    void peekToBuffer(ByteSequence buffer);

    /**
     * returns next char as unicode codepoint, do not move cursor forward.
     *
     * @return next character as unicode codepoint.
     */
    int peekToUnicode();

    /**
     * Returns all informations from the next char.
     * character bytes are written in the bytebuffer and character codepoint / byte length
     * are return in the int array.
     *
     * Caution, the same array is always returned.
     *
     * This method does not move cursor forward.
     *
     * @param bytebuffer byte buffer
     * @return codePoint used to store result,
     *    int array : [0] unicode code point, [1] character byte length
     */
    int[] peekBulk(ByteSequence bytebuffer);

    /**
     * Test if next char equals given one.
     *
     * @param ch character to test
     * @return true if current char equals given char.
     */
    boolean nextEquals(Char ch);

    /**
     *  Test if next char equals given one.
     *
     * @param cdata character to test
     * @return true if current char equals given bytes.
     */
    boolean nextEquals(byte[] cdata);

    /**
     *  Test if next char equals given unicode codepoint.
     *
     * @param codepoint character to test
     * @return true if current char equals given codepoint.
     */
    boolean nextEquals(int codepoint);

}
