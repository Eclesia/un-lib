
package science.unlicense.common.api.exception;

/**
 * Invalid argument exception are thrown when function parameters do not
 * match the expected constraints.
 *
 * @author Johann Sorel
 */
public class InvalidArgumentException extends MishandleException {

    public InvalidArgumentException() {
    }

    public InvalidArgumentException(String s) {
        super(s);
    }

    public InvalidArgumentException(Throwable cause) {
        super(cause);
    }

    public InvalidArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

}
