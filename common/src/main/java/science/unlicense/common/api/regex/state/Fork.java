
package science.unlicense.common.api.regex.state;

/**
 *
 * @author Johann Sorel
 */
public interface Fork extends NFAState {

    NFAState getNext1State();

    NFAState getNext2State();

}
