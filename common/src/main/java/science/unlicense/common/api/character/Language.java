
package science.unlicense.common.api.character;

/**
 * ISO 639.
 * draft TODO
 *
 * @author Johann Sorel
 */
public interface Language {

    /**
     * Linguistics division which classify this language.
     * @return array never null. at least one element.
     */
    Division[] getDivisions();

    /**
     * Language CharCase.
     * @return CharCase, never null
     */
    CharCase getCase();

    public static interface Division{

        /**
         * Division type, can be country, dialect, ...
         * @return Chars, never null
         */
        public Chars getType();

        /**
         * Division code, should be unique. (fr,gr,en,es,pt,... for country types)
         * @return Chars never null
         */
        public Chars getCode();

    }

}
