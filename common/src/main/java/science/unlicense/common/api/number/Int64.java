
package science.unlicense.common.api.number;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.CharIterator;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.exception.ParseRuntimeException;
import science.unlicense.common.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public final class Int64 extends AbstractNumber {

    public static final NumberType TYPE = new NumberType() {

        @Override
        public Number create(Arithmetic value) {
            if (!(value instanceof Number)) throw new UnsupportedOperationException();
            return new Int64(((Number) value).toLong());
        }

        @Override
        public Class getPrimitiveClass() {
            return long.class;
        }

        @Override
        public Class getValueClass() {
            return Int64.class;
        }

        @Override
        public int getSizeInBits() {
            return 64;
        }

        @Override
        public int getSizeInBytes() {
            return 8;
        }

        @Override
        public int getPrimitiveCode() {
            return Primitive.INT64;
        }
    };

    public static Chars encode(long candidate) {
        final ByteSequence s = new ByteSequence();

        final boolean negative = (candidate < 0);
        if (negative) {
            candidate *= -1;
        }

        long remainder;
        while (true) {
            remainder = candidate % 10;
            candidate = candidate / 10;
            s.put((byte) (remainder+48));
            if (candidate == 0) {
                break;
            }
        }

        byte[] array = s.toArrayByte();
        Arrays.reverse(array, 0, array.length);
        if (negative) {
            array = Arrays.copy(array, 0, array.length, new byte[array.length+1], 1);
            array[0] = 45;
        }
        return new Chars(array);
    }

    public static Chars encodeHexa(long candidate) {
        if (candidate == 0) {
            return new Chars(new byte[]{48});
        }
        final ByteSequence buffer = new ByteSequence();
        while (candidate > 0) {
            buffer.put(Numbers.HEXA[(int) (candidate % 16)]);
            candidate = candidate / 16;
        }
        final byte[] bytes = buffer.toArrayByte();
        Arrays.reverse(bytes, 0, bytes.length);
        return new Chars(bytes);
    }

    public static long decode(CharArray candidate) throws ParseRuntimeException {
        return decode(candidate.createIterator(),true);
    }

    /**
     *
     * @param candidate text de decode
     * @param fromIndex start offset
     * @param toIndex last character excluded,
     *          or use -1 to stop on sequence end or invalid character.
     * @return decoded value
     */
    public static long decode(CharArray candidate, int fromIndex, int toIndex) throws ParseRuntimeException {
        return decode(candidate.createIterator(fromIndex,toIndex),true);
    }

    public static long decode(CharIterator ite, boolean strict) throws ParseRuntimeException {
        if (!ite.hasNext()) {
            throw new ParseRuntimeException("Not an number");
        }
        long value = 0;
        int sign = 1;

        int c = ite.peekToUnicode();
        if (c == 43) { // +
            ite.skip();
        } else if (c == 45) { // -
            sign = -1;
            ite.skip();
        }

        while (ite.hasNext()) {
            c = ite.peekToUnicode();
            if (c < 48 || c > 57) {
                if (strict) {
                    throw new ParseRuntimeException("Not a number");
                } else {
                    break;
                }
            }
            value *= 10;
            value += c - 48;
            ite.skip();
        }

        value *= sign;
        return value;
    }

    public static long decodeHexa(CharArray candidate) throws ParseRuntimeException {
        candidate = candidate.recode(CharEncodings.US_ASCII).toUpperCase();
        final CharIterator ite = candidate.createIterator();
        return decodeHexa(ite);
    }

    public static long decodeHexa(CharArray candidate, int fromIndex, int toIndex) throws ParseRuntimeException {
        candidate = candidate.recode(CharEncodings.US_ASCII).toUpperCase();
        final CharIterator ite = candidate.createIterator(fromIndex,toIndex);
        return decodeHexa(ite);
    }

    public static long decodeHexa(CharIterator ite) throws ParseRuntimeException {
        long value = 0;
        while (ite.hasNext()) {
            int b = ite.nextToBytes()[0] - 48;
            if (b>9) b-=7;
            value = 16*value + b;
        }
        return value;
    }

    private final long value;

    public Int64(long value) {
        this.value = value;
    }

    @Override
    public NumberType getType() {
        return TYPE;
    }

    @Override
    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public int toInteger() {
        return (int) value;
    }

    @Override
    public long toLong() {
        return value;
    }

    @Override
    public float toFloat() {
        return value;
    }

    @Override
    public double toDouble() {
        return value;
    }

    @Override
    public Chars toChars() {
        return encode(value);
    }

    @Override
    public int order(Object other) {
        long of = ((Number) other).toLong();
        return value < of ? -1 : (value > of ? +1 : 0) ;
    }

}
