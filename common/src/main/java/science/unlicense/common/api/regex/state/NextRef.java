
package science.unlicense.common.api.regex.state;

/**
 *
 * @author Johann Sorel
 */
public class NextRef {

    public NFAState state;

    public NextRef() {
    }

    public NextRef(NFAState state) {
        this.state = state;
    }

}
