
package science.unlicense.common.api.collection;

import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.predicate.Predicate;

/**
 *
 * @author Johann Sorel
 */
class ReadOnlyDictionary implements Dictionary {

    private final Dictionary base;

    public ReadOnlyDictionary(Dictionary base) {
        this.base = base;
    }

    @Override
    public int getSize() {
        return base.getSize();
    }

    @Override
    public Set getKeys() {
        return Collections.readOnlySet(base.getKeys());
    }

    @Override
    public Collection getValues() {
        return Collections.readOnlyCollection(base.getValues());
    }

    @Override
    public Collection getPairs() {
        return Collections.readOnlyCollection(base.getPairs());
    }

    @Override
    public Object getValue(Object key) {
        return base.getValue(key);
    }

    @Override
    public void add(Object key, Object value) {
        throw new UnsupportedOperationException("Dictionary is read only.");
    }

    @Override
    public void addAll(Dictionary index) {
        throw new UnsupportedOperationException("Dictionary is read only.");
    }

    @Override
    public Object remove(Object key) {
        throw new UnsupportedOperationException("Dictionary is read only.");
    }

    @Override
    public void removeAll() {
        throw new UnsupportedOperationException("Dictionary is read only.");
    }

    @Override
    public Class[] getEventClasses() {
        return base.getEventClasses();
    }

    @Override
    public EventListener[] getListeners(Predicate predicate) {
        return base.getListeners(predicate);
    }

    @Override
    public void addEventListener(Predicate predicate, EventListener listener) {
        base.addEventListener(predicate, listener);
    }

    @Override
    public void removeEventListener(Predicate predicate, EventListener listener) {
        base.removeEventListener(predicate, listener);
    }

}
