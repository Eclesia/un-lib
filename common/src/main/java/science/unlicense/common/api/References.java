
package science.unlicense.common.api;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/**
 *
 * @author Johann Sorel
 */
public final class References {

    public static final ReferenceQueue QUEUE = new ReferenceQueue();
    static {
        final Thread t = new Thread() {
            public void run() {
                for (;;) {
                    try {
                        Weak ref = (Weak) QUEUE.remove();
                        ref.r.release();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        };
        t.setName("[Unlicense-lib] Weak reference disposer");
        t.start();
    }

    private References() {}

    /**
     * A weak reference which will call the attached releasable when the reference
     * is garbage collected.
     */
    public static final class Weak extends WeakReference {

        private final Releasable r;

        public Weak(java.lang.Object referent, Releasable releasable) {
            super(referent, QUEUE);
            this.r = releasable;
        }
    }

}
