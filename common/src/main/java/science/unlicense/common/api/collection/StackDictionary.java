
package science.unlicense.common.api.collection;

import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Read only dictionary implementation which peek values from other dictionaries.
 *
 * @author Johann Sorel
 */
public final class StackDictionary implements Dictionary {

    private Dictionary[] dicos;

    public StackDictionary(Dictionary[] dicos) {
        this.dicos = dicos;
    }

    @Override
    public int getSize() {
        return getKeys().getSize();
    }

    @Override
    public Set getKeys() {
        final Set keys = new HashSet();
        for (int i=0;i<dicos.length;i++) {
            keys.addAll(dicos[i].getKeys());
        }
        return keys;
    }

    @Override
    public Collection getValues() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Collection getPairs() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object getValue(Object key) {
        for (int i=0;i<dicos.length;i++) {
            Object value = dicos[i].getValue(key);
            if (value != null) return value;
        }
        return null;
    }

    @Override
    public void add(Object key, Object value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void addAll(Dictionary index) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public Object remove(Object key) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void removeAll() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[0];
    }

    @Override
    public EventListener[] getListeners(Predicate predicate) {
        return new EventListener[0];
    }

    @Override
    public void addEventListener(Predicate predicate, EventListener listener) {
    }

    @Override
    public void removeEventListener(Predicate predicate, EventListener listener) {
    }

}
