
package science.unlicense.common.api.number;

/**
 *
 * @author Johann Sorel
 */
public class Bits extends AbstractNumber {

    public static final NumberType TYPE_1_BIT = new Bits.Type(1);
    public static final NumberType TYPE_2_BIT = new Bits.Type(2);
    public static final NumberType TYPE_4_BIT = new Bits.Type(4);

    private final long value;
    private final int size;

    public Bits(long value) {
        this(value, 64);
    }

    public Bits(long value, int size) {
        this.value = value;
        this.size = size;
    }

    @Override
    public NumberType getType() {
        return new Type(size);
    }

    @Override
    public int toInteger() {
        return (int) value;
    }

    @Override
    public long toLong() {
        return value;
    }

    @Override
    public float toFloat() {
        return value;
    }

    @Override
    public double toDouble() {
        return value;
    }

    @Override
    public Arithmetic add(Arithmetic other) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Arithmetic subtract(Arithmetic other) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Arithmetic mult(Arithmetic other) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Arithmetic divide(Arithmetic other) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Arithmetic zero() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isZero() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Arithmetic one() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isOne() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Arithmetic pow(int n) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int order(Object other) {
        long of = ((Number) other).toLong();
        return value < of ? -1 : (value > of ? +1 : 0) ;
    }

    public static class Type implements NumberType {

        private final int nbBit;

        public Type(int nbBit) {
            this.nbBit = nbBit;
        }

        public int getNbBit() {
            return nbBit;
        }

        public Bits create(Arithmetic value) {
            if (!(value instanceof Number)) throw new UnsupportedOperationException();
            return new Bits(((Number) value).toLong());
        }

        public Class getPrimitiveClass() {
            if (nbBit==1) return boolean.class;
            if (nbBit<9) return byte.class;
            return long.class;
        }

        @Override
        public Class getValueClass() {
            return Bits.class;
        }

        @Override
        public int getSizeInBits() {
            return nbBit;
        }

        @Override
        public int getSizeInBytes() {
            return (nbBit+7)/8;
        }

        @Override
        public int getPrimitiveCode() {
            switch(nbBit) {
                case 1 : return Primitive.BITS1;
                case 2 : return Primitive.BITS2;
                case 4 : return Primitive.BITS4;
                default : return Primitive.UNKNOWNED;
            }
        }
    }

}
