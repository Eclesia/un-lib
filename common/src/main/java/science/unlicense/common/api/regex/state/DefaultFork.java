
package science.unlicense.common.api.regex.state;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class DefaultFork extends CObject implements Fork {

    public final NextRef next1 = new NextRef();
    public final NextRef next2 = new NextRef();

    public DefaultFork(NFAState state1) {
        this.next1.state = state1;
    }

    public DefaultFork(NFAState state1, NFAState state2) {
        this.next1.state = state1;
        this.next2.state = state2;
    }

    public Chars toChars() {
        return new Chars("Fork");
    }

    public NextRef getNext1Ref() {
        return next1;
    }

    public NFAState getNext1State() {
        return getNext1Ref().state;
    }

    public NextRef getNext2Ref() {
        return next2;
    }

    public NFAState getNext2State() {
        return getNext2Ref().state;
    }

}
