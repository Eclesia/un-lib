
package science.unlicense.common.api.character;

import science.unlicense.common.api.collection.Iterator;

/**
 *
 * @author Johann Sorel
 */
public interface Translatable {

    /**
     * Get the current language of this object.
     * @return Lanaguage never null
     */
    Language getLanguage();

    /**
     * List available languages translation.
     * @return Language iterator. not null, must contain at least one element.
     */
    Iterator getLanguages();

    /**
     * Get object in given language.
     * @param language; if null or language is not available, defaut language is used
     * @return Object, never null, Object must be of same type as this one.
     */
    Translatable translate(Language language);

}
