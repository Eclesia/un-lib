
package science.unlicense.common.api.predicate;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;

/**
 * Negate a predicate.
 * Will always return the opposite value of wrapped predicate.
 *
 * @author Johann Sorel
 */
public class Not extends CObject implements Predicate, Function {

    public static final Chars NAME = Chars.constant("NOT");
    private final Predicate predicate;

    public Not(Predicate predicate) {
        CObjects.ensureNotNull(predicate);
        this.predicate = predicate;
    }

    public Chars getName() {
        return NAME;
    }

    public Predicate getPredicate() {
        return predicate;
    }

    public Expression[] getParameters() {
        return new Expression[]{predicate};
    }

    public Boolean evaluate(Object candidate) {
        return !predicate.evaluate(candidate);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Not other = (Not) obj;
        return this.predicate.equals(other.predicate);
    }

    public int getHash() {
        return 43 * 7 + this.predicate.hashCode();
    }

}
