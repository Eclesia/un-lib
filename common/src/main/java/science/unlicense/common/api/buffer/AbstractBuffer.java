
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractBuffer extends CObject implements Buffer{

    protected final NumberType numericType;
    protected final Endianness endianness;
    protected final BufferFactory factory;

    public AbstractBuffer(NumberType primitiveType, Endianness encoding, BufferFactory factory) {
        this.numericType = primitiveType;
        this.endianness = encoding;
        this.factory = factory;
    }

    public BufferFactory getFactory() {
        return factory;
    }

    public NumberType getNumericType() {
        return numericType;
    }

    public Endianness getEndianness() {
        return endianness;
    }

    public long getNumbersSize() {
        return getPrimitiveCount(numericType);
    }

    public long getPrimitiveCount(NumberType numericType) {
        final long byteLength = getByteSize();
        return (long) (byteLength/numericType.getSizeInBytes());
    }

    @Override
    public Buffer.Int8 asInt8() {
        return new DefaultInt8Cursor(this, endianness);
    }

    @Override
    public Buffer.Int16 asInt16() {
        return new DefaultInt16Cursor(this, endianness);
    }

    @Override
    public Buffer.Int32 asInt32() {
        return new DefaultInt32Cursor(this, endianness);
    }

    @Override
    public Buffer.Int64 asInt64() {
        return new DefaultInt64Cursor(this, endianness);
    }

    @Override
    public Buffer.Float32 asFloat32() {
        return new DefaultFloat32Cursor(this, endianness);
    }

    @Override
    public Buffer.Float64 asFloat64() {
        return new DefaultFloat64Cursor(this, endianness);
    }

    public DataCursor dataCursor() {
        return dataCursor(numericType,endianness);
    }

    public DataCursor dataCursor(NumberType primitive, Endianness encoding) {
        return new DefaultCursor(this,encoding);
    }

    public Float32Cursor cursorFloat() {
        return new DefaultFloat32Cursor(this, endianness);
    }

    public Float64Cursor cursorDouble() {
        return new DefaultFloat64Cursor(this, endianness);
    }

    @Override
    public final Buffer copy() {
        return copy(null, null, null);
    }

    @Override
    public final Buffer copy(Endianness endianness) {
        return copy(null, null, endianness);
    }

    @Override
    public final Buffer copy(NumberType primitive, Endianness encoding) {
        return copy(null, primitive, encoding);
    }

    @Override
    public Buffer copy(BufferFactory factory, NumberType primitive, Endianness endianness) {
        if (factory == null) factory = this.factory;
        if (primitive == null) primitive = this.numericType;
        if (endianness == null) endianness = this.endianness;

        final double nbElement = getByteSize() / primitive.getSizeInBytes();
        final Buffer newBuffer = factory.create((int) nbElement, primitive, endianness);
        final byte[] array = toByteArray();
        if (this.endianness != endianness) {
            Endianness.reverseOrder(array, primitive.getSizeInBytes());
        }
        newBuffer.writeInt8(array, 0);
        return newBuffer;
    }

    ////////////////////////////////////////////////////////////////////////////

    public byte[] toByteArray() {
        final long size = getPrimitiveCount(science.unlicense.common.api.number.Int8.TYPE);
        final byte[] array = new byte[(int) size];
        readInt8(array, 0);
        return array;
    }

    public int[] toUByteArray() {
        final long size = getPrimitiveCount(science.unlicense.common.api.number.UInt8.TYPE);
        final int[] array = new int[(int) size];
        readUInt8(array, 0);
        return array;
    }

    public short[] toShortArray() {
        final long size = getPrimitiveCount(science.unlicense.common.api.number.Int16.TYPE);
        final short[] array = new short[(int) size];
        readInt16(array, 0);
        return array;
    }

    public int[] toUShortArray() {
        final long size = getPrimitiveCount(science.unlicense.common.api.number.UInt16.TYPE);
        final int[] array = new int[(int) size];
        readUInt16(array, 0);
        return array;
    }

    public int[] toInt24Array() {
        final long size = getPrimitiveCount(science.unlicense.common.api.number.Int24.TYPE);
        final int[] array = new int[(int) size];
        readInt24(array, 0);
        return array;
    }

    public int[] toUInt24Array() {
        final long size = getPrimitiveCount(science.unlicense.common.api.number.UInt24.TYPE);
        final int[] array = new int[(int) size];
        readUInt24(array, 0);
        return array;
    }

    public int[] toIntArray() {
        final long size = getPrimitiveCount(science.unlicense.common.api.number.Int32.TYPE);
        final int[] array = new int[(int) size];
        readInt32(array, 0);
        return array;
    }

    public long[] toUIntArray() {
        final long size = getPrimitiveCount(science.unlicense.common.api.number.UInt32.TYPE);
        final long[] array = new long[(int) size];
        readUInt32(array, 0);
        return array;
    }

    public long[] toLongArray() {
        final long size = getPrimitiveCount(science.unlicense.common.api.number.Int64.TYPE);
        final long[] array = new long[(int) size];
        readInt64(array, 0);
        return array;
    }

    public float[] toFloatArray() {
        final long size = getPrimitiveCount(science.unlicense.common.api.number.Float32.TYPE);
        final float[] array = new float[(int) size];
        readFloat32(array, 0);
        return array;
    }

    public double[] toDoubleArray() {
        final long size = getPrimitiveCount(science.unlicense.common.api.number.Float64.TYPE);
        final double[] array = new double[(int) size];
        readFloat64(array, 0);
        return array;
    }

    ////////////////////////////////////////////////////////////////////////////

    public void readInt8(byte[] array, long offset) {
        readInt8(array, 0, array.length, offset);
    }

    public void readInt8(byte[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            array[arrayOffset+k] = readInt8(offset+k);
        }
    }

    public int readUInt8(long offset) {
        return readInt8(offset) &0xFF;
    }

    public void readUInt8(int[] array, long offset) {
        readUInt8(array, 0, array.length, offset);
    }

    public void readUInt8(int[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            array[arrayOffset+k] = readUInt8(offset+k);
        }
    }

    public short readInt16(long offset) {
        final byte[] array = new byte[2];
        readInt8(array, offset);
        return endianness.readShort(array, 0);
    }

    public void readInt16(short[] array, long offset) {
        readInt16(array, 0, array.length, offset);
    }

    public void readInt16(short[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            array[arrayOffset+k] = readInt16(offset+k*2);
        }
    }

    public int readUInt16(long offset) {
        final byte[] array = new byte[2];
        readInt8(array, offset);
        return endianness.readUShort(array, 0);
    }

    public void readUInt16(int[] array, long offset) {
        readUInt16(array, 0, array.length, offset);
    }

    public void readUInt16(int[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            array[arrayOffset+k] = readUInt16(offset+k*2);
        }
    }

    public int readInt24(long offset) {
        final byte[] array = new byte[3];
        readInt8(array, offset);
        return endianness.readInt24(array, 0);
    }

    public void readInt24(int[] array, long offset) {
        readInt24(array, 0, array.length, offset);
    }

    public void readInt24(int[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            array[arrayOffset+k] = readInt24(offset+k*3);
        }
    }

    public int readUInt24(long offset) {
        final byte[] array = new byte[3];
        readInt8(array, offset);
        return endianness.readUInt24(array, 0);
    }

    public void readUInt24(int[] array, long offset) {
        readUInt24(array, 0, array.length, offset);
    }

    public void readUInt24(int[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            array[arrayOffset+k] = readUInt24(offset+k*3);
        }
    }

    public int readInt32(long offset) {
        final byte[] array = new byte[4];
        readInt8(array, offset);
        return endianness.readInt(array, 0);
    }

    public void readInt32(int[] array, long offset) {
        readInt32(array, 0, array.length, offset);
    }

    public void readInt32(int[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            array[arrayOffset+k] = readInt32(offset+k*4);
        }
    }

    public long readUInt32(long offset) {
        final byte[] array = new byte[4];
        readInt8(array, offset);
        return endianness.readUInt(array, 0);
    }

    public void readUInt32(long[] array, long offset) {
        readUInt32(array, 0, array.length, offset);
    }

    public void readUInt32(long[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            array[arrayOffset+k] = readUInt32(offset+k*4);
        }
    }

    public long readInt64(long offset) {
        final byte[] array = new byte[8];
        readInt8(array, offset);
        return endianness.readLong(array, 0);
    }

    public void readInt64(long[] array, long offset) {
        readInt64(array, 0, array.length, offset);
    }

    public void readInt64(long[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            array[arrayOffset+k] = readInt64(offset+k*8);
        }
    }

    public float readFloat32(long offset) {
        final byte[] array = new byte[4];
        readInt8(array, offset);
        return endianness.readFloat(array, 0);
    }

    public void readFloat32(float[] array, long offset) {
        readFloat32(array, 0, array.length, offset);
    }

    public void readFloat32(float[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            array[arrayOffset+k] = readFloat32(offset+k*4);
        }
    }

    public double readFloat64(long offset) {
        final byte[] array = new byte[8];
        readInt8(array, offset);
        return endianness.readDouble(array, 0);
    }

    public void readFloat64(double[] array, long offset) {
        readFloat64(array, 0, array.length, offset);
    }

    public void readFloat64(double[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            array[arrayOffset+k] = readFloat64(offset+k*8);
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    public void writeInt8(byte[] array, long offset) {
        writeInt8(array, 0, array.length, offset);
    }

    public void writeInt8(byte[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            writeInt8(array[arrayOffset+k],offset+k);
        }
    }

    public void writeUInt8(int value, long offset) {
        writeInt8((byte) value, offset);
    }

    public void writeUInt8(int[] array, long offset) {
        writeUInt8(array, 0, array.length, offset);
    }

    public void writeUInt8(int[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            writeUInt8(array[arrayOffset+k],offset+k);
        }
    }

    public void writeInt16(short value, long offset) {
        final byte[] array = new byte[2];
        endianness.writeShort(value, array, 0);
        writeInt8(array, offset);
    }

    public void writeInt16(short[] array, long offset) {
        writeInt16(array, 0, array.length, offset);
    }

    public void writeInt16(short[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            writeInt16(array[arrayOffset+k],offset+k*2);
        }
    }

    public void writeUInt16(int value, long offset) {
        final byte[] array = new byte[2];
        endianness.writeUShort(value, array, 0);
        writeInt8(array, offset);
    }

    public void writeUInt16(int[] array, long offset) {
        writeUInt16(array, 0, array.length, offset);
    }

    public void writeUInt16(int[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            writeUInt16(array[arrayOffset+k],offset+k*2);
        }
    }

    public void writeInt24(int value, long offset) {
        final byte[] array = new byte[3];
        endianness.writeInt24(value, array, 0);
        writeInt8(array, offset);
    }

    public void writeInt24(int[] array, long offset) {
        writeInt24(array, 0, array.length, offset);
    }

    public void writeInt24(int[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            writeInt24(array[arrayOffset+k],offset+k*3);
        }
    }

    public void writeUInt24(int value, long offset) {
        final byte[] array = new byte[3];
        endianness.writeUInt24(value, array, 0);
        writeInt8(array, offset);
    }

    public void writeUInt24(int[] array, long offset) {
        writeUInt24(array, 0, array.length, offset);
    }

    public void writeUInt24(int[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            writeUInt24(array[arrayOffset+k],offset+k*3);
        }
    }

    public void writeInt32(int value, long offset) {
        final byte[] array = new byte[4];
        endianness.writeInt(value, array, 0);
        writeInt8(array, offset);
    }

    public void writeInt32(int[] array, long offset) {
        writeInt32(array, 0, array.length, offset);
    }

    public void writeInt32(int[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            writeInt32(array[arrayOffset+k],offset+k*4);
        }
    }

    public void writeUInt32(long value, long offset) {
        final byte[] array = new byte[4];
        endianness.writeUInt(value, array, 0);
        writeInt8(array, offset);
    }

    public void writeUInt32(long[] array, long offset) {
        writeUInt32(array, 0, array.length, offset);
    }

    public void writeUInt32(long[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            writeUInt32(array[arrayOffset+k],offset+k*4);
        }
    }

    public void writeInt64(long value, long offset) {
        final byte[] array = new byte[8];
        endianness.writeLong(value, array, 0);
        writeInt8(array, offset);
    }

    public void writeInt64(long[] array, long offset) {
        writeInt64(array, 0, array.length, offset);
    }

    public void writeInt64(long[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            writeInt64(array[arrayOffset+k],offset+k*8);
        }
    }

    public void writeFloat32(float value, long offset) {
        final byte[] array = new byte[4];
        endianness.writeFloat(value, array, 0);
        writeInt8(array, offset);
    }

    public void writeFloat32(float[] array, long offset) {
        writeFloat32(array, 0, array.length, offset);
    }

    public void writeFloat32(float[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            writeFloat32(array[arrayOffset+k],offset+k*4);
        }
    }

    public void writeFloat64(double value, long offset) {
        final byte[] array = new byte[8];
        endianness.writeDouble(value, array, 0);
        writeInt8(array, offset);
    }

    public void writeFloat64(double[] array, long offset) {
        writeFloat64(array, 0, array.length, offset);
    }

    public void writeFloat64(double[] array, int arrayOffset, int length, long offset) {
        for (int k=0;k<length;k++) {
            writeFloat64(array[arrayOffset+k],offset+k*8);
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Buffer other = (Buffer) obj;
        if (this.numericType != other.getNumericType()) {
            return false;
        }
        if (this.endianness != other.getEndianness() && (this.endianness == null || !this.endianness.equals(other.getEndianness()))) {
            return false;
        }

        if (!Arrays.equals(toByteArray(), other.toByteArray())) {
            return false;
        }

        return true;
    }

    public int getHash() {
        int hash = 5;
        hash = 13 * hash + CObjects.getHash(this.numericType);
        hash = 13 * hash + (this.endianness != null ? this.endianness.hashCode() : 0);
        return hash;
    }

    public void dispose() {

    }
}
