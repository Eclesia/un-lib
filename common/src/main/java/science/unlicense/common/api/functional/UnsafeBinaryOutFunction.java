
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface UnsafeBinaryOutFunction {

    Object perform(Object in1, Object in2) throws Exception;

}
