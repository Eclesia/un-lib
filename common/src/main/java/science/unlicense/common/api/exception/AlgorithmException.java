
package science.unlicense.common.api.exception;

/**
 * Throw by processes when some cases did happen when they normally should never happen.
 * Those exceptions are caused by incorrect algorithms or unhandle corner cases.
 *
 * @author Johann Sorel
 */
public class AlgorithmException extends RuntimeException {

    public AlgorithmException() {
    }

    public AlgorithmException(String s) {
        super(s);
    }

    public AlgorithmException(Throwable cause) {
        super(cause);
    }

    public AlgorithmException(String message, Throwable cause) {
        super(message, cause);
    }

}
