
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.number.Endianness;

/**
 * Both Byte cursor and byte buffer view.
 *
 * @author Johann Sorel
 */
public class DefaultInt16Cursor extends AbstractCursor implements Int16Cursor, Buffer.Int16 {

    final Buffer buffer;

    //current offset from start.
    private long byteOffset = 0;

    public DefaultInt16Cursor(Buffer buffer, Endianness encoding) {
        super(encoding);
        this.buffer = buffer;
    }

    @Override
    public Buffer getBuffer() {
        return buffer;
    }

    @Override
    public long getByteOffset() {
        return byteOffset;
    }

    @Override
    public long getOffset() {
        return byteOffset/2;
    }

    @Override
    public long getSize() {
        return buffer.getByteSize() / 2;
    }

    public Int16Cursor cursor() {
        return new DefaultInt16Cursor(buffer, encoding);
    }

    @Override
    public DefaultInt16Cursor offset(long position) {
        byteOffset = position*2;
        return this;
    }

    @Override
    public short read(long position) {
        return buffer.readInt16(position*2);
    }

    @Override
    public void read(long position, short[] array) {
        buffer.readInt16(array,position*2);
    }

    @Override
    public void read(long position, short[] array, int arrayOffset, int length) {
        buffer.readInt16(array,arrayOffset,length,position*2);
    }

    @Override
    public void write(long position, short value) {
        buffer.writeInt16(value, position*2);
    }

    @Override
    public void write(long position, short[] array) {
        write(position, array, 0, array.length);
    }

    @Override
    public void write(long position, short[] array, int arrayOffset, int length) {
        buffer.writeInt16(array,arrayOffset,length, position*2);
    }

    @Override
    public short read() {
        short v = buffer.readInt16(byteOffset);
        byteOffset +=2;
        return v;
    }

    @Override
    public void read(short[] array) {
        read(array,0,array.length);
    }

    @Override
    public void read(short[] array, int arrayOffset, int length) {
        buffer.readInt16(array,byteOffset);
        byteOffset += length*2;
    }

    @Override
    public Int16Cursor write(short value) {
        buffer.writeInt16(value, byteOffset);
        byteOffset += 2;
        return this;
    }

    @Override
    public Int16Cursor write(short[] array) {
        return write(array, 0, array.length);
    }

    @Override
    public Int16Cursor write(short[] array, int arrayOffset, int length) {
        buffer.writeInt16(array,arrayOffset,length, byteOffset);
        byteOffset += length*2;
        return this;
    }

}
