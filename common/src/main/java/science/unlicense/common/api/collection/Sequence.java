
package science.unlicense.common.api.collection;

import science.unlicense.common.api.exception.InvalidIndexException;

/**
 * Collection with ordered elements.
 *
 * @author Johann Sorel
 * @author Yann D'Isanto
 */
public interface Sequence extends Collection {

    /**
     * Returns the item at the specified index.
     *
     * @param index the index.
     * @return the item corresponding to the specified index.
     */
    Object get(int index) throws InvalidIndexException;

    /**
     * Inserts the specified item at the specified index.
     *
     * @param index the index to insert the item at.
     * @param value the item to insert.
     * @return true if the item has properly been added.
     * @throws InvalidIndexException if index &lt; 0 or index &gt;= this
     * list size
     */
    boolean add(int index, Object value) throws InvalidIndexException;

    /**
     * Inserts the specified items at the specified index.
     *
     * @param index the index to insert the items at.
     * @param candidates elements to insert.
     * @return true if all elements successfuly inserted.
     */
    boolean addAll(int index, Object[] candidates) throws InvalidIndexException;

    /**
     * Inserts the specified items at the specified index.
     *
     * @param index the index to insert the items at.
     * @param candidates elements to insert.
     * @return true if all elements successfuly inserted.
     */
    boolean addAll(int index, Collection candidates) throws InvalidIndexException;

    /**
     * Replaces the item at the specified index with the specified index.
     *
     * @param index the index of the item to replace.
     * @param item the item to set at the specified index.
     * @return the replaced item.
     * @throws InvalidIndexException if index &lt; 0 or index &gt;= this
     * list size
     */
    Object replace(int index, Object item) throws InvalidIndexException;

    /**
     * Removes the item at the specified index.
     *
     * @param index the index to remove the item at.
     * @return the removed item.
     */
    boolean remove(int index) throws InvalidIndexException;

    /**
     * Search given object in this sequence and return the first instance
     * index.
     *
     * @param item object to search
     * @return index or -1 if not found.
     */
    int search(Object item);

    /**
     * @return an iterator to iterate this list in reverse order.
     */
    Iterator createReverseIterator();

}
