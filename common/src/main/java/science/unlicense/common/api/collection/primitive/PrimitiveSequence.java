
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public interface PrimitiveSequence extends Sequence {

    PrimitiveSequence moveTo(int position);

    boolean removeNext(int nbElement);

}
