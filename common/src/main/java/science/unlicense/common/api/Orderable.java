
package science.unlicense.common.api;

/**
 * Interface for classes able to order instances.
 *
 * @author Johann Sorel
 */
public interface Orderable {

    /**
     *
     * @param other object to compare
     * @return negative,zero or positive value if this object is
     *         before , same or after the given object.
     */
    int order(java.lang.Object other);

}
