
package science.unlicense.common.api.model;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public interface Presentable {

    /**
     * Get identifier.
     * Should be unique on the system.
     * @return Chars, never null
     */
    Chars getId();

    /**
     * Get human readable short title.
     * @return CharArray, never null
     */
    CharArray getTitle();

    /**
     * Get task description.
     * @return CharArray, never null
     */
    CharArray getDescription();

}
