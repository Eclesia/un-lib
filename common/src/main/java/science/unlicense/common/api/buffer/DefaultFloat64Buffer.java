
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class DefaultFloat64Buffer extends AbstractBuffer{

    private static final long[] MASKS = {
        0xFF00000000000000l,
        0x00FF000000000000l,
        0x0000FF0000000000l,
        0x000000FF00000000l,
        0x00000000FF000000l,
        0x0000000000FF0000l,
        0x000000000000FF00l,
        0x00000000000000FFl};
    private static final int[] OFFSET = {
        56,48,40,32,24,16,8,0};

    private final double[] buffer;

    public DefaultFloat64Buffer(double[] buffer) {
        super(science.unlicense.common.api.number.Float64.TYPE, Endianness.BIG_ENDIAN, DefaultBufferFactory.INSTANCE);
        this.buffer = buffer;
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public Object getBackEnd() {
        return buffer;
    }

    @Override
    public long getByteSize() {
        return buffer.length*8;
    }

    @Override
    public byte readInt8(long offset) {
        final int index = (int) (offset/8);
        final int mod = (int) (offset%8);
        final double f = buffer[index];
        final long i = java.lang.Double.doubleToRawLongBits(f);
        return (byte) ( (i & MASKS[mod]) >> OFFSET[mod] );
    }

    @Override
    public double readFloat64(long offset) {
        if (offset%8==0) {
            return buffer[(int) (offset/8)];
        } else {
            return super.readFloat64(offset);
        }
    }

    @Override
    public void readFloat64(double[] array, int arrayOffset, int length, long offset) {
        if (offset%8==0) {
            Arrays.copy(buffer, (int) (offset/8), length, array, arrayOffset);
        } else {
            super.readFloat64(array, arrayOffset, length, offset);
        }
    }

    @Override
    public void writeInt8(byte value, long offset) {
        final int index = (int) (offset/8);
        final int mod = (int) (offset%8);
        final double f = buffer[index];
        long i = java.lang.Double.doubleToRawLongBits(f);
        i &= ~MASKS[mod];
        i |= (long) (value&0xFF) << OFFSET[mod];
        buffer[index] = java.lang.Double.longBitsToDouble(i);
    }

    @Override
    public void writeFloat64(double value, long offset) {
        if (offset%8==0) {
            buffer[(int) (offset/8)] = value;
        } else {
            super.writeFloat64(value, offset);
        }
    }

    @Override
    public void writeFloat64(double[] array, int arrayOffset, int length, long offset) {
        if (offset%8==0) {
            Arrays.copy(array, arrayOffset, length, buffer, (int) (offset/8));
        } else {
            super.writeFloat64(array, arrayOffset, length, offset);
        }
    }

    @Override
    public Buffer copy(BufferFactory factory, NumberType primitive, Endianness encoding) {
        if (  (factory == null || this.factory == factory)
           && (primitive == null || this.numericType == primitive)
           && (encoding == null || this.endianness == encoding)) {
            return new DefaultFloat64Buffer(Arrays.copy(buffer));
        }
        return super.copy(factory, primitive, encoding);
    }
}
