
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface InOutFunction extends UnsafeInOutFunction {

    Object perform(Object input);

}
