
package science.unlicense.common.api.regex.state;

import science.unlicense.common.api.character.Chars;

/**
 * Case-sensitive char.
 *
 * @author Johann Sorel
 */
public final class Char extends AbstractForward implements Evaluator {

    public final int cp;

    public Char(int cp) {
        this.cp = cp;
    }

    public boolean evaluate(Object cp) {
        return this.cp == (Integer) cp;
    }

    public Chars toChars() {
        return new Chars("Char[").concat(cp).concat(']');
    }

}
