
package science.unlicense.common.api.event;

/**
 * Object listening to events must implement this interface.
 * Events will be catch in the receiveEvent method.
 *
 * @author Johann Sorel
 */
public interface EventListener {

    /**
     * Called by the EventSource when a new event is fired.
     *
     * @param event event object, never null
     */
    void receiveEvent(Event event);

}
