
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class DefaultInt16Buffer extends AbstractBuffer{

    private static final int[] MASKS = {
        0xFF00,
        0x00FF};
    private static final int[] OFFSET = {
        8,0};

    private final short[] buffer;

    public DefaultInt16Buffer(short[] buffer) {
        super(science.unlicense.common.api.number.Int16.TYPE, Endianness.BIG_ENDIAN, DefaultBufferFactory.INSTANCE);
        this.buffer = buffer;
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public Object getBackEnd() {
        return buffer;
    }

    @Override
    public long getByteSize() {
        return buffer.length*2;
    }

    @Override
    public byte readInt8(long offset) {
        final int index = (int) (offset/2);
        final int mod = (int) (offset%2);
        final int i = buffer[index];
        return (byte) ( (i & MASKS[mod]) >> OFFSET[mod] );
    }

    @Override
    public short readInt16(long offset) {
        if (offset%2==0) {
            return buffer[(int) (offset/2)];
        } else {
            return super.readInt16(offset);
        }
    }

    @Override
    public void readInt16(short[] array, int arrayOffset, int length, long offset) {
        if (offset%2==0) {
            Arrays.copy(buffer, (int) (offset/2), length, array, arrayOffset);
        } else {
            super.readInt16(array, arrayOffset, length, offset);
        }
    }

    @Override
    public void writeInt8(byte value, long offset) {
        final int index = (int) (offset/2);
        final int mod = (int) (offset%2);
        int i = buffer[index];
        i &= ~MASKS[mod];
        i |= (value&0xFF) << OFFSET[mod];
        buffer[index] = (short) i;
    }

    @Override
    public void writeInt16(short value, long offset) {
        if (offset%2==0) {
            buffer[(int) (offset/2)] = value;
        } else {
            super.writeInt16(value, offset);
        }
    }

    @Override
    public void writeInt16(short[] array, int arrayOffset, int length, long offset) {
        if (offset%2==0) {
            Arrays.copy(array, arrayOffset, length, buffer, (int) (offset/4));
        } else {
            super.writeInt16(array, arrayOffset, length, offset);
        }
    }

    @Override
    public Buffer copy(BufferFactory factory, NumberType primitive, Endianness encoding) {
        if (  (factory == null || this.factory == factory)
           && (primitive == null || this.numericType == primitive)
           && (encoding == null || this.endianness == encoding)) {
            return new DefaultInt16Buffer(Arrays.copy(buffer));
        }
        return super.copy(factory, primitive, encoding);
    }
}
