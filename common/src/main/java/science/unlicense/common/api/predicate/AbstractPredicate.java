
package science.unlicense.common.api.predicate;

import science.unlicense.common.api.CObject;

/**
 * @author Yann D'Isanto
 */
public abstract class AbstractPredicate extends CObject implements Predicate {

}
