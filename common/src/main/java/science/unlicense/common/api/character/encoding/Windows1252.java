
package science.unlicense.common.api.character.encoding;

import science.unlicense.common.api.character.AbstractCharEncoding;
import science.unlicense.common.api.character.CharEncoding;

/**
 * Windows-1252 encoding.
 * Also called CP-1252.
 *
 * Resources :
 * http://unicode.org/Public/MAPPINGS/VENDORS/MICSFT/WINDOWS/CP1252.TXT
 * http://en.wikipedia.org/wiki/Windows-1252
 *
 * @author Johann Sorel
 */
public class Windows1252 extends AbstractCharEncoding{

    private static final byte nomatch = (byte) 95; //underscore used to match unsupported characters

    //from range 0x80 to 0xA0
    private final int[] tounicode = new int[]{
        0x20AC,    -1,0x201A,0x0192,0x201E,0x2026,0x2020,0x2021,
        0x02C6,0x2030,0x0160,0x2039,0x0152,    -1,0x017D,    -1,
            -1,0x2018,0x2019,0x201C,0x201D,0x2022,0x2013,0x2014,
        0x02DC,0x2122,0x0161,0x203A,0x0153,    -1,0x017E,0x0178
        };

    public Windows1252() {
        super(new byte[]{'W','I','N','D','O','W','S','-','1','2','5','2'},true,1);
    }

    public int charlength(byte[] array, int offset) {
        return 1;
    }

    public int length(byte[] array) {
        return array.length;
    }

    public void toUnicode(byte[] array, int[] codePoint, int offset) {
        final int c = array[offset] & 0xFF;
        codePoint[0] = (c<0x80 || c>0x9F) ? c : tounicode[c-0x80];
        codePoint[1] = 1;
    }

    public byte[] toBytes(int codePoint) {
        final byte[] res = new byte[]{nomatch};
        if (codePoint<0x80 || (codePoint>0x9F && codePoint<=0xFF) ) {
            res[0] = (byte) codePoint;
        } else {
            for (int i=0;i<tounicode.length;i++) {
                if (tounicode[i] == -1) continue;
                if (tounicode[i] == codePoint) {
                    res[0] = (byte) (i+0x80);
                    break;
                }
            }
        }
        return res;
    }

    public void toBytes(int[] cp, byte[] array, int offset) {
        cp[1] = 1;
        final int codePoint = cp[0];
        array[offset]=nomatch;
        if (codePoint<0x80 || (codePoint>0x9F && codePoint<=0xFF) ) {
            array[offset] = (byte) codePoint;
        } else {
            for (int i=0;i<tounicode.length;i++) {
                if (tounicode[i] == -1) continue;
                if (tounicode[i] == codePoint) {
                    array[offset] = (byte) (i+0x80);
                    break;
                }
            }
        }
    }

    public boolean isComplete(byte[] array, int offset, int length) {
        return length == 1;
    }

}
