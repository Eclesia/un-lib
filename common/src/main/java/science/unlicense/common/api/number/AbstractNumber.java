
package science.unlicense.common.api.number;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractNumber extends CObject implements Number {

    @Override
    public Arithmetic op(int opCode) {
        throw new UnimplementedException("Unsupported operand "+ opCode);
    }

    @Override
    public Chars toChars() {
        return Float64.encode(toDouble());
    }

    @Override
    public int getHash() {
        return toInteger();
    }

}
