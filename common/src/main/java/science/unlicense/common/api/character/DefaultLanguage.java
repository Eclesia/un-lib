

package science.unlicense.common.api.character;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.Arrays;

/**
 *
 * @author Johann Sorel
 */
public class DefaultLanguage extends CObject implements Language {

    protected final Division[] divisions;
    protected final CharCase cas;

    public DefaultLanguage(Division division, CharCase cas) {
        this(new Division[]{division},cas);
    }

    public DefaultLanguage(Division[] divisions, CharCase cas) {
        this.divisions = divisions;
        this.cas = cas;
    }

    public Division[] getDivisions() {
        return Arrays.copy(divisions, new Division[divisions.length]);
    }

    public CharCase getCase() {
        return cas;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultLanguage other = (DefaultLanguage) obj;
        if (!Arrays.equals(this.divisions, other.divisions)) {
            return false;
        }
        if (this.cas != other.cas && (this.cas == null || !this.cas.equals(other.cas))) {
            return false;
        }
        return true;
    }

    public int getHash() {
        int hash = 7;
        hash = 89 * hash + Arrays.computeHash(this.divisions);
        hash = 89 * hash + (this.cas != null ? this.cas.hashCode() : 0);
        return hash;
    }

    public Chars toChars() {
        if (divisions.length==1) {
            return new Chars(divisions[0].toString());
        }
        return super.toChars();
    }

}
