
package science.unlicense.common.api.regex;

import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.common.api.collection.primitive.IntSequence;
import science.unlicense.common.api.character.CharIterator;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.ArrayStack;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Stack;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.regex.state.AnyChar;
import science.unlicense.common.api.regex.state.Char;
import science.unlicense.common.api.regex.state.DefaultFork;
import science.unlicense.common.api.regex.state.Fork;
import science.unlicense.common.api.regex.state.InsChar;
import science.unlicense.common.api.regex.state.List;
import science.unlicense.common.api.regex.state.Match;
import science.unlicense.common.api.regex.state.Range;

/**
 * This regex implementation uses an NFA approach.
 * TODO DFA
 *
 * there are plenty of docs on regex, those are just the most useful I found :
 * http://en.wikipedia.org/wiki/Thompson's_construction_algorithm
 * http://en.wikipedia.org/wiki/Nondeterministic_finite_automaton
 * http://swtch.com/~rsc/regexp/regexp1.html
 *
 *
 * @author Johann Sorel
 */
public final class Regex {

    private static final int ESCAPE = '\\';
    private static final int POSTFIX_CONCAT = '>';
    private static final int CHOICE = '|';
    private static final int ZERO_ONE = '?';
    private static final int ZERO_MANY = '*';
    private static final int ONE_MANY = '+';
    private static final int GROUP_START = '(';
    private static final int GROUP_END = ')';
    private static final int RANGE = '-';
    private static final int RANGE_START = '[';
    private static final int RANGE_END = ']';
    private static final int NOT = '^';
    private static final int WORD = '\'';
    private static final int INSWORD = '\"';
    private static final int ANY = '.';

    //escape values
    private static final int ESCAPE_TAB = 't';
    private static final int ESCAPE_RETURN = 'r';
    private static final int ESCAPE_NEWLINE = 'n';
    private static final int ESCAPE_UNICODE16 = 'u';
    private static final int ESCAPE_UNICODE32 = 'U';

    private Regex() {}

    public static Chars[] split(Chars text, Chars exp) {
        return compile(exp).split(text);
    }

    public static Chars replace(Chars text, Chars exp, Chars replacement) {
        return compile(exp).replace(text,replacement);
    }

    /**
     * Compile regular expression written in standard syntax.
     *
     * @param exp regex expression
     * @return RegexExec
     */
    public static RegexExec compile(final Chars exp) {
        return compile(exp,Chars.EMPTY);
    }

    /**
     * Compile regular expression written in standard syntax.
     *
     * @param exp regex expression
     * @param matchName name of the nfa state result
     * @return RegexExec
     */
    public static RegexExec compile(final Chars exp, Chars matchName) {
        final NFAState state = toNFA(exp, matchName);
        return new RegexExec(state);
    }

    /**
     * Compile regular expression written in postfix syntax.
     *
     * @param exp regex expression
     * @return RegexExec
     */
    public static RegexExec compilePostfix(final Chars exp) {
        final NFAState state = postfixtoNFA(exp);
        return new RegexExec(state);
    }

    public static NFAState toNFA(final Chars exp) {
        return toNFA(exp, Chars.EMPTY);
    }

    public static NFAState toNFA(final Chars exp,final Chars matchName) {
        final CharIterator ite = exp.createIterator();
        final NFATemp last = toNFA(ite,false);
        last.setNext(new Match(matchName));
        return last.state;
    }

    public static NFATemp toNFA(final CharIterator ite, boolean skipGroupEnd) {

        final Stack stack = new ArrayStack();

        while (ite.hasNext()) {
            //special case for groups
            int c = ite.peekToUnicode();
            if (c==GROUP_END) {
                //end of group, return
                if (skipGroupEnd) {
                    ite.skip();
                }
                break;
            }

            c = ite.nextToUnicode();
            if (c==GROUP_START) {
                final NFATemp state = toNFA(ite,true);
                stack.add(state);
            } else if (c==WORD) {
                //add a serie of case sensitive char until word end
                stack.add(readWord(ite));
            } else if (c==ANY) {
                //add a char anything
                final AnyChar state = new AnyChar();
                stack.add(new NFATemp(state, state.next));
            } else if (c==INSWORD) {
                //add a serie of case insensitive char until word end
                stack.add(readInsWord(ite));
            } else if (c==CHOICE) {
                //parse what is on the right side of the choice
                final NFATemp left = (NFATemp) stack.pickEnd();
                final NFATemp right = toNFA(ite,false);
                final Fork state = new DefaultFork(left.state, right.state);
                final Sequence seq = new ArraySequence(left.nexts);
                seq.addAll(right.nexts);
                stack.add(new NFATemp(state, seq));
            } else if (c==RANGE_START) {
                int start = ite.nextToUnicode();
                boolean negate = false;
                if (start==NOT) {
                    negate=true;
                    start = ite.nextToUnicode();
                }
                if (start==ESCAPE) {
                    start = readEscape(ite);
                }

                int sep = ite.nextToUnicode();
                if (sep!=RANGE) {
                    //this is a list definition
                    final IntSequence ib = new IntSequence();
                    ib.put(start);

                    int wc = sep;
                    while (wc!=RANGE_END) {
                        if (wc==ESCAPE) wc = readEscape(ite);
                        ib.put(wc);
                        wc = ite.nextToUnicode();
                    }

                    final List state = new List(ib.toArrayInt(),negate);
                    stack.add(new NFATemp(state, state.next));
                } else {
                    //this is a range definition
                    int end = ite.nextToUnicode();
                    if (end==ESCAPE) {
                        end = readEscape(ite);
                    }
                    if (ite.nextToUnicode()!=RANGE_END) {
                        throw new RuntimeException("Invalid regex range definition");
                    }

                    final Range state = new Range(start,end,negate);
                    stack.add(new NFATemp(state, state.next));
                }

            } else {
                //single char
                if (c==ESCAPE) {
                    c = readEscape(ite);
                }
                final Char state = new Char(c);
                stack.add(new NFATemp(state, state.next));
            }

            //check for cardinality
            if (ite.hasNext()) {
                final int cp = ite.peekToUnicode();
                if (cp==ZERO_ONE) {
                    ite.skip();
                    final NFATemp temp = (NFATemp) stack.pickEnd();
                    final DefaultFork state = new DefaultFork(temp.state,null);
                    final Sequence seq = new ArraySequence(temp.nexts);
                    seq.add(state.getNext2Ref());
                    stack.add(new NFATemp(state, seq));
                } else if (cp==ZERO_MANY) {
                    ite.skip();
                    final NFATemp temp = (NFATemp) stack.pickEnd();
                    final DefaultFork state = new DefaultFork(temp.state,null);
                    temp.setNext(state);
                    stack.add(new NFATemp(state, state.getNext2Ref()));
                } else if (cp==ONE_MANY) {
                    ite.skip();
                    final NFATemp temp = (NFATemp) stack.pickEnd();
                    final DefaultFork state = new DefaultFork(temp.state,null);
                    temp.setNext(state);
                    stack.add(new NFATemp(temp.state, state.getNext2Ref()));
                }
            }

            //concat with previous in the stack
            if (stack.getSize()>1) {
                final NFATemp temp2 = (NFATemp) stack.pickEnd();
                final NFATemp temp1 = (NFATemp) stack.pickEnd();
                temp1.setNext(temp2.state);
                stack.add(new NFATemp(temp1.state, temp2.nexts));
            }

        }

        //the last temp in the stack contains the full graph
        return (NFATemp) stack.pickEnd();
    }

    public static NFAState postfixtoNFA(final Chars exp) {
        final CharIterator ite = exp.createIterator();

        final Stack stack = new ArrayStack();

        while (ite.hasNext()) {
            int c = ite.nextToUnicode();
            if (c==ESCAPE) {
                //escaped single char
                c = readEscape(ite);
                final Char state = new Char(c);
                stack.add(new NFATemp(state, state.next));
            } else if (c==POSTFIX_CONCAT) {
                final NFATemp temp2 = (NFATemp) stack.pickEnd();
                final NFATemp temp1 = (NFATemp) stack.pickEnd();
                temp1.setNext(temp2.state);
                stack.add(new NFATemp(temp1.state, temp2.nexts));
            } else if (c==CHOICE) {
                final NFATemp temp2 = (NFATemp) stack.pickEnd();
                final NFATemp temp1 = (NFATemp) stack.pickEnd();
                final Fork state = new DefaultFork(temp1.state, temp2.state);
                final Sequence seq = new ArraySequence(temp1.nexts);
                seq.addAll(temp2.nexts);
                stack.add(new NFATemp(state, seq));
            } else if (c==ZERO_ONE) {
                final NFATemp temp = (NFATemp) stack.pickEnd();
                final DefaultFork state = new DefaultFork(temp.state,null);
                final Sequence seq = new ArraySequence(temp.nexts);
                seq.add(state.getNext2Ref());
                stack.add(new NFATemp(state, seq));
            } else if (c==ZERO_MANY) {
                final NFATemp temp = (NFATemp) stack.pickEnd();
                final DefaultFork state = new DefaultFork(temp.state,null);
                temp.setNext(state);
                stack.add(new NFATemp(state, state.getNext2Ref()));
            } else if (c==ONE_MANY) {
                final NFATemp temp = (NFATemp) stack.pickEnd();
                final DefaultFork state = new DefaultFork(temp.state,null);
                temp.setNext(state);
                stack.add(new NFATemp(temp.state, state.getNext2Ref()));
            } else {
                //single char
                final Char state = new Char(c);
                stack.add(new NFATemp(state, state.next));
            }
        }

        //the last temp in the stack contains the full graph
        final NFATemp last = (NFATemp) stack.pickEnd();
        last.setNext(new Match());
        return last.state;
    }

    private static int readEscape(CharIterator ite) {
        final int c = ite.nextToUnicode();
        if (c==ESCAPE_TAB) {
            return '\t';
        } else if (c==ESCAPE_RETURN) {
            return '\r';
        } else if (c==ESCAPE_NEWLINE) {
            return '\n';
        } else if (c==ESCAPE_UNICODE16) {
            final Chars str = new Chars(new int[]{
                ite.nextToUnicode(),
                ite.nextToUnicode(),
                ite.nextToUnicode(),
                ite.nextToUnicode()
            });
            return Int32.decodeHexa(str);
        } else if (c==ESCAPE_UNICODE32) {
            final Chars str = new Chars(new int[]{
                ite.nextToUnicode(),
                ite.nextToUnicode(),
                ite.nextToUnicode(),
                ite.nextToUnicode(),
                ite.nextToUnicode(),
                ite.nextToUnicode(),
                ite.nextToUnicode(),
                ite.nextToUnicode()
            });
            return Int32.decodeHexa(str);
        } else {
            return c;
        }
    }

    private static NFATemp readWord(CharIterator ite) {
        final Stack stack = new ArrayStack();

        for (;;) {
            int c = ite.nextToUnicode();
            if (c==WORD) break;

            //single char
            if (c==ESCAPE) {
                c = readEscape(ite);
            }
            final Char state = new Char(c);
            stack.add(new NFATemp(state, state.next));

            //concat with previous in the stack
            if (stack.getSize()>1) {
                final NFATemp temp2 = (NFATemp) stack.pickEnd();
                final NFATemp temp1 = (NFATemp) stack.pickEnd();
                temp1.setNext(temp2.state);
                stack.add(new NFATemp(temp1.state, temp2.nexts));
            }
        }

        return (NFATemp) stack.pickEnd();
    }

    private static NFATemp readInsWord(CharIterator ite) {
        final Stack stack = new ArrayStack();

        for (;;) {
            int c = ite.nextToUnicode();
            if (c==INSWORD) break;

            //single char
            if (c==ESCAPE) {
                c = readEscape(ite);
            }
            final InsChar state = new InsChar(c);
            stack.add(new NFATemp(state, state.next));

            //concat with previous in the stack
            if (stack.getSize()>1) {
                final NFATemp temp2 = (NFATemp) stack.pickEnd();
                final NFATemp temp1 = (NFATemp) stack.pickEnd();
                temp1.setNext(temp2.state);
                stack.add(new NFATemp(temp1.state, temp2.nexts));
            }
        }

        return (NFATemp) stack.pickEnd();
    }

}
