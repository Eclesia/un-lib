
package science.unlicense.common.api;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public abstract class CObject implements Obj {

    /**
     * Calculate an hash value for this object.
     * @return int
     */
    public int getHash() {
        return 1;
    }

    /**
     * Get a character representation of this object.
     * @return Chars, never null.
     */
    public Chars toChars() {
        return new Chars("CO@"+this.getClass().getName()+":"+System.identityHashCode(this));
    }

    /**
     * Convinient JVM mapping.
     * Avoid using it, use toChars.
     */
    public final String toString() {
        return toChars().toJVMString();
    }

    /**
     * Convinient JVM mapping.
     * Avoid using it, use getHash.
     */
    public final int hashCode() {
        return getHash();
    }

}
