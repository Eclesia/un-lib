
package science.unlicense.common.api.model.doc;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public class DefaultDocumentType extends CObject implements DocumentType {

    private static final DocumentType[] EMPTY = new DocumentType[0];

    protected final Chars id;
    protected final CharArray title;
    protected final CharArray description;
    protected final boolean strict;
    protected final FieldType[] localFields;
    protected FieldType[] allFields;
    protected final DocumentType[] parentTypes;
    protected final Dictionary attributes = new HashDictionary(0);

    public DefaultDocumentType() {
        this(Chars.EMPTY,Chars.EMPTY,Chars.EMPTY,false,new FieldType[0],null);
    }

    public DefaultDocumentType(Chars id, CharArray title, CharArray description,
            boolean strict, FieldType[] fields, Dictionary attributes) {
        this(id, title, description, strict, fields, attributes, null);
    }

    public DefaultDocumentType(Chars id, CharArray title, CharArray description,
            boolean strict, FieldType[] fields, Dictionary attributes,DocumentType[] supers) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.strict = strict;
        this.localFields = fields;
        if (attributes!=null) {
            this.attributes.addAll(attributes);
        }
        parentTypes = supers==null ? EMPTY : supers;

        reindex();
    }

    public Chars getId() {
        return id;
    }

    public CharArray getTitle() {
        return title;
    }

    public CharArray getDescription() {
        return description;
    }

    public boolean isStrict() {
        return strict;
    }

    public FieldType[] getLocalFields() {
        return Arrays.copy(localFields, new FieldType[localFields.length]);
    }

    public FieldType[] getFields() {
        return Arrays.copy(allFields, new FieldType[allFields.length]);
    }

    public FieldType getField(Chars name) {
        for (int i=0;i<allFields.length;i++) {
            if (allFields[i].getId().equals(name)) {
                return allFields[i];
            }
        }
        return null;
    }

    public Dictionary getAttributes() {
        return attributes;
    }

    public DocumentType[] getSuper() {
        return Arrays.copy(parentTypes, new DocumentType[parentTypes.length]);
    }

    protected void reindex() {
        if (parentTypes.length==0) {
            allFields = localFields;
        } else {
            final Sequence all = new ArraySequence();
            for (int i=0;i<parentTypes.length;i++) {
                if (parentTypes[i]!=null) {
                    //TODO find a better solution : type may be null while we are in creation mode.
                    //see DBN format
                    all.addAll(parentTypes[i].getFields());
                }
            }
            all.addAll(localFields);
            allFields = (FieldType[]) all.toArray(FieldType.class);
        }
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        if (title!=null) cb.append(title);
        if (id!=null) cb.append("  (").append(id).append(')');
        return Nodes.toChars(cb.toChars(), allFields);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof DocumentType)) {
            return false;
        }
        final DocumentType other = (DocumentType) obj;
        if (this.id != other.getId() && (this.id == null || !this.id.equals(other.getId()))) {
            return false;
        }
        if (this.title != other.getTitle() && (this.title == null || !this.title.equals(other.getTitle()))) {
            return false;
        }
        if (this.description != other.getDescription() && (this.description == null || !this.description.equals(other.getDescription()))) {
            return false;
        }
        if (this.strict != other.isStrict()) {
            return false;
        }
        if (!Arrays.equals(this.localFields, other.getLocalFields())) {
            return false;
        }
        if (!Arrays.equals(this.parentTypes, other.getSuper())) {
            return false;
        }
        if (this.attributes != other.getAttributes() && (this.attributes == null || !this.attributes.equals(other.getAttributes()))) {
            return false;
        }
        return true;
    }

    public int getHash() {
        int hash = 3;
        hash = 67 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 67 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 67 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 67 * hash + (this.strict ? 1 : 0);
        hash = 67 * hash + Arrays.computeHash(this.allFields);
        hash = 67 * hash + (this.attributes != null ? this.attributes.hashCode() : 0);
        return hash;
    }

}
