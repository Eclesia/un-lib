
package science.unlicense.common.api.collection;

import science.unlicense.common.api.exception.UnmodifiableException;


/**
 * Optimized singleton collection.
 *
 * @author Johann Sorel
 */
final class SingletonCollection extends AbstractCollection {

    private final Object singleton;

    SingletonCollection(Object singleton) {
        this.singleton = singleton;
    }

    public boolean add(Object candidate) {
        throw new UnmodifiableException("Singleton collection, writing not supported");
    }

    public boolean remove(Object candidate) {
        throw new UnmodifiableException("Singleton collection, writing not supported");
    }

    public Iterator createIterator() {
        return Collections.singletonIterator(singleton);
    }

    public void replaceAll(Collection items) {
        throw new UnmodifiableException("Singleton collection, writing not supported");
    }

    public int getSize() {
        return 1;
    }

}
