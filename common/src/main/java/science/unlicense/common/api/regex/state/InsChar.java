
package science.unlicense.common.api.regex.state;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.character.Languages;

/**
 * Case-insensitive char.
 *
 * @author Johann Sorel
 */
public final class InsChar extends AbstractForward implements Evaluator {

    public final int cpLower;
    public final int cpUpper;

    public InsChar(int cp) {
        this.cpLower = Languages.UNSET.toLowerCase(cp);
        this.cpUpper = Languages.UNSET.toUpperCase(cp);
    }

    public boolean evaluate(Object cp) {
        final int i = (Integer) cp;
        return this.cpLower == i || this.cpUpper == i;
    }

    public Chars toChars() {
        return new Chars("InsChar[").concat(cpLower).concat(']');
    }

}
