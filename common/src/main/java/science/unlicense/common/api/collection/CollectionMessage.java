
package science.unlicense.common.api.collection;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.event.DefaultEventMessage;
import science.unlicense.common.api.event.MessagePredicate;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Collection event message.
 *
 * @author Johann Sorel
 */
public class CollectionMessage extends DefaultEventMessage{

    public static final Predicate PREDICATE = new MessagePredicate(CollectionMessage.class);

    public static final int TYPE_ADD = 1;
    public static final int TYPE_REMOVE = 2;
    public static final int TYPE_REPLACE = 3;
    public static final int TYPE_REPLACEALL = 4;

    private final int type;
    private final int startIndex;
    private final int endIndex;
    private final Object[] oldElements;
    private final Object[] newElements;

    public CollectionMessage(int type,
            int startIndex, int endIndex,
            Object[] oldElements, Object[] newElements) {
        super(false);
        this.type = type;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.oldElements = (oldElements==null) ? Arrays.ARRAY_OBJECT_EMPTY : oldElements;
        this.newElements = (newElements==null) ? Arrays.ARRAY_OBJECT_EMPTY : newElements;
    }

    /**
     * Collection event type.
     *
     * @return int, one of CollectionEvent.TYPE_X
     */
    public int getType() {
        return type;
    }

    /**
     * For sequence collections, get the start index of the event.
     *
     * @return int or -1 if not a sequence
     */
    public int getStartIndex() {
        return startIndex;
    }

    /**
     * For sequence collections, get the end index of the event.
     *
     * @return int or -1 if not a sequence
     */
    public int getEndIndex() {
        return endIndex;
    }

    /**
     * Get the old elements, removed or replaced.
     *
     * @return Object[] never null
     */
    public Object[] getOldElements() {
        return oldElements;
    }

    /**
     * Get the new elements, inserted or replacing.
     *
     * @return Object[] never null
     */
    public Object[] getNewElements() {
        return newElements;
    }

}
