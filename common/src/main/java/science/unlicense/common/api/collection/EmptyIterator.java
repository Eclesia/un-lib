
package science.unlicense.common.api.collection;

import science.unlicense.common.api.exception.MishandleException;
import science.unlicense.common.api.exception.UnmodifiableException;

/**
 * Empty iterator.
 *
 * @author Johann Sorel
 */
final class EmptyIterator implements Iterator {

    static final Iterator INSTANCE = new EmptyIterator();

    private EmptyIterator() {
    }

    public boolean hasNext() {
        return false;
    }

    public Object next() {
        throw new MishandleException("Empty iterator");
    }

    public boolean remove() {
        throw new UnmodifiableException("Not supported.");
    }

    public void close() {
    }

}
