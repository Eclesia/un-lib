
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface UnsafeBinaryFunction {

    void perform(Object in1, Object in2) throws Exception;

}
