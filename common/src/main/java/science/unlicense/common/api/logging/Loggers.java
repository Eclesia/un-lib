package science.unlicense.common.api.logging;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;

/**
 * Manage loggers.
 *
 * @author Johann Sorel
 */
public class Loggers {

    private static final LoggerFactory FACTORY = new DefaultLoggerFactory();

    public static final Chars DEFAULT = Chars.constant("default");
    private static final Dictionary LOGGERS = new HashDictionary();

    /**
     * Avoid instanciation.
     */
    private Loggers() {
    }

    /**
     * Get the default logger.
     * @return Logger, never null
     */
    public static Logger get() {
        return getLogger(DEFAULT);
    }

    /**
     * Get the logger associated with given label.
     * If no logger is associated with this label, a new one will be created.
     *
     * @param label logger label
     * @return Logger, never null
     */
    public static Logger getLogger(Chars label) {
        Logger logger = (Logger) LOGGERS.getValue(label);
        if (logger == null) {
            //TODO this should rely on the module registry to find factories
            //but it's a sub module, have to find a clean solution.
            logger = FACTORY.create();
        }
        return logger;
    }


}
