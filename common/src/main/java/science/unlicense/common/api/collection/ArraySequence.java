
package science.unlicense.common.api.collection;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.exception.MishandleException;

/**
 * Array sequence.
 *
 * @author Johann Sorel
 * @author Yann D'Isanto
 */
public class ArraySequence extends AbstractCollection implements Sequence{

    protected Object[] values;
    protected int size;

    public ArraySequence() {
        values = new Object[1];
        size = 0;
    }

    public ArraySequence(int size) {
        this.values = new Object[size];
        this.size = 0;
    }

    public ArraySequence(Collection other) {
        this(other.toArray());
    }

    public ArraySequence(Object[] array) {
        this.values = Arrays.copy(array);
        this.size = this.values.length;
    }

    protected void growIfNecessary(final int capacity) {
        if (capacity > values.length) {
            int newCapacity = (values.length * 3)/2 + 1;
            if (newCapacity < capacity) {
                newCapacity = capacity;
            }
            values = Arrays.resize(values, newCapacity);
        }
    }

    public Object get(int index) {
        if (index<0 || index>=size) throw new InvalidIndexException(""+index);
        return values[index];
    }

    public boolean add(Object candidate) {
        growIfNecessary(size + 1);
        values[size] = candidate;
        size++;
        if (hasListeners()) fireAdd(size-1, size-1, new Object[]{candidate});
        return true;
    }

    public boolean add(int index, Object value) {
        growIfNecessary(size + 1);
        for (int i=size-1;i>=index;i--) {
            values[i+1] = values[i];
        }
        values[index] = value;
        size++;
        if (hasListeners()) fireAdd(index, index, new Object[]{value});
        return true;
    }

    public boolean addAll(int index, Collection candidate) {
        return addAll(index,candidate.toArray());
    }

    public boolean addAll(int index, Object[] candidate) {
        growIfNecessary(size + candidate.length);
        for (int i=size-1;i>=index;i--) {
            values[i+candidate.length] = values[i];
        }
        Arrays.copy(candidate, 0, candidate.length, values, index);
        size+=candidate.length;
        if (hasListeners()) fireAdd(index, index+candidate.length, Arrays.copy(candidate));
        return true;
    }

    public Object replace(int index, Object item) {
        if (index < 0 || index >= size) {
            throw new InvalidIndexException(""+index);
        }
        Object oldValue = values[index];
        values[index] = item;
        if (hasListeners()) fireReplace(index, index, new Object[]{oldValue},new Object[]{item});
        return oldValue;
    }

    public void replaceAll(Collection items) {
        if (hasListeners()) {
            Object[] old = toArray();
            values = items.toArray();
            size = values.length;
            fireReplaceAll(0, old.length, old, Arrays.copy(values));
        } else {
            values = items.toArray();
            size = values.length;
        }
    }

    public void replaceAll(Object[] items) {
        if (hasListeners()) {
            Object[] old = toArray();
            if (Arrays.equals(old, items)) return;
            values = Arrays.copy(items);
            size = values.length;
            fireReplaceAll(0, old.length, old, Arrays.copy(values));
        } else {
            values = Arrays.copy(items);
            size = values.length;
        }
    }

    public boolean addAll(Collection candidate) {
        return addAll(size, candidate.toArray());
    }

    public boolean addAll(Object[] candidates) {
        return addAll(size, candidates);
    }

    public boolean remove(Object candidate) {
        for (int i=0;i<size;i++) {
            if (CObjects.equals(values[i],candidate)) {
                return remove(i);
            }
        }
        return false;
    }

    public boolean remove(int index) {
        if (index < 0 || index >= size) {
            throw new InvalidIndexException(""+index);
        }
        final Object removedValue = values[index];
        if (index+1 < size) {
            Arrays.copy(values, index+1, size-index-1, values, index);
        }
        size--;
        values[size] = null; //don't keep the reference
        if (hasListeners()) fireRemove(index, index, new Object[]{removedValue});
        return true;
    }

    public boolean removeAll(Collection candidate) {
        final Iterator ite = candidate.createIterator();
        boolean all = true;
        while (ite.hasNext()) {
            all = remove(ite.next()) && all;
        }
        ite.close();
        return all;
    }

    public boolean removeAll() {
        if (hasListeners()) {
            Object[] old = toArray();
            values = new Object[1];
            size = 0;
            fireRemove(0, old.length, old);
        } else {
            values = new Object[1];
            size = 0;
        }
        return true;
    }

    public boolean contains(Object candidate) {
        return Arrays.contains(values,0,size,candidate);
    }

    public int search(Object item) {
        return Arrays.getFirstOccurence(values, 0, size, item);
    }

    public int searchIdentity(Object item) {
        return Arrays.getFirstOccurenceIdentity(values, 0, size, item);
    }

    public Iterator createIterator() {
        return new ArrayIterator();
    }

    public Iterator createReverseIterator() {
        return new ReverseArrayIterator();
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Sequence)) {
            return false;
        }
        final Sequence other = (Sequence) obj;
        if (this.getSize() != other.getSize()) {
            return false;
        }
        for (int i=0;i<this.getSize();i++) {
            if (!get(i).equals(other.get(i))) {
                return false;
            }
        }
        return true;
    }

    public Object[] toArray() {
        return Arrays.copy(values,0,size);
    }

    private class ArrayIterator extends AbstractIterator{

        //keep a reference, avoids resizing issues in concurency.
        private final Object[] arrayvalues = ArraySequence.this.values;
        private final int size = ArraySequence.this.size;
        private int index = 0;
        private Object nextValue = null;

        public boolean hasNext() {
            if (nextValue==null) findNext();
            return nextValue!=null;
        }

        public Object next() {
            if (nextValue==null) findNext();
            if (nextValue==null) {
                throw new MishandleException("No more elements");
            }
            Object temp = nextValue;
            nextValue = null;
            return temp;
        }

        public boolean remove() {
            return ArraySequence.this.remove(index);
        }

        public void close() {
        }

        @Override
        protected void findNext() {
            Object[] safe = arrayvalues;
            if (arrayvalues.length<=index || size<=index) return;
            nextValue = safe[index];
            index++;
        }
    }

    private final class ReverseArrayIterator<A> implements Iterator {

        private int index = size - 1;

        @Override
        public boolean hasNext() {
            return size > 0 && index >= 0;
        }

        @Override
        public A next() {
            if (index < 0) {
                throw new MishandleException("No more element");
            }
            return (A) values[index--];
        }

        @Override
        public boolean remove() {
            return ArraySequence.this.remove(index);
        }

        @Override
        public void close() {
        }
    }

}
