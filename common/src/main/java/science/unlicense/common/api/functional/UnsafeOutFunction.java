
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface UnsafeOutFunction {

    Object perform() throws Exception;

}
