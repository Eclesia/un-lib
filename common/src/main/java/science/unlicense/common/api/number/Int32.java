
package science.unlicense.common.api.number;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.CharIterator;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.exception.ParseRuntimeException;
import science.unlicense.common.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public final class Int32 extends AbstractNumber {

    public static final NumberType TYPE = new NumberType() {

        @Override
        public Number create(Arithmetic value) {
            if (!(value instanceof Number)) throw new UnsupportedOperationException();
            return new Int32(((Number) value).toInteger());
        }

        @Override
        public Class getPrimitiveClass() {
            return int.class;
        }

        @Override
        public Class getValueClass() {
            return Int32.class;
        }

        @Override
        public int getSizeInBits() {
            return 32;
        }

        @Override
        public int getSizeInBytes() {
            return 4;
        }

        @Override
        public int getPrimitiveCode() {
            return Primitive.INT32;
        }
    };

    private final int value;

    public Int32(int value) {
        this.value = value;
    }

    @Override
    public NumberType getType() {
        return TYPE;
    }

    @Override
    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public int toInteger() {
        return value;
    }

    @Override
    public long toLong() {
        return value;
    }

    @Override
    public float toFloat() {
        return value;
    }

    @Override
    public double toDouble() {
        return value;
    }

    @Override
    public Chars toChars() {
        return encode(value);
    }

    @Override
    public int order(Object other) {
        int of = ((Number) other).toInteger();
        return value < of ? -1 : (value > of ? +1 : 0) ;
    }

    public static Chars encode(int candidate) {
        final ByteSequence s = new ByteSequence();

        final boolean negative = (candidate < 0);
        if (negative) {
            candidate *= -1;
        }

        int remainder;
        while (true) {
            remainder = candidate % 10;
            candidate = candidate / 10;
            s.put((byte) (remainder+48));
            if (candidate == 0) {
                break;
            }
        }

        byte[] array = s.toArrayByte();
        Arrays.reverse(array, 0, array.length);
        if (negative) {
            array = Arrays.copy(array, 0, array.length, new byte[array.length+1], 1);
            array[0] = 45;
        }
        return new Chars(array);
    }

    public static Chars encodeHexa(int candidate) {
        if (candidate == 0) {
            return new Chars(new byte[]{48});
        }
        final ByteSequence buffer = new ByteSequence();
        while (candidate > 0) {
            buffer.put(Numbers.HEXA[candidate % 16]);
            candidate = candidate / 16;
        }
        final byte[] bytes = buffer.toArrayByte();
        Arrays.reverse(bytes, 0, bytes.length);
        return new Chars(bytes);
    }

    public static Chars encodeHexa(byte[] array) {
        final CharBuffer cb = new CharBuffer();
        for (int i=0;i<array.length;i++) {
            //TODO inefficient, can do better
            cb.append(encodeHexa((array[i]&0xFF) >> 4));
            cb.append(encodeHexa(array[i]&0x0F));
        }
        return cb.toChars();
    }

    public static int decode(CharArray candidate) throws ParseRuntimeException {
        try{
            return decode(candidate.createIterator(),true);
        }catch(RuntimeException ex) {
            throw new ParseRuntimeException("Not a Number : "+candidate);
        }
    }

    /**
     *
     * @param candidate text de decode
     * @param fromIndex start offset
     * @param toIndex last character excluded,
     *          or use -1 to stop on sequence end or invalid character.
     * @return decoded value
     */
    public static int decode(CharArray candidate, int fromIndex, int toIndex) throws ParseRuntimeException {
        return decode(candidate.createIterator(fromIndex,toIndex),true);
    }

    public static int decode(CharIterator ite, boolean strict) throws ParseRuntimeException {
        if (!ite.hasNext()) {
            throw new ParseRuntimeException("Not an number");
        }
        int value = 0;
        int sign = 1;

        int c = ite.peekToUnicode();
        if (c == 43) { // +
            ite.skip();
        } else if (c == 45) { // -
            sign = -1;
            ite.skip();
        }

        while (ite.hasNext()) {
            c = ite.peekToUnicode();
            if (c < 48 || c > 57) {
                if (strict) {
                    throw new ParseRuntimeException("Not a number");
                } else {
                    break;
                }
            }
            value *= 10;
            value += c - 48;
            ite.skip();
        }

        value *= sign;
        return value;
    }

    /**
     * Read a fixed length unsigned integer.
     * @param ite iterator to decoder integer from
     * @param length integer length
     * @return unsigned integer
     */
    public static int decodeUnsigned(CharIterator ite, int length) throws ParseRuntimeException {
        int value = 0;
        int c;
        while (length>0) {
            c = ite.peekToUnicode();
            if (c < 48 || c > 57) {
                throw new ParseRuntimeException("Not a number");
            }
            value *= 10;
            value += c - 48;
            ite.skip();
            length--;
        }

        return value;
    }

    public static int decodeHexa(CharArray candidate) throws ParseRuntimeException {
        candidate = candidate.recode(CharEncodings.US_ASCII).toUpperCase();
        final CharIterator ite = candidate.createIterator();
        return decodeHexa(ite);
    }

    public static int decodeHexa(CharArray candidate, int fromIndex, int toIndex) throws ParseRuntimeException {
        candidate = candidate.recode(CharEncodings.US_ASCII).toUpperCase();
        final CharIterator ite = candidate.createIterator(fromIndex,toIndex);
        return decodeHexa(ite);
    }

    public static int decodeHexa(CharIterator ite) throws ParseRuntimeException {
        int value = 0;
        while (ite.hasNext()) {
            int b = ite.nextToBytes()[0] - 48;
            if (b>9) b-=7;
            value = 16*value + b;
        }
        return value;
    }

    public static int decodeHexa(CharIterator ite, int length) throws ParseRuntimeException {
        int value = 0;
        while (ite.hasNext() && length>0) {
            int b = ite.nextToBytes()[0] - 48;
            if (b>9) b-=7;
            value = 16*value + b;
            length--;
        }
        return value;
    }

    /**
     * Count the minimum number of bits needed to represent this value.
     *
     * @param value unsigned integer.
     * @return number of significant bits.
     */
    public static int getSignificantNumberOfNBits(int value) {
        for (long i=31;i>=0;i--) {
            if ( ((1l<<i) & value) != 0) {
                return (int) i+1;
            }
        }
        return 0;
    }

}
