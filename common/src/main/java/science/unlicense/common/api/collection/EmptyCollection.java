
package science.unlicense.common.api.collection;

import science.unlicense.common.api.exception.UnmodifiableException;

/**
 * Empty collection, implements Collection,Sequence and Set.
 *
 * @author Johann Sorel
 */
final class EmptyCollection extends AbstractCollection implements Sequence, Set {

    static final EmptyCollection INSTANCE = new EmptyCollection();

    private EmptyCollection() {
    }

    public boolean add(Object candidate) {
        throw new UnmodifiableException("Empty collection, writing not supported");
    }

    public boolean remove(Object candidate) {
        throw new UnmodifiableException("Empty collection, writing not supported");
    }

    public Iterator createIterator() {
        return EmptyIterator.INSTANCE;
    }

    public Object get(int index) {
        throw new UnmodifiableException("Empty collection");
    }

    public boolean add(int index, Object value) {
        throw new UnmodifiableException("Empty collection, writing not supported");
    }

    public boolean addAll(int index, Collection candidate) {
        throw new UnmodifiableException("Empty collection, writing not supported");
    }

    public boolean addAll(int index, Object[] candidate) {
        throw new UnmodifiableException("Empty collection, writing not supported");
    }

    public Object replace(int index, Object item) {
        throw new UnmodifiableException("Empty collection, writing not supported");
    }

    public void replaceAll(Collection items) {
        throw new UnmodifiableException("Empty collection, writing not supported");
    }

    public boolean remove(int index) {
        throw new UnmodifiableException("Empty collection, writing not supported");
    }

    public int search(Object item) {
        return -1;
    }

    public Iterator createReverseIterator() {
        return EmptyIterator.INSTANCE;
    }

    public int getSize() {
        return 0;
    }

}
