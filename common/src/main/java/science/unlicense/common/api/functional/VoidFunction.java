
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface VoidFunction extends UnsafeVoidFunction {

    void perform();

}
