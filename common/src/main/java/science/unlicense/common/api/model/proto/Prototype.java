
package science.unlicense.common.api.model.proto;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.event.Property;

/**
 * A prototype is a object with modifiable properties.
 * Java do not have prototype capabilities, this class only emulate this behavior
 * through methods.
 *
 * @author Johann Sorel
 */
public interface Prototype {

    /**
     * Get all property names.
     * This list changes when new properties are added or removed.
     * It is important to note that even if the name is listed the value
     * may still be null.
     *
     * @return
     */
    Set getPropertyNames();

    /**
     * Get property value.
     *
     * @param name property name
     * @return property value or null
     */
    Object getPropertyValue(Chars name);

    /**
     * Set property value, sends property event if value has changed.
     *
     * @param name property name
     * @param value, new value
     * @return true if value has changed
     */
    boolean setPropertyValue(Chars name, Object value);

    /**
     * Get Property object which can be sync to other values.
     *
     * @param name property name
     * @return property never null
     */
    Property getProperty(Chars name);

    /**
     * View this prototype properties as a read only dictionary.
     *
     * @return dictionary view.
     */
    Dictionary asDictionary();
}
