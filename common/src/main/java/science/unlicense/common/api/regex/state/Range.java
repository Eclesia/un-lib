
package science.unlicense.common.api.regex.state;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class Range extends AbstractForward implements Evaluator {

    public final int start;
    public final int end;
    public final boolean negate;

    public Range(int start, int end) {
        this(start, end, false);
    }

    public Range(int start, int end, boolean negate) {
        this.start = start;
        this.end = end;
        this.negate = negate;
    }

    public boolean evaluate(Object cp) {
        int icp = (Integer) cp;
        final boolean val = start <= icp && icp <= end;
        return negate ? !val : val;
    }

    public Chars toChars() {
        int nstr = negate ? '-' : '+';
        return new Chars("Range").concat(nstr).concat('[').concat(start).concat('-').concat(end).concat(']');
    }

}
