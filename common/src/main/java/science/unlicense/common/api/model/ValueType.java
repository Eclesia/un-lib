
package science.unlicense.common.api.model;

/**
 * There are 3 variantes of self defined structures.
 * Nodes, Parameters and Documents.
 *
 * ValueType is a common abstraction
 *
 * @author Johann Sorel
 */
public interface ValueType extends Presentable {

    Class getValueClass();

    Object getDefaultValue();

}
