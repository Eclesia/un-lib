
package science.unlicense.common.api.character;

import science.unlicense.common.api.country.Country;

/**
 * List of all supported Languages.
 *
 * @author Johann Sorel
 */
public final class Languages {

    private Languages() {}

    /**
     * Base family only providing general latin/occidental characters lower/upper case values.
     */
    public static final CharCase UNSET = new AbstractCharCase() {

        public boolean isUpperCase(int codepoint) {
            return !(codepoint>96 && codepoint<123);
        }

        public boolean isLowerCase(int unicode) {
            return !(unicode>64 && unicode<91);
        }

        public int toUpperCase(int codepoint) {
            if (codepoint>96 && codepoint<123) {
                return codepoint-32;
            }
            return codepoint;
        }

        public int toLowerCase(int codepoint) {
            if (codepoint>64 && codepoint<91) {
                return codepoint+32;
            }
            return codepoint;
        }

    };

}
