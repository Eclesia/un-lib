
package science.unlicense.common.api.number;

/**
 * A numeric type defines how a number is stored.
 * Common numeric types include, Integer, Float, Double.
 * Advanced mathematic types could be Frational, LongDouble, BigDecimal
 * And more pragmatic types like FixedDecimals.
 *
 * @author Johann Sorel
 */
public interface NumberType extends ArithmeticType {

    public static final NumberType TYPE_UNKNOWNED  = null;

    /**
     * Create a new number instance with it's initial value.
     *
     * @param value initial value
     * @return created number
     */
    @Override
    Number create(Arithmetic value);

    /**
     * Returns those most appropriate java primitive type.
     *
     * @return most appropriate java primitive type.
     */
    Class getPrimitiveClass();

    /**
     *
     * @return number of bits used to store this type
     */
    int getSizeInBits();

    /**
     *
     * @return number of bytes used to store this type
     */
    int getSizeInBytes();

}
