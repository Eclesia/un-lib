
package science.unlicense.common.api.model;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DefaultValueType implements ValueType {

    private final Chars id;
    private final CharArray title;
    private final CharArray description;
    private final Class valueClass;
    private final Object defaultValue;

    public DefaultValueType(Chars id, CharArray title, CharArray description, Class valueClass, Object defaultValue) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.valueClass = valueClass;
        this.defaultValue = defaultValue;
    }

    @Override
    public Class getValueClass() {
        return valueClass;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

    @Override
    public Chars getId() {
        return id;
    }

    @Override
    public CharArray getTitle() {
        return title;
    }

    @Override
    public CharArray getDescription() {
        return description;
    }

}
