
package science.unlicense.common.api.collection;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Event;

/**
 * Abstract dictionary.
 *
 * @author Johann Sorel
 */
public abstract class AbstractDictionary extends AbstractEventSource implements Dictionary {


    public void addAll(Dictionary index) {
        final Iterator ite = index.getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            add(pair.value1,pair.value2);
        }
    }

    public Class[] getEventClasses() {
        return new Class[]{
            CollectionMessage.class
        };
    }

     /**
     * Send a collection add event.
     *
     * @param startIndex first index of modified element, -1 if unknowed
     * @param endIndex last index of modified element, -1 if unknowed
     * @param news values added
     */
    protected void fireAdd(int startIndex, int endIndex, Object[] news) {
        if (hasListeners()) {
            getEventManager().sendEvent(new Event(this,new CollectionMessage(CollectionMessage.TYPE_ADD, startIndex, endIndex, null, news)));
        }
    }

    /**
     * Send a collection remove event.
     *
     * @param startIndex first index of modified element, -1 if unknowed
     * @param endIndex last index of modified element, -1 if unknowed
     * @param olds values removed
     */
    protected void fireRemove(int startIndex, int endIndex, Object[] olds) {
        if (hasListeners()) {
            getEventManager().sendEvent(new Event(this,new CollectionMessage(CollectionMessage.TYPE_REMOVE, startIndex, endIndex, olds, null)));
        }
    }

    /**
     * Send a collection replace event.
     *
     * @param startIndex first index of modified element, -1 if unknowed
     * @param endIndex last index of modified element, -1 if unknowed
     * @param olds values before replacement
     * @param news values after replacement
     */
    protected void fireReplace(int startIndex, int endIndex, Object[] olds, Object[] news) {
        if (hasListeners()) {
            getEventManager().sendEvent(new Event(this,new CollectionMessage(CollectionMessage.TYPE_REPLACE, startIndex, endIndex, olds, news)));
        }
    }

    public int getHash() {
        int hash = 3;
        return 798456;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Dictionary)) {
            return false;
        }
        final Dictionary other = (Dictionary) obj;
        if (getSize()!= other.getSize()) {
            return false;
        }

        //check pairs
        final Iterator ite = getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            Object val = pair.getValue2();
            Object valo = other.getValue(pair.value1);
            if (!CObjects.equals(val,valo)) return false;
        }

        return true;
    }

}
