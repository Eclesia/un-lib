
package science.unlicense.common.api.regex.state;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class List extends AbstractForward implements Evaluator {

    public final int[] values;
    public final boolean negate;

    public List(int[] values, boolean negate) {
        this.values = values;
        Arrays.sort(values);
        this.negate = negate;
    }

    public boolean evaluate(Object cp) {
        final boolean val = Arrays.contains(values, (Integer) cp);
        return negate ? !val : val;
    }

    public Chars toChars() {
        int nstr = negate ? '-' : '+';
        return new Chars("List").concat(nstr).concat(Arrays.toChars(values));
    }

}
