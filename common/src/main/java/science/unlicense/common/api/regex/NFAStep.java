
package science.unlicense.common.api.regex;

import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.common.api.CObject;

/**
 *
 * @author Johann Sorel
 */
public class NFAStep extends CObject {

    public NFAState state;
    public Object value;

    public NFAStep() {
    }

    public NFAStep(NFAState state) {
        this.state = state;
    }

    public NFAStep(NFAState state, Object value) {
        this.state = state;
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }


}
