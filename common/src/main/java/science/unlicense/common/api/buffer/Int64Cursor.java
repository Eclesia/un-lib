
package science.unlicense.common.api.buffer;

/**
 *
 * @author Johann Sorel
 */
public interface Int64Cursor extends Cursor {

    long getOffset();

    Int64Cursor offset(long position);

    long read();

    void read(long[] array);

    void read(long[] array, int arrayOffset, int length);

    Int64Cursor write(long value);

    Int64Cursor write(long[] array);

    Int64Cursor write(long[] array, int arrayOffset, int length);

}
