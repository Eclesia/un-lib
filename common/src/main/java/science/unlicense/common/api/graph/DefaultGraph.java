
package science.unlicense.common.api.graph;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Default graph implementation.
 *
 * @author Johann Sorel
 */
public class DefaultGraph extends AbstractGraph{

    private final Sequence vertices = new ArraySequence();
    private final Sequence edges = new ArraySequence();

    public Sequence getVertices() {
        return vertices;
    }

    public Sequence getEdges() {
        return edges;
    }

    public Vertex createVertex() {
        return new DefaultVertex(this);
    }

    public Edge createEdge(Vertex v1, Vertex v2) {
        return new DefaultEdge(v1, v2);
    }

    public void addVertex(Vertex vertex) {
        vertices.add(vertex);
    }

    public void addEdge(Edge edge) {
        edges.add(edge);
    }

    public void removeVertex(Vertex vertex) {
        vertices.remove(vertex);
    }

    public void removeEdge(Edge edge) {
        edges.remove(edge);
    }

    public Sequence getEdges(Vertex vertex) {
        return getEdges(new VertexPredicate(vertex));
    }

    public Sequence getEdges(Predicate filter) {
        return Collections.filter(edges, filter);
    }

    private static final class VertexPredicate implements Predicate {

        private final Vertex toFind;

        public VertexPredicate(Vertex toFind) {
            this.toFind = toFind;
        }

        public Boolean evaluate(Object candidate) {
            final Edge e = (Edge) candidate;
            return toFind.equals(e.getVertex1()) || toFind.equals(e.getVertex2());
        }
    };

}
