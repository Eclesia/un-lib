

package science.unlicense.common.api.predicate;

/**
 *
 * @author Johann Sorel
 */
public class ClassPredicate extends AbstractPredicate{

    private final Class clazz;

    public ClassPredicate(Class clazz) {
        this.clazz = clazz;
    }

    public Boolean evaluate(Object candidate) {
        return clazz.isInstance(candidate);
    }

    public int getHash() {
        int hash = 5;
        hash = 29 * hash + (this.clazz != null ? this.clazz.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClassPredicate other = (ClassPredicate) obj;
        if (this.clazz != other.clazz && (this.clazz == null || !this.clazz.equals(other.clazz))) {
            return false;
        }
        return true;
    }

}
