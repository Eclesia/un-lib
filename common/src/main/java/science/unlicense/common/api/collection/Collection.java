
package science.unlicense.common.api.collection;

import science.unlicense.common.api.event.EventSource;

/**
 * Groupement of elements.
 *
 * @author Johann Sorel
 */
public interface Collection extends EventSource{

    /**
     * Add the given element in the collection.
     *
     * @param candidate element to insert.
     * @return true if element successfully inserted.
     */
    boolean add(Object candidate);

    /**
     * Add all given elements in the collection.
     *
     * @param candidate elements to insert.
     * @return true if all elements successfully inserted.
     */
    boolean addAll(Collection candidate);

    /**
     * Add all given elements in the collection.
     *
     * @param candidate elements to insert.
     * @return true if all elements successfully inserted.
     */
    boolean addAll(Object[] candidate);

    /**
     * Remove the given element from the collection.
     *
     * @param candidate element to remove
     * @return true if element successfully removed.
     */
    boolean remove(Object candidate);

    /**
     * Remove the given elements from the collection.
     *
     * @param candidate elements to remove
     * @return true if all elements successfully removed.
     */
    boolean removeAll(Collection candidate);

    /**
     * Remove the given elements from the collection.
     *
     * @param candidate elements to remove
     * @return true if all elements successfully removed.
     */
    boolean removeAll(Object[] candidate);

    /**
     * Removes all elements in the collection.
     *
     * @return true if all elements successfully removed.
     */
    boolean removeAll();

    /**
     * Remove all items and set given ones.
     * This is similar to removeAll + addAll ath the difference
     * that the collection can optimize the insertions and will raise only one event.
     *
     * @param items
     */
    void replaceAll(Collection items);
    /**
     * Remove all items and set given ones.
     * This is similar to removeAll + addAll ath the difference
     * that the collection can optimize the insertions and will raise only one event.
     *
     * @param items
     */
    void replaceAll(Object[] items);


    /**
     * Check if an object is in this collection.
     *
     * @param candidate object to test
     * @return true if given object was found.
     */
    boolean contains(Object candidate);

    /**
     * Create a new iterator on this collection.
     *
     * @return Iterator new iterator
     */
    Iterator createIterator();

    /**
     * @return number of element in the collection
     */
    int getSize();

    /**
     * @return true if this collection is empty, false otherwise.
     */
    boolean isEmpty();

    /**
     * Copy all collection elements in an array.
     *
     * @return Object[] never null
     */
    Object[] toArray();

    /**
     * Copy all collection elements in an array.
     *
     * @param clazz
     * @return Object[] never null
     */
    Object[] toArray(Class clazz);

}
