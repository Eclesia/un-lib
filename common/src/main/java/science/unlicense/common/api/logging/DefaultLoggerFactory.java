
package science.unlicense.common.api.logging;

/**
 * Default logger factory.
 * Only provides output logs.
 *
 * TODO add file log support.
 *
 * @author Johann Sorel
 */
public class DefaultLoggerFactory implements LoggerFactory{

    public Logger create() {
        return new DefaultLogger();
    }

}
