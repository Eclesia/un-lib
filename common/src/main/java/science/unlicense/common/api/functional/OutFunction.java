
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface OutFunction extends UnsafeOutFunction {

    @Override
    Object perform();

}
