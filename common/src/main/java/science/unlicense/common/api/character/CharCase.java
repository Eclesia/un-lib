

package science.unlicense.common.api.character;

/**
 * A CharCase define the relation in a language between upper and lower case.
 *
 * @author Johann Sorel
 */
public interface CharCase {

    /**
     * Test if a character is uppercase.
     * For characters which have no upper/lower variant this method return true.
     *
     * @param unicode character unicode
     * @return true is given character is uppercase
     */
    boolean isUpperCase(int unicode);

    /**
     * Test if a character is uppercase.
     * For characters which have no upper/lower variant this method return true.
     *
     * @param c character to test
     * @return true is given character is uppercase
     */
    boolean isUpperCase(Char c);

    /**
     * Test if a character array is uppercase.
     *
     * @param sequence character sequence
     * @return true if all characters are uppercase
     */
    boolean isUpperCase(CharArray sequence);

    /**
     * Test if a character is lowercase.
     * For characters which have no upper/lower variant this method return true.
     *
     * @param unicode character unicode
     * @return true is given character is uppercase
     */
    boolean isLowerCase(int unicode);

    /**
     * Test if a character is lowercase.
     * For characters which have no upper/lower variant this method return true.
     *
     * @param c character
     * @return true is given character is uppercase
     */
    boolean isLowerCase(Char c);

    /**
     * Test if a character array is lowercase.
     *
     * @param array character sequence
     * @return true if all characters are uppercase
     */
    boolean isLowerCase(CharArray array);

    /**
     * Convert the character to upper case.
     *
     * @param codepoint unicode code point
     * @return character is uppercase
     */
    int toUpperCase(int codepoint);

    /**
     * Convert the character to upper case.
     *
     * @param c character
     * @return character is uppercase
     */
    Char toUpperCase(Char c);

    /**
     * Convert the character array to upper case.
     *
     * @param array character array
     * @return character is uppercase
     */
    Chars toUpperCase(CharArray array);

    /**
     * Convert the character to lower case.
     *
     * @param codepoint unicode code point
     * @return character is lowercase
     */
    int toLowerCase(int codepoint);

    /**
     * Convert the character to lower case.
     *
     * @param c character
     * @return character is lowercase
     */
    Char toLowerCase(Char c);

    /**
     * Convert the character array to lower case.
     *
     * @param array character array
     * @return characters is lowercase
     */
    Chars toLowerCase(CharArray array);

}
