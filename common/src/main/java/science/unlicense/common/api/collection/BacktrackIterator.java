
package science.unlicense.common.api.collection;

import science.unlicense.common.api.exception.MishandleException;
import science.unlicense.common.api.exception.UnmodifiableException;

/**
 * Backtrack iterator.
 *
 * @author Johann Sorel
 */
public class BacktrackIterator implements Iterator {

    private final Iterator in;
    private final Sequence buffer = new ArraySequence();
    private int mark = -1;

    public BacktrackIterator(Iterator base) {
        this.in = base;
    }

    public void mark() {
        if (mark>= buffer.getSize()) {
            buffer.removeAll();
        } else if (mark>0) {
            //current position might not be the end
            for (int i=0;i<mark;i++) {
                buffer.remove(0);
            }
        }
        mark = 0;
    }

    public void rewind() {
        mark = 0;
    }

    public boolean hasNext() {
        if (mark>=0 && mark<buffer.getSize()) {
            return true;
        }
        return in.hasNext();
    }

    public Object next() {
        if (mark>=0) {
            final Object b;
            if (mark<buffer.getSize()) {
                b = buffer.get(mark);
            } else if (!in.hasNext()) {
                throw new MishandleException("No more elements.");
            } else {
                b = in.next();
                buffer.add(b);
            }
            mark++;
            return b;
        }
        return in.next();
    }

    public boolean remove() {
        throw new UnmodifiableException("Not supported.");
    }

    public void close() {
        in.close();
    }
}
