
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Numbers;

/**
 *
 * @author Johann Sorel
 */
public class DefaultCursor extends AbstractDataCursor {

    final Buffer buffer;
    final long bufferSize;

    //current offset from start.
    long byteOffset = 0;

    // when reading bit per bit, keep track of remaining bits
    int bitOffset = 0;
    private int currentByte = -1;

    public DefaultCursor(Buffer buffer, Endianness encoding) {
        super(encoding);
        this.buffer = buffer;
        this.bufferSize = buffer.getByteSize();
    }

    @Override
    public Buffer getBuffer() {
        return buffer;
    }

    @Override
    public boolean hasNext() {
        return bufferSize > byteOffset;
    }

    @Override
    public long getRemaining() {
        return bufferSize - byteOffset;
    }

    @Override
    public int getBitOffset() {
        return bitOffset;
    }

    @Override
    public long getByteOffset() {
        return byteOffset;
    }

    @Override
    public void setBitOffset(int position) {
        this.bitOffset = position;
    }

    @Override
    public void setByteOffset(long position) {
        this.byteOffset = position;
        currentByte = buffer.readUInt8(byteOffset);
    }

    @Override
    public byte readByte() {
        if (bitOffset == 0) {
            byte b = buffer.readInt8(byteOffset);
            byteOffset++;
            return b;
        } else {
            return (byte) readBit(8);
        }
    }

    @Override
    public int readBit(int nbBits, int startOrder) {
        if (nbBits==8 && bitOffset==0) {
            int b = buffer.readUInt8(byteOffset);
            byteOffset++;
            return b;
        }
        if (nbBits < 0) {
            throw new InvalidArgumentException("Number of bits must be positive");
        }

        int value = 0;
        if (Buffer.LEFT_TO_RIGHT == startOrder) {
            for (int i=0;i<nbBits;i++) {
                value <<= 1;
                if (currentByte == -1) {
                    int next = buffer.readUInt8(byteOffset);
                    byteOffset++;
                    currentByte = next;
                }

                final int displacement = (7-bitOffset);
                final int mask = 1 << displacement;
                int t = currentByte & mask;
                value |= t >> displacement;
                bitOffset++;
                if (bitOffset == 8) {
                    currentByte = -1;
                    byteOffset++;
                    bitOffset = 0;
                }
            }
        } else if (Buffer.RIGHT_TO_LEFT == startOrder) {
            for (int i=0;i<nbBits;i++) {
                if (currentByte == -1) {
                    int next = buffer.readUInt8(byteOffset);
                    byteOffset++;
                    currentByte = next;
                }

                final int mask = 1 << bitOffset;
                int t = currentByte & mask;
                value |= (t >> bitOffset) << i;
                bitOffset++;
                if (bitOffset == 8) {
                    currentByte = -1;
                    bitOffset = 0;
                }
            }
        } else {
            throw new InvalidArgumentException("start order not supported, must be LEFT_TO_RIGHT or RIGHT_TO_LEFT : " + startOrder);
        }

        return value;
    }

    @Override
    public DataCursor writeByte(byte value) {
        if (bitOffset == 0) {
            buffer.writeInt8(value, byteOffset);
            byteOffset++;
        } else {
            writeBit(value, 8);
            byteOffset++;
        }
        return this;
    }

    @Override
    public DataCursor writeBit(int value, int nbBit) {
        currentByte = buffer.readInt8(byteOffset);
        for (int i=nbBit; i>0; i--) {

            int offset = (i-1);
            int mask = 1 << offset;
            int t = value & mask;
            t = t >> offset;
            currentByte = Numbers.setBit(currentByte, t>0, 7-bitOffset);

            buffer.writeInt8((byte) currentByte, byteOffset);

            bitOffset++;
            if (bitOffset == 8) {
                bitOffset = 0;
                byteOffset++;
                currentByte = byteOffset==bufferSize ? -1 : buffer.readInt8(byteOffset);
            }
        }
        return this;
    }

    @Override
    public void readByte(byte[] array) {
        if (bitOffset==0) {
            buffer.readInt8(array, byteOffset);
            byteOffset +=array.length;
        } else {
            super.readByte(array);
        }
    }

    @Override
    public void readByte(byte[] array, int arrayOffset, int length) {
        if (bitOffset==0) {
            buffer.readInt8(array, arrayOffset, length, byteOffset);
            byteOffset += length;
        } else {
            super.readByte(array);
        }
    }

}
