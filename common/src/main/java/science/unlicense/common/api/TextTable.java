
package science.unlicense.common.api;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int64;

/**
 * Produce nice table view for debugging.
 *
 * @author Johann sorel
 */
public class TextTable {

    private static final Chars CHARS_NULL = Chars.constant(new byte[]{'n','u','l','l'});
    private static final Chars CHARS_TRUE = Chars.constant(new byte[]{'t','r','u','e'});
    private static final Chars CHARS_FALSE = Chars.constant(new byte[]{'f','a','l','s','e'});

    private static final int TABLE_UL = '\u250C';
    private static final int TABLE_UR = '\u2510';
    private static final int TABLE_BL = '\u2514';
    private static final int TABLE_BR = '\u2518';


    private static final int TABLE_V = '\u2502';
    private static final int TABLE_VL = '\u2524';
    private static final int TABLE_VR = '\u251C';

    private static final int TABLE_H = '\u2500';
    private static final int TABLE_HU = '\u2534';
    private static final int TABLE_HB = '\u252C';

    private static final int TABLE_C = '\u253C';

    private final Sequence rows = new ArraySequence();
    private final Sequence widths = new ArraySequence();

    public void newLine() {
        rows.add(new ArraySequence());
    }

    public void appendCell(Chars val) {
        final Sequence s = (Sequence) rows.get(rows.getSize()-1);
        s.add(val);

        //update columns width
        final int length = val.getCharLength();

        if (widths.getSize()<s.getSize()) {
            widths.add(length);
        } else {
            final Integer cl = (Integer) widths.get(s.getSize()-1);
            if (cl<length) {
                widths.replace(s.getSize()-1, length);
            }
        }
    }

    public void reset() {
        rows.removeAll();
        widths.removeAll();
    }

    public Chars toChars() {
        final CharBuffer buffer = new CharBuffer(CharEncodings.UTF_8);
        final int nbCol = widths.getSize();
        final int nbRow = rows.getSize();

        //table top line
        buffer.append(TABLE_UL);
        for (int i=0;i<nbCol;i++) {
            if (i>0) buffer.append(TABLE_HB);
            final int width = (Integer) widths.get(i);
            for (int k=0;k<width;k++) {
                buffer.append(TABLE_H);
            }
        }
        buffer.append(TABLE_UR);
        buffer.append('\n');

        //each row
        for (int r=0;r<nbRow;r++) {
            if (r>0) {
                buffer.append(TABLE_VR);
                for (int i=0;i<nbCol;i++) {
                    if (i>0) buffer.append(TABLE_C);
                    final int width = (Integer) widths.get(i);
                    for (int k=0;k<width;k++) {
                        buffer.append(TABLE_H);
                    }
                }
                buffer.append(TABLE_VL);
                buffer.append('\n');
            }

            final Sequence values = (Sequence) rows.get(r);
            buffer.append(TABLE_V);
            for (int c=0;c<nbCol;c++) {
                if (c>0) buffer.append(TABLE_V);
                final int width = (Integer) widths.get(c);
                final Chars text = (values.getSize()>c) ? (Chars) values.get(c) : Chars.EMPTY;
                //fill with blanks to fit widths
                for (int k=0,n=(width-text.getCharLength()); k<n;k++) {
                    buffer.append(' ');
                }
                buffer.append(text);
            }
            buffer.append(TABLE_V);
            buffer.append('\n');
        }

        //table bottom line
        buffer.append(TABLE_BL);
        for (int i=0;i<nbCol;i++) {
            if (i>0) buffer.append(TABLE_HU);
            final int width = (Integer) widths.get(i);
            for (int k=0;k<width;k++) {
                buffer.append(TABLE_H);
            }
        }
        buffer.append(TABLE_BR);
        buffer.append('\n');

        return buffer.toChars();
    }


    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final boolean[][] array) {
        if (array == null) return CHARS_NULL;

        final TextTable buffer = new TextTable();
        for (int i=0;i<array.length;i++) {
            buffer.newLine();
            for (int j=0;j<array[i].length;j++) {
                buffer.appendCell(array[i][j]?CHARS_TRUE:CHARS_FALSE);
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final byte[][] array) {
        if (array == null) return CHARS_NULL;

        final TextTable buffer = new TextTable();
        for (int i=0;i<array.length;i++) {
            buffer.newLine();
            for (int j=0;j<array[i].length;j++) {
                buffer.appendCell(Int32.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final short[][] array) {
        if (array == null) return CHARS_NULL;

        final TextTable buffer = new TextTable();
        for (int i=0;i<array.length;i++) {
            buffer.newLine();
            for (int j=0;j<array[i].length;j++) {
                buffer.appendCell(Int32.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final int[][] array) {
        if (array == null) return CHARS_NULL;

        final TextTable buffer = new TextTable();
        for (int i=0;i<array.length;i++) {
            buffer.newLine();
            for (int j=0;j<array[i].length;j++) {
                buffer.appendCell(Int32.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final long[][] array) {
        if (array == null) return CHARS_NULL;

        final TextTable buffer = new TextTable();
        for (int i=0;i<array.length;i++) {
            buffer.newLine();
            for (int j=0;j<array[i].length;j++) {
                buffer.appendCell(Int64.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final float[][] array) {
        if (array == null) return CHARS_NULL;

        final TextTable buffer = new TextTable();
        for (int i=0;i<array.length;i++) {
            buffer.newLine();
            for (int j=0;j<array[i].length;j++) {
                buffer.appendCell(Float64.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final double[][] array) {
        if (array == null) return CHARS_NULL;

        final TextTable buffer = new TextTable();
        for (int i=0;i<array.length;i++) {
            buffer.newLine();
            for (int j=0;j<array[i].length;j++) {
                buffer.appendCell(Float64.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final Obj[][] array) {
        if (array == null) return CHARS_NULL;

        final TextTable buffer = new TextTable();
        for (int i=0;i<array.length;i++) {
            buffer.newLine();
            for (int j=0;j<array[i].length;j++) {
                buffer.appendCell(CObjects.toChars(array[i][j]));
            }
        }
        return buffer.toChars();
    }

}
