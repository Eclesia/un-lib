package science.unlicense.common.api.character;

import java.util.Objects;
import science.unlicense.common.api.collection.primitive.ByteSequence;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractCharEncoding implements CharEncoding {

    protected final Chars name;
    protected final boolean isFixedSize;
    protected final int maxNbByte;

    public AbstractCharEncoding(byte[] bname, boolean isFixedSize, int maxNbByte) {
        this.name = new Chars(bname);
        this.isFixedSize = isFixedSize;
        this.maxNbByte = maxNbByte;
    }

    public Chars getName() {
        return name;
    }

    public int length(byte[] array) {
        int l = 0;
        int boffset = 0;
        while (boffset < array.length) {
            boffset += charlength(array, boffset);
            l++;
        }
        return l;
    }

    public int toUnicode(byte[] array) {
        final int[] buffer = new int[2];
        toUnicode(array, buffer, 0);
        return buffer[0];
    }

    public byte[] toBytes(int[] codePoints) {
        final ByteSequence buffer = new ByteSequence();
        for (int i=0;i<codePoints.length;i++) {
            buffer.put(toBytes(codePoints[i]));
        }
        return buffer.toArrayByte();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof CharEncoding)) {
            return false;
        }
        final CharEncoding other = (CharEncoding) obj;
        if (!Objects.equals(this.name, other.getName())) {
            return false;
        }
        return true;
    }

}
