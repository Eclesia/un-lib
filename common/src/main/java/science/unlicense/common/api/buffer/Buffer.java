
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;

/**
 * Buffers are a way to store large number of primitive datas.
 * You should see a Buffer as a memory space of bytes which you can
 * explore, read and write freely.
 * Buffers can be more efficient for a give primitive type or may be more
 * optimized for reading or writing.
 * The most efficient primitive type is given by method : getPrimitiveType
 *
 * Different BufferFactory produce more efficient or dedicated buffers for
 * some specific use cases : OpenGL, GPU, WebGL, Sockets, ...
 *
 * @author Johann Sorel
 */
public interface Buffer {

    /** Indicate to read bit starting from the left most bit */
    public static final int LEFT_TO_RIGHT = 0;
    /** Indicate to read bit starting from the right most bit */
    public static final int RIGHT_TO_LEFT = 1;

    /**
     * Get the buffer factory which created this buffer.
     *
     * @return BufferFactory, can be null
     */
    BufferFactory getFactory();

    /**
     * Get default primitive type.
     * The buffer should be optimized for this type.
     *
     * @return one of science.unlicense.api.number.NumericType.*
     */
    NumberType getNumericType();

    /**
     * Get default encoding.
     * The buffer should be optimized for this encoding.
     *
     * @return primitive encoding
     */
    Endianness getEndianness();

    /**
     * Get the number of value stored in the buffer.
     *
     * @return number of values in buffer
     */
    long getNumbersSize();

    /**
     * @return size of the buffer in bytes.
     */
    long getByteSize();

    /**
     *
     * @return true if writing is supported
     */
    boolean isWritable();

    /**
     * Obtain a copy of this buffer.
     *
     * @return Buffer
     */
    Buffer copy();

    /**
     * Obtain a copy of this buffer.
     * Changing Endianness doing so.
     *
     * @return Buffer
     */
    Buffer copy(Endianness encoding);

    /**
     * Obtain a copy of this buffer with a different primitive type and encoding.
     * The returned buffer is not linked to the parent buffer.
     * The new copy buffer should be optimized.
     *
     * @param primitive
     * @param encoding
     * @return Buffer
     */
    Buffer copy(NumberType primitive, Endianness encoding);

    /**
     * Obtain a copy of this buffer with a different primitive type and encoding.
     * The returned buffer is not linked to the parent buffer.
     * The new copy buffer should be optimized.
     *
     * @param factory can be null for same factory
     * @param primitive can be null for same primitive
     * @param encoding can be null for same encoding
     * @return Buffer
     */
    Buffer copy(BufferFactory factory, NumberType primitive, Endianness encoding);

    Buffer.Int8 asInt8();

    Buffer.Int16 asInt16();

    Buffer.Int32 asInt32();

    Buffer.Int64 asInt64();

    Buffer.Float32 asFloat32();

    Buffer.Float64 asFloat64();

    /**
     * Obtain a view of this buffer with the same primitive type and encoding.
     * This is a view, modifications made to one will change the parent buffer.
     * The view do not share the same cursor.
     *
     * @return Cursor
     */
    DataCursor dataCursor();

    /**
     * Obtain a view of this buffer with a different primitive type and encoding.
     * This is a view, modifications made to one will change the parent buffer.
     * The view buffer may not be optimized.
     * The view do not share the same cursor.
     *
     * @param primitive
     * @param encoding
     * @return Buffer
     */
    DataCursor dataCursor(NumberType primitive, Endianness encoding);


    ////////////////////////////////////////////////////////////////////////////

    /**
     * Backend object of this store.
     * @return backend object, often an array, can be null
     */
    Object getBackEnd();

    /**
     * Convert the content of the buffer to a byte array.
     *
     * @return byte array
     */
    byte[] toByteArray();

    /**
     * Convert the content of the buffer to an unsigned byte array.
     *
     * @return unsigned byte array
     */
    int[] toUByteArray();

    /**
     * Convert the content of the buffer to a short array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     *
     * @return short array
     */
    short[] toShortArray();

    /**
     * Convert the content of the buffer to a unsigned short array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     *
     * @return unsigned short array
     */
    int[] toUShortArray();

    /**
     * Convert the content of the buffer to a 24bits int array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     *
     * @return 24bits int array
     */
    int[] toInt24Array();

    /**
     * Convert the content of the buffer to a unsigned 24bits int array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     *
     * @return unsigned 24bits int array
     */
    int[] toUInt24Array();

    /**
     * Convert the content of the buffer to a int array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     *
     * @return int array
     */
    int[] toIntArray();

    /**
     * Convert the content of the buffer to a unsigned int array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     *
     * @return unsigned int array
     */
    long[] toUIntArray();

    /**
     * Convert the content of the buffer to a long array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     *
     * @return long array
     */
    long[] toLongArray();

    /**
     * Convert the content of the buffer to a float array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     *
     * @return float array
     */
    float[] toFloatArray();

    /**
     * Convert the content of the buffer to a double array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     *
     * @return double array
     */
    double[] toDoubleArray();

    ////////////////////////////////////////////////////////////////////////////

    byte readInt8(long offset);
    void readInt8(byte[] array, long offset);
    void readInt8(byte[] array, int arrayOffset, int length, long offset);

    int readUInt8(long offset);
    void readUInt8(int[] array, long offset);
    void readUInt8(int[] array, int arrayOffset, int length, long offset);

    short readInt16(long offset);
    void readInt16(short[] array, long offset);
    void readInt16(short[] array, int arrayOffset, int length, long offset);

    int readUInt16(long offset);
    void readUInt16(int[] array, long offset);
    void readUInt16(int[] array, int arrayOffset, int length, long offset);

    int readInt24(long offset);
    void readInt24(int[] array, long offset);
    void readInt24(int[] array, int arrayOffset, int length, long offset);

    int readUInt24(long offset);
    void readUInt24(int[] array, long offset);
    void readUInt24(int[] array, int arrayOffset, int length, long offset);

    int readInt32(long offset);
    void readInt32(int[] array, long offset);
    void readInt32(int[] array, int arrayOffset, int length, long offset);

    long readUInt32(long offset);
    void readUInt32(long[] array, long offset);
    void readUInt32(long[] array, int arrayOffset, int length, long offset);

    long readInt64(long offset);
    void readInt64(long[] array, long offset);
    void readInt64(long[] array, int arrayOffset, int length, long offset);

    float readFloat32(long offset);
    void readFloat32(float[] array, long offset);
    void readFloat32(float[] array, int arrayOffset, int length, long offset);

    double readFloat64(long offset);
    void readFloat64(double[] array, long offset);
    void readFloat64(double[] array, int arrayOffset, int length, long offset);

    ////////////////////////////////////////////////////////////////////////////

    void writeInt8(byte value, long offset);
    void writeInt8(byte[] array, long offset);
    void writeInt8(byte[] array, int arrayOffset, int length, long offset);

    void writeUInt8(int value, long offset);
    void writeUInt8(int[] array, long offset);
    void writeUInt8(int[] array, int arrayOffset, int length, long offset);

    void writeInt16(short value, long offset);
    void writeInt16(short[] array, long offset);
    void writeInt16(short[] array, int arrayOffset, int length, long offset);

    void writeUInt16(int value, long offset);
    void writeUInt16(int[] array, long offset);
    void writeUInt16(int[] array, int arrayOffset, int length, long offset);

    void writeInt24(int value, long offset);
    void writeInt24(int[] array, long offset);
    void writeInt24(int[] array, int arrayOffset, int length, long offset);

    void writeUInt24(int value, long offset);
    void writeUInt24(int[] array, long offset);
    void writeUInt24(int[] array, int arrayOffset, int length, long offset);

    void writeInt32(int value, long offset);
    void writeInt32(int[] array, long offset);
    void writeInt32(int[] array, int arrayOffset, int length, long offset);

    void writeUInt32(long value, long offset);
    void writeUInt32(long[] array, long offset);
    void writeUInt32(long[] array, int arrayOffset, int length, long offset);

    void writeInt64(long value, long offset);
    void writeInt64(long[] array, long offset);
    void writeInt64(long[] array, int arrayOffset, int length, long offset);

    void writeFloat32(float value, long offset);
    void writeFloat32(float[] array, long offset);
    void writeFloat32(float[] array, int arrayOffset, int length, long offset);

    void writeFloat64(double value, long offset);
    void writeFloat64(double[] array, long offset);
    void writeFloat64(double[] array, int arrayOffset, int length, long offset);

    /**
     * Release buffer resources.
     */
    void dispose();

    public interface View {

        Buffer getBuffer();

        /**
         * Get the number of value stored in the buffer.
         *
         * @return number of values in buffer
         */
        long getSize();

        Cursor cursor();

    }

    public interface Int8 extends View {

        byte read(long position);

        void read(long position, byte[] array);

        void read(long position, byte[] array, int arrayOffset, int length);

        void write(long position, byte value);

        void write(long position, byte[] array);

        void write(long position, byte[] array, int arrayOffset, int length);

        @Override
        Int8Cursor cursor();

    }

    public interface Int16 extends View {

        short read(long position);

        void read(long position, short[] array);

        void read(long position, short[] array, int arrayOffset, int length);

        void write(long position, short value);

        void write(long position, short[] array);

        void write(long position, short[] array, int arrayOffset, int length);

        @Override
        Int16Cursor cursor();

    }

    public interface Int32 extends View {

        int read(long position);

        void read(long position, int[] array);

        void read(long position, int[] array, int arrayOffset, int length);

        void write(long position, int value);

        void write(long position, int[] array);

        void write(long position, int[] array, int arrayOffset, int length);

        @Override
        Int32Cursor cursor();

        int[] toIntArray();
    }

    public interface Int64 extends View {

        long read(long position);

        void read(long position, long[] array);

        void read(long position, long[] array, int arrayOffset, int length);

        void write(long position, long value);

        void write(long position, long[] array);

        void write(long position, long[] array, int arrayOffset, int length);

        @Override
        Int64Cursor cursor();

        long[] toLongArray();
    }

    public interface Float32 extends View {

        float read(long position);

        void read(long position, float[] array);

        void read(long position, float[] array, int arrayOffset, int length);

        void write(long position, float value);

        void write(long position, float[] array);

        void write(long position, float[] array, int arrayOffset, int length);

        @Override
        Float32Cursor cursor();

        float[] toFloatArray();

    }

    public interface Float64 extends View {

        double read(long position);

        void read(long position, double[] array);

        void read(long position, double[] array, int arrayOffset, int length);

        void write(long position, double value);

        void write(long position, double[] array);

        void write(long position, double[] array, int arrayOffset, int length);

        @Override
        Float64Cursor cursor();

        double[] toDoubleArray();
    }
}
