
package science.unlicense.common.api.number;

/**
 * An arithmetic type defines a value class which can support the different arithmetic operations.
 *
 * @author Johann Sorel
 */
public interface ArithmeticType {

    /**
     * Create a new number instance with it's initial value.
     *
     * @param value initial value
     * @return created number
     */
    Arithmetic create(Arithmetic value);

    /**
     * Returns the Numeric class this type represent.
     *
     * @return Class of type Number
     */
    Class getValueClass();

    /**
     * @return Code from Primitive.*
     */
    int getPrimitiveCode();
}
