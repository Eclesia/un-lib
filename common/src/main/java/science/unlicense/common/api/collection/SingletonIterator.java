
package science.unlicense.common.api.collection;

import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.exception.MishandleException;
import science.unlicense.common.api.exception.UnmodifiableException;

/**
 *  Optimized singleton iterator.
 *
 * @author Johann Sorel
 */
final class SingletonIterator implements Iterator {

    private Object obj;

    SingletonIterator(Object obj) {
        this.obj = obj;
    }

    public boolean hasNext() {
        return obj!=null;
    }

    public Object next() {
        if (obj==null) {
            throw new MishandleException("No more elements");
        }
        Object c = obj;
        obj = null;
        return c;
    }

    public boolean remove() {
        throw new UnmodifiableException("Not supported.");
    }

    public void close() {
    }

}
