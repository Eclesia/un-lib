
package science.unlicense.common.api.regex.state;

import science.unlicense.common.api.character.Chars;

/**
 * Marker state to indicate the successful end.
 *
 * @author Johann Sorel
 */
public final class Match extends AbstractForward {

    public final Chars name;

    public Match() {
        this.name = Chars.EMPTY;
    }

    public Match(Chars name) {
        this.name = name;
    }

    public Chars toChars() {
        return new Chars("MATCH(").concat(name).concat(')');
    }

}
