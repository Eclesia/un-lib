
package science.unlicense.common.api.graph;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.predicate.Predicate;

/**
 * A Graph is a finite number of vertices.
 * TODO : some more informations could be calculated :
 * - isBipartite
 * - isConnected (isolated/connexe node groups)
 * - cut-vertex (nodes when remove which raise number of connexe node groups)
 * - cut-edge (edges when remove which raise number of connexe node groups)
 * - Kuratowski theorem to find if graph is planar
 * - chromatic number
 *
 * @author Johann Sorel
 */
public interface Graph {

    /**
     * Get all vertices attached to this graph.
     * @return Sequence all vertices, never null
     */
    Sequence getVertices();

    /**
     * Get all edges attached to this graph.
     * @return Sequence all edges, never null
     */
    Sequence getEdges();

    /**
     * Create a new vertex, but does not add it in the graph.
     * @return Vertex created vertex
     */
    Vertex createVertex();

    /**
     * Create a new edge, but does not add it in the graph.
     * @param v1 first edge vertex
     * @param v2 second edge vertex
     * @return Edge created edge
     */
    Edge createEdge(Vertex v1, Vertex v2);

    /**
     * Add given vertex in the graph.
     * @param vertex vertex to add in the graph
     */
    void addVertex(Vertex vertex);

    /**
     * Add given edge in the graph.
     * @param edge edge to add in the graph
     */
    void addEdge(Edge edge);

    /**
     * Remove given vertex from the graph.
     * @param vertex vertex to remove from the graph
     */
    void removeVertex(Vertex vertex);

    /**
     * Remove given edge from the graph.
     * @param edge edge to remove from the graph
     */
    void removeEdge(Edge edge);

    ////////////////////////////////////////////////////////////////////////////
    // Graph query methods /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    Sequence getEdges(Vertex vertex);

    Sequence getEdges(Predicate filter);

    ////////////////////////////////////////////////////////////////////////////
    // Graph information methods ///////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * A Graph is simple when it does not contain parallale or loop edges.
     * @return true if graph is simple
     */
    boolean isSimpleGraph();

    /**
     * A Graph is a multi graph if it contains parallale edges.
     * @return true if graph is simple
     */
    boolean isMultiGraph();

    /**
     * A Graph is a pseudo graph if it contains parallale or loop edges.
     * @return true if graph is a pseudograph
     */
    boolean isPseudoGraph();

    /**
     * A Graph is complete if all vertices are linked to all others vertices.
     * @return true if graph is complete
     */
    boolean isComplete();

}
