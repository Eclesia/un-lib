
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class DefaultInt8Buffer extends AbstractBuffer {

    private final byte[] buffer;

    public DefaultInt8Buffer(byte[] buffer) {
        this(buffer,science.unlicense.common.api.number.Int8.TYPE,Endianness.BIG_ENDIAN);
    }

    public DefaultInt8Buffer(byte[] buffer, NumberType primitiveType, Endianness encoding) {
        super(primitiveType, encoding, DefaultBufferFactory.INSTANCE);
        this.buffer = buffer;
    }

    public DefaultInt8Buffer(int nbValue, NumberType primitiveType, Endianness encoding) {
        super(primitiveType, encoding, DefaultBufferFactory.INSTANCE);
        final double size = primitiveType.getSizeInBytes()*nbValue;
        this.buffer = new byte[(int) Math.ceil(size)];
    }

    @Override
    public Object getBackEnd() {
        return buffer;
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public byte[] toByteArray() {
        return Arrays.copy(buffer);
    }

    @Override
    public long getByteSize() {
        return buffer.length;
    }

    @Override
    public byte readInt8(long offset) {
        return buffer[(int) offset];
    }

    @Override
    public int readUInt8(long offset) {
        return buffer[(int) offset] & 0xFF;
    }

    @Override
    public short readInt16(long offset) {
        return endianness.readShort(buffer, (int) offset);
    }

    @Override
    public int readUInt16(long offset) {
        return endianness.readUShort(buffer, (int) offset);
    }

    @Override
    public int readInt24(long offset) {
        return endianness.readInt24(buffer, (int) offset);
    }

    @Override
    public int readUInt24(long offset) {
        return endianness.readUInt24(buffer, (int) offset);
    }

    @Override
    public int readInt32(long offset) {
        return endianness.readInt(buffer, (int) offset);
    }

    @Override
    public long readUInt32(long offset) {
        return endianness.readUInt(buffer, (int) offset);
    }

    @Override
    public long readInt64(long offset) {
        return endianness.readLong(buffer, (int) offset);
    }

    @Override
    public float readFloat32(long offset) {
        return endianness.readFloat(buffer, (int) offset);
    }

    @Override
    public double readFloat64(long offset) {
        return endianness.readDouble(buffer, (int) offset);
    }

    @Override
    public void writeInt8(byte value, long offset) {
        buffer[(int) offset] = value;
    }

    @Override
    public void writeInt8(byte[] array, int arrayOffset, int length, long offset) {
        Arrays.copy(array, arrayOffset, length, buffer, (int) offset);
    }

    @Override
    public void writeUInt8(int value, long offset) {
        buffer[(int) offset] = (byte) value;
    }

    @Override
    public void writeInt16(short value, long offset) {
        endianness.writeShort(value, buffer, (int) offset);
    }

    @Override
    public void writeUInt16(int value, long offset) {
        endianness.writeUShort(value, buffer, (int) offset);
    }

    @Override
    public void writeInt24(int value, long offset) {
        endianness.writeInt24(value, buffer, (int) offset);
    }

    @Override
    public void writeUInt24(int value, long offset) {
        endianness.writeUInt24(value, buffer, (int) offset);
    }

    @Override
    public void writeInt32(int value, long offset) {
        endianness.writeInt(value, buffer, (int) offset);
    }

    @Override
    public void writeUInt32(long value, long offset) {
        endianness.writeUInt(value, buffer, (int) offset);
    }

    @Override
    public void writeInt64(long value, long offset) {
        endianness.writeLong(value, buffer, (int) offset);
    }

    @Override
    public void writeFloat32(float value, long offset) {
        endianness.writeFloat(value, buffer, (int) offset);
    }

    @Override
    public void writeFloat64(double value, long offset) {
        endianness.writeDouble(value, buffer, (int) offset);
    }

    @Override
    public Buffer copy(BufferFactory factory, NumberType primitive, Endianness encoding) {
        if (  (factory == null || this.factory == factory)
           && (primitive == null || this.numericType == primitive)
           && (encoding == null || this.endianness == encoding)) {
            return new DefaultInt8Buffer(Arrays.copy(buffer));
        }
        return super.copy(factory, primitive, encoding);
    }
}
