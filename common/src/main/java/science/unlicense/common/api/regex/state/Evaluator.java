
package science.unlicense.common.api.regex.state;

/**
 *
 * @author Johann Sorel
 */
public interface Evaluator extends Forward {

    boolean evaluate(Object cp);

}
