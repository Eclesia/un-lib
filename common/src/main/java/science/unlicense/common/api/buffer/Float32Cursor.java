
package science.unlicense.common.api.buffer;

/**
 *
 * @author Johann Sorel
 */
public interface Float32Cursor extends Cursor {

    long getOffset();

    Float32Cursor offset(long position);

    float read();

    void read(float[] array);

    void read(float[] array, int arrayOffset, int length);

    Float32Cursor write(float value);

    Float32Cursor write(float[] array);

    Float32Cursor write(float[] array, int arrayOffset, int length);

}
