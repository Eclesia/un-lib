
package science.unlicense.common.api.event;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.ValueType;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.predicate.Variable;

/**
 * Convinient class to manipulate a single property of a bean.
 *
 * @author Johann Sorel
 */
public interface Property extends Variable {

    /**
     * Get property name.
     *
     * @return name, never null
     */
    Chars getName();

    /**
     * Get property type.
     *
     * @return property type
     */
    ValueType getType();

    /**
     * Get a event predicate which can be used to match this property.
     * The event message must be PropertyMessage with this property name.
     *
     * @return PropertyPredicate
     */
    Predicate asEventPredicate();
}
