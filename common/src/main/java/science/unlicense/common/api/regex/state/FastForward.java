
package science.unlicense.common.api.regex.state;

/**
 * Special kind of token which are not processed, they are usually subclassed
 * to store informations in the nfa path.
 *
 * @author Johann Sorel
 */
public interface FastForward extends Forward {}
