
package science.unlicense.common.api.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.DefaultNamedNode;
import science.unlicense.common.api.model.tree.Node;

/**
 * Aggregate multiple metadata as a single node.
 *
 * @author Johann Sorel
 */
public class DefaultMetadataGroup implements Metadata{

    private final Sequence subMetas = new ArraySequence();
    private final Chars name;

    public DefaultMetadataGroup(Chars name, Sequence metadatas) {
        this.name = name;
        subMetas.addAll(metadatas);
    }

    public DefaultMetadataGroup(Chars name, Metadata[] metadatas) {
        this.name = name;
        subMetas.addAll(metadatas);
    }

    public Chars getName() {
        return name;
    }

    public Sequence getSubMetas() {
        return subMetas;
    }

    @Override
    public Node toNode() {
        final Node[] children = new Node[subMetas.getSize()];
        for (int i=0;i<children.length;i++) {
            children[i] = ((Metadata) subMetas.get(i)).toNode();
        }
        return new DefaultNamedNode(name, children, false);
    }

}
