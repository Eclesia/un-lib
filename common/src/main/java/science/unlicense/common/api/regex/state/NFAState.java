

package science.unlicense.common.api.regex.state;

/**
 * Thompson NFA automaton state.
 *
 * @author Johann Sorel
 */
public interface NFAState {
}
