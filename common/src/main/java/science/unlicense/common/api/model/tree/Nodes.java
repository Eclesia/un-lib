
package science.unlicense.common.api.model.tree;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Convenient methods for node manipulation.
 *
 * @author Johann Sorel
 */
public final class Nodes {

    private static final NodeVisitor FLATTENER = new NodeVisitor() {
        @Override
        public Object visit(Node node, Object context) {
            ((Sequence) context).add(node);
            return super.visit(node, context);
        }
    };

    public static final NodeVisitor SEARCHER = new SearchVisitor();

    private static final Chars HSPACE = Chars.constant("  ");

    private Nodes() {}

    public static Chars toCharsTree(Node node, int depth) {
        return toCharsTree(node, node.toChars(), depth);
    }

    public static Chars toCharsTree(Node node, Chars currentText, int depth) {
        final CharBuffer sb = new CharBuffer();
        if (currentText != null) {
            sb.append(currentText);
        } else if (node instanceof TypedNode) {
            final TypedNode sn = (TypedNode) node;
            sb.append(sn.getType().getId());

            if (sn.getValue() != null) {
                sb.append(':').append(CObjects.toChars(sn.getValue()));
            }
        }

        //print childrens
        final Object[] array = node.getChildren().toArray();
        final int nbChild = array.length;
        if (nbChild>0) {
            if (depth>0) {
                depth--;
                sb.append('\n');
                for (int i=0;i<nbChild;i++) {
                    if (i==nbChild-1) {
                        sb.append("\u2514\u2500 ");
                    } else {
                        sb.append("\u251C\u2500 ");
                    }

                    Chars sc = toCharsTree((Node) array[i], depth-1);
                    Chars[] parts = sc.split('\n');
                    sb.append(parts[0]).append('\n');
                    for (int k=1;k<parts.length;k++) {
                        if (i==nbChild-1) {
                            sb.append(' ');
                        } else {
                            sb.append('\u2502');
                        }
                        sb.append(HSPACE);
                        sb.append(parts[k]);
                        sb.append('\n');
                    }
                }
            } else {
                sb.append(" ... ");
            }
        }

        Chars c = sb.toChars();
        if (c.endsWith('\n')) {
            c = c.truncate(0, c.getCharLength()-1);
        }

        return c;
    }

    public static Chars toCharsTree(NodeType node, Chars currentText, int depth) {
        final CharBuffer sb = new CharBuffer();
        if (currentText != null) {
            sb.append(currentText);
        } else if (node instanceof TypedNode) {
            final TypedNode sn = (TypedNode) node;
            sb.append(sn.getType().getId());

            if (sn.getValue() != null) {
                sb.append(':').append(CObjects.toChars(sn.getValue()));
            }
        }

        //print childrens
        final NodeCardinality[] children = node.getChildrenTypes();
        final int nbChild = children.length;
        if (nbChild>0) {
            if (depth>0) {
                depth--;
                sb.append('\n');
                for (int i=0;i<nbChild;i++) {
                    if (i==nbChild-1) {
                        sb.append("\u2514\u2500 ");
                    } else {
                        sb.append("\u251C\u2500 ");
                    }

                    final Chars sc = children[i].toCharsTree(depth);
                    final Chars[] parts = sc.split('\n');
                    sb.append(parts[0]).append('\n');
                    for (int k=1;k<parts.length;k++) {
                        if (i==nbChild-1) {
                            sb.append(' ');
                        } else {
                            sb.append('\u2502');
                        }
                        sb.append(HSPACE);
                        sb.append(parts[k]);
                        sb.append('\n');
                    }
                }
            } else {
                sb.append(" ... ");
            }
        }

        return sb.toChars();
    }

    public static Chars toChars(Chars rootText, Object[] children) {
        final Collection col = new ArraySequence(children);
        return toChars(rootText, col);
    }

    public static Chars toChars(Chars rootText, Collection children) {
        final CharBuffer sb = new CharBuffer();
        sb.append(rootText);

        //print childrens
        final int nbChild = children.getSize();
        if (nbChild != 0) {
            final Iterator ite = children.createIterator();
            sb.append('\n');
            int i=0;
            while (ite.hasNext()) {

                if (i==nbChild-1) {
                    sb.append("\u2514\u2500 ");
                } else {
                    sb.append("\u251C\u2500 ");
                }

                Chars sc = CObjects.toChars(ite.next());
                if (sc.endsWith('\n')) {
                    sc = sc.truncate(0, sc.getCharLength()-1);
                }
                Chars[] parts = sc.split('\n');
                sb.append(parts[0]).append('\n');
                for (int k=1;k<parts.length;k++) {
                    if (i==nbChild-1) {
                        sb.append(' ');
                    } else {
                        sb.append('\u2502');
                    }
                    sb.append(HSPACE);
                    sb.append(parts[k]);
                    sb.append('\n');
                }
                i++;
            }
        }

        return sb.toChars();
    }

    /**
     * Flatten the node tree in a sequence.
     *
     * @param node
     * @param includeThisNode true to include the root node
     * @return sequence of all nodes
     */
    public static Sequence flatten(Node node, boolean includeThisNode) {
        return flatten(node, includeThisNode, Predicate.TRUE);
    }

    /**
     * Flatten the node tree in a sequence.
     *
     * @param node
     * @param includeThisNode true to include the root node
     * @return sequence of all nodes
     */
    public static Sequence flatten(Node node, boolean includeThisNode, final Predicate filter) {
        final Sequence nodes = new ArraySequence();

        final NodeVisitor visitor;
        if (filter==null || Predicate.TRUE.equals(filter)) {
            visitor = FLATTENER;
        } else {
            visitor = new NodeVisitor() {
                public Object visit(Node node, Object context) {
                    if (filter.evaluate(node)) ((Sequence) context).add(node);
                    return super.visit(node, context);
                }
            };
        }

        if (includeThisNode) {
            visitor.visit(node, nodes);
        } else {
            final Iterator ite = node.getChildren().createIterator();
            while (ite.hasNext()) {
                visitor.visit((Node) ite.next(), nodes);
            }
        }
        return nodes;
    }

    /**
     *
     * @param parent Parent node to search in
     * @param path simple path with '/' separators
     * @param create create the node if it does not exist
     * @return NamedNode or null if not found
     */
    public static NamedNode getPathNode(NamedNode parent, Chars path, boolean create) {
        final Chars[] parts = path.split('/');
        for (int i=0;i<parts.length;i++) {
            NamedNode candidate = getChildNode(parent,parts[i],false);
            if (candidate == null) {
                if (create) {
                    candidate = new DefaultNamedNode(parts[i],true);
                    parent.getChildren().add(candidate);
                } else {
                    break;
                }
            }
            parent = candidate;
        }
        return parent;
    }

    /**
     *
     * @param parent Parent node to search in
     * @param name Name of the searched node
     * @param create create the node if it does not exist
     * @return NamedNode or null if not found
     */
    public static NamedNode getChildNode(NamedNode parent, Chars name, boolean create) {
        final Iterator ite = parent.getChildren().createIterator();
        while (ite.hasNext()) {
            final NamedNode n = (NamedNode) ite.next();
            if (n.getName().equals(name, true, true)) {
                return n;
            }
        }
        if (create) {
            //create it
            NamedNode n = new DefaultNamedNode(name,true);
            parent.getChildren().add(n);
            return n;
        } else {
            return null;
        }

    }

    /**
     * Extract children node values which match given predicate.
     *
     * @param parent
     * @param filter
     * @return
     */
    public static Sequence getChildrenValues(NamedNode parent, Predicate filter) {
        final Sequence values = new ArraySequence();
        getChildrenValues(values, parent, filter);
        return values;
    }

    private static void getChildrenValues(Sequence values, NamedNode node, Predicate filter) {

        if (filter==null || filter.evaluate(node)) {
            final Object value = node.getValue();
            if (value!=null) values.add(value);
        }

        final Iterator ite = node.getChildren().createIterator();
        while (ite.hasNext()) {
            final NamedNode next = (NamedNode) ite.next();
            getChildrenValues(values, next, filter);
        }
    }


    public static final class SearchVisitor extends NodeVisitor{

        public Object visit(Node node, Object context) {

            if (((Predicate) context).evaluate(node)) {
                return node;
            }

            final Iterator ite = node.getChildren().createIterator();
            while (ite.hasNext()) {
                Object next = ite.next();
                if (next!=null) {
                    Object res = visit( (Node) next, context);
                    if (res!=null) return res;
                }
            }
            return null;
        }

    }

}
