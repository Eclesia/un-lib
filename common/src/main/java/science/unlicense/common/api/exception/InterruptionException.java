
package science.unlicense.common.api.exception;

/**
 * Raised when a process has been interrupted.
 * This exception reflects hardware low level interruptions but also volonteer
 * stopped processes or threads.
 *
 * @author Johann Sorel
 */
public class InterruptionException extends Exception {

    public InterruptionException() {
    }

    public InterruptionException(String s) {
        super(s);
    }

    public InterruptionException(Throwable cause) {
        super(cause);
    }

    public InterruptionException(String message, Throwable cause) {
        super(message, cause);
    }

}
