
package science.unlicense.common.api.model.tree;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Event;

/**
 * Abstract node class.
 * Provides :
 * - default implementations for add methods falling back on addChidlren.
 * - event manager and methods
 * - clean toChars
 *
 * @author Johann Sorel
 */
public abstract class AbstractNode extends AbstractEventSource implements Node{

    public Chars toChars() {
        return new Chars("AbstractNode("+getClass().getSimpleName()+")");
    }

    protected void sendNodeAdd(Node[] added, int start) {
        if (hasListeners()) {
            getEventManager().sendEvent(new Event(this,new NodeMessage(
                NodeMessage.TYPE_ADD, start, start+added.length,
                    null, added)));
        }
    }

    protected void sendNodeRemove(Node[] removed, int start) {
        if (hasListeners()) {
            getEventManager().sendEvent(new Event(this,new NodeMessage(
                NodeMessage.TYPE_REMOVE, start, start+removed.length, removed, null)));
        }
    }

    protected void sendNodeReplace(Node[] olds, Node[] updated, int start, Event subEvent) {
        if (hasListeners()) {
            getEventManager().sendEvent(
                    new Event(this,
                            new NodeMessage(NodeMessage.TYPE_REPLACE, start, start+updated.length, olds, updated),
                            subEvent));
        }
    }

    /**
     * Send a node replace all event.
     *
     * @param startIndex first index of modified element, -1 if unknowed
     * @param endIndex last index of modified element, -1 if unknowed
     * @param olds values before replacement
     * @param news values after replacement
     */
    protected void sendNodeReplaceAll(int startIndex, int endIndex, Node[] olds, Node[] news) {
        if (hasListeners()) {
            getEventManager().sendEvent(new Event(this,new NodeMessage(NodeMessage.TYPE_REPLACEALL, startIndex, endIndex,
                    olds, news)));
        }
    }

}
