
package science.unlicense.common.api.model.doc;

import science.unlicense.common.api.character.Chars;

/**
 * Exception send when trying to access a field which doesn't exist.
 *
 * @author Johann Sorel
 */
public class FieldNotFoundException extends RuntimeException {

    public FieldNotFoundException(Chars fieldName) {
        super("Field "+fieldName+" does not exist");
    }

}
