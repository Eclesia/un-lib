

package science.unlicense.common.api.number;

/**
 *
 * @author Johann Sorel
 */
public class IEEE754 {

    public long fraction;
    public int exponent;
    public int sign;

}
