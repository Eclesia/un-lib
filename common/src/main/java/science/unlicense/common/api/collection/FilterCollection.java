
package science.unlicense.common.api.collection;

import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Filter collection elements based on a predicate.
 *
 * @author Johann Sorel
 */
final class FilterCollection extends AbstractCollection {

    private final Collection sub;
    private final Predicate predicate;

    FilterCollection(Collection sub, Predicate predicate) {
        this.sub = sub;
        this.predicate = predicate;
    }

    public boolean add(Object candidate) {
        if (predicate.evaluate(candidate)) {
            return sub.add(candidate);
        } else {
            throw new InvalidArgumentException("Object " + candidate + " do not match predicate");
        }
    }

    public boolean remove(Object candidate) {
        if (predicate.evaluate(candidate)) {
            return sub.remove(candidate);
        } else {
            throw new InvalidArgumentException("Object " + candidate + " do not match predicate");
        }
    }

    public Iterator createIterator() {
        return new FilterIterator(sub.createIterator(), predicate);
    }

}
