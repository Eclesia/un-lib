

package science.unlicense.common.api.model.tree;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class NodePath extends CObject {

    private final Node[] stack;

    public NodePath() {
        this.stack = new Node[0];
    }

    public NodePath(Node[] stack) {
        this.stack = stack;
    }

    public NodePath(NodePath parent, Node leaf) {
        stack = new Node[parent.stack.length+1];
        Arrays.copy(parent.stack, 0, parent.stack.length, stack, 0);
        stack[stack.length-1] = leaf;
    }

    public Node[] getStack() {
        return stack;
    }

    /**
     * Get the last element in the path.
     * @return Node or null of path is empty
     */
    public Node getLeaf() {
        if (stack.length==0) return null;
        return stack[stack.length-1];
    }

    public Chars toChars() {
        return CObjects.toChars(getLeaf());
    }

}
