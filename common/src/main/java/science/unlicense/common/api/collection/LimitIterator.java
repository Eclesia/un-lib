
package science.unlicense.common.api.collection;

/**
 *
 * @author Johann Sorel
 */
class LimitIterator extends AbstractIterator {

    private final Iterator base;
    private int remaining;

    public LimitIterator(Iterator base, int remaining) {
        this.base = base;
        this.remaining = remaining;
    }

    @Override
    protected void findNext() {
        if (remaining < 0) {
            return;
        }
        if (base.hasNext()) {
            nextValue = base.next();
            remaining--;
        } else {
            remaining = -1;
        }
    }

}
