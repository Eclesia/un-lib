package science.unlicense.common.api.collection;

/**
 *
 * @author Johann Sorel
 */
public interface Iterator {

    /**
     *
     * @return true if more elements are available.
     */
    boolean hasNext();

    /**
     *
     * @return next element.
     */
    Object next();

    /**
     * Remove the current element.
     * @return true if element was successfully removed.
     */
    boolean remove();

    /**
     * Must be called when ending iteration.
     * Underlying collection mith require to close resources.
     */
    void close();

}
