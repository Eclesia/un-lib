
package science.unlicense.common.api.number;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public final class UInt32 extends AbstractNumber {

    public static final NumberType TYPE = new NumberType() {

        @Override
        public Number create(Arithmetic value) {
            if (!(value instanceof Number)) throw new UnsupportedOperationException();
            return new UInt32( ((Number) value).toInteger());
        }

        @Override
        public Class getPrimitiveClass() {
            return int.class;
        }

        @Override
        public Class getValueClass() {
            return UInt32.class;
        }

        @Override
        public int getSizeInBits() {
            return 32;
        }

        @Override
        public int getSizeInBytes() {
            return 4;
        }

        @Override
        public int getPrimitiveCode() {
            return Primitive.UINT32;
        }
    };

    private final int value;

    public UInt32(int value) {
        this.value = value;
    }

    @Override
    public NumberType getType() {
        return TYPE;
    }

    @Override
    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public int toInteger() {
        return value;
    }

    @Override
    public long toLong() {
        return value & 0xFFFFFFFFl;
    }

    @Override
    public float toFloat() {
        return value & 0xFFFFFFFFl;
    }

    @Override
    public double toDouble() {
        return value & 0xFFFFFFFFl;
    }

    @Override
    public Chars toChars() {
        return Int64.encode(toLong());
    }

    @Override
    public int order(Object other) {
        long of = ((Number) other).toLong();
        long v = value & 0xFFFFFFFFl;
        return v < of ? -1 : (v > of ? +1 : 0) ;
    }

}
