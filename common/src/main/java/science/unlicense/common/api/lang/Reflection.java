
package science.unlicense.common.api.lang;

import java.lang.reflect.Method;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;

/**
 * Utility methods for reflection.
 *
 * @author Johann Sorel
 */
public final class Reflection {

    private Reflection() {}

    /**
     * Find all properties with a getter and a setter.
     *
     * @param candidate
     * @return
     */
    public static Sequence listProperties(Class candidate) {
        final Method[] methods = candidate.getMethods();

        final Sequence names = new ArraySequence();

        for (Method method : methods) {
            final String name = method.getName();
            if (name.startsWith("set") && Void.TYPE.equals(method.getReturnType()) && method.getParameterTypes().length==1) {
                final String propertyName = name.substring(3);
                final Class propertyClass = method.getParameterTypes()[0];
                final String name1 = "get"+propertyName;
                final String name2 = "is"+propertyName;
                for (Method getterMethod : methods) {
                    final String cname = getterMethod.getName();
                    final Class<?> returnType = getterMethod.getReturnType();
                    if ((cname.equals(name1) || cname.equals(name2)) && getterMethod.getParameterTypes().length==0 && propertyClass.isAssignableFrom(returnType)) {
                        names.add(new Chars(propertyName));
                    }
                }
            }
        }

        return names;
    }

    public static Class getBoxingClass(Class c) {
        if (c.isPrimitive()) {
            if (boolean.class.equals(c)) c = Boolean.class;
            else if (byte.class.equals(c)) c = Byte.class;
            else if (short.class.equals(c)) c = Short.class;
            else if (char.class.equals(c)) c = Character.class;
            else if (int.class.equals(c)) c = Integer.class;
            else if (long.class.equals(c)) c = Long.class;
            else if (float.class.equals(c)) c = Float.class;
            else if (double.class.equals(c)) c = Double.class;
        }
        return c;
    }

    /**
     * Get all implemented interfaces.
     *
     * @return
     */
    public static Sequence getInterfaces(Class clazz) {
        final Sequence seq = new ArraySequence();
        getInterfaces(clazz, seq);
        return seq;
    }

    private static void getInterfaces(Class clazz, Sequence seq) {
        if (clazz==null) return;
        if (clazz.isInterface()) {
            seq.add(clazz);
        }

        for (Class c : clazz.getInterfaces()) getInterfaces(c,seq);
        getInterfaces(clazz.getSuperclass(),seq);
    }

    public static Set getAllImplementOrExtendClasses(Class candidate) {
        final HashSet hs = new HashSet();
        loopAllImplementOrExtendClasses(candidate, hs);
        return hs;
    }

    private static void loopAllImplementOrExtendClasses(Class candidate, Set hs) {
        //loop on parent hierarchy
        do {
            hs.add(candidate);
            final Class[] interfaces = candidate.getInterfaces();
            for (int i=0;i<interfaces.length;i++) {
                loopAllImplementOrExtendClasses(interfaces[i],hs);
            }
            candidate = candidate.getSuperclass();
        } while (candidate != null);
    }

}
