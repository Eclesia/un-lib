
package science.unlicense.common.api.collection;

/**
 *
 * @author Johann Sorel
 */
final class EmptyDictionary extends AbstractDictionary {

    static final EmptyDictionary INSTANCE = new EmptyDictionary();

    private EmptyDictionary(){}

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public Set getKeys() {
        return EmptyCollection.INSTANCE;
    }

    @Override
    public Collection getValues() {
        return EmptyCollection.INSTANCE;
    }

    @Override
    public Collection getPairs() {
        return EmptyCollection.INSTANCE;
    }

    @Override
    public Object getValue(Object key) {
        return null;
    }

    @Override
    public void add(Object key, Object value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public Object remove(Object key) {
        return null;
    }

    @Override
    public void removeAll() {
    }

}
