
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.AbstractCollection;
import science.unlicense.common.api.collection.AbstractIterator;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.OrderedSet;
import science.unlicense.common.api.collection.CollectionException;

/**
 *
 * @author Johann Sorel
 */
public class DoubleSet extends AbstractCollection implements OrderedSet{

    private double[] buffer;
    private int size = 0;

    public DoubleSet() {
        buffer = new double[1000];
        size = 0;
    }

    @Override
    public int getSize() {
        return size;
    }

    public double getFirst() {
        if (size==0) throw new CollectionException("Collection is empty");
        return buffer[0];
    }

    public double getLast() {
        if (size==0) throw new CollectionException("Collection is empty");
        return buffer[size-1];
    }

    public double[] toArrayDouble() {
        return Arrays.copy(buffer, 0, size);
    }

    private void growIfNecessary(int length) {
        size += length;
        if (size >= buffer.length) {
            int gs = Math.max(length, buffer.length*3);
            buffer = Arrays.resize(buffer, buffer.length+gs);
        }
    }

    public boolean add(double candidate) {
        if (size==0) {
            buffer[0] = candidate;
            size = 1;
            return true;
        } else {

            int index = Arrays.binarySearch(candidate, buffer,0,size-1);
            if (index<0) {
                //new value
                index = -(index+1);
                growIfNecessary(1);
                for (int i=size-1;i>=index;i--) {
                    buffer[i+1] = buffer[i];
                }
                buffer[index] = candidate;
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean remove(double candidate) {
        if (size==0) return false;
        final int index = Arrays.binarySearch(candidate, buffer,0,size-1);
        if (index>=0) {
            Arrays.copy(buffer, index+1, size-index, buffer, index);
            size--;
            return true;
        } else {
            return false;
        }
    }

    public boolean contains(double candidate) {
        if (size==0) return false;
        return Arrays.binarySearch(candidate, buffer,0,size-1) >=0;
    }

    public boolean add(Object candidate) {
        return add(((Double) candidate).doubleValue());
    }

    public boolean remove(Object candidate) {
        return remove(((Double) candidate).doubleValue());
    }

    public boolean contains(Object candidate) {
        return contains(((Double) candidate).doubleValue());
    }

    public Iterator createIterator() {
        return new Ite();
    }

    public boolean removeAll() {
        buffer = new double[1000];
        size = 0;
        return true;
    }

    private final class Ite extends AbstractIterator{

        int index=0;

        @Override
        protected void findNext() {
            if (index>=size) return;
            nextValue = buffer[index];
            index++;
        }

    }

}
