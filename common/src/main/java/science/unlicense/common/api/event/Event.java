
package science.unlicense.common.api.event;

import science.unlicense.common.api.CObject;

/**
 * An event is a container for informations.
 * Whenever an object needs to send notifications to others it should implement
 * the EventSource class, and then send Event objects.
 *
 * Events can be used for example as :
 * - mouse move event
 * - button click
 * - data update notification
 *
 * Event objects should not be subclasses, event object should only hold
 * informations of the source, message and trigger. Since new events can
 * be created with different sources, the only property which will be preserved
 * is the EventMessage. It is recommended to subclass EventMessage and not Event.
 *
 * @author Johann Sorel
 */
public final class Event extends CObject {

    private final EventSource source;
    private final EventMessage message;
    private final Event trigger;

    public Event(EventSource source, EventMessage message) {
        this(source,message,null);
    }

    public Event(EventSource source, EventMessage message, Event trigger) {
        this.source = source;
        this.message = message;
        this.trigger = trigger;
    }

    /**
     * @return EventSource who raised the event, never null.
     */
    public EventSource getSource() {
        return source;
    }

    /**
     * @return EventMessage the event informations, never null.
     */
    public EventMessage getMessage() {
        return message;
    }

    /**
     * An event might have been trigger by another.
     * This method returns the original trigger event.
     *
     * @return Event, can be null.
     */
    public Event getTrigger() {
        return trigger;
    }

    public int getHash() {
        int hash = 7;
        hash = 31 * hash + (this.source != null ? this.source.hashCode() : 0);
        hash = 31 * hash + (this.message != null ? this.message.hashCode() : 0);
        hash = 31 * hash + (this.trigger != null ? this.trigger.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Event other = (Event) obj;
        if (this.source != other.source && (this.source == null || !this.source.equals(other.source))) {
            return false;
        }
        if (this.message != other.message && (this.message == null || !this.message.equals(other.message))) {
            return false;
        }
        if (this.trigger != other.trigger && (this.trigger == null || !this.trigger.equals(other.trigger))) {
            return false;
        }
        return true;
    }

}
