
package science.unlicense.common.api.regex;

import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.regex.state.Evaluator;
import science.unlicense.common.api.regex.state.FastForward;
import science.unlicense.common.api.regex.state.Fork;

/**
 * NFA graph executor.
 * This executor may be used by with any kind of objects.
 * Can be characters codepoint in a regex or tokens in a parser.
 *
 * This class is not concurrent.
 *
 * @author Johann Sorel
 */
public class NFAStateExec {

    protected final NFAState state;
    protected Seq buffer1 = new Seq();
    protected Seq buffer2 = new Seq();
    //cached start states
    private Object[] baseStates;

    public NFAStateExec(final NFAState state) {
        this.state = state;
    }

    public void init() {
        //cache the starting states, there are often a lot of forks at the beginning
        //since those conditions never change, caching those bring a huge performance
        //increase when parsing large files.
        addState(buffer1, state);

        baseStates = buffer1.toArray();
        buffer1.removeAll();
        buffer2.setMinSize(baseStates.length);
    }

    public NFAState getState() {
        return state;
    }

    /**
     * Return the list of current automaton states.
     * This list can contain NFAState  objects.
     *
     * @return Sequence current automaton states.
     */
    public Sequence getProcessState() {
        return buffer1;
    }

    /**
     * Reset executor state.
     * Must be called before any text is evaluated.
     */
    public void reset() {
        //reset buffers
        buffer1.removeAll();
        buffer2.removeAll();
        buffer1.setValues(baseStates);
    }

    /**
     * Process a new elements.
     * This returned list can contain NFAState or NFAPath objects if
     * the createPath parameter has been set to True in the constructor.
     *
     * @param candidate unicode code point
     * @return Collection of current NFA paths
     */
    public Sequence process(Object candidate) {
        buffer2.removeAll();
        stepState(candidate);

        //swap buffers
        final Seq temp = buffer1;
        buffer1 = buffer2;
        buffer2 = temp;
        return buffer1;
    }

    private static void addState(final Sequence states, NFAState state) {
        //TODO the contains operation is expensive here, it could be removed but
        //this may duplicate possible paths
        //if (path == null || states.contains(path)) {
        if (state == null) {
            return;
        }

        while (state instanceof FastForward) {
            state = ((FastForward) state).getNextState();
        }

        if (state instanceof Fork) {
            final Fork fork = (Fork) state;
            addState(states, fork.getNext1State());
            addState(states, fork.getNext2State());
        } else {
            states.add(state);
        }
    }
    /**
     * Conditions :
     * - buffer1 must contain previous States
     * - buffer2 will contain next States
     *
     * @param cp
     * @param
     * @param buffer2
     */
    private void stepState(final Object cp) {
        final Object[] obj = buffer1.getValues();
        for (int i = 0, n = buffer1.getSize(); i < n; i++) {
            if (obj[i] instanceof Evaluator) {
                final Evaluator evaluator = (Evaluator) obj[i];
                if (evaluator.evaluate(cp)) {
                    addState(buffer2, evaluator.getNextState());
                }
            }
        }
    }

    public static final class Seq extends ArraySequence {

        public Object[] getValues() {
            return values;
        }

        void setMinSize(int size) {
            growIfNecessary(size);
        }

        void setValues(Object[] vals) {
            Arrays.copy(vals, 0, vals.length, values, 0);
            size = vals.length;
        }

        public boolean removeAll() {
            size = 0;
            return true;
        }
    }

}
