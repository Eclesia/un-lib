
package science.unlicense.common.api.model.doc;

import science.unlicense.common.api.event.Property;

/**
 * Single property of a document.
 *
 * @author Johann Sorel
 */
public interface Field extends Property {

    /**
     * Get field type.
     *
     * @return field type, can be null if field is not defined in the document type.
     */
    @Override
    FieldType getType();

}
