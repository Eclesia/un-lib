
package science.unlicense.common.api.logging;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.DefaultDocumentType;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;

/**
 *
 * @author Johann Sorel
 */
public class DefaultLogger extends AbstractLogger {

    private static final DocumentType PARAMETERS = new DefaultDocumentType();

    private Document configuration;

    public DefaultLogger() {
        configuration = new DefaultDocument(true,PARAMETERS);
    }

    @Override
    public DocumentType getParametersType() {
        return PARAMETERS;
    }

    @Override
    public Document getParameters() {
        return configuration;
    }

    @Override
    public void setParameters(Document config) throws LoggerException {
        this.configuration = config;
    }

    @Override
    public void log(Throwable exception, float level) {
        String str = exception.getMessage();
        if (str != null) {
            log(new Chars(str), exception, level);
        } else {
            log(null, exception, level);
        }
    }

    @Override
    public void log(Chars message, Throwable exception, float level) {
        if (level > LEVEL_DEBUG) {
            if (message != null) {
                System.err.println(message);
            } else {
                //print the exception type
                System.err.println(exception.getClass().getName());
            }
        }
        if (exception != null && level > LEVEL_INFORMATION) {
            exception.printStackTrace(System.err);
        }
    }

}
