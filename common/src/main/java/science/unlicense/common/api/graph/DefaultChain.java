
package science.unlicense.common.api.graph;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;

/**
 * Default chain implementation.
 *
 * @author Johann Sorel
 */
public class DefaultChain extends AbstractChain{

    private final Sequence edges = new ArraySequence();

    public DefaultChain() {
    }

    @Override
    public Sequence getEdges() {
        return edges;
    }

}
