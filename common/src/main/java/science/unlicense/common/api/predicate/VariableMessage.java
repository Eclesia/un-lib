
package science.unlicense.common.api.predicate;

import science.unlicense.common.api.event.DefaultEventMessage;
import science.unlicense.common.api.event.MessagePredicate;

/**
 *
 * @author Johann Sorel
 */
public class VariableMessage extends DefaultEventMessage {

    public static final Predicate PREDICATE = new MessagePredicate(VariableMessage.class);

    private final Object oldValue;
    private final Object newValue;

    /**
     * New variable event.
     *
     * @param oldValue old property value
     * @param newValue new property value
     */
    public VariableMessage(Object oldValue, Object newValue) {
        super(false);
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    /**
     * Get old variable value.
     *
     * @return Object, old variable value
     */
    public Object getOldValue() {
        return oldValue;
    }

    /**
     * Get new variable value
     *
     * @return Object, new variable value
     */
    public Object getNewValue() {
        return newValue;
    }
}
