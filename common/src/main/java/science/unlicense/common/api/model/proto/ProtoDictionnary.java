
package science.unlicense.common.api.model.proto;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.predicate.Predicate;

/**
 *
 * @author Johann Sorel
 */
public class ProtoDictionnary implements Dictionary {

    private final Prototype pt;

    public ProtoDictionnary(Prototype pt) {
        this.pt = pt;
    }

    @Override
    public int getSize() {
        return pt.getPropertyNames().getSize();
    }

    @Override
    public Set getKeys() {
        return pt.getPropertyNames();
    }

    @Override
    public Collection getValues() {
        final Sequence seq = new ArraySequence();
        final Iterator ite = getKeys().createIterator();
        while (ite.hasNext()) {
            seq.add(pt.getPropertyValue((Chars) ite.next()));
        }
        return seq;
    }

    @Override
    public Collection getPairs() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public Object getValue(Object key) {
        if (key instanceof Chars) {
            return pt.getPropertyValue((Chars) key);
        } else {
            return null;
        }
    }

    @Override
    public void add(Object key, Object value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void addAll(Dictionary index) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public Object remove(Object key) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void removeAll() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[0];
    }

    @Override
    public EventListener[] getListeners(Predicate predicate) {
        return new EventListener[0];
    }

    @Override
    public void addEventListener(Predicate predicate, EventListener listener) {
    }

    @Override
    public void removeEventListener(Predicate predicate, EventListener listener) {
    }
}
