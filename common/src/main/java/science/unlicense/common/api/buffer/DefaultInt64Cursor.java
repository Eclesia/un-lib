
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.number.Endianness;

/**
 * Both Byte cursor and byte buffer view.
 *
 * @author Johann Sorel
 */
public class DefaultInt64Cursor extends AbstractCursor implements Int64Cursor, Buffer.Int64 {

    final Buffer buffer;

    //current offset from start.
    private long byteOffset = 0;

    public DefaultInt64Cursor(Buffer buffer, Endianness encoding) {
        super(encoding);
        this.buffer = buffer;
    }

    @Override
    public Buffer getBuffer() {
        return buffer;
    }

    @Override
    public long getByteOffset() {
        return byteOffset;
    }

    @Override
    public long getOffset() {
        return byteOffset/8;
    }

    @Override
    public long getSize() {
        return buffer.getByteSize() / 8;
    }

    public Int64Cursor cursor() {
        return new DefaultInt64Cursor(buffer, encoding);
    }

    @Override
    public DefaultInt64Cursor offset(long position) {
        byteOffset = position*8;
        return this;
    }

    @Override
    public long read(long position) {
        return buffer.readInt64(position*8);
    }

    @Override
    public void read(long position, long[] array) {
        buffer.readInt64(array,position*8);
    }

    @Override
    public void read(long position, long[] array, int arrayOffset, int length) {
        buffer.readInt64(array,arrayOffset,length,position*8);
    }

    @Override
    public void write(long position, long value) {
        buffer.writeInt64(value, position*8);
    }

    @Override
    public void write(long position, long[] array) {
        write(position, array, 0, array.length);
    }

    @Override
    public void write(long position, long[] array, int arrayOffset, int length) {
        buffer.writeInt64(array,arrayOffset,length, position*8);
    }

    @Override
    public long read() {
        long v = buffer.readInt64(byteOffset);
        byteOffset +=8;
        return v;
    }

    @Override
    public void read(long[] array) {
        read(array,0,array.length);
    }

    @Override
    public void read(long[] array, int arrayOffset, int length) {
        buffer.readInt64(array,byteOffset);
        byteOffset += length*8;
    }

    @Override
    public Int64Cursor write(long value) {
        buffer.writeInt64(value, byteOffset);
        byteOffset += 8;
        return this;
    }

    @Override
    public Int64Cursor write(long[] array) {
        return write(array, 0, array.length);
    }

    @Override
    public Int64Cursor write(long[] array, int arrayOffset, int length) {
        buffer.writeInt64(array,arrayOffset,length, byteOffset);
        byteOffset += length*8;
        return this;
    }

    @Override
    public long[] toLongArray() {
        final long[] array = new long[(int) getSize()];
        read(array);
        return array;
    }
}
