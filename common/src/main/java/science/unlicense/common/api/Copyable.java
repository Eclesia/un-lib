
package science.unlicense.common.api;

/**
 *
 * @author Johann Sorel
 */
public interface Copyable {

    Obj copy();

}
