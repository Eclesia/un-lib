

package science.unlicense.common.api.character;

import science.unlicense.common.api.Orderable;

/**
 *
 * @author Johann Sorel
 */
public interface CharArray extends Orderable {

    /**
     * Get character sequence encoding.
     * @return CharEncoding
     */
    CharEncoding getEncoding();

    /**
     * Number of characters in this character sequence.
     * @return int
     */
    int getCharLength();

    /**
     * Number of bytes used by this character sequence.
     * @return int
     */
    int getByteLength();

    /**
     * Test if this char sequence is empty, contains no characters.
     * @return boolean, true is sequence is empty
     */
    boolean isEmpty();

    /**
     * Get Char at given index.
     * @param index : char index
     * @return Char
     */
    Char getCharacter(int index);

    /**
     * Get character unicode value at given index.
     * @param index : char index
     * @return int
     */
    int getUnicode(int index);

    /**
     * Get byte at given index.
     * @param index : byteindex
     * @return byte
     */
    byte getByte(int index);

    /**
     * Truncate this char sequence.
     *
     * @param start start clip index, inclusive
     * @param end end clip index, exclusive, use -1 for end of char array
     * @return clipped text
     */
    CharArray truncate(int start, int end);

    /**
     * Test if sequence starts with given char.
     *
     * @param cp unicode codepoint
     * @return true if this char sequence starts with given char
     */
    boolean startsWith(int cp);

    /**
     * Test if sequence starts with given char.
     * Char encoding is tested and reencoded if needed.
     *
     * @param ch test character
     * @return true if this char sequence starts with given char
     */
    boolean startsWith(Char ch);

    /**
     * Test if sequence starts with given char sequence.
     * Char sequence encoding is tested and reencoded if needed.
     *
     * @param cs test character
     * @return true if this char sequence starts with given sequence
     */
    boolean startsWith(CharArray cs);

    /**
     * Test if sequence starts with given byte array.
     *
     * @param cdata test character
     * @return true if this char sequence starts with given bytes
     */
    boolean startsWith(byte[] cdata);

    /**
     * Test if sequence ends with given char sequence.
     * Char sequence encoding is tested and reencoded if needed.
     *
     * @param cs test character
     * @return true if this char sequence ends with given sequence
     */
    boolean endsWith(CharArray cs);

    /**
     * Test if sequence ends with given char.
     * Char encoding is tested and reencoded if needed.
     *
     * @param ch test character
     * @return true if this char sequence ends with given char
     */
    boolean endsWith(Char ch);

    /**
     * Test if sequence ends with given unicode char.
     * Char encoding is tested and reencoded if needed.
     *
     * @param ch unicode
     * @return true if this char sequence ends with given char
     */
    boolean endsWith(int ch);

    /**
     * Test if sequence ends with given byte array.
     *
     * @param cdata test character
     * @return true if this char sequence ends with given bytes
     */
    boolean endsWith(byte[] cdata);

    /**
     * Get the first occurence of given char sequence.
     * Char sequence encoding is tested and reencoded if needed.
     *
     * @param cs test character
     * @return first occurence char index, -1 if not found
     */
    int getFirstOccurence(CharArray cs);

    /**
     * Get the first occurence of given char sequence starting from given position.
     * Char sequence encoding is tested and reencoded if needed.
     *
     * @param cs test character
     * @param startPosition search start offset
     * @return first occurence char index, -1 if not found
     */
    int getFirstOccurence(CharArray cs, int startPosition);

    /**
     * Get the first occurence of given character.
     * Char is tested and reencoded if needed.
     *
     * @param ch test character
     * @return first occurence char index, -1 if not found
     */
    int getFirstOccurence(Char ch);

    /**
     * Get the first occurence of given character starting from given position.
     * Char encoding is tested and reencoded if needed.
     *
     * @param ch test character
     * @param startPosition search start offset
     * @return first occurence char index, -1 if not found
     */
    int getFirstOccurence(Char ch, int startPosition);

    /**
     * Get the first occurence of given character.
     *
     * @param codepoint test character
     * @return first occurence char index, -1 if not found
     */
    int getFirstOccurence(int codepoint);

    /**
     * Get the first occurence of given character starting from given position.
     *
     * @param codepoint test character
     * @param startPosition search start offset
     * @return first occurence char index, -1 if not found
     */
    int getFirstOccurence(int codepoint, int startPosition);

    /**
     * Get the first occurence of given bytes.
     *
     * @param cd test character
     * @return first occurence char index, -1 if not found
     */
    int getFirstOccurence(byte[] cd);

    /**
     * Get the first occurence of given bytes starting from given position.
     *
     * @param cd test character
     * @param startPosition search start offset
     * @return first occurence char index, -1 if not found
     */
    int getFirstOccurence(byte[] cd, int startPosition);

    /**
     * Get the last occurence of given char sequence.
     * Char sequence encoding is tested and reencoded if needed.
     *
     * @param cs test character
     * @return last occurence char index, -1 if not found
     */
    int getLastOccurence(CharArray cs);

    /**
     * Get the last occurence of given char sequence starting from given position.
     * Char sequence encoding is tested and reencoded if needed.
     *
     * @param cs test character
     * @param startPosition search start offset
     * @return last occurence char index, -1 if not found
     */
    int getLastOccurence(CharArray cs, int startPosition);

    /**
     * Get the last occurence of given character.
     * Char is tested and reencoded if needed.
     *
     * @param ch test character
     * @return last occurence char index, -1 if not found
     */
    int getLastOccurence(Char ch);

    /**
     * Get the last occurence of given character starting from given position.
     * Char encoding is tested and reencoded if needed.
     *
     * @param ch test character
     * @param startPosition search start offset
     * @return last occurence char index, -1 if not found
     */
    int getLastOccurence(Char ch, int startPosition);

    /**
     * Get the last occurence of given character.
     *
     * @param codepoint test character
     * @return last occurence char index, -1 if not found
     */
    int getLastOccurence(int codepoint);

    /**
     * Get the last occurence of given character starting from given position.
     *
     * @param codepoint test character
     * @param startPosition search start offset
     * @return last occurence char index, -1 if not found
     */
    int getLastOccurence(int codepoint, int startPosition);

    /**
     * Get the last occurence of given bytes.
     *
     * @param cd test character
     * @return last occurence char index, -1 if not found
     */
    int getLastOccurence(byte[] cd);

    /**
     * Get the last occurence of given bytes starting from given position.
     *
     * @param cd test character
     * @param startPosition search start offset
     * @return last occurence char index, -1 if not found
     */
    int getLastOccurence(byte[] cd, int startPosition);

    /**
     * Replace all occurences of given unicode.
     *
     * @param original value to replace
     * @param replacement replacement value
     * @return new Chars with replaced texts
     */
    CharArray replaceAll(int original, int replacement);

    /**
     * Replace all occurences of given chars starting from given position.
     *
     * @param original value to replace
     * @param replacement replacement value
     * @param startPosition start position
     * @return new Chars with replaced texts
     */
    CharArray replaceAll(int original, int replacement, int startPosition);

    /**
     * Replace all occurences of given char.
     *
     * @param original value to replace
     * @param replacement replacement value
     * @return new Chars with replaced texts
     */
    CharArray replaceAll(Char original, Char replacement);

    /**
     * Replace all occurences of given chars starting from given position.
     *
     * @param original value to replace
     * @param replacement replacement value
     * @param startPosition start position
     * @return new Chars with replaced texts
     */
    CharArray replaceAll(Char original, Char replacement, int startPosition);

    /**
     * Replace all occurences of given chars.
     *
     * @param original value to replace
     * @param replacement replacement value
     * @return new Chars with replaced texts
     */
    CharArray replaceAll(CharArray original, CharArray replacement);

    /**
     * Replace all occurences of given chars starting from given position.
     *
     * @param original value to replace
     * @param replacement replacement value
     * @param startPosition start position
     * @return new Chars with replaced texts
     */
    CharArray replaceAll(CharArray original, CharArray replacement, int startPosition);

    /**
     * Replace all occurences of given value.
     *
     * @param original value to replace
     * @param replacement replacement value
     * @return new Chars with replaced texts
     */
    CharArray replaceAll(byte[] original, byte[] replacement);

    /**
     * Replace all occurences starting from given position.
     *
     * @param original value to replace
     * @param replacement replacement value
     * @param startPosition start position
     * @return new Chars with replaced texts
     */
    CharArray replaceAll(byte[] original, byte[] replacement, int startPosition);

    /**
     * Get the char array as bytes in it's current encoding.
     * @return byte array
     */
    byte[] toBytes();

    /**
     * Get the char array as bytes in given encoding.
     * @param encoding target encoding
     * @return byte array
     */
    byte[] toBytes(CharEncoding encoding);

    /**
     * @return true if all characters are uppercase
     */
    boolean isUpperCase();

    /**
     * @param lf Character case
     * @return true if all characters are uppercase
     */
    boolean isUpperCase(CharCase lf);

    /**
     * @return true if all characters are lowercase
     */
    boolean isLowerCase();

    /**
     * @param lf Character case
     * @return true if all characters are lowercase
     */
    boolean isLowerCase(CharCase lf);

    CharArray toUpperCase();

    CharArray toUpperCase(CharCase lf);

    CharArray toLowerCase();

    CharArray toLowerCase(CharCase lf);

    /**
     * Trim start and end of the char sequence.
     * @return trimmed char sequence
     */
    CharArray trim();

    /**
     * Trim start of the char sequence.
     * @return trimmed char sequence
     */
    CharArray trimStart();

    /**
     * Trim end of the char sequence.
     * @return trimmed char sequence
     */
    CharArray trimEnd();

    CharArray concat(int unicode);

    CharArray concat(Char other);

    CharArray concat(CharArray other);

    /**
     * Encode this character sequence in a different encoding.
     * @param encoding target encoding
     * @return CharSequence
     */
    CharArray recode(final CharEncoding encoding);

    /**
     * Create a char iterator.
     * @return CharIterator
     */
    CharIterator createIterator();

    /**
     * Create a char iterator from given character.
     * @param fromIndex starting character index, included
     * @param toIndex ending character index, excluded.
     * @return CharIterator
     */
    CharIterator createIterator(int fromIndex, int toIndex);

    Chars toChars();

    String toJVMString();

    /**
     * Default equals method.
     * Case sensitive and encoding unsensitive.
     * @param obj , object to test, can be null
     * @return true if equals
     */
    boolean equals(Object obj);

    /**
     * Equals method with current default language.
     * @param obj , object to test, can be null
     * @param ignoreCase set to true to ignore case
     * @param ignoreEncoding set to true to ignore encoding
     * @return true if equals
     */
    boolean equals(Object obj, boolean ignoreCase, boolean ignoreEncoding);

    /**
     * Equals method with current default language.
     * @param obj , object to test, can be null
     * @param ignoreCase set to true to ignore case
     * @param ignoreEncoding set to true to ignore encoding
     * @param language language to use for case test, if null will use default language.
     * @return true if equals
     */
    boolean equals(Object obj, boolean ignoreCase, boolean ignoreEncoding, CharCase language);

    int getHash();

    /**
     * @param charIndex charecter index
     * @return byte offset for given char index.
     */
    int toByteOffset(int charIndex);

    CharArray[] split(int unicode);

    /**
     * Order is not alphabetic, order matches unicode codepoint values order, this
     * guarantee a perfectly regular ordering whatever encoding and characters used.
     * - Smaller unicode codepoint values are placed before.
     * - Shorter chars are placed before.
     *
     * @param other object to compare with
     * @return -1,0,+1 if current object is before,equal or after given one.
     */
    int order(Object other) ;

}
