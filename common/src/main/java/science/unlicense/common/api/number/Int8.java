
package science.unlicense.common.api.number;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.ParseRuntimeException;
import science.unlicense.common.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public final class Int8 extends AbstractNumber {

    public static final NumberType TYPE = new NumberType() {

        @Override
        public Number create(Arithmetic value) {
            if (!(value instanceof Number)) throw new UnsupportedOperationException();
            return new Int8((byte) ((Number) value).toInteger());
        }

        @Override
        public Class getPrimitiveClass() {
            return byte.class;
        }

        @Override
        public Class getValueClass() {
            return Int8.class;
        }

        @Override
        public int getSizeInBits() {
            return 8;
        }

        @Override
        public int getSizeInBytes() {
            return 1;
        }

        @Override
        public int getPrimitiveCode() {
            return Primitive.INT8;
        }
    };

    private final byte value;

    public Int8(byte value) {
        this.value = value;
    }

    @Override
    public NumberType getType() {
        return TYPE;
    }

    @Override
    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public int toInteger() {
        return value;
    }

    @Override
    public long toLong() {
        return value;
    }

    @Override
    public float toFloat() {
        return value;
    }

    @Override
    public double toDouble() {
        return value;
    }

    public static byte decode(CharArray candidate) throws ParseRuntimeException {
        //TODO
        try {
            return Byte.parseByte(candidate.toString());
        } catch (NumberFormatException ex) {
            throw new ParseRuntimeException(ex);
        }
    }

    @Override
    public Chars toChars() {
        return Int32.encode(value);
    }

    @Override
    public int order(Object other) {
        int of = ((Number) other).toInteger();
        return value < of ? -1 : (value > of ? +1 : 0) ;
    }

}
