
package science.unlicense.common.api.model.tree;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DefaultNodeCardinality extends CObject implements NodeCardinality {

    private Chars name;
    private CharArray title;
    private CharArray description;
    private NodeType type;
    private int minOcc;
    private int maxOcc;

    public DefaultNodeCardinality(Chars name, CharArray title, CharArray description, NodeType type, int minOcc, int maxOcc) {
        CObjects.ensureNotNull(name);
        CObjects.ensureNotNull(type);
        this.name = name;
        this.title = title;
        this.description = description;
        this.type = type;
        this.minOcc = minOcc;
        this.maxOcc = maxOcc;
    }

    public DefaultNodeCardinality(NodeType type, int minOcc, int maxOcc) {
        this(type.getId(),type.getTitle(),type.getDescription(),type,minOcc,maxOcc);
    }

    /**
     * Create node cardinality with it's type directly (type will have the same name.
     * @param name cardinality name
     * @param title
     * @param description description
     * @param binding value class
     * @param defaultValue default value
     * @param nullable if value can be null
     * @param minOcc minimum number of occurence
     * @param maxOcc maximum number of occurence
     * @param descriptors sub descriptors
     */
    public DefaultNodeCardinality(Chars name, CharArray title, CharArray description,Class binding, Object defaultValue,
            boolean nullable, int minOcc, int maxOcc, NodeCardinality[] descriptors) {
        this(new DefaultNodeType(name, title, description, binding, defaultValue, nullable, descriptors),minOcc,maxOcc);
    }

    public DefaultNodeCardinality(Chars name, CharArray title, CharArray description,Class binding,
            boolean nullable, NodeCardinality[] descriptors) {
        this(new DefaultNodeType(name, title, description, binding, null, nullable, descriptors),1,1);
    }

    public DefaultNodeCardinality(Chars name, CharArray title, CharArray description,Class binding, Object defaultValue,
            boolean nullable, NodeCardinality[] descriptors) {
        this(new DefaultNodeType(name, title, description, binding, defaultValue, nullable, descriptors),1,1);
    }

    public Chars getId() {
        return name;
    }

    public void setName(Chars name) {
        this.name = name;
    }

    public CharArray getTitle() {
        return title;
    }

    public void setTitle(CharArray title) {
        this.title = title;
    }

    public CharArray getDescription() {
        return description;
    }

    public void setDescription(Chars description) {
        this.description = description;
    }

    public NodeType getType() {
        return type;
    }

    public void setType(NodeType type) {
        this.type = type;
    }

    public int getMinOccurences() {
        return minOcc;
    }

    public void setMinimumOccurrences(int minOcc) {
        this.minOcc = minOcc;
    }

    public int getMaxOccurences() {
        return maxOcc;
    }

    public void setMaximumOccurrences(int maxOcc) {
        this.maxOcc = maxOcc;
    }

    public Chars toChars() {
        return toCharsTree(3);
    }

    public Chars toCharsTree(int depth) {
        return getId().concat(' ').concat(getType().toCharsTree(depth));
    }

}
