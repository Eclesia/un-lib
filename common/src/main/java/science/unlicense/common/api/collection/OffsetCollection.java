
package science.unlicense.common.api.collection;

/**
 *
 * @author Johann Sorel
 */
class OffsetCollection extends AbstractCollection {

    private final Collection base;
    private final int offset;

    public OffsetCollection(Collection base, int offset) {
        this.base = base;
        this.offset = offset;
    }

    @Override
    public int getSize() {
        final int size = base.getSize();
        return size<offset ? 0 : size-offset;
    }

    @Override
    public Iterator createIterator() {
        return new OffsetIterator(base.createIterator(), offset);
    }

}
