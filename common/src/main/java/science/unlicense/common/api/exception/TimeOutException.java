/*
  Originally written by Doug Lea and released into the public domain.
  This may be used for any purposes whatsoever without acknowledgment.
  Thanks for the assistance and support of Sun Microsystems Labs,
  and everyone contributing, testing, and using this code.

  History:
  Date       Who                What
  29Jun1998  dl               Create public version
   4Aug1998  dl               Change to extend InterruptedException
 */
package science.unlicense.common.api.exception;

/**
 * Raised by operations which have reached a defined time out value.
 *
 * @author Doug Lea (Original author)
 * @author Johann Sorel
 */
public class TimeOutException extends InterruptionException {

    /**
     * The approximate time that the operation lasted before this timeout
     * exception was thrown.
     *
     */
    public final long duration;

    /**
     * Constructs a TimeoutException with given duration value.
     *
     */
    public TimeOutException(long time) {
        duration = time;
    }

    /**
     * Constructs a TimeoutException with the specified duration value and
     * detail message.
     */
    public TimeOutException(long time, String message) {
        super(message);
        duration = time;
    }

}
