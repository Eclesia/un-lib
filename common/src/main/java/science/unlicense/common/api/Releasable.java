
package science.unlicense.common.api;

/**
 * Releasable resource.
 *
 * @author Johann Sorel
 */
public interface Releasable {

    void release();

}
