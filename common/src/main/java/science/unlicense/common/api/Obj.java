
package science.unlicense.common.api;

import science.unlicense.common.api.character.Chars;


/**
 *
 * @author Johann Sorel
 */
public interface Obj {

    int getHash();

    /**
     * Get a character representation of this object.
     * @return Chars, never null.
     */
    Chars toChars();

}
