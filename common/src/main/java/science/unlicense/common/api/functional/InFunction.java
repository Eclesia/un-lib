
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface InFunction extends UnsafeInFunction {

    void perform(Object input);

}
