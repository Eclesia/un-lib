
package science.unlicense.common.api.model.doc;

import science.unlicense.common.api.Orderable;

/**
 * Constraint are used to enforce parameters validity.
 * A logic constraint can be used as example for self-exclusive parameters (choices).
 *
 * @author Johann Sorel
 */
public interface RangeConstraint extends Constraint {

    /**
     * Parameter minimum value.
     *
     * @return Object or null.
     */
    Orderable getMinimum();

    /**
     * @return true if minimum is inclusive.
     */
    boolean isMinimumInclusive();

    /**
     * Parameter maximum value.
     *
     * @return Object or null.
     */
    Orderable getMaximum();

    /**
     * @return true if maximum is inclusive.
     */
    boolean isMaximumInclusive();

    /**
     * Test if an object is valid, included in the range.
     *
     * @param candidate object to test
     * @return true if given object in this range.
     */
    boolean isValid(Object candidate);

}
