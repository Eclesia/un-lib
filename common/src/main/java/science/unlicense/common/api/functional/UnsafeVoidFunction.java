
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface UnsafeVoidFunction {

    void perform() throws Exception;

}
