
package science.unlicense.common.api.character;

import science.unlicense.common.api.character.encoding.Iso8859;
import science.unlicense.common.api.character.encoding.ShilftJIS;
import science.unlicense.common.api.character.encoding.UsAscii;
import science.unlicense.common.api.character.encoding.Utf16be;
import science.unlicense.common.api.character.encoding.Utf16le;
import science.unlicense.common.api.character.encoding.Utf32be;
import science.unlicense.common.api.character.encoding.Utf32le;
import science.unlicense.common.api.character.encoding.Utf8;
import science.unlicense.common.api.character.encoding.Windows1252;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.exception.InvalidArgumentException;

/**
 * List of all supported Encodings.
 *
 * @author Johann Sorel
 */
public final class CharEncodings {

    private CharEncodings() {}

    public static final CharEncoding SHIFT_JIS = new ShilftJIS();

    public static final CharEncoding WINDOWS_1252 = new Windows1252();

    public static final CharEncoding US_ASCII = new UsAscii();

    /** RFC 3629 */
    public static final CharEncoding UTF_8 = new Utf8();

    /** RFC 2781 */
    public static final CharEncoding UTF_16 = new Utf16be();

    /** RFC 2781 */
    public static final CharEncoding UTF_16BE = UTF_16;

    /** RFC 2781 */
    public static final CharEncoding UTF_16LE = new Utf16le();

    public static final CharEncoding UTF_32 = new Utf32be();

    public static final CharEncoding UTF_32BE = UTF_32;

    public static final CharEncoding UTF_32LE = new Utf32le();

    public static final CharEncoding ISO_8859_1 = Iso8859.ISO_8859_1;

    public static final CharEncoding ISO_8859_2 = Iso8859.ISO_8859_2;

    public static final CharEncoding ISO_8859_3 = Iso8859.ISO_8859_3;

    public static final CharEncoding ISO_8859_4 = Iso8859.ISO_8859_4;

    public static final CharEncoding ISO_8859_5 = Iso8859.ISO_8859_5;

    public static final CharEncoding ISO_8859_6 = Iso8859.ISO_8859_6;

    public static final CharEncoding ISO_8859_7 = Iso8859.ISO_8859_7;

    public static final CharEncoding ISO_8859_8 = Iso8859.ISO_8859_8;

    public static final CharEncoding ISO_8859_9 = Iso8859.ISO_8859_9;

    public static final CharEncoding ISO_8859_10 = Iso8859.ISO_8859_10;

    public static final CharEncoding ISO_8859_11 = Iso8859.ISO_8859_11;

    public static final CharEncoding ISO_8859_13 = Iso8859.ISO_8859_13;

    public static final CharEncoding ISO_8859_14 = Iso8859.ISO_8859_14;

    public static final CharEncoding ISO_8859_15 = Iso8859.ISO_8859_15;

    public static final CharEncoding ISO_8859_16 = Iso8859.ISO_8859_16;

    /**
     * Default encoding to be used in application.
     * Set to UTF-8.
     */
    public static final CharEncoding DEFAULT = UTF_8;

    private static final Dictionary ENCODINGS = new HashDictionary();
    static {
        ENCODINGS.add(SHIFT_JIS.getName(), SHIFT_JIS);
        ENCODINGS.add(WINDOWS_1252.getName(), WINDOWS_1252);
        ENCODINGS.add(US_ASCII.getName(), US_ASCII);
        ENCODINGS.add(UTF_8.getName(), UTF_8);
        ENCODINGS.add(UTF_16.getName(), UTF_16);
        ENCODINGS.add(UTF_16BE.getName(), UTF_16BE);
        ENCODINGS.add(UTF_16LE.getName(), UTF_16LE);
        ENCODINGS.add(UTF_32.getName(), UTF_32);
        ENCODINGS.add(UTF_32BE.getName(), UTF_32BE);
        ENCODINGS.add(UTF_32LE.getName(), UTF_32LE);
        ENCODINGS.add(ISO_8859_1.getName(), ISO_8859_1);
        ENCODINGS.add(ISO_8859_2.getName(), ISO_8859_2);
        ENCODINGS.add(ISO_8859_3.getName(), ISO_8859_3);
        ENCODINGS.add(ISO_8859_4.getName(), ISO_8859_4);
        ENCODINGS.add(ISO_8859_5.getName(), ISO_8859_5);
        ENCODINGS.add(ISO_8859_6.getName(), ISO_8859_6);
        ENCODINGS.add(ISO_8859_7.getName(), ISO_8859_7);
        ENCODINGS.add(ISO_8859_8.getName(), ISO_8859_8);
        ENCODINGS.add(ISO_8859_9.getName(), ISO_8859_9);
        ENCODINGS.add(ISO_8859_10.getName(), ISO_8859_10);
        ENCODINGS.add(ISO_8859_11.getName(), ISO_8859_11);
        ENCODINGS.add(ISO_8859_13.getName(), ISO_8859_13);
        ENCODINGS.add(ISO_8859_14.getName(), ISO_8859_14);
        ENCODINGS.add(ISO_8859_15.getName(), ISO_8859_15);
        ENCODINGS.add(ISO_8859_16.getName(), ISO_8859_16);
    }

    public static CharEncoding find(Chars name) {
        final CharEncoding value = (CharEncoding) ENCODINGS.getValue(name);
        if (value == null) throw new InvalidArgumentException("Encoding "+name+" not found");
        return value;
    }

}
