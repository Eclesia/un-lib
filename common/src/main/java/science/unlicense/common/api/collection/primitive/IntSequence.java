
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.AbstractSequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.InvalidIndexException;

/**
 *
 * @author Johann Sorel
 */
public final class IntSequence extends AbstractSequence implements PrimitiveSequence{

    private int[] buffer;
    private int size = 0;
    private int nextIndex = 0;

    public IntSequence() {
        buffer = new int[1000];
        size = 0;
    }

    public IntSequence(int[] buffer) {
        CObjects.ensureNotNull(buffer);
        this.buffer = buffer;
        this.size = buffer.length;
    }

    private void growIfNecessary(int length) {
        final int nindex = nextIndex+length;
        if (nindex >= buffer.length) {
            int gs = Math.max(length, buffer.length*3);
            buffer = Arrays.resize(buffer, buffer.length+gs);
        }
        if (nindex>=size) size = nindex;
    }

    /**
     * move one pixel ahead preserving buffer value or
     * filling it with zero if buffer was resized.
     */
    public IntSequence skip() {
        growIfNecessary(1);
        nextIndex++;
        return this;
    }

    public IntSequence put(int b) {
        growIfNecessary(1);
        buffer[nextIndex] = b;
        nextIndex++;
        return this;
    }

    public IntSequence put(int[] b) {
        return put(b, 0, b.length);
    }

    public IntSequence put(int[] b, int offset, int length) {
        final int nindex = nextIndex+length;
        growIfNecessary(length);
        Arrays.copy(b, offset, length, buffer, nextIndex);
        nextIndex = nindex;
        return this;
    }

    /**
     * @return the underlying array,
     * it's size might be larger then the current buffer size.
     */
    public int[] getBackArray() {
        return buffer;
    }

    /**
     * @return int array
     */
    public int[] toArrayInt() {
        return Arrays.copy(buffer, 0, size, new int[size], 0);
    }

    public Object[] toArray() {
        final Object[] array = new Object[size];
        for (int i=0;i<array.length;i++) {
            array[i] = buffer[i];
        }
        return array;
    }

    public int read(int index) {
        return buffer[index];
    }

    public int read(int index, int[] buffer, int offset, int length) {
        int nb = length;
        if (getSize()-index < nb) nb = getSize()-index;

        Arrays.copy(this.buffer, index, nb, buffer, offset);

        return nb;
    }

    /**
     * Test if byte buffer starts by a given array.
     * @param buffer start array
     * @return true if the buffer starts with the given serie of bytes.
     */
    public boolean startWidth(int[] buffer) {
        return startWidth(buffer, 0, buffer.length);
    }

    /**
     * Test if byte buffer starts by a given array.
     * @param buffer start array
     * @param offset start offset
     * @param length number of elements to test
     * @return true if the buffer starts with the given serie of bytes.
     */
    public boolean startWidth(int[] buffer, int offset, int length) {
        return Arrays.equals(buffer, offset, length, this.buffer, 0);
    }

    /**
     * Test if byte buffer ends by a given array.
     * @param buffer end array
     * @return true if the buffer ends with the given serie of bytes.
     */
    public boolean endWidth(int[] buffer) {
        return endWidth(buffer, 0, buffer.length);
    }

    /**
     * Test if byte buffer ends by a given array.
     * @param buffer end array
     * @param offset end offset
     * @param length number of elements to test
     * @return true if the buffer ends with the given serie of bytes.
     */
    public boolean endWidth(int[] buffer, int offset, int length) {
        return Arrays.equals(buffer, offset, length, this.buffer, size-length);
    }

    /**
     * Remove given number of bytes at buffer start.
     * @param nbbyte number of bytes to remove
     */
    public void trimStart(int nbbyte) {
        Arrays.copy(buffer, nbbyte, size-nbbyte, buffer, 0);
        nextIndex -= nbbyte;
        size -= nbbyte;
    }

    /**
     * Remove given number of bytes at buffer end.
     * @param nbbyte number of bytes to remove
     */
    public void trimEnd(int nbbyte) {
        nextIndex -= nbbyte;
        size -= nbbyte;
    }

    public int getSize() {
        return size;
    }

    public boolean removeAll() {
        final boolean hasListener = hasListeners();
        Object[] removed = null;
        if (hasListener) {
            removed = toArray();
        }

        nextIndex = 0;
        size = 0;
        buffer = new int[1000];

        if (hasListener) {
            fireRemove(-1, -1, removed);
        }
        return true;
    }

    public boolean contains(int i) {
        return Arrays.contains(buffer, 0, size, i);
    }

    private void growInsertIfNecessary(int length) {
        final int nsize = size+length;
        if (nsize >= buffer.length) {
            int gs = Math.max(length, buffer.length*3);
            buffer = Arrays.resize(buffer, buffer.length+gs);
        }
        if (nsize>=size) size = nsize;
    }

    public IntSequence moveTo(int position) {
        nextIndex = position;
        return this;
    }

    /**
     * Remove the N next elements.
     * @param nbElement
     */
    public boolean removeNext(int nbElement) {
        if (nextIndex+nbElement>size) {
            throw new InvalidArgumentException("Too much elements to remove "+nbElement+", remaining size is "+(size-nextIndex));
        }
        if (nextIndex+nbElement<size) {
            Arrays.copy(buffer, nextIndex+nbElement, nbElement, buffer, nextIndex);
        }
        size -= nbElement;
        return true;
    }

    public IntSequence insert(int b) {
        growInsertIfNecessary(1);
        for (int i=size-1;i>=nextIndex;i--) {
            buffer[i+1] = buffer[i];
        }
        buffer[nextIndex] = b;
        nextIndex++;
        return this;
    }

    public IntSequence insert(int[] b) {
        return insert(b, 0, b.length);
    }

    public IntSequence insert(int[] b, int offset, int length) {
        growInsertIfNecessary(length);
        for (int i=size-1;i>=nextIndex;i--) {
            buffer[i+length] = buffer[i];
        }
        Arrays.copy(b, 0, b.length, buffer, nextIndex);
        return this;
    }

    public Object get(int index) {
        if (index<0 || index>=size) throw new InvalidIndexException(""+index);
        return read(index);
    }

    public boolean add(int index, Object value) {
        growIfNecessary(size + 1);
        for (int i=size-1;i>=index;i--) {
            buffer[i+1] = buffer[i];
        }
        buffer[index] = (Integer) value;
        if (hasListeners()) fireAdd(index, index, new Object[]{value});
        return true;
    }

    public boolean remove(int index) {
        if (index < 0 || index >= size) {
            throw new InvalidIndexException(""+index);
        }
        final Object removedValue = buffer[index];
        if (index+1 < size) {
            Arrays.copy(buffer, index+1, size-index-1, buffer, index);
        }
        size--;
        if (hasListeners()) fireRemove(index, index, new Object[]{removedValue});
        return true;
    }

}
