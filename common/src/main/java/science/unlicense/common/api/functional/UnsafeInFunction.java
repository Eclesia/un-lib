
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface UnsafeInFunction {

    void perform(Object input) throws Exception;

}
