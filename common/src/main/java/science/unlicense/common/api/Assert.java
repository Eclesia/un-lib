
package science.unlicense.common.api;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.AssertException;

/**
 *
 * @author Johann Sorel
 */
public final class Assert {

    private Assert() {

    }

    public static void assertTrue(boolean value) {
        assertTrue(value, null);
    }

    public static void assertTrue(boolean value, String message) {
        if (!value) {
            throw new AssertException(message, true, value);
        }
    }

    public static void assertFalse(boolean value) {
        assertFalse(value, null);
    }

    public static void assertFalse(boolean value, String message) {
        if (value) {
            throw new AssertException(message, false, value);
        }
    }

    public static void assertEquals(boolean expected, boolean value) {
        assertEquals(expected, value, null);
    }

    public static void assertEquals(boolean expected, boolean value, String message) {
        if (expected != value) {
            throw new AssertException(message, expected, value);
        }
    }

    public static void assertEquals(long expected, long value) {
        assertEquals(expected, value, null);
    }

    public static void assertEquals(long expected, long value, String message) {
        if (expected != value) {
            throw new AssertException(message, expected, value);
        }
    }

    public static void assertEquals(double expected, double value) {
        assertEquals(expected, value, null);
    }

    public static void assertEquals(double expected, double value, String message) {
        if (expected != value) {
            throw new AssertException(message, expected, value);
        }
    }

    public static void assertEquals(double expected, double value, double tolerance) {
        assertEquals(expected, value, tolerance, null);
    }

    public static void assertEquals(double expected, double value, double tolerance, String message) {
        if (expected != value) {
            if (Math.abs(expected-value) > tolerance ) {
                throw new AssertException(message, expected,value);
            }
        }
    }

    public static void assertEquals(java.lang.Object expected, java.lang.Object value) {
        assertEquals(expected, value, null);
    }

    public static void assertEquals(java.lang.Object expected, java.lang.Object value, String message) {
        if (expected == null) {
            if (value != null) {
                throw new AssertException(message, expected, value);
            }
        } else if (!expected.equals(value)) {
            if (expected instanceof CharArray && value instanceof CharArray) {
                CharArray ex = (CharArray) expected;
                CharArray re = (CharArray) value;
                ex = ex.replaceAll(new Chars("\n"), new Chars("\\n"));
                re = re.replaceAll(new Chars("\n"), new Chars("\\n"));
                expected = ex;
                value = re;
            }

            throw new AssertException(message, expected, value);
        }
    }

    public static void assertArrayEquals(byte[] expected, byte[] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(byte[] expected, byte[] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException( ((message == null) ? "" : (message+"\n"))+"Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(short[] expected, short[] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(short[] expected, short[] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException( ((message == null) ? "" : (message+"\n"))+"Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(int[] expected, int[] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(int[] expected, int[] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException( ((message == null) ? "" : (message+"\n"))+"Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(long[] expected, long[] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(long[] expected, long[] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException( ((message == null) ? "" : (message+"\n"))+"Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(float[] expected, float[] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(float[] expected, float[] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException( ((message == null) ? "" : (message+"\n"))+"Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(float[] expected, float[] value, float tolerance) {
        assertArrayEquals(expected, value, tolerance, null);
    }

    public static void assertArrayEquals(float[] expected, float[] value, float tolerance, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                if (Math.abs(expected[i]-value[i]) > tolerance ) {
                    throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
                }
            }
        }
    }

    public static void assertArrayEquals(double[] expected, double[] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(double[] expected, double[] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                throw new AssertException( ((message == null) ? "" : (message+"\n"))+"Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(double[] expected, double[] value, double tolerance) {
        assertArrayEquals(expected, value, tolerance, null);
    }

    public static void assertArrayEquals(double[] expected, double[] value, double tolerance, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            if (expected[i] != value[i]) {
                if (Math.abs(expected[i]-value[i]) > tolerance ) {
                    throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
                }
            }
        }
    }

    public static void assertArrayEquals(java.lang.Object[] expected, java.lang.Object[] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(java.lang.Object[] expected, java.lang.Object[] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {

            if (expected[i] == null) {
                if (value[i] != null) {
                    throw new AssertException( ((message == null) ? "" : (message+"\n"))+"Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
                }
            } else if (!expected[i].equals(value[i])) {
                throw new AssertException( ((message == null) ? "" : (message+"\n"))+"Arrays values differ at index "+i+" expected "+expected[i]+" but was "+value[i]);
            }
        }
    }

    public static void assertArrayEquals(byte[][] expected, byte[][] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(byte[][] expected, byte[][] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            assertArrayEquals(expected[i], value[i], message);
        }
    }

    public static void assertArrayEquals(short[][] expected, short[][] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(short[][] expected, short[][] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            assertArrayEquals(expected[i], value[i], message);
        }
    }

    public static void assertArrayEquals(int[][] expected, int[][] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(int[][] expected, int[][] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            assertArrayEquals(expected[i], value[i], message);
        }
    }

    public static void assertArrayEquals(long[][] expected, long[][] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(long[][] expected, long[][] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            assertArrayEquals(expected[i], value[i], message);
        }
    }

    public static void assertArrayEquals(float[][] expected, float[][] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(float[][] expected, float[][] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            assertArrayEquals(expected[i], value[i], message);
        }
    }

    public static void assertArrayEquals(float[][] expected, float[][] value, float tolerance) {
        assertArrayEquals(expected, value, tolerance, null);
    }

    public static void assertArrayEquals(float[][] expected, float[][] value, float tolerance, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            assertArrayEquals(expected[i], value[i], tolerance, message);
        }
    }

    public static void assertArrayEquals(double[][] expected, double[][] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(double[][] expected, double[][] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            assertArrayEquals(expected[i], value[i], message);
        }
    }

    public static void assertArrayEquals(double[][] expected, double[][] value, double tolerance) {
        assertArrayEquals(expected, value, tolerance, null);
    }

    public static void assertArrayEquals(double[][] expected, double[][] value, double tolerance, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            assertArrayEquals(expected[i], value[i], tolerance, message);
        }
    }

    public static void assertArrayEquals(java.lang.Object[][] expected, java.lang.Object[][] value) {
        assertArrayEquals(expected, value, null);
    }

    public static void assertArrayEquals(java.lang.Object[][] expected, java.lang.Object[][] value, String message) {
        if (expected.length != value.length) throw new AssertException(((message == null) ? "" : (message+"\n"))+"Arrays sizes differ expected "+expected.length+" but was "+value.length);
        for (int i=0;i<expected.length;i++) {
            assertArrayEquals(expected[i], value[i], message);
        }
    }

    public static void assertNull(java.lang.Object next) {
        assertNull(next, "Expected null value");
    }

    public static void assertNull(java.lang.Object next, String message) {
        if (next != null) {
            throw new AssertException(message);
        }
    }

    public static void assertNotNull(java.lang.Object next) {
        assertNotNull(next, "Expected not null value");
    }

    public static void assertNotNull(java.lang.Object next, String message) {
        if (next == null) {
            throw new AssertException(message);
        }
    }

    public static void assertIdentity(java.lang.Object expected, java.lang.Object value) {
        assertEquals(expected, value, null);
    }

    public static void assertIdentity(java.lang.Object expected, java.lang.Object value, String message) {
        if (expected != value) {
            throw new AssertException(message, expected, value);
        }
    }

    public static void assertInstance(java.lang.Object next, Class clazz) {
        assertInstance(next, clazz, null);
    }

    public static void assertInstance(java.lang.Object next, Class clazz, String message) {
        if (!clazz.isInstance(next)) {
            if (message != null) {
                throw new AssertException(message);
            } else if (next == null) {
                throw new AssertException("Object is null");
            } else {
                throw new AssertException("Expected an instanceof of " + clazz.getName() + " but was " + next.getClass().getName());
            }
        }
    }

    public static void fail(){
        throw new AssertException();
    }

    public static void fail(String message){
        throw new AssertException(message);
    }

    public static void fail(String message, Throwable cause){
        throw new AssertException(message,cause);
    }

}
