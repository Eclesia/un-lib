
package science.unlicense.common.api.regex.state;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DefaultFastForward extends AbstractForward implements FastForward {

    public DefaultFastForward() {
    }

    public Chars toChars() {
        return new Chars("FastForward");
    }

}
