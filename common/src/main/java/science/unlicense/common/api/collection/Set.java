package science.unlicense.common.api.collection;

/**
 * Groupement of distinct elements.
 *
 * @author Johann Sorel
 */
public interface Set extends Collection {

}
