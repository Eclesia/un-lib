
package science.unlicense.common.api.exception;


/**
 * Exception thrown when an assertion fails.
 *
 * @author Johann Sorel
 */
public class AssertException extends RuntimeException {

    public AssertException() {
    }

    public AssertException(String message) {
        super(message);
    }

    public AssertException(Object expected, Object value) {
        super("Expected value : "+expected+" but was : "+value);
    }

    public AssertException(String message, Object expected, Object value) {
        super( ((message == null) ? "" : (message+"\n")) + "Expected value : "+expected+" but was : "+value);
    }

    public AssertException(String message, Throwable cause) {
        super(message,cause);
    }

}
