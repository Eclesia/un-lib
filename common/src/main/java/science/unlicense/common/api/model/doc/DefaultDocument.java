
package science.unlicense.common.api.model.doc;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.predicate.Variable;
import science.unlicense.common.api.predicate.VariableSyncException;
import science.unlicense.common.api.predicate.Variables;
import science.unlicense.common.api.predicate.Variables.VarSync;

/**
 * Default document implementation using an HashDictionary as backend.
 *
 * @author Johann Sorel
 */
public class DefaultDocument extends AbstractDocument {

    /**
     * Stored properties values.
     * CAUTION : modify with care, send events when necessary.
     */
    protected final Dictionary properties;

    public DefaultDocument() {
        this(false);
    }

    public DefaultDocument(boolean preserveOrder) {
        this(preserveOrder,null);
    }

    /**
     * Create a document, do not preserve field order.
     *
     * @param docType
     */
    public DefaultDocument(DocumentType docType) {
        this(false,docType);
    }

    public DefaultDocument(boolean preserveOrder, DocumentType docType) {
        super(docType);
        if (preserveOrder) {
            properties = new OrderedHashDictionary();
        } else {
            properties = new HashDictionary();
        }

        //set the default values
        if (docType!=null) {
            for (FieldType ft : docType.getFields()) {
                final Object dv = ft.getDefaultValue();
                if (dv!=null) {
                    properties.add(ft.getId(), dv);
                }
            }
        }

    }

    @Override
    public Set getPropertyNames() {
        return properties.getKeys();
    }

    @Override
    public Field getProperty(Chars name) {
        return new IProperty(name);
    }

    @Override
    public Object getPropertyValue(Chars id) {
        return properties.getValue(id);
    }

    @Override
    public boolean setPropertyValue(Chars name, Object value) {
        //check field exist
        if (docType!=null && docType.isStrict() && docType.getField(name)==null) {
            throw new InvalidArgumentException("No field for name : "+name);
        }
        final DocMessage.FieldChange evt = ((IProperty) getProperty(name)).setValueAndEvent(value);
        if (evt!=null) {
            sendDocEvent(evt);
            return true;
        }
        return false;
    }

    public void set(Document doc) {
        final Collection baseNames = new ArraySequence(getPropertyNames());

        final Sequence changes = hasListeners() ? new ArraySequence() : null;

        //replace properties
        final Iterator docIte = doc.getPropertyNames().createIterator();
        while (docIte.hasNext()) {
            final Chars name = (Chars) docIte.next();
            final DocMessage.FieldChange c = ((IProperty) getProperty(name)).setValueAndEvent(doc.getProperty(name).getValue());
            if (changes!=null && c!=null) changes.add(c);
            baseNames.remove(name);
        }

        //remove properties that do not exist any more
        final Iterator ite = baseNames.createIterator();
        while (ite.hasNext()) {
            final Chars name = (Chars) ite.next();
            final DocMessage.FieldChange c = ((IProperty) getProperty(name)).setValueAndEvent(null);
            if (changes!=null && c!=null) changes.add(c);
        }

        sendDocEvent(changes);
    }

    public void removeProperty(Chars id) {
        properties.remove(id);
    }

    public void removeAllProperties() {
        properties.removeAll();
    }

    private class IProperty implements Field, Predicate {

        private final Chars name;

        public IProperty(Chars id) {
            this.name = id;
        }

        public Chars getName() {
            return name;
        }

        public FieldType getType() {
            return (docType==null) ? null : docType.getField(name);
        }

        public EventSource getHolder() {
            return DefaultDocument.this;
        }

        public Object getValue() {
            return properties.getValue(name);
        }

        public void setValue(Object value) {
            final DocMessage.FieldChange c = setValueAndEvent(value);
            sendDocEvent(c);
        }

        public DocMessage.FieldChange setValueAndEvent(Object value) {
            if (value!=null) {
                //check type
                final FieldType type = getType();
                if (type!=null) {
                    if (type.getMaxOccurences()>1) {
                        if (!(value instanceof Collection)) {
                            throw new InvalidArgumentException("Unvalid value class, expected a Collection but was a "+value.getClass());
                         }
                    } else {
                        if (!type.getValueClass().isInstance(value)) {
                            throw new InvalidArgumentException("Unvalid value class, extected a "+type.getValueClass()+" but was a "+value.getClass());
                        }
                    }
                }
            }

            Object base = properties.getValue(name);
            Object old = base;
            if (CObjects.equals(old, value)) return null;

            if (value==null) {
                properties.remove(name);
            } else {
                properties.add(name, value);
            }
            if (hasListeners()) {
                return new DocMessage.FieldChange(name, old, value);
            } else {
                return null;
            }
        }

        public boolean isReadable() {
            return true;
        }

        public boolean isWritable() {
            return true;
        }

        public void sync(Variable target) {
            sync(target, false);
        }

        public void sync(Variable target, boolean readOnly) {
            final VarSync sync = getVarSync();
            if (sync!=null) throw new VariableSyncException("Variable is already synchronized");
            Variables.sync(this, target, readOnly);
        }

        public void unsync() {
            final VarSync sync = getVarSync();
            if (sync!=null) sync.release();
        }

        private VarSync getVarSync() {
            final EventSource holder = DefaultDocument.this;
            final EventListener[] listeners = holder.getListeners(new PropertyPredicate(name));
            if (listeners.length==0) return null;
            for (int i=0;i<listeners.length;i++) {
                if (listeners[i] instanceof VarSync) {
                    final VarSync sync = ((VarSync) listeners[i]);
                    if (sync.getVar1().equals(this)) {
                        return sync;
                    }
                }
            }
            return null;
        }

        public void addListener(EventListener listener) {
            addEventListener(new PropertyPredicate(name), listener);
        }

        public void removeListener(EventListener listener) {
            DefaultDocument.this.removeEventListener(new PropertyPredicate(name), listener);
        }

        public Class[] getEventClasses() {
            return new Class[]{PropertyMessage.class};
        }

        public void addEventListener(Predicate predicate, EventListener listener) {
            DefaultDocument.this.addEventListener(predicate, listener);
        }

        public void removeEventListener(Predicate predicate, EventListener listener) {
            DefaultDocument.this.removeEventListener(predicate, listener);
        }

        public EventListener[] getListeners(Predicate predicate) {
            return DefaultDocument.this.getListeners(predicate);
        }

        @Override
        public Predicate asEventPredicate() {
            return this;
        }

        @Override
        public Boolean evaluate(Object candidate) {
            if (candidate instanceof Event) {
                final EventMessage message = ((Event) candidate).getMessage();
                return message instanceof PropertyMessage && name.equals(((PropertyMessage) message).getPropertyName());
            }
            return false;
        }

    }

    @Override
    public int getHash() {
        return 456413;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultDocument other = (DefaultDocument) obj;

        if (properties.getSize() != other.properties.getSize()) {
            return false;
        }

        //check pairs
        final Iterator ite = properties.getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            Object val = pair.getValue2();
            Object valo = other.properties.getValue(pair.getValue1());
            if (!CObjects.equals(val, valo)) return false;
        }

        return true;
    }

}
