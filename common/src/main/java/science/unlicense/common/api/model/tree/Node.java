package science.unlicense.common.api.model.tree;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.event.EventSource;

/**
 * Basic node.
 *
 * @author Johann Sorel
 */
public interface Node extends EventSource {

    /**
     *
     * @return Sequence of children nodes.
     */
    Collection getChildren();

    /**
     * Indicate if this node can contain sub nodes.
     * @return true if may have sub nodes.
     */
    boolean canHaveChildren();

    /**
     * Get a char sequence representation.
     * This method should describe only this node and not children nodes.
     *
     * @return Chars, not null
     */
    Chars toChars();

}
