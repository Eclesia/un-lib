
package science.unlicense.common.api.character.encoding;

import science.unlicense.common.api.character.AbstractCharEncoding;

/**
 *
 * @author Johann Sorel
 */
public class Utf16le extends AbstractCharEncoding{

    private static final byte nomatch = (byte) 95; //underscore used to match unsupported characters

    public Utf16le() {
        super(new byte[]{'U','T','F','-','1','6','L','E'},false,4);
    }

    public int charlength(byte[] array, int offset) {
        final int W1 = ((array[offset+0] & 0xff) << 0)+
                       ((array[offset+1] & 0xff) << 8);
        if (W1 < 0xD800 || W1 > 0xDFFF) {
            return 2;
        }
        if (!(W1 > 0xD800 && W1 < 0xDBFF)) {
            return 2;
        }
        final int W2 = ((array[offset+2] & 0xff) << 0)+
                       ((array[offset+3] & 0xff) << 8);

        if (W2 < 0xDC00 || W2 > 0xDFFF) {
            return 4;
        }
        return 4;
    }

    public void toUnicode(byte[] array, int[] codePoint, int offset) {
        final int W1 = ((array[offset+0] & 0xff) << 0)+
                       ((array[offset+1] & 0xff) << 8);
        if (W1 < 0xD800 || W1 > 0xDFFF) {
            codePoint[0] = W1;
            codePoint[1] = 2;
            return;
        }
        if (!(W1 > 0xD800 && W1 < 0xDBFF)) {
            //invalid char
            codePoint[0] = nomatch;
            codePoint[1] = 2;
            return;
        }
        final int W2 = ((array[offset+2] & 0xff) << 0)+
                       ((array[offset+3] & 0xff) << 8);

        if (W2 < 0xDC00 || W2 > 0xDFFF) {
            //invalid char
            codePoint[0] = nomatch;
            codePoint[1] = 4;
            return;
        }

        final int U1 = ((W1 & 0x3FF) << 10)+
                       ((W2 & 0x3FF) <<  0);
        final int U = U1 + 0x10000;

        codePoint[0] = U;
        codePoint[1] = 4;
    }

    public byte[] toBytes(int U) {
        if (U < 0x10000) {
            final byte[] array = new byte[2];
            array[0] = (byte) (U >> 0);
            array[1] = (byte) (U >> 8);
            return array;
        }

        if (U > 0xFFFFF) {
            //can't be encoded on 20bit,
            //TODO improve api to allow user to choose behavior in such cases
            throw new RuntimeException("Can not be encoded in UTF-16");
        }

        final byte[] array = new byte[4];
        int U1 = U - 0x10000;
        int W1 = 0xD800 + (U1 >> 10);
        int W2 = 0xDC00 + (U1 & 0x3ff);
        array[0] = (byte) (byte) (W1 >> 0);
        array[1] = (byte) (byte) (W1 >> 8);
        array[2] = (byte) (byte) (W2 >> 0);
        array[3] = (byte) (byte) (W2 >> 8);
        return array;
    }

    public void toBytes(int[] codePoint, byte[] array, int offset) {
        int U = codePoint[0];

        if (U < 0x10000) {
            array[offset+0] = (byte) (U >> 0);
            array[offset+1] = (byte) (U >> 8);
            codePoint[1] = 2;
            return;
        }

        if (U > 0xFFFFF) {
            //can't be encoded on 20bit,
            //TODO improve api to allow user to choose behavior in such cases
            throw new RuntimeException("Can not be encoded in UTF-16");
        }

        codePoint[1] = 4;
        int U1 = U - 0x10000;
        int W1 = 0xD800 + (U1 >> 10);
        int W2 = 0xDC00 + (U1 & 0x3ff);

        array[offset+0] = (byte) (byte) (W1 >> 0);
        array[offset+1] = (byte) (byte) (W1 >> 8);
        array[offset+2] = (byte) (byte) (W2 >> 0);
        array[offset+3] = (byte) (byte) (W2 >> 8);
    }

    public boolean isComplete(byte[] array, int offset, int length) {
        if (length<2) return false;

        final int W1 = ((array[offset+0] & 0xff) << 0)+
                       ((array[offset+1] & 0xff) << 8);
        if (W1 < 0xD800 || W1 > 0xDFFF) {
            return length == 2;
        }
        if (!(W1 > 0xD800 && W1 < 0xDBFF)) {
            return length == 2;
        }
        final int W2 = ((array[offset+2] & 0xff) << 0)+
                       ((array[offset+3] & 0xff) << 8);

        if (W2 < 0xDC00 || W2 > 0xDFFF) {
            return length == 4;
        }
        return length == 4;
    }

}
