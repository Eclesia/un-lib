
package science.unlicense.common.api.model;

/**
 *
 * @author Johann Sorel
 */
public interface MultiplicityType extends Presentable {

    int getMinOccurences();

    int getMaxOccurences();

}
