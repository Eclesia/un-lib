
package science.unlicense.common.api.event;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.DefaultValueType;
import science.unlicense.common.api.model.ValueType;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.predicate.Variable;
import science.unlicense.common.api.predicate.VariableSyncException;
import science.unlicense.common.api.predicate.Variables;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractProperty implements Property, Predicate {

    protected final EventSource source;
    protected final Chars name;
    protected final ValueType valueType;
    protected final boolean readable;
    protected final boolean writable;

    public AbstractProperty(EventSource source, Chars name, Class valueClass, boolean readable, boolean writable) {
        this.source = source;
        this.name = name;
        this.valueType = new DefaultValueType(name, name, name, valueClass, null);
        this.readable = readable;
        this.writable = writable;
    }

    public AbstractProperty(EventSource source, Chars name, ValueType valueType, boolean readable, boolean writable) {
        this.source = source;
        this.name = name;
        this.valueType = valueType;
        this.readable = readable;
        this.writable = writable;
    }

    @Override
    public EventSource getHolder() {
        return source;
    }

    @Override
    public Chars getName() {
        return name;
    }

    @Override
    public ValueType getType() {
        return valueType;
    }

    @Override
    public boolean isReadable() {
        return readable;
    }

    @Override
    public boolean isWritable() {
        return writable;
    }

    @Override
    public void sync(Variable vr) {
        sync(vr, false);
    }

    @Override
    public void sync(Variable vr, boolean readOnly) {
        final Variables.VarSync sync = getVarSync();
        if (sync!=null) throw new VariableSyncException("Variable is already synchronized");
        Variables.sync(this, vr, readOnly);
    }

    @Override
    public void unsync() {
        final Variables.VarSync sync = getVarSync();
        if (sync!=null) sync.release();
    }

    private Variables.VarSync getVarSync() {
        final EventListener[] listeners = source.getListeners(new PropertyPredicate(name));
        if (listeners.length==0) return null;
        for (int i=0;i<listeners.length;i++) {
            if (listeners[i] instanceof Variables.VarSync) {
                final Variables.VarSync sync = ((Variables.VarSync) listeners[i]);
                if (sync.getVar1().equals(this)) {
                    return sync;
                }
            }
        }
        return null;
    }

    @Override
    public void addListener(EventListener listener) {
        addEventListener(new PropertyPredicate(name), listener);
    }

    @Override
    public void removeListener(EventListener listener) {
        source.removeEventListener(new PropertyPredicate(name), listener);
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

    @Override
    public void addEventListener(Predicate predicate, EventListener listener) {
        source.addEventListener(predicate, listener);
    }

    @Override
    public void removeEventListener(Predicate predicate, EventListener listener) {
        source.removeEventListener(predicate, listener);
    }

    @Override
    public EventListener[] getListeners(Predicate predicate) {
        return source.getListeners(predicate);
    }

    @Override
    public Predicate asEventPredicate() {
        return this;
    }

    @Override
    public Boolean evaluate(Object candidate) {
        if (candidate instanceof Event) {
            final EventMessage message = ((Event) candidate).getMessage();
            return message instanceof PropertyMessage && name.equals(((PropertyMessage) message).getPropertyName());
        }
        return false;
    }
}
