
package science.unlicense.common.api.predicate;

import science.unlicense.common.api.CObjects;

/**
 *
 * @author Johann Sorel
 */
public class DefaultVariable extends AbstractVariable{

    private Object value;

    public DefaultVariable() {
    }

    public DefaultVariable(Object value) {
        this.value = value;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        if (CObjects.equals(value, this.value)) return;
        final Object old = this.value;
        this.value = value;
        fireValueChanged(old, value);
    }

}
