
package science.unlicense.common.api.logging;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;

/**
 * Logger.
 *
 * @author Johann Sorel
 */
public interface Logger {

    public static final int LEVEL_CRITICAL = 3;
    public static final int LEVEL_WARNING = 2;
    public static final int LEVEL_INFORMATION = 1;
    public static final int LEVEL_DEBUG = 0;

    /**
     * Describe this logger parameters.
     *
     * @return ParameterType, never null.
     */
    DocumentType getParametersType();

    /**
     * Get this logger configuration.
     *
     * @return Parameter, never null
     */
    Document getParameters();

    /**
     * Replace this logger configuration.
     * Since various implementations of logger may coexist, the configuration
     * may be different so a LogException may be throw.
     *
     * @param config , never null
     * @throws science.unlicense.common.api.logging.LoggerException if configuration is incorrect
     */
    void setParameters(Document config) throws LoggerException;

    /**
     * Shortcut for : log(null,exception,level)
     *
     * @param exception can be null
     * @param level
     */
    void log(Throwable exception, float level);

    /**
     * Shortcut for : log(message,level)
     *
     * @param message can be null
     * @param level
     */
    void log(Chars message, float level);

    /**
     * Log given message and exception
     *
     * @param message can be null
     * @param exception can be null
     * @param level loggin level high values for high importance
     */
    void log(Chars message, Throwable exception, float level);

    /**
     * Shortcut for : log(message,null,LEVEL_DEBUG)
     *
     * @param message can be null
     */
    void debug(Chars message);

    /**
     * Shortcut for : log(message,exception,LEVEL_DEBUG)
     *
     * @param message can be null
     * @param exception can be null
     */
    void debug(Chars message, Throwable exception);

    /**
     * Shortcut for : log(exception,LEVEL_DEBUG)
     *
     * @param exception can be null
     */
    void debug(Throwable exception);

    /**
     * Shortcut for : log(message,null,LEVEL_INFORMATION)
     *
     * @param message can be null
     */
    void info(Chars message);

    /**
     * Shortcut for : log(message,exception,LEVEL_INFORMATION)
     *
     * @param message can be null
     * @param exception can be null
     */
    void info(Chars message, Throwable exception);

    /**
     * Shortcut for : log(exception,LEVEL_INFORMATION)
     *
     * @param exception can be null
     */
    void info(Throwable exception);

    /**
     * Shortcut for : log(message,null,LEVEL_WARNING)
     *
     * @param message can be null
     */
    void warning(Chars message);

    /**
     * Shortcut for : log(message,exception,LEVEL_WARNING)
     *
     * @param message can be null
     * @param exception can be null
     */
    void warning(Chars message, Throwable exception);

    /**
     * Shortcut for : log(exception,LEVEL_WARNING)
     *
     * @param exception can be null
     */
    void warning(Throwable exception);

    /**
     * Shortcut for : log(message,null,LEVEL_CRITICAL)
     *
     * @param message can be null
     */
    void critical(Chars message);

    /**
     * Shortcut for : log(message,exception,LEVEL_CRITICAL)
     *
     * @param message can be null
     * @param exception can be null
     */
    void critical(Chars message, Throwable exception);

    /**
     * Shortcut for : log(exception,LEVEL_CRITICAL)
     *
     * @param exception can be null
     */
    void critical(Throwable exception);

}
