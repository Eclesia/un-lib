
package science.unlicense.common.api.functional;

/**
 *
 * @author Johann Sorel
 */
public interface BinaryOutFunction extends UnsafeBinaryOutFunction {

    Object perform(Object in1, Object in2);

}
