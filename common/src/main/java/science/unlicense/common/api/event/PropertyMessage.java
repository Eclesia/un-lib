
package science.unlicense.common.api.event;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.predicate.VariableMessage;

/**
 * Commonly used event for properties.
 * This event is composed of :
 * - the name of the property
 * - the old property value
 * - the new property value
 *
 * @author Johann Sorel
 */
public class PropertyMessage extends VariableMessage {

    public static final Predicate PREDICATE = new MessagePredicate(PropertyMessage.class);

    private final Chars propertyName;

    /**
     * New property event.
     *
     * @param propertyName the changed property name, not null
     * @param oldValue old property value
     * @param newValue new property value
     */
    public PropertyMessage(Chars propertyName, Object oldValue, Object newValue) {
        super(oldValue, newValue);
        this.propertyName = propertyName;
    }

    /**
     * Get the changed property name.
     *
     * @return property name
     */
    public Chars getPropertyName() {
        return propertyName;
    }

}
