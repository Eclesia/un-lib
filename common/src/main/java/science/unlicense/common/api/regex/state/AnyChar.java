
package science.unlicense.common.api.regex.state;

import science.unlicense.common.api.character.Chars;

/**
 * Case-insensitive char.
 *
 * @author Johann Sorel
 */
public final class AnyChar extends AbstractForward implements Evaluator {

    public AnyChar() {
    }

    public boolean evaluate(Object cp) {
        return true;
    }

    public Chars toChars() {
        return new Chars("AnyChar");
    }

}
