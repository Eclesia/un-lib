
package science.unlicense.common.api.model.doc;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.model.proto.Prototype;

/**
 * The document model is common way to structure object by fields.
 * A document is similar to a dictionary but provide a type which in turn
 * defines the possible fields and their types.
 *
 * @author Johann Sorel
 */
public interface Document extends Prototype {

    /**
     * Get document type.
     *
     * @return Document type, not null
     */
    DocumentType getType();

    /**
     * Get all field names.
     * If the document type is strict then the field names matches the document
     * type fields. If not this collection may contain additional names.
     *
     * @return
     */
    @Override
    Set getPropertyNames();

    /**
     * Get field object.
     *
     * @param name field name
     * @return Field, never null
     * @throws FieldNotFoundException if field does not exist and document type is strict
     */
    @Override
    Field getProperty(Chars name) throws FieldNotFoundException;

    /**
     * Get field value.
     * If field type exist and max occurences > 1, value is a collection.
     *
     * @param name field name, not null
     * @return field value, can be null
     */
    @Override
    Object getPropertyValue(Chars name);

    /**
     * Set field value.
     *
     * @param name field name, not null
     * @param value
     * @throws FieldNotFoundException if field does not exist and document type is strict
     */
    @Override
    boolean setPropertyValue(Chars name, Object value) throws FieldNotFoundException;

    /**
     * Erase properties and copy new values from given document.
     * @param doc Dociment to copy from, not null
     */
    void set(Document doc);
}
