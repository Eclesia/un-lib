
package science.unlicense.common.api.number;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public final class UInt64 extends AbstractNumber {

    public static final NumberType TYPE = new NumberType() {

        @Override
        public Number create(Arithmetic value) {
            if (!(value instanceof Number)) throw new UnsupportedOperationException();
            return new UInt64( ((Number) value).toLong());
        }

        @Override
        public Class getPrimitiveClass() {
            return long.class;
        }

        @Override
        public Class getValueClass() {
            return UInt64.class;
        }

        @Override
        public int getSizeInBits() {
            return 64;
        }

        @Override
        public int getSizeInBytes() {
            return 8;
        }

        @Override
        public int getPrimitiveCode() {
            return Primitive.INT64;
        }
    };

    private final long value;

    public UInt64(long value) {
        this.value = value;
    }

    @Override
    public NumberType getType() {
        return TYPE;
    }

    @Override
    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public int toInteger() {
        return (int) value;
    }

    @Override
    public long toLong() {
        return value;
    }

    @Override
    public float toFloat() {
        return value;
    }

    @Override
    public double toDouble() {
        return value;
    }

    @Override
    public Chars toChars() {
        return Int64.encode(toLong());
    }

    @Override
    public int order(Object other) {
        throw new UnimplementedException();
    }

}
