
package science.unlicense.common.api.event;

import science.unlicense.common.api.CObject;

/**
 * Default event, does not provide additional informations.
 * Extend this class and append specific properties for the event.
 *
 * @author Johann Sorel
 */
public class DefaultEventMessage extends CObject implements EventMessage {

    protected final boolean consumable;
    protected boolean consumed;

    /**
     * Create a default event,
     *
     * @param consumable set to true is event is consumable.
     */
    public DefaultEventMessage(boolean consumable) {
        this.consumable = consumable;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isConsumable() {
        return consumable;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isConsumed() {
        return consumed;
    }

    /**
     * {@inheritDoc}
     */
    public void consume() {
        consumed = consumable;
    }

}
