
package science.unlicense.common.api.character;

import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.number.Int32;

/**
 * Utility class for characters.
 *
 * @author Johann Sorel
 */
public final class Characters {

    private Characters() {}

    public static Char recode(final Char character, final CharEncoding encoding) {
        return new Char(recode(character.data, character.encoding, encoding), encoding);
    }

    public static Chars recode(final CharArray sequence, final CharEncoding encoding) {
        if (sequence instanceof Chars) {
            return new Chars(recode(((Chars) sequence).data, sequence.getEncoding(), encoding), encoding);
        } else {
            return new Chars(recode(sequence.toBytes(), sequence.getEncoding(), encoding), encoding);
        }
    }

    public static byte[] recode(final byte[] array, final CharEncoding sourceEncoding,
            final CharEncoding targetEncoding) {
        final CharConverter converter = getConverter(sourceEncoding, targetEncoding);
        final ByteSequence buffer = new ByteSequence();

        final int[] size = new int[2];
        final byte[] out = new byte[4];
        int offset = 0;
        while (offset<array.length) {
            converter.convert(array, offset, out, 0, size);
            offset += size[0];
            buffer.put(out, 0, size[1]);
        }

        return buffer.toArrayByte();
    }

    /**
     *
     * @param source source encoding
     * @param target target encoding
     * @return CharConverter
     */
    public static CharConverter getConverter(CharEncoding source, CharEncoding target) {
        //Using a converter lets use possibility to improve performances
        return new CharConverter(source, target);
    }

    /**
     * Encode boolean value to a character sequence.
     * @param candidate boolean
     * @return "true" or "false"
     */
    public static Chars encode(boolean candidate) {
        final int code = candidate ? 49 : 48;
        return new Chars(CharEncodings.DEFAULT.toBytes(code),CharEncodings.DEFAULT);
    }

    /***
     * Resolve escaped character starting by a slash.
     *
     * @return resolved chars
     */
    public static Chars resolveEscapes(CharArray text) {
        final CharBuffer cb = new CharBuffer(text.getEncoding());
        final CharIterator ite = text.createIterator();
        while (ite.hasNext()) {
            final int u = ite.nextToUnicode();
            if (u=='\\') {
                int n = ite.nextToUnicode();
                switch(n) {
                    case 't': cb.append(9); break;
                    case 'n': cb.append(10); break;
                    case 'r': cb.append(13); break;
                    case 'u': {
                        cb.append(Int32.decodeHexa(ite, 4));
                        break;
                    }
                    default : throw new CharEncodingException("Unknown escape \\"+(char) n);
                }
            } else {
                cb.append(u);
            }
        }

        return cb.toChars();
    }

    public static Chars toBitChars(int code) {
        final CharBuffer cb = new CharBuffer();

        int mask;
        for (int i=31;i>=0;i--) {
            mask = 1<<i;
            cb.append( ((code & mask) == 0) ? '0' : '1' );
        }

        return cb.toChars();
    }
}
