
package science.unlicense.common.api.predicate;

/**
 * An expression is a dynamic value which evaluation result change
 * based on the evaluated object and context.
 *
 * @author Johann Sorel
 */
public interface Expression {

    /**
     * Evaluation given object in context.
     *
     * @param candidate, object to evaluate
     * @return result of the evaluation
     */
    Object evaluate(Object candidate);

}
