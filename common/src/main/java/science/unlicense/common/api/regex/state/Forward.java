
package science.unlicense.common.api.regex.state;

/**
 * A forward state, is a state with only one next state.
 * Opposite of a fork which have 2 possible states.
 *
 * @author Johann Sorel
 */
public interface Forward extends NFAState {

    NFAState getNextState();

}
