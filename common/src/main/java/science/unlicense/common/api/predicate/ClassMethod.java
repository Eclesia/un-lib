
package science.unlicense.common.api.predicate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.lang.Reflection;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.number.Float64;

/**
 *
 * @author Johann Sorel
 */
public class ClassMethod extends AbstractFunction {

    private final Method method;
    private final boolean varLength;

    public ClassMethod(Method method, Expression[] parameters) {
        super(new Chars(method.getName()), parameters);
        this.method = method;

        final int nbParam = method.getParameterTypes().length;
        if (nbParam==2) {
            //special case if the function support a expression array
            //we don't check the length in this case
            final Class c0 = method.getParameterTypes()[0];
            final Class c1 = method.getParameterTypes()[1];
            if (c0.equals(Object.class) && c1.isArray() && c1.getComponentType().equals(Expression.class)) {
                varLength = true;
                return;
            }
        }

        varLength = false;
        if (parameters.length!=nbParam) {
            throw new RuntimeException("Wrong number of arguments, expected "+nbParam+" but was "+parameters.length);
        }
    }

    public Method getMethod() {
        return method;
    }

    public Object evaluate(Object candidate) {
        if (varLength) {
            try {
                return method.invoke(null, new Object[]{candidate,parameters});
            } catch (IllegalAccessException ex) {
                Loggers.get().info(ex);
            } catch (InvalidArgumentException ex) {
                Loggers.get().info(ex);
            } catch (InvocationTargetException ex) {
                Loggers.get().info(ex);
            }
        }

        final Class[] argTypes = method.getParameterTypes();
        final Object[] values = new Object[argTypes.length];
        for (int i=0;i<argTypes.length;i++) {
            values[i] = parameters[i].evaluate(candidate);
            //we are tolerant to numeric types
            if (values[i] instanceof Number) {
                if (science.unlicense.common.api.number.Number.class.equals(argTypes[i])) {
                    values[i] = new Float64(((Number) values[i]).doubleValue());
                } else {
                    final Class boxingClass = Reflection.getBoxingClass(argTypes[i]);
                    if (Number.class.isAssignableFrom(boxingClass)) {
                        if (Byte.class.equals(boxingClass)) values[i] = ((Number) values[i]).byteValue();
                        else if (Short.class.equals(boxingClass)) values[i] = ((Number) values[i]).shortValue();
                        else if (Integer.class.equals(boxingClass)) values[i] = ((Number) values[i]).intValue();
                        else if (Long.class.equals(boxingClass)) values[i] = ((Number) values[i]).longValue();
                        else if (Float.class.equals(boxingClass)) values[i] = ((Number) values[i]).floatValue();
                        else if (Double.class.equals(boxingClass)) values[i] = ((Number) values[i]).doubleValue();
                    }
                }
            }
        }
        try {
            return method.invoke(null, values);
        } catch (IllegalAccessException ex) {
            Loggers.get().info(ex);
        } catch (InvalidArgumentException ex) {
            Loggers.get().info(ex);
        } catch (InvocationTargetException ex) {
            Loggers.get().info(ex);
        } catch (IllegalArgumentException ex) {
            Loggers.get().info(ex);
        }
        return null;
    }

}
