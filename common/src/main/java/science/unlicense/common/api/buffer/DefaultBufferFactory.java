
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int16;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int64;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt8;

/**
 *
 * @author Johann Sorel
 */
public class DefaultBufferFactory implements BufferFactory {

    public static final DefaultBufferFactory INSTANCE = new DefaultBufferFactory();

    @Override
    public Buffer.Int8 createInt8(long nbValue) {
        return create(nbValue, UInt8.TYPE, Endianness.BIG_ENDIAN).asInt8();
    }

    public Buffer.Int16 createShort(long nbValue) {
        return create(nbValue, Int16.TYPE, Endianness.BIG_ENDIAN).asInt16();
    }

    @Override
    public Buffer.Int32 createInt32(long nbValue) {
        return create(nbValue, Int32.TYPE, Endianness.BIG_ENDIAN).asInt32();
    }

    @Override
    public Buffer.Int64 createInt64(long nbValue) {
        return create(nbValue, Int64.TYPE, Endianness.BIG_ENDIAN).asInt64();
    }

    @Override
    public Buffer.Float32 createFloat32(long nbValue) {
        return create(nbValue, Float32.TYPE, Endianness.BIG_ENDIAN).asFloat32();
    }

    @Override
    public Buffer.Float64 createFloat64(long nbValue) {
        return create(nbValue, Float64.TYPE, Endianness.BIG_ENDIAN).asFloat64();
    }

    public static Buffer wrap(byte[] buffer) {
        return new DefaultInt8Buffer(buffer);
    }

    public static Buffer wrap(short[] buffer) {
        return new DefaultInt16Buffer(buffer);
    }

    public static Buffer wrap(int[] buffer) {
        return new DefaultInt32Buffer(buffer);
    }

    public static Buffer wrap(float[] buffer) {
        return new DefaultFloat32Buffer(buffer);
    }

    public static Buffer wrap(double[] buffer) {
        return new DefaultFloat64Buffer(buffer);
    }

    public static Buffer wrap(byte[] buffer, NumberType primitiveType, Endianness encoding) {
        return new DefaultInt8Buffer(buffer,primitiveType,encoding);
    }

    public Buffer create(long nbValue, NumberType primitiveType, Endianness encoding) {
        if (primitiveType==Int32.TYPE && encoding == Endianness.BIG_ENDIAN) {
            return new DefaultInt32Buffer(new int[(int) nbValue]);
        } else if (primitiveType==Int16.TYPE && encoding == Endianness.BIG_ENDIAN) {
            return new DefaultInt16Buffer(new short[(int) nbValue]);
        } else if (primitiveType==Float32.TYPE && encoding == Endianness.BIG_ENDIAN) {
            return new DefaultFloat32Buffer(new float[(int) nbValue]);
        } else if (primitiveType==Float64.TYPE && encoding == Endianness.BIG_ENDIAN) {
            return new DefaultFloat64Buffer(new double[(int) nbValue]);
        } else {
            return new DefaultInt8Buffer((int) nbValue,primitiveType,encoding);
        }
    }

}
