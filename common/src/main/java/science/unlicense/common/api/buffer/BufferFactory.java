
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author johann Sorel
 */
public interface BufferFactory {

    Buffer create(long nbValue, NumberType numType, Endianness endianness);

    Buffer.Int8 createInt8(long nbValue);

    Buffer.Int32 createInt32(long nbValue);

    Buffer.Int64 createInt64(long nbValue);

    Buffer.Float32 createFloat32(long nbValue);

    Buffer.Float64 createFloat64(long nbValue);

}
