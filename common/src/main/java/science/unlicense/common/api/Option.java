
package science.unlicense.common.api;

/**
 * An optional value.
 *
 * @author Yann D'Isanto
 */
public final class Option {

    /**
     * The undefined optional value
     */
    public static final Option NONE = new Option(null);

    /**
     * the value.
     */
    private final java.lang.Object value;

    /**
     * Create an optional value.
     * @param value the value
     */
    private Option(final java.lang.Object value) {
        this.value = value;
    }

    public java.lang.Object get() {
        return value;
    }

    /**
     * @return true if this value is defined.
     */
    public boolean isDefined() {
        return value != null;
    }

    /**
     * @return true is this value is empty (not defined).
     */
    public boolean isEmpty() {
        return value == null;
    }

    /**
     * @param defaultValue the default value to return if not defined.
     * @return this value if defined, the specified default value otherwise.
     */
    public java.lang.Object getOrElse(java.lang.Object defaultValue) {
        return isDefined() ? value : defaultValue;
    }

    /**
     * Creates an optional value.
     * @param value the value.
     * @return an optional value, NONE if value is null.
     */
    public static Option some(java.lang.Object value) {
        return value != null ? new Option(value) : NONE;
    }

    /**
     * @return the NONE optional value (value is null).
     */
    public static Option none() {
        return NONE;
    }
}
