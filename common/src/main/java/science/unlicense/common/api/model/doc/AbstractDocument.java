
package science.unlicense.common.api.model.doc;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.common.api.model.proto.ProtoDictionnary;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.predicate.Variable;
import science.unlicense.common.api.predicate.VariableSyncException;
import science.unlicense.common.api.predicate.Variables;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractDocument extends AbstractEventSource implements Document {

    protected final DocumentType docType;

    public AbstractDocument() {
        this.docType = null;
    }

    public AbstractDocument(DocumentType docType) {
        this.docType = docType;
    }

    public DocumentType getType() {
        return docType;
    }

    @Override
    public Field getProperty(Chars name) throws FieldNotFoundException {
        return new IProperty(name);
    }

    @Override
    public Dictionary asDictionary() {
        return new ProtoDictionnary(this);
    }

    @Override
    public void set(Document doc) {
        final Object[] names = getPropertyNames().toArray();
        for (Object name : names) {
            setPropertyValue((Chars) name, null);
        }
        final Iterator newnames = doc.getPropertyNames().createIterator();
        while (newnames.hasNext()) {
            final Chars fieldName = (Chars) newnames.next();
            setPropertyValue(fieldName, doc.getPropertyValue(fieldName));
        }
    }

    protected void sendDocEvent(DocMessage.FieldChange fieldEvent) {
        if (fieldEvent==null || !hasListeners()) return;
        final DocMessage msg = new DocMessage(false, new ArraySequence(new Object[]{fieldEvent}));
        getEventManager().sendEvent(new Event(this,msg));
    }

    protected void sendDocEvent(Sequence fieldEvents) {
        if (fieldEvents==null || fieldEvents.isEmpty() || !hasListeners()) return;
        final DocMessage msg = new DocMessage(false, fieldEvents);
        getEventManager().sendEvent(new Event(this,msg));
    }
    @Override
    public Chars toChars() {
        final CharBuffer buffer = new CharBuffer();
        buffer.append("Doc");
        if (docType!=null) buffer.append(" : ").append(docType.getTitle());
        buffer.append(" {");

        final Iterator ite = getPropertyNames().createIterator();
        while (ite.hasNext()) {
            final Chars fieldName = (Chars) ite.next();
            final Object value = getPropertyValue(fieldName);
            buffer.append("\n  ");
            buffer.append(fieldName);
            buffer.append(" = ");
            Chars text = CObjects.toChars(value);
            text = text.replaceAll(new Chars("\n"), new Chars("\n  "));
            buffer.append(text);
        }
        buffer.append("\n}");
        return buffer.toChars();
    }

    private class IProperty implements Field, Predicate {

        private final Chars name;

        public IProperty(Chars id) {
            this.name = id;
        }

        @Override
        public Chars getName() {
            return name;
        }

        @Override
        public FieldType getType() {
            return (docType==null) ? null : docType.getField(name);
        }

        @Override
        public EventSource getHolder() {
            return AbstractDocument.this;
        }

        @Override
        public Object getValue() {
            return getPropertyValue(name);
        }

        @Override
        public void setValue(Object value) {
            setPropertyValue(name, value);
        }

        @Override
        public boolean isReadable() {
            return true;
        }

        @Override
        public boolean isWritable() {
            return true;
        }

        @Override
        public void sync(Variable target) {
            sync(target, false);
        }

        @Override
        public void sync(Variable target, boolean readOnly) {
            final Variables.VarSync sync = getVarSync();
            if (sync!=null) throw new VariableSyncException("Variable is already synchronized");
            Variables.sync(this, target, readOnly);
        }

        @Override
        public void unsync() {
            final Variables.VarSync sync = getVarSync();
            if (sync!=null) sync.release();
        }

        private Variables.VarSync getVarSync() {
            final EventSource holder = AbstractDocument.this;
            final EventListener[] listeners = holder.getListeners(new PropertyPredicate(name));
            if (listeners.length==0) return null;
            for (int i=0;i<listeners.length;i++) {
                if (listeners[i] instanceof Variables.VarSync) {
                    final Variables.VarSync sync = ((Variables.VarSync) listeners[i]);
                    if (sync.getVar1().equals(this)) {
                        return sync;
                    }
                }
            }
            return null;
        }

        @Override
        public void addListener(EventListener listener) {
            addEventListener(new PropertyPredicate(name), listener);
        }

        @Override
        public void removeListener(EventListener listener) {
            AbstractDocument.this.removeEventListener(new PropertyPredicate(name), listener);
        }

        @Override
        public Class[] getEventClasses() {
            return new Class[]{PropertyMessage.class};
        }

        @Override
        public void addEventListener(Predicate predicate, EventListener listener) {
            AbstractDocument.this.addEventListener(predicate, listener);
        }

        @Override
        public void removeEventListener(Predicate predicate, EventListener listener) {
            AbstractDocument.this.removeEventListener(predicate, listener);
        }

        @Override
        public EventListener[] getListeners(Predicate predicate) {
            return AbstractDocument.this.getListeners(predicate);
        }

        @Override
        public Predicate asEventPredicate() {
            return this;
        }

        @Override
        public Boolean evaluate(Object candidate) {
            if (candidate instanceof Event) {
                final EventMessage message = ((Event) candidate).getMessage();
                return message instanceof PropertyMessage && name.equals(((PropertyMessage) message).getPropertyName());
            }
            return false;
        }

    }

}
