
package science.unlicense.common.api.exception;

/**
 * Raised when objects are manipulated in an incorrect concurrency environement.
 *
 * @author Johann Sorel
 */
public class ConcurrencyException extends Exception {

    public ConcurrencyException() {
    }

    public ConcurrencyException(String s) {
        super(s);
    }

    public ConcurrencyException(Throwable cause) {
        super(cause);
    }

    public ConcurrencyException(String message, Throwable cause) {
        super(message, cause);
    }

}
