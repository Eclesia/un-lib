
package science.unlicense.common.api.event;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Hasher;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Convinient class to manage event listeners and send events.
 *
 * @author Johann Sorel
 */
public class EventManager {

    public static final EventListener[] EMPTY_EVENTLISTENER_ARRAY = new EventListener[0];

    private final Dictionary listeners = new HashDictionary();
    private int nbListeners = 0;

    /**
     * Indicate if the manager currently stores listeners.
     *
     * @return true if has listeners, false otherwise
     */
    public boolean hasListeners() {
        return nbListeners>0;
    }

    /**
     * Get number of listeners.
     * @return number of listeners.
     */
    public int getNbListeners() {
        return nbListeners;
    }

    /**
     * Send a property event to listeners.
     *
     * @param source the event source
     * @param name name of the changed property
     * @param oldValue property old value
     * @param newValue property new value
     */
    public void sendPropertyEvent(EventSource source, Chars name, Object oldValue, Object newValue) {
        if (nbListeners==0) return;
        sendEvent(new Event(source, new PropertyMessage(name, oldValue, newValue)));
    }

    /**
     * Send the given event to listeners registered for this class.
     *
     * @param event event to send
     */
    public void sendEvent(Event event) {
        if (nbListeners==0) return;
        final Iterator ite = listeners.getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            final Predicate predicate = (Predicate) pair.getValue1();
            if (predicate.evaluate(event)) {
                final Object listeners = pair.getValue2();
                if (listeners instanceof Collection) {
                    //defensive copy
                    final Object[] array = ((Collection) listeners).toArray();
                    for (int i=0;i<array.length;i++) {
                        ((EventListener) array[i]).receiveEvent(event);
                    }
                } else {
                    ((EventListener) listeners).receiveEvent(event);
                }
            }
        }
    }

    public EventListener[] getListeners(Predicate predicate) {
        if (nbListeners==0) return EMPTY_EVENTLISTENER_ARRAY;
        if (predicate==null) predicate = Predicate.TRUE;
        final Object listeners = this.listeners.getValue(predicate);
        if (listeners==null) {
            return EMPTY_EVENTLISTENER_ARRAY;
        } else if (listeners instanceof Collection) {
            int size = ((Collection) listeners).getSize();
            final EventListener[] array = new EventListener[size];
            Collections.copy((Collection) listeners, array, 0);
            return array;
        } else {
            return new EventListener[]{(EventListener) listeners};
        }
    }

    /**
     * Register a listener for the given type of event.
     *
     * @param predicate, wanted event filter, can be null.
     * @param listener, listener to register, can not be null
     */
    public void addEventListener(Predicate predicate, EventListener listener) {
        CObjects.ensureNotNull(listener);
        if (predicate==null) predicate = Predicate.TRUE;
        Object lst = listeners.getValue(predicate);
        if (lst==null) {
            listeners.add(predicate, listener);
            nbListeners++;
        } else if (lst instanceof Collection) {
            final Collection set = (Collection) lst;
            if (set.add(listener)) {
                nbListeners++;
            }
        } else if (listener != lst) {
            final HashSet set = new HashSet(Hasher.IDENTITY,2);
            set.add(listener);
            set.add(lst);
            listeners.add(predicate, set);
            nbListeners++;
        }
    }

    /**
     * Unregister a listener for the given type of event.
     *
     * @param predicate, wanted event filter, can be null.
     * @param listener, listener to unregister
     */
    public void removeEventListener(Predicate predicate, EventListener listener) {
        if (predicate==null) predicate = Predicate.TRUE;
        final Object lst = listeners.getValue(predicate);
        if (lst instanceof Collection) {
            if (((Collection) lst).remove(listener)) {
                nbListeners--;
            }
        } else if (lst !=null && lst == listener) {
            listeners.remove(predicate);
            nbListeners--;
        }
    }

    /**
     * Unregister all listeners.
     *
     */
    public void removeAllListener() {
        listeners.removeAll();
    }

}
