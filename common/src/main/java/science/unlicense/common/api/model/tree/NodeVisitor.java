
package science.unlicense.common.api.model.tree;

import science.unlicense.common.api.collection.Iterator;

/**
 * Node visitor.
 * Recursively visit each child nodes.
 * Extends this class to add specific operations.
 *
 * @author Johann Sorel
 */
public class NodeVisitor {

    /**
     * Visit the given node.
     *
     * @param node, node to visit, not null
     * @param context, context information object, may be null.
     * @return visit result, may be null
     */
    public Object visit(Node node, Object context) {

        Object result = null;
        final Iterator children = node.getChildren().createIterator();
        while (children.hasNext()) {
            Object next = children.next();
            if (next!=null) {
                result = visit((Node) next, context);
            }
        }
        return result;
    }

}
