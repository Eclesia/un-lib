
package science.unlicense.common.api.collection;

import java.lang.reflect.Array;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.exception.UnmodifiableException;
import science.unlicense.common.api.model.tree.Nodes;

/**
 * Abstract collection, falls back on : createIterator, add(Object), remove(Object)
 * to implements methods :
 * - addAll
 * - removeAll
 * - contains
 * - getSize
 * - isEmpty
 * Also provide convenient method to fire events.
 *
 * Writable collections must override add and remove methods.
 *
 * @author Johann Sorel
 */
public abstract class AbstractCollection extends AbstractEventSource implements Collection{

    public boolean add(Object candidate) {
        throw new UnmodifiableException("Not supported.");
    }

    public boolean remove(Object candidate) {
        throw new UnmodifiableException("Not supported.");
    }

    public boolean addAll(Object[] candidate) {
        for (int i=0;i<candidate.length;i++) {
            add(candidate[i]);
        }
        return true;
    }

    public boolean addAll(Collection candidate) {
        final Iterator ite = candidate.createIterator();
        while (ite.hasNext()) {
            add(ite.next());
        }
        return true;
    }

    public boolean removeAll(Collection candidate) {
        final Iterator ite = candidate.createIterator();
        while (ite.hasNext()) {
            remove(ite.next());
        }
        return true;
    }

    public boolean removeAll(Object[] candidate) {
        for (int i=0;i<candidate.length;i++) {
            remove(candidate[i]);
        }
        return true;
    }

    public boolean removeAll() {
        final Sequence seq = new ArraySequence();
        seq.addAll(this);
        return removeAll(seq);
    }

    public void replaceAll(Collection items) {
        removeAll();
        addAll(items);
    }

    public void replaceAll(Object[] items) {
        removeAll();
        addAll(items);
    }

    public boolean contains(Object candidate) {
        final Iterator ite = createIterator();
        while (ite.hasNext()) {
            if (candidate.equals(ite.next())) {
                return true;
            }
        }
        return false;
    }

    public int getSize() {
        final Iterator ite = createIterator();
        int nb = 0;
        while (ite.hasNext()) {
            ite.next();
            nb++;
        }
        return nb;
    }

    public boolean isEmpty() {
        final Iterator ite = createIterator();
        return !ite.hasNext();
    }

    public Class[] getEventClasses() {
        return new Class[]{
            CollectionMessage.class
        };
    }

    public Object[] toArray() {
        return toArray(Object.class);
    }

    public Object[] toArray(Class clazz) {
        final int size = getSize();
        Object[] array = (Object[]) Array.newInstance(clazz, size);
        if (size==0) return array;
        final Iterator ite = createIterator();
        int i = 0;
        while (ite.hasNext()) {
            if (i==array.length) array = Arrays.resize(array, i+1);
            array[i] = ite.next();
            i++;
        }
        if (i<array.length) {
            array = Arrays.resize(array, i);
        }
        return array;
    }

    /**
     * Send a collection add event.
     *
     * @param startIndex first index of modified element, -1 if unknowed
     * @param endIndex last index of modified element, -1 if unknowed
     * @param news values added
     */
    protected void fireAdd(int startIndex, int endIndex, Object[] news) {
        if (hasListeners()) {
            getEventManager().sendEvent(new Event(this,new CollectionMessage(CollectionMessage.TYPE_ADD, startIndex, endIndex, null, news)));
        }
    }

    /**
     * Send a collection remove event.
     *
     * @param startIndex first index of modified element, -1 if unknowed
     * @param endIndex last index of modified element, -1 if unknowed
     * @param olds values removed
     */
    protected void fireRemove(int startIndex, int endIndex, Object[] olds) {
        if (hasListeners()) {
            getEventManager().sendEvent(new Event(this,new CollectionMessage(CollectionMessage.TYPE_REMOVE, startIndex, endIndex, olds, null)));
        }
    }

    /**
     * Send a collection replace event.
     *
     * @param startIndex first index of modified element, -1 if unknowed
     * @param endIndex last index of modified element, -1 if unknowed
     * @param olds values before replacement
     * @param news values after replacement
     */
    protected void fireReplace(int startIndex, int endIndex, Object[] olds, Object[] news) {
        if (hasListeners()) {
            getEventManager().sendEvent(new Event(this,new CollectionMessage(CollectionMessage.TYPE_REPLACE, startIndex, endIndex, olds, news)));
        }
    }

    /**
     * Send a collection replace all event.
     *
     * @param startIndex first index of modified element, -1 if unknowed
     * @param endIndex last index of modified element, -1 if unknowed
     * @param olds values before replacement
     * @param news values after replacement
     */
    protected void fireReplaceAll(int startIndex, int endIndex, Object[] olds, Object[] news) {
        if (hasListeners()) {
            getEventManager().sendEvent(new Event(this,new CollectionMessage(CollectionMessage.TYPE_REPLACEALL, startIndex, endIndex, olds, news)));
        }
    }

    public Chars toChars() {
        return Nodes.toChars(new Chars(this.getClass().getName()), this);
    }

}
