

package science.unlicense.common.api.character;


/**
 * Translatable character array.
 *
 * @author Johann Sorel
 */
public interface LChars extends CharArray,Translatable{

    /**
     * Get chars in given language.
     * @param language; if null or language is not available, defaut language is used
     * @return Chars, never null,
     */
    LChars translate(Language language);

    /**
     * Print the default chars text.
     * @return Chars
     */
    Chars toChars();

    /**
     * Similar to toChars, but print all translations concatenated.
     * @return Chars
     */
    Chars toCharsAll();

}
