
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class DefaultFloat64Cursor extends AbstractCursor implements Float64Cursor, Buffer.Float64 {

    final Buffer buffer;

    //current offset from start.
    private long byteOffset = 0;

    public DefaultFloat64Cursor(Buffer buffer, Endianness encoding) {
        super(encoding);
        this.buffer = buffer;
    }

    @Override
    public Buffer getBuffer() {
        return buffer;
    }

    @Override
    public long getByteOffset() {
        return byteOffset;
    }

    @Override
    public long getOffset() {
        return byteOffset/8;
    }

    @Override
    public long getSize() {
        return buffer.getByteSize() / 8;
    }

    @Override
    public DefaultFloat64Cursor offset(long position) {
        byteOffset = position*8;
        return this;
    }

    @Override
    public Float64Cursor cursor() {
        return new DefaultFloat64Cursor(buffer, encoding);
    }

    @Override
    public double read(long position) {
        return buffer.readFloat64(position*8);
    }

    @Override
    public void read(long position, double[] array) {
        buffer.readFloat64(array,position*8);
    }

    @Override
    public void read(long position, double[] array, int arrayOffset, int length) {
        buffer.readFloat64(array,arrayOffset,length,position*8);
    }

    @Override
    public void write(long position, double value) {
        buffer.writeFloat64(value, position*8);
    }

    @Override
    public void write(long position, double[] array) {
        write(position, array, 0, array.length);
    }

    @Override
    public void write(long position, double[] array, int arrayOffset, int length) {
        buffer.writeFloat64(array,arrayOffset,length, position*8);
    }

    @Override
    public double read() {
        double v = buffer.readFloat64(byteOffset);
        byteOffset += 8;
        return v;
    }

    @Override
    public void read(double[] array) {
        read(array,0,array.length);
    }

    @Override
    public void read(double[] array, int arrayOffset, int length) {
        buffer.readFloat64(array,byteOffset);
        byteOffset += length*8;
    }

    @Override
    public Float64Cursor write(double value) {
        buffer.writeFloat64(value, byteOffset);
        byteOffset += 8;
        return this;
    }

    @Override
    public Float64Cursor write(double[] array) {
        return write(array, 0, array.length);
    }

    @Override
    public Float64Cursor write(double[] array, int arrayOffset, int length) {
        buffer.writeFloat64(array,arrayOffset,length, byteOffset);
        byteOffset += length*4;
        return this;
    }

    @Override
    public double[] toDoubleArray() {
        final double[] array = new double[(int) getSize()];
        read(array);
        return array;
    }
}
