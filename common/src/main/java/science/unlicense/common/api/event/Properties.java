
package science.unlicense.common.api.event;

import science.unlicense.common.api.model.proto.Prototype;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.model.DefaultValueType;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.predicate.Variables;

/**
 * Control a dynamic dictionnary of properties.
 * This class also propagate events and store default values in a separate
 * map to avoid nulls or possible duplicated among instances.
 *
 * @author Johann Sorel
 */
public class Properties implements Prototype {

    private final Dictionary properties = new HashDictionary();
    private final Dictionary readonly = new Dictionary() {
        @Override
        public int getSize() {
            //TODO not right, does not contain defaults.
            return properties.getSize();
        }

        @Override
        public Set getKeys() {
            final HashSet set = new HashSet();
            set.addAll(properties.getKeys());
            set.addAll(defaults.getKeys());
            return set;
        }

        @Override
        public Collection getValues() {
            final Sequence seq = new ArraySequence();
            final Iterator ite = getKeys().createIterator();
            while (ite.hasNext()) {
                seq.add(getPropertyValue((Chars) ite.next()));
            }
            return seq;
        }

        @Override
        public Collection getPairs() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Object getValue(Object key) {
            if (key instanceof Chars) {
                return getPropertyValue((Chars) key);
            } else {
                return properties.getValue(key);
            }
        }

        @Override
        public void add(Object key, Object value) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public void addAll(Dictionary index) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Object remove(Object key) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public void removeAll() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Class[] getEventClasses() {
            return new Class[0];
        }

        @Override
        public EventListener[] getListeners(Predicate predicate) {
            return new EventListener[0];
        }

        @Override
        public void addEventListener(Predicate predicate, EventListener listener) {
        }

        @Override
        public void removeEventListener(Predicate predicate, EventListener listener) {
        }
    };
    private Dictionary defaults = new HashDictionary();
    private EventManager eventManager;
    private EventSource eventSource;

    public Properties() {
        this(new AbstractEventSource(), new EventManager());
    }

    public Properties(EventSource eventSource, EventManager eventManager) {
        this.eventSource = eventSource;
        this.eventManager = eventManager;
    }

    /**
     *
     * @return dictionnary of default property values.
     */
    public Dictionary getDefaults() {
        return defaults;
    }

    public void setDefaults(Dictionary defaults) {
        this.defaults = defaults;
    }

    public void set(Properties toCopy) {
        final Dictionary dico = toCopy.asDictionary();
        Iterator ite = dico.getKeys().createIterator();
        while (ite.hasNext()) {
            Object key = ite.next();
            setPropertyValue((Chars) key, dico.getValue(key));
        }
    }

    @Override
    public Set getPropertyNames() {
        final HashSet set = new HashSet();
        set.addAll(properties.getKeys());
        set.addAll(defaults.getKeys());
        return set;
    }

    @Override
    public Object getPropertyValue(Chars name){
        final Object value = properties.getValue(name);
        return value == null ? defaults.getValue(name) : value;
    }

    /**
     * Get property value, if value is null the fallback value is returned.
     *
     * @param name property name
     * @param fallbackValue returned if value is null
     * @return
     */
    public Object getPropertyValue(Chars name, Object fallbackValue){
        final Object value = getPropertyValue(name);
        return value == null ? fallbackValue : value;
    }

    /**
     * Set property value, sends property event if value has changed.
     *
     * @param name property name
     * @param value, new value
     * @return true if value has changed
     */
    @Override
    public boolean setPropertyValue(Chars name, Object value){
        return setPropertyValue(name, value, defaults.getValue(name));
    }

    /**
     * Get Property object which can be sync to other values.
     *
     * @param name property name
     * @return property never null
     */
    @Override
    public Property getProperty(Chars name) {
        final Object def = defaults.getValue(name);
        final Class valueClass = def == null ? Object.class : def.getClass();
        return new DynamicProperty(name, valueClass);
    }

    /**
     * Set property value, sends property event if value has changed.
     * if the new value is equal to the default value, then the value
     * will not be stored in the internal properties dictionary.
     *
     * @param name property name
     * @param value, new value
     * @param defaultValue
     * @return true if value has changed
     */
    private boolean setPropertyValue(Chars name, Object value, Object defaultValue){
        Object oldVal = properties.getValue(name);
        if (oldVal == null) oldVal = defaultValue;
        final boolean changed = !CObjects.equals(oldVal, value);
        if (changed) {
            if (!prevalidateChange(name, oldVal, value)) {
                throw new RuntimeException("Value change has been refused.");
            }

            if (value == null || CObjects.equals(value, defaultValue)){
                properties.remove(name);
            } else {
                properties.add(name, value);
            }
            valueChanged(name, oldVal, value);
            eventManager.sendPropertyEvent(eventSource, name, oldVal, value);
        }
        return changed;
    }

    /**
     * Called before replacing value.
     *
     * @param name
     * @param oldValue
     * @param value
     * @return true if value can be replaced
     */
    protected boolean prevalidateChange(Chars name, Object oldValue, Object value) {
        return true;
    }

    /**
     * Called after a value has been changed.
     * @param name
     * @param oldValue
     * @param value
     */
    protected void valueChanged(Chars name, Object oldValue, Object value) {
    }

    /**
     * Release properties.
     * Releases listeners and any other resources.
     */
    public void dispose() {
        //clean listeners
        final EventListener[] listeners = eventManager.getListeners(Predicate.TRUE);
        for (EventListener l : listeners) {
            if (l instanceof Variables.VarSync) {
                ((Variables.VarSync) l).release();
            }
        }
    }

    @Override
    public Dictionary asDictionary() {
        return readonly;
    }

    private class DynamicProperty extends AbstractProperty {

        public DynamicProperty(Chars name,Class valueClass) {
            super(eventSource, name, new DefaultValueType(name, name, name, valueClass, null), true, true);
        }

        @Override
        public Object getValue() throws RuntimeException {
            return Properties.this.getPropertyValue(name);
        }

        @Override
        public void setValue(Object value) throws RuntimeException {
            Properties.this.setPropertyValue(name, value);
        }

    }
}
