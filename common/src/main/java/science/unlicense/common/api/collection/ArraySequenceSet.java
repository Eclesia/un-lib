
package science.unlicense.common.api.collection;

/**
 * An array sequence which do not allow multiple occurence of the same object.
 *
 * @author Johann Sorel
 */
public class ArraySequenceSet extends ArraySequence implements Set{

    public boolean add(Object candidate) {
        if (contains(candidate)) return false;
        return super.add(candidate);
    }

    @Override
    public boolean add(int index, Object value) {
        if (contains(value)) return false;
        return super.add(index, value);
    }

    @Override
    public boolean addAll(int index, Object[] candidate) {
        boolean all = true;
        for (int i=0;i<candidate.length;i++) {
            boolean b = add(index, candidate[i]);
            if (b) index++;
            all = all && b;
        }
        return all;
    }

    @Override
    public Object replace(int index, Object item) {
        if (contains(item)) return false;
        return super.replace(index, item);
    }

}
