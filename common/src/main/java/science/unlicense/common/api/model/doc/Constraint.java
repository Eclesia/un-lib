
package science.unlicense.common.api.model.doc;


/**
 * Constraint are used to enforce parameters validity.
 * A logic constraint can be used as example for self-exclusive parameters (choices).
 *
 * TODO
 *
 * @author Johann Sorel
 */
public interface Constraint {

    void control(Field parameter);

}
