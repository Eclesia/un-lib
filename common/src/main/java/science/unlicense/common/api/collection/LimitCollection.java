
package science.unlicense.common.api.collection;

/**
 *
 * @author Johann Sorel
 */
class LimitCollection extends AbstractCollection {

    private final Collection base;
    private final int limit;

    public LimitCollection(Collection base, int limit) {
        this.base = base;
        this.limit = limit;
    }


    @Override
    public int getSize() {
        final int size = base.getSize();
        return size<limit ? size : limit;
    }

    @Override
    public Iterator createIterator() {
        return new LimitIterator(base.createIterator(), limit);
    }

}
