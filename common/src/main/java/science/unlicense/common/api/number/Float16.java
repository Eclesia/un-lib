
package science.unlicense.common.api.number;

import science.unlicense.common.api.exception.UnimplementedException;

/**
 * 16bit decimal, called half.
 * Encoded in IEEE754
 * http://en.wikipedia.org/wiki/Half-precision_floating-point_format
 *
 * @author Johann Sorel
 */
public final class Float16 extends AbstractNumber {

    public static final Type TYPE = new Type();

    public static final int MASK_FRACTION = 0x03FF;
    public static final int MASK_EXPONENT = 0x7C00;
    public static final int MASK_SIGN = 1 >> 15;

    public static final int FRACTION_SIZE = 10;
    public static final int EXPONENT_SIZE = 5;
    public static final int EXP_BIAS = 127;


    public static IEEE754 decompose(int value) {
        return decompose(value, null);
    }

    public static IEEE754 decompose(int value, IEEE754 buffer) {
        if (buffer == null) buffer = new IEEE754();
        final int bits = value; //java does not have Half type
        buffer.fraction = bits & MASK_FRACTION;
        buffer.exponent = ((bits & MASK_EXPONENT) >> FRACTION_SIZE);
        buffer.sign = bits >> 15;

        return buffer;
    }

    public static float compose(IEEE754 ieee) {
        //TODO debugged
        int val = ieee.sign << 31;
        val |= ieee.exponent << 23;
        val |= ieee.fraction;
        return Float.intBitsToFloat(val);
    }

    private final float value;

    public Float16(float value) {
        this.value = value;
    }

    @Override
    public NumberType getType() {
        return TYPE;
    }

    @Override
    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public int toInteger() {
        return (int) value;
    }

    @Override
    public long toLong() {
        return (long) value;
    }

    @Override
    public float toFloat() {
        return value;
    }

    @Override
    public double toDouble() {
        return value;
    }

    @Override
    public int order(Object other) {
        float of = ((Number) other).toFloat();
        return value < of ? -1 : (value > of ? +1 : 0) ;
    }

    private static class Type implements NumberType {

        public Type() {
        }

        @Override
        public Number create(Arithmetic value) {
            if (!(value instanceof Number)) throw new UnsupportedOperationException();
            return new Float16(((Number) value).toFloat());
        }

        @Override
        public Class getPrimitiveClass() {
            return float.class;
        }

        @Override
        public Class getValueClass() {
            return Float16.class;
        }

        @Override
        public int getSizeInBits() {
            return 16;
        }

        @Override
        public int getSizeInBytes() {
            return 2;
        }

        @Override
        public int getPrimitiveCode() {
            return Primitive.UNKNOWNED;
        }
    }

}
