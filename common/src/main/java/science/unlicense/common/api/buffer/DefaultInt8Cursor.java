
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.number.Endianness;

/**
 * Both Byte cursor and byte buffer view.
 *
 * @author Johann Sorel
 */
public class DefaultInt8Cursor extends AbstractCursor implements Int8Cursor, Buffer.Int8 {

    final Buffer buffer;

    //current offset from start.
    private long byteOffset = 0;

    public DefaultInt8Cursor(Buffer buffer, Endianness encoding) {
        super(encoding);
        this.buffer = buffer;
    }

    @Override
    public Buffer getBuffer() {
        return buffer;
    }

    @Override
    public long getByteOffset() {
        return byteOffset;
    }

    @Override
    public long getOffset() {
        return byteOffset;
    }

    @Override
    public long getSize() {
        return buffer.getByteSize();
    }

    public Int8Cursor cursor() {
        return new DefaultInt8Cursor(buffer, encoding);
    }

    @Override
    public DefaultInt8Cursor offset(long position) {
        byteOffset = position;
        return this;
    }

    @Override
    public byte read(long position) {
        return buffer.readInt8(position);
    }

    @Override
    public void read(long position, byte[] array) {
        buffer.readInt8(array, position);
    }

    @Override
    public void read(long position, byte[] array, int arrayOffset, int length) {
        buffer.readInt8(array, arrayOffset, length, position);
    }

    @Override
    public void write(long position, byte value) {
        buffer.writeInt8(value, position);
    }

    @Override
    public void write(long position, byte[] array) {
        write(position, array, 0, array.length);
    }

    @Override
    public void write(long position, byte[] array, int arrayOffset, int length) {
        buffer.writeInt8(array, arrayOffset,length, position);
    }

    @Override
    public byte read() {
        byte v = buffer.readInt8(byteOffset);
        byteOffset++;
        return v;
    }

    @Override
    public void read(byte[] array) {
        read(array,0,array.length);
    }

    @Override
    public void read(byte[] array, int arrayOffset, int length) {
        buffer.readInt8(array,byteOffset);
        byteOffset += length;
    }

    @Override
    public Int8Cursor write(byte value) {
        buffer.writeInt8(value, byteOffset);
        byteOffset++;
        return this;
    }

    @Override
    public Int8Cursor write(byte[] array) {
        return write(array, 0, array.length);
    }

    @Override
    public Int8Cursor write(byte[] array, int arrayOffset, int length) {
        buffer.writeInt8(array, arrayOffset, length, byteOffset);
        byteOffset += length;
        return this;
    }

}
