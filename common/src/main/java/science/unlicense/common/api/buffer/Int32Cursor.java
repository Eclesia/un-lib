
package science.unlicense.common.api.buffer;

/**
 *
 * @author Johann Sorel
 */
public interface Int32Cursor extends Cursor {

    long getOffset();

    Int32Cursor offset(long position);

    int read();

    void read(int[] array);

    void read(int[] array, int arrayOffset, int length);

    Int32Cursor write(int value);

    Int32Cursor write(int[] array);

    Int32Cursor write(int[] array, int arrayOffset, int length);

}
