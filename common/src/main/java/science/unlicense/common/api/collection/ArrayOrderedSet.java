
package science.unlicense.common.api.collection;

import science.unlicense.common.api.Arrays;

/**
 *
 * @author Johann Sorel
 */
public class ArrayOrderedSet extends AbstractCollection implements OrderedSet{

    private Object[] values = Arrays.ARRAY_OBJECT_EMPTY;

    public boolean add(Object candidate) {
        for (int i=0;i<values.length;i++) {
            if (values[i].equals(candidate)) {
                return false;
            }
        }
        this.values = Arrays.insert(values, values.length, candidate);
        Arrays.sort(values);
        if (hasListeners()) fireAdd(-1, -1, new Object[]{candidate});
        return true;
    }

    public boolean addAll(Collection candidate) {
        final Iterator ite = candidate.createIterator();
        boolean all = true;
        while (ite.hasNext()) {
            all = add(ite.next()) && all;
        }
        ite.close();
        Arrays.sort(values);
        return all;
    }

    public boolean remove(Object candidate) {
        for (int i=0;i<values.length;i++) {
            if (values[i].equals(candidate)) {
                this.values = Arrays.remove(values, i);
                if (hasListeners()) fireRemove(-1, -1, new Object[]{candidate});
                return true;
            }
        }
        return false;
    }

    public boolean removeAll(Collection candidate) {
        final Iterator ite = candidate.createIterator();
        boolean all = true;
        while (ite.hasNext()) {
            all = remove(ite.next()) && all;
        }
        ite.close();
        return all;
    }

    public boolean removeAll() {
        if (values.length!=0) {
            if (hasListeners()) {
                Object[] old = toArray();
                values = Arrays.ARRAY_OBJECT_EMPTY;
                fireRemove(-1, -1, old);
            } else {
                values = Arrays.ARRAY_OBJECT_EMPTY;
            }
        }
        return true;
    }

    public boolean contains(Object candidate) {
        return Arrays.contains(values, candidate);
    }

    public Iterator createIterator() {
        return new ArrayIterator();
    }

    public int getSize() {
        return values.length;
    }

    public boolean isEmpty() {
        return values.length == 0;
    }

    public Object[] toArray() {
        return Arrays.copy(values);
    }

    private class ArrayIterator implements Iterator{

        private Object next = null;
        private int index = -1;

        public boolean hasNext() {
            findNext();
            return next != null;
        }

        public Object next() {
            findNext();
            final Object c = next;
            next = null;
            return c;
        }

        private void findNext() {
            if (next != null) return;
            index++;
            if (index< values.length) {
                next = values[index];
            }
        }

        public boolean remove() {
            return ArrayOrderedSet.this.remove(index);
        }

        public void close() {
        }

    }

}
