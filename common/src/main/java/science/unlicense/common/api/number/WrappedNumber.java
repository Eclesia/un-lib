
package science.unlicense.common.api.number;

/**
 *
 * @author Johann Sorel
 */
public class WrappedNumber implements Number {

    protected final Number value;

    public WrappedNumber(Number value) {
        this.value = value;
    }

    @Override
    public NumberType getType() {
        return value.getType();
    }

    @Override
    public int toInteger() {
        return value.toInteger();
    }

    @Override
    public long toLong() {
        return value.toLong();
    }

    @Override
    public float toFloat() {
        return value.toFloat();
    }

    @Override
    public double toDouble() {
        return value.toDouble();
    }

    @Override
    public Arithmetic add(Arithmetic other) {
        return value.add(other);
    }

    @Override
    public Arithmetic subtract(Arithmetic other) {
        return value.subtract(other);
    }

    @Override
    public Arithmetic mult(Arithmetic other) {
        return value.mult(other);
    }

    @Override
    public Arithmetic divide(Arithmetic other) {
        return value.divide(other);
    }

    @Override
    public Arithmetic zero() {
        return value.zero();
    }

    @Override
    public boolean isZero() {
        return value.isZero();
    }

    @Override
    public Arithmetic one() {
        return value.one();
    }

    @Override
    public boolean isOne() {
        return value.isOne();
    }

    @Override
    public Arithmetic pow(int n) {
        return value.pow(n);
    }

    @Override
    public int order(Object other) {
        return value.order(other);
    }

    @Override
    public Arithmetic op(int opCode) {
        return value.op(opCode);
    }

}
