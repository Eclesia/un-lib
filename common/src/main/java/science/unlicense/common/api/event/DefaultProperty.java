
package science.unlicense.common.api.event;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.lang.Reflection;
import science.unlicense.common.api.model.DefaultValueType;
import science.unlicense.common.api.model.ValueType;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.predicate.Variable;
import science.unlicense.common.api.predicate.VariableSyncException;
import science.unlicense.common.api.predicate.Variables;

/**
 * Default property implementation based on java reflection capabilities.
 *
 * @author Johann Sorel
 */
public class DefaultProperty extends CObject implements Property, Predicate {

    private final WeakReference<EventSource> source;
    private final Chars name;
    private final ValueType valueType;

    public DefaultProperty(EventSource source, Chars name) {
        this.source = new WeakReference<EventSource>(source);
        this.name = name;

        try {
            Class c = BeanUtils.getReadMethod(source.getClass(), name).getReturnType();
            //remove any autoboxing
            c = Reflection.getBoxingClass(c);
            Class valueClass = c;
            valueType = new DefaultValueType(name, name, name, valueClass, null);

        } catch (InvalidArgumentException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    @Override
    public EventSource getHolder() {
        return source.get();
    }

    EventSource getHolderError() {
        EventSource holder = source.get();
        if (holder==null) throw new IllegalStateException("Property source object has been garbage collected");
        return holder;
    }

    @Override
    public Chars getName() {
        return name;
    }

    @Override
    public ValueType getType() {
        return valueType;
    }

    @Override
    public Object getValue() {
        final EventSource holder = getHolderError();
        try {
            return BeanUtils.getReadMethod(holder.getClass(), name).invoke(holder);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (InvalidArgumentException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    @Override
    public void setValue(Object value) {
        EventSource holder = source.get();
        if (holder==null) return;
        try {
            Method writeMethod = BeanUtils.getWriteMethod(holder.getClass(), name);
            if (writeMethod==null) {
                throw new RuntimeException("Object "+holder.getClass()+" has no write method for property "+name);
            }
            writeMethod.invoke(holder, value);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (InvalidArgumentException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    @Override
    public boolean isReadable() {
        final EventSource holder = getHolderError();
        return BeanUtils.getReadMethod(source.getClass(), name) != null;
    }

    @Override
    public boolean isWritable() {
        final EventSource holder = getHolderError();
        return BeanUtils.getWriteMethod(holder.getClass(), name) != null;
    }

    @Override
    public void sync(Variable target) {
        sync(target, false);
    }

    @Override
    public void sync(Variable target, boolean readOnly) {
        final Variables.VarSync sync = getVarSync();
        if (sync!=null) throw new VariableSyncException("Variable is already synchronized");
        Variables.sync(this, target, readOnly);
    }

    @Override
    public void unsync() {
        final Variables.VarSync sync = getVarSync();
        if (sync!=null) sync.release();
    }

    private Variables.VarSync getVarSync() {
        final EventSource holder = getHolderError();
        final EventListener[] listeners = holder.getListeners(new PropertyPredicate(name));
        if (listeners.length==0) return null;
        for (int i=0;i<listeners.length;i++) {
            if (listeners[i] instanceof Variables.VarSync) {
                final Variables.VarSync sync = ((Variables.VarSync) listeners[i]);
                if (sync.getVar1().equals(this)) {
                    return sync;
                }
            }
        }
        return null;
    }

    @Override
    public void addListener(EventListener listener) {
        addEventListener(new PropertyPredicate(name), listener);
    }

    @Override
    public void removeListener(EventListener listener) {
        final EventSource holder = source.get();
        if (holder!=null) holder.removeEventListener(new PropertyPredicate(name), listener);
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

    @Override
    public void addEventListener(Predicate predicate, EventListener listener) {
        getHolderError().addEventListener(predicate, listener);
    }

    @Override
    public void removeEventListener(Predicate predicate, EventListener listener) {
        final EventSource holder = source.get();
        if (holder!=null) holder.removeEventListener(predicate, listener);
    }

    @Override
    public EventListener[] getListeners(Predicate predicate) {
        return getHolderError().getListeners(predicate);
    }

    @Override
    public Predicate asEventPredicate() {
        return this;
    }

    @Override
    public Boolean evaluate(Object candidate) {
        if (candidate instanceof Event) {
            final EventMessage message = ((Event) candidate).getMessage();
            return message instanceof PropertyMessage && name.equals(((PropertyMessage) message).getPropertyName());
        }
        return false;
    }

    @Override
    public Chars toChars() {
        return new Chars("Property ").concat(name);
    }

}
