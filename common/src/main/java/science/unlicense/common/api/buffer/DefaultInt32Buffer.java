
package science.unlicense.common.api.buffer;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class DefaultInt32Buffer extends AbstractBuffer{

    private static final int[] MASKS = {
        0xFF000000,
        0x00FF0000,
        0x0000FF00,
        0x000000FF};
    private static final int[] OFFSET = {
        24,16,8,0};

    private final int[] buffer;

    public DefaultInt32Buffer(int[] buffer) {
        super(science.unlicense.common.api.number.Int32.TYPE, Endianness.BIG_ENDIAN, DefaultBufferFactory.INSTANCE);
        this.buffer = buffer;
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public Object getBackEnd() {
        return buffer;
    }

    @Override
    public long getByteSize() {
        return buffer.length*4;
    }

    @Override
    public byte readInt8(long offset) {
        final int index = (int) (offset/4);
        final int mod = (int) (offset%4);
        final int i = buffer[index];
        return (byte) ( (i & MASKS[mod]) >> OFFSET[mod] );
    }

    @Override
    public int readInt32(long offset) {
        if (offset%4==0) {
            return buffer[(int) (offset/4)];
        } else {
            return super.readInt32(offset);
        }
    }

    @Override
    public void readInt32(int[] array, int arrayOffset, int length, long offset) {
        if (offset%4==0) {
            Arrays.copy(buffer, (int) (offset/4), length, array, arrayOffset);
        } else {
            super.readInt32(array, arrayOffset, length, offset);
        }
    }

    @Override
    public void writeInt8(byte value, long offset) {
        final int index = (int) (offset/4);
        final int mod = (int) (offset%4);
        int i = buffer[index];
        i &= ~MASKS[mod];
        i |= (value&0xFF) << OFFSET[mod];
        buffer[index] = i;
    }

    @Override
    public void writeInt32(int value, long offset) {
        if (offset%4==0) {
            buffer[(int) (offset/4)] = value;
        } else {
            super.writeInt32(value, offset);
        }
    }

    @Override
    public void writeInt32(int[] array, int arrayOffset, int length, long offset) {
        if (offset%4==0) {
            Arrays.copy(array, arrayOffset, length, buffer, (int) (offset/4));
        } else {
            super.writeInt32(array, arrayOffset, length, offset);
        }
    }

    @Override
    public Buffer copy(BufferFactory factory, NumberType primitive, Endianness encoding) {
        if (  (factory == null || this.factory == factory)
           && (primitive == null || this.numericType == primitive)
           && (encoding == null || this.endianness == encoding)) {
            return new DefaultInt32Buffer(Arrays.copy(buffer));
        }
        return super.copy(factory, primitive, encoding);
    }
}
