

package science.unlicense.common.api.number;

/**
 * Constant of the different primitive types.
 *
 * @author Johann Sorel
 */
public final class Primitive {

    public static final int UNKNOWNED = 0;
    public static final int BITS1 = 1;
    public static final int BITS2 = 2;
    public static final int BITS4 = 3;
    public static final int INT8 = 4;
    public static final int UINT8 = 5;
    public static final int INT16 = 6;
    public static final int UINT16 = 7;
    public static final int INT24 = 8;
    public static final int UINT24 = 9;
    public static final int INT32 = 10;
    public static final int UINT32 = 11;
    public static final int INT64 = 12;
    public static final int UINT64 = 13;
    public static final int FLOAT32 = 14;
    public static final int FLOAT64 = 15;

    protected static final double[] NB_BYTES = new double[]{
        0.0,
        1.0/8.0,
        1.0/4.0,
        1.0/2.0,
        1.0,
        1.0,
        2.0,
        2.0,
        3.0,
        3.0,
        4.0,
        4.0,
        8.0,
        8.0,
        4.0,
        8.0
    };
    protected static final int[] NB_BITS = new int[]{
        0,
        1,
        2,
        4,
        8,
        8,
        16,
        16,
        24,
        24,
        32,
        32,
        64,
        64,
        32,
        64
    };

    private Primitive() {}

    public static double getSizeInBytes(int primitive) {
        return NB_BYTES[primitive];
    }

    public static int getSizeInBits(int primitive) {
        return NB_BITS[primitive];
    }

}
