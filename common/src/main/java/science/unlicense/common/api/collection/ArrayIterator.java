
package science.unlicense.common.api.collection;

import science.unlicense.common.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public class ArrayIterator implements Iterator{

    private final Object[] arrayvalues;
    private int index = 0;

    public ArrayIterator(Object[] array) {
        this.arrayvalues = array;
    }

    public boolean hasNext() {
        return index<arrayvalues.length;
    }

    public Object next() {
        Object obj = arrayvalues[index];
        index++;
        return obj;
    }

    public boolean remove() {
        throw new UnimplementedException("Unsupported");
    }

    public void close() {
    }

}
