
package science.unlicense.common.api.collection.primitive;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.AbstractCollection;
import science.unlicense.common.api.collection.AbstractIterator;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.OrderedSet;

/**
 *
 * @author Johann Sorel
 */
public class FloatSet extends AbstractCollection implements OrderedSet{

    private float[] buffer;
    private int size = 0;

    public FloatSet() {
        buffer = new float[1000];
        size = 0;
    }

    @Override
    public int getSize() {
        return size;
    }

    private void growIfNecessary(int length) {
        size += length;
        if (size >= buffer.length) {
            int gs = Math.max(length, buffer.length*3);
            buffer = Arrays.resize(buffer, buffer.length+gs);
        }
    }

    public boolean add(float candidate) {
        int index = size==0 ? -1 : Arrays.binarySearch(candidate, buffer,0,size-1);
        if (index<0) {
            //new value
            index = -(index+1);
            growIfNecessary(1);
            Arrays.copy(buffer, index, size-1-index, buffer, index+1);
            buffer[index] = candidate;
            return true;
        } else {
            return false;
        }
    }

    public boolean remove(float candidate) {
        if (size==0) return false;
        final int index = Arrays.binarySearch(candidate, buffer,0,size-1);
        if (index>=0) {
            Arrays.copy(buffer, index+1, size-index, buffer, index);
            size--;
            return true;
        } else {
            return false;
        }
    }

    public boolean contains(float candidate) {
        if (size==0) return false;
        return Arrays.binarySearch(candidate, buffer,0,size-1) >=0;
    }

    public boolean add(Object candidate) {
        return add(((Float) candidate).floatValue());
    }

    public boolean remove(Object candidate) {
        return remove(((Float) candidate).floatValue());
    }

    public boolean contains(Object candidate) {
        return contains(((Float) candidate).floatValue());
    }

    public Iterator createIterator() {
        return new Ite();
    }

    public boolean removeAll() {
        buffer = new float[1000];
        size = 0;
        return true;
    }

    /**
     * @return float array
     */
    public float[] toArrayFloat() {
        return Arrays.copy(buffer, 0, size, new float[size], 0);
    }

    private final class Ite extends AbstractIterator{

        int index=0;

        @Override
        protected void findNext() {
            if (index>=size) return;
            nextValue = buffer[index];
            index++;
        }

    }

}
