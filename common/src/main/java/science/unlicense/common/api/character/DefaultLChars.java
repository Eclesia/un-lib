

package science.unlicense.common.api.character;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;

/**
 * Default translatable char array.
 *
 * @author Johann Sorel
 */
public class DefaultLChars extends AbstractCharArray implements LChars {

    private final Dictionary translations = new HashDictionary();
    private final Language defaultLg;
    private final Chars defaultTrs;

    /**
     *
     * @param translations dictionary of Language to Chars
     * @param defaultLg default language
     */
    public DefaultLChars(Dictionary translations, Language defaultLg) {
        this.defaultLg = defaultLg;
        this.defaultTrs = (Chars) translations.getValue(defaultLg);
        this.translations.addAll(translations);
    }

    /**
     * Constructor by copy.
     * @param toCopy to copy
     */
    public DefaultLChars(LChars toCopy) {
        this.defaultLg = toCopy.getLanguage();
        this.defaultTrs = toCopy.toChars();
        final Iterator ite = toCopy.getLanguages();
        while (ite.hasNext()) {
            final Language lg = (Language) ite.next();
            this.translations.add(lg, toCopy.translate(lg).toChars());
        }
    }

    protected byte[] getBytesInternal() {
        return defaultTrs.getBytesInternal();
    }

    protected boolean isByteInternalCopy() {
        return defaultTrs.isByteInternalCopy();
    }

    public CharEncoding getEncoding() {
        return defaultTrs.getEncoding();
    }

    public LChars translate(Language language) {
        final Chars trs = (Chars) translations.getValue(language);
        if (trs==null) return null;
        return new DefaultLChars(translations, language);
    }

    public Language getLanguage() {
        return defaultLg;
    }

    public Iterator getLanguages() {
        return translations.getKeys().createIterator();
    }

    public Chars toChars() {
        return defaultTrs;
    }

    public Chars toCharsAll() {
        final CharBuffer cb = new CharBuffer();
        final Iterator ite = translations.getPairs().createIterator();
        boolean first = true;
        while (ite.hasNext()) {
            if (!first) {
                cb.append(" / ");
            } else {
                first = false;
            }
            final Pair pair = (Pair) ite.next();
            cb.append((CObject) pair.getValue1()).append(':').append((CObject) pair.getValue2());
        }
        return cb.toChars();
    }

}
