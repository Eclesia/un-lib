
package science.unlicense.common.api.collection;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.exception.MishandleException;

/**
 * Array sequence.
 *
 * @author Johann Sorel
 * @author Yann D'Isanto
 */
public abstract class AbstractSequence extends AbstractCollection implements Sequence{


    public boolean add(Object candidate) {
        return add(getSize(),candidate);
    }

    public boolean addAll(Object[] candidates) {
        return addAll(getSize(),candidates);
    }

    public boolean addAll(int index, Collection candidate) {
        return addAll(index,candidate.toArray());
    }

    public boolean addAll(int index, Object[] candidate) {
        for (int i=0;i<candidate.length;i++) {
            add(index, candidate[i]);
            index++;
        }
        return true;
    }

    public Object replace(int index, Object item) {
        if (index < 0 || index >= getSize()) {
            throw new InvalidIndexException(""+index);
        }
        Object old = get(index);
        remove(index);
        add(index, item);
        return old;
    }

    public void replaceAll(Collection items) {
        removeAll();
        addAll(items);
    }

    public void replaceAll(Object[] items) {
        removeAll();
        addAll(items);
    }

    public boolean addAll(Collection candidate) {
        final Iterator ite = candidate.createIterator();
        boolean all = true;
        while (ite.hasNext()) {
            all = add(ite.next()) && all;
        }
        ite.close();
        return all;
    }

    public boolean remove(Object candidate) {
        for (int i=0,n=getSize();i<n;i++) {
            if (CObjects.equals(get(i),candidate)) {
                return remove(i);
            }
        }
        return false;
    }

    public boolean removeAll(Collection candidate) {
        final Iterator ite = candidate.createIterator();
        boolean all = true;
        while (ite.hasNext()) {
            all = remove(ite.next()) && all;
        }
        ite.close();
        return all;
    }

    public boolean contains(Object candidate) {
        final Object[] array = toArray();
        if (array.length==0) return false;
        return Arrays.contains(array,0,array.length,candidate);
    }

    public int search(Object item) {
        return Arrays.getFirstOccurence(toArray(), 0, getSize(), item);
    }

    public Iterator createIterator() {
        return new ArrayIterator();
    }

    public Iterator createReverseIterator() {
        return new ReverseArrayIterator();
    }

    public boolean isEmpty() {
        return getSize() == 0;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Sequence)) {
            return false;
        }
        final Sequence other = (Sequence) obj;
        if (this.getSize() != other.getSize()) {
            return false;
        }
        for (int i=0;i<this.getSize();i++) {
            if (!get(i).equals(other.get(i))) {
                return false;
            }
        }
        return true;
    }

    private class ArrayIterator implements Iterator{

        //keep a reference, avoids resizing issues in concurency.
        private final Object[] arrayvalues = AbstractSequence.this.toArray();
        private final int size = arrayvalues.length;
        private int index = 0;

        public boolean hasNext() {
            return index<size;
        }

        public Object next() {
            Object obj = arrayvalues[index];
            index++;
            return obj;
        }

        public boolean remove() {
            return AbstractSequence.this.remove(index);
        }

        public void close() {
        }

    }

    private final class ReverseArrayIterator<A> implements Iterator {

        private int index = getSize() - 1;

        @Override
        public boolean hasNext() {
            return getSize() > 0 && index >= 0;
        }

        @Override
        public A next() {
            if (index < 0) {
                throw new MishandleException("No more element");
            }
            return (A) get(index--);
        }

        @Override
        public boolean remove() {
            return AbstractSequence.this.remove(index);
        }

        @Override
        public void close() {
        }
    }

}
