
package science.unlicense.common.api.graph;

import science.unlicense.common.api.collection.Dictionary;

/**
 * An Edge is a link between two vertices.
 *
 * @author Johann Sorel
 */
public interface Edge {

    /**
     * Get first vertex.
     * @return Vertex first edge vertex
     */
    Vertex getVertex1();

    /**
     * Get second vertex.
     * @return Vertex second edge vertex
     */
    Vertex getVertex2();

    /**
     * Get Properties attached to this edge.
     * @return Dictionary
     */
    Dictionary getProperties();

    /**
     * A Loop is an edge which source and target vertex are the same.
     * @return true if edge is a loop
     */
    boolean isLoop();

    /**
     * Return the adjacent vertex of two edges.
     * @param edge edge to test
     * @return 0 : no adjacent vertices, 1 : first , 2 : second, 3 : both are adjacent
     */
    int getAdjacentVertices(Edge edge);

    /**
     * Two edge are parallale if they link the same vertices.
     * @param edge edge to test
     * @return true if edges are parallale
     */
    boolean isParallale(Edge edge);

}
