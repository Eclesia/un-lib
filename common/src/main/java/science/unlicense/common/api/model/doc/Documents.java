
package science.unlicense.common.api.model.doc;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;

/**
 *
 * @author Johann Sorel
 */
public final class Documents {

    private Documents(){}

    /**
     * Override fields values of base document by 'over' document.
     *
     * @param base
     * @param over
     */
    public static void overrideFields(Document base, Document over) {
        final Iterator ite = over.getPropertyNames().createIterator();
        while (ite.hasNext()) {
            final Chars name = (Chars) ite.next();
            base.setPropertyValue(name, over.getPropertyValue(name));
        }
    }


    public static Document mergeDoc(Document base, Document diff) {
        CObjects.ensureNotNull(base);
        CObjects.ensureNotNull(diff);

        Document output = base;

        final Iterator ite = diff.getPropertyNames().createIterator();
        while (ite.hasNext()) {
            final Chars name = (Chars) ite.next();
            final Field baseProp = base.getProperty(name);
            final Field diffProp = diff.getProperty(name);
            final Field outProp = output.getProperty(name);
            final Object[] baseValues = getFieldValues(baseProp);
            final Object[] diffValues = getFieldValues(diffProp);

            if (diffValues.length==0) {
                //nothing to replace
            } else if (baseValues.length==0) {
                //replace values
                setFieldValues(outProp, diffValues);
            } else {
                //smart merge
                final int size = Math.max(baseValues.length,diffValues.length);
                final Object[] res = new Object[size];
                for (int i=0;i<size;i++) {
                    if (baseValues.length<=i) {
                        //no more base values
                        res[i] = deepCopy(diffValues[i]);
                    } else if (diffValues.length<=i) {
                        //no more diff values
                        res[i] = deepCopy(baseValues[i]);
                    } else {
                        ///merge
                        Object bv = baseValues[i];
                        Object dv = diffValues[i];
                        if (bv instanceof Document || dv instanceof Document) {
                            //merge doc
                            res[i] = mergeDoc((Document) bv, (Document) dv);
                        } else {
                            //replace value
                            res[i] = deepCopy(dv);
                        }
                    }
                }
                setFieldValues(outProp, res);
            }
        }

        return output;
    }

    public static Object deepCopy(Object obj) {
        if (obj instanceof Document) {
            final Document copy = new DefaultDocument(true); //TODO : we don't known if the field order is important, copy it in any case

            final Document doc = (Document) obj;
            final Iterator ite = doc.getPropertyNames().createIterator();
            while (ite.hasNext()) {
                final Chars name = (Chars) ite.next();
                final Object[] values = getFieldValues(doc.getProperty(name));
                final Object[] cp = new Object[values.length];
                for (int i=0;i<cp.length;i++) {
                    cp[i] = deepCopy(values[i]);
                }
                setFieldValues(copy.getProperty(name), cp);
            }
            return copy;
        } else {
            return obj;
        }
    }

    private static Object[] getFieldValues(Field field) {
        Object value = field.getValue();
        if (value==null) {
            return Arrays.ARRAY_OBJECT_EMPTY;
        } else if (value.getClass().isArray()) {
            return (Object[]) value;
        } else {
            return new Object[]{value};
        }
    }

    private static void setFieldValues(Field field, Object value) {
        if (value==null) {
            field.setValue(null);
        } else if (value instanceof Object[] && ((Object[]) value).length==1) {
            field.setValue(((Object[]) value)[0]);
        } else if (value.getClass().isArray() && Arrays.getSize(value)==1) {
            field.setValue(Arrays.getValue(value,0));
        } else {
            field.setValue(value);
        }
    }


}
