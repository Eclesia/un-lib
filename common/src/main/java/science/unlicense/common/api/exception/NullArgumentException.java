
package science.unlicense.common.api.exception;

/**
 * Special case of invalid argument exception for null values.
 *
 * @author Johann Sorel
 */
public class NullArgumentException extends InvalidArgumentException {

    public NullArgumentException() {
    }

    public NullArgumentException(String s) {
        super(s);
    }

    public NullArgumentException(Throwable cause) {
        super(cause);
    }

    public NullArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

}
