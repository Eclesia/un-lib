
package science.unlicense.common.api.event;

import science.unlicense.common.api.References;
import science.unlicense.common.api.Releasable;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Weak event listener.
 *
 * @author Johann Sorel
 */
public class WeakEventListener implements EventListener,Releasable {

    private final References.Weak refSource;
    private final References.Weak refListener;
    private final Predicate predicate;

    public WeakEventListener(EventSource source, Predicate predicate, EventListener listener) {
        this.refSource = new References.Weak(source, this);
        this.refListener = new References.Weak(listener, this);
        this.predicate = predicate;
        source.addEventListener(predicate, this);
    }

    public void release() {
        final EventSource src1 = (EventSource) refSource.get();
        if (src1!=null) src1.removeEventListener(predicate, this);
    }

    public void receiveEvent(Event event) {
        final EventListener lst = (EventListener) refListener.get();
        if (lst!=null) {
            lst.receiveEvent(event);
        }
    }

}
