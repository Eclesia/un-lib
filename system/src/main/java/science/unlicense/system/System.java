
package science.unlicense.system;

import java.lang.reflect.InvocationTargetException;
import science.unlicense.common.api.exception.MishandleException;
import science.unlicense.system.api.desktop.TransferBag;

/**
 * Each environment should declare a System instance with exactly the same
 * methods  definition.
 *
 * @author Johann Sorel
 */
public abstract class System {

    public static science.unlicense.system.System SYSTEM;

    public static final String TYPE_UNIX = "unix";
    public static final String TYPE_WINDOWS = "windows";

    protected System(){
    }

    public static System get() {
        if (SYSTEM == null) {

            //try to load it by reflexion
            //this will work on JVM < 8, otherwise user must call himself System.init()
            try {
                Class<?> clazz = Class.forName("science.unlicense.runtime.Runtime");
                clazz.getMethod("init").invoke(null);
                if (SYSTEM != null) {
                    return SYSTEM;
                }
            } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
                //do nothing
            }

            throw new MishandleException("System has not been initialized, call science.unlicense.runtime.Runtime.init() at application start");
        }
        return SYSTEM;
    }

    public static void set(System system) {
        SYSTEM = system;
    }

    /**
     * System type, windows,linux,...
     * TODO : find a better way to define the os.
     *
     * @return String, never null
     */
    public abstract String getType();

    /**
     * Returns the module manager.
     *
     * @return ModuleManager, never null
     */
    public abstract ModuleManager getModuleManager();

    /**
     * Returns the socket manager.
     *
     * @return SocketManager, never null
     */
    public abstract SocketManager getSocketManager();

    /**
     * Returns the system properties.
     *
     * @return GlobalProperties, never null
     */
    public abstract GlobalProperties getProperties();

    /**
     * Returns the system clipboard.
     *
     * @return TransferBag, never null
     */
    public abstract TransferBag getClipBoard();

    /**
     * Returns the system drag and drop bag.
     *
     * @return TransferBag, never null
     */
    public abstract TransferBag getDragAndDrapBag();

    /**
     * Create a new work units.
     *
     * @param priority work priority, range should be between 0 and 100,
     *        0 for lowest priority and 100 for high priority.
     * @return never null
     */
    public abstract WorkUnits createWorkUnits(float priority);

}
