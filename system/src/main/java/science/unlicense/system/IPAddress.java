
package science.unlicense.system;

import science.unlicense.common.api.Arrays;

/**
 * Internet Protocol Address.
 * Stores an IPv4 or IPv6 address.
 *
 * @author Johann Sorel
 */
public class IPAddress {

    private final byte[] values;

    /**
     * Create an IPAddress from given byte array.
     *
     * @param values lenght must be 4 or 6 for IPv4 or IPv6
     */
    public IPAddress(byte[] values) {
        this.values = Arrays.copy(values);
    }

    /**
     * Get the adress value.
     * Lenght can be 4 or 6 if protocol IPv4 or IPv6.
     * @return byte[] never null
     */
    public byte[] getValues() {
        return Arrays.copy(values);
    }

}
