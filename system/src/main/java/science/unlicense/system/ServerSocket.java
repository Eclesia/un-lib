
package science.unlicense.system;

import science.unlicense.encoding.api.io.IOException;

/**
 * A Server side socket.
 * TODO, draft interface.
 *
 * @author Johann Sorel
 */
public interface ServerSocket {

    /**
     *
     * @return socket port.
     */
    int getPort();

    /**
     * Release this socket.
     */
    void close() throws IOException;

     /**
     * Whenever new clients connect to the server socket.
     * Each connection will open a client socket.
     * The handler decides what to do with each connection.
     *
     * @param clientsocket
     */
    public interface ClientHandler{

        /**
         * New client socket opened.
         * @param clientsocket
         */
        void create(ClientSocket clientsocket);

    }

}
