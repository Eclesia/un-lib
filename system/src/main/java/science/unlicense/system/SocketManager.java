
package science.unlicense.system;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 * In charge of managing sockets.
 *
 * @author Johann Sorel
 */
public interface SocketManager {

    /**
     * Resolve the IPAddress
     *
     * @param name host name
     * @return IPAddress never null
     * @throws IOException if host name could not be resolved
     */
    IPAddress resolve(Chars name) throws IOException;

    /**
     * Create a new client socker.
     *
     * @param host host name, not null
     * @param port socket port
     * @return ClientSocket never null
     * @throws IOException if failed to create socket
     */
    ClientSocket createClientSocket(Chars host, int port) throws IOException;

    /**
     * Create a new client socker.
     *
     * @param host host address, not null
     * @param port socket port
     * @return ClientSocket never null
     * @throws IOException if failed to create socket
     */
    ClientSocket createClientSocket(IPAddress host, int port) throws IOException;

    /**
     * Create a new server socker.
     *
     * @param port socket port
     * @param handler ServerSocket.ClientHandler in charge to manage client connections.
     * @return ServerSocket never null
     * @throws IOException if failed to create socket
     */
    ServerSocket createServerSocket(int port, ServerSocket.ClientHandler handler) throws IOException;

}
