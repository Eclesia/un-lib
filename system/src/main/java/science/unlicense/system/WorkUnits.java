
package science.unlicense.system;

/**
 * Overly simplistic parallel working pool.
 *
 * @author Johann Sorel
 */
public interface WorkUnits {

    void submit(long range, Work work);

    void dispose();
}
