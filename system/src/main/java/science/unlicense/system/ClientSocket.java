
package science.unlicense.system;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface ClientSocket {

    /**
     * @return socket address.
     */
    IPAddress getAddress();

    /**
     * @return socket port.
     */
    int getPort();

    /**
     * Get socket input stream.
     *
     * @return ByteOutputStream never null
     */
    ByteInputStream getInputStream() throws IOException;

    /**
     * Get socket output stream.
     *
     * @return ByteOutputStream never null
     */
    ByteOutputStream getOutputStream() throws IOException;

    /**
     * Release this socket.
     */
    void close() throws IOException;

}
