
package science.unlicense.system;

import java.lang.reflect.Array;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.common.api.predicate.Predicate;

/**
 *
 * Utility methods to search module resources and services.
 * Services must be registered in the meta tree in root group 'services'
 *
 * @author Johann Sorel
 */
public class ModuleSeeker {

    private ModuleSeeker(){}

    public static Object[] findServices(final Class serviceClass) {
        final NamedNode root = System.get().getModuleManager().getMetadataRoot().search(new Chars("services"));
        if (root==null){
            return (Object[]) Array.newInstance(serviceClass, 0);
        }
        final Predicate p = new Predicate() {
            @Override
            public Boolean evaluate(Object candidate) {
                if (candidate==null) return false;
                final Object v = ((NamedNode) candidate).getValue();
                return serviceClass.isInstance(v);
            }
        };
        return Nodes.getChildrenValues(root, p).toArray(serviceClass);
    }

    /**
     *  Search the main meta tree.
     * @param path
     * @param predicate
     * @return
     */
    public static Sequence searchNodes(final Chars path, final Predicate predicate){
        return searchNodes(System.get().getModuleManager().getMetadataRoot(), path, true, predicate);
    }

    /**
     *
     * @param tree
     * @param path
     * @param recursive
     * @param predicate
     * @return
     */
    public static Sequence searchNodes(final MetaTree tree, final Chars path,
            final boolean recursive, final Predicate predicate){

        final Sequence results = new ArraySequence();
        final NamedNode nn = tree.search(path);

        if (nn==null) return results;

        if (recursive){
            //use a visitor to loop on all sub-nodes
            NodeVisitor visitor = new NodeVisitor() {
                public Object visit(Node node, Object context) {
                    super.visit(node, context);
                    if (predicate==null || predicate.evaluate(node)){
                        results.add(node);
                    }
                    return null;
                }
            };
            visitor.visit(nn, null);
        } else {
            //simple loop
            for (Iterator ite=nn.getChildren().createIterator();ite.hasNext();) {
                final Object n = ite.next();
                if (predicate==null || predicate.evaluate(n)){
                    results.add(n);
                }
            }
        }

        return results;
    }

    /**
     *  Search the main meta tree.
     * @param path
     * @param predicate
     * @return
     */
    public static Sequence searchValues(final Chars path, final Predicate predicate){
        return searchValues(System.get().getModuleManager().getMetadataRoot(), path, true, predicate);
    }

    /**
     *
     * @param tree
     * @param path
     * @param recursive
     * @param predicate
     * @return
     */
    public static Sequence searchValues(final MetaTree tree, final Chars path,
            final boolean recursive, final Predicate predicate){

        final Sequence results = new ArraySequence();
        final NamedNode nn = tree.search(path);

        if (nn==null) return results;

        if (recursive){
            //use a visitor to loop on all sub-nodes
            NodeVisitor visitor = new NodeVisitor() {
                public Object visit(Node node, Object context) {
                    super.visit(node, context);
                    final NamedNode named = (NamedNode) node;
                    if (predicate==null || predicate.evaluate(named.getValue())){
                        results.add(named.getValue());
                    }
                    return null;
                }
            };
            visitor.visit(nn, null);
        } else {
            //simple loop
            for (Iterator ite=nn.getChildren().createIterator();ite.hasNext();) {
                final NamedNode named = (NamedNode) ite.next();
                if (predicate==null || predicate.evaluate(named.getValue())){
                    results.add(named.getValue());
                }
            }
        }

        return results;
    }

}
