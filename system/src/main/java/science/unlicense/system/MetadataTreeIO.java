
package science.unlicense.system;

import java.lang.reflect.Field;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.tree.DefaultNamedNode;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 * Utility class to read and write metadata trees from files.
 *
 * @author Johann Sorel
 */
public final class MetadataTreeIO {

    private static final Chars DEPTH = Chars.constant("    ");
    private static final Chars INTEGER = Chars.constant("i");
    private static final Chars DOUBLE = Chars.constant("d");
    private static final Chars CHARS = Chars.constant("c");
    private static final Chars INSTANCE = Chars.constant("instance");
    private static final Chars CONSTANT = Chars.constant("constant");

    private MetadataTreeIO(){}

    public static NamedNode read(ByteInputStream in) throws IOException, ClassNotFoundException, InstantiationException, NoSuchFieldException, InvalidArgumentException, IllegalAccessException {
        return read(readFile(in));
    }

    public static NamedNode read(Path u) throws IOException, ClassNotFoundException, InstantiationException, NoSuchFieldException, InvalidArgumentException, IllegalAccessException {
        return read(readFile(u));
    }

    private static NamedNode read(Chars text) throws IOException, ClassNotFoundException, InstantiationException, NoSuchFieldException, InvalidArgumentException, IllegalAccessException {

        final NamedNode root = new DefaultNamedNode(true);

        //current path
        Chars[] currentPath = new Chars[0];
        for (Chars line : text.split('\n')) {
            final int valueSeparator = line.getFirstOccurence('=');
            final Chars fullpath = (valueSeparator>0) ? line.truncate(0, valueSeparator) : line;

            if (line.trim().isEmpty() || line.trim().startsWith('#')) continue;

            final Chars[] path = toPath(fullpath);
            currentPath = merge(currentPath, path);

            //create or found path
            NamedNode node = root;
            for (int i=0; i<currentPath.length; i++) {
                node = getNode(node, currentPath[i], true);
            }

            //set value if any
            if (valueSeparator <= 0) {
                continue;
            }

            final Chars valueStr = line.truncate(valueSeparator+1, line.getCharLength());
            final int splitIndex = valueStr.getFirstOccurence(':');
            final Chars type = valueStr.truncate(0, splitIndex);
            final Chars value = valueStr.truncate(splitIndex+1, -1);
            if (INSTANCE.equals(type, true, true)) {
                final Class c = Class.forName(value.toString());
                final Object instance = c.newInstance();
                if (instance == null) throw new RuntimeException("Null instance for node : "+line);
                node.setValue(instance);
            } else if (CONSTANT.equals(type, true, true)) {
                final int s = value.getLastOccurence('.');
                final Chars className = value.truncate(0, s);
                final Chars propName = value.truncate(s+1, -1);
                final Class c = Class.forName(className.toString());
                final Field f = c.getDeclaredField(propName.toString());
                final Object constant = f.get(null);
                if (constant == null) throw new RuntimeException("Null constant for node : "+line);
                node.setValue(constant);
            } else if (INTEGER.equals(type, true, true)) {
                node.setValue(Int32.decode(value));
            } else if (DOUBLE.equals(type, true, true)) {
                node.setValue(Float64.decode(value));
            } else if (CHARS.equals(type, true, true)) {
                node.setValue(new Chars(value));
            } else {
                throw new RuntimeException("missing node value");
            }
        }

        return root;
    }

    public static void write(NamedNode node, Path url) throws IOException {
        final CharOutputStream cs = new CharOutputStream(url.createOutputStream(), CharEncodings.UTF_8);
        write(-1,node,cs);
        cs.close();
    }

    private static void write(int depth, NamedNode node, CharOutputStream out) throws IOException {

        for (int i=0; i<depth; i++) {
            out.write(DEPTH);
        }
        if (depth >= 0) {
            out.write(node.getName());

            final Object value = node.getValue();
            if (value != null) {
                out.write('=');
                if (value instanceof Integer) {
                    out.write(INTEGER).write(':').write(Int32.encode((Integer) value));
                } else if (value instanceof Double) {
                    out.write(DOUBLE).write(':').write(Float64.encode((Integer) value));
                } else if (value instanceof Chars) {
                    out.write(CHARS).write(':').write((Chars) value);
                } else {
                    throw new science.unlicense.encoding.api.io.IOException("Value class can not be stored : "+value.getClass());
                }
            }
            out.write('\n');
        }

        //store children nodes
        for (Iterator ite=node.getChildren().createIterator();ite.hasNext();) {
            write(depth+1, (NamedNode) ite.next(), out);
        }
    }

    /**
     * Merge the second tree in the first one.
     *
     * @param base
     * @param other
     * @return
     */
    public static void merge(NamedNode base, NamedNode other){
        //merge value
        if (other.getValue() != null) {
            if (base.getValue() != null) {
                if (!other.getValue().equals(base.getValue())) {
                    throw new InvalidArgumentException("Node for same path have different values "+base);
                }
            } else {
                base.setValue(other.getValue());
            }
        }

        //merge children
        for (Iterator ite=other.getChildren().createIterator();ite.hasNext();) {
            final NamedNode n = (NamedNode) ite.next();
            NamedNode sibling = base.getNamedChild(n.getName());
            if (sibling == null) {
                base.getChildren().add(n);
            } else {
                merge(sibling,n);
            }
        }
    }

    private static Chars[] toPath(Chars path) {
        Chars[] stack = new Chars[0];
        while (path.startsWith(DEPTH)) {
            stack = (Chars[]) Arrays.copy(stack, 0, stack.length+1);
            path = path.truncate(4, -1);
        }
        stack = (Chars[]) Arrays.copy(stack, 0, stack.length+1);
        stack[stack.length-1] = path;
        return stack;
    }

    private static Chars[] merge(Chars[] currentPath, Chars[] path) {
        final Chars[] result = path.clone();
        for (int i=0; i<result.length-1; i++) {
            if (result[i] == null) {
                result[i] = currentPath[i];
            }
        }
        return result;
    }

    private static NamedNode getNode(NamedNode parent, Chars name, boolean create) {
        for (Iterator ite=parent.getChildren().createIterator();ite.hasNext();) {
            final Object n = ite.next();
            if (((NamedNode) n).getName().equals(name, true, true)) {
                return (NamedNode) n;
            }
        }
        if (create) {
            //create it
            NamedNode n = new DefaultNamedNode(name,true);
            parent.getChildren().add(n);
            return n;
        } else {
            return null;
        }
    }

    private static Chars readFile(Path path) throws IOException {
        final ByteInputStream stream = path.createInputStream();
        return readFile(stream);
    }

    private static Chars readFile(ByteInputStream stream) throws IOException {
        final CharBuffer sb  = new CharBuffer();
        final CharInputStream cs = new CharInputStream(stream, CharEncodings.UTF_8);
        Chars line;
        try {
            while ((line = cs.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } finally {
            stream.dispose();
        }
        return sb.toChars();
    }
}
