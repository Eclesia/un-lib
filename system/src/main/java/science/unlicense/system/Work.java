
package science.unlicense.system;

/**
 *
 * @author Johann Sorel
 */
public interface Work {

    void run(long index);

}
