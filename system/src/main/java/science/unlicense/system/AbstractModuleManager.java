

package science.unlicense.system;


/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractModuleManager implements ModuleManager {

    public MetaTree getMetadataRoot() {
        return getMetadataRoot(DEFAULT_PATH);
    }

}
