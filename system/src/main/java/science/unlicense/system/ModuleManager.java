
package science.unlicense.system;

import science.unlicense.common.api.collection.Set;

/**
 * Each environement should provide a module manager instance
 * capable loading and unloading modules on the fly.
 *
 * @author Johann Sorel
 */
public interface ModuleManager {

    public static final String DEFAULT_PATH = "metadata";

    /**
     * Get the default tree structure.
     * Equivalent to getMetadataRoot("metadata");
     *
     * @return Node, never null
     */
    MetaTree getMetadataRoot();

    /**
     * Get a metadata tree structure.
     * The tree structure is loaded by merging the 'name' files from each module.
     * Each file is expected to be in STML syntax.
     *
     * @param name
     * @return Node, never null
     */
    MetaTree getMetadataRoot(String name);

    /**
     * List currently loaded modules.
     *
     * @return Set, never null
     */
    Set getModules();


}
