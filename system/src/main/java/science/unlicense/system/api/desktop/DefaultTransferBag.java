
package science.unlicense.system.api.desktop;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;

/**
 * Default transfer bag using an ArraySequence as back end.
 *
 * @author Johann Sorel
 */
public class DefaultTransferBag extends AbstractTransferBag {

    private final Sequence bag = new ArraySequence();

    public Sequence getAttachments() {
        return bag;
    }

}
