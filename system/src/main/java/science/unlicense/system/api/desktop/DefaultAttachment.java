
package science.unlicense.system.api.desktop;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;

/**
 * Default attachment.
 *
 * @author Johann Sorel
 */
public class DefaultAttachment implements Attachment {

    private final Dictionary metas = new HashDictionary();
    private final Object value;

    public DefaultAttachment(Object value, Chars mimeType) {
        CObjects.ensureNotNull(mimeType, META_MIME_TYPE.toString());
        this.value = value;
        this.metas.add(META_MIME_TYPE, mimeType);
    }

    public Dictionary getMetadatas() {
        return metas;
    }

    public Object getObject() {
        return value;
    }

}
