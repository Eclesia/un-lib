
package science.unlicense.system.api.desktop;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;

/**
 * A transfer bag is a place where objects are shared between objects and applications.
 * Operating systems offer two default bags know as the clipboard and drag&drop.
 *
 * @author Johann Sorel
 */
public interface TransferBag {

    /**
     * List of attachments in the bag.
     * This is a live sequence, not a snapshot, meaning events occurs
     * when new attachments are added and removed.
     *
     * @return sequence, never null
     */
    Sequence getAttachments();

    /**
     * Search for the first attachment matching given mime type and value class.
     *
     * @param mimeType searched mime type, can be null
     * @param expectedType searched value class, can be null.
     * @return attachement or null if none find matching criterias
     */
    Attachment getAttachment(Chars mimeType, Class expectedType);

}
