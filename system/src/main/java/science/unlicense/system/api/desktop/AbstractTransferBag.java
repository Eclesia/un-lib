
package science.unlicense.system.api.desktop;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;

/**
 * Default transfer bag using an ArraySequence as back end.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTransferBag implements TransferBag {

    @Override
    public Attachment getAttachment(Chars mimeType, Class expectedType) {

        final Object[] attachments = getAttachments().toArray();

        for (int i=0;i<attachments.length;i++) {
            final Attachment att = (Attachment) attachments[i];

            if (mimeType != null) {
                Object mime = att.getMetadatas().getValue(Attachment.META_MIME_TYPE);
                if (!CObjects.equals(mime, mimeType)) continue;
            }

            if (expectedType != null) {
                final Object value = att.getObject();
                if (!expectedType.isInstance(value)) {
                    continue;
                }
            }

            return att;
        }

        return null;
    }

}
