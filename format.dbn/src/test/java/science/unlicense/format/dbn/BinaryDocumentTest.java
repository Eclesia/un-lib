

package science.unlicense.format.dbn;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocumentType;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.FieldType;

/**
 *
 * @author Johann Sorel
 */
public class BinaryDocumentTest {

    /**
     * Test converting a document to document type and reverse.
     *
     */
    @Test
    public void testDocToDocType() {

        //create type
        final BinaryFieldType fieldBool = BinaryFieldType.bool(
                new Chars("fieldBool"), new Chars("fieldBoolTitle"), new Chars("fieldBoolDesc"),
                1, 1, null,null);
        final FieldType[] fields = new FieldType[]{
            fieldBool
        };
        final DocumentType docType = new DefaultDocumentType(
                new Chars("typeId"),
                new Chars("typeTitle"),
                new Chars("typeDesc"),
                true, fields, null);


        //convert to document
        final Document doc = BinaryDocuments.toDocument(docType);
        Assert.assertNotNull(doc);

        //convert back to doctype
        final DocumentType result = BinaryDocuments.toDocumentType(doc);
        result.equals(docType);
        Assert.assertEquals(docType, result);

    }

    @Test
    public void testDocToDocType2() {

        final DocumentType docType = BinaryDocuments.REGISTERTYPE;

        //convert to document
        final Document doc = BinaryDocuments.toDocument(docType);
        Assert.assertNotNull(doc);

        //convert back to doctype
        final DocumentType result = BinaryDocuments.toDocumentType(doc);
        result.equals(docType);
        Assert.assertEquals(docType, result);

    }

    @Test
    public void testHeritageReadWriteDocType() {

        final FieldType[] fieldsupers = new FieldType[]{
            BinaryFieldType.text(new Chars("fieldSuper"), new Chars("fieldSuperTitle"), new Chars("fieldSuperDesc"), 1, 1, null, CharEncodings.UTF_8, null)
        };
        final DocumentType superType = new DefaultDocumentType(
                new Chars("superId"),
                new Chars("superTitle"),
                new Chars("superDesc"),
                true, fieldsupers, null);

        final FieldType[] fields = new FieldType[]{
            BinaryFieldType.bool(new Chars("fieldBool"), new Chars("fieldBoolTitle"), new Chars("fieldBoolDesc"), 1, 1, null,null)
        };
        final DocumentType docType = new DefaultDocumentType(
                new Chars("typeId"),
                new Chars("typeTitle"),
                new Chars("typeDesc"),
                true, fields, null, new DocumentType[]{superType});

        //convert to document
        final Document doc = BinaryDocuments.toDocument(docType);
        Assert.assertNotNull(doc);

        //convert back to doctype
        final DocumentType result = BinaryDocuments.toDocumentType(doc);
        result.equals(docType);
        Assert.assertEquals(docType, result);
    }

}
