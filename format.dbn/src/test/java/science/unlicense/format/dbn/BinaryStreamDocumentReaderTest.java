
package science.unlicense.format.dbn;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.dbn.BinaryFieldValueType.Mapping.FLOAT32LE;
import static science.unlicense.format.dbn.BinaryFieldValueType.Mapping.FLOAT64LE;
import static science.unlicense.format.dbn.BinaryFieldValueType.Mapping.INT16LE;
import static science.unlicense.format.dbn.BinaryFieldValueType.Mapping.INT32LE;
import static science.unlicense.format.dbn.BinaryFieldValueType.Mapping.INT64LE;
import static science.unlicense.format.dbn.BinaryFieldValueType.Mapping.INT8;
import static science.unlicense.format.dbn.BinaryFieldValueType.Mapping.UINT16LE;
import static science.unlicense.format.dbn.BinaryFieldValueType.Mapping.UINT32LE;
import static science.unlicense.format.dbn.BinaryFieldValueType.Mapping.UINT64LE;
import static science.unlicense.format.dbn.BinaryFieldValueType.Mapping.UINT8;
import static science.unlicense.format.dbn.BinaryFieldValueType.Mapping.ULEB128;

/**
 *
 * @author Johann Sorel
 */
public class BinaryStreamDocumentReaderTest {

    @Test
    public void testRead() throws IOException {

        final FieldType[] fields = new FieldType[]{
                    BinaryFieldType.bool(new Chars("bool"), 1, 1, null, null),
                    BinaryFieldType.numeric(new Chars("byte"), 1, 1, INT8.code, null,  null),
                    BinaryFieldType.numeric(new Chars("ubyte"), 1, 1, UINT8.code, null,  null),
                    BinaryFieldType.numeric(new Chars("short"), 1, 1, INT16LE.code, null,  null),
                    BinaryFieldType.numeric(new Chars("ushort"), 1, 1, UINT16LE.code, null,  null),
                    BinaryFieldType.numeric(new Chars("int"), 1, 1, INT32LE.code, null,  null),
                    BinaryFieldType.numeric(new Chars("uint"), 1, 1, UINT32LE.code, null,  null),
                    BinaryFieldType.numeric(new Chars("long"), 1, 1, INT64LE.code, null,  null),
                    BinaryFieldType.numeric(new Chars("ulong"), 1, 1, UINT64LE.code, null,  null),
                    BinaryFieldType.numeric(new Chars("float"), 1, 1, FLOAT32LE.code, null,  null),
                    BinaryFieldType.numeric(new Chars("double"), 1, 1, FLOAT64LE.code, null,  null),
                    BinaryFieldType.numeric(new Chars("uvint"), 1, 1, ULEB128.code, null,  null)
                };
        final DocumentType docType = new BinaryDocumentType(new Chars("id"), null, null, true, fields,null);

        final Document doc = new DefaultDocument(false, docType);
        doc.setPropertyValue(new Chars("bool"), Boolean.TRUE);
        doc.setPropertyValue(new Chars("byte"), (byte)-123);
        doc.setPropertyValue(new Chars("ubyte"), (byte) 210);
        doc.setPropertyValue(new Chars("short"), (short)-45);
        doc.setPropertyValue(new Chars("ushort"), (int) 780);
        doc.setPropertyValue(new Chars("int"), -900);
        doc.setPropertyValue(new Chars("uint"), 1234l);
        doc.setPropertyValue(new Chars("long"), -456789l);
        doc.setPropertyValue(new Chars("ulong"), 456789l);
        doc.setPropertyValue(new Chars("float"), 3.14f);
        doc.setPropertyValue(new Chars("double"), 3.14);
        doc.setPropertyValue(new Chars("uvint"), 42l);

        final ArrayOutputStream out = new ArrayOutputStream();
        DocumentWriter.writeDoc(new DataOutputStream(out), doc, 0,false);

        final ArrayInputStream in = new ArrayInputStream(out.getBuffer().toArrayByte());
        final BinaryStreamDocumentReader reader = new BinaryStreamDocumentReader(docType);
        reader.setInput(in);
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_DOC_START,reader.next());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[0],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals(Boolean.TRUE,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[1],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals((byte)-123,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[2],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals((byte) 210,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[3],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals((short)-45,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[4],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals((int) 780,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[5],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals(-900,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[6],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals(1234l,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[7],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals(-456789l,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[8],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals(456789l,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[9],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals(3.14f,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[10],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals(3.14,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_FIELD,reader.next());
        Assert.assertEquals(fields[11],reader.getFieldType());
        Assert.assertEquals(1,reader.getFieldValueCount());
        Assert.assertEquals(42l,reader.getFieldValue());
        Assert.assertEquals(BinaryStreamDocumentReader.TYPE_DOC_END,reader.next());

    };

}
