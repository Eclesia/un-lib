
package science.unlicense.format.dbn;

import java.lang.reflect.Array;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class DocumentWriter extends AbstractWriter {

    private int mode = 0;

    public DocumentWriter() {
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    /**
     * Write document.
     *
     * @param doc
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public void write(Document doc) throws IOException {
        final DataOutputStream ds = getOutputAsDataStream(Endianness.BIG_ENDIAN);
        writeDoc(ds,doc,mode,false);
    }

    public static void writeDoc(DataOutputStream ds, Document doc, int mode, boolean inline) throws IOException {
        final DocumentType docType = doc.getType();
        final FieldType[] fields = docType.getFields();

        if (mode==0) {
            //streamed mode
            if (!inline) ds.writeUByte(mode);

             for (int i=0;i<fields.length;i++) {
                final BinaryFieldType field = (BinaryFieldType) fields[i];
                final BinaryFieldValueType valueType = field.getValueType();
                final Chars id = field.getId();
                final int minOcc = field.getMinOccurences();
                final int maxOcc = field.getMaxOccurences();
                final Object value = doc.getPropertyValue(id);

                //write number of occurence
                final int nbOcc;
                if (minOcc == 0 && maxOcc == 1) {
                    if (value==null) {
                        ds.writeBit(0);
                        continue;
                    }
                    ds.writeBit(1);
                    nbOcc = 1;
                } else if (maxOcc != minOcc) {

                    if (valueType.getMapping()==BinaryFieldValueType.Mapping.DOCUMENT) {
                        //use progressive mode for document types
                        ds.writeVarLengthUInt(0);
                        nbOcc=-1;
                    } else {
                        nbOcc = ((Collection) value).getSize();
                        ds.writeVarLengthUInt(nbOcc-minOcc+1);
                        if (nbOcc==0) continue;
                    }
                } else {
                    nbOcc = maxOcc;
                }
                ds.skipToByteEnd();

                //write values
                if (maxOcc==1) {
                    writeFieldValue(ds, valueType,value);
                } else if (nbOcc==-1) {
                    final Collection values = (Collection) value;
                    final Iterator ite = values.createIterator();
                    while (ite.hasNext()) {
                        ds.write((byte) 1);
                        writeFieldValue(ds, valueType,ite.next());
                    }
                    ds.write((byte) 0);//end
                } else {
                    final Collection values = (Collection) value;
                    final Iterator ite = values.createIterator();
                    while (ite.hasNext()) {
                        writeFieldValue(ds, valueType,ite.next());
                    }
                }


             }

        } else {
            //indexed mode
            throw new IOException(ds, "Not supported yet.");
        }

    }

    public static void writeFieldValue(DataOutputStream ds, BinaryFieldValueType valueType, Object value) throws IOException {

        //write array size
        final long[] arraySize = valueType.getArraySize();
        Object oneValue = value;
        if (arraySize!=null) {
            for (int k=0;k<arraySize.length;k++) {
                if (arraySize[k]<=0) {
                    Array.getLength(oneValue);
                }
                oneValue = Array.get(oneValue, 0);
            }
        }

        //write values
        if (arraySize!=null) {
            writeArrayValue(ds, valueType, value);
        } else {
            writeSingleValue(ds, valueType, value);
        }
    }

    private static void writeArrayValue(DataOutputStream ds, BinaryFieldValueType valueType, Object value) throws IOException {
        throw new IOException(ds, "Not supported yet.");
    }

    public static void writeSingleValue(DataOutputStream ds, BinaryFieldValueType valueType, Object value) throws IOException {
        final BinaryFieldValueType.Mapping fieldType = valueType.getMapping();
        final Endianness ne = valueType.getNumberEncoding();
        final CharEncoding ce = valueType.getCharEncoding();
        final DocumentType dt = valueType.getDocumentType();

        switch (fieldType) {
            case BOOL_BIT:    ds.writeBit( ((Boolean) value) ? 1 : 0 );break;
            case BOOL_BYTE:   ds.writeByte( ((Boolean) value) ? (byte) 1 : (byte) 0 );break;
            case INT8:        ds.writeByte( (Byte) value );break;
            case UINT8:       ds.writeUByte( (Byte) value );break;
            case INT16LE:     ds.writeShort( (Short) value,ne);break;
            case UINT16LE:    ds.writeUShort( (Integer) value,ne);break;
            case INT32LE:     ds.writeInt( (Integer) value,ne);break;
            case UINT32LE:    ds.writeUInt( (Long) value,ne);break;
            case INT64LE:     ds.writeLong( (Long) value,ne);break;
            case UINT64LE:    ds.writeLong( (Long) value,ne);break;
            case FLOAT32LE:   ds.writeFloat( (Float) value,ne);break;
            case FLOAT64LE:   ds.writeDouble( (Double) value,ne);break;
            case INT16BE:     ds.writeShort( (Short) value,ne);break;
            case UINT16BE:    ds.writeUShort( (Integer) value,ne);break;
            case INT32BE:     ds.writeInt( (Integer) value,ne);break;
            case UINT32BE:    ds.writeUInt( (Long) value,ne);break;
            case INT64BE:     ds.writeLong( (Long) value,ne);break;
            case UINT64BE:    ds.writeLong( (Long) value,ne);break;
            case FLOAT32BE:   ds.writeFloat( (Float) value,ne);break;
            case FLOAT64BE:   ds.writeDouble( (Double) value,ne);break;
            case LEB128:      ds.writeVarLengthInt((Long) value );break;
            case ULEB128:     ds.writeVarLengthUInt((Long) value );break;
            case VARBITS:     ds.writeBits(((Long) value).intValue(), valueType.getNbBit());break;
            case CHARS:
                final byte[] data = ((Chars) value).toBytes(ce);
                ds.writeVarLengthUInt(data.length);
                ds.write(data);
                break;
            case DOCUMENT:
                //sub documents always in stream mode
                writeDoc(ds,(Document) value,0,valueType.isInline());
                break;
            case VARYING: {
                valueType = BinaryFieldValueType.forValue(value);

            }
            default: throw new InvalidArgumentException("Unexpected field type : "+fieldType);
        }
    }

}
