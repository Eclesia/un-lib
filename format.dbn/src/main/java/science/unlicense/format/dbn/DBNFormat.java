
package science.unlicense.format.dbn;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * DBN Format.
 *
 * Specification :
 * http://unlicense.developpez.com/specifications/index.html
 *
 *
 * @author Johann Sorel
 */
public class DBNFormat extends DefaultFormat {

    public static final byte[] SIGNATURE = new byte[]{'D','B','N',' '};

    public DBNFormat() {
        super(new Chars("dbn"));
        shortName = new Chars("DBN");
        longName = new Chars("Document as binary");
        extensions.add(new Chars("dbn"));
        signatures.add(SIGNATURE);
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
