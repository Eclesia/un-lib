
package science.unlicense.format.dbn;

import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class DBNWriter extends AbstractWriter {

    public void write(Document doc) throws IOException {
        write(doc,true,0);
    }

    public void write(Document doc, boolean writeType, int mode) throws IOException {

        final DataOutputStream ds = getOutputAsDataStream(Endianness.LITTLE_ENDIAN);
        ds.write(DBNFormat.SIGNATURE);
        ds.writeUInt(1);
        if (writeType) {
            DocumentWriter.writeDoc(ds, BinaryDocuments.toDocument(doc.getType()),mode,false);
        }
        DocumentWriter.writeDoc(ds,doc,mode,false);
    }

}
