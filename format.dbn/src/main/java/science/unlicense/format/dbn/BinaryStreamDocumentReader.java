
package science.unlicense.format.dbn;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Binary document reader reading fields one after another without loading the all
 * document in memory.
 *
 * @author Johann Sorel
 */
public class BinaryStreamDocumentReader extends AbstractReader {

    public static final int TYPE_DOC_START = 1;
    public static final int TYPE_DOC_END = 2;
    public static final int TYPE_FIELD = 3;

    private final DocumentType docType;
    private final FieldType[] fields;
    private int currentFieldIndex = -2;
    private BinaryFieldType currentFieldType;
    private int fieldValueCount = 0;
    private int currentValueIndex = 0;
    private Integer docForm;

    /**
     *
     * @param docType not null.
     */
    public BinaryStreamDocumentReader(DocumentType docType) {
        CObjects.ensureNotNull(docType);
        this.docType = docType;
        this.fields = docType.getFields();
    }

    /**
     * Returns this reader document type.
     *
     * @return DocumentType, never null
     */
    public DocumentType getDocumentType() {
        return docType;
    }

    /**
     * Returns the current field type.
     * @return FieldType, null if reader is not on a field.
     */
    public FieldType getFieldType() {
        return currentFieldType;
    }

    /**
     * Returns the number of values for the current field.
     * @return number of field values, -1 if number is unknown
     */
    public int getFieldValueCount() {
        return fieldValueCount;
    }

    /**
     * Move to next element.
     *
     * @return
     * @throws IOException
     */
    public int next() throws IOException {

        if (currentFieldIndex==-2) {
           //start reading
            final DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);
            docForm = ds.read();
            currentFieldIndex=-1;
            return TYPE_DOC_START;
        } else if (currentFieldIndex>=fields.length) {
            //end document
            return TYPE_DOC_END;
        } else {
            //read fields
            final DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);
            if (docForm == 0) {
                //streamed mode
                //skip any not read values
                skipFieldValues();

                while (currentFieldIndex<fields.length-1) {
                    currentFieldIndex++;
                    currentFieldType = (BinaryFieldType) fields[currentFieldIndex];
                    fieldValueCount = DocumentReader.readNbOccurence(ds, currentFieldType);
                    currentValueIndex = 0;
                    return TYPE_FIELD;
                }

                //no fields left
                currentFieldType = null;
                fieldValueCount = 0;
                return TYPE_DOC_END;

            } else if (docForm == 1) {
                //indexed mode
                throw new IOException(ds, "No supported yet.");
            } else {
                throw new IOException(ds, "Unvalid encoding mode "+docForm);
            }
        }
    }

    /**
     * Read one field value.
     *
     * @return field value
     * @throws IOException
     */
    public Object getFieldValue() throws IOException{
        if (fieldValueCount==-1) {
            //progressive mode
            if (currentValueIndex==-1) {
                throw new IOException(this, "No more values to read.");
            } else {
                currentValueIndex++;
                final DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);
                if (ds.read()!=0) {
                    return DocumentReader.readFieldValue(ds, currentFieldType.getValueType());
                } else {
                    currentValueIndex=-1;
                    return null;
                }
            }
        } else {
            if (currentValueIndex>=fieldValueCount) {
                throw new IOException(this, "No more values to read.");
            }
            currentValueIndex++;
            final DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);
            return DocumentReader.readFieldValue(ds, currentFieldType.getValueType());
        }
    }

    /**
     * Skip all field values.
     *
     * @throws IOException
     */
    public void skipFieldValues() throws IOException{
        while (currentValueIndex<fieldValueCount) {
            skipFieldValue();
        }
    }

    /**
     * Skip a single field value.
     * @throws IOException
     */
    public void skipFieldValue() throws IOException{
        if (currentValueIndex>=fieldValueCount) return; //already skipped
        currentValueIndex++;
        final DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);
        DocumentReader.skipFieldValue(ds, currentFieldType.getValueType());
    }

}
