
package science.unlicense.format.dbn;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.model.doc.DefaultFieldType;
import science.unlicense.common.api.model.doc.DocumentType;

/**
 *
 * @author Johann Sorel
 */
public class BinaryFieldType extends DefaultFieldType {

    private final BinaryFieldValueType valueType;

    public BinaryFieldType(Chars id, CharArray title, CharArray description, int minOcc, int maxOcc,
            int primitiveType, Class valueClass, Object defaultValue, CharEncoding charEncoding,
            long[] arraySize, DocumentType subType, boolean inline, Dictionary atributes) {
        this(id,title,description,minOcc,maxOcc,defaultValue,
                new BinaryFieldValueType(primitiveType, charEncoding, subType, arraySize, inline), atributes);
    }

    public BinaryFieldType(Chars id, CharArray title, CharArray description, int minOcc, int maxOcc,
            Object defaultValue, BinaryFieldValueType valueType, Dictionary atributes) {
        super(id, title, description, minOcc, maxOcc, valueType.getValueClass(), defaultValue, valueType.getDocumentType(),atributes);
        this.valueType = valueType;
    }

    public BinaryFieldValueType getValueType() {
        return valueType;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BinaryFieldType other = (BinaryFieldType) obj;
        if (!this.valueType.equals(other.valueType)) {
            return false;
        }
        return super.equals(obj);
    }

    public int getHash() {
        return super.getHash() + valueType.getHash();
    }

    public static BinaryFieldType bool(Chars name, int minOcc, int maxOcc, long[] size, Dictionary attributes) {
        return bool(name, null, null, minOcc, maxOcc, size, attributes);
    }

    public static BinaryFieldType bool(Chars name, Chars title, Chars desc, int minOcc, int maxOcc, long[] size, Dictionary attributes) {
        return new BinaryFieldType(name, title, desc, minOcc, maxOcc, null, new BinaryFieldValueType(BinaryFieldValueType.Mapping.BOOL_BYTE.code, null, null, size, true), attributes);
    }

    public static BinaryFieldType numeric(Chars name, int minOcc, int maxOcc, int type, long[] size, Dictionary attributes) {
        return numeric(name, null, null, minOcc, maxOcc, type, size, attributes);
    }

    public static BinaryFieldType numeric(Chars name, Chars title, Chars desc, int minOcc, int maxOcc, int type, long[] size, Dictionary attributes) {
        return new BinaryFieldType(name, title, desc, minOcc, maxOcc, null, new BinaryFieldValueType(type, null, null, size, true), attributes);
    }

    public static BinaryFieldType text(Chars name, int minOcc, int maxOcc, long[] size, CharEncoding charEncoding, Dictionary attributes) {
        return text(name, null, null, minOcc, maxOcc, size, charEncoding, attributes);
    }

    public static BinaryFieldType text(Chars name, Chars title, Chars desc, int minOcc, int maxOcc, long[] size, CharEncoding charEncoding, Dictionary attributes) {
        return new BinaryFieldType(name, title, desc, minOcc, maxOcc, null, new BinaryFieldValueType(BinaryFieldValueType.Mapping.CHARS.code, charEncoding, null, size, true),attributes);
    }

    public static BinaryFieldType doc(Chars name, int minOcc, int maxOcc, long[] size, DocumentType subType, boolean inline, Dictionary attributes) {
        return doc(name, null, null, minOcc, maxOcc, size, subType, inline, attributes);
    }

    public static BinaryFieldType doc(Chars name, Chars title, Chars desc, int minOcc, int maxOcc, long[] size, DocumentType subType, boolean inline, Dictionary attributes) {
        return new BinaryFieldType(name, title, desc, minOcc, maxOcc, null, new BinaryFieldValueType(BinaryFieldValueType.Mapping.DOCUMENT.code, null, subType, size, inline),attributes);
    }

}
