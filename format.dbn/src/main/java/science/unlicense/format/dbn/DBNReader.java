
package science.unlicense.format.dbn;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class DBNReader extends AbstractReader {

    private DocumentType docType;

    public DBNReader() {
        this(null);
    }

    /**
     *
     * @param docType
     */
    public DBNReader(DocumentType docType) {
        this.docType = docType;
    }

    public DocumentType getDocumentType() throws IOException {
        readHeader();
        return docType;
    }

    public Document read() throws IOException {
        readHeader();
        final DataInputStream ds = getInputAsDataStream(Endianness.LITTLE_ENDIAN);
        return DocumentReader.readDoc(ds, docType,false);
    }

    public BinaryStreamDocumentReader AsStream() throws IOException {
        readHeader();
        final DataInputStream ds = getInputAsDataStream(Endianness.LITTLE_ENDIAN);
        final BinaryStreamDocumentReader reader = new BinaryStreamDocumentReader(docType) {
            @Override
            public void dispose() throws IOException {
                super.dispose();
                DBNReader.this.dispose();
            }
        };
        reader.setInput(ds);
        return reader;
    }

    private void readHeader() throws IOException {
        final DataInputStream ds = getInputAsDataStream(Endianness.LITTLE_ENDIAN);
        if (!Arrays.equals(DBNFormat.SIGNATURE,ds.readBytes(4))) {
            throw new IOException(this, "Unvalid file signature.");
        }
        final long version = ds.readUInt();
        if (version != 1) {
            throw new IOException(this, "Unsupported version "+version);
        }
        final Document ddType = DocumentReader.readDoc(ds, BinaryDocuments.REGISTERTYPE,false);
        docType = BinaryDocuments.toDocumentType(ddType);
        //skip any remaining bits
        ds.skipToByteEnd();
    }

}
