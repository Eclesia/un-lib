
package science.unlicense.format.dbn;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DocumentTypeBuilder;

/**
 * Builder for Document types.
 *
 * @author Johann Sorel
 */
public class BinaryDocumentTypeBuilder extends DocumentTypeBuilder {

    public BinaryDocumentTypeBuilder() {
    }

    public BinaryDocumentTypeBuilder(Chars id) {
        this.id = id;
    }

    public BinaryDocumentTypeBuilder id(Chars id) {
        super.id(id);
        return this;
    }

    public BinaryDocumentTypeBuilder title(CharArray title) {
        super.title(title);
        return this;
    }

    public BinaryDocumentTypeBuilder description(CharArray description) {
        super.description(description);
        return this;
    }

    public BinaryDocumentTypeBuilder strict(boolean strict) {
        super.description(description);
        return this;
    }

    public BinaryFieldTypeBuilder addField(Chars id) {
        final BinaryFieldTypeBuilder ftb = new BinaryFieldTypeBuilder();
        ftb.id(id);
        fields.add(ftb);
        return ftb;
    }

}
