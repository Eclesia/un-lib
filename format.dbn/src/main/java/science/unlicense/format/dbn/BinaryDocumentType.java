
package science.unlicense.format.dbn;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.model.doc.DefaultDocumentType;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.FieldType;

/**
 *
 * @author Johann Sorel
 */
final class BinaryDocumentType extends DefaultDocumentType {

    public BinaryDocumentType(Chars id, CharArray title, CharArray description, boolean strict, FieldType[] fields, Dictionary attributes) {
        super(id, title, description, strict, fields, attributes, null);
    }

    public BinaryDocumentType(Chars id, CharArray title, CharArray description, boolean strict, FieldType[] fields, Dictionary attributes, DocumentType[] parents) {
        super(id, title, description, strict, fields, attributes, parents);
    }

    FieldType[] getFieldsInternal() {
        return localFields;
    }

    DocumentType[] getSuperInternal() {
        return parentTypes;
    }

    void reindexInternal() {
        reindex();
    }

}
