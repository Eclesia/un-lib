
package science.unlicense.format.dbn;

import java.lang.reflect.Array;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class BinaryFieldValueType {

    public static enum Mapping {
        BOOL_BIT    (0,Boolean.class,boolean.class,null),
        BOOL_BYTE   (1,Boolean.class,boolean.class,null),
        INT8        (2,Byte.class,byte.class,null),
        UINT8       (3,Byte.class,byte.class,null),
        INT16LE     (4,Short.class,short.class,Endianness.LITTLE_ENDIAN),
        UINT16LE    (5,Integer.class,int.class,Endianness.LITTLE_ENDIAN),
        INT32LE     (6,Integer.class,int.class,Endianness.LITTLE_ENDIAN),
        UINT32LE    (7,Long.class,long.class,Endianness.LITTLE_ENDIAN),
        INT64LE     (8,Long.class,long.class,Endianness.LITTLE_ENDIAN),
        UINT64LE    (9,Long.class,long.class,Endianness.LITTLE_ENDIAN),
        FLOAT32LE   (10,Float.class,float.class,Endianness.LITTLE_ENDIAN),
        FLOAT64LE   (11,Double.class,double.class,Endianness.LITTLE_ENDIAN),
        INT16BE     (12,Short.class,short.class,Endianness.BIG_ENDIAN),
        UINT16BE    (13,Integer.class,int.class,Endianness.BIG_ENDIAN),
        INT32BE     (14,Integer.class,int.class,Endianness.BIG_ENDIAN),
        UINT32BE    (15,Long.class,long.class,Endianness.BIG_ENDIAN),
        INT64BE     (16,Long.class,long.class,Endianness.BIG_ENDIAN),
        UINT64BE    (17,Long.class,long.class,Endianness.BIG_ENDIAN),
        FLOAT32BE   (18,Float.class,float.class,Endianness.BIG_ENDIAN),
        FLOAT64BE   (19,Double.class,double.class,Endianness.BIG_ENDIAN),
        LEB128      (20,Long.class,long.class,null),
        ULEB128     (21,Long.class,long.class,null),
        CHARS       (22,Chars.class,Chars.class,null),
        DOCUMENT    (23,Document.class,Document.class,null),
        VARYING     (24,Object.class,Object.class,null),
        VARBITS     (0x80,Long.class,long.class,null);

        final int code;
        final Class valueClass;
        final Class primitiveClass;
        final Endianness encoding;

        Mapping(int code, Class valueClass, Class primitiveClass, Endianness encoding) {
            this.code = code;
            this.valueClass = valueClass;
            this.primitiveClass = primitiveClass;
            this.encoding = encoding;
        }

        static Mapping forCode(int code) {
            for (Mapping m : Mapping.values()) {
                if (m.code == code) {
                    return m;
                }
            }
            throw new InvalidArgumentException("Unknown code "+code);
        }

    }

    private static final Dictionary CLASS_TO_VALUETYPE = new HashDictionary();
    static {
        CLASS_TO_VALUETYPE.add(Boolean.class,   new BinaryFieldValueType(Mapping.BOOL_BYTE.code, null, null, null,true));
        CLASS_TO_VALUETYPE.add(Byte.class,      new BinaryFieldValueType(Mapping.INT8.code, null, null, null,true));
        CLASS_TO_VALUETYPE.add(Short.class,     new BinaryFieldValueType(Mapping.INT16LE.code, null, null, null,true));
        CLASS_TO_VALUETYPE.add(Integer.class,   new BinaryFieldValueType(Mapping.INT32LE.code, null, null, null,true));
        CLASS_TO_VALUETYPE.add(Long.class,      new BinaryFieldValueType(Mapping.INT64LE.code, null, null, null,true));
        CLASS_TO_VALUETYPE.add(Float.class,     new BinaryFieldValueType(Mapping.FLOAT32LE.code, null, null, null,true));
        CLASS_TO_VALUETYPE.add(Double.class,    new BinaryFieldValueType(Mapping.FLOAT64LE.code, null, null, null,true));
        CLASS_TO_VALUETYPE.add(Chars.class,     new BinaryFieldValueType(Mapping.CHARS.code, CharEncodings.UTF_8, null, null,true));
    }


    private final Mapping mapping;
    private final int nbBit;
    private final CharEncoding charEncoding;
    private final DocumentType docType;
    private final boolean inline;
    private final long[] arraySize;
    private final Class javaClass;

    public static BinaryFieldValueType forValueClass(Class valueClass) {
        BinaryFieldValueType bft = (BinaryFieldValueType) CLASS_TO_VALUETYPE.getValue(valueClass);
        if (bft==null) throw new InvalidArgumentException("Unsupported class "+valueClass);
        return bft;
    }

    public static BinaryFieldValueType forValue(Object value) {
        if (value==null) {
            throw new InvalidArgumentException("Unsupported value "+value);
        }
        final Class<? extends Object> valueClass = value.getClass();
        BinaryFieldValueType bft = (BinaryFieldValueType) CLASS_TO_VALUETYPE.getValue(value.getClass());
        if (bft!=null) {
            return bft;
        } else if (value instanceof Document) {
            return new BinaryFieldValueType(Mapping.DOCUMENT.code, null,  ((Document) value).getType(), null,false);
        } else if (valueClass.isArray()) {
            throw new UnimplementedException("TODO");
        } else {
            throw new InvalidArgumentException("Unsupported value "+value);
        }
    }

    public static BinaryFieldValueType bits(int nbBits) {
        return bits(nbBits, null);
    }

    public static BinaryFieldValueType bits(int nbBits, long[] arraySize) {
        return new BinaryFieldValueType(Mapping.VARBITS.code | nbBits,null,null,arraySize,false);
    }

    public static BinaryFieldValueType numeric(Mapping primitiveType) {
        return numeric(primitiveType, null);
    }

    public static BinaryFieldValueType numeric(Mapping primitiveType, long[] arraySize) {
        return new BinaryFieldValueType(primitiveType.code, null, null, arraySize, true);
    }

    public static BinaryFieldValueType chars(CharEncoding charEncoding) {
        return new BinaryFieldValueType(Mapping.CHARS.code, charEncoding, null, null, true);
    }

    public static BinaryFieldValueType chars(CharEncoding charEncoding, long[] arraySize) {
        return new BinaryFieldValueType(Mapping.CHARS.code, charEncoding, null, arraySize, true);
    }

    public static BinaryFieldValueType doc(DocumentType docType, boolean inline) {
        return new BinaryFieldValueType(Mapping.DOCUMENT.code, null, docType, null, inline);
    }

    public static BinaryFieldValueType doc(DocumentType docType, long[] arraySize, boolean inline) {
        return new BinaryFieldValueType(Mapping.DOCUMENT.code, null, docType, arraySize, inline);
    }

    public BinaryFieldValueType(int primitiveType, CharEncoding charEncoding, DocumentType docType, long[] arraySize, boolean inline) {
        this(
            (primitiveType & Mapping.VARBITS.code) != 0 ? Mapping.VARBITS : Mapping.forCode(primitiveType),
            (primitiveType & Mapping.VARBITS.code) != 0 ? primitiveType&0x7F : 0,
            charEncoding, docType, arraySize, inline
        );
    }

    private BinaryFieldValueType(Mapping mapping, int nbBits, CharEncoding charEncoding, DocumentType docType, long[] arraySize, boolean inline) {
        this.mapping = mapping;
        this.nbBit = nbBits;
        this.charEncoding = charEncoding;
        this.docType = docType;
        this.arraySize = arraySize;
        this.inline = inline;

        if (mapping==Mapping.DOCUMENT && charEncoding!=null) {
            throw new InvalidArgumentException("Character and number encoding must be null for document types");
        } else if (mapping==Mapping.CHARS && charEncoding==null) {
            throw new InvalidArgumentException("Character encoding must be set for text type, and number encoding must be null");
        } else if ((mapping.code>1 && mapping.code<13) && charEncoding!=null) {
            throw new InvalidArgumentException("Character encoding must be null for numeric type");
        } else if (mapping.code==1 && charEncoding!=null) {
            throw new InvalidArgumentException("Character encoding must be null for boolean type");
        }

        if (arraySize!=null) {
            javaClass = Array.newInstance(mapping.primitiveClass, new int[arraySize.length]).getClass();
        } else {
            javaClass = mapping.valueClass;
        }
    }

    public Mapping getMapping() {
        return mapping;
    }

    public int getPrimitiveType() {
        int ptype = mapping.code;
        if (mapping==Mapping.VARBITS) {
            ptype |= nbBit;
        }
        return ptype;
    }

    /**
     * Number of bits used by variable size bit primitive type.
     * @return
     */
    public int getNbBit() {
        return nbBit;
    }

    public Class getValueClass() {
        return javaClass;
    }

    /**
     * If the field of of type Chars, indicate the encoding.
     * If the encoding is null, each value may have a different encoding.
     *
     * @return Char encoding, may be null
     */
    public CharEncoding getCharEncoding() {
        return charEncoding;
    }

    /**
     * If the field is of numeric type, indicate the encoding.
     *
     * @return number encoding, never null if numeric type
     */
    public Endianness getNumberEncoding() {
        return mapping.encoding;
    }

    /**
     * If the field is of document type, indicate the contained document type.
     *
     * @return sub document type.
     */
    public DocumentType getDocumentType() {
        return docType;
    }

    /**
     * If the field is of document type, indicate the contained document is inline.
     * Inline documents are not preceded by the stream mode, in this case it is always 0x00 (streamed).
     * This is convenient for small types which use only a few bits/bytes.
     *
     * @return true if document are stored inline
     */
    public boolean isInline() {
        return inline;
    }

    /**
     * If the field is an array this property is not null.
     * Array can be of any size.
     * A value of -1 for one dimension means variable size.
     *
     * @return array size, null if not an array
     */
    public long[] getArraySize() {
        if (arraySize==null) return null;
        return Arrays.copy(arraySize, new long[arraySize.length]);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BinaryFieldValueType other = (BinaryFieldValueType) obj;
        if (this.mapping != other.mapping) {
            return false;
        }
        if (this.nbBit != other.nbBit) {
            return false;
        }
        if (this.charEncoding != other.charEncoding && (this.charEncoding == null || !this.charEncoding.equals(other.charEncoding))) {
            return false;
        }
        if (this.docType != other.docType && (this.docType == null || !this.docType.equals(other.docType))) {
            return false;
        }
        if (!Arrays.equals(this.arraySize, other.arraySize)) {
            return false;
        }
        return true;
    }

    public int getHash() {
        int hash = 31;
        hash = 59 * hash + this.mapping.hashCode();
        hash = 59 * hash + (this.charEncoding != null ? this.charEncoding.hashCode() : 0);
        hash = 59 * hash + Arrays.computeHash(this.arraySize);
        return hash;
    }


}
