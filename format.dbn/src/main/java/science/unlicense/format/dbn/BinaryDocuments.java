
package science.unlicense.format.dbn;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.format.dbn.BinaryFieldValueType.Mapping;
import static science.unlicense.format.dbn.BinaryFieldValueType.chars;
import static science.unlicense.format.dbn.BinaryFieldValueType.doc;
import static science.unlicense.format.dbn.BinaryFieldValueType.numeric;

/**
 *
 * @author Johann Sorel
 */
public final class BinaryDocuments {

    private static final Chars ATT_ID = Chars.constant("id");
    private static final Chars ATT_TITLE = Chars.constant("title");
    private static final Chars ATT_DESCRIPTION = Chars.constant("description");
    private static final Chars ATT_MINOCC = Chars.constant("minocc");
    private static final Chars ATT_MAXOCC = Chars.constant("maxocc");
    private static final Chars ATT_NBOCC = Chars.constant("nbocc");
    private static final Chars ATT_SIZE = Chars.constant("size");
    private static final Chars ATT_PTYPE = Chars.constant("ptype");
    private static final Chars ATT_DOCTYPE = Chars.constant("doctype");
    private static final Chars ATT_INLINE = Chars.constant("inline");
    private static final Chars ATT_CHARENC = Chars.constant("charenc");
    private static final Chars ATT_PARENTS = Chars.constant("parents");
    private static final Chars ATT_FIELDS = Chars.constant("fields");
    private static final Chars ATT_ATTRIBUTES = Chars.constant("attributes");
    private static final Chars ATT_VALUETYPE = Chars.constant("valuetype");
    private static final Chars ATT_VALUEDEFAULT = Chars.constant("default");
    private static final Chars ATT_ATTVALUE = Chars.constant("value");
    private static final Chars ATT_DOCS = Chars.constant("docs");

    public static final DocumentType FIELDTYPE;
    public static final DocumentType FIELDVALUETYPE;
    public static final DocumentType ATTRIBUTETYPE;
    public static final DocumentType REGISTERTYPE;
    public static final DocumentType DOCUMENTTYPE;
    static {
        BinaryDocumentTypeBuilder dtb;

        final BinaryFieldTypeBuilder fieldId = new BinaryFieldTypeBuilder().id(ATT_ID).type(chars(CharEncodings.UTF_8));

        dtb = new BinaryDocumentTypeBuilder();
        dtb.id(new Chars("FieldValueType"));
        dtb.addField(ATT_PTYPE).type(numeric(Mapping.INT8));
        dtb.addField(ATT_SIZE).type(numeric(Mapping.ULEB128,new long[]{0})).minOcc(0);
        dtb.addField(ATT_DOCTYPE).type(numeric(Mapping.INT32LE)).minOcc(0);
        dtb.addField(ATT_INLINE).type(numeric(Mapping.BOOL_BYTE)).minOcc(0);
        dtb.addField(ATT_CHARENC).type(chars(CharEncodings.UTF_8)).minOcc(0);
        FIELDVALUETYPE = dtb.build();

        dtb = new BinaryDocumentTypeBuilder();
        dtb.id(new Chars("AttributeType"));
        dtb.addField(fieldId);
        dtb.addField(ATT_NBOCC).type(numeric(Mapping.ULEB128));
        dtb.addField(ATT_VALUETYPE).type(doc(FIELDVALUETYPE,true));
        dtb.addField(ATT_ATTVALUE).type(numeric(Mapping.INT8,new long[]{0})).minOcc(0);
        ATTRIBUTETYPE = dtb.build();

        dtb = new BinaryDocumentTypeBuilder();
        dtb.id(new Chars("FieldType"));
        dtb.addField(fieldId);
        dtb.addField(ATT_MINOCC).type(numeric(Mapping.INT32LE));
        dtb.addField(ATT_MAXOCC).type(numeric(Mapping.INT32LE));
        dtb.addField(ATT_ATTRIBUTES).type(doc(ATTRIBUTETYPE,false)).minOcc(0).maxOcc(Integer.MAX_VALUE);
        dtb.addField(ATT_VALUETYPE).type(doc(FIELDVALUETYPE,true));
        dtb.addField(ATT_VALUEDEFAULT).type(numeric(Mapping.INT8,new long[]{0})).minOcc(0);
        FIELDTYPE = dtb.build();

        dtb = new BinaryDocumentTypeBuilder();
        dtb.id(new Chars("DocumentType"));
        dtb.addField(fieldId);
        dtb.addField(ATT_PARENTS).type(numeric(Mapping.INT32LE)).minOcc(0).maxOcc(Integer.MAX_VALUE);
        dtb.addField(ATT_FIELDS).type(doc(FIELDTYPE,false)).minOcc(0).maxOcc(Integer.MAX_VALUE);
        dtb.addField(ATT_ATTRIBUTES).type(doc(ATTRIBUTETYPE,false)).minOcc(0).maxOcc(Integer.MAX_VALUE);
        DOCUMENTTYPE = dtb.build();

        dtb = new BinaryDocumentTypeBuilder();
        dtb.id(new Chars("RegisterType"));
        dtb.addField(ATT_DOCS).type(doc(DOCUMENTTYPE,false)).minOcc(0).maxOcc(Integer.MAX_VALUE);
        REGISTERTYPE = dtb.build();

    }

    private BinaryDocuments() {}

    /**
     * Rebuild document type from description in given document.
     *
     * @param doc document of type register
     * @return DocumentType rebuilded document type
     */
    public static DocumentType toDocumentType(Document doc) {

        final Sequence subdocs = (Sequence) doc.getPropertyValue(ATT_DOCS);

        final Sequence types = new ArraySequence();
        for (int i=0,n=subdocs.getSize();i<n;i++) {
            final Document sdoc = (Document) subdocs.get(i);
            final Chars id = (Chars) sdoc.getPropertyValue(ATT_ID);
            final Sequence supers = (Sequence) sdoc.getPropertyValue(ATT_PARENTS);
            final Sequence fields = (Sequence) sdoc.getPropertyValue(ATT_FIELDS);
            final BinaryFieldType[] fieldTypes = new BinaryFieldType[fields.getSize()];
            final DocumentType[] parents = new DocumentType[supers.getSize()];

            Chars title = null;
            Chars desc = null;
            final Dictionary attMap = new HashDictionary();
            final Sequence attributes = (Sequence) sdoc.getPropertyValue(ATT_ATTRIBUTES);
            if (!attributes.isEmpty()) {
                try {
                    for (int k=0,kn=attributes.getSize();k<kn;k++) {
                        final Pair att = toAttribute((Document) attributes.get(k));
                        if (ATT_TITLE.equals(att.getValue1())) {
                            title = (Chars) att.getValue2();
                        } else if (ATT_DESCRIPTION.equals(att.getValue1())) {
                            desc = (Chars) att.getValue2();
                        } else {
                            attMap.add(att.getValue1(), att.getValue2());
                        }
                    }
                } catch(IOException ex) {
                    throw new RuntimeException(ex.getMessage(),ex);
                }
            }

            final DocumentType type = new BinaryDocumentType(id, title, desc, true, fieldTypes, attMap, parents);
            types.add(type);
        }

        //second pass to build attributes
        for (int i=0,n=subdocs.getSize();i<n;i++) {
            final Document sdoc = (Document) subdocs.get(i);
            final Sequence fields = (Sequence) sdoc.getPropertyValue(ATT_FIELDS);
            final FieldType[] fieldTypes = ((BinaryDocumentType) types.get(i)).getFieldsInternal();
            for (int k=0;k<fieldTypes.length;k++) {
                fieldTypes[k] = toFieldType((Document) fields.get(k),types);
            }
        }

        //third pass to build heritage
        for (int i=0,n=subdocs.getSize();i<n;i++) {
            final Document sdoc = (Document) subdocs.get(i);
            final Sequence supers = (Sequence) sdoc.getPropertyValue(ATT_PARENTS);
            final BinaryDocumentType type = (BinaryDocumentType) types.get(i);
            for (int k=0,kn=supers.getSize(); k<kn; k++) {
                type.getSuperInternal()[k] = (DocumentType) types.get((Integer) supers.get(k));
            }
            //rebuild index
            type.reindexInternal();
        }


        return (DocumentType) types.get(0);
    }

    private static Pair toAttribute(Document doc) throws IOException{

        final Chars id = (Chars) doc.getPropertyValue(ATT_ID);
        final Long nbOcc = (Long) doc.getPropertyValue(ATT_NBOCC);
        final BinaryFieldValueType valueType = toFieldValueType((Document) doc.getPropertyValue(ATT_VALUETYPE));
        final byte[] array = (byte[]) doc.getPropertyValue(ATT_ATTVALUE);
        final DataInputStream ds = new DataInputStream(new ArrayInputStream(array));
        final Object value;
        if (nbOcc==1) {
            value = DocumentReader.readFieldValue(ds, valueType);
        } else {
            final Sequence seq = new ArraySequence(nbOcc.intValue());
            for (int i=0;i<nbOcc;i++) {
                seq.add(DocumentReader.readFieldValue(ds, valueType));
            }
            value = seq;
        }
        return new Pair(id, value);
    }

    private static BinaryFieldType toFieldType(Document doc, Sequence stack) {

        final Chars id = (Chars) doc.getPropertyValue(ATT_ID);
        final int minOcc = (Integer) doc.getPropertyValue(ATT_MINOCC);
        final int maxOcc = (Integer) doc.getPropertyValue(ATT_MAXOCC);
        final Document valueTypeDoc = (Document) doc.getPropertyValue(ATT_VALUETYPE);

        Chars title = null;
        Chars desc = null;
        final Dictionary attMap = new HashDictionary();
        final Sequence attributes = (Sequence) doc.getPropertyValue(ATT_ATTRIBUTES);
        if (attributes!=null && !attributes.isEmpty()) {
            try {
                for (int k=0,kn=attributes.getSize();k<kn;k++) {
                    final Pair att = toAttribute((Document) attributes.get(k));
                    if (ATT_TITLE.equals(att.getValue1())) {
                        title = (Chars) att.getValue2();
                    } else if (ATT_DESCRIPTION.equals(att.getValue1())) {
                        desc = (Chars) att.getValue2();
                    } else {
                        attMap.add(att.getValue1(), att.getValue2());
                    }
                }
            } catch(IOException ex) {
                throw new RuntimeException(ex.getMessage(),ex);
            }
        }

        final BinaryFieldValueType valueType = toFieldValueType(valueTypeDoc, stack);
        return new BinaryFieldType(id,title,desc, minOcc, maxOcc, null, valueType, attMap);
    }

    public static BinaryFieldValueType toFieldValueType(Document doc) {
        return toFieldValueType(doc, null);
    }

    private static BinaryFieldValueType toFieldValueType(Document doc, Sequence stack) {

        final int ptype = ((Byte) doc.getPropertyValue(ATT_PTYPE)) & 0xFF;
        final long[] size = (long[]) doc.getPropertyValue(ATT_SIZE);
        final Integer reftype = (Integer) doc.getPropertyValue(ATT_DOCTYPE);
        final Boolean inline = (Boolean) doc.getPropertyValue(ATT_INLINE);
        final Chars charenc = (Chars) doc.getPropertyValue(ATT_CHARENC);

        if (reftype!=null) {
            final DocumentType docType = (DocumentType) stack.get(reftype);
            return new BinaryFieldValueType(ptype, null,  docType, size,inline);
        } else if (ptype == Mapping.CHARS.code) {
            return new BinaryFieldValueType(ptype, CharEncodings.find(charenc),  null, size,true);
        } else if (ptype == Mapping.BOOL_BYTE.code) {
            return new BinaryFieldValueType(ptype, null, null, size,true);
        } else {
            return new BinaryFieldValueType(ptype, null, null, size,true);
        }
    }

    /**
     * Convert document type to a descriptive document.
     *
     * @param docType document type to convert
     * @return Descriptive document
     */
    public static Document toDocument(DocumentType docType) {
        final Document doc = new DefaultDocument(true, REGISTERTYPE);
        final Sequence allTypes = new ArraySequence();
        final Sequence alldocs = new ArraySequence();
        toDocument(docType,allTypes,alldocs);
        doc.setPropertyValue(ATT_DOCS,alldocs);
        return doc;
    }

    private static Document toDocument(DocumentType docType, Sequence alltypes, Sequence alldocs) {

        final Document doc = new DefaultDocument(true, DOCUMENTTYPE);
        doc.setPropertyValue(ATT_ID, docType.getId());

        alltypes.add(docType);
        alldocs.add(doc);

        //convert parent types
        final DocumentType[] parents = docType.getSuper();
        final Sequence supers = new ArraySequence();
        for (int i=0; i<parents.length; i++) {
            final DocumentType refType = parents[i];
            int idx = alltypes.search(refType);
            if (idx<0) {
               toDocument(refType,alltypes,alldocs);
               idx = alltypes.search(refType);
            }
            supers.add(idx);
        }
        doc.setPropertyValue(ATT_PARENTS, supers);

        //convert fields
        final Sequence fields = new ArraySequence();
        for (FieldType fieldType : docType.getLocalFields()) {
            fields.add(toDocument(fieldType,alltypes,alldocs));
        }
        doc.setPropertyValue(ATT_FIELDS, fields);

        //convert attributes
        final Sequence attributes = new ArraySequence();
        try {
            if (docType.getTitle()!=null) attributes.add(attributeToDocument(ATT_TITLE, docType.getTitle()));
            if (docType.getDescription()!=null) attributes.add(attributeToDocument(ATT_DESCRIPTION, docType.getDescription()));
            final Dictionary atts = docType.getAttributes();
            final Iterator ite = atts.getPairs().createIterator();
            while (ite.hasNext()) {
                final Pair pair = (Pair) ite.next();
                attributes.add(attributeToDocument((Chars) pair.getValue1(), pair.getValue2()));
            }
        } catch(IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
        doc.setPropertyValue(ATT_ATTRIBUTES, attributes);


        return doc;
    }

    private static Document toDocument(FieldType fieldType, Sequence alltypes, Sequence alldocs) {
        final BinaryFieldType bft = (BinaryFieldType) fieldType;
        final Document doc = new DefaultDocument(true, FIELDTYPE);
        doc.setPropertyValue(ATT_ID, bft.getId());
        doc.setPropertyValue(ATT_MINOCC, bft.getMinOccurences());
        doc.setPropertyValue(ATT_MAXOCC, bft.getMaxOccurences());
        doc.setPropertyValue(ATT_VALUETYPE, toDocument(bft.getValueType(), alltypes, alldocs));

        final Object def = bft.getDefaultValue();
        if (def!=null) {
            final ArrayOutputStream out = new ArrayOutputStream();
            try {
                DocumentWriter.writeSingleValue(new DataOutputStream(out), bft.getValueType(), def);
                doc.setPropertyValue(ATT_VALUEDEFAULT, out.getBuffer().toArrayByte());
            } catch (IOException ex) {
                //won't happen
                throw new RuntimeException("Should not happen");
            }
        }


        //convert attributes
        final Sequence attributes = new ArraySequence();
        try {
            if (fieldType.getTitle()!=null) attributes.add(attributeToDocument(ATT_TITLE, fieldType.getTitle()));
            if (fieldType.getDescription()!=null) attributes.add(attributeToDocument(ATT_DESCRIPTION, fieldType.getDescription()));
            final Dictionary atts = fieldType.getAttributes();
            final Iterator ite = atts.getPairs().createIterator();
            while (ite.hasNext()) {
                final Pair pair = (Pair) ite.next();
                attributes.add(attributeToDocument((Chars) pair.getValue1(), pair.getValue2()));
            }
        } catch(IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
        doc.setPropertyValue(ATT_ATTRIBUTES, attributes);

        return doc;
    }

    private static Document attributeToDocument(Chars key, Object value) throws IOException {
        final Document doc = new DefaultDocument(false, ATTRIBUTETYPE);


        final BinaryFieldValueType valueType;
        final long nbOcc;
        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ds = new DataOutputStream(out);
        if (value instanceof Sequence) {
            final Sequence seq = (Sequence) value;
            nbOcc = seq.getSize();
            valueType = BinaryFieldValueType.forValue(seq.get(0));
            for (int i=0,n=seq.getSize();i<n;i++) {
                DocumentWriter.writeFieldValue(ds, valueType, seq.get(i));
            }
        } else {
            nbOcc = 1;
            valueType = BinaryFieldValueType.forValue(value);
            DocumentWriter.writeFieldValue(ds, valueType, value);
        }
        doc.setPropertyValue(ATT_ID, key);
        doc.setPropertyValue(ATT_NBOCC, nbOcc);
        doc.setPropertyValue(ATT_VALUETYPE, toDocument(valueType,null,null));
        doc.setPropertyValue(ATT_ATTVALUE, out.getBuffer().toArrayByte());

        return doc;
    }

    private static Document toDocument(BinaryFieldValueType bft, Sequence alltypes, Sequence alldocs) {

        final Document doc = new DefaultDocument(true, FIELDVALUETYPE);
        doc.setPropertyValue(ATT_SIZE, bft.getArraySize());
        doc.setPropertyValue(ATT_PTYPE, (byte) bft.getPrimitiveType());
        if (bft.getCharEncoding()!=null) {
            doc.setPropertyValue(ATT_CHARENC, bft.getCharEncoding().getName());
        }

        final DocumentType refType = bft.getDocumentType();
        if (refType != null) {
            doc.setPropertyValue(ATT_INLINE, bft.isInline());
            int idx = alltypes.search(refType);
            if (idx<0) {
               toDocument(refType,alltypes,alldocs);
               idx = alltypes.search(refType);
            }
            doc.setPropertyValue(ATT_DOCTYPE, idx);
        }

        return doc;
    }



}
