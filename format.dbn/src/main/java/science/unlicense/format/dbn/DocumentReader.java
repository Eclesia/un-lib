
package science.unlicense.format.dbn;

import java.lang.reflect.Array;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Read a document from binary encoding.
 *
 * @author Johann Sorel
 */
public class DocumentReader extends AbstractReader {

    private final DocumentType docType;

    public DocumentReader(DocumentType docType) {
        this.docType = docType;
    }

    /**
     * Document type read by this reader.
     *
     * @return
     */
    public DocumentType getDocumentType() {
        return docType;
    }

    /**
     * Read document.
     *
     * @return Document
     */
    public Document read() throws IOException {
        final DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);
        return readDoc(ds, docType,false);
    }

    public static Document readDoc(DataInputStream ds, DocumentType docType, boolean inline) throws IOException {

        if (inline) {
            return readDocStreamed(docType, 0, ds);
        }

        for (;;) { //skip possible deleted documents
            final int mode = ds.read();
            if (mode==-1) {
                //end of stream
                return null;
            }
            if (mode == 0x00 || mode == 0x01) {
                //streamed mode
                return readDocStreamed(docType, mode, ds);
            } else if (mode == 0x02) {
                //encapsulated mode
                final Chars method = new Chars(ds.readBytes((int) ds.readVarLengthUInt()),CharEncodings.UTF_8);
                final int compressedSize = (int) ds.readVarLengthUInt();
                final byte[] bdoc;
                if (compressedSize!=0) {
                    //fixed size
                    bdoc = ds.readBytes(compressedSize);
                } else {
                    //by blocks
                    final int blockSize = (int) ds.readVarLengthUInt();
                    final ByteSequence buffer = new ByteSequence();
                    for (;;) {
                        buffer.put(ds.readBytes(blockSize));
                        if (ds.readByte()==0) break;
                    }
                    bdoc = buffer.toArrayByte();
                }

                throw new IOException(ds, "No supported yet.");
            } else if (mode == 0x03) {
                //reference mode
                final Chars ref = new Chars(ds.readBytes((int) ds.readVarLengthUInt()),CharEncodings.UTF_8);
                throw new IOException(ds, "No supported yet.");
            } else if (mode == 0xFF) {
                //deleted mode
                ds.skipFully(ds.readVarLengthUInt());
                continue;
            }
        }
    }

    private static Document readDocStreamed(DocumentType docType, int mode, DataInputStream ds) throws IOException {

        final Document doc = new DefaultDocument(false,docType);
        FieldType[] fields = docType.getFields();
        //index
        if (mode == 0x01) {
            //one bit for index map if defined
            final boolean hasIndex = ds.readBits(1) != 0;
            //read mask, one bit set for each field which may be null
            final Sequence filterFields = new ArraySequence();
            for (int i=0;i<fields.length;i++) {
                if (fields[i].getMinOccurences()==0) {
                    //skip field if not present
                    if (ds.readBits(1)==0) continue;
                }
                filterFields.add(fields[i]);
            }
            fields = (FieldType[]) filterFields.toArray(FieldType.class);

            ds.skipToByteEnd();
            //read index
            if (hasIndex) {
                final int size = ds.readUByte();
                final int[] index = ds.readBits(new int[fields.length], size*8);
            }
        }

        //streamed mode
        for (int i=0;i<fields.length;i++) {
            final BinaryFieldType field = (BinaryFieldType) fields[i];
            BinaryFieldValueType valueType = field.getValueType();
            final Chars id = field.getId();

            //read number of occurences
            final int maxOcc = field.getMaxOccurences();
            final int nbOcc = readNbOccurence(ds, field);

            //read value document type if not defined
            if (valueType.getMapping()==BinaryFieldValueType.Mapping.DOCUMENT && valueType.getDocumentType()==null) {
                final DocumentType valueDocType = BinaryDocuments.toDocumentType(readDoc(ds, BinaryDocuments.REGISTERTYPE,false));
                valueType = BinaryFieldValueType.doc(valueDocType, valueType.getArraySize(), false);
            }

            //read values
            if (nbOcc==-1) {
                //progressive mode
                final Sequence values = new ArraySequence();
                while (ds.read()!=0) {
                    values.add(readFieldValue(ds, valueType));
                }
                doc.setPropertyValue(id, values);

            } else if (nbOcc!=0) {
                if (maxOcc==1) {
                    doc.setPropertyValue(id, readFieldValue(ds, valueType));
                } else {
                    final Sequence values = new ArraySequence(nbOcc);
                    for (int k=0;k<nbOcc;k++) {
                        values.add(readFieldValue(ds, valueType));
                    }
                    doc.setPropertyValue(id, values);
                }
            }
        }
        return doc;
    }

    public static void skipDoc(DataInputStream ds, DocumentType docType, boolean inline) throws IOException {
        if (inline) {
            skipDocStreamed(ds, docType, 0);
        }

        final int mode = ds.read();

        if (mode == 0x00 || mode == 0x01) {
            skipDocStreamed(ds, docType, mode);

        } else if (mode == 0x02) {
            //encapsulated mode
            ds.skipFully(ds.readUByte());
            final int compressedSize = (int) ds.readVarLengthUInt();
            if (compressedSize!=0) {
                //fixed size
                ds.skipFully(compressedSize);
            } else {
                //by blocks
                final int blockSize = (int) ds.readVarLengthUInt();
                for (;;) {
                    ds.skipFully(blockSize);
                    if (ds.readByte()==0) break;
                }
            }
        } else if (mode == 0x03) {
            //reference mode
            ds.skipFully(ds.readVarLengthUInt());
            throw new IOException(ds, "No supported yet.");
        } else if (mode == 0xFF) {
            //deleted mode
            ds.skipFully(ds.readVarLengthUInt());
        }
    }

    private static void skipDocStreamed(DataInputStream ds, DocumentType docType, int mode) throws IOException {
        final FieldType[] fields = docType.getFields();
        //index
        if (mode == 0x01) {
            //read index
            final int size = ds.readUByte();
            final int[] index = ds.readBits(new int[fields.length], size*8);
        }

        for (int i=0;i<fields.length;i++) {
            final BinaryFieldType field = (BinaryFieldType) fields[i];
            BinaryFieldValueType valueType = field.getValueType();

            //read number of occurences
            final int maxOcc = field.getMaxOccurences();
            final int nbOcc = readNbOccurence(ds, field);

            //read value document type if not defined
            if (valueType.getMapping()==BinaryFieldValueType.Mapping.DOCUMENT && valueType.getDocumentType()==null) {
                final DocumentType valueDocType = BinaryDocuments.toDocumentType(readDoc(ds, BinaryDocuments.REGISTERTYPE,false));
                valueType = BinaryFieldValueType.doc(valueDocType, valueType.getArraySize(), false);
            }

            //skip values
            if (nbOcc==-1) {
                //progressive mode
                while (ds.read()!=0) {
                    skipFieldValue(ds, valueType);
                }
            } else if (nbOcc!=0) {
                if (maxOcc==1) {
                    skipFieldValue(ds, valueType);
                } else {
                    for (int k=0;k<nbOcc;k++) {
                        skipFieldValue(ds, valueType);
                    }
                }
            }
        }
    }

    public static Object readFieldValue(DataInputStream ds, BinaryFieldValueType valueType) throws IOException {

        //read array size
        final long[] arraySize = valueType.getArraySize();
        if (arraySize!=null) {
            for (int d=0;d<arraySize.length;d++) {
                if (arraySize[d]<=0) arraySize[d] = (int) ds.readVarLengthUInt();
            }
        }

        //read values
        if (arraySize!=null) {
            return readArrayValue(ds, valueType, arraySize, 0);
        } else {
            return readSingleValue(ds, valueType);
        }

    }

    private static Object readArrayValue(DataInputStream ds, BinaryFieldValueType valueType, long[] array, int depth) throws IOException{

        final Object[] values = new Object[(int) array[depth]];

        for (int i=0;i<values.length;i++) {
            if (depth==array.length-1) {
                Array.set(values, i, readSingleValue(ds, valueType));
            } else {
                Array.set(values, i, readArrayValue(ds, valueType, array, depth+1));
            }
        }
        return values;
    }

    private static Object readSingleValue(DataInputStream ds, BinaryFieldValueType valueType) throws IOException {
        final BinaryFieldValueType.Mapping fieldType = valueType.getMapping();
        final Endianness ne = valueType.getNumberEncoding();
        final CharEncoding ce = valueType.getCharEncoding();
        final DocumentType dt = valueType.getDocumentType();
        final boolean inline = valueType.isInline();

        switch (fieldType) {
            case BOOL_BIT:    return ds.readBits(1)!=0;
            case BOOL_BYTE:   return ds.readByte()!=0;
            case INT8:        return ds.readByte();
            case UINT8:       return ds.readByte();
            case INT16LE:     return ds.readShort(ne);
            case UINT16LE:    return ds.readUShort(ne);
            case INT32LE:     return ds.readInt(ne);
            case UINT32LE:    return ds.readUInt(ne);
            case INT64LE:     return ds.readLong(ne);
            case UINT64LE:    return ds.readLong(ne);
            case FLOAT32LE:   return ds.readFloat(ne);
            case FLOAT64LE:   return ds.readDouble(ne);
            case INT16BE:     return ds.readShort(ne);
            case UINT16BE:    return ds.readUShort(ne);
            case INT32BE:     return ds.readInt(ne);
            case UINT32BE:    return ds.readUInt(ne);
            case INT64BE:     return ds.readLong(ne);
            case UINT64BE:    return ds.readLong(ne);
            case FLOAT32BE:   return ds.readFloat(ne);
            case FLOAT64BE:   return ds.readDouble(ne);
            case LEB128:      return ds.readVarLengthInt();
            case ULEB128:      return ds.readVarLengthUInt();
            case VARBITS:     return ds.readBits(valueType.getNbBit());
            case CHARS:     return new Chars(ds.readFully(new byte[(int) ds.readVarLengthUInt()]),ce);
            case DOCUMENT:       return readDoc(ds, dt, inline);
            case VARYING: {
                final Document typeDoc = readDoc(ds, BinaryDocuments.FIELDVALUETYPE,false);
                return readFieldValue(ds, BinaryDocuments.toFieldValueType(typeDoc));
            }
            default: throw new InvalidArgumentException("Unexpected field type : "+fieldType);
        }
    }

    public static void skipFieldValue(DataInputStream ds, BinaryFieldValueType valueType) throws IOException {

        //read array size
        final long[] arraySize = valueType.getArraySize();
        if (arraySize!=null) {
            for (int d=0;d<arraySize.length;d++) {
                if (arraySize[d]<=0) arraySize[d] = (int) ds.readVarLengthUInt();
            }
        }

        //skip values
        if (arraySize!=null) {
            skipArrayValue(ds, valueType, arraySize, 0);
        } else {
            skipSingleValue(ds, valueType);
        }
    }

    private static void skipArrayValue(DataInputStream ds, BinaryFieldValueType valueType, long[] array, int depth) throws IOException{
        for (int i=0;i<array[depth];i++) {
            if (depth==array.length-1) {
                skipSingleValue(ds, valueType);
            } else {
                skipArrayValue(ds, valueType, array, depth+1);
            }
        }
    }

    private static void skipSingleValue(DataInputStream ds, BinaryFieldValueType valueType) throws IOException {
        final BinaryFieldValueType.Mapping fieldType = valueType.getMapping();
        final DocumentType dt = valueType.getDocumentType();

        switch (fieldType) {
            case BOOL_BIT:    ds.readBits(1);break;
            case BOOL_BYTE:   ds.skipFully(1);break;
            case INT8:        ds.skipFully(1);break;
            case UINT8:       ds.skipFully(1);break;
            case INT16LE:    ds.skipFully(2);break;
            case UINT16LE:   ds.skipFully(2);break;
            case INT32LE:      ds.skipFully(4);break;
            case UINT32LE:     ds.skipFully(4);break;
            case INT64LE:     ds.skipFully(8);break;
            case UINT64LE:    ds.skipFully(8);break;
            case FLOAT32LE:    ds.skipFully(4);break;
            case FLOAT64LE:   ds.skipFully(8);break;
            case INT16BE:    ds.skipFully(2);break;
            case UINT16BE:   ds.skipFully(2);break;
            case INT32BE:      ds.skipFully(4);break;
            case UINT32BE:     ds.skipFully(4);break;
            case INT64BE:     ds.skipFully(8);break;
            case UINT64BE:    ds.skipFully(8);break;
            case FLOAT32BE:    ds.skipFully(4);break;
            case FLOAT64BE:   ds.skipFully(8);break;
            case LEB128:     ds.readVarLengthInt();break;
            case ULEB128:     ds.readVarLengthUInt();break;
            case VARBITS:     ds.readBits(valueType.getNbBit());break;
            case CHARS:        ds.skipFully(ds.readVarLengthUInt()); break;
            case DOCUMENT:         skipDoc(ds, dt, valueType.isInline()); break;
            case VARYING: {
                final Document typeDoc = readDoc(ds, BinaryDocuments.FIELDVALUETYPE,false);
                skipFieldValue(ds, BinaryDocuments.toFieldValueType(typeDoc));
                break;
            }
            default: throw new InvalidArgumentException("Unexpected field type : "+fieldType);
        }
    }

    public static int readNbOccurence(DataInputStream ds, BinaryFieldType field) throws IOException {

        //read number of occurences
        final int minOcc = field.getMinOccurences();
        final int maxOcc = field.getMaxOccurences();
        if (minOcc == 0 && maxOcc == 1) {
            if (ds.readBits(1)==0) {
                return 0;
            } else {
                //realign stream
                ds.skipToByteEnd();
                return 1;
            }
        } else if (maxOcc != minOcc) {
            int num = (int) ds.readVarLengthUInt();
            if (num==0) return -1;
            return minOcc + num -1;
        } else {
            return maxOcc;
        }
    }

}
