
package science.unlicense.format.riff;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.model.Chunk;

/**
 *
 * @author Johann Sorel
 */
public class RIFFWriter extends AbstractWriter {

    private DataOutputStream ds;

    public void write(Chunk chunk) throws IOException{

        if (ds==null) {
            //we expect a RIFF or RIFX chunk
            if (RIFFConstants.CHUNK_RIFF.equals(chunk.getFourCC())) {
                //little endian encoding
                ds = getOutputAsDataStream(Endianness.LITTLE_ENDIAN);
            } else if (RIFFConstants.CHUNK_RIFX.equals(chunk.getFourCC())) {
                //big endian encoding
                ds = getOutputAsDataStream(Endianness.BIG_ENDIAN);
            } else {
                throw new IOException(ds, "Chunk is not a RIFF.");
            }
        }

        //ensure 2bytes padding
        ds.realign(2);

        //write chunk
        ds.write(chunk.getFourCC().toBytes(CharEncodings.US_ASCII));
        long size = chunk.getSize();
        if (size<0) throw new IOException(ds, "Chunk "+chunk.getFourCC()+" size not set.");
        ds.writeUInt(size);
        chunk.write(ds);

    }

}
