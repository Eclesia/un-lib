
package science.unlicense.format.riff.model;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Int64;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.RIFFConstants;

/**
 *
 * @author Johann Sorel
 */
public class LISTChunk extends DefaultChunk {

    // 4 bytes to identify the list  type.
    private Chars type;
    private final Sequence subchunks = new ArraySequence();

    public LISTChunk() {
        super(RIFFConstants.CHUNK_LIST);
    }

    public LISTChunk(Chars fourCC) {
        super(fourCC);
    }

    public Chars getType() {
        return type;
    }

    public void setType(Chars type) {
        this.type = type;
    }

    public Sequence getSubchunks() {
        return subchunks;
    }

    public void computeSize() {
        long size = 0;
        for (int i=0,n=subchunks.getSize();i<n;i++) {
            final Chunk chunk = (Chunk) subchunks.get(i);
            chunk.computeSize();
            size += chunk.getSize() + 8; //8 : chunk fourcc and size
        }
        setSize(size);
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.write(type.toBytes(CharEncodings.US_ASCII));
        for (int i=0,n=subchunks.getSize();i<n;i++) {

            //ensure 2bytes padding
            ds.realign(2);

            final Chunk chunk = (Chunk) subchunks.get(i);
            ds.write(chunk.getFourCC().toBytes(CharEncodings.US_ASCII));
            long size = chunk.getSize();
            if (size<0) throw new IOException(ds, "Chunk "+chunk.getFourCC()+" size not set.");
            ds.writeUInt(size);
            chunk.write(ds);
        }
    }

    public Chars toChars() {
        final CharBuffer buffer = new CharBuffer();
        buffer.append(fourCC);
        buffer.append(' ');
        buffer.append(Int64.encode(size));
        buffer.append(' ');
        buffer.append(type);
        return buffer.toChars();
    }

}
