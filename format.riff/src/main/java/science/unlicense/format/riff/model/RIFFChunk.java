
package science.unlicense.format.riff.model;

import science.unlicense.format.riff.RIFFConstants;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class RIFFChunk extends LISTChunk {


    public RIFFChunk() {
        super(RIFFConstants.CHUNK_RIFF);
    }

    public RIFFChunk(Endianness encoding) {
        super(encoding == Endianness.LITTLE_ENDIAN ? RIFFConstants.CHUNK_RIFF : RIFFConstants.CHUNK_RIFX);
    }

    public Endianness getEndianness() {
        return RIFFConstants.CHUNK_RIFF.equals(fourCC) ? Endianness.LITTLE_ENDIAN : Endianness.BIG_ENDIAN;
    }

}
