

package science.unlicense.format.riff;

import science.unlicense.common.api.character.Chars;

/**
 * RIFF Constants
 * @author Johann Sorel
 */
public final class RIFFConstants {

    /** FileTypeBlocID - Little Endian file */
    public static final Chars CHUNK_RIFF = Chars.constant(new byte[]{'R','I','F','F'});
    /** FileTypeBlocID - Big Endian file */
    public static final Chars CHUNK_RIFX = Chars.constant(new byte[]{'R','I','F','X'});
    public static final Chars CHUNK_LIST = Chars.constant(new byte[]{'L','I','S','T'});

    private RIFFConstants(){}

}
