
package science.unlicense.format.collada;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class ColladaConstants {

    public static final Chars NS_COLLADA_1_4 = Chars.constant("http://www.collada.org/2005/11/COLLADASchema");
    public static final Chars NS_COLLADA_1_5 = Chars.constant("http://www.collada.org/2008/03/COLLADASchema");

    private ColladaConstants(){}

}
