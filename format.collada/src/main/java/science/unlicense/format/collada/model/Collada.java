
package science.unlicense.format.collada.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.NamespaceContext;
import science.unlicense.format.xml.dom.DomUtilities;
import science.unlicense.common.api.character.Chars;
import static science.unlicense.format.collada.ColladaConstants.NS_COLLADA_1_4;

/**
 *
 * @author Johann Sorel
 */
public class Collada extends DaeNode {

    public static final FName NAME = new FName(NS_COLLADA_1_4, Chars.constant("COLLADA"));

    public Collada(FName name, NamespaceContext nsc) {
        super(name, nsc);
    }

    public LibraryCameras getLibraryCameras() {
        return (LibraryCameras) DomUtilities.getNodeForName(this, LibraryCameras.NAME);
    }

    public LibraryEffects getLibraryEffects() {
        return (LibraryEffects) DomUtilities.getNodeForName(this, LibraryEffects.NAME);
    }

    public LibraryGeometries getLibraryGeometries() {
        return (LibraryGeometries) DomUtilities.getNodeForName(this, LibraryGeometries.NAME);
    }

    public LibraryImages getLibraryImages() {
        return (LibraryImages) DomUtilities.getNodeForName(this, LibraryImages.NAME);
    }

    public LibraryLights getLibraryLights() {
        return (LibraryLights) DomUtilities.getNodeForName(this, LibraryLights.NAME);
    }

    public LibraryMaterials getLibraryMaterials() {
        return (LibraryMaterials) DomUtilities.getNodeForName(this, LibraryMaterials.NAME);
    }

    public LibraryVisualScenes getLibraryVisualScenes() {
        return (LibraryVisualScenes) DomUtilities.getNodeForName(this, LibraryVisualScenes.NAME);
    }

    public Scene getScene() {
        return (Scene) DomUtilities.getNodeForName(this, Scene.NAME);
    }
}
