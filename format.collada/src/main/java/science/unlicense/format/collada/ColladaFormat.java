
package science.unlicense.format.collada;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class ColladaFormat extends AbstractModel3DFormat {

    public static final ColladaFormat INSTANCE = new ColladaFormat();

    private ColladaFormat() {
        super(new Chars("Collada"));
        shortName = new Chars("DAE");
        longName = new Chars("DAE Collada");
        extensions.add(new Chars("dae"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new ColladaStore(input);
    }

}
