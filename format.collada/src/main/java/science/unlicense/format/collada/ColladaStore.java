
package science.unlicense.format.collada;

import science.unlicense.format.xml.dom.DomNode;
import science.unlicense.format.xml.dom.DomReader;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.model3d.api.AbstractModel3DStore;

/**
 *
 * @author Johann Sorel
 */
public class ColladaStore extends AbstractModel3DStore {

    ColladaStore(Object input) {
        super(ColladaFormat.INSTANCE, input);
    }

    @Override
    public Collection getElements() throws StoreException {

        try {
            //read the xml
            final DomReader reader = new DomReader();
            reader.setInput(getInput());
            final DomNode root = reader.read();
            System.out.println(Nodes.toCharsTree(root, 50));
            reader.dispose();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        throw new UnsupportedOperationException("Not supported yet.");
    }

}
