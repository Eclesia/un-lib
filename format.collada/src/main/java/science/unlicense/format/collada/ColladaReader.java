package science.unlicense.format.collada;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.NamespaceContext;
import science.unlicense.format.xml.XMLAttributes;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.xml.dom.DomNode;
import science.unlicense.format.xml.dom.DomReader;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.format.collada.model.Collada;
import science.unlicense.format.collada.model.LibraryCameras;
import science.unlicense.format.collada.model.LibraryEffects;
import science.unlicense.format.collada.model.LibraryGeometries;
import science.unlicense.format.collada.model.LibraryImages;
import science.unlicense.format.collada.model.LibraryLights;
import science.unlicense.format.collada.model.LibraryMaterials;
import science.unlicense.format.collada.model.LibraryVisualScenes;
import science.unlicense.format.collada.model.Scene;

/**
 * @author Johann Sorel
 */
public class ColladaReader extends DomReader {

    private static final Dictionary NAME_TO_CLASS = new HashDictionary();
    static {
        NAME_TO_CLASS.add(Collada.NAME, Collada.class);
        NAME_TO_CLASS.add(LibraryCameras.NAME, LibraryCameras.class);
        NAME_TO_CLASS.add(LibraryEffects.NAME, LibraryEffects.class);
        NAME_TO_CLASS.add(LibraryGeometries.NAME, LibraryGeometries.class);
        NAME_TO_CLASS.add(LibraryImages.NAME, LibraryImages.class);
        NAME_TO_CLASS.add(LibraryLights.NAME, LibraryLights.class);
        NAME_TO_CLASS.add(LibraryMaterials.NAME, LibraryMaterials.class);
        NAME_TO_CLASS.add(LibraryVisualScenes.NAME, LibraryVisualScenes.class);
        NAME_TO_CLASS.add(Scene.NAME, Scene.class);


    }

    @Override
    protected DomElement createNode(DomNode parent, NamespaceContext nsc, FName name, XMLAttributes atts) {

        Class clazz = (Class) NAME_TO_CLASS.getValue(name);
        if (clazz == null) clazz = DomElement.class;

        try {
            Constructor cstr = clazz.getConstructor(FName.class, NamespaceContext.class);
            DomElement ele = (DomElement) cstr.newInstance(name,nsc);
            ele.getProperties().addAll(atts);
            return ele;
        } catch (NoSuchMethodException ex) {
            throw new RuntimeException(ex);
        } catch (SecurityException ex) {
            throw new RuntimeException(ex);
        } catch (InstantiationException ex) {
            throw new RuntimeException(ex);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        } catch (IllegalArgumentException ex) {
            throw new RuntimeException(ex);
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
    }

}
