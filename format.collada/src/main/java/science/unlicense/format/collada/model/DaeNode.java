
package science.unlicense.format.collada.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.NamespaceContext;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;

/**
 *
 * @author Johann Sorel
 */
public class DaeNode extends DomElement {

    public DaeNode(FName name, NamespaceContext nsc) {
        super(name, nsc);
    }

    protected Chars getPropertyChars(Chars name){
        final Object obj = getProperties().getValue(name);
        if (obj==null){
            return null;
        } else if (obj instanceof Chars){
            return (Chars) obj;
        } else {
            return new Chars(""+obj);
        }
    }

    protected Integer getPropertyInt(Chars name){
        final Object obj = getProperties().getValue(name);
        if (obj==null){
            return null;
        } else if (obj instanceof Chars){
            return Int32.decode((Chars) obj);
        } else if (obj instanceof Integer){
            return (Integer) obj;
        } else if (obj instanceof Number){
            return ((Number) obj).intValue();
        } else {
            return null;
        }
    }

    protected Double getPropertyDouble(Chars name){
        final Object obj = getProperties().getValue(name);
        if (obj==null){
            return null;
        } else if (obj instanceof Chars){
            return Float64.decode((Chars) obj);
        } else if (obj instanceof Double){
            return (Double) obj;
        } else if (obj instanceof Number){
            return ((Number) obj).doubleValue();
        } else {
            return null;
        }
    }

}
