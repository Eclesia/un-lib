
package science.unlicense.format.collada.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.NamespaceContext;
import science.unlicense.common.api.character.Chars;
import static science.unlicense.format.collada.ColladaConstants.NS_COLLADA_1_4;

/**
 *
 * @author Johann Sorel
 */
public class LibraryGeometries extends DaeNode {

    public static final FName NAME = new FName(NS_COLLADA_1_4, Chars.constant("library_geometries"));

    public LibraryGeometries(FName name, NamespaceContext nsc) {
        super(name, nsc);
    }

}
