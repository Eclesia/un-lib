
package science.unlicense.format.collada;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class DaeReaderTest {

    @Ignore
    @Test
    public void testRead() throws Exception{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/collada/cube.dae"));

        final ColladaStore store = new ColladaStore(path);

        final Collection elements = store.getElements();
        Assert.assertEquals(1, elements.getSize());



    }

}
