

package science.unlicense.format.midi;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;

/**
 *
 * @author Johann Sorel
 */
public class MidiStore extends AbstractStore implements Media {

    public MidiStore(Path input) {
        super(MidiFormat.INSTANCE,input);
    }

    @Override
    public Track[] getTracks() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
