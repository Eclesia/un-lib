
package science.unlicense.format.midi.model;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class TrackChunk {

    public int length;
    public final Sequence events = new  ArraySequence();

    public void read(DataInputStream ds) throws IOException {
        length   = ds.readInt();



    }

}
