

package science.unlicense.format.midi;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;

/**
 *
 * Useful docs :
 * http://en.wikipedia.org/wiki/MIDI
 * http://faydoc.tripod.com/formats/mid.htm
 * http://www.ccarh.org/courses/253/handout/smf/
 * http://www.sonicspot.com/guide/midifiles.html
 *
 * @author Johann Sorel
 */
public class MidiFormat extends AbstractMediaFormat{

    public static final MidiFormat INSTANCE = new MidiFormat();

    public MidiFormat() {
        super(new Chars("midi"));
        shortName = new Chars("MIDI");
        longName = new Chars("Musical Instrument Digital Interface");
        extensions.add(new Chars("mid"));
        resourceTypes.add(Media.class);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public Store open(Object input) throws IOException {
        return new MidiStore((Path) input);
    }
}
