
package science.unlicense.format.midi.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class HeaderChunk {

    public int length;
    public int format;
    public int nbTrack;
    public int division;

    public void read(DataInputStream ds) throws IOException {
        length   = ds.readInt();
        format   = ds.readUShort();
        nbTrack  = ds.readUShort();
        division = ds.readUShort();
    }

}
