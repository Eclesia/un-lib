package science.unlicense.format.wav;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.media.api.AudioSamples;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.media.api.DefaultAudioPacket;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.format.riff.model.RIFFChunk;
import science.unlicense.format.wav.model.DataChunk;
import science.unlicense.format.wav.model.FmtChunk;

/**
 *
 * @author Johann Sorel
 */
public class WAVReader extends AbstractReader implements MediaReadStream {

    private final RIFFChunk wavriff;
    private final FmtChunk wavmeta;
    private final DataChunk wavdata;

    private int[] samples;
    private AudioSamples as;
    private int bitsPerSample;
    private long nbSteps;
    private double timeStep;

    private DataInputStream ds;
    private long currentStep;
    private int[] next;

    public WAVReader(RIFFChunk wavriff, FmtChunk wavmeta, DataChunk wavdata) {
        this.wavriff = wavriff;
        this.wavmeta = wavmeta;
        this.wavdata = wavdata;
    }

    private void init() throws IOException{
        if (as!=null) return;

        ds = getInputAsDataStream(wavriff.getEndianness());
        ds.skipFully(wavdata.getFileOffset()+8);

        //build buffers and infos
        final int[] channels = WAVMetaModel.nbChannelToMapping(wavmeta.getAudioNbChannel());
        bitsPerSample = wavmeta.getAudioBitPerSample();
        samples = new int[channels.length];
        as = new AudioSamples(channels, AudioSamples.ENCODING_PCM, bitsPerSample, samples);
        final int bitsPerStep = samples.length * bitsPerSample;
        nbSteps = (wavdata.getSize() * 8) / bitsPerStep;
        timeStep = 1d / wavmeta.getAudioFrequency();
        currentStep = -1;
    }

    public RIFFChunk getRIFFChunk() {
        return wavriff;
    }

    public FmtChunk getFmtChunk() {
        return wavmeta;
    }

    public DataChunk getDataChunk() {
        return wavdata;
    }

    public MediaPacket next() throws IOException {
        findNext();
        if (next!=null){
            final long time = (long) (currentStep * timeStep);
            next = null;

            return new DefaultAudioPacket(null, time, (long) timeStep, as);
        }
        return null;
    }

    private void findNext() throws IOException{
        if (next!=null) return;
        init();
        currentStep++;

        if (currentStep>=nbSteps){
            //nothing left
            return;
        }

        for (int i=0;i<samples.length;i++){
            if (bitsPerSample==8) samples[i] = ds.readUByte();
            else if (bitsPerSample==16) samples[i] = ds.readUShort();
            else if (bitsPerSample==24) samples[i] = ds.readUInt24();
            else if (bitsPerSample==32) samples[i] = ds.readInt();
            else throw new IOException("Unexpected number of bits per sample : "+bitsPerSample);
        }
        next = samples;
    }

    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
