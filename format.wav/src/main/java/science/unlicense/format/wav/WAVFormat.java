
package science.unlicense.format.wav;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaCapabilities;

/**
 *
 * @author Johann Sorel
 */
public class WAVFormat extends AbstractMediaFormat {

    public static final WAVFormat INSTANCE = new WAVFormat();

    public WAVFormat() {
        super(new Chars("wav"));
        shortName = new Chars("WAVE");
        longName = new Chars("Waveform Audio File Format");
        mimeTypes.add(new Chars("audio/vnd.wave"));
        mimeTypes.add(new Chars("audio/wav"));
        mimeTypes.add(new Chars("audio/wave"));
        mimeTypes.add(new Chars("audio/x-wav"));
        extensions.add(new Chars("wav"));
        extensions.add(new Chars("wave"));
        resourceTypes.add(Media.class);
    }

    @Override
    public MediaCapabilities getCapabilities() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object input) {
        return new WAVMediaStore((Path) input);
    }

}
