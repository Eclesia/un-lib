

package science.unlicense.format.wav.model;

import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.wav.WAVMetaModel;

/**
 *
 * @author Johann Sorel
 */
public class DataChunk extends DefaultChunk{

    public DataChunk() {
        super(WAVMetaModel.DATA);
    }

}
