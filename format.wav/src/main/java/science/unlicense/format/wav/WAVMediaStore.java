

package science.unlicense.format.wav;

import science.unlicense.format.riff.RIFFReader;
import science.unlicense.format.riff.model.Chunk;
import science.unlicense.format.riff.model.RIFFChunk;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.media.api.AudioTrack;
import science.unlicense.media.api.DefaultAudioTrack;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;
import science.unlicense.format.wav.model.DataChunk;
import science.unlicense.format.wav.model.FmtChunk;

/**
 *
 * @author Johann Sorel
 */
public class WAVMediaStore extends AbstractStore implements Media {

    private final Path input;
    private RIFFChunk wavriff;
    private FmtChunk wavmeta;
    private DataChunk wavdata;

    public WAVMediaStore(Path input) {
        super(WAVFormat.INSTANCE,input);
        this.input = input;
    }

    /**
     * Parse file header to build metadatas
     */
    private void analyze() throws IOException{
        if (wavriff!=null) return;
        final RIFFReader reader = createReader();
        while (reader.hasNext()){
            final Chunk chunk = reader.next().getChunk();
            if (chunk instanceof RIFFChunk){
                wavriff = (RIFFChunk) chunk;
            } else if (chunk instanceof FmtChunk){
                wavmeta = (FmtChunk) chunk;
            } else if (chunk instanceof DataChunk){
                wavdata = (DataChunk) chunk;
            }
        }
    }

    @Override
    public Track[] getTracks() throws IOException {
        analyze();

        final int[] channels = WAVMetaModel.nbChannelToMapping(wavmeta.getAudioNbChannel());
        final int[] bitsPerSample = new int[channels.length];
        Arrays.fill(bitsPerSample, wavmeta.getAudioBitPerSample());

        final AudioTrack audioMeta = new DefaultAudioTrack(
                new Chars("stream"), channels,bitsPerSample, wavmeta.getAudioFrequency());
        return new Track[]{audioMeta};
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        analyze();
        final WAVReader reader = new WAVReader(wavriff,wavmeta,wavdata);
        reader.setInput(input);
        return reader;
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    private RIFFReader createReader() throws IOException{
        final Dictionary dico = new HashDictionary();
        dico.add(WAVMetaModel.FMT, FmtChunk.class);
        dico.add(WAVMetaModel.DATA, DataChunk.class);
        final RIFFReader reader = new RIFFReader(dico);
        reader.setInput(input);
        return reader;
    }

}
