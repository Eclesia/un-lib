

package science.unlicense.format.wav;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.media.api.MediaFormat;
import science.unlicense.media.api.Medias;

/**
 *
 * @author Johann Sorel
 */
public class WAVFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final MediaFormat[] formats = Medias.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof WAVFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("WAV format not found.");
    }
}
