
package science.unlicense.format.wav;

import org.junit.Test;
import science.unlicense.format.riff.model.RIFFChunk;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.wav.model.DataChunk;
import science.unlicense.format.wav.model.FmtChunk;
import science.unlicense.media.api.AudioPacket;
import science.unlicense.media.api.AudioSamples;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.media.api.Medias;

/**
 *
 * @author Johann Sorel
 */
public class WAVReaderTest {

    @Test
    public void testRead() throws IOException{

        final WAVMediaStore store = (WAVMediaStore) Medias.open(Paths.resolve(new Chars("mod:/science/unlicense/format/wav/sample.wav")));

        final WAVReader reader = (WAVReader) store.createReader(null);

        final RIFFChunk riff = reader.getRIFFChunk();
        final FmtChunk meta = reader.getFmtChunk();
        final DataChunk data = reader.getDataChunk();

        Assert.assertEquals(new Chars("RIFF"), riff.getFourCC());
        Assert.assertEquals(11960l, (long) riff.getSize());
        Assert.assertEquals(new Chars("WAVE"), riff.getType());

        Assert.assertEquals(new Chars("fmt "), meta.getFourCC());
        Assert.assertEquals(16l, (long) meta.getSize());
        Assert.assertEquals(1, (int) meta.getAudioFormat());
        Assert.assertEquals(1, (int) meta.getAudioNbChannel());
        Assert.assertEquals(44100, (long) meta.getAudioFrequency());
        Assert.assertEquals(88200, (long) meta.getAudioBytePerSec());
        Assert.assertEquals(2, (int) meta.getAudioBytePerBlock());
        Assert.assertEquals(16, (int) meta.getAudioBitPerSample());

        Assert.assertEquals(new Chars("data"), data.getFourCC());
        Assert.assertEquals(11924, (long) data.getSize());

        final int[] expectedSamples = new int[]{1,4095,8195,12284,16387,20478,21505,17409};

        int nbSample = 0;
        for (MediaPacket pack=reader.next();pack!=null;pack=reader.next()){
            //check the 8 first samples
            AudioSamples as = (AudioSamples) ((AudioPacket) pack).getSamples();
            int[] samples = as.asPCM(null, 16);
            if (nbSample<8){
                Assert.assertEquals(expectedSamples[nbSample], samples[0]);
            }
            nbSample++;
        }

        Assert.assertEquals(5962, nbSample);
    }

}
