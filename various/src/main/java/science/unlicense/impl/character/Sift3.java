package science.unlicense.impl.character;

import science.unlicense.common.api.character.Chars;

/**
 * Algorithm to calculate distances between Strings.
 * http://siderite.blogspot.com/2007/04/super-fast-and-accurate-string-distance.html
 * https://github.com/awnist/distance
 *
 * @author Johann Sorel (UN Project adaptation)
 */
public final class Sift3 {

    private Sift3() {}

    public static float distance(Chars s1, Chars s2) {
        if (s1==null || s1.isEmpty()) {
            if (s2 == null || s2.isEmpty()) {
                return 0;
            } else {
                return s2.getCharLength();
            }
        }
        if (s2 == null || s2.isEmpty()) {
            return s1.getCharLength();
        }

        int c = 0;
        int i = 0;
        int lcs = 0;
        int maxOffset = 5;
        int offset1 = 0;
        int offset2 = 0;

        final int s1Length = s1.getCharLength();
        final int s2Length = s2.getCharLength();

        while ((c + offset1 < s1Length) && (c + offset2 < s2Length)) {
            if (s1.getUnicode(c + offset1) == s2.getUnicode(c + offset2)) {
                lcs++;
            } else {
                offset1 = offset2 = i = 0;
                while (i < maxOffset) {
                    if ((c + i < s1Length) && (s1.getUnicode(c + i) == s2.getUnicode(c))) {
                        offset1 = i;
                        break;
                    }
                    if ((c + i < s2Length) && (s1.getUnicode(c) == s2.getUnicode(c + i))) {
                        offset2 = i;
                        break;
                    }
                    i++;
                }
            }
            c++;
        }
        return (s1Length + s2Length)/2f - lcs;
    }
}
