package science.unlicense.engine.android;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;

import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.visual.ViewRenderLoop;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class ADWidgetView extends View {

    private final WContainer container = new WContainer();


    public ADWidgetView(Context context) {
        super(context);

        //repaint on widget updates
        container.addEventListener(new PropertyPredicate(WContainer.PROPERTY_DIRTY), new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                BBox bbox = (BBox) ((PropertyMessage) event.getMessage()).getNewValue();
                double x = bbox.getMin(0)+getWidth()/2.0;
                double y = bbox.getMin(1)+getHeight()/2.0;
                double w = bbox.getSpan(0);
                double h = bbox.getSpan(1);
                invalidate((int) x,(int) y, (int) (w+0.5), (int) (h+0.5));
            }
        });

    }

    public WContainer getContainer() {
        return container;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        final ADPainter2D painter = new ADPainter2D(canvas);
        container.setEffectiveExtent(new Extent.Long(getWidth(),getHeight()));

        final double trsX = getWidth()/2.0;
        final double trsY = getHeight()/2.0;
        final BBox bbox = new BBox(2);
        bbox.setRange(0,-trsX,+trsX);
        bbox.setRange(1,-trsY,+trsY);

        final Affine2 trs = new Affine2();
        trs.setM02(trsX);
        trs.setM12(trsY);

        Painter2D t = painter.derivate(true);
        t.setTransform(trs);
        ViewRenderLoop.render(container, t, bbox);
    }
}
