package science.unlicense.engine.android;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Shader;
import android.graphics.Typeface;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.painter2d.AbstractPainter2D;
import science.unlicense.display.api.painter2d.Brush;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.LinearGradientPaint;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.display.api.painter2d.RadialGradientPaint;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector4f64;

/**
 *
 * @author Johann Sorel
 */
public class ADPainter2D extends AbstractPainter2D {

    private static final Matrix IDENTITY = new Matrix();
    private final Canvas g;

    public ADPainter2D(Canvas canvas) {
        g = canvas;
    }

    @Override
    public void setClip(PlanarGeometry geom) {
        super.setClip(geom);
        if (geom!=null) {
            g.setMatrix(IDENTITY);
            g.clipPath(toShape(TransformedGeometry.create(geom, transform)));
        } else {
            g.clipRect(new Rect(0,0,g.getWidth(),g.getHeight()), Region.Op.REPLACE);
        }
    }

    @Override
    public void fill(CharArray text, float x, float y) {
        final Paint paint = toPaint(this.fillPaint);
        paint.setStyle(Paint.Style.FILL);
        paint.setXfermode(toComposite(alphaBlending));
        paint.setTypeface(toFont(font));
        paint.setTextSize(25);
        g.setMatrix(toTransform(transform));
        g.drawText(text.toString(), x, y, paint);
    }

    @Override
    public void fill(PlanarGeometry geom) {

        final Path shp = toShape(geom);
        final Paint paint = toPaint(this.fillPaint);
        final Matrix trs = toTransform(transform);

        if (shp!=null && paint!=null) {
            paint.setStyle(Paint.Style.FILL);
            paint.setXfermode(toComposite(alphaBlending));
            g.setMatrix(trs);
            g.drawPath(shp,paint);
        }

    }

    @Override
    public void stroke(PlanarGeometry geom) {
        final Path shp = toShape(geom);
        final Paint paint = toPaint(this.fillPaint);
        final Matrix trs = toTransform(transform);

        if (shp!=null && paint!=null) {
            paint.setStrokeWidth(toStroke(brush));
            paint.setStyle(Paint.Style.STROKE);
            paint.setXfermode(toComposite(alphaBlending));
            g.setMatrix(trs);
            g.drawPath(shp,paint);
        }
    }

    @Override
    public void paint(Image image, Affine transform) {
        final Bitmap img = toImage(image);
        final Paint paint = new Paint();
        Matrix trs = toTransform(transform);
        if (trs==null) trs = new Matrix();

        if (img!=null) {
            paint.setXfermode(toComposite(alphaBlending));
            g.setMatrix(toTransform(transform));
            g.drawBitmap(img, trs, paint);
        }
    }

    @Override
    public void dispose() {
    }

    @Override
    public FontStore getFontStore() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private Path toShape(PlanarGeometry geom) {
        if (geom==null) return null;

        final PathIterator ite = geom.createPathIterator();
        final Path path = new Path();

        final TupleRW p = new Vector4f64();
        final TupleRW c1 = new Vector4f64();
        final TupleRW c2 = new Vector4f64();
        while (ite.next()) {
            final int type = ite.getType();
            if (type==PathIterator.TYPE_MOVE_TO) {
                ite.getPosition(p);
                path.moveTo((float) p.get(0), (float) p.get(1));
            } else if (type==PathIterator.TYPE_LINE_TO) {
                ite.getPosition(p);
                path.lineTo((float) p.get(0), (float) p.get(1));
            } else if (type==PathIterator.TYPE_ARC) {
                ite.getPosition(p);
                path.lineTo((float) p.get(0), (float) p.get(1));
            } else if (type==PathIterator.TYPE_CUBIC) {
                ite.getPosition(p);
                ite.getFirstControl(c1);
                ite.getSecondControl(c2);
                path.rCubicTo((float) c1.get(0), (float) c1.get(1), (float) c2.get(0), (float) c2.get(1), (float) p.get(0), (float) p.get(1));
            } else if (type==PathIterator.TYPE_QUADRATIC) {
                ite.getPosition(p);
                ite.getFirstControl(c1);
                path.quadTo((float) c1.get(0), (float) c1.get(1), (float) p.get(0), (float) p.get(1));
            } else if (type==PathIterator.TYPE_CLOSE) {
                path.close();
            }
        }
        return path;
    }

    private Paint toPaint(science.unlicense.display.api.painter2d.Paint paint) {
        if (paint==null) return null;

        Paint p = new Paint();
        if (paint instanceof ColorPaint) {
            p.setColor(toColor( ((ColorPaint) paint).getColor()));
        } else if (paint instanceof LinearGradientPaint) {
            final LinearGradientPaint lgp = (LinearGradientPaint) paint;
            final science.unlicense.image.api.color.Color[] colors = lgp.getStopColors();
            final int[] array = new int[colors.length];
            for (int i=0;i<array.length;i++) array[i] = toColor(colors[i]);
            final float[] positions = Arrays.reformatToFloat(lgp.getStopOffsets());
            p.setShader(new LinearGradient((float) lgp.getStartX(), (float) lgp.getStartY(), (float) lgp.getEndX(),(float) lgp.getEndY(), array, positions, Shader.TileMode.MIRROR));
        } else if (paint instanceof RadialGradientPaint) {
            throw new UnimplementedException("type : "+paint.getClass());
        } else {
            throw new InvalidArgumentException("Unexpected type : "+paint.getClass());
        }
        return p;
    }

    private float toStroke(Brush brush) {
        if (brush instanceof PlainBrush) {
            final PlainBrush b = (PlainBrush) this.brush;
            return b.getWidth();
        } else {
            throw new InvalidArgumentException("Unexpected type : "+brush.getClass());
        }
    }

    private PorterDuffXfermode toComposite(AlphaBlending blending) {
        if (blending==null) return null;

        final float alpha = blending.getAlpha();
        final int type = blending.getType();
        //AlphaBlending.DST
        final PorterDuff.Mode g2dtype;
        if (type==AlphaBlending.CLEAR)           g2dtype = PorterDuff.Mode.CLEAR;
        else if (type==AlphaBlending.XOR)        g2dtype = PorterDuff.Mode.XOR;
        else if (type==AlphaBlending.DST)        g2dtype = PorterDuff.Mode.DST;
        else if (type==AlphaBlending.DST_ATOP)   g2dtype = PorterDuff.Mode.DST_ATOP;
        else if (type==AlphaBlending.DST_IN)     g2dtype = PorterDuff.Mode.DST_IN;
        else if (type==AlphaBlending.DST_OUT)    g2dtype = PorterDuff.Mode.DST_OUT;
        else if (type==AlphaBlending.DST_OVER)   g2dtype = PorterDuff.Mode.DST_OVER;
        else if (type==AlphaBlending.SRC)        g2dtype = PorterDuff.Mode.SRC;
        else if (type==AlphaBlending.SRC_ATOP)   g2dtype = PorterDuff.Mode.SRC_ATOP;
        else if (type==AlphaBlending.SRC_IN)     g2dtype = PorterDuff.Mode.SRC_IN;
        else if (type==AlphaBlending.SRC_OUT)    g2dtype = PorterDuff.Mode.SRC_OUT;
        else if (type==AlphaBlending.SRC_OVER)   g2dtype = PorterDuff.Mode.SRC_OVER;
        else throw new InvalidArgumentException("Unexpected type : "+type);

        return new PorterDuffXfermode(g2dtype); //todo missing alpha value
    }

    private Bitmap toImage(Image image) {
        if (image==null) return null;

        //convert it to a RGBA Bitmap.
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final Vector2i32 coord = new Vector2i32();

        final Bitmap bi = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        for (coord.y = 0; coord.y <height; coord.y++) {
            for (coord.x = 0; coord.x < width; coord.x++) {
                bi.setPixel(coord.x, coord.y, image.getColor(coord).toARGB());
            }
        }

        return bi;
    }

    public static Matrix toTransform(Affine trs) {
        if (trs==null) return null;

        final Matrix m = new Matrix();
        m.setValues(trs.toMatrix().toArrayFloat());
        return m;
    }

    public static Affine2 toMatrix(Matrix trs) {
        if (trs==null) return null;

        float[] values = new float[9];
        trs.getValues(values);

        return new Affine2( values[0], values[1], values[2],
                            values[3], values[4], values[5]);
    }

    private int toColor(science.unlicense.image.api.color.Color color) {
        return color.toARGB();
    }

    private Typeface toFont(science.unlicense.display.api.font.FontChoice font) {
        return Typeface.DEFAULT;
//        final String family = font.getFamilies()[0].toString();
//        final int weight = font.getWeight();
//        int fw = Font.PLAIN;
//        if (weight==Font.WEIGHT_BOLD) {
//            fw = Typeface.DEFAULT_BOLD;
//        }
//        Typeface tf = Typeface.create(family,fw);
//        return tf.
//        return new Font(family, fw, (int) font.getSize());
    }


}
