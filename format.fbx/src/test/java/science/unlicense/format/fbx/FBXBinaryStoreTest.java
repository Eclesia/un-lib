
package science.unlicense.format.fbx;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.fbx.binary.FBXBinaryStore;

/**
 *
 * @author Johann Sorel
 */
public class FBXBinaryStoreTest {

    @Test
    public void testReadCube() throws StoreException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/fbx/cube.fbx"));
        final FBXStore store = new FBXBinaryStore(path);

        store.getElements();

    }

}
