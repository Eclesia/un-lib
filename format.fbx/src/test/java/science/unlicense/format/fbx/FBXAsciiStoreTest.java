
package science.unlicense.format.fbx;

import science.unlicense.format.fbx.FBXStore;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.fbx.ascii.FBXAsciiStore;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class FBXAsciiStoreTest {

    @Ignore
    @Test
    public void testReadCube() throws StoreException{

        final Path path = Paths.resolve(new Chars("todo generate an fbx file"));
        final FBXStore store = new FBXAsciiStore(path);

        store.getElements();

    }

}
