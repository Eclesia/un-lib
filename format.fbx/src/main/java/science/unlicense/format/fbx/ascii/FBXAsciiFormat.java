
package science.unlicense.format.fbx.ascii;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;
import science.unlicense.format.fbx.FBXConstants;
/**
 *
 * Some doc :
 * http://code.blender.org/index.php/2013/08/fbx-binary-file-format-specification/
 * http://wiki.blender.org/index.php/User:Mont29/Foundation/FBX_File_Structure
 *
 * @author Johann Sorel
 */
public class FBXAsciiFormat extends AbstractModel3DFormat {

    public static final FBXAsciiFormat INSTANCE = new FBXAsciiFormat();

    private FBXAsciiFormat() {
        super(new Chars("FBX-ASCII"));
        shortName = new Chars("Fbx ascii");
        longName = new Chars("FBX Ascii");
        extensions.add(new Chars("fbx"));
        signatures.add(FBXConstants.SIGNATURE_ASCII);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new FBXAsciiStore(input);
    }

}
