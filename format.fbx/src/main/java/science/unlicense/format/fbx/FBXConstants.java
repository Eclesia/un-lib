
package science.unlicense.format.fbx;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class FBXConstants {

    public static final byte[] SIGNATURE_BINARY = new byte[]{'K','a','y','d','a','r','a',' ','F','B','X',' ','B','i','n','a','r','y',' ',' ','\0'};
    public static final byte[] SIGNATURE_ASCII = new byte[]{';',' ','F','B','X'};

    public static final byte[] NULL = new byte[13];

    public static final Chars NODE_OBJECTS = Chars.constant("Objects");
    public static final Chars NODE_MODEL = Chars.constant("Model");
    public static final Chars NODE_MODEL_VERTICES = Chars.constant("Vertices");
    public static final Chars NODE_MODEL_INDEX = Chars.constant("PolygonVertexIndex");
    public static final Chars NODE_MODEL_EDGES = Chars.constant("Edges");
    public static final Chars NODE_MODEL_LAYER_NORMAL = Chars.constant("LayerElementNormal");
    public static final Chars NODE_MODEL_LAYER_COLOR = Chars.constant("LayerElementColor");
    public static final Chars NODE_MODEL_LAYER_UV = Chars.constant("LayerElementUV");
    public static final Chars NODE_MODEL_LAYER_TRSUV = Chars.constant("LayerElementTransparentUV");
    public static final Chars NODE_MODEL_LAYER_SMOOTHING = Chars.constant("LayerElementSmoothing");
    public static final Chars NODE_MODEL_LAYER_VISIBILITY = Chars.constant("LayerElementVisibility");
    public static final Chars NODE_MODEL_LAYER_MATERIAL = Chars.constant("LayerElementMaterial");
    public static final Chars NODE_MODEL_LAYER_TEXTURE = Chars.constant("LayerElementTexture");
    public static final Chars NODE_MODEL_LAYER_TRSTEXTURE = Chars.constant("LayerElementTransparentTextures");
    public static final Chars NODE_VIDEO = Chars.constant("Video");
    public static final Chars NODE_TEXTURE = Chars.constant("Texture");
    public static final Chars NODE_TEXTURE_FILENAME = Chars.constant("FileName");




    private FBXConstants(){}
}
