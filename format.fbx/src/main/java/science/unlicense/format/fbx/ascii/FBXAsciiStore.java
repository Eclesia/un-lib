
package science.unlicense.format.fbx.ascii;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.fbx.FBXStore;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenType;
import science.unlicense.syntax.api.parser.Parser;
import science.unlicense.syntax.api.parser.Rule;
import science.unlicense.syntax.api.parser.SyntaxNode;
import science.unlicense.syntax.impl.grammar.io.UNGrammarReader;

/**
 *
 * @author Johann Sorel
 */
public class FBXAsciiStore extends FBXStore{

    public FBXAsciiStore(Object input) {
        super(FBXAsciiFormat.INSTANCE,input);
    }

    protected NamedNode read() throws IOException {
        final ByteInputStream in = getSourceAsInputStream();

        //parse text content
        final UNGrammarReader reader = new UNGrammarReader();
        reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/format/fbx/fbx.gr")));
        final OrderedHashDictionary tokens = new OrderedHashDictionary();
        final OrderedHashDictionary rules = new OrderedHashDictionary();
        reader.read(tokens, rules);

        final Rule rule = (Rule) rules.getValue(new Chars("file"));

        //prepare lexer
        final Lexer lexer = new Lexer();
        lexer.setInput(in);

        //prepare parser
        final Parser parser = new Parser(rule);
        parser.setInput(lexer);
        final SyntaxNode node = parser.parse();

        final TokenType whitespace = (TokenType) tokens.getValue(new Chars("WS"));

        node.trim(new Predicate() {
            public Boolean evaluate(Object candidate) {
                final SyntaxNode sn = (SyntaxNode) candidate;
                final Token token = sn.getToken();
                return token!=null && token.type == whitespace;
            }
        });
        System.out.println(Nodes.toCharsTree(node, Chars.EMPTY, 20));

        return null;
    }

}
