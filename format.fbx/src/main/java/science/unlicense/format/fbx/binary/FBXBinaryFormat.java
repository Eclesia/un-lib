
package science.unlicense.format.fbx.binary;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;
import science.unlicense.format.fbx.FBXConstants;

/**
 *
 * Some doc :
 * http://code.blender.org/index.php/2013/08/fbx-binary-file-format-specification/
 * http://wiki.blender.org/index.php/User:Mont29/Foundation/FBX_File_Structure
 *
 * @author Johann Sorel
 */
public class FBXBinaryFormat extends AbstractModel3DFormat{

    public static final FBXBinaryFormat INSTANCE = new FBXBinaryFormat();

    private FBXBinaryFormat() {
        super(new Chars("FBX-BINARY"));
        shortName = new Chars("Fbx binary");
        longName = new Chars("FBX binary");
        extensions.add(new Chars("fbx"));
        signatures.add(FBXConstants.SIGNATURE_BINARY);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new FBXBinaryStore(input);
    }

}
