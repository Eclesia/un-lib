
package science.unlicense.format.fbx;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.IntSequence;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 *
 * @author Johann Sorel
 */
public abstract class FBXStore extends AbstractModel3DStore{

    private NamedNode root;

    public FBXStore(Format format, Object input) {
        super(format,input);
    }

    @Override
    public Collection getElements() throws StoreException {
        try {
            root = read();
            return convert(root);
        } catch (IOException ex) {
            throw new StoreException(ex);
        }
    }

    protected abstract NamedNode read() throws IOException;

    private Collection convert(NamedNode root) throws StoreException{
        final Sequence models = new ArraySequence();

        final NamedNode objects = root.getNamedChild(FBXConstants.NODE_OBJECTS);
        final Sequence modelNodes = objects.getNamedChildren(FBXConstants.NODE_MODEL);
        for (int i=0,n=modelNodes.getSize();i<n;i++){
            final DefaultModel mesh = convertModel((NamedNode) modelNodes.get(i));
            if (mesh!=null){
                models.add(mesh);
            }
        }

        return models;
    }

    private DefaultModel convertModel(NamedNode model) throws StoreException{

        final NamedNode verticesNode = model.getNamedChild(FBXConstants.NODE_MODEL_VERTICES);
        final NamedNode indexNode = model.getNamedChild(FBXConstants.NODE_MODEL_INDEX);

        if (verticesNode==null || indexNode==null){
            return null;
        }

        //convert vertices
        final Object[] vs = (Object[]) verticesNode.getValue();
        final float[] vertices = new float[vs.length];
        for (int i=0;i<vertices.length;i++){
            vertices[i] = ((Number) vs[i]).floatValue();
        }

        //convert indexes
        final Object[] is = (Object[]) indexNode.getValue();
        final IntSequence index = new IntSequence();
        final IntSequence polygon = new IntSequence();
        for (int i=0;i<is.length;){
            polygon.removeAll();
            int d;
            do {
               d = ((Number) is[i]).intValue();
               i++;
               if (d>=0){
                   polygon.put(d);
               } else {
                   polygon.put(d*-1+1);
               }
            } while (d>=0);

            if (polygon.getSize()==3){
                index.put(polygon.toArrayInt());
            } else if (polygon.getSize()==4){
                final int[] array = polygon.toArrayInt();
                index.put(array[0]);
                index.put(array[1]);
                index.put(array[2]);
                index.put(array[0]);
                index.put(array[2]);
                index.put(array[3]);
            } else {
                final int[] polyvi = polygon.toArrayInt();
                final Tuple[] contour = new Tuple[polyvi.length];
                final VBO vbo = new VBO(vertices, 3);
                for (int k=0;k<polyvi.length;k++){
                    final TupleRW v = vbo.createTuple();
                    vbo.getTuple(polyvi[k], v);
                    contour[k] = v;
                }
                try {
                    final int[][] idx = Geometries.triangulate(contour);
                    for (int k=0;k<idx.length;k++){
                        index.put(polyvi[idx[k][0]]);
                        index.put(polyvi[idx[k][1]]);
                        index.put(polyvi[idx[k][2]]);
                    }
                } catch (OperationException ex) {
                    throw new StoreException(ex);
                }
            }
        }

        final DefaultModel mesh = new DefaultModel();
        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(new VBO(vertices, 3));
        shell.setIndex(new IBO(index.toArrayInt()));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, index.getSize()/3)});
        Mesh.calculateNormals(shell);

        mesh.setShape(shell);
        return mesh;
    }

}
