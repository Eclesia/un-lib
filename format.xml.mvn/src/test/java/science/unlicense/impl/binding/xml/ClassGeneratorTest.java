

package science.unlicense.impl.binding.xml;

import java.util.Date;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.code.java.model.JavaClass;
import science.unlicense.code.java.model.JavaProperty;
import science.unlicense.code.java.model.JavaScope;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class ClassGeneratorTest {

    @Ignore
    @Test
    public void testFlatSingleClass() throws IOException{
        final Path xsdPath = Paths.resolve(new Chars("mod:/un/storage/binding/xml/FlatSingleClass.xsd"));
        final Dictionary nsTopk = new HashDictionary();
        nsTopk.add(new Chars("http://unlicense.developpez.com"), new Chars("un.test"));

        final ClassGenerator generator = new ClassGenerator(xsdPath, nsTopk, "POJO");
        final Collection classes = generator.generate().getValues();

        Assert.assertEquals(1,classes.getSize());

        final JavaClass clazz = (JavaClass) classes.createIterator().next();
        Assert.assertEquals(JavaScope.PUBLIC, clazz.getScope().getType());
        Assert.assertEquals(new Chars("un.test.Something"), clazz.id);
        Assert.assertEquals(null, clazz.getDocumentation().getText());
        Assert.assertEquals(0, clazz.functions.getSize());
        Assert.assertEquals(17, clazz.properties.getSize());

        int i=-1;
        Assert.assertEquals(new JavaProperty(new Chars("attBoolean"),null,null,Boolean.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attByte"),null,null,Byte.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attInteger"),null,null,Integer.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attLong"),null,null,Long.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attFloat"),null,null,Float.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attDouble"),null,null,Double.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attUnsignedByte"),null,null,Byte.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attUnsignedInt"),null,null,Integer.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attUnsignedLong"),null,null,Long.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attNonNegativeInteger"),null,null,Integer.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attPositiveInteger"),null,null,Integer.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attDate"),null,null,Date.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attDateTime"),null,null,Date.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attTime"),null,null,Date.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attInt"),null,null,Integer.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attAnyURI"),null,null,Chars.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attToken"),null,null,Chars.class,JavaScope.PUBLIC), clazz.properties.get(++i));

    }

    @Ignore
    @Test
    public void testDuplicateName() throws IOException{
        final Path xsdPath = Paths.resolve(new Chars("mod:/un/storage/binding/xml/DuplicateName.xsd"));
        final Dictionary nsTopk = new HashDictionary();
        nsTopk.add(new Chars("http://unlicense.developpez.com"), new Chars("un.test"));

        final ClassGenerator generator = new ClassGenerator(xsdPath, nsTopk, "POJO");
        final Dictionary classes = generator.generate();

        Assert.assertEquals(2,classes.getSize());

        final JavaClass somethingClass = (JavaClass) classes.getValue(new Chars("http://unlicense.developpez.com:Something"));
        final JavaClass otherClass = (JavaClass) classes.getValue(new Chars("http://unlicense.developpez.com:Other"));


        Assert.assertEquals(JavaScope.PUBLIC, otherClass.getScope().getType());
        Assert.assertEquals(new Chars("un.test.Other"), otherClass.id);
        Assert.assertEquals(null, otherClass.getDocumentation().getText());
        Assert.assertEquals(0, otherClass.functions.getSize());
        Assert.assertEquals(1, otherClass.properties.getSize());
        Assert.assertEquals(new JavaProperty(new Chars("attBoolean"),null,null,Boolean.class,JavaScope.PUBLIC), otherClass.properties.get(0));


        Assert.assertEquals(JavaScope.PUBLIC, somethingClass.getScope().getType());
        Assert.assertEquals(new Chars("un.test.Something"), somethingClass.id);
        Assert.assertEquals(null, somethingClass.getDocumentation().getText());
        Assert.assertEquals(0, somethingClass.functions.getSize());
        Assert.assertEquals(2, somethingClass.properties.getSize());

        //attribute should have the att prefix
        Assert.assertEquals(new JavaProperty(new Chars("attchild"),null,null,Boolean.class,JavaScope.PUBLIC), somethingClass.properties.get(0));
        Assert.assertEquals(new JavaProperty(new Chars("child"),null,otherClass,null,JavaScope.PUBLIC), somethingClass.properties.get(1));

    }

    @Ignore
    @Test
    public void testReservedName() throws IOException{
        final Path xsdPath = Paths.resolve(new Chars("mod:/un/storage/binding/xml/ReservedName.xsd"));
        final Dictionary nsTopk = new HashDictionary();
        nsTopk.add(new Chars("http://unlicense.developpez.com"), new Chars("un.test"));

        final ClassGenerator generator = new ClassGenerator(xsdPath, nsTopk, "POJO");
        final Collection classes = generator.generate().getValues();

        Assert.assertEquals(1,classes.getSize());

        final JavaClass clazz = (JavaClass) classes.createIterator().next();
        Assert.assertEquals(JavaScope.PUBLIC, clazz.getScope().getType());
        Assert.assertEquals(new Chars("un.test.Something"), clazz.id);
        Assert.assertEquals(null, clazz.getDocumentation().getText());
        Assert.assertEquals(0, clazz.functions.getSize());
        Assert.assertEquals(7, clazz.properties.getSize());

        //java reserved keyword, should be renamed
        int i=-1;
        Assert.assertEquals(new JavaProperty(new Chars("attboolean"),null,null,Boolean.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attbyte"),null,null,Byte.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attshort"),null,null,Integer.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attint"),null,null,Integer.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attlong"),null,null,Long.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attfloat"),null,null,Float.class,JavaScope.PUBLIC), clazz.properties.get(++i));
        Assert.assertEquals(new JavaProperty(new Chars("attdouble"),null,null,Double.class,JavaScope.PUBLIC), clazz.properties.get(++i));

    }

}
