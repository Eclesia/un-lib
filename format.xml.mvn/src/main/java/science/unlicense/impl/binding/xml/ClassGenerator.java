

package science.unlicense.impl.binding.xml;

import science.unlicense.code.java.model.JavaClass;
import science.unlicense.code.java.model.JavaDocumentation;
import science.unlicense.code.java.model.JavaMetas;
import science.unlicense.code.java.model.JavaProperty;
import science.unlicense.code.java.model.JavaScope;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.character.Languages;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.NodeCardinality;
import science.unlicense.common.api.model.tree.NodeType;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.schema.XSDConstants;
import science.unlicense.format.xml.schema.XSDStack;
import science.unlicense.format.xml.schema.model.XSDAll;
import science.unlicense.format.xml.schema.model.XSDAnnotated;
import science.unlicense.format.xml.schema.model.XSDAnnotation;
import science.unlicense.format.xml.schema.model.XSDAny;
import science.unlicense.format.xml.schema.model.XSDAttribute;
import science.unlicense.format.xml.schema.model.XSDAttributeGroup;
import science.unlicense.format.xml.schema.model.XSDChoice;
import science.unlicense.format.xml.schema.model.XSDComplexType;
import science.unlicense.format.xml.schema.model.XSDDocumentation;
import science.unlicense.format.xml.schema.model.XSDElement;
import science.unlicense.format.xml.schema.model.XSDExtensionType;
import science.unlicense.format.xml.schema.model.XSDGroup;
import science.unlicense.format.xml.schema.model.XSDSchema;
import science.unlicense.format.xml.schema.model.XSDSimpleType;

/**
 * Utility class to generate classes from XSD.
 *
 * @author Johann Sorel
 */
public class ClassGenerator {


    // FName -> JavaProperty
    private final Dictionary attCache = new HashDictionary();
    // FName -> JavaClass
    private final Dictionary ctypeCache = new HashDictionary();

    private final Dictionary nsToPackage;
    private final Path xsdPath;
    private final String classType;

    /**
     *
     * @param xsdPath
     * @param classPackages namespace=package
     * @param classType
     */
    public ClassGenerator(Path xsdPath, Dictionary classPackages, String classType) {
        this.xsdPath = xsdPath;
        this.nsToPackage = classPackages;
        this.classType = classType;
    }

    /**
     * Generate classes.
     *
     * @return Dictionary of fullNsName = class
     * @throws IOException
     */
    public Dictionary generate() throws IOException {

        //cache of prepared classes
        final Dictionary dico = new HashDictionary();

        //read the xsds
        final XSDStack stack = new XSDStack(xsdPath,true);
        final Iterator schemas = stack.getSchemas().getValues().createIterator();

        while (schemas.hasNext()) {
            final XSDSchema schema = (XSDSchema) schemas.next();
            final Chars ns = schema.targetNamespace;

            final Sequence elements = schema.elements;
            for (int i=0,n=elements.getSize();i<n;i++) {
                final Object ele = elements.get(i);
                System.out.println(ele);
                if (ele instanceof XSDAnnotation) {
                    final XSDAnnotation cdt = (XSDAnnotation) ele;
                    //TODO

                } else if (ele instanceof XSDElement) {
                    final XSDElement cdt = (XSDElement) ele;
                    final FName name = cdt.name==null ? new FName(ns, cdt.id) : cdt.name;

                    if (cdt.complexType !=null) {
                        if (cdt.type!=null) {
                            //TODO subtype
                            final JavaClass clazz = getOrcreateCType(stack,schema,cdt.complexType,name);
                            dico.add(clazz.id, clazz);
                        } else {
                            final JavaClass clazz = getOrcreateCType(stack,schema,cdt.complexType,name);
                            dico.add(clazz.id, clazz);
                        }
                    }


                } else if (ele instanceof XSDGroup) {
                    final XSDGroup cdt = (XSDGroup) ele;
                    final JavaClass clazz = getOrCreateGroup(stack,schema,cdt);
//                    dico.add(clazz.id, clazz);

                } else if (ele instanceof XSDSimpleType) {
                    final XSDSimpleType cdt = (XSDSimpleType) ele;
                    //TODO

                } else if (ele instanceof XSDComplexType) {
                    final XSDComplexType cdt = (XSDComplexType) ele;
                    final JavaClass clazz = getOrcreateCType(stack,schema,cdt,null);
                    dico.add(clazz.id, clazz);

                } else if (ele instanceof XSDAttribute) {
                    //TODO

                } else if (ele instanceof XSDAttributeGroup) {
                    final XSDAttributeGroup cdt = (XSDAttributeGroup) ele;
                    //TODO

                } else {
                    throw new IOException("Unexpected : "+ ele.getClass());
                }
            }
        }

        return dico;
    }

    /**
     *
     * @param stack
     * @param name
     * @return [0] schema, [1] xsd object
     * @throws IOException
     */
    private static Object[] find(XSDStack stack, final FName name) throws IOException{
        Iterator ite = stack.getSchemas().getValues().createIterator();
        while (ite.hasNext()) {
            final XSDSchema schema = (XSDSchema) ite.next();
            Object obj = schema.getForName(name.getLocalPart());
            if (obj!=null) {
                return new Object[]{schema,obj};
            }
        }
        throw new IOException("Unable to find element for name : "+name);
    }

    private JavaProperty getOrcreateAtt(final XSDStack stack, final FName name) throws IOException{
        JavaProperty prop = (JavaProperty) attCache.getValue(name);
        if (prop!=null) return prop;
        Object[] obj = find(stack, name);
        if (obj!=null) {
            return getOrCreateAtt(stack, (XSDSchema) obj[0], (XSDAttribute) obj[1]);
        }
        throw new IOException("Unable to find element for name : "+name);
    }

    private JavaProperty getOrCreateAtt(final XSDStack stack, final XSDSchema schema, final XSDAttribute cdt) {
        final Chars ns = schema.targetNamespace;
        final FName name = cdt.name==null ? new FName(ns, cdt.id) : cdt.name;

        JavaProperty prop = (JavaProperty) attCache.getValue(name);
        if (prop!=null) return prop;

        prop = new JavaProperty();
        prop.metas.add(JavaScope.PUBLIC);
        prop.id = name.getLocalPart();
        prop.objclass = new JavaClass();
        prop.objclass.id = new Chars(Chars.class.getName());

        attCache.add(name, prop);
        return prop;
    }

    private Collection getOrcreateAttGroup(final XSDStack stack, final FName name) throws IOException{
        Collection props = (Collection) attCache.getValue(name);
        if (props!=null) return props;
        Object[] obj = find(stack, name);
        if (obj!=null) {
            return getOrCreateAttGroup(stack, (XSDSchema) obj[0], (XSDAttributeGroup) obj[1]);
        }
        throw new IOException("Unable to find element for name : "+name);
    }

    private Collection getOrCreateAttGroup(final XSDStack stack, final XSDSchema schema, final XSDAttributeGroup cdt) throws IOException{
        final Chars ns = schema.targetNamespace;
        final FName name = cdt.name==null ? new FName(ns, cdt.id) : cdt.name;

        Collection prop = (Collection) attCache.getValue(name);
        if (prop!=null) return prop;

        final Sequence col = new ArraySequence();

        //other attributes
        fillOtherAttributes(cdt.otherAttributes,col);
        //attrDecls
        fillAttrDecls(stack, schema, cdt.attrDecls, col);

        attCache.add(name, col);
        return col;
    }

    private void fillAttrDecls(XSDStack stack, XSDSchema schema, Sequence attrDecls, Collection buffer) throws IOException{
        final Iterator attIte = attrDecls.createIterator();
        while (attIte.hasNext()) {
            final Object obj = attIte.next();
            if (obj instanceof XSDAttribute) {
                final JavaProperty prop = getOrCreateAtt(stack, schema, (XSDAttribute) obj);
                buffer.add(prop);
            } else if (obj instanceof XSDAttributeGroup) {
                buffer.addAll(getOrCreateAttGroup(stack, schema, (XSDAttributeGroup) obj));
            } else {
                throw new IOException("Unexpected : "+obj);
            }
        }
    }

    private JavaClass getOrCreateGroup(final XSDStack stack, final XSDSchema schema, final XSDGroup cdt) throws IOException{
        final Chars ns = schema.targetNamespace;
        final FName name = cdt.name==null ? new FName(ns, cdt.id) : cdt.name;
        if (name==null) throw new IOException("No name for object : "+cdt);

        final JavaClass clazz = new JavaClass();
        clazz.id = toClassName(name);
        clazz.metas.add(JavaMetas.ABSTRACT);
        clazz.metas.add(JavaMetas.INTERFACE);
        ctypeCache.add(name, clazz);

        return clazz;
    }

    private JavaClass getOrcreateCType(final XSDStack stack, final FName name) throws IOException{
        JavaClass clazz = (JavaClass) ctypeCache.getValue(name);
        if (clazz!=null) return clazz;
        Object[] obj = find(stack, name);
        if (obj!=null) {
            return getOrcreateCType(stack, (XSDSchema) obj[0], (XSDComplexType) obj[1], null);
        }
        throw new IOException("Unable to find element for name : "+name);
    }

    private JavaClass getOrcreateCType(final XSDStack stack, final XSDSchema schema, final XSDComplexType cdt, FName parentName) throws IOException{
        final Chars ns = schema.targetNamespace;
        final FName name;
        if (cdt.name==null) {
            if (cdt.id==null) {
                name = parentName;
            } else {
                name = new FName(ns, cdt.id);
            }
        } else {
            name = cdt.name;
        }
        if (name==null) throw new IOException("No name for object : "+cdt);

        final JavaClass clazz = new JavaClass();
        clazz.id = toClassName(name);
        //clazz.metas.add(new DefaultAbstract());
        //clazz.metas.add(new JavaInterface());
        ctypeCache.add(name, clazz);

        //metas
        if (Boolean.TRUE.equals(cdt.abstrct)) {
            clazz.metas.add(JavaMetas.ABSTRACT);
        }

        //description
        Chars desc = findDescription(cdt);
        if (desc!=null) {
            clazz.metas.add(new JavaDocumentation(desc));
        }

        //default attributes
        fillAttrDecls(stack, schema, cdt.attrDecls, clazz.properties);

        //other attributes
        fillOtherAttributes(cdt.otherAttributes,clazz.properties);

        //loop on properties
        if (cdt.typeDefParticle instanceof XSDGroup) {
            fillGroup(stack, schema, (XSDGroup) cdt.typeDefParticle, clazz.properties);
        } else if (cdt.typeDefParticle!=null) {
            throw new IOException("Unexpected type : "+cdt.typeDefParticle);
        }

        //simple type properties
        if (cdt.simpleContent!=null) {

        }
        //complex type properties
        if (cdt.complexContent!=null) {
            fillOtherAttributes(cdt.complexContent.otherAttributes,clazz.properties);
            final XSDExtensionType ext = cdt.complexContent.extension;
            if (ext!=null) {
                if (ext.base!=null) {
                    //add parent declaration
                    JavaClass parent = getOrcreateCType(stack, ext.base);
                    if (parent!=null) {
                        clazz.parents.add(parent);
                    }
                }

                //default attributes
                fillAttrDecls(stack, schema, ext.attrDecls, clazz.properties);
                //other attributes
                fillOtherAttributes(ext.otherAttributes,clazz.properties);
                //loop on properties
                if (ext.typeDefParticle instanceof XSDGroup) {
                    fillGroup(stack, schema, (XSDGroup) cdt.typeDefParticle, clazz.properties);
                } else if (ext.typeDefParticle!=null) {
                    throw new IOException("Unexpected type : "+cdt.typeDefParticle);
                }
            }
        }

        return clazz;
    }

    private void fillGroup(final XSDStack stack, final XSDSchema schema,
            final XSDGroup group, final Collection properties) throws IOException{
        final int min = toNumber(group.minOccurs,1);
        final int max = toNumber(group.maxOccurs,1);

        //other attributes
        fillOtherAttributes(group.otherAttributes,properties);

        for (int i=0,n=group.particles.getSize();i<n;i++) {
            Object ele = group.particles.get(i);
            if (ele instanceof XSDElement) {

            } else if (ele instanceof XSDGroup) {

            } else if (ele instanceof XSDAll) {

            } else if (ele instanceof XSDChoice) {

            } else if (ele instanceof XSDAny) {

            } else {
                throw new IOException("Unexpected type : "+ele);
            }
        }

    }

    private void fillOtherAttributes(Dictionary otherAttributes, Collection buffer) {
        final Iterator otherIte = otherAttributes.getPairs().createIterator();
        while (otherIte.hasNext()) {
            final Pair pair = (Pair) otherIte.next();
            final FName aname = (FName) pair.getValue1();
            final Chars value = (Chars) pair.getValue2();
            //TODO
        }
    }

    private int toNumber(Chars minOccurs, int def) {
        if (minOccurs==null) return def;

        if (XSDConstants.VAL_UNBOUNDED.equals(minOccurs, true, true)) {
            return Integer.MAX_VALUE;
        }

        return Int32.decode(minOccurs);
    }

    private static Chars findDescription(XSDAnnotated pelement) {
        //find description if any
        if (pelement.annotation!=null) {
            final Iterator ite = pelement.annotation.choice.createIterator();
            while (ite.hasNext()) {
                Object next = ite.next();
                if (next instanceof XSDDocumentation) {
                    return ((XSDDocumentation) next).text;
                }
            }
        }
        return null;
    }

    private Chars toClassName(FName name) throws IOException{
        if (name.getNamespace()==null) throw new IOException("No namespace : "+name);
        if (name.getLocalPart()==null) throw new IOException("No name : "+name);
        return toClassName(name.getNamespace(), name.getLocalPart());
    }

    private Chars toClassName(Chars ns, Chars name) {
        final Chars pack = ns.replaceAll(new Chars("http://"), new Chars(""));
        final Chars[] split = pack.split('/');
        final CharBuffer cb = new CharBuffer(CharEncodings.UTF_8);
        for (int i=0;i<split.length;i++) {
            if (i>0) {
                cb.append('.');
            }
            if (Char.isDigit(split[i].getCharacter(0).toUnicode())) {
                cb.append('_');
            }
            cb.append(split[i]);
        }
        cb.append('.');

        //force upper case for at least first letter
        name = name.replaceAll('-', '_');
        //yes 'dot', it happens like in MathML
        name = name.replaceAll('.', '_');
        cb.append(Languages.UNSET.toUpperCase(name.getCharacter(0).toUnicode()));
        cb.append(name.truncate(1, name.getCharLength()));

        return cb.toChars();
    }



    private JavaClass toClass(NodeType type, Dictionary dico) throws IOException{
        JavaClass clazz = (JavaClass) dico.getValue(type.getId());
        if (clazz!=null) {
            //clazz has already been generated
            return clazz;
        }

        //create a proper short name
        final Chars name = type.getId();
        final int index = name.getLastOccurence(':');
        Chars shortName = name.truncate(index+1, name.getCharLength());
        Chars ns = name.truncate(0, index);
        shortName = cleanName(shortName);

        //find package for namespace
        final Chars pack = (Chars) nsToPackage.getValue(ns);
        System.out.println(ns+"   "+pack);
        if (pack==null) {
            throw new IOException("package has not been defined for namespace : "+ns +"("+name+")");
        }

        clazz = new JavaClass();
        clazz.id = pack.concat('.').concat(shortName);
        clazz.metas.add(new JavaDocumentation(type.getDescription()));
        clazz.metas.add(JavaScope.PUBLIC);
        //put it in the dictionary to avoid recursive construction
        dico.add(type.getId(), clazz);

        if (type.isComplexe()) {
            final NodeCardinality[] children = type.getChildrenTypes();
            for (int i=0; i<children.length; i++) {
                final NodeCardinality nc = children[i];
                JavaClass objClass = null;
                Class primClass = null;
                if (nc.getType().isComplexe()) {
                    objClass = toClass(nc.getType(), dico);
                } else {
                    //primitive type
                    primClass = nc.getType().getValueClass();
                    if (primClass == Sequence.class) {
                        objClass = GeneratorConstants.CLASS_SEQUENCE;
                        primClass = null;
                    } else if (primClass == Collection.class) {
                        objClass = GeneratorConstants.CLASS_COLLECTION;
                        primClass = null;
                    }
                }

                final JavaProperty prop = new JavaProperty();
                prop.id = nc.getId();
                prop.metas.add(new JavaDocumentation(nc.getDescription()));
                prop.metas.add(JavaScope.PUBLIC);

                if (nc.getMaxOccurences()>1) {
                    prop.objclass = GeneratorConstants.CLASS_SEQUENCE;
                } else {
                    prop.objclass = objClass;
                    prop.primclass = primClass;
                }

                clazz.properties.add(prop);
            }
        }

        return clazz;
    }

    private static Chars cleanName(Chars shortName) {
        //remove the possible 'type' end
        if (shortName.toLowerCase().endsWith(new Chars("type"))) {
            shortName = shortName.truncate(0, shortName.getCharLength()-4);
        }
        if (   shortName.endsWith(new Char('.'))
           || shortName.endsWith(new Char('-'))
           || shortName.endsWith(new Char('_'))) {
            shortName = shortName.truncate(0, shortName.getCharLength()-1);
        }

        //camel case pour all other special chars
        while (true) {
            int index = shortName.getFirstOccurence('.');
            if (index<0) index = shortName.getFirstOccurence('-');
            if (index<0) index = shortName.getFirstOccurence('_');
            if (index<0) index = shortName.getFirstOccurence(' ');
            if (index<0) break;

            Chars rightUpper = shortName.truncate(index+2, shortName.getCharLength());
            rightUpper = shortName.truncate(index+1, index+2).toUpperCase().concat(rightUpper);

            shortName = shortName.truncate(0, index).concat(rightUpper);
        }

        //first letter to upper case
        shortName = shortName.truncate(0, 1).toUpperCase().concat(shortName.truncate(1, shortName.getCharLength()));
        return shortName;
    }
}
