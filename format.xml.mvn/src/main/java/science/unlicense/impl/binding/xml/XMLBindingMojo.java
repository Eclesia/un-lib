
package science.unlicense.impl.binding.xml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import science.unlicense.code.java.JavaWriter;
import science.unlicense.code.java.model.JavaClass;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.regex.Regex;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 * automatically generate classes from an XSD schema.
 * Integrated in maven build process.
 *
 * TODO remove maven some day, make an ant/ivy/maven replacement.
 *
 * @author Johann Sorel
 */
@Mojo(name = "xmlbinding", defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresProject = true)
public class XMLBindingMojo extends AbstractMojo {

    public static final JavaClass SEQUENCE_CLASS = new JavaClass();
    public static final JavaClass COLLECTION_CLASS = new JavaClass();
    static {
        science.unlicense.runtime.Runtime.init();
        SEQUENCE_CLASS.id = new Chars("science.unlicense.common.api.collection.Sequence");
        COLLECTION_CLASS.id = new Chars("science.unlicense.common.api.collection.Collection");
    }

    @Parameter(property = "project", required = true, readonly = true)
    private MavenProject project;

    /**
     * Path to xsd file.
     */
    @Parameter(property = "xsdPath", required = true, readonly = true)
    protected String xsdPath;

    /**
     * Folder where to store generated code.
     */
    @Parameter(property = "sourcePath", required = true, readonly = true)
    protected String sourcePath;

    /**
     * Class package of generated classes.
     * Pattern : Namespace=package
     */
    @Parameter(property = "classPackages", required = true, readonly = true)
    protected List classPackages = new ArrayList();

    /**
     * Class package of reader/writer classes.
     * Pattern : package
     */
    @Parameter(property = "ioPackage", required = true, readonly = true)
    protected String ioPackage;

    /**
     * Generated classes type :
     * POJO or DOM
     */
    @Parameter(property = "classType", required = true, readonly = true, defaultValue = "POJO")
    protected String classType;


    @Override
    public void execute() throws MojoExecutionException {
        if (File.separator.equals("\\")) {
            //windows hack
            xsdPath = xsdPath.replace('/', '\\');
            sourcePath = sourcePath.replace('/', '\\');
        }
        project.addCompileSourceRoot(sourcePath);


        final Path xsdfile = Paths.resolve(new Chars(xsdPath));
        final Path targetFolder = Paths.resolve(new Chars(sourcePath));
        final Dictionary nsToPackage = new HashDictionary();

        //generate the java package paths
//        for (int i=0,n=classPackages.size();i<n;i++) {
//            final String entry = (String) classPackages.get(i);
//            final String ns = entry.split("=")[0];
//            final String pac = entry.split("=")[1];
//
//            //build the output folder
//            Path outFolder = targetFolder;
//            final String[] parts = pac.split("\\.");
//            try{
//                outFolder.createContainer();
//                for (String part : parts) {
//                    outFolder = outFolder.resolve(part);
//                    outFolder.createContainer();
//                }
//            }catch(IOException ex) {
//                ex.printStackTrace();
//                throw new MojoExecutionException(ex.getMessage(), ex);
//            }
//            nsToPackage.add(new Chars(ns), new Chars(pac));
//            packageToFolder.add(new Chars(pac), outFolder);
//        }

        try {
            //clean output folder
            Iterator ite = targetFolder.getChildren().createIterator();
            while (ite.hasNext()) {
                Path n = (Path) ite.next();
                n.delete();
            }

            //generate classes
            final ClassGenerator classGenerator = new ClassGenerator(xsdfile, nsToPackage, classType);
            final Collection classes = classGenerator.generate().getValues();

            //write all explored types
            final Iterator classite = classes.createIterator();
            while (classite.hasNext()) {
                final JavaClass clazz = (JavaClass) classite.next();
                final Path output = getOutputPath(targetFolder, clazz);
                System.out.println(clazz.getShortName().toString() +"   "+output);
                writeClass(clazz, output);
            }

            //create reader class
            final XMLReaderGenerator readerGenerator = new XMLReaderGenerator();
            final JavaClass readerClazz = readerGenerator.create(new Chars(ioPackage+"Reader"), classes);
            final Path readerOutput = getOutputPath(targetFolder, readerClazz);
            writeClass(readerClazz, readerOutput);

            //create writer class
            final XMLWriterGenerator writerGenerator = new XMLWriterGenerator();
            final JavaClass writerClazz = writerGenerator.create(new Chars(ioPackage+"Writer"), classes);
            final Path writerOutput = getOutputPath(targetFolder, writerClazz);
            writeClass(writerClazz, writerOutput);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new MojoExecutionException("Failed to generate classes."+ex.getMessage(),ex);
        }

    }

    private static Path getOutputPath(final Path targetFolder, JavaClass clazz) throws IOException{
        final Chars pack = clazz.getPackage();
        Path outFolder = targetFolder;
        outFolder.createContainer();
        final Chars[] parts = Regex.split(pack, new Chars("\\."));
        for (Chars part : parts) {
            outFolder = outFolder.resolve(part);
            outFolder.createContainer();
        }
        return outFolder;
    }

    private void writeClass(JavaClass clazz, Path targetFolder) throws IOException{
        final JavaWriter writer = new JavaWriter();
        writer.setOutput(targetFolder.resolve(clazz.getShortName().concat(new Chars(".java"))));
        writer.write(clazz);
        writer.dispose();
    }

}
