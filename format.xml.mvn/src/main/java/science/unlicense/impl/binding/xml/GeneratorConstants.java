
package science.unlicense.impl.binding.xml;

import science.unlicense.common.api.character.Chars;
import science.unlicense.code.java.model.JavaClass;

/**
 *
 * @author Johann Sorel
 */
final class GeneratorConstants {

    static final JavaClass CLASS_ABSTRACTREADER = new JavaClass();
    static final JavaClass CLASS_ABSTRACTWRITER = new JavaClass();
    static final JavaClass CLASS_OBJECT = new JavaClass();
    static final JavaClass CLASS_IOEXCEPTION = new JavaClass();
    static final JavaClass CLASS_SEQUENCE = new JavaClass();
    static final JavaClass CLASS_COLLECTION = new JavaClass();
    static {
        CLASS_ABSTRACTREADER.id = new Chars("science.unlicense.encoding.api.io.AbstractReader");
        CLASS_ABSTRACTWRITER.id = new Chars("science.unlicense.encoding.api.io.AbstractWriter");
        CLASS_OBJECT.id = new Chars("java.lang.Object");
        CLASS_IOEXCEPTION.id = new Chars("science.unlicense.encoding.api.io.IOException");
        CLASS_SEQUENCE.id = new Chars("science.unlicense.common.api.collection.Sequence");
        CLASS_COLLECTION.id = new Chars("science.unlicense.common.api.collection.Collection");
    }

    private GeneratorConstants() {}

}
