
package science.unlicense.impl.binding.xml;

import science.unlicense.code.api.meta.DefaultMeta;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class XMLMeta extends DefaultMeta {

    private static final Chars ID = Chars.constant("XML");

    public XMLMeta() {
        super(ID);
    }

}
