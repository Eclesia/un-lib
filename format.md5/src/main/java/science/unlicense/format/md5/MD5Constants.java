
package science.unlicense.format.md5;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class MD5Constants {

    public static final Chars MD5_VERSION = Chars.constant("MD5Version");
    public static final Chars COMMAND_LINE = Chars.constant("commandline");
    public static final Chars NUM_JOINTS = Chars.constant("numJoints");

    //Mesh constants
    public static final Chars NUM_MESHES = Chars.constant("numMeshes");
    public static final Chars JOINTS = Chars.constant("joints");
    public static final Chars MESH = Chars.constant("mesh");
    public static final Chars SHADER = Chars.constant("shader");
    public static final Chars VERTEX = Chars.constant("vert");
    public static final Chars NUM_VERTEX = Chars.constant("numverts");
    public static final Chars NUM_TRIANGLE = Chars.constant("numtris");
    public static final Chars TRIANGLE = Chars.constant("tri");
    public static final Chars NUM_WEIGHTS = Chars.constant("numweights");
    public static final Chars WEIGHT = Chars.constant("weight");

    //Anim constants
    public static final Chars NUM_FRAMES = Chars.constant("numFrames");
    public static final Chars FRAME_RATE = Chars.constant("frameRate");
    public static final Chars NUM_ANIMATEDCOMP = Chars.constant("numAnimatedComponents");
    public static final Chars HIERARCHY = Chars.constant("hierarchy");
    public static final Chars BOUNDS = Chars.constant("bounds");
    public static final Chars BASEFRAME = Chars.constant("baseframe");
    public static final Chars FRAME = Chars.constant("frame");



    private MD5Constants() {}

}
