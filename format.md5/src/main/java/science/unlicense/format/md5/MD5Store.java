
package science.unlicense.format.md5;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.display.impl.updater.AnimationUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.impl.SimilarityNd;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.model3d.impl.physic.JointTimeSerie;
import science.unlicense.model3d.impl.physic.SkeletonAnimation;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public class MD5Store extends AbstractModel3DStore{

    private MD5Mesh mesh;
    private MD5Animation anim;

    public MD5Store(Object input) {
        super(MD5Format.INSTANCE,input);
    }

    public Collection getElements() {

        if (mesh==null){
            readModel();
        }

        //rebuild the skeleton
        final Skeleton skeleton = new Skeleton();
        for (int i=0;i<mesh.joints.length;i++){
            final MD5Mesh.Joint meshjoint = mesh.joints[i];
            final Joint joint = new Joint(3,meshjoint.name);
            joint.setId(i);
            joint.getNodeTransform().getRotation().set(meshjoint.orientation.toMatrix3());
            joint.getNodeTransform().getTranslation().set(meshjoint.position);
            joint.getNodeTransform().notifyChanged();
            if (meshjoint.parent>=0){
                final Chars parentName = mesh.joints[meshjoint.parent].name;
                final Joint parent = skeleton.getJointByTitle(parentName);
                parent.getChildren().add(joint);
            } else {
                //root joint
                skeleton.getChildren().add(joint);
            }
        }

        //rebuild mesh
        final MotionModel root = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);

        try{
            for (int m=0;m<mesh.meshes.length;m++){
                final MD5Mesh.Mesh part = mesh.meshes[m];
                final DefaultModel node = buildMeshPart(part, skeleton);
                root.getChildren().add(node);
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }

        //rebuild animation
        skeleton.reverseWorldPose();
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();
        final SkeletonAnimation glanim = buildAnimation(skeleton);
        root.getUpdaters().add(new AnimationUpdater(glanim));
        root.setSkeleton(skeleton);

        final Collection col = new ArraySequence();
        col.add(root);
        return col;
    }

    private void readModel(){
        final MD5MeshReader meshReader = new MD5MeshReader();
        try{
            meshReader.setInput(source);
            mesh = meshReader.read();
        }catch(IOException ex){
            ex.printStackTrace();
        }

        final MD5AnimationReader animReader = new MD5AnimationReader();
        try{
            final Path pi = ((Path) source);
            Path p = pi.getParent().resolve(pi.getName().replaceAll(new Chars("md5mesh"), new Chars("md5anim")));
            animReader.setInput(p);
            anim = animReader.read();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    private DefaultModel buildMeshPart(final MD5Mesh.Mesh part, Skeleton skeleton) throws IOException{
        final DefaultModel glmesh = new DefaultModel();
        glmesh.getTechniques().add(new SimpleBlinnPhong());

        final Float32Cursor vertexBuffer = DefaultBufferFactory.INSTANCE.createFloat32(part.vertices.length*3).cursor();
        final Float32Cursor normalBuffer = DefaultBufferFactory.INSTANCE.createFloat32(part.vertices.length*3).cursor();
        final Float32Cursor uvBuffer = DefaultBufferFactory.INSTANCE.createFloat32(part.vertices.length*2).cursor();
        final Int32Cursor indexBuffer = DefaultBufferFactory.INSTANCE.createInt32(part.triangles.length*3).cursor();

        int maxweights = 0;
        //calculate vertex positions
        for (int i=0;i<part.vertices.length;i++){
            final MD5Mesh.Vertice mv = part.vertices[i];
            mv.position = VectorNf64.createDouble(3);
            mv.normal = VectorNf64.createDouble(3);
            maxweights = Maths.max(maxweights,mv.weightCount);

            for (int w=0;w<mv.weightCount;w++){
                final MD5Mesh.Weight weight = part.weights[mv.weightStart+w];
                final MD5Mesh.Joint joint = mesh.joints[weight.jointIndex];
                //use weigth to calculate vertex position
                final VectorRW rotpos = joint.orientation.rotate(weight.weightPosition,null);
                final VectorRW sumpos = joint.position.add(rotpos, null).localScale(weight.weightBias);
                mv.position.localAdd(sumpos);
            }

            vertexBuffer.write(mv.position.toFloat());
            uvBuffer.write(mv.textureUV.toFloat());
        }
        //we must have at least 2 weights
        maxweights = Maths.max(2, maxweights);

        //calculate normals
        for (int i=0;i<part.triangles.length;i++){
            final MD5Mesh.Triangle triangle = part.triangles[i];
            final VectorRW v0 = part.vertices[triangle.verticeIndexes[0]].position;
            final VectorRW v1 = part.vertices[triangle.verticeIndexes[1]].position;
            final VectorRW v2 = part.vertices[triangle.verticeIndexes[2]].position;
            final VectorRW v2v0 = v2.subtract(v0, null);
            final VectorRW v1v0 = v1.subtract(v0, null);
            final VectorRW normal = v2v0.cross(v1v0, null);
            part.vertices[triangle.verticeIndexes[0]].normal.localAdd(normal);
            part.vertices[triangle.verticeIndexes[1]].normal.localAdd(normal);
            part.vertices[triangle.verticeIndexes[2]].normal.localAdd(normal);
            indexBuffer.write(triangle.verticeIndexes);
        }

        //normalize normals
        for (int i=0;i<part.vertices.length;i++){
            final MD5Mesh.Vertice mv = part.vertices[i];
            final VectorRW normal = part.vertices[i].normal.normalize(null);
            part.vertices[i].normal.setAll(0.0);

            //set the bind pose normal
            for (int w=0;w<mv.weightCount;w++){
                final MD5Mesh.Weight weight = part.weights[mv.weightStart+w];
                final MD5Mesh.Joint joint = mesh.joints[weight.jointIndex];
                final VectorRW temp = joint.orientation.rotate(normal, null).localScale(weight.weightBias);
                part.vertices[i].normal.localAdd(temp);
            }

            normalBuffer.write(normal.toFloat());
        }

        //load texture
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        final Path pi = ((Path) source);
        final Image image = Images.read(pi.getParent().resolve(part.shader));
        material.setDiffuseTexture(new TextureMapping(new Texture2D(image)));
        glmesh.getMaterials().add(material);

        //build the skin
        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        final Skin skin = new Skin();
        final Float32Cursor weightBuffer = DefaultBufferFactory.INSTANCE.createFloat32(part.vertices.length*maxweights).cursor();
        final Int32Cursor jointBuffer = DefaultBufferFactory.INSTANCE.createInt32(part.vertices.length*maxweights).cursor();
        for (int i=0;i<part.vertices.length;i++){
            final MD5Mesh.Vertice mv = part.vertices[i];

            int w=0;
            for (;w<mv.weightCount;w++){
                final MD5Mesh.Weight weight = part.weights[mv.weightStart+w];
                weightBuffer.write(weight.weightBias);
                jointBuffer.write(weight.jointIndex);
            }
            //fill remaining weigths with zeros
            for (;w<maxweights;w++){
                weightBuffer.write(0f);
                jointBuffer.write(0);
            }
        }

        skin.setSkeleton(skeleton);
        skin.setMaxWeightPerVertex(maxweights);
        shell.setPositions(new VBO(vertexBuffer.getBuffer(),3));
        shell.setNormals(new VBO(normalBuffer.getBuffer(),3));
        shell.setUVs(new VBO(uvBuffer.getBuffer(), 2));
        shell.getAttributes().add(Skin.ATT_WEIGHTS_0, new VBO(weightBuffer.getBuffer(),maxweights));
        shell.getAttributes().add(Skin.ATT_JOINTS_0, new VBO(jointBuffer.getBuffer(),maxweights));
        shell.setIndex(new IBO(indexBuffer.getBuffer()));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) indexBuffer.getBuffer().getNumbersSize())});

        //build mesh object
//        final DirectShell shell = new DirectShell();
//        shell.setVertexBuffer(vertexBuffer);
//        shell.setNormalBuffer(normalBuffer);
//        shell.setIndexBuffer(indexBuffer);
//        glmesh.setShell(shell);
        glmesh.setShape(shell);
        glmesh.setSkin(skin);
        glmesh.getShape().getBoundingBox();

        return glmesh;
    }

    private SkeletonAnimation buildAnimation(final Skeleton meshSkeleton){
        final SkeletonAnimation glanim = new SkeletonAnimation(meshSkeleton);

        //build the base skeleton
        for (int i=0;i<anim.joints.length;i++){
            final MD5Animation.Joint animJoint = anim.joints[i];
            final Joint gljoint = meshSkeleton.getJointByTitle(animJoint.name);
            gljoint.getNodeTransform().getRotation().set(anim.baseFrame.orientation[i].toMatrix3());
            gljoint.getNodeTransform().getTranslation().set(anim.baseFrame.position[i]);
            gljoint.getNodeTransform().notifyChanged();
        }

        //build each joint serie
        for (int i=0;i<anim.joints.length;i++){
            final MD5Animation.Joint animJoint = anim.joints[i];
            final Joint gljoint = meshSkeleton.getJointByTitle(animJoint.name);

            //create a time serie for each joint
            final JointTimeSerie serie = new JointTimeSerie(gljoint);
            glanim.getSeries().add(serie);

            //create each frame
            for (int k=0;k<anim.frames.length;k++){
                final MD5Animation.Frame frame = anim.frames[k];
                final JointKeyFrame glframe = new JointKeyFrame();
                glframe.setValue(SimilarityNd.create(3));
                glframe.setTime((int) ( (float) k/anim.frameRate*1000));
                glframe.setJoint(gljoint);
                final SimilarityRW poseTrs = glframe.getValue();
                poseTrs.getTranslation().set(anim.baseFrame.position[i]);

                final Quaternion q = new Quaternion();

                int offset = 0;
                if ((animJoint.flags &  1) != 0) poseTrs.getTranslation().setX(frame.data[animJoint.startIndex+offset++]);
                if ((animJoint.flags &  2) != 0) poseTrs.getTranslation().setY(frame.data[animJoint.startIndex+offset++]);
                if ((animJoint.flags &  4) != 0) poseTrs.getTranslation().setZ(frame.data[animJoint.startIndex+offset++]);
                if ((animJoint.flags &  8) != 0) q.setX(frame.data[animJoint.startIndex+offset++]);
                if ((animJoint.flags & 16) != 0) q.setY(frame.data[animJoint.startIndex+offset++]);
                if ((animJoint.flags & 32) != 0) q.setZ(frame.data[animJoint.startIndex+offset++]);
                MD5Utilities.rebuildQuaternionW(q);

                poseTrs.getRotation().set(q.toMatrix3());
                serie.getFrames().add(glframe);
                poseTrs.notifyChanged();
            }
        }

        return glanim;
    }

}
