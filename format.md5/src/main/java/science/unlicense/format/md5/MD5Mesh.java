
package science.unlicense.format.md5;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.api.VectorRW;

/**
 * MD5 mesh.
 *
 * @author Johann Sorel
 */
public class MD5Mesh {

    public int version;
    public Chars commandline;
    /**
     * Skeleton joints.
     */
    public Joint[] joints;
    /**
     * Mesh part, one by texture.
     */
    public Mesh[] meshes;

    public static final class Joint{
        public Chars name;
        public int parent;
        public VectorRW position;
        public Quaternion orientation;
    }

    public static final class Mesh{
        public Chars shader;
        public Vertice[] vertices;
        public Triangle[] triangles;
        public Weight[] weights;
    }

    public static final class Vertice{
        public int index;
        public VectorRW textureUV; //size 2
        public int weightStart;
        public int weightCount;

        //calculated field
        public VectorRW position;
        public VectorRW normal;
    }

    public static final class Triangle{
        public int index;
        public int[] verticeIndexes; //size 3
    }

    public static final class Weight{
        public int index;
        public int jointIndex;
        /** Bias is the sum of all weights ,it should be 1 for a vertex */
        public float weightBias;
        public VectorRW weightPosition; //size 3
    }

}
