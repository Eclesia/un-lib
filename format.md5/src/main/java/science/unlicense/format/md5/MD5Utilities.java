
package science.unlicense.format.md5;

import science.unlicense.math.impl.Quaternion;

/**
 *
 * @author Johann Sorel
 */
public final class MD5Utilities {

    public static void rebuildQuaternionW(Quaternion q){
        final double x = q.get(0);
        final double y = q.get(1);
        final double z = q.get(2);
        final double t = 1.0 - (x*x) - (y*y) - (z*z);
        final double w = (t<0)? 0 : -Math.sqrt(t);
        q.setW(w);
    }

    private MD5Utilities(){}

}
