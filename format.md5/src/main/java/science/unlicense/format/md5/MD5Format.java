
package science.unlicense.format.md5;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class MD5Format extends AbstractModel3DFormat{

    public static final MD5Format INSTANCE = new MD5Format();

    private MD5Format() {
        super(new Chars("MD5"));
        shortName = new Chars("MD5");
        longName = new Chars("MD5");
        extensions.add(new Chars("md5mesh"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new MD5Store(input);
    }

}
