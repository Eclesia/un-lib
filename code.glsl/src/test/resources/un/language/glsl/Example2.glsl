#version 330
layout (location = 0) out vec4 f_out0;

struct MaterialInfo {
    //entry parameters
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
    //result after lighting
    vec3 totalAmbient;
    vec3 totalDiffuse;
    vec3 totalSpecular;
    vec4 color;
};

uniform vec4 matlayer0_value;


in VsDataModel{
    flat mat4 V;
    vec2 uv;
    vec4 normal_camera;
    vec4 normal_model;
    vec4 normal_proj;
    vec4 normal_world;
    vec4 position_camera;
    vec4 position_model;
    vec4 position_proj;
    vec4 position_world;
} vsData;

MaterialInfo material = MaterialInfo(vec4(-1,-1,-1,-1),vec4(-1,-1,-1,-1),vec4(-1,-1,-1,-1),5.0,vec3(0,0,0),vec3(0,0,0),vec3(0,0,0),vec4(0,0,0,0));
mat4 V;
vec3 normal = vec3(99,99,99);


void main(){
    V = vsData.V;
    if(material.color.x<0){ material.color = matlayer0_value; }
    else{             material.color += matlayer0_value; }
    if(normal.x == 99){
        normal = normalize(vsData.normal_camera.xyz);
    }
    float alpha = material.color.w;
    //skip transparent and nearly transparent fragments
    if(alpha < 0.01) discard;
    if(material.ambient.x < 0){
        //use diffuse as ambient
        material.ambient = material.color;
    }
    if(material.specular.x < 0){
        //use diffuse as specular
        material.specular = material.color;
    }
    f_out0 = material.color;
}