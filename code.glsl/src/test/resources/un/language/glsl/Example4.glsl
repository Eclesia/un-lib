#version 330
layout (location = 0) out vec4 f_out0;


uniform mat3 MV;
uniform mat3 P;
uniform int NBSTEP;
uniform float GSTEPS[32];
uniform vec4 GCOLORS[32];
uniform vec2 GSTART;
uniform vec2 GEND;


in VsDataModel{
    vec4 position_model;
    vec4 position_proj;
} vsData;

vec4 color;

float distance(vec2 c){
    vec3 vstart = MV*vec3(GSTART,1);
    vec3 vend = MV*vec3(GEND,1);
    float adj = vend.x - vstart.x;
    float opp = vend.y - vstart.y;
    float hyp = sqrt(adj*adj + opp*opp);
    return ( (vstart.y - c.y)*(vstart.y - vend.y) - (vstart.x - c.x)*(vend.x - vstart.x) ) / (hyp*hyp) ;
}
vec4 interpolate(vec4 start, vec4 end, float ratio){
    return vec4(
        start.x + ratio*(end.x - start.x),
        start.y + ratio*(end.y - start.y),
        start.z + ratio*(end.z - start.z),
        start.w + ratio*(end.w - start.w)
        );
}

void main(){
    float d = distance(gl_FragCoord.xy);
    if(d<0) d=0;
    if(d>1) d=1;
    //find interval
    color = vec4(0);
    for(int index=0;index<NBSTEP-1;index++){
        if(d >= GSTEPS[index] && d <= GSTEPS[index+1]){
            d = (d-GSTEPS[index]) / (GSTEPS[index+1]-GSTEPS[index]);
            vec4 c0 = GCOLORS[index];
            vec4 c1 = GCOLORS[index+1];
            color = interpolate(c0, c1, d);
            break;
        }
    }
    f_out0 = color;
}