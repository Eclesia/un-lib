#version 330

layout(location = 0) in vec2 l_position;
layout(lines) in;
layout(triangle_strip, max_vertices = 4) out;
