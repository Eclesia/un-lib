
package science.unlicense.code.glsl;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.parser.SyntaxNode;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class GLSLReaderTest {

    @Test
    public void testReadEmptyProgram() throws IOException{

        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/EmptyProgram.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        Assert.assertEquals(new Chars(
                "program\n" +
                "├─ version\n" +
                "│  └─ NUMBER[0:9,0:12]='330'\n" +
                "└─ function\n" +
                "   ├─ VOID[2:0,2:4]='void'\n" +
                "   ├─ WORD[2:5,2:9]='main'\n" +
                "   └─ statements"),
                Nodes.toCharsTree(node, 10));
    }

    @Test
    public void testReadExpression() throws IOException{

        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/ExpressionProgram.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(Nodes.toCharsTree(node, Chars.EMPTY, 20));
    }

    @Test
    public void testReadBranching() throws IOException{

        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/branching.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(node);
    }

    @Test
    public void testReadLayout() throws IOException{

        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/layout.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(node);
    }

    @Test
    public void testReadExample() throws IOException{

        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/Example.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(node);
    }

    @Test
    public void testReadExample2() throws IOException{
        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/Example2.glsl")));
        final SyntaxNode node = reader.readAST();
        System.out.println(node);
    }

    @Test
    public void testReadExample3() throws IOException{
        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/Example3.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(node);

    }

    @Test
    public void testReadExample4() throws IOException{
        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/Example4.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(node);
    }

}
