
package science.unlicense.code.glsl;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.code.glsl.model.GLSLProgram;
import science.unlicense.code.glsl.model.GLSLShader;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenGroup;
import science.unlicense.syntax.api.lexer.TokenType;
import science.unlicense.syntax.api.parser.Parser;
import science.unlicense.syntax.api.parser.Rule;
import science.unlicense.syntax.api.parser.SyntaxNode;
import science.unlicense.syntax.impl.grammar.io.UNGrammarReader;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class GLSLReader extends AbstractReader {

    private static final TokenGroup TOKENS;
    private static final OrderedHashDictionary RULES = new OrderedHashDictionary();
    private static final Rule RULE_PROGRAM;
    public static final Predicate TRIM;
    static {
        try{
            //parse grammar
            final UNGrammarReader reader = new UNGrammarReader();
            reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/glsl/glsl.gr")));
            HashDictionary groups = new HashDictionary();
            reader.read(groups, RULES);
            RULE_PROGRAM = (Rule) RULES.getValue(new Chars("program"));

            TOKENS = (TokenGroup) groups.getValues().createIterator().next();

            final TokenType[] toExclude = new TokenType[]{
                (TokenType) TOKENS.getValue(new Chars("WS")),
                (TokenType) TOKENS.getValue(new Chars("SEMI")),
                (TokenType) TOKENS.getValue(new Chars("COM")),
                (TokenType) TOKENS.getValue(new Chars("LP")),
                (TokenType) TOKENS.getValue(new Chars("RP")),
                (TokenType) TOKENS.getValue(new Chars("LB")),
                (TokenType) TOKENS.getValue(new Chars("RB")),
                (TokenType) TOKENS.getValue(new Chars("LA")),
                (TokenType) TOKENS.getValue(new Chars("RA")),
                (TokenType) TOKENS.getValue(new Chars("VERSION")),
                (TokenType) TOKENS.getValue(new Chars("EXTENSION")),
                (TokenType) TOKENS.getValue(new Chars("RETURN"))
            };

            TRIM = new Predicate() {
                public Boolean evaluate(Object candidate) {
                    final SyntaxNode sn = (SyntaxNode) candidate;
                    final Token token = sn.getToken();
                    return token!=null && Arrays.containsIdentity(toExclude, 0, toExclude.length, token.type);
                }
            };

        }catch(IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public SyntaxNode readAST() throws IOException{
        final ByteInputStream in = getInputAsByteStream();

        //prepare lexer
        final Lexer lexer = new Lexer(CharEncodings.UTF_8);
        lexer.setInput(in);

        final Parser parser = new Parser(RULE_PROGRAM);
        parser.setInput(lexer);
        return parser.parse();
    }

    public GLSLShader readShader() throws IOException{
        final SyntaxNode ast = readAST();
        final GLSLShader shader = new GLSLShader();
        convertShader(ast, shader);
        return shader;
    }

    private static void convertShader(SyntaxNode ast, GLSLShader shader) {
        //TODO
    }


    public static GLSLProgram load(Object vertexPath, Object tessControlPath,
            Object tessEvalPath, Object geometryPath, Object fragmentPath) throws IOException {

        final GLSLReader reader = new GLSLReader();
        final GLSLProgram program = new GLSLProgram();

        if (vertexPath!=null) {
            reader.setInput(vertexPath);
            program.setVertexShader(reader.readShader());
            reader.dispose();
        }
        if (tessControlPath!=null) {
            reader.setInput(tessControlPath);
            program.setTessControlShader(reader.readShader());
            reader.dispose();
        }
        if (tessEvalPath!=null) {
            reader.setInput(tessEvalPath);
            program.setTessEvalShader(reader.readShader());
            reader.dispose();
        }
        if (geometryPath!=null) {
            reader.setInput(geometryPath);
            program.setGeometryShader(reader.readShader());
            reader.dispose();
        }
        if (fragmentPath!=null) {
            reader.setInput(fragmentPath);
            program.setFragmentShader(reader.readShader());
            reader.dispose();
        }

        return program;
    }

    public static GLSLProgram load(Chars vertexText, Chars tessControlText,
            Chars tessEvalText, Chars geometryText, Chars fragmentText) throws IOException {
        return load(
                vertexText!=null ? new ArrayInputStream(vertexText.toBytes()) : null,
                tessControlText!=null ? new ArrayInputStream(tessControlText.toBytes()) : null,
                tessEvalText!=null ? new ArrayInputStream(tessEvalText.toBytes()) : null,
                geometryText!=null ? new ArrayInputStream(geometryText.toBytes()) : null,
                fragmentText!=null ? new ArrayInputStream(fragmentText.toBytes()) : null);
    }


}
