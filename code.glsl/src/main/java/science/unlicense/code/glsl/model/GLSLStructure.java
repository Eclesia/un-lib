
package science.unlicense.code.glsl.model;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class GLSLStructure {

    private Chars comment;

    public Chars getComment() {
        return comment;
    }

    public void setComment(Chars comment) {
        this.comment = comment;
    }

}
