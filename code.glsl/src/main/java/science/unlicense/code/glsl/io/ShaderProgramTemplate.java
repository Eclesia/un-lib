
package science.unlicense.code.glsl.io;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.math.api.Maths;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class ShaderProgramTemplate {

    private static final Chars DELIM_VERTEX_SHADER      = Chars.constant("//@VERTEX SHADER");
    private static final Chars DELIM_TESSCONTROL_SHADER = Chars.constant("//@TESSCONTROL SHADER");
    private static final Chars DELIM_TESSEVAL_SHADER    = Chars.constant("//@TESSEVAL SHADER");
    private static final Chars DELIM_GEOMETRY_SHADER    = Chars.constant("//@GEOMETRY SHADER");
    private static final Chars DELIM_FRAGMENT_SHADER    = Chars.constant("//@FRAGMENT SHADER");


    private final ShaderTemplate vs;
    private final ShaderTemplate tc;
    private final ShaderTemplate te;
    private final ShaderTemplate ge;
    private final ShaderTemplate fr;

    public ShaderProgramTemplate() {
        this(null,null,null,null,null);
    }

    public ShaderProgramTemplate(
            ShaderTemplate vs,
            ShaderTemplate tc,
            ShaderTemplate te,
            ShaderTemplate gs,
            ShaderTemplate fg) {
        this.vs = (vs!=null) ? vs : new ShaderTemplate(ShaderTemplate.SHADER_VERTEX);
        this.tc = (tc!=null) ? tc : new ShaderTemplate(ShaderTemplate.SHADER_TESS_CONTROL);
        this.te = (te!=null) ? te : new ShaderTemplate(ShaderTemplate.SHADER_TESS_EVAL);
        this.ge = (gs!=null) ? gs : new ShaderTemplate(ShaderTemplate.SHADER_GEOMETRY);
        this.fr = (fg!=null) ? fg : new ShaderTemplate(ShaderTemplate.SHADER_FRAGMENT);

    }

    public ShaderTemplate getVertexShaderTemplate() {
        return vs;
    }

    public ShaderTemplate getTessaltionControlShaderTemplate() {
        return tc;
    }

    public ShaderTemplate getTesselationEvalShaderTemplate() {
        return te;
    }

    public ShaderTemplate getGeometryShaderTemplate() {
        return ge;
    }

    public ShaderTemplate getFragmentShaderTemplate() {
        return fr;
    }

    public int getMinGLSLVersion() {
        int minversion = GLC.GLSL.V110_GL20;
        if (vs!=null) minversion = Maths.max(minversion, vs.getMinGLSLVersion());
        if (tc!=null) minversion = Maths.max(minversion, tc.getMinGLSLVersion());
        if (te!=null) minversion = Maths.max(minversion, te.getMinGLSLVersion());
        if (ge!=null) minversion = Maths.max(minversion, ge.getMinGLSLVersion());
        if (fr!=null) minversion = Maths.max(minversion, fr.getMinGLSLVersion());
        return minversion;
    }

    public void append(ShaderProgramTemplate template){
        append(template, true, true);
    }

    public void append(ShaderProgramTemplate template, boolean tess, boolean geom){
        vs.append(template.getVertexShaderTemplate());
        if (tess){
            tc.append(template.getTessaltionControlShaderTemplate());
            te.append(template.getTesselationEvalShaderTemplate());
        }
        if (geom){
            ge.append(template.getGeometryShaderTemplate());
        }
        fr.append(template.getFragmentShaderTemplate());
    }

    /**
     * Load template from text program template
     *
     * @param text
     * @return ShaderProgramTemplate
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public static ShaderProgramTemplate loadFromChars(Chars text) throws IOException{
        final ArrayInputStream is = new ArrayInputStream(text.toBytes());
        final CharInputStream ci = new CharInputStream(is, text.getEncoding(), new Char('\n'));
        return load(ci);
    }

    /**
     * Load template from given path.
     *
     * @param path
     * @return ShaderProgramTemplate
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public static ShaderProgramTemplate load(Chars path) throws IOException{
        final Path p = Paths.resolve(path);
        return load(p);
    }

    /**
     * Load template from given path.
     *
     * @param p
     * @return ShaderProgramTemplate
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public static ShaderProgramTemplate load(Path p) throws IOException{
        final ByteInputStream bi = p.createInputStream();
        final CharInputStream ci = new CharInputStream(bi, CharEncodings.UTF_8, new Char('\n'));
        return load(ci);
    }

    private static ShaderProgramTemplate load(CharInputStream ci) throws IOException{
        final CharBuffer vscb = new CharBuffer();
        final CharBuffer tccb = new CharBuffer();
        final CharBuffer tecb = new CharBuffer();
        final CharBuffer gecb = new CharBuffer();
        final CharBuffer frcb = new CharBuffer();

        CharBuffer current = null;
        for (Chars line=ci.readLine();line!=null;line=ci.readLine()){
            final Chars trimLine = line.trim();
            if (trimLine.isEmpty()) continue;

            if     (trimLine.equals(DELIM_VERTEX_SHADER     )){ current = vscb; continue;}
            else if (trimLine.equals(DELIM_TESSCONTROL_SHADER)){ current = tccb; continue;}
            else if (trimLine.equals(DELIM_TESSEVAL_SHADER   )){ current = tecb; continue;}
            else if (trimLine.equals(DELIM_GEOMETRY_SHADER   )){ current = gecb; continue;}
            else if (trimLine.equals(DELIM_FRAGMENT_SHADER   )){ current = frcb; continue;}

            if (current!=null){
                current.append(line).append('\n');
            }
        }

        ci.dispose();

        final ShaderProgramTemplate template = new ShaderProgramTemplate();
        if (!vscb.isEmpty()) template.vs.append(ShaderTemplate.createFromChars(vscb.toChars(), ShaderTemplate.SHADER_VERTEX));
        if (!tccb.isEmpty()) template.tc.append(ShaderTemplate.createFromChars(tccb.toChars(), ShaderTemplate.SHADER_TESS_CONTROL));
        if (!tecb.isEmpty()) template.te.append(ShaderTemplate.createFromChars(tecb.toChars(), ShaderTemplate.SHADER_TESS_EVAL));
        if (!gecb.isEmpty()) template.ge.append(ShaderTemplate.createFromChars(gecb.toChars(), ShaderTemplate.SHADER_GEOMETRY));
        if (!frcb.isEmpty()) template.fr.append(ShaderTemplate.createFromChars(frcb.toChars(), ShaderTemplate.SHADER_FRAGMENT));

        return template;
    }

}
