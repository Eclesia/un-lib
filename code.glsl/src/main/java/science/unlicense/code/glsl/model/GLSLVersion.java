
package science.unlicense.code.glsl.model;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class GLSLVersion {

    private Chars comment;
    private int value;

    public Chars getComment() {
        return comment;
    }

    public void setComment(Chars comment) {
        this.comment = comment;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}
