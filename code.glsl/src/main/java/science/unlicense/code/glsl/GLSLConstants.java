
package science.unlicense.code.glsl;

import science.unlicense.common.api.character.Chars;


/**
 *
 * @author Johann Sorel
 */
public final class GLSLConstants {


    //TODO exaustive list
    public static final Chars KW_UNIFORM   = Chars.constant("uniform");
    public static final Chars KW_LOCATION  = Chars.constant("location");
    public static final Chars KW_LAYOUT    = Chars.constant("layout");
    public static final Chars KW_MAIN      = Chars.constant("main");
    public static final Chars KW_VAR       = Chars.constant("var");
    public static final Chars KW_IN        = Chars.constant("in");
    public static final Chars KW_OUT       = Chars.constant("out");
    public static final Chars KW_CONST     = Chars.constant("const");

    public static final Chars KW_PREPROC_VERSION   = Chars.constant("#version");
    public static final Chars KW_PREPROC_EXTENSION = Chars.constant("#extension");
    public static final Chars KW_PREPROC_LINE      = Chars.constant("#line");


    public static final Chars KW_EXTENSION_ALL      = Chars.constant("all");
    public static final Chars KW_BEHAVIOR_ENABLE    = Chars.constant("enable");
    public static final Chars KW_BEHAVIOR_REQUIRE   = Chars.constant("require");
    public static final Chars KW_BEHAVIOR_WARN      = Chars.constant("warn");
    public static final Chars KW_BEHAVIOR_DISABLE   = Chars.constant("disable");

    //primitive types
    public static final Chars KW_BOOL           = Chars.constant("bool");
    public static final Chars KW_INT            = Chars.constant("int");
    public static final Chars KW_UINT           = Chars.constant("uint");
    public static final Chars KW_FLOAT          = Chars.constant("float");
    public static final Chars KW_DOUBLE         = Chars.constant("double");
    public static final Chars KW_BVEC2          = Chars.constant("bvec2");
    public static final Chars KW_BVEC3          = Chars.constant("bvec3");
    public static final Chars KW_BVEC4          = Chars.constant("bvec4");
    public static final Chars KW_IVEC2          = Chars.constant("ivec2");
    public static final Chars KW_IVEC3          = Chars.constant("ivec3");
    public static final Chars KW_IVEC4          = Chars.constant("ivec4");
    public static final Chars KW_UVEC2          = Chars.constant("uvec2");
    public static final Chars KW_UVEC3          = Chars.constant("uvec3");
    public static final Chars KW_UVEC4          = Chars.constant("uvec4");
    public static final Chars KW_VEC2           = Chars.constant("vec2");
    public static final Chars KW_VEC3           = Chars.constant("vec3");
    public static final Chars KW_VEC4           = Chars.constant("vec4");
    public static final Chars KW_DVEC2          = Chars.constant("dvec2");
    public static final Chars KW_DVEC3          = Chars.constant("dvec3");
    public static final Chars KW_DVEC4          = Chars.constant("dvec4");

    public static final Chars KW_MAT2           = Chars.constant("mat2");
    public static final Chars KW_MAT2x2         = Chars.constant("mat2x2");
    public static final Chars KW_MAT2x3         = Chars.constant("mat2x3");
    public static final Chars KW_MAT2x4         = Chars.constant("mat2x4");
    public static final Chars KW_MAT3           = Chars.constant("mat3");
    public static final Chars KW_MAT3x2         = Chars.constant("mat3x2");
    public static final Chars KW_MAT3x3         = Chars.constant("mat3x3");
    public static final Chars KW_MAT3x4         = Chars.constant("mat3x4");
    public static final Chars KW_MAT4           = Chars.constant("mat4");
    public static final Chars KW_MAT4x2         = Chars.constant("mat4x2");
    public static final Chars KW_MAT4x3         = Chars.constant("mat4x3");
    public static final Chars KW_MAT4x4         = Chars.constant("mat4x4");

    public static final Chars KW_SAMPLER2D      = Chars.constant("sampler2D");
    public static final Chars KW_ISAMPLER2D     = Chars.constant("isampler2D");
    public static final Chars KW_USAMPLER2D     = Chars.constant("usampler2D");
    public static final Chars KW_SAMPLER2DMS    = Chars.constant("sampler2DMS");
    public static final Chars KW_SAMPLERCUBE    = Chars.constant("samplerCube");
    public static final Chars KW_ISAMPLERBUFFER = Chars.constant("isamplerBuffer");
    public static final Chars KW_SAMPLERBUFFER  = Chars.constant("samplerBuffer");

    private GLSLConstants(){}

}
