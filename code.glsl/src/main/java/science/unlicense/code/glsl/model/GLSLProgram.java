
package science.unlicense.code.glsl.model;

import science.unlicense.common.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public class GLSLProgram {

    private GLSLShader vertexShader;
    private GLSLShader tessControlShader;
    private GLSLShader tessEvalShader;
    private GLSLShader geometryShader;
    private GLSLShader fragmentShader;

    public GLSLProgram() {
    }

    public GLSLProgram(GLSLShader vertexShader, GLSLShader tessControlShader,
            GLSLShader tessEvalShader, GLSLShader geometryShader, GLSLShader fragmentShader) {
        this.vertexShader = vertexShader;
        this.tessControlShader = tessControlShader;
        this.tessEvalShader = tessEvalShader;
        this.geometryShader = geometryShader;
        this.fragmentShader = fragmentShader;
    }

    public GLSLShader getVertexShader() {
        return vertexShader;
    }

    public void setVertexShader(GLSLShader vertexShader) {
        this.vertexShader = vertexShader;
    }

    public GLSLShader getTessControlShader() {
        return tessControlShader;
    }

    public void setTessControlShader(GLSLShader tessControlShader) {
        this.tessControlShader = tessControlShader;
    }

    public GLSLShader getTessEvalShader() {
        return tessEvalShader;
    }

    public void setTessEvalShader(GLSLShader tessEvalShader) {
        this.tessEvalShader = tessEvalShader;
    }

    public GLSLShader getGeometryShader() {
        return geometryShader;
    }

    public void setGeometryShader(GLSLShader geometryShader) {
        this.geometryShader = geometryShader;
    }

    public GLSLShader getFragmentShader() {
        return fragmentShader;
    }

    public void setFragmentShader(GLSLShader fragmentShader) {
        this.fragmentShader = fragmentShader;
    }

    public void optimize() {
        throw new UnimplementedException("Not done yet.");
    }

}
