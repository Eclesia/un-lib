

package science.unlicense.format.vp8.vp8;

/**
 * A Sublock is a piece of macroblock.
 * Y : 4x4
 * U : 2x2
 * V : 2x2
 *
 * Specification :
 * http://tools.ietf.org/html/rfc6386#page-5
 *
 * @author Johann Sorel
 */
public class VP8SubBlock {

    public final VP8MacroBlock macroblock;
    public final int coordX;
    public final int coordY;

    /** used for probality decoding */
    public int mode;

    public VP8SubBlock(VP8MacroBlock macroblock, int coordX, int coordY) {
        this.macroblock = macroblock;
        this.coordX = coordX;
        this.coordY = coordY;
    }

    /**
     * http://tools.ietf.org/html/rfc6386#section-11.3
     * Probalility table is defined by the left and above subblocks.
     *
     * @return
     */
    public int getAboveBlockProb(){
        if (coordY==0){
            //we need to look at above macroblock
            if (macroblock.coordY==0){
                //block is on the image edge, we use the expected constant
                return VP8Constants.INTRA_BMODE.B_DC_PRED;
            } else {
                final VP8MacroBlock aboveMacroBlock = macroblock.frame.macroblocks[macroblock.coordY-1][macroblock.coordX];
                return aboveMacroBlock.subBlocks[3][coordX].mode;
            }
        } else {
            //same macroblock
            return macroblock.subBlocks[coordY-1][coordX].mode;
        }
    }

    public int getLeftBlockProb(){
        if (coordX==0){
            //we need to look at left macroblock
            if (macroblock.coordX==0){
                //block is on the image edge, we use the expected constant
                return VP8Constants.INTRA_BMODE.B_DC_PRED;
            } else {
                final VP8MacroBlock leftMacroBlock = macroblock.frame.macroblocks[macroblock.coordY][macroblock.coordX-1];
                return leftMacroBlock.subBlocks[coordY][3].mode;
            }
        } else {
            //same macroblock
            return macroblock.subBlocks[coordY][coordX-1].mode;
        }
    }


}
