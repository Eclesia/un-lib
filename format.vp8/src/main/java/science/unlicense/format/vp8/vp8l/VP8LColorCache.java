

package science.unlicense.format.vp8.vp8l;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Specification :
 * https://developers.google.com/speed/webp/docs/webp_lossless_bitstream_specification#4_image_data
 * Part : 4.2.3
 *
 * @author Johann Sorel
 */
public class VP8LColorCache {

    public int color_cache_code_bits;
    public int color_cache_size;
    public int[] cache;

    public void read(DataInputStream ds) throws IOException{
        color_cache_code_bits = ds.readBits(4);
        if (color_cache_code_bits<1 || color_cache_code_bits>11){
            throw new IOException("Unvalid color cache size : "+color_cache_code_bits);
        }
        color_cache_size = 1 << color_cache_code_bits;
        cache = new int[color_cache_size];
    }

    // WTF ????? spec makes no sense
    public void lookup(int color){
        int i = (0x1e35a7bd * color) >> (32 - color_cache_code_bits);
        //now what ?
    }

}
