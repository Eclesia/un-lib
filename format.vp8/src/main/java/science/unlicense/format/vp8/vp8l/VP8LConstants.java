

package science.unlicense.format.vp8.vp8l;

/**
 *
 * @author Johann Sorel
 */
public final class VP8LConstants {

    public static final int SIGNATURE = 0x2F;

    public static final int TRANSFORM_PREDICTOR = 0;
    public static final int TRANSFORM_COLOR = 1;
    public static final int TRANSFORM_SUBSTRACTGREEN = 2;
    public static final int TRANSFORM_INDEX = 3;

    public static final int PREDICATOR_BLACK = 0;
    public static final int PREDICATOR_LEFT = 1;
    public static final int PREDICATOR_TOP = 2;
    public static final int PREDICATOR_TOPRIGHT = 3;
    public static final int PREDICATOR_TOPLEFT = 4;
    public static final int PREDICATOR_AVG_L_TR_T = 5;
    public static final int PREDICATOR_AVG_L_TL = 6;
    public static final int PREDICATOR_AVG_L_T = 7;
    public static final int PREDICATOR_AVG_TL_T = 8;
    public static final int PREDICATOR_AVG_T_TR = 9;
    public static final int PREDICATOR_AVG_L_TL_T_TR = 10;
    public static final int PREDICATOR_SELECT = 11;
    public static final int PREDICATOR_CLAMP_FULL = 12;
    public static final int PREDICATOR_CLAMP_HALF = 13;

    public static int[][] LZ77_DISTANCE_MAP = new int[][]{
        {0, 1},  {1, 0},  {1, 1},  {-1, 1}, {0, 2},  {2, 0},  {1, 2},  {-1, 2},
        {2, 1},  {-2, 1}, {2, 2},  {-2, 2}, {0, 3},  {3, 0},  {1, 3},  {-1, 3},
        {3, 1},  {-3, 1}, {2, 3},  {-2, 3}, {3, 2},  {-3, 2}, {0, 4},  {4, 0},
        {1, 4},  {-1, 4}, {4, 1},  {-4, 1}, {3, 3},  {-3, 3}, {2, 4},  {-2, 4},
        {4, 2},  {-4, 2}, {0, 5},  {3, 4},  {-3, 4}, {4, 3},  {-4, 3}, {5, 0},
        {1, 5},  {-1, 5}, {5, 1},  {-5, 1}, {2, 5},  {-2, 5}, {5, 2},  {-5, 2},
        {4, 4},  {-4, 4}, {3, 5},  {-3, 5}, {5, 3},  {-5, 3}, {0, 6},  {6, 0},
        {1, 6},  {-1, 6}, {6, 1},  {-6, 1}, {2, 6},  {-2, 6}, {6, 2},  {-6, 2},
        {4, 5},  {-4, 5}, {5, 4},  {-5, 4}, {3, 6},  {-3, 6}, {6, 3},  {-6, 3},
        {0, 7},  {7, 0},  {1, 7},  {-1, 7}, {5, 5},  {-5, 5}, {7, 1},  {-7, 1},
        {4, 6},  {-4, 6}, {6, 4},  {-6, 4}, {2, 7},  {-2, 7}, {7, 2},  {-7, 2},
        {3, 7},  {-3, 7}, {7, 3},  {-7, 3}, {5, 6},  {-5, 6}, {6, 5},  {-6, 5},
        {8, 0},  {4, 7},  {-4, 7}, {7, 4},  {-7, 4}, {8, 1},  {8, 2},  {6, 6},
        {-6, 6}, {8, 3},  {5, 7},  {-5, 7}, {7, 5},  {-7, 5}, {8, 4},  {6, 7},
        {-6, 7}, {7, 6},  {-7, 6}, {8, 5},  {7, 7},  {-7, 7}, {8, 6},  {8, 7}
    };

    private VP8LConstants() {}



}
