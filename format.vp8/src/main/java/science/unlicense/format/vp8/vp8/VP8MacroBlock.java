

package science.unlicense.format.vp8.vp8;

import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.vp8.vp8.VP8Constants.*;

/**
 * A MacroBlock is a YUV block 4:2:0
 * Y : 16x16
 * U : 8x8
 * V : 8x8
 *
 * Specification :
 * http://tools.ietf.org/html/rfc6386#page-5
 *
 * @author Johann Sorel
 */
public class VP8MacroBlock {

    public static final class ResidualBlock{
        public int[] token = new int[16];
        public int[] extra_bits = new int[16];
        public int[] sign = new int[16];
    }

    //coordinate of this macroblock and reference to parent
    public final VP8Frame frame;
    public final int coordX;
    public final int coordY;

    //macroblock header
    public int segment_id;
    public boolean mb_skip_coeff;
    public boolean is_inter_mb;
    /** mb_ref_frame_sel1 selects the reference frame to be used; last frame (0), golden/alternate (1) */
    public boolean mb_ref_frame_sel1;
    /** mb_ref_frame_sel2 selects whether the golden (0) or alternate reference frame (1) is used*/
    public boolean mb_ref_frame_sel2;
    public int mv_mode;
    public int mv_split_mode;
    public int[] sub_mv_mode = new int[4];
    public int intra_y_mode;
    public int intra_uv_mode;

    public final VP8SubBlock[][] subBlocks = new VP8SubBlock[4][4];

    //residual data
    public VP8MacroBlock(VP8Frame frame, int coordX, int coordY) {
        this.frame = frame;
        this.coordX = coordX;
        this.coordY = coordY;

        //build macro blocks
        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                subBlocks[y][x] = new VP8SubBlock(this, x, y);
                if (frame.keyframe){
                    subBlocks[y][x].mode = INTRA_BMODE.B_DC_PRED;
                }
            }
        }

    }

    public void read(BoolReader ds, VP8FrameHeader header) throws IOException {

        macroblock_header(ds, header);
        residual_data(ds,header);

    }

    private void macroblock_header(BoolReader ds, VP8FrameHeader header) throws IOException {
        if (header.segmentation!=null && header.segmentation.updateMbSegmentationMap){
            // expected to use the mb_segment_tree_probs[3] prob
            //doc is not clear but this is header.segment_prob
            segment_id = ds.readTree(MB_SEGMENT_TREE,header.segmentation.segment_prob);
        }
        if (header.mbNoSkipCoeff){
            mb_skip_coeff = ds.readBool(header.probSkipFalse)==1;
        }
        if (!header.isKeyFrame){
            is_inter_mb = ds.readBool(header.probIntra)==1;
        }
        if (is_inter_mb) {
            mb_ref_frame_sel1 = ds.readBool(header.probLast) == 1;
            if (mb_ref_frame_sel1) {
                mb_ref_frame_sel2 = ds.readBool(header.probGf) == 1;
            }
            mv_mode = ds.readTree(MV_REF_TREE,null);
//            if (mv_mode == SPLITMV) {
//                mv_split_mode = ds.readTree(null);
//                for (int i = 0; i < numMvs; i++) {
//                    sub_mv_mode = ds.readTree(null);
//                    if (sub_mv_mode == NEWMV4x4) {
//                        read_mvcomponent();
//                        read_mvcomponent();
//                    }
//                }
//            } else if (mv_mode == NEWMV) {
//                read_mvcomponent();
//                read_mvcomponent();
//            }
        } else { /* intra mb */

            intra_y_mode = ds.readTree(KF_YMODE_TREE,KF_YMODE_PROB);
            if (intra_y_mode == INTRA_MBMODE.B_PRED) {
                for (int y=0;y<4;y++){
                    for (int x=0;x<4;x++){
                        final int indiceAbove = subBlocks[y][x].getAboveBlockProb();
                        final int indiceLeft = subBlocks[y][x].getLeftBlockProb();
                        subBlocks[y][x].mode = ds.readTree(BMODE_TREE, KF_BMODE_PROB[indiceAbove][indiceLeft]);
                    }
                }
            }
            intra_uv_mode = ds.readTree(UV_MODE_TREE, KF_UV_MODE_PROB);
        }
    }

    private void residual_data(BoolReader ds, VP8FrameHeader header) {
//        if (!mb_skip_coeff) {
//            if ((is_inter_mb && mv_mode != SPLITMV)
//                    || (!is_inter_mb && intra_y_mode != B_PRED)) {
//                residual_block() /* Y2 */
//
//            }
//            for (int i = 0; i < 24; i++) {
//                residual_block(ds); /* 16 Y, 4 U, 4 V */
//            }
//        }
    }

    private ResidualBlock residual_block(BoolReader ds, int n, VP8FrameHeader header) {
        final ResidualBlock rb = new ResidualBlock();
//        for (int i = firstCoeff; i < 16; i++) {
//            token = ds.readTree(null);
//            if (token == EOB) {
//                break;
//            }
//            if (token_has_extra_bits) {
//                extra_bits = ds.readULiteral(n);
//            }
//            if (coefficient != 0) {
//                sign = ds.readULiteral(1);
//            }
//        }

        return rb;
    }

}
