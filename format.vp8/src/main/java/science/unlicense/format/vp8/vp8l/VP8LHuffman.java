

package science.unlicense.format.vp8.vp8l;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class VP8LHuffman {

    public void read(DataInputStream ds) throws IOException{
        final int b = ds.readBits(1);

        if (b==0){
            //a single entropy image
        } else {
            //multiple entropy images
        }

    }

    public void readEntroyImage(DataInputStream ds, int xsize, int ysize) throws IOException{
        int huffman_bits = ds.readBits(3) + 2;
        int huffman_xsize = VP8LReader.divRoundUp(xsize, 1 << huffman_bits);
        int huffman_ysize = VP8LReader.divRoundUp(ysize, 1 << huffman_bits);
        //TODO entropy image
    }

}
