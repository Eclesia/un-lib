
package science.unlicense.format.vp8.vp8;

import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class VP8Reader extends AbstractReader{

    public VP8Frame readFrame() throws IOException{
        final VP8Frame frame = new VP8Frame();
        final DataInputStream ds = getInputAsDataStream(Endianness.LITTLE_ENDIAN);
        ds.setBitsDirection(DataInputStream.LSB);
        frame.read(ds);
        return frame;
    }

}
