

package science.unlicense.format.vp8.vp8l;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class VP8LLZ77 {

    private VP8LLZ77(){}

    /**
     * https://developers.google.com/speed/webp/docs/webp_lossless_bitstream_specification#42_encoding_of_image_data
     * part 4.2.2
     *
     * @param ds
     * @param prefix_code
     * @return
     * @throws IOException
     */
    public static int toValue(final DataInputStream ds, int prefix_code) throws IOException {
        if (prefix_code < 4) {
            return prefix_code + 1;
        }
        final int extra_bits = (prefix_code - 2) >> 1;
        final int offset = (2 + (prefix_code & 1)) << extra_bits;
        return offset + ds.readBits(extra_bits) + 1;
    }

    public static int distance(int i, int xsize){
        final int[] xy = VP8LConstants.LZ77_DISTANCE_MAP[i];
        int dist = xy[0] + xy[1] * xsize;
        if (dist < 1) {
            dist = 1;
        }
        return dist;
    }

}
