

package science.unlicense.protocol.pop;

import science.unlicense.common.api.character.Chars;

/**
 * Reference :
 * http://tools.ietf.org/html/rfc1939
 *
 * @author Johann Sorel
 */
public final class POPConstants {

    /** Used for argument separation */
    public static final int SPACE = ' ';
    /** Used to delimite message and commands parts */
    public static final Chars CRLF = Chars.constant(new byte[]{0x0D,0x0A});
    /** Used to delimite end of data */
    public static final Chars END_OF_DATA = CRLF.concat('.').concat(CRLF);

    public static final Chars STATUS_OK = Chars.constant("+OK");
    public static final Chars STATUS_ERROR = Chars.constant("-ERR");

    public static final Chars COMMAND_STAT = Chars.constant("STAT");
    public static final Chars COMMAND_LIST = Chars.constant("LIST");
    public static final Chars COMMAND_RETR = Chars.constant("RETR");
    public static final Chars COMMAND_DELE = Chars.constant("DELE");
    public static final Chars COMMAND_NOOP = Chars.constant("NOOP");
    public static final Chars COMMAND_RSET = Chars.constant("RSET");
    public static final Chars COMMAND_QUIT = Chars.constant("QUIT");
    public static final Chars COMMAND_TOP = Chars.constant("TOP");
    public static final Chars COMMAND_UIDL = Chars.constant("UIDL");
    public static final Chars COMMAND_USER = Chars.constant("USER");
    public static final Chars COMMAND_PASS = Chars.constant("PASS");
    public static final Chars COMMAND_APOP = Chars.constant("APOP");

    private POPConstants(){}

}
