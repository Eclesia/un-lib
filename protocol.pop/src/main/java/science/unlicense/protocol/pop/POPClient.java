

package science.unlicense.protocol.pop;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.IPAddress;

/**
 * POP protocol client.
 *
 * @author Johann Sorel
 */
public class POPClient {

    private final ClientSocket socket;

    /**
     * Create an POP client on default port (110).
     *
     * @param host pop server name
     * @throws IOException
     */
    public POPClient(Chars host) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host));
    }

    /**
     * Create an POP client on default port (110).
     *
     * @param address pop server address
     * @throws IOException
     */
    public POPClient(IPAddress address) throws IOException {
        this(address,110);
    }

    /**
     * Create an POP client.
     *
     * @param host pop server name
     * @param port pop server port
     * @throws IOException
     */
    public POPClient(Chars host, int port) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host),port);
    }

    /**
     * Create an POP client.
     *
     * @param address pop server address
     * @param port pop server port
     * @throws IOException
     */
    public POPClient(IPAddress address, int port) throws IOException {
        socket = science.unlicense.system.System.get().getSocketManager().createClientSocket(address, port);
    }

    /**
     * Close pop client.
     *
     * @throws IOException
     */
    public void close() throws IOException{
        socket.close();
    }
}
