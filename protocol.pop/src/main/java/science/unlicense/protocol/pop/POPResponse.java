

package science.unlicense.protocol.pop;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class POPResponse {

    public Chars status;
    public Chars keyword;
    public Chars[] arguments;

}
