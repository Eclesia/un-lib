
package science.unlicense.filesystem.ntfs.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int64;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class NTFSPartitionBootSector extends CObject{

    /** 3 bytes JMP instruction */
    public int jumpOffset;
    /** 8 bytes OEM ID : signature */
    public Chars oemId;
    /** 2 bytes Bytes per sector */
    public int bytePerSector;
    /** 1 byte  Sectors Per Cluster */
    public int sectorPerCluster;
    /** 2 bytes Reserved Sectors */
    public int reserverdSectors;
    /** 3 bytes Unused */
    public int unused1;
    /** 2 bytes Unused */
    public int unused2;
    /** 1 byte  Media Descriptor */
    public int mediaDescriptor;
    /** 2 bytes Unused */
    public int unused3;
    /** 2 bytes Sectors Per Track */
    public int sectorPerTrack;
    /** 2 bytes Number Of Heads */
    public int numberOfHeads;
    /** 4 bytes Hidden Sectors */
    public int hiddenSectors;
    /** 4 bytes Unused */
    public int unused4;
    /** 4 bytes Unused */
    public int unused5;
    /** 8 bytes Total sectors */
    public long totalSectors;
    /** 8 bytes $MFT cluster number */
    public long mftCluster;
    /** 8 bytes $MFTMirr cluster number */
    public long mftMirrorCluster;
    /** 4 bytes Clusters Per File Record Segment */
    public int clustersPerFileRecordSegment;
    /** 1 byte  Clusters Per Index Buffer */
    public int clustersPerIndexBuffer;
    /** 3 bytes Unused */
    public int unused6;
    /** 8 bytes Volume Serial Number */
    public long volumeSerial;
    /** 4 bytes Checksum */
    public int checksum;
    /** 426 bytes Bootstrap Code */
    public byte[] bootstrapCode;
    /** 2 bytes End-of-sector Marker */
    public int endOfSector;


    public void read(DataInputStream ds) throws IOException{

        jumpOffset          = ds.readUInt24();
        oemId               = new Chars(ds.readFully(new byte[8]));
        bytePerSector       = ds.readUShort();
        sectorPerCluster    = ds.readUByte();
        reserverdSectors    = ds.readUShort();
        unused1             = ds.readUInt24();
        unused2             = ds.readUShort();
        mediaDescriptor     = ds.readUByte();
        unused3             = ds.readUShort();
        sectorPerTrack      = ds.readUShort();
        numberOfHeads       = ds.readUShort();
        hiddenSectors       = ds.readInt();
        unused4             = ds.readInt();
        unused5             = ds.readInt();
        totalSectors        = ds.readLong();
        mftCluster          = ds.readLong();
        mftMirrorCluster    = ds.readLong();
        clustersPerFileRecordSegment = ds.readInt();
        clustersPerIndexBuffer = ds.readUByte();
        unused6             = ds.readUInt24();
        volumeSerial        = ds.readLong();
        checksum            = ds.readInt();
        bootstrapCode       = ds.readFully(new byte[426]);
        endOfSector         = ds.readUShort();

        if (endOfSector != 0xAA55){
            throw new IOException(ds, "Unvalid boot sector, end marker do not match");
        }

    }

    public long getMFTBteOffset(){
        return mftCluster * sectorPerCluster * bytePerSector;
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(" -NTFSPartitionBootSector").append('\n');
        cb.append(" -jumpOffset : ").append(Int32.encode(jumpOffset)).append('\n');
        cb.append(" -oemId : ").append(oemId).append('\n');
        cb.append(" -bytePerSector : ").append(Int32.encode(bytePerSector)).append('\n');
        cb.append(" -sectorPerCluster : ").append(Int32.encode(sectorPerCluster)).append('\n');
        cb.append(" -reserverdSectors : ").append(Int32.encode(reserverdSectors)).append('\n');
        cb.append(" -mediaDescriptor : ").append(Int32.encode(mediaDescriptor)).append('\n');
        cb.append(" -sectorPerTrack : ").append(Int32.encode(sectorPerTrack)).append('\n');
        cb.append(" -numberOfHeads : ").append(Int32.encode(numberOfHeads)).append('\n');
        cb.append(" -hiddenSectors : ").append(Int32.encode(hiddenSectors)).append('\n');
        cb.append(" -totalSectors : ").append(Int64.encode(totalSectors)).append('\n');
        cb.append(" -mftCluster : ").append(Int64.encode(mftCluster)).append('\n');
        cb.append(" -mftMirrotCluster : ").append(Int64.encode(mftMirrorCluster)).append('\n');
        cb.append(" -clustersPerFileRecordSegment : ").append(Int32.encode(clustersPerFileRecordSegment)).append('\n');
        cb.append(" -clustersPerIndexBuffer : ").append(Int32.encode(clustersPerIndexBuffer)).append('\n');
        cb.append(" -volumeSerial : ").append(Int64.encode(volumeSerial)).append('\n');
        cb.append(" -checksum : ").append(Int32.encode(checksum)).append('\n');
        cb.append(" -endOfSector : ").append(Int32.encode(endOfSector)).append('\n');
        return cb.toChars();
    }

}
