
package science.unlicense.filesystem.ntfs;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.filesystem.ntfs.model.NTFSPartitionBootSector;
import science.unlicense.filesystem.ntfs.model.NTFSRecord;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class NTFSFileSystem extends AbstractStore{

    public NTFSPartitionBootSector bootSector;

    public NTFSFileSystem(Object input) {
        super(null, input);
    }

    public void read() throws IOException{

        final boolean[] close = new boolean[1];
        final ByteInputStream bs = IOUtilities.toInputStream(getInput(), close);
        try{
            final DataInputStream ds = new DataInputStream(bs, Endianness.LITTLE_ENDIAN);

            //read boot sector
            bootSector = new NTFSPartitionBootSector();
            bootSector.read(ds);
            System.out.println(bootSector);

            //read MFT
            final long mftOffset = bootSector.getMFTBteOffset();
            ds.skipFully(mftOffset-ds.getByteOffset());

            final NTFSRecord record = new NTFSRecord();
            record.read(ds);

            System.out.println(record);


        }finally{
            bs.dispose();
        }

    }

}
