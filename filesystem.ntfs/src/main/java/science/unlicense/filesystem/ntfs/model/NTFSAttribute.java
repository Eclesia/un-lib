
package science.unlicense.filesystem.ntfs.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class NTFSAttribute extends CObject {

    /** 4 bytes : Attribute Type Identifier */
    public int typeId;
    /** 4 bytes : Length of Attribute, offset to next attribute */
    public int length;
    /** 1 byte : Non-resident flag */
    public byte nonResident;
    /** 1 bytes : Length of name */
    public int nameLength;
    /** 2 bytes : Offset to name */
    public int nameOffset;
    /** 2 bytes : Flags */
    public int flags;
    /** 2 bytes : Attribute Identifier */
    public int attId;

    //Resident properties
    /** 4 bytes : Size of content */
    public int contentSize;
    /** 2 bytes : Offset to content */
    public int contentOffset;

    //Non-resident properties
    /** 8 bytes : Starting Virtual Cluster Number of the runlist */
    public long startClusterNum;
    /** 8 bytes : Ending Virtual Cluster Number of the runlist */
    public long endClusterNum;
    /** 2 bytes : Offset to the runlist */
    public int offset;
    /** 2 bytes : Compression unit size */
    public int compressionUnitSize;
    /** 4 bytes : unused */
    /** 8 bytes : Allocated size of the attribute content */
    public long contentAllocatedSize;
    /** 8 bytes : Actual size of attribute content */
    public long contentActualSize;
    /** 8 bytes : Initialized size of the attribute content. */
    public long contentInitializedSize;

    public void read(DataInputStream ds) throws IOException{
        typeId = ds.readInt();
        length = ds.readInt();
        nonResident = ds.readByte();
        nameLength = ds.readUByte();
        nameOffset = ds.readUShort();
        flags = ds.readUShort();

        if (nonResident==0){
            contentSize = ds.readInt();
            contentOffset = ds.readUShort();
        } else {
            startClusterNum = ds.readLong();
            endClusterNum = ds.readLong();
            offset = ds.readUShort();
            compressionUnitSize = ds.readUShort();
            ds.skipFully(4);
            contentAllocatedSize = ds.readLong();
            contentActualSize = ds.readLong();
            contentInitializedSize = ds.readLong();
        }

    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("ATT : "+typeId).append(" "+length).append(" "+nonResident);
        return cb.toChars();
    }

}
