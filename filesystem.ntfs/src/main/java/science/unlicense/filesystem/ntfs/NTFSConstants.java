
package science.unlicense.filesystem.ntfs;

import science.unlicense.common.api.character.Chars;

/**
 *
 * Resources :
 * http://en.wikipedia.org/wiki/NTFS
 * http://www.ntfs.com/ntfs_basics.htm
 * https://technet.microsoft.com/en-us/library/cc781134(v=ws.10).aspx
 * http://www.cse.scu.edu/~tschwarz/coen252_07Fall/Lectures/NTFS.html
 *
 * @author Johann Sorel
 */
public final class NTFSConstants {

    public static final Chars FILE_MFT              = Chars.constant("$MFT");
    public static final Chars FILE_MFTMIRROR        = Chars.constant("$MFTMirr");
    public static final Chars FILE_LOGFILE          = Chars.constant("$LogFile");
    public static final Chars FILE_VOLUME           = Chars.constant("$Volume");
    public static final Chars FILE_ATTRDEF          = Chars.constant("$AttrDef");
    public static final Chars FILE_DOT              = Chars.constant(".");
    public static final Chars FILE_BITMAP           = Chars.constant("$Bitmap");
    public static final Chars FILE_BADCLUS          = Chars.constant("$BadClus");
    public static final Chars FILE_SECURE           = Chars.constant("$Secure");
    public static final Chars FILE_UPCASE           = Chars.constant("$UpCase");
    public static final Chars FILE_EXTEND           = Chars.constant("$Extend");
    public static final Chars FILE_EXTEND_QUOTA     = Chars.constant("$Extend\\$Quota");
    public static final Chars FILE_EXTEND_OBJID     = Chars.constant("$Extend\\$ObjId");
    public static final Chars FILE_EXTEND_REPARSE   = Chars.constant("$Extend\\$Reparse");

    public static final Chars MTF_SIGNATURE = Chars.constant("FILE");

    public static final int ATT_STANDARD_INFORMATION = 0x10;
    public static final int ATT_ATTRIBUTE_LIST = 0x20;
    public static final int ATT_FILE_NAME = 0x30;
    public static final int ATT_VOLUME_VERSION = 0x40;
    public static final int ATT_OBJECT_ID = 0x40;
    public static final int ATT_SECURITY_DESCRIPTOR = 0x50;
    public static final int ATT_VOLUME_NAME = 0x60;
    public static final int ATT_VOLUME_INFORMATION = 0x70;
    public static final int ATT_DATA = 0x80;
    public static final int ATT_INDEX_ROOT = 0x90;
    public static final int ATT_INDEX_ALLOCATION = 0xA0;
    public static final int ATT_BITMAP = 0xB0;
    public static final int ATT_SYMBOLIC_LINK = 0xC0;
    public static final int ATT_REPARSE_POINT = 0xC0;
    public static final int ATT_EA_INFORMATION = 0xD0;
    public static final int ATT_EA = 0xE0;
    public static final int ATT_PROPERTY_SET = 0xF0;
    public static final int ATT_LOGGED_UTILITY_STREAM = 0x100;

    private NTFSConstants(){}

}
