package science.unlicense.format.xml.schema;

import java.util.Date;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.NodeCardinality;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class SchemaReaderTest {

    private static final CharEncoding ENC = CharEncodings.US_ASCII;

    @Test
    public void testReadSimple() throws Exception {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/xml/schema/person.xsd"));

        final XSDStack stack = new XSDStack(path,false);
        final NodeCardinality card = stack.read();
        Assert.assertNotNull(card);

        Assert.assertEquals(new Chars("person"), card.getId());
        Assert.assertEquals(new Chars("person"), card.getType().getId()); //inherit
        final NodeCardinality[] properties = card.getType().getChildrenTypes();
        Assert.assertEquals(5, properties.length);
        Assert.assertEquals(new Chars("firstname"), properties[0].getId());
        Assert.assertEquals(Chars.class, properties[0].getType().getValueClass());
        Assert.assertEquals(new Chars("lastname"), properties[1].getId());
        Assert.assertEquals(Chars.class, properties[1].getType().getValueClass());
        Assert.assertEquals(new Chars("birth_date"), properties[2].getId());
        Assert.assertEquals(Date.class, properties[2].getType().getValueClass());
        Assert.assertEquals(new Chars("place"), properties[3].getId());
        Assert.assertEquals(Chars.class, properties[3].getType().getValueClass());
        Assert.assertEquals(new Chars("phone"), properties[4].getId());
        Assert.assertEquals(Chars.class, properties[4].getType().getValueClass());

    }

    @Test
    public void testReadComplex() throws Exception {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/xml/schema/building.xsd"));

        final XSDStack stack = new XSDStack(path,false);
        final NodeCardinality card = stack.read();
        Assert.assertNotNull(card);

        Assert.assertEquals(new Chars("building"), card.getId());
        Assert.assertEquals(new Chars("building"), card.getType().getId()); //inherit
        final NodeCardinality[] properties = card.getType().getChildrenTypes();
        Assert.assertEquals(5, properties.length);
        Assert.assertEquals(new Chars("attage"), properties[0].getId());
        Assert.assertEquals(Chars.class, properties[0].getType().getValueClass());
        Assert.assertEquals(1, properties[0].getMinOccurences());
        Assert.assertEquals(1, properties[0].getMaxOccurences());

        Assert.assertEquals(new Chars("asset"), properties[1].getId());
        Assert.assertEquals(Chars.class, properties[1].getType().getValueClass());
        Assert.assertEquals(1, properties[1].getMinOccurences());
        Assert.assertEquals(1, properties[1].getMaxOccurences());

        Assert.assertEquals(new Chars("extra"), properties[2].getId());
        Assert.assertEquals(Chars.class, properties[2].getType().getValueClass());
        Assert.assertEquals(0, properties[2].getMinOccurences());
        Assert.assertEquals(2, properties[2].getMaxOccurences());

        Assert.assertEquals(new Chars("room"), properties[3].getId());
        Assert.assertEquals(Chars.class, properties[3].getType().getValueClass());
        Assert.assertEquals(0, properties[3].getMinOccurences());
        Assert.assertEquals(Integer.MAX_VALUE, properties[3].getMaxOccurences());

        Assert.assertEquals(new Chars("class"), properties[4].getId());
        Assert.assertEquals(Chars.class, properties[4].getType().getValueClass());
        Assert.assertEquals(0, properties[4].getMinOccurences());
        Assert.assertEquals(Integer.MAX_VALUE, properties[4].getMaxOccurences());

    }

    @Test
    public void testReadEmpty() throws Exception {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/xml/schema/empty.xsd"));

        final XSDStack stack = new XSDStack(path,false);
        final NodeCardinality card = stack.read();
        Assert.assertNotNull(card);

        Assert.assertEquals(new Chars("empty"), card.getId());
        Assert.assertEquals(new Chars("empty"), card.getType().getId()); //inherit
        final NodeCardinality[] properties = card.getType().getChildrenTypes();
        Assert.assertEquals(0, properties.length);

    }

}
