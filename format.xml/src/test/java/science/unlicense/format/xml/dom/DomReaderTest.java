
package science.unlicense.format.xml.dom;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLInputStream;
import science.unlicense.format.xml.XMLTestConstants;

/**
 *
 * @author Johann Sorel
 */
public class DomReaderTest {

     private static final CharEncoding ENC = CharEncodings.US_ASCII;

    @Test
    public void testRead() throws Exception {

        final XMLInputStream stream = new XMLInputStream();
        stream.setEncoding(ENC);
        stream.setInput(new ArrayInputStream(XMLTestConstants.SAMPLE));

        final DomReader reader = new DomReader();
        reader.setInput(stream);

        Node node = reader.read();
        Assert.assertTrue(node instanceof DomElement);

        // <human gender = "male" > --------------------------------------------
        final DomElement human = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("human", ENC)), human.getName());
        Assert.assertEquals(null, human.getText());
        Assert.assertEquals(1, human.getProperties().getSize());
        Assert.assertEquals(5,  human.getChildren().getSize());

        // <!-- someone very important --> -------------------------------------
        node = (Node) human.getChildren().get(0);
        Assert.assertTrue(node instanceof DomComment);
        DomComment cmt = (DomComment) human.getChildren().get(0);
        Assert.assertEquals(new Chars("someone very important", ENC), cmt.getText());

        // <firstName>hubert</firstName>----------------------------------------
        node = (Node) human.getChildren().get(1);
        Assert.assertTrue(node instanceof DomElement);
        final DomElement firstName = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("firstName", ENC)), firstName.getName());
        Assert.assertEquals(new Chars("hubert", ENC), firstName.getText());
        Assert.assertEquals(0, firstName.getProperties().getSize());

        // <lastName>dupont</lastName> -----------------------------------------
        node = (Node) human.getChildren().get(2);
        Assert.assertTrue(node instanceof DomElement);
        final DomElement lastName = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("lastName", ENC)), lastName.getName());
        Assert.assertEquals(new Chars("dupont", ENC), lastName.getText());
        Assert.assertEquals(0, lastName.getProperties().getSize());

        // <children>0</children> -----------------------------------------
        node = (Node) human.getChildren().get(3);
        Assert.assertTrue(node instanceof DomElement);
        final DomElement children = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("children", ENC)), children.getName());
        Assert.assertEquals(new Chars("0", ENC), children.getText());
        Assert.assertEquals(0, children.getProperties().getSize());

        // <single/> -----------------------------------------------------------
        node = (Node) human.getChildren().get(4);
        Assert.assertTrue(node instanceof DomElement);
        final DomElement single = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("single", ENC)), single.getName());
        Assert.assertEquals(null, single.getText());
        Assert.assertEquals(0, single.getProperties().getSize());

        System.out.println(human);
    }

    @Test
    public void testReadTyped() throws Exception {

        final XMLInputStream stream = new XMLInputStream();
        stream.setEncoding(ENC);
        stream.setInput(new ArrayInputStream(XMLTestConstants.SAMPLE_DOCTYPE));

        final DomReader reader = new DomReader();
        reader.setInput(stream);

        Node node = reader.read();
        Assert.assertTrue(node instanceof DomElement);

        // <human gender = "male" > --------------------------------------------
        final DomElement human = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("human", ENC)), human.getName());
        Assert.assertEquals(null, human.getText());
        Assert.assertEquals(0, human.getProperties().getSize());
        Assert.assertEquals(0,  human.getChildren().getSize());

        System.out.println(human);
    }

    @Test
    public void testReadXmlts() throws IOException{

        Chars str = new Chars(" <?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
"<metadata>\n" +
"  <groupId>org.jsweet</groupId>\n" +
"  <artifactId>jsweet-transpiler</artifactId>\n" +
"  <version>2.1.0-SNAPSHOT</version>\n" +
"  <versioning>\n" +
"    <snapshot>\n" +
"      <timestamp>20171115.191131</timestamp>\n" +
"      <buildNumber>3</buildNumber>\n" +
"    </snapshot>\n" +
"    <lastUpdated>20171115191132</lastUpdated>\n" +
"    <snapshotVersions>\n" +
"      <snapshotVersion>\n" +
"        <classifier>javadoc</classifier>\n" +
"        <extension>jar</extension>\n" +
"        <value>2.1.0-20171115.191131-3</value>\n" +
"        <updated>20171115191131</updated>\n" +
"      </snapshotVersion>\n" +
"      <snapshotVersion>\n" +
"        <classifier>sources</classifier>\n" +
"        <extension>jar</extension>\n" +
"        <value>2.1.0-20171115.191131-3</value>\n" +
"        <updated>20171115191131</updated>\n" +
"      </snapshotVersion>\n" +
"      <snapshotVersion>\n" +
"        <extension>jar</extension>\n" +
"        <value>2.1.0-20171115.191131-3</value>\n" +
"        <updated>20171115191131</updated>\n" +
"      </snapshotVersion>\n" +
"      <snapshotVersion>\n" +
"        <extension>pom</extension>\n" +
"        <value>2.1.0-20171115.191131-3</value>\n" +
"        <updated>20171115191131</updated>\n" +
"      </snapshotVersion>\n" +
"      <snapshotVersion>\n" +
"        <classifier>jar-with-dependencies</classifier>\n" +
"        <extension>jar</extension>\n" +
"        <value>2.1.0-20171115.191131-3</value>\n" +
"        <updated>20171115191131</updated>\n" +
"      </snapshotVersion>\n" +
"    </snapshotVersions>\n" +
"  </versioning>\n" +
"</metadata>");

        ByteInputStream in = new ArrayInputStream(str.toBytes());

        DomReader reader = new DomReader();
        reader.setInput(in);
         DomNode node = reader.read();
         System.out.println(node);


    }

}
