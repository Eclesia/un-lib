
package science.unlicense.format.xml;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.encoding.api.io.ArrayInputStream;
import static science.unlicense.format.xml.XMLElement.*;

/**
 *
 * @author Johann Sorel
 */
public class XMLInputStreamTest {

    private static final CharEncoding ENC = CharEncodings.US_ASCII;

    @Test
    public void testReadNoBlankSkip() throws Exception {

        XMLElement element = null;
        Dictionary properties = null;
        Iterator iterator = null;

        final XMLInputStream stream = new XMLInputStream();
        stream.setEncoding(ENC);
        stream.setInput(new ArrayInputStream(XMLTestConstants.SAMPLE));

        // <?xml version="1.0" encoding="UTF-8"?> ------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_META,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("xml",ENC),element.getName());
        Assert.assertEquals(new Char('?',ENC),element.getEscape());

        properties = stream.readProperties();
        Assert.assertEquals(2, properties.getKeys().getSize());
        Assert.assertEquals(new Chars("1.0",ENC),properties.getValue(new Chars("version",ENC)));
        Assert.assertEquals(new Chars("UTF-8",ENC),properties.getValue(new Chars("encoding",ENC)));

        //some blanks
        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());

        // <human gender = "male" > --------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("human",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        properties = stream.readProperties();
        Assert.assertEquals(1, properties.getKeys().getSize());
        Assert.assertEquals(new Chars("male",ENC),properties.getValue(new Chars("gender",ENC)));

        //some blanks
        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());

        // <!-- someone very important --> -------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_COMMENT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(new Char('!',ENC),element.getEscape());
        properties = stream.readProperties();
        Assert.assertEquals(0, properties.getKeys().getSize());
        Assert.assertEquals(new Chars("someone very important",ENC),stream.readText());

        //some blanks
        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());

        // <firstName>hubert</firstName>----------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("firstName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());
        Assert.assertEquals(new Chars("hubert",ENC),stream.readText());

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("firstName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        //some blanks
        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());

        // <lastName>dupont</lastName> -----------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("lastName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());
        Assert.assertEquals(new Chars("dupont",ENC),stream.readText());

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("lastName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        //some blanks
        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());

        // <children>0</children> ----------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("children",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());
        Assert.assertEquals(new Chars("0",ENC),stream.readText());

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("children",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        //some blanks
        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());

        // <single/> -----------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("single",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("single",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        //some blanks
        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());

        // </human> ------------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("human",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        //some blanks
        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());

        element = stream.readElement();
        Assert.assertNull(element);

    }

    @Test
    public void testReadBlankSkip() throws Exception {

        XMLElement element = null;
        Dictionary properties = null;
        Iterator iterator = null;

        final XMLInputStream stream = new XMLInputStream();
        stream.setSkipEmptyText(true);
        stream.setEncoding(ENC);
        stream.setInput(new ArrayInputStream(XMLTestConstants.SAMPLE));

        // <?xml version="1.0" encoding="UTF-8"?> ------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_META,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("xml",ENC),element.getName());
        Assert.assertEquals(new Char('?',ENC),element.getEscape());

        properties = stream.readProperties();
        Assert.assertEquals(2, properties.getKeys().getSize());
        Assert.assertEquals(new Chars("1.0",ENC),properties.getValue(new Chars("version",ENC)));
        Assert.assertEquals(new Chars("UTF-8",ENC),properties.getValue(new Chars("encoding",ENC)));

        // <human gender = "male" > -------------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("human",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        properties = stream.readProperties();
        Assert.assertEquals(1, properties.getKeys().getSize());
        Assert.assertEquals(new Chars("male",ENC),properties.getValue(new Chars("gender",ENC)));

        // <!-- someone very important --> -------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_COMMENT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(new Char('!',ENC),element.getEscape());
        properties = stream.readProperties();
        Assert.assertEquals(0, properties.getKeys().getSize());
        Assert.assertEquals(new Chars("someone very important",ENC),stream.readText());

        // <firstName>hubert</firstName>----------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("firstName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());
        Assert.assertEquals(new Chars("hubert",ENC),stream.readText());

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("firstName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        // <lastName>dupont</lastName> ----------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("lastName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());
        Assert.assertEquals(new Chars("dupont",ENC),stream.readText());

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("lastName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        // <children>0</children> ----------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("children",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());
        Assert.assertEquals(new Chars("0",ENC),stream.readText());

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("children",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        // <single/> -----------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("single",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("single",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        // </human> ------------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("human",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertNull(element);

    }

    @Test
    public void testReadPrefixed() throws Exception {

        XMLElement element = null;
        Dictionary properties = null;
        Iterator iterator = null;

        final XMLInputStream stream = new XMLInputStream();
        stream.setSkipEmptyText(true);
        stream.setEncoding(ENC);
        stream.setInput(new ArrayInputStream(XMLTestConstants.SAMPLE_PREFIXED));

        // <?xml version="1.0" encoding="UTF-8"?> ------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_META,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("xml",ENC),element.getName());
        Assert.assertEquals(new Char('?',ENC),element.getEscape());

        properties = stream.readProperties();
        Assert.assertEquals(2, properties.getKeys().getSize());
        Assert.assertEquals(new Chars("1.0",ENC),properties.getValue(new Chars("version",ENC)));
        Assert.assertEquals(new Chars("UTF-8",ENC),properties.getValue(new Chars("encoding",ENC)));

        // <human gender = "male" > -------------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(new Chars("exp",ENC),element.getPrefix());
        Assert.assertEquals(new Chars("human",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        properties = stream.readProperties();
        Assert.assertEquals(1, properties.getKeys().getSize());
        Assert.assertEquals(new Chars("male",ENC),properties.getValue(new Chars("gender",ENC)));

        // <!-- someone very important --> -------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_COMMENT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(new Char('!',ENC),element.getEscape());
        properties = stream.readProperties();
        Assert.assertEquals(0, properties.getKeys().getSize());
        Assert.assertEquals(new Chars("someone very important",ENC),stream.readText());

        // <firstName>hubert</firstName>----------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(new Chars("att1",ENC),element.getPrefix());
        Assert.assertEquals(new Chars("firstName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());
        Assert.assertEquals(new Chars("hubert",ENC),stream.readText());

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(new Chars("att1",ENC),element.getPrefix());
        Assert.assertEquals(new Chars("firstName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        // <lastName>dupont</lastName> ----------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(new Chars("att2",ENC),element.getPrefix());
        Assert.assertEquals(new Chars("lastName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertEquals(TYPE_TEXT,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(null,element.getEscape());
        Assert.assertEquals(new Chars("dupont",ENC),stream.readText());

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(new Chars("att2",ENC),element.getPrefix());
        Assert.assertEquals(new Chars("lastName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        // <single/> -----------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(new Chars("att3",ENC),element.getPrefix());
        Assert.assertEquals(new Chars("single",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(new Chars("att3",ENC),element.getPrefix());
        Assert.assertEquals(new Chars("single",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        // </human> ------------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(new Chars("exp",ENC),element.getPrefix());
        Assert.assertEquals(new Chars("human",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertNull(element);

    }

    @Test
    public void testReadEscape() throws Exception {

        XMLElement element = null;
        Dictionary properties = null;
        Iterator iterator = null;

        final XMLInputStream stream = new XMLInputStream();
        stream.setSkipEmptyText(true);
        stream.setEncoding(ENC);
        stream.setInput(new ArrayInputStream(XMLTestConstants.SAMPLE_CHARESCAPE));

        // <?xml version="1.0" encoding="UTF-8"?> ------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_META,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("xml",ENC),element.getName());
        Assert.assertEquals(new Char('?',ENC),element.getEscape());

        properties = stream.readProperties();
        Assert.assertEquals(2, properties.getKeys().getSize());
        Assert.assertEquals(new Chars("1.0",ENC),properties.getValue(new Chars("version",ENC)));
        Assert.assertEquals(new Chars("UTF-8",ENC),properties.getValue(new Chars("encoding",ENC)));

        // <human xmlns:math=\"http://www.w3.org/1998/Math/MathML\" > -------------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("human",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        properties = stream.readProperties();
        Assert.assertEquals(1, properties.getKeys().getSize());
        iterator = properties.getKeys().createIterator();
        Assert.assertEquals(new Chars("xmlns:math",ENC),iterator.next());
        Assert.assertEquals(new Chars("http://www.w3.org/1998/Math/MathML",ENC),properties.getValue(new Chars("xmlns:math",ENC)));

        // <firstName att=\"http://www.w3.org/1998/Math/MathML\" />----------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("firstName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        properties = stream.readProperties();
        Assert.assertEquals(1, properties.getKeys().getSize());
        iterator = properties.getKeys().createIterator();
        Assert.assertEquals(new Chars("att",ENC),iterator.next());
        Assert.assertEquals(new Chars("http://www.w3.org/1998/Math/MathML",ENC),properties.getValue(new Chars("att",ENC)));

        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("firstName",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        // </human> ------------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("human",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertNull(element);

    }

    @Test
    public void testReadTyped() throws Exception {

        XMLElement element = null;
        Dictionary properties = null;
        Iterator iterator = null;

        final XMLInputStream stream = new XMLInputStream();
        stream.setSkipEmptyText(true);
        stream.setEncoding(ENC);
        stream.setInput(new ArrayInputStream(XMLTestConstants.SAMPLE_DOCTYPE));

        // <?xml version="1.0" encoding="UTF-8"?> ------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_META,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("xml",ENC),element.getName());
        Assert.assertEquals(new Char('?',ENC),element.getEscape());

        properties = stream.readProperties();
        Assert.assertEquals(2, properties.getKeys().getSize());
        Assert.assertEquals(new Chars("1.0",ENC),properties.getValue(new Chars("version",ENC)));
        Assert.assertEquals(new Chars("utf-8",ENC),properties.getValue(new Chars("encoding",ENC)));

        // <!DOCTYPE humanize PUBLIC \"DTD UN PROJECT\" \"https://bitbucket.org/Eclesia/un/humanize.dtd\">
        element = stream.readElement();
        Assert.assertEquals(TYPE_DOCTYPE,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(null,element.getName());
        Assert.assertEquals(new Char('!',ENC),element.getEscape());
        Assert.assertEquals(new Chars(" humanize PUBLIC \"DTD UN PROJECT\" \"https://bitbucket.org/Eclesia/un/humanize.dtd\"",ENC),stream.readText());




        // <human> -------------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_START,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("human",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        properties = stream.readProperties();
        Assert.assertEquals(0, properties.getKeys().getSize());

        // </human> ------------------------------------------------------------
        element = stream.readElement();
        Assert.assertEquals(TYPE_ELEMENT_END,element.getType());
        Assert.assertEquals(null,element.getPrefix());
        Assert.assertEquals(new Chars("human",ENC),element.getName());
        Assert.assertEquals(new Char('/',ENC),element.getEscape());

        element = stream.readElement();
        Assert.assertNull(element);

    }

}
