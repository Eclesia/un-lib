
package science.unlicense.format.xml.dom;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLOutputStream;

/**
 *
 * @author Johann Sorel
 */
public class DomWriterTest {

     private static final CharEncoding ENC = CharEncodings.US_ASCII;

    @Test
    public void testWrite() throws Exception {

        //<human gender = "male" >
        //    <!--someone very important-->
        //    <firstName>hubert</firstName>
        //    <lastName>dupont</lastName>
        //    <single/>
        //</human>
        final DomElement human = new DomElement(new FName(null, new Chars("human", ENC)));
        human.getProperties().add(new Chars("gender", ENC), new Chars("male", ENC));
        final DomNode comment = new DomComment(new Chars("someone very important", ENC));
        final DomElement firstName = new DomElement(new FName(null, new Chars("firstName", ENC)));
        firstName.setText(new Chars("hubert", ENC));
        final DomElement lastName = new DomElement(new FName(null, new Chars("lastName", ENC)));
        lastName.setText(new Chars("dupont", ENC));
        final DomNode single = new DomElement(new FName(null, new Chars("single", ENC)));

        human.getChildren().add(comment);
        human.getChildren().add(firstName);
        human.getChildren().add(lastName);
        human.getChildren().add(single);

        final ArrayOutputStream back = new ArrayOutputStream();
        final XMLOutputStream stream = new XMLOutputStream();
        stream.setEncoding(ENC);
        stream.setIndent(new Chars("    ", ENC));
        stream.setOutput(back);

        final DomWriter writer = new DomWriter();
        writer.setOutput(stream);
        writer.write(human);
        writer.dispose();

        Chars result = new Chars(back.getBuffer().toArrayByte());

        final Chars expected = new Chars(
                  "<human gender=\"male\">\n"
                + "    <!--someone very important-->\n"
                + "    <firstName>hubert</firstName>\n"
                + "    <lastName>dupont</lastName>\n"
                + "    <single/>\n"
                + "</human>", ENC);
        Assert.assertEquals(expected, result);

    }
}
