package science.unlicense.format.xml;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;

/**
 * @author Johann Sorel
 */
public final class XMLTestConstants {

    public static final byte[] SAMPLE = new Chars(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<human gender = \"male\" >\n" +
            "    <!--someone very important-->\n" +
            "    <firstName>hubert</firstName>\n" +
            "    <lastName>dupont</lastName>\n" +
            "    <children>0</children>\n" +
            "    <single/>\n" +
            "</human>   \n" +
            "",
            CharEncodings.US_ASCII).toBytes();

    public static final byte[] SAMPLE_DOCTYPE = new Chars(
            "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
            "<!DOCTYPE humanize PUBLIC \"DTD UN PROJECT\" \"https://bitbucket.org/Eclesia/un/humanize.dtd\">\n" +
            "<human>\n" +
            "</human>\n" +
            "",
            CharEncodings.US_ASCII).toBytes();

    public static final byte[] SAMPLE_PREFIXED = new Chars(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<exp:human gender = \"male\" >\n" +
            "    <!--someone very important-->\n" +
            "    <att1:firstName>hubert</att1:firstName>\n" +
            "    <att2:lastName>dupont</att2:lastName>\n" +
            "    <att3:single/>\n" +
            "</exp:human>   \n" +
            "",
            CharEncodings.US_ASCII).toBytes();

    public static final byte[] SAMPLE_CHARESCAPE = new Chars(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<human xmlns:math=\"http://www.w3.org/1998/Math/MathML\" >\n" +
            "    <firstName att=\"http://www.w3.org/1998/Math/MathML\" />\n" +
            "</human>   \n" +
            "",
            CharEncodings.US_ASCII).toBytes();

}
