
package science.unlicense.format.xml;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class XMLConstants {

    public static final Chars NS_PREFIX = Chars.constant("xml");
    public static final Chars NS_NAMESPACE = Chars.constant("http://www.w3.org/XML/1998/namespace");
    /**
     * This namespace must be used for attributes which doesn't have any namespace.
     */
    public static final Chars NS_NONE = Chars.EMPTY;

    /** < character in unicode */
    public static final int CHAR_TAG_START = 60;
    /** > character in unicode */
    public static final int CHAR_TAG_END = 62;
    /** / character in unicode */
    public static final int CHAR_SLASH = 47;
    /** space character in unicode */
    public static final int CHAR_SPACE = 32;
    /** tab character in unicode */
    public static final int CHAR_TAB = 9;
    /** new line character in unicode */
    public static final int CHAR_NEWLINE = 10;
    /** carriage return in unicode */
    public static final int CHAR_CARRIAGE_RETURN = 13;
    /** : character in unicode */
    public static final int CHAR_DOUBLEDOT = 58;
    /** " character in unicode */
    public static final int CHAR_DOUBLEQUOTE = 34;
    /** = character in unicode */
    public static final int CHAR_EQUAL = 61;
    /** ? character in unicode */
    public static final int CHAR_QUESTION = 63;
    /** ! character in unicode */
    public static final int CHAR_ESCLAMATION = 33;

    public static final Chars TAG_XML = Chars.constant(new byte[]{'x','m','l'});
    public static final Chars ATT_XMLNS = Chars.constant(new byte[]{'x','m','l','n','s'});


    private XMLConstants() {}

}
