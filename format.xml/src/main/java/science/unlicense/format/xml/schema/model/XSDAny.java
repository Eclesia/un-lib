
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDAny extends XSDWildCard implements GNestedParticle,GParticle {

    public static final FName NAME = new FName(NS_BASE,new Chars("any",CharEncodings.US_ASCII));

    public static final FName ATT_MINOCCURS = new FName(XMLConstants.NS_NONE,new Chars("minOccurs",CharEncodings.US_ASCII));
    public static final FName ATT_MAXOCCURS = new FName(XMLConstants.NS_NONE,new Chars("maxOccurs",CharEncodings.US_ASCII));

    /**
     * <xs:attributeGroup ref="xs:occurs"/>
     */
    public Chars minOccurs;
    public Chars maxOccurs;

}
