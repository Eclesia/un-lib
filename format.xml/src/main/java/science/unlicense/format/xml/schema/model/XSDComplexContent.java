
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDComplexContent extends XSDAnnotated implements GComplexTypeModel{

    public static final FName NAME = new FName(NS_BASE,new Chars("complexContent",CharEncodings.US_ASCII));

    public static final FName ATT_MIXED = new FName(NS_BASE,new Chars("mixed",CharEncodings.US_ASCII));

    /**
     * <xs:choice>
     *   <xs:element name="restriction" type="xs:complexRestrictionType"/>
     *   <xs:element name="extension" type="xs:extensionType"/>
     * </xs:choice>
     */
    public XSDComplexRestrictionType restriction;
    public XSDExtensionType extension;

    /**
     * <xs:attribute name="mixed" type="xs:boolean">
     */
    public Boolean mixed;

}
