
package science.unlicense.format.xml.dom;

import science.unlicense.format.xml.NamespaceContext;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.DefaultNode;
import science.unlicense.common.api.model.tree.Node;

/**
 *
 * @author Johann Sorel
 */
public abstract class DomNode extends DefaultNode {

    private final NamespaceContext context;

    public DomNode() {
        this(new NamespaceContext());
    }

    public DomNode(NamespaceContext nsc) {
        super(true);
        CObjects.ensureNotNull(nsc);
        this.context = nsc;
    }

    public DomNode(Node[] children) {
        this(children, new NamespaceContext());
    }

    public DomNode(Node[] children, NamespaceContext nsc) {
        super(children);
        CObjects.ensureNotNull(nsc);
        this.context = nsc;
    }

    public NamespaceContext getNamespaceContext() {
        return context;
    }

    public Chars toChars() {
        return new Chars("DomNode");
    }

}
