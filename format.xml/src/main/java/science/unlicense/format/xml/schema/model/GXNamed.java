
package science.unlicense.format.xml.schema.model;

import science.unlicense.format.xml.FName;

/**
 * Not part of the XSD model, but convinient to regroup all types that have names.
 *
 * @author Johann Sorel
 */
public interface GXNamed {

    FName getName();

    void setName(FName name);

}
