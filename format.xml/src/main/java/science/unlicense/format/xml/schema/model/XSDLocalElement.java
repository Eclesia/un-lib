
package science.unlicense.format.xml.schema.model;

/**
 *
 * @author Johann Sorel
 */
public class XSDLocalElement extends XSDElement implements GNestedParticle,GParticle {

    /**
     * <xs:sequence>
     *   <xs:element ref="xs:annotation" minOccurs="0"/>
     *   <xs:choice minOccurs="0">
     *     <xs:element name="simpleType" type="xs:localSimpleType"/>
     *     <xs:element name="complexType" type="xs:localComplexType"/>
     *   </xs:choice>
     * <xs:group ref="xs:identityConstraint" minOccurs="0" maxOccurs="unbounded"/>
     * </xs:sequence>
     * <xs:attribute name="substitutionGroup" use="prohibited"/>
     * <xs:attribute name="final" use="prohibited"/>
     * <xs:attribute name="abstract" use="prohibited"/>
     * <xs:anyAttribute namespace="##other" processContents="lax"/>
     */

}
