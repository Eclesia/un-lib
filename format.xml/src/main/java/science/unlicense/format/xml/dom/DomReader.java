
package science.unlicense.format.xml.dom;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.NamespaceContext;
import science.unlicense.format.xml.XMLAttributes;
import science.unlicense.format.xml.XMLElement;
import science.unlicense.format.xml.XMLInputStream;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.Reader;

/**
 *
 * @author Johann Sorel
 */
public class DomReader implements Reader {

    private boolean close;
    private Object input;
    private XMLInputStream stream;

    public DomReader() {
    }

    @Override
    public final void setInput(Object source) {
        this.input = source;
        if (source instanceof XMLInputStream) {
            stream = (XMLInputStream) source;
            close = false;
        } else {
            stream = new XMLInputStream();
            stream.setInput(source);
            stream.setEncoding(CharEncodings.UTF_8);
            close = true;
        }
    }

    @Override
    public Object getInput() {
        return input;
    }

    @Override
    public void setConfiguration(Document configuration) {
    }

    @Override
    public Document getConfiguration() {
        return null;
    }

    public DomNode read() throws IOException {
        return fill(stream);
    }

    @Override
    public void dispose() throws IOException {
        if (close) {
            stream.dispose();
        }
    }

    private DomNode fill(XMLInputStream input) throws IOException {
        input.setSkipEmptyText(true); //we don't want blank texts
        DomNode node = null;
        XMLElement element;

        final Sequence stack = new ArraySequence();

        for (;;) {
            element = input.readElement();
            if (element == null) {
                break;
            }

            if (element.getType() == XMLElement.TYPE_ELEMENT_START) {
                //element
                final DomElement parent = stack.isEmpty() ? null : (DomElement) stack.get(stack.getSize()-1);
                final XMLAttributes atts = input.readProperties();

                //extract namespaces
                final NamespaceContext nsc = new NamespaceContext(parent == null ? null : parent.getNamespaceContext());
                atts.getNamespaceMappings(true, nsc);

                final Chars namespace = nsc.getNamespace(element.getPrefix());
                final FName fname = new FName(namespace, element.getName());

                final DomElement current = createNode(parent, nsc, fname, atts);
                stack.add(current);

                if (parent != null) {
                    //add in parent
                    parent.getChildren().add(current);
                }
            } else if (element.getType() == XMLElement.TYPE_ELEMENT_END) {

                if (stack.getSize() == 1) {
                    node = (DomNode) stack.get(stack.getSize()-1);
                }
                stack.remove(stack.getSize()-1);
            } else if (element.getType() == XMLElement.TYPE_COMMENT) {
                //comment
                final DomComment current = new DomComment(input.readText());
                if (stack.getSize()>0) {
                    //add in parent
                    ((DomNode) stack.get(stack.getSize()-1)).getChildren().add(current);
                }
            } else if (element.getType() == XMLElement.TYPE_TEXT) {
                if (stack.getSize()>0) {
                    final Chars text = input.readText();
                    final DomElement n = (DomElement) stack.get(stack.getSize()-1);
                    n.setText(text);
                }
            }
        }

        return node;
    }

    protected DomElement createNode(DomNode parent, NamespaceContext nsc, FName name, XMLAttributes atts) {
        final DomElement node = new DomElement(name, nsc);
        node.getProperties().addAll(atts);
        return node;
    }

}
