
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDExtensionType extends XSDAnnotated{

    public static final FName NAME = new FName(NS_BASE,new Chars("extension",CharEncodings.US_ASCII));

    public static final FName ATT_BASE = new FName(XMLConstants.NS_NONE,new Chars("base",CharEncodings.US_ASCII));

    /**
     * <xs:group ref="xs:typeDefParticle" minOccurs="0"/>
     */
    public GTypeDefParticle typeDefParticle;

    /**
     * <xs:group ref="xs:attrDecls"/>
     */
    public final Sequence attrDecls = new ArraySequence();
    public XSDWildCard anyAttribute;

    /**
     * <xs:attribute name="base" type="xs:QName" use="required"/>
     */
    public FName base;

}
