
package science.unlicense.format.xml.xpath;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.predicate.Predicate;

/**
 * TODO , need to study xpath spec
 *
 * @author Johann Sorel
 */
public class XPath {

    private final Sequence segments;

    public XPath(Sequence segments) {
        this.segments = segments;
    }

    public Sequence getSegments() {
        return segments;
    }

    public static final class Segment{

        private final Chars name;
        private final Predicate filter;

        public Segment(Chars name, Predicate filter) {
            this.name = name;
            this.filter = filter;
        }

        public Chars getName() {
            return name;
        }

        public Predicate getFilter() {
            return filter;
        }

    }

}
