
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.xml.FName;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDAnnotation extends XSDOpenAttrs{

    public static final FName NAME = new FName(NS_BASE,new Chars("annotation",CharEncodings.US_ASCII));

    public static final FName ATT_ID = new FName(NS_BASE,new Chars("id",CharEncodings.US_ASCII));

    /**
     * <xs:choice minOccurs="0" maxOccurs="unbounded">
     *     <xs:element ref="xs:appinfo"/>
     *     <xs:element ref="xs:documentation"/>
     * </xs:choice>
     */
    public final Sequence choice = new ArraySequence();

    /**
     * <xs:attribute name="id" type="xs:ID"/>
     */
    public Chars id;

}
