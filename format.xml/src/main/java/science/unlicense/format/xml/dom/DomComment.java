
package science.unlicense.format.xml.dom;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class DomComment extends DomNode {

    private Chars text;

    public DomComment() {
    }

    public DomComment(Chars text) {
        this.text = text;
    }

    public Chars getText() {
        return text;
    }

    public void setText(Chars text) {
        this.text = text;
    }

    public Chars toChars() {
        return new Chars("Comment : "+text);
    }

}
