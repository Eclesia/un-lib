
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDDocumentation {

    public static final FName NAME = new FName(NS_BASE,new Chars("documentation",CharEncodings.US_ASCII));

    public static final FName ATT_SOURCE = new FName(XMLConstants.NS_NONE,new Chars("source",CharEncodings.US_ASCII));
    public static final FName ATT_LANG = new FName(XMLConstants.NS_NAMESPACE,new Chars("lang",CharEncodings.US_ASCII));

    /**
     * <xs:complexType mixed="true">
     */
    public Chars text;

    /**
     * <xs:sequence minOccurs="0" maxOccurs="unbounded">
     *     <xs:any processContents="lax"/>
     * </xs:sequence>
     */
    public final Sequence any = new ArraySequence();

    /**
     * <xs:attribute name="source" type="xs:anyURI"/>
     */
    public Chars source;

    /**
     * <xs:attribute ref="xml:lang"/>
     */
    public Chars lang;

    /**
     * <xs:anyAttribute namespace="##other" processContents="lax"/>
     */
    public final Dictionary otherAttributes = new HashDictionary();

}
