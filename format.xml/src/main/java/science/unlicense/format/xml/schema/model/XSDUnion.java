
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * Part of group : simpleDerivation
 *
 * @author Johann Sorel
 */
public class XSDUnion extends XSDAnnotated implements GSimpleDerivation{

    public static final FName NAME = new FName(NS_BASE,new Chars("union",CharEncodings.US_ASCII));

    public static final FName ATT_MEMBER_TYPES = new FName(XMLConstants.NS_NONE,new Chars("memberTypes",CharEncodings.US_ASCII));

    /**
     * <xs:element name="simpleType" type="xs:localSimpleType" minOccurs="0" maxOccurs="unbounded"/>
     */
    public final Sequence simpleTypes = new ArraySequence();

    /**
     * <xs:attribute name="memberTypes" use="optional">
     *   <xs:simpleType>
     *     <xs:list itemType="xs:QName"/>
     *   </xs:simpleType>
     * </xs:attribute>
     */
    public Chars memberTypes;


}
