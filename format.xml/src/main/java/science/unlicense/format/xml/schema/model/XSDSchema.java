
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDSchema extends XSDOpenAttrs {

    public static final FName NAME = new FName(NS_BASE,new Chars("schema",CharEncodings.US_ASCII));

    public static final FName ATT_TARGETNAMESPACE = new FName(XMLConstants.NS_NONE,new Chars("targetNamespace",CharEncodings.US_ASCII));
    public static final FName ATT_VERSION = new FName(XMLConstants.NS_NONE,new Chars("version",CharEncodings.US_ASCII));
    public static final FName ATT_FINALDEFAULT = new FName(XMLConstants.NS_NONE,new Chars("finalDefault",CharEncodings.US_ASCII));
    public static final FName ATT_BLOCKDEFAULT = new FName(XMLConstants.NS_NONE,new Chars("blockDefault",CharEncodings.US_ASCII));
    public static final FName ATT_ELEMENTFORMDEFAULT = new FName(XMLConstants.NS_NONE,new Chars("elementFormDefault",CharEncodings.US_ASCII));
    public static final FName ATT_ATTRIBUTEFORMDEFAULT = new FName(XMLConstants.NS_NONE,new Chars("attributeFormDefault",CharEncodings.US_ASCII));
    public static final FName ATT_ID = new FName(XMLConstants.NS_NONE,new Chars("ID",CharEncodings.US_ASCII));
    public static final FName ATT_LANG = new FName(XMLConstants.NS_NAMESPACE,new Chars("lang",CharEncodings.US_ASCII));


    /**
     * <xs:choice minOccurs="0" maxOccurs="unbounded">
     * <xs:element ref="xs:include"/>
     * <xs:element ref="xs:import"/>
     * <xs:element ref="xs:redefine"/>
     * <xs:element ref="xs:annotation"/>
     * </xs:choice>
     */
    public final Sequence references = new ArraySequence();

    /**
     * <xs:sequence minOccurs="0" maxOccurs="unbounded">
     * <xs:group ref="xs:schemaTop"/>
     * <xs:element ref="xs:annotation" minOccurs="0" maxOccurs="unbounded"/>
     * </xs:sequence>
     */
    public final Sequence elements = new ArraySequence();
    /**
     * <xs:attribute name="targetNamespace" type="xs:anyURI"/>
     */
    public Chars targetNamespace;
    /**
     * <xs:attribute name="version" type="xs:token"/>
     */
    public Chars version;
    /**
     * <xs:attribute name="finalDefault" type="xs:fullDerivationSet" use="optional" default=""/>
     */
    public Chars[] finalDefault;
    /**
     * <xs:attribute name="blockDefault" type="xs:blockSet" use="optional" default=""/>
     */
    public Chars[] blockDefault;
    /**
     * <xs:attribute name="attributeFormDefault" type="xs:formChoice" use="optional" default="unqualified"/>
     */
    public FormChoice attributeFormDefault;
    /**
     * <xs:attribute name="elementFormDefault" type="xs:formChoice" use="optional" default="unqualified"/>
     */
    public FormChoice elementFormDefault;
    /**
     * <xs:attribute name="id" type="xs:ID"/>
     */
    public Chars id;
    /**
     * <xs:attribute ref="xml:lang"/>
     */
    public Chars lang;

    /**
     * Get first object in the element sequence with given name.
     *
     * @param name
     * @return
     */
    public Object getForName(Chars name) {

        for (int i=0,n=elements.getSize();i<n;i++) {
            Object ele = elements.get(i);
            if (ele instanceof XSDAnnotated && name.equals(((XSDAnnotated) ele).id)) {
                return ele;
            } else if (ele instanceof GXNamed && name.equals(((GXNamed) ele).getName())) {
                return ele;
            }
        }

        return null;
    }

}
