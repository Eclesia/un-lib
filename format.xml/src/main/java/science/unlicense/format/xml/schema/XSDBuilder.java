
package science.unlicense.format.xml.schema;

import java.util.Date;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.model.tree.DefaultNodeCardinality;
import science.unlicense.common.api.model.tree.DefaultNodeType;
import science.unlicense.common.api.model.tree.NodeCardinality;
import science.unlicense.common.api.model.tree.NodeType;
import science.unlicense.format.xml.FName;
import static science.unlicense.format.xml.schema.XSDConstants.*;
import science.unlicense.format.xml.schema.model.XSDAnnotated;
import science.unlicense.format.xml.schema.model.XSDAttribute;
import science.unlicense.format.xml.schema.model.XSDAttributeGroup;
import science.unlicense.format.xml.schema.model.XSDChoice;
import science.unlicense.format.xml.schema.model.XSDComplexType;
import science.unlicense.format.xml.schema.model.XSDDocumentation;
import science.unlicense.format.xml.schema.model.XSDElement;
import science.unlicense.format.xml.schema.model.XSDGroup;
import science.unlicense.format.xml.schema.model.XSDImport;
import science.unlicense.format.xml.schema.model.XSDInclude;
import science.unlicense.format.xml.schema.model.XSDList;
import science.unlicense.format.xml.schema.model.XSDRestriction;
import science.unlicense.format.xml.schema.model.XSDSchema;
import science.unlicense.format.xml.schema.model.XSDSimpleType;
import science.unlicense.format.xml.schema.model.XSDUnion;
import science.unlicense.concurrent.api.Paths;

/**
 * Utility class to rebuild types from a dom schema tree.
 *
 * @author Johann Sorel
 */
final class XSDBuilder {

    // FName > Class
    private final Dictionary typeMapping = new HashDictionary();
    // Path
    private final Sequence relatedSchemas = new ArraySequence();
    // FName
    private final Sequence declaredTypes = new ArraySequence();
    // FName
    private final Sequence declaredElements = new ArraySequence();
    // FName > Node for elements
    private final Dictionary elementlinks = new HashDictionary();
    // FName > Node for types
    private final Dictionary typelinks = new HashDictionary();

    private final Path basePath;
    private final XSDSchema root;
    private final boolean nsInNames;
    private Chars namespace;

    /**
     *
     * @param basePath
     * @param source
     * @param nsInNames
     */
    public XSDBuilder(Path basePath, XSDSchema source, boolean nsInNames) {
        this.basePath = basePath;
        this.root = source;
        this.nsInNames = nsInNames;
    }

    public Sequence getRelatedSchemas() {
        return relatedSchemas;
    }

    public Sequence getDeclaredElements() {
        return declaredElements;
    }

    public Sequence getDeclaredTypes() {
        return declaredTypes;
    }

    public void read() throws IOException {

        namespace = root.targetNamespace;

        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("boolean")), Boolean.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("byte")), Byte.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("integer")), Integer.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("long")), Long.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("float")), Float.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("double")), Double.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("unsignedByte")), Byte.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("unsignedInt")), Integer.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("unsignedLong")), Long.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("nonNegativeInteger")), Integer.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("positiveInteger")), Integer.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("date")), Date.class); //TODO need a replacement for date
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("dateTime")), Date.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("time")), Date.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("int")), Integer.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("anyURI")), Chars.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("token")), Chars.class);


        //all possible string types
        // http://www.w3schools.com/schema/schema_dtypes_string.asp
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("ENTITIES")), Chars.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("ENTITY")), Chars.class);
        // A string that represents the ID attribute in XML (only used with schema attributes)
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("ID")), Chars.class);
        // A string that represents the IDREF attribute in XML (only used with schema attributes)
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("IDREF")), Chars.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("IDREFS")), Chars.class);
        // A string that contains a valid language id
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("language")), Chars.class);
        // A string that contains a valid XML name
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("Name")), Chars.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("NCName")), Chars.class);
        // A string that represents the NMTOKEN attribute in XML (only used with schema attributes)
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("NMTOKEN")), Chars.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("NMTOKENS")), Chars.class);
        // A string that does not contain line feeds, carriage returns, or tabs
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("normalizedString")), Chars.class);
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("FName")), Chars.class);
        //A string
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("string")), Chars.class);
        //A string that does not contain line feeds, carriage returns, tabs, leading or trailing spaces, or multiple spaces
        typeMapping.add(new FName(XSDConstants.NS_BASE, new Chars("token")), Chars.class);

        readSchema();
    }

    private void readSchema() throws IOException{

        //just to find the main element
        final Iterator eleIte = root.elements.createIterator();
        while (eleIte.hasNext()) {
            final Object next = eleIte.next();

            if (next instanceof XSDElement) {
                final XSDElement element = (XSDElement) next;
                FName fid = null;
                if (element.id != null) {
                    fid = new FName(root.targetNamespace, element.id);
                } else if (element.name != null) {
                    fid = element.name;
                }
                if (fid!=null) {
                    declaredElements.add(fid);
                    elementlinks.add(fid, element);

                    //check if type is directly defined
                    boolean defined = element.type !=null || element.ref !=null;
                    if (!defined) {
                        if (element.simpleType!=null) {
                            readSimpleType(element.simpleType,fid);
                            defined = true;
                        } else if (element.complexType!=null) {
                            readComplexType(element.complexType,fid);
                            defined = true;
                        }
                    }

                    if (!defined) {
                        //element without definited type
                        typelinks.add(fid, element);
                    }
                }

            } else if (next instanceof XSDSimpleType) {
                readSimpleType((XSDSimpleType) next,null);

            } else if (next instanceof XSDComplexType) {
                readComplexType((XSDComplexType) next,null);

            } else if (next instanceof XSDAttribute) {
                readAttribute((XSDAttribute) next,null);

            } else if (next instanceof XSDAttributeGroup) {
                readAttributeGroup((XSDAttributeGroup) next,null);

            }

        }

        final Iterator refIte = root.references.createIterator();
        while (refIte.hasNext()) {
            final Object next = refIte.next();
            if (next instanceof XSDInclude) {
                final XSDInclude cdt = (XSDInclude) next;
                if (cdt.schemaLocation.startsWith(new Chars("http"))) {
                    relatedSchemas.add(Paths.resolve(cdt.schemaLocation));
                } else {
                    relatedSchemas.add(basePath.getParent().resolve(cdt.schemaLocation));
                }
            } else if (next instanceof XSDImport) {
                final XSDImport cdt = (XSDImport) next;
                if (cdt.schemaLocation==null) {
                    //this sometimes happen : <xs:import/>
                    //is it really valid ?
                    continue;
                }
                if (cdt.schemaLocation.startsWith(new Chars("http"))) {
                    relatedSchemas.add(Paths.resolve(cdt.schemaLocation));
                } else {
                    relatedSchemas.add(basePath.getParent().resolve(cdt.schemaLocation));
                }
            }
        }

    }

    private void readGroup(XSDGroup pelement, FName forceName) throws IOException{
        FName fid = null;
        if (pelement.id != null) {
            fid = new FName(root.targetNamespace, pelement.id);
        } else if (pelement.name != null) {
            fid = pelement.name;
        } else if (forceName != null) {
            fid = forceName;
        }

        if (fid!=null) {
            declaredTypes.add(fid);
            typelinks.add(fid, pelement);
        }
    }

    private void readAttributeGroup(XSDAttributeGroup pelement, FName forceName) throws IOException{
        FName fid = null;
        if (pelement.id != null) {
            fid = new FName(root.targetNamespace, pelement.id);
        } else if (pelement.name != null) {
            fid = pelement.name;
        } else if (forceName != null) {
            fid = forceName;
        }

        if (fid!=null) {
            declaredTypes.add(fid);
            typelinks.add(fid, pelement);
        }
    }

    private void readAttribute(final XSDAttribute pelement, FName forceName) throws IOException{
        FName fid = null;
        if (pelement.id != null) {
            fid = new FName(root.targetNamespace, pelement.id);
        } else if (pelement.name != null) {
            fid = pelement.name;
        } else if (forceName != null) {
            fid = forceName;
        }

        if (fid!=null) {
            declaredTypes.add(fid);
            typelinks.add(fid, pelement);
        }
    }

    private void readSimpleType(final XSDSimpleType pelement, FName forceName) throws IOException{
        FName fid = null;
        if (pelement.id != null) {
            fid = new FName(root.targetNamespace, pelement.id);
        } else if (pelement.name != null) {
            fid = pelement.name;
        } else if (forceName != null) {
            fid = forceName;
        }

        if (fid!=null) {
            declaredTypes.add(fid);
            typelinks.add(fid, pelement);
        }
    }

    private void readComplexType(XSDComplexType pelement, FName forceName) throws IOException{
        FName fid = null;
        if (pelement.id != null) {
            fid = new FName(root.targetNamespace, pelement.id);
        } else if (pelement.name != null) {
            fid = pelement.name;
        } else if (forceName != null) {
            fid = forceName;
        }

        if (fid!=null) {
            declaredTypes.add(fid);
            typelinks.add(fid, pelement);
        }
    }

    public NodeCardinality readDescriptor(FName id, XSDStack xsd) throws IOException{
        final XSDAnnotated element = (XSDAnnotated) elementlinks.getValue(id);
        if (element == null) {
            throw new IOException("XML Node for type :"+ id +" not found");
        }
        return readDescriptorInternal((XSDElement) element,id,xsd);
    }

    private NodeCardinality readDescriptorInternal(XSDElement element, FName id, XSDStack xsd) throws IOException{
        final Chars name = element.name.getLocalPart();
        final FName typeId = element.type;
        final FName refId = element.ref;
        final Chars strmin = element.minOccurs;
        final Chars strmax = element.maxOccurs;
        final Chars description = findDescription(element);

        final int min;
        final int max;
        if (strmin != null) {
            min = Int32.decode(strmin);
        } else {
            min = 1;
        }
        if (strmax != null) {
            if (VAL_UNBOUNDED.equals(strmax,true,true)) {
                max = Integer.MAX_VALUE;
            } else {
                max = Int32.decode(strmax);
            }
        } else {
            max = 1;
        }

        final NodeCardinality desc;
        if (typeId != null) {
            Class c = (Class) typeMapping.getValue(typeId);

            final NodeType type;
            if (c != null) {
                type = new DefaultNodeType(new Chars(c.getSimpleName()),null, null, c, false, null);
            } else {
                type = xsd.getType(typeId);
            }

            desc = new DefaultNodeCardinality(name,null, description, type, min, max);
        } else if (refId != null) {
            final NodeCardinality refDesc = xsd.getDescriptor(refId);
            desc = new DefaultNodeCardinality(refDesc.getId(),null, description, refDesc.getType(), min, max);
        } else {
            //self defined
            if (id==null) id = new FName(namespace, name);
            NodeType type;
            try{
                type = xsd.getType(id);
            }catch(IllegalStateException ex) {
                //no type, means it's a plain text value
                type = new DefaultNodeType(id.getLocalPart(),null, null, Chars.class, true, new NodeCardinality[0]);
            }
            desc = new DefaultNodeCardinality(id.getLocalPart(),null, description, type, min, max);
        }

        return desc;
    }

    public void readType(FName id, XSDStack xsd, DefaultNodeType nodeType) throws IOException{
        final XSDAnnotated pelement = (XSDAnnotated) typelinks.getValue(id);
        final Chars description = findDescription(pelement);

        if (pelement instanceof XSDSimpleType) {
            final Class binding = readRestriction((XSDSimpleType) pelement,xsd);
            nodeType.setId(id.getLocalPart());
            nodeType.setDescription(description);
            nodeType.setBinding(binding);
            nodeType.setNullable(false);
            nodeType.setChildrenTypes(null);
        } else if (pelement instanceof XSDComplexType) {
            final XSDComplexType ct = (XSDComplexType) pelement;

            final Sequence props = new ArraySequence();

            final Iterator ite = ct.attrDecls.createIterator();
            while (ite.hasNext()) {
                final Object next = ite.next();
                if (next instanceof XSDAttribute) {
                    readAttribute((XSDAttribute) next, props, xsd);
                }
            }

            if (ct.typeDefParticle instanceof XSDGroup) {
                readSequenceOrAll((XSDGroup) ct.typeDefParticle, props, xsd);
            } else if (ct.typeDefParticle instanceof XSDAttribute) {
                readAttribute((XSDAttribute) ct.typeDefParticle, props, xsd);
            }

            final NodeCardinality[] subs = new NodeCardinality[props.getSize()];
            Arrays.copy(props.toArray(), 0, subs.length, subs, 0);
            nodeType.setId(id.getLocalPart());
            nodeType.setDescription(description);
            nodeType.setBinding(Collection.class);
            nodeType.setNullable(true);
            nodeType.setChildrenTypes(subs);
        } else if (pelement instanceof XSDGroup) {
            final XSDGroup ct = (XSDGroup) pelement;

            final Sequence props = new ArraySequence();
            readSequenceOrAll(ct, props, xsd);

            final NodeCardinality[] subs = new NodeCardinality[props.getSize()];
            Arrays.copy(props.toArray(), 0, subs.length, subs, 0);
            nodeType.setId(id.getLocalPart());
            nodeType.setDescription(description);
            nodeType.setBinding(Collection.class);
            nodeType.setNullable(true);
            nodeType.setChildrenTypes(subs);
        } else {
            throw new IllegalStateException("Unexpected type "+pelement);
        }

        if (nsInNames) {
            //prepand the namespace on node type names
            nodeType.setId(namespace.concat(':').concat(nodeType.getId()));
        }

    }

    private void readSequenceOrAll(XSDGroup pelement, Sequence props, XSDStack xsd) throws IOException{

        final Iterator ite = pelement.particles.createIterator();
        while (ite.hasNext()) {
            final Object next = ite.next();
            if (next instanceof XSDElement) {
                props.add(readDescriptorInternal((XSDElement) next,null,xsd));
            } else if (next instanceof XSDChoice) {
                final XSDChoice choice = (XSDChoice) next;
                final Chars description = findDescription(choice);
                final Chars strmin = choice.minOccurs;
                final Chars strmax = choice.maxOccurs;
                final int max;
                if (strmax != null) {
                    if (VAL_UNBOUNDED.equals(strmax,true,true)) {
                        max = Integer.MAX_VALUE;
                    } else {
                        max = Int32.decode(strmax);
                    }
                } else {
                    max = 1;
                }

                final Sequence subEle = new ArraySequence();
                readSequenceOrAll(choice, subEle, xsd);
                //modify all choice cardinality
                for (int k=0,n=subEle.getSize();k<n;k++) {
                    final NodeCardinality nc = (NodeCardinality) subEle.get(k);
                    final NodeCardinality ncc = new DefaultNodeCardinality(
                            nc.getId(),null, description, nc.getType(), 0, max);
                    props.add(ncc);
                }
            } else if (next instanceof XSDGroup) {
                final XSDGroup group = (XSDGroup) next;
                final Chars description = findDescription(group);
                final Chars strmin = group.minOccurs;
                final Chars strmax = group.maxOccurs;
                final int min = 0;
                final int max;
                if (strmax != null) {
                    if (VAL_UNBOUNDED.equals(strmax,true,true)) {
                        max = Integer.MAX_VALUE;
                    } else {
                        max = Int32.decode(strmax);
                    }
                } else {
                    max = 1;
                }

                //get the type
                final NodeType type = xsd.getType(group.ref);

                Chars name = group.name.getLocalPart();
                if (name == null) name = group.id;
                if (name == null) name = group.ref.getLocalPart();

                final NodeCardinality card = new DefaultNodeCardinality(name,null,description, type, min, max);
                props.add(card);
            }
        }
    }

    private void readAttribute(XSDAttribute pelement, Sequence props, XSDStack xsd) throws IOException{
        final Chars name = pelement.name.getLocalPart();
        final FName typeId = pelement.type;
        final FName refId = pelement.ref;
        final Chars use = pelement.use;

        final int min;
        final int max;
        if (USE_REQUIRED.equals(use, true, true)) {
            min = 1;
            max = 1;
        } else if (USE_PROHIBITED.equals(use, true, true)) {
            min = 0;
            max = 0;
        } else if (use == null || USE_OPTIONAL.equals(use, true, true)) {
            min = 0;
            max = 1;
        } else {
            throw new IOException("unexpected use value : "+use);
        }

        final Chars description = findDescription(pelement);

        final NodeCardinality desc;
        if (typeId != null) {
            Class c = (Class) typeMapping.getValue(typeId);

            final NodeType type;
            if (c != null) {
                type = new DefaultNodeType(new Chars(c.getSimpleName()),null,null, c, false, null);
            } else {
                type = xsd.getType(typeId);
            }

            desc = new DefaultNodeCardinality(new Chars("att").concat(name),null, description, type, min, max);
        } else if (refId != null) {
            final NodeCardinality refDesc = xsd.getDescriptor(refId);
            desc = new DefaultNodeCardinality(refDesc.getId(),null, description, refDesc.getType(), min, max);
        } else {
            throw new IOException("not type for attribute : "+name);
        }

        props.add(desc);
    }

    private Class readRestriction(XSDSimpleType pelement, XSDStack xsd) throws IOException{

        FName classType = null;

        if (pelement.derivation instanceof XSDRestriction) {
            classType = ((XSDRestriction) pelement.derivation).base;
        } else if (pelement.derivation instanceof XSDList) {
            //TODO not exact
            return Sequence.class;
        } else if (pelement.derivation instanceof XSDUnion) {
            //TODO not exact
            return Object.class;
        }

        if (classType == null) {
            throw new IllegalStateException("No base type defined for element : "+ pelement);
        } else {
            Class c = (Class) typeMapping.getValue(classType);

            //it can be the name of another type
            if (c == null) {
                c = xsd.getType(classType).getValueClass();
            }

            if (c == null) {
                throw new InvalidArgumentException("Unexpected restrition base type : " + classType);
            }
            return c;
        }
    }

    /**
     * Search for Annotation and Description tag.
     * @param element
     */
    private static Chars findDescription(XSDAnnotated pelement) {
        //find description if any
        if (pelement.annotation!=null) {
            final Iterator ite = pelement.annotation.choice.createIterator();
            while (ite.hasNext()) {
                Object next = ite.next();
                if (next instanceof XSDDocumentation) {
                    return ((XSDDocumentation) next).text;
                }
            }
        }
        return null;
    }

}
