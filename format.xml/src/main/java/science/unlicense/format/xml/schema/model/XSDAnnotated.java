
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public abstract class XSDAnnotated extends XSDOpenAttrs {

    public static final FName ATT_ID = new FName(NS_BASE,new Chars("id",CharEncodings.US_ASCII));

    /**
     * <xs:element ref="xs:annotation" minOccurs="0"/>
     */
    public XSDAnnotation annotation;
    /**
     * <<xs:attribute name="id" type="xs:ID"/>
     */
    public Chars id;

}
