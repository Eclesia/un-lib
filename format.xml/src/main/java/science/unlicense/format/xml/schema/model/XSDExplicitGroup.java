
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDExplicitGroup extends XSDGroup implements GTypeDefParticle {

    public static final FName NAME_SEQUENCE = new FName(NS_BASE,new Chars("sequence",CharEncodings.US_ASCII));

    /**
     * <xs:sequence>
     * <xs:element ref="xs:annotation" minOccurs="0"/>
     * <xs:group ref="xs:nestedParticle" minOccurs="0" maxOccurs="unbounded"/>
     * </xs:sequence>
     * <xs:attribute name="name" type="xs:NCName" use="prohibited"/>
     * <xs:attribute name="ref" type="xs:QName" use="prohibited"/>
     * <xs:anyAttribute namespace="##other" processContents="lax"/>
     */
}
