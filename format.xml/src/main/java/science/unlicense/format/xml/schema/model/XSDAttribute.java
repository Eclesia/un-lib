
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 *
 * @author Johann Sorel
 */
public class XSDAttribute extends XSDAnnotated implements GAttrDecls,GSchemaTop,GXNamed {

    public static final FName NAME = new FName(NS_BASE,new Chars("attribute",CharEncodings.US_ASCII));

    public static final FName ATT_NAME = new FName(XMLConstants.NS_NONE,new Chars("name",CharEncodings.US_ASCII));
    public static final FName ATT_REF = new FName(XMLConstants.NS_NONE,new Chars("ref",CharEncodings.US_ASCII));
    public static final FName ATT_TYPE = new FName(XMLConstants.NS_NONE,new Chars("type",CharEncodings.US_ASCII));
    public static final FName ATT_USE = new FName(XMLConstants.NS_NONE,new Chars("use",CharEncodings.US_ASCII));
    public static final FName ATT_DEFAULT = new FName(XMLConstants.NS_NONE,new Chars("default",CharEncodings.US_ASCII));
    public static final FName ATT_FIXED = new FName(XMLConstants.NS_NONE,new Chars("fixed",CharEncodings.US_ASCII));
    public static final FName ATT_FORM = new FName(XMLConstants.NS_NONE,new Chars("form",CharEncodings.US_ASCII));

    /**
     * <xs:element name="simpleType" minOccurs="0" type="xs:localSimpleType"/>
     */
    public XSDSimpleType simpleType;

    /**
     * <xs:attributeGroup ref="xs:defRef"/>
     * -->
     * <xs:attribute name="name" type="xs:NCName"/>
     * <xs:attribute name="ref" type="xs:QName"/>
     */
    public FName name;
    public FName ref;

    /**
     * <xs:attribute name="type" type="xs:QName"/>
     */
    public FName type;

    /**
     * <xs:attribute name="use" use="optional" default="optional">
     *   <xs:simpleType>
     *     <xs:restriction base="xs:NMTOKEN">
     *       <xs:enumeration value="prohibited"/>
     *       <xs:enumeration value="optional"/>
     *       <xs:enumeration value="required"/>
     *     </xs:restriction>
     *   </xs:simpleType>
     * </xs:attribute>
     */
    public Chars use;

    /**
     * <xs:attribute name="default" type="xs:string"/>
     */
    public Chars defaut;

    /**
     * <xs:attribute name="fixed" type="xs:string"/>
     */
    public Chars fixed;

    /**
     * <xs:attribute name="form" type="xs:formChoice"/>
     */
    public FormChoice form;

    public FName getName() {
        return name;
    }

    public void setName(FName name) {
        this.name = name;
    }

}
