
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDAttributeGroup extends XSDAnnotated implements GAttrDecls,GRedefinable,GXNamed{

    public static final FName NAME = new FName(NS_BASE,new Chars("attributeGroup",CharEncodings.US_ASCII));

    public static final FName ATT_NAME = new FName(XMLConstants.NS_NONE,new Chars("name",CharEncodings.US_ASCII));
    public static final FName ATT_REF = new FName(XMLConstants.NS_NONE,new Chars("ref",CharEncodings.US_ASCII));

    /**
     * <xs:group ref="xs:attrDecls"/>
     */
    public final Sequence attrDecls = new ArraySequence();
    public XSDWildCard anyAttribute;

    /**
     * <xs:attributeGroup ref="xs:defRef"/>
     */
    public FName name;
    public FName ref;

    public FName getName() {
        return name;
    }

    public void setName(FName name) {
        this.name = name;
    }

}
