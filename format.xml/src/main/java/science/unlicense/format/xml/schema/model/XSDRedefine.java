
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDRedefine extends XSDOpenAttrs{

    public static final FName NAME = new FName(NS_BASE,new Chars("redefine",CharEncodings.US_ASCII));

    public static final FName ATT_ID = new FName(XMLConstants.NS_NONE,new Chars("id",CharEncodings.US_ASCII));
    public static final FName ATT_SCHEMA_LOCATION = new FName(XMLConstants.NS_NONE,new Chars("schemaLocation",CharEncodings.US_ASCII));

    /**
     * <xs:choice minOccurs="0" maxOccurs="unbounded">
     * <xs:element ref="xs:annotation"/>
     * <xs:group ref="xs:redefinable"/>
     * </xs:choice>
     */
    public Sequence choice;

    /**
     * <xs:attribute name="schemaLocation" type="xs:anyURI" use="required"/>
     */
    public Chars schemaLocation;
    /**
     * <xs:attribute name="id" type="xs:ID"/>
     */
    public Chars id;

}
