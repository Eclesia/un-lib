
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;

/**
 *
 * @author Johann Sorel
 */
public class TypeDerivationControl {


    public static final TypeDerivationControl EXTENSION = new TypeDerivationControl(new Chars("extension"));
    public static final TypeDerivationControl RESTRICTION = new TypeDerivationControl(new Chars("restriction"));
    public static final TypeDerivationControl LIST = new TypeDerivationControl(new Chars("list"));
    public static final TypeDerivationControl UNION = new TypeDerivationControl(new Chars("union"));

    private final Chars text;

    private TypeDerivationControl(Chars text) {
        this.text = text;
    }

    public Chars getText() {
        return text;
    }

    public static TypeDerivationControl fromText(Chars text) {
        if (text==null) return null;

        if (EXTENSION.text.equals(text, true, true)) {
            return EXTENSION;
        } else if (RESTRICTION.text.equals(text, true, true)) {
            return RESTRICTION;
        } else if (LIST.text.equals(text, true, true)) {
            return LIST;
        } else if (UNION.text.equals(text, true, true)) {
            return UNION;
        } else {
            throw new InvalidArgumentException("Unknowned value : "+text);
        }
    }

}
