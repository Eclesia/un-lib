
package science.unlicense.format.xml.dom;

import science.unlicense.format.xml.FName;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;

/**
 *
 * @author Johann Sorel
 */
public final class DomUtilities {

    private DomUtilities() {}

    public static DomElement getNodeForName(DomNode parent, Chars name) {
        final Iterator ite = parent.getChildren().createIterator();
        while (ite.hasNext()) {
            final Object next = ite.next();
            if (next instanceof DomElement) {
                final DomElement element = (DomElement) next;
                if (element.getName().getLocalPart().equals(name)) {
                    return element;
                }
            }
        }
        return null;
    }

    public static DomElement getNodeForName(DomNode parent, FName name) {
        final Iterator ite = parent.getChildren().createIterator();
        while (ite.hasNext()) {
            final Object next = ite.next();
            if (next instanceof DomElement) {
                final DomElement element = (DomElement) next;
                if (element.getName().equals(name)) {
                    return element;
                }
            }
        }
        return null;
    }
}
