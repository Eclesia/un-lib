
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;

/**
 *
 * @author Johann Sorel
 */
public abstract class XSDOpenAttrs {

    /**
     * FName -> Chars
     *
     * <xs:restriction base="xs:anyType">
     * <xs:anyAttribute namespace="##other" processContents="lax"/>
     * </xs:restriction>
     */
    public Dictionary otherAttributes = new HashDictionary();

}
