
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDWildCard extends XSDAnnotated implements GNestedParticle,GParticle {

    public static final FName NAME = new FName(NS_BASE,new Chars("wildcard",CharEncodings.US_ASCII));

    public static final FName ATT_NAMESPACE = new FName(XMLConstants.NS_NONE,new Chars("namespace",CharEncodings.US_ASCII));
    public static final FName ATT_PROCESSCONTENTS = new FName(XMLConstants.NS_NONE,new Chars("processContents",CharEncodings.US_ASCII));

    /**
     * <xs:attribute name="namespace" type="xs:namespaceList" use="optional" default="##any"/>
     */
    public Chars namespace;

    /**
     * <xs:attribute name="processContents" use="optional" default="strict">
     *   <xs:simpleType>
     *     <xs:restriction base="xs:NMTOKEN">
     *       <xs:enumeration value="skip"/>
     *       <xs:enumeration value="lax"/>
     *       <xs:enumeration value="strict"/>
     *     </xs:restriction>
     *   </xs:simpleType>
     * </xs:attribute>
     */
    public Chars processContents;

}
