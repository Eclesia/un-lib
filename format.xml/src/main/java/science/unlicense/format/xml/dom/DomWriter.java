
package science.unlicense.format.xml.dom;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLOutputStream;

/**
 *
 * @author Johann Sorel
 */
public class DomWriter {

    private XMLOutputStream output;

    public DomWriter() {
    }

    public void setOutput(XMLOutputStream output) {
        this.output = output;
    }

    public XMLOutputStream getOutput() {
        return output;
    }

    public void write(DomNode node) throws IOException{
        final XMLOutputStream stream = getOutput();

        if (node instanceof DomComment) {
            final DomComment cmt = (DomComment) node;
            stream.writeComment(cmt.getText());
        } else if (node instanceof DomElement) {
            final DomElement ele = (DomElement) node;
            final FName name = ele.getName();
            final Dictionary properties = ele.getProperties();
            //TODO namespace
            stream.writeElementStart(null, name.getLocalPart());
            final Iterator ite = properties.getPairs().createIterator();
            while (ite.hasNext()) {
                final Pair pair = (Pair) ite.next();
                stream.writeProperty((Chars) pair.getValue1(), CObjects.toChars(pair.getValue2()));
            }
            for (int i=0,n=ele.getChildren().getSize();i<n;i++) {
                write((DomNode) ele.getChildren().get(i));
            }
            if (ele.getText() != null) {
                stream.writeText(ele.getText());
            }

            stream.writeElementEnd(null, name.getLocalPart());
        } else {
            throw new IOException("Unexpected node type");
        }

    }

    public void dispose() throws IOException {
        output.dispose();
    }
}
