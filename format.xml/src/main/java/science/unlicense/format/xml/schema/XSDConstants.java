
package science.unlicense.format.xml.schema;

import science.unlicense.format.xml.FName;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class XSDConstants {

    public static final Chars NS_BASE = Chars.constant("http://www.w3.org/2001/XMLSchema");

    public static final FName TAG_SCHEMA = new FName(NS_BASE,new Chars(new byte[]{'s','c','h','e','m','a'}));
    public static final FName TAG_IMPORT = new FName(NS_BASE,new Chars(new byte[]{'i','m','p','o','r','t'}));
    public static final FName TAG_INCLUDE = new FName(NS_BASE,new Chars(new byte[]{'i','n','c','l','u','d','e'}));
    public static final FName TAG_ELEMENT = new FName(NS_BASE,new Chars(new byte[]{'e','l','e','m','e','n','t'}));
    public static final FName TAG_ANNOTATION = new FName(NS_BASE,new Chars(new byte[]{'a','n','n','o','t','a','t','i','o','n'}));
    public static final FName TAG_SIMPLETYPE = new FName(NS_BASE,new Chars(new byte[]{'s','i','m','p','l','e','T','y','p','e'}));
    public static final FName TAG_COMPLEXTYPE = new FName(NS_BASE,new Chars(new byte[]{'c','o','m','p','l','e','x','T','y','p','e'}));
    public static final FName TAG_RESTRICTION = new FName(NS_BASE,new Chars(new byte[]{'r','e','s','t','r','i','c','t','i','o','n'}));
    public static final FName TAG_LIST = new FName(NS_BASE,new Chars(new byte[]{'l','i','s','t'}));
    public static final FName TAG_UNION = new FName(NS_BASE,new Chars(new byte[]{'u','n','i','o','n'}));
    public static final FName TAG_ENUMERATION = new FName(NS_BASE,new Chars(new byte[]{'e','n','u','m','e','r','a','t','i','o','n'}));
    public static final FName TAG_SUBSTITUTION = new FName(NS_BASE,new Chars(new byte[]{'s','u','b','s','t','i','t','u','t','i','o','n'}));
    public static final FName TAG_ANYATTRIBUTE = new FName(NS_BASE,new Chars(new byte[]{'a','n','y','A','t','t','r','i','b','u','t','e'}));
    public static final FName TAG_NAMESPACE = new FName(NS_BASE,new Chars(new byte[]{'n','a','m','e','s','p','a','c','e'}));
    public static final FName TAG_PROCESSCONTENTS = new FName(NS_BASE,new Chars(new byte[]{'p','r','o','c','e','s','s','C','o','n','t','e','n','t','s'}));

    public static final FName TAG_ANY = new FName(NS_BASE,new Chars(new byte[]{'a','n','y'}));
    public static final FName TAG_ATTRIBUTE = new FName(NS_BASE,new Chars(new byte[]{'a','t','t','r','i','b','u','t','e'}));
    public static final FName TAG_ATTRIBUTEGROUP = new FName(NS_BASE,new Chars(new byte[]{'a','t','t','r','i','b','u','t','e','G','r','o','u','p'}));
    public static final FName TAG_GROUP = new FName(NS_BASE,new Chars(new byte[]{'g','r','o','u','p'}));
    public static final FName TAG_SIMPLECONTENT = new FName(NS_BASE,new Chars(new byte[]{'s','i','m','p','l','e','C','o','n','t','e','n','t'}));
    public static final FName TAG_EXTENSION = new FName(NS_BASE,new Chars(new byte[]{'e','x','t','e','n','s','i','o','n'}));
    public static final FName TAG_MIN_LENGTH = new FName(NS_BASE,new Chars(new byte[]{'m','i','n','L','e','n','g','t','h'}));
    public static final FName TAG_MAX_LENGTH = new FName(NS_BASE,new Chars(new byte[]{'m','a','x','L','e','n','g','t','h'}));
    public static final FName TAG_DOCUMENTATION = new FName(NS_BASE,new Chars(new byte[]{'d','o','c','u','m','e','n','t','a','t','i','o','n'}));

    public static final FName TAG_ALL = new FName(NS_BASE,new Chars(new byte[]{'a','l','l'}));
    public static final FName TAG_CHOICE = new FName(NS_BASE,new Chars(new byte[]{'c','h','o','i','c','e'}));
    public static final FName TAG_SEQUENCE = new FName(NS_BASE,new Chars(new byte[]{'s','e','q','u','e','n','c','e'}));


    public static final FName TAG_TYPE_PATTERN = new FName(NS_BASE,new Chars(new byte[]{'p','a','t','t','e','r','n'}));
    public static final FName TAG_TYPE_LIST = new FName(NS_BASE,new Chars(new byte[]{'l','i','s','t'}));
    public static final FName TAG_TYPE_DOUBLE = new FName(NS_BASE,new Chars(new byte[]{'d','o','u','b','l','e'}));
    public static final FName TAG_TYPE_LONG = new FName(NS_BASE,new Chars(new byte[]{'l','o','n','g'}));
    public static final FName TAG_TYPE_ULONG = new FName(NS_BASE,new Chars(new byte[]{'u','n','s','i','g','n','e','d','L','o','n','g'}));
    public static final FName TAG_TYPE_HEXBINARY = new FName(NS_BASE,new Chars(new byte[]{'h','e','x','B','i','n','a','r','y'}));
    public static final FName TAG_TYPE_NAME = new FName(NS_BASE,new Chars(new byte[]{'N','a','m','e'}));
    public static final FName TAG_TYPE_TOKEN = new FName(NS_BASE,new Chars(new byte[]{'t','o','k','e','n'}));
    public static final FName TAG_TYPE_ID = new FName(NS_BASE,new Chars(new byte[]{'i','d'}));

    public static final FName ATT_ID = new FName(NS_BASE,new Chars(new byte[]{'i','d'}));
    public static final FName ATT_NAME = new FName(NS_BASE,new Chars(new byte[]{'n','a','m','e'}));
    public static final FName ATT_BASE = new FName(NS_BASE,new Chars(new byte[]{'b','a','s','e'}));
    public static final FName ATT_TYPE = new FName(NS_BASE,new Chars(new byte[]{'t','y','p','e'}));
    public static final FName ATT_REF = new FName(NS_BASE,new Chars(new byte[]{'r','e','f'}));
    public static final FName ATT_USE = new FName(NS_BASE,new Chars(new byte[]{'u','s','e'}));
    public static final FName ATT_DEFAULT = new FName(NS_BASE,new Chars(new byte[]{'d','e','f','a','u','l','t'}));
    public static final FName ATT_MIN_OCCURS = new FName(NS_BASE,new Chars(new byte[]{'m','i','n','O','c','c','u','r','s'}));
    public static final FName ATT_MAX_OCCURS = new FName(NS_BASE,new Chars(new byte[]{'m','a','x','O','c','c','u','r','s'}));
    public static final FName ATT_VALUE = new FName(NS_BASE,new Chars(new byte[]{'v','a','l','u','e'}));
    public static final FName ATT_SCHEMA_LOCATION = new FName(NS_BASE,new Chars(new byte[]{'s','c','h','e','m','a','L','o','c','a','t','i','o','n'}));

    public static final Chars USE_REQUIRED = Chars.constant(new byte[]{'r','e','q','u','i','r','e','d'});
    public static final Chars USE_OPTIONAL = Chars.constant(new byte[]{'o','p','t','i','o','n','a','l'});
    public static final Chars USE_PROHIBITED = Chars.constant(new byte[]{'p','r','o','h','i','b','i','t','e','d'});

    public static final Chars VAL_UNBOUNDED = Chars.constant(new byte[]{'u','n','b','o','u','n','d','e','d'});

    private XSDConstants() {}

}
