
package science.unlicense.format.xml.xpath;

/**
 * A Resolver is an accessor capable of interpreting an xpath for a given kind of
 * object.
 * XPath are mainly used for xml documents but can also be used for more general
 * objets like nodes and beans.
 *
 * @author Johann Sorel
 */
public interface XPathResolver {

    /**
     * There might be multiple resolvers who can support the same object.
     * The priority ensure the most accurate one will be used.
     *
     * @return priority. high priority resolver should be tested first
     */
    float getPriority();

    /**
     * Test if this resolver can work with given class.
     *
     * @param clazz
     * @return true if clazz is supported
     */
    boolean canHandle(Class clazz);

    /**
     * Test if this resolver can work with given object.
     *
     * @param candidate
     * @return true if object is supported
     */
    boolean canHandle(Object candidate);

    /**
     * Resolve given xpath for object.
     *
     * @param path xpath
     * @param candidate object to evaluate
     * @return resolved value
     */
    Object resolve(XPath path, Object candidate);

}
