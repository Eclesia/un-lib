
package science.unlicense.format.xml;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;

/**
 * Qualified Name.
 *
 * Prefix + local part
 *
 * @author Johann Sorel
 */
public class QName extends CObject{

    private final Chars prefix;
    private final Chars name;

    public QName(Chars prefix, Chars name) {
        if (name == null) throw new IllegalStateException("Name can not be null.");
        this.prefix = prefix;
        this.name = name;
    }

    public Chars getPrefix() {
        return prefix;
    }

    public Chars getLocalPart() {
        return name;
    }

    public Chars toChars() {
        if (prefix==null) {
            return name;
        } else {
            return prefix.toChars().concat(' ').concat(name);
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final QName other = (QName) obj;
        if (this.prefix != other.prefix && (this.prefix == null || !this.prefix.equals(other.prefix))) {
            return false;
        }
        if (this.name != other.name && (this.name == null || !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

}
