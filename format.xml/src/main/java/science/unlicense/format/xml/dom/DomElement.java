
package science.unlicense.format.xml.dom;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.NamespaceContext;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;

/**
 *
 * @author Johann Sorel
 */
public class DomElement extends DomNode {

    private final Dictionary properties = new HashDictionary();
    private FName name;
    private Chars text;

    public DomElement(FName name) {
        this.name = name;
    }

    public DomElement(FName name, NamespaceContext nsc) {
        super(nsc);
        this.name = name;
    }

    public FName getName() {
        return name;
    }

    public void setName(FName name) {
        this.name = name;
    }

    public Chars getText() {
        return text;
    }

    public void setText(Chars text) {
        this.text = text;
    }

    public Dictionary getProperties() {
        return properties;
    }

    @Override
    public Chars toChars() {
        final CharBuffer sb = new CharBuffer();
        sb.append(new Chars(getName().toString(), CharEncodings.US_ASCII));
        if (text != null) {
            sb.append(text);
        }
        final Iterator ite = properties.getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            sb.append(new Char(' ', CharEncodings.US_ASCII));
            sb.append((Chars) pair.getValue1());
            sb.append(new Char(':', CharEncodings.US_ASCII));
            sb.append((Chars) pair.getValue2());
        }
        return sb.toChars();
    }
}
