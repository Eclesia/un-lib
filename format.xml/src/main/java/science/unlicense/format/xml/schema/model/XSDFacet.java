
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDFacet extends XSDAnnotated implements GFacets,GXNamed{

    public static final FName NAME_MIN_EXCLUSIVE = new FName(NS_BASE,new Chars("minExclusive",CharEncodings.US_ASCII));
    public static final FName NAME_MIN_INCLUSIVE= new FName(NS_BASE,new Chars("minInclusive",CharEncodings.US_ASCII));
    public static final FName NAME_MAX_EXCLUSIVE = new FName(NS_BASE,new Chars("maxExclusive",CharEncodings.US_ASCII));
    public static final FName NAME_MAX_INCLUSIVE = new FName(NS_BASE,new Chars("maxInclusive",CharEncodings.US_ASCII));
    public static final FName NAME_TOTALDIGITS = new FName(NS_BASE,new Chars("totalDigits",CharEncodings.US_ASCII));
    public static final FName NAME_FRACTIONDIGITS = new FName(NS_BASE,new Chars("fractionDigits",CharEncodings.US_ASCII));
    public static final FName NAME_LENGTH = new FName(NS_BASE,new Chars("length",CharEncodings.US_ASCII));
    public static final FName NAME_MINLENGTH = new FName(NS_BASE,new Chars("minLength",CharEncodings.US_ASCII));
    public static final FName NAME_MAXLENGTH = new FName(NS_BASE,new Chars("maxLength",CharEncodings.US_ASCII));
    public static final FName NAME_ENUMERATION = new FName(NS_BASE,new Chars("enumeration",CharEncodings.US_ASCII));
    public static final FName NAME_WHITESPACE = new FName(NS_BASE,new Chars("whiteSpace",CharEncodings.US_ASCII));
    public static final FName NAME_PATTERN = new FName(NS_BASE,new Chars("pattern",CharEncodings.US_ASCII));

    public static final FName ATT_VALUE = new FName(XMLConstants.NS_NONE,new Chars("value",CharEncodings.US_ASCII));
    public static final FName ATT_FIXED = new FName(XMLConstants.NS_NONE,new Chars("fixed",CharEncodings.US_ASCII));

    /**
     * differentiation of each facet by it's name.
     */
    public FName name;

    /**
     * <xs:attribute name="value" use="required"/>
     */
    public Chars value;

    /**
     * <xs:attribute name="fixed" type="xs:boolean" use="optional" default="false"/>
     */
    public Boolean fixed;

    public FName getName() {
        return name;
    }

    public void setName(FName name) {
        this.name = name;
    }

}
