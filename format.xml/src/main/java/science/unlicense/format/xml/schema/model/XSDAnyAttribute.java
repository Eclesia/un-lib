
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLAttributes;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 *
 * @author Johann Sorel
 */
public class XSDAnyAttribute implements GAttrDecls {

    public static final FName NAME = new FName(NS_BASE,new Chars("anyAttribute",CharEncodings.US_ASCII));

    public XMLAttributes atts;

}
