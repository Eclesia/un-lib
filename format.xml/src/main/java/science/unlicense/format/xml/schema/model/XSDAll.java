
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDAll extends XSDExplicitGroup implements GTypeDefParticle,GParticle {

    public static final FName NAME = new FName(NS_BASE,new Chars("all",CharEncodings.US_ASCII));

}
