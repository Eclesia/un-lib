
package science.unlicense.format.xml;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;

/**
 *
 * @author Johann Sorel
 */
public class XMLElement extends CObject{

    public static final int TYPE_ELEMENT_START = 1;
    public static final int TYPE_ELEMENT_END = 2;
    public static final int TYPE_TEXT = 3;
    public static final int TYPE_META = 4;
    public static final int TYPE_COMMENT = 5;
    public static final int TYPE_CDATA = 6;
    public static final int TYPE_DOCTYPE = 7;

    private final int type;
    private final Chars prefix;
    private final Chars name;
    private final Char escape;

    public XMLElement(int type, Chars prefix, Chars name, Char escape) {
        this.type = type;
        this.prefix = prefix;
        this.name = name;
        this.escape = escape;
    }

    public int getType() {
        return type;
    }

    public Chars getPrefix() {
        return prefix;
    }

    public Chars getName() {
        return name;
    }

    /**
     * Escape character for this element.
     * can be one of : / ? !
     * @return Char
     */
    public Char getEscape() {
        return escape;
    }

    public Chars toChars() {
        return new CharBuffer()
                .append(getName())
                .append(Int32.encode(type))
                .toChars();
    }

}
