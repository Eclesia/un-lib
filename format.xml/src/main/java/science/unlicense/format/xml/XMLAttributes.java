
package science.unlicense.format.xml;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;

/**
 * convenient dictionary storing xml element properties.
 *
 * @author Johann Sorel
 */
public class XMLAttributes extends HashDictionary {

    /**
     * Get all namespace mappings.
     *
     * @param remove true to remove read attribute from dictionary.
     * @param ctx NamespaceContext to insert namespaces into, can be null
     * @return mapping or null if no mapping defined
     */
    public NamespaceContext getNamespaceMappings(boolean remove, NamespaceContext ctx) {

        final Sequence toRemove = remove ? new ArraySequence() : null;
        final Iterator ite = getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            final Chars name = (Chars) pair.getValue1();
            if (name.startsWith(XMLConstants.ATT_XMLNS)) {
                if (ctx==null) ctx = new NamespaceContext();
                final int idx = name.getFirstOccurence(':');
                if (idx<0) {
                    ctx.add(null, (Chars) pair.getValue2());
                } else {
                    final Chars prefix = name.truncate(idx+1, name.getCharLength());
                    ctx.add(prefix, (Chars) pair.getValue2());
                }
                if (remove) toRemove.add(name);
            }
        }

        if (remove) {
            for (int i=0,n=toRemove.getSize();i<n;i++) {
                remove(toRemove.get(i));
            }
        }

        return ctx;
    }

    public Chars getValueChars(NamespaceContext nsctx, FName name) {
        return (Chars) getValue(nsctx.toQNameChars(name));
    }

    public Chars[] getValueCharsArray(NamespaceContext nsctx, FName name) {
        final Chars c = (Chars) getValue(nsctx.toQNameChars(name));
        if (c==null) return new Chars[0];
        return new Chars[]{c};
    }

    public Chars removeValueChars(NamespaceContext nsctx, FName name) {
        return (Chars) remove(nsctx.toQNameChars(name));
    }

    public FName removeValueFName(NamespaceContext nsctx, FName name) {
        final Chars val = (Chars) remove(nsctx.toQNameChars(name));
        if (val==null) return null;
        int idx = val.getFirstOccurence(':');
        if (idx<0) {
            return new FName(nsctx.getDefaultNamespace(), val);
        } else {
            return new FName(nsctx.getNamespace(val.truncate(0, idx)), val.truncate(idx+1, val.getCharLength()));
        }
    }

    public Chars[] removeValueCharsArray(NamespaceContext nsctx, FName name) {
        final Chars c = (Chars) remove(nsctx.toQNameChars(name));
        if (c==null) return new Chars[0];
        return new Chars[]{c};
    }

    public Boolean removeValueBoolean(NamespaceContext nsctx, FName name) {
        final Chars c = (Chars) remove(nsctx.toQNameChars(name));
        if (c==null) return null;
        return Boolean.valueOf(c.toString());
    }

}
