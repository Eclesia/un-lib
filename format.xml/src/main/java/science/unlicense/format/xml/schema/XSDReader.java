
package science.unlicense.format.xml.schema;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.NamespaceContext;
import science.unlicense.format.xml.XMLAttributes;
import static science.unlicense.format.xml.XMLConstants.*;
import science.unlicense.format.xml.XMLElement;
import static science.unlicense.format.xml.XMLElement.*;
import science.unlicense.format.xml.XMLInputStream;
import static science.unlicense.format.xml.schema.XSDConstants.*;
import science.unlicense.format.xml.schema.model.FormChoice;
import science.unlicense.format.xml.schema.model.XSDAll;
import science.unlicense.format.xml.schema.model.XSDAnnotated;
import science.unlicense.format.xml.schema.model.XSDAnnotation;
import science.unlicense.format.xml.schema.model.XSDAny;
import science.unlicense.format.xml.schema.model.XSDAnyAttribute;
import science.unlicense.format.xml.schema.model.XSDAppInfo;
import science.unlicense.format.xml.schema.model.XSDAttribute;
import science.unlicense.format.xml.schema.model.XSDAttributeGroup;
import science.unlicense.format.xml.schema.model.XSDChoice;
import science.unlicense.format.xml.schema.model.XSDComplexContent;
import science.unlicense.format.xml.schema.model.XSDComplexRestrictionType;
import science.unlicense.format.xml.schema.model.XSDComplexType;
import science.unlicense.format.xml.schema.model.XSDDocumentation;
import science.unlicense.format.xml.schema.model.XSDElement;
import science.unlicense.format.xml.schema.model.XSDExplicitGroup;
import science.unlicense.format.xml.schema.model.XSDExtensionType;
import science.unlicense.format.xml.schema.model.XSDFacet;
import science.unlicense.format.xml.schema.model.XSDGroup;
import science.unlicense.format.xml.schema.model.XSDGroupRef;
import science.unlicense.format.xml.schema.model.XSDImport;
import science.unlicense.format.xml.schema.model.XSDInclude;
import science.unlicense.format.xml.schema.model.XSDList;
import science.unlicense.format.xml.schema.model.XSDLocalElement;
import science.unlicense.format.xml.schema.model.XSDLocalSimpleType;
import science.unlicense.format.xml.schema.model.XSDNotation;
import science.unlicense.format.xml.schema.model.XSDOpenAttrs;
import science.unlicense.format.xml.schema.model.XSDRedefine;
import science.unlicense.format.xml.schema.model.XSDRestriction;
import science.unlicense.format.xml.schema.model.XSDRestrictionType;
import science.unlicense.format.xml.schema.model.XSDSchema;
import science.unlicense.format.xml.schema.model.XSDSimpleContent;
import science.unlicense.format.xml.schema.model.XSDSimpleExtensionType;
import science.unlicense.format.xml.schema.model.XSDSimpleRestrictionType;
import science.unlicense.format.xml.schema.model.XSDSimpleType;
import science.unlicense.format.xml.schema.model.XSDUnion;
import science.unlicense.format.xml.schema.model.XSDWildCard;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 *
 * @author Johann Sorel
 */
public class XSDReader extends AbstractReader{

    private XMLInputStream xs;
    private XMLElement ele;
    private NamespaceContext nsctx;

    public XSDSchema read() throws IOException{
        xs = new XMLInputStream();
        xs.setInput(getInput());
        xs.setSkipEmptyText(true);
        xs.setEncoding(CharEncodings.UTF_8);

        //xml header, sometimes missing
        ele = xs.readElement();
        if (TAG_XML.equals(ele.getName())) {
            readEle(true);
        } else {
            readEle(false);
        }

        //schema
        ensureType(TAG_SCHEMA, TYPE_ELEMENT_START);

        return readSchema();
    }

    private XSDSchema readSchema() throws IOException{
        final XSDSchema cdt = new XSDSchema();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.targetNamespace = atts.removeValueChars(nsctx,XSDSchema.ATT_TARGETNAMESPACE);
        cdt.version = atts.removeValueChars(nsctx,XSDSchema.ATT_VERSION);
        cdt.finalDefault = atts.removeValueCharsArray(nsctx,XSDSchema.ATT_FINALDEFAULT);
        cdt.blockDefault = atts.removeValueCharsArray(nsctx,XSDSchema.ATT_BLOCKDEFAULT);
        cdt.attributeFormDefault =  FormChoice.fromText(atts.removeValueChars(nsctx,XSDSchema.ATT_ATTRIBUTEFORMDEFAULT));
        cdt.elementFormDefault = FormChoice.fromText(atts.removeValueChars(nsctx,XSDSchema.ATT_ELEMENTFORMDEFAULT));
        cdt.id = atts.removeValueChars(nsctx,XSDSchema.ATT_ID);
        cdt.lang = atts.removeValueChars(nsctx,XSDSchema.ATT_LANG);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());

            //references
            if (XSDInclude.NAME.equals(fname)) {
                cdt.references.add(readInclude());
            } else if (XSDImport.NAME.equals(fname)) {
                cdt.references.add(readImport());
            } else if (XSDRedefine.NAME.equals(fname)) {
                cdt.references.add(readRedefine());
            } else if (XSDAnnotation.NAME.equals(fname)) {
                cdt.references.add(readAnnotation());
            }
            //elements
            else if (XSDSimpleType.NAME.equals(fname)) {
                cdt.elements.add(readSimpleType());
            } else if (XSDComplexType.NAME.equals(fname)) {
                cdt.elements.add(readComplexType());
            } else if (XSDGroup.NAME.equals(fname)) {
                cdt.elements.add(readGroup());
            } else if (XSDAttributeGroup.NAME.equals(fname)) {
                cdt.elements.add(readAttributeGroup());
            } else if (XSDElement.NAME.equals(fname)) {
                cdt.elements.add(readElement());
            } else if (XSDAttribute.NAME.equals(fname)) {
                cdt.elements.add(readAttribute());
            } else if (XSDNotation.NAME.equals(fname)) {
                cdt.elements.add(readNotation());
            }
            else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDInclude readInclude() throws IOException{
        final XSDInclude cdt = new XSDInclude();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.schemaLocation = atts.removeValueChars(nsctx,XSDInclude.ATT_SCHEMA_LOCATION);
        cdt.id = atts.removeValueChars(nsctx,XSDInclude.ATT_ID);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            throw new IOException("Unexpected tag : "+fname);
        }

        return cdt;
    }

    private XSDImport readImport() throws IOException{
        final XSDImport cdt = new XSDImport();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.schemaLocation = atts.removeValueChars(nsctx,XSDImport.ATT_SCHEMA_LOCATION);
        cdt.namespace = atts.removeValueChars(nsctx,XSDImport.ATT_NAMESPACE);
        cdt.id = atts.removeValueChars(nsctx,XSDImport.ATT_ID);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            throw new IOException("Unexpected tag : "+fname);
        }

        return cdt;
    }

    private XSDRedefine readRedefine() throws IOException{
        final XSDRedefine cdt = new XSDRedefine();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.schemaLocation = atts.removeValueChars(nsctx,XSDRedefine.ATT_SCHEMA_LOCATION);
        cdt.id = atts.removeValueChars(nsctx,XSDRedefine.ATT_ID);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.choice.add(readAnnotation());
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDAnnotation readAnnotation() throws IOException{
        final XSDAnnotation cdt = new XSDAnnotation();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDAnnotation.ATT_ID);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            if (XSDAppInfo.NAME.equals(fname)) {
                cdt.choice.add(readAppInfo());
            } else if (XSDDocumentation.NAME.equals(fname)) {
                cdt.choice.add(readDocumentation());
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDAppInfo readAppInfo() throws IOException{
        //TODO
        final XSDAppInfo cdt = new XSDAppInfo();
        xs.skipElement();
        return cdt;
    }

    private XSDDocumentation readDocumentation() throws IOException{
        //TODO
        final XSDDocumentation cdt = new XSDDocumentation();
        xs.skipElement();
        return cdt;
    }

    private XSDElement readElement() throws IOException{
        final XSDElement cdt = new XSDElement();
        readAbstractElement(cdt);
        return cdt;
    }

    private XSDAttribute readAttribute() throws IOException{
        final XSDAttribute cdt = new XSDAttribute();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDAttribute.ATT_ID);
        cdt.name = atts.removeValueFName(nsctx,XSDAttribute.ATT_NAME);
        cdt.ref = atts.removeValueFName(nsctx,XSDAttribute.ATT_REF);
        cdt.type = atts.removeValueFName(nsctx,XSDAttribute.ATT_TYPE);
        cdt.use = atts.removeValueChars(nsctx,XSDAttribute.ATT_USE);
        cdt.defaut = atts.removeValueChars(nsctx,XSDAttribute.ATT_DEFAULT);
        cdt.fixed = atts.removeValueChars(nsctx,XSDAttribute.ATT_FIXED);
        cdt.form = FormChoice.fromText(atts.removeValueChars(nsctx,XSDAttribute.ATT_FORM));
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());

            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            }
            //simple type
            else if (XSDSimpleType.NAME.equals(fname)) {
                cdt.simpleType = readLocalSimpleType();
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDNotation readNotation() throws IOException{
        final XSDNotation cdt = new XSDNotation();
        if (true) throw new IOException("Not supported yet");
        //read attributes
        final XMLAttributes atts = xs.readProperties();

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            throw new IOException("Unexpected tag : "+fname);
        }

        return cdt;
    }

    private XSDSimpleType readSimpleType() throws IOException{
        return readAbstractSimpleType(new XSDSimpleType());
    }

    private XSDComplexType readComplexType() throws IOException{
        final XSDComplexType cdt = new XSDComplexType();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDComplexType.ATT_ID);
        cdt.name = atts.removeValueFName(nsctx,XSDComplexType.ATT_NAME);
        cdt.mixed = atts.removeValueBoolean(nsctx,XSDComplexType.ATT_MIXED);
        cdt.abstrct = atts.removeValueBoolean(nsctx,XSDComplexType.ATT_ABSTRACT);
        cdt.finale = atts.removeValueChars(nsctx,XSDComplexType.ATT_FINAL);
        cdt.block = atts.removeValueChars(nsctx,XSDComplexType.ATT_BLOCK);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            }
            //complexTypeModel
            else if (XSDSimpleContent.NAME.equals(fname)) {
                cdt.simpleContent = readSimpleContent();
            } else if (XSDComplexContent.NAME.equals(fname)) {
                cdt.complexContent = readComplexContent();
            }
            //typeDefParticle
            else if (XSDGroupRef.NAME.equals(fname)) {
                cdt.typeDefParticle = readGroupRef();
            } else if (XSDAll.NAME.equals(fname)) {
                cdt.typeDefParticle = readAll();
            } else if (XSDChoice.NAME.equals(fname)) {
                cdt.typeDefParticle = readChoice();
            } else if (XSDExplicitGroup.NAME_SEQUENCE.equals(fname)) {
                cdt.typeDefParticle = readExplicitGroup();
            }
            //attrDecls
            else if (XSDAttribute.NAME.equals(fname)) {
                cdt.attrDecls.add(readAttribute());
            } else if (XSDAttributeGroup.NAME.equals(fname)) {
                cdt.attrDecls.add(readAttributeGroup());
            } else if (XSDAnyAttribute.NAME.equals(fname)) {
                cdt.anyAttribute = readAny();
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDAll readAll() throws IOException{
        final XSDAll cdt = new XSDAll();
        readAbstractGoup(cdt);
        return cdt;
    }

    private XSDChoice readChoice() throws IOException{
        final XSDChoice cdt = new XSDChoice();
        readAbstractGoup(cdt);
        return cdt;
    }

    private XSDExplicitGroup readExplicitGroup() throws IOException{
        final XSDExplicitGroup group = new XSDExplicitGroup();
        readAbstractGoup(group);
        return group;
    }

    private XSDSimpleContent readSimpleContent() throws IOException{
        final XSDSimpleContent cdt = new XSDSimpleContent();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDSimpleContent.ATT_ID);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            } else if (XSDSimpleRestrictionType.NAME.equals(fname)) {
                cdt.restriction = readSimpleRestrictionType();
            } else if (XSDSimpleExtensionType.NAME.equals(fname)) {
                cdt.extension = readSimpleExtensionType();
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDComplexContent readComplexContent() throws IOException{
        final XSDComplexContent cdt = new XSDComplexContent();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDComplexContent.ATT_ID);
        cdt.mixed = atts.removeValueBoolean(nsctx,XSDComplexContent.ATT_MIXED);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            } else if (XSDSimpleRestrictionType.NAME.equals(fname)) {
                cdt.restriction = readComplexRestrictionType();
            } else if (XSDSimpleExtensionType.NAME.equals(fname)) {
                cdt.extension = readExtensionType();
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDAnyAttribute readAnyAttribute() throws IOException{
        final XSDAnyAttribute cdt = new XSDAnyAttribute();
        if (true) throw new IOException("Not supported yet");
        //read attributes
        final XMLAttributes atts = xs.readProperties();

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            throw new IOException("Unexpected tag : "+fname);
        }

        return cdt;
    }

    private XSDComplexRestrictionType readComplexRestrictionType() throws IOException{
        final XSDComplexRestrictionType cdt = new XSDComplexRestrictionType();
        readAbstractRestrictionType(cdt);
        return cdt;
    }

    private XSDSimpleRestrictionType readSimpleRestrictionType() throws IOException{
        final XSDSimpleRestrictionType cdt = new XSDSimpleRestrictionType();
        readAbstractRestrictionType(cdt);
        return cdt;
    }

    private XSDExtensionType readExtensionType() throws IOException{
        final XSDExtensionType cdt = new XSDExtensionType();
        readAbstractExtensionType(cdt);
        return cdt;
    }

    private XSDSimpleExtensionType readSimpleExtensionType() throws IOException{
        final XSDSimpleExtensionType cdt = new XSDSimpleExtensionType();
        readAbstractExtensionType(cdt);
        return cdt;
    }

    private XSDGroup readGroup() throws IOException{
        final XSDGroup cdt = new XSDGroup();
        readAbstractGoup(cdt);
        return cdt;
    }

    private XSDGroupRef readGroupRef() throws IOException{
        final XSDGroupRef cdt = new XSDGroupRef();
        readAbstractGoup(cdt);
        return cdt;
    }

    private XSDAttributeGroup readAttributeGroup() throws IOException{
        final XSDAttributeGroup cdt = new XSDAttributeGroup();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDAttribute.ATT_ID);
        cdt.name = atts.removeValueFName(nsctx,XSDAttribute.ATT_NAME);
        cdt.ref = atts.removeValueFName(nsctx,XSDAttribute.ATT_REF);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());

            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            }
            //attrDecls
            else if (XSDAttribute.NAME.equals(fname)) {
                cdt.attrDecls.add(readAttribute());
            } else if (XSDAttributeGroup.NAME.equals(fname)) {
                cdt.attrDecls.add(readAttributeGroup());
            } else if (XSDAnyAttribute.NAME.equals(fname)) {
                cdt.anyAttribute = readAny();
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDRestriction readRestriction() throws IOException{
        final XSDRestriction cdt = new XSDRestriction();
        readAbstractRestriction(cdt);
        return cdt;
    }

    private XSDFacet readFacet(FName name) throws IOException{
        final XSDFacet cdt = new XSDFacet();
        cdt.name = name;

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDFacet.ATT_ID);
        cdt.value = atts.removeValueChars(nsctx,XSDFacet.ATT_VALUE);
        Chars fixed = atts.removeValueChars(nsctx,XSDFacet.ATT_FIXED);
        cdt.fixed = fixed==null ? null : Boolean.valueOf(fixed.toString());
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDList readList() throws IOException{
        final XSDList cdt = new XSDList();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDList.ATT_ID);
        cdt.itemType = atts.removeValueFName(nsctx,XSDList.ATT_ITEMTYPE);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDUnion readUnion() throws IOException{
        final XSDUnion cdt = new XSDUnion();

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDUnion.ATT_ID);
        cdt.memberTypes = atts.removeValueChars(nsctx,XSDUnion.ATT_MEMBER_TYPES);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            }
            //simple types
            else if (XSDSimpleType.NAME.equals(fname)) {
                cdt.simpleTypes.add(readLocalSimpleType());
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDAny readAny() throws IOException{
        final XSDAny cdt = new XSDAny();
        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.minOccurs = atts.removeValueChars(nsctx, XSDAny.ATT_MINOCCURS);
        cdt.maxOccurs = atts.removeValueChars(nsctx, XSDAny.ATT_MAXOCCURS);

        readAbstractWildcard(cdt);
        return cdt;
    }

    private XSDLocalElement readLocalElement() throws IOException{
        final XSDLocalElement cdt = new XSDLocalElement();
        readAbstractElement(cdt);
        return cdt;
    }

    private XSDLocalSimpleType readLocalSimpleType() throws IOException{
        final XSDLocalSimpleType cdt = new XSDLocalSimpleType();
        readAbstractSimpleType(cdt);
        return cdt;
    }

    private XSDExtensionType readAbstractExtensionType(XSDExtensionType cdt) throws IOException{

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDExtensionType.ATT_ID);
        cdt.base = atts.removeValueFName(nsctx,XSDExtensionType.ATT_BASE);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            }
            //typeDefParticle
            else if (XSDAll.NAME.equals(fname)) {
                cdt.typeDefParticle = readAll();
            } else if (XSDChoice.NAME.equals(fname)) {
                cdt.typeDefParticle = readChoice();
            } else if (XSDExplicitGroup.NAME_SEQUENCE.equals(fname)) {
                cdt.typeDefParticle = readExplicitGroup();
            }
            //attrDecls
            else if (XSDAttribute.NAME.equals(fname)) {
                cdt.attrDecls.add(readAttribute());
            } else if (XSDAttributeGroup.NAME.equals(fname)) {
                cdt.attrDecls.add(readAttributeGroup());
            } else if (XSDAnyAttribute.NAME.equals(fname)) {
                cdt.anyAttribute = readAny();
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDElement readAbstractElement(XSDElement cdt) throws IOException{

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDElement.ATT_ID);
        cdt.name = atts.removeValueFName(nsctx,XSDElement.ATT_NAME);
        cdt.ref = atts.removeValueFName(nsctx,XSDElement.ATT_REF);
        cdt.type = atts.removeValueFName(nsctx,XSDElement.ATT_TYPE);
        cdt.substitutionGroup = atts.removeValueFName(nsctx,XSDElement.ATT_SUBSTITUTION_GROUP);
        cdt.minOccurs = atts.removeValueChars(nsctx,XSDElement.ATT_MINOCCURS);
        cdt.maxOccurs = atts.removeValueChars(nsctx,XSDElement.ATT_MAXOCCURS);
        cdt.defaut = atts.removeValueChars(nsctx,XSDElement.ATT_DEFAULT);
        cdt.fixed = atts.removeValueChars(nsctx,XSDElement.ATT_FIXED);
        cdt.nillable = atts.removeValueBoolean(nsctx,XSDElement.ATT_NILLABLE);
        cdt.abstrat = atts.removeValueBoolean(nsctx,XSDElement.ATT_ABSTRACT);
        cdt.finale = atts.removeValueChars(nsctx,XSDElement.ATT_FINAL);
        cdt.block = atts.removeValueChars(nsctx,XSDElement.ATT_BLOCK);
        cdt.form = FormChoice.fromText(atts.removeValueChars(nsctx,XSDElement.ATT_FORM));
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            }
            //particles
            else if (XSDSimpleType.NAME.equals(fname)) {
                cdt.simpleType = readSimpleType();
            } else if (XSDComplexType.NAME.equals(fname)) {
                cdt.complexType = readComplexType();
            }
            else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDGroup readAbstractGoup(XSDGroup cdt) throws IOException{

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDGroup.ATT_ID);
        cdt.name = atts.removeValueFName(nsctx,XSDGroup.ATT_NAME);
        cdt.ref = atts.removeValueFName(nsctx,XSDGroup.ATT_REF);
        cdt.minOccurs = atts.removeValueChars(nsctx,XSDGroup.ATT_MINOCCURS);
        cdt.maxOccurs = atts.removeValueChars(nsctx,XSDGroup.ATT_MAXOCCURS);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            }
            //particles
            else if (XSDLocalElement.NAME.equals(fname)) {
                cdt.particles.add(readLocalElement());
            } else if (XSDGroup.NAME.equals(fname)) {
                cdt.particles.add(readGroup());
            } else if (XSDAll.NAME.equals(fname)) {
                cdt.particles.add(readAll());
            } else if (XSDChoice.NAME.equals(fname)) {
                cdt.particles.add(readChoice());
            } else if (XSDExplicitGroup.NAME_SEQUENCE.equals(fname)) {
                cdt.particles.add(readExplicitGroup());
            } else if (XSDAny.NAME.equals(fname)) {
                cdt.particles.add(readAny());
            }
            else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDSimpleType readAbstractSimpleType(XSDSimpleType cdt) throws IOException{

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDSimpleType.ATT_ID);
        cdt.finale = atts.removeValueChars(nsctx,XSDSimpleType.ATT_FINAL);
        cdt.name = atts.removeValueFName(nsctx,XSDSimpleType.ATT_NAME);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            }
            //derivation
            else if (XSDRestriction.NAME.equals(fname)) {
                cdt.derivation = readRestriction();
            } else if (XSDList.NAME.equals(fname)) {
                cdt.derivation = readList();
            } else if (XSDUnion.NAME.equals(fname)) {
                cdt.derivation = readUnion();
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDRestriction readAbstractRestriction(XSDRestriction cdt) throws IOException{

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDRestriction.ATT_ID);
        cdt.base = atts.removeValueFName(nsctx,XSDRestriction.ATT_BASE);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            }
            //simple type
            else if (XSDRestriction.NAME.equals(fname)) {
                cdt.simpleType = readLocalSimpleType();
            }
            //facets
            else if (XSDFacet.NAME_MIN_EXCLUSIVE.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MIN_EXCLUSIVE));
            } else if (XSDFacet.NAME_MIN_INCLUSIVE.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MIN_INCLUSIVE));
            } else if (XSDFacet.NAME_MAX_EXCLUSIVE.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MAX_EXCLUSIVE));
            } else if (XSDFacet.NAME_MAX_INCLUSIVE.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MAX_INCLUSIVE));
            } else if (XSDFacet.NAME_TOTALDIGITS.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_TOTALDIGITS));
            } else if (XSDFacet.NAME_FRACTIONDIGITS.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_FRACTIONDIGITS));
            } else if (XSDFacet.NAME_LENGTH.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_LENGTH));
            } else if (XSDFacet.NAME_MINLENGTH.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MINLENGTH));
            } else if (XSDFacet.NAME_MAXLENGTH.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MAXLENGTH));
            } else if (XSDFacet.NAME_ENUMERATION.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_ENUMERATION));
            } else if (XSDFacet.NAME_WHITESPACE.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_WHITESPACE));
            } else if (XSDFacet.NAME_PATTERN.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_PATTERN));
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDRestrictionType readAbstractRestrictionType(XSDRestrictionType cdt) throws IOException{

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDRestrictionType.ATT_ID);
        cdt.base = atts.removeValueFName(nsctx,XSDRestrictionType.ATT_BASE);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            }
            //typeDefParticle
            else if (XSDAll.NAME.equals(fname)) {
                cdt.typeDefParticle = readAll();
            } else if (XSDChoice.NAME.equals(fname)) {
                cdt.typeDefParticle = readChoice();
            } else if (XSDExplicitGroup.NAME_SEQUENCE.equals(fname)) {
                cdt.typeDefParticle = readExplicitGroup();
            }
            //simpleRestrictionModel
            else if (XSDLocalSimpleType.NAME.equals(fname)) {
                cdt.simpleType = readLocalSimpleType();
            }
            //facets
            else if (XSDFacet.NAME_MIN_EXCLUSIVE.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MIN_EXCLUSIVE));
            } else if (XSDFacet.NAME_MIN_INCLUSIVE.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MIN_INCLUSIVE));
            } else if (XSDFacet.NAME_MAX_EXCLUSIVE.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MAX_EXCLUSIVE));
            } else if (XSDFacet.NAME_MAX_INCLUSIVE.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MAX_INCLUSIVE));
            } else if (XSDFacet.NAME_TOTALDIGITS.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_TOTALDIGITS));
            } else if (XSDFacet.NAME_FRACTIONDIGITS.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_FRACTIONDIGITS));
            } else if (XSDFacet.NAME_LENGTH.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_LENGTH));
            } else if (XSDFacet.NAME_MINLENGTH.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MINLENGTH));
            } else if (XSDFacet.NAME_MAXLENGTH.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_MAXLENGTH));
            } else if (XSDFacet.NAME_ENUMERATION.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_ENUMERATION));
            } else if (XSDFacet.NAME_WHITESPACE.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_WHITESPACE));
            } else if (XSDFacet.NAME_PATTERN.equals(fname)) {
                cdt.facets.add(readFacet(XSDFacet.NAME_PATTERN));
            }

            //attrDecls
            else if (XSDAttribute.NAME.equals(fname)) {
                cdt.attrDecls.add(readAttribute());
            } else if (XSDAttributeGroup.NAME.equals(fname)) {
                cdt.attrDecls.add(readAttributeGroup());
            }
            else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDWildCard readAbstractWildcard(XSDWildCard cdt) throws IOException{

        //read attributes
        final XMLAttributes atts = xs.readProperties();
        cdt.id = atts.removeValueChars(nsctx,XSDWildCard.ATT_ID);
        cdt.namespace = atts.removeValueChars(nsctx,XSDWildCard.ATT_NAMESPACE);
        cdt.processContents = atts.removeValueChars(nsctx,XSDWildCard.ATT_PROCESSCONTENTS);
        readOpenAttrs(cdt);

        //read sub elements
        for (readEle(); ele!=null;readEle()) {
            final int type = ele.getType();
            if (type==XMLElement.TYPE_ELEMENT_END) break;
            if (type!=XMLElement.TYPE_ELEMENT_START) continue;

            final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
            //annotation
            if (XSDAnnotation.NAME.equals(fname)) {
                cdt.annotation = readAnnotation();
            } else {
                throw new IOException("Unexpected tag : "+fname);
            }
        }

        return cdt;
    }

    private XSDAnnotated readOther() throws IOException{
        final FName fname = new FName(nsctx.getNamespace(ele.getPrefix()), ele.getName());
        if (XSDElement.NAME.equals(fname)) {
            return readElement();
        } else if (XSDGroup.NAME.equals(fname)) {
            return readGroup();
        } else if (XSDAttribute.NAME.equals(fname)) {
            return readAttribute();
        } else if (XSDAttributeGroup.NAME.equals(fname)) {
            return readAttributeGroup();
        } else if (XSDAll.NAME.equals(fname)) {
            return readAll();
        } else if (XSDAny.NAME.equals(fname)) {
            return readAny();
        } else {
            throw new IOException("Unexpected tag : "+fname);
        }
    }

    private void readOpenAttrs(XSDOpenAttrs cdt) throws IOException{
        final XMLAttributes atts = xs.readProperties();
        final Iterator ite = atts.getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            final FName name = nsctx.toFName((Chars) pair.getValue1());
            cdt.otherAttributes.add(name, pair.getValue2());
        }
    }

    private void readEle() throws IOException{
        readEle(true);
    }

    private void readEle(boolean forward) throws IOException{
        if (forward) ele = xs.readElement();
        final int type = ele.getType();
        if (type == XMLElement.TYPE_ELEMENT_START) {
            if (nsctx==null) nsctx = new NamespaceContext();
            else nsctx = nsctx.createChildContext();
            final XMLAttributes atts = xs.readProperties();
            atts.getNamespaceMappings(true, nsctx);
        } else if (type == XMLElement.TYPE_ELEMENT_END) {
            nsctx = nsctx.getParent();
        }
    }

    private void ensureType(Chars name, int type) throws IOException{
        if (!CObjects.equals(name,ele.getName())) {
            throw new IOException("Unexpected element : "+ele);
        }
        if (ele.getType()!=type) {
            throw new IOException("Unexpected element : "+ele);
        }
    }

    private void ensureType(FName name, int type) throws IOException{
        if (ele.getType()!=type) {
            throw new IOException("Unexpected element : "+ele);
        }
        if (!name.getLocalPart().equals(ele.getName())) {
            throw new IOException("Unexpected element : "+ele);
        }
        if (!CObjects.equals(name.getNamespace(),nsctx.getNamespace(ele.getPrefix()))) {
            throw new IOException("Unexpected element : "+ele);
        }
    }

}
