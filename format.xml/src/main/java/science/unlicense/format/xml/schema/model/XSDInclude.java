
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDInclude extends XSDAnnotated{

    public static final FName NAME = new FName(NS_BASE,new Chars("include",CharEncodings.US_ASCII));

    public static final FName ATT_SCHEMA_LOCATION = new FName(XMLConstants.NS_NONE,new Chars("schemaLocation",CharEncodings.US_ASCII));

    /**
     * <xs:attribute name="schemaLocation" type="xs:anyURI" use="required"/>
     */
    public Chars schemaLocation;
}
