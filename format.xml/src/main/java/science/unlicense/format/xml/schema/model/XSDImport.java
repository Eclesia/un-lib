
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDImport extends XSDAnnotated{

    public static final FName NAME = new FName(NS_BASE,new Chars("import",CharEncodings.US_ASCII));

    public static final FName ATT_NAMESPACE = new FName(XMLConstants.NS_NONE,new Chars("namespace",CharEncodings.US_ASCII));
    public static final FName ATT_SCHEMA_LOCATION = new FName(XMLConstants.NS_NONE,new Chars("schemaLocation",CharEncodings.US_ASCII));

    /**
     * <xs:attribute name="namespace" type="xs:anyURI"/>
     */
    public Chars namespace;
    /**
     * <xs:attribute name="schemaLocation" type="xs:anyURI"/>
     */
    public Chars schemaLocation;

}
