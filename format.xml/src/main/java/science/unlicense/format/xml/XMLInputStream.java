
package science.unlicense.format.xml;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

import static science.unlicense.format.xml.XMLConstants.*;

/**
 *
 * @author Johann Sorel
 */
public class XMLInputStream {


    //grammar xml element in given encoding.
    private byte[] tagStart;
    private byte[] tagEnd;
    private byte[] slash;
    private byte[] space;
    private byte[] tab;
    private byte[] jumpline;
    private byte[] cariagereturn;
    private byte[] pseparation;
    private byte[] quote;
    private byte[] equal;
    private byte[] question;
    private byte[] esclamation;
    private byte[] commentStart;
    private byte[] commentEnd;
    private byte[] cdataStart;
    private byte[] cdataEnd;
    private byte[] doctype;

    private final ByteSequence buffer = new ByteSequence();
    private boolean prepeek = false;

    private Object input;
    private CharEncoding encoding;
    private boolean closeStream = false;
    private boolean skipEmptyText = false;
    private boolean currentTextIsBlank = false;
    private CharInputStream stream;
    private XMLElement currentElement = null;
    private XMLElement previousElement = null;
    private XMLAttributes currentProperties = null;
    private Chars currentText = null;


    public final void setInput(Object source) {
        this.input = source;
    }

    public void setEncoding(CharEncoding encoding) {
        this.encoding = encoding;
        this.tagStart = encoding.toBytes(CHAR_TAG_START);
        this.tagEnd = encoding.toBytes(CHAR_TAG_END);
        this.slash = encoding.toBytes(CHAR_SLASH);
        this.space = encoding.toBytes(CHAR_SPACE);
        this.tab = encoding.toBytes(CHAR_TAB);
        this.jumpline = encoding.toBytes(CHAR_NEWLINE);
        this.cariagereturn = encoding.toBytes(CHAR_CARRIAGE_RETURN);
        this.pseparation = encoding.toBytes(CHAR_DOUBLEDOT);
        this.quote = encoding.toBytes(CHAR_DOUBLEQUOTE);
        this.equal = encoding.toBytes(CHAR_EQUAL);
        this.question = encoding.toBytes(CHAR_QUESTION);
        this.esclamation = encoding.toBytes(CHAR_ESCLAMATION);
        this.commentStart = Arrays.concatenate(new byte[][]{ // --
            encoding.toBytes(45),
            encoding.toBytes(45)
            });
        this.commentEnd = Arrays.concatenate(new byte[][]{ // -->
            encoding.toBytes(45),
            encoding.toBytes(45),
            encoding.toBytes(CHAR_TAG_END)
            });
        this.cdataStart = Arrays.concatenate(new byte[][]{ // [CDATA[
            encoding.toBytes(91),
            encoding.toBytes(67),
            encoding.toBytes(68),
            encoding.toBytes(65),
            encoding.toBytes(84),
            encoding.toBytes(65),
            encoding.toBytes(91),
            });
        this.cdataEnd = Arrays.concatenate(new byte[][]{ // ]]>
            encoding.toBytes(93),
            encoding.toBytes(93),
            encoding.toBytes(CHAR_TAG_END)
            });
        this.doctype = Arrays.concatenate(new byte[][]{ // DOCTYPE
            encoding.toBytes(68),
            encoding.toBytes(79),
            encoding.toBytes(67),
            encoding.toBytes(84),
            encoding.toBytes(89),
            encoding.toBytes(80),
            encoding.toBytes(69)
            });
    }

    public void setSkipEmptyText(boolean skipEmptyText) {
        this.skipEmptyText = skipEmptyText;
    }

    public boolean isSkipEmptyText() {
        return skipEmptyText;
    }

    public Object getInput() {
        return input;
    }

    public CharEncoding getEncoding() {
        return encoding;
    }

    private CharInputStream getInputStream() throws IOException {
        if (stream != null) {
            return stream;
        }

        if (input instanceof ByteInputStream) {
            stream = new CharInputStream((ByteInputStream) input,encoding);
            //we did not create the stream, we do not close it.
            closeStream = false;
            return stream;
        } else if (input instanceof Path) {
            stream = new CharInputStream(((Path) input).createInputStream(),encoding);
            closeStream = true;
            return stream;
        } else {
            throw new IOException("Unsupported input " + input);
        }
    }

    private int nextChar() throws IOException {
        if (prepeek && buffer.getSize() != 0) {
            //there is a character in the buffer already
            prepeek = false;
            return buffer.getSize();
        }
        final CharInputStream in = getInputStream();
        return in.readChar(buffer);
    }

    /**
     * Read next element.
     *
     * @return XMLElement or null if nothing more.
     * @throws IOException
     */
    public XMLElement readElement() throws IOException {

        previousElement = currentElement;

        if (previousElement != null && previousElement.getType() == XMLElement.TYPE_ELEMENT_START && currentProperties == null) {
            //previous element properties has not been read, skip them
            skipProperties();
        }

        currentElement = null;
        currentProperties = null;
        currentText = null;

        elementLoop:
        while (currentElement == null) {
            int nbbyte = nextChar();
            if (nbbyte <= 0) {
                //nothing left
                return null;
            }

            if (buffer.endWidth(tagStart)) {
                //node start
                buffer.removeAll();
                int prefixcut = -1;

                nbbyte = nextChar();
                if (nbbyte <= 0) {
                    throw new IOException("Unfinished xml element");
                }

                final int type;
                final Char escape;
                if (buffer.endWidth(question)) {
                    escape = new Char(question, encoding);
                    type = XMLElement.TYPE_META;
                    buffer.removeAll();
                } else if (buffer.endWidth(esclamation)) {
                    //a comment or cdata block
                    escape = new Char(esclamation, encoding);
                    int count = 0;
                    nextChar();
                    nextChar();

                    if (buffer.endWidth(commentStart)) {
                        //comment block
                        type = XMLElement.TYPE_COMMENT;
                        buffer.removeAll();
                        readUntil(commentEnd);
                        currentText = new Chars(buffer.toArrayByte(),encoding);
                        buffer.removeAll();
                    } else {
                        nextChar();
                        nextChar();
                        nextChar();
                        nextChar();
                        nextChar();
                        if (buffer.endWidth(cdataStart)) {
                            //cdata block
                            type = XMLElement.TYPE_CDATA;
                            buffer.removeAll();
                            readUntil(cdataEnd);
                            currentText = new Chars(buffer.toArrayByte(),encoding);
                            buffer.removeAll();
                        } else if (buffer.endWidth(doctype)) {
                            //doctype block
                            type = XMLElement.TYPE_DOCTYPE;
                            buffer.removeAll();
                            readUntil(tagEnd);
                            currentText = new Chars(buffer.toArrayByte(),encoding);
                            buffer.removeAll();
                        } else {
                            //unknowned block
                            throw new IOException("Unknowed tag : "+new String(buffer.toArrayByte()));
                        }
                    }
                    currentElement = new XMLElement(type, null, null, escape);
                    break elementLoop;

                } else if (buffer.endWidth(slash)) {
                    //element end
                    escape = new Char(slash, encoding);
                    type = XMLElement.TYPE_ELEMENT_END;
                    buffer.removeAll();
                } else {
                    escape = new Char(slash, encoding);
                    type = XMLElement.TYPE_ELEMENT_START;
                }

                for (;;) {
                    nbbyte = nextChar();
                    if (nbbyte <= 0) {
                        throw new IOException("Unfinished xml element");
                    } else if (buffer.endWidth(pseparation)) {
                        prefixcut = buffer.getSize();
                    }

                    boolean end = false;
                    if (buffer.endWidth(space)) {
                        buffer.trimEnd(space.length);
                        trimEmpty();
                        end = true;
                    } else if (buffer.endWidth(tab)) {
                        buffer.trimEnd(tab.length);
                        trimEmpty();
                        end = true;
                    } else if (buffer.endWidth(jumpline)) {
                        buffer.trimEnd(jumpline.length);
                        trimEmpty();
                        end = true;
                    } else if (buffer.endWidth(cariagereturn)) {
                        buffer.trimEnd(cariagereturn.length);
                        trimEmpty();
                        end = true;
                    } else if (buffer.endWidth(tagEnd)) {
                        //no properties
                        buffer.trimEnd(tagEnd.length);
                        currentProperties = new XMLAttributes();
                        end = true;
                    } else if (buffer.endWidth(escape.toBytes())) {
                        //element is already closing
                        end = true;
                        prepeek = true;
                    }

                    if (end) {
                        //end of element name
                        final Chars prefix;
                        final Chars name;
                        int from, to;
                        if (prefixcut < 0) {
                            //no prefix
                            prefix = null;
                            from = 0;
                            to = buffer.getSize();
                        } else {
                            final byte[] prefixbuffer = new byte[prefixcut-pseparation.length];
                            buffer.read(0, prefixbuffer, 0, prefixbuffer.length);
                            prefix = new Chars(prefixbuffer, encoding);
                            from = prefixcut;
                            to = buffer.getSize();
                        }

                        if (prepeek) {
                            to -= escape.getByteLength();
                        }
                        final byte[] namebuffer = new byte[to-from];
                        buffer.read(from, namebuffer, 0, to-from);
                        name = new Chars(namebuffer, encoding);
                        buffer.trimStart(to);
                        currentElement = new XMLElement(type, prefix, name, escape);
                        if (type == XMLElement.TYPE_META) {
                            //read properties now
                            readProperties();
                        }
                        break;
                    }

                }
            } else if (  previousElement != null
                    && previousElement.getType() == XMLElement.TYPE_ELEMENT_START
                    && buffer.endWidth(previousElement.getEscape().toBytes())) {
                //end of self finished tag
                nbbyte = nextChar();
                if (nbbyte <= 0 || !buffer.endWidth(tagEnd)) {
                    throw new IOException("Expecting '>' after '/'");
                }

                currentElement = new XMLElement(
                        XMLElement.TYPE_ELEMENT_END,
                        previousElement.getPrefix(),
                        previousElement.getName(),
                        previousElement.getEscape());
                buffer.removeAll();
            } else {
                // text block
                if (buffer.endWidth(tagEnd)) {
                    buffer.removeAll();
                    nextChar();
                }

                currentTextIsBlank = true;
                for (;;nbbyte = nextChar()) {

                    if (nbbyte <= 0) {
                        //end of text block
                        if (skipEmptyText && currentTextIsBlank) {
                            //blank text, skip it
                            buffer.removeAll();
                            continue elementLoop;
                        } else {
                            currentElement = new XMLElement(XMLElement.TYPE_TEXT, null, null,null);
                            currentText = new Chars(buffer.toArrayByte(), encoding);
                            buffer.removeAll();
                            break;
                        }
                    } else if (buffer.endWidth(tagStart)) {
                        //end of text block
                        prepeek = true;
                        if (skipEmptyText && currentTextIsBlank) {
                            //blank text, skip it
                            buffer.trimStart(buffer.getSize()-tagStart.length);
                            continue elementLoop;
                        } else {
                            currentElement = new XMLElement(XMLElement.TYPE_TEXT, null, null,null);
                            final byte[] tb = new byte[buffer.getSize()-tagStart.length];
                            buffer.read(0, tb, 0, tb.length);
                            currentText = new Chars(tb, encoding);
                            buffer.trimStart(buffer.getSize()-tagStart.length);
                            break;
                        }
                    } else if (skipEmptyText && currentTextIsBlank) {
                        //check blank
                        if (!(buffer.endWidth(space) || buffer.endWidth(tab) || buffer.endWidth(jumpline) || buffer.endWidth(cariagereturn))) {
                            currentTextIsBlank = false;
                        }
                    }
                }
            }
        }

        return currentElement;
    }

    /**
     * If element is a text block, this method return the text content.
     * otherwise it will throw an ioexception.
     * @return
     * @throws IOException
     */
    public Chars readText() throws IOException{
        return currentText;
    }

    /**
     * If element is a beginning of a node, this method parse the node properties.
     * otherwise it will throw an ioexception.
     * @throws IOException
     */
    public XMLAttributes readProperties() throws IOException {
        if (currentProperties != null) {
            return currentProperties;
        }

        currentProperties = new XMLAttributes();
        final int type = currentElement.getType();

        if (type == XMLElement.TYPE_COMMENT || type == XMLElement.TYPE_CDATA) {
            //comment(!) no properties
            return currentProperties;
        }

        if (type != XMLElement.TYPE_ELEMENT_START && type != XMLElement.TYPE_META) {
            throw new IOException("Current element is not a node start or meta");
        }

        boolean inName = false;
        boolean inValue = false;
        boolean equalFound = false;
        Chars attName = null;
        Chars attValue = null;

        while (true) {
            int nbbyte = nextChar();
            if (nbbyte <= 0) {
                //nothing left
                throw new IOException("Unexpected end of stream");
            }

            if (inValue) {
                //we are over a value char sequence, loop until double quote.
                if (buffer.endWidth(quote)) {
                    //end of value
                    final byte[] b = new byte[buffer.getSize()-quote.length];
                    buffer.read(0, b, 0, b.length);
                    attValue = new Chars(b, encoding);
                    buffer.removeAll();
                    currentProperties.add(attName, attValue);

                    //reset for next property
                    inName = false;
                    inValue = false;
                    equalFound = false;
                    attName = null;
                    attValue = null;
                }
                continue;
            } else if (inName) {
                //we are over an attribute name, loop until = or blank
                boolean end = false;
                if (buffer.endWidth(space)) {
                    buffer.trimEnd(space.length);
                    end = true;
                } else if (buffer.endWidth(tab)) {
                    buffer.trimEnd(tab.length);
                    end = true;
                } else if (buffer.endWidth(jumpline)) {
                    buffer.trimEnd(jumpline.length);
                    end = true;
                } else if (buffer.endWidth(cariagereturn)) {
                    buffer.trimEnd(cariagereturn.length);
                    end = true;
                } else if (buffer.endWidth(equal)) {
                    buffer.trimEnd(equal.length);
                    equalFound = true;
                    end = true;
                }

                if (end) {
                    attName = new Chars(buffer.toArrayByte(), encoding);
                    buffer.removeAll();
                    inName = false;
                }
                continue;
            }


            if (buffer.endWidth(space)) {
                buffer.removeAll();
            } else if (buffer.endWidth(tab)) {
                buffer.removeAll();
            } else if (buffer.endWidth(jumpline)) {
                buffer.removeAll();
            } else if (buffer.endWidth(cariagereturn)) {
                buffer.removeAll();
            } else if (buffer.endWidth(equal)) {
                //equal separation
                if (attName == null) {
                    throw new IOException("Unexpected '=' without attribute name");
                } else if (equalFound) {
                    throw new IOException("Unexpected succesive '=' ");
                }
                buffer.removeAll();
                equalFound = true;
            } else if (buffer.endWidth(quote)) {
                //start attribute value
                if (!equalFound) {
                    throw new IOException("Unexpected attribute value without attribute name");
                }
                inValue = true;
                buffer.removeAll();
            } else if (buffer.endWidth(tagEnd) || buffer.endWidth(currentElement.getEscape().toBytes())) {
                //end
                if (attName != null) {
                    throw new IOException("Unexpected none-finished attribute");
                }

                if (currentElement.getType() == XMLElement.TYPE_META) {
                    //skip the tag end
                    nextChar();
                    buffer.removeAll();
                } else {
                    prepeek = true;
                }
                break;
            } else {
                //attribute name begin
                inName = true;
            }
        }

        return currentProperties;
    }

    /**
     * If element is a beginning of a node, this method moves to the end
     * of the node. otherwise it will throw an ioexception.
     * @throws IOException
     */
    public void skipElement() throws IOException {
        if (currentElement.getType()==XMLElement.TYPE_ELEMENT_START) {
            int depth = 1;
            while (depth>0) {
                XMLElement ele=readElement();
                final int type = ele.getType();
                if (type==XMLElement.TYPE_ELEMENT_START) depth++;
                else if (type==XMLElement.TYPE_ELEMENT_END) depth--;
            }
        } else {
            throw new IOException("Current element is not a tag start");
        }
    }

    private void skipProperties() throws IOException{
        readProperties();
        //TODO fix it, bugs does not detect escaped / or verify proper declarations
//        for (;;) {
//            int nbbyte = nextChar();
//            if (nbbyte <=0) {
//                throw new IOException("Unfinished element");
//            }
//            if (buffer.endWidth(quote)) {
//                //start attribute value
//                if (!equalFound) {
//                    throw new IOException("Unexpected attribute value without attribute name");
//                }
//                inValue = true;
//                buffer.removeAll();
//            }if (buffer.endWidth(tagEnd)) {
//                //we found the end
//                buffer.removeAll();
//                return;
//
//            } else if (buffer.endWidth(currentElement.getEscape().toBytes())) {
//                //we found the end
//                buffer.trimStart(buffer.getSize()-currentElement.getEscape().getByteLength());
//                prepeek = true;
//                return;
//            }
//        }
    }

    private void trimEmpty() throws IOException{
        int nbbyte;
        for (;;) {
            nbbyte = nextChar();
            if (nbbyte <= 0) {
                throw new IOException("Unfinished xml element");
            }

            if (buffer.endWidth(space)) {
                buffer.trimEnd(space.length);
            } else if (buffer.endWidth(tab)) {
                buffer.trimEnd(tab.length);
            } else if (buffer.endWidth(jumpline)) {
                buffer.trimEnd(jumpline.length);
            } else if (buffer.endWidth(cariagereturn)) {
                buffer.trimEnd(cariagereturn.length);
            } else {
                prepeek = true;
                break;
            }
        }
    }

    private void readUntil(byte[] end) throws IOException{
        int nbbyte;
        for (;;) {
            nbbyte = nextChar();
            if (nbbyte <= 0) {
                throw new IOException("Unfinished xml element");
            }

            if (buffer.endWidth(end)) {
                buffer.trimEnd(end.length);
                break;
            }
        }
    }

    public void dispose() throws IOException {
        if (closeStream) {
            stream.dispose();
        }
    }
}
