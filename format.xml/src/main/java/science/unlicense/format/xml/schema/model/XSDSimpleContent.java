
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDSimpleContent extends XSDAnnotated implements GComplexTypeModel{

    public static final FName NAME = new FName(NS_BASE,new Chars("simpleContent",CharEncodings.US_ASCII));

    /**
     * <xs:element name="restriction" type="xs:simpleRestrictionType"/>
     */
    public XSDSimpleRestrictionType restriction;

    /**
     * <xs:element name="extension" type="xs:simpleExtensionType"/>
     */
    public XSDSimpleExtensionType extension;

}
