
package science.unlicense.format.xml;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import static science.unlicense.format.xml.XMLConstants.CHAR_DOUBLEDOT;
import static science.unlicense.format.xml.XMLConstants.CHAR_DOUBLEQUOTE;
import static science.unlicense.format.xml.XMLConstants.CHAR_EQUAL;
import static science.unlicense.format.xml.XMLConstants.CHAR_ESCLAMATION;
import static science.unlicense.format.xml.XMLConstants.CHAR_NEWLINE;
import static science.unlicense.format.xml.XMLConstants.CHAR_QUESTION;
import static science.unlicense.format.xml.XMLConstants.CHAR_SLASH;
import static science.unlicense.format.xml.XMLConstants.CHAR_SPACE;
import static science.unlicense.format.xml.XMLConstants.CHAR_TAG_END;
import static science.unlicense.format.xml.XMLConstants.CHAR_TAG_START;

/**
 *
 * @author Johann Sorel
 */
public class XMLOutputStream {

    //grammar xml element in given encoding.
    private byte[] tagStart;
    private byte[] tagEnd;
    private byte[] slash;
    private byte[] space;
    private byte[] jumpline;
    private byte[] pseparation;
    private byte[] quote;
    private byte[] equal;
    private byte[] question;
    private byte[] esclamation;
    private byte[] commentStart;
    private byte[] commentEnd;
    private byte[] cdataStart;
    private byte[] cdataEnd;

    //stream variables
    private Object output;
    private CharEncoding encoding;
    private boolean closeStream = false;
    private CharOutputStream stream;

    //indentation variables
    private boolean indent = false;
    private Chars indentseq = null;
    private byte[] bindent = null;
    private int depth = 0;

    //configuration
    private boolean autoclosing = true;


    private int previous = -1;
    private boolean inTagStart = false;

    public XMLOutputStream() {
        setEncoding(CharEncodings.UTF_8);
    }

    public void setOutput(Object output) {
        this.output = output;
    }

    public void setEncoding(CharEncoding encoding) {
        this.encoding = encoding;
        this.tagStart = encoding.toBytes(CHAR_TAG_START);
        this.tagEnd = encoding.toBytes(CHAR_TAG_END);
        this.slash = encoding.toBytes(CHAR_SLASH);
        this.space = encoding.toBytes(CHAR_SPACE);
        this.jumpline = encoding.toBytes(CHAR_NEWLINE);
        this.pseparation = encoding.toBytes(CHAR_DOUBLEDOT);
        this.quote = encoding.toBytes(CHAR_DOUBLEQUOTE);
        this.equal = encoding.toBytes(CHAR_EQUAL);
        this.question = encoding.toBytes(CHAR_QUESTION);
        this.esclamation = encoding.toBytes(CHAR_ESCLAMATION);
        this.commentStart = Arrays.concatenate(new byte[][]{ // <!--
            tagStart,
            esclamation,
            encoding.toBytes(45),
            encoding.toBytes(45)
            });
        this.commentEnd = Arrays.concatenate(new byte[][]{ // -->
            encoding.toBytes(45),
            encoding.toBytes(45),
            tagEnd
            });
        this.cdataStart = Arrays.concatenate(new byte[][]{ // <![CDATA[
            tagStart,
            esclamation,
            encoding.toBytes(91),
            encoding.toBytes(67),
            encoding.toBytes(68),
            encoding.toBytes(65),
            encoding.toBytes(84),
            encoding.toBytes(65),
            encoding.toBytes(91),
            });
        this.cdataEnd = Arrays.concatenate(new byte[][]{ // ]]>
            encoding.toBytes(93),
            encoding.toBytes(93),
            tagEnd
            });
    }

    public void setIndent(Chars indent) {
        this.indent = indent != null;
        this.indentseq = indent;
        this.bindent = indent.toBytes(encoding);
    }

    public Chars getIndent() {
        return indentseq;
    }

    /**
     * Set to true to write self closed tags when possible.
     * @param autoclosing
     */
    public void setAutoclosing(boolean autoclosing) {
        this.autoclosing = autoclosing;
    }

    public boolean isAutoclosing() {
        return autoclosing;
    }

    private CharOutputStream getOutputStream() throws IOException {
        if (stream != null) {
            return stream;
        }

        if (output instanceof ByteOutputStream) {
            stream = new CharOutputStream((ByteOutputStream) output);
            //we did not create the stream, we do not close it.
            closeStream = false;
            return stream;
        } else if (output instanceof Path) {
            stream = new CharOutputStream(((Path) output).createOutputStream());
            closeStream = true;
            return stream;
        } else {
            throw new IOException("Unsupported input");
        }
    }

    private void closeUnfinished() throws IOException{
        final CharOutputStream out = getOutputStream();
        if (inTagStart) {
            out.write(tagEnd);
            depth++;
        }
        inTagStart = false;
    }

    private void fillIndent() throws IOException{
        final CharOutputStream out = getOutputStream();
        for (int i=0;i<depth;i++) {
            out.write(bindent);
        }
    }

    public void writeMetaStart(final Chars name) throws IOException {
        final CharOutputStream out = getOutputStream();

        closeUnfinished();

        if (indent && previous != -1 && depth != 0) {
            out.write(jumpline);
            fillIndent();
        }

        out.write(tagStart);
        out.write(question);
        out.write(name.toBytes(encoding));
        previous = XMLElement.TYPE_META;
    }

    public void writeMetaEnd() throws IOException {
        final CharOutputStream out = getOutputStream();
        out.write(question);
        out.write(tagEnd);
        previous = XMLElement.TYPE_META;
    }

    public void writeComment(final Chars text) throws IOException {
        final CharOutputStream out = getOutputStream();

        closeUnfinished();

        if (indent && previous != -1 && depth != 0) {
            out.write(jumpline);
            fillIndent();
        }

        out.write(commentStart);
        out.write(text.toBytes(encoding));
        out.write(commentEnd);
        previous = XMLElement.TYPE_COMMENT;
    }

    public void writeCdata(final Chars text) throws IOException {
        final CharOutputStream out = getOutputStream();

        closeUnfinished();

        if (indent && previous != -1 && depth != 0) {
            out.write(jumpline);
            fillIndent();
        }

        out.write(cdataStart);
        out.write(text.toBytes(encoding));
        out.write(cdataEnd);
        previous = XMLElement.TYPE_CDATA;
    }

    public void writeText(final CharArray text) throws IOException {
        final CharOutputStream out = getOutputStream();

        closeUnfinished();

        out.write(text.toBytes(encoding));
        previous = XMLElement.TYPE_TEXT;
    }

    public void writeProperty(final Chars name, final Chars value) throws IOException {
        final CharOutputStream out = getOutputStream();
        out.write(space);
        out.write(name.toBytes(encoding));
        out.write(equal);
        out.write(quote);
        out.write(value.toBytes(encoding));
        out.write(quote);
    }

    public void writeElementStart(final Chars prefix, final Chars name) throws IOException {
        final CharOutputStream out = getOutputStream();

        closeUnfinished();

        if (indent) {
            if (previous != -1) { out.write(jumpline); }
            fillIndent();
        }

        out.write(tagStart);
        if (prefix != null) {
            out.write(prefix.toBytes(encoding));
            out.write(pseparation);
        }
        out.write(name.toBytes(encoding));

        inTagStart = true;
        previous = XMLElement.TYPE_ELEMENT_START;
    }

    public void writeElementEnd(final Chars prefix, final Chars name) throws IOException {
        final CharOutputStream out = getOutputStream();

        if (inTagStart) {
            if (autoclosing) {
                //close the tag directly
                out.write(slash);
                out.write(tagEnd);
            } else {
                out.write(tagEnd);
                out.write(tagStart);
                out.write(slash);
                if (prefix != null) {
                    out.write(prefix.toBytes(encoding));
                    out.write(pseparation);
                }
                out.write(name.toBytes(encoding));
                out.write(tagEnd);
            }
            depth--;

        } else {
            if (indent && previous != XMLElement.TYPE_TEXT && previous != XMLElement.TYPE_CDATA) {
                out.write(jumpline);
                fillIndent();
            }
            depth--;
            out.write(tagStart);
            out.write(slash);
            if (prefix != null) {
                out.write(prefix.toBytes(encoding));
                out.write(pseparation);
            }
            out.write(name.toBytes(encoding));
            out.write(tagEnd);
        }
        inTagStart = false;

        previous = XMLElement.TYPE_ELEMENT_END;
    }

    public void dispose() throws IOException {
        if (closeStream) {
            stream.close();
        }
    }

}
