
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDRestrictionType extends XSDAnnotated{

    public static final FName NAME = new FName(NS_BASE,new Chars("restriction",CharEncodings.US_ASCII));

    public static final FName ATT_BASE = new FName(XMLConstants.NS_NONE,new Chars("base",CharEncodings.US_ASCII));

    /**
     * <xs:choice minOccurs="0">
     *   <xs:group ref="xs:typeDefParticle"/>
     *   <xs:group ref="xs:simpleRestrictionModel"/>
     * </xs:choice>
     */
    public GTypeDefParticle typeDefParticle;
    public XSDLocalSimpleType simpleType;
    public final Sequence facets = new ArraySequence();

    /**
     * <xs:group ref="xs:attrDecls"/>
     */
    public final Sequence attrDecls = new ArraySequence();
    public XSDWildCard anyAttribute;

    /**
     * <xs:attribute name="base" type="xs:QName" use="required"/>
     */
    public FName base;

}
