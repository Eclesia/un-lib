
package science.unlicense.format.xml.schema.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.XMLConstants;
import static science.unlicense.format.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * Part of group : simpleDerivation
 *
 * @author Johann Sorel
 */
public class XSDList extends XSDAnnotated implements GSimpleDerivation{

    public static final FName NAME = new FName(NS_BASE,new Chars("list",CharEncodings.US_ASCII));

    public static final FName ATT_ITEMTYPE = new FName(XMLConstants.NS_NONE,new Chars("itemType",CharEncodings.US_ASCII));

    /**
     * TODO
     * <xs:sequence>
     *   <xs:element name="simpleType" type="xs:localSimpleType" minOccurs="0"/>
     * </xs:sequence>
     */

    /**
     * <xs:attribute name="itemType" type="xs:QName" use="optional"/>
     */
    public FName itemType;

}
