
package science.unlicense.format.m3u;

import science.unlicense.common.api.character.Chars;

/**
 * https://fr.wikipedia.org/wiki/M3U
 *
 * @author Johann Sorel
 */
public final class M3UConstants {

    public static final Chars SIGNATURE = Chars.constant("#EXTM3U");

    public static final Chars EXTINF = Chars.constant("#EXTINF:");
    public static final Chars EXTREM = Chars.constant("#EXTREM:");

    private M3UConstants(){}

}
