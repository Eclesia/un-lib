
package science.unlicense.format.m3u;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.m3u.model.M3UEntry;

/**
 *
 * @author Johann Sorel
 */
public class M3UReader extends AbstractReader {

    public static final int FORMAT_M3U = 1;
    public static final int FORMAT_M3U8 = 2;

    private final int format;

    public M3UReader(int format) {
        this.format = format;
    }

    /**
     * Read next entry.
     *
     * @return M3UEntry or null if none remaining
     * @throws IOException
     */
    public M3UEntry next() throws IOException{
        final CharInputStream cs;
        if (format==FORMAT_M3U) {
            cs = getInputAsCharStream(CharEncodings.ISO_8859_1);
        } else if (format==FORMAT_M3U8) {
            cs = getInputAsCharStream(CharEncodings.UTF_8);
        } else {
            throw new IOException(null, "Invalid format : "+ format);
        }

        M3UEntry entry = null;
        for (Chars line=cs.readLine();line!=null;line=cs.readLine()) {
            line = line.trim();
            if (line.isEmpty()) continue;
            if (line.startsWith(M3UConstants.SIGNATURE)) continue;

            if (entry==null) {
                entry = new M3UEntry();
            }

            if (line.startsWith(M3UConstants.EXTINF)) {
                line = line.truncate(M3UConstants.EXTREM.getCharLength(), line.getCharLength());
                final int splitIndex = line.getFirstOccurence(',');
                entry.setTitle(line.truncate(splitIndex+1,line.getCharLength()).trim());

                //TODO parse time like 10:50
                Chars time = line.truncate(0, splitIndex).trim();
                entry.setLength(Int32.decode(time));

            } else if (line.startsWith(M3UConstants.EXTREM)) {
                entry.setComment(line.truncate(M3UConstants.EXTREM.getCharLength(), line.getCharLength()));
            } else {
                entry.setPath(Paths.resolve(line));
                break;
            }
        }

        return entry;
    }

}
