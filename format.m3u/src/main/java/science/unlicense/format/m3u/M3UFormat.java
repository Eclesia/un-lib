
package science.unlicense.format.m3u;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * Resources :
 * http://forums.winamp.com/showthread.php?threadid=65772
 * https://en.wikipedia.org/wiki/M3U
 *
 * @author Johann Sorel
 */
public class M3UFormat extends DefaultFormat{

    public static final M3UFormat INSTANCE = new M3UFormat();

    private M3UFormat() {
        super(new Chars("M3U"));
        shortName = new Chars("M3U");
        longName = new Chars("Multimedia playlist");
        mimeTypes.add(new Chars("application/x-mpegurl"));
        extensions.add(new Chars("m3u"));
        extensions.add(new Chars("m3u8"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
