

package science.unlicense.protocol.smtp;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class SMTPResponse {

    public int code;
    public Chars[] arguments;

}
