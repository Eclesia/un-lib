

package science.unlicense.protocol.smtp;

import science.unlicense.common.api.character.Chars;

/**
 * Reference :
 * Simple Mail Transfer Protocol : http://tools.ietf.org/html/rfc5321
 * Internet Message Format : http://tools.ietf.org/html/rfc5322
 * Multipurpose Internet Mail Extensions : http://tools.ietf.org/html/rfc2045
 *
 * @author Johann Sorel
 */
public final class SMTPConstants {

    /** Used for argument separation */
    public static final int SPACE = ' ';
    /** Used to delimite message and commands parts */
    public static final Chars CRLF = Chars.constant(new byte[]{0x0D,0x0A});
    /** Used to delimite end of data */
    public static final Chars END_OF_DATA = CRLF.concat('.').concat(CRLF);

    public static final Chars COMMAND_EHLO = Chars.constant("EHLO");
    public static final Chars COMMAND_MAIL = Chars.constant("MAIL");
    public static final Chars COMMAND_RCPT = Chars.constant("RCPT");
    public static final Chars COMMAND_RSET = Chars.constant("RSET");
    public static final Chars COMMAND_DATA = Chars.constant("DATA");
    public static final Chars COMMAND_VRFY = Chars.constant("VRFY");
    public static final Chars COMMAND_EXPN = Chars.constant("EXPN");
    public static final Chars COMMAND_HELP = Chars.constant("HELP");
    public static final Chars COMMAND_NOOP = Chars.constant("NOOP");
    public static final Chars COMMAND_QUIT = Chars.constant("QUIT");

    //deprecated commands
    public static final Chars COMMAND_TURN = Chars.constant("TURN");
    public static final Chars COMMAND_HELO = Chars.constant("HELO");
    public static final Chars COMMAND_SEND = Chars.constant("SEND");
    public static final Chars COMMAND_SAML = Chars.constant("SAML");
    public static final Chars COMMAND_SOML = Chars.constant("SOML");


    public static final int CODE_211_SYSTEM_STATU_OR_HELP = 211;
    public static final int CODE_214_HELP_MESSAGE = 214;
    public static final int CODE_220_SERVICE_READY = 220;
    public static final int CODE_221_SERVICE_CLOSING = 221;
    public static final int CODE_250_ACTION_OK = 250;
    public static final int CODE_251_USER_NOT_LOCAL = 251;
    public static final int CODE_252_USER_UNVERIFY_SENDING_ATTEMPT = 252;

    public static final int CODE_354_START_MAIL_INPUT = 354;

    public static final int CODE_421_SERVICE_UNAVAILABLE = 421;
    public static final int CODE_450_MAILBOX_UNAVAILABLE = 450;
    public static final int CODE_451_ACTION_ABORTED = 451;
    public static final int CODE_452_INSUFFICIENT_SYSTEM_STORAGE = 452;
    public static final int CODE_455_UNSUPPORTED_PARAMETERS_SET = 455;

    public static final int CODE_500_SYNTAX_ERROR = 500;
    public static final int CODE_501_SYNTAX_ERROR_IN_ARGS_OR_PARAMS = 501;
    public static final int CODE_502_COMMAND_NOT_IMPLEMENTED = 502;
    public static final int CODE_503_BAD_COMMANDS_SEQUENCE = 503;
    public static final int CODE_504_PARAMETER_NOT_IMPLEMENTED = 504;
    public static final int CODE_550_MAILBOX_UNAVAILABLE = 550;
    public static final int CODE_551_USER_NOT_LOCAL = 551;
    public static final int CODE_552_EXCEEDED_STORAGE_ALLOCATION = 552;
    public static final int CODE_553_MAILBOX_NAME_NOT_ALLOWED = 553;
    public static final int CODE_554_TRANSACTION_FAILED = 554;
    public static final int CODE_555_INVALID_MAIL_OR_RCPT_TO = 555;

    private SMTPConstants(){}

}
