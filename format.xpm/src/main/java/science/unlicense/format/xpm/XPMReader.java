

package science.unlicense.format.xpm;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGESET;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION_ID;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorRW;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class XPMReader extends AbstractImageReader {

    private TypedNode mdImage = null;
    private int width;
    private int height;
    private int nbcolor;
    private int charPerSample;
    private int hotspotx;
    private int hotspoty;

    private byte[][] paletteValues;
    private int[] paletteColors;

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        final CharInputStream cs = new CharInputStream(stream, CharEncodings.US_ASCII, new Char('\n'));

        final Chars signature = cs.readLine();
        final Chars cdefinition = cs.readLine();
        final Chars header = trimCStyle(cs.readLine());
        final Chars[] parts = header.split(' ');
        width           = Int32.decode(parts[0]);
        height          = Int32.decode(parts[1]);
        nbcolor         = Int32.decode(parts[2]);
        charPerSample   = Int32.decode(parts[3]);

        //check if there is a cursor hotspot coordinate
        if (parts.length>4){
            hotspotx = Int32.decode(parts[4]);
            hotspoty = Int32.decode(parts[5]);
        }

        //read the palette
        paletteValues = new byte[nbcolor][charPerSample];
        paletteColors = new int[nbcolor];
        for (int i=0;i<nbcolor;i++){
            final Chars line = trimCStyle(cs.readLine());
            paletteValues[i] = line.truncate(0, charPerSample).toBytes();
            final Char type = line.getCharacter(charPerSample+1);
            final Chars colorValue = line.truncate(charPerSample+3,line.getCharLength());
            if (XPMConstants.TYPE_COlOR.equals(type)){
                if (XPMConstants.COLOR_NONE.equals(colorValue, true, true)){
                    paletteColors[i] = Color.TRS_WHITE.toARGB();
                } else {
                    paletteColors[i] = Colors.fromHexa(colorValue).toARGB();
                }
            } else if (XPMConstants.TYPE_MONOCHROME.equals(type)){
                throw new IOException("monochrome colors not supported yet.");
            } else if (XPMConstants.TYPE_GRAYSCALE.equals(type)){
                throw new IOException("grayscale colors not supported yet.");
            } else if (XPMConstants.TYPE_SYMBOLIC.equals(type)){
                throw new IOException("Symbolic colors not supported yet.");
            } else {
                throw new IOException("Unkowned color type : "+type);
            }
        }

        //rebuild metas
        mdImage =
        new DefaultTypedNode(MD_IMAGE,new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,width)}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,height)})
        });

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        stream.mark();
        readMetadatas(stream);
        stream.rewind();

        final CharInputStream charStream = new CharInputStream(stream, CharEncodings.US_ASCII,new Char('\n'));
        //skip header and palette
        for (int i=0,n=3+nbcolor;i<n;i++){
            charStream.readLine();
        }

        final Image image = Images.create(new Extent.Long(width, height),Images.IMAGE_TYPE_RGBA,parameters.getBufferFactory());
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor cursor = cm.asTupleBuffer(image).cursor();
        final ColorRW pixel = Colors.castOrWrap(cursor.samples(), cs);

        final Vector2i32 coord = new Vector2i32();
        for (;coord.y<height;coord.y++){
            coord.x=0;
            final Chars line = trimCStyle(charStream.readLine());
            final byte[] bytes = line.toBytes();
            for (int offset=0;coord.x<width;coord.x++,offset+=charPerSample){
                final int color = paletteColors[findColor(bytes, offset)];
                cursor.moveTo(coord);
                pixel.set(new ColorRGB(color));
            }
        }

        return image;
    }

    private int findColor(byte[] line, int offset) throws IOException{
        for (int i=0;i<nbcolor;i++){
            if (Arrays.equals(paletteValues[i],0,charPerSample,line,offset)){
                return i;
            }
        }
        throw new IOException("No color found for value : "+new Chars(Arrays.copy(line, offset, charPerSample)));
    }

    private static Chars trimCStyle(Chars candidate){
        final int start = candidate.getFirstOccurence('"');
        final int end = candidate.getLastOccurence('"');
        return candidate.truncate(start+1, end);
    }

}
