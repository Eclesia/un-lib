
package science.unlicense.format.xpm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.ImageResource;
import science.unlicense.image.api.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class XPMStore extends AbstractStore implements ImageResource {

    public XPMStore(ImageFormat format, Object input) {
        super(format, input);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public ImageReader createReader() throws IOException {
        final XPMReader reader = new XPMReader();
        reader.setInput(source);
        return reader;
    }

    @Override
    public ImageWriter createWriter() throws IOException {
        throw new IOException("Not supported");
    }

    @Override
    public Chars getId() {
        return null;
    }

}
