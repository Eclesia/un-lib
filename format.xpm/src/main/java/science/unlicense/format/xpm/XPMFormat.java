
package science.unlicense.format.xpm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * Resources :
 * http://fr.wikipedia.org/wiki/X_PixMap
 * http://en.wikipedia.org/wiki/X_PixMap
 *
 * @author Johann Sorel
 */
public class XPMFormat extends AbstractImageFormat {

    public static final XPMFormat INSTANCE = new XPMFormat();

    private XPMFormat() {
        super(new Chars("xpm"));
        shortName = new Chars("XPM");
        longName = new Chars("X PixMap");
        mimeTypes.add(new Chars("image/x-xpixmap"));
        mimeTypes.add(new Chars("image/x-xpm"));
        extensions.add(new Chars("xpm"));
        signatures.add(XPMConstants.SIGNATURE.toBytes());
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new XPMStore(this, source);
    }

}
