
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class PPtrType implements UnityValueType {

    private static final Chars NAME = Chars.constant("PPtr");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return name.startsWith(NAME);
    }

    public Class valueClass() {
        return UnityPointer.class;
    }

    public Object toValue(TypedNode node) {
        final UnityPointer pptr = new UnityPointer();
        pptr.fileId = ((Number) node.getChild(new Chars("m_FileID")).getValue()).longValue();
        pptr.pathId = ((Number) node.getChild(new Chars("m_PathID")).getValue()).longValue();
        return pptr;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
