
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public interface UnityValueType {

    /**
     * Name of the type.
     * @return
     */
    Chars getName();

    /**
     * Test if the given type name is supported.
     *
     * @param name
     * @return
     */
    boolean match(Chars name);

    /**
     * Type of the mapped value.
     *
     * @return
     */
    Class valueClass();

    /**
     * Convert typed node to real value object.
     *
     * @param node
     * @return
     */
    Object toValue(TypedNode node);

    /**
     * Convert value object to typed node.
     *
     * @param value
     * @param type
     * @return
     */
    TypedNode toNode(Object value, UnityNodeType type);


}
