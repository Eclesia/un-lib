
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class ColorRGBAType implements UnityValueType {

    private static final Chars NAME = Chars.constant("ColorRGBA");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return Color.class;
    }

    public Object toValue(TypedNode node) {
        //version 2
        final TypedNode rgbaNode = node.getChild(new Chars("rgba"));
        if (rgbaNode!=null){
            return new ColorRGB( ((Number) rgbaNode.getValue()).intValue() );
        }

        //version 1
        final TypedNode rNode = node.getChild(new Chars("r"));
        final TypedNode gNode = node.getChild(new Chars("g"));
        final TypedNode bNode = node.getChild(new Chars("b"));
        final TypedNode aNode = node.getChild(new Chars("a"));
        if (rNode!=null){
            return new ColorRGB(
                    (Float) rNode.getValue(),
                    (Float) gNode.getValue(),
                    (Float) bNode.getValue(),
                    (Float) aNode.getValue()
            );
        }

        return null;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
