
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class RectfType implements UnityValueType {

    private static final Chars NAME = Chars.constant("Rectf");
    public static final Chars ATT_X = Chars.constant("x");
    public static final Chars ATT_Y = Chars.constant("y");
    public static final Chars ATT_WIDTH = Chars.constant("width");
    public static final Chars ATT_HEIGHT = Chars.constant("height");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return Rectangle.class;
    }

    public Rectangle toValue(TypedNode node) {
        final Rectangle rect = new Rectangle();
        rect.setX((Float) node.getChild(ATT_X).getValue());
        rect.setY((Float) node.getChild(ATT_Y).getValue());
        rect.setWidth((Float) node.getChild(ATT_WIDTH).getValue());
        rect.setHeight((Float) node.getChild(ATT_HEIGHT).getValue());
        return rect;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
