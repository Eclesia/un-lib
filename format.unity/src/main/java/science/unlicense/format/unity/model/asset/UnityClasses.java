
package science.unlicense.format.unity.model.asset;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.unity.model.UnityVersion;
import science.unlicense.format.unity.model.ioclass.JSONClassReader;
import science.unlicense.format.unity.model.ioclass.JSONClassWriter;
import science.unlicense.format.xz.LZMA2Options;
import science.unlicense.format.xz.XZInputStream;
import science.unlicense.format.xz.XZOutputStream;

/**
 *
 * @author Johann Sorel
 */
public class UnityClasses {

    private final Sequence classes = new ArraySequence();

    public UnityClasses() {
    }

    public Sequence getClasses() {
        return classes;
    }

    public void readFolder(Path path){
        JSONClassReader reader = new JSONClassReader();
        for (int i=0;i<2000;i++){
            try{
                Path file = path.resolve(new Chars(i+".json.xz"));
                ByteInputStream is;
                try {
                    is = file.createInputStream();
                }catch(IOException ex){
                    continue;
                }
                final XZInputStream s = new XZInputStream(is);
                reader.setInput(s);
                reader.fill(this);
            }catch(Exception ex){
                throw new RuntimeException(ex);
            }
        }
    }

    public void writeFolder(String path) throws IOException, science.unlicense.encoding.api.io.IOException{
        //group classes by number
        final Dictionary sorted = new HashDictionary();
        for (int i=0,n=classes.getSize();i<n;i++){
            final UnityClass clazz = (UnityClass) classes.get(i);
            UnityClasses set = (UnityClasses) sorted.getValue(clazz.id);
            if (set==null){
                set = new UnityClasses();
                sorted.add(clazz.id, set);
            }
            set.add(clazz);
        }

        //sort by version number and write
        final Iterator ite = sorted.getValues().createIterator();
        while (ite.hasNext()){
            final UnityClasses set = (UnityClasses) ite.next();
            Collections.sort(set.classes);

            final int id = ((UnityClass) set.classes.get(0)).id;
            final Path jsonPath = Paths.resolve(new Chars("file:"+path+"/"+id+".json"));
            final Path xzPath = Paths.resolve(new Chars("file:"+path+"/"+id+".json.xz"));

            final JSONClassWriter writer = new JSONClassWriter();
            writer.setOutput(jsonPath);
            writer.write(set);
            writer.dispose();

            //compress it
            ByteOutputStream bout = xzPath.createOutputStream();
            final XZOutputStream out = new XZOutputStream(bout, new LZMA2Options());
            IOUtilities.copy(jsonPath.createInputStream(), out);
            out.flush();
            out.close();

            jsonPath.delete();
        }

    }

    /**
     *
     * @param id
     * @param version
     * @param strict allow to return the nearest older version of the type if any
     * @return
     */
    public UnityClass getClass(int id, UnityVersion version, boolean strict){
        final Iterator ite = classes.createIterator();
        UnityClass clazz = null;
        while (ite.hasNext()){
            final UnityClass cdt = (UnityClass) ite.next();
            if (cdt.id==id){
                final int order = cdt.version.order(version);
                if (order==0) return cdt;
                if (!strict && order<0){
                    //keep the closes version
                    if (clazz==null || cdt.version.order(clazz.version)>0) clazz = cdt;
                }
            }
        }

        return strict ? null : clazz;
    }

    public void add(UnityClass clazz) {
        classes.add(clazz);
    }

    private static UnityClasses CLASSES;
    public static synchronized UnityClasses getDefaultClasses() {
        if (CLASSES==null){
            CLASSES = new UnityClasses();
            CLASSES.readFolder(Paths.resolve(new Chars("mod:/science/unlicense/impl/format/unity")));
        }
        return CLASSES;
    }

}
