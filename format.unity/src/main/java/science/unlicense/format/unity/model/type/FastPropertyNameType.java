
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class FastPropertyNameType implements UnityValueType {

    private static final Chars NAME = Chars.constant("FastPropertyName");
    private static final Chars ATT_name = Chars.constant("name");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return Chars.class;
    }

    public Object toValue(TypedNode node) {
        return node.getChild(ATT_name).getValue();
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
