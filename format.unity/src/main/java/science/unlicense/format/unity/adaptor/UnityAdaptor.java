
package science.unlicense.format.unity.adaptor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.mesh.GLMesh;
import science.unlicense.format.unity.UnityConstants;
import science.unlicense.format.unity.model.asset.UnityAssetObject;
import science.unlicense.format.unity.model.asset.UnityAssetRegistry;
import science.unlicense.format.unity.model.primitive.GameObject1;
import science.unlicense.format.unity.model.primitive.GameObject2;
import science.unlicense.format.unity.model.primitive.GameObject3;
import science.unlicense.format.unity.model.primitive.MeshFilter1;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer;
import science.unlicense.format.unity.model.primitive.Texture2D;
import science.unlicense.format.unity.model.type.UnityPointer;
import science.unlicense.image.api.Image;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public final class UnityAdaptor {

    private UnityAdaptor(){}

    /**
     *
     * @param ite
     * @param registry
     * @param loadAll load all objects, not only game objects
     * @return
     * @throws IOException
     */
    public static Sequence convert(Iterator ite, UnityAssetRegistry registry, boolean loadAll) throws IOException{
        final Sequence elements = new ArraySequence();
        final AdaptorContext context = new AdaptorContext(registry);

        while (ite.hasNext()){
            final UnityAssetObject obj = (UnityAssetObject) ite.next();
            //System.out.println(deepPrint(obj.decode(true),registry));

            try{
                if (loadAll || obj.ref.classID==UnityConstants.CLASS_GAMEOBJECT){
                    convert(context, obj);
                    //elements.add(convert(context, obj));
                }
            }catch(Throwable ex){
                //too much bugs in unity decoder, skip what we don't support yet
                ex.printStackTrace();
            }
        }

        //elements.add(context.aggregateScene());
        //System.out.println(context.aggregateScene().toCharsTree(20));
        return elements;
    }

    private static Object convert(AdaptorContext context, UnityAssetObject obj) throws IOException{
        if (obj.ref.classID==UnityConstants.CLASS_SKINNEDMESHRENDERER){
            final TypedNode node = obj.decode(true);
            final MotionModel mpm = UnityMeshAdaptor.readSkinnedRenderer(context, (SkinnedMeshRenderer) node);
            return mpm;
        } else if (obj.ref.classID==UnityConstants.CLASS_MESH){
            final TypedNode node = obj.decode(true);
            GLMesh mpm = UnityMeshAdaptor.readMesh(context,node);
            return mpm;
        } else if (obj.ref.classID==UnityConstants.CLASS_TEXTURE2D ){
            final TypedNode node = obj.decode(true);
            final Image img = UnityTextureAdaptor.read((Texture2D) node, false);
            return img;
        } else if (obj.ref.classID==UnityConstants.CLASS_GAMEOBJECT ){
            GraphicNode sceneNode = context.getOrReadGameObject(obj.ref.toPointer());
            return sceneNode;
        }
        return null;
    }

    private static Chars deepPrint(Object node, UnityAssetRegistry registry) throws IOException{
        Chars name = new Chars(node.getClass().getSimpleName());

        UnityPointer pptr = null;
        if (node instanceof UnityPointer){
            pptr = (UnityPointer) node;
            name = new Chars(pptr.toString());

            final UnityAssetObject obj = registry.get(pptr);
            node = obj;
            if (obj!=null){
                node = obj.decode(true);
                if (node!=null){
                    name = name.concat(' ').concat(new Chars(node.getClass().getSimpleName()));
                }
            }
        }

        if (node instanceof GameObject1){
            return deepPrint(name, ((GameObject1) node).getm_Component(),registry);
        } else if (node instanceof GameObject2){
            return deepPrint(name, ((GameObject2) node).getm_Component(),registry);
        } else if (node instanceof GameObject3){
            return deepPrint(name, ((GameObject3) node).getm_Component(),registry);
        } else if (node instanceof MeshFilter1){
            //UnityPointer m_GameObject = ((MeshFilter1) node).getm_GameObject();
            UnityPointer m_Mesh = ((MeshFilter1) node).getm_Mesh();
            return Nodes.toChars(name, new Object[]{deepPrint(m_Mesh, registry)});
        } else {
            return new Chars(name);
        }
    }

    private static Chars deepPrint(Chars root, Sequence seq, UnityAssetRegistry registry) throws IOException{
        final Object[] children = new Object[seq.getSize()];
        for (int i=0;i<children.length;i++){
            final TypedNode tnode = (TypedNode) seq.get(i);
            final Pair pair = (Pair) tnode.getValue();
            final UnityPointer pptr = (UnityPointer) pair.getValue2();
            children[i] = deepPrint(pptr,registry);

            final UnityAssetObject obj = registry.get(pptr);
            final TypedNode cdt = obj.decode(true);
            if (cdt==null){
            } else {
                children[i] = deepPrint(cdt,registry);
            }
        }
        return Nodes.toChars(root, children);
    }
}
