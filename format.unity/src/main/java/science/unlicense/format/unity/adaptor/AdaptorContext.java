
package science.unlicense.format.unity.adaptor;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.mesh.GLMesh;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.format.unity.UnityConstants;
import static science.unlicense.format.unity.adaptor.UnityMeshAdaptor.*;
import science.unlicense.format.unity.model.asset.UnityAssetObject;
import science.unlicense.format.unity.model.asset.UnityAssetRegistry;
import science.unlicense.format.unity.model.type.UnityPointer;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;

/**
 *
 * @author Johann Sorel
 */
public class AdaptorContext {

    public final UnityAssetRegistry registry;
    /**
     * UnityPPtr -> Texture2D
     */
    public final Dictionary textureCache = new HashDictionary();
    /**
     * UnityPPtr -> Shell
     */
    public final Dictionary meshCache = new HashDictionary();
    /**
     * UnityPPtr -> GLNode
     */
    public final Dictionary glNodeCache = new HashDictionary();

    private final Sequence roots = new ArraySequence();

    public AdaptorContext(UnityAssetRegistry registry) {
        this.registry = registry;
    }

    /**
     * Rebuild parent -> child scene.
     * @return
     */
    public GraphicNode aggregateScene(){
        final GraphicNode root = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        root.getChildren().addAll(roots);
        return root;
    }

    public GraphicNode getOrReadGameObject(UnityPointer pptr) throws IOException{
        if (pptr==null) return null;
        GraphicNode glNode = (GraphicNode) glNodeCache.getValue(pptr);

        if (glNode==null){
            final TypedNode node = getNode(pptr);
            Object[] vals = UnityMeshAdaptor.readGameObject(this, node);
            glNode = (GraphicNode) vals[0];
            glNodeCache.add(pptr, glNode);
            if (Boolean.TRUE.equals(vals[1])){
                roots.add(glNode);
            }
        }
        return glNode;
    }

    public GLMesh getOrReadShell(UnityPointer pptr) throws IOException{
        if (pptr==null) return null;
        GLMesh shell = (GLMesh) meshCache.getValue(pptr);
        if (shell==null){
            System.out.println("load mesh :"+pptr);
            UnityAssetObject mobj = registry.get(pptr);
            if (mobj!=null && mobj.ref.classID == UnityConstants.CLASS_MESH){
                TypedNode decode = mobj.decode(true);
                if (decode instanceof GLModel){
                    shell =  readMesh(this,(science.unlicense.format.unity.model.primitive.Mesh) decode);
                    meshCache.add(pptr, shell);
                }
            }
        }
        return shell;
    }

    public Texture2D getOrReadTexture(UnityPointer pptr) throws IOException{
        if (pptr==null) return null;
        Texture2D tex = (Texture2D) textureCache.getValue(pptr);
        if (tex==null){
            System.out.println("load texture :"+pptr);
            final UnityAssetObject textureObj = registry.get(pptr);
            final TypedNode textureNode = textureObj.decode(true);
            final Image textureImage = UnityTextureAdaptor.read((science.unlicense.format.unity.model.primitive.Texture2D) textureNode, false);
            tex = new Texture2D(textureImage);
            textureCache.add(pptr, tex);
        }
        return tex;
    }

    public TypedNode getNode(UnityPointer pptr) throws IOException{
        final UnityAssetObject mobj = registry.get(pptr);
        if (mobj!=null){
            return mobj.decode(true);
        }
        return null;
    }

}
