
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class MapType implements UnityValueType {

    private static final Chars NAME = Chars.constant("map");
    private static final Chars ATT_Array = Chars.constant("Array");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return Dictionary.class;
    }

    public Object toValue(TypedNode node) {
        final Object[] pairs = (Object[]) node.getChild(ATT_Array).getValue();
        final Dictionary dico = new HashDictionary();
        for (int i=0;i<pairs.length;i++){
            final Pair p = (Pair) ((TypedNode) pairs[i]).getValue();
            dico.add(p.getValue1(), p.getValue2());
        }
        return dico;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
