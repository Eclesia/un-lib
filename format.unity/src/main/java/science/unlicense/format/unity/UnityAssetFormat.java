
package science.unlicense.format.unity;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Unity Engine asset format.
 *
 * Many decoding informations are from :
 * https://github.com/ata4/disunity (also in public domain)
 *
 *
 * @author Johann Sorel
 */
public class UnityAssetFormat extends AbstractModel3DFormat {

    public static final UnityAssetFormat INSTANCE = new UnityAssetFormat();

    private UnityAssetFormat() {
        super(new Chars("UNITY_ASSET"));
        shortName = new Chars("Unity asset");
        longName = new Chars("Unity engine asset Format");
        extensions.add(new Chars("asset"));
        extensions.add(new Chars("assets"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new UnityStore(input);
    }

}
