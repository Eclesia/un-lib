
package science.unlicense.format.unity.model.asset;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.unity.model.type.UnityPointer;

/**
 * Regroups multiple asset files in a single asset registry.
 *
 * @author Johann Sorel
 */
public class UnitySharedAssets implements UnityAssetRegistry{

    private final Sequence assetFiles = new ArraySequence();

    public UnitySharedAssets() {
    }

    /**
     * Create a shared assets derivate from the first file.
     * The first file is often named : sharedassets0.assets
     *
     * this method will load all consecutive files :
     * - sharedassets1.assets
     * - sharedassets2.assets
     * - ...
     *
     * @param mainFile
     */
    public UnitySharedAssets(Path mainFile) throws IOException {
        final Path base = mainFile.getParent();
        final Chars name = mainFile.getName();

        //load main file
        final UnityAsset asset = new UnityAsset(mainFile);
        asset.read();
        assetFiles.add(asset);
        //load other files
        for (int i=1;;i++){
            Path file = base.resolve(name.replaceAll(new Chars("0"), new Chars(""+i)));
            if (file.exists()){
                final UnityAsset asf = new UnityAsset(file);
                asf.read();
                assetFiles.add(asf);
            } else {
                break;
            }
        }
    }

    public Sequence getAssetFiles() {
        return assetFiles;
    }

    @Override
    public UnityAssetObject get(UnityPointer pptr) {
        for (int i=0,n=assetFiles.getSize();i<n;i++){
            final UnityAssetRegistry assetFile = (UnityAssetRegistry) assetFiles.get(i);
            final UnityAssetObject res = assetFile.get(pptr);
            if (res!=null) return res;
        }
        return null;
    }

    @Override
    public Iterator getIterator() {
        final Iterator[] iterators = new Iterator[assetFiles.getSize()];
        for (int i=0;i<iterators.length;i++){
            iterators[i] = ((UnityAssetRegistry) assetFiles.get(i)).getIterator();
        }
        return Collections.serie(iterators);
    }

}
