
package science.unlicense.format.unity.model.asset;

import science.unlicense.common.api.CObject;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.SeekableByteBuffer;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.encoding.impl.io.ClipInputStream;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class UnityAssetObject extends CObject{

    public final UnityAsset asset;
    public final UnityObjectReference ref;
    public byte[] objectBytes;

    public UnityAssetObject(UnityAsset asset, UnityObjectReference reference) {
        this.asset = asset;
        this.ref = reference;
    }

    public UnityNodeType getUnityNodeType(){
        UnityNodeType type = (UnityNodeType) asset.types.getValue(ref.typeID);
        if (type==null){
            //get from class list
            final UnityClass clazz = asset.getClasses().getClass(ref.classID, asset.unityVersion,false);
            if (clazz!=null){
                type = clazz.type;
                //make type name match class name
                type.type = clazz.name;
            }
        }
        return type;
    }

    /**
     * Load node bytes in memory.
     *
     * @throws IOException
     */
    public void load() throws IOException{
        final ByteInputStream is = asset.input.createInputStream();
        final DataInputStream ds = new DataInputStream(is, Endianness.LITTLE_ENDIAN);
        ds.skipFully(asset.header.dataOffset + ref.offset);
        objectBytes = ds.readFully(new byte[(int) ref.length]);
    }

    public ByteInputStream getAsInputStream() throws IOException{
        final ByteInputStream is = asset.input.createInputStream();
        final DataInputStream ds = new DataInputStream(is, Endianness.LITTLE_ENDIAN);
        ds.skipFully(asset.header.dataOffset + ref.offset);
        return new ClipInputStream(is, (int) ref.length);
    }

    public void setBytes(byte[] datas) throws IOException{
        final SeekableByteBuffer sb = asset.input.createSeekableBuffer(true, true, true);
        sb.skip(asset.header.dataOffset + ref.offset);
        sb.remove((int) ref.length);
        sb.insert(datas);
        ref.length = datas.length;
    }

    public TypedNode decode(boolean simplify) throws IOException{

        final ByteInputStream is = getAsInputStream();
        final DataInputStream ds = new DataInputStream(is, Endianness.LITTLE_ENDIAN);

        //find object type
        final UnityNodeType type = getUnityNodeType();
        if (type==null){
            return null;
        }

        return type.readInstance(this,ds,simplify);
    }

    public void updateByte(TypedNode node) throws IOException{
        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ds = new DataOutputStream(out, Endianness.LITTLE_ENDIAN);
        ((UnityNodeType) node.getType()).writeInstance(ds, this, (TypedNode) node);
        objectBytes = out.getBuffer().toArrayByte();
    }

}
