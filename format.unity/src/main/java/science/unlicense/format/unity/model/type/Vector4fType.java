
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class Vector4fType implements UnityValueType {

    private static final Chars NAME = Chars.constant("Vector4f");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return VectorRW.class;
    }

    public Object toValue(TypedNode node) {
        final VectorRW vector = VectorNf64.createDouble(4);
        vector.set(0,(Float) node.getChild(QuaternionType.ATT_X).getValue());
        vector.set(1,(Float) node.getChild(QuaternionType.ATT_Y).getValue());
        vector.set(2,(Float) node.getChild(QuaternionType.ATT_Z).getValue());
        vector.set(3,(Float) node.getChild(QuaternionType.ATT_W).getValue());
        return vector;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
