
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class PairType implements UnityValueType {

    private static final Chars NAME = Chars.constant("pair");
    private static final Chars ATT_first = Chars.constant("first");
    private static final Chars ATT_second = Chars.constant("second");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return Pair.class;
    }

    public Object toValue(TypedNode node) {
        TypedNode firstNode = node.getChild(ATT_first);
        TypedNode secondNode = node.getChild(ATT_second);
        Object firstValue = firstNode.getValue();
        Object secondValue = secondNode.getValue();
        return new Pair(firstValue==null ? firstNode : firstValue, secondValue==null? secondNode : secondValue);
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
