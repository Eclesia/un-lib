
package science.unlicense.format.unity.adaptor;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.material.mapping.SpriteInfo;
import science.unlicense.format.dxt.DXTConstants;
import science.unlicense.format.dxt.DXTHeader;
import science.unlicense.format.dxt.DXTPixelFormat;
import science.unlicense.format.dxt.DXTReadParameters;
import science.unlicense.format.dxt.DXTReader;
import static science.unlicense.format.unity.UnityConstants.*;
import science.unlicense.format.unity.model.primitive.Sprite;
import science.unlicense.format.unity.model.primitive.Sprite1;
import science.unlicense.format.unity.model.primitive.Sprite2;
import science.unlicense.format.unity.model.primitive.Texture2D;
import science.unlicense.format.unity.model.primitive.Texture2D1;
import science.unlicense.format.unity.model.primitive.Texture2D2;
import science.unlicense.format.unity.model.primitive.Texture2D3;
import science.unlicense.format.unity.model.primitive.Texture2D4;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.DefaultImageReadParameters;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import science.unlicense.image.impl.RawImageReader;

/**
 *
 * @author Johann Sorel
 */
public final class UnityTextureAdaptor {

    private static final Chars ATT_m_Width = Chars.constant("m_Width");
    private static final Chars ATT_m_Height = Chars.constant("m_Height");
    private static final Chars ATT_m_CompleteImageSize = Chars.constant("m_CompleteImageSize");
    private static final Chars ATT_m_TextureFormat = Chars.constant("m_TextureFormat");
    private static final Chars ATT_m_MipMap = Chars.constant("m_MipMap");
    private static final Chars ATT_m_IsReadable = Chars.constant("m_IsReadable");
    private static final Chars ATT_m_ImageCount = Chars.constant("m_ImageCount");
    private static final Chars ATT_m_TextureDimension = Chars.constant("m_TextureDimension");
    private static final Chars ATT_m_TextureSettings = Chars.constant("m_TextureSettings");
        private static final Chars ATT_m_FilterMode = Chars.constant("m_FilterMode");
        private static final Chars ATT_m_Aniso = Chars.constant("m_Aniso");
        private static final Chars ATT_m_MipBias = Chars.constant("m_MipBias");
        private static final Chars ATT_m_WrapMode = Chars.constant("m_WrapMode");
    private static final Chars ATT_m_LightmapFormat = Chars.constant("m_LightmapFormat");
    private static final Chars ATT_m_ColorSpace = Chars.constant("m_ColorSpace");
    private static final Chars ATT_image_data = Chars.constant("image data");

    private UnityTextureAdaptor(){}

    public static Image read(Texture2D tex, boolean uncompress) throws IOException{
        final int width;
        final int height;
        final int format;
        final boolean mipmap;
        final int nbImage;
        final int dimension;
        final byte[] buffer;

        if (tex instanceof Texture2D1){
            final Texture2D1 t = (Texture2D1) tex;
            width = t.getm_Width();
            height = t.getm_Height();
            format = t.getm_TextureFormat();
            mipmap = t.getm_MipMap();
            nbImage = t.getm_ImageCount();
            dimension = t.getm_TextureDimension();
            buffer = t.getimage_data();
        } else if (tex instanceof Texture2D2){
            final Texture2D2 t = (Texture2D2) tex;
            width = t.getm_Width();
            height = t.getm_Height();
            format = t.getm_TextureFormat();
            mipmap = t.getm_MipMap();
            nbImage = t.getm_ImageCount();
            dimension = t.getm_TextureDimension();
            buffer = t.getimage_data();

        } else if (tex instanceof Texture2D3){
            final Texture2D3 t = (Texture2D3) tex;
            width = t.getm_Width();
            height = t.getm_Height();
            format = t.getm_TextureFormat();
            mipmap = t.getm_MipMap();
            nbImage = t.getm_ImageCount();
            dimension = t.getm_TextureDimension();
            buffer = t.getimage_data();

        } else if (tex instanceof Texture2D4){
            final Texture2D4 t = (Texture2D4) tex;
            width = t.getm_Width();
            height = t.getm_Height();
            format = t.getm_TextureFormat();
            mipmap = t.getm_MipMap();
            nbImage = t.getm_ImageCount();
            dimension = t.getm_TextureDimension();
            buffer = t.getimage_data();

        } else {
            throw new IOException("Unexpected texture type : "+tex);
        }

        if (format==TEXFORMAT_DXT1){
            final DXTReader reader = new DXTReader();
            reader.setInput(buffer);

            final DXTHeader header = new DXTHeader();
            header.width = width;
            header.height = height;
            header.pixelFormat = new DXTPixelFormat();
            header.pixelFormat.fourCC = DXTConstants.D3DFMT_DXT1;
            final DXTReadParameters params = new DXTReadParameters();
            params.setDecompress(uncompress);
            params.setHeader(header);
            return reader.read(params);
        } else if (format==TEXFORMAT_DXT5){
            final DXTReader reader = new DXTReader();
            reader.setInput(buffer);

            final DXTHeader header = new DXTHeader();
            header.width = width;
            header.height = height;
            header.pixelFormat = new DXTPixelFormat();
            header.pixelFormat.fourCC = DXTConstants.D3DFMT_DXT5;
            final DXTReadParameters params = new DXTReadParameters();
            params.setDecompress(uncompress);
            params.setHeader(header);
            return reader.read(params);
        }

        final Extent.Long ext = new Extent.Long(width, height);

        final RawImageReader reader;
        if (format==TEXFORMAT_ARGB32)        reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_ARGB32);
        else if (format==TEXFORMAT_BGRA32)   reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_BGRA32);
        else if (format==TEXFORMAT_RGB24)    reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_RGB24);
        else if (format==TEXFORMAT_RGBA32)   reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_RGBA32);
        else if (format==TEXFORMAT_RGB565)   reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_RGB565);
        else if (format==TEXFORMAT_ALPHA8)   reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_ALPHA8);
        else if (format==TEXFORMAT_RGBA4444) reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_RGBA4444);
        else if (format==TEXFORMAT_ARGB4444) reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_ARGB4444);
        else                                throw new IOException("Unsupported texture format : "+format);

        final ImageReadParameters params = new DefaultImageReadParameters();
        params.setBufferFactory(DefaultBufferFactory.INSTANCE);
        params.setDecompress(uncompress);
        reader.setInput(buffer);
        final Image img = reader.read(params);

        return img;
    }

    public static SpriteInfo read(Sprite sprite){

        if (sprite instanceof Sprite1){

        } else if (sprite instanceof Sprite2){

        }
        throw new UnimplementedException("TODO");
    }

}
