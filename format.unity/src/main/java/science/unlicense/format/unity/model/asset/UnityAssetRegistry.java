
package science.unlicense.format.unity.model.asset;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.format.unity.model.type.UnityPointer;

/**
 *
 * @author Johann Sorel
 */
public interface UnityAssetRegistry {

    /**
     * Iterator over all unity asset objects in the registry.
     * @return
     */
    Iterator getIterator();

    /**
     * Find object for given pointer.
     *
     * @param pptr
     * @return
     */
    UnityAssetObject get(UnityPointer pptr);

}
