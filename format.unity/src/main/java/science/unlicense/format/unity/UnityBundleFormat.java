
package science.unlicense.format.unity;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Unity Engine package format.
 *
 * Many decoding informations are from :
 * https://github.com/ata4/disunity (also in public domain)
 *
 *
 * @author Johann Sorel
 */
public class UnityBundleFormat extends AbstractModel3DFormat {

    public static final UnityBundleFormat INSTANCE = new UnityBundleFormat();

    private UnityBundleFormat() {
        super(new Chars("UNITY_BUNDLE"));
        shortName = new Chars("Unity bundle");
        longName = new Chars("Unity engine package Format");
        extensions.add(new Chars("unity3d"));
        signatures.add(UnityConstants.SIGNATURE_RAW);
        signatures.add(UnityConstants.SIGNATURE_WEB);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new UnityStore(input);
    }

}
