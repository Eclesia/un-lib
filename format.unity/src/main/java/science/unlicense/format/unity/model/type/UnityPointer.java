
package science.unlicense.format.unity.model.type;

import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.unity.model.asset.UnityAsset;
import science.unlicense.format.unity.model.asset.UnityAssetReference;

/**
 *
 * @author Johann Sorel
 */
public class UnityPointer {

    /**
     * File id, relative to reference in unity asset.
     * - Value 0 means current file.
     * - Value 1+ correspond to referenced file index minus one.
     */
    public long fileId;
    /**
     * Object id in the given file.
     */
    public long pathId;
    /**
     * Internal use only : resolved path to fileId
     */
    public Path fileAbsolutePath;

    public String toString() {
        return "PPtr<"+fileId+","+pathId+">";
    }

    public void makeAbsolute(UnityAsset asset){
        if (fileId==0){
            fileAbsolutePath = asset.input;
        } else {
            final UnityAssetReference ref = (UnityAssetReference) asset.assetRefs.get((int) fileId-1);
            fileAbsolutePath = asset.input.getParent().resolve(ref.filePath);
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (this.pathId ^ (this.pathId >>> 32));
        hash = 59 * hash + (this.fileAbsolutePath != null ? this.fileAbsolutePath.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnityPointer other = (UnityPointer) obj;
        if (this.pathId != other.pathId) {
            return false;
        }
        if (this.fileAbsolutePath != other.fileAbsolutePath && (this.fileAbsolutePath == null || !this.fileAbsolutePath.equals(other.fileAbsolutePath))) {
            return false;
        }
        return true;
    }

}
