
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.VectorRW;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class LocalAABBType implements UnityValueType {

    private static final Chars NAME = Chars.constant("AABB");
    private static final Chars ATT_m_Center = Chars.constant("m_Center");
    private static final Chars ATT_m_Extent = Chars.constant("m_Extent");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return BBox.class;
    }

    public Object toValue(TypedNode node) {
        final BBox bbox = new BBox(3);
        final VectorRW center = (VectorRW) node.getChild(ATT_m_Center).getValue();
        final VectorRW extent = (VectorRW) node.getChild(ATT_m_Extent).getValue();

        for (int i=0;i<3;i++){
            final double c = center.get(i);
            final double x = extent.get(i);
            bbox.setRange(i, c-x, c+x);
        }

        return bbox;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
