
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class Matrix4x4fType implements UnityValueType {

    private static final Chars NAME = Chars.constant("Matrix4x4f");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return Matrix4x4.class;
    }

    public Object toValue(TypedNode node) {
        final Matrix4x4 matrix = new Matrix4x4();
        matrix.set(0,0,(Float) node.getChild(new Chars("e00")).getValue());
        matrix.set(0,1,(Float) node.getChild(new Chars("e01")).getValue());
        matrix.set(0,2,(Float) node.getChild(new Chars("e02")).getValue());
        matrix.set(0,3,(Float) node.getChild(new Chars("e03")).getValue());

        matrix.set(1,0,(Float) node.getChild(new Chars("e10")).getValue());
        matrix.set(1,1,(Float) node.getChild(new Chars("e11")).getValue());
        matrix.set(1,2,(Float) node.getChild(new Chars("e12")).getValue());
        matrix.set(1,3,(Float) node.getChild(new Chars("e13")).getValue());

        matrix.set(2,0,(Float) node.getChild(new Chars("e20")).getValue());
        matrix.set(2,1,(Float) node.getChild(new Chars("e21")).getValue());
        matrix.set(2,2,(Float) node.getChild(new Chars("e22")).getValue());
        matrix.set(2,3,(Float) node.getChild(new Chars("e23")).getValue());

        matrix.set(3,0,(Float) node.getChild(new Chars("e30")).getValue());
        matrix.set(3,1,(Float) node.getChild(new Chars("e31")).getValue());
        matrix.set(3,2,(Float) node.getChild(new Chars("e32")).getValue());
        matrix.set(3,3,(Float) node.getChild(new Chars("e33")).getValue());
        return matrix;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
