
package science.unlicense.format.unity.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.Orderable;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.regex.Regex;

/**
 * Unity version composed of 3 numbers and a build number.
 *
 * @author Johann Sorel
 */
public class UnityVersion extends CObject implements Orderable{

    public static final Chars KIND_ALPHA = Chars.constant("a");
    public static final Chars KIND_BETA = Chars.constant("b");
    public static final Chars KIND_RELEASE_CANDIDATE = Chars.constant("rc");
    public static final Chars KIND_PATCH = Chars.constant("p"); //TODO not sure about this one, need to find more infos
    public static final Chars KIND_FINAL = Chars.constant("f");
    private static final Chars[] KIND_ORDERED = new Chars[]{KIND_ALPHA,KIND_BETA,KIND_RELEASE_CANDIDATE,KIND_PATCH,KIND_FINAL};

    private static final Chars NO_DIGIT = Chars.constant("[^0123456789]+");

    public Chars str;

    public UnityVersion(Chars str) {
        this.str = str;
    }

    public UnityVersion(int n0, int n1, int n2, int build) {
        this.str = new Chars(n0+"."+n1+"."+n2+"f"+build);
    }

    public int[] getVersionNumbers(){
        final Chars[] split = Regex.split(str, NO_DIGIT);
        return new int[]{
            Int32.decode(split[0]),
            Int32.decode(split[1]),
            Int32.decode(split[2])
        };
    }

    /**
     * See KIND_X
     * @return
     */
    public Chars getBuildKind(){
        if (str.getFirstOccurence(KIND_ALPHA)>0){
            return KIND_ALPHA;
        } else if (str.getFirstOccurence(KIND_BETA)>0){
            return KIND_BETA;
        } else if (str.getFirstOccurence(KIND_RELEASE_CANDIDATE)>0){
            return KIND_RELEASE_CANDIDATE;
        } else if (str.getFirstOccurence(KIND_PATCH)>0){
            return KIND_PATCH;
        } else if (str.getFirstOccurence(KIND_FINAL)>0){
            return KIND_FINAL;
        } else {
            throw new InvalidArgumentException("Unvalid version, missing kind(a,b,rc,f) : "+ str);
        }
    }

    public int getBuildNumber(){
        final Chars[] split = Regex.split(str, NO_DIGIT);
        return Int32.decode(split[3]);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnityVersion other = (UnityVersion) obj;
        if (this.str != other.str && (this.str == null || !this.str.equals(other.str))) {
            return false;
        }
        return true;
    }

    public int getHash() {
        return str.getHash();
    }

    public int order(Object other) {
        final UnityVersion o = (UnityVersion) other;

        //test version
        final int[] ov = o.getVersionNumbers();
        final int[] tv = getVersionNumbers();
        for (int i=0;i<ov.length;i++){
            if (tv[i]<ov[i]) return -1;
            else if (tv[i]>ov[i]) return +1;
        }

        //check build kind
        int okind = Arrays.getFirstOccurence(KIND_ORDERED, 0, KIND_ORDERED.length, o.getBuildKind());
        int tkind = Arrays.getFirstOccurence(KIND_ORDERED, 0, KIND_ORDERED.length, getBuildKind());
        if (okind != tkind){
            return tkind-okind;
        }

        //test build number
        final int obv = o.getBuildNumber();
        final int tbv = getBuildNumber();

        if (tbv<obv) return -1;
        else if (tbv>obv) return +1;
        else return 0;
    }

    @Override
    public Chars toChars() {
        return str;
    }

}
