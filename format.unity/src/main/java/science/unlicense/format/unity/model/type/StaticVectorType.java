
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.collection.primitive.DoubleSequence;
import science.unlicense.common.api.collection.primitive.FloatSequence;
import science.unlicense.common.api.collection.primitive.IntSequence;
import science.unlicense.common.api.collection.primitive.LongSequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 * vector types contains a single array type node named 'Array';
 * @author Johann Sorel
 */
public class StaticVectorType implements UnityValueType {

    private static final Chars NAME = Chars.constant("staticvector");
    private static final Chars ATT_Array = Chars.constant("Array");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return Sequence.class;
    }

    public Object toValue(TypedNode node) {
        final Object array = node.getChild(ATT_Array).getValue();
        if (array instanceof byte[]){
            return new ByteSequence((byte[]) array);
        } else if (array instanceof int[]){
            return new IntSequence((int[]) array);
        } else if (array instanceof long[]){
            return new LongSequence((long[]) array);
        } else if (array instanceof float[]){
            return new FloatSequence((float[]) array);
        } else if (array instanceof double[]){
            return new DoubleSequence((double[]) array);
        } else if (array instanceof Object[]){
            return new ArraySequence((Object[]) array);
        }
        throw new RuntimeException("Unexpected array type : "+array.getClass());
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
