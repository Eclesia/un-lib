
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class TypelessDataType implements UnityValueType {

    private static final Chars NAME = Chars.constant("TypelessData");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return byte[].class;
    }

    public Object toValue(TypedNode node) {
        final TypedNode child1 = node.getChild(new Chars("size"));
        final TypedNode child2 = node.getChild(new Chars("data"));
        return (byte[]) child2.getValue();
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
