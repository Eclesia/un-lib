
package science.unlicense.format.unity.adaptor;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DataCursor;
import science.unlicense.common.api.buffer.DefaultInt8Buffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.UInt16;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.GLMesh;
import science.unlicense.format.unity.model.asset.UnityAssetObject;
import science.unlicense.format.unity.model.primitive.BlendShapeData1;
import science.unlicense.format.unity.model.primitive.BoneInfluence1;
import science.unlicense.format.unity.model.primitive.ChannelInfo1;
import science.unlicense.format.unity.model.primitive.GameObject1;
import science.unlicense.format.unity.model.primitive.GameObject2;
import science.unlicense.format.unity.model.primitive.GameObject3;
import science.unlicense.format.unity.model.primitive.Material3;
import science.unlicense.format.unity.model.primitive.Mesh1;
import science.unlicense.format.unity.model.primitive.Mesh2;
import science.unlicense.format.unity.model.primitive.Mesh3;
import science.unlicense.format.unity.model.primitive.Mesh4;
import science.unlicense.format.unity.model.primitive.Mesh5;
import science.unlicense.format.unity.model.primitive.Mesh6;
import science.unlicense.format.unity.model.primitive.Mesh7;
import science.unlicense.format.unity.model.primitive.Mesh8;
import science.unlicense.format.unity.model.primitive.Mesh9;
import science.unlicense.format.unity.model.primitive.MeshFilter1;
import science.unlicense.format.unity.model.primitive.MeshRenderer;
import science.unlicense.format.unity.model.primitive.MeshRenderer1;
import science.unlicense.format.unity.model.primitive.MeshRenderer2;
import science.unlicense.format.unity.model.primitive.MeshRenderer3;
import science.unlicense.format.unity.model.primitive.MeshRenderer4;
import science.unlicense.format.unity.model.primitive.MeshRenderer5;
import science.unlicense.format.unity.model.primitive.MeshRenderer6;
import science.unlicense.format.unity.model.primitive.MeshRenderer7;
import science.unlicense.format.unity.model.primitive.MeshRenderer8;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer1;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer10;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer11;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer2;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer3;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer4;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer5;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer6;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer7;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer8;
import science.unlicense.format.unity.model.primitive.SkinnedMeshRenderer9;
import science.unlicense.format.unity.model.primitive.StreamInfo2;
import science.unlicense.format.unity.model.primitive.TrailRenderer1;
import science.unlicense.format.unity.model.primitive.TrailRenderer2;
import science.unlicense.format.unity.model.primitive.TrailRenderer3;
import science.unlicense.format.unity.model.primitive.TrailRenderer4;
import science.unlicense.format.unity.model.primitive.TrailRenderer5;
import science.unlicense.format.unity.model.primitive.TrailRenderer6;
import science.unlicense.format.unity.model.primitive.Transform;
import science.unlicense.format.unity.model.primitive.Transform1;
import science.unlicense.format.unity.model.primitive.UnityPropertySheet1;
import science.unlicense.format.unity.model.primitive.UnityTexEnv1;
import science.unlicense.format.unity.model.primitive.VertexData2;
import science.unlicense.format.unity.model.type.UnityPointer;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.impl.SimilarityNd;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.RenderState;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.Technique;

/**
 *
 * @author Johann Sorel
 */
public final class UnityMeshAdaptor {

    //texture envs types seens so far
    private static final Chars TEXENV_MAIN = Chars.constant("_MainTex");
    private static final Chars TEXENV_SPECULAR = Chars.constant("_SpecTex");
    private static final Chars TEXENV_BUMPMAP = Chars.constant("_BumpMap");
    private static final Chars TEXENV_SPECCUBE_IBL = Chars.constant("_SpecCubeIBL");
    private static final Chars TEXENV_DIFFCUBE_IBL = Chars.constant("_DiffCubeIBL");
    //color types seens so far
    private static final Chars COLOR_MAIN = Chars.constant("_Color");
    private static final Chars COLOR_SPECULAR = Chars.constant("_SpecColor");
    //floats types seens so far
    private static final Chars FLOAT_OFFSETH = Chars.constant("_OffsetH");
    private static final Chars FLOAT_OFFSETS = Chars.constant("_OffsetS");
    private static final Chars FLOAT_OFFSETV = Chars.constant("_OffsetV");
    private static final Chars FLOAT_FRESNEL = Chars.constant("_Fresnel");
    private static final Chars FLOAT_SPECINT = Chars.constant("_Specint");
    private static final Chars FLOAT_SHININESS = Chars.constant("_Shininess");

    private UnityMeshAdaptor(){}

    public static MotionModel readSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer node) throws IOException{

        if (node instanceof SkinnedMeshRenderer1) return adaptSkinnedRenderer(context, (SkinnedMeshRenderer1) node);
        else if (node instanceof SkinnedMeshRenderer2) return adaptSkinnedRenderer(context, (SkinnedMeshRenderer2) node);
        else if (node instanceof SkinnedMeshRenderer3) return adaptSkinnedRenderer(context, (SkinnedMeshRenderer3) node);
        else if (node instanceof SkinnedMeshRenderer4) return adaptSkinnedRenderer(context, (SkinnedMeshRenderer4) node);
        else if (node instanceof SkinnedMeshRenderer5) return adaptSkinnedRenderer(context, (SkinnedMeshRenderer5) node);
        else if (node instanceof SkinnedMeshRenderer6) return adaptSkinnedRenderer(context, (SkinnedMeshRenderer6) node);
        else if (node instanceof SkinnedMeshRenderer7) return adaptSkinnedRenderer(context, (SkinnedMeshRenderer7) node);
        else if (node instanceof SkinnedMeshRenderer8) return adaptSkinnedRenderer(context, (SkinnedMeshRenderer8) node);
        else if (node instanceof SkinnedMeshRenderer9) return adaptSkinnedRenderer(context, (SkinnedMeshRenderer9) node);
        else if (node instanceof SkinnedMeshRenderer10) return adaptSkinnedRenderer(context, (SkinnedMeshRenderer10) node);
        else if (node instanceof SkinnedMeshRenderer11) return adaptSkinnedRenderer(context, (SkinnedMeshRenderer11) node);
        else {
            throw new IOException("Unknown node : "+node.getClass());
        }

    }

    /**
     *
     * @param context
     * @param node
     * @return [GLNode,isRoot]
     * @throws IOException
     */
    public static Object[] readGameObject(AdaptorContext context, TypedNode node) throws IOException{
        final Object[] vals;
        final boolean active;
        if (node instanceof GameObject1){
            active = ((GameObject1) node).getm_IsActive()!=0;
            vals = adaptGameObject(context, ((GameObject1) node).getm_Component());
        } else if (node instanceof GameObject2){
            active = ((GameObject2) node).getm_IsActive()!=0;
            vals = adaptGameObject(context, ((GameObject2) node).getm_Component());
        } else if (node instanceof GameObject3){
            active = ((GameObject3) node).getm_IsActive();
            vals = adaptGameObject(context, ((GameObject3) node).getm_Component());
        } else {
            throw new IOException("Unknown node : "+node.getClass());
        }

        //((GLNode) vals[0]).setVisible(active);
        return vals;
    }

    private static Object[] adaptGameObject(AdaptorContext context, Sequence components) throws IOException{

        /* NOTES : simple scene elements often have the components :
        - Transform
        - MeshFilter
        - MeshRenderer
        */

        GLMesh meshNode = null;
        TypedNode rendererNode = null;
        Transform1 trsNode = null;
        for (int i=0,n=components.getSize();i<n;i++){
            final TypedNode tnode = (TypedNode) components.get(i);
            final Pair pair = (Pair) tnode.getValue();
            final UnityPointer pptr = (UnityPointer) pair.getValue2();

            GLMesh shell = context.getOrReadShell(pptr);
            if (shell!=null) meshNode = shell;

            final UnityAssetObject obj = context.registry.get(pptr);
            final TypedNode cdt = obj.decode(true);
            if (cdt instanceof MeshRenderer) rendererNode = cdt;
            else if (cdt instanceof Transform) trsNode = (Transform1) cdt;
            else if (cdt instanceof MeshFilter1){
                MeshFilter1 mf = (MeshFilter1) cdt;
                meshNode = context.getOrReadShell(mf.getm_Mesh());
            }
        }

        if (trsNode==null){
            throw new IOException("GameObject without transform ? that should not be possible.");
        }

        GraphicNode glNode = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        if (meshNode!=null && rendererNode!=null){
            if (rendererNode instanceof MeshRenderer1) glNode = adaptRenderer(context, (MeshRenderer1) rendererNode, meshNode);
            else if (rendererNode instanceof MeshRenderer2) glNode = adaptRenderer(context, (MeshRenderer2) rendererNode, meshNode);
            else if (rendererNode instanceof MeshRenderer3) glNode = adaptRenderer(context, (MeshRenderer3) rendererNode, meshNode);
            else if (rendererNode instanceof MeshRenderer4) glNode = adaptRenderer(context, (MeshRenderer4) rendererNode, meshNode);
            else if (rendererNode instanceof MeshRenderer5) glNode = adaptRenderer(context, (MeshRenderer5) rendererNode, meshNode);
            else if (rendererNode instanceof MeshRenderer6) glNode = adaptRenderer(context, (MeshRenderer6) rendererNode, meshNode);
            else if (rendererNode instanceof MeshRenderer7) glNode = adaptRenderer(context, (MeshRenderer7) rendererNode, meshNode);
            else if (rendererNode instanceof MeshRenderer8) glNode = adaptRenderer(context, (MeshRenderer8) rendererNode, meshNode);
        }

        //apply transform
        glNode.getNodeTransform().set(adaptTransform(trsNode));

        //build children
        Sequence children = trsNode.getm_Children();
        for (int i=0,n=children.getSize();i<n;i++){

            //chid pointer point to a transform object
            UnityPointer pptr = null;
            Object candidate = children.get(i);
            if (candidate instanceof TypedNode){
                final TypedNode tnode = (TypedNode) candidate;
                pptr = (UnityPointer)  tnode.getValue();
            } else if (candidate instanceof UnityPointer){
                pptr = (UnityPointer) candidate;
            }

            if (pptr!=null){
                final Transform1 childTrs = (Transform1) context.getNode(pptr);
                GraphicNode child = context.getOrReadGameObject(childTrs.getm_GameObject());
                if (child!=null){
                    glNode.getChildren().add(child);
                }
            } else {
                throw new IOException("Unexpeted type : "+candidate);
            }
        }

        return new Object[]{glNode,(trsNode.getm_Father().pathId==0 && trsNode.getm_Father().fileId==0)};
    }

    private static MotionModel adaptRenderer(AdaptorContext context, MeshRenderer1 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptRenderer(AdaptorContext context, MeshRenderer2 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptRenderer(AdaptorContext context, MeshRenderer3 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptRenderer(AdaptorContext context, MeshRenderer4 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptRenderer(AdaptorContext context, MeshRenderer5 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptRenderer(AdaptorContext context, MeshRenderer6 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptRenderer(AdaptorContext context, MeshRenderer7 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptRenderer(AdaptorContext context, MeshRenderer8 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptTrailRenderer(AdaptorContext context, TrailRenderer1 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptTrailRenderer(AdaptorContext context, TrailRenderer2 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptTrailRenderer(AdaptorContext context, TrailRenderer3 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptTrailRenderer(AdaptorContext context, TrailRenderer4 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptTrailRenderer(AdaptorContext context, TrailRenderer5 node, GLMesh meshPptr) throws IOException{
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptTrailRenderer(AdaptorContext context, TrailRenderer6 node, GLMesh meshPptr) throws IOException{
       return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer1 node) throws IOException{
        final UnityPointer meshPptr = node.getm_GameObject();
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer2 node) throws IOException{
        final UnityPointer meshPptr = node.getm_Mesh();
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer3 node) throws IOException{
        final UnityPointer meshPptr = node.getm_Mesh();
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer4 node) throws IOException{
        final UnityPointer meshPptr = node.getm_Mesh();
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer5 node) throws IOException{
        final UnityPointer meshPptr = node.getm_Mesh();
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer6 node) throws IOException{
        final UnityPointer meshPptr = node.getm_Mesh();
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer7 node) throws IOException{
        final UnityPointer meshPptr = node.getm_Mesh();
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer8 node) throws IOException{
        final UnityPointer meshPptr = node.getm_Mesh();
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer9 node) throws IOException{
        final UnityPointer meshPptr = node.getm_Mesh();
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer10 node) throws IOException{
        final UnityPointer meshPptr = node.getm_Mesh();
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptSkinnedRenderer(AdaptorContext context, SkinnedMeshRenderer11 node) throws IOException{
        final UnityPointer meshPptr = node.getm_Mesh();
        return adaptRenderer(context, node.getm_Materials(), meshPptr);
    }

    private static MotionModel adaptRenderer(AdaptorContext context, Sequence materialPptrs, UnityPointer meshPptr) throws IOException{

        GLMesh shell = context.getOrReadShell(meshPptr);
        return adaptRenderer(context, materialPptrs, shell);
    }

     private static MotionModel adaptRenderer(AdaptorContext context, Sequence materialPptrs, GLMesh meshNode) throws IOException{

        final MotionModel mpm = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);
        final science.unlicense.engine.opengl.mesh.GLModel mesh = toMesh(meshNode);
        mpm.setTitle(mesh.getTitle());
        mpm.getChildren().add(mesh);

        for (int i=0,n=materialPptrs.getSize();i<n;i++){
            final TypedNode tn = (TypedNode) materialPptrs.get(i);
            final UnityAssetObject materialObj = context.registry.get((UnityPointer) tn.getValue());
            if (materialObj==null){
                System.out.println("Missing material : "+tn.getValue());
                continue;
            }
            final Material3 material = (Material3) materialObj.decode(true);
            final UnityPropertySheet1 props = material.getm_SavedProperties();
            final Dictionary texEnvs = props.getm_TexEnvs();

            final SimpleBlinnPhong.Material mat = (SimpleBlinnPhong.Material) mesh.getMaterials().get(0);
            mesh.getMaterials().add(mat);

            //adapt textures
            final UnityTexEnv1 uteDiffuse = (UnityTexEnv1) texEnvs.getValue(TEXENV_MAIN);
            if (uteDiffuse!=null){
                final UnityPointer texturePptr = uteDiffuse.getm_Texture();
                final UnityAssetObject textureObj = context.registry.get(texturePptr);
                if (textureObj!=null){
                    final Texture2D tex = context.getOrReadTexture(textureObj.ref.toPointer());
                    final UVMapping mapping = new UVMapping(tex);
                    final Layer layer = new Layer(mapping);
                    layer.getTransform().getTranslation().set(uteDiffuse.getm_Offset());
                    layer.getTransform().getScale().set(uteDiffuse.getm_Scale());
                    mat.setDiffuseTexture(layer);
                } else {
                    System.out.println("Missing texture : "+texturePptr);
                }
            }
            final UnityTexEnv1 uteSpecular = (UnityTexEnv1) texEnvs.getValue(TEXENV_SPECULAR);
            if (uteSpecular!=null){
                final UnityPointer texturePptr = uteSpecular.getm_Texture();
                final UnityAssetObject textureObj = context.registry.get(texturePptr);
                if (textureObj!=null){
                    final Texture2D tex = context.getOrReadTexture(textureObj.ref.toPointer());
                    final UVMapping mapping = new UVMapping(tex);
                    final Layer layer = new Layer(mapping);
                    layer.getTransform().getTranslation().set(uteSpecular.getm_Offset());
                    layer.getTransform().getScale().set(uteSpecular.getm_Scale());
                    mat.setSpecularTexture(layer);
                } else {
                    System.out.println("Missing texture : "+texturePptr);
                }
            }
            //TODO bump mapping not correctly working in the engine
//            final UnityTexEnv1 uteBumpMap = (UnityTexEnv1) texEnvs.getValue(TEXENV_BUMPMAP);
//            if (uteBumpMap!=null){
//                final UnityPointer texturePptr = uteBumpMap.getTexturePptr();
//                final UnityAssetObject textureObj = asset.get(texturePptr);
//                final TypedNode textureNode = textureObj.decode();
//                final Image textureImage = UnityTextureAdaptor.read(textureNode);
//                final UVMapping mapping = new UVMapping(new Texture2D(textureImage));
//                mapping.setOffset(uteBumpMap.getOffset());
//                mapping.setScale(uteBumpMap.getScale());
//                mesh.getMaterial().getLayers().add(new Layer(mapping,Layer.TYPE_NORMAL));
//            }

            //adapt colors
            final Dictionary colors = props.getm_Colors();
            final Color diffuseColor = (Color) colors.getValue(COLOR_MAIN);
            if (diffuseColor!=null) mat.setDiffuse(diffuseColor);
            final Color specColor = (Color) colors.getValue(COLOR_SPECULAR);
            if (specColor!=null) mat.setSpecular(specColor);

            //adapt values
            final Dictionary floats = props.getm_Floats();
            final Float shininess = (Float) floats.getValue(FLOAT_SHININESS);
            if (shininess!=null) mat.setShininess(shininess);

            //materials sometimes appear multiples times in the list, likely with different shaders
            //we can only handle one
            //TODO more work needed to handle everything ...
            break;
        }

        return mpm;
    }


    public static GLMesh readMesh(AdaptorContext context, TypedNode node) throws IOException{

        if (node instanceof Mesh1) return adaptMesh(context,(Mesh1) node);
        else if (node instanceof Mesh2) return adaptMesh(context,(Mesh2) node);
        else if (node instanceof Mesh3) return adaptMesh(context,(Mesh3) node);
        else if (node instanceof Mesh4) return adaptMesh(context,(Mesh4) node);
        else if (node instanceof Mesh5) return adaptMesh(context,(Mesh5) node);
        else if (node instanceof Mesh6) return adaptMesh(context,(Mesh6) node);
        else if (node instanceof Mesh7) return adaptMesh(context,(Mesh7) node);
        else if (node instanceof Mesh8) return adaptMesh(context,(Mesh8) node);
        else if (node instanceof Mesh9) return adaptMesh(context,(Mesh9) node);
        else {
            throw new IOException("Unknown node : "+node.getClass());
        }

    }

    public static science.unlicense.engine.opengl.mesh.GLModel toMesh(GLMesh shell){
        final science.unlicense.engine.opengl.mesh.GLModel mesh = new science.unlicense.engine.opengl.mesh.GLModel();
        mesh.setTitle(shell.getName());
        mesh.setShape(shell);

        final SimpleBlinnPhong.Material mat = SimpleBlinnPhong.newMaterial();
        mesh.getMaterials().add(mat);
        mat.setDiffuse(Color.GRAY_LIGHT);
        //face culling information seems to be in the shader
        //see : http://forum.unity3d.com/threads/backface-culling-in-unity.48919
        ((Technique) mesh.getTechniques().get(0)).getState().setCulling(RenderState.CULLING_NONE);
        return mesh;
    }

    private static GLMesh adaptMesh(AdaptorContext context, Mesh1 node){
        throw new UnimplementedException("Not supported yet.");
    }

    private static GLMesh adaptMesh(AdaptorContext context, Mesh2 node){
        throw new UnimplementedException("Not supported yet.");
    }

    private static GLMesh adaptMesh(AdaptorContext context, Mesh3 node){
        throw new UnimplementedException("Not supported yet.");
    }

    private static GLMesh adaptMesh(AdaptorContext context, Mesh4 node){
        throw new UnimplementedException("Not supported yet.");
    }

    private static GLMesh adaptMesh(AdaptorContext context, Mesh5 node){
        throw new UnimplementedException("Not supported yet.");
    }

    private static GLMesh adaptMesh(AdaptorContext context, Mesh6 node){
        throw new UnimplementedException("Not supported yet.");
    }

    private static GLMesh adaptMesh(AdaptorContext context, Mesh7 node){

        final Sequence subs = node.getm_SubMeshes();
//        for (int i=0;i<subs.getSize();i++){
//            final SubMesh2 sub = (SubMesh2) subs.get(i);
//            final BBox bbox = sub.getLocalAABB();
//            final Mesh mesh = new GeometryMesh(bbox);
//            mesh.getMaterial().setDiffuse(Color.RED);
//            mpm.addChild(mesh);
//        }
        final BlendShapeData1 blendShape = node.getm_Shapes();
        final VertexData2 vertexData = node.getm_VertexData();
        final Sequence bindPoses = node.getm_BindPose();
        final Sequence skins = node.getm_Skin();
        for (int i=0,n=skins.getSize();i<n;i++){
            final BoneInfluence1 bi = (BoneInfluence1) skins.get(i);
        }


        //rebuild mesh shell
        final Buffer buffer = new DefaultInt8Buffer(vertexData.getm_DataSize());
        final Sequence channels = vertexData.getm_Channels();
        final Sequence streams = vertexData.getm_Streams();

        final GLMesh shell = new GLMesh();
        shell.setName(node.getName());

        /*
        Format analyze :
        - vertex data define channels, channels are like : vertice,normal,uv,...
        Those have a size and an offset in the block.
        by index it seems to be :
        0 : vertice
        1 : normal
        2 : ?
        3 : uv
        4 : ?
        5 : something of size 4 ? quaternion ? bone weights ?
        6 : ?
        7 : ?
        8 : ?
        - channel values are then paquet in streams, each stream has a stride and
        an offset in the byte buffer.
        */
        final int usedChannels = vertexData.getm_CurrentChannels().intValue();
        final int nbVertex = vertexData.getm_VertexCount().intValue();

        if ((usedChannels & 0x1) !=0){
            //read vertices
            final ChannelInfo1 chanVertex = (ChannelInfo1) channels.get(0);
            final int stream = chanVertex.getstream();
            final int offset = chanVertex.getoffset();
            final StreamInfo2 streamInfo = (StreamInfo2) streams.get(stream);
            final int byteOffset = streamInfo.getoffset().intValue();
            final int stride = streamInfo.getstride().intValue();
            final DataCursor cursor = buffer.dataCursor(Float32.TYPE, Endianness.LITTLE_ENDIAN);
            cursor.skipBytes(byteOffset);
            final float[] b = new float[nbVertex*3];
            for (int i=0;i<nbVertex;i++){
                cursor.skipBytes(offset);
                cursor.readFloat(b, i*3, 3);
                cursor.skipBytes(stride-offset-12);
            }
            shell.setPositions(new VBO(b, 3));
        }
        if ((usedChannels & 0x2) !=0){
            //read normals
            final ChannelInfo1 chanNormal = (ChannelInfo1) channels.get(1);
            final int stream = chanNormal.getstream();
            final int offset = chanNormal.getoffset();
            final StreamInfo2 streamInfo = (StreamInfo2) streams.get(stream);
            final int byteOffset = streamInfo.getoffset().intValue();
            final int stride = streamInfo.getstride().intValue();
            final DataCursor cursor = buffer.dataCursor(Float32.TYPE, Endianness.LITTLE_ENDIAN);
            cursor.skipBytes(byteOffset);
            final float[] b = new float[nbVertex*3];
            for (int i=0;i<nbVertex;i++){
                cursor.skipBytes(offset);
                cursor.readFloat(b, i*3, 3);
                cursor.skipBytes(stride-offset-12);
            }
            shell.setNormals(new VBO(b, 3));
        }
        if ((usedChannels & 0x8) !=0){
            //read uvs
            final ChannelInfo1 chanUV = (ChannelInfo1) channels.get(3);
            final int stream = chanUV.getstream();
            final int offset = chanUV.getoffset();
            final StreamInfo2 streamInfo = (StreamInfo2) streams.get(stream);
            final int byteOffset = streamInfo.getoffset().intValue();
            final int stride = streamInfo.getstride().intValue();
            final DataCursor cursor = buffer.dataCursor(Float32.TYPE, Endianness.LITTLE_ENDIAN);
            cursor.skipBytes(byteOffset);
            final float[] b = new float[nbVertex*2];
            for (int i=0;i<nbVertex;i++){
                cursor.skipBytes(offset);
                cursor.readFloat(b, i*2, 2);
                cursor.skipBytes(stride-offset-8);
            }
            shell.setUVs(new VBO(b, 2));
        }

        //rebuild index
        final byte[] indexBuffer = ((ByteSequence) node.getm_IndexBuffer()).toArrayByte();
        final Buffer ib = new DefaultInt8Buffer(indexBuffer,UInt16.TYPE,Endianness.LITTLE_ENDIAN);
        final IBO ibo = new IBO();
        ibo.setBuffer(ib, science.unlicense.gpu.api.opengl.GLC.GL_UNSIGNED_SHORT, 3);
        shell.setIndex(ibo);
        shell.setRanges(new IndexedRange[]{new IndexedRange(GLC.PRIMITIVE.TRIANGLES, 3, 0, indexBuffer.length/2)});


        if (shell.getNormals()==null){
            science.unlicense.geometry.impl.Mesh.calculateNormals(shell);
        }
        if (shell.getNormals()!=null && shell.getUVs()!=null && shell.getTangents()==null){
            science.unlicense.geometry.impl.Mesh.calculateTangents(shell);
        }

        return shell;
    }

    private static GLMesh adaptMesh(AdaptorContext context, Mesh8 node){

        final Sequence subs = node.getm_SubMeshes();
//        for (int i=0;i<subs.getSize();i++){
//            final SubMesh3 sub = (SubMesh3) subs.get(i);
//            System.out.println("sub "+sub.getvertexCount());
//            final BBox bbox = sub.getLocalAABB();
//            final Mesh mesh = new GeometryMesh(bbox);
//            mesh.getMaterial().setDiffuse(Color.RED);
//            mpm.addChild(mesh);
//        }
        final BlendShapeData1 blendShape = node.getm_Shapes();
        final VertexData2 vertexData = node.getm_VertexData();
        final Sequence bindPoses = node.getm_BindPose();
        final Sequence skins = node.getm_Skin();
        for (int i=0,n=skins.getSize();i<n;i++){
            final BoneInfluence1 bi = (BoneInfluence1) skins.get(i);
        }

//        System.out.println("Name : "+node.getm_Name());
//        System.out.println("Skin : "+node.getm_Skin().getSize());
//        System.out.println("NbSub : "+node.getm_SubMeshes().getSize());
//        System.out.println("NbBone : "+node.getm_BoneNameHashes().getSize());
//        System.out.println("Compressed Triangles : "+node.getm_CompressedMesh().getm_Triangles().getm_NumItems());
//        System.out.println("Nb keyframe : "+node.getm_Shapes().getshapes().getSize());


        //rebuild mesh shell
        final Buffer buffer = new DefaultInt8Buffer(vertexData.getm_DataSize());
        final Sequence channels = vertexData.getm_Channels();
        final Sequence streams = vertexData.getm_Streams();

        final GLMesh shell = new GLMesh();
        shell.setName(node.getName());

        /*
        Format analyze :
        - vertex data define channels, channels are like : vertice,normal,uv,...
        Those have a size and an offset in the block.
        by index it seems to be :
        0 : vertice
        1 : normal
        2 : ?
        3 : uv
        4 : ?
        5 : something of size 4 ? quaternion ? bone weights ?
        6 : ?
        7 : ?
        8 : ?
        - channel values are then paquet in streams, each stream has a stride and
        an offset in the byte buffer.
        */
        final int usedChannels = vertexData.getm_CurrentChannels().intValue();
        final int nbVertex = vertexData.getm_VertexCount().intValue();

        if ((usedChannels & 0x1) !=0){
            //read vertices
            final ChannelInfo1 chanVertex = (ChannelInfo1) channels.get(0);
            final int stream = chanVertex.getstream();
            final int offset = chanVertex.getoffset();
            final StreamInfo2 streamInfo = (StreamInfo2) streams.get(stream);
            final int byteOffset = streamInfo.getoffset().intValue();
            final int stride = streamInfo.getstride().intValue();
            final DataCursor cursor = buffer.dataCursor(Float32.TYPE, Endianness.LITTLE_ENDIAN);
            cursor.skipBytes(byteOffset);
            final float[] b = new float[nbVertex*3];
            for (int i=0;i<nbVertex;i++){
                cursor.skipBytes(offset);
                cursor.readFloat(b, i*3, 3);
                cursor.skipBytes(stride-offset-12);
            }
            shell.setPositions(new VBO(b, 3));
        }
        if ((usedChannels & 0x2) !=0){
            //read normals
            final ChannelInfo1 chanNormal = (ChannelInfo1) channels.get(1);
            final int stream = chanNormal.getstream();
            final int offset = chanNormal.getoffset();
            final StreamInfo2 streamInfo = (StreamInfo2) streams.get(stream);
            final int byteOffset = streamInfo.getoffset().intValue();
            final int stride = streamInfo.getstride().intValue();
            final DataCursor cursor = buffer.dataCursor(Float32.TYPE, Endianness.LITTLE_ENDIAN);
            cursor.skipBytes(byteOffset);
            final float[] b = new float[nbVertex*3];
            for (int i=0;i<nbVertex;i++){
                cursor.skipBytes(offset);
                cursor.readFloat(b, i*3, 3);
                cursor.skipBytes(stride-offset-12);
            }
            shell.setNormals(new VBO(b, 3));
        }
        if ((usedChannels & 0x8) !=0){
            //read uvs
            final ChannelInfo1 chanUV = (ChannelInfo1) channels.get(3);
            final int stream = chanUV.getstream();
            final int offset = chanUV.getoffset();
            final StreamInfo2 streamInfo = (StreamInfo2) streams.get(stream);
            final int byteOffset = streamInfo.getoffset().intValue();
            final int stride = streamInfo.getstride().intValue();
            final DataCursor cursor = buffer.dataCursor(Float32.TYPE, Endianness.LITTLE_ENDIAN);
            cursor.skipBytes(byteOffset);
            final float[] b = new float[nbVertex*2];
            for (int i=0;i<nbVertex;i++){
                cursor.skipBytes(offset);
                cursor.readFloat(b, i*2, 2);
                cursor.skipBytes(stride-offset-8);
            }
            shell.setUVs(new VBO(b, 2));
        }

        //rebuild index
        final byte[] indexBuffer = ((ByteSequence) node.getm_IndexBuffer()).toArrayByte();
        final Buffer ib = new DefaultInt8Buffer(indexBuffer,UInt16.TYPE,Endianness.LITTLE_ENDIAN);
        final IBO ibo = new IBO();
        ibo.setBuffer(ib, science.unlicense.gpu.api.opengl.GLC.GL_UNSIGNED_SHORT, 3);
        shell.setIndex(ibo);
        shell.setRanges(new IndexedRange[]{new IndexedRange(GLC.PRIMITIVE.TRIANGLES, 3, 0, indexBuffer.length/2)});

        if (shell.getNormals()==null){
            science.unlicense.geometry.impl.Mesh.calculateNormals(shell);
        }
        if (shell.getNormals()!=null && shell.getUVs()!=null && shell.getTangents()==null){
            science.unlicense.geometry.impl.Mesh.calculateTangents(shell);
        }

        return shell;
    }

    private static GLMesh adaptMesh(AdaptorContext context,Mesh9 node){
        throw new UnimplementedException("Not supported yet.");
    }

    private static Similarity adaptTransform(Transform1 transform){
        final SimilarityRW trs = SimilarityNd.create(3);
        trs.getScale().set(transform.getm_LocalScale());
        trs.getTranslation().set(transform.getm_LocalPosition());
        trs.getRotation().set(transform.getm_LocalRotation().toMatrix3());
        trs.notifyChanged();
        return trs;
    }

}
