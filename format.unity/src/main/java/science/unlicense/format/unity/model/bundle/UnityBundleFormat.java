
package science.unlicense.format.unity.model.bundle;

import science.unlicense.archive.api.AbstractArchiveFormat;
import science.unlicense.archive.api.Archive;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.format.unity.UnityConstants;

/**
 *
 * @author Johann Sorel
 */
public class UnityBundleFormat extends AbstractArchiveFormat {

    public static final UnityBundleFormat INSTANCE = new UnityBundleFormat();

    private UnityBundleFormat() {
        super(new Chars("unitybundle"));
        shortName = new Chars("Unity-Bundle");
        longName = new Chars("Unity-Bundle");
        mimeTypes.add(new Chars("application/x-unity-bundle"));
        extensions.add(new Chars("unity3d"));
        signatures.add(UnityConstants.SIGNATURE_RAW);
        signatures.add(UnityConstants.SIGNATURE_WEB);
    }

    @Override
    public boolean canDecode(Object candidate) {
        if (candidate instanceof Path) {
            return ((Path) candidate).getName().toLowerCase().endsWith(new Chars(".unity3d"));
        }
        return false;
    }

    @Override
    public Archive open(Object candidate) throws IOException {
        if (candidate instanceof Path) {
            return new UnityBundle(candidate);
        }
        throw new IOException("Unsupported input");
    }

    public boolean isAbsolute() {
        return false;
    }

    public boolean canCreate(Path base) throws IOException {
        return false;
    }

    public PathResolver createResolver(Path base) throws IOException {
        return open(base);
    }

}
