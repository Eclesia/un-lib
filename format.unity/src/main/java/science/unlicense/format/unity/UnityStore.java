
package science.unlicense.format.unity;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.unity.adaptor.UnityAdaptor;
import science.unlicense.format.unity.model.asset.UnityAsset;
import science.unlicense.format.unity.model.asset.UnityAssetReference;
import science.unlicense.format.unity.model.asset.UnitySharedAssets;
import science.unlicense.format.unity.model.bundle.UnityBundle;
import science.unlicense.format.unity.model.bundle.UnityBundleEntry;

/**
 *
 * @author Johann Sorel
 */
public class UnityStore extends AbstractModel3DStore {

    private UnityAsset sceneAsset;
    private UnitySharedAssets registry;

    public UnityStore(Object input) {
        super(UnityBundleFormat.INSTANCE, input);
    }

    public UnitySharedAssets getRegistry() throws IOException {
        getAssets();
        return registry;
    }

    private synchronized void getAssets() throws IOException {
        if (registry!=null) return;
        final Path path = (Path) getInput();

        registry = new UnitySharedAssets();

        if (path.isContainer()){
            //make a group with all assets files in folder
            searchRecursive(path);

        } else if (path.getName().toLowerCase().endsWith(new Chars(".unity3d"))){
            //bundle file
            final UnityBundle bundle = new UnityBundle(path);
            final Iterator ite = bundle.getChildren().createIterator();
            while (ite.hasNext()){
                final UnityAsset asset = new UnityAsset((UnityBundleEntry) ite.next());
                asset.read();
                registry.getAssetFiles().add(asset);
            }
        } else if (path.getName().toLowerCase().startsWith(new Chars("level"))){
            //scene file, load reference files too
            sceneAsset = loadRecursive(registry, path, new HashSet());

        } else {
            //ahndle it as a single asset file
            final UnityAsset asset = new UnityAsset(path);
            asset.read();
            registry.getAssetFiles().add(asset);
        }

    }

    private void searchRecursive(Path path) throws IOException{

        if (path.isContainer()){
            final Iterator ite = path.getChildren().createIterator();
            while (ite.hasNext()) {
                searchRecursive((Path) ite.next());
            }
        } else if (path.getName().endsWith(new Chars(".assets")) || path.getName().startsWith(new Chars("level"))
                 ){
            final UnityAsset asf = new UnityAsset(path);
            asf.read();
            registry.getAssetFiles().add(asf);
        }

    }

    private UnityAsset loadRecursive(UnitySharedAssets registry, Path path, Set visited) throws IOException{
        if (visited.contains(path)) return null;
        visited.add(path);

        if (!path.exists()){
            System.out.println("Missing resource : "+ path);
            return null;
        }

        final UnityAsset asset = new UnityAsset(path);
        asset.read();
        registry.getAssetFiles().add(asset);

        //loop on relations
        for (int i=0,n=asset.assetRefs.getSize();i<n;i++){
            final UnityAssetReference ref = (UnityAssetReference) asset.assetRefs.get(i);
            final Path childPath = path.getParent().resolve(ref.filePath);
            loadRecursive(registry, childPath, visited);
        }

        return asset;
    }

    public Collection getElements() throws StoreException {

        try {
            getAssets();
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        final Sequence elements = new ArraySequence();


        try{
            if (sceneAsset!=null){
                //load only scene elements
                elements.addAll(UnityAdaptor.convert(sceneAsset.getIterator(),registry,false));
            } else {
                //load eveything we find
                elements.addAll(UnityAdaptor.convert(registry.getIterator(),registry,true));
            }

        }catch(Exception ex){
            throw new StoreException(ex);
        }

        return elements;
    }


}
