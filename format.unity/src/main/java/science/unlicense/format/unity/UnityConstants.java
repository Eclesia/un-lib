
package science.unlicense.format.unity;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class UnityConstants {

    public static final byte[] SIGNATURE_RAW = new byte[]{'U','n','i','t','y','R','a','w'};
    public static final byte[] SIGNATURE_WEB = new byte[]{'U','n','i','t','y','W','e','b'};
    public static final Chars SIGNATURE_RAW_CHARS = Chars.constant("UnityRaw",CharEncodings.US_ASCII);
    public static final Chars SIGNATURE_WEB_CHARS = Chars.constant("UnityWeb",CharEncodings.US_ASCII);

    //from doc : http://docs.unity3d.com/Manual/ClassIDReference.html
    public static final int CLASS_GAMEOBJECT                = 1;
    public static final int CLASS_COMPONENT                 = 2;
    public static final int CLASS_LEVELGAMEMANAGER          = 3;
    public static final int CLASS_TRANSFORM                 = 4;
    public static final int CLASS_TIMEMANAGER               = 5;
    public static final int CLASS_GLOBALGAMEMANAGER         = 6;
    public static final int CLASS_GAMEMANAGER               = 7;
    public static final int CLASS_BEHAVIOUR                 = 8;
    public static final int CLASS_GAMEMANAGER1              = 9;
    public static final int CLASS_AUDIOMANAGER              = 11;
    public static final int CLASS_PARTICLEANIMATOR          = 12;
    public static final int CLASS_INPUTMANAGER              = 13;
    public static final int CLASS_ELLIPSOIDPARTICLEEMITTER  = 15;
    public static final int CLASS_PIPELINE                  = 17;
    public static final int CLASS_EDITOREXTENSION           = 18;
    public static final int CLASS_PHYSICS2DSETTINGS         = 19;
    public static final int CLASS_CAMERA                    = 20;
    public static final int CLASS_MATERIAL                  = 21;
    public static final int CLASS_MESHRENDERER              = 23;
    public static final int CLASS_RENDERER                  = 25;
    public static final int CLASS_PARTICLERENDERER          = 26;
    public static final int CLASS_TEXTURE                   = 27;
    public static final int CLASS_TEXTURE2D                 = 28;
    public static final int CLASS_SCENE                     = 29;
    public static final int CLASS_RENDERMANAGER             = 30;
    public static final int CLASS_PIPELINEMANAGER           = 31;
    public static final int CLASS_MESHFILTER                = 33;
    public static final int CLASS_GAMEMANAGER2              = 35;
    public static final int CLASS_OCCLUSIONPORTAL           = 41;
    public static final int CLASS_MESH                      = 43;
    public static final int CLASS_SKYBOX                    = 45;
    public static final int CLASS_GAMEMANAGER3              = 46;
    public static final int CLASS_QUALITYSETTINGS           = 47;
    public static final int CLASS_SHADER                    = 48;
    public static final int CLASS_TEXTASSET                 = 49;
    public static final int CLASS_RIGIDBODY2D               = 50;
    public static final int CLASS_PHYSICS2DMANAGER          = 51;
    public static final int CLASS_NOTIFICATIONMANAGER       = 52;
    public static final int CLASS_RIGIDBODY                 = 54;
    public static final int CLASS_PHYSICSMANAGER            = 55;
    public static final int CLASS_COLLIDER                  = 56;
    public static final int CLASS_JOINT                     = 57;
    public static final int CLASS_CIRCLECOLLIDER2D          = 58;
    public static final int CLASS_HINGEJOINT                = 59;
    public static final int CLASS_POLYGONECOLLIDER2D        = 60;
    public static final int CLASS_BOXCOLLIDER2D             = 61;
    public static final int CLASS_PHYSICSCOLLIDER2D         = 62;
    public static final int CLASS_GAMEMANAGER4              = 63;
    public static final int CLASS_MESHCOLLIDER              = 64;
    public static final int CLASS_BOXCOLLIDER               = 65;
    public static final int CLASS_SPRITECOLLIDER2D          = 66;
    public static final int CLASS_EDGECOLLIDER2D            = 68;
    public static final int CLASS_ANIMATIONMANAGER          = 71;
    public static final int CLASS_COMPUTESHADER             = 72;
    public static final int CLASS_ANIMATIONCLIP             = 74;
    public static final int CLASS_CONSTANTFORCE             = 75;
    public static final int CLASS_WORLDPARTICLECOLLIDER     = 76;
    public static final int CLASS_TAGMANAGER                = 78;
    public static final int CLASS_AUDIOLISTENER             = 81;
    public static final int CLASS_AUDIOSOURCE               = 82;
    public static final int CLASS_AUDIOCLIP                 = 83;
    public static final int CLASS_RENDERTEXTURE             = 84;
    public static final int CLASS_MESHPARTICLEEMITTER       = 87;
    public static final int CLASS_PARTICLEEMITTER           = 88;
    public static final int CLASS_CUBEMAP                   = 89;
    public static final int CLASS_AVATAR                    = 90;
    public static final int CLASS_ANIMATORCONTROLLER        = 91;
    public static final int CLASS_GUILAYER                  = 92;
    public static final int CLASS_RUNTIMEANIMATORCONTROLLER = 93;
    public static final int CLASS_SCRIPTMAPPER              = 94;
    public static final int CLASS_ANIMATOR                  = 95;
    public static final int CLASS_TRAILRENDERER             = 96;
    public static final int CLASS_DELAYEDCALLMANAGER        = 98;
    public static final int CLASS_TEXTMESH                  = 102;
    public static final int CLASS_RENDERSETTINGS            = 104;
    public static final int CLASS_LIGHT                     = 108;
    public static final int CLASS_CGPROGRAM                 = 109;
    public static final int CLASS_BASEANIMATIONTRACK        = 110;
    public static final int CLASS_ANIMATION                 = 111;
    public static final int CLASS_MONOBEHAVIOUR             = 114;
    public static final int CLASS_MONOSCRIPT                = 115;
    public static final int CLASS_MONOMANAGER               = 116;
    public static final int CLASS_TEXTURE3D                 = 117;
    public static final int CLASS_NEWANIMATIONTRACK         = 118;
    public static final int CLASS_PROJECTOR                 = 119;
    public static final int CLASS_LINERENDERER              = 120;
    public static final int CLASS_FLARE                     = 121;
    public static final int CLASS_HALO                      = 122;
    public static final int CLASS_LENSFLARE                 = 123;
    public static final int CLASS_FLARELAYER                = 124;
    public static final int CLASS_HALOLAYER                 = 125;
    public static final int CLASS_NAVMESHLAYERS             = 126;
    public static final int CLASS_HALOMANAGER               = 127;
    public static final int CLASS_FONT                      = 128;
    public static final int CLASS_PLAYERSETTINGS            = 129;
    public static final int CLASS_NAMEDOBJECT               = 130;
    public static final int CLASS_GUITEXTURE                = 131;
    public static final int CLASS_GUITEXT                   = 132;
    public static final int CLASS_GUIELEMENT                = 133;
    public static final int CLASS_PHYSICMATERIAL            = 134;
    public static final int CLASS_SPHERECOLLIDER            = 135;
    public static final int CLASS_CAPSULECOLLIDER           = 136;
    public static final int CLASS_SKINNEDMESHRENDERER       = 137;
    public static final int CLASS_FIXEDJOINT                = 138;
    public static final int CLASS_RAYCASTCOLLIDER           = 140;
    public static final int CLASS_BUILDSETTINGS             = 141;
    public static final int CLASS_ASSETBUNDLE               = 142;
    public static final int CLASS_CHARACTERCONTROLLER       = 143;
    public static final int CLASS_CHARACTERJOINT            = 144;
    public static final int CLASS_SPRINGJOINT               = 145;
    public static final int CLASS_WHEELCOLLIDER             = 146;
    public static final int CLASS_RESOURCEMANAGER           = 147;
    public static final int CLASS_NETWORKVIEW               = 148;
    public static final int CLASS_NETWORKMANAGER            = 149;
    public static final int CLASS_PRELOADDATA               = 150;
    public static final int CLASS_MOVIETEXTURE              = 152;
    public static final int CLASS_CONFIGURABLEJOINT         = 153;
    public static final int CLASS_TERRAINCOLLIDER           = 154;
    public static final int CLASS_MASTERSERVERINTERFACE     = 155;
    public static final int CLASS_TERRAINDATA               = 156;
    public static final int CLASS_LIGHTMAPSETTINGS          = 157;
    public static final int CLASS_WEBCAMTEXTURE             = 158;
    public static final int CLASS_EDITORSETTINGS            = 159;
    public static final int CLASS_INTERACTIVECLOTH          = 160;
    public static final int CLASS_CLOTHRENDERER             = 161;
    public static final int CLASS_EDITORUSERSETTINGS        = 162;
    public static final int CLASS_SKINNEDCLOTH              = 163;
    public static final int CLASS_AUDIOREVERBFILTER         = 164;
    public static final int CLASS_AUDIOHIGHPASSFILTER       = 165;
    public static final int CLASS_AUDIOCHORUSFILTER         = 166;
    public static final int CLASS_AUDIOREVERBZONE           = 167;
    public static final int CLASS_AUDIOECHOFILTER           = 168;
    public static final int CLASS_AUDIOLOWPASSFILTER        = 169;
    public static final int CLASS_AUDIODISTORTIONFILTER     = 170;
    public static final int CLASS_SPARSETEXTURE             = 171;
    public static final int CLASS_AUDIOBEHAVIOUR            = 180;
    public static final int CLASS_AUDIOFILTER               = 181;
    public static final int CLASS_WINDZONE                  = 182;
    public static final int CLASS_CLOTH                     = 183;
    public static final int CLASS_SUBSTANCEARCHIVE          = 184;
    public static final int CLASS_PROCEDURALMATERIAL        = 185;
    public static final int CLASS_PROCEDURALTEXTURE         = 186;
    public static final int CLASS_OFFMESHLINK               = 191;
    public static final int CLASS_OCCLUSIONAREA             = 192;
    public static final int CLASS_TREE                      = 193;
    public static final int CLASS_NAVMESH                   = 194;
    public static final int CLASS_NAVMESHAGENT              = 195;
    public static final int CLASS_NAVMESHSETTINGS           = 196;
    public static final int CLASS_LIGHTPROBECLOUD           = 197;
    public static final int CLASS_PARTICLESYSTEM            = 198;
    public static final int CLASS_PARTICLESYSTEMRENDERER    = 199;
    public static final int CLASS_SHADERVARIANTCOLLECTION   = 200;
    public static final int CLASS_LODGROUP                  = 205;
    public static final int CLASS_BLENDTREE                 = 206;
    public static final int CLASS_MOTION                    = 207;
    public static final int CLASS_NAVMESHOBSTACLE           = 208;
    public static final int CLASS_TERRAININSTANCE           = 210;
    public static final int CLASS_SPRITERENDERER            = 212;
    public static final int CLASS_SPRITE                    = 213;
    public static final int CLASS_CACHEDSPRITEATLAS         = 214;
    public static final int CLASS_REFLECTIONPROBE           = 215;
    public static final int CLASS_REFLECTIONPROBES          = 216;
    public static final int CLASS_LIGHTPROBEGROUP           = 220;
    public static final int CLASS_ANIMATOROVERRIDECONTROLLER= 221;
    public static final int CLASS_CANVASRENDERER            = 222;
    public static final int CLASS_CANVAS                    = 223;
    public static final int CLASS_RECTTRANSFORM             = 224;
    public static final int CLASS_CANVASGROUP               = 225;
    public static final int CLASS_BILLBOARDASSET            = 226;
    public static final int CLASS_BILLBOARDRENDERER         = 227;
    public static final int CLASS_SPEEDTREEWINDASSET        = 228;
    public static final int CLASS_ANCHOREDJOINT2D           = 229;
    public static final int CLASS_JOINT2D                   = 230;
    public static final int CLASS_SPRINGJOINT2D             = 231;
    public static final int CLASS_DISTANCEJOINT2D           = 232;
    public static final int CLASS_HINGEJOINT2D              = 233;
    public static final int CLASS_SLIDERJOINT2D             = 234;
    public static final int CLASS_WHEELJOINT2D              = 235;
    public static final int CLASS_NAVMESHDATA               = 238;
    public static final int CLASS_AUDIOMIXER                = 240;
    public static final int CLASS_AUDIOMIXERCONTROLLER      = 241;
    public static final int CLASS_AUDIOMIXERGROUPCONTROLLER = 243;
    public static final int CLASS_AUDIOMIXEREFFECTCONTROLLER= 244;
    public static final int CLASS_AUDIOMIXERSNAPSHOTCONTROLLER= 245;
    public static final int CLASS_PHYSICSUPDATEBEHAVIOR     = 246;
    public static final int CLASS_CONSTANTFORCE2D           = 247;
    public static final int CLASS_EFFECTOR2D                = 248;
    public static final int CLASS_AREAEFFECTOR2D            = 249;
    public static final int CLASS_POINTEFFECTOR2D           = 250;
    public static final int CLASS_PLATFORMEFFECTOR2D        = 251;
    public static final int CLASS_SURFACEEFFECTOR2D         = 252;
    public static final int CLASS_LIGHTPROBES               = 258;
    public static final int CLASS_SAMPLECLIP                = 271;
    public static final int CLASS_AUDIOMIXERSNAPSHOT        = 272;
    public static final int CLASS_AUDIOMIXERGROUP           = 273;
    public static final int CLASS_ASSETBUNDLEMANIFEST       = 290;
    public static final int CLASS_PREFAB                    = 1001;
    public static final int CLASS_EDITOREXTENSIONIMPL       = 1002;
    public static final int CLASS_ASSETIMPORTER             = 1003;
    public static final int CLASS_ASSETDATABASE             = 1004;
    public static final int CLASS_MESH3DSIMPORTER           = 1005;
    public static final int CLASS_TEXTUREIMPORTER           = 1006;
    public static final int CLASS_SHADERIMPORTER            = 1007;
    public static final int CLASS_COMPUTESHADERIMPORTER     = 1008;
    public static final int CLASS_AVATARMASK                = 1011;
    public static final int CLASS_AUDIOIMPORTER             = 1020;
    public static final int CLASS_HIERARCHYSTATE            = 1026;
    public static final int CLASS_GUIDSERIALIZER            = 1027;
    public static final int CLASS_ASSETMETADATA             = 1028;
    public static final int CLASS_DEFAULTASSET              = 1029;
    public static final int CLASS_DEFAULTIMPORTER           = 1030;
    public static final int CLASS_TEXTSCRIPTIMPORTER        = 1031;
    public static final int CLASS_SCENEASSET                = 1032;
    public static final int CLASS_NATIVEFORMATIMPORTER      = 1034;
    public static final int CLASS_MONOIMPORTER              = 1035;
    public static final int CLASS_ASSETSERVERCACHE          = 1037;
    public static final int CLASS_LIBRARYASSETIMPORTER      = 1038;
    public static final int CLASS_MODELIMPORTER             = 1040;
    public static final int CLASS_FBXIMPORTER               = 1041;
    public static final int CLASS_TRUETYPEFONTIMPORTER      = 1042;
    public static final int CLASS_MOVIEIMPORTER             = 1044;
    public static final int CLASS_EDITORBUILDSETTINGS       = 1045;
    public static final int CLASS_DDSIMPORTER               = 1046;
    public static final int CLASS_INSPECTOREXPANDEDSTATE    = 1048;
    public static final int CLASS_ANNOTATIONMANAGER         = 1049;
    public static final int CLASS_MONOASSEMBLYIMPORTER      = 1050;
    public static final int CLASS_EDITORUSERBUILDSETTINGS   = 1051;
    public static final int CLASS_PVRIMPORTER               = 1052;
    public static final int CLASS_ASTCIMPORTER              = 1053;
    public static final int CLASS_KTXIMPORTER               = 1054;
    public static final int CLASS_ANIMATORSTATETRANSITION   = 1101;
    public static final int CLASS_ANIMATORSTATE             = 1102;
    public static final int CLASS_HUMANTEMPLATE             = 1105;
    public static final int CLASS_ANIMATORSTATEMACHINE      = 1107;
    public static final int CLASS_PREVIEWASSETTYPE          = 1108;
    public static final int CLASS_ANIMATORTRANSITION        = 1109;
    public static final int CLASS_SPEEDTREEIMPORTER         = 1110;
    public static final int CLASS_ANIMATORTRANSITIONBASE    = 1111;
    public static final int CLASS_SUBSTANCEIMPORTER         = 1112;
    public static final int CLASS_LIGHTMAPPARAMETERS        = 1113;
    public static final int CLASS_LIGHTMAPSNAPSHOT          = 1120;

    public static final int TEXFORMAT_ALPHA8        = 1;
    public static final int TEXFORMAT_ARGB4444      = 2;
    public static final int TEXFORMAT_RGB24         = 3;
    public static final int TEXFORMAT_RGBA32        = 4;
    public static final int TEXFORMAT_ARGB32        = 5;
    public static final int TEXFORMAT_UNUSED06      = 6;
    public static final int TEXFORMAT_RGB565        = 7;
    public static final int TEXFORMAT_UNUSED08      = 8;
    public static final int TEXFORMAT_UNUSED09      = 9;
    public static final int TEXFORMAT_DXT1          = 10;
    public static final int TEXFORMAT_UNUSED11      = 11;
    public static final int TEXFORMAT_DXT5          = 12;
    public static final int TEXFORMAT_RGBA4444      = 13;
    public static final int TEXFORMAT_UNUSED14      = 14;
    public static final int TEXFORMAT_UNUSED15      = 15;
    public static final int TEXFORMAT_UNUSED16      = 16;
    public static final int TEXFORMAT_UNUSED17      = 17;
    public static final int TEXFORMAT_UNUSED18      = 18;
    public static final int TEXFORMAT_UNUSED19      = 19;
    public static final int TEXFORMAT_WIII4         = 20;
    public static final int TEXFORMAT_WIII8         = 21;
    public static final int TEXFORMAT_WIIIA4        = 22;
    public static final int TEXFORMAT_WIIIA8        = 23;
    public static final int TEXFORMAT_WIIRGB565     = 24;
    public static final int TEXFORMAT_WIIRGB5A3     = 25;
    public static final int TEXFORMAT_WIIRGBA8      = 26;
    public static final int TEXFORMAT_WIICMPR       = 27;
    public static final int TEXFORMAT_UNUSED28      = 28;
    public static final int TEXFORMAT_UNUSED29      = 29;
    public static final int TEXFORMAT_PVRTC_RGB2    = 30;
    public static final int TEXFORMAT_PVRTC_RGBA2   = 31;
    public static final int TEXFORMAT_PVRTC_RGB4    = 32;
    public static final int TEXFORMAT_PVRTC_RGBA4   = 33;
    public static final int TEXFORMAT_ETC_RGB4      = 34;
    public static final int TEXFORMAT_ATC_RGB4      = 35;
    public static final int TEXFORMAT_ATC_RGBA8     = 36;
    public static final int TEXFORMAT_BGRA32        = 37;
    public static final int TEXFORMAT_ATF_RGB_DXT1  = 38;
    public static final int TEXFORMAT_ATF_RGBA_JPG  = 39;
    public static final int TEXFORMAT_ATF_RGB_JPG   = 40;
    public static final int TEXFORMAT_EAC_R         = 41;
    public static final int TEXFORMAT_EAC_R_SIGNED  = 42;
    public static final int TEXFORMAT_EAC_RG        = 43;
    public static final int TEXFORMAT_EAC_RG_SIGNED = 44;
    public static final int TEXFORMAT_ETC2_RGB4     = 45;
    public static final int TEXFORMAT_ETC2_RGB4_PUNCHTHROUGH_ALPHA = 46;
    public static final int TEXFORMAT_ETC2_RGBA8    = 47;
    public static final int TEXFORMAT_ASTC_RGB_4x4  = 48;
    public static final int TEXFORMAT_ASTC_RGB_5x5  = 49;
    public static final int TEXFORMAT_ASTC_RGB_6x6  = 50;
    public static final int TEXFORMAT_ASTC_RGB_8x8  = 51;
    public static final int TEXFORMAT_ASTC_RGB_10x10= 52;
    public static final int TEXFORMAT_ASTC_RGB_12x12= 53;
    public static final int TEXFORMAT_ASTC_RGBA_4x4 = 54;
    public static final int TEXFORMAT_ASTC_RGBA_5x5 = 55;
    public static final int TEXFORMAT_ASTC_RGBA_6x6 = 56;
    public static final int TEXFORMAT_ASTC_RGBA_8x8 = 57;
    public static final int TEXFORMAT_ASTC_RGBA_10x10 = 58;
    public static final int TEXFORMAT_ASTC_RGBA_12x12 = 59;

    public static final int AUDIO_UNKNOWN       = 1;
    public static final int AUDIO_ACC           = 2;
    public static final int AUDIO_AIFF          = 3;
    public static final int AUDIO_UNUSED04      = 4;
    public static final int AUDIO_UNUSED05      = 5;
    public static final int AUDIO_UNUSED06      = 6;
    public static final int AUDIO_UNUSED07      = 7;
    public static final int AUDIO_UNUSED08      = 8;
    public static final int AUDIO_GCADPCM       = 9;
    public static final int AUDIO_IT            = 10;
    public static final int AUDIO_UNUSED11      = 11;
    public static final int AUDIO_MOD           = 12;
    public static final int AUDIO_MPEG          = 13;
    public static final int AUDIO_OGGVORBIS     = 14;
    public static final int AUDIO_UNUSED15      = 15;
    public static final int AUDIO_UNUSED16      = 16;
    public static final int AUDIO_S3M           = 17;
    public static final int AUDIO_UNUSED18      = 18;
    public static final int AUDIO_UNUSED19      = 19;
    public static final int AUDIO_WAV           = 20;
    public static final int AUDIO_XM            = 21;
    public static final int AUDIO_XMA           = 22;
    public static final int AUDIO_UNUSED23      = 23;
    public static final int AUDIO_AUDIOQUEUE    = 24;

    private UnityConstants(){}

}
