
package science.unlicense.format.unity.adaptor;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.impl.anim.KeyFrameNumberTimeSerie;
import science.unlicense.display.impl.anim.KeyFrameQuaternionTimeSerie;
import science.unlicense.display.impl.anim.KeyFrameTupleTimeSerie;
import science.unlicense.display.impl.anim.NumberKeyFrame;
import science.unlicense.display.impl.anim.NumberTimeSerie;
import science.unlicense.display.impl.anim.TupleKeyFrame;
import science.unlicense.display.impl.anim.TupleTimeSerie;
import science.unlicense.format.unity.model.primitive.AnimationCurve1;
import science.unlicense.format.unity.model.primitive.FloatCurve1;
import science.unlicense.format.unity.model.primitive.Keyframe1;
import science.unlicense.format.unity.model.primitive.QuaternionCurve1;
import science.unlicense.format.unity.model.primitive.QuaternionKeyframe1;
import science.unlicense.format.unity.model.primitive.Vector3Curve1;
import science.unlicense.format.unity.model.primitive.VectorKeyframe1;

/**
 *
 * @author Johann Sorel
 */
public final class AnimationAdaptor {

    private AnimationAdaptor(){}

    public static KeyFrameQuaternionTimeSerie read(QuaternionCurve1 curve){
        final KeyFrameQuaternionTimeSerie serie = new KeyFrameQuaternionTimeSerie();

        final AnimationCurve1 anim = curve.getcurve();
        final Sequence frames = anim.getm_Curve();
        for (int i=0,n=frames.getSize();i<n;i++){
            final QuaternionKeyframe1 kf = (QuaternionKeyframe1) frames.get(i);
            //TODO handle in/out slopes
            final TupleKeyFrame tkf = new TupleKeyFrame(kf.gettime(), kf.getvalue());
            serie.getFrames().add(tkf);
        }

        return serie;
    }

    public static TupleTimeSerie read(Vector3Curve1 curve){
        final KeyFrameTupleTimeSerie serie = new KeyFrameTupleTimeSerie();

        final AnimationCurve1 anim = curve.getcurve();
        final Sequence frames = anim.getm_Curve();
        for (int i=0,n=frames.getSize();i<n;i++){
            final VectorKeyframe1 kf = (VectorKeyframe1) frames.get(i);
            //TODO handle in/out slopes
            final TupleKeyFrame tkf = new TupleKeyFrame(kf.gettime(), kf.getvalue());
            serie.getFrames().add(tkf);
        }

        return serie;
    }

    public static NumberTimeSerie read(FloatCurve1 curve){
        final KeyFrameNumberTimeSerie serie = new KeyFrameNumberTimeSerie();

        final AnimationCurve1 anim = curve.getcurve();
        final Sequence frames = anim.getm_Curve();
        for (int i=0,n=frames.getSize();i<n;i++){
            final Keyframe1 kf = (Keyframe1) frames.get(i);
            //TODO handle in/out slopes
            final NumberKeyFrame tkf = new NumberKeyFrame(kf.gettime(), kf.getvalue());
            serie.getFrames().add(tkf);
        }

        return serie;
    }


}
