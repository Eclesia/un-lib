
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.impl.MatrixNxN;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class Matrix3x4fType implements UnityValueType {

    private static final Chars NAME = Chars.constant("Matrix3x4f");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return MatrixRW.class;
    }

    public Object toValue(TypedNode node) {
        final MatrixRW matrix = MatrixNxN.create(3, 4);
        matrix.set(0,0,(Float) node.getChild(new Chars("e00")).getValue());
        matrix.set(0,1,(Float) node.getChild(new Chars("e01")).getValue());
        matrix.set(0,2,(Float) node.getChild(new Chars("e02")).getValue());
        matrix.set(0,3,(Float) node.getChild(new Chars("e03")).getValue());

        matrix.set(1,0,(Float) node.getChild(new Chars("e10")).getValue());
        matrix.set(1,1,(Float) node.getChild(new Chars("e11")).getValue());
        matrix.set(1,2,(Float) node.getChild(new Chars("e12")).getValue());
        matrix.set(1,3,(Float) node.getChild(new Chars("e13")).getValue());

        matrix.set(2,0,(Float) node.getChild(new Chars("e20")).getValue());
        matrix.set(2,1,(Float) node.getChild(new Chars("e21")).getValue());
        matrix.set(2,2,(Float) node.getChild(new Chars("e22")).getValue());
        matrix.set(2,3,(Float) node.getChild(new Chars("e23")).getValue());
        return matrix;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
