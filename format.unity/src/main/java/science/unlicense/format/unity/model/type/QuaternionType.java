
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class QuaternionType implements UnityValueType {

    private static final Chars NAME = Chars.constant("Quaternionf");
    public static final Chars ATT_X = Chars.constant("x");
    public static final Chars ATT_Y = Chars.constant("y");
    public static final Chars ATT_Z = Chars.constant("z");
    public static final Chars ATT_W = Chars.constant("w");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return Quaternion.class;
    }

    public Quaternion toValue(TypedNode node) {
        final Quaternion vector = new Quaternion();
        vector.set(0,(Float) node.getChild(ATT_X).getValue());
        vector.set(1,(Float) node.getChild(ATT_Y).getValue());
        vector.set(2,(Float) node.getChild(ATT_Z).getValue());
        vector.set(3,(Float) node.getChild(ATT_W).getValue());
        return vector;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
