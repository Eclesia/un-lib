
package science.unlicense.format.unity.model.bundle;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.unity.model.UnityVersion;

/**
 *
 * @author Nico Bergemann <barracuda415 at yahoo.de> (from disunity project)
 * @author Johann Sorel
 */
public class UnityBundleHeader {

    public int fileVersion;
    public UnityVersion playerVersion;
    public UnityVersion engineVersion;
    public long fileSize;
    public long headerSize;
    public int nbLevelDownload;
    public int nbLevel;
    public byte unknown;
    private long dataHeaderSize;

    public long[][] levels;

    public void read(DataInputStream ds) throws IOException{
        fileVersion = ds.readInt();
        playerVersion = new UnityVersion(ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII));
        engineVersion = new UnityVersion(ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII));
        fileSize = ds.readUInt();
        headerSize = ds.readInt();
        nbLevelDownload = ds.readInt();
        nbLevel = ds.readInt();

        levels = new long[nbLevel][2];
        for (int i=0; i<nbLevel; i++){
            levels[i] = ds.readUInt(2);
        }

        if (fileVersion >= 2){
            fileSize = ds.readUInt();
        }
        if (fileVersion >= 3) {
            dataHeaderSize = ds.readUInt();
        }
        ds.skipFully(1);
    }

}
