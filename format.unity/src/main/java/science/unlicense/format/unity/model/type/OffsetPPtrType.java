
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class OffsetPPtrType implements UnityValueType {

    private static final Chars NAME = Chars.constant("OffsetPtr");
    private static final Chars ATT_data = Chars.constant("data");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return Object.class;
    }

    public Object toValue(TypedNode node) {
        final TypedNode child2 = node.getChild(ATT_data);
        return child2.getValue();
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
