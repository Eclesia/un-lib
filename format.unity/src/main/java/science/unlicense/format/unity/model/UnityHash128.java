
package science.unlicense.format.unity.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Int32;

/**
 *
 * @author Nico Bergemann <barracuda415 at yahoo.de>
 */
public class UnityHash128 extends CObject {

    private final byte[] hash = new byte[16];

    public UnityHash128() {
    }

    public byte[] hash() {
        return hash;
    }

    public void read(DataInputStream in) throws IOException {
        in.readFully(hash);
    }

    public void write(DataOutputStream out) throws IOException {
        out.write(hash);
    }

    public Chars toChars() {
        return Int32.encodeHexa(hash);
    }

}
