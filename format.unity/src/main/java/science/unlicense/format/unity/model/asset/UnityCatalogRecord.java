
package science.unlicense.format.unity.model.asset;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Note : this isn't really a catalg record, it's usage in unkown.
 *
 *
 * @author Johann Sorel
 */
public class UnityCatalogRecord {

    public int unknown1;
    //looks like an offset
    public int unknown2;
    //looks like a flag
    public int unknown3;

    public void read(DataInputStream ds) throws IOException{
        unknown1 = ds.readInt();
        unknown2 = ds.readInt();
        unknown3 = ds.readInt();
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeInt(unknown1);
        ds.writeInt(unknown2);
        ds.writeInt(unknown3);
    }

}
