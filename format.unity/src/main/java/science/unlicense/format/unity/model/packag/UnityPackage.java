
package science.unlicense.format.unity.model.packag;

/**
 * Unity package.
 *
 * A *.unitypackage file is a tar archive with special structure where
 * the files and there path are store in separate folders with a metadata file.
 *
 * TODO need to finish tar integration
 *
 * @author Johann Sorel
 */
public class UnityPackage {

}
