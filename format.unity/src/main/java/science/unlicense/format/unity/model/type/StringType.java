
package science.unlicense.format.unity.model.type;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class StringType implements UnityValueType {

    private static final Chars NAME = Chars.constant("string");
    private static final Chars ATT_Array = Chars.constant("Array");
    private static final Chars ATT_size = Chars.constant("size");
    private static final Chars ATT_data = Chars.constant("data");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return Chars.class;
    }

    public Object toValue(TypedNode node) {
        final TypedNode child1 = node.getChild(ATT_Array);
        return new Chars((byte[]) child1.getValue());
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
