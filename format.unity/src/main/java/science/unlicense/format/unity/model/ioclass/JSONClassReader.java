
package science.unlicense.format.unity.model.ioclass;

import science.unlicense.format.json.JSONElement;
import science.unlicense.format.json.JSONReader;
import science.unlicense.format.json.JSONWriter;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.DefaultNodeCardinality;
import science.unlicense.common.api.model.tree.NodeCardinality;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.unity.model.UnityVersion;
import science.unlicense.format.unity.model.asset.UnityClass;
import science.unlicense.format.unity.model.asset.UnityClasses;
import science.unlicense.format.unity.model.asset.UnityNodeType;

/**
 * Store unity classes definition in JSON.
 *
 * @author Johann Sorel
 */
public class JSONClassReader extends AbstractReader {

    //common properties
    private static final Chars PROP_NAME = Chars.constant("name");
    private static final Chars PROP_VERSION = Chars.constant("version");
    private static final Chars PROP_UNVERSION = Chars.constant("unversion");
    private static final Chars PROP_TYPE = Chars.constant("type");

    //class properties
    private static final Chars PROP_ID = Chars.constant("id");

    //type properties
    private static final Chars PROP_SIZE = Chars.constant("size");
    private static final Chars PROP_INDEX = Chars.constant("index");
    private static final Chars PROP_ARRAY = Chars.constant("array");
    private static final Chars PROP_FLAGS = Chars.constant("flags");
    private static final Chars PROP_NODES = Chars.constant("nodes");

    public void fill(UnityClasses classes) throws IOException{
        final JSONReader reader = new JSONReader();
        reader.setInput(getInput());

        if (!reader.hashNext()) throw new IOException("Unexpected json file end");
        if (reader.next().getType() != JSONElement.TYPE_OBJECT_BEGIN)  throw new IOException("Expected object start");
        if (!reader.hashNext()) throw new IOException("Unexpected json file end");
        if (reader.next().getType() != JSONElement.TYPE_NAME)  throw new IOException("Expected property name");
        if (!reader.hashNext()) throw new IOException("Unexpected json file end");
        if (reader.next().getType() != JSONElement.TYPE_ARRAY_BEGIN)  throw new IOException("Expected array begin");
        while (reader.hashNext()){
            final JSONElement ele = reader.next();
            if (ele.getType()== JSONElement.TYPE_OBJECT_BEGIN){
                final UnityClass clazz = new UnityClass();
                readClass(clazz, reader);
                classes.add(clazz);
            } else {
                break;
            }
        }
    }

    private void readClass(UnityClass clazz, JSONReader reader) throws IOException{
        while (reader.hashNext()){
            final JSONElement next = reader.next();
            final int eleType = next.getType();
            if (eleType==JSONElement.TYPE_NAME){
                readClassProperty(clazz,next, reader);
            } else if (eleType==JSONElement.TYPE_OBJECT_END){
                break;
            } else {
                throw new IOException("Unexpected type : "+eleType);
            }
        }
    }

    private void readClassProperty(UnityClass clazz, JSONElement nameEle, JSONReader reader) throws IOException{
        final Chars name = (Chars) nameEle.getValue();

        final JSONElement next = reader.next();
        final int eleType = next.getType();
        if (eleType==JSONElement.TYPE_VALUE){
            final Object value = next.getValue();
            if (PROP_NAME.equals(name)){
                clazz.name = (Chars) value;
            } else if (PROP_ID.equals(name)){
                clazz.id = ((Number) value).intValue();
            } else if (PROP_VERSION.equals(name)){
                clazz.version = new UnityVersion( (Chars) value);
            } else {
                throw new IOException("Unexpected name : "+name);
            }

        } else if (eleType==JSONElement.TYPE_OBJECT_BEGIN){
            if (PROP_TYPE.equals(name)){
                clazz.type = new UnityNodeType();
                readNodeType(clazz.type,reader);
            } else {
                throw new IOException("Unexpected name for array type : "+name);
            }
        } else {
            throw new IOException("Unexpected type : "+eleType);
        }
    }


    private void readNodeType(UnityNodeType type, JSONReader reader) throws IOException{
        while (reader.hashNext()){
            final JSONElement next = reader.next();
            final int eleType = next.getType();
            if (eleType==JSONElement.TYPE_NAME){
                readTypeProperty(type,next, reader);
            } else if (eleType==JSONElement.TYPE_OBJECT_END){
                break;
            } else {
                throw new IOException("Unexpected type : "+eleType);
            }
        }
    }

    private void readTypeProperty(UnityNodeType type, JSONElement nameEle, JSONReader reader) throws IOException{
        final Chars name = (Chars) nameEle.getValue();

        final JSONElement next = reader.next();
        final int eleType = next.getType();
        if (eleType==JSONElement.TYPE_VALUE){
            final Object value = next.getValue();
            if (PROP_NAME.equals(name)){
                type.setId((Chars) value);
                type.setTitle((Chars) value);
            } else if (PROP_TYPE.equals(name)){
                type.type = (Chars) value;
            } else if (PROP_SIZE.equals(name)){
                type.size = ((Number) value).intValue();
            } else if (PROP_INDEX.equals(name)){
                type.index = ((Number) value).intValue();
            } else if (PROP_ARRAY.equals(name)){
                type.isArray = ((Boolean) value);
            } else if (PROP_VERSION.equals(name)){
                type.version = ((Number) value).intValue();
            } else if (PROP_UNVERSION.equals(name)){
                type.unversion = ((Number) value).intValue();
            } else if (PROP_FLAGS.equals(name)){
                type.metaFlag = ((Number) value).intValue();
            } else {
                throw new IOException("Unexpected name : "+name);
            }

        } else if (eleType==JSONElement.TYPE_ARRAY_BEGIN){
            if (PROP_NODES.equals(name)){
                final Sequence children = new ArraySequence();
                while (reader.hashNext()){
                    final JSONElement next1 = reader.next();
                    final int type1 = next1.getType();
                    if (type1==JSONElement.TYPE_OBJECT_BEGIN){
                        final UnityNodeType child = new UnityNodeType();
                        readNodeType(child,reader);
                        children.add(new DefaultNodeCardinality(child, 1, 1));
                    } else if (type1==JSONElement.TYPE_ARRAY_END){
                        break;
                    }
                }
                final NodeCardinality[] cards = new NodeCardinality[children.getSize()];
                Collections.copy(children, cards, 0);
                type.setChildrenTypes(cards);
            } else {
                throw new IOException("Unexpected name for array type : "+name);
            }
        } else {
            throw new IOException("Unexpected type : "+eleType);
        }
    }

    private void writeNodeType(UnityNodeType type, JSONWriter writer) throws IOException{
        writer.writeObjectBegin();

        writer.writeName(PROP_NAME);
        writer.setFormatted(false);
        writer.writeValue(type.getId());
        writer.writeProperty(PROP_TYPE, type.type);
        writer.writeProperty(PROP_SIZE, type.size);
        writer.writeProperty(PROP_INDEX, type.index);
        writer.writeProperty(PROP_ARRAY, type.isArray);
        writer.writeProperty(PROP_VERSION, type.version);
        writer.writeProperty(PROP_FLAGS, type.metaFlag);
        writer.setFormatted(true);

        //children
        final NodeCardinality[] children = type.getChildrenTypes();
        if (children.length!=0){
            writer.writeName(PROP_NODES);
            writer.writeArrayBegin();
            for (int i=0;i<children.length;i++){
                final UnityNodeType subType = (UnityNodeType) children[i].getType();
                writeNodeType(subType,writer);
            }
            writer.writeArrayEnd();
        }

        writer.writeObjectEnd();
    }


}
