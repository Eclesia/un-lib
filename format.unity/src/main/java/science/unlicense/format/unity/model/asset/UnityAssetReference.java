
package science.unlicense.format.unity.model.asset;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Nico Bergemann <barracuda415 at yahoo.de> (from disunity project)
 * @author Johann Sorel
 */
public class UnityAssetReference {

    public Chars assetPath;
    public long[] guid;
    public int type;
    public Chars filePath;

    public void read(UnityAsset asset, DataInputStream ds) throws IOException{
        if (asset.header.version>5){
            assetPath = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
        }
        guid = new long[]{ds.readLong(Endianness.BIG_ENDIAN),ds.readLong(Endianness.BIG_ENDIAN)};
        type = ds.readInt();
        filePath = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
    }

    public void write(UnityAsset asset, DataOutputStream ds) throws IOException{
        if (asset.header.version>5){
            ds.writeZeroTerminatedChars(assetPath, 0, CharEncodings.US_ASCII);
        }
        ds.writeLong(guid, Endianness.BIG_ENDIAN);
        ds.writeInt(type);
        ds.writeZeroTerminatedChars(filePath, 0, CharEncodings.US_ASCII);
    }

    public String toString() {
        return "AssetReference("+assetPath+","+guid[0]+","+guid[1]+","+type+","+filePath+")";
    }

}
