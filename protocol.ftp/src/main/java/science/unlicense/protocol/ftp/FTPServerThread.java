
package science.unlicense.protocol.ftp;

import java.net.InetAddress;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import static science.unlicense.protocol.ftp.FTPConstants.*;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.ServerSocket;

/**
 *
 * @author Johann Sorel
 */
class FTPServerThread extends Thread {

    private static final byte[] UNSUPPORTED = "500 Unsupported\r\n".getBytes();
    private static final byte[] OK = "200 Ok\r\n".getBytes();

    private final ClientSocket socketControl;
    private DataHandler dataHandler;
    private ClientSocket socketData;
    private ByteInputStream in;
    private ByteOutputStream out;

    private final Path root;

    //current
    private Chars command;
    private Chars argument;

    //states
    private Chars user;
    private Chars password;
    private Path path;
    private Chars type;
    private Chars stype;

    public FTPServerThread(ClientSocket socketControl) {
        this.socketControl = socketControl;
        root = Paths.resolve(new Chars("file:/home/husky/temp/ftp/"));
        path = root;
    }

    public void run() {
        try {
            in = socketControl.getInputStream();
            out = socketControl.getOutputStream();
            //connection ok message
            out.write("200 Ok\r\n".getBytes());
            while (true) {
                //read command
                final CharBuffer cb = new CharBuffer();
                for (int b = in.read(); !(b == -1 || b == '\n'); b = in.read()) {
                    cb.append(b);
                }
                final Chars message = cb.toChars().trim();
                System.out.println(message);
                final int split = message.getFirstOccurence(' ');
                command = message.truncate(0, split);
                argument = message.truncate(split+1, -1);

                if (FTPConstants.CMD_USER.equals(command)) {
                    treatUSER();
                } else if (FTPConstants.CMD_PASS.equals(command)) {
                    treatPASS();
                } else if (FTPConstants.CMD_ACCT.equals(command)) {
                    treatACCT();
                } else if (FTPConstants.CMD_CWD.equals(command)) {
                    treatCWD();
                } else if (FTPConstants.CMD_CDUP.equals(command)) {
                    treatCDUP();
                } else if (FTPConstants.CMD_SMNT.equals(command)) {
                     treatSMNT();
                } else if (FTPConstants.CMD_REIN.equals(command)) {
                     treatREIN();
                } else if (FTPConstants.CMD_QUIT.equals(command)) {
                     treatQUIT();
                } else if (FTPConstants.CMD_PORT.equals(command)) {
                     treatPORT();
                } else if (FTPConstants.CMD_PASV.equals(command)) {
                     treatPASV();
                } else if (FTPConstants.CMD_TYPE.equals(command)) {
                     treatTYPE();
                } else if (FTPConstants.CMD_STRU.equals(command)) {
                     treatSTRU();
                } else if (FTPConstants.CMD_MODE.equals(command)) {
                     treatMODE();
                } else if (FTPConstants.CMD_RETR.equals(command)) {
                     treatRETR();
                } else if (FTPConstants.CMD_STOR.equals(command)) {
                     treatSTOR();
                } else if (FTPConstants.CMD_STOU.equals(command)) {
                     treatSTOU();
                } else if (FTPConstants.CMD_APPE.equals(command)) {
                     treatAPPE();
                } else if (FTPConstants.CMD_ALLO.equals(command)) {
                     treatALLO();
                } else if (FTPConstants.CMD_REST.equals(command)) {
                     treatREST();
                } else if (FTPConstants.CMD_RNFR.equals(command)) {
                     treatRNFR();
                } else if (FTPConstants.CMD_RNTO.equals(command)) {
                     treatRNTO();
                } else if (FTPConstants.CMD_ABOR.equals(command)) {
                     treatABOR();
                } else if (FTPConstants.CMD_DELE.equals(command)) {
                     treatDELE();
                } else if (FTPConstants.CMD_RMD.equals(command)) {
                     treatRMD();
                } else if (FTPConstants.CMD_MKD.equals(command)) {
                     treatMKD();
                } else if (FTPConstants.CMD_PWD.equals(command)) {
                     treatPWD();
                } else if (FTPConstants.CMD_LIST.equals(command)) {
                     treatLIST();
                } else if (FTPConstants.CMD_NLST.equals(command)) {
                     treatNLST();
                } else if (FTPConstants.CMD_SITE.equals(command)) {
                     treatSITE();
                } else if (FTPConstants.CMD_SYST.equals(command)) {
                     treatSYST();
                } else if (FTPConstants.CMD_STAT.equals(command)) {
                     treatSTAT();
                } else if (FTPConstants.CMD_HELP.equals(command)) {
                     treatHELP();
                } else if (FTPConstants.CMD_NOOP.equals(command)) {
                     treatNOOP();
                } else {
                     out.write(UNSUPPORTED);
                }
            }
        } catch (IOException ex) {
            //connection close or failed
            return;
        }
    }

    public void treatUSER() throws IOException {
        user = argument;
        out.write("330 User received\r\n".getBytes());
        out.flush();
    }

    public void treatPASS() throws IOException {
        password = argument;
        out.write("230 Password received, authentified\r\n".getBytes());
        out.flush();
    }

    public void treatACCT() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatCWD() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatCDUP() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatSMNT() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatREIN() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatQUIT() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatPORT() throws IOException {
        out.write(UNSUPPORTED);
    }

    /**
     * Enter passive mode
     */
    public void treatPASV() throws IOException {
        final ServerSocket sSocket = science.unlicense.system.System.get().getSocketManager().createServerSocket(0, new DataHandler());
        final int port = sSocket.getPort();

        final InetAddress adr;
        try {
            adr = InetAddress.getLocalHost();
        } catch(Exception ex) {
            throw new IOException(ex.getMessage(), ex);
        }
        String adrStr = adr.getHostAddress().toString().replace('.', ',');
        adrStr += "," + port/256;
        adrStr += "," + port%256;


        out.write(("227 "+adrStr).getBytes());
        out.write(CRLFB);
        out.flush();
    }

    public void treatTYPE() throws IOException {
        type = argument.truncate(0, 1);
        if (argument.getCharLength()>1) {
            stype = argument.truncate(1, 2);
        }
        out.write(OK);
        out.flush();
    }

    public void treatSTRU() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatMODE() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatRETR() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatSTOR() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatSTOU() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatAPPE() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatALLO() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatREST() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatRNFR() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatRNTO() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatABOR() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatDELE() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatRMD() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatMKD() throws IOException {
        out.write(UNSUPPORTED);
    }

    /**
     * Print the current working directory name.
     */
    public void treatPWD() throws IOException {
        Chars p = Paths.relativePath(path, root);
        if (p.equals(new Chars("."))) p = new Chars("/");
        out.write(("257 "+p).getBytes());
        out.write(CRLFB);
        out.flush();
    }

    public void treatLIST() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatNLST() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatSITE() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatSYST() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatSTAT() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatHELP() throws IOException {
        out.write(UNSUPPORTED);
    }

    public void treatNOOP() throws IOException {
        out.write(UNSUPPORTED);
    }

    private class DataHandler implements ServerSocket.ClientHandler {

        @Override
        public void create(ClientSocket clientsocket) {
            FTPServerThread.this.socketData = clientsocket;
        }

    }

}
