
package science.unlicense.protocol.ftp;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class FTPConstants {

    public static final Chars CRLF = Chars.constant("\r\n");
    static final byte[] CRLFB = CRLF.toBytes();



    public static final Chars CMD_USER = Chars.constant("USER");
    public static final Chars CMD_PASS = Chars.constant("PASS");
    public static final Chars CMD_ACCT = Chars.constant("ACCT");
    public static final Chars CMD_CWD = Chars.constant("CWD");
    public static final Chars CMD_CDUP = Chars.constant("CDUP");
    public static final Chars CMD_SMNT = Chars.constant("SMNT");
    public static final Chars CMD_REIN = Chars.constant("REIN");
    public static final Chars CMD_QUIT = Chars.constant("QUIT");

    public static final Chars CMD_PORT = Chars.constant("PORT");
    public static final Chars CMD_PASV = Chars.constant("PASV");
    public static final Chars CMD_TYPE = Chars.constant("TYPE");
    public static final Chars CMD_STRU = Chars.constant("STRU");
    public static final Chars CMD_MODE = Chars.constant("MODE");

    public static final Chars CMD_RETR = Chars.constant("RETR");
    public static final Chars CMD_STOR = Chars.constant("STOR");
    public static final Chars CMD_STOU = Chars.constant("STOU");
    public static final Chars CMD_APPE = Chars.constant("APPE");
    public static final Chars CMD_ALLO = Chars.constant("ALLO");
    public static final Chars CMD_REST = Chars.constant("REST");
    public static final Chars CMD_RNFR = Chars.constant("RNFR");
    public static final Chars CMD_RNTO = Chars.constant("RNTO");
    public static final Chars CMD_ABOR = Chars.constant("ABOR");
    public static final Chars CMD_DELE = Chars.constant("DELE");
    public static final Chars CMD_RMD = Chars.constant("RMD");
    public static final Chars CMD_MKD = Chars.constant("MKD");
    public static final Chars CMD_PWD = Chars.constant("PWD");
    public static final Chars CMD_LIST = Chars.constant("LIST");
    public static final Chars CMD_NLST = Chars.constant("NLST");
    public static final Chars CMD_SITE = Chars.constant("SITE");
    public static final Chars CMD_SYST = Chars.constant("SYST");
    public static final Chars CMD_STAT = Chars.constant("STAT");
    public static final Chars CMD_HELP = Chars.constant("HELP");
    public static final Chars CMD_NOOP = Chars.constant("NOOP");

    public static final Chars TYPE_ASCII = Chars.constant("A");
    public static final Chars TYPE_EBCDIC = Chars.constant("E");
    public static final Chars TYPE_IMAGE = Chars.constant("I");
    public static final Chars TYPE_LOCAL = Chars.constant("L");

    public static final Chars STYPE_NON_PRINT = Chars.constant("N");
    public static final Chars STYPE_TELNET = Chars.constant("T");
    public static final Chars STYPE_ASA = Chars.constant("C");

    public static final int POSITIVE_PRELIMINARY_REPLY = '1';
    public static final int POSITIVE_COMPLETION_REPLY = '2';
    public static final int POSITIVE_INTERMEDIATE_REPLY = '3';
    public static final int TRANSIENT_NEGATIVE_COMPLETION_REPLY = '4';
    public static final int PPERMANENT_NEGATIVE_COMPLETION_REPLY = '5';

    public static final int SYNTAX = '0';
    public static final int INFORMATION = '1';
    public static final int CONNECTIONS = '2';
    public static final int AUTHENTICATION = '3';
    public static final int UNSPECIFIED = '4';
    public static final int FILE_SYSTEM = '5';

    private FTPConstants(){}

}
