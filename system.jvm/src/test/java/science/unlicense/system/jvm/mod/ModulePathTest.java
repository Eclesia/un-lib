
package science.unlicense.system.jvm.mod;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.system.ModuleSeeker;
import science.unlicense.system.jvm.JVMTest;
import science.unlicense.system.jvm.ModuleFormat;

/**
 *
 * @author Johann Sorel
 */
public class ModulePathTest extends JVMTest {

    /**
     * Check we can open a simple module path.
     */
    @Test
    public void testModuleSimplePath() throws IOException{
        final PathResolver resolver = ((ModuleFormat) ModuleSeeker.findServices(ModuleFormat.class)[0]).createResolver(null);
        final Chars str = new Chars("mod:/simple/path/test.txt");
        final Path path = resolver.resolve(str);
        final ByteInputStream stream = path.createInputStream();
        stream.dispose();

    }

    /**
     * Check we can open a complex module path.
     */
    @Test
    public void testModuleComplexPath() throws IOException{
        final PathResolver resolver = ((ModuleFormat) ModuleSeeker.findServices(ModuleFormat.class)[0]).createResolver(null);
        final Chars str = new Chars("mod:/simple/path/[@re  _ x3$   .t [xt");
        final Path path = resolver.resolve(str);
        final ByteInputStream stream = path.createInputStream();
        stream.dispose();

    }

    /**
     * Check we can navigate parent/child.
     */
    @Test
    public void testModuleParentChildSolving() throws IOException{
        final PathResolver resolver = ((ModuleFormat) ModuleSeeker.findServices(ModuleFormat.class)[0]).createResolver(null);
        final Chars str = new Chars("mod:/simple/path/[@re  _ x3$   .t [xt");
        final Path path = resolver.resolve(str);

        final Path parent = path.getParent();
        Assert.assertNotNull(parent);
        Assert.assertEquals(new Chars("mod:/simple/path/"),parent.toURI());

        final Path sibling = parent.resolve(new Chars("test.txt"));
        Assert.assertNotNull(sibling);
        Assert.assertEquals(new Chars("mod:/simple/path/test.txt"),sibling.toURI());


    }

}
