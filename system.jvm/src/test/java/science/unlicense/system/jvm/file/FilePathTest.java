
package science.unlicense.system.jvm.file;

import java.io.File;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.system.jvm.JVMTest;

/**
 *
 * @author Johann Sorel
 */
public class FilePathTest extends JVMTest {

    @Test
    public void testRelativePath() {

        final Path base = new FilePath(new FileResolver(), new File("test"));
        Assert.assertTrue(base.toURI().endsWith(new Chars("test")));

        final Path child1 = base.resolve(new Chars("child"));
        Assert.assertTrue(child1.toURI().endsWith(new Chars("test/child")));

        final Path child2 = base.resolve(new Chars(".child"));
        Assert.assertTrue(child2.toURI().endsWith(new Chars("test/.child")));

    }

}
