
package science.unlicense.system.jvm;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.system.ModuleSeeker;
import science.unlicense.system.jvm.file.FileFormat;

/**
 *
 * @author Johann Sorel
 */
public class PathsTest extends JVMTest {

    @Test
    public void testRelativePath() throws IOException {
        final PathResolver resolver = ((FileFormat) ModuleSeeker.findServices(FileFormat.class)[0]).createResolver(null);

        final Path p1 = resolver.resolve(new Chars("file:/home/user/"));
        final Path p2 = resolver.resolve(new Chars("file:/home/user/folder1/folder2/test.xml"));

    }

}
