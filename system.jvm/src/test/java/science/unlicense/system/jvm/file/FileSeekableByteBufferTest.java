
package science.unlicense.system.jvm.file;

import java.io.File;
import java.io.IOException;
import org.junit.Test;
import static science.unlicense.common.api.Assert.*;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.io.SeekableByteBuffer;
import science.unlicense.system.jvm.JVMTest;

/**
 *
 * @author Johann Sorel
 */
public class FileSeekableByteBufferTest extends JVMTest {

    @Test
    public void testWriteBytes() throws IOException, science.unlicense.encoding.api.io.IOException {

        File file = File.createTempFile("tbin", "bin");
        FilePath p = new FilePath(new FileResolver(), file);
        ByteOutputStream out = p.createOutputStream();
        out.write(new byte[]{0,1,2,3,4,5,6,7,8,9,});
        out.flush();
        out.close();

        //replace bytes
        SeekableByteBuffer sk = p.createSeekableBuffer(true, true, false);
        sk.setPosition(3);
        sk.write(new byte[]{13,14,15});
        sk.flush();
        assertEquals(6, sk.getPosition());
        assertEquals(10, sk.getSize());
        sk.dispose();

        ByteInputStream is = p.createInputStream();
        byte[] all = IOUtilities.readAll(is);
        is.dispose();
        assertArrayEquals(new byte[]{0,1,2,13,14,15,6,7,8,9}, all);

        //write at file end, must fail
        sk = p.createSeekableBuffer(true, true, false);
        sk.setPosition(10);
        try {
            sk.write(new byte[]{13,14,15});
            fail("Expanding file should fail");
        }catch (science.unlicense.encoding.api.io.IOException ex) {
            //ok
        }

    }

}
