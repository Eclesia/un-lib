
package science.unlicense.runtime;

import science.unlicense.system.System;
import science.unlicense.system.jvm.JVMSystem;

/**
 *
 * @author Johann Sorel
 */
public final class Runtime {

    private static final JVMSystem SYSTEM = new JVMSystem();

    public static void init() {
        System.set(SYSTEM);
    }

    private Runtime(){}

}
