

package science.unlicense.system.jvm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public final class ModuleFormat implements PathFormat{

    private final ModuleResolver resolver;

    public ModuleFormat() {
        this.resolver = new ModuleResolver(this);
    }

    @Override
    public boolean isAbsolute() {
        return  true;
    }

    @Override
    public boolean canCreate(Path base) throws IOException {
        return false;
    }

    @Override
    public PathResolver createResolver(Path base) throws IOException {
        return resolver;
    }

    @Override
    public Chars getPrefix() {
        return new Chars("mod");
    }
}
