
package science.unlicense.system.jvm;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.LongConsumer;
import java.util.stream.LongStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.system.GlobalProperties;
import science.unlicense.system.ModuleManager;
import science.unlicense.system.SocketManager;
import science.unlicense.system.System;
import science.unlicense.system.Work;
import science.unlicense.system.WorkUnits;
import science.unlicense.system.api.desktop.DefaultTransferBag;
import science.unlicense.system.api.desktop.TransferBag;

/**
 * JVM environment.
 *
 * @author Johann Sorel
 */
public final class JVMSystem extends System {

    private static final ModuleManager MODULE_MANAGER = new JVMModuleManager();
    private static final SocketManager SOCKET_MANAGER = new JVMSocketManager();
    private static final GlobalProperties PROPERTIES = new JVMGlobalProperties();
    private static final TransferBag CLIPBOARD = new JVMClipBoard();
    private static final TransferBag DRAGANDDROP = new DefaultTransferBag();

    @Override
    public ModuleManager getModuleManager() {
        return MODULE_MANAGER;
    }

    @Override
    public SocketManager getSocketManager() {
        return SOCKET_MANAGER;
    }

    @Override
    public GlobalProperties getProperties(){
        return PROPERTIES;
    }

    @Override
    public String getType() {
        if (File.pathSeparator.equals("\\")){
            return TYPE_WINDOWS;
        } else {
            return TYPE_UNIX;
        }
    }

    @Override
    public TransferBag getClipBoard() {
        return CLIPBOARD;
    }

    @Override
    public TransferBag getDragAndDrapBag() {
        return DRAGANDDROP;
    }

    @Override
    public WorkUnits createWorkUnits(float priority) {
        return new JVMWorkUnits();
    }

    public static InputStream toInputStream(final ByteInputStream in) {
        if (in instanceof JVMInputStream) {
            return ((JVMInputStream) in).in;
        }

        return new InputStream() {
            @Override
            public int read() throws IOException {
                try {
                    return in.read();
                } catch (science.unlicense.encoding.api.io.IOException ex) {
                    throw new IOException(ex.getMessage(),ex);
                }
            }

            @Override
            public void close() throws IOException {
                try {
                    in.dispose();
                } catch (science.unlicense.encoding.api.io.IOException ex) {
                    throw new IOException(ex.getMessage(),ex);
                }
            }
        };
    }

    private static class JVMWorkUnits implements WorkUnits {

        @Override
        public void submit(long range, Work work) {
            LongStream.range(0, range).parallel().forEach(new LongConsumer() {
                @Override
                public void accept(long value) {
                    work.run(value);
                }
            });
        }

        @Override
        public void dispose() {
        }

    }

}
