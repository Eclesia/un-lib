
package science.unlicense.system.jvm.http;

import java.net.URL;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.AbstractPath;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.system.jvm.JVMInputStream;
import science.unlicense.system.jvm.JVMOutputStream;

/**
 * Map a JVM URL to Path.
 *
 * @author Johann Sorel
 */
final class HttpPath extends AbstractPath {

    private static final Chars PREFIX = Chars.constant("http");
    private final PathResolver resolver;
    private final Chars url;

    public HttpPath(final PathResolver resolver, Chars url) {
        this.resolver = resolver;
        this.url = url;
    }

    @Override
    public Chars getName() {
        final Chars[] parts = url.split('/');
        return parts[parts.length-1];
    }

    @Override
    public Path getParent() {
        final int index = url.getLastOccurence('/');
        final Chars str = url.truncate(0, index);
        return new HttpPath(resolver,str);
    }

    @Override
    public boolean isContainer() throws IOException {
        return false;
    }

    public boolean exists() throws IOException {
        try {
            final ByteInputStream in = createInputStream();
            in.dispose();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    @Override
    public boolean createContainer() throws IOException {
        throw new IOException("Can not create url path");
    }

    public boolean createLeaf() throws IOException {
        throw new IOException("Can not create path");
    }

    @Override
    public Path resolve(Chars address) {
        if (address.startsWith(PREFIX)){
            //absolute path
            return getResolver().resolve(address);
        } else {
            if (address.startsWith('/')){
                if (url.endsWith('/')){
                    return getResolver().resolve(url.concat(address.truncate(1,-1)));
                } else {
                    return getResolver().resolve(url.concat(address));
                }
            } else {
                if (url.endsWith('/')){
                    return getResolver().resolve(url.concat(address));
                } else {
                    return getResolver().resolve(url.concat('/').concat(address));
                }
            }
        }
    }

    @Override
    public PathResolver getResolver() {
        return resolver;
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        try {
            return new JVMInputStream(new URL(url.toString()).openStream());
        } catch (java.io.IOException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        try {
            return new JVMOutputStream(new URL(url.toString()).openConnection().getOutputStream());
        } catch (java.io.IOException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public Collection getChildren() {
        return Collections.emptyCollection();
    }

    @Override
    public Chars toURI() {
        return url;
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[0];
    }

    @Override
    public Chars toChars() {
        return new Chars("URL path : "+url);
    }

}
