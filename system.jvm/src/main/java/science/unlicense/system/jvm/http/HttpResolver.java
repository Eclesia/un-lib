
package science.unlicense.system.jvm.http;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;

/**
 * Http path resolver
 *
 * @author Johann Sorel
 */
public final class HttpResolver implements PathResolver{

    private static final Chars PREFIX = Chars.constant("http:");
    private final PathFormat format;

    public HttpResolver(PathFormat format) {
        this.format = format;
    }

    @Override
    public PathFormat getPathFormat() {
        return format;
    }

    @Override
    public Path resolve(Chars path) {
        if (!path.startsWith(PREFIX)){
            return null;
        }

        return new HttpPath(this, path);
    }

}
