

package science.unlicense.system.jvm.file;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public final class FileFormat implements PathFormat{

    private final FileResolver resolver;

    public FileFormat() {
        this.resolver = new FileResolver(this);
    }

    @Override
    public boolean isAbsolute() {
        return  true;
    }

    @Override
    public boolean canCreate(Path base) throws IOException {
        return false;
    }

    @Override
    public PathResolver createResolver(Path base) throws IOException {
        return resolver;
    }

    @Override
    public Chars getPrefix() {
        return new Chars("file");
    }

}
