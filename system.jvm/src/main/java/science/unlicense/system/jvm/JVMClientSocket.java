
package science.unlicense.system.jvm;

import java.io.IOException;
import java.net.Socket;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.IPAddress;

/**
 * Map JVM Socket to UN Socket.
 *
 * @author Johann Sorel
 */
public final class JVMClientSocket implements ClientSocket{

    private final Socket socket;
    private final IPAddress address;

    JVMClientSocket(Socket socket, IPAddress address) {
        this.socket = socket;
        this.address = address;
    }

    public IPAddress getAddress() {
        return address;
    }

    public int getPort() {
        return socket.getPort();
    }

    public ByteInputStream getInputStream() throws science.unlicense.encoding.api.io.IOException{
        try {
            return new JVMInputStream(socket.getInputStream());
        } catch (IOException ex) {
            throw new science.unlicense.encoding.api.io.IOException(ex);
        }
    }

    public ByteOutputStream getOutputStream() throws science.unlicense.encoding.api.io.IOException{
        try {
            return new JVMOutputStream(socket.getOutputStream());
        } catch (IOException ex) {
            throw new science.unlicense.encoding.api.io.IOException(ex);
        }
    }

    public void close() throws science.unlicense.encoding.api.io.IOException{
        try {
            socket.close();
        } catch (IOException ex) {
            throw new science.unlicense.encoding.api.io.IOException(ex);
        }
    }

}
