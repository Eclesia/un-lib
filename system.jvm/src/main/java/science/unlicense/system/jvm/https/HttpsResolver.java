
package science.unlicense.system.jvm.https;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;

/**
 * Http path resolver
 *
 * @author Johann Sorel
 */
public final class HttpsResolver implements PathResolver{

    private static final Chars PREFIX = Chars.constant("https:");
    private final PathFormat format;

    public HttpsResolver(PathFormat format) {
        this.format = format;
    }

    @Override
    public PathFormat getPathFormat() {
        return format;
    }

    @Override
    public Path resolve(Chars path) {
        if (!path.startsWith(PREFIX)){
            return null;
        }

        return new HttpsPath(this, path);
    }

}
