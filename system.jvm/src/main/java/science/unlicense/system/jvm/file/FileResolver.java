
package science.unlicense.system.jvm.file;

import java.io.File;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;

/**
 * File path resolver
 *
 * @author Johann Sorel
 */
public final class FileResolver implements PathResolver{

    private static final Chars PREFIX = Chars.constant("file:");
    private final PathFormat format;

    public FileResolver() {
        this.format = new FileFormat();
    }

    public FileResolver(PathFormat format) {
        this.format = format;
    }

    @Override
    public PathFormat getPathFormat() {
        return format;
    }

    @Override
    public Path resolve(Chars path) {
        if (!path.startsWith(PREFIX)){
            return null;
        }

        path = path.truncate(5,-1);
        File f = new File(path.toString());
        return new FilePath(this, f);
    }

}
