
package science.unlicense.system.jvm;

import java.net.URL;
import java.util.Enumeration;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.model.tree.DefaultNamedNode;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.system.AbstractModuleManager;
import science.unlicense.system.DefaultMetaTree;
import science.unlicense.system.MetaTree;
import science.unlicense.system.MetadataTreeIO;

/**
 * Module manager for JVM.
 *
 * @author Johann Sorel
 */
public class JVMModuleManager extends AbstractModuleManager{

    private final Dictionary trees = new HashDictionary();
    private final Dictionary loading = new HashDictionary();

    public JVMModuleManager() {
    }

    public synchronized MetaTree getMetadataRoot(String name) {

        MetaTree meta = (MetaTree) trees.getValue(name);
        if (meta!= null) return meta;

        //check for recursive creation
        if (loading.getValue(name) != null) throw new RuntimeException("Recursive meta tree creation, resolve recursion.");

        loading.add(name, Boolean.TRUE);

        //load market.
        final NamedNode root = new DefaultNamedNode(Chars.EMPTY,true);
        meta = new DefaultMetaTree(root);

        try {
            final Enumeration<URL> urls = JVMModuleManager.class.getClassLoader().getResources("module-inf/"+name+".tree");
            while (urls.hasMoreElements()) {
                final URL u = urls.nextElement();
                final NamedNode node = MetadataTreeIO.read(new JVMInputStream(u.openStream()));
                MetadataTreeIO.merge(root, node);
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }

        trees.add(name, meta);
        loading.remove(name);
        return meta;
    }

    public Set getModules() {
        //TODO
        return new HashSet();
    }

}
