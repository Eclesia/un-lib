
package science.unlicense.system.jvm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;

/**
 * Module path resolver
 *
 * @author Johann Sorel
 */
public final class ModuleResolver implements PathResolver{

    private static final Chars PREFIX = Chars.constant("mod:");
    private final PathFormat format;

    public ModuleResolver(PathFormat format) {
        this.format = format;
    }

    @Override
    public PathFormat getPathFormat() {
        return format;
    }

    @Override
    public Path resolve(Chars path) {
        if (!path.startsWith(PREFIX)){
            return null;
        }
        return new ModulePath(this, path.truncate(4,-1));
    }

}
