
package science.unlicense.system.jvm;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.system.ServerSocket;

/**
 * Map JVM Socket to UN Socket.
 *
 * @author Johann Sorel
 */
public class JVMServerSocket implements ServerSocket{

    private final java.net.ServerSocket socket;
    private final ServerSocket.ClientHandler handler;
    private volatile boolean open = true;

    public JVMServerSocket(final java.net.ServerSocket socket, ServerSocket.ClientHandler hanlder) throws SocketException {
        this.socket = socket;
        this.handler = hanlder;
        socket.setSoTimeout(10000);

        new Thread(){

            public void run() {
                while (open){
                    try {
                        handler.create(new JVMClientSocket(socket.accept(), null));
                    } catch (SocketTimeoutException ex) {
                        //normal
                    } catch (IOException ex) {
                        Loggers.get().warning(ex);
                    }
                }
            }

        }.start();

    }

    public int getPort() {
        return socket.getLocalPort();
    }

    public void close() throws science.unlicense.encoding.api.io.IOException{
        open = false;
        try {
            socket.close();
        } catch (java.io.IOException ex) {
            throw new science.unlicense.encoding.api.io.IOException(ex);
        }
    }

}
