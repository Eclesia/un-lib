
package science.unlicense.system.jvm;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.AbstractSequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.system.api.desktop.AbstractTransferBag;
import science.unlicense.system.api.desktop.Attachment;
import science.unlicense.system.api.desktop.DefaultAttachment;

/**
 *
 * @author Johann Sorel
 */
public class JVMClipBoard extends AbstractTransferBag {

    private static final ClipboardOwner CO = new ClipboardOwner() {
        @Override
        public void lostOwnership(Clipboard clipboard, Transferable contents) {
        }
    };

    private final Sequence attachments = new AbstractSequence() {
        @Override
        public int getSize() {
            final Clipboard cp = Toolkit.getDefaultToolkit().getSystemClipboard();
            return cp.getAvailableDataFlavors().length;
        }


        @Override
        public Object get(int index) throws InvalidIndexException {
            final Clipboard cp = Toolkit.getDefaultToolkit().getSystemClipboard();
            final DataFlavor df = cp.getAvailableDataFlavors()[index];
            try {
                return toAttachement(df, cp.getData(df));
            } catch (UnsupportedFlavorException | IOException ex) {
                ex.printStackTrace();
                return null;
            }
        }

        @Override
        public boolean add(int index, Object value) throws InvalidIndexException {
            final Clipboard cp = Toolkit.getDefaultToolkit().getSystemClipboard();
            final Attachment att = (Attachment) value;
            final Chars mime = (Chars) att.getMetadatas().getValue(Attachment.META_MIME_TYPE);
            final DataFlavor df = new DataFlavor(att.getObject().getClass(), mime.toJVMString());

            final Transferable trs = new Transferable() {
                @Override
                public DataFlavor[] getTransferDataFlavors() {
                    return new DataFlavor[]{df};
                }

                @Override
                public boolean isDataFlavorSupported(DataFlavor flavor) {
                    return df.equals(flavor);
                }

                @Override
                public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
                    return att.getObject();
                }
            };

            cp.setContents(trs, CO);
            return true;
        }

        @Override
        public boolean remove(int index) throws InvalidIndexException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean removeAll() {
            final Clipboard cp = Toolkit.getDefaultToolkit().getSystemClipboard();
            cp.setContents(new StringSelection(""), null);
            return true;
        }

    };

    @Override
    public Sequence getAttachments() {
        return attachments;
    }

    private Attachment toAttachement(DataFlavor flavor, Object value) {
        return new DefaultAttachment(value, CObjects.toChars(flavor.getMimeType()));
    }


}
