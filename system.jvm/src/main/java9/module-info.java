module science.unlicense.system.jvm {
    requires java.desktop;
    requires science.unlicense.common;
    requires science.unlicense.system;
    requires science.unlicense.encoding;
    requires science.unlicense.concurrent;
    exports science.unlicense.runtime;
    exports science.unlicense.system.jvm;
    exports science.unlicense.system.jvm.file;
    exports science.unlicense.system.jvm.http;
    exports science.unlicense.system.jvm.https;
    exports science.unlicense.system.jvm.workaround;
}
