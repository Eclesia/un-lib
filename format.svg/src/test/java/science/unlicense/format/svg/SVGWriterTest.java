

package science.unlicense.format.svg;

import science.unlicense.format.svg.SVGWriter;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.svg.model.SVGDocument;
import static science.unlicense.format.svg.SVGTestConstants.*;

/**
 *
 * @author Johann Sorel
 */
public class SVGWriterTest {

    //@Test
    public void testWriteDocument() throws IOException{
        final SVGDocument doc = new SVGDocument();
        doc.setWidth(800.0);
        doc.setHeight(600.0);
        write(doc, TEST_DOCUMENT);
    }

    public void write(SVGDocument doc, Chars expected) throws IOException{
        final SVGWriter writer = new SVGWriter();
        final ArrayOutputStream out = new ArrayOutputStream();
        writer.setOutput(out);
        writer.write(doc);
        writer.dispose();

        final Chars res = new Chars(out.getBuffer().toArrayByte(),CharEncodings.UTF_8);
        Assert.assertEquals(expected, res);
    }

}
