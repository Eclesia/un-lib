

package science.unlicense.format.svg;

import science.unlicense.format.svg.SVGReader;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2f64;
import static science.unlicense.format.svg.SVGTestConstants.*;
import science.unlicense.format.svg.model.SVGCircle;
import science.unlicense.format.svg.model.SVGDocument;
import science.unlicense.format.svg.model.SVGEllipse;
import science.unlicense.format.svg.model.SVGLine;
import science.unlicense.format.svg.model.SVGPath;
import science.unlicense.format.svg.model.SVGPolygon;
import science.unlicense.format.svg.model.SVGPolyline;
import science.unlicense.format.svg.model.SVGRect;

/**
 *
 * @author Johann Sorel
 */
public class SVGReaderTest {

    private static final double DELTA = 0.000001;

    @Test
    public void testReadDocument() throws IOException{
        final SVGDocument doc = read(TEST_DOCUMENT);
        Assert.assertEquals(800.0,doc.getWidth(),      DELTA);
        Assert.assertEquals(600.0,doc.getHeight(),     DELTA);
    }

    @Test
    public void testReadRect() throws IOException{
        final SVGDocument doc = read(TEST_RECTANGLE);
        final SVGRect element = (SVGRect) doc.getChildren().get(0);
        Assert.assertEquals(100.0,element.getWidth(),  DELTA);
        Assert.assertEquals(200.0,element.getHeight(), DELTA);
        Assert.assertEquals( 10.0,element.getX(),      DELTA);
        Assert.assertEquals( 20.0,element.getY(),      DELTA);
        Assert.assertEquals( 12.0,element.getRadiusX(),DELTA);
        Assert.assertEquals( 24.0,element.getRadiusY(),DELTA);
    }

    @Test
    public void testReadCercle() throws IOException{
        final SVGDocument doc = read(TEST_CIRCLE);
        final SVGCircle element = (SVGCircle) doc.getChildren().get(0);
        Assert.assertEquals( 3.0,element.getCenterX(),  DELTA);
        Assert.assertEquals( 2.0,element.getCenterY(),  DELTA);
        Assert.assertEquals(10.0,element.getRadius(),   DELTA);
    }

    @Test
    public void testReadEllipse() throws IOException{
        final SVGDocument doc = read(TEST_ELLIPSE);
        final SVGEllipse element = (SVGEllipse) doc.getChildren().get(0);
        Assert.assertEquals( 3.0,element.getCenterX(),  DELTA);
        Assert.assertEquals( 2.0,element.getCenterY(),  DELTA);
        Assert.assertEquals(10.0,element.getRadiusX(),  DELTA);
        Assert.assertEquals(12.0,element.getRadiusY(),  DELTA);
    }

    @Test
    public void testReadLine() throws IOException{
        final SVGDocument doc = read(TEST_LINE);
        final SVGLine element = (SVGLine) doc.getChildren().get(0);
        Assert.assertEquals(1.0,element.getX1(),  DELTA);
        Assert.assertEquals(2.0,element.getY1(),  DELTA);
        Assert.assertEquals(3.0,element.getX2(),  DELTA);
        Assert.assertEquals(4.0,element.getY2(),  DELTA);
    }

    @Test
    public void testReadPolygon() throws IOException{
        final SVGDocument doc = read(TEST_POLYGON);
        final SVGPolygon element = (SVGPolygon) doc.getChildren().get(0);
        final TupleGrid1D points = element.getPointsAsCoords();
        Assert.assertEquals(3, points.getDimension());
        final Vector2f64 v = new Vector2f64();

        points.getTuple(0,v);
        Assert.assertEquals(new Vector2f64(1, 2), v);
        points.getTuple(1,v);
        Assert.assertEquals(new Vector2f64(3, 4), v);
        points.getTuple(2,v);
        Assert.assertEquals(new Vector2f64(5, 6), v);
    }

    @Test
    public void testReadPolyline() throws IOException{
        final SVGDocument doc = read(TEST_POLYLINE);
        final SVGPolyline element = (SVGPolyline) doc.getChildren().get(0);
        final TupleGrid1D points = element.getPointsAsCoords();
        Assert.assertEquals(3, points.getDimension());
        final Vector2f64 v = new Vector2f64();

        points.getTuple(0,v);
        Assert.assertEquals(new Vector2f64(1, 2), v);
        points.getTuple(1,v);
        Assert.assertEquals(new Vector2f64(3, 4), v);
        points.getTuple(2,v);
        Assert.assertEquals(new Vector2f64(5, 6), v);
    }

    @Test
    public void testReadPath1() throws IOException{
        final TupleRW tuple = new Vector2f64();

        final SVGDocument doc = read(TEST_PATH1);
        final SVGPath element = (SVGPath) doc.getChildren().get(0);
        final PathIterator ite = element.asGeometry().createPathIterator();
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO, ite.getType());
        Assert.assertEquals(new Vector2f64(1, 2), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO, ite.getType());
        Assert.assertEquals(new Vector2f64(3, 4), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO, ite.getType());
        Assert.assertEquals(new Vector2f64(5, 6), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_CLOSE, ite.getType());
        Assert.assertFalse(ite.next());
    }

    @Test
    public void testReadPath2() throws IOException{
        final TupleRW tuple = new Vector2f64();

        final SVGDocument doc = read(TEST_PATH2);
        final SVGPath element = (SVGPath) doc.getChildren().get(0);
        final PathIterator ite = element.asGeometry().createPathIterator();
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO, ite.getType());
        Assert.assertEquals(new Vector2f64(1, 2), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO, ite.getType());
        Assert.assertEquals(new Vector2f64(3, 4), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO, ite.getType());
        Assert.assertEquals(new Vector2f64(5, 6), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_CLOSE, ite.getType());
        Assert.assertFalse(ite.next());
    }

    @Test
    public void testReadPath3() throws IOException{
        final TupleRW tuple = new Vector2f64();

        final SVGDocument doc = read(TEST_PATH3);
        final SVGPath element = (SVGPath) doc.getChildren().get(0);
        final PathIterator ite = element.asGeometry().createPathIterator();
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO, ite.getType());
        Assert.assertEquals(new Vector2f64(10e-4, 0.859), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO, ite.getType());
        Assert.assertEquals(new Vector2f64(-1.427,-0.059), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_CLOSE, ite.getType());
        Assert.assertFalse(ite.next());
    }

    private static SVGDocument read(Chars text) throws IOException{
        final ArrayInputStream in = new ArrayInputStream(text.toBytes());
        final SVGReader reader = new SVGReader();
        reader.setInput(in);
        return reader.read();
    }

}
