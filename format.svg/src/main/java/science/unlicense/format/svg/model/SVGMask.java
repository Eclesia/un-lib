

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/masking.html#Masking
 *
 * @author Johann Sorel
 */
public class SVGMask extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_MASK;

    public SVGMask() {
        super(NAME);
    }

    public SVGMask(DomElement node) {
        super(node);
    }

}
