

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/pservers.html#GradientStops
 *
 * @author Johann Sorel
 */
public class SVGStop extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_STOP;

    public SVGStop() {
        super(NAME);
    }

    public SVGStop(DomElement node) {
        super(node);
    }

}
