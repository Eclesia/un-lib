

package science.unlicense.format.svg;

import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.xml.XMLOutputStream;
import science.unlicense.format.xml.dom.DomWriter;
import science.unlicense.format.svg.model.SVGDocument;

/**
 *
 * @author Johann Sorel
 */
public class SVGWriter extends AbstractWriter {

    public void write(SVGDocument doc) throws IOException{
        Object out = getOutput();
        XMLOutputStream xmlOut = null;
        if (out instanceof XMLOutputStream){
            xmlOut = (XMLOutputStream) out;
        } else {
            xmlOut = new XMLOutputStream();
            xmlOut.setOutput(out);
        }

        final DomWriter writer = new DomWriter();
        writer.setOutput(xmlOut);
        writer.write(doc);
    }

}
