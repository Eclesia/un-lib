

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#TextPathElement
 *
 * @author Johann Sorel
 */
public class SVGTextPath extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_TEXTPATH;

    public SVGTextPath() {
        super(NAME);
    }

    public SVGTextPath(DomElement node) {
        super(node);
    }

}
