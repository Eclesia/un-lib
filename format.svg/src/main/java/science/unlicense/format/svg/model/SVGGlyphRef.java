

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#AlternateGlyphDefinitions
 *
 * @author Johann Sorel
 */
public class SVGGlyphRef extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_GLYPHREF;

    public SVGGlyphRef() {
        super(NAME);
    }

    public SVGGlyphRef(DomElement node) {
        super(node);
    }

}
