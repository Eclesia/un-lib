

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/struct.html#UseElement
 *
 * @author Johann Sorel
 */
public class SVGUse extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_USE;

    public SVGUse() {
        super(NAME);
    }

    public SVGUse(DomElement node) {
        super(node);
    }

}
