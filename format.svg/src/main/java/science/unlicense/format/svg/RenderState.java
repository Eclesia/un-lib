

package science.unlicense.format.svg;

import science.unlicense.display.api.painter2d.Brush;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 * Rebuild all style informations from an SVG Graphic.
 * CSS and local style informations could be mixed together which makes it troublesome
 * to choose the right parameters.
 * Animations and expression can also be part of the style definition in such case
 * the RenderState is for a fix time.
 *
 * @author Johann Sorel
 */
public class RenderState {

    public PlanarGeometry fillGeometry;
    public Paint fillPaint;

    public PlanarGeometry strokeGeometry;
    public Paint strokePaint;
    public Brush strokeBrush;

}
