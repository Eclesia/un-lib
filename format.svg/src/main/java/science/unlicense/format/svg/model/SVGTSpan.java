

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#TSpanElement
 *
 * @author Johann Sorel
 */
public class SVGTSpan extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_TSPAN;

    public SVGTSpan() {
        super(NAME);
    }

    public SVGTSpan(DomElement node) {
        super(node);
    }

}
