

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/pservers.html#LinearGradients
 *
 * @author Johann Sorel
 */
public class SVGLinearGradient extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_LINEARGRADIENT;

    public SVGLinearGradient() {
        super(NAME);
    }

    public SVGLinearGradient(DomElement node) {
        super(node);
    }

}
