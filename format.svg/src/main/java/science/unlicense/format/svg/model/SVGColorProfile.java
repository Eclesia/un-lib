

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/color.html#ColorProfileElement
 *
 * @author Johann Sorel
 */
public class SVGColorProfile extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_COLORPROFILE;

    public SVGColorProfile() {
        super(NAME);
    }

    public SVGColorProfile(DomElement node) {
        super(node);
    }

}
