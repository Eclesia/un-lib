
package science.unlicense.format.svg;

import science.unlicense.format.xml.FName;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class SVGConstants {

    public static final Chars NAMESPACE = Chars.constant("http://www.w3.org/2000/svg");

    public static final FName NAME_SVG      = new FName(NAMESPACE, new Chars("svg"));
    public static final FName NAME_DESC     = new FName(NAMESPACE, new Chars("desc"));
    public static final FName NAME_TITLE    = new FName(NAMESPACE, new Chars("title"));
    //shapes
    public static final FName NAME_RECT     = new FName(NAMESPACE, new Chars("rect"));
    public static final FName NAME_CERCLE   = new FName(NAMESPACE, new Chars("circle"));
    public static final FName NAME_ELLIPSE  = new FName(NAMESPACE, new Chars("ellipse"));
    public static final FName NAME_LINE     = new FName(NAMESPACE, new Chars("line"));
    public static final FName NAME_POLYLINE = new FName(NAMESPACE, new Chars("polyline"));
    public static final FName NAME_POLYGON  = new FName(NAMESPACE, new Chars("polygon"));
    public static final FName NAME_PATH     = new FName(NAMESPACE, new Chars("path"));
    public static final FName NAME_G        = new FName(NAMESPACE, new Chars("g"));
    //defs
    public static final FName NAME_DEFS     = new FName(NAMESPACE, new Chars("defs"));
    public static final FName NAME_SYMBOL   = new FName(NAMESPACE, new Chars("symbol"));
    public static final FName NAME_USE      = new FName(NAMESPACE, new Chars("use"));
    public static final FName NAME_IMAGE    = new FName(NAMESPACE, new Chars("image"));
    public static final FName NAME_SWITCH   = new FName(NAMESPACE, new Chars("switch"));
    public static final FName NAME_STYLE    = new FName(NAMESPACE, new Chars("style"));
    //texts
    public static final FName NAME_TEXT     = new FName(NAMESPACE, new Chars("text"));
    public static final FName NAME_TSPAN    = new FName(NAMESPACE, new Chars("tspan"));
    public static final FName NAME_TEXTPATH = new FName(NAMESPACE, new Chars("textPath"));
    public static final FName NAME_TREF     = new FName(NAMESPACE, new Chars("tref"));
    public static final FName NAME_ALTGLYPH = new FName(NAMESPACE, new Chars("altGlyph"));
    public static final FName NAME_ALTGLYPHDEF = new FName(NAMESPACE, new Chars("altGlyphDef"));
    public static final FName NAME_ALTGLYPHITEM= new FName(NAMESPACE, new Chars("altGlyphItem"));
    public static final FName NAME_GLYPHREF    = new FName(NAMESPACE, new Chars("glyphRef"));
    //painting
    public static final FName NAME_MARKER         = new FName(NAMESPACE, new Chars("marker"));
    public static final FName NAME_COLORPROFILE   = new FName(NAMESPACE, new Chars("color-profile"));
    public static final FName NAME_LINEARGRADIENT = new FName(NAMESPACE, new Chars("linearGradient"));
    public static final FName NAME_RADIALGRADIENT = new FName(NAMESPACE, new Chars("radialGradient"));
    public static final FName NAME_STOP           = new FName(NAMESPACE, new Chars("stop"));
    public static final FName NAME_PATTERN        = new FName(NAMESPACE, new Chars("pattern"));
    public static final FName NAME_CLIPPATH       = new FName(NAMESPACE, new Chars("clipPath"));
    public static final FName NAME_MASK           = new FName(NAMESPACE, new Chars("mask"));


    public static final Chars ID = Chars.constant("id");
    public static final Chars STYLE = Chars.constant("style");
    public static final Chars CLASS = Chars.constant("class");

    public static final Chars FILL = Chars.constant("fill");
    public static final Chars STROKEWIDTH = Chars.constant("stroke-width");
    public static final Chars STROKELINECAP = Chars.constant("stroke-linecap");
    public static final Chars WIDTH = Chars.constant("width");
    public static final Chars HEIGHT = Chars.constant("height");
    public static final Chars INHERIT = Chars.constant("inherit");
    public static final Chars d = Chars.constant("d");
    public static final Chars X = Chars.constant("x");
    public static final Chars Y = Chars.constant("y");
    public static final Chars RX = Chars.constant("rx");
    public static final Chars RY = Chars.constant("ry");
    public static final Chars CX = Chars.constant("cx");
    public static final Chars CY = Chars.constant("cy");
    public static final Chars R = Chars.constant("r");
    public static final Chars X1 = Chars.constant("x1");
    public static final Chars Y1 = Chars.constant("y1");
    public static final Chars X2 = Chars.constant("x2");
    public static final Chars Y2 = Chars.constant("y2");
    public static final Chars POINTS = Chars.constant("points");
    public static final Chars CAP_BUTT = Chars.constant("butt");
    public static final Chars CAP_ROUND = Chars.constant("round");
    public static final Chars CAP_SQUARE = Chars.constant("square");

    public static final Char P_SEP1 = new Char(' ');
    public static final Char P_SEP2 = new Char(',');
    public static final Char P_SEP3 = new Char('\n');
    public static final Char P_SEP4 = new Char('\t');

    public static final Char P_ADD = new Char('+');
    public static final Char P_SUB = new Char('-');

    public static final Chars NONE = Chars.constant("none");

    private SVGConstants(){}

}
