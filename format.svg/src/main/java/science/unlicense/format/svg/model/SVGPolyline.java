

package science.unlicense.format.svg.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.tuple.TupleGrid1D;

/**
 * http://www.w3.org/TR/SVG/shapes.html#PolylineElement
 *
 * @author Johann Sorel
 */
public class SVGPolyline extends SVGGraphic {

    public static final FName FName = SVGConstants.NAME_POLYLINE;
    public static final Chars PROP_POINTS = SVGConstants.POINTS;

    public SVGPolyline() {
        super(FName);
    }

    public SVGPolyline(DomElement base){
        super(base);
    }

    public Chars getPoints() {
        return getPropertyChars(PROP_POINTS);
    }

    public void setPoints(Chars value) {
        getProperties().add(PROP_POINTS, value);
    }

    public TupleGrid1D getPointsAsCoords(){
        return readCoordinates(getPoints());
    }

    public PlanarGeometry asGeometry(){
        final TupleGrid1D coords = getPointsAsCoords();
        final Polyline geom = new Polyline(coords);
        return geom;
    }

}
