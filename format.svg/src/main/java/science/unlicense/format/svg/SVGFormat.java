package science.unlicense.format.svg;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * PostScript format.
 *
 * Resource :
 * http://en.wikipedia.org/wiki/PostScript
 * http://www.adobe.com/products/postscript/pdfs/PLRM.pdf
 * http://partners.adobe.com/public/developer/en/ps/PLRM.pdf
 * http://www-cdf.fnal.gov/offline/PostScript/BLUEBOOK.PDF
 *
 * @author Johann Sorel
 */
public class SVGFormat extends DefaultFormat {

    public SVGFormat() {
        super(new Chars("svg"));
        shortName = new Chars("SVG");
        longName = new Chars("Scalable Vector Graphics");
        mimeTypes.add(new Chars("image/svg+xml"));
        extensions.add(new Chars("svg"));
        extensions.add(new Chars("svgz"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
