

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.api.Line;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/shapes.html#LineElement
 *
 * @author Johann Sorel
 */
public class SVGLine extends SVGGraphic {

    public static final FName FName = SVGConstants.NAME_LINE;
    public static final Chars PROP_X1 = SVGConstants.X1;
    public static final Chars PROP_Y1 = SVGConstants.Y1;
    public static final Chars PROP_X2 = SVGConstants.X2;
    public static final Chars PROP_Y2 = SVGConstants.Y2;

    public SVGLine() {
        super(FName);
    }

    public SVGLine(DomElement base){
        super(base);
    }

    public Double getX1() {
        return getPropertyDouble(PROP_X1);
    }

    public void setX1(Double value) {
        getProperties().add(PROP_X1, value);
    }

    public Double getY1() {
        return getPropertyDouble(PROP_Y1);
    }

    public void setY1(Double value) {
        getProperties().add(PROP_Y1, value);
    }

    public Double getX2() {
        return getPropertyDouble(PROP_X2);
    }

    public void setX2(Double value) {
        getProperties().add(PROP_X2, value);
    }

    public Double getY2() {
        return getPropertyDouble(PROP_Y2);
    }

    public void setY2(Double value) {
        getProperties().add(PROP_Y2, value);
    }

    public PlanarGeometry asGeometry(){
        final Line geom = new DefaultLine(2);
        geom.getStart().set(0,getX1());
        geom.getStart().set(1,getY1());
        geom.getEnd().set(0,getX2());
        geom.getEnd().set(1,getY2());
        return geom;
    }

}
