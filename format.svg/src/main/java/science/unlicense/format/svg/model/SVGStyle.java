

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/styling.html#StyleElement
 *
 * @author Johann Sorel
 */
public class SVGStyle extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_USE;

    public SVGStyle() {
        super(NAME);
    }

    public SVGStyle(DomElement node) {
        super(node);
    }

}
