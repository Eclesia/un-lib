

package science.unlicense.format.svg.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 * Map an SVG DOM as a more friendly manipulation model.
 *
 * @author Johann Sorel
 */
public class SVGDocument extends SVGGraphic implements ISVGStylable {

    public static final FName FName = SVGConstants.NAME_SVG;
    public static final Chars PROP_WIDTH = SVGConstants.WIDTH;
    public static final Chars PROP_HEIGHT = SVGConstants.HEIGHT;

    public SVGDocument(){
        super(FName);
    }

    public SVGDocument(DomElement root){
        super(root);
    }

    public Double getWidth() {
        return getPropertyDouble(PROP_WIDTH);
    }

    public void setWidth(Double width) {
        getProperties().add(PROP_WIDTH, width);
    }

    public Double getHeight() {
        return getPropertyDouble(PROP_HEIGHT);
    }

    public void setHeight(Double height) {
        getProperties().add(PROP_HEIGHT, height);
    }

    public PlanarGeometry asGeometry() {
        return null;
    }

}
