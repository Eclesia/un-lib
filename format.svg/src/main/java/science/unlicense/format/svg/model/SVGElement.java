
package science.unlicense.format.svg.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.engine.css.CSSProperties;
import science.unlicense.engine.css.CSSReader;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.color.Color;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.format.svg.SVGConstants;

/**
 *
 * @author Johann Sorel
 */
public class SVGElement extends DomElement{

    /** FLAG COLOR to indicate, NONE, which mean remove previous if any */
    public static final Color NOCOLOR = new ColorRGB(0);

    public static final Chars PROPERTY_ID = SVGConstants.ID;

    private SVGElement parent;

    public SVGElement(FName name) {
        super(name);
    }

    public SVGElement(DomElement node) {
        super(node.getName());
        getProperties().addAll(node.getProperties());
    }

    public SVGElement getParent() {
        return parent;
    }

    private void setParent(SVGElement parent) {
        this.parent = parent;
    }


    /**
     * {@inheritDoc }
     * Set parent reference on new children nodes.
     *
     * @param array
     * @param index
     */
    protected void addChildren(int index, Object[] array) {
        for (Object n : array){
            //search in the parents if we did not create a loop
            SVGElement p = getParent();
            while (p!=null){
                if (p==n){
                    throw new RuntimeException("Loop in node hierarchy");
                }
                p = p.getParent();
            }

            if (n instanceof SVGElement){
                final SVGElement previous = ((SVGElement) n).getParent();
                if (previous!=null) previous.getChildren().remove(n);
                ((SVGElement) n).setParent(this);
            }
        }
        super.addChildren(index,array);
    }

    /**
     * {@inheritDoc }
     * Removes parent reference on children node.
     *
     * @param index
     */
    protected boolean removeChildNode(int index){
        //remove parent reference
        final Node child = (Node) getChildren().get(index);
        if (child instanceof SVGElement){
            ((SVGElement) child).setParent(null);
        }
        return super.removeChildNode(index);
    }


    public Chars getId(){
        return getPropertyChars(PROPERTY_ID);
    }

    // ISVGStylable ////////////////////////////////////////////////////////////

    protected Chars getStyleClass(){
        return getPropertyChars(ISVGStylable.PROPERTY_CLASS);
    }

    protected CSSProperties getStyle(){
        final Chars text = getPropertyChars(ISVGStylable.PROPERTY_STYLE);
        if (text==null){
            return new CSSProperties();
        } else {
            final ArrayInputStream in = new ArrayInputStream(text.toBytes());
            final CSSReader reader = new CSSReader();
            try {
                reader.setInput(in);
                return reader.readProperties();
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
        }
    }

    protected Chars getPropertyChars(Chars name){
        final Object obj = getProperties().getValue(name);
        if (obj==null){
            return null;
        } else if (obj instanceof Chars){
            return (Chars) obj;
        } else {
            return new Chars(""+obj);
        }
    }

    protected Integer getPropertyInt(Chars name){
        final Object obj = getProperties().getValue(name);
        if (obj==null){
            return null;
        } else if (obj instanceof Chars){
            return Int32.decode((Chars) obj);
        } else if (obj instanceof Integer){
            return (Integer) obj;
        } else if (obj instanceof Number){
            return ((Number) obj).intValue();
        } else {
            return null;
        }
    }

    protected Double getPropertyDouble(Chars name){
        final Object obj = getProperties().getValue(name);
        if (obj==null){
            return null;
        } else if (obj instanceof Chars){
            return Float64.decode((Chars) obj);
        } else if (obj instanceof Double){
            return (Double) obj;
        } else if (obj instanceof Number){
            return ((Number) obj).doubleValue();
        } else {
            return null;
        }
    }

    protected Color getPropertyColor(Chars name){
        final Chars buffer = (Chars) getProperties().getValue(name);
        if (SVGConstants.NONE.equals(buffer, true, true)){
            return NOCOLOR;
        } else if (buffer != null){
            final int red = Int32.decodeHexa(buffer.truncate(1, 3));
            final int green = Int32.decodeHexa(buffer.truncate(3, 5));
            final int blue = Int32.decodeHexa(buffer.truncate(5, 7));
            return new ColorRGB(red, green, blue, 255);
        } else {
            //default
            return null;
        }
    }

}
