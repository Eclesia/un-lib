

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/painting.html#MarkerElement
 *
 * @author Johann Sorel
 */
public class SVGMarker extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_MARKER;

    public SVGMarker() {
        super(NAME);
    }

    public SVGMarker(DomElement node) {
        super(node);
    }

}
