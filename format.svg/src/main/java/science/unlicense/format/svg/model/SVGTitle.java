
package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/struct.html#DescriptionAndTitleElements
 *
 * @author Johann Sorel
 */
public class SVGTitle extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_TITLE;

    public SVGTitle() {
        super(NAME);
    }

    public SVGTitle(DomElement base) {
        super(base);
        setText(base.getText());
    }

}
