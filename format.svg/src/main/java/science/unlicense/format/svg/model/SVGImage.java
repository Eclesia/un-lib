

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/struct.html#ImageElement
 *
 * @author Johann Sorel
 */
public class SVGImage extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_IMAGE;

    public SVGImage() {
        super(NAME);
    }

    public SVGImage(DomElement node) {
        super(node);
    }

}
