

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#TextElement
 *
 * @author Johann Sorel
 */
public class SVGText extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_TEXT;

    public SVGText() {
        super(NAME);
    }

    public SVGText(DomElement node) {
        super(node);
    }

}
