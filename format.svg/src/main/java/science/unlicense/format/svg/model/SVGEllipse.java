

package science.unlicense.format.svg.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.impl.Ellipse;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 * http://www.w3.org/TR/SVG/shapes.html#EllipseElement
 *
 * @author Johann Sorel
 */
public class SVGEllipse extends SVGGraphic {

    public static final FName FName = SVGConstants.NAME_ELLIPSE;
    public static final Chars PROP_CX = SVGConstants.CX;
    public static final Chars PROP_CY = SVGConstants.CY;
    public static final Chars PROP_RX = SVGConstants.RX;
    public static final Chars PROP_RY = SVGConstants.RY;

    public SVGEllipse() {
        super(FName);
    }

    public SVGEllipse(DomElement base){
        super(base);
    }

    public Double getCenterX() {
        return getPropertyDouble(PROP_CX);
    }

    public void setCenterX(Double value) {
        getProperties().add(PROP_CX, value);
    }

    public Double getCenterY() {
        return getPropertyDouble(PROP_CY);
    }

    public void setCenterY(Double value) {
        getProperties().add(PROP_CY, value);
    }

    public Double getRadiusX() {
        return getPropertyDouble(PROP_RX);
    }

    public void setRadiusX(Double value) {
        getProperties().add(PROP_RX, value);
    }

    public Double getRadiusY() {
        return getPropertyDouble(PROP_RY);
    }

    public void setRadiusY(Double value) {
        getProperties().add(PROP_RY, value);
    }

    public PlanarGeometry asGeometry(){
        final Ellipse geom = new Ellipse();
        geom.setCenterX(getCenterX());
        geom.setCenterY(getCenterY());
        geom.setRadiusX(getRadiusX());
        geom.setRadiusY(getRadiusY());
        return geom;
    }

}
