

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#AlternateGlyphDefinitions
 *
 * @author Johann Sorel
 */
public class SVGAltGlyphItem extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_ALTGLYPHITEM;

    public SVGAltGlyphItem() {
        super(NAME);
    }

    public SVGAltGlyphItem(DomElement node) {
        super(node);
    }

}
