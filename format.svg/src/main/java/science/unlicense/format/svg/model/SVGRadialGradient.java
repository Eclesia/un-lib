

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/pservers.html#RadialGradients
 *
 * @author Johann Sorel
 */
public class SVGRadialGradient extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_RADIALGRADIENT;

    public SVGRadialGradient() {
        super(NAME);
    }

    public SVGRadialGradient(DomElement node) {
        super(node);
    }

}
