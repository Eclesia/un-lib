

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/masking.html#EstablishingANewClippingPath
 *
 * @author Johann Sorel
 */
public class SVGClipPath extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_CLIPPATH;

    public SVGClipPath() {
        super(NAME);
    }

    public SVGClipPath(DomElement node) {
        super(node);
    }

}
