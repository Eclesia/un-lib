

package science.unlicense.format.svg.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.engine.css.CSSProperties;
import science.unlicense.image.api.color.Color;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/types.html#InterfaceSVGStylable
 *
 * @author Johann Sorel
 */
public interface ISVGStylable {

    public static final Chars PROPERTY_STYLE = SVGConstants.STYLE;
    public static final Chars PROPERTY_CLASS = SVGConstants.CLASS;

    public static final Chars PROPERTY_ALIGNMENT_BASELINE           = Chars.constant("alignment-baseline");
    public static final Chars PROPERTY_BASELINE_SHIFT               = Chars.constant("baseline-shift");
    public static final Chars PROPERTY_CLIP                         = Chars.constant("clip");
    public static final Chars PROPERTY_CLIP_PATH                    = Chars.constant("clip-path");
    public static final Chars PROPERTY_CLIP_RULE                    = Chars.constant("clip-rule");
    public static final Chars PROPERTY_COLOR                        = Chars.constant("color");
    public static final Chars PROPERTY_COLOR_INTERPOLATION          = Chars.constant("color-interpolation");
    public static final Chars PROPERTY_COLOR_INTERPOLATION_FILTERS  = Chars.constant("color-interpolation-filters");
    public static final Chars PROPERTY_COLOR_PROFILE                = Chars.constant("color-profile");
    public static final Chars PROPERTY_COLOR_RENDERING              = Chars.constant("color-rendering");
    public static final Chars PROPERTY_CURSOR                       = Chars.constant("cursor");
    public static final Chars PROPERTY_DIRECTION                    = Chars.constant("direction");
    public static final Chars PROPERTY_DISPLAY                      = Chars.constant("display");
    public static final Chars PROPERTY_DOMINANT_BASELINE            = Chars.constant("dominant-baseline");
    public static final Chars PROPERTY_ENABLE_BACKGROUND            = Chars.constant("enable-background");
    public static final Chars PROPERTY_FILL                         = Chars.constant("fill");
    public static final Chars PROPERTY_FILL_OPACITY                 = Chars.constant("fill-opacity");
    public static final Chars PROPERTY_FILL_RULE                    = Chars.constant("fill-rule");
    public static final Chars PROPERTY_FILTER                       = Chars.constant("filter");
    public static final Chars PROPERTY_FLOOD_COLOR                  = Chars.constant("flood-color");
    public static final Chars PROPERTY_FLOOD_OPACITY                = Chars.constant("flood-opacity");
    public static final Chars PROPERTY_FONT_FAMILY                  = Chars.constant("font-family");
    public static final Chars PROPERTY_FONT_SIZE                    = Chars.constant("font-size");
    public static final Chars PROPERTY_FONT_SIZE_ADJUST             = Chars.constant("font-size-adjust");
    public static final Chars PROPERTY_FONT_STRETCH                 = Chars.constant("font-stretch");
    public static final Chars PROPERTY_FONT_STYLE                   = Chars.constant("font-style");
    public static final Chars PROPERTY_FONT_VARIANT                 = Chars.constant("font-variant");
    public static final Chars PROPERTY_FONT_WEIGHT                  = Chars.constant("font-weight");
    public static final Chars PROPERTY_GLYPH_ORIENTATION_HORIZONTAL = Chars.constant("glyph-orientation-horizontal");
    public static final Chars PROPERTY_GLYPH_ORIENTATION_VERTICAL   = Chars.constant("glyph-orientation-vertical");
    public static final Chars PROPERTY_IMAGE_RENDERING              = Chars.constant("image-rendering");
    public static final Chars PROPERTY_KERNING                      = Chars.constant("kerning");
    public static final Chars PROPERTY_LETTER_SPACING               = Chars.constant("letter-spacing");
    public static final Chars PROPERTY_LIGHTNING_COLOR              = Chars.constant("lighting-color");
    public static final Chars PROPERTY_MARKER_END                   = Chars.constant("marker-end");
    public static final Chars PROPERTY_MARKER_MID                   = Chars.constant("marker-mid");
    public static final Chars PROPERTY_MARKER_START                 = Chars.constant("marker-start");
    public static final Chars PROPERTY_MASK                         = Chars.constant("mask");
    public static final Chars PROPERTY_OPACITY                      = Chars.constant("opacity");
    public static final Chars PROPERTY_OVERFLOW                     = Chars.constant("overflow");
    public static final Chars PROPERTY_POINTER_EVENTS               = Chars.constant("pointer-events");
    public static final Chars PROPERTY_SHAPE_RENDERING              = Chars.constant("shape-rendering");
    public static final Chars PROPERTY_STOP_COLOR                   = Chars.constant("stop-color");
    public static final Chars PROPERTY_STOP_OPACITY                 = Chars.constant("stop-opacity");
    public static final Chars PROPERTY_STROKE                       = Chars.constant("stroke");
    public static final Chars PROPERTY_STROKE_DASHARRAY             = Chars.constant("stroke-dasharray");
    public static final Chars PROPERTY_STROKE_DASHOFFSET            = Chars.constant("stroke-dashoffset");
    public static final Chars PROPERTY_STROKE_LINECAP               = Chars.constant("stroke-linecap");
    public static final Chars PROPERTY_STROKE_LINEJOIN              = Chars.constant("stroke-linejoin");
    public static final Chars PROPERTY_STROKE_MITERLIMIT            = Chars.constant("stroke-miterlimit");
    public static final Chars PROPERTY_STROKE_OPACITY               = Chars.constant("stroke-opacity");
    public static final Chars PROPERTY_STROKE_WIDTH                 = Chars.constant("stroke-width");
    public static final Chars PROPERTY_TEXT_ANCHOR                  = Chars.constant("text-anchor");
    public static final Chars PROPERTY_TEXT_DECORATION              = Chars.constant("text-decoration");
    public static final Chars PROPERTY_TEXT_RENDERING               = Chars.constant("text-rendering");
    public static final Chars PROPERTY_UNICODE_BIDI                 = Chars.constant("unicode-bidi");
    public static final Chars PROPERTY_VISIBILITY                   = Chars.constant("visibility");
    public static final Chars PROPERTY_WORD_SPACING                 = Chars.constant("word-spacing");
    public static final Chars PROPERTY_WRITING_MODE                 = Chars.constant("writing-mode");



    // CSS style ///////////////////////////////////////////////////////////////

    Chars getStyleClass();

    CSSProperties getStyle();

    // attributes //////////////////////////////////////////////////////////////

    Chars getAlignmentBaseline();

    void setAlignmentBaseline(Chars value);

    Chars getBaselineShift();

    void setBaselineShift(Chars value);

    Chars getClip();

    void setClip(Chars value);

    Chars getClipPath();

    void setClipPath(Chars value);

    Chars getClipRule();

    void setClipRule(Chars value);

    Color getColor();

    void setColor(Color value);

    Chars getColorInterpolation();

    void setColorInterpolation(Chars value);

    Chars getColorInterpolationFilters();

    void setColorInterpolationFilters(Chars value);

    Chars getColorProfile();

    void setColorProfile(Chars value);

    Chars getColorRendering();

    void setColorRendering(Chars value);

    Chars getCursor();

    void setCursor(Chars value);

    Chars getDirection();

    void setDirection(Chars value);

    Chars getDisplay();

    void setDisplay(Chars value);

    Chars getDominantBaseline();

    void setDominantBaseline(Chars value);

    Chars getEnableBackground();

    void setEnableBackground(Chars value);

    Color getFill();

    void setFill(Color value);

    Chars getFillOpacity();

    void setFillOpacity(Chars value);

    Chars getfillRule();

    void setfillRule(Chars value);

    Chars getFilter();

    void setFilter(Chars value);

    Color getFloodColor();

    void setFloodColor(Color value);

    Chars getFloodOpacity();

    void setFloodOpacity(Chars value);

    Chars getFontFamily();

    void setFontFamily(Chars value);

    Chars getFontSize();

    void setFontSize(Chars value);

    Chars getFontSizeAdjust();

    void setFontSizeAdjust(Chars value);

    Chars getFontStretch();

    void setFontStretch(Chars value);

    Chars getFontStyle();

    void setFontStyle(Chars value);

    Chars getFontVariant();

    void setFontVariant(Chars value);

    Chars getFontWeight();

    void setFontWeight(Chars value);

    Chars getGlyphOrientationHorizontal();

    void setGlyphOrientationHorizontal(Chars value);

    Chars getGlyphOrientationVertical();

    void setGlyphOrientationVertical(Chars value);

    Chars getImageRendering();

    void setImageRendering(Chars value);

    Chars getKerning();

    void setKerning(Chars value);

    Chars getLetterSpacing();

    void setLetterSpacing(Chars value);

    Color getLightingColor();

    void setLightingColor(Color value);

    Chars getMarkerEnd();

    void setMarkerEnd(Chars value);

    Chars getMarkerMid();

    void setMarkerMid(Chars value);

    Chars getMarkerStart();

    void setMarkerStart(Chars value);

    Chars getMask();

    void setMask(Chars value);

    Chars getOpacity();

    void setOpacity(Chars value);

    Chars getOverflow();

    void setOverflow(Chars value);

    Chars getPointerEvents();

    void setPointerEvents(Chars value);

    Chars getShapeRendering();

    void setShapeRendering(Chars value);

    Color getStopColor();

    void setStopColor(Color value);

    Chars getStopOpacity();

    void setStopOpacity(Chars value);

    Color getStroke();

    void setStroke(Color value);

    Chars getStrokeDasharray();

    void setStrokeDasharray(Chars value);

    Chars getStrokeDashoffset();

    void setStrokeDashoffset(Chars value);

    Chars getStrokeLinecap();

    void setStrokeLinecap(Chars value);

    Chars getStrokeLinejoin();

    void setStrokeLinejoin(Chars value);

    Chars getStrokeMiterlimit();

    void setStrokeMiterlimit(Chars value);

    Chars getStrokeOpacity();

    void setStrokeOpacity(Chars value);

    Double getStrokeWidth();

    void setStrokeWidth(Double value);

    Chars getTextAnchor();

    void setTextAnchor(Chars value);

    Chars getTextDecoration();

    void setTextDecoration(Chars value);

    Chars getTextRendering();

    void setTextRendering(Chars value);

    Chars getUnicodeBidi();

    void setUnicodeBidi(Chars value);

    Chars getVisibility();

    void setVisibility(Chars value);

    Chars getWordSpacing();

    void setWordSpacing(Chars value);

    Chars getWritingMode();

    void setWritingMode(Chars value);

}
