

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#TRefElement
 *
 * @author Johann Sorel
 */
public class SVGTRef extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_TREF;

    public SVGTRef() {
        super(NAME);
    }

    public SVGTRef(DomElement node) {
        super(node);
    }

}
