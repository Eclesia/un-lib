

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/struct.html#SymbolElement
 *
 * @author Johann Sorel
 */
public class SVGSymbol extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_SYMBOL;

    public SVGSymbol() {
        super(NAME);
    }

    public SVGSymbol(DomElement node) {
        super(node);
    }

}
