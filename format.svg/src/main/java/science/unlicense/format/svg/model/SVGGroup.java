

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 *
 * @author Johann Sorel
 */
public class SVGGroup extends SVGGraphic {

    public static final FName FName = SVGConstants.NAME_G;

    public SVGGroup() {
        super(FName);
    }

    public SVGGroup(DomElement base){
        super(base);
    }

    public PlanarGeometry asGeometry() {
        return null;
    }

}
