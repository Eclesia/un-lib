

package science.unlicense.format.svg.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 * http://www.w3.org/TR/SVG/shapes.html#CircleElement
 *
 * @author Johann Sorel
 */
public class SVGCircle extends SVGGraphic {

    public static final FName FName = SVGConstants.NAME_CERCLE;
    public static final Chars PROP_CX = SVGConstants.CX;
    public static final Chars PROP_CY = SVGConstants.CY;
    public static final Chars PROP_R = SVGConstants.R;

    public SVGCircle() {
        super(FName);
    }

    public SVGCircle(DomElement base){
        super(base);
    }

    public Double getCenterX() {
        return getPropertyDouble(PROP_CX);
    }

    public void setCenterX(Double value) {
        getProperties().add(PROP_CX, value);
    }

    public Double getCenterY() {
        return getPropertyDouble(PROP_CY);
    }

    public void setCenterY(Double value) {
        getProperties().add(PROP_CY, value);
    }

    public Double getRadius() {
        return getPropertyDouble(PROP_R);
    }

    public void setRadius(Double value) {
        getProperties().add(PROP_R, value);
    }

    public PlanarGeometry asGeometry(){
        final Circle geom = new Circle();
        geom.setCenterX(getCenterX());
        geom.setCenterY(getCenterY());
        geom.setRadius(getRadius());
        return geom;
    }

}
