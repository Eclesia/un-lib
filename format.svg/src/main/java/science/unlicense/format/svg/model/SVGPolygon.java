

package science.unlicense.format.svg.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.tuple.TupleGrid1D;

/**
 * http://www.w3.org/TR/SVG/shapes.html#PolygonElement
 *
 * @author Johann Sorel
 */
public class SVGPolygon extends SVGGraphic {

    public static final FName FName = SVGConstants.NAME_RECT;
    public static final Chars PROP_POINTS = SVGConstants.POINTS;

    public SVGPolygon() {
        super(FName);
    }

    public SVGPolygon(DomElement base){
        super(base);
    }

    public Chars getPoints() {
        return getPropertyChars(PROP_POINTS);
    }

    public void setPoints(Chars value) {
        getProperties().add(PROP_POINTS, value);
    }

    public TupleGrid1D getPointsAsCoords(){
        return readCoordinates(getPoints());
    }

    public PlanarGeometry asGeometry(){
        final TupleGrid1D coords = getPointsAsCoords();
        final Polyline outter = new Polyline(coords);
        final Polygon geom = new Polygon(outter,new ArraySequence());
        return geom;
    }

}
