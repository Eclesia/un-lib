

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/pservers.html#Patterns
 *
 * @author Johann Sorel
 */
public class SVGPattern extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_PATTERN;

    public SVGPattern() {
        super(NAME);
    }

    public SVGPattern(DomElement node) {
        super(node);
    }

}
