

package science.unlicense.format.svg;

import java.lang.reflect.Field;
import science.unlicense.common.api.character.Chars;
import science.unlicense.image.api.color.Color;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.image.api.color.ColorRGB;

/**
 *
 * @author Johann Sorel
 */
public final class SVGColorPalette {

    public static final Color ALICE_BLUE            = new ColorRGB(240, 248, 255);
    public static final Color ANTIQUEWHITE          = new ColorRGB(250, 235, 215);
    public static final Color AQUA                  = new ColorRGB(  0, 255, 255);
    public static final Color AQUAMARINE            = new ColorRGB(127, 255, 212);
    public static final Color AZURE                 = new ColorRGB(240, 255, 255);
    public static final Color BEIGE                 = new ColorRGB(245, 245, 220);
    public static final Color BISQUE                = new ColorRGB(255, 228, 196);
    public static final Color BLACK                 = new ColorRGB(  0,   0,   0);
    public static final Color BLANCHEDALMOND        = new ColorRGB(255, 235, 205);
    public static final Color BLUE                  = new ColorRGB(  0,   0, 255);
    public static final Color BLUEVIOLET            = new ColorRGB(138,  43, 226);
    public static final Color BROWN                 = new ColorRGB(165,  42,  42);
    public static final Color BURLYWOOD             = new ColorRGB(222, 184, 135);
    public static final Color CADETBLUE             = new ColorRGB( 95, 158, 160);
    public static final Color CHARTREUSE            = new ColorRGB(127, 255,   0);
    public static final Color CHOCOLATE             = new ColorRGB(210, 105,  30);
    public static final Color CORAL                 = new ColorRGB(255, 127,  80);
    public static final Color CORNFLOWERBLUE        = new ColorRGB(100, 149, 237);
    public static final Color CORNSILK              = new ColorRGB(255, 248, 220);
    public static final Color CRIMSON               = new ColorRGB(220,  20,  60);
    public static final Color CYAN                  = new ColorRGB(  0, 255, 255);
    public static final Color DARKBLUE              = new ColorRGB(  0,   0, 139);
    public static final Color DARKcyan              = new ColorRGB(  0, 139, 139);
    public static final Color DARKGOLDENROD         = new ColorRGB(184, 134,  11);
    public static final Color DARKGRAY              = new ColorRGB(169, 169, 169);
    public static final Color DARKGREEN             = new ColorRGB(  0, 100,   0);
    public static final Color DARKGREY              = new ColorRGB(169, 169, 169);
    public static final Color DARKkhaki             = new ColorRGB(189, 183, 107);
    public static final Color DARKMAGENTA           = new ColorRGB(139,   0, 139);
    public static final Color DARKOLIVEGREEN        = new ColorRGB( 85, 107,  47);
    public static final Color DARKORANGE            = new ColorRGB(255, 140,   0);
    public static final Color DARKORCHID            = new ColorRGB(153,  50, 204);
    public static final Color DARKRED               = new ColorRGB(139,   0,   0);
    public static final Color DARKSALMON            = new ColorRGB(233, 150, 122);
    public static final Color DARKSEAGREEN          = new ColorRGB(143, 188, 143);
    public static final Color DARKSLATEBLUE         = new ColorRGB( 72,  61, 139);
    public static final Color DARKSLATEGRAY         = new ColorRGB( 47,  79,  79);
    public static final Color DARKSLATEGREY         = new ColorRGB( 47,  79,  79);
    public static final Color DARKTURQUOISE         = new ColorRGB(  0, 206, 209);
    public static final Color DARKVIOLET            = new ColorRGB(148,   0, 211);
    public static final Color DEEPPINK              = new ColorRGB(255,  20, 147);
    public static final Color DEEPskyBLUE           = new ColorRGB(  0, 191, 255);
    public static final Color DIMGRAY               = new ColorRGB(105, 105, 105);
    public static final Color DIMGREY               = new ColorRGB(105, 105, 105);
    public static final Color DODGERBLUE            = new ColorRGB( 30, 144, 255);
    public static final Color FIREBRICK             = new ColorRGB(178,  34,  34);
    public static final Color FLORALWHITE           = new ColorRGB(255, 250, 240);
    public static final Color FORESTGREEN           = new ColorRGB( 34, 139,  34);
    public static final Color FUCHSIA               = new ColorRGB(255,   0, 255);
    public static final Color GAINSBORO             = new ColorRGB(220, 220, 220);
    public static final Color GHOSTWHITE            = new ColorRGB(248, 248, 255);
    public static final Color GOLD                  = new ColorRGB(255, 215,   0);
    public static final Color GOLDENROD             = new ColorRGB(218, 165,  32);
    public static final Color GRAY                  = new ColorRGB(128, 128, 128);
    public static final Color GREY                  = new ColorRGB(128, 128, 128);
    public static final Color GREEN                 = new ColorRGB(  0, 128,   0);
    public static final Color GREENYELLOW           = new ColorRGB(173, 255,  47);
    public static final Color HONEYDEW              = new ColorRGB(240, 255, 240);
    public static final Color HOTPINK               = new ColorRGB(255, 105, 180);
    public static final Color INDIANRED             = new ColorRGB(205,  92,  92);
    public static final Color INDIGO                = new ColorRGB( 75,   0, 130);
    public static final Color IVORY                 = new ColorRGB(255, 255, 240);
    public static final Color KHAKI                 = new ColorRGB(240, 230, 140);
    public static final Color LAVENDER              = new ColorRGB(230, 230, 250);
    public static final Color LAVENDERBLUSH         = new ColorRGB(255, 240, 245);
    public static final Color LAWNGREEN             = new ColorRGB(124, 252,   0);
    public static final Color LEMONCHIFFON          = new ColorRGB(255, 250, 205);
    public static final Color LIGHTBLUE             = new ColorRGB(173, 216, 230);
    public static final Color LIGHTCORAL            = new ColorRGB(240, 128, 128);
    public static final Color LIGHTCYAN             = new ColorRGB(224, 255, 255);
    public static final Color LIGHTGOLDENRODYELLOW  = new ColorRGB(250, 250, 210);
    public static final Color LIGHTGRAY             = new ColorRGB(211, 211, 211);
    public static final Color LIGHTGREEN            = new ColorRGB(144, 238, 144);
    public static final Color LIGHTGREY             = new ColorRGB(211, 211, 211);
    public static final Color LIGHTPINK             = new ColorRGB(255, 182, 193);
    public static final Color LIGHTSALMON           = new ColorRGB(255, 160, 122);
    public static final Color LIGHTSEAGREEN         = new ColorRGB( 32, 178, 170);
    public static final Color LIGHTskyBLUE          = new ColorRGB(135, 206, 250);
    public static final Color LIGHTSLATEGRAY        = new ColorRGB(119, 136, 153);
    public static final Color LIGHTSLATEGREY        = new ColorRGB(119, 136, 153);
    public static final Color LIGHTSTEELBLUE        = new ColorRGB(176, 196, 222);
    public static final Color LIGHTYELLOW           = new ColorRGB(255, 255, 224);
    public static final Color LIME                  = new ColorRGB(  0, 255,   0);
    public static final Color LIMEGREEN             = new ColorRGB( 50, 205,  50);
    public static final Color LINEN                 = new ColorRGB(250, 240, 230);
    public static final Color MAGENTA               = new ColorRGB(255,   0, 255);
    public static final Color MAROON                = new ColorRGB(128,   0,   0);
    public static final Color MEDIUMAQUAMARINE      = new ColorRGB(102, 205, 170);
    public static final Color MEDIUMBLUE            = new ColorRGB(  0,   0, 205);
    public static final Color MEDIUMORCHID          = new ColorRGB(186,  85, 211);
    public static final Color MEDIUMPURPLE          = new ColorRGB(147, 112, 219);
    public static final Color MEDIUMSEAGREEN        = new ColorRGB( 60, 179, 113);
    public static final Color MEDIUMSLATEBLUE       = new ColorRGB(123, 104, 238);
    public static final Color MEDIUMspringGREEN     = new ColorRGB(  0, 250, 154);
    public static final Color MEDIUMTURQUOISE       = new ColorRGB( 72, 209, 204);
    public static final Color MEDIUMVIOLETRED       = new ColorRGB(199,  21, 133);
    public static final Color MIDNIGHTBLUE          = new ColorRGB( 25,  25, 112);
    public static final Color MINTCREAM             = new ColorRGB(245, 255, 250);
    public static final Color MISTYROSE             = new ColorRGB(255, 228, 225);
    public static final Color MOCCASIN              = new ColorRGB(255, 228, 181);
    public static final Color NAVAJOWHITE           = new ColorRGB(255, 222, 173);
    public static final Color NAVY                  = new ColorRGB(  0,   0, 128);
    public static final Color OLDLACE               = new ColorRGB(253, 245, 230);
    public static final Color OLIVE                 = new ColorRGB(128, 128,   0);
    public static final Color OLIVEDRAB             = new ColorRGB(107, 142,  35);
    public static final Color ORANGE                = new ColorRGB(255, 165,   0);
    public static final Color ORANGERED             = new ColorRGB(255,  69,   0);
    public static final Color ORCHID                = new ColorRGB(218, 112, 214);
    public static final Color PALEGOLDENROD         = new ColorRGB(238, 232, 170);
    public static final Color PALEGREEN             = new ColorRGB(152, 251, 152);
    public static final Color PALETURQUOISE         = new ColorRGB(175, 238, 238);
    public static final Color PALEVIOLETRED         = new ColorRGB(219, 112, 147);
    public static final Color PAPAYAWHIP            = new ColorRGB(255, 239, 213);
    public static final Color PEACHPUFF             = new ColorRGB(255, 218, 185);
    public static final Color PERU                  = new ColorRGB(205, 133,  63);
    public static final Color PINK                  = new ColorRGB(255, 192, 203);
    public static final Color PLUM                  = new ColorRGB(221, 160, 221);
    public static final Color POWDERBLUE            = new ColorRGB(176, 224, 230);
    public static final Color PURPLE                = new ColorRGB(128,   0, 128);
    public static final Color RED                   = new ColorRGB(255,   0,   0);
    public static final Color ROSYBROWN             = new ColorRGB(188, 143, 143);
    public static final Color ROYALBLUE             = new ColorRGB( 65, 105, 225);
    public static final Color SADDLEBROWN           = new ColorRGB(139,  69,  19);
    public static final Color SALMON                = new ColorRGB(250, 128, 114);
    public static final Color SANDYBROWN            = new ColorRGB(244, 164,  96);
    public static final Color SEAGREEN              = new ColorRGB( 46, 139,  87);
    public static final Color SEASHELL              = new ColorRGB(255, 245, 238);
    public static final Color SIENNA                = new ColorRGB(160,  82,  45);
    public static final Color SILVER                = new ColorRGB(192, 192, 192);
    public static final Color SKYBLUE               = new ColorRGB(135, 206, 235);
    public static final Color SLATEBLUE             = new ColorRGB(106,  90, 205);
    public static final Color SLATEGRAY             = new ColorRGB(112, 128, 144);
    public static final Color SLATEGREY             = new ColorRGB(112, 128, 144);
    public static final Color SNOW                  = new ColorRGB(255, 250, 250);
    public static final Color SPRINGGREEN           = new ColorRGB(  0, 255, 127);
    public static final Color STEELBLUE             = new ColorRGB( 70, 130, 180);
    public static final Color TAN                   = new ColorRGB(210, 180, 140);
    public static final Color TEAL                  = new ColorRGB(  0, 128, 128);
    public static final Color THISTLE               = new ColorRGB(216, 191, 216);
    public static final Color TOMATO                = new ColorRGB(255,  99,  71);
    public static final Color TURQUOISE             = new ColorRGB( 64, 224, 208);
    public static final Color VIOLET                = new ColorRGB(238, 130, 238);
    public static final Color WHEAT                 = new ColorRGB(245, 222, 179);
    public static final Color WHITE                 = new ColorRGB(255, 255, 255);
    public static final Color WHITESMOKE            = new ColorRGB(245, 245, 245);
    public static final Color YELLOW                = new ColorRGB(255, 255,   0);
    public static final Color YELLOWGREEN           = new ColorRGB(154, 205,  50);

    private SVGColorPalette(){}

    public static Color forName(Chars name){
        try {
            final Field field = SVGColorPalette.class.getField(name.toUpperCase().toString());
            return (Color) field.get(null);
        } catch (NoSuchFieldException ex) {
            return null;
        } catch (SecurityException ex) {
            return null;
        } catch (InvalidArgumentException ex) {
            return null;
        } catch (IllegalAccessException ex) {
            return null;
        }
    }

}
