

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#AltGlyphElement
 *
 * @author Johann Sorel
 */
public class SVGAltGlyph extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_ALTGLYPH;

    public SVGAltGlyph() {
        super(NAME);
    }

    public SVGAltGlyph(DomElement node) {
        super(node);
    }

}
