

package science.unlicense.format.svg.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.impl.Matrix3x3;

/**
 * http://www.w3.org/TR/SVG/coords.html
 *
 * @author Johann Sorel
 */
public interface ISVGTransform {

    public static final Chars PROPERTY_TRANSFORM          = Chars.constant("transforme");
    public static final Chars PROPERTY_VIEWBOX            = Chars.constant("viewBox");
    public static final Chars PROPERTY_PRESERVEASPECRATIO = Chars.constant("preserveAspectRatio");

    public static final Chars TRANSFORM_MATRIX      = Chars.constant("matrix");
    public static final Chars TRANSFORM_TRANSLATE   = Chars.constant("translate");
    public static final Chars TRANSFORM_SCALE       = Chars.constant("scale");
    public static final Chars TRANSFORM_ROTATE      = Chars.constant("rotate");
    public static final Chars TRANSFORM_SKEWX       = Chars.constant("skewX");
    public static final Chars TRANSFORM_SKEWY       = Chars.constant("skewY");

    Matrix3x3 getTransform();

    void setTransform(Matrix3x3 trs);

    BBox getViewBox();

    void setViewBox(BBox bbox);

    //TODO preserve ratio, akward definition

}
