

package science.unlicense.format.svg.model;

import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/struct.html#SwitchElement
 *
 * @author Johann Sorel
 */
public class SVGSwitch extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_SWITCH;

    public SVGSwitch() {
        super(NAME);
    }

    public SVGSwitch(DomElement node) {
        super(node);
    }

}
