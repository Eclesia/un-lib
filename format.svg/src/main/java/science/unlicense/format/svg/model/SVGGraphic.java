

package science.unlicense.format.svg.model;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharIterator;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.DoubleSequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.engine.css.CSSProperties;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.image.api.color.Color;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.impl.path.PathStep2D;
import science.unlicense.geometry.api.Curve;
import science.unlicense.geometry.impl.Path;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.format.xml.FName;
import science.unlicense.format.xml.dom.DomElement;
import science.unlicense.format.svg.RenderState;
import static science.unlicense.format.svg.SVGConstants.*;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.tuple.TupleGrid1D;

/**
 *
 * @author Johann Sorel
 */
public abstract class SVGGraphic extends SVGElement implements ISVGStylable, ISVGTransform {

    public static final Char P_MOVETO_ABS = new Char('M');
    public static final Char P_MOVETO_REL = new Char('m');
    public static final Char P_LINETO_ABS = new Char('L');
    public static final Char P_LINETO_REL = new Char('l');
    public static final Char P_VERTICAL_ABS = new Char('V');
    public static final Char P_VERTICAL_REL = new Char('v');
    public static final Char P_HORIZONTAL_ABS = new Char('H');
    public static final Char P_HORIZONTAL_REL = new Char('h');
    public static final Char P_CURVETO_ABS = new Char('C');
    public static final Char P_CURVETO_REL = new Char('c');
    public static final Char P_SCURVETO_ABS = new Char('S');
    public static final Char P_SCURVETO_REL = new Char('s');
    public static final Char P_CLOSE_ABS = new Char('Z');
    public static final Char P_CLOSE_REL = new Char('z');
    public static final Char P_ARC_ABS = new Char('A');
    public static final Char P_ARC_REL = new Char('a');

    public SVGGraphic(FName name) {
        super(name);
    }

    public SVGGraphic(DomElement base) {
        super(base);
    }

    public abstract PlanarGeometry asGeometry();

    /**
     * The fill geometry may be slightly different since it may contain
     * an additional close iteration.
     *
     * SVG 11.3 :
     * The fill operation fills open subpaths by performing the fill operation
     * as if an additional "closepath" command were added to the path to connect
     * the last point of the subpath with the first point of the subpath.
     * Thus, fill operations apply to both open subpaths within ‘path’ elements
     * (i.e., subpaths without a closepath command) and ‘polyline’ elements.
     */
    public PlanarGeometry getFillGeometry(){
        final PlanarGeometry geometry = asGeometry();
        if (geometry instanceof Curve){
            final Path p = new Path();
            final PathIterator ite = geometry.createPathIterator();
            boolean closed = false;
            while (ite.next()){
                p.append(new PathStep2D(ite));
                closed = ite.getType() == PathIterator.TYPE_CLOSE;
            }
            if (!closed) p.appendClose();
            return p;
        }
        //same as original geometry
        return geometry;
    }

    public RenderState buildRenderState(){
        final RenderState state = new RenderState();
        final SVGElement parent = getParent();
        if (parent instanceof SVGGraphic){
            //inherit parent style
            final RenderState parentState = ((SVGGraphic) parent).buildRenderState();
            state.fillPaint = parentState.fillPaint;
            state.strokePaint = parentState.strokePaint;
            state.strokeBrush = parentState.strokeBrush;
        }

        final CSSProperties properties = getStyle();

        state.strokeGeometry = asGeometry();
        state.fillGeometry = getFillGeometry();

        //fill with attributes
        final Color fillColor = getFill();
        if (fillColor==NOCOLOR) state.fillPaint = null;
        else if (fillColor!=null) state.fillPaint = new ColorPaint(fillColor);
        else if (state.fillPaint==null) state.fillPaint = new ColorPaint(Color.BLACK);//default

        //overload with style definition
        final Color fillStyleColor = properties.getColor(PROPERTY_FILL);
        if (fillStyleColor==NOCOLOR) state.fillPaint = null;
        else if (fillStyleColor!=null) state.fillPaint = new ColorPaint(fillStyleColor);
        else if (state.fillPaint==null) state.fillPaint = new ColorPaint(Color.BLACK);//default


        final Color strokeColor = getStroke();
        if (strokeColor==NOCOLOR) state.strokePaint = null;
        else if (strokeColor!=null) state.strokePaint = new ColorPaint(strokeColor);
        else if (state.strokePaint==null) state.strokePaint = new ColorPaint(Color.BLACK);//default

        state.strokeBrush = new PlainBrush(getStrokeWidth().floatValue(), lineCapCode(getStrokeLinecap()));

        //overload with style definition
        final Color strokeStyleColor = properties.getColor(PROPERTY_STROKE);
        if (strokeStyleColor==NOCOLOR) state.strokePaint = null;
        else if (strokeStyleColor!=null) state.strokePaint = new ColorPaint(strokeStyleColor);
        else if (state.strokePaint==null) state.strokePaint = new ColorPaint(Color.BLACK);//default

        return state;
    }

    private static int lineCapCode(Chars buffer){
        if (CAP_ROUND.equals(buffer, true, true)){
            return 2;
        } else if (CAP_SQUARE.equals(buffer, true, true)){
            return 3;
        } else {
            //set to default value : butt
            return 1;
        }
    }


    protected static TupleGrid1D readCoordinates(Chars cs){
        final CharIterator ite = cs.trim().createIterator();
        final DoubleSequence ds = new DoubleSequence();

        int state = 0;
        double x = 0;
        double y = 0;
        while (ite.hasNext()){
            if ( ite.nextEquals(P_SEP1)||ite.nextEquals(P_SEP2)
              ||ite.nextEquals(P_SEP3)||ite.nextEquals(P_SEP4)){
                ite.skip();
                continue;
            }
            if (state==0){
                x = readNumber(ite);
                state=1;
            } else if (state==1){
                y = readNumber(ite);
                state=0;
                ds.put(x).put(y);
            }
        }
        return InterleavedTupleGrid1D.create(ds.toArrayDouble(), 2);
    }

    protected static double readNumber(CharIterator ite){
        while (ite.hasNext()){
            if ( ite.nextEquals(P_SEP1)||ite.nextEquals(P_SEP3)||ite.nextEquals(P_SEP4)){
                ite.skip();
            } else {
                break;
            }
        }

        double d = Float64.decode(ite, false);
        //skip any upcoming separator
        while (ite.hasNext()){
            if ( ite.nextEquals(P_SEP1)|| ite.nextEquals(P_SEP2)|| ite.nextEquals(P_SEP3)||ite.nextEquals(P_SEP4)){
                ite.skip();
            } else {
                break;
            }
        }
        return d;
    }



    // ISVGStylable ////////////////////////////////////////////////////////////
    public Chars getStyleClass() {
        return super.getStyleClass();
    }

    public CSSProperties getStyle() {
        return super.getStyle();
    }

    public Chars getAlignmentBaseline() {
        return getPropertyChars(PROPERTY_ALIGNMENT_BASELINE);
    }

    public void setAlignmentBaseline(Chars value) {
        getProperties().add(PROPERTY_ALIGNMENT_BASELINE, value);
    }

    public Chars getBaselineShift() {
        return getPropertyChars(PROPERTY_BASELINE_SHIFT);
    }

    public void setBaselineShift(Chars value) {
        getProperties().add(PROPERTY_BASELINE_SHIFT, value);
    }

    public Chars getClip() {
        return getPropertyChars(PROPERTY_CLIP);
    }

    public void setClip(Chars value) {
        getProperties().add(PROPERTY_CLIP, value);
    }

    public Chars getClipPath() {
        return getPropertyChars(PROPERTY_CLIP_PATH);
    }

    public void setClipPath(Chars value) {
        getProperties().add(PROPERTY_CLIP_PATH, value);
    }

    public Chars getClipRule() {
        return getPropertyChars(PROPERTY_CLIP_RULE);
    }

    public void setClipRule(Chars value) {
        getProperties().add(PROPERTY_CLIP_RULE, value);
    }

    public Color getColor() {
        return getPropertyColor(PROPERTY_COLOR);
    }

    public void setColor(Color value) {
        getProperties().add(PROPERTY_COLOR, value);
    }

    public Chars getColorInterpolation() {
        return getPropertyChars(PROPERTY_COLOR_INTERPOLATION);
    }

    public void setColorInterpolation(Chars value) {
        getProperties().add(PROPERTY_COLOR_INTERPOLATION, value);
    }

    public Chars getColorInterpolationFilters() {
        return getPropertyChars(PROPERTY_COLOR_INTERPOLATION_FILTERS);
    }

    public void setColorInterpolationFilters(Chars value) {
        getProperties().add(PROPERTY_COLOR_INTERPOLATION_FILTERS, value);
    }

    public Chars getColorProfile() {
        return getPropertyChars(PROPERTY_COLOR_PROFILE);
    }

    public void setColorProfile(Chars value) {
        getProperties().add(PROPERTY_COLOR_PROFILE, value);
    }

    public Chars getColorRendering() {
        return getPropertyChars(PROPERTY_COLOR_RENDERING);
    }

    public void setColorRendering(Chars value) {
        getProperties().add(PROPERTY_COLOR_RENDERING, value);
    }

    public Chars getCursor() {
        return getPropertyChars(PROPERTY_CURSOR);
    }

    public void setCursor(Chars value) {
        getProperties().add(PROPERTY_CURSOR, value);
    }

    public Chars getDirection() {
        return getPropertyChars(PROPERTY_DIRECTION);
    }

    public void setDirection(Chars value) {
        getProperties().add(PROPERTY_DIRECTION, value);
    }

    public Chars getDisplay() {
        return getPropertyChars(PROPERTY_DISPLAY);
    }

    public void setDisplay(Chars value) {
        getProperties().add(PROPERTY_DISPLAY, value);
    }

    public Chars getDominantBaseline() {
        return getPropertyChars(PROPERTY_DOMINANT_BASELINE);
    }

    public void setDominantBaseline(Chars value) {
        getProperties().add(PROPERTY_DOMINANT_BASELINE, value);
    }

    public Chars getEnableBackground() {
        return getPropertyChars(PROPERTY_ENABLE_BACKGROUND);
    }

    public void setEnableBackground(Chars value) {
        getProperties().add(PROPERTY_ENABLE_BACKGROUND, value);
    }

    public Color getFill() {
        return getPropertyColor(PROPERTY_FILL);
    }

    public void setFill(Color value) {
        getProperties().add(PROPERTY_FILL, value);
    }

    public Chars getFillOpacity() {
        return getPropertyChars(PROPERTY_FILL_OPACITY);
    }

    public void setFillOpacity(Chars value) {
        getProperties().add(PROPERTY_FILL_OPACITY, value);
    }

    public Chars getfillRule() {
        return getPropertyChars(PROPERTY_FILL_RULE);
    }

    public void setfillRule(Chars value) {
        getProperties().add(PROPERTY_FILL_RULE, value);
    }

    public Chars getFilter() {
        return getPropertyChars(PROPERTY_FILTER);
    }

    public void setFilter(Chars value) {
        getProperties().add(PROPERTY_FILTER, value);
    }

    public Color getFloodColor() {
        return getPropertyColor(PROPERTY_FLOOD_COLOR);
    }

    public void setFloodColor(Color value) {
        getProperties().add(PROPERTY_FLOOD_COLOR, value);
    }

    public Chars getFloodOpacity() {
        return getPropertyChars(PROPERTY_FLOOD_OPACITY);
    }

    public void setFloodOpacity(Chars value) {
        getProperties().add(PROPERTY_FLOOD_OPACITY, value);
    }

    public Chars getFontFamily() {
        return getPropertyChars(PROPERTY_FONT_FAMILY);
    }

    public void setFontFamily(Chars value) {
        getProperties().add(PROPERTY_FONT_FAMILY, value);
    }

    public Chars getFontSize() {
        return getPropertyChars(PROPERTY_FONT_SIZE);
    }

    public void setFontSize(Chars value) {
        getProperties().add(PROPERTY_FONT_SIZE, value);
    }

    public Chars getFontSizeAdjust() {
        return getPropertyChars(PROPERTY_FONT_SIZE_ADJUST);
    }

    public void setFontSizeAdjust(Chars value) {
        getProperties().add(PROPERTY_FONT_SIZE_ADJUST, value);
    }

    public Chars getFontStretch() {
        return getPropertyChars(PROPERTY_FONT_STRETCH);
    }

    public void setFontStretch(Chars value) {
        getProperties().add(PROPERTY_FONT_STRETCH, value);
    }

    public Chars getFontStyle() {
        return getPropertyChars(PROPERTY_FONT_STYLE);
    }

    public void setFontStyle(Chars value) {
        getProperties().add(PROPERTY_FONT_STYLE, value);
    }

    public Chars getFontVariant() {
        return getPropertyChars(PROPERTY_FONT_VARIANT);
    }

    public void setFontVariant(Chars value) {
        getProperties().add(PROPERTY_FONT_VARIANT, value);
    }

    public Chars getFontWeight() {
        return getPropertyChars(PROPERTY_FONT_WEIGHT);
    }

    public void setFontWeight(Chars value) {
        getProperties().add(PROPERTY_FONT_WEIGHT, value);
    }

    public Chars getGlyphOrientationHorizontal() {
        return getPropertyChars(PROPERTY_GLYPH_ORIENTATION_HORIZONTAL);
    }

    public void setGlyphOrientationHorizontal(Chars value) {
        getProperties().add(PROPERTY_GLYPH_ORIENTATION_HORIZONTAL, value);
    }

    public Chars getGlyphOrientationVertical() {
        return getPropertyChars(PROPERTY_GLYPH_ORIENTATION_VERTICAL);
    }

    public void setGlyphOrientationVertical(Chars value) {
        getProperties().add(PROPERTY_GLYPH_ORIENTATION_VERTICAL, value);
    }

    public Chars getImageRendering() {
        return getPropertyChars(PROPERTY_IMAGE_RENDERING);
    }

    public void setImageRendering(Chars value) {
        getProperties().add(PROPERTY_IMAGE_RENDERING, value);
    }

    public Chars getKerning() {
        return getPropertyChars(PROPERTY_KERNING);
    }

    public void setKerning(Chars value) {
        getProperties().add(PROPERTY_KERNING, value);
    }

    public Chars getLetterSpacing() {
        return getPropertyChars(PROPERTY_LETTER_SPACING);
    }

    public void setLetterSpacing(Chars value) {
        getProperties().add(PROPERTY_LETTER_SPACING, value);
    }

    public Color getLightingColor() {
        return getPropertyColor(PROPERTY_LIGHTNING_COLOR);
    }

    public void setLightingColor(Color value) {
        getProperties().add(PROPERTY_LIGHTNING_COLOR, value);
    }

    public Chars getMarkerEnd() {
        return getPropertyChars(PROPERTY_MARKER_END);
    }

    public void setMarkerEnd(Chars value) {
        getProperties().add(PROPERTY_MARKER_END, value);
    }

    public Chars getMarkerMid() {
        return getPropertyChars(PROPERTY_MARKER_MID);
    }

    public void setMarkerMid(Chars value) {
        getProperties().add(PROPERTY_MARKER_MID, value);
    }

    public Chars getMarkerStart() {
        return getPropertyChars(PROPERTY_MARKER_START);
    }

    public void setMarkerStart(Chars value) {
        getProperties().add(PROPERTY_MARKER_START, value);
    }

    public Chars getMask() {
        return getPropertyChars(PROPERTY_MASK);
    }

    public void setMask(Chars value) {
        getProperties().add(PROPERTY_MASK, value);
    }

    public Chars getOpacity() {
        return getPropertyChars(PROPERTY_OPACITY);
    }

    public void setOpacity(Chars value) {
        getProperties().add(PROPERTY_OPACITY, value);
    }

    public Chars getOverflow() {
        return getPropertyChars(PROPERTY_OVERFLOW);
    }

    public void setOverflow(Chars value) {
        getProperties().add(PROPERTY_OVERFLOW, value);
    }

    public Chars getPointerEvents() {
        return getPropertyChars(PROPERTY_POINTER_EVENTS);
    }

    public void setPointerEvents(Chars value) {
        getProperties().add(PROPERTY_POINTER_EVENTS, value);
    }

    public Chars getShapeRendering() {
        return getPropertyChars(PROPERTY_SHAPE_RENDERING);
    }

    public void setShapeRendering(Chars value) {
        getProperties().add(PROPERTY_SHAPE_RENDERING, value);
    }

    public Color getStopColor() {
        return getPropertyColor(PROPERTY_STOP_COLOR);
    }

    public void setStopColor(Color value) {
        getProperties().add(PROPERTY_STOP_COLOR, value);
    }

    public Chars getStopOpacity() {
        return getPropertyChars(PROPERTY_STOP_OPACITY);
    }

    public void setStopOpacity(Chars value) {
        getProperties().add(PROPERTY_STOP_OPACITY, value);
    }

    public Color getStroke() {
        return getPropertyColor(PROPERTY_STROKE);
    }

    public void setStroke(Color value) {
        getProperties().add(PROPERTY_STROKE, value);
    }

    public Chars getStrokeDasharray() {
        return getPropertyChars(PROPERTY_STROKE_DASHARRAY);
    }

    public void setStrokeDasharray(Chars value) {
        getProperties().add(PROPERTY_STROKE_DASHARRAY, value);
    }

    public Chars getStrokeDashoffset() {
        return getPropertyChars(PROPERTY_STROKE_DASHOFFSET);
    }

    public void setStrokeDashoffset(Chars value) {
        getProperties().add(PROPERTY_STROKE_DASHOFFSET, value);
    }

    public Chars getStrokeLinecap() {
        return getPropertyChars(PROPERTY_STROKE_LINECAP);
    }

    public void setStrokeLinecap(Chars value) {
        getProperties().add(PROPERTY_STROKE_LINECAP, value);
    }

    public Chars getStrokeLinejoin() {
        return getPropertyChars(PROPERTY_STROKE_LINEJOIN);
    }

    public void setStrokeLinejoin(Chars value) {
        getProperties().add(PROPERTY_STROKE_LINEJOIN, value);
    }

    public Chars getStrokeMiterlimit() {
        return getPropertyChars(PROPERTY_STROKE_MITERLIMIT);
    }

    public void setStrokeMiterlimit(Chars value) {
        getProperties().add(PROPERTY_STROKE_MITERLIMIT, value);
    }

    public Chars getStrokeOpacity() {
        return getPropertyChars(PROPERTY_STROKE_OPACITY);
    }

    public void setStrokeOpacity(Chars value) {
        getProperties().add(PROPERTY_STROKE_OPACITY, value);
    }

    public Double getStrokeWidth() {
        Chars buffer = getPropertyChars(PROPERTY_STROKE_WIDTH);
        if (buffer != null){
            return Float64.decode(buffer);
        } else {
            return 1.0;
        }
    }

    public void setStrokeWidth(Double value) {
        getProperties().add(PROPERTY_STROKE_WIDTH, value);
    }

    public Chars getTextAnchor() {
        return getPropertyChars(PROPERTY_TEXT_ANCHOR);
    }

    public void setTextAnchor(Chars value) {
        getProperties().add(PROPERTY_TEXT_ANCHOR, value);
    }

    public Chars getTextDecoration() {
        return getPropertyChars(PROPERTY_TEXT_DECORATION);
    }

    public void setTextDecoration(Chars value) {
        getProperties().add(PROPERTY_TEXT_DECORATION, value);
    }

    public Chars getTextRendering() {
        return getPropertyChars(PROPERTY_TEXT_RENDERING);
    }

    public void setTextRendering(Chars value) {
        getProperties().add(PROPERTY_TEXT_RENDERING, value);
    }

    public Chars getUnicodeBidi() {
        return getPropertyChars(PROPERTY_UNICODE_BIDI);
    }

    public void setUnicodeBidi(Chars value) {
        getProperties().add(PROPERTY_UNICODE_BIDI, value);
    }

    public Chars getVisibility() {
        return getPropertyChars(PROPERTY_VISIBILITY);
    }

    public void setVisibility(Chars value) {
        getProperties().add(PROPERTY_VISIBILITY, value);
    }

    public Chars getWordSpacing() {
        return getPropertyChars(PROPERTY_WORD_SPACING);
    }

    public void setWordSpacing(Chars value) {
        getProperties().add(PROPERTY_WORD_SPACING, value);
    }

    public Chars getWritingMode() {
        return getPropertyChars(PROPERTY_WRITING_MODE);
    }

    public void setWritingMode(Chars value) {
        getProperties().add(PROPERTY_WRITING_MODE, value);
    }

    // ISVGSTransform //////////////////////////////////////////////////////////

    public Matrix3x3 getTransform() {
        final Chars txt = getPropertyChars(PROPERTY_TRANSFORM);
        if (txt==null || txt.isEmpty()) return null;
        final Chars[] parts = txt.split(')');
        final Matrix3x3 trs = new Matrix3x3().setToIdentity();
        for (Chars op : parts){
            concateTransformOp(op, trs);
        }
        return trs;
    }

    private void concateTransformOp(Chars op, Matrix3x3 base){
        final Chars[] opParts = op.split('(');
        final Chars name = opParts[0].trim();
        final Chars[] params = opParts[1].split(',');
        final double[] dps = new double[params.length];
        for (int i=0;i<dps.length;i++){
            dps[i] = Float64.decode(params[i].trim());
        }

        if (TRANSFORM_MATRIX.equals(name)){
            Matrix3x3 m = new Matrix3x3(dps[0], dps[2], dps[4],
                            dps[1], dps[3], dps[5],
                            0, 0, 1);
            base.localMultiply(m);
        } else if (TRANSFORM_TRANSLATE.equals(name)){
            Matrix3x3 m = new Matrix3x3(1, 0, dps[0],
                            0, 1, dps[1],
                            0, 0, 1);
            base.localMultiply(m);
        } else if (TRANSFORM_ROTATE.equals(name)){
            if (dps.length==1){
                Matrix3x3 m = new Matrix3x3(Math.cos(dps[0]), -Math.sin(dps[0]), 0,
                                Math.sin(dps[0]), Math.cos(dps[0]), 0,
                                0, 0, 1);
                base.localMultiply(m);
            } else {
                concateTransformOp(new Chars("translate("+dps[1]+","+dps[2]), base);
                concateTransformOp(new Chars("rotate("+dps[0]), base);
                concateTransformOp(new Chars("translate("+(-dps[1])+","+(-dps[2])), base);
            }

        } else if (TRANSFORM_SCALE.equals(name)){
            Matrix3x3 m = new Matrix3x3(dps[0], 0,      0,
                            0,      dps[1], 0,
                            0,      0,      1);
            base.localMultiply(m);
        } else if (TRANSFORM_SKEWX.equals(name)){
            Matrix3x3 m = new Matrix3x3(1, Math.tan(dps[0]), 0,
                            0, 1, 0,
                            0, 0, 1);
            base.localMultiply(m);
        } else if (TRANSFORM_SKEWY.equals(name)){
            Matrix3x3 m = new Matrix3x3(1, 0, 0,
                            Math.tan(dps[0]), 1, 0,
                            0, 0, 1);
            base.localMultiply(m);
        } else {
            //unvalid op
            throw new RuntimeException("Unvalid transform name : "+name);
        }
    }

    public void setTransform(Matrix3x3 trs) {
        if (trs==null){
            getProperties().add(PROPERTY_TRANSFORM, null);
        } else {
            getProperties().add(PROPERTY_TRANSFORM, new Chars("matrix("+
                    trs.get(0,0)+","+trs.get(1,0)+","+
                    trs.get(0,1)+","+trs.get(1,1)+","+
                    trs.get(0,2)+","+trs.get(1,2)+")"));
        }
    }

    public BBox getViewBox() {
        Chars text = getPropertyChars(PROPERTY_VIEWBOX);
        if (text==null) return null;
        text = text.replaceAll(',', ' ');
        final Chars[] parts = text.split(' ');
        final double minx = Float64.decode(parts[0]);
        final double miny = Float64.decode(parts[1]);
        final double width = Float64.decode(parts[2]);
        final double height = Float64.decode(parts[3]);
        return new BBox(new double[]{minx,miny}, new double[]{minx+width,miny+width});
    }

    public void setViewBox(BBox bbox) {
        if (bbox==null){
            getProperties().add(PROPERTY_VIEWBOX, null);
        } else {
            getProperties().add(PROPERTY_VIEWBOX, new Chars(""+bbox.getMin(0)+" "+bbox.getMin(1)+" "+bbox.getSpan(0)+" "+bbox.getSpan(1)));
        }
    }




}
