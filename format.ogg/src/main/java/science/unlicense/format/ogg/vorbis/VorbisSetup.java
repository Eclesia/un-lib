
package science.unlicense.format.ogg.vorbis;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * The setup header contains the bulk of the codec setup information
 * needed for decode. The setup header contains, in order, the lists of codebook
 * configurations, time-domain transform configurations (placeholders in Vorbis I),
 * floor configurations, residue configurations, channel mapping configurations and mode configurations.
 *
 * @author Johann Sorel
 */
public class VorbisSetup extends VorbisPacket{

    /** [framing_flag] = read one bit */
    public boolean framingFlag;

    public VorbisCodeBook[] codeBooks;

    public VorbisSetup() {
        super(VorbisConstants.TYPE_SETUP);
    }

    public void read(DataInputStream ds) throws IOException{
        //4. Codec Setup and Packet Decode

        //Codebooks
        final int vorbis_codebook_count = ds.readUByte()+1;
        codeBooks = new VorbisCodeBook[vorbis_codebook_count];
        for (int i=0;i<vorbis_codebook_count;i++){
            codeBooks[i] = new VorbisCodeBook();
            codeBooks[i].read(ds);
        }

        //Time domain transform
        final int vorbis_time_count = ds.readBits(6,DataInputStream.LSB);

        //TODO

        //Floors
        //TODO

        //Residues
        //TODO

        //Mappings
        //TODO

        //Modes
        //TODO

        framingFlag = ds.readBits(1) != 0;
        //skip to end of byte
        ds.readBits(7);


        if (!framingFlag) throw new IOException(ds, "Invalid frame flag value, should be 1");
    }



}
