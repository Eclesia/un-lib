
package science.unlicense.format.ogg;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;

/**
 *
 * @author Johann Sorel
 */
public class OGGStore extends AbstractStore implements Media {

    private final Path path;


    public OGGStore(Path input) {
        super(OGGFormat.INSTANCE,input);
        this.path = input;
    }

    @Override
    public Track[] getTracks() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnsupportedOperationException("Not supported.");
    }


}
