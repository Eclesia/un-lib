package science.unlicense.format.ogg;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.EOSException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.Maths;

/**
 * http://xiph.org/ogg/doc/framing.html
 *
 * @author Johann Sorel
 */
public class OGGReader extends AbstractReader{

    /**
     * Read next OGG page.
     *
     * @return next page, or null if none remaining
     * @throws IOException
     */
    public OGGPage read() throws IOException{

        final DataInputStream ds = getInputAsDataStream(Endianness.LITTLE_ENDIAN);


        final byte[] signature;
        try {
            signature = ds.readFully(new byte[4]);
        } catch(EOSException ex) {
            return null;
        }
        if (!Arrays.equals(OGGConstants.SIGNATURE,signature)){
            throw new IOException(ds, "Not an OGG stream");
        }

        final OGGPageHeader header = new OGGPageHeader();
        header.read(ds);

        final int dataSize = Maths.sum(header.segment_table);

        final OGGPage page = new OGGPage();
        page.header = header;
        page.datas = ds.readBytes(dataSize);

        return page;
    }

}
