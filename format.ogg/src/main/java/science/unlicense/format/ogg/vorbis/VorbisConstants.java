
package science.unlicense.format.ogg.vorbis;

/**
 *
 * @author Johann Sorel
 */
public final class VorbisConstants {

    public static final byte TYPE_AUDIO = 0;
    public static final byte TYPE_IDENTIFICATION = 1;
    public static final byte TYPE_COMMENTS = 3;
    public static final byte TYPE_SETUP = 5;

    /**
     * OGG Vorbis signature, placed after packet type.
     */
    public static final byte[] SIGNATURE = new byte[]{'v','o','r','b','i','s'};

    /** Possible audio block size */
    public static final int[] BLOCK_SIZES = new int[]{64,128,256,512,1024,2048,4096,8192};

    /**
     * A codebook begins with a 24 bit sync pattern, 0x564342
     */
    public static final int CODEBOOK_SYNC = 0x564342;

    private VorbisConstants(){}

}
