
package science.unlicense.format.ogg;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaCapabilities;

/**
 *
 * @author Johann Sorel
 */
public class OGGFormat extends AbstractMediaFormat {

    public static final OGGFormat INSTANCE = new OGGFormat();

    public OGGFormat() {
        super(new Chars("ogg"));
        shortName = new Chars("OGG");
        longName = new Chars("OGG Vorbis");
        mimeTypes.add(new Chars("video/ogg"));
        mimeTypes.add(new Chars("audio/ogg"));
        mimeTypes.add(new Chars("application/ogg"));
        extensions.add(new Chars("ogg"));
        extensions.add(new Chars("ogv"));
        extensions.add(new Chars("oga"));
        extensions.add(new Chars("ogx"));
        extensions.add(new Chars("spx"));
        extensions.add(new Chars("opus"));
        resourceTypes.add(Media.class);
    }

    @Override
    public MediaCapabilities getCapabilities() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object input) {
        return new OGGStore((Path) input);
    }

}
