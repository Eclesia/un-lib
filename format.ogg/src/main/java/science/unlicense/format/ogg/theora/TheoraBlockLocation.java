
package science.unlicense.format.ogg.theora;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class TheoraBlockLocation extends CObject {

    public static enum Type{
        Y(0),
        CB(1),
        CR(2);

        public final int planeIndex;

        private Type(int planeIndex) {
            this.planeIndex = planeIndex;
        }
    }

    public int rasterIndex;
    public int codedIndex;
    public Type type;
    public int superBlock;
    public TheoraMacroBlockLocation macroBlock;
    //in frame position X/Y, in block units
    public int x;
    public int y;

    //left, right, top, down blocks
    public TheoraBlockLocation left;
    public TheoraBlockLocation right;
    public TheoraBlockLocation top;
    public TheoraBlockLocation down;

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(type.name());
        cb.append(" ri:"+rasterIndex);
        cb.append(" ci:"+codedIndex);
        cb.append(" X/Y:"+x+"/"+y);
        cb.append(" L:"+left!=null);
        cb.append(" R:"+right!=null);
        cb.append(" T:"+top!=null);
        cb.append(" D:"+down!=null);
        return cb.toChars();
    }

}
