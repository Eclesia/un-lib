
package science.unlicense.format.ogg.theora;

import science.unlicense.common.api.number.Numbers;
import science.unlicense.encoding.impl.io.huffman.HuffmanNode;
import science.unlicense.encoding.impl.io.huffman.HuffmanTree;

/**
 *
 * @author Johann Sorel
 */
public final class TheoraConstants {

    public static final int COLORSPACE_470M = 1;
    public static final int COLORSPACE_470BG = 2;

    public static final int PIXELFORMAT_420 = 0;
    public static final int PIXELFORMAT_422 = 2;
    public static final int PIXELFORMAT_444 = 3;

    static final byte[] HEADER_SIGNATURE = new byte[]{'t','h','e','o','r','a'};

    /**
     * huffman tree for table 7.7, page 83
     */
    static final HuffmanTree T7_7_LONGRUN_TREE = new HuffmanTree(new HuffmanNode[]{
         new HuffmanNode(1, Numbers.reverseBits(0b0,     1),   new int[]{ 1, 0}),
         new HuffmanNode(2, Numbers.reverseBits(0b10,    2),   new int[]{ 2, 1}),
         new HuffmanNode(3, Numbers.reverseBits(0b110,   3),   new int[]{ 4, 1}),
         new HuffmanNode(4, Numbers.reverseBits(0b1110,  4),   new int[]{ 6, 2}),
         new HuffmanNode(5, Numbers.reverseBits(0b11110, 5),   new int[]{10, 3}),
         new HuffmanNode(6, Numbers.reverseBits(0b111110,6),   new int[]{18, 4}),
         new HuffmanNode(6, Numbers.reverseBits(0b111111,6),   new int[]{34,12}),
    });


    /**
     * huffman tree for table 7.11, page 84
     */
    static final HuffmanTree T7_11_SHORTRUN_RTEE = new HuffmanTree(new HuffmanNode[]{
         new HuffmanNode(1, Numbers.reverseBits(0b0,    1),    new int[]{ 1, 1}),
         new HuffmanNode(2, Numbers.reverseBits(0b10,   2),    new int[]{ 3, 1}),
         new HuffmanNode(3, Numbers.reverseBits(0b110,  3),    new int[]{ 5, 1}),
         new HuffmanNode(4, Numbers.reverseBits(0b1110, 4),    new int[]{ 7, 2}),
         new HuffmanNode(5, Numbers.reverseBits(0b11110,5),    new int[]{11, 2}),
         new HuffmanNode(5, Numbers.reverseBits(0b11111,5),    new int[]{15, 4}),
    });

    /**
     * huffman tree for table 7.19, page 89
     */
    static final HuffmanTree T7_19 = new HuffmanTree(new HuffmanNode[]{   //mi, coding mode
         new HuffmanNode(1, Numbers.reverseBits(0b0,      1),    0),
         new HuffmanNode(2, Numbers.reverseBits(0b10,     2),    1),
         new HuffmanNode(3, Numbers.reverseBits(0b110,    3),    2),
         new HuffmanNode(4, Numbers.reverseBits(0b1110,   4),    3),
         new HuffmanNode(5, Numbers.reverseBits(0b11110,  5),    4),
         new HuffmanNode(6, Numbers.reverseBits(0b111110, 6),    5),
         new HuffmanNode(7, Numbers.reverseBits(0b1111110,7),    6),
         new HuffmanNode(7, Numbers.reverseBits(0b1111111,7),    7),
    });

    /**
     * huffman tree for table 7.23, page 99
     */
    static final HuffmanTree T7_23_MOTIONVECTOR_TREE = new HuffmanTree(new HuffmanNode[]{
        new HuffmanNode(3, Numbers.reverseBits(0b000,     3),   0),
        new HuffmanNode(3, Numbers.reverseBits(0b001,     3),   1),
        new HuffmanNode(4, Numbers.reverseBits(0b0110,    4),   2),
        new HuffmanNode(4, Numbers.reverseBits(0b1000,    4),   3),
        new HuffmanNode(6, Numbers.reverseBits(0b101000,  6),   4),
        new HuffmanNode(6, Numbers.reverseBits(0b101010,  6),   5),
        new HuffmanNode(6, Numbers.reverseBits(0b101100,  6),   6),
        new HuffmanNode(6, Numbers.reverseBits(0b101110,  6),   7),
        new HuffmanNode(7, Numbers.reverseBits(0b1100000, 7),   8),
        new HuffmanNode(7, Numbers.reverseBits(0b1100010, 7),   9),
        new HuffmanNode(7, Numbers.reverseBits(0b1100100, 7),  10),
        new HuffmanNode(7, Numbers.reverseBits(0b1100110, 7),  11),
        new HuffmanNode(7, Numbers.reverseBits(0b1101000, 7),  12),
        new HuffmanNode(7, Numbers.reverseBits(0b1101010, 7),  13),
        new HuffmanNode(7, Numbers.reverseBits(0b1101100, 7),  14),
        new HuffmanNode(7, Numbers.reverseBits(0b1101110, 7),  15),
        new HuffmanNode(8, Numbers.reverseBits(0b11100000,8),  16),
        new HuffmanNode(8, Numbers.reverseBits(0b11100010,8),  17),
        new HuffmanNode(8, Numbers.reverseBits(0b11100100,8),  18),
        new HuffmanNode(8, Numbers.reverseBits(0b11100110,8),  19),
        new HuffmanNode(8, Numbers.reverseBits(0b11101000,8),  20),
        new HuffmanNode(8, Numbers.reverseBits(0b11101010,8),  21),
        new HuffmanNode(8, Numbers.reverseBits(0b11101100,8),  22),
        new HuffmanNode(8, Numbers.reverseBits(0b11101110,8),  23),
        new HuffmanNode(8, Numbers.reverseBits(0b11110000,8),  24),
        new HuffmanNode(8, Numbers.reverseBits(0b11110010,8),  25),
        new HuffmanNode(8, Numbers.reverseBits(0b11110100,8),  26),
        new HuffmanNode(8, Numbers.reverseBits(0b11110110,8),  27),
        new HuffmanNode(8, Numbers.reverseBits(0b11111000,8),  28),
        new HuffmanNode(8, Numbers.reverseBits(0b11111010,8),  29),
        new HuffmanNode(8, Numbers.reverseBits(0b11111100,8),  30),
        new HuffmanNode(8, Numbers.reverseBits(0b11111110,8),  31),
        new HuffmanNode(3, Numbers.reverseBits(0b010,     3),  -1),
        new HuffmanNode(4, Numbers.reverseBits(0b0111,    4),  -2),
        new HuffmanNode(4, Numbers.reverseBits(0b1001,    4),  -3),
        new HuffmanNode(6, Numbers.reverseBits(0b101001,  6),  -4),
        new HuffmanNode(6, Numbers.reverseBits(0b101011,  6),  -5),
        new HuffmanNode(6, Numbers.reverseBits(0b101101,  6),  -6),
        new HuffmanNode(6, Numbers.reverseBits(0b101111,  6),  -7),
        new HuffmanNode(7, Numbers.reverseBits(0b1100001, 7),  -8),
        new HuffmanNode(7, Numbers.reverseBits(0b1100011, 7),  -9),
        new HuffmanNode(7, Numbers.reverseBits(0b1100101, 7),  -10),
        new HuffmanNode(7, Numbers.reverseBits(0b1100111, 7),  -11),
        new HuffmanNode(7, Numbers.reverseBits(0b1101001, 7),  -12),
        new HuffmanNode(7, Numbers.reverseBits(0b1101011, 7),  -13),
        new HuffmanNode(7, Numbers.reverseBits(0b1101101, 7),  -14),
        new HuffmanNode(7, Numbers.reverseBits(0b1101111, 7),  -15),
        new HuffmanNode(8, Numbers.reverseBits(0b11100001,8),  -16),
        new HuffmanNode(8, Numbers.reverseBits(0b11100011,8),  -17),
        new HuffmanNode(8, Numbers.reverseBits(0b11100101,8),  -18),
        new HuffmanNode(8, Numbers.reverseBits(0b11100111,8),  -19),
        new HuffmanNode(8, Numbers.reverseBits(0b11101001,8),  -20),
        new HuffmanNode(8, Numbers.reverseBits(0b11101011,8),  -21),
        new HuffmanNode(8, Numbers.reverseBits(0b11101101,8),  -22),
        new HuffmanNode(8, Numbers.reverseBits(0b11101111,8),  -23),
        new HuffmanNode(8, Numbers.reverseBits(0b11110001,8),  -24),
        new HuffmanNode(8, Numbers.reverseBits(0b11110011,8),  -25),
        new HuffmanNode(8, Numbers.reverseBits(0b11110101,8),  -26),
        new HuffmanNode(8, Numbers.reverseBits(0b11110111,8),  -27),
        new HuffmanNode(8, Numbers.reverseBits(0b11111001,8),  -28),
        new HuffmanNode(8, Numbers.reverseBits(0b11111011,8),  -29),
        new HuffmanNode(8, Numbers.reverseBits(0b11111101,8),  -30),
        new HuffmanNode(8, Numbers.reverseBits(0b11111111,8),  -31)
    });


    static final int[][] T2_8 = new int[][]{
        { 0, 1, 5, 6,14,15,27,28},
        { 2, 4, 7,13,16,26,29,42},
        { 3, 8,12,17,25,30,41,43},
        { 9,11,18,24,31,40,44,53},
        {10,19,23,32,39,45,52,54},
        {20,22,33,38,46,51,55,60},
        {21,34,37,47,50,56,59,61},
        {35,36,48,49,57,58,62,63}
    };

    static final int[] T7_46 = new int[]{
        1, // Previous
        0, // None
        1, // Previous
        1, // Previous
        1, // Previous
        2, // Golden
        2, // Golden
        1  // Previous
    };

    interface CodingMode {
        static final int INTER_NOMV        = 0;
        static final int INTRA             = 1;
        static final int INTER_MV          = 2;
        static final int INTER_MV_LAST     = 3;
        static final int INTER_MV_LAST2    = 4;
        static final int INTER_GOLDEN_NOMV = 5;
        static final int INTER_GOLDEN_MV   = 6;
        static final int INTER_MV_FOUR     = 7;
    }

    private TheoraConstants(){}

}
