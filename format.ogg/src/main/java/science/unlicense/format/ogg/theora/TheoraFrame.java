
package science.unlicense.format.ogg.theora;

/**
 * Store all values needed to decode a frame and store the result.
 *
 * @author Johann Sorel
 */
class TheoraFrame {

    public int RPYW;
    public int RPYH;
    public int RPCW;
    public int RPCH;

    // Spec : 7.1 Frame Header Decode, page 63
    public int FTYPE;
    public int NQIS;
    public int[] QIS;

    // Spec : 7.3 Coded Block Flags Decode, page 69
    public int[] BCODED;

    // Spec : 7.4 Macro Block Coding Modes, page 87
    public int[] MBMODES;

    // Spec : 7.5.1 Motion Vector Decode, page 90
    public int MVX;
    public int MVY;

    // Spec : 7.5.2 Macro Block Motion Vector Decode
    public int[][] MVECTS;

    // Spec : 7.6 Block-Level qi Decode, page 97
    public int[] QIIS;

    // Spec : 7.7.1 EOB Token Decode, page 101
    public short[][] COEFFS;

    // Spec : 7.7.3 DCT Coefficient Decode, page 112
    public int[] NCOEFFS;


    // Spec : 7.9.4 The Complete Reconstruction Algorithm, page 138
    public int[][] RECY;
    public int[][] RECCB;
    public int[][] RECCR;

    // Spec : 7.10.3 Complete Loop Filter, page 149
    //public int[][] RECY; //values are updated
    //public int[][] RECCB; //values are updated
    //public int[][] RECCR; //values are updated

}
