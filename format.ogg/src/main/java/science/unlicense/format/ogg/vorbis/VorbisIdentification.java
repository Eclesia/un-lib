
package science.unlicense.format.ogg.vorbis;

import science.unlicense.common.api.Arrays;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * The identification header is a short header of only a few fields
 * used to declare the stream definitively as Vorbis, and provide a
 * few externally relevant pieces of information about the audio stream.
 *
 * @author Johann Sorel
 */
public class VorbisIdentification extends VorbisPacket{

    /** [vorbis_version] = read 32 bits as unsigned integer */
    public long version;
    /** [audio_channels] = read 8 bit integer as unsigned */
    public int audioChannels;
    /** [audio_sample_rate] = read 32 bits as unsigned integer */
    public long audioSampleRate;
    /** [bitrate_maximum] = read 32 bits as signed integer */
    public int bitrateMaximum;
    /** [bitrate_nominal] = read 32 bits as signed integer */
    public int bitrateNominal;
    /** [bitrate_minimum] = read 32 bits as signed integer */
    public int bitrateMinimum;
    /** [blocksize_0] = 2 exponent (read 4 bits as unsigned integer) */
    public int blocksize0;
    /** [blocksize_1] = 2 exponent (read 4 bits as unsigned integer) */
    public int blocksize1;
    /** [framing_flag] = read one bit */
    public boolean framingFlag;


    public VorbisIdentification() {
        super(VorbisConstants.TYPE_IDENTIFICATION);
    }

    public void read(DataInputStream ds) throws IOException{
        version = ds.readUInt();
        audioChannels = ds.readUByte();
        audioSampleRate = ds.readUInt();
        bitrateMaximum = ds.readInt();
        bitrateNominal = ds.readInt();
        bitrateMinimum = ds.readInt();
        blocksize0 = ds.readBits(4, DataInputStream.LSB);
        blocksize1 = ds.readBits(4, DataInputStream.LSB);
        framingFlag = ds.readBits(1) != 0;
        //skip to end of byte
        ds.readBits(7);

        //conformance checks
        if (version!=0) throw new IOException(ds, "Version do not match, expected 0 but was "+version);
        if (audioChannels==0) throw new IOException(ds, "Audio channel count should not be 0");
        if (audioSampleRate==0) throw new IOException(ds, "Audio sample rate should not be 0");
        if (!Arrays.contains(VorbisConstants.BLOCK_SIZES, blocksize0)) throw new IOException(ds, "Invalid block size "+blocksize0);
        if (!Arrays.contains(VorbisConstants.BLOCK_SIZES, blocksize1)) throw new IOException(ds, "Invalid block size "+blocksize1);
        if (blocksize0>blocksize1) throw new IOException(ds, "Invalid block size, block 0 must be smaller or equal to block 1");
        if (framingFlag) throw new IOException(ds, "Invalid frame flag value, should be 1");

    }

}
