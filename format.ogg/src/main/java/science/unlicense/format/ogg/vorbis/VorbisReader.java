package science.unlicense.format.ogg.vorbis;

import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;

/**
 * http://xiph.org/vorbis/doc/Vorbis_I_spec.html
 * 4. Codec Setup and Packet Decode
 *
 * @author Johann Sorel
 */
public class VorbisReader extends AbstractReader {

    protected TypedNode[] metadatas;

    protected VorbisIdentification identification;
    protected VorbisComments comments;
    protected VorbisSetup setup;

    public VorbisReader() {
    }

    public void read() throws IOException{

        final DataInputStream ds = getInputAsDataStream(Endianness.LITTLE_ENDIAN);

        //read information header
        identification = new VorbisIdentification();
        identification.read(ds);

        //read comments header
        comments = new VorbisComments();
        comments.read(ds);

        //read setup header
        setup = new VorbisSetup();
        setup.read(ds);

        //read audio blocks


//    1. decode packet type flag
//    2. decode mode number
//    3. decode window shape (long windows only)
//    4. decode floor
//    5. decode residue into residue vectors
//    6. inverse channel coupling of residue vectors
//    7. generate floor curve from decoded floor data
//    8. compute dot product of floor and residue, producing audio spectrum vector
//    9. inverse monolithic transform of audio spectrum vector, always an MDCT in Vorbis I
//    10. overlap/add left-hand output of transform with right-hand output of previous frame
//    11. store right hand-data from transform of current frame for future lapping
//    12. if not first frame, return results of overlap/add as audio result of current frame

    }

    /**
     * 9.2.1. ilog
     *
     * The ”ilog(x)” function returns the position number (1 through n)
     * of the highest set bit in the two’s complement integer value [x].
     * Values of [x] less than zero are defined to return zero.
     */
    public static int ilog(int x){
        // 1) [return\_value] = 0;
        int ret = 0;
        // 2) if ( [x] is greater than zero ) {
        while (x>0){
            //  3) increment [return\_value];
            ret++;
            // 4) logical shift [x] one bit to the right, padding the MSb with zero
            x >>>= 1;
            //5) repeat at step 2)
        }
        return ret;
    }

    /**
     * 9.2.2. float32_unpack
     *
     * ”float32_unpack(x)” is intended to translate the packed binary representation
     * of a Vorbis codebook float value into the representation used by the decoder
     * for floating point numbers. For purposes of this example, we will unpack a
     * Vorbis float32 into a host-native floating point number.
     *
     *
     */
    public static float unpackFloat32(long data){
        // 1) [mantissa] = [x] bitwise AND 0x1fffff (unsigned result)
        long mantissa = data & 0x1fffff;
        // 2) [sign] = [x] bitwise AND 0x80000000 (unsigned result)
        long sign = data & 0x80000000;
        // 3) [exponent] = ( [x] bitwise AND 0x7fe00000) shifted right 21 bits (unsigned result)
        long exponent = data & 0x7fe00000 >> 21;
        // 4) if ( [sign] is nonzero ) then negate [mantissa]
        if (sign!=0) mantissa = -mantissa;
        // 5) return [mantissa] * ( 2 ^ ( [exponent] - 788 ) )
        return mantissa * (2^(exponent-768));
    }

}
