
package science.unlicense.format.ogg.theora;

import science.unlicense.common.api.Arrays;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class Bits {

    private final ArrayOutputStream out = new ArrayOutputStream();
    private final DataOutputStream ds = new DataOutputStream(out,Endianness.LITTLE_ENDIAN);
    private DataInputStream in;
    private int nbBit = 0;

    public void append(int bit, int nb) throws IOException {
        for (int i=0;i<nb;i++) {
            ds.writeBit(bit);
        }
        nbBit += nb;
    }

    /**
     * Switch the Bits to read mode.
     */
    public Bits readMode() throws IOException {
        ds.flush();
        byte[] data = out.getBuffer().toArrayByte();
//        Arrays.reverse(data, 0, data.length);
        in = new DataInputStream(new ArrayInputStream(data));
//        in.setBitsDirection(DataInputStream.LSB);
//
//        if ((nbBit%8) !=0) {
//            in.readBits(8-(nbBit%8));
//        }

        return this;
    }

    public int pollHeadBit() throws IOException {
        nbBit--;
        return in.readBits(1);
    }

    public boolean isFinished() {
        return nbBit == 0;
    }

}
