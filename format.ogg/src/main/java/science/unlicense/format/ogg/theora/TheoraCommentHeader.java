
package science.unlicense.format.ogg.theora;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class TheoraCommentHeader {

    public Chars vendor;
    public Chars[] comments;

    public void read(DataInputStream ds) throws IOException {

        //1. Decode the common header fields according to the procedure described in
        //   Section 6.1. If HEADERTYPE returned by this procedure is not 0x80,
        //   then stop. This packet is not the identification header.
        if (!Arrays.equals(ds.readBytes(6),TheoraConstants.HEADER_SIGNATURE)) {
            throw new IOException(ds, "Unvalid signature");
        }

        vendor = ds.readBlockZeroTerminatedChars(ds.readInt(Endianness.LITTLE_ENDIAN), CharEncodings.US_ASCII);
        comments = new Chars[ds.readInt(Endianness.LITTLE_ENDIAN)];
        for (int i=0;i<comments.length;i++) {
            comments[i] = ds.readBlockZeroTerminatedChars(ds.readInt(Endianness.LITTLE_ENDIAN), CharEncodings.US_ASCII);
        }
    }

}
