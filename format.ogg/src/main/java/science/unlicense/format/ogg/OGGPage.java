
package science.unlicense.format.ogg;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class OGGPage extends CObject {

    public OGGPageHeader header;
    public byte[] datas;

    @Override
    public Chars toChars() {
        return new Chars("Page stream:"+header.streamSerialNumber+" size:"+datas.length);
    }

}
