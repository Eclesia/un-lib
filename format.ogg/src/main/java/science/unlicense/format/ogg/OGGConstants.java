
package science.unlicense.format.ogg;

/**
 *
 * @author Johann Sorel
 */
public final class OGGConstants {

    /**
     * OGG page header signature.
     */
    public static final byte[] SIGNATURE = new byte[]{'O','g','g','S'};

    /**
     * 0x01: unset = fresh packet
     *         set = continued packet
     */
    public static final byte FRESHPACKET_MASK = 0x01;
    /**
     * 0x02: unset = not first page of logical bitstream
     *         set = first page of logical bitstream (bos)
     */
    public static final byte FIRSTPAGE_MASK = 0x02;
    /**
     * 0x04: unset = not last page of logical bitstream
     *         set = last page of logical bitstream (eos)
     */
    public static final byte LASTPAGE_MASK = 0x04;

    private OGGConstants(){}

}
