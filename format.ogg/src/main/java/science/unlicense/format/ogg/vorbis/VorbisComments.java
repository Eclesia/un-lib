
package science.unlicense.format.ogg.vorbis;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * The Vorbis text comment header is the second (of three) header packets
 * that begin a Vorbis bitstream. It is meant for short text comments,
 * not arbitrary metadata; arbitrary metadata belongs in a separate logical bitstream
 * (usually an XML stream type) that provides greater structure and machine parseability.
 *
 * @author Johann Sorel
 */
public class VorbisComments extends VorbisPacket{

    public Chars vendor;
    public final Sequence comments = new ArraySequence();
    /** [framing_flag] = read one bit */
    public boolean framingFlag;

    public VorbisComments() {
        super(VorbisConstants.TYPE_COMMENTS);
    }

    public void read(DataInputStream ds) throws IOException{
        final int vendorLength = ds.readInt();
        vendor = new Chars(ds.readFully(new byte[vendorLength]), CharEncodings.UTF_8);

        final int nbComments = ds.readInt();
        for (int i=0;i<nbComments;i++){
            final int commentLength = ds.readInt();
            comments.add(new Chars(ds.readFully(new byte[commentLength]), CharEncodings.UTF_8));
        }

        framingFlag = ds.readBits(1) != 0;
        //skip to end of byte
        ds.readBits(7);

        if (framingFlag) throw new IOException(ds, "Invalid frame flag value, should be 1");

    }

}
