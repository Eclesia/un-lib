

package science.unlicense.format.ogg.vorbis;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * http://xiph.org/vorbis/doc/Vorbis_I_spec.html#x1-470003
 *
 * @author Johann Sorel
 */
public class VorbisCodeBook {

    public int codebookDimensions;
    public int codebookEntries;
    public boolean ordered;
    public int[] codebookCodewordLengths;
    public int codebookLookupValues;
    public int[] codebookMultiplicands;

    public void read(DataInputStream ds) throws IOException{
        final int sync = ds.readBits(24);
        if (sync != VorbisConstants.CODEBOOK_SYNC){
            throw new IOException(ds, "Unvalid codebook sync value");
        }

        codebookDimensions = ds.readBits(16);
        codebookEntries = ds.readBits(24);
        ordered = ds.readBits(1) == 1;

        //read code lengths
        codebookCodewordLengths = new int[codebookEntries];
        if (!ordered){
            for (int i=0;i<codebookEntries;i++){
                final boolean sparse = ds.readBits(1) ==1;
                if (sparse){
                    final int flag = ds.readBits(1);
                    if (flag==1){
                        final int length = ds.readBits(5);
                        codebookCodewordLengths[i] = length+1;
                    } else {
                        //this entry is unused.  mark it as such.
                        codebookCodewordLengths[i] = -1;
                    }
                } else {
                    final int length = ds.readBits(5);
                    codebookCodewordLengths[i] = length+1;
                }
            }
        } else {

            // 1) [current_entry] = 0;
            int currentEntry = 0;
            // 2) [current_length] = read a five bit unsigned integer and add 1;
            int currentLength = ds.readBits(5) + 1;
            do {
                // 3) [number] = read ilog([codebook_entries] - [current_entry]) bits as an unsigned integer
                int number = ds.readBits( VorbisReader.ilog(codebookEntries - currentEntry) );
                // 4) set the entries [current_entry] through [current_entry]+[number]-1, inclusive,
                //    of the [codebook_codeword_lengths] array to [current_length]
                for (int i=currentEntry,n=currentEntry+currentLength-1;i<=n;i++){
                    codebookCodewordLengths[i] = currentLength;
                }
                // 5) set [current_entry] to [number] + [current_entry]
                currentEntry += number;
                // 6) increment [current_length] by 1
                currentLength++;
                // 7) if [current_entry] is greater than [codebook_entries] ERROR CONDITION;
                //    the decoder will not be able to read this stream.
                if (currentEntry>codebookEntries){
                    throw new IOException(ds, "Invalid codebook definition.");
                }
                // 8) if [current_entry] is less than [codebook_entries], repeat process starting at 3)
            } while (currentEntry<codebookEntries);
        }

        //read lookup
        final int codebookLookupType = ds.readBits(4);

        if (codebookLookupType == 0){
            //no lookup
        } else if (codebookLookupType == 1 || codebookLookupType == 2){
            // 1) [codebook_minimum_value] = float32_unpack( read 32 bits as an unsigned integer)
            final float codebookMinimumValue = VorbisReader.unpackFloat32(ds.readUInt());
            // 2) [codebook_delta_value] = float32_unpack( read 32 bits as an unsigned integer)
            final float codebookDeltaValue = VorbisReader.unpackFloat32(ds.readUInt());
            // 3) [codebook_value_bits] = read 4 bits as an unsigned integer and add 1
            final int codebookValueBits = ds.readBits(4) + 1;
            // 4) [codebook_sequence_p] = read 1 bit as a boolean flag
            final boolean codebookSequenceP = ds.readBits(1) == 1;

            if (codebookLookupType == 1){
                // 5) [codebook_lookup_values] = lookup1_values([codebook_entries], [codebook_dimensions] )
                codebookLookupValues = lookup1Values(codebookEntries, codebookDimensions);
            } else {
                // 6) [codebook_lookup_values] = [codebook_entries] * [codebook_dimensions]
                codebookLookupValues = codebookEntries * codebookDimensions;
            }

            // 7) read a total of [codebook_lookup_values] unsigned integers of [codebook_value_bits] each;
            //    store these in order in the array [codebook_multiplicands]
            codebookMultiplicands = new int[codebookLookupValues];
            for (int i=0;i<codebookLookupValues;i++){
                codebookMultiplicands[i] = ds.readBits(codebookValueBits);
            }

        } else {
            throw new IOException(ds, "Invalid lookup type : "+codebookLookupType);
        }

    }


    /**
     * 9.2.3. lookup1_values
     *
     * ”lookup1_values(codebook_entries,codebook_dimensions)” is used to compute
     * the correct length of the value index for a codebook VQ lookup table of lookup type 1.
     * The values on this list are permuted to construct the VQ vector lookup
     * table of size [codebook_entries].
     *
     * The return value for this function is defined to be ’the greatest integer
     * value for which [return_value] to the power of [codebook_dimensions] is
     * less than or equal to [codebook_entries]’.
     */
    private int lookup1Values(int codebookEntries, int codebookDimensions){
        int returnValue = 0;
        while (Math.pow(returnValue, codebookDimensions) <= codebookEntries){
            returnValue++;
        }
        return returnValue;
    }

}
