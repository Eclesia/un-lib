
package science.unlicense.format.ogg.theora;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class TheoraMacroBlockLocation extends CObject {

    public int index;
    public int codedIndex;
    public int superBlock;
    public int x;
    public int y;

    /** in raster order */
    public TheoraBlockLocation[] luma;
    /** in raster order */
    public TheoraBlockLocation[] chromaB;
    /** in raster order */
    public TheoraBlockLocation[] chromaR;

    public TheoraMacroBlockLocation(int PF) {
        luma = new TheoraBlockLocation[4];
        switch (PF) {
            case 0 : // 4:2:0
                chromaB = new TheoraBlockLocation[1];
                chromaR = new TheoraBlockLocation[1];
                break;
            case 2 : // 4:2:2
                chromaB = new TheoraBlockLocation[2];
                chromaR = new TheoraBlockLocation[2];
                break;
            case 3 : // 4:4:4
                chromaB = new TheoraBlockLocation[4];
                chromaR = new TheoraBlockLocation[4];
                break;
        }
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("MacroBlock ");
        cb.append(" ri:"+index);
        cb.append(" ci:"+codedIndex);
        cb.append(" X/Y:"+x+"/"+y);
        return cb.toChars();
    }

}
