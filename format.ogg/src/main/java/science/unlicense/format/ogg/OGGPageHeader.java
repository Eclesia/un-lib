
package science.unlicense.format.ogg;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * A page in an OGG stream.
 * Pages can contain up to 64KB of datas for a single logical bitstream.
 *
 * @author Johann Sorel
 */
public class OGGPageHeader {

    /**
     * Streaming structure version.
     */
    public byte structureVersion;
    /**
     * Page content type :
     * - 0x01 : continued packet
     * - 0x02 : first page of a logical bitstream
     * - 0x04 : last page of a logical bitstream
     */
    public byte typeFlag;
    /**
     * The current index in the stream, could be the sample index for audio or
     * frame number for video.
     */
    public long granulePosition;
    /**
     * Unique identifier of the logicial bitstream.
     *
     */
    public int streamSerialNumber;
    /**
     * The page number.
     */
    public int pageSequenceNumber;
    /**
     * Page checksum.
     */
    public int crc;
    /**
     * Number of segments in the page.
     * from 0 to 255 segments.
     */
    public int pageSegments;

    /**
     * Page segment table, contains the size of each segment.
     * Segment size from 0 to 255 bytes.
     */
    public int[] segment_table;

    public boolean isFreshPacket(){
        return (typeFlag & OGGConstants.FRESHPACKET_MASK) == 0;
    }

    public boolean isFirstPage(){
        return (typeFlag & OGGConstants.FIRSTPAGE_MASK) == 1;
    }

    public boolean isLastPage(){
        return (typeFlag & OGGConstants.LASTPAGE_MASK) == 1;
    }

    public void read(DataInputStream ds) throws IOException{
        structureVersion = ds.readByte();
        typeFlag = ds.readByte();
        granulePosition = ds.readLong();
        streamSerialNumber = ds.readInt();
        pageSequenceNumber = ds.readInt();
        crc = ds.readInt();
        pageSegments = ds.readUByte();
        segment_table = ds.readUByte(pageSegments);
    }

}
