
package science.unlicense.format.ogg.theora;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author husky
 */
public class BitsTest {

    @Test
    public void testRW() throws IOException {

        Bits bits = new Bits();
        bits.append(1, 3);
        bits.append(0, 1);
        bits.append(1, 1);
        bits.append(0, 8);
        bits.readMode();

        Assert.assertEquals(1,bits.pollHeadBit());
        Assert.assertEquals(1,bits.pollHeadBit());
        Assert.assertEquals(1,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(1,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());

//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(1,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(1,bits.pollHeadBit());
//        Assert.assertEquals(1,bits.pollHeadBit());
//        Assert.assertEquals(1,bits.pollHeadBit());
    }

}
