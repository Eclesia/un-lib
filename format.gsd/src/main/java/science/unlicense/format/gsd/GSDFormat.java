

package science.unlicense.format.gsd;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * Graphtec Vector Graphic Data format.
 *
 * Resource :
 * http://en.wikipedia.org/wiki/Graphtec_Corporation
 *
 * @author Johann Sorel
 */
public class GSDFormat extends DefaultFormat {

    public GSDFormat() {
        super(new Chars("gsd"));
        shortName = new Chars("GSD");
        longName = new Chars("Graphtec Vector Graphic Data");
        extensions.add(new Chars("gsd"));
        signatures.add(GSDConstants.SIGNATURE);
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
