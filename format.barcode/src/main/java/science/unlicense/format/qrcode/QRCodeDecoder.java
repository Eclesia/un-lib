
package science.unlicense.format.qrcode;

import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class QRCodeDecoder {

    public static void main(String[] args) throws IOException {

        Image image = Images.read(Paths.resolve(new Chars("/home/clubelec/Documents/qr-code-avin.png")));

        System.out.println(image);


        Images.write(image, new Chars("png"), Paths.resolve(new Chars("/home/clubelec/Documents/qr-code-avin-threshold.png")));


    }

    private QRCodeDecoder() {

    }


}
