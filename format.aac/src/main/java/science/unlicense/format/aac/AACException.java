package science.unlicense.format.aac;

import science.unlicense.encoding.api.store.FormatEncodingException;


/**
 * Standard exception, thrown when decoding of an AAC frame fails.
 * The message gives more detailed information about the error.
 *
 * @author Alexander Simm
 */
public class AACException extends FormatEncodingException {

    private final boolean eos;

    public AACException(String message) {
        this(null, message, false);
    }

    public AACException(String message, boolean eos) {
        super(null, message);
        this.eos = eos;
    }

    public AACException(Throwable cause) {
        super(null, cause);
        eos = false;
    }

    public AACException(Object origin, String message) {
        this(origin,message, false);
    }

    public AACException(Object origin, String message, boolean eos) {
        super(origin,message);
        this.eos = eos;
    }

    public AACException(Object origin, Throwable cause) {
        super(origin,cause);
        eos = false;
    }

    boolean isEndOfStream() {
        return eos;
    }
}
