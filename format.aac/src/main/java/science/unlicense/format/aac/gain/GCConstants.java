package science.unlicense.format.aac.gain;

/**
 *
 * @author Alexander Simm
 */
public final class GCConstants {

    public static final int BANDS = 4;
    public static final int MAX_CHANNELS = 5;
    public static final int NPQFTAPS = 96;
    public static final int NPEPARTS = 64;    //number of pre-echo inhibition parts
    public static final int ID_GAIN = 16;
    public static final int[] LN_GAIN = {
                -4, -3, -2, -1, 0, 1, 2, 3,
                4, 5, 6, 7, 8, 9, 10, 11
            };

    private GCConstants() {}


}
