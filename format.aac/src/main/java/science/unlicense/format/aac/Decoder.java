package science.unlicense.format.aac;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.format.aac.filterbank.FilterBank;
import science.unlicense.format.aac.syntax.BitStream;
import static science.unlicense.format.aac.syntax.Constants.*;
import science.unlicense.format.aac.syntax.PCE;
import science.unlicense.format.aac.syntax.SyntacticElements;
import science.unlicense.format.aac.transport.ADIFHeader;

/**
 * Main AAC decoder class
 *
 * @author Alexander Simm
 * @author Johann Sorel
 */
public class Decoder {

    private final DecoderConfig config;
    private final SyntacticElements syntacticElements;
    private final FilterBank filterBank;
    private BitStream in;
    private ADIFHeader adifHeader;

    /**
     * The methods returns true, if a profile is supported by the decoder.
     * @param profile an AAC profile
     * @return true if the specified profile can be decoded
     * @see Profile#isDecodingSupported()
     */
    public static boolean canDecode(Profile profile) {
        return profile.isDecodingSupported();
    }

    /**
     * Initializes the decoder with a MP4 decoder specific info.
     *
     * After this the MP4 frames can be passed to the
     * <code>decodeFrame(byte[], SampleBuffer)</code> method to decode them.
     *
     * @param decoderSpecificInfo a byte array containing the decoder specific info from an MP4 container
     * @throws AACException if the specified profile is not supported
     */
    public Decoder(byte[] decoderSpecificInfo) throws AACException {
        config = DecoderConfig.parseMP4DecoderSpecificInfo(decoderSpecificInfo);
        if (config==null) throw new InvalidArgumentException("illegal MP4 decoder specific info");

        if (!canDecode(config.getProfile())) throw new AACException("unsupported profile: "+config.getProfile().getDescription());

        syntacticElements = new SyntacticElements(config);
        filterBank = new FilterBank(config.isSmallFrameUsed(), config.getChannelConfiguration().getChannelCount());

        in = new BitStream();

        LOGGER.info(new Chars("profile: "+config.getProfile()));
        LOGGER.info(new Chars("sf: "+ config.getSampleFrequency().getFrequency()));
        LOGGER.info(new Chars("channels: "+ config.getChannelConfiguration().getDescription()));
    }

    public DecoderConfig getConfig() {
        return config;
    }

    /**
     * Decodes one frame of AAC data in frame mode and returns the raw PCM
     * data.
     * @param frame the AAC frame
     * @param buffer a buffer to hold the decoded PCM data
     * @throws AACException if decoding fails
     */
    public void decodeFrame(byte[] frame, SampleBuffer buffer) throws AACException {
        if (frame!=null) in.setData(frame);
        try {
            decode(buffer);
        }
        catch(AACException e) {
            if (!e.isEndOfStream()){
                throw e;
            } else {
                LOGGER.warning(new Chars("unexpected end of frame"));
            }
        }
    }

    private void decode(SampleBuffer buffer) throws AACException {
        if (ADIFHeader.isPresent(in)) {
            adifHeader = ADIFHeader.readHeader(in);
            final PCE pce = adifHeader.getFirstPCE();
            config.setProfile(pce.getProfile());
            config.setSampleFrequency(pce.getSampleFrequency());
            config.setChannelConfiguration(ChannelConfiguration.forInt(pce.getChannelCount()));
        }

        if (!canDecode(config.getProfile())) throw new AACException("unsupported profile: "+config.getProfile().getDescription());

        syntacticElements.startNewFrame();

        try {
            //1: bitstream parsing and noiseless coding
            syntacticElements.decode(in);
            //2: spectral processing
            syntacticElements.process(filterBank);
            //3: send to output buffer
            syntacticElements.sendToOutput(buffer);
        }
        catch(AACException e) {
            buffer.setData(new byte[0], 0, 0, 0, 0);
            throw e;
        }
        catch(Exception e) {
            buffer.setData(new byte[0], 0, 0, 0, 0);
            throw new AACException(e);
        }
    }
}
