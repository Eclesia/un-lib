package science.unlicense.format.aac.syntax;

import science.unlicense.format.aac.AACException;
import science.unlicense.format.aac.SampleFrequency;
import science.unlicense.format.aac.sbr.SBR;
import static science.unlicense.format.aac.syntax.Constants.*;

/**
 *
 * @author Alexander Simm
 */
public abstract class Element {

    private int elementInstanceTag;
    private SBR sbr;

    protected void readElementInstanceTag(BitStream in) throws AACException {
        elementInstanceTag = in.readBits(4);
    }

    public int getElementInstanceTag() {
        return elementInstanceTag;
    }

    void decodeSBR(BitStream in, SampleFrequency sf, int count, boolean stereo, boolean crc, boolean downSampled,boolean smallFrames) throws AACException {
        if (sbr==null) sbr = new SBR(smallFrames,elementInstanceTag==ELEMENT_CPE,sf,downSampled);
        sbr.decode(in, count);
    }

    boolean isSBRPresent() {
        return sbr!=null;
    }

    SBR getSBR() {
        return sbr;
    }
}
