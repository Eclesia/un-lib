package science.unlicense.format.aac.ps;

/**
 *
 * @author Alexander Simm
 */
public final class PSConstants {

    public static final int MAX_PS_ENVELOPES = 5;
    public static final int NO_ALLPASS_LINKS = 3;
    public static final int NEGATE_IPD_MASK = 0x1000;
    public static final float DECAY_SLOPE = 0.05f;
    public static final float COEF_SQRT2 = 1.4142135623731f;

    private PSConstants(){}

}
