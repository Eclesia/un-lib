package science.unlicense.format.aac.syntax;

import science.unlicense.format.aac.AACException;
import science.unlicense.format.aac.DecoderConfig;

/**
 *
 * @author Alexander Simm
 */
class SCE_LFE extends Element {

    private final ICStream ics;

    SCE_LFE(int frameLength) {
        super();
        ics = new ICStream(frameLength);
    }

    void decode(BitStream in, DecoderConfig conf) throws AACException {
        readElementInstanceTag(in);
        ics.decode(in, false, conf);
    }

    public ICStream getICStream() {
        return ics;
    }
}
