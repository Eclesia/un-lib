
package science.unlicense.format.stl;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * STL Binary Model store.
 *
 * @author Johann Sorel
 */
public class STLBinaryStore extends STLAbstractStore {

    public STLBinaryStore(Object input) {
        super(STLBinaryFormat.INSTANCE,input);
    }

    @Override
    protected void readFacets() throws IOException {
        facets = new ArraySequence();

        final byte[] header = new byte[80];
        final DataInputStream ds = new DataInputStream(getSourceAsInputStream(), Endianness.LITTLE_ENDIAN);
        ds.readFully(header);

        int nbTriangle = ds.readInt();
        for (int i=0;i<nbTriangle;i++){
            final STLFacet facet = new STLFacet();
            facet.normal = ds.readFloat(3);
            facet.vertices = ds.readFloat(9);
            facet.hint = ds.readShort();
            facets.add(facet);
        }
    }

}
