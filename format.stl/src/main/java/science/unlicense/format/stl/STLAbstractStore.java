
package science.unlicense.format.stl;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.Techniques;

/**
 * Abstract STL store for ASCII and Binary format.
 *
 * @author Johann Sorel
 */
public abstract class STLAbstractStore extends AbstractModel3DStore{

    protected Sequence facets;
    protected Chars name;

    public STLAbstractStore(Format format,Object input) {
        super(format,input);
    }

    protected abstract void readFacets() throws IOException;

    @Override
    public Collection getElements() {

        if (facets == null){
            try {
                readFacets();
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
        }

        //rebuild the mesh
        final int nbFacets = facets.getSize();
        final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(nbFacets*3*3).cursor();
        final Float32Cursor normals = DefaultBufferFactory.INSTANCE.createFloat32(nbFacets*3*3).cursor();
        final Int32Cursor indices = DefaultBufferFactory.INSTANCE.createInt32(nbFacets*3).cursor();
        for (int i=0,k=0;i<nbFacets;i++,k+=3){
            final STLFacet facet = (STLFacet) facets.get(i);
            vertices.write(facet.vertices);
            normals.write(facet.normal);
            normals.write(facet.normal);
            normals.write(facet.normal);
            indices.write(k);
            indices.write(k+1);
            indices.write(k+2);
        }
        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(new VBO(vertices.getBuffer(), 3));
        shell.setNormals(new VBO(normals.getBuffer(), 3));
        shell.setIndex(new IBO(indices.getBuffer()));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) indices.getBuffer().getNumbersSize())});
        final DefaultModel mesh = new DefaultModel();

        Techniques.styleCAD(mesh);
        mesh.setShape(shell);
        mesh.updateBoundingBox();

        if (name != null){
            mesh.setTitle(name);
        }

        final Collection col = new ArraySequence();
        col.add(mesh);
        return col;
    }

}
