
package science.unlicense.format.stl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * STL, STereoLithography or Standard Tessellation Language format.
 *
 * http://en.wikipedia.org/wiki/STL_(file_format)
 *
 * @author Johann Sorel
 */
public class STLAsciiFormat extends AbstractModel3DFormat {

    public static final STLAsciiFormat INSTANCE = new STLAsciiFormat();

    private STLAsciiFormat() {
        super(new Chars("STL-ASCII"));
        shortName = new Chars("STL-ASCII");
        longName = new Chars("STereoLithography");
        extensions.add(new Chars("stl"));
        signatures.add(STLConstants.START_SOLID.toBytes());
    }

    @Override
    public Store open(Object input) throws IOException {
        return new STLAsciiStore(input);
    }

}
