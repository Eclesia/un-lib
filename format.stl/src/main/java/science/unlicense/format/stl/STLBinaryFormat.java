
package science.unlicense.format.stl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * STL, STereoLithography or Standard Tessellation Language format.
 *
 * http://en.wikipedia.org/wiki/STL_(file_format)
 *
 * @author Johann Sorel
 */
public class STLBinaryFormat extends AbstractModel3DFormat {

    public static final STLBinaryFormat INSTANCE = new STLBinaryFormat();

    private STLBinaryFormat() {
        super(new Chars("STL-BIN"));
        shortName = new Chars("STL-BIN");
        longName = new Chars("STereoLithography");
        extensions.add(new Chars("stl"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new STLBinaryStore(input);
    }

}
