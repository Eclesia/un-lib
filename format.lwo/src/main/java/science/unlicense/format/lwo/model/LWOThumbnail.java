
package science.unlicense.format.lwo.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class LWOThumbnail extends LWOChunk {

    /** UInt2 */
    public int encoding;
    /** UInt2 */
    public int width;
    /** byte* */
    public byte[] data;

    public void readInternal(DataInputStream ds) throws IOException {
        encoding = ds.readUShort();
        width = ds.readUShort();
        int height = (int) ((length-4)/width);
        data = ds.readFully(new byte[width*height*3]);
    }

}
