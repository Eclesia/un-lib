
package science.unlicense.format.lwo.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOVertexMapping extends LWOChunk {

    public static final class Element{
        public int index;
        public float[] values;
    }

    /** ID4 */
    public Chars type;
    /** UInt2 */
    public int dimension;
    /** String */
    public Chars name;

    public Sequence elements = new ArraySequence();

    public void readInternal(DataInputStream ds) throws IOException {
        type = ds.readZeroTerminatedChars(4, CharEncodings.US_ASCII);
        dimension = ds.readUShort();
        name = LWOUtils.readChars(ds);

        while ( ds.getByteOffset() < (offset+length)){
            final Element ele = new Element();
            ele.index = LWOUtils.readVarInt(ds);
            ele.values = ds.readFloat(dimension);
            elements.add(ele);
        }
    }

    public Chars toChars() {
        return super.toChars().concat(' ').concat(type).concat(' ').concat(name);
    }
}
