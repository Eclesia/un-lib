
package science.unlicense.format.lwo.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class LWOVertexMapParameter extends LWOChunk {

    /** Int4 */
    public int type;
    /** Int4 */
    public int color;

    public void readInternal(DataInputStream ds) throws IOException {
        type = ds.readInt();
        color = ds.readInt();
    }

}
