
package science.unlicense.format.lwo.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOCommentaryText extends LWOChunk {

    /** String */
    public Chars cmt;

    public void readInternal(DataInputStream ds) throws IOException {
        cmt = LWOUtils.readChars(ds);
    }

    public Chars toChars() {
        return super.toChars().concat(' ').concat(cmt);
    }

}
