
package science.unlicense.format.lwo.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.VectorRW;
import science.unlicense.format.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOBoundingBox extends LWOChunk {

    /** Vec12 */
    public VectorRW upper;
    /** Vec12 */
    public VectorRW lower;

    public void readInternal(DataInputStream ds) throws IOException {
        upper = LWOUtils.readVec3(ds);
        lower = LWOUtils.readVec3(ds);
    }

    public Chars toChars() {
        return super.toChars().concat(' ').concat(lower.toChars()).concat(' ').concat(upper.toChars());
    }

}
