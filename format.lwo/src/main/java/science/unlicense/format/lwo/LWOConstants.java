

package science.unlicense.format.lwo;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class LWOConstants {

    public static final byte[] SIGNATURE = new byte[]{'F','O','R','M'};

    public static final Chars CHUNK_FORM            = Chars.constant(new byte[]{'F','O','R','M'});
    public static final Chars CHUNK_LAYER           = Chars.constant(new byte[]{'L','A','Y','R'});
    public static final Chars CHUNK_POINT_LIST      = Chars.constant(new byte[]{'P','N','T','S'});
    public static final Chars CHUNK_VERTEX_MAPPING  = Chars.constant(new byte[]{'V','M','A','P'});
    public static final Chars CHUNK_POLYGON_LIST    = Chars.constant(new byte[]{'P','O','L','S'});
    public static final Chars CHUNK_TAGS            = Chars.constant(new byte[]{'T','A','G','S'});
    public static final Chars CHUNK_POLYGON_TAG_MAPPING = Chars.constant(new byte[]{'P','T','A','G'});
    public static final Chars CHUNK_DISC_VERTEX_MAPPING = Chars.constant(new byte[]{'V','M','A','D'});
    public static final Chars CHUNK_VERTEX_MAP_PARAM= Chars.constant(new byte[]{'V','M','P','A'});
    public static final Chars CHUNK_ENVELOPE        = Chars.constant(new byte[]{'E','N','V','L'});
    public static final Chars CHUNK_IMAGE           = Chars.constant(new byte[]{'C','L','I','P'});
    public static final Chars CHUNK_SURFACE         = Chars.constant(new byte[]{'S','U','R','F'});
    public static final Chars CHUNK_BOUNDINGBOX     = Chars.constant(new byte[]{'B','B','O','X'});
    public static final Chars CHUNK_DESCRIPTION     = Chars.constant(new byte[]{'D','E','S','C'});
    public static final Chars CHUNK_COMMENTARY      = Chars.constant(new byte[]{'T','E','X','T'});
    public static final Chars CHUNK_THUMBNAIL       = Chars.constant(new byte[]{'I','C','O','N'});

    public static final Chars SUBCHUNK_ENV_TYPE       = Chars.constant(new byte[]{'T','Y','P','E'});
    public static final Chars SUBCHUNK_ENV_PRE       = Chars.constant(new byte[]{'P','R','E',' '});
    public static final Chars SUBCHUNK_ENV_POST       = Chars.constant(new byte[]{'P','O','S','T'});
    public static final Chars SUBCHUNK_ENV_KEY       = Chars.constant(new byte[]{'K','E','Y',' '});
    public static final Chars SUBCHUNK_ENV_SPAN       = Chars.constant(new byte[]{'S','P','A','N'});
    public static final Chars SUBCHUNK_ENV_CHAN       = Chars.constant(new byte[]{'C','H','A','N'});
    public static final Chars SUBCHUNK_ENV_NAME       = Chars.constant(new byte[]{'N','A','M','E'});

    public static final Chars SUBCHUNK_CLIP_STIL        = Chars.constant(new byte[]{'S','T','I','L'});
    public static final Chars SUBCHUNK_CLIP_ISEQ        = Chars.constant(new byte[]{'I','S','E','Q'});
    public static final Chars SUBCHUNK_CLIP_ANIM        = Chars.constant(new byte[]{'A','N','I','M'});
    public static final Chars SUBCHUNK_CLIP_XREF        = Chars.constant(new byte[]{'X','R','E','F'});
    public static final Chars SUBCHUNK_CLIP_STCC        = Chars.constant(new byte[]{'S','T','C','C'});
    public static final Chars SUBCHUNK_CLIP_TIME        = Chars.constant(new byte[]{'T','I','M','E'});
    public static final Chars SUBCHUNK_CLIP_CSRGB       = Chars.constant(new byte[]{'C','L','R','S'});
    public static final Chars SUBCHUNK_CLIP_CSALPHA     = Chars.constant(new byte[]{'C','L','R','A'});
    public static final Chars SUBCHUNK_CLIP_FILT        = Chars.constant(new byte[]{'F','I','L','T'});
    public static final Chars SUBCHUNK_CLIP_DITH        = Chars.constant(new byte[]{'D','I','T','H'});
    public static final Chars SUBCHUNK_CLIP_CONST       = Chars.constant(new byte[]{'C','O','N','T'});
    public static final Chars SUBCHUNK_CLIP_BRIT        = Chars.constant(new byte[]{'B','R','I','T'});
    public static final Chars SUBCHUNK_CLIP_SATR        = Chars.constant(new byte[]{'S','A','T','R'});
    public static final Chars SUBCHUNK_CLIP_HUE         = Chars.constant(new byte[]{'H','U','E',' '});
    public static final Chars SUBCHUNK_CLIP_GAMM        = Chars.constant(new byte[]{'G','A','M','M'});
    public static final Chars SUBCHUNK_CLIP_NEGA        = Chars.constant(new byte[]{'N','E','G','A'});
    public static final Chars SUBCHUNK_CLIP_IFLT        = Chars.constant(new byte[]{'I','F','L','T'});
    public static final Chars SUBCHUNK_CLIP_PFLT        = Chars.constant(new byte[]{'P','F','L','T'});

    public static final Chars SUBCHUNK_SURF_COLOR        = Chars.constant(new byte[]{'C','O','L','R'});
    public static final Chars SUBCHUNK_SURF_DIFF        = Chars.constant(new byte[]{'D','I','F','F'});
    public static final Chars SUBCHUNK_SURF_LUMI        = Chars.constant(new byte[]{'L','U','M','I'});
    public static final Chars SUBCHUNK_SURF_SPEC        = Chars.constant(new byte[]{'S','P','E','C'});
    public static final Chars SUBCHUNK_SURF_REFL        = Chars.constant(new byte[]{'R','E','F','L'});
    public static final Chars SUBCHUNK_SURF_TRAN        = Chars.constant(new byte[]{'T','R','A','N'});
    public static final Chars SUBCHUNK_SURF_TRNL        = Chars.constant(new byte[]{'T','R','N','L'});
    public static final Chars SUBCHUNK_SURF_GLOS        = Chars.constant(new byte[]{'G','L','O','S'});
    public static final Chars SUBCHUNK_SURF_SHRP        = Chars.constant(new byte[]{'S','H','R','P'});
    public static final Chars SUBCHUNK_SURF_BUMP        = Chars.constant(new byte[]{'B','U','M','P'});
    public static final Chars SUBCHUNK_SURF_SIDE        = Chars.constant(new byte[]{'S','I','D','E'});
    public static final Chars SUBCHUNK_SURF_SMAN        = Chars.constant(new byte[]{'S','M','A','N'});
    public static final Chars SUBCHUNK_SURF_RFOP        = Chars.constant(new byte[]{'R','F','O','P'});
    public static final Chars SUBCHUNK_SURF_RIMG        = Chars.constant(new byte[]{'R','I','M','G'});
    public static final Chars SUBCHUNK_SURF_RSAN        = Chars.constant(new byte[]{'R','S','A','N'});
    public static final Chars SUBCHUNK_SURF_RBLR        = Chars.constant(new byte[]{'R','B','L','R'});
    public static final Chars SUBCHUNK_SURF_RIND        = Chars.constant(new byte[]{'R','I','N','D'});
    public static final Chars SUBCHUNK_SURF_TROP        = Chars.constant(new byte[]{'T','R','O','P'});
    public static final Chars SUBCHUNK_SURF_TIMG        = Chars.constant(new byte[]{'T','I','M','G'});
    public static final Chars SUBCHUNK_SURF_TBLR        = Chars.constant(new byte[]{'T','B','L','R'});
    public static final Chars SUBCHUNK_SURF_CLRH        = Chars.constant(new byte[]{'C','L','R','H'});
    public static final Chars SUBCHUNK_SURF_CLRF        = Chars.constant(new byte[]{'C','L','R','F'});
    public static final Chars SUBCHUNK_SURF_ADTR        = Chars.constant(new byte[]{'A','D','T','R'});
    public static final Chars SUBCHUNK_SURF_GLOW        = Chars.constant(new byte[]{'G','L','O','W'});
    public static final Chars SUBCHUNK_SURF_LINE        = Chars.constant(new byte[]{'L','I','N','E'});
    public static final Chars SUBCHUNK_SURF_ALPH        = Chars.constant(new byte[]{'A','L','P','H'});
    public static final Chars SUBCHUNK_SURF_VCOL        = Chars.constant(new byte[]{'V','C','O','L'});
    public static final Chars SUBCHUNK_SURF_BLOCK        = Chars.constant(new byte[]{'B','L','O','K'});

    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_IMAP        = Chars.constant(new byte[]{'I','M','A','P'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_PROC        = Chars.constant(new byte[]{'P','R','O','C'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_GRAD        = Chars.constant(new byte[]{'G','R','A','D'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_SHDR        = Chars.constant(new byte[]{'S','H','D','R'});

    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_CHAN        = Chars.constant(new byte[]{'C','H','A','N'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_ENAB        = Chars.constant(new byte[]{'E','N','A','B'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_OPAC        = Chars.constant(new byte[]{'O','P','A','C'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_AXIS        = Chars.constant(new byte[]{'A','X','I','S'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_NEGA        = Chars.constant(new byte[]{'N','E','G','A'});

    public static final Chars SUBCHUNK_SURF_BLOK_TMAP             = Chars.constant(new byte[]{'T','M','A','P'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_CNTR        = Chars.constant(new byte[]{'C','N','T','R'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_SIZE        = Chars.constant(new byte[]{'S','I','Z','E'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_ROTA        = Chars.constant(new byte[]{'R','O','T','A'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_OREF        = Chars.constant(new byte[]{'O','R','E','F'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_FALL        = Chars.constant(new byte[]{'F','A','L','L'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_CSYS        = Chars.constant(new byte[]{'C','S','Y','S'});

    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_PROJ        = Chars.constant(new byte[]{'P','R','O','J'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_AXIS        = Chars.constant(new byte[]{'A','X','I','S'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_IMAG        = Chars.constant(new byte[]{'I','M','A','G'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_WRAP        = Chars.constant(new byte[]{'W','R','A','P'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_WRPW        = Chars.constant(new byte[]{'W','R','P','W'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_WRPH        = Chars.constant(new byte[]{'W','R','P','H'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_VMAP        = Chars.constant(new byte[]{'V','M','A','P'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_AAST        = Chars.constant(new byte[]{'A','A','S','T'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_PIXB        = Chars.constant(new byte[]{'P','I','X','B'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_STCK        = Chars.constant(new byte[]{'S','T','C','K'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_TAMP        = Chars.constant(new byte[]{'T','A','M','P'});

    public static final Chars SUBCHUNK_SURF_BLOK_PTEX_AXIS        = Chars.constant(new byte[]{'A','X','I','S'});
    public static final Chars SUBCHUNK_SURF_BLOK_PTEX_VALU        = Chars.constant(new byte[]{'V','A','L','U'});
    public static final Chars SUBCHUNK_SURF_BLOK_PTEX_FUNC        = Chars.constant(new byte[]{'F','U','N','C'});

    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_PNAM        = Chars.constant(new byte[]{'P','N','A','M'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_INAM        = Chars.constant(new byte[]{'I','N','A','M'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_GRST        = Chars.constant(new byte[]{'G','R','S','T'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_GREN        = Chars.constant(new byte[]{'G','R','E','N'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_GRPT        = Chars.constant(new byte[]{'G','R','P','T'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_FKEY        = Chars.constant(new byte[]{'F','K','E','Y'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_IKEY        = Chars.constant(new byte[]{'I','K','E','Y'});

    public static final Chars SUBCHUNK_SURF_BLOK_SHDR        = Chars.constant(new byte[]{'S','H','D','R'});
    public static final Chars SUBCHUNK_SURF_BLOK_SHDR_FUNC        = Chars.constant(new byte[]{'F','U','N','C'});

    private LWOConstants() {
    }

}
