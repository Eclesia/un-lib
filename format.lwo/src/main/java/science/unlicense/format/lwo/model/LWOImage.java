
package science.unlicense.format.lwo.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.Float64;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.lwo.LWOConstants;
import science.unlicense.format.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOImage extends LWOChunk {


    private static final Dictionary SUBCHUNKS = new HashDictionary();
    static {
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_STIL, StilImage.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_ISEQ, ImageSequence.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_ANIM, PluginAnimation.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_XREF, Reference.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_STCC, ColorCyclingStill.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_TIME, Time.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_CSRGB, ColorSpaceRGB.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_CSALPHA, ColorSpaceAlpha.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_FILT, ImageFiltering.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_DITH, ImageDithering.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_CONST, Contrast.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_BRIT, Brightness.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_SATR, Saturation.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_HUE, Hue.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_GAMM, Gamma.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_NEGA, Negative.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_IFLT, PluginImageFilter.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_CLIP_PFLT, PluginPixelFilter.class);
    }

    /** U4 */
    public int index;

    public void readInternal(DataInputStream ds) throws IOException {
        index = ds.readInt();
        readSubChunks(ds, SUBCHUNKS);
    }

    public static final class StilImage extends LWOChunk{
        public Chars name;
        public void readInternal(DataInputStream ds) throws IOException {
            name = LWOUtils.readChars(ds);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(name);
        }
    }
    public static final class ImageSequence extends LWOChunk{
        public int numDigits;
        public int flags;
        public int offset;
        public int reserved;
        public int start;
        public int end;
        public Chars prefix;
        public Chars suffix;
        public void readInternal(DataInputStream ds) throws IOException {
            numDigits = ds.readUByte();
            flags = ds.readUByte();
            offset = ds.readShort();
            reserved = ds.readUShort();
            start = ds.readShort();
            end = ds.readShort();
            prefix = LWOUtils.readChars(ds);
            suffix = LWOUtils.readChars(ds);
        }
    }
    public static final class PluginAnimation extends LWOChunk{
        public Chars fileName;
        public Chars serverName;
        public int flags;
        public byte[] datas;
        public void readInternal(DataInputStream ds) throws IOException {
            fileName = LWOUtils.readChars(ds);
            serverName = LWOUtils.readChars(ds);
            flags = ds.readUShort();
            datas = ds.readFully(new byte[(int) ((offset+length)-ds.getByteOffset())]);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(fileName);
        }
    }
    public static final class Reference extends LWOChunk{
        public int index;
        public Chars str;
        public void readInternal(DataInputStream ds) throws IOException {
            index = ds.readInt();
            str = LWOUtils.readChars(ds);
        }
    }
    public static final class ColorCyclingStill extends LWOChunk{
        public int lo;
        public int hi;
        public Chars name;
        public void readInternal(DataInputStream ds) throws IOException {
            lo = ds.readShort();
            hi = ds.readShort();
            name = LWOUtils.readChars(ds);
        }
    }
    public static final class Time extends LWOChunk{
        public float startTime;
        public float duration;
        public float frameRate;
        public void readInternal(DataInputStream ds) throws IOException {
            startTime = ds.readFloat();
            duration = ds.readFloat();
            frameRate = ds.readFloat();
        }
    }
    public static final class ColorSpaceRGB extends LWOChunk{
        public int flags;
        public int colorspace;
        public Chars filename;
        public void readInternal(DataInputStream ds) throws IOException {
            flags = ds.readUShort();
            colorspace = ds.readUShort();
            filename = LWOUtils.readChars(ds);
        }
    }
    public static final class ColorSpaceAlpha extends LWOChunk{
        public int flags;
        public int colorspace;
        public Chars filename;
        public void readInternal(DataInputStream ds) throws IOException {
            flags = ds.readUShort();
            colorspace = ds.readUShort();
            filename = LWOUtils.readChars(ds);
        }
    }
    public static final class ImageFiltering extends LWOChunk{
        public int flags;
        public void readInternal(DataInputStream ds) throws IOException {
            flags = ds.readUShort();
        }
    }
    public static final class ImageDithering extends LWOChunk{
        public int flags;
        public void readInternal(DataInputStream ds) throws IOException {
            flags = ds.readUShort();
        }
    }
    public static final class Contrast extends LWOChunk{
        public float contrastDelta;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            contrastDelta = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(contrastDelta)).concat(' ').concat(envelopeIndex);
        }
    }
    public static final class Brightness extends LWOChunk{
        public float brightnessDelta;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            brightnessDelta = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(brightnessDelta)).concat(' ').concat(envelopeIndex);
        }
    }
    public static final class Saturation extends LWOChunk{
        public float saturationDelta;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            saturationDelta = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(saturationDelta)).concat(' ').concat(envelopeIndex);
        }
    }
    public static final class Hue extends LWOChunk{
        public float hueRotation;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            hueRotation = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(hueRotation)).concat(' ').concat(envelopeIndex);
        }
    }
    public static final class Gamma extends LWOChunk{
        public float gamma;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            gamma = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(gamma)).concat(' ').concat(envelopeIndex);
        }
    }
    public static final class Negative extends LWOChunk{
        public int enable;
        public void readInternal(DataInputStream ds) throws IOException {
            enable = ds.readUShort();
        }
    }
    public static final class PluginImageFilter extends LWOChunk{
        public Chars serverName;
        public int flags;
        public byte[] data;
        public void readInternal(DataInputStream ds) throws IOException {
            serverName = LWOUtils.readChars(ds);
            flags = ds.readUShort();
            data = ds.readFully(new byte[(int) ((offset+length)-ds.getByteOffset())]);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(serverName);
        }
    }
    public static final class PluginPixelFilter extends LWOChunk{
        public Chars serverName;
        public int flags;
        public byte[] data;
        public void readInternal(DataInputStream ds) throws IOException {
            serverName = LWOUtils.readChars(ds);
            flags = ds.readUShort();
            data = ds.readFully(new byte[(int) ((offset+length)-ds.getByteOffset())]);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(serverName);
        }
    }

}
