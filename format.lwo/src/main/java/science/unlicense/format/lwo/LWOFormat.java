

package science.unlicense.format.lwo;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Lightwave object format.
 *
 * Docs :
 * http://content.gpwiki.org/index.php/LWO
 * http://muskrat.middlebury.edu/lt/cr/ongoing/sciviz/LightWave/SDK/docs/filefmts/lwo2.html
 *
 * @author Johann Sorel
 */
public class LWOFormat extends AbstractModel3DFormat {

    public static final LWOFormat INSTANCE = new LWOFormat();

    private LWOFormat() {
        super(new Chars("LWO"));
        shortName = new Chars("LWO");
        longName = new Chars("Lightwave Object");
        extensions.add(new Chars("lwo"));
        signatures.add(LWOConstants.SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new LWOStore(input);
    }

}
