
package science.unlicense.format.lwo.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.VectorRW;
import science.unlicense.format.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOPointList extends LWOChunk {

    public VectorRW[] points;

    public void readInternal(DataInputStream ds) throws IOException {
        points = new VectorRW[(int) (length/12)];
        for (int i=0;i<points.length;i++){
            points[i] = LWOUtils.readVec3(ds);
        }
    }

}
