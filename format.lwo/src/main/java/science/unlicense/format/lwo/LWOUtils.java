

package science.unlicense.format.lwo;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.lwo.LWOConstants.*;
import science.unlicense.format.lwo.model.LWOBoundingBox;
import science.unlicense.format.lwo.model.LWOChunk;
import science.unlicense.format.lwo.model.LWOCommentaryText;
import science.unlicense.format.lwo.model.LWODescriptionLine;
import science.unlicense.format.lwo.model.LWODiscVertexMapping;
import science.unlicense.format.lwo.model.LWOEnvelopeDefinition;
import science.unlicense.format.lwo.model.LWOImage;
import science.unlicense.format.lwo.model.LWOLayer;
import science.unlicense.format.lwo.model.LWOPointList;
import science.unlicense.format.lwo.model.LWOPolygonList;
import science.unlicense.format.lwo.model.LWOPolygonTagMapping;
import science.unlicense.format.lwo.model.LWOSurfaceDefinition;
import science.unlicense.format.lwo.model.LWOTags;
import science.unlicense.format.lwo.model.LWOThumbnail;
import science.unlicense.format.lwo.model.LWOVertexMapParameter;
import science.unlicense.format.lwo.model.LWOVertexMapping;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public final class LWOUtils {

    private static final Dictionary CHUNKS = new HashDictionary();
    static {
        CHUNKS.add(CHUNK_LAYER,                 LWOLayer.class);
        CHUNKS.add(CHUNK_POINT_LIST,            LWOPointList.class);
        CHUNKS.add(CHUNK_VERTEX_MAPPING,        LWOVertexMapping.class);
        CHUNKS.add(CHUNK_POLYGON_LIST,          LWOPolygonList.class);
        CHUNKS.add(CHUNK_TAGS,                  LWOTags.class);
        CHUNKS.add(CHUNK_POLYGON_TAG_MAPPING,   LWOPolygonTagMapping.class);
        CHUNKS.add(CHUNK_DISC_VERTEX_MAPPING,   LWODiscVertexMapping.class);
        CHUNKS.add(CHUNK_VERTEX_MAP_PARAM,      LWOVertexMapParameter.class);
        CHUNKS.add(CHUNK_ENVELOPE,              LWOEnvelopeDefinition.class);
        CHUNKS.add(CHUNK_IMAGE,                 LWOImage.class);
        CHUNKS.add(CHUNK_SURFACE,               LWOSurfaceDefinition.class);
        CHUNKS.add(CHUNK_BOUNDINGBOX,           LWOBoundingBox.class);
        CHUNKS.add(CHUNK_DESCRIPTION,           LWODescriptionLine.class);
        CHUNKS.add(CHUNK_COMMENTARY,            LWOCommentaryText.class);
        CHUNKS.add(CHUNK_THUMBNAIL,             LWOThumbnail.class);
    }

    private LWOUtils() {}

    public static int readVarInt(DataInputStream ds) throws IOException{
        final int b1 = ds.readUByte();
        if (b1==0xFF){
            //4 bytes int
            final int b2 = ds.readUByte();
            final int b3 = ds.readUByte();
            final int b4 = ds.readUByte();
            return (b2<<16) +  (b3<<8) + b4;
        } else {
            //2 bytes int
            final int b2 = ds.readUByte();
            return (b1<<8) + b2;
        }
    }

    public static Color readColor(DataInputStream ds) throws IOException{
        final float r = Maths.clamp(ds.readFloat(), 0, 1);
        final float g = Maths.clamp(ds.readFloat(), 0, 1);
        final float b = Maths.clamp(ds.readFloat(), 0, 1);
        return new ColorRGB(r, g, b);
    }

    public static VectorRW readVec3(DataInputStream ds) throws IOException{
        return new Vector3f64(ds.readFloat(), ds.readFloat(), ds.readFloat());
    }

    public static Chars readChars(DataInputStream ds) throws IOException{
        final Chars cs = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
        if (ds.getByteOffset()%2!=0){
            ds.skipFully(1);
        }
        return cs;
    }

    public static LWOChunk getChunk(Chars name) throws IOException{
        return getChunk(name, CHUNKS);
    }

    public static LWOChunk getChunk(Chars name, Dictionary chunkMap) throws IOException {
        Class c = (Class) chunkMap.getValue(name);
        if (c==null){
            System.out.println("Unknwoned chunk "+ name);
        }
        if (c==null) c = LWOChunk.class;
        try {
            return (LWOChunk) c.newInstance();
        } catch (InstantiationException ex) {
            throw new IOException(null, ex);
        } catch (IllegalAccessException ex) {
            throw new IOException(null, ex);
        }
    }

}
