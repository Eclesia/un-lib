
package science.unlicense.format.lwo.model;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOTags extends LWOChunk {

    public final Sequence tags = new ArraySequence();

    public void readInternal(DataInputStream ds) throws IOException {
        while (ds.getByteOffset() < (offset+length) ){
            tags.add(LWOUtils.readChars(ds));
        }
    }

}
