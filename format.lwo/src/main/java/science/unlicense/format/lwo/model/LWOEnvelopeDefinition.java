
package science.unlicense.format.lwo.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.Float64;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.lwo.LWOConstants;
import science.unlicense.format.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOEnvelopeDefinition extends LWOChunk {

    private static final Dictionary SUBCHUNKS = new HashDictionary();
    static {
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_ENV_TYPE, Type.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_ENV_PRE, PreBehavior.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_ENV_POST, PostBehavior.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_ENV_KEY, KeyFrame.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_ENV_SPAN, Interval.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_ENV_CHAN, PluginChannelModifier.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_ENV_NAME, PluginChannelName.class);
    }

    /** VX */
    public int index;

    public void readInternal(DataInputStream ds) throws IOException {
        index = LWOUtils.readVarInt(ds);
        readSubChunks(ds, SUBCHUNKS);
    }

    public static final class Type extends LWOChunk{
        public int userFormat;
        public int type;
        public void readInternal(DataInputStream ds) throws IOException {
            userFormat = ds.readUByte();
            type = ds.readUByte();
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(userFormat).concat(' ').concat(type);
        }

    }

    public static final class PreBehavior extends LWOChunk{
        public int type;
        public void readInternal(DataInputStream ds) throws IOException {
            type = ds.readUShort();
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(type);
        }
    }

    public static final class PostBehavior extends LWOChunk{
        public int type;
        public void readInternal(DataInputStream ds) throws IOException {
            type = ds.readUShort();
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(type);
        }
    }

    public static final class KeyFrame extends LWOChunk{
        public float time;
        public float value;
        public void readInternal(DataInputStream ds) throws IOException {
            time = ds.readFloat();
            value = ds.readFloat();
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(time)).concat(' ').concat(Float64.encode(value));
        }
    }

    public static final class Interval extends LWOChunk{
        public Chars type;
        public float[] params;
        public void readInternal(DataInputStream ds) throws IOException {
            type = ds.readBlockZeroTerminatedChars(4, CharEncodings.US_ASCII);
            final int nbParams = (int) (((offset+length)-ds.getByteOffset()) / 4);
            params = ds.readFloat(nbParams);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(type);
        }
    }

    public static final class PluginChannelModifier extends LWOChunk{
        public Chars name;
        public int flags;
        public byte[] data;
        public void readInternal(DataInputStream ds) throws IOException {
            name = LWOUtils.readChars(ds);
            flags = ds.readUShort();
            final int nbBytes = (int) ((offset+length)-ds.getByteOffset());
            data = ds.readFully(new byte[nbBytes]);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(name);
        }
    }

    public static final class PluginChannelName extends LWOChunk{
        public Chars name;
        public void readInternal(DataInputStream ds) throws IOException {
            name = LWOUtils.readChars(ds);
        }

        public Chars toChars() {
            return super.toChars().concat(' ').concat(name);
        }
    }

}
