

package science.unlicense.engine.jogl.openal.resource;

import com.jogamp.openal.AL;
import java.nio.ByteBuffer;
import science.unlicense.engine.jogl.openal.AudioContext;

/**
 *
 * @author Johann Sorel
 */
public class ALBuffer {

    /** buffer id : contains the audio sequence */
    private final int[] alid = new int[1];
    private int format;
    private int size;
    private int freq;
    private ByteBuffer data;

    private boolean loaded = false;

    public int getAlid() {
        return alid[0];
    }

    public int getFormat() {
        return format;
    }

    public void setFormat(int format) {
        this.format = format;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getFrequency() {
        return freq;
    }

    public void setFrequency(int freq) {
        this.freq = freq;
    }

    public ByteBuffer getData() {
        return data;
    }

    public void setData(ByteBuffer data) {
        this.data = data;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void load() {
        if (loaded) return;

        //allocate a buffer
        AudioContext.AL.alGenBuffers(1, alid, 0);
        if (AudioContext.AL.alGetError() != AL.AL_NO_ERROR) {
          throw new RuntimeException("Error generating OpenAL buffers");
        }

        //load audio data buffer
        AudioContext.AL.alBufferData(alid[0], format, data, size, freq);
    }

    /**
     * Release this buffer.
     */
    public void unload() {
        AudioContext.AL.alDeleteBuffers(1, alid, 0);
        loaded = false;
    }

}
