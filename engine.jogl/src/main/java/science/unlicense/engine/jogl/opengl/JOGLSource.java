
package science.unlicense.engine.jogl.opengl;

import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.engine.jogl.JogampContext;
import science.unlicense.gpu.api.SharedContext;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GLBinding;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;

/**
 *
 * @author Johann Sorel
 */
public class JOGLSource implements GLSource, GLEventListener{

    private final Sequence callbacks = new ArraySequence();
    private final SharedContext context = new JogampContext();

    com.jogamp.opengl.GLAutoDrawable drawable;
    com.jogamp.opengl.GL gl;
    private GL ugl;
    int x;
    int y;
    int width;
    int height;

    public JOGLSource() {
    }

    void setDrawable(GLAutoDrawable drawable) {
        this.gl = drawable.getGL();
        if (this.drawable!=null && this.drawable != drawable) {
            throw new InvalidArgumentException("Replacing already set drawable");
        }
        if (this.drawable != null) return;
        this.drawable = drawable;
        this.drawable.addGLEventListener(this);
    }

    @Override
    public GLBinding getBinding() {
        return JOGLBinding.INSTANCE;
    }

    @Override
    public SharedContext getContext() {
        return context;
    }

    @Override
    public GL getGL() {
        if (ugl==null) ugl = new JGL(gl);
        return ugl;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public Sequence getCallbacks() {
        return callbacks;
    }

    @Override
    public void render() {
        drawable.display();
    }

    @Override
    public void dispose() {
        drawable.destroy();
    }

    private void renderInternal(GLAutoDrawable glad) {
        setDrawable(glad);
        for (int i=0,n=callbacks.getSize();i<n;i++) {
            ((GLCallback) callbacks.get(i)).execute(this);
        }
    }

    @Override
    public void init(GLAutoDrawable glad) {
        setDrawable(glad);
    }

    @Override
    public void display(GLAutoDrawable glad) {
        renderInternal(glad);
    }

    @Override
    public void reshape(GLAutoDrawable glad, int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        setDrawable(glad);
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
        setDrawable(glad);
        for (int i=0,n=callbacks.getSize();i<n;i++) {
            ((GLCallback) callbacks.get(i)).dispose(this);
        }
    }
}
