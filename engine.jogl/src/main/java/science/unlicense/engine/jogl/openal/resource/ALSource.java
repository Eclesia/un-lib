
package science.unlicense.engine.jogl.openal.resource;

import com.jogamp.openal.AL;
import science.unlicense.engine.jogl.openal.AudioContext;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * A source is a located speaker in OpenAL.
 *
 * @author Johann Sorel
 */
public class ALSource {

    /** source id :  */
    private final int[] alid = new int[1];

    private final VectorRW position = VectorNf64.createDouble(3);
    private final VectorRW velocity = VectorNf64.createDouble(3);

    private boolean playing;
    private boolean paused;
    private boolean loop = false;
    private boolean loaded = false;

    private float gain = 1f;
    private float pitch = 1f;

    private ALBuffer buffer;

    public int[] getALId() {
        return alid;
    }

    /**
     * Play the audio sequence if paused or stopped.
     * No effect if sequence is already running.
     */
    public void play() {
        AudioContext.AL.alSourcePlay(alid[0]);
    }

    /**
     * @return true if audio sequence is playing.
     */
    public boolean isPlaying() {
        return playing;
    }

    /**
     * Pause the audio sequence if playing.
     * No effect otherwise.
     */
    public void pause() {
        AudioContext.AL.alSourcePause(alid[0]);
    }

    /**
     * @return true if audio sequence is paused.
     */
    public boolean isPaused() {
        return paused;
    }

    /**
     * Stop the audio sequence. returning to the begining.
     */
    public void stop() {
        AudioContext.AL.alSourceStop(alid[0]);
    }

    /**
     * Set the audio gain.
     * @param gain
     */
    public void setGain(float gain) {
        this.gain = gain;
    }

    /**
     * Get the audio gain.
     * @return float
     */
    public float getGain() {
        return gain;
    }

    /**
     * Set audio pitch.
     * @param pitch
     */
    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    /**
     * Get audio pitch.
     * @return float
     */
    public float getPitch() {
        return pitch;
    }

    /**
     * Set the audio sequence to loop.
     * @param loop audio sequence
     */
    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    /**
     * Is audio sequence looping.
     * Means restart from begining when finished.
     * @return true if loop
     */
    public boolean isLoop() {
        return loop;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setBuffer(ALBuffer buffer) {
        this.buffer = buffer;
    }

    public void load() {
        if (loaded) return;

        //generate source
        AudioContext.AL.alGenSources(1, alid, 0);

        AudioContext.AL.alSourcef(alid[0], AL.AL_PITCH, pitch);
        AudioContext.AL.alSourcef(alid[0], AL.AL_GAIN, gain);
        AudioContext.AL.alSourcei(alid[0], AL.AL_LOOPING, loop?1:0);
        AudioContext.AL.alSourcefv(alid[0], AL.AL_POSITION, position.toFloat(), 0);
        AudioContext.AL.alSourcefv(alid[0], AL.AL_VELOCITY, velocity.toFloat(), 0);

        if (AudioContext.AL.alGetError() != AL.AL_NO_ERROR) {
          throw new RuntimeException("Failed to configure audio sequence");
        }

        //attach buffer
        AudioContext.AL.alSourcei(alid[0], AL.AL_BUFFER, buffer.getAlid());

        if (AudioContext.AL.alGetError() != AL.AL_NO_ERROR) {
            throw new RuntimeException("Failed to load audio sequence");
        }

        loaded = true;
    }

    /**
     * Release this source.
     */
    public void unload() {
        AudioContext.AL.alDeleteSources(1, alid, 0);
        loaded = false;
    }

}
