package science.unlicense.engine.jogl.openal;

import java.nio.ByteBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Resources;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import static science.unlicense.engine.jogl.openal.AudioContext.AL;
import science.unlicense.engine.jogl.openal.resource.ALBuffer;
import science.unlicense.engine.jogl.openal.resource.ALSource;
import science.unlicense.media.api.AudioPacket;
import science.unlicense.media.api.AudioSamples;
import science.unlicense.media.api.AudioTrack;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.Medias;

/**
 *
 * @author Johann Sorel
 */
public class RSFunctions {

    //TODO : make a real cache, weak/soft/phantom references.
    private static final Dictionary CACHE = new HashDictionary();

    private RSFunctions(){}

    private static synchronized ALBuffer read(Chars pathStr) throws IOException, StoreException {
        ALBuffer data = (ALBuffer) CACHE.getValue(pathStr);
        if (data!=null) return data;

        final Path path = Paths.resolve(pathStr);

        final Store store = Medias.open(path);
        final Media media = (Media) Resources.findFirst(store, Media.class);

        //tranform audio in a supported byte buffer
        final AudioTrack meta = (AudioTrack) media.getTracks()[0];
        final MediaReadStream reader = media.createReader(null);

        //recode stream in a stereo 16 bits per sample.
        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ds = new DataOutputStream(out, Endianness.LITTLE_ENDIAN);

        for (MediaPacket pack = reader.next(); pack != null; pack = reader.next()) {
            final AudioSamples samples = ((AudioPacket) pack).getSamples();
            final int[] audiosamples = samples.asPCM(null, 16);
            if (audiosamples.length == 1) {
                ds.writeUShort(audiosamples[0]);
                ds.writeUShort(audiosamples[0]);
            } else {
                ds.writeUShort(audiosamples[0]);
                ds.writeUShort(audiosamples[1]);
            }
        }

        final byte[] all = out.getBuffer().toArrayByte();

        //buffer which contains audio datas
        data = new ALBuffer();
        data.setFormat(AL.AL_FORMAT_STEREO16);
        data.setFrequency((int) meta.getSampleRate());
        data.setSize(all.length);
        data.setData(ByteBuffer.wrap(all));
        data.load();

        CACHE.add(pathStr, data);

        return data;
    }

    public static void playAudio(final Chars pathStr) throws IOException {
        new Thread() {

            @Override
            public void run() {
                try{
                    final ALBuffer data = read(pathStr);

                    //the audio source
                    final ALSource source = new ALSource();
                    source.setBuffer(data);
                    source.load();
                    source.play();

                    sleep(3000);
                    source.stop();
                    source.unload();
                }catch(Exception ex) {
                    ex.printStackTrace();
                }
            }

        }.start();

    }

}
