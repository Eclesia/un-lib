
package science.unlicense.engine.jogl.opengl;

import com.jogamp.opengl.GL4;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.exception.UnimplementedException;
import static science.unlicense.gpu.api.GLBuffer.*;
import science.unlicense.gpu.api.opengl.GL1;
import science.unlicense.gpu.api.opengl.GL1ES;
import science.unlicense.gpu.api.opengl.GL2;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GL2ES3;
import science.unlicense.gpu.api.opengl.GL3;
import science.unlicense.gpu.api.opengl.GL45;

/**
 *
 * @author Johann Sorel
 */
public class JGL4 implements GL45,science.unlicense.gpu.api.opengl.GL4{

    private JGL base;
    private GL4 gl;

    JGL4(JGL gl) {
        this.base = gl;
        this.gl = gl.gl.getGL4();
    }

    @Override
    public BufferFactory getBufferFactory() {
        return base.getBufferFactory();
    }

    @Override
    public boolean isGL1() {
        return base.isGL1();
    }

    @Override
    public boolean isGL2() {
        return base.isGL2();
    }

    @Override
    public boolean isGL3() {
        return base.isGL3();
    }

    @Override
    public boolean isGL4() {
        return base.isGL4();
    }

    @Override
    public boolean isGL1ES() {
        return base.isGL1ES();
    }

    @Override
    public boolean isGL2ES2() {
        return base.isGL2ES2();
    }

    @Override
    public boolean isGL2ES3() {
        return base.isGL2ES3();
    }

    public GL1 asGL1() {
        return base.asGL1();
    }

    public GL2 asGL2() {
        return base.asGL2();
    }

    public GL3 asGL3() {
        return base.asGL3();
    }

    public science.unlicense.gpu.api.opengl.GL4 asGL4() {
        return this;
    }

    public GL1ES asGL1ES() {
        return base.asGL1ES();
    }

    public GL2ES2 asGL2ES2() {
        return base.asGL2ES2();
    }

    public GL2ES3 asGL2ES3() {
        return base.asGL2ES3();
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 4.0 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glMinSampleShading(float value) {
        gl.glMinSampleShading(value);
    }

    @Override
    public void glBlendEquationi(int buf, int mode) {
        gl.glBlendEquationi(buf, mode);
    }

    @Override
    public void glBlendEquationSeparatei(int buf, int modeRGB, int modeAlpha) {
        gl.glBlendEquationSeparatei(buf, modeRGB, modeAlpha);
    }

    @Override
    public void glBlendFunci(int buf, int src, int dst) {
        gl.glBlendFunci(buf, src, dst);
    }

    @Override
    public void glBlendFuncSeparatei(int buf, int srcRGB, int dstRGB, int srcAlpha, int dstAlpha) {
        gl.glBlendFuncSeparatei(buf, srcRGB, dstRGB, srcAlpha, dstAlpha);
    }

    @Override
    public void glDrawArraysIndirect(int mode, long indirect) {
        gl.glDrawArraysIndirect(mode, indirect);
    }

    @Override
    public void glDrawElementsIndirect(int mode, int type, long indirect) {
        gl.glDrawElementsIndirect(mode, type, indirect);
    }

    @Override
    public void glUniform1d(int location, double x) {
        gl.glUniform1d(location, x);
    }

    @Override
    public void glUniform2d(int location, double x, double y) {
        gl.glUniform2d(location, x, y);
    }

    @Override
    public void glUniform3d(int location, double x, double y, double z) {
        gl.glUniform3d(location, x, y, z);
    }

    @Override
    public void glUniform4d(int location, double x, double y, double z, double w) {
        gl.glUniform4d(location, x, y, z, w);
    }

    @Override
    public void glUniform1dv(int location, Buffer.Float64 value) {
        gl.glUniform1dv(location, (int) value.getSize(), unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniform2dv(int location, Buffer.Float64 value) {
        gl.glUniform2dv(location, (int) value.getSize()/2, unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniform3dv(int location, Buffer.Float64 value) {
        gl.glUniform3dv(location, (int) value.getSize()/3, unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniform4dv(int location, Buffer.Float64 value) {
        gl.glUniform4dv(location, (int) value.getSize()/4, unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniformMatrix2dv(int location, boolean transpose, Buffer.Float64 value) {
        gl.glUniformMatrix2dv(location, (int) value.getSize()/4, transpose, unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniformMatrix3dv(int location, boolean transpose, Buffer.Float64 value) {
        gl.glUniformMatrix3dv(location, (int) value.getSize()/9, transpose, unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniformMatrix4dv(int location, boolean transpose, Buffer.Float64 value) {
        gl.glUniformMatrix4dv(location, (int) value.getSize()/16, transpose, unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniformMatrix2x3dv(int location, boolean transpose, Buffer.Float64 value) {
        gl.glUniformMatrix2x3dv(location, (int) value.getSize()/6, transpose, unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniformMatrix2x4dv(int location, boolean transpose, Buffer.Float64 value) {
        gl.glUniformMatrix2x4dv(location, (int) value.getSize()/8, transpose, unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniformMatrix3x2dv(int location, boolean transpose, Buffer.Float64 value) {
        gl.glUniformMatrix3x2dv(location, (int) value.getSize()/6, transpose, unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniformMatrix3x4dv(int location, boolean transpose, Buffer.Float64 value) {
        gl.glUniformMatrix3x4dv(location, (int) value.getSize()/12, transpose, unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniformMatrix4x2dv(int location, boolean transpose, Buffer.Float64 value) {
        gl.glUniformMatrix4x2dv(location, (int) value.getSize()/8, transpose, unwrapOrCopyDouble(value));
    }

    @Override
    public void glUniformMatrix4x3dv(int location, boolean transpose, Buffer.Float64 value) {
        gl.glUniformMatrix4x3dv(location, (int) value.getSize()/12, transpose, unwrapOrCopyDouble(value));
    }

    @Override
    public void glGetUniformdv(int program, int location, Buffer.Float64 params) {
        gl.glGetUniformdv(program, location, unwrapOrCopyDouble(params));
    }

    @Override
    public int glGetSubroutineUniformLocation(int program, int shadertype, CharArray name) {
        return gl.glGetSubroutineUniformLocation(program, shadertype, name.toString());
    }

    @Override
    public int glGetSubroutineIndex(int program, int shadertype, CharArray name) {
        return gl.glGetSubroutineIndex(program, shadertype, name.toString());
    }

    @Override
    public void glGetActiveSubroutineUniformiv(int program, int shadertype, int index, int pname, Buffer.Int32 values) {
        gl.glGetActiveSubroutineUniformiv(program, shadertype, index, pname, unwrapOrCopyInt(values));
    }

    @Override
    public void glGetActiveSubroutineUniformName(int program, int shadertype, int index, Buffer.Int32 length, Buffer.Int8 name) {
        gl.glGetActiveSubroutineUniformName(program, shadertype, index, (int) name.getSize(), unwrapOrCopyInt(length), unwrapOrCopyByte(name));
    }

    @Override
    public void glGetActiveSubroutineName(int program, int shadertype, int index, Buffer.Int32 length, Buffer.Int8 name) {
        gl.glGetActiveSubroutineName(program, shadertype, index, (int) name.getSize(), unwrapOrCopyInt(length), unwrapOrCopyByte(name));
    }

    @Override
    public void glUniformSubroutinesuiv(int shadertype, Buffer.Int32 indices) {
        gl.glUniformSubroutinesuiv(shadertype, (int) indices.getSize(), unwrapOrCopyInt(indices));
    }

    @Override
    public void glGetUniformSubroutineuiv(int shadertype, int location, Buffer.Int32 params) {
        gl.glGetUniformSubroutineuiv(shadertype, location, unwrapOrCopyInt(params));
    }

    @Override
    public void glGetProgramStageiv(int program, int shadertype, int pname, Buffer.Int32 values) {
        gl.glGetProgramStageiv(program, shadertype, pname, unwrapOrCopyInt(values));
    }

    @Override
    public void glPatchParameteri(int pname, int value) {
        gl.glPatchParameteri(pname, value);
    }

    @Override
    public void glPatchParameterfv(int pname, Buffer.Float32 values) {
        gl.glPatchParameterfv(pname, unwrapOrCopyFloat(values));
    }

    @Override
    public void glBindTransformFeedback(int target, int id) {
        gl.glBindTransformFeedback(target, id);
    }

    @Override
    public void glDeleteTransformFeedbacks(Buffer.Int32 ids) {
        gl.glDeleteTransformFeedbacks((int) ids.getSize(), unwrapOrCopyInt(ids));
    }

    @Override
    public void glGenTransformFeedbacks(Buffer.Int32 ids) {
        gl.glGenTransformFeedbacks((int) ids.getSize(), unwrapOrCopyInt(ids));
    }

    @Override
    public void glGenTransformFeedbacks(int[] ids) {
        gl.glGenTransformFeedbacks(ids.length, ids,0);
    }

    @Override
    public boolean glIsTransformFeedback(int id) {
        return gl.glIsTransformFeedback(id);
    }

    @Override
    public void glPauseTransformFeedback() {
        gl.glPauseTransformFeedback();
    }

    @Override
    public void glResumeTransformFeedback() {
        gl.glResumeTransformFeedback();
    }

    @Override
    public void glDrawTransformFeedback(int mode, int id) {
        gl.glDrawTransformFeedback(mode, id);
    }

    @Override
    public void glDrawTransformFeedbackStream(int mode, int id, int stream) {
        gl.glDrawTransformFeedbackStream(mode, id, stream);
    }

    @Override
    public void glBeginQueryIndexed(int target, int index, int id) {
        gl.glBeginQueryIndexed(target, index, id);
    }

    @Override
    public void glEndQueryIndexed(int target, int index) {
        gl.glEndQueryIndexed(target, index);
    }

    @Override
    public void glGetQueryIndexediv(int target, int index, int pname, Buffer.Int32 params) {
        gl.glGetQueryIndexediv(target, index, pname, unwrapOrCopyInt(params));
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 4.1 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glReleaseShaderCompiler() {
        gl.glReleaseShaderCompiler();
    }

    @Override
    public void glShaderBinary(Buffer.Int32 shaders, int binaryformat, Buffer.Int8 binary) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetShaderPrecisionFormat(int shadertype, int precisiontype, Buffer.Int32 range, Buffer.Int32 precision) {
        gl.glGetShaderPrecisionFormat(shadertype, precisiontype, unwrapOrCopyInt(range), unwrapOrCopyInt(precision));
    }

    @Override
    public void glDepthRangef(float n, float f) {
        gl.glDepthRangef(n, f);
    }

    @Override
    public void glClearDepthf(float d) {
        gl.glClearDepthf(d);
    }

    @Override
    public void glGetProgramBinary(int program, Buffer.Int32 length, Buffer.Int32 binaryFormat, Buffer.Int8 binary) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramBinary(int program, int binaryFormat, Buffer.Int8 binary) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramParameteri(int program, int pname, int value) {
        gl.glProgramParameteri(program, pname, value);
    }

    @Override
    public void glUseProgramStages(int pipeline, int stages, int program) {
        gl.glUseProgramStages(pipeline, stages, program);
    }

    @Override
    public void glActiveShaderProgram(int pipeline, int program) {
        gl.glActiveShaderProgram(pipeline, program);
    }

    @Override
    public int glCreateShaderProgramv(int type, Buffer.Int8 strings) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindProgramPipeline(int pipeline) {
        gl.glBindProgramPipeline(pipeline);
    }

    @Override
    public void glDeleteProgramPipelines(Buffer.Int32 pipelines) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGenProgramPipelines(Buffer.Int32 pipelines) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean glIsProgramPipeline(int pipeline) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetProgramPipelineiv(int pipeline, int pname, Buffer.Int32 params) {
        gl.glGetProgramPipelineiv(pipeline, pname, unwrapOrCopyInt(params));
    }

    @Override
    public void glProgramUniform1i(int program, int location, int v0) {
        gl.glProgramUniform1i(program, location, v0);
    }

    @Override
    public void glProgramUniform1iv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform1f(int program, int location, float v0) {
        gl.glProgramUniform1f(program, location, v0);
    }

    @Override
    public void glProgramUniform1fv(int program, int location, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform1d(int program, int location, double v0) {
        gl.glProgramUniform1d(program, location, v0);
    }

    @Override
    public void glProgramUniform1dv(int program, int location, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform1ui(int program, int location, int v0) {
        gl.glProgramUniform1ui(program, location, v0);
    }

    @Override
    public void glProgramUniform1uiv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform2i(int program, int location, int v0, int v1) {
        gl.glProgramUniform2i(program, location, v0, v1);
    }

    @Override
    public void glProgramUniform2iv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform2f(int program, int location, float v0, float v1) {
        gl.glProgramUniform2f(program, location, v0, v1);
    }

    @Override
    public void glProgramUniform2fv(int program, int location, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform2d(int program, int location, double v0, double v1) {
        gl.glProgramUniform2d(program, location, v0, v1);
    }

    @Override
    public void glProgramUniform2dv(int program, int location, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform2ui(int program, int location, int v0, int v1) {
        gl.glProgramUniform2ui(program, location, v0, v1);
    }

    @Override
    public void glProgramUniform2uiv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform3i(int program, int location, int v0, int v1, int v2) {
        gl.glProgramUniform3i(program, location, v0, v1, v2);
    }

    @Override
    public void glProgramUniform3iv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform3f(int program, int location, float v0, float v1, float v2) {
        gl.glProgramUniform3f(program, location, v0, v1, v2);
    }

    @Override
    public void glProgramUniform3fv(int program, int location, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform3d(int program, int location, double v0, double v1, double v2) {
        gl.glProgramUniform3d(program, location, v0, v1, v2);
    }

    @Override
    public void glProgramUniform3dv(int program, int location, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform3ui(int program, int location, int v0, int v1, int v2) {
        gl.glProgramUniform3ui(program, location, v0, v1, v2);
    }

    @Override
    public void glProgramUniform3uiv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform4i(int program, int location, int v0, int v1, int v2, int v3) {
        gl.glProgramUniform4i(program, location, v0, v1, v2, v3);
    }

    @Override
    public void glProgramUniform4iv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform4f(int program, int location, float v0, float v1, float v2, float v3) {
        gl.glProgramUniform4f(program, location, v0, v1, v2, v3);
    }

    @Override
    public void glProgramUniform4fv(int program, int location, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform4d(int program, int location, double v0, double v1, double v2, double v3) {
        gl.glProgramUniform4d(program, location, v0, v1, v2, v3);
    }

    @Override
    public void glProgramUniform4dv(int program, int location, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform4ui(int program, int location, int v0, int v1, int v2, int v3) {
        gl.glProgramUniform4ui(program, location, v0, v1, v2, v3);
    }

    @Override
    public void glProgramUniform4uiv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix2fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix3fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix4fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix2dv(int program, int location, boolean transpose, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix3dv(int program, int location, boolean transpose, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix4dv(int program, int location, boolean transpose, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix2x3fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix3x2fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix2x4fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix4x2fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix3x4fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix4x3fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix2x3dv(int program, int location, boolean transpose, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix3x2dv(int program, int location, boolean transpose, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix2x4dv(int program, int location, boolean transpose, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix4x2dv(int program, int location, boolean transpose, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix3x4dv(int program, int location, boolean transpose, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix4x3dv(int program, int location, boolean transpose, Buffer.Float64 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glValidateProgramPipeline(int pipeline) {
        gl.glValidateProgramPipeline(pipeline);
    }

    @Override
    public void glGetProgramPipelineInfoLog(int pipeline, Buffer.Int32 length, Buffer.Int8 infoLog) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribL1d(int index, double x) {
        gl.glVertexAttribL1d(index, x);
    }

    @Override
    public void glVertexAttribL2d(int index, double x, double y) {
        gl.glVertexAttribL2d(index, x, y);
    }

    @Override
    public void glVertexAttribL3d(int index, double x, double y, double z) {
        gl.glVertexAttribL3d(index, x, y, z);
    }

    @Override
    public void glVertexAttribL4d(int index, double x, double y, double z, double w) {
        gl.glVertexAttribL4d(index, x, y, z, w);
    }

    @Override
    public void glVertexAttribL1dv(int index, Buffer.Float64 v) {
        gl.glVertexAttribL1dv(index, unwrapOrCopyDouble(v));
    }

    @Override
    public void glVertexAttribL2dv(int index, Buffer.Float64 v) {
        gl.glVertexAttribL2dv(index, unwrapOrCopyDouble(v));
    }

    @Override
    public void glVertexAttribL3dv(int index, Buffer.Float64 v) {
        gl.glVertexAttribL3dv(index, unwrapOrCopyDouble(v));
    }

    @Override
    public void glVertexAttribL4dv(int index, Buffer.Float64 v) {
        gl.glVertexAttribL4dv(index, unwrapOrCopyDouble(v));
    }

    @Override
    public void glVertexAttribLPointer(int index, int type, int stride, long pointer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetVertexAttribLdv(int index, int pname, Buffer.Float64 params) {
        gl.glGetVertexAttribLdv(index, pname, unwrapOrCopyDouble(params));
    }

    @Override
    public void glViewportArrayv(int first, int count, Buffer.Float32 v) {
        gl.glViewportArrayv(first, count, unwrapOrCopyFloat(v));
    }

    @Override
    public void glViewportIndexedf(int index, float x, float y, float w, float h) {
        gl.glViewportIndexedf(index, x, y, w, h);
    }

    @Override
    public void glViewportIndexedfv(int index, Buffer.Float32 v) {
        gl.glViewportIndexedfv(index, unwrapOrCopyFloat(v));
    }

    @Override
    public void glScissorArrayv(int first, int count, Buffer.Int32 v) {
        gl.glScissorArrayv(first, count, unwrapOrCopyInt(v));
    }

    @Override
    public void glScissorIndexed(int index, int left, int bottom, int width, int height) {
        gl.glScissorIndexed(index, left, bottom, width, height);
    }

    @Override
    public void glScissorIndexedv(int index, Buffer.Int32 v) {
        gl.glScissorIndexedv(index, unwrapOrCopyInt(v));
    }

    @Override
    public void glDepthRangeArrayv(int first, int count, Buffer.Float64 v) {
        gl.glDepthRangeArrayv(first, count, unwrapOrCopyDouble(v));
    }

    @Override
    public void glDepthRangeIndexed(int index, double n, double f) {
        gl.glDepthRangeIndexed(index, n, f);
    }

    @Override
    public void glGetFloati_v(int target, int index, Buffer.Float32 data) {
        gl.glGetFloati_v(target, index, unwrapOrCopyFloat(data));
    }

    @Override
    public void glGetDoublei_v(int target, int index, Buffer.Float64 data) {
        gl.glGetDoublei_v(target, index, unwrapOrCopyDouble(data));
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 4.2 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glDrawArraysInstancedBaseInstance(int mode, int first, int count, int instancecount, int baseinstance) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDrawElementsInstancedBaseInstance(int mode, int type, long indices, int instancecount, int baseinstance) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDrawElementsInstancedBaseVertexBaseInstance(int mode, int type, long indices, int instancecount, int basevertex, int baseinstance) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetInternalformativ(int target, int internalformat, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetActiveAtomicCounterBufferiv(int program, int bufferIndex, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindImageTexture(int unit, int texture, int level, boolean layered, int layer, int access, int format) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMemoryBarrier(int barriers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexStorage1D(int target, int levels, int internalformat, int width) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexStorage2D(int target, int levels, int internalformat, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexStorage3D(int target, int levels, int internalformat, int width, int height, int depth) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDrawTransformFeedbackInstanced(int mode, int id, int instancecount) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDrawTransformFeedbackStreamInstanced(int mode, int id, int stream, int instancecount) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 4.3 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glClearBufferData(int target, int internalformat, int format, int type, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearBufferSubData(int target, int internalformat, long offset, long size, int format, int type, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDispatchCompute(int num_groups_x, int num_groups_y, int num_groups_z) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDispatchComputeIndirect(long indirect) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCopyImageSubData(int srcName, int srcTarget, int srcLevel, int srcX, int srcY, int srcZ, int dstName, int dstTarget, int dstLevel, int dstX, int dstY, int dstZ, int srcWidth, int srcHeight, int srcDepth) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFramebufferParameteri(int target, int pname, int param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetFramebufferParameteriv(int target, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetInternalformati64v(int target, int internalformat, int pname, Buffer.Int8 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glInvalidateTexSubImage(int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glInvalidateTexImage(int texture, int level) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glInvalidateBufferSubData(int buffer, long offset, long length) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glInvalidateBufferData(int buffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glInvalidateFramebuffer(int target, Buffer.Int32 attachments) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glInvalidateSubFramebuffer(int target, Buffer.Int32 attachments, int x, int y, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiDrawArraysIndirect(int mode, Buffer.Int8 indirect, int drawcount, int stride) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiDrawElementsIndirect(int mode, int type, Buffer.Int8 indirect, int drawcount, int stride) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetProgramInterfaceiv(int program, int programInterface, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetProgramResourceIndex(int program, int programInterface, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetProgramResourceName(int program, int programInterface, int index, Buffer.Int32 length, Buffer.Int8 name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetProgramResourceiv(int program, int programInterface, int index, Buffer.Int32 props, Buffer.Int32 length, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetProgramResourceLocation(int program, int programInterface, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetProgramResourceLocationIndex(int program, int programInterface, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glShaderStorageBlockBinding(int program, int storageBlockIndex, int storageBlockBinding) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexBufferRange(int target, int internalformat, int buffer, long offset, long size) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexStorage2DMultisample(int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexStorage3DMultisample(int target, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureView(int texture, int target, int origtexture, int internalformat, int minlevel, int numlevels, int minlayer, int numlayers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindVertexBuffer(int bindingindex, int buffer, long offset, int stride) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribFormat(int attribindex, int size, int type, boolean normalized, int relativeoffset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribIFormat(int attribindex, int size, int type, int relativeoffset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribLFormat(int attribindex, int size, int type, int relativeoffset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribBinding(int attribindex, int bindingindex) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexBindingDivisor(int bindingindex, int divisor) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDebugMessageControl(int source, int type, int severity, Buffer.Int32 ids, boolean enabled) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDebugMessageInsert(int source, int type, int id, int severity, int length, CharArray buf) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDebugMessageCallback(Object callback, long userParam) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetDebugMessageLog(Buffer.Int32 sources, Buffer.Int32 types, Buffer.Int32 ids, Buffer.Int32 severities, Buffer.Int32 lengths, Buffer.Int8 messageLog) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glPushDebugGroup(int source, int id, int length, CharArray message) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glPopDebugGroup() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glObjectLabel(int identifier, int name, int length, CharArray label) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetObjectLabel(int identifier, int name, Buffer.Int32 length, Buffer.Int8 label) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glObjectPtrLabel(long ptr, int length, CharArray label) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetObjectPtrLabel(long ptr, Buffer.Int32 length, Buffer.Int8 label) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetPointerv(int pname, Buffer.Int8 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 4.4 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glBufferStorage(int target, Buffer.Int8 data, int flags) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearTexImage(int texture, int level, int format, int type, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearTexSubImage(int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindBuffersBase(int target, int first, Buffer.Int32 buffers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindBuffersRange(int target, int first, Buffer.Int32 buffers, Buffer.Int8 offsets, Buffer.Int8 sizes) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindTextures(int first, Buffer.Int32 textures) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindSamplers(int first, Buffer.Int32 samplers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindImageTextures(int first, Buffer.Int32 textures) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindVertexBuffers(int first, Buffer.Int32 buffers, Buffer.Int8 offsets, Buffer.Int32 strides) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    ////////////////////////////////////////////////////////////////////////////
    // GL 4.5 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glClipControl(int origin, int depth) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCreateTransformFeedbacks(int n, Buffer.Int32 ids) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTransformFeedbackBufferBase(int xfb, int index, int buffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTransformFeedbackBufferRange(int xfb, int index, int buffer, long offset, long size) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTransformFeedbackiv(int xfb, int pname, Buffer.Int32 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTransformFeedbacki_v(int xfb, int pname, int index, Buffer.Int32 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTransformFeedbacki64_v(int xfb, int pname, int index, Buffer.Int8 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCreateBuffers(int n, Buffer.Int32 buffers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedBufferStorage(int buffer, Buffer.Int8 data, int flags) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedBufferData(int buffer, long size, Buffer.Int8 data, int usage) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedBufferSubData(int buffer, long offset, long size, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCopyNamedBufferSubData(int readBuffer, int writeBuffer, long readOffset, long writeOffset, long size) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearNamedBufferData(int buffer, int internalformat, int format, int type, Buffer.Int8 data) {
        gl.glClearNamedBufferData(buffer, internalformat, format, type, unwrapOrCopyByte(data));
    }

    @Override
    public void glClearNamedBufferSubData(int buffer, int internalformat, long offset, long size, int format, int type, long data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMapNamedBuffer(int buffer, int access) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMapNamedBufferRange(int buffer, long offset, long length, int access) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean glUnmapNamedBuffer(int buffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFlushMappedNamedBufferRange(int buffer, long offset, long length) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetNamedBufferParameteriv(int buffer, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetNamedBufferParameteri64v(int buffer, int pname, Buffer.Int8 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetNamedBufferPointerv(int buffer, int pname, Buffer.Int8 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetNamedBufferSubData(int buffer, long offset, long size, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCreateFramebuffers(int n, Buffer.Int32 framebuffers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedFramebufferRenderbuffer(int framebuffer, int attachment, int renderbuffertarget, int renderbuffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedFramebufferParameteri(int framebuffer, int pname, int param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedFramebufferTexture(int framebuffer, int attachment, int texture, int level) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedFramebufferTextureLayer(int framebuffer, int attachment, int texture, int level, int layer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedFramebufferDrawBuffer(int framebuffer, int buf) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedFramebufferDrawBuffers(int framebuffer, int n, Buffer.Int32 bufs) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedFramebufferReadBuffer(int framebuffer, int src) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glInvalidateNamedFramebufferData(int framebuffer, int numAttachments, Buffer.Int32 attachments) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glInvalidateNamedFramebufferSubData(int framebuffer, int numAttachments, Buffer.Int32 attachments, int x, int y, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearNamedFramebufferiv(int framebuffer, int buffer, int drawbuffer, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearNamedFramebufferuiv(int framebuffer, int buffer, int drawbuffer, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearNamedFramebufferfv(int framebuffer, int buffer, int drawbuffer, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearNamedFramebufferfi(int framebuffer, int buffer, int drawbuffer, float depth, int stencil) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBlitNamedFramebuffer(int readFramebuffer, int drawFramebuffer, int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, int mask, int filter) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glCheckNamedFramebufferStatus(int framebuffer, int target) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetNamedFramebufferParameteriv(int framebuffer, int pname, Buffer.Int32 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetNamedFramebufferAttachmentParameteriv(int framebuffer, int attachment, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCreateRenderbuffers(int n, Buffer.Int32 renderbuffers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedRenderbufferStorage(int renderbuffer, int internalformat, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNamedRenderbufferStorageMultisample(int renderbuffer, int samples, int internalformat, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetNamedRenderbufferParameteriv(int renderbuffer, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCreateTextures(int target, int n, Buffer.Int32 textures) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureBuffer(int texture, int internalformat, int buffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureBufferRange(int texture, int internalformat, int buffer, long offset, long size) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureStorage1D(int texture, int levels, int internalformat, int width) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureStorage2D(int texture, int levels, int internalformat, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureStorage3D(int texture, int levels, int internalformat, int width, int height, int depth) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureStorage2DMultisample(int texture, int samples, int internalformat, int width, int height, boolean fixedsamplelocations) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureStorage3DMultisample(int texture, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureSubImage1D(int texture, int level, int xoffset, int width, int format, int type, Buffer.Int8 pixels) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureSubImage2D(int texture, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer.Int8 pixels) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureSubImage3D(int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, Buffer.Int8 pixels) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCompressedTextureSubImage1D(int texture, int level, int xoffset, int width, int format, int imageSize, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCompressedTextureSubImage2D(int texture, int level, int xoffset, int yoffset, int width, int height, int format, int imageSize, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCompressedTextureSubImage3D(int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int imageSize, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCopyTextureSubImage1D(int texture, int level, int xoffset, int x, int y, int width) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCopyTextureSubImage2D(int texture, int level, int xoffset, int yoffset, int x, int y, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCopyTextureSubImage3D(int texture, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureParameterf(int texture, int pname, float param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureParameterfv(int texture, int pname, Buffer.Float32 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureParameteri(int texture, int pname, int param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureParameterIiv(int texture, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureParameterIuiv(int texture, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureParameteriv(int texture, int pname, Buffer.Int32 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGenerateTextureMipmap(int texture) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindTextureUnit(int unit, int texture) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTextureImage(int texture, int level, int format, int type, int bufSize, Buffer.Int8 pixels) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetCompressedTextureImage(int texture, int level, int bufSize, Buffer.Int8 pixels) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTextureLevelParameterfv(int texture, int level, int pname, Buffer.Float32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTextureLevelParameteriv(int texture, int level, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTextureParameterfv(int texture, int pname, Buffer.Float32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTextureParameterIiv(int texture, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTextureParameterIuiv(int texture, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTextureParameteriv(int texture, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCreateVertexArrays(int n, Buffer.Int32 arrays) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDisableVertexArrayAttrib(int vaobj, int index) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glEnableVertexArrayAttrib(int vaobj, int index) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexArrayElementBuffer(int vaobj, int buffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexArrayVertexBuffer(int vaobj, int bindingindex, int buffer, long offset, int stride) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexArrayVertexBuffers(int vaobj, int first, int count, Buffer.Int32 buffers, Buffer.Int8 offsets, Buffer.Int32 strides) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexArrayAttribBinding(int vaobj, int attribindex, int bindingindex) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexArrayAttribFormat(int vaobj, int attribindex, int size, int type, boolean normalized, int relativeoffset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexArrayAttribIFormat(int vaobj, int attribindex, int size, int type, int relativeoffset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexArrayAttribLFormat(int vaobj, int attribindex, int size, int type, int relativeoffset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexArrayBindingDivisor(int vaobj, int bindingindex, int divisor) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetVertexArrayiv(int vaobj, int pname, Buffer.Int32 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetVertexArrayIndexediv(int vaobj, int index, int pname, Buffer.Int32 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetVertexArrayIndexed64iv(int vaobj, int index, int pname, Buffer.Int8 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCreateSamplers(int n, Buffer.Int32 samplers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCreateProgramPipelines(int n, Buffer.Int32 pipelines) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCreateQueries(int target, int n, Buffer.Int32 ids) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetQueryBufferObjecti64v(int id, int buffer, int pname, long offset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetQueryBufferObjectiv(int id, int buffer, int pname, long offset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetQueryBufferObjectui64v(int id, int buffer, int pname, long offset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetQueryBufferObjectuiv(int id, int buffer, int pname, long offset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMemoryBarrierByRegion(int barriers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTextureSubImage(int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, int bufSize, Buffer.Int8 pixels) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetCompressedTextureSubImage(int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int bufSize, Buffer.Int8 pixels) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetGraphicsResetStatus() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnCompressedTexImage(int target, int lod, int bufSize, Buffer.Int8 pixels) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnTexImage(int target, int level, int format, int type, int bufSize, Buffer.Int8 pixels) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnUniformdv(int program, int location, int bufSize, Buffer.Float64 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnUniformfv(int program, int location, int bufSize, Buffer.Float32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnUniformiv(int program, int location, int bufSize, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnUniformuiv(int program, int location, int bufSize, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glReadnPixels(int x, int y, int width, int height, int format, int type, int bufSize, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnMapdv(int target, int query, int bufSize, Buffer.Float64 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnMapfv(int target, int query, int bufSize, Buffer.Float32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnMapiv(int target, int query, int bufSize, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnPixelMapfv(int map, int bufSize, Buffer.Float32 values) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnPixelMapuiv(int map, int bufSize, Buffer.Int32 values) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnPixelMapusv(int map, int bufSize, Buffer.Int16 values) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnPolygonStipple(int bufSize, Buffer.Int8 pattern) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnColorTable(int target, int format, int type, int bufSize, Buffer.Int8 table) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnConvolutionFilter(int target, int format, int type, int bufSize, Buffer.Int8 image) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnSeparableFilter(int target, int format, int type, int rowBufSize, Buffer.Int8 row, int columnBufSize, Buffer.Int8 column, Buffer.Int8 span) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnHistogram(int target, boolean reset, int format, int type, int bufSize, Buffer.Int8 values) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnMinmax(int target, boolean reset, int format, int type, int bufSize, Buffer.Int8 values) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTextureBarrier() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
