

package science.unlicense.engine.jogl.openal;

import com.jogamp.openal.AL;
import com.jogamp.openal.ALException;
import com.jogamp.openal.ALFactory;
import com.jogamp.openal.util.ALut;

/**
 *
 * @author Johann Sorel
 */
public class AudioContext {

    // JOAL loading
    public static final AL AL;

    static {
        //initialize OpenAL
        try {
            ALut.alutInit();
            AL = ALFactory.getAL();
            AL.alGetError();
        } catch (ALException ex) {
            throw new RuntimeException("Failed to initialize OpenAL : " + ex.getMessage(), ex);
        }

        // Listener : TODO move this in the camera
        final float[] listenerPos = new float[]{0f,0f,0f};
        final float[] listenerVel = new float[]{0f,0f,0f};
        // 3 * 2 : direction + up vectors
        final float[] listenerOri = {0f,0f,-1f,  0f,1f,0f};
        AL.alListenerfv(AL.AL_POSITION, listenerPos, 0);
        AL.alListenerfv(AL.AL_VELOCITY, listenerVel, 0);
        AL.alListenerfv(AL.AL_ORIENTATION, listenerOri, 0);
    }

    public static final AudioContext INSTANCE = new AudioContext();

    private AudioContext() {

    }


}
