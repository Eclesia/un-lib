
package science.unlicense.engine.jogl.openal;

import science.unlicense.engine.jogl.openal.resource.ALBuffer;
import science.unlicense.engine.jogl.openal.resource.ALSource;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.geometry.api.system.CoordinateSystems;

/**
 * A speaker emit a serie of audio sequences.
 *
 * @author Johann Sorel
 */
public abstract class Speaker extends DefaultSceneNode {

    private final ALSource source = new ALSource();
    private ALBuffer data;

    public Speaker() {
        super(CoordinateSystems.UNDEFINED_3D);
    }

    public ALSource getSource() {
        return source;
    }

    public ALBuffer getData() {
        return data;
    }

    public void setData(ALBuffer data) {
        this.data = data;
    }

    public void load() {
        source.load();
        if (data!=null) data.load();
    }

    public void unload() {
        source.unload();
        if (data!=null) data.unload();
    }

}
