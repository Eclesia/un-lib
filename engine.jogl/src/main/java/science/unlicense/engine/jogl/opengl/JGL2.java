
package science.unlicense.engine.jogl.opengl;

import com.jogamp.opengl.GL2;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.exception.UnimplementedException;
import static science.unlicense.gpu.api.GLBuffer.*;
import science.unlicense.gpu.api.opengl.GL1;
import science.unlicense.gpu.api.opengl.GL1ES;
import science.unlicense.gpu.api.opengl.GL21;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GL2ES3;
import science.unlicense.gpu.api.opengl.GL3;
import science.unlicense.gpu.api.opengl.GL4;

/**
 *
 * @author Johann Sorel
 */
public class JGL2 implements GL21,science.unlicense.gpu.api.opengl.GL2{

    private JGL base;
    private GL2 gl2;

    JGL2(JGL gl) {
        this.base = gl;
        this.gl2 = gl.gl.getGL2();
    }

    @Override
    public BufferFactory getBufferFactory() {
        return base.getBufferFactory();
    }

    @Override
    public boolean isGL1() {
        return base.isGL1();
    }

    @Override
    public boolean isGL2() {
        return base.isGL2();
    }

    @Override
    public boolean isGL3() {
        return base.isGL3();
    }

    @Override
    public boolean isGL4() {
        return base.isGL4();
    }

    @Override
    public boolean isGL1ES() {
        return base.isGL1ES();
    }

    @Override
    public boolean isGL2ES2() {
        return base.isGL2ES2();
    }

    @Override
    public boolean isGL2ES3() {
        return base.isGL2ES3();
    }

    public GL1 asGL1() {
        return base.asGL1();
    }

    public science.unlicense.gpu.api.opengl.GL2 asGL2() {
        return this;
    }

    public GL3 asGL3() {
        return base.asGL3();
    }

    public GL4 asGL4() {
        return base.asGL4();
    }

    public GL1ES asGL1ES() {
        return base.asGL1ES();
    }

    public GL2ES2 asGL2ES2() {
        return base.asGL2ES2();
    }

    public GL2ES3 asGL2ES3() {
        return base.asGL2ES3();
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 2.0 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glBlendEquationSeparate(int modeRGB, int modeAlpha) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDrawBuffers(Buffer.Int32 bufs) {
        gl2.glDrawBuffers((int) bufs.getSize(), unwrapOrCopyInt(bufs));
    }

    @Override
    public void glDrawBuffers(int[] bufs) {
        gl2.glDrawBuffers(bufs.length, bufs,0);
    }

    @Override
    public void glStencilOpSeparate(int face, int sfail, int dpfail, int dppass) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glStencilFuncSeparate(int face, int func, int ref, int mask) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glStencilMaskSeparate(int face, int mask) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glAttachShader(int program, int shader) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindAttribLocation(int program, int index, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCompileShader(int shader) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glCreateProgram() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glCreateShader(int type) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDeleteProgram(int program) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDeleteShader(int shader) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDetachShader(int program, int shader) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDisableVertexAttribArray(int index) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glEnableVertexAttribArray(int index) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetActiveAttrib(int program, int index, Buffer.Int32 length, Buffer.Int32 size, Buffer.Int32 type, Buffer.Int8 name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetActiveUniform(int program, int index, Buffer.Int32 length, Buffer.Int32 size, Buffer.Int32 type, Buffer.Int8 name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetAttachedShaders(int program, Buffer.Int32 count, Buffer.Int32 shaders) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetAttribLocation(int program, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetProgramiv(int program, int pname, Buffer.Int32 params) {
        gl2.glGetProgramiv(program, pname, unwrapOrCopyInt(params));
    }

    @Override
    public void glGetProgramiv(int program, int pname, int[] params) {
        gl2.glGetProgramiv(program, pname, params,0);
    }

    @Override
    public void glGetProgramInfoLog(int program, int[] length, Buffer.Int8 infoLog) {
        gl2.glGetProgramInfoLog(program, (int) infoLog.getSize(), of(length), unwrapOrCopyByte(infoLog));
    }

    @Override
    public void glGetShaderiv(int shader, int pname, Buffer.Int32 params) {
        gl2.glGetShaderiv( shader, pname, unwrapOrCopyInt(params));
    }

    @Override
    public void glGetShaderiv(int shader, int pname, int[] params) {
        gl2.glGetShaderiv( shader, pname, params, 0);
    }

    @Override
    public void glGetShaderInfoLog(int shader, Buffer.Int32 length, Buffer.Int8 infoLog) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetShaderSource(int shader, Buffer.Int32 length, Buffer.Int8 source) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetUniformLocation(int program, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetUniformfv(int program, int location, Buffer.Float32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetUniformiv(int program, int location, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetVertexAttribdv(int index, int pname, Buffer.Float64 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetVertexAttribfv(int index, int pname, Buffer.Float32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetVertexAttribiv(int index, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetVertexAttribPointerv(int index, int pname, Buffer.Int8 pointer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean glIsProgram(int program) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean glIsShader(int shader) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glLinkProgram(int program) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glShaderSource(int shader, CharArray[] strings) {
        final String[] jvmStrings = JGL.toString(strings);
        final int[] lengths = new int[jvmStrings.length];
        for (int i=0;i<lengths.length;i++) lengths[i] = jvmStrings[i].length();
        gl2.glShaderSource(shader, jvmStrings.length, jvmStrings, lengths, 0);
    }

    @Override
    public void glUseProgram(int program) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform1f(int location, float v0) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform2f(int location, float v0, float v1) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform3f(int location, float v0, float v1, float v2) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform4f(int location, float v0, float v1, float v2, float v3) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform1i(int location, int v0) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform2i(int location, int v0, int v1) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform3i(int location, int v0, int v1, int v2) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform4i(int location, int v0, int v1, int v2, int v3) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform1fv(int location, Buffer.Float32 value) {
        gl2.glUniform1fv(location, (int) value.getSize()/1, unwrapOrCopyFloat(value));
    }

    @Override
    public void glUniform1fv(int location, float[] value) {
        gl2.glUniform1fv(location, value.length/1, value, 0);
    }

    @Override
    public void glUniform2fv(int location, Buffer.Float32 value) {
        gl2.glUniform2fv(location, (int) value.getSize()/2, unwrapOrCopyFloat(value));
    }

    @Override
    public void glUniform2fv(int location, float[] value) {
        gl2.glUniform2fv(location, value.length/2, value, 0);
    }

    @Override
    public void glUniform3fv(int location, Buffer.Float32 value) {
        gl2.glUniform3fv(location, (int) value.getSize()/3, unwrapOrCopyFloat(value));
    }

    @Override
    public void glUniform3fv(int location, float[] value) {
        gl2.glUniform3fv(location, value.length/3, value, 0);
    }

    @Override
    public void glUniform4fv(int location, Buffer.Float32 value) {
        gl2.glUniform4fv(location, (int) value.getSize()/4, unwrapOrCopyFloat(value));
    }

    @Override
    public void glUniform4fv(int location, float[] value) {
        gl2.glUniform4fv(location, value.length/4, value, 0);
    }

    @Override
    public void glUniform1iv(int location, Buffer.Int32 value) {
        gl2.glUniform1iv(location, (int) value.getSize()/1, unwrapOrCopyInt(value));
    }

    @Override
    public void glUniform1iv(int location, int[] value) {
        gl2.glUniform1iv(location, value.length/1, value, 0);
    }

    @Override
    public void glUniform2iv(int location, Buffer.Int32 value) {
        gl2.glUniform2iv(location, (int) value.getSize()/2, unwrapOrCopyInt(value));
    }

    @Override
    public void glUniform2iv(int location, int[] value) {
        gl2.glUniform2iv(location, value.length/2, value, 0);
    }

    @Override
    public void glUniform3iv(int location, Buffer.Int32 value) {
        gl2.glUniform3iv(location, (int) value.getSize()/3, unwrapOrCopyInt(value));
    }

    @Override
    public void glUniform3iv(int location, int[] value) {
        gl2.glUniform3iv(location, value.length/3, value, 0);
    }

    @Override
    public void glUniform4iv(int location, Buffer.Int32 value) {
        gl2.glUniform4iv(location, (int) value.getSize()/4, unwrapOrCopyInt(value));
    }

    @Override
    public void glUniform4iv(int location, int[] value) {
        gl2.glUniform4iv(location, value.length/4, value, 0);
    }

    @Override
    public void glUniformMatrix2fv(int location, boolean transpose, Buffer.Float32 value) {
        gl2.glUniformMatrix2fv(location, (int) value.getSize()/4, transpose, unwrapOrCopyFloat(value));
    }

    @Override
    public void glUniformMatrix2fv (int location, boolean transpose, float[] value) {
        gl2.glUniformMatrix2fv(location, value.length/4, transpose, value,0);
    }

    @Override
    public void glUniformMatrix3fv(int location, boolean transpose, Buffer.Float32 value) {
        gl2.glUniformMatrix3fv(location, (int) value.getSize()/9, transpose, unwrapOrCopyFloat(value));
    }

    @Override
    public void glUniformMatrix3fv (int location, boolean transpose, float[] value) {
        gl2.glUniformMatrix3fv(location, value.length/9, transpose, value,0);
    }

    @Override
    public void glUniformMatrix4fv(int location, boolean transpose, Buffer.Float32 value) {
        gl2.glUniformMatrix4fv(location, (int) value.getSize()/16, transpose, unwrapOrCopyFloat(value));
    }

    @Override
    public void glUniformMatrix4fv (int location, boolean transpose, float[] value) {
        gl2.glUniformMatrix4fv(location, value.length/16, transpose, value,0);
    }

    @Override
    public void glValidateProgram(int program) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib1d(int index, double x) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib1dv(int index, Buffer.Float64 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib1f(int index, float x) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib1fv(int index, Buffer.Float32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib1s(int index, short x) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib1sv(int index, Buffer.Int16 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib2d(int index, double x, double y) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib2dv(int index, Buffer.Float64 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib2f(int index, float x, float y) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib2fv(int index, Buffer.Float32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib2s(int index, short x, short y) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib2sv(int index, Buffer.Int16 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib3d(int index, double x, double y, double z) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib3dv(int index, Buffer.Float64 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib3f(int index, float x, float y, float z) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib3fv(int index, Buffer.Float32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib3s(int index, short x, short y, short z) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib3sv(int index, Buffer.Int16 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4Nbv(int index, Buffer.Int8 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4Niv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4Nsv(int index, Buffer.Int16 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4Nub(int index, byte x, byte y, byte z, byte w) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4Nubv(int index, Buffer.Int8 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4Nuiv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4Nusv(int index, Buffer.Int16 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4bv(int index, Buffer.Int8 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4d(int index, double x, double y, double z, double w) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4dv(int index, Buffer.Float64 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4f(int index, float x, float y, float z, float w) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4fv(int index, Buffer.Float32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4iv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4s(int index, short x, short y, short z, short w) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4sv(int index, Buffer.Int16 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4ubv(int index, Buffer.Int8 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4uiv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttrib4usv(int index, Buffer.Int16 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribPointer(int index, int size, int type, boolean normalized, int stride, long pointer) {
        gl2.glVertexAttribPointer(index, size, type, normalized, stride, pointer);
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 2.1 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glUniformMatrix2x3fv(int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniformMatrix3x2fv(int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniformMatrix2x4fv(int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniformMatrix4x2fv(int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniformMatrix3x4fv(int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniformMatrix4x3fv(int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
