
package science.unlicense.engine.jogl.opengl;

import com.jogamp.opengl.GL3;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.exception.UnimplementedException;
import static science.unlicense.gpu.api.GLBuffer.*;
import science.unlicense.gpu.api.opengl.GL1;
import science.unlicense.gpu.api.opengl.GL1ES;
import science.unlicense.gpu.api.opengl.GL2;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GL2ES3;
import science.unlicense.gpu.api.opengl.GL33;
import science.unlicense.gpu.api.opengl.GL4;

/**
 *
 * @author Johann Sorel
 */
public class JGL3 implements GL33,science.unlicense.gpu.api.opengl.GL3{

    private JGL base;
    private GL3 gl3;

    JGL3(JGL gl) {
        this.base = gl;
        this.gl3 = gl.gl.getGL3();
    }

    @Override
    public BufferFactory getBufferFactory() {
        return base.getBufferFactory();
    }

    @Override
    public boolean isGL1() {
        return base.isGL1();
    }

    @Override
    public boolean isGL2() {
        return base.isGL2();
    }

    @Override
    public boolean isGL3() {
        return base.isGL3();
    }

    @Override
    public boolean isGL4() {
        return base.isGL4();
    }

    @Override
    public boolean isGL1ES() {
        return base.isGL1ES();
    }

    @Override
    public boolean isGL2ES2() {
        return base.isGL2ES2();
    }

    @Override
    public boolean isGL2ES3() {
        return base.isGL2ES3();
    }

    public GL1 asGL1() {
        return base.asGL1();
    }

    public GL2 asGL2() {
        return base.asGL2();
    }

    public science.unlicense.gpu.api.opengl.GL3 asGL3() {
        return base.asGL3();
    }

    public GL4 asGL4() {
        return base.asGL4();
    }

    public GL1ES asGL1ES() {
        return base.asGL1ES();
    }

    public GL2ES2 asGL2ES2() {
        return base.asGL2ES2();
    }

    public GL2ES3 asGL2ES3() {
        return base.asGL2ES3();
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 3.0 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glColorMaski(int index, boolean r, boolean g, boolean b, boolean a) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetBooleani_v(int target, int index, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetIntegeri_v(int target, int index, Buffer.Int32 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glEnablei(int target, int index) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDisablei(int target, int index) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean glIsEnabledi(int target, int index) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBeginTransformFeedback(int primitiveMode) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glEndTransformFeedback() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindBufferRange(int target, int index, int buffer, long offset, long size) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindBufferBase(int target, int index, int buffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTransformFeedbackVaryings(int program, CharArray[] varyings, int bufferMode) {
        gl3.glTransformFeedbackVaryings(program, varyings.length, JGL.toString(varyings), program);
    }

    @Override
    public void glGetTransformFeedbackVarying(int program, int index, Buffer.Int32 length, Buffer.Int32 size, Buffer.Int32 type, Buffer.Int8 name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClampColor(int target, int clamp) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBeginConditionalRender(int id, int mode) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glEndConditionalRender() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribIPointer(int index, int size, int type, int stride, long pointer) {
        gl3.glVertexAttribIPointer(index, size, type, stride, pointer);
    }

    @Override
    public void glGetVertexAttribIiv(int index, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetVertexAttribIuiv(int index, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI1i(int index, int x) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI2i(int index, int x, int y) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI3i(int index, int x, int y, int z) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4i(int index, int x, int y, int z, int w) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI1ui(int index, int x) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI2ui(int index, int x, int y) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI3ui(int index, int x, int y, int z) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4ui(int index, int x, int y, int z, int w) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI1iv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI2iv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI3iv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4iv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI1uiv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI2uiv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI3uiv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4uiv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4bv(int index, Buffer.Int8 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4sv(int index, Buffer.Int16 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4ubv(int index, Buffer.Int8 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4usv(int index, Buffer.Int16 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetUniformuiv(int program, int location, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindFragDataLocation(int program, int color, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetFragDataLocation(int program, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform1ui(int location, int v0) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform2ui(int location, int v0, int v1) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform3ui(int location, int v0, int v1, int v2) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform4ui(int location, int v0, int v1, int v2, int v3) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform1uiv(int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform2uiv(int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform3uiv(int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniform4uiv(int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexParameterIiv(int target, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexParameterIuiv(int target, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTexParameterIiv(int target, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTexParameterIuiv(int target, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearBufferiv(int buffer, int drawbuffer, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearBufferuiv(int buffer, int drawbuffer, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearBufferfv(int buffer, int drawbuffer, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glClearBufferfi(int buffer, int drawbuffer, float depth, int stencil) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public byte glGetStringi(int name, int index) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean glIsRenderbuffer(int renderbuffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindRenderbuffer(int target, int renderbuffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDeleteRenderbuffers(Buffer.Int32 renderbuffers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGenRenderbuffers(Buffer.Int32 renderbuffers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glRenderbufferStorage(int target, int internalformat, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetRenderbufferParameteriv(int target, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean glIsFramebuffer(int framebuffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindFramebuffer(int target, int framebuffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDeleteFramebuffers(Buffer.Int32 framebuffers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGenFramebuffers(Buffer.Int32 framebuffers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glCheckFramebufferStatus(int target) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFramebufferTexture1D(int target, int attachment, int textarget, int texture, int level) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFramebufferTexture2D(int target, int attachment, int textarget, int texture, int level) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFramebufferTexture3D(int target, int attachment, int textarget, int texture, int level, int zoffset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFramebufferRenderbuffer(int target, int attachment, int renderbuffertarget, int renderbuffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetFramebufferAttachmentParameteriv(int target, int attachment, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGenerateMipmap(int target) {
        gl3.glGenerateMipmap(target);
    }

    @Override
    public void glBlitFramebuffer(int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, int mask, int filter) {
        gl3.glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
    }

    @Override
    public void glRenderbufferStorageMultisample(int target, int samples, int internalformat, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFramebufferTextureLayer(int target, int attachment, int texture, int level, int layer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMapBufferRange(int target, long offset, long length, int access) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFlushMappedBufferRange(int target, long offset, long length) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindVertexArray(int array) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDeleteVertexArrays(Buffer.Int32 arrays) {
        gl3.glDeleteVertexArrays((int) arrays.getSize(), unwrapOrCopyInt(arrays));
    }

    @Override
    public void glDeleteVertexArrays(int[] arrays) {
        gl3.glDeleteVertexArrays(arrays.length, arrays,0);
    }

    @Override
    public void glGenVertexArrays(Buffer.Int32 arrays) {
        gl3.glGenVertexArrays((int) arrays.getSize(), unwrapOrCopyInt(arrays));
    }

    @Override
    public void glGenVertexArrays(int[] arrays) {
        gl3.glGenVertexArrays(arrays.length, arrays,0);
    }

    @Override
    public boolean glIsVertexArray(int array) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 3.1 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glDrawArraysInstanced(int mode, int first, int count, int instancecount) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDrawElementsInstanced(int mode, int count, int type, long indices, int instancecount) {
        gl3.glDrawElementsInstanced(mode, count, type, indices, instancecount);
    }

    @Override
    public void glTexBuffer(int target, int internalformat, int buffer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glPrimitiveRestartIndex(int index) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCopyBufferSubData(int readTarget, int writeTarget, long readOffset, long writeOffset, long size) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetUniformIndices(int program, int uniformCount, CharArray uniformNames, Buffer.Int32 uniformIndices) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetActiveUniformsiv(int program, Buffer.Int32 uniformIndices, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetActiveUniformName(int program, int uniformIndex, Buffer.Int32 length, Buffer.Int8 uniformName) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetUniformBlockIndex(int program, CharArray uniformBlockName) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetActiveUniformBlockiv(int program, int uniformBlockIndex, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetActiveUniformBlockName(int program, int uniformBlockIndex, Buffer.Int32 length, Buffer.Int8 uniformBlockName) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUniformBlockBinding(int program, int uniformBlockIndex, int uniformBlockBinding) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 3.2 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glDrawElementsBaseVertex(int mode, int count, int type, long indices, int basevertex) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDrawRangeElementsBaseVertex(int mode, int start, int end, int count, int type, long indices, int basevertex) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDrawElementsInstancedBaseVertex(int mode, int count, int type, long indices, int instancecount, int basevertex) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiDrawElementsBaseVertex(int mode, Buffer.Int32 count, int type, int indices, int drawcount, Buffer.Int32 basevertex) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProvokingVertex(int mode) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long glFenceSync(int condition, int flags) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean glIsSync(long sync) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDeleteSync(long sync) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glClientWaitSync(long sync, int flags, long timeout) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glWaitSync(long sync, int flags, long timeout) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetInteger64v(int pname, Buffer.Int64 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetInteger64v(int pname, long[] data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetSynciv(long sync, int pname, Buffer.Int32 length, Buffer.Int32 values) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetInteger64i_v(int target, int index, Buffer.Int64 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetBufferParameteri64v(int target, int pname, Buffer.Int8 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFramebufferTexture(int target, int attachment, int texture, int level) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexImage2DMultisample(int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations) {
        gl3.glTexImage2DMultisample(target, samples, internalformat, width, height, fixedsamplelocations);
    }

    @Override
    public void glTexImage3DMultisample(int target, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations) {
        gl3.glTexImage3DMultisample(target, samples, internalformat, width, height, depth, fixedsamplelocations);
    }

    @Override
    public void glGetMultisamplefv(int pname, int index, Buffer.Float32 val) {
        gl3.glGetMultisamplefv(pname, index, unwrapOrCopyFloat(val));
    }

    @Override
    public void glSampleMaski(int maskNumber, int mask) {
        gl3.glSampleMaski(maskNumber, mask);
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 3.3 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glBindFragDataLocationIndexed(int program, int colorNumber, int index, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetFragDataIndex(int program, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGenSamplers(Buffer.Int32 samplers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDeleteSamplers(Buffer.Int32 samplers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean glIsSampler(int sampler) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindSampler(int unit, int sampler) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glSamplerParameteri(int sampler, int pname, int param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glSamplerParameteriv(int sampler, int pname, Buffer.Int32 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glSamplerParameterf(int sampler, int pname, float param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glSamplerParameterfv(int sampler, int pname, Buffer.Float32 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glSamplerParameterIiv(int sampler, int pname, Buffer.Int32 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glSamplerParameterIuiv(int sampler, int pname, Buffer.Int32 param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetSamplerParameteriv(int sampler, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetSamplerParameterIiv(int sampler, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetSamplerParameterfv(int sampler, int pname, Buffer.Float32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetSamplerParameterIuiv(int sampler, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glQueryCounter(int id, int target) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetQueryObjecti64v(int id, int pname, Buffer.Int8 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetQueryObjectui64v(int id, int pname, Buffer.Int8 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribDivisor(int index, int divisor) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP1ui(int index, int type, boolean normalized, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP1uiv(int index, int type, boolean normalized, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP2ui(int index, int type, boolean normalized, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP2uiv(int index, int type, boolean normalized, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP3ui(int index, int type, boolean normalized, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP3uiv(int index, int type, boolean normalized, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP4ui(int index, int type, boolean normalized, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP4uiv(int index, int type, boolean normalized, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP2ui(int type, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP2uiv(int type, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP3ui(int type, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP3uiv(int type, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP4ui(int type, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP4uiv(int type, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP1ui(int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP1uiv(int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP2ui(int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP2uiv(int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP3ui(int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP3uiv(int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP4ui(int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP4uiv(int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP1ui(int texture, int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP1uiv(int texture, int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP2ui(int texture, int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP2uiv(int texture, int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP3ui(int texture, int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP3uiv(int texture, int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP4ui(int texture, int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP4uiv(int texture, int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNormalP3ui(int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNormalP3uiv(int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glColorP3ui(int type, int color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glColorP3uiv(int type, Buffer.Int32 color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glColorP4ui(int type, int color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glColorP4uiv(int type, Buffer.Int32 color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glSecondaryColorP3ui(int type, int color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glSecondaryColorP3uiv(int type, Buffer.Int32 color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
