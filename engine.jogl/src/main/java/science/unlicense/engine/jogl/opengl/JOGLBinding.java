
package science.unlicense.engine.jogl.opengl;

import com.jogamp.nativewindow.util.InsetsImmutable;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLOffscreenAutoDrawable;
import com.jogamp.opengl.GLProfile;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.gpu.api.GLBufferFactory;
import science.unlicense.gpu.api.SharedContext;
import science.unlicense.gpu.api.SharedContextException;
import science.unlicense.gpu.api.opengl.GLBinding;
import science.unlicense.gpu.api.opengl.GLFrame;
import science.unlicense.gpu.api.opengl.GLSource;

/**
 *
 * @author Johann Sorel
 */
public class JOGLBinding implements GLBinding {

    public static final GLBinding INSTANCE = new JOGLBinding();

    private static Margin margin = null;

    @Override
    public GLSource createOffScreen(int width, int height, SharedContext context) {
        if (context != null) throw new SharedContextException();
        GLOffscreenAutoDrawable drawable = JOGLUtils.createOffscreenDrawable(width, height, GLProfile.getMaxProgrammable(true));
        JOGLSource source = new JOGLSource();
        source.setDrawable(drawable);
        return source;
    }

    @Override
    public GLFrame createFrame(boolean opaque, SharedContext context) {
        if (context != null) throw new SharedContextException();
        return new JOGLFrame(opaque);
    }

    @Override
    public synchronized Margin getFrameMargin() {
        if (margin==null) {
            GLCapabilities glCapabilities = new GLCapabilities(GLProfile.getDefault());
            GLWindow glWindow = GLWindow.create(glCapabilities);

            final InsetsImmutable in = glWindow.getInsets();
            margin =  new Margin(in.getTopHeight(), in.getRightWidth(), in.getBottomHeight(), in.getLeftWidth());
        }
        return new Margin(margin.top, margin.right, margin.bottom, margin.left);
    }

    @Override
    public BufferFactory getBufferFactory() {
        return GLBufferFactory.INSTANCE;
    }

}
