
package science.unlicense.engine.jogl.opengl;

import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLCapabilitiesImmutable;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLDrawableFactory;
import com.jogamp.opengl.GLOffscreenAutoDrawable;
import com.jogamp.opengl.GLProfile;

/**
 *
 * @author Johann Sorel
 */
public class JOGLUtils {

    private JOGLUtils(){}

    /**
     * Create an offscreen drawable.
     *
     * @return GLOffscreenAutoDrawable
     */
    public static GLOffscreenAutoDrawable createOffscreenDrawable(int width, int height, GLProfile glProfile) {
        final GLCapabilities glCapabilities = new GLCapabilities(glProfile);
        final GLDrawableFactory factory = GLDrawableFactory.getFactory(glProfile);
        final GLOffscreenAutoDrawable e = factory.createOffscreenAutoDrawable(null,
                glCapabilities, null, width, height);
        return e;
    }

    /**
     * Create a dummy drawable, could be used for loading resources or
     * processing in parallel threads.
     *
     * @return GLAutoDrawable
     */
    public static GLAutoDrawable createDummyDrawable(GLAutoDrawable drawable) {
        final GLCapabilitiesImmutable capas = drawable.getChosenGLCapabilities();
        GLAutoDrawable dummyDrawable = drawable.getFactory().createDummyAutoDrawable(null, true, capas, null);
        dummyDrawable.setRealized(true);
        GLContext sharedContext = dummyDrawable.createContext(drawable.getContext());
        dummyDrawable.setContext(sharedContext, true);
        return dummyDrawable;
    }

}
