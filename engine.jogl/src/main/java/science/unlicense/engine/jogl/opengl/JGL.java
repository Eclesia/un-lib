
package science.unlicense.engine.jogl.opengl;

import com.jogamp.opengl.GL;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.gpu.api.GLBufferFactory;
import science.unlicense.gpu.api.opengl.GL1;
import science.unlicense.gpu.api.opengl.GL1ES;
import science.unlicense.gpu.api.opengl.GL2;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GL2ES3;
import science.unlicense.gpu.api.opengl.GL3;
import science.unlicense.gpu.api.opengl.GL4;

/**
 *
 * @author Johann Sorel
 */
public class JGL implements science.unlicense.gpu.api.opengl.GL{

    final GL gl;
    private GL1 gl1;
    private GL2 gl2;
    private GL3 gl3;
    private GL4 gl4;
    private GL1ES gl1es;
    private GL2ES2 gl2es2;
    private GL2ES3 gl2es3;

    public JGL(GL gl) {
        CObjects.ensureNotNull(gl);
        this.gl = gl;
    }

    @Override
    public BufferFactory getBufferFactory() {
        return GLBufferFactory.INSTANCE;
    }

    @Override
    public boolean isGL1() {
        return gl.isGL();
    }

    @Override
    public boolean isGL2() {
        return gl.isGL2();
    }

    @Override
    public boolean isGL3() {
        return gl.isGL3();
    }

    @Override
    public boolean isGL4() {
        return gl.isGL4();
    }

    @Override
    public boolean isGL1ES() {
        return gl.isGLES1();
    }

    @Override
    public boolean isGL2ES2() {
        return gl.isGL2ES2();
    }

    @Override
    public boolean isGL2ES3() {
        return gl.isGL2ES3();
    }

    @Override
    public GL1 asGL1() {
        if (gl==null) throw new IllegalStateException("GL is not set");
        if (gl1==null) gl1 = new JGL1(this);
        return gl1;
    }

    @Override
    public GL2 asGL2() {
        if (gl==null) throw new IllegalStateException("GL is not set");
        if (gl2==null) gl2 = new JGL2(this);
        return gl2;
    }

    @Override
    public GL3 asGL3() {
        if (gl==null) throw new IllegalStateException("GL is not set");
        if (gl3==null) gl3 = new JGL3(this);
        return gl3;
    }

    @Override
    public GL4 asGL4() {
        if (gl==null) throw new IllegalStateException("GL is not set");
        if (gl4==null) gl4 = new JGL4(this);
        return gl4;
    }

    @Override
    public GL1ES asGL1ES() {
        if (gl==null) throw new IllegalStateException("GL is not set");
        if (gl1es==null) throw new UnimplementedException("Not supported yet.");
        return gl1es;
    }

    @Override
    public GL2ES2 asGL2ES2() {
        if (gl==null) throw new IllegalStateException("GL is not set");
        if (gl2es2==null) gl2es2 = new JGL2ES2(this);
        return gl2es2;
    }

    @Override
    public GL2ES3 asGL2ES3() {
        if (gl==null) throw new IllegalStateException("GL is not set");
        if (gl2es3==null) gl2es3 = new JGL2ES3(this);
        return gl2es3;
    }

    static String[] toString(CharArray[] strings) {
        final String[] jvmStrings = new String[strings.length];
        for (int i=0;i<jvmStrings.length;i++) jvmStrings[i] = strings[i].toString();
        return jvmStrings;
    }

}
