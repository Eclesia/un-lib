
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * Segment := 18538067 container [ card:*; ] {
 *
 *      // Meta Seek Information
 *      SeekHead := 114d9b74 container [ card:*; ] ...
 *
 *      // Segment Information
 *      Info := 1549a966 container [ card:*; ] ...
 *
 *      // Cluster
 *      Cluster := 1f43b675 container [ card:*; ] ...
 *
 *      // Track
 *      Tracks := 1654ae6b container [ card:*; ] ...
 *
 *      // Cueing Data
 *      Cues := 1c53bb6b container ...
 *
 *      // Attachment
 *      Attachments := 1941a469 container ...
 *
 *      // Chapters
 *      Chapters := 1043a770 container ...
 *
 *      // Tagging
 *      Tags := 1254c367 container [ card:*; ] ...
 *  }
 *
 * @author Johann Sorel
 */
public class Segment extends EBMLChunk {

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x114d9b74,  TYPE_SUB, new Chars("SeekHead")),
        new PropertyType(0x1549a966,  TYPE_SUB, new Chars("Info")),
        new PropertyType(0x1f43b675,  TYPE_SUB, new Chars("Cluster")),
        new PropertyType(0x1654ae6b,  TYPE_SUB, new Chars("Tracks")),
        new PropertyType(0x1c53bb6b,  TYPE_SUB, new Chars("Cues")),
        new PropertyType(0x1941a469,  TYPE_SUB, new Chars("Attachments")),
        new PropertyType(0x1043a770,  TYPE_SUB, new Chars("Chapters")),
        new PropertyType(0x1254c367,  TYPE_SUB, new Chars("Tags"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Sequence getSeekHead() {
        return getSubs(0x114d9b74);
    }

    public Sequence getInfo() {
        return getSubs(0x1549a966);
    }

    public Sequence getCluster() {
        return getSubs(0x1f43b675);
    }

    public Sequence getTracks() {
        return getSubs(0x1654ae6b);
    }

    public Cues getCues() {
        return (Cues) getSub(0x1c53bb6b);
    }

    public Attachements getAttachments() {
        return (Attachements) getSub(0x1941a469);
    }

    public Chapters getChapters() {
        return (Chapters) getSub(0x1043a770);
    }

    public Sequence getTags() {
        return getSubs(0x1254c367);
    }
}
