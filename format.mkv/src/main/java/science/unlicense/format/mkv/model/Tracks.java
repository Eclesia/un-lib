
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * Tracks := 1654ae6b container [ card:*; ] {
 *    TrackEntry := ae container [ card:*; ] ...
 *  }
 *
 * @author Johann Sorel
 */
public class Tracks extends EBMLChunk{


    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xae,  TYPE_SUB,    new Chars("TrackEntry"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Sequence getTrackEntry() {
        return getSubs(0xae);
    }

}
