
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * Attachments := 1941a469 container {
 *    AttachedFile := 61a7 container [ card:*; ] ...
 *  }
 * @author Johann Sorel
 */
public class Attachements extends EBMLChunk{

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x61a7, TYPE_SUB, new Chars("AttachedFile"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Sequence getAttachedFiles(){
        return getSubs(0x61a7);
    }

}
