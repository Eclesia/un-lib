
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * EditionEntry := 45b9 container [ card:*; ] {
 *    ChapterAtom := b6 container [ card:*; ] ...
 *  }
 * @author Johann Sorel
 */
public class EditionEntry extends EBMLChunk{


    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xb6, TYPE_SUB, new Chars("ChapterAtom"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Sequence getChapterAtom() {
        return getSubs(0xb6);
    }

}
