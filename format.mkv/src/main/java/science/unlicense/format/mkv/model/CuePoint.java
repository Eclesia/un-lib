
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * CuePoint := bb container [ card:*; ] {
 *    CueTime := b3 uint;
 *    CueTrackPositions := b7 container [ card:*; ] ...
 *  }
 * @author Johann Sorel
 */
public class CuePoint extends EBMLChunk{


    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xb3,  TYPE_UINT, new Chars("CueTime")),
        new PropertyType(0xb7,  TYPE_SUB,  new Chars("CueTrackPositions"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Integer getCueTime() {
        return getPropertyUInt(0xb3, null);
    }

    public Sequence getCueTrackPositions() {
        return getSubs(0xb7);
    }
}
