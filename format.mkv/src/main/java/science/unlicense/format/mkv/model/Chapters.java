package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * Chapters := 1043a770 container {
 *    EditionEntry := 45b9 container [ card:*; ] ...
 *  }
 *
 * @author Johann Sorel
 */
public class Chapters extends EBMLChunk {

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x45b9, TYPE_SUB, new Chars("EditionEntry"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Sequence getEditionEntry() {
        return getSubs(0x45b9);
    }
}
