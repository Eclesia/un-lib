
package science.unlicense.format.mkv;

import science.unlicense.format.ebml.EBMLConstants;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaCapabilities;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaCapabilities;

/**
 *
 * @author Johann Sorel
 */
public class MKVMediaFormat extends AbstractMediaFormat{

    public static final MKVMediaFormat INSTANCE = new MKVMediaFormat();

    public MKVMediaFormat() {
        super(new Chars("mkv"));
        shortName = new Chars("Matroska");
        longName = new Chars("Matroska");
        mimeTypes.add(new Chars("video/x-matroska"));
        mimeTypes.add(new Chars("audio/x-matroska"));
        extensions.add(new Chars("mkv"));
        signatures.add(EBMLConstants.SIGNATURE);
        resourceTypes.add(Media.class);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    public Store open(Object input) throws IOException {
        return new MKVMediaStore((Path) input);
    }

}
