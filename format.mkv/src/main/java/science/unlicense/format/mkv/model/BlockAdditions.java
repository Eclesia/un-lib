package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * BlockAdditions := 75a1 container {
 *    BlockMore := a6 container [ card:*; ] ...
 *  }
 *
 * @author Johann Sorel
 */
public class BlockAdditions extends EBMLChunk {

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xa6, TYPE_SUB, new Chars("BlockMore"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Sequence getBlockMore() {
        return getSubs(0xa6);
    }
}
