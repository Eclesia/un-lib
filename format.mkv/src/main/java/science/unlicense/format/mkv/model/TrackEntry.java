
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * TrackEntry := ae container [ card:*; ] {
 *   TrackNumber := d7 uint [ range:1..; ]
 *   TrackUID := 73c5 uint [ range:1..; ]
 *   TrackType := 83 uint [ range:1..254; ]
 *   FlagEnabled := b9 uint [ range:0..1; def:1; ]
 *   FlagDefault := 88 uint [ range:0..1; def:1; ]
 *   FlagLacing  := 9c uint [ range:0..1; def:1; ]
 *   MinCache := 6de7 uint [ def:0; ]
 *   MaxCache := 6df8 uint;
 *   DefaultDuration := 23e383 uint [ range:1..; ]
 *   TrackTimecodeScale := 23314f float [ range:>0.0; def:1.0; ]
 *   Name := 536e string;
 *   Language := 22b59c string [ def:"eng"; range:32..126; ]
 *   CodecID := 86 string [ range:32..126; ];
 *   CodecPrivate := 63a2 binary;
 *   CodecName := 258688 string;
 *   CodecSettings := 3a9697 string;
 *   CodecInfoURL := 3b4040 string [ card:*; range:32..126; ]
 *   CodecDownloadURL := 26b240 string [ card:*; range:32..126; ]
 *   CodecDecodeAll := aa uint [ range:0..1; def:1; ]
 *   TrackOverlay := 6fab uint;
 *
 *   // Video
 *   Video := e0 container ...
 *
 *   // Audio
 *   Audio := e1 container ...
 *
 *   // Content Encoding
 *   ContentEncodings := 6d80 container ...
 * }
 *
 * @author Johann Sorel
 */
public class TrackEntry extends EBMLChunk{

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xd7,      TYPE_UINT,      new Chars("TrackNumber")),
        new PropertyType(0x73c5,    TYPE_UINT,      new Chars("TrackUID")),
        new PropertyType(0x83,      TYPE_UINT,      new Chars("TrackType")),
        new PropertyType(0xb9,      TYPE_UINT,      new Chars("FlagEnabled")),
        new PropertyType(0x88,      TYPE_UINT,      new Chars("FlagDefault")),
        new PropertyType(0x9c,      TYPE_UINT,      new Chars("FlagLacing")),
        new PropertyType(0x6de7,    TYPE_UINT,      new Chars("MinCache")),
        new PropertyType(0x6df8,    TYPE_UINT,      new Chars("MaxCache")),
        new PropertyType(0x23e383,  TYPE_UINT,      new Chars("DefaultDuration")),
        new PropertyType(0x23314f,  TYPE_FLOAT,     new Chars("TrackTimecodeScale")),
        new PropertyType(0x536e,    TYPE_STRING,    new Chars("Name")),
        new PropertyType(0x22b59c,  TYPE_STRING,    new Chars("Language")),
        new PropertyType(0x86,      TYPE_STRING,    new Chars("CodecID")),
        new PropertyType(0x63a2,    TYPE_BINARY,    new Chars("CodecPrivate")),
        new PropertyType(0x258688,  TYPE_STRING,    new Chars("CodecName")),
        new PropertyType(0x3a9697,  TYPE_STRING,    new Chars("CodecSettings")),
        new PropertyType(0x3b4040,  TYPE_STRING,    new Chars("CodecInfoURL")),
        new PropertyType(0x26b240,  TYPE_STRING,    new Chars("CodecDownloadURL")),
        new PropertyType(0xaa,      TYPE_UINT,      new Chars("CodecDecodeAll")),
        new PropertyType(0x6fab,    TYPE_UINT,      new Chars("TrackOverlay")),
        new PropertyType(0xe0,      TYPE_SUB,       new Chars("Video")),
        new PropertyType(0xe1,      TYPE_SUB,       new Chars("Audio")),
        new PropertyType(0x6d80,    TYPE_SUB,       new Chars("ContentEncodings"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Integer getTrackNumber() {
        return getPropertyUInt(0xd7, null);
    }

    public Integer getTrackUID() {
        return getPropertyUInt(0x73c5, null);
    }

    public Integer getTrackType() {
        return getPropertyUInt(0x83, null);
    }

    public Integer getFlagEnabled() {
        return getPropertyUInt(0xb9, null);
    }

    public Integer getFlagDefault() {
        return getPropertyUInt(0x88, null);
    }

    public Integer getFlagLacing() {
        return getPropertyUInt(0x9c, null);
    }

    public Integer getMinCache() {
        return getPropertyUInt(0x6de7, null);
    }

    public Integer getMaxCache() {
        return getPropertyUInt(0x6df8, null);
    }

    public Integer getDefaultDuration() {
        return getPropertyUInt(0x23e383, null);
    }

    public Float getTrackTimecodeScale() {
        return getPropertyFloat(0x23314f, null);
    }

    public Chars getName() {
        return getPropertyString(0x536e, null);
    }

    public Chars getLanguage() {
        return getPropertyString(0x22b59c, null);
    }

    public Chars getCodecID() {
        return getPropertyString(0x86, null);
    }

    public byte[] getCodecPrivate() {
        return getPropertyBinary(0x63a2, null);
    }

    public Chars getCodecName() {
        return getPropertyString(0x258688, null);
    }

    public Chars getCodecSettings() {
        return getPropertyString(0x3a9697, null);
    }

    public Sequence getCodecInfoURL() {
        return getPropertiesString(0x26b240);
    }

    public Sequence getCodecDownloadURL() {
        return getPropertiesString(0x3b4040);
    }

    public Integer getCodecDecodeAll() {
        return getPropertyUInt(0xaa, null);
    }

    public Integer getTrackOverlay() {
        return getPropertyUInt(0x6fab, null);
    }

    public Video getVideo() {
        return (Video) getSub(0xe0);
    }

    public Audio getAudio() {
        return (Audio) getSub(0xe1);
    }

    public ContentEncodings getContentEncodings() {
        return (ContentEncodings) getSub(0x6d80);
    }
}
