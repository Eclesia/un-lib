
package science.unlicense.format.mkv;

import science.unlicense.format.ebml.EBMLChunk;
import science.unlicense.format.ebml.EBMLReader;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;
import science.unlicense.format.mkv.model.AttachedFile;
import science.unlicense.format.mkv.model.Attachements;
import science.unlicense.format.mkv.model.Audio;
import science.unlicense.format.mkv.model.BlockAdditions;
import science.unlicense.format.mkv.model.BlockGroup;
import science.unlicense.format.mkv.model.BlockMore;
import science.unlicense.format.mkv.model.ChapterAtom;
import science.unlicense.format.mkv.model.ChapterDisplay;
import science.unlicense.format.mkv.model.ChapterTrack;
import science.unlicense.format.mkv.model.Chapters;
import science.unlicense.format.mkv.model.Cluster;
import science.unlicense.format.mkv.model.ContentCompression;
import science.unlicense.format.mkv.model.ContentEncoding;
import science.unlicense.format.mkv.model.ContentEncodings;
import science.unlicense.format.mkv.model.ContentEncryption;
import science.unlicense.format.mkv.model.CuePoint;
import science.unlicense.format.mkv.model.CueReference;
import science.unlicense.format.mkv.model.CueTrackPositions;
import science.unlicense.format.mkv.model.Cues;
import science.unlicense.format.mkv.model.EditionEntry;
import science.unlicense.format.mkv.model.Info;
import science.unlicense.format.mkv.model.Seek;
import science.unlicense.format.mkv.model.SeekHead;
import science.unlicense.format.mkv.model.Segment;
import science.unlicense.format.mkv.model.SimpleTag;
import science.unlicense.format.mkv.model.Slices;
import science.unlicense.format.mkv.model.Tag;
import science.unlicense.format.mkv.model.Tags;
import science.unlicense.format.mkv.model.Targets;
import science.unlicense.format.mkv.model.TimeSlice;
import science.unlicense.format.mkv.model.TrackEntry;
import science.unlicense.format.mkv.model.Tracks;
import science.unlicense.format.mkv.model.Video;

/**
 * View a GIF as a video media.
 *
 * @author Johann Sorel
 */
public class MKVMediaStore extends AbstractStore implements Media {

    private static final Dictionary CHUNKS = new HashDictionary();
    static {
        CHUNKS.add(0x61a7,      AttachedFile.class);
        CHUNKS.add(0x1941a469,  Attachements.class);
        CHUNKS.add(0xe1,        Audio.class);
        CHUNKS.add(0x75a1,      BlockAdditions.class);
        CHUNKS.add(0xa0,        BlockGroup.class);
        CHUNKS.add(0xa6,        BlockMore.class);
        CHUNKS.add(0xb6,        ChapterAtom.class);
        CHUNKS.add(0x80,        ChapterDisplay.class);
        CHUNKS.add(0x8f,        ChapterTrack.class);
        CHUNKS.add(0x1043a770,  Chapters.class);
        CHUNKS.add(0x1f43b675,  Cluster.class);
        CHUNKS.add(0x5034,      ContentCompression.class);
        CHUNKS.add(0x6240,      ContentEncoding.class);
        CHUNKS.add(0x6d80,      ContentEncodings.class);
        CHUNKS.add(0x5035,      ContentEncryption.class);
        CHUNKS.add(0xbb,        CuePoint.class);
        CHUNKS.add(0xdb,        CueReference.class);
        CHUNKS.add(0xb7,        CueTrackPositions.class);
        CHUNKS.add(0x1c53bb6b,  Cues.class);
        CHUNKS.add(0x45b9,      EditionEntry.class);
        CHUNKS.add(0x1549a966,  Info.class);
        CHUNKS.add(0x4dbb,      Seek.class);
        CHUNKS.add(0x114d9b74,  SeekHead.class);
        CHUNKS.add(0x18538067,  Segment.class);
        CHUNKS.add(0x67c8,      SimpleTag.class);
        CHUNKS.add(0x8e,        Slices.class);
        CHUNKS.add(0x7373,      Tag.class);
        CHUNKS.add(0x1254c367,  Tags.class);
        CHUNKS.add(0x63c0,      Targets.class);
        CHUNKS.add(0xe8,        TimeSlice.class);
        CHUNKS.add(0xae,        TrackEntry.class);
        CHUNKS.add(0x1654ae6b,  Tracks.class);
        CHUNKS.add(0xe0,        Video.class);
    }

    private final Path path;

    public MKVMediaStore(Path path) {
        super(MKVMediaFormat.INSTANCE,path);
        this.path = path;
    }

    @Override
    public Track[] getTracks() throws IOException {
        final EBMLReader reader = new EBMLReader(CHUNKS);
        reader.setInput(path);

        while (reader.hasNext()){
            final EBMLChunk chunk = reader.next();
            System.out.println(Nodes.toCharsTree(chunk, 10));
        }

        return null;
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        getTracks();
        return null;
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
