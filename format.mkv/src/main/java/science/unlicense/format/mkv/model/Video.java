
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * Video := e0 container {
 *    FlagInterlaced := 9a uint [ range:0..1; def:0; ]
 *    StereoMode := 53b8 uint [ range:0..3; def:0; ]
 *    PixelWidth := b0 uint [ range:1..; ]
 *    PixelHeight := ba uint [ range:1..; ]
 *    DisplayWidth := 54b0 uint [ def:PixelWidth; ]
 *    DisplayHeight := 54ba uint [ def:PixelHeight; ]
 *    DisplayUnit := 54b2 uint [ def:0; ]
 *    AspectRatioType := 54b3 uint [ def:0; ]
 *    ColourSpace := 2eb524 binary;
 *    GammaValue := 2fb523 float [ range:>0.0; ]
 *  }
 *
 * @author Johann Sorel
 */
public class Video extends EBMLChunk{

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x9a,      TYPE_UINT, new Chars("FlagInterlaced")),
        new PropertyType(0x53b8,    TYPE_UINT, new Chars("StereoMode")),
        new PropertyType(0xb0,      TYPE_UINT, new Chars("PixelWidth")),
        new PropertyType(0xba,      TYPE_UINT, new Chars("PixelHeight")),
        new PropertyType(0x54b0,    TYPE_UINT, new Chars("DisplayWidth")),
        new PropertyType(0x54ba,    TYPE_UINT, new Chars("DisplayHeight")),
        new PropertyType(0x54b2,    TYPE_UINT, new Chars("DisplayUnit")),
        new PropertyType(0x54b3,    TYPE_UINT, new Chars("AspectRatioType")),
        new PropertyType(0x2eb524,  TYPE_BINARY, new Chars("ColourSpace")),
        new PropertyType(0x2fb523,  TYPE_FLOAT, new Chars("GammaValue"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Integer getFlagInterlaced() {
        return getPropertyUInt(0x9a, 0);
    }

    public Integer getStereoMode() {
        return getPropertyUInt(0x53b8, 0);
    }

    public Integer getPixelWidth() {
        return getPropertyUInt(0xb0, null);
    }

    public Integer getPixelHeight() {
        return getPropertyUInt(0xd7, null);
    }

    public Integer getDisplayWidth() {
        return getPropertyUInt(0x54b0, getPixelWidth());
    }

    public Integer getDisplayHeight() {
        return getPropertyUInt(0xba, getPixelHeight());
    }

    public Integer getDisplayUnit() {
        return getPropertyUInt(0x54b2, 0);
    }

    public Integer getAspectRatioType() {
        return getPropertyUInt(0x54b3, 0);
    }

    public byte[] getColourSpace() {
        return getPropertyBinary(0x2eb524, null);
    }

    public Float getGammaValue() {
        return getPropertyFloat(0x2fb523, null);
    }
}
