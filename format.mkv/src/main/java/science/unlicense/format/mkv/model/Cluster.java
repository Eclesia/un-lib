
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * Cluster := 1f43b675 container [ card:*; ] {
 *    Timecode := e7 uint;
 *    Position := a7 uint;
 *    PrevSize := ab uint;
 *    BlockGroup := a0 container [ card:*; ] ...
 *  }
 *
 * @author Johann Sorel
 */
public class Cluster extends EBMLChunk{


    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xe7, TYPE_UINT, new Chars("Timecode")),
        new PropertyType(0xa7, TYPE_UINT, new Chars("Position")),
        new PropertyType(0xab, TYPE_UINT, new Chars("PrevSize")),
        new PropertyType(0xa0, TYPE_SUB,  new Chars("BlockGroup"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Integer getTimecode() {
        return getPropertyUInt(0xe7, null);
    }

    public Integer getPosition() {
        return getPropertyUInt(0xa7, null);
    }

    public Integer getPrevSize() {
        return getPropertyUInt(0xab, null);
    }

    public Sequence getBlockGroup() {
        return getSubs(0xa0);
    }
}
