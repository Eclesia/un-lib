
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * ChapterDisplay := 80 container [ card:*; ] {
 *    ChapString := 85 string;
 *    ChapLanguage := 437c string [ card:*; def:"eng"; range:32..126; ]
 *    ChapCountry := 437e string [ card:*; range:32..126; ]
 *  }
 *
 * @author Johann Sorel
 */
public class ChapterDisplay extends EBMLChunk{

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x85,   TYPE_STRING, new Chars("ChapString")),
        new PropertyType(0x437c, TYPE_STRING, new Chars("ChapLanguage")),
        new PropertyType(0x437e, TYPE_STRING, new Chars("ChapCountry"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Chars getChapString() {
        return getPropertyString(0x85, null);
    }

    public Sequence getChapLanguage() {
        return getSubs(0x437c);
    }

    public Sequence getChapCountry() {
        return getSubs(0x437e);
    }
}
