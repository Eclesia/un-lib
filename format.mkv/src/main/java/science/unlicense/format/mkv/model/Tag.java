
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * Tag := 7373 container [ card:*; ] {
 *  Targets := 63c0 container ...
 *  SimpleTag := 67c8 container [ card:*; ] ...
 }
 * @author Johann Sorel
 */
public class Tag extends EBMLChunk{


    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x63c0,  TYPE_SUB,    new Chars("Targets")),
        new PropertyType(0x67c8,  TYPE_SUB,    new Chars("SimpleTag"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Targets getTargets() {
        return (Targets) getSub(0x63c0);
    }

    public Sequence getSimpleTag() {
        return getSubs(0x67c8);
    }
}
