
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * Tags := 1254c367 container [ card:*; ] {
 *    Tag := 7373 container [ card:*; ] ...
 * }
 *
 * @author Johann Sorel
 */
public class Tags extends EBMLChunk{


    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x7373,  TYPE_SUB, new Chars("Tag"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Sequence getTag() {
        return getSubs(0x7373);
    }

}
