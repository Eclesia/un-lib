
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * AttachedFile := 61a7 container [ card:*; ] {
 *  FileDescription := 467e string;
 *  FileName := 466e string;
 *  FileMimeType := 4660 string [ range:32..126; ]
 *  FileData := 465c binary;
 *  FileUID := 46ae uint;
 * }
 *
 * @author Johann Sorel
 */
public class AttachedFile extends EBMLChunk{

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x467e, TYPE_STRING, new Chars("FileDescription")),
        new PropertyType(0x466e, TYPE_STRING, new Chars("FileName")),
        new PropertyType(0x4660, TYPE_STRING, new Chars("FileMimeType")),
        new PropertyType(0x465c, TYPE_BINARY, new Chars("FileData")),
        new PropertyType(0x46ae, TYPE_UINT, new Chars("FileUID"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Chars getFileDescription(){
        return getPropertyString(0x467e, null);
    }

    public Chars getFileName(){
        return getPropertyString(0x466e, null);
    }

    public Chars getFileMimeType(){
        return getPropertyString(0x4660, null);
    }

    public Chars getFileData(){
        return getPropertyString(0x465c, null);
    }

    public Integer getFileUID(){
        return getPropertyUInt(0x46ae, null);
    }

}
