
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * Slices := 8e container [ card:*; ] {
 *   TimeSlice := e8 container [ card:*; ] ...
 * }
 *
 * @author Johann Sorel
 */
public class Slices extends EBMLChunk{


    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xe8,  TYPE_SUB, new Chars("TimeSlice"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Sequence getTimeSlice() {
        return getSubs(0xe8);
    }

}
