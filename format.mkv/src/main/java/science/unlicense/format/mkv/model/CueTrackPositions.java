
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * CueTrackPositions := b7 container [ card:*; ] {
 *    CueTrack := f7 uint [ range:1..; ]
 *    CueClusterPosition := f1 uint;
 *    CueBlockNumber := 5378 uint [ range:1..; def:1; ]
 *    CueCodecState := ea uint [ def:0; ]
 *    CueReference := db container [ card:*; ] ...
 *  }
 * @author Johann Sorel
 */
public class CueTrackPositions extends EBMLChunk{


    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xf7,  TYPE_UINT, new Chars("CueTrack")),
        new PropertyType(0xf1,  TYPE_UINT, new Chars("CueClusterPosition")),
        new PropertyType(0x5378,TYPE_UINT, new Chars("CueBlockNumber")),
        new PropertyType(0xea,  TYPE_UINT, new Chars("CueCodecState")),
        new PropertyType(0xdb,  TYPE_SUB,  new Chars("CueReference"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Integer getCueTrack() {
        return getPropertyUInt(0xf7, null);
    }

    public Integer getCueClusterPosition() {
        return getPropertyUInt(0xf1, null);
    }

    public Integer getCueBlockNumber() {
        return getPropertyUInt(0x5378, 1);
    }

    public Integer getCueCodecState() {
        return getPropertyUInt(0xea, 0);
    }

    public Sequence getCueReference() {
        return getSubs(0xdb);
    }
}
