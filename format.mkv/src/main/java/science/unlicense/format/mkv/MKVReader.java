
package science.unlicense.format.mkv;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.media.api.MediaPacket;

/**
 *
 * @author Johann Sorel
 */
public class MKVReader implements MediaReadStream {

    @Override
    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaPacket next() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }


}
