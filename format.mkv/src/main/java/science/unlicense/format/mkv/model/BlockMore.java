
package science.unlicense.format.mkv.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ebml.EBMLChunk;

/**
 *
 * BlockMore := a6 container [ card:*; ] {
 *  BlockAddID := ee uint [ range:1..; ]
 *  BlockAdditional := a5 binary;
 * }
 *
 * @author Johann Sorel
 */
public class BlockMore extends EBMLChunk{

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xee, TYPE_UINT,   new Chars("BlockAddID")),
        new PropertyType(0xa5, TYPE_BINARY, new Chars("BlockAdditional"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Integer getBlockAddID(){
        return getPropertyUInt(0xee, null);
    }

    public byte[] getBlockAdditional(){
        return getPropertyBinary(0xa5, null);
    }
}
