package science.unlicense.geometry.api.index.quadtrees;

import java.util.SortedSet;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.api.BBox;

import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.geometry.impl.Ray;

/**
 * The interface provided by quadtree implementations.
 *
 * @param <T> The precise type of quadtree members.
 * @author Mark Raynsford
 */
public interface QuadTreeType<T extends QuadTreeMemberType<T>> {

    /**
     * Delete all objects, if any, contained within the quadtree.
     */
    void quadTreeClear();

    /**
     * @return The position of the lower corner of the quadtree on the X axis.
     */
    double quadTreeGetPositionX();

    /**
     * @return The position of the lower corner of the quadtree on the Y axis.
     */
    double quadTreeGetPositionY();

    /**
     * @return The maximum size of the quadtree on the X axis.
     */
    double quadTreeGetSizeX();

    /**
     * @return The maximum size of the quadtree on the Y axis.
     */
    double quadTreeGetSizeY();

    /**
     * Insert the object <code>item</code> into the quadtree.
     * <p>
     * The function returns <code>false</code> if the object could not be inserted for any reason (perhaps due to being too large).
     * </p>
     *
     * @param item The object to insert
     *
     * @return <code>true</code> if the object was inserted
     * @throws InvalidArgumentException If the object's bounding area is not <i>well-formed</i>.
     *
     * @see com.io7m.jspatial.BoundingAreaCheck#isWellFormed(BBox)
     */
    boolean quadTreeInsert(T item) throws InvalidArgumentException;

    /**
     * Pass each object in the quadtree to <code>f.call()</code>, in no particular order. Iteration stops if <code>f.call()</code> returns <code>false</code>,
     * or raises an exception.
     *
     * @param f The function that will receive each object
     */
    void quadTreeIterateObjects(Predicate f) throws Exception;

    /**
     * Returns all objects in the tree that are completely contained within <code>area</code>, saving the results to <code>items</code>.
     *
     * @param area The area to examine
     * @param items The returned items
     * @throws InvalidArgumentException Iff <code>area</code> is not well formed
     *
     * @see com.io7m.jspatial.BoundingAreaCheck#isWellFormed(BBox)
     */
    void quadTreeQueryAreaContaining(BBox area, SortedSet<T> items) throws InvalidArgumentException;

    /**
     * Returns all objects in the tree that are at least partially contained within <code>area</code>, saving the results to <code>items</code>.
     *
     * @param area The area to examine
     * @param items The returned items
     *
     * @throws InvalidArgumentException Iff <code>area</code> is not well formed
     *
     * @see com.io7m.jspatial.BoundingAreaCheck#isWellFormed(BBox)
     */
    void quadTreeQueryAreaOverlapping(BBox area, SortedSet<T> items) throws InvalidArgumentException;

    /**
     * Returns the objects intersected by the ray <code>ray</code> in <code>items</code>.
     *
     * The objects are returned in order of increasing scalar distance from the origin of <code>ray</code>. That is, the nearest object to the origin of
     * <code>ray</code> will be the first item in <code>items</code>.
     *
     * @see com.io7m.jtensors.Vector#distance(com.io7m.jtensors.VectorReadable2DType, com.io7m.jtensors.VectorReadable2DType)
     *
     * @param items The returned objects
     * @param ray The ray
     */
    void quadTreeQueryRaycast(Ray ray, SortedSet<QuadTreeRaycastResult<T>> items);

    /**
     * Return the set of quadrants intersected by <code>ray</code>.
     *
     * @param ray The ray
     * @param items The resulting set of quadrants
     */
    void quadTreeQueryRaycastQuadrants(Ray ray, SortedSet<QuadTreeRaycastResult<QuadrantType>> items);

    /**
     * Remove the object <code>item</code> from the quadtree.
     * <p>
     * The function returns <code>false</code> if the object could not be removed for any reason (perhaps due to not being in the tree in the first place).
     * </p>
     *
     * @param item The object to remove
     *
     * @return <code>true</code> if the object was removed
     */
    boolean quadTreeRemove(T item);

    /**
     * Pass each node of the given quadtree to <code>traversal.visit()</code>, in depth-first order.
     *
     * @param traversal The traversal
     * @throws E Propagated from <code>traversal.visit()</code>
     * @param <E> The type of raised exceptions
     */
    <E extends Throwable> void quadTreeTraverse(QuadTreeTraversalType<E> traversal) throws E;

}
