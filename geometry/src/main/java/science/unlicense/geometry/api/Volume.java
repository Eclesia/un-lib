
package science.unlicense.geometry.api;

import science.unlicense.common.api.collection.Collection;

/**
 * Geometries which define a volume, composed of filled spaces.
 *
 * @author Johann Sorel
 */
public interface Volume extends Geometry {

    Surface getExterior();

    Collection getInteriors();
}
