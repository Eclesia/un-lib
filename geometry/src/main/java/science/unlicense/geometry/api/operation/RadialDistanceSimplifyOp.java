
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class RadialDistanceSimplifyOp extends AbstractSingleOperation{

    private double radius;

    public RadialDistanceSimplifyOp(Geometry geometry, double radius) {
        super(geometry);
        this.radius = radius;
    }

    public double getDistance() {
        return radius;
    }

    public void setDistance(double radius) {
        this.radius = radius;
    }

    @Override
    public Geometry execute() throws OperationException {
        return (Geometry) super.execute();
    }
}
