
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 * Compute the bounding circle or sphere of a geometry.
 *
 * @author Johann Sorel
 */
public class BoundingCircleOp extends AbstractSingleOperation {

    public BoundingCircleOp(Geometry first) {
        super(first);
    }

    @Override
    public Geometry execute() throws OperationException {
        return (Geometry) super.execute();
    }

    public static double[] calculateCircleCenter(double[] a, double[] b, double[] c) {
        final double as = (b[1]-a[1]) / (b[0]-a[0]);
        final double bs = (c[1]-b[1]) / (c[0]-b[0]);
        final double[] center = new double[2];
        center[0] = (as * bs * (a[1]-c[1]) + bs * (a[0]+b[0]) - as * (b[0]+c[0])) / (2 * (bs-as));
        center[1] = -1.0 * (center[0] - (a[0]+b[0])/2.0) / as + (a[1]+b[1])/2.0;
        return center;
    }

}
