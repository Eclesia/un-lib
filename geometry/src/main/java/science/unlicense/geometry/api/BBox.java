
package science.unlicense.geometry.api;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.AbstractGeometry;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * N dimension bounding box.
 *
 * @author Johann Sorel
 */
public class BBox extends AbstractGeometry {

    protected final TupleRW upper;
    protected final TupleRW lower;

    public BBox(CoordinateSystem cs) {
        super(cs);
        final int dim = cs.getDimension();
        upper = VectorNf64.createDouble(dim);
        lower = VectorNf64.createDouble(dim);
    }

    public BBox(int dimension) {
        this(VectorNf64.createDouble(dimension), VectorNf64.createDouble(dimension));
    }

    public BBox(double[] min, double[] max) {
        super(min.length);
        upper = VectorNf64.create(max);
        lower = VectorNf64.create(min);
    }

    public BBox(TupleRW lower,TupleRW upper) {
        super(lower.getSampleCount());
        this.upper = upper;
        this.lower = lower;
    }

    public BBox(BBox other) {
        super(other.cs);
        this.upper = other.upper.copy();
        this.lower = other.lower.copy();
    }

    public BBox(Extent extent) {
        super(extent.getDimension());
        int dimension = extent.getDimension();
        this.upper = VectorNf64.createDouble(dimension);
        this.lower = VectorNf64.createDouble(dimension);
        for (int i=0;i<dimension;i++){
            upper.set(i,extent.get(i));
        }
    }

    public TupleRW getUpper() {
        return upper;
    }

    public TupleRW getLower() {
        return lower;
    }

    public TupleRW getMiddle() {
        return VectorNf64.create(upper).localAdd(lower).localScale(0.5);
    }

    public double getMin(int ordinate){
        return lower.get(ordinate);
    }

    public double getMiddle(int ordinate){
        return (upper.get(ordinate) + lower.get(ordinate)) / 2;
    }

    public double getMax(int ordinate){
        return upper.get(ordinate);
    }

    public double getSpan(int ordinate){
        return getMax(ordinate)-getMin(ordinate);
    }

    public Extent getExtent(){
        final Extent ext = new Extent.Double(dimension);
        for (int i=0;i<dimension;i++){
            ext.set(i, getSpan(i));
        }
        return ext;
    }

    /**
     * Copy values from given bounding box.
     * @param bbox not null
     */
    public void set(BBox bbox){
        upper.set(bbox.getUpper());
        lower.set(bbox.getLower());
    }

    public void setRange(int ordinate, double min, double max){
        lower.set(ordinate, min);
        upper.set(ordinate, max);
    }

    /**
     * Grow this bounding to contain the given one.
     * @param bbox
     */
    public void expand(BBox bbox){
        for (int i=0;i<dimension;i++) {
            lower.set(i, Maths.min(lower.get(i), bbox.getMin(i)));
            upper.set(i, Maths.max(upper.get(i), bbox.getMax(i)));
        }
    }

    /**
     * Grow this bounding to contain the given point.
     * @param point
     */
    public void expand(Tuple point) {
        for (int i=0;i<dimension;i++) {
            lower.set(i, Maths.min(lower.get(i), point.get(i)));
            upper.set(i, Maths.max(upper.get(i), point.get(i)));
        }
    }

    /**
     * Grow this bounding to contain the given point.
     * @param point
     */
    public void expand(double[] point){
        for (int i=0;i<dimension;i++){
            lower.set(i, Maths.min(lower.get(i), point[i]));
            upper.set(i, Maths.max(upper.get(i), point[i]));
        }
    }

    /**
     * Grow this bounding to contain all tuples in buffer.
     * @param buffer
     */
    public void expand(TupleGrid buffer){
        final TupleGridCursor ite = buffer.cursor();
        double[] tuple = new double[dimension];
        while (ite.next()) {
            ite.samples().toDouble(tuple, 0);
            for (int i=0;i<dimension;i++){
                lower.set(i, Maths.min(lower.get(i), tuple[i]));
                upper.set(i, Maths.max(upper.get(i), tuple[i]));
            }
        }
    }

    /**
     * Grow this bounding to contain the given point.
     * @param point
     */
    public void expand(float[] point){
        for (int i=0;i<dimension;i++){
            lower.set(i, Maths.min(lower.get(i), point[i]));
            upper.set(i, Maths.max(upper.get(i), point[i]));
        }
    }

    public void expand(double px){
        lower.set(0, Maths.min(lower.get(0), px));
        upper.set(0, Maths.max(upper.get(0), px));
    }

    public void expand(double px, double py){
        lower.set(0, Maths.min(lower.get(0), px));
        upper.set(0, Maths.max(upper.get(0), px));
        lower.set(1, Maths.min(lower.get(1), py));
        upper.set(1, Maths.max(upper.get(1), py));
    }

    public void expand(double px, double py, double pz){
        lower.set(0, Maths.min(lower.get(0), px));
        upper.set(0, Maths.max(upper.get(0), px));
        lower.set(1, Maths.min(lower.get(1), py));
        upper.set(1, Maths.max(upper.get(1), py));
        lower.set(2, Maths.min(lower.get(2), pz));
        upper.set(2, Maths.max(upper.get(2), pz));
    }

    public void expand(double px, double py, double pz, double pw){
        lower.set(0, Maths.min(lower.get(0), px));
        upper.set(0, Maths.max(upper.get(0), px));
        lower.set(1, Maths.min(lower.get(1), py));
        upper.set(1, Maths.max(upper.get(1), py));
        lower.set(2, Maths.min(lower.get(2), pz));
        upper.set(2, Maths.max(upper.get(2), pz));
        lower.set(3, Maths.min(lower.get(3), pw));
        upper.set(3, Maths.max(upper.get(3), pw));
    }

    /**
     * Expand the bbox in all directions by given amount.
     */
    public void grow(double amount){
        for (int i=0;i<dimension;i++){
            lower.set(i, lower.get(i)-amount);
            upper.set(i, upper.get(i)+amount);
        }
    }

    /**
     *
     * @return true if lower corner equals upper corner.
     */
    public boolean isEmpty(){
        return lower.equals(upper);
    }

    /**
     * Set all values to zero
     */
    public void setToZero(){
        upper.setAll(0.0);
        lower.setAll(0.0);
    }

    /**
     * Set all values to NaN.
     */
    public void setToNaN(){
        upper.setAll(Double.NaN);
        lower.setAll(Double.NaN);
    }

    /**
     * Set lower values to max value and upper values to negative max value
     */
    public void setToReverseMax(){
        lower.setAll(Double.MAX_VALUE);
        upper.setAll(-Double.MAX_VALUE);
    }

    /**
     * Check that all values are not NaN or Infinites.
     * @return true if valid
     */
    public boolean isValid(){
        return lower.isValid() && upper.isValid();
    }

    /**
     * Reorder values, ensure min/max values are in
     * lower/upper corners.
     */
    public void reorder(){
        for (int i=0;i<dimension;i++){
            final double tl = lower.get(i);
            final double tu = upper.get(i);
            if (tl>tu){
                upper.set(i, tl);
                lower.set(i, tu);
            }
        }
    }

    @Override
    public BBox getBoundingBox() {
        return this;
    }

    public Point getCentroid() {
        return new DefaultPoint(getMiddle());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BBox other = (BBox) obj;
        if (this.upper != other.upper && (this.upper == null || !this.upper.equals(other.upper))) {
            return false;
        }
        if (this.lower != other.lower && (this.lower == null || !this.lower.equals(other.lower))) {
            return false;
        }
        return true;
    }

    @Override
    public Chars toChars() {
        return new Chars("BBOX : ").concat(CObjects.toChars(lower)).concat(CObjects.toChars(upper));
    }

    public boolean intersects(Tuple coord){
        return intersects(coord, true);
    }

    public boolean intersects(Tuple coord, boolean includeBorder){
        final int dim = getDimension();
        if (includeBorder){
            for (int i=0;i<dim;i++){
                if (getMax(i) < coord.get(i) || getMin(i) > coord.get(i)) return false;
            }
        } else {
            for (int i=0;i<dim;i++){
                if (getMax(i) <= coord.get(i) || getMin(i) >= coord.get(i)) return false;
            }
        }

        return true;
    }

    public boolean intersects(BBox bbox){
        return intersects(bbox, true);
    }

    public boolean intersects(BBox bbox, boolean includeBorder){
        final int dim = getDimension();
        if (includeBorder){
            for (int i=0;i<dim;i++){
                if (getMax(i) < bbox.getMin(i) || getMin(i) > bbox.getMax(i)) return false;
            }
        } else {
            for (int i=0;i<dim;i++){
                if (getMax(i) <= bbox.getMin(i) || getMin(i) >= bbox.getMax(i)) return false;
            }
        }

        return true;
    }

    /**
     * Test intersection with an Extent.
     * The extent is interpreted as a BBox starting at coordinate zero on each dimension.
     *
     * TODO : make extent a geometry ?
     *
     * @param extent
     * @param includeBorder
     * @return
     */
    public boolean intersects(Extent extent, boolean includeBorder){
        for (int i=getDimension()-1;i>-1;i--){
            if (includeBorder){
                if (getMin(i) > extent.get(i) || getMax(i) < 0.0) return false;
            } else {
                if (getMin(i) >= extent.get(i) || getMax(i) <= 0.0) return false;
            }
        }
        return true;
    }

    public boolean contains(BBox bbox) {
        final int dim = getDimension();
        for (int i=0;i<dim;i++) {
            if (getMax(i) < bbox.getMax(i) || getMin(i) > bbox.getMin(i)) return false;
        }
        return true;
    }

    /**
     * Intersect this bbox with given one, store the result in this bbox.
     * This method does not perform any verification.
     *
     * @param bbox
     * @return
     */
    public void localIntersect(BBox bbox) {
        final int size = lower.getSampleCount();
        final Tuple otherLower = bbox.getLower();
        final Tuple otherUpper = bbox.getUpper();
        for (int i=0;i<size;i++) {
            double low = Maths.max(lower.get(i), otherLower.get(i));
            double up = Maths.min(upper.get(i), otherUpper.get(i));
            if (low > up) {
                //no intersection, set to NaN
                low = Double.NaN;
                up = Double.NaN;
            }
            lower.set(i, low);
            upper.set(i, up);
        }
    }

}
