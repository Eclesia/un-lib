
package science.unlicense.geometry.api.index;

/**
 * The SD categorization of an arbitrary object: Whether an object is static
 * (immovable), or dynamic (movable).
 *
 * @author Mark Raynsford
 */
public enum SDType{

  /**
   * The object is dynamic.
   */
  SD_DYNAMIC,

  /**
   * The object is static.
   */
  SD_STATIC

}
