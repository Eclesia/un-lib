
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 * Buffer operation, expands the geometry contour.
 *
 * @author Johann Sorel
 */
public class BufferOp extends AbstractSingleOperation {

    private final double distance;

    public BufferOp(Geometry geometry, double distance) {
        super(geometry);
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public Geometry execute() throws OperationException {
        return (Geometry) super.execute();
    }

}
