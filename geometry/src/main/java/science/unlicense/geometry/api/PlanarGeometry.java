
package science.unlicense.geometry.api;

import science.unlicense.geometry.api.path.PathIterator;

/**
 * A planar geometry is a geometry which can be represented by a path.
 *
 * @author Johann Sorel
 */
public interface PlanarGeometry extends Geometry {

    /**
     * Create a path iteraor.
     *
     * @return PathIterator
     */
    PathIterator createPathIterator();

}
