
package science.unlicense.geometry.api.operation;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.system.ModuleSeeker;

/**
 *
 * @author Johann Sorel
 */
public final class Operations {

    private static final Chars OPERATION_PATH = Chars.constant("services");

    private static Dictionary CACHE;

    private Operations(){}

    /**
     * Lists available operation executors.
     *
     * @return array of OperationExecutor, never null but can be empty.
     */
    public static OperationExecutor[] getExecutors(){
        return (OperationExecutor[]) ModuleSeeker.findServices(OperationExecutor.class);
    }

    private static OperationExecutor[] getExecutors(Class opClass){
        synchronized(OPERATION_PATH){
            if (CACHE==null){
                CACHE = new HashDictionary();
                final OperationExecutor[] execs = getExecutors();
                for (int i=0;i<execs.length;i++){
                    final Class oc = execs[i].getOperationClass();
                    OperationExecutor[] array = (OperationExecutor[]) CACHE.getValue(oc);
                    if (array==null){
                        array = new OperationExecutor[]{execs[i]};
                    } else {
                        array = (OperationExecutor[]) Arrays.insert(array, array.length, execs[i]);
                    }
                    CACHE.add(oc, array);
                }
            }
        }
        return (OperationExecutor[]) CACHE.getValue(opClass);
    }


    public static Object execute(Operation operation) throws OperationException{
        final OperationExecutor[] executors = getExecutors(operation.getClass());
        for (int i=0,n=executors.length;i<n;i++){
            if (executors[i].canHandle(operation)){
                return executors[i].execute(operation);
            }
        }
        throw new OperationException("No executor can handle the operation : "+operation);
    }

}
