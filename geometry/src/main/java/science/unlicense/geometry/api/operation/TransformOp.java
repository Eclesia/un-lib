
package science.unlicense.geometry.api.operation;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.Geometry;

/**
 * Geometry transform operation.
 *
 * @author Johann Sorel
 */
public class TransformOp extends AbstractSingleOperation {

    public static final Chars NAME = Chars.constant(new byte[]{'T','R','A','N','S','F','O','R','M'});

    private final science.unlicense.math.api.transform.Transform trs;

    public TransformOp(Geometry geometry, science.unlicense.math.api.transform.Transform trs) {
        this.geometry = geometry;
        this.trs = trs;
    }

    public Chars getName() {
        return NAME;
    }

    public science.unlicense.math.api.transform.Transform getTransform() {
        return trs;
    }

}
