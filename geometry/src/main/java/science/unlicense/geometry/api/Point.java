
package science.unlicense.geometry.api;

import science.unlicense.math.api.TupleRW;

/**
 * A Point.
 *
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_Point
 *   An ST_Point value is a 0-dimensional geometry and represents a single location.
 *
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public interface Point extends PlanarGeometry {

    TupleRW getCoordinate();

}
