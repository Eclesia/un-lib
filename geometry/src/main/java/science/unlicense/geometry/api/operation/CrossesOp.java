
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class CrossesOp extends AbstractBinaryOperation{

    public CrossesOp(Geometry first, Geometry second) {
        super(first,second);
    }

    @Override
    public Boolean execute() throws OperationException {
        return (Boolean) super.execute();
    }
}
