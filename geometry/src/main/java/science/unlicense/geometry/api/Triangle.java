
package science.unlicense.geometry.api;

import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vectors;

/**
 * Specification :
 *  - WKT/WKB ISO 13249-3 : ST_Triangle
 *   The ST_Triangle type is a subtype of ST_Polygon with an exterior
 *   boundary having exactly four points (the last point being the same
 *   as the first point) and no interior boundaries
 *
 * @author Johann Sorel
 */
public interface Triangle extends Geometry {

    TupleRW getFirstCoord();

    TupleRW getSecondCoord();

    TupleRW getThirdCoord();

    /**
     * Calculate the barycentric value in triangle for given point.
     * @param p
     * @return Vector barycentric values
     */
    public static double[] getBarycentricValue(final double[] a, final double[] b, final double[] c, final double[] p){
        final double[] v0 = Vectors.subtract(b, a);
        final double[] v1 = Vectors.subtract(c, a);
        final double[] v2 = Vectors.subtract(p, a);
        final double d00 = Vectors.dot(v0, v0);
        final double d01 = Vectors.dot(v0,v1);
        final double d11 = Vectors.dot(v1,v1);
        final double d20 = Vectors.dot(v2,v0);
        final double d21 = Vectors.dot(v2,v1);
        final double denom = d00 * d11 - d01 * d01;
        final double v = (d11 * d20 - d01 * d21) / denom;
        final double w = (d00 * d21 - d01 * d20) / denom;
        final double u = 1.0f - v - w;
        return new double[]{u, v, w};
    }

    /**
     * Calculate the barycentric value in triangle for given point 2D.
     * @return Vector barycentric values
     */
    public static double[] getBarycentricValue2D(
            final double ax, final double ay,
            final double bx, final double by,
            final double cx, final double cy,
            final double px, final double py){
        final double v0x = bx - ax;
        final double v0y = by - ay;
        final double v1x = cx - ax;
        final double v1y = cy - ay;
        final double v2x = px - ax;
        final double v2y = py - ay;
        final double d00 = v0x*v0x + v0y*v0y;
        final double d01 = v0x*v1x + v0y*v1y;
        final double d11 = v1x*v1x + v1y*v1y;
        final double d20 = v2x*v0x + v2y*v0y;
        final double d21 = v2x*v1x + v2y*v1y;
        final double denom = d00 * d11 - d01 * d01;
        final double v = (d11 * d20 - d01 * d21) / denom;
        final double w = (d00 * d21 - d01 * d20) / denom;
        final double u = 1.0f - v - w;
        return new double[]{u, v, w};
    }

}
