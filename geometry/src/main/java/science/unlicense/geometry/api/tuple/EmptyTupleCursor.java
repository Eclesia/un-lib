
package science.unlicense.geometry.api.tuple;

import science.unlicense.common.api.exception.MishandleException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Number;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.math.api.AbstractTupleRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Empty sample iterator.
 *
 * @author Johann Sorel
 */
public class EmptyTupleCursor extends AbstractTupleRW implements TupleGridCursor{

    private final NumberType type;
    private final int nbSample;

    public EmptyTupleCursor(NumberType type, int nbSample) {
        this.type = type;
        this.nbSample = nbSample;
    }

    @Override
    public TupleRW samples() {
        return this;
    }

    @Override
    public Tuple coordinate() {
        throw new MishandleException("Empty cursor");
    }

    @Override
    public void moveTo(Tuple coordinate) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public boolean next() {
        return false;
    }

    @Override
    public NumberType getNumericType() {
        return type;
    }

    @Override
    public int getSampleCount() {
        return nbSample;
    }

    @Override
    public double get(int indice) {
        throw new MishandleException("Not supported.");
    }

    @Override
    public Number getNumber(int indice) {
        throw new MishandleException("Not supported.");
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        throw new MishandleException("Not supported.");
    }

    @Override
    public void set(int indice, double value) {
        throw new MishandleException("Not supported.");
    }

    @Override
    public void set(int indice, Arithmetic value) {
        throw new MishandleException("Not supported.");
    }

    @Override
    public void moveTo(int coordinate) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public TupleRW create(int size) {
        return VectorNf64.createDouble(size);
    }
}
