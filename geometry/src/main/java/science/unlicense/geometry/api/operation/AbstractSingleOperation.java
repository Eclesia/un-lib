
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractSingleOperation extends AbstractOperation {

    protected Geometry geometry;

    public AbstractSingleOperation() {
    }

    public AbstractSingleOperation(Geometry geom) {
        this.geometry = geom;
    }

    public AbstractSingleOperation(Geometry geom, double epsilon) {
        super(epsilon);
        this.geometry = geom;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geom) {
        this.geometry = geom;
    }

}
