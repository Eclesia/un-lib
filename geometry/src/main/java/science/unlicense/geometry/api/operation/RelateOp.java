
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class RelateOp extends AbstractBinaryOperation{

    public RelateOp(Geometry first, Geometry second) {
        super(first,second);
    }

}
