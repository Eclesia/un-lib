
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class SymDifferenceOp extends AbstractBinaryOperation{

    public SymDifferenceOp(Geometry first, Geometry second) {
        super(first,second);
    }

    @Override
    public Geometry execute() throws OperationException {
        return (Geometry) super.execute();
    }
}
