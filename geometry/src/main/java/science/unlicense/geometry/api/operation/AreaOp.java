
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 * Calculate geometry area.
 *
 * @author Johann Sorel
 */
public class AreaOp extends AbstractSingleOperation {

    public AreaOp(Geometry geometry) {
        super(geometry);
    }

    @Override
    public Double execute() throws OperationException {
        return (Double) super.execute();
    }

}
