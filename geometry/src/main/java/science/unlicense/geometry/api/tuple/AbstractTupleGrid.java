
package science.unlicense.geometry.api.tuple;

import java.lang.reflect.Array;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.common.api.number.UInt32;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.tuple.BoxCursor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractTupleGrid extends CObject implements TupleGrid {

    protected SampleSystem coordinateSystem;
    protected SampleSystem sampleSystem;
    protected ArithmeticType sampleType;
    protected int nbSample;
    protected Extent.Long dimensions;
    protected BBox fullBbox;

    public AbstractTupleGrid() {
        updateInternal(new Extent.Long(0l),UInt8.TYPE,new UndefinedSystem(1));
    }

    /**
     *
     * @param dimensions
     * @param sampleType : one of Primitive.Primitive.TYPE_X
     * @param nbSample
     */
    public AbstractTupleGrid(final Extent.Long dimensions, final ArithmeticType sampleType, final SampleSystem ss) {
        updateInternal(dimensions, sampleType, ss);
    }
    /**
     *
     * @param dimensions
     * @param sampleType
     * @param nbSample
     */
    protected void updateModel(final Extent.Long dimensions, final ArithmeticType sampleType, final int nbSample){
        updateInternal(dimensions,sampleType,new UndefinedSystem(nbSample));
    }

    private void updateInternal(final Extent.Long dimensions, final ArithmeticType sampleType, final SampleSystem ss){
        this.sampleSystem = ss;
        this.coordinateSystem = new UndefinedSystem(dimensions.getDimension());
        this.dimensions = dimensions;
        this.sampleType = sampleType;
        this.fullBbox = new BBox(dimensions);
        this.nbSample = sampleSystem.getNumComponents();
    }

    @Override
    public SampleSystem getCoordinateSystem() {
        return coordinateSystem;
    }

    @Override
    public SampleSystem getSampleSystem() {
        return sampleSystem;
    }

    @Override
    public BBox getCoordinateGeometry() {
        return new BBox(fullBbox);
    }

    @Override
    public Extent.Long getExtent() {
        return dimensions;
    }

    @Override
    public final ArithmeticType getNumericType() {
        return sampleType;
    }

    @Override
    public TupleRW createTuple() {
        return VectorNf64.createDouble(nbSample);
    }

    @Override
    public void setTuple(BBox box, Tuple sample) {
        box = clipToBufferBox(box);
        if (box==null) return;

        final int nbDim = box.getDimension();
        final VectorRW coordinate = VectorNf64.create(box.getLower());

        combinaison:
        for (;;){
            setTuple(coordinate, sample);
            //prepare next iteration
            for (int i=nbDim-1;i>=0;i--){
                coordinate.set(i, coordinate.get(i)+1);
                if (coordinate.get(i) >= box.getMax(i)){
                    if (i==0){
                        //we have finish
                        break combinaison;
                    }
                    //continue loop increment previous dimension
                    coordinate.set(i, box.getMin(i));
                } else {
                    break;
                }
            }
        }
    }

    @Override
    public void getTuple(BBox box, TupleGrid buffer) {
        if (box == null) {
            final TupleGridCursor c = cursor();
            while (c.next()) {
                buffer.setTuple(c.coordinate(), c.samples());
            }
        } else {
            final TupleRW lower = box.getLower();
            final VectorRW position = VectorNf64.createDouble(coordinateSystem.getNumComponents());
            final TupleGridCursor c = new BoxCursor(cursor(), box);
            while (c.next()) {
                position.set(c.coordinate());
                position.localSubtract(lower);
                buffer.setTuple(position, c.samples());
            }
        }
    }

    @Override
    public TupleGridCursor cursor() {
        return new DefaultTupleGridCursor(this);
    }

    /**
     * Ensure given bbox do not exit buffer bounds.
     * @param box
     * @return BoundingBox or null if no intersection, do not modify returned bbox
     */
    protected BBox clipToBufferBox(BBox box) {
        if (box == null) {
            //return full extent
            return fullBbox;
        }

        //ensure we are in the buffer zone
        final int size = dimensions.getDimension();
        for (int i=0;i<size;i++) {
            long min = (int) box.getMin(i);
            if (min<0) min=0;
            if (min>=dimensions.getL(i)) return null;

            long max = (int) (box.getMax(i)+0.5);
            if (max<0) return null;
            if (max>dimensions.getL(i)) max = dimensions.getL(i);

            box.setRange(i, min, max);
        }

        return box;
    }

    /**
     * Test and convert sample container to this buffer type.
     * @param sampleContainer
     * @return
     */
    protected Object ensureType(Object sampleContainer){
        switch(sampleType.getPrimitiveCode()){
            case Primitive.BITS1 :
                if (sampleContainer instanceof boolean[]) return sampleContainer;
                final boolean[] bools = new boolean[nbSample];
                for (int i=0;i<bools.length;i++) bools[i] = ((Number) Array.get(sampleContainer, i)).intValue() != 0;
                return bools;
            case Primitive.BITS2 :
            case Primitive.BITS4 :
            case Primitive.INT8 :
                if (sampleContainer instanceof byte[]) return sampleContainer;
                final byte[] bytes = new byte[nbSample];
                for (int i=0;i<bytes.length;i++) bytes[i] = ((Number) Array.get(sampleContainer, i)).byteValue();
                return bytes;
            case Primitive.UINT8 :
                if (sampleContainer instanceof int[]) return sampleContainer;
                final int[] ubytes = new int[nbSample];
                for (int i=0;i<nbSample;i++) ubytes[i] = ((Number) Array.get(sampleContainer, i)).intValue();
                return ubytes;
            case Primitive.INT16 :
                if (sampleContainer instanceof short[]) return sampleContainer;
                final short[] shorts = new short[nbSample];
                for (int i=0;i<shorts.length;i++) shorts[i] = ((Number) Array.get(sampleContainer, i)).shortValue();
                return shorts;
            case Primitive.UINT16 :
                if (sampleContainer instanceof int[]) return sampleContainer;
                final int[] ushorts = new int[nbSample];
                for (int i=0;i<ushorts.length;i++) ushorts[i] = ((Number) Array.get(sampleContainer, i)).intValue();
                return ushorts;
            case Primitive.INT32 :
                if (sampleContainer instanceof int[]) return sampleContainer;
                final int[] ints = new int[nbSample];
                for (int i=0;i<ints.length;i++) ints[i] = ((Number) Array.get(sampleContainer, i)).intValue();
                return ints;
            case Primitive.UINT32 :
                if (sampleContainer instanceof long[]) return sampleContainer;
                final long[] uints = new long[nbSample];
                for (int i=0;i<uints.length;i++) uints[i] = ((Number) Array.get(sampleContainer, i)).longValue();
                return uints;
            case Primitive.INT64 :
                if (sampleContainer instanceof long[]) return sampleContainer;
                final long[] longs = new long[nbSample];
                for (int i=0;i<longs.length;i++) longs[i] = ((Number) Array.get(sampleContainer, i)).longValue();
                return longs;
            case Primitive.FLOAT32 :
                if (sampleContainer instanceof float[]) return sampleContainer;
                final float[] floats = new float[nbSample];
                for (int i=0;i<floats.length;i++) floats[i] = ((Number) Array.get(sampleContainer, i)).floatValue();
                return floats;
            case Primitive.FLOAT64 :
                if (sampleContainer instanceof double[]) return sampleContainer;
                final double[] doubles = new double[nbSample];
                for (int i=0;i<doubles.length;i++) doubles[i] = ((Number) Array.get(sampleContainer, i)).doubleValue();
                return doubles;
            default: throw new UnimplementedException("Unusual data type, sample model should overide this method.");
        }
    }

    @Override
    public int getHash() {
        int hash = 7;
        hash = 47 * hash + CObjects.getHash(getNumericType());
        hash = 47 * hash + getSampleSystem().getNumComponents();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TupleGrid other = (TupleGrid) obj;
        if (this.sampleType != other.getNumericType()) {
            return false;
        }
        if (this.nbSample != other.getSampleSystem().getNumComponents()) {
            return false;
        }
        if (!this.dimensions.equals(other.getExtent())) {
            return false;
        }

        final TupleGridCursor cursor = cursor();
        final TupleGridCursor otherCursor = other.cursor();

        while (cursor.next()) {
            otherCursor.moveTo(cursor.coordinate());
            if (!cursor.equals(otherCursor)) {
                return false;
            }
        }
        return true;
    }

    public static Buffer createPrimitiveBuffer(Extent.Long dimensions, NumberType sampleType, int nbSample, BufferFactory factory) {
        long size = nbSample;
        for (int i=0,n=dimensions.getDimension();i<n;i++){
            size *= dimensions.get(i);
        }

        if (sampleType == Int32.TYPE || sampleType == UInt32.TYPE) {
            return factory.createInt32(size).getBuffer();
        } else if (sampleType == Float32.TYPE) {
            return factory.createFloat32(size).getBuffer();
        } else if (sampleType == Float64.TYPE) {
            return factory.createFloat64(size).getBuffer();
        } else {
            size *= sampleType.getSizeInBits();
            size = ((size+7) / 8) ;
            return factory.createInt8(size).getBuffer();
        }

    }

}
