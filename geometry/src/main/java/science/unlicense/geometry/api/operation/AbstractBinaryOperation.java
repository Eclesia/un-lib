
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractBinaryOperation extends AbstractOperation{

    protected Geometry first;
    protected Geometry second;

    public AbstractBinaryOperation() {
    }

    public AbstractBinaryOperation(Geometry first, Geometry second) {
        this.first = first;
        this.second = second;
    }

    public AbstractBinaryOperation(Geometry first, Geometry second, double epsilon) {
        super(epsilon);
        this.first = first;
        this.second = second;
    }

    public Object getFirst() {
        return first;
    }

    public void setFirst(Geometry first) {
        this.first = first;
    }

    public Object getSecond() {
        return second;
    }

    public void setSecond(Geometry second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+" ("+first.getClass().getSimpleName()+","+second.getClass().getSimpleName()+")";
    }

}
