
package science.unlicense.geometry.api;

import science.unlicense.math.api.TupleRW;

/**
 * A Line segment .
 *
 * Specification :
 * - SVG v1.1:9.5 :
 *   The ‘line’ element defines a line segment that starts at one point and ends at another.
 *
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public interface Line extends PlanarGeometry {

    TupleRW getStart();

    TupleRW getEnd();

}
