
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 * Transform a geometry to triangles.
 * This operation is possible only on geometries with surfaces.
 *
 * @author Johann Sorel
 */
public class TriangulateOp extends AbstractSingleOperation {

    private double resolution;
    private int nbRing;
    private int nbSector;

    public TriangulateOp(Geometry geometry, double resolution) {
        super(geometry);
        this.resolution = resolution;
    }

    public double getResolution() {
        return resolution;
    }

    public void setResolution(double resolution) {
        this.resolution = resolution;
    }

    public int getNbRing() {
        return nbRing;
    }

    public void setNbRing(int nbRing) {
        this.nbRing = nbRing;
    }

    public int getNbSector() {
        return nbSector;
    }

    public void setNbSector(int nbSector) {
        this.nbSector = nbSector;
    }

    /**
     * Returned geometry can be a multi-geometry, triangle, triangle fan or triangle trips.
     *
     * @return
     * @throws OperationException
     */
    @Override
    public Geometry execute() throws OperationException {
        return (Geometry) super.execute();
    }

}
