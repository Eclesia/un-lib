
package science.unlicense.geometry.api.system;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.MeasuredAxis;
import science.unlicense.math.api.unitold.Unit;
import science.unlicense.math.api.unitold.Units;

/**
 *
 * @author Johann Sorel
 */
public class Axis extends MeasuredAxis {

    public static final Axis UNDEFINED = new Axis(Direction.UNDEFINED, Units.UNDEFINED);

    private final Direction direction;

    public Axis() {
        this(Direction.UNDEFINED, Units.UNDEFINED);
    }

    public Axis(Direction direction, Unit unit) {
        super(unit);
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

    public Chars toChars() {
        return new Chars("Axis[").concat(direction.getName()).concat(',').concat(unit.toChars()).concat(']');
    }

    @Override
    public int getHash() {
        int hash = 7;
        hash = 67 * hash + (this.direction != null ? this.direction.hashCode() : 0);
        hash = 67 * hash + (this.unit != null ? this.unit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Axis other = (Axis) obj;
        if (this.direction != other.direction && (this.direction == null || !this.direction.equals(other.direction))) {
            return false;
        }
        return !(this.unit != other.unit && (this.unit == null || !this.unit.equals(other.unit)));
    }

}
