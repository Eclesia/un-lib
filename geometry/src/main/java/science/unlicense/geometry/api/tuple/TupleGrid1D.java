
package science.unlicense.geometry.api.tuple;

import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public interface TupleGrid1D extends TupleGrid {

    int getDimension();

    void getTuple(int coordinate, TupleRW buffer);

    /**
     * set the given coordinate with given samples.
     *
     * @param coordinate
     * @param sample
     */
    void setTuple(int coordinate, Tuple sample);

}
