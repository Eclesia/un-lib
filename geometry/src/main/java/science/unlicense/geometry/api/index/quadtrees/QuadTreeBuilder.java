
package science.unlicense.geometry.api.index.quadtrees;

import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;


/**
 * The default implementation of the {@link QuadTreeBuilderType}.
 *
 * @param <T> The precise type of quadtree members.
 * @author Mark Raynsford
 */
public final class QuadTreeBuilder<T extends QuadTreeMemberType<T>> implements QuadTreeBuilderType<T> {

  /**
   * @return A new quadtree builder.
   *
   * @param <T>
   *          The precise type of quadtree members.
   */
  public static
    <T extends QuadTreeMemberType<T>>
    QuadTreeBuilderType<T>
    newBuilder()
  {
    return new QuadTreeBuilder<T>();
  }

  private boolean         limit;
  private final VectorRW limit_size;
  private final VectorRW position;
  private boolean         prune;
  private final VectorRW size;

  private QuadTreeBuilder()
  {
    this.limit_size = VectorNf64.createDouble(2);
    this.size = VectorNf64.createDouble(2);
    this.position = VectorNf64.createDouble(2);
  }

  @Override public QuadTreeType<T> build()
  {
    if (this.prune) {
      if (this.limit) {
        return QuadTreePruneLimit.newQuadTree(
          this.size,
          this.position,
          this.limit_size);
      }
      return QuadTreePrune.newQuadTree(this.size, this.position);
    }

    if (this.limit) {
      return QuadTreeLimit.newQuadTree(
        this.size,
        this.position,
        this.limit_size);
    }

    return QuadTreeBasic.newQuadTree(this.size, this.position);
  }

  @Override public QuadTreeSDType<T> buildWithSD()
  {
    if (this.prune) {
      if (this.limit) {
        return QuadTreeSDPruneLimit.newQuadTree(
          this.size,
          this.position,
          this.limit_size);
      }
      return QuadTreeSDPrune.newQuadTree(this.size, this.position);
    }
    if (this.limit) {
      return QuadTreeSDLimit.newQuadTree(
        this.size,
        this.position,
        this.limit_size);
    }
    return QuadTreeSDBasic.newQuadTree(this.size, this.position);
  }

  @Override public void disableLimitedQuadrantSizes()
  {
    this.limit = false;
  }

  @Override public void disablePruning()
  {
    this.prune = false;
  }

  @Override public void enableLimitedQuadrantSizes(
    final int x,
    final int y)
  {
    QuadTreeChecks.checkSize("Quadrant minimum size", x, y);
    this.limit = true;
    this.limit_size.setXY(x, y);
  }

  @Override public void enablePruning()
  {
    this.prune = true;
  }

  @Override public void setPosition2i(
    final int x,
    final int y)
  {
    this.position.setXY(x, y);
  }

  @Override public void setSize2i(
    final int x,
    final int y)
  {
    QuadTreeChecks.checkSize("Quadtree size", x, y);
    this.size.setXY(x, y);
  }

}
