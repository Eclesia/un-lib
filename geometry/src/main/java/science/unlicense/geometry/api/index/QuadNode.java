
package science.unlicense.geometry.api.index;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.Maths;
import science.unlicense.common.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public class QuadNode extends BBox{

    private final Sequence records = new ArraySequence();
    private QuadNode[] children;

    public QuadNode(int dimension) {
        super(dimension);
    }

    public QuadNode(BBox base) {
        super(base);
    }

    public QuadNode[] getChildren() {
        return children;
    }

    public QuadNode[] getChildrenCreate() {
        if (children==null){
            //create children nodes
            children = new QuadNode[(int) Maths.pow(2,dimension)];
            for (int i=0;i<children.length;i++) children[i] = new QuadNode(this);
            for (int i=0;i<dimension;i++) createChildren(i);
        }
        return children;
    }

    private void createChildren(int d){
        final double minx = getMin(d);
        final double medx = getMiddle(d);
        final double maxx = getMax(d);
        final int step = ((int) Maths.pow(2,dimension-d))/2;

        for (int i=0;i<children.length;i++) {
            if ((i/step)%2==0){
                children[i].setRange(d, minx, medx);
            } else {
                children[i].setRange(d, medx, maxx);
            }
        }

    }

    public Sequence getRecords() {
        return records;
    }

    public void getAllRecords(Sequence lst){
        lst.addAll(records);
        if (children!=null){
            for (int i=0;i<children.length;i++){
                children[i].getAllRecords(lst);
            }
        }
    }

    public int getNbRecords(){
        int nb = records.getSize();
        if (children!=null){
            for (int i=0;i<children.length;i++){
                nb += children[i].getNbRecords();
            }
        }
        return nb;
    }

    public void getRecordBounds(BBox bbox){
        for (int i=0;i<records.getSize();i++){
            final IndexRecord rec = (IndexRecord) records.get(i);
            if (bbox.isValid()){
                bbox.expand(rec);
            } else {
                bbox.set(rec);
            }
        }
        if (children!=null){
            for (int i=0;i<children.length;i++){
                children[i].getRecordBounds(bbox);
            }
        }
    }

    public Chars toChars() {
        Chars text = super.toChars();
        if (!records.isEmpty()){
            text = text.concat(new Chars(" nbrec : "+records.getSize()));
        }

        if (children!=null){
            return Nodes.toChars(text, children);
        } else {
            return text;
        }
    }

}
