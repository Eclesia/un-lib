
package science.unlicense.geometry.api;

import science.unlicense.geometry.api.tuple.TupleGrid1D;

/**
 *
 * Specification :
 * - OpenGL : one of the opengl primitives
 *
 * @author Johann Sorel
 */
public interface TriangleFan extends Geometry {

    TupleGrid1D getCoordinates();

}
