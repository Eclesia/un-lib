
package science.unlicense.geometry.api.system;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.math.api.transform.ConcatenateTransform;
import science.unlicense.math.api.transform.DimStackTransform;
import science.unlicense.math.api.transform.RearrangeTransform;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.api.unitold.Units;
import science.unlicense.math.impl.Affine1;

/**
 *
 * @author Johann Sorel
 */
public final class CoordinateSystems {

    public static final CoordinateSystem UNDEFINED_1D = new DefaultCoordinateSystem(
            new Axis[]{Axis.UNDEFINED});
    public static final CoordinateSystem UNDEFINED_2D = new DefaultCoordinateSystem(
            new Axis[]{Axis.UNDEFINED, Axis.UNDEFINED});
    public static final CoordinateSystem UNDEFINED_3D = new DefaultCoordinateSystem(
            new Axis[]{Axis.UNDEFINED, Axis.UNDEFINED, Axis.UNDEFINED});
    public static final CoordinateSystem UNDEFINED_4D = new DefaultCoordinateSystem(
            new Axis[]{Axis.UNDEFINED, Axis.UNDEFINED, Axis.UNDEFINED, Axis.UNDEFINED});
    public static final CoordinateSystem UNDEFINED_5D = new DefaultCoordinateSystem(
            new Axis[]{Axis.UNDEFINED, Axis.UNDEFINED, Axis.UNDEFINED, Axis.UNDEFINED, Axis.UNDEFINED});


    public static final CoordinateSystem CARTESIAN3D_METRIC_RIGH_HANDED = new DefaultCoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.METER),
                new Axis(Direction.UP, Units.METER),
                new Axis(Direction.BACKWARD, Units.METER)
            }
    );

    public static final CoordinateSystem CARTESIAN3D_METRIC_LEFT_HANDED = new DefaultCoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.METER),
                new Axis(Direction.UP, Units.METER),
                new Axis(Direction.FORWARD, Units.METER)
            }
    );

    /**
     * https://en.wikipedia.org/wiki/ECEF
     *
     * Axis in order :
     * Forward
     * Left
     * Up
     */
    public static final CoordinateSystem EARTH_CENTERED_ROTATIONAL = new DefaultCoordinateSystem(
            new Axis[]{
                new Axis(Direction.FORWARD, Units.METER),
                new Axis(Direction.LEFT, Units.METER),
                new Axis(Direction.UP, Units.METER)
            }
    );

    private CoordinateSystems(){}

    public static CoordinateSystem undefined(int dimension) {
        switch (dimension) {
            case 1 : return UNDEFINED_1D;
            case 2 : return UNDEFINED_2D;
            case 3 : return UNDEFINED_3D;
            case 4 : return UNDEFINED_4D;
            case 5 : return UNDEFINED_5D;
            default : {
                final Axis[] axis = new Axis[dimension];
                for (int i=0; i<dimension; i++) axis[i] = Axis.UNDEFINED;
                return new DefaultCoordinateSystem(axis);
            }
        }
    }

    public static boolean isUndefined(CoordinateSystem cs) {
        if (   UNDEFINED_1D == cs
            || UNDEFINED_2D == cs
            || UNDEFINED_3D == cs
            || UNDEFINED_4D == cs
            || UNDEFINED_5D == cs ) return true;

        final Axis[] axis = cs.getAxis();
        for (int i=0;i<axis.length;i++) {
            if (!Axis.UNDEFINED.equals(axis[i])) return false;
        }
        return true;
    }

    /**
     * @param source
     * @param target
     * @return
     */
    public static Transform createTransform(CoordinateSystem source, CoordinateSystem target){

        final Axis[] srcAxis = source.getAxis();
        final Axis[] trgAxis = target.getAxis();
        if (srcAxis.length < trgAxis.length){
            throw new InvalidArgumentException("Source dimension has less axis then target : "
                    +srcAxis.length+" "+trgAxis.length);
        }


        //find axis mapping
        final int dim = target.getDimension();
        final int[] mapping = new int[srcAxis.length];
        Arrays.fill(mapping, -1);
        final Transform[] trsArray = new Transform[dim];

        boolean needRearrange = srcAxis.length != trgAxis.length;

        loop:
        for (int i=0;i<trgAxis.length;i++){
            final Axis tgtAxis = trgAxis[i];
            for (int k=0;k<srcAxis.length;k++){
                final Axis srAxis = srcAxis[k];
                if (tgtAxis.getDirection().isCompatible(srAxis.getDirection())!=0){
                    trsArray[i] = createTransform(srAxis, tgtAxis);
                    mapping[k] = i;
                    if (i!=k) needRearrange = true;
                    continue loop;
                }
            }
            throw new InvalidArgumentException("No mapping for axis : "+tgtAxis);
        }

        //aggregate each axis transform
        final Transform stackTrs = DimStackTransform.create(trsArray);

        if (needRearrange){
            //rearrange values
            final Transform rearrange = RearrangeTransform.create(mapping);
            return ConcatenateTransform.create(rearrange,stackTrs);
        } else {
            return stackTrs;
        }

    }

    public static Transform createTransform(Axis source, Axis target){

        final Direction sourceDirection = source.getDirection();
        final Direction targetDirection = target.getDirection();
        //direction factor
        final int factor = sourceDirection.isCompatible(targetDirection);
        if (factor==0){
            throw new InvalidArgumentException("Axis are not compatible");
        }


        //transform for units
        final Transform unitTransform = Units.getTransform(source.getUnit(), target.getUnit());

        if (factor==1){
            return unitTransform;
        } else {
            //invert values
            final Transform scaleTransform = new Affine1(-1,0);
            return ConcatenateTransform.create(unitTransform, scaleTransform);
        }

    }

}
