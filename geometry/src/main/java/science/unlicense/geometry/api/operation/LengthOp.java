
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 * Calculate geometry length.
 *
 * @author Johann Sorel
 */
public class LengthOp extends AbstractSingleOperation {

    public LengthOp(Geometry geometry) {
        super(geometry);
    }

    @Override
    public Double execute() throws OperationException {
        return (Double) super.execute();
    }

}
