
package science.unlicense.geometry.api;

import science.unlicense.geometry.api.tuple.TupleGrid1D;

/**
 * A line strip is a serie of connected line segments.
 * Each coordinate connected to the previous.
 *
 * Specification :
 * - OpenGL : one of the opengl primitives
 *
 * @author Johann Sorel
 */
public interface LineStrip extends Geometry {

    TupleGrid1D getCoordinates();

}
