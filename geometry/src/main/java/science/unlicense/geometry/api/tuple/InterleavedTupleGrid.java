
package science.unlicense.geometry.api.tuple;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DataCursor;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Bits;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int16;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int64;
import science.unlicense.common.api.number.Int8;
import science.unlicense.common.api.number.Number;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.common.api.number.UInt16;
import science.unlicense.common.api.number.UInt32;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.AbstractTupleRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.impl.Scalari32;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.VectorNf64;

/**
 * Default tuple buffer implementation where each tuple are next to each other.
 *
 *
 * @author Johann Sorel
 */
public class InterleavedTupleGrid extends AbstractNumberTupleGrid{

    protected final Buffer bank;
    //contains the steps between each value of a dimension
    private final int[] bitDimSteps;
    private final long[] extentL;
    private final long totalBitsLength;

    public static TupleGrid create(NumberType sampleType,
                SampleSystem sampleSystem, Extent.Long dims) {
        long size = 1;
        for (int i=0;i<dims.getDimension();i++) {
            size *= dims.getL(i);
        }
        Buffer bank = DefaultBufferFactory.INSTANCE.create(size, sampleType, Endianness.BIG_ENDIAN);
        return create(bank,sampleType, sampleSystem, dims);
    }

    public static TupleGrid create(Buffer bank,NumberType sampleType,
                SampleSystem sampleSystem, Extent.Long dims) {
        final int nbComp = sampleSystem.getNumComponents();
        final int pcode = sampleType.getPrimitiveCode();

        switch (nbComp) {
            case 2 :
                switch(pcode) {
                    case Primitive.UINT8 : return new UByte2(bank, sampleSystem, dims);
                    case Primitive.FLOAT32 : return new Float2(bank, sampleSystem, dims);
                }
            case 3 :
                switch(pcode) {
                    case Primitive.UINT8 : return new UByte3(bank, sampleSystem, dims);
                    case Primitive.FLOAT32 : return new Float3(bank, sampleSystem, dims);
                }
            case 4 :
                switch(pcode) {
                    case Primitive.UINT8 : return new UByte4(bank, sampleSystem, dims);
                    case Primitive.FLOAT32 : return new Float4(bank, sampleSystem, dims);
                }
        }

        if (nbComp < 6) {
            switch (sampleType.getPrimitiveCode()) {
                case Primitive.UINT8 : return new UByte(bank, sampleSystem, dims);
                case Primitive.INT32 : return new Int(bank, sampleSystem, dims);
                case Primitive.UINT32 : return new UInt(bank, sampleSystem, dims);
                case Primitive.FLOAT32 : return new Float(bank, sampleSystem, dims);
            }
        }
        return new InterleavedTupleGrid(bank, sampleType, sampleSystem, dims);
    }

    /**
     * Constructor for ND samples.
     */
    protected InterleavedTupleGrid(Buffer bank, NumberType sampleType, SampleSystem sampleSystem,
            Extent.Long dims) {
        super(dims, sampleType, sampleSystem);
        this.bank = bank;
        this.bitDimSteps = new int[this.dimensions.getDimension()];
        this.bitDimSteps[0] = 1;
        for (int i=1;i<bitDimSteps.length;i++){
            this.bitDimSteps[i] = this.bitDimSteps[i-1]* (int) dimensions.get(i-1);
        }
        this.extentL = getExtent().toArray();
        long length = bitsPerDoxel;
        for (int i=0;i<extentL.length;i++){
            length *= extentL[i];
        }
        this.totalBitsLength = length;
    }

    /**
     * @return Image data bank
     */
    public Buffer getPrimitiveBuffer() {
        return bank;
    }

    protected int getStartIndexInByte(Tuple coordinate){
        return (int) (getStartIndexInBits(coordinate)/8l);
    }

    protected int getStartIndexInByte(int[] coordinate){
        return getStartIndexInBits(coordinate)/8;
    }

    protected int getStartIndexInByte(int coordinate){
        return getStartIndexInBits(coordinate)/8;
    }

    protected int getStartIndexInBits(int coordinate){
        return bitDimSteps[0]*coordinate*bitsPerDoxel;
    }

    protected int getStartIndexInBits(int[] coordinate){
        //we do not check coordinate
        int position = 0;
        switch (extentL.length) {
            case 3 :
                if (coordinate[2] >= extentL[2]) throw new RuntimeException("Invalid coordinate at index 2 value "+coordinate[2]+", out of buffer range");
                position += bitDimSteps[2]*coordinate[2];
            case 2 :
                if (coordinate[1] >= extentL[1]) throw new RuntimeException("Invalid coordinate at index 1 value "+coordinate[1]+", out of buffer range");
                position += bitDimSteps[1]*coordinate[1];
            case 1 :
                if (coordinate[0] >= extentL[0]) throw new RuntimeException("Invalid coordinate at index 0 value "+coordinate[0]+", out of buffer range");
                position += bitDimSteps[0]*coordinate[0];
                break;
            default:
                for (int i=0;i<coordinate.length;i++){
                    if (coordinate[i] >= extentL[i]){
                        throw new RuntimeException("Invalid coordinate at index "+i+" value "+coordinate[i]+", out of buffer range");
                    }
                    position += bitDimSteps[i]*coordinate[i];
                }
        }
        return position * bitsPerDoxel;
    }

    protected long getStartIndexInBits(Tuple coordinate){
        //we do not check coordinate
        long position = 0;
        long l;
        switch (extentL.length) {
            case 3 :
                l = Math.round(coordinate.get(2));
                if (l >= extentL[2]) throw new RuntimeException("Invalid coordinate at index 2 value " + l + ", out of buffer range");
                position += bitDimSteps[2] * l;
            case 2 :
                l = Math.round(coordinate.get(1));
                if (l >= extentL[1]) throw new RuntimeException("Invalid coordinate at index 1 value " + l + ", out of buffer range");
                position += bitDimSteps[1] * l;
            case 1 :
                l = Math.round(coordinate.get(0));
                if (l >= extentL[0]) throw new RuntimeException("Invalid coordinate at index 0 value " + l + ", out of buffer range");
                position += bitDimSteps[0] * l;
                break;
            default:
                for (int i=0; i<extentL.length; i++){
                    l = Math.round(coordinate.get(i));
                    if (l >= extentL[i]){
                        throw new RuntimeException("Invalid coordinate at index " + i + " value " + l + ", out of buffer range");
                    }
                    position += bitDimSteps[i] * l;
                }
        }
        return position * bitsPerDoxel;
    }

    @Override
    public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {

        switch(sampleType.getPrimitiveCode()){
            case Primitive.BITS1 : {
                final long k = getStartIndexInBits(coordinate);
                final long kb = k/8l;
                final int offset = (int) (k%8l);
                final DataCursor cursor = bank.dataCursor();
                cursor.setByteOffset(kb);
                cursor.setBitOffset(offset);
                for (int i=0;i<nbSample;i++){
                    buffer.set(i, cursor.readBit(1));
                }
            } break;
            case Primitive.BITS2 : {
                final long k = getStartIndexInBits(coordinate);
                final long kb = k/8;
                final int offset = (int) (k%8);
                final DataCursor cursor = bank.dataCursor();
                cursor.setByteOffset(kb);
                cursor.setBitOffset(offset);
                for (int i=0;i<nbSample;i++){
                    buffer.set(i,cursor.readBit(2));
                }
            } break;
            case Primitive.BITS4 : {
                final long k = getStartIndexInBits(coordinate);
                final long kb = k/8;
                final int offset = (int) (k%8);
                final DataCursor cursor = bank.dataCursor();
                cursor.setByteOffset(kb);
                cursor.setBitOffset(offset);
                for (int i=0;i<nbSample;i++){
                    buffer.set(i,cursor.readBit(4));
                }
            } break;
            case Primitive.INT8 : {
                int k = getStartIndexInByte(coordinate);
                for (int i=0;i<nbSample;i++,k+=1) buffer.set(i,bank.readInt8(k));
            } break;
            case Primitive.UINT8 : {
                final int start = getStartIndexInByte(coordinate);
                for (int i=0,k=start;i<nbSample;i++,k+=1) buffer.set(i, bank.readInt8(k) & 0xff);
            } break;
            case Primitive.INT16 : {
                int offset = getStartIndexInByte(coordinate);
                for (int i=0;i<nbSample;i++,offset+=2){
                    buffer.set(i, bank.readInt16(offset));
                }
            } break;
            case Primitive.UINT16 : {
                int offset = getStartIndexInByte(coordinate);
                for (int i=0;i<nbSample;i++,offset+=2){
                    buffer.set(i, bank.readUInt16(offset));
                }
            } break;
            case Primitive.INT32 : {
                int offset = getStartIndexInByte(coordinate);
                for (int i=0;i<nbSample;i++,offset+=4){
                    buffer.set(i, bank.readInt32(offset));
                }
            } break;
            case Primitive.UINT32 : {
                int offset = getStartIndexInByte(coordinate);
                for (int i=0;i<nbSample;i++,offset+=4){
                    buffer.set(i, bank.readUInt32(offset));
                }
            } break;
            case Primitive.INT64 : {
                int offset = getStartIndexInByte(coordinate);
                for (int i=0;i<nbSample;i++,offset+=8){
                    buffer.set(i, bank.readInt64(offset));
                }
            } break;
            case Primitive.FLOAT32 : {
                int offset = getStartIndexInByte(coordinate);
                for (int i=0;i<nbSample;i++,offset+=4){
                    buffer.set(i, bank.readFloat32(offset));
                }
            } break;
            case Primitive.FLOAT64 : {
                int offset = getStartIndexInByte(coordinate);
                for (int i=0;i<nbSample;i++,offset+=8){
                    buffer.set(i, bank.readFloat64(offset));
                }
            } break;
            default: throw new RuntimeException("unexpected numeric type : "+sampleType);
        }
        return buffer;
    }

    @Override
    public void setTuple(Tuple coordinate, Tuple tuple) {
        if (sampleType == Bits.TYPE_1_BIT){
            final int start = (int) getStartIndexInBits(coordinate);
            int byteOffset = start/8;
            int bitoffset = start%8;
            for (int i=0;i<nbSample;i++){
                byte b = bank.readInt8(byteOffset);
                boolean v = tuple.get(i) != 0;
                if (v){
                    b |= 1 << (7-bitoffset);
                } else {
                    b &= ~(1 << (7-bitoffset));
                }

                bank.writeInt8(b, byteOffset);
                //prepare next iteration
                if (bitoffset == 7){
                    byteOffset++;
                    bitoffset=0;
                } else {
                    bitoffset++;
                }
            }
        } else if (sampleType == Bits.TYPE_2_BIT){
            throw new RuntimeException("Unexpected type "+ sampleType);
        } else if (sampleType == Bits.TYPE_4_BIT){
            throw new RuntimeException("Unexpected type "+ sampleType);
        } else if (sampleType == Int8.TYPE){
            final int start = getStartIndexInByte(coordinate);
            final byte[] buffer = tuple.toByte();
            bank.writeInt8(buffer, start);
        } else if (sampleType == UInt8.TYPE){
            final int start = getStartIndexInByte(coordinate);
            final int[] buffer = tuple.toInt();
            bank.writeUInt8(buffer, start);
        } else if (sampleType == Int16.TYPE){
            final int start = getStartIndexInByte(coordinate);
            final short[] buffer = tuple.toShort();
            bank.writeInt16(buffer, start);
        } else if (sampleType == UInt16.TYPE){
            final int start = getStartIndexInByte(coordinate);
            final int[] buffer = tuple.toInt();
            bank.writeUInt16(buffer, start);
        } else if (sampleType == Int32.TYPE){
            final int start = getStartIndexInByte(coordinate);
            final int[] buffer = tuple.toInt();
            bank.writeInt32(buffer, start);
        } else if (sampleType == UInt32.TYPE){
            final int start = getStartIndexInByte(coordinate);
            final long[] buffer = tuple.toLong();
            bank.writeUInt32(buffer, start);
        } else if (sampleType == Int64.TYPE){
            final int start = getStartIndexInByte(coordinate);
            final long[] buffer = tuple.toLong();
            bank.writeInt64(buffer, start);
        } else if (sampleType == Float32.TYPE){
            final int start = getStartIndexInByte(coordinate);
            final float[] buffer = tuple.toFloat();
            bank.writeFloat32(buffer, start);
        } else if (sampleType == Float64.TYPE){
            final int start = getStartIndexInByte(coordinate);
            final double[] buffer = tuple.toDouble();
            bank.writeFloat64(buffer, start);
        } else {
            throw new RuntimeException("Unexpected type "+ sampleType);
        }
    }

    @Override
    public void setTuple(BBox box, Tuple sample) {
        if (sampleType == Int8.TYPE && nbSample == 1 && box.getDimension() == 2){
            //more effecient fill for casual 2d buffers
            final int bytePerLine = (int) dimensions.getL(0);

            box = clipToBufferBox(box);
            if (box==null) return;

            //ensure we are in the buffer zone
            int minx = (int) box.getMin(0);
            int maxx = (int) box.getMax(0);
            int miny = (int) box.getMin(1);
            int maxy = (int) box.getMax(1);
            final int spanx = maxx-minx;

            //TODO this is not guarante, backend can be anything
            final byte[] array = (byte[]) bank.getBackEnd();
            final byte b = (byte) sample.get(0);

            int offset = miny*bytePerLine + minx;
            for (int i=miny;i<maxy;i++){
                Arrays.fill(array, offset, spanx, b);
                offset += bytePerLine;
            }

        } else {
            super.setTuple(box, sample);
        }
    }

    @Override
    public TupleGridCursor cursor() {
        return new Cursor();
    }

    private class Cursor extends AbstractTupleRW implements TupleGridCursor {

        protected long offsetBit = -bitsPerDoxel;

        private Cursor() {
        }

        @Override
        public TupleRW samples() {
            return this;
        }

        @Override
        public Tuple coordinate() {
            long v = offsetBit / bitsPerDoxel;

            if (extentL.length == 1){
                return new Scalari32((int) v);
            } else if (extentL.length == 2){
                long vx = v % extentL[0];
                long vy = v / extentL[0];
                return new Vector2i32((int) vx, (int) vy);
            } else {
                throw new RuntimeException("TODO");
            }
        }

        @Override
        public void moveTo(Tuple coordinate) {
            //verify coordinate
            offsetBit = 0;
            for (int i=0;i<extentL.length;i++){
                int c = Math.round((int) coordinate.get(i));
                if (c<0 || c >= extentL[i]) {
                    throw new InvalidIndexException("Invalid coordinate, outside of data range");
                }
                offsetBit += bitDimSteps[i]*c*bitsPerDoxel;
            }
        }

        @Override
        public void moveTo(int coordinate) {
            if (extentL.length != 1) {
                throw new InvalidIndexException("Tuple coordinate have more then one coordinate");
            }
            //verify coordinate
            if (coordinate<0 || coordinate >= extentL[0]) {
                throw new InvalidIndexException("Invalid coordinate " + coordinate + ", outside of data range [0," + extentL[0] + "]. ");
            }
            offsetBit = bitDimSteps[0]*coordinate*bitsPerDoxel;
        }

        @Override
        public boolean next() {
            offsetBit += bitsPerDoxel;
            return offsetBit < totalBitsLength;
        }

        @Override
        public NumberType getNumericType() {
            return sampleType;
        }

        @Override
        public int getSampleCount() {
            return sampleSystem.getNumComponents();
        }

        @Override
        public double get(int indice) {
            switch (sampleType.getPrimitiveCode()) {
                case Primitive.BITS1 : {
                    final int k = (int) (offsetBit + indice);
                    final int kb = k/8;
                    final int offset = k%8;
                    final DataCursor cursor = bank.dataCursor();
                    cursor.setByteOffset(kb);
                    cursor.setBitOffset(offset);
                    return cursor.readBit(1);
                }
                case Primitive.BITS2 : {
                    final int k = (int) (offsetBit + indice*2);
                    final int kb = k/8;
                    final int offset = k%8;
                    final DataCursor cursor = bank.dataCursor();
                    cursor.setByteOffset(kb);
                    cursor.setBitOffset(offset);
                    return cursor.readBit(2);
                }
                case Primitive.BITS4 : {
                    final int k = (int) (offsetBit + indice*4);
                    final int kb = k/8;
                    final int offset = k%8;
                    final DataCursor cursor = bank.dataCursor();
                    cursor.setByteOffset(kb);
                    cursor.setBitOffset(offset);
                    return cursor.readBit(4);
                }
                case Primitive.INT8 : return bank.readInt8(offsetBit/8 + indice);
                case Primitive.UINT8 : return bank.readUInt8(offsetBit/8 + indice);
                case Primitive.INT16 : return bank.readInt16(offsetBit/8 + indice*2);
                case Primitive.UINT16 : return bank.readUInt16(offsetBit/8 + indice*2);
                case Primitive.INT32 : return bank.readInt32(offsetBit/8 + indice*4);
                case Primitive.UINT32 : return bank.readUInt32(offsetBit/8 + indice*4);
                case Primitive.INT64 : return bank.readInt64(offsetBit/8 + indice*8);
                case Primitive.FLOAT32 : return bank.readFloat32(offsetBit/8 + indice*4);
                case Primitive.FLOAT64 : return bank.readFloat64(offsetBit/8 + indice*8);
                default: throw new RuntimeException("unexpected numeric type : "+sampleType);
            }
        }

        @Override
        public Number getNumber(int indice) {
            return new Float64(get(indice));
        }

        @Override
        public void toNumber(Arithmetic[] buffer, int offset) {
            for (int i=0;i<getSampleCount();i++) {
                buffer[offset+i] = getNumber(i);
            }
        }

        @Override
        public void set(int indice, double value) {
            switch (sampleType.getPrimitiveCode()) {
                case Primitive.BITS1 : {
                    final int k = (int) (offsetBit + indice);
                    final int kb = k/8;
                    final int offset = k%8;
                    final DataCursor cursor = bank.dataCursor();
                    cursor.setByteOffset(kb);
                    cursor.setBitOffset(offset);
                    cursor.writeBit((int) value, 1);
                }
                break;
                case Primitive.BITS2 : {
                    final int k = (int) (offsetBit + indice*2);
                    final int kb = k/8;
                    final int offset = k%8;
                    final DataCursor cursor = bank.dataCursor();
                    cursor.setByteOffset(kb);
                    cursor.setBitOffset(offset);
                    cursor.writeBit((int) value, 2);
                }
                break;
                case Primitive.BITS4 : {
                    final int k = (int) (offsetBit + indice*4);
                    final int kb = k/8;
                    final int offset = k%8;
                    final DataCursor cursor = bank.dataCursor();
                    cursor.setByteOffset(kb);
                    cursor.setBitOffset(offset);
                    cursor.writeBit((int) value, 4);
                }
                break;
                case Primitive.INT8 : bank.writeInt8((byte) value, offsetBit/8 + indice); break;
                case Primitive.UINT8 : bank.writeUInt8((int) value, offsetBit/8 + indice); break;
                case Primitive.INT16 : bank.writeInt16((short) value, offsetBit/8 + indice*2); break;
                case Primitive.UINT16 : bank.writeUInt16((int) value, offsetBit/8 + indice*2); break;
                case Primitive.INT32 : bank.writeInt32((int) value, offsetBit/8 + indice*4); break;
                case Primitive.UINT32 : bank.writeUInt32((long) value, offsetBit/8 + indice*4); break;
                case Primitive.INT64 : bank.writeInt64((long) value, offsetBit/8 + indice*8); break;
                case Primitive.FLOAT32 : bank.writeFloat32((float) value, offsetBit/8 + indice*4); break;
                case Primitive.FLOAT64 : bank.writeFloat64(value, offsetBit/8 + indice*8); break;
                default: throw new RuntimeException("unexpected numeric type : "+sampleType);
            }
        }

        @Override
        public void set(int indice, Arithmetic value) throws InvalidIndexException {
            set(indice, ((Number) value).toDouble());
        }

        @Override
        public TupleRW create(int size) {
            return VectorNf64.createDouble(size);
        }

    }

    private static class UByte extends InterleavedTupleGrid {

        public UByte(Buffer bank, SampleSystem ss, Extent.Long dims) {
            super(bank, UInt8.TYPE, ss, dims);
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final int k = getStartIndexInByte(coordinate);
            switch (nbSample) {
                case 5 : buffer.set(4, bank.readInt8(k+4) & 0xff);
                case 4 : buffer.set(3, bank.readInt8(k+3) & 0xff);
                case 3 : buffer.set(2, bank.readInt8(k+2) & 0xff);
                case 2 : buffer.set(1, bank.readInt8(k+1) & 0xff);
                case 1 : buffer.set(0, bank.readInt8(k  ) & 0xff);
            }
            return buffer;
        }
    }

    private static class UByte2 extends InterleavedTupleGrid {

        public UByte2(Buffer bank, SampleSystem ss, Extent.Long dims) {
            super(bank, UInt8.TYPE, ss, dims);
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final int k = getStartIndexInByte(coordinate);
            buffer.set(0, bank.readInt8(k  ) & 0xff);
            buffer.set(1, bank.readInt8(k+1) & 0xff);
            return buffer;
        }

        @Override
        public TupleGridCursor cursor() {
            return new Cursor() {
                @Override
                public double get(int indice) {
                    return bank.readUInt8(offsetBit/8 + indice);
                }
            };
        }
    }

    private static class UByte3 extends InterleavedTupleGrid {

        public UByte3(Buffer bank, SampleSystem ss, Extent.Long dims) {
            super(bank, UInt8.TYPE, ss, dims);
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final int k = getStartIndexInByte(coordinate);
            buffer.set(0, bank.readInt8(k  ) & 0xff);
            buffer.set(1, bank.readInt8(k+1) & 0xff);
            buffer.set(2, bank.readInt8(k+2) & 0xff);
            return buffer;
        }

        @Override
        public TupleGridCursor cursor() {
            return new Cursor() {
                @Override
                public double get(int indice) {
                    return bank.readUInt8(offsetBit/8 + indice);
                }
            };
        }
    }

    private static class UByte4 extends InterleavedTupleGrid {

        public UByte4(Buffer bank, SampleSystem ss, Extent.Long dims) {
            super(bank, UInt8.TYPE, ss, dims);
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final int k = getStartIndexInByte(coordinate);
            buffer.set(0, bank.readInt8(k  ) & 0xff);
            buffer.set(1, bank.readInt8(k+1) & 0xff);
            buffer.set(2, bank.readInt8(k+2) & 0xff);
            buffer.set(3, bank.readInt8(k+3) & 0xff);
            return buffer;
        }
    }

    private static class Int extends InterleavedTupleGrid {

        public Int(Buffer bank, SampleSystem ss, Extent.Long dims) {
            super(bank, Int32.TYPE, ss, dims);
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final int k = getStartIndexInByte(coordinate);
            switch (nbSample) {
                case 5 : buffer.set(4, bank.readInt32(k+16));
                case 4 : buffer.set(3, bank.readInt32(k+12));
                case 3 : buffer.set(2, bank.readInt32(k+ 8));
                case 2 : buffer.set(1, bank.readInt32(k+ 4));
                case 1 : buffer.set(0, bank.readInt32(k   ));
            }
            return buffer;
        }

        @Override
        public TupleGridCursor cursor() {
            return new Cursor() {
                @Override
                public double get(int indice) {
                    return bank.readInt32(offsetBit/8 + indice*4);
                }
            };
        }
    }

    private static class UInt extends InterleavedTupleGrid {

        public UInt(Buffer bank, SampleSystem ss, Extent.Long dims) {
            super(bank, UInt32.TYPE, ss, dims);
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final int k = getStartIndexInByte(coordinate);
            switch (nbSample) {
                case 5 : buffer.set(4, bank.readUInt32(k+16));
                case 4 : buffer.set(3, bank.readUInt32(k+12));
                case 3 : buffer.set(2, bank.readUInt32(k+ 8));
                case 2 : buffer.set(1, bank.readUInt32(k+ 4));
                case 1 : buffer.set(0, bank.readUInt32(k   ));
            }
            return buffer;
        }

        @Override
        public TupleGridCursor cursor() {
            return new Cursor() {
                @Override
                public double get(int indice) {
                    return bank.readUInt32(offsetBit/8 + indice*4);
                }
            };
        }
    }

    private static class Float extends InterleavedTupleGrid {

        public Float(Buffer bank, SampleSystem ss, Extent.Long dims) {
            super(bank, Float32.TYPE, ss, dims);
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final int k = getStartIndexInByte(coordinate);
            switch (nbSample) {
                case 5 : buffer.set(4, bank.readFloat32(k+16));
                case 4 : buffer.set(3, bank.readFloat32(k+12));
                case 3 : buffer.set(2, bank.readFloat32(k+ 8));
                case 2 : buffer.set(1, bank.readFloat32(k+ 4));
                case 1 : buffer.set(0, bank.readFloat32(k   ));
            }
            return buffer;
        }
    }

    private static class Float2 extends InterleavedTupleGrid {

        public Float2(Buffer bank, SampleSystem ss, Extent.Long dims) {
            super(bank, Float32.TYPE, ss, dims);
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final int k = getStartIndexInByte(coordinate);
            buffer.set(0, bank.readFloat32(k   ));
            buffer.set(1, bank.readFloat32(k+4));
            return buffer;
        }
    }

    private static class Float3 extends InterleavedTupleGrid {

        public Float3(Buffer bank, SampleSystem ss, Extent.Long dims) {
            super(bank, Float32.TYPE, ss, dims);
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final int k = getStartIndexInByte(coordinate);
            buffer.set(0, bank.readFloat32(k   ));
            buffer.set(1, bank.readFloat32(k+4));
            buffer.set(2, bank.readFloat32(k+8));
            return buffer;
        }

        @Override
        public TupleGridCursor cursor() {
            return new Cursor() {
                public void toFloat(float[] buffer, int offset) {
                    final int k = (int) (offsetBit / 8);
                    buffer[offset  ] = bank.readFloat32(k);
                    buffer[offset+1] = bank.readFloat32(k+4);
                    buffer[offset+2] = bank.readFloat32(k+8);
                }

                @Override
                public double get(int indice) {
                    return bank.readFloat32(offsetBit/8 + indice*4);
                }
            };
        }

    }

    private static class Float4 extends InterleavedTupleGrid {

        public Float4(Buffer bank, SampleSystem ss, Extent.Long dims) {
            super(bank, Float32.TYPE, ss, dims);
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final int k = getStartIndexInByte(coordinate);
            buffer.set(0, bank.readFloat32(k   ));
            buffer.set(1, bank.readFloat32(k+ 4));
            buffer.set(2, bank.readFloat32(k+ 8));
            buffer.set(3, bank.readFloat32(k+12));
            return buffer;
        }
    }

}
