
package science.unlicense.geometry.api.tuple;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DataCursor;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Bits;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int64;
import science.unlicense.common.api.number.Int8;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt32;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Scalari32;

/**
 * Default tuple buffer implementation where each tuple have only 1 dimension.
 *
 * @author Johann Sorel
 */
public class InterleavedTupleGrid1D extends InterleavedTupleGrid implements TupleGrid1D {

    public static InterleavedTupleGrid1D create(byte[] buffer, int nbSample) {
        return new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(buffer), UInt8.TYPE, UndefinedSystem.create(nbSample), buffer.length/nbSample);
    }

    public static InterleavedTupleGrid1D create(int[] buffer, int nbSample) {
        return new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(buffer), Int32.TYPE, UndefinedSystem.create(nbSample), buffer.length/nbSample);
    }

    public static InterleavedTupleGrid1D create(float[] buffer, int nbSample) {
        return new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(buffer), Float32.TYPE, UndefinedSystem.create(nbSample), buffer.length/nbSample);
    }

    public static InterleavedTupleGrid1D create(double[] buffer, int nbSample) {
        return new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(buffer), Float64.TYPE, UndefinedSystem.create(nbSample), buffer.length/nbSample);
    }

    public static InterleavedTupleGrid1D create(Buffer buffer, int nbSample) {
        return new InterleavedTupleGrid1D(buffer, buffer.getNumericType(), UndefinedSystem.create(nbSample), buffer.getNumbersSize()/nbSample);
    }

    /**
     * Constructor for 1D buffer sample.
     */
    public InterleavedTupleGrid1D(NumberType sampleType, SampleSystem sampleSystem, long size) {
        this(DefaultBufferFactory.INSTANCE.create(size*sampleSystem.getNumComponents(), sampleType, Endianness.BIG_ENDIAN),
                sampleType, sampleSystem, size);
    }

    /**
     * Constructor for 1D buffer sample.
     */
    public InterleavedTupleGrid1D(Buffer bank, SampleSystem sampleSystem) {
        this(bank,bank.getNumericType(),sampleSystem, bank.getNumbersSize()/sampleSystem.getNumComponents());
    }

    /**
     * Constructor for 1D buffer sample.
     */
    public InterleavedTupleGrid1D(Buffer bank,NumberType sampleType, SampleSystem sampleSystem, long size) {
        super(bank,sampleType,sampleSystem,new Extent.Long(size));
    }

    public InterleavedTupleGrid1D(TupleGrid grid) {
        this((NumberType) grid.getNumericType(), grid.getSampleSystem(), grid.getExtent().getL(0));
        grid.getTuple(null, this);
    }

    @Override
    public int getDimension() {
        //NOTE : should it be long or int ?
        return (int) dimensions.getL(0);
    }

    @Override
    public void getTuple(int coordinate, TupleRW buffer) {
        if (sampleType == Int8.TYPE){
            int k = getStartIndexInByte(coordinate);
            for (int i=0;i<nbSample;i++,k+=1) buffer.set(i,bank.readInt8(k));
        } else if (sampleType == Bits.TYPE_1_BIT){
            final int k = getStartIndexInBits(coordinate);
            final int kb = k/8;
            final int offset = k%8;

            final DataCursor cursor = bank.dataCursor();
            cursor.setByteOffset(kb);
            cursor.setBitOffset(offset);
            for (int i=0;i<nbSample;i++){
                buffer.set(i,cursor.readBit(1));
            }

        } else if (sampleType == Bits.TYPE_2_BIT){
            final int k = getStartIndexInBits(coordinate);
            final int kb = k/8;
            final int offset = k%8;

            final DataCursor cursor = bank.dataCursor();
            cursor.setByteOffset(kb);
            cursor.setBitOffset(offset);
            for (int i=0;i<nbSample;i++){
                buffer.set(i,cursor.readBit(2));
            }

        } else if (sampleType == Bits.TYPE_4_BIT){
            final int k = getStartIndexInBits(coordinate);
            final int kb = k/8;
            final int offset = k%8;

            final DataCursor cursor = bank.dataCursor();
            cursor.setByteOffset(kb);
            cursor.setBitOffset(offset);
            for (int i=0;i<nbSample;i++){
                buffer.set(i,cursor.readBit(4));
            }

        } else if (sampleType == UInt8.TYPE){
            final int start = getStartIndexInByte(coordinate);
            for (int i=0,k=start;i<nbSample;i++,k+=1) {
                buffer.set(i, bank.readInt8(k) & 0xff);
            }
        } else if (sampleType == Int32.TYPE){
            final int start = getStartIndexInByte(coordinate);
            for (int i=0,k=start;i<nbSample;i++,k+=4) {
                buffer.set(i, bank.readInt32(k));
            }
        } else if (sampleType == UInt32.TYPE){
            final int start = getStartIndexInByte(coordinate);
            for (int i=0,k=start;i<nbSample;i++,k+=4) {
                buffer.set(i, bank.readUInt32(k));
            }
        } else if (sampleType == Int64.TYPE){
            final int start = getStartIndexInByte(coordinate);
            for (int i=0,k=start;i<nbSample;i++,k+=8) {
                buffer.set(i, bank.readInt64(k));
            }
        } else if (sampleType == Float32.TYPE){
            final int start = getStartIndexInByte(coordinate);
            for (int i=0,k=start;i<nbSample;i++,k+=4) {
                buffer.set(i, bank.readFloat32(k));
            }
        } else if (sampleType == Float64.TYPE){
            final int start = getStartIndexInByte(coordinate);
            for (int i=0,k=start;i<nbSample;i++,k+=8) {
                buffer.set(i, bank.readFloat64(k));
            }
        } else {
            throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
        }
    }

    @Override
    public void setTuple(int coordinate, Tuple sample) {
        setTuple(new Scalari32(coordinate), sample);
    }

}
