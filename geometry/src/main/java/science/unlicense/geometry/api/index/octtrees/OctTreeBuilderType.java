
package science.unlicense.geometry.api.index.octtrees;

import science.unlicense.common.api.exception.InvalidArgumentException;

/**
 * A mutable builder for instantiating octtree implementations.
 *
 * @param <T>
 *          The precise type of members.
 * @author Mark Raynsford
 */
public interface OctTreeBuilderType<T extends OctTreeMemberType<T>>
{
  /**
   * @return A new octtree based on the parameters given so far.
   */

  OctTreeType<T> build();

  /**
   * @return A new SD octtree based on the parameters given so far.
   */

  OctTreeSDType<T> buildWithSD();

  /**
   * Do not use specific minimum quadrant sizes.
   */

  void disableLimitedOctantSizes();

  /**
   * Disable pruning of empty octtree nodes.
   */

  void disablePruning();

  /**
   * Use specific minimum quadrant sizes.
   *
   * @param x
   *          The minimum quadrant width - Must be even and greater than or
   *          equal to <code>2</code>.
   * @param y
   *          The minimum quadrant height - Must be even and greater than or
   *          equal to <code>2</code>.
   * @param z
   *          The minimum quadrant depth - Must be even and greater than or
   *          equal to <code>2</code>.
   *
   * @throws InvalidArgumentException
   *           If the preconditions above are not satisfied.
   */

  void enableLimitedOctantSizes(
    int x,
    int y,
    int z)
      throws InvalidArgumentException;

  /**
   * Enable pruning of empty octtree nodes.
   */

  void enablePruning();

  /**
   * Set the position of the octtree.
   *
   * @param x
   *          The X coordinate.
   * @param y
   *          The Y coordinate.
   * @param z
   *          The Z coordinate.
   */

  void setPosition3i(
    int x,
    int y,
    int z);

  /**
   * <p>
   * Set the size of the octtree.
   * </p>
   *
   * @param x
   *          The width - Must be even and greater than or equal to
   *          <code>2</code>.
   * @param y
   *          The height - Must be even and greater than or equal to
   *          <code>2</code>.
   * @param z
   *          The depth - Must be even and greater than or equal to
   *          <code>2</code>.
   *
   * @throws InvalidArgumentException
   *           If the preconditions above are not satisfied.
   */

  void setSize3i(
    int x,
    int y,
    int z)
      throws InvalidArgumentException;
}
