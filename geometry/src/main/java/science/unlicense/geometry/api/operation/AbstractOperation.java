
package science.unlicense.geometry.api.operation;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractOperation implements Operation{

    public double epsilon;

    public AbstractOperation() {
        this(0.000000001);
    }

    public AbstractOperation(double epsilon) {
        this.epsilon = epsilon;
    }

    public double getEpsilon() {
        return epsilon;
    }

    public void setEpsilon(double epsilon) {
        this.epsilon = epsilon;
    }

    public Object execute() throws OperationException {
        return Operations.execute(this);
    }

}
