
package science.unlicense.geometry.api.operation;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.api.Point;
import science.unlicense.math.api.TupleRW;

/**
 * @author Johann Sorel
 * @author Izyumov Konstantin
 * @see WikiPedia: https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
 */
public class ConvexHullOp extends AbstractSingleOperation {

    public ConvexHullOp(Geometry first) {
        super(first);
    }

    @Override
    public Geometry execute() throws OperationException {
        return (Geometry) super.execute();
    }

    public static Point[] convexHull(Point[] inputPoints) {
        if (inputPoints.length < 2)
            return inputPoints;

        HashSet uniquePoints = new HashSet();
        for (int i = 0; i < inputPoints.length; i++) {
            uniquePoints.add(inputPoints[i]);
        }


        ArraySequence arrayUnique = new ArraySequence(uniquePoints.toArray());

        Collections.sort(arrayUnique, new Sorter() {
            @Override
            public int sort(Object first, Object second) {
                final TupleRW coord1 = ((Point) first).getCoordinate();
                final TupleRW coord2 = ((Point) second).getCoordinate();
                if (coord1.get(0) == coord2.get(0)){
                    if (coord1.get(1) > coord2.get(1))
                        return 1;
                    if (coord1.get(1) < coord2.get(1))
                        return -1;
                    return 0;
                }
                if (coord1.get(0) > coord2.get(0))
                    return 1;
                if (coord1.get(0) < coord2.get(0))
                    return -1;
                return 0;
            }
        });

        int n = arrayUnique.getSize();
        Point[] P = new Point[n];
        for (int i = 0; i < n; i++) {
            P[i] = (Point) arrayUnique.get(i);
        }

        Point[] H = new Point[2 * n];

        int k = 0;
        // Build lower hull
        for (int i = 0; i < n; ++i) {
            while (k >= 2 && Geometries.isCounterClockwise(H[k - 2], H[k - 1], P[i])) {
                k--;
            }
            H[k++] = P[i];
        }

        // Build upper hull
        for (int i = n - 2, t = k + 1; i >= 0; i--) {
            while (k >= t && Geometries.isCounterClockwise(H[k - 2], H[k - 1], P[i])) {
                k--;
            }
            H[k++] = P[i];
        }
        if (k > 1) {
            H = (Point[]) Arrays.copy(H, 0, k - 1); // remove non-hull vertices after k; remove k - 1 which is a duplicate
        }
        return H;
    }
}
