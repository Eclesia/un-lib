
package science.unlicense.geometry.api.index.octtrees;

import science.unlicense.geometry.api.index.Dimensions;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
/**
 * Functions to produce new octants from an existing pair of corners.
 *
 * @author Mark Raynsford
 */
public final class Octants{

  /**
   * Split an octant defined by the two points <code>lower</code> and
   * <code>upper</code> into eight octants.
   *
   * @return Eight new octants
   * @param upper
   *          The upper corner
   * @param lower
   *          The lower corner
   */

  public static Octants split(
    final Tuple lower,
    final Tuple upper)
  {
    final double size_x = Dimensions.getSpanSizeX(lower, upper);
    final double size_y = Dimensions.getSpanSizeY(lower, upper);
    final double size_z = Dimensions.getSpanSizeZ(lower, upper);

    assert size_x >= 2;
    assert size_y >= 2;
    assert size_z >= 2;

    final double[] x_spans = new double[4];
    final double[] y_spans = new double[4];
    final double[] z_spans = new double[4];

    Dimensions.split1D(lower.get(0), upper.get(0), x_spans);
    Dimensions.split1D(lower.get(1), upper.get(1), y_spans);
    Dimensions.split1D(lower.get(2), upper.get(2), z_spans);

    final VectorRW in_x0y0z0_lower =
      new Vector3f64(x_spans[0], y_spans[0], z_spans[0]);
    final VectorRW in_x0y0z0_upper =
      new Vector3f64(x_spans[1], y_spans[1], z_spans[1]);

    final VectorRW in_x1y0z0_lower =
      new Vector3f64(x_spans[2], y_spans[0], z_spans[0]);
    final VectorRW in_x1y0z0_upper =
      new Vector3f64(x_spans[3], y_spans[1], z_spans[1]);

    final VectorRW in_x0y1z0_lower =
      new Vector3f64(x_spans[0], y_spans[2], z_spans[0]);
    final VectorRW in_x0y1z0_upper =
      new Vector3f64(x_spans[1], y_spans[3], z_spans[1]);

    final VectorRW in_x1y1z0_lower =
      new Vector3f64(x_spans[2], y_spans[2], z_spans[0]);
    final VectorRW in_x1y1z0_upper =
      new Vector3f64(x_spans[3], y_spans[3], z_spans[1]);

    final VectorRW in_x0y0z1_lower =
      new Vector3f64(x_spans[0], y_spans[0], z_spans[2]);
    final VectorRW in_x0y0z1_upper =
      new Vector3f64(x_spans[1], y_spans[1], z_spans[3]);

    final VectorRW in_x1y0z1_lower =
      new Vector3f64(x_spans[2], y_spans[0], z_spans[2]);
    final VectorRW in_x1y0z1_upper =
      new Vector3f64(x_spans[3], y_spans[1], z_spans[3]);

    final VectorRW in_x0y1z1_lower =
      new Vector3f64(x_spans[0], y_spans[2], z_spans[2]);
    final VectorRW in_x0y1z1_upper =
      new Vector3f64(x_spans[1], y_spans[3], z_spans[3]);

    final VectorRW in_x1y1z1_lower =
      new Vector3f64(x_spans[2], y_spans[2], z_spans[2]);
    final VectorRW in_x1y1z1_upper =
      new Vector3f64(x_spans[3], y_spans[3], z_spans[3]);

    return new Octants(
      in_x0y0z0_lower,
      in_x0y0z0_upper,
      in_x1y0z0_lower,
      in_x1y0z0_upper,
      in_x0y1z0_lower,
      in_x0y1z0_upper,
      in_x1y1z0_lower,
      in_x1y1z0_upper,
      in_x0y0z1_lower,
      in_x0y0z1_upper,
      in_x1y0z1_lower,
      in_x1y0z1_upper,
      in_x0y1z1_lower,
      in_x0y1z1_upper,
      in_x1y1z1_lower,
      in_x1y1z1_upper);
  }

  private final VectorRW x0y0z0_lower;
  private final VectorRW x0y0z0_upper;
  private final VectorRW x0y0z1_lower;
  private final VectorRW x0y0z1_upper;
  private final VectorRW x0y1z0_lower;
  private final VectorRW x0y1z0_upper;
  private final VectorRW x0y1z1_lower;
  private final VectorRW x0y1z1_upper;
  private final VectorRW x1y0z0_lower;
  private final VectorRW x1y0z0_upper;
  private final VectorRW x1y0z1_lower;
  private final VectorRW x1y0z1_upper;
  private final VectorRW x1y1z0_lower;
  private final VectorRW x1y1z0_upper;
  private final VectorRW x1y1z1_lower;

  private final VectorRW x1y1z1_upper;

  private Octants(
    final VectorRW in_x0y0z0_lower,
    final VectorRW in_x0y0z0_upper,
    final VectorRW in_x1y0z0_lower,
    final VectorRW in_x1y0z0_upper,
    final VectorRW in_x0y1z0_lower,
    final VectorRW in_x0y1z0_upper,
    final VectorRW in_x1y1z0_lower,
    final VectorRW in_x1y1z0_upper,
    final VectorRW in_x0y0z1_lower,
    final VectorRW in_x0y0z1_upper,
    final VectorRW in_x1y0z1_lower,
    final VectorRW in_x1y0z1_upper,
    final VectorRW in_x0y1z1_lower,
    final VectorRW in_x0y1z1_upper,
    final VectorRW in_x1y1z1_lower,
    final VectorRW in_x1y1z1_upper)
  {
    this.x0y0z0_lower = in_x0y0z0_lower;
    this.x0y0z0_upper = in_x0y0z0_upper;
    this.x1y0z0_lower = in_x1y0z0_lower;
    this.x1y0z0_upper = in_x1y0z0_upper;
    this.x0y1z0_lower = in_x0y1z0_lower;
    this.x0y1z0_upper = in_x0y1z0_upper;
    this.x1y1z0_lower = in_x1y1z0_lower;
    this.x1y1z0_upper = in_x1y1z0_upper;
    this.x0y0z1_lower = in_x0y0z1_lower;
    this.x0y0z1_upper = in_x0y0z1_upper;
    this.x1y0z1_lower = in_x1y0z1_lower;
    this.x1y0z1_upper = in_x1y0z1_upper;
    this.x0y1z1_lower = in_x0y1z1_lower;
    this.x0y1z1_upper = in_x0y1z1_upper;
    this.x1y1z1_lower = in_x1y1z1_lower;
    this.x1y1z1_upper = in_x1y1z1_upper;
  }

  /**
   * @return The lower corner of the <code>(x0, y0, z0)</code> octant.
   */

  public VectorRW getX0Y0Z0Lower()
  {
    return this.x0y0z0_lower;
  }

  /**
   * @return The upper corner of the <code>(x0, y0, z0)</code> octant.
   */

  public VectorRW getX0Y0Z0Upper()
  {
    return this.x0y0z0_upper;
  }

  /**
   * @return The lower corner of the <code>(x0, y0, z1)</code> octant.
   */

  public VectorRW getX0Y0Z1Lower()
  {
    return this.x0y0z1_lower;
  }

  /**
   * @return The upper corner of the <code>(x0, y0, z1)</code> octant.
   */

  public VectorRW getX0Y0Z1Upper()
  {
    return this.x0y0z1_upper;
  }

  /**
   * @return The lower corner of the <code>(x0, y1, z0)</code> octant.
   */

  public VectorRW getX0Y1Z0Lower()
  {
    return this.x0y1z0_lower;
  }

  /**
   * @return The upper corner of the <code>(x0, y1, z0)</code> octant.
   */

  public VectorRW getX0Y1Z0Upper()
  {
    return this.x0y1z0_upper;
  }

  /**
   * @return The lower corner of the <code>(x0, y1, z1)</code> octant.
   */

  public VectorRW getX0Y1Z1Lower()
  {
    return this.x0y1z1_lower;
  }

  /**
   * @return The upper corner of the <code>(x0, y1, z1)</code> octant.
   */

  public VectorRW getX0Y1Z1Upper()
  {
    return this.x0y1z1_upper;
  }

  /**
   * @return The lower corner of the <code>(x1, y0, z0)</code> octant.
   */

  public VectorRW getX1Y0Z0Lower()
  {
    return this.x1y0z0_lower;
  }

  /**
   * @return The upper corner of the <code>(x1, y0, z0)</code> octant.
   */

  public VectorRW getX1Y0Z0Upper()
  {
    return this.x1y0z0_upper;
  }

  /**
   * @return The lower corner of the <code>(x1, y0, z1)</code> octant.
   */

  public VectorRW getX1Y0Z1Lower()
  {
    return this.x1y0z1_lower;
  }

  /**
   * @return The upper corner of the <code>(x1, y0, z1)</code> octant.
   */

  public VectorRW getX1Y0Z1Upper()
  {
    return this.x1y0z1_upper;
  }

  /**
   * @return The lower corner of the <code>(x1, y1, z0)</code> octant.
   */

  public VectorRW getX1Y1Z0Lower()
  {
    return this.x1y1z0_lower;
  }

  /**
   * @return The upper corner of the <code>(x1, y1, z0)</code> octant.
   */

  public VectorRW getX1Y1Z0Upper()
  {
    return this.x1y1z0_upper;
  }

  /**
   * @return The lower corner of the <code>(x1, y1, z1)</code> octant.
   */

  public VectorRW getX1Y1Z1Lower()
  {
    return this.x1y1z1_lower;
  }

  /**
   * @return The upper corner of the <code>(x1, y1, z1)</code> octant.
   */

  public VectorRW getX1Y1Z1Upper()
  {
    return this.x1y1z1_upper;
  }
}
