
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class IntersectionOp extends AbstractBinaryOperation {

    public IntersectionOp(Geometry first, Geometry second) {
        super(first,second);
    }

    public IntersectionOp(Geometry first, Geometry second, double epsilon) {
        super(first,second,epsilon);
    }

    @Override
    public Geometry execute() throws OperationException {
        return (Geometry) super.execute();
    }

}
