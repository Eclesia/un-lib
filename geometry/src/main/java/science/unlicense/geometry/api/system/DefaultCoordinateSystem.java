
package science.unlicense.geometry.api.system;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;

/**
 * A coordinate system define a ordered set of axis.
 *
 * In 3D models and animations may be stored in different coordinate system conventions.
 * Those are know as LeftHand or RightHand coordinate systems.
 *
 * TODO, find a proper way to merge with CSUtilities and to declare this information
 * on classes which need it.
 *
 * @author Johann Sorel
 */
public class DefaultCoordinateSystem extends CoordinateSystem {

    private final Axis[] axis;

    public DefaultCoordinateSystem(Axis[] axis) {
        this.axis = axis;
    }

    @Override
    public int getDimension(){
        return axis.length;
    }

    @Override
    public Axis[] getAxis() {
        return Arrays.copy(axis, new Axis[axis.length]);
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(new Chars("CS["));
        cb.append(axis[0].toChars());
        for (int i=1;i<axis.length;i++){
            cb.append(',').append(axis[i].toChars());
        }
        cb.append(new Chars("]"));
        return cb.toChars();
    }

    @Override
    public int getHash() {
        int hash = 5;
        hash = 41 * hash + Arrays.computeHash(this.axis);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultCoordinateSystem other = (DefaultCoordinateSystem) obj;
        return Arrays.equals(this.axis, other.axis);
    }

}
