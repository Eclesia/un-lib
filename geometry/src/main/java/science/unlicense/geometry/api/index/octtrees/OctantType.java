
package science.unlicense.geometry.api.index.octtrees;

import science.unlicense.geometry.api.BBox;

/**
 * The type of octants in octtrees.
 *
 * @author Mark Raynsford
 */
public class OctantType extends BBox{

    public OctantType() {
        super(3);
    }

}
