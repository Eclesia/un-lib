package science.unlicense.geometry.api.index.octtrees;

import java.util.SortedSet;
import java.util.TreeSet;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.index.BoundingVolumeCheck;
import science.unlicense.geometry.api.index.Dimensions;
import science.unlicense.geometry.api.index.SDType;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;

/**
 * <p>
 * An octtree implementation combining {@link OctTreeSDPrune} and
 * {@link OctTreeSDLimit}.
 * </p>
 *
 * @param <T> The precise type of octtree members.
 * @author Mark Raynsford
 */
public final class OctTreeSDPruneLimit<T extends OctTreeMemberType<T>> extends CObject implements OctTreeSDType<T> {

    private final class Octant extends OctantType {

        private boolean leaf;
        private final SortedSet<T> octant_objects_dynamic;
        private final SortedSet<T> octant_objects_static;
        private final double octant_size_x;
        private final double octant_size_y;
        private final double octant_size_z;
        private Octant x0y0z0;
        private Octant x0y0z1;
        private Octant x0y1z0;
        private Octant x0y1z1;
        private Octant x1y0z0;
        private Octant x1y0z1;

        private Octant x1y1z0;
        private Octant x1y1z1;
        private final Octant parent;

        public Octant(
                final Octant in_parent,
                final VectorRW in_lower,
                final VectorRW in_upper) {
            this.parent = in_parent;
            this.lower.set(in_lower);
            this.upper.set(in_upper);
            this.octant_size_x = Dimensions.getSpanSizeX(this.lower, this.upper);
            this.octant_size_y = Dimensions.getSpanSizeY(this.lower, this.upper);
            this.octant_size_z = Dimensions.getSpanSizeZ(this.lower, this.upper);

            this.octant_objects_dynamic = new TreeSet<T>();
            this.octant_objects_static = new TreeSet<T>();

            this.leaf = true;
            this.x0y0z0 = null;
            this.x1y0z0 = null;
            this.x0y1z0 = null;
            this.x1y1z0 = null;
            this.x0y0z1 = null;
            this.x1y0z1 = null;
            this.x0y1z1 = null;
            this.x1y1z1 = null;
        }

        /**
         * Attempt to turn this node back into a leaf.
         */
        private void unsplitAttempt() {
            if (this.leaf == false) {
                boolean prunable = true;
                assert this.x0y0z0 != null;
                prunable &= this.x0y0z0.unsplitCanPrune();
                assert this.x1y0z0 != null;
                prunable &= this.x1y0z0.unsplitCanPrune();
                assert this.x0y1z0 != null;
                prunable &= this.x0y1z0.unsplitCanPrune();
                assert this.x1y1z0 != null;
                prunable &= this.x1y1z0.unsplitCanPrune();

                assert this.x0y0z1 != null;
                prunable &= this.x0y0z1.unsplitCanPrune();
                assert this.x1y0z1 != null;
                prunable &= this.x1y0z1.unsplitCanPrune();
                assert this.x0y1z1 != null;
                prunable &= this.x0y1z1.unsplitCanPrune();
                assert this.x1y1z1 != null;
                prunable &= this.x1y1z1.unsplitCanPrune();

                if (prunable) {
                    this.leaf = true;
                    this.x0y0z0 = null;
                    this.x0y1z0 = null;
                    this.x1y0z0 = null;
                    this.x1y1z0 = null;

                    this.x0y0z1 = null;
                    this.x0y1z1 = null;
                    this.x1y0z1 = null;
                    this.x1y1z1 = null;
                }
            }
        }

        /**
         * Attempt to turn this node and as many ancestors if this node back
         * into leaves as possible.
         */
        private void unsplitAttemptRecursive() {
            this.unsplitAttempt();
            if (this.parent != null) {
                this.parent.unsplitAttemptRecursive();
            }
        }

        private boolean unsplitCanPrune() {
            return (this.leaf == true)
                    && this.octant_objects_dynamic.isEmpty()
                    && this.octant_objects_static.isEmpty();
        }

        private boolean canSplit() {
            final double mx = OctTreeSDPruneLimit.this.minimum_size_x;
            final double my = OctTreeSDPruneLimit.this.minimum_size_y;
            final double mz = OctTreeSDPruneLimit.this.minimum_size_z;
            return this.octant_size_x >= mx * 2
                    && this.octant_size_y >= my * 2
                    && this.octant_size_z >= mz * 2;
        }

        void clear() {
            this.octant_objects_dynamic.clear();
            this.octant_objects_static.clear();

            if (this.leaf == false) {
                assert this.x0y0z0 != null;
                this.x0y0z0.clear();
                assert this.x1y0z0 != null;
                this.x1y0z0.clear();
                assert this.x0y1z0 != null;
                this.x0y1z0.clear();
                assert this.x1y1z0 != null;
                this.x1y1z0.clear();

                assert this.x0y0z1 != null;
                this.x0y0z1.clear();
                assert this.x1y0z1 != null;
                this.x1y0z1.clear();
                assert this.x0y1z1 != null;
                this.x0y1z1.clear();
                assert this.x1y1z1 != null;
                this.x1y1z1.clear();
            }
        }

        void clearDynamic() {
            this.octant_objects_dynamic.clear();

            if (this.leaf == false) {
                assert this.x0y0z0 != null;
                this.x0y0z0.clearDynamic();
                assert this.x1y0z0 != null;
                this.x1y0z0.clearDynamic();
                assert this.x0y1z0 != null;
                this.x0y1z0.clearDynamic();
                assert this.x1y1z0 != null;
                this.x1y1z0.clearDynamic();

                assert this.x0y0z1 != null;
                this.x0y0z1.clearDynamic();
                assert this.x1y0z1 != null;
                this.x1y0z1.clearDynamic();
                assert this.x0y1z1 != null;
                this.x0y1z1.clearDynamic();
                assert this.x1y1z1 != null;
                this.x1y1z1.clearDynamic();
            }
        }

        private void collectRecursive(
                final SortedSet<T> items) {
            items.addAll(this.octant_objects_dynamic);
            items.addAll(this.octant_objects_static);

            if (this.leaf == false) {
                assert this.x0y0z0 != null;
                this.x0y0z0.collectRecursive(items);
                assert this.x0y1z0 != null;
                this.x0y1z0.collectRecursive(items);
                assert this.x1y0z0 != null;
                this.x1y0z0.collectRecursive(items);
                assert this.x1y1z0 != null;
                this.x1y1z0.collectRecursive(items);

                assert this.x0y0z1 != null;
                this.x0y0z1.collectRecursive(items);
                assert this.x0y1z1 != null;
                this.x0y1z1.collectRecursive(items);
                assert this.x1y0z1 != null;
                this.x1y0z1.collectRecursive(items);
                assert this.x1y1z1 != null;
                this.x1y1z1.collectRecursive(items);
            }
        }

        /**
         * Attempt to insert <code>item</code> into this node, or the children
         * of this node.
         *
         * @return <code>true</code>, if the item was inserted and
         * <code>false</code> otherwise.
         */
        boolean insert(
                final T item,
                final SDType type) {
            return this.insertBase(item, type);
        }

        /**
         * Insertion base case: item may or may not fit within node.
         */
        private boolean insertBase(
                final T item,
                final SDType type) {
            if (OctTreeSDPruneLimit.this.objects_all_dynamic.contains(item)) {
                return false;
            }
            if (OctTreeSDPruneLimit.this.objects_all_static.contains(item)) {
                return false;
            }
            if (BoundingVolumeCheck.containedWithin(this, item)) {
                return this.insertStep(item, type);
            }
            return false;
        }

        /**
         * Insert the given object into the current octant's object list, and
         * also inserted into the "global" object list.
         */
        private boolean insertObject(
                final T item,
                final SDType type) {
            switch (type) {
                case SD_DYNAMIC: {
                    OctTreeSDPruneLimit.this.objects_all_dynamic.add(item);
                    this.octant_objects_dynamic.add(item);
                    return true;
                }
                case SD_STATIC: {
                    OctTreeSDPruneLimit.this.objects_all_static.add(item);
                    this.octant_objects_static.add(item);
                    return true;
                }
            }

            throw new UnimplementedException("Not implemented");
        }

        /**
         * Insertion inductive case: item fits within node, but may fit more
         * precisely within a child node.
         */
        // CHECKSTYLE:OFF
        private boolean insertStep(
                // CHECKSTYLE:ON
                final T item,
                final SDType type) {
            /**
             * The object can fit in this node, but perhaps it is possible to
             * fit it more precisely within one of the child nodes.
             */

            /**
             * If this node is a leaf, and is large enough to split, do so.
             */
            if (this.leaf == true) {
                if (this.canSplit()) {
                    this.split();
                } else {

                    /**
                     * The node is a leaf, but cannot be split further. Insert
                     * directly.
                     */
                    return this.insertObject(item, type);
                }
            }

            /**
             * See if the object will fit in any of the child nodes.
             */
            assert this.leaf == false;

            assert this.x0y0z0 != null;
            if (BoundingVolumeCheck.containedWithin(this.x0y0z0, item)) {
                assert this.x0y0z0 != null;
                return this.x0y0z0.insertStep(item, type);
            }
            assert this.x1y0z0 != null;
            if (BoundingVolumeCheck.containedWithin(this.x1y0z0, item)) {
                assert this.x1y0z0 != null;
                return this.x1y0z0.insertStep(item, type);
            }
            assert this.x0y1z0 != null;
            if (BoundingVolumeCheck.containedWithin(this.x0y1z0, item)) {
                assert this.x0y1z0 != null;
                return this.x0y1z0.insertStep(item, type);
            }
            assert this.x1y1z0 != null;
            if (BoundingVolumeCheck.containedWithin(this.x1y1z0, item)) {
                assert this.x1y1z0 != null;
                return this.x1y1z0.insertStep(item, type);
            }

            assert this.x0y0z1 != null;
            if (BoundingVolumeCheck.containedWithin(this.x0y0z1, item)) {
                assert this.x0y0z1 != null;
                return this.x0y0z1.insertStep(item, type);
            }
            assert this.x1y0z1 != null;
            if (BoundingVolumeCheck.containedWithin(this.x1y0z1, item)) {
                assert this.x1y0z1 != null;
                return this.x1y0z1.insertStep(item, type);
            }
            assert this.x0y1z1 != null;
            if (BoundingVolumeCheck.containedWithin(this.x0y1z1, item)) {
                assert this.x0y1z1 != null;
                return this.x0y1z1.insertStep(item, type);
            }
            assert this.x1y1z1 != null;
            if (BoundingVolumeCheck.containedWithin(this.x1y1z1, item)) {
                assert this.x1y1z1 != null;
                return this.x1y1z1.insertStep(item, type);
            }

            /**
             * Otherwise, insert the object into this node.
             */
            return this.insertObject(item, type);
        }

        void raycast(
                final Ray ray,
                final SortedSet<OctTreeRaycastResult<T>> items) {
            if (BoundingVolumeCheck.rayBoxIntersects(
                    ray,
                    this.lower.get(0),
                    this.lower.get(1),
                    this.lower.get(2),
                    this.upper.get(0),
                    this.upper.get(1),
                    this.upper.get(2))) {

                this.raycastObjects(ray, this.octant_objects_dynamic, items);
                this.raycastObjects(ray, this.octant_objects_static, items);

                if (this.leaf == false) {
                    assert this.x0y0z0 != null;
                    this.x0y0z0.raycast(ray, items);
                    assert this.x0y1z0 != null;
                    this.x0y1z0.raycast(ray, items);
                    assert this.x1y0z0 != null;
                    this.x1y0z0.raycast(ray, items);
                    assert this.x1y1z0 != null;
                    this.x1y1z0.raycast(ray, items);

                    assert this.x0y0z1 != null;
                    this.x0y0z1.raycast(ray, items);
                    assert this.x0y1z1 != null;
                    this.x0y1z1.raycast(ray, items);
                    assert this.x1y0z1 != null;
                    this.x1y0z1.raycast(ray, items);
                    assert this.x1y1z1 != null;
                    this.x1y1z1.raycast(ray, items);
                }
            }
        }

        void raycastObjects(
                final Ray ray,
                final SortedSet<T> objects,
                final SortedSet<OctTreeRaycastResult<T>> items) {
            for (final T object : objects) {
                final Tuple object_lower
                        = object.getLower();
                final Tuple object_upper
                        = object.getUpper();

                if (BoundingVolumeCheck.rayBoxIntersects(
                        ray,
                        object_lower.get(0),
                        object_lower.get(1),
                        object_lower.get(2),
                        object_upper.get(0),
                        object_upper.get(1),
                        object_upper.get(2))) {

                    final OctTreeRaycastResult<T> r
                            = new OctTreeRaycastResult<T>(object, DistanceOp.distance(new Vector3f64(
                                            object_lower.get(0),
                                            object_lower.get(1),
                                            object_lower.get(2)),
                                    ray.getPosition()));
                    items.add(r);
                }
            }
        }

        boolean remove(
                final T item) {
            if ((OctTreeSDPruneLimit.this.objects_all_dynamic.contains(item) == false)
                    && (OctTreeSDPruneLimit.this.objects_all_static.contains(item) == false)) {
                return false;
            }

            /**
             * If an object is in objects_all, then it must be within the bounds
             * of the tree, according to insert().
             */
            assert BoundingVolumeCheck.containedWithin(this, item);
            return this.removeStep(item);
        }

        // CHECKSTYLE:OFF
        private boolean removeStep(
                // CHECKSTYLE:ON
                final T item) {
            if (this.octant_objects_dynamic.contains(item)) {
                this.octant_objects_dynamic.remove(item);
                OctTreeSDPruneLimit.this.objects_all_dynamic.remove(item);
                this.unsplitAttemptRecursive();
                return true;
            }
            if (this.octant_objects_static.contains(item)) {
                this.octant_objects_static.remove(item);
                OctTreeSDPruneLimit.this.objects_all_static.remove(item);
                this.unsplitAttemptRecursive();
                return true;
            }

            this.unsplitAttemptRecursive();

            if (this.leaf == false) {
                assert this.x0y0z0 != null;
                if (BoundingVolumeCheck.containedWithin(this.x0y0z0, item)) {
                    assert this.x0y0z0 != null;
                    return this.x0y0z0.removeStep(item);
                }
                assert this.x1y0z0 != null;
                if (BoundingVolumeCheck.containedWithin(this.x1y0z0, item)) {
                    assert this.x1y0z0 != null;
                    return this.x1y0z0.removeStep(item);
                }
                assert this.x0y1z0 != null;
                if (BoundingVolumeCheck.containedWithin(this.x0y1z0, item)) {
                    assert this.x0y1z0 != null;
                    return this.x0y1z0.removeStep(item);
                }
                assert this.x1y1z0 != null;
                if (BoundingVolumeCheck.containedWithin(this.x1y1z0, item)) {
                    assert this.x1y1z0 != null;
                    return this.x1y1z0.removeStep(item);
                }

                assert this.x0y0z1 != null;
                if (BoundingVolumeCheck.containedWithin(this.x0y0z1, item)) {
                    assert this.x0y0z1 != null;
                    return this.x0y0z1.removeStep(item);
                }
                assert this.x1y0z1 != null;
                if (BoundingVolumeCheck.containedWithin(this.x1y0z1, item)) {
                    assert this.x1y0z1 != null;
                    return this.x1y0z1.removeStep(item);
                }
                assert this.x0y1z1 != null;
                if (BoundingVolumeCheck.containedWithin(this.x0y1z1, item)) {
                    assert this.x0y1z1 != null;
                    return this.x0y1z1.removeStep(item);
                }
                assert this.x1y1z1 != null;
                if (BoundingVolumeCheck.containedWithin(this.x1y1z1, item)) {
                    assert this.x1y1z1 != null;
                    return this.x1y1z1.removeStep(item);
                }
            }

            /**
             * The object must be in the tree, according to objects_all.
             * Therefore it must be in this node, or one of the child octants.
             */
            throw new RuntimeException("Unreachable code");
        }

        /**
         * Split this node into four octants.
         */
        private void split() {
            assert this.canSplit();

            final Octants q = Octants.split(this.lower, this.upper);
            this.x0y0z0 = new Octant(this, q.getX0Y0Z0Lower(), q.getX0Y0Z0Upper());
            this.x0y1z0 = new Octant(this, q.getX0Y1Z0Lower(), q.getX0Y1Z0Upper());
            this.x1y0z0 = new Octant(this, q.getX1Y0Z0Lower(), q.getX1Y0Z0Upper());
            this.x1y1z0 = new Octant(this, q.getX1Y1Z0Lower(), q.getX1Y1Z0Upper());
            this.x0y0z1 = new Octant(this, q.getX0Y0Z1Lower(), q.getX0Y0Z1Upper());
            this.x0y1z1 = new Octant(this, q.getX0Y1Z1Lower(), q.getX0Y1Z1Upper());
            this.x1y0z1 = new Octant(this, q.getX1Y0Z1Lower(), q.getX1Y0Z1Upper());
            this.x1y1z1 = new Octant(this, q.getX1Y1Z1Lower(), q.getX1Y1Z1Upper());
            this.leaf = false;
        }

        <E extends Throwable> void traverse(
                final int depth,
                final OctTreeTraversalType<E> traversal)
                throws E {
            traversal.visit(depth, this.lower, this.upper);

            if (this.leaf == false) {
                assert this.x0y0z0 != null;
                this.x0y0z0.traverse(depth + 1, traversal);
                assert this.x1y0z0 != null;
                this.x1y0z0.traverse(depth + 1, traversal);
                assert this.x0y1z0 != null;
                this.x0y1z0.traverse(depth + 1, traversal);
                assert this.x1y1z0 != null;
                this.x1y1z0.traverse(depth + 1, traversal);

                assert this.x0y0z1 != null;
                this.x0y0z1.traverse(depth + 1, traversal);
                assert this.x1y0z1 != null;
                this.x1y0z1.traverse(depth + 1, traversal);
                assert this.x0y1z1 != null;
                this.x0y1z1.traverse(depth + 1, traversal);
                assert this.x1y1z1 != null;
                this.x1y1z1.traverse(depth + 1, traversal);
            }
        }

        void volumeContaining(
                final BBox volume,
                final SortedSet<T> items) {
            /**
             * If <code>volume</code> completely contains this octant, collect
             * everything in this octant and all children of this octant.
             */

            if (BoundingVolumeCheck.containedWithin(volume, this)) {
                this.collectRecursive(items);
                return;
            }

            /**
             * Otherwise, <code>volume</code> may be overlapping this octant and
             * therefore some items may still be contained within
             * <code>volume</code>.
             */
            for (final T object : this.octant_objects_dynamic) {
                assert object != null;
                if (BoundingVolumeCheck.containedWithin(volume, object)) {
                    items.add(object);
                }
            }
            for (final T object : this.octant_objects_static) {
                assert object != null;
                if (BoundingVolumeCheck.containedWithin(volume, object)) {
                    items.add(object);
                }
            }

            if (this.leaf == false) {
                assert this.x0y0z0 != null;
                this.x0y0z0.volumeContaining(volume, items);
                assert this.x0y1z0 != null;
                this.x0y1z0.volumeContaining(volume, items);
                assert this.x1y0z0 != null;
                this.x1y0z0.volumeContaining(volume, items);
                assert this.x1y1z0 != null;
                this.x1y1z0.volumeContaining(volume, items);

                assert this.x0y0z1 != null;
                this.x0y0z1.volumeContaining(volume, items);
                assert this.x0y1z1 != null;
                this.x0y1z1.volumeContaining(volume, items);
                assert this.x1y0z1 != null;
                this.x1y0z1.volumeContaining(volume, items);
                assert this.x1y1z1 != null;
                this.x1y1z1.volumeContaining(volume, items);
            }
        }

        void volumeOverlapping(
                final BBox volume,
                final SortedSet<T> items) {
            /**
             * If <code>volume</code> overlaps this octant, test each object
             * against <code>volume</code>.
             */

            if (BoundingVolumeCheck.overlapsVolume(volume, this)) {
                for (final T object : this.octant_objects_dynamic) {
                    assert object != null;
                    if (BoundingVolumeCheck.overlapsVolume(volume, object)) {
                        items.add(object);
                    }
                }
                for (final T object : this.octant_objects_static) {
                    assert object != null;
                    if (BoundingVolumeCheck.overlapsVolume(volume, object)) {
                        items.add(object);
                    }
                }

                if (this.leaf == false) {
                    assert this.x0y0z0 != null;
                    this.x0y0z0.volumeOverlapping(volume, items);
                    assert this.x1y0z0 != null;
                    this.x1y0z0.volumeOverlapping(volume, items);
                    assert this.x0y1z0 != null;
                    this.x0y1z0.volumeOverlapping(volume, items);
                    assert this.x1y1z0 != null;
                    this.x1y1z0.volumeOverlapping(volume, items);

                    assert this.x0y0z1 != null;
                    this.x0y0z1.volumeOverlapping(volume, items);
                    assert this.x1y0z1 != null;
                    this.x1y0z1.volumeOverlapping(volume, items);
                    assert this.x0y1z1 != null;
                    this.x0y1z1.volumeOverlapping(volume, items);
                    assert this.x1y1z1 != null;
                    this.x1y1z1.volumeOverlapping(volume, items);
                }
            }
        }
    }

    /**
     * Construct a new octtree with the given size and position.
     *
     * @param size The size.
     * @param position The position.
     * @param limit_size The minimum octant size.
     *
     * @return A new octtree.
     * @param <T> The precise type of octtree members.
     */
    public static <T extends OctTreeMemberType<T>> OctTreeSDType<T> newOctTree(
            final VectorRW size,
            final VectorRW position,
            final VectorRW limit_size) {
        return new OctTreeSDPruneLimit<T>(position, size, limit_size);
    }

    private final SortedSet<T> objects_all_dynamic;
    private final SortedSet<T> objects_all_static;
    private final Octant root;
    private final double minimum_size_x;
    private final double minimum_size_y;
    private final double minimum_size_z;

    private OctTreeSDPruneLimit(
            final VectorRW position,
            final VectorRW size,
            final VectorRW size_minimum) {
        CObjects.ensureNotNull(position, "Position");
        CObjects.ensureNotNull(size, "Size");
        CObjects.ensureNotNull(size_minimum, "Minimum size");

        OctTreeChecks.checkSize(
                "Octtree size",
                size.get(0),
                size.get(1),
                size.get(2));
        OctTreeChecks.checkSize(
                "Minimum octant size",
                size_minimum.get(0),
                size_minimum.get(1),
                size_minimum.get(2));

        if (size_minimum.get(0) > size.get(0)) {
            final String s
                    = String.format(
                            "Minimum octant width (%f) is greater than the octtree width (%f)",
                            size_minimum.get(0),
                            size.get(0));
            throw new InvalidArgumentException(s);
        }
        if (size_minimum.get(1) > size.get(1)) {
            final String s
                    = String
                            .format(
                                    "Minimum octant height (%f) is greater than the octtree height (%f)",
                                    size_minimum.get(1),
                                    size.get(1));
            throw new InvalidArgumentException(s);
        }
        if (size_minimum.get(2) > size.get(2)) {
            final String s
                    = String.format(
                            "Minimum octant depth (%f) is greater than the octtree depth (%f)",
                            size_minimum.get(2),
                            size.get(2));
            throw new InvalidArgumentException(s);
        }

        this.minimum_size_x = size_minimum.get(0);
        this.minimum_size_y = size_minimum.get(1);
        this.minimum_size_z = size_minimum.get(2);
        this.objects_all_dynamic = new TreeSet<T>();
        this.objects_all_static = new TreeSet<T>();

        final VectorRW lower = VectorNf64.create(position);
        final VectorRW upper
                = new Vector3f64(
                        (position.get(0) + size.get(0)) - 1,
                        (position.get(1) + size.get(1)) - 1,
                        (position.get(2) + size.get(2)) - 1);
        this.root = new Octant(null, lower, upper);
    }

    @Override
    public void octTreeClear() {
        this.objects_all_dynamic.clear();
        this.objects_all_static.clear();
        this.root.clear();
    }

    @Override
    public double octTreeGetPositionX() {
        return this.root.getLower().get(0);
    }

    @Override
    public double octTreeGetPositionY() {
        return this.root.getLower().get(1);
    }

    @Override
    public double octTreeGetPositionZ() {
        return this.root.getLower().get(2);
    }

    @Override
    public double octTreeGetSizeX() {
        return this.root.octant_size_x;
    }

    @Override
    public double octTreeGetSizeY() {
        return this.root.octant_size_y;
    }

    @Override
    public double octTreeGetSizeZ() {
        return this.root.octant_size_z;
    }

    @Override
    public boolean octTreeInsert(
            final T item) {
        BoundingVolumeCheck.checkWellFormed(item);
        return this.root.insert(item, SDType.SD_DYNAMIC);
    }

    @Override
    public boolean octTreeInsertSD(
            final T item,
            final SDType type) {
        BoundingVolumeCheck.checkWellFormed(item);
        CObjects.ensureNotNull(type, "Object type");
        return this.root.insert(item, type);
    }

    @Override
    public void octTreeIterateObjects(final Predicate f) throws Exception {
        CObjects.ensureNotNull(f, "Function");

        for (final T object : this.objects_all_dynamic) {
            assert object != null;
            final Boolean r = f.evaluate(object);
            if (r.booleanValue() == false) {
                break;
            }
        }
        for (final T object : this.objects_all_static) {
            assert object != null;
            final Boolean r = f.evaluate(object);
            if (r.booleanValue() == false) {
                break;
            }
        }
    }

    @Override
    public void octTreeQueryRaycast(
            final Ray ray,
            final SortedSet<OctTreeRaycastResult<T>> items) {
        CObjects.ensureNotNull(ray, "Ray");
        CObjects.ensureNotNull(items, "Items");
        this.root.raycast(ray, items);
    }

    @Override
    public void octTreeQueryVolumeContaining(
            final BBox volume,
            final SortedSet<T> items) {
        BoundingVolumeCheck.checkWellFormed(volume);
        CObjects.ensureNotNull(items, "Items");
        this.root.volumeContaining(volume, items);
    }

    @Override
    public void octTreeQueryVolumeOverlapping(
            final BBox volume,
            final SortedSet<T> items) {
        BoundingVolumeCheck.checkWellFormed(volume);
        CObjects.ensureNotNull(items, "Items");
        this.root.volumeOverlapping(volume, items);
    }

    @Override
    public boolean octTreeRemove(
            final T item) {
        BoundingVolumeCheck.checkWellFormed(item);
        return this.root.remove(item);
    }

    @Override
    public void octTreeSDClearDynamic() {
        this.root.clearDynamic();
        this.objects_all_dynamic.clear();
    }

    @Override
    public <E extends Throwable> void octTreeTraverse(
            final OctTreeTraversalType<E> traversal)
            throws E {
        CObjects.ensureNotNull(traversal, "Traversal");
        this.root.traverse(0, traversal);
    }

    @Override
    public Chars toChars() {
        final CharBuffer b = new CharBuffer();
        b.append("[OctTree ");
        b.append(this.root.getLower());
        b.append(" ");
        b.append(this.root.getUpper());
        b.append(" ");
        b.append(this.objects_all_dynamic);
        b.append(" ");
        b.append(this.objects_all_static);
        b.append("]");
        return b.toChars();
    }
}
