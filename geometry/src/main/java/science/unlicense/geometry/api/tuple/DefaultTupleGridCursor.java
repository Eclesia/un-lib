
package science.unlicense.geometry.api.tuple;

import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.api.AbstractTupleRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Default Tuple iterator.
 *
 * @author Johann Sorel
 */
public class DefaultTupleGridCursor extends AbstractTupleRW implements TupleGridCursor{

    protected final TupleGrid sm;
    private final long[] dimensions;
    protected final VectorRW coordinate;
    protected final VectorRW coordinateD;
    protected boolean hasNext = true;

    private boolean tupleDRead = false;
    private final TupleRW base;

    public DefaultTupleGridCursor(TupleGrid tb) {
        this.sm = tb;
        this.dimensions = tb.getExtent().toArray();
        this.coordinate = VectorNf64.createDouble(this.dimensions.length);
        coordinate.set(0, -1); //place just before first coordinate
        this.base = tb.createTuple();
        this.coordinateD = VectorNf64.createDouble(coordinate.getSampleCount());
    }

    @Override
    public TupleRW samples() {
        return this;
    }

    @Override
    public Tuple coordinate() {
        coordinateD.set(coordinate);
        return coordinate;
    }

    @Override
    public void moveTo(Tuple coordinate) {
        //verify coordinate
        hasNext = false;
        for (int i=0;i<this.dimensions.length;i++){
            this.coordinate.set(i, coordinate.get(i));
            if (this.coordinate.get(i) < 0 || this.coordinate.get(i) >= dimensions[i]) {
                throw new InvalidIndexException("Invalid coordinate, outside of data range");
            }
            hasNext |= (this.coordinate.get(i) != dimensions[i]-1);
        }
        tupleDRead = false;
    }

    @Override
    public void moveTo(int coordinate) {
        if (dimensions.length != 1) {
            throw new InvalidIndexException("Tuple coordinate have more then one coordinate");
        }
        //verify coordinate
        if (coordinate<0 || coordinate >= dimensions[0]) {
            throw new InvalidIndexException("Invalid coordinate, outside of data range");
        }
        hasNext = (coordinate != dimensions[0]-1);

        this.coordinate.set(0, coordinate);
        tupleDRead = false;
    }

    @Override
    public boolean next() {
        if (!hasNext) return false;

        coordinate.set(0, coordinate.getX() + 1);
        for (int i=0;i<this.dimensions.length;i++){
            if (coordinate.get(i) >= dimensions[i]){
                //get back to this dimension start
                coordinate.set(i, 0);
                if (i<dimensions.length-1){
                    //iterator next dimension
                    coordinate.set(i+1, coordinate.get(i+1)+1);
                } else {
                    //we have finish
                    hasNext = false;
                }
            }
        }
        tupleDRead = false;
        return hasNext;
    }

    @Override
    public ArithmeticType getNumericType() {
        return sm.getNumericType();
    }

    @Override
    public int getSampleCount() {
        return sm.getSampleSystem().getNumComponents();
    }

    @Override
    public void toFloat(float[] buffer, int offset) {
        if (!tupleDRead) {
            tupleDRead = true;
            sm.getTuple(coordinate, base);
        }
        base.toFloat(buffer, offset);
    }

    @Override
    public void toDouble(double[] buffer, int offset) {
        if (!tupleDRead) {
            tupleDRead = true;
            sm.getTuple(coordinate, base);
        }
        base.toDouble(buffer, offset);
    }

    @Override
    public double get(int indice) {
        if (!tupleDRead) {
            tupleDRead = true;
            sm.getTuple(coordinate, base);
        }
        return base.get(indice);
    }

    @Override
    public Number getNumber(int indice) {
        return new Float64(get(indice));
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        for (int i=0;i<getSampleCount();i++) {
            buffer[offset+i] = getNumber(i);
        }
    }

    @Override
    public void set(int indice, double value) {
        if (!tupleDRead) sm.getTuple(coordinate, base);
        base.set(indice, value);
        sm.setTuple(coordinate, base);
    }

    @Override
    public void set(int indice, Arithmetic value) throws InvalidIndexException {
        if (!tupleDRead) sm.getTuple(coordinate, base);
        base.set(indice, value);
        sm.setTuple(coordinate, base);
    }

    @Override
    public TupleRW create(int size) {
        return VectorNf64.createDouble(size);
    }

}
