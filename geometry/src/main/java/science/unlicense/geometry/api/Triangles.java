
package science.unlicense.geometry.api;

import science.unlicense.geometry.api.tuple.TupleGrid1D;

/**
 *
 * @author Johann Sorel
 */
public interface Triangles extends Geometry {

    TupleGrid1D getCoordinates();

}
