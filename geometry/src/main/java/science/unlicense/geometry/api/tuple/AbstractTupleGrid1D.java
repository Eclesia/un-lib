
package science.unlicense.geometry.api.tuple;

import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Scalari32;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractTupleGrid1D extends AbstractNumberTupleGrid implements TupleGrid1D{

    @Override
    public int getDimension() {
        return (int) dimensions.getL(0);
    }

    @Override
    public void getTuple(int coordinate, TupleRW buffer) {
        getTuple(new Scalari32(coordinate), buffer);
    }

    @Override
    public void setTuple(int coordinate, Tuple buffer) {
        setTuple(new Scalari32(coordinate), buffer);
    }

}
