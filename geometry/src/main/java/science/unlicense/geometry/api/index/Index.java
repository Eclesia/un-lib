
package science.unlicense.geometry.api.index;

import science.unlicense.common.api.collection.Iterator;

/**
 *
 * @author Johann Sorel
 */
public interface Index {

    void put(Object key, Object value);

    Iterator search(Object condition);

}
