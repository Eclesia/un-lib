
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class TouchesOp extends AbstractBinaryOperation{

    public TouchesOp(Geometry first, Geometry second) {
        super(first,second);
    }

    @Override
    public Boolean execute() throws OperationException {
        return (Boolean) super.execute();
    }
}
