
package science.unlicense.geometry.api.index;

import science.unlicense.geometry.api.BBox;

/**
 *
 * @author Johann Sorel
 */
public class IndexRecord extends BBox{

    private final Object value;

    public IndexRecord(int dimension, Object value) {
        super(dimension);
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

}
