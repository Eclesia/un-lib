
package science.unlicense.geometry.api.operation;

import science.unlicense.common.api.Arrays;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.impl.Sheet;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import static science.unlicense.math.impl.Vectors.add;
import static science.unlicense.math.impl.Vectors.dot;
import static science.unlicense.math.impl.Vectors.scale;
import static science.unlicense.math.impl.Vectors.subtract;

/**
 *
 * @author Johann Sorel
 * @author Izyumov Konstantin
 */
public class DistanceOp extends AbstractBinaryOperation{

    public DistanceOp(Geometry first, Geometry second) {
        super(first,second);
    }

    @Override
    public Double execute() throws OperationException {
        return (Double) super.execute();
    }

    /**
     * Calculate distance between two points.
     * @param p1 first point
     * @param p2 second point
     * @return double , distance
     */
    public static double distance(Tuple p1, Tuple p2){
        return distance(p1.toDouble(), p2.toDouble());
    }

    /**
     * Calculate distance between two points.
     * @param p1 first point
     * @param p2 second point
     * @return double , distance
     */
    public static double distance(double[] p1, double[] p2){
        return Math.sqrt(distanceSquare(p1, p2));
    }

    /**
     * Calculate square distance between two points.
     * @param p1 first point
     * @param p2 second point
     * @return double , distance
     */
    public static double distanceSquare(Tuple p1, Tuple p2){
        return distanceSquare(p1.toDouble(), p2.toDouble());
    }

    /**
     * Calculate square distance between two points.
     * @param p1 first point
     * @param p2 second point
     * @return double , distance
     */
    public static double distanceSquare(double[] p1, double[] p2){
        double sum = 0;
        for (int i=0;i<p1.length;i++){
            final double diff = p1[i]-p2[i];
            sum += diff*diff;
        }
        return sum;
    }

    /**
     * Calculate distance between a segment and a point.
     *
     * @param segmentStart line segment start
     * @param segmentEnd lien segment end
     * @param point position
     * @return double , square distance
     */
    public static double distance(final Tuple segmentStart, final Tuple segmentEnd, final Tuple point){
        return Math.sqrt(distanceSquare(segmentStart.toDouble(), segmentEnd.toDouble(), point.toDouble()));
    }

    /**
     * Calculate distance between a segment and a point.
     *
     * @param segmentStart line segment start
     * @param segmentEnd lien segment end
     * @param point position
     * @return double , square distance
     */
    public static double distance(final double[] segmentStart, final double[] segmentEnd, final double[] point){
        return Math.sqrt(distanceSquare(segmentStart, segmentEnd, point));
    }

    /**
     * Calculate square distance between a segment and a point.
     *
     * @param segmentStart line segment start
     * @param segmentEnd lien segment end
     * @param point position
     * @return double , square distance
     */
    public static double distanceSquare(final Tuple segmentStart, final Tuple segmentEnd, final Tuple point){
        return distanceSquare(segmentStart.toDouble(), segmentEnd.toDouble(), point.toDouble());
    }

    /**
     * Calculate square distance between a segment and a point.
     *
     * @param segmentStart line segment start
     * @param segmentEnd lien segment end
     * @param point position
     * @return double , square distance
     */
    public static double distanceSquare(final double[] segmentStart, final double[] segmentEnd, final double[] point){
        final double[] ab = subtract(segmentEnd, segmentStart);
        final double[] ac = subtract(point,segmentStart);
        final double[] bc = subtract(point,segmentEnd);
        final double e = dot(ac, ab);
        // cases where point is outside segment
        if (e <= 0.0f) return dot(ac, ac);
        final double f = dot(ab, ab);
        if (e >= f) return dot(bc, bc);
        // cases where point projects onto segment
        return dot(ac, ac)- e*e /f;
    }

    /**
     * Calculate distance between two line segments.
     *
     * @param line1Start line 1 start point
     * @param line1End line 1 start point
     * @param buffer1 closest point on line 1
     * @param line2Start line 1 start point
     * @param line2End line 1 start point
     * @param buffer2 closest point on line 2
     * @param ratio size 2 , for each line,
     *  ratio [0..1] of the closest point position between start and end points.
     * @return distance
     */
    public static double distance(Tuple line1Start, Tuple line1End, TupleRW buffer1,
                                   Tuple line2Start, Tuple line2End, TupleRW buffer2,
                                   double[] ratio, double epsilon) {
        final double[] array1 = buffer1.toDouble();
        final double[] array2 = buffer2.toDouble();
        double d = distance(
                line1Start.toDouble(), line1End.toDouble(), array1,
                line2Start.toDouble(), line2End.toDouble(), array2,
                ratio, epsilon);
        buffer1.set(array1);
        buffer2.set(array2);
        return d;
    }

    /**
     * Calculate distance between two line segments.
     *
     * Adapted from book : Real-TimeCollision Detection by Christer Ericson
     * (ClosestPtSegmentSegment p.149)
     *
     * @param line1Start line 1 start point
     * @param line1End line 1 start point
     * @param buffer1 closest point on line 1
     * @param line2Start line 1 start point
     * @param line2End line 1 start point
     * @param buffer2 closest point on line 2
     * @param ratio size 2 , for each line,
     *  ratio [0..1] of the closest point position between start and end points.
     * @param epsilon tolerance
     * @return distance
     */
    public static double distance(double[] line1Start, double[] line1End, double[] buffer1,
                                   double[] line2Start, double[] line2End, double[] buffer2,
                                   double[] ratio, double epsilon) {
        return Math.sqrt(distanceSquare(line1Start, line1End, buffer1,
                                        line2Start, line2End, buffer2,
                                        ratio, epsilon));
    }

    /**
     * Calculate square distance between two line segments.
     *
     * @param line1Start line 1 start point
     * @param line1End line 1 start point
     * @param buffer1 closest point on line 1
     * @param line2Start line 1 start point
     * @param line2End line 1 start point
     * @param buffer2 closest point on line 2
     * @param ratio size 2 , for each line,
     *  ratio [0..1] of the closest point position between start and end points.
     * @return distance
     */
    public static double distanceSquare(Tuple line1Start, Tuple line1End, TupleRW buffer1,
                                        Tuple line2Start, Tuple line2End, TupleRW buffer2,
                                        double[] ratio, double epsilon) {
        final double[] array1 = buffer1.toDouble();
        final double[] array2 = buffer2.toDouble();
        double d = distanceSquare(
                line1Start.toDouble(), line1End.toDouble(), array1,
                line2Start.toDouble(), line2End.toDouble(), array2,
                ratio, epsilon);
        buffer1.set(array1);
        buffer2.set(array2);
        return d;
    }

    /**
     * Calculate square distance between two line segments.
     *
     * Adapted from book : Real-TimeCollision Detection by Christer Ericson
     * (ClosestPtSegmentSegment p.149)
     *
     * @param line1Start line 1 start point
     * @param line1End line 1 start point
     * @param buffer1 closest point on line 1
     * @param line2Start line 1 start point
     * @param line2End line 1 start point
     * @param buffer2 closest point on line 2
     * @param ratio size 2 , for each line,
     *  ratio [0..1] of the closest point position between start and end points.
     * @param epsilon tolerance
     * @return distance
     */
    public static double distanceSquare(double[] line1Start, double[] line1End, double[] buffer1,
                                   double[] line2Start, double[] line2End, double[] buffer2,
                                   double[] ratio, double epsilon) {
        final double[] d1 = subtract(line1End,line1Start); // Direction vector of segment S1
        final double[] d2 = subtract(line2End,line2Start); // Direction vector of segment S2
        final double[] r = subtract(line1Start, line2Start);
        final double a = dot(d1, d1); // Squared length of segment S1, always nonnegative
        final double e = dot(d2, d2); // Squared length of segment S2, always nonnegative
        final double f = dot(d2, r);
        // Check if either or both segments degenerate into points
        if (a <= epsilon && e <= epsilon) {
            // Both segments degenerate into points
            ratio[0] = 0;
            ratio[1] = 0;
            Arrays.copy(line1Start,0,line1Start.length,buffer1,0);
            Arrays.copy(line2Start,0,line2Start.length,buffer2,0);
            final double[] t = subtract(buffer1,buffer2);
            return dot(t,t);
        }
        if (a <= epsilon) {
            // First segment degenerates into a point
            ratio[0] = 0;
            ratio[1] = Maths.clamp( f/e, 0, 1);
        } else {
            final double c = dot(d1, r);
            if (e <= epsilon) {
                // Second segment degenerates into a point
                ratio[0] = Maths.clamp( -c/a, 0, 1);
                ratio[1] = 0;
            } else {
                // The general nondegenerate case starts here
                final double b = dot(d1, d2);
                final double denom = a*e-b*b; // Always nonnegative
                // If segments not parallel, compute closest point on L1 to L2 and
                // clamp to segment S1. Else pick arbitrary s (here 0)
                if (denom != 0d) {
                    ratio[0] = Maths.clamp((b*f - c*e) / denom, 0.0f, 1.0f);
                } else {
                    ratio[0] = 0;
                }
                // Compute point on L2 closest to S1(s) using
                // t = Dot((P1 + D1*s) - P2,D2) / Dot(D2,D2) = (b*s + f) / e
                ratio[1] = (b*ratio[0] + f) / e;

                //If t in [0,1] done. Else clamp t, recompute s for the new value
                //of t using s = Dot((P2 + D2*t) - P1,D1) / Dot(D1,D1)= (t*b - c) / a
                //and clamp s to [0, 1]
                if (ratio[1] < 0){
                    ratio[1] = 0;
                    ratio[0] = Maths.clamp(-c / a, 0.0f, 1.0f);
                } else if (ratio[1] > 1){
                    ratio[1] = 1;
                    ratio[0] = Maths.clamp((b - c) / a, 0.0f, 1.0f);
                }

            }
        }

        add(line1Start, scale(d1, ratio[0]), buffer1);
        add(line2Start, scale(d2, ratio[1]), buffer2);
        final double[] t = subtract(buffer1,buffer2);
        return dot(t,t);
    }

    /**
     * Calculate square distance between line and point segments.
     *
     * Adapted from book : Real-TimeCollision Detection by Christer Ericson
     * (ClosestPtSegmentSegment p.149)
     *
     * @param line1Start line 1 start point
     * @param line1End line 1 start point
     * @param buffer1 closest point on line 1
     * @param point
     * @param ratio size 1, for line,
     *  ratio [0..1] of the closest point position between start and end points.
     * @param epsilon tolerance
     * @return distance
     */
    public static double distanceSquare(double[] line1Start, double[] line1End, double[] buffer1,
                                        double[] point, double[] ratio, double epsilon) {
        final double[] d1 = subtract(line1End,line1Start); // Direction vector of segment S1
        final double[] r = subtract(line1Start, point);
        final double a = dot(d1, d1); // Squared length of segment S1, always nonnegative
        // Check if either or both segments degenerate into points
        if (a <= epsilon) {
            // segment degenerate into point
            ratio[0] = 0;
            Arrays.copy(line1Start,0,line1Start.length,buffer1,0);
            final double[] t = subtract(buffer1,point);
            return dot(t,t);
        }

        final double c = dot(d1, r);
        ratio[0] = Maths.clamp( -c/a, 0, 1);

        add(line1Start, scale(d1, ratio[0]), buffer1);
        final double[] t = subtract(buffer1,point);
        return dot(t,t);
    }

    public static double distance(Tuple a, Plane b){
        // could be written like this
        return b.getNormal().dot(a) - b.getD();
//        final double[] sub = Vectors.subtract(a.getValues(), b.getPoint().getValues());
//        return Vectors.dot(sub, b.getNormal().getValues());
    }

    public static double distance(Tuple a, Sheet b){
        // could be written like this
        return b.getNormal().dot(a) - b.getD();
//        final double[] sub = Vectors.subtract(a.getValues(), b.getPoint().getValues());
//        return Vectors.dot(sub, b.getNormal().getValues());
    }

    public static double distanceSquareSegmentTriangle(double[] segmentStart, double[] segmentEnd, double[] buffer1,
            double[] t0, double[] t1, double[] t2, double[] buffer2, double epsilon){

        final double[] planNormal = Geometries.calculateNormal(t0,t1,t2);
        final double planD = Geometries.calculatePlanD(planNormal, t0);

        //result
        double distance;

        //cache
        final double[] ratio = new double[2];
        final double[] c1 = new double[segmentStart.length];
        final double[] c2 = new double[segmentStart.length];

        //test first edge to segment
        double dist = DistanceOp.distanceSquare(segmentStart, segmentEnd, c1,t0, t1, c2, ratio, epsilon);
        distance = dist;
        Arrays.copy(c1,0,c1.length,buffer1,0);
        Arrays.copy(c2,0,c2.length,buffer2,0);

        //test second edge to segment
        dist = DistanceOp.distanceSquare(segmentStart, segmentEnd, c1, t1, t2, c2, ratio, epsilon);
        if (dist<distance){
            distance = dist;
            Arrays.copy(c1,0,c1.length,buffer1,0);
            Arrays.copy(c2,0,c2.length,buffer2,0);
        }

        //test third edge to segment
        dist = DistanceOp.distanceSquare(segmentStart, segmentEnd, c1, t2, t0, c2, ratio, epsilon);
        if (dist<distance){
            distance = dist;
            Arrays.copy(c1,0,c1.length,buffer1,0);
            Arrays.copy(c2,0,c2.length,buffer2,0);
        }

        //test points if they are projected inside the triangle
        double[] pointOnPlan = Geometries.projectPointOnPlan(segmentStart, planNormal, planD);
        dist = DistanceOp.distanceSquare(segmentStart, pointOnPlan);
        if (dist < distance && Geometries.inTriangle(t0, t1, t2, pointOnPlan)){
            distance = dist;
            Arrays.copy(segmentStart,0,segmentStart.length,buffer1,0);
            Arrays.copy(pointOnPlan,0,pointOnPlan.length,buffer2,0);
        }
        pointOnPlan = Geometries.projectPointOnPlan(segmentEnd, planNormal, planD);
        dist = DistanceOp.distanceSquare(segmentEnd, pointOnPlan);
        if (dist < distance && Geometries.inTriangle(t0, t1, t2, pointOnPlan)){
            distance = dist;
            Arrays.copy(segmentEnd,0,segmentEnd.length,buffer1,0);
            Arrays.copy(pointOnPlan,0,pointOnPlan.length,buffer2,0);
        }

        return distance;
    }

    /**
     * Calculate rectilinear distance between two tuples.
     * https://en.wikipedia.org/wiki/Rectilinear_distance
     *
     * @param p1 first point
     * @param p2 second point
     * @return double , rectilinear distance
     */
    public static double rectilinearDistance(Tuple p1, Tuple p2){
        return rectilinearDistance(p1.toDouble(), p2.toDouble());
    }

    /**
     * Calculate rectilinear distance between two points.
     * https://en.wikipedia.org/wiki/Rectilinear_distance
     *
     * @param p1 first point
     * @param p2 second point
     * @return double , rectilinear distance
     */
    public static double rectilinearDistance(double[] p1, double[] p2){
        double d = 0;
        for (int i=0;i<p1.length;i++){
            d += Math.abs(p1[i]-p2[i]);
        }
        return d;
    }


    /**
     * Calculate minimal distance between line and point
     * formula of line: A*x + B*y + C = 0
     *
     * @param lineP1 - point of line
     * @param lineP2 - point of line
     * @param p      - point outside line
     * @return
     */
    public static double distanceLineAndPoint(Point lineP1, Point lineP2, Point p) {
        double A;
        double B = 1;
        double C;
        double distance;

        final Tuple coord1 = lineP1.getCoordinate();
        final Tuple coord2 = lineP2.getCoordinate();
        final double x1 = coord1.get(0);
        final double y1 = coord1.get(1);
        final double x2 = coord2.get(0);
        final double y2 = coord2.get(1);
        final double px = p.getCoordinate().get(0);
        final double py = p.getCoordinate().get(1);
        if (Math.abs(y2 - y1) < Math.abs(x2 - x1)) {
            A = -(y2 - y1) / (x2 - x1);
            C = -y1 - A * x1;
            distance = Math.abs((A * px + B * py + C) / Math.sqrt(A * A + B * B));
        } else {
            A = -(x2 - x1) / (y2 - y1);
            C = -x1 - A * y1;
            distance = Math.abs((A * py + B * px + C) / Math.sqrt(A * A + B * B));
        }

        return distance;
    }

    /**
     * Calculate square distance between two ray.
     *
     * Simplified version of distanceSquare method for segments.
     *
     * TODO : refactor with line methods, and add a 'clamp' parameter.
     *
     * @param ray1Start ray 1 start point
     * @param ray1Direction ray 1 direction
     * @param buffer1 closest point on ray 1
     * @param ray2Start ray 2 start point
     * @param ray2Direction ray 2 direction
     * @param buffer2 closest point on ray 2
     * @param ratio
     * @param epsilon tolerance
     * @return distance
     */
    public static double rayDistanceSquare(double[] ray1Start, double[] ray1Direction, double[] buffer1,
                                            double[] ray2Start, double[] ray2Direction, double[] buffer2,
                                            double[] ratio, double epsilon) {
        final double[] d1 = ray1Direction; // Direction vector of ray S1
        final double[] d2 = ray2Direction; // Direction vector of ray S1
        final double[] r = subtract(ray1Start, ray2Start);
        final double a = dot(d1, d1); // Squared length of segment S1, always nonnegative
        final double e = dot(d2, d2); // Squared length of segment S1, always nonnegative
        final double f = dot(d2, r);

        final double c = dot(d1, r);
        final double b = dot(d1, d2);
        final double denom = a*e-b*b; // Always nonnegative


        if (denom != 0d) {
            ratio[0] = (b*f - c*e) / denom;
            ratio[1] = (a*f - b*c) / denom;
        } else {
            ratio[0] = 0.0;
            ratio[1] = (b>e) ? (c/b) : (f/e); // use largest denom
        }

        add(ray1Start, scale(d1, ratio[0]), buffer1);
        add(ray2Start, scale(d2, ratio[1]), buffer2);
        final double[] t = subtract(buffer1, buffer2);
        return dot(t, t);

    }

}
