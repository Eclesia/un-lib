
package science.unlicense.geometry.api;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.geometry.api.system.CoordinateSystem;

/**
 * Top level interface for all geometry type.
 *
 * This package contain the merge result of several specifications :
 * - SVG
 * - OGC WKT/WKB
 * - ISO 13249-3
 * - Postscript
 * - PDF
 * - TTF
 *
 * @author Johann Sorel
 */
public interface Geometry {

    /**
     * Get the coordinate system in which the coordinates are declared.
     *
     * @return CoordinateSystem, not null
     */
    CoordinateSystem getCoordinateSystem();

    /**
     * Set coordinate system in which the coordinates are declared.
     * This method does not transform the coordinates.
     *
     * @param cs , not null
     */
    void setCoordinateSystem(CoordinateSystem cs);

    /**
     * Get the geometry number of dimensions.
     * @return number of dimension
     */
    int getDimension();

    /**
     * Get this geometry BoundingBox.
     *
     * @return BoundingBox
     */
    BBox getBoundingBox();

    /**
     * Decompose geometry to simpler geometries.
     *
     * Decomposition must not be destructive, a curve must not be reduced to straight segments
     * or a surface must not be triangulated unless it's structure is made explicitly of segments or triangles.
     *
     * The resulting geometries must be of the same type of the parent, if this geometry
     * is linear or volumetric, so must be the decomposed components.
     *
     * @return an iterator over decomposition, null if geometry can not be decomposed further.
     */
    //TODO not sure yet it is a good approach
    //Iterator decompose();

    /**
     * Dictionary which may contain additional geometry attributes.
     * Returned dictionary may or may no support writing operation, behavior is implementation specific.
     *
     * @return Index, never null
     */
    Dictionary getUserProperties();

}
