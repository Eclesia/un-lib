
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;

/**
 * Compute the bounding box of a geometry.
 *
 * @author Johann Sorel
 */
public class BoundingBoxOp extends AbstractSingleOperation{

    public BoundingBoxOp(Geometry first) {
        super(first);
    }

    @Override
    public BBox execute() throws OperationException {
        return (BBox) super.execute();
    }
}
