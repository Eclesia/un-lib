
package science.unlicense.geometry.api.index.octtrees;

import science.unlicense.geometry.api.index.SDType;

/**
 * <p>
 * The interface provided by octtree implementations that distinguish between
 * static (immovable) and dynamic (movable) objects.
 * </p>
 *
 * @param <T> The precise type of octtree members.
 * @author Mark Raynsford
 */
public interface OctTreeSDType<T extends OctTreeMemberType<T>> extends OctTreeType<T> {

  /**
   * Insert the object <code>item</code> into the octtree. The object will be
   * given the categorization <code>type</code>.
   * <p>
   * The function returns <code>false</code> if the object could not be
   * inserted for any reason (perhaps due to being too large).
   * </p>
   *
   * @param item
   *          The object to insert
   * @param type
   *          The categorization of the object
   *
   * @return <code>true</code> if the object was inserted
   */

  boolean octTreeInsertSD(
    final T item,
    final SDType type);

  /**
   * Remove all dynamic (movable) objects from the tree.
   */

  void octTreeSDClearDynamic();
}
