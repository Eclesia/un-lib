
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.Point;

/**
 * Centroid operation.
 *
 * @author Johann Sorel
 */
public class CentroidOp extends AbstractSingleOperation{

    public CentroidOp(Geometry geometry) {
        super(geometry);
    }

    @Override
    public Point execute() throws OperationException {
        return (Point) super.execute();
    }

}
