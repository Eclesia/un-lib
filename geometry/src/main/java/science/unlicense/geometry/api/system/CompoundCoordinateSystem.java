
package science.unlicense.geometry.api.system;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author johann Sorel
 */
public class CompoundCoordinateSystem extends CoordinateSystem {

    private final CoordinateSystem cs1;
    private final CoordinateSystem cs2;

    public CompoundCoordinateSystem(CoordinateSystem cs1, CoordinateSystem cs2) {
        this.cs1 = cs1;
        this.cs2 = cs2;
    }

    @Override
    public Axis[] getAxis() {
        final Axis[] axis1 = cs1.getAxis();
        final Axis[] axis2 = cs2.getAxis();
        final Axis[] axis = new Axis[axis1.length+axis2.length];
        Arrays.copy(axis1, 0, axis1.length, axis, 0);
        Arrays.copy(axis2, 0, axis2.length, axis, axis1.length);
        return axis;
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(new Chars("CompoundCS["));
        cb.append(cs1.toChars());
        cb.append(',');
        cb.append(cs2.toChars());
        cb.append(new Chars("]"));
        return cb.toChars();
    }

}
