package science.unlicense.geometry.api.index.quadtrees;

import java.util.SortedSet;
import java.util.TreeSet;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.index.BoundingAreaCheck;
import science.unlicense.geometry.api.index.Dimensions;
import science.unlicense.geometry.api.index.SDType;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.VectorNf64;

/**
 * <p>
 * A quadtree implementation based on {@link QuadTreeSDBasic} but implementing a
 * minimum size limit on quadrants.
 * </p>
 *
 * @param <T> The type of objects contained within the tree.
 * @author Mark Raynsford
 */
public final class QuadTreeSDLimit<T extends QuadTreeMemberType<T>> extends CObject implements QuadTreeSDType<T> {

    final class Quadrant extends QuadrantType {

        private boolean leaf;
        private final SortedSet<T> quadrant_objects_dynamic;
        private final SortedSet<T> quadrant_objects_static;
        private final double quadrant_size_x;
        private final double quadrant_size_y;
        private Quadrant x0y0;
        private Quadrant x0y1;
        private Quadrant x1y0;
        private Quadrant x1y1;

        /**
         * Construct a quadrant defined by the inclusive ranges given by
         * <code>lower</code> and <code>upper</code>.
         */
        Quadrant(
                final VectorRW in_lower,
                final VectorRW in_upper) {
            this.upper.set(in_upper);
            this.lower.set(in_lower);
            this.x0y0 = null;
            this.x1y0 = null;
            this.x0y1 = null;
            this.x1y1 = null;
            this.leaf = true;
            this.quadrant_objects_static = new TreeSet<T>();
            this.quadrant_objects_dynamic = new TreeSet<T>();
            this.quadrant_size_x = Dimensions.getSpanSizeX(this.lower, this.upper);
            this.quadrant_size_y = Dimensions.getSpanSizeY(this.lower, this.upper);
        }

        void areaContaining(
                final BBox area,
                final SortedSet<T> items) {
            /**
             * If <code>area</code> completely contains this quadrant, collect
             * everything in this quadrant and all children of this quadrant.
             */

            if (BoundingAreaCheck.containedWithin(area, this)) {
                this.collectRecursive(items);
                return;
            }

            /**
             * Otherwise, <code>area</code> may be overlapping this quadrant and
             * therefore some items may still be contained within
             * <code>area</code>.
             */
            for (final T object : this.quadrant_objects_static) {
                assert object != null;
                if (BoundingAreaCheck.containedWithin(area, object)) {
                    items.add(object);
                }
            }
            for (final T object : this.quadrant_objects_dynamic) {
                assert object != null;
                if (BoundingAreaCheck.containedWithin(area, object)) {
                    items.add(object);
                }
            }

            if (this.leaf == false) {
                assert this.x0y0 != null;
                this.x0y0.areaContaining(area, items);
                assert this.x0y1 != null;
                this.x0y1.areaContaining(area, items);
                assert this.x1y0 != null;
                this.x1y0.areaContaining(area, items);
                assert this.x1y1 != null;
                this.x1y1.areaContaining(area, items);
            }
        }

        void areaOverlapping(
                final BBox area,
                final SortedSet<T> items) {
            /**
             * If <code>area</code> overlaps this quadrant, test each object
             * against <code>area</code>.
             */

            if (BoundingAreaCheck.overlapsArea(area, this)) {
                for (final T object : this.quadrant_objects_static) {
                    assert object != null;
                    if (BoundingAreaCheck.overlapsArea(area, object)) {
                        items.add(object);
                    }
                }
                for (final T object : this.quadrant_objects_dynamic) {
                    assert object != null;
                    if (BoundingAreaCheck.overlapsArea(area, object)) {
                        items.add(object);
                    }
                }

                if (this.leaf == false) {
                    assert this.x0y0 != null;
                    this.x0y0.areaOverlapping(area, items);
                    assert this.x1y0 != null;
                    this.x1y0.areaOverlapping(area, items);
                    assert this.x0y1 != null;
                    this.x0y1.areaOverlapping(area, items);
                    assert this.x1y1 != null;
                    this.x1y1.areaOverlapping(area, items);
                }
            }
        }

        private boolean canSplit() {
            final VectorRW min = QuadTreeSDLimit.this.minimum_size;
            return this.quadrant_size_x >= min.get(0) * 2
                    && this.quadrant_size_y >= min.get(1) * 2;
        }

        void clear() {
            this.quadrant_objects_static.clear();
            this.quadrant_objects_dynamic.clear();
            if (this.leaf == false) {
                assert this.x0y0 != null;
                this.x0y0.clear();
                assert this.x1y0 != null;
                this.x1y0.clear();
                assert this.x0y1 != null;
                this.x0y1.clear();
                assert this.x1y1 != null;
                this.x1y1.clear();
            }
        }

        void clearDynamic() {
            this.quadrant_objects_dynamic.clear();
            if (this.leaf == false) {
                assert this.x0y0 != null;
                this.x0y0.clearDynamic();
                assert this.x1y0 != null;
                this.x1y0.clearDynamic();
                assert this.x0y1 != null;
                this.x0y1.clearDynamic();
                assert this.x1y1 != null;
                this.x1y1.clearDynamic();
            }
        }

        private void collectRecursive(
                final SortedSet<T> items) {
            items.addAll(this.quadrant_objects_static);
            items.addAll(this.quadrant_objects_dynamic);
            if (this.leaf == false) {
                assert this.x0y0 != null;
                this.x0y0.collectRecursive(items);
                assert this.x0y1 != null;
                this.x0y1.collectRecursive(items);
                assert this.x1y0 != null;
                this.x1y0.collectRecursive(items);
                assert this.x1y1 != null;
                this.x1y1.collectRecursive(items);
            }
        }

        /**
         * Attempt to insert <code>item</code> into this node, or the children
         * of this node.
         *
         * @return <code>true</code>, if the item was inserted and
         * <code>false</code> otherwise.
         */
        boolean insert(
                final T item,
                final SDType type) {
            return this.insertBase(item, type);
        }

        /**
         * Insertion base case: item may or may not fit within node.
         */
        private boolean insertBase(
                final T item,
                final SDType type) {
            if (QuadTreeSDLimit.this.objects_all_static.contains(item)) {
                return false;
            }
            if (QuadTreeSDLimit.this.objects_all_dynamic.contains(item)) {
                return false;
            }
            if (BoundingAreaCheck.containedWithin(this, item)) {
                return this.insertStep(item, type);
            }
            return false;
        }

        /**
         * Insert the given object into the current quadrant's object list, and
         * also inserted into the "global" object list.
         */
        private boolean insertObject(
                final T item,
                final SDType type) {
            switch (type) {
                case SD_DYNAMIC: {
                    QuadTreeSDLimit.this.objects_all_dynamic.add(item);
                    this.quadrant_objects_dynamic.add(item);
                    return true;
                }
                case SD_STATIC: {
                    QuadTreeSDLimit.this.objects_all_static.add(item);
                    this.quadrant_objects_static.add(item);
                    return true;
                }
            }

            throw new RuntimeException("Unreachable code");
        }

        /**
         * Insertion inductive case: item fits within node, but may fit more
         * precisely within a child node.
         */
        private boolean insertStep(
                final T item,
                final SDType type) {
            /**
             * The object can fit in this node, but perhaps it is possible to
             * fit it more precisely within one of the child nodes.
             */

            /**
             * If this node is a leaf, and is large enough to split, do so.
             */
            if (this.leaf == true) {
                if (this.canSplit()) {
                    this.split();
                } else {

                    /**
                     * The node is a leaf, but cannot be split further. Insert
                     * directly.
                     */
                    return this.insertObject(item, type);
                }
            }

            /**
             * See if the object will fit in any of the child nodes.
             */
            assert this.leaf == false;
            assert this.x0y0 != null;
            if (BoundingAreaCheck.containedWithin(this.x0y0, item)) {
                assert this.x0y0 != null;
                return this.x0y0.insertStep(item, type);
            }
            assert this.x1y0 != null;
            if (BoundingAreaCheck.containedWithin(this.x1y0, item)) {
                assert this.x1y0 != null;
                return this.x1y0.insertStep(item, type);
            }
            assert this.x0y1 != null;
            if (BoundingAreaCheck.containedWithin(this.x0y1, item)) {
                assert this.x0y1 != null;
                return this.x0y1.insertStep(item, type);
            }
            assert this.x1y1 != null;
            if (BoundingAreaCheck.containedWithin(this.x1y1, item)) {
                assert this.x1y1 != null;
                return this.x1y1.insertStep(item, type);
            }

            /**
             * Otherwise, insert the object into this node.
             */
            return this.insertObject(item, type);
        }

        void raycast(
                final Ray ray,
                final SortedSet<QuadTreeRaycastResult<T>> items) {
            if (BoundingAreaCheck.rayBoxIntersects(
                    ray,
                    this.lower.get(0),
                    this.lower.get(1),
                    this.upper.get(0),
                    this.upper.get(1))) {

                this.raycastCheckObjects(ray, this.quadrant_objects_static, items);
                this.raycastCheckObjects(ray, this.quadrant_objects_dynamic, items);

                if (this.leaf == false) {
                    assert this.x0y0 != null;
                    this.x0y0.raycast(ray, items);
                    assert this.x0y1 != null;
                    this.x0y1.raycast(ray, items);
                    assert this.x1y0 != null;
                    this.x1y0.raycast(ray, items);
                    assert this.x1y1 != null;
                    this.x1y1.raycast(ray, items);
                }
            }
        }

        void raycastCheckObjects(
                final Ray ray,
                final SortedSet<T> objects,
                final SortedSet<QuadTreeRaycastResult<T>> items) {
            for (final T object : objects) {
                final Tuple object_lower = object.getLower();
                final Tuple object_upper = object.getUpper();

                if (BoundingAreaCheck.rayBoxIntersects(
                        ray,
                        object_lower.get(0),
                        object_lower.get(1),
                        object_upper.get(0),
                        object_upper.get(1))) {

                    final QuadTreeRaycastResult<T> r
                            = new QuadTreeRaycastResult<T>(object, DistanceOp.distance(new Vector2f64(object_lower.get(0), object_lower.get(1)),
                                    ray.getPosition()));
                    items.add(r);
                }
            }
        }

        void raycastQuadrants(
                final Ray ray,
                final SortedSet<QuadTreeRaycastResult<QuadrantType>> quadrants) {
            if (BoundingAreaCheck.rayBoxIntersects(
                    ray,
                    this.lower.get(0),
                    this.lower.get(1),
                    this.upper.get(0),
                    this.upper.get(1))) {

                if (this.leaf) {
                    final QuadTreeRaycastResult<QuadrantType> r
                            = new QuadTreeRaycastResult<QuadrantType>(this, DistanceOp.distance(new Vector2f64(this.lower.get(0), this.lower.get(1)),
                                    ray.getPosition()));
                    quadrants.add(r);
                    return;
                }

                assert this.x0y0 != null;
                this.x0y0.raycastQuadrants(ray, quadrants);
                assert this.x0y1 != null;
                this.x0y1.raycastQuadrants(ray, quadrants);
                assert this.x1y0 != null;
                this.x1y0.raycastQuadrants(ray, quadrants);
                assert this.x1y1 != null;
                this.x1y1.raycastQuadrants(ray, quadrants);
            }
        }

        boolean remove(
                final T item) {
            if ((QuadTreeSDLimit.this.objects_all_dynamic.contains(item) == false)
                    && (QuadTreeSDLimit.this.objects_all_static.contains(item) == false)) {
                return false;
            }

            /**
             * If an object is in objects_all, then it must be within the bounds
             * of the tree, according to insert().
             */
            assert BoundingAreaCheck.containedWithin(this, item);
            return this.removeStep(item);
        }

        private boolean removeStep(
                final T item) {
            if (this.quadrant_objects_dynamic.contains(item)) {
                this.quadrant_objects_dynamic.remove(item);
                QuadTreeSDLimit.this.objects_all_dynamic.remove(item);
                return true;
            }

            if (this.quadrant_objects_static.contains(item)) {
                this.quadrant_objects_static.remove(item);
                QuadTreeSDLimit.this.objects_all_static.remove(item);
                return true;
            }

            if (this.leaf == false) {
                assert this.x0y0 != null;
                if (BoundingAreaCheck.containedWithin(this.x0y0, item)) {
                    assert this.x0y0 != null;
                    return this.x0y0.removeStep(item);
                }
                assert this.x1y0 != null;
                if (BoundingAreaCheck.containedWithin(this.x1y0, item)) {
                    assert this.x1y0 != null;
                    return this.x1y0.removeStep(item);
                }
                assert this.x0y1 != null;
                if (BoundingAreaCheck.containedWithin(this.x0y1, item)) {
                    assert this.x0y1 != null;
                    return this.x0y1.removeStep(item);
                }
                assert this.x1y1 != null;
                if (BoundingAreaCheck.containedWithin(this.x1y1, item)) {
                    assert this.x1y1 != null;
                    return this.x1y1.removeStep(item);
                }
            }

            /**
             * The object must be in the tree, according to remove(). Therefore
             * it must be in this node, or one of the child quadrants.
             */
            throw new RuntimeException("Unreachable code");
        }

        /**
         * Split this node into four quadrants.
         */
        private void split() {
            assert this.canSplit();

            final Quadrants q = Quadrants.split(this.lower, this.upper);
            this.x0y0 = new Quadrant(q.getX0Y0Lower(), q.getX0Y0Upper());
            this.x0y1 = new Quadrant(q.getX0Y1Lower(), q.getX0Y1Upper());
            this.x1y0 = new Quadrant(q.getX1Y0Lower(), q.getX1Y0Upper());
            this.x1y1 = new Quadrant(q.getX1Y1Lower(), q.getX1Y1Upper());
            this.leaf = false;
        }

        <E extends Throwable> void traverse(
                final int depth,
                final QuadTreeTraversalType<E> traversal)
                throws E {
            traversal.visit(depth, this.lower, this.upper);
            if (this.leaf == false) {
                assert this.x0y0 != null;
                this.x0y0.traverse(depth + 1, traversal);
                assert this.x1y0 != null;
                this.x1y0.traverse(depth + 1, traversal);
                assert this.x0y1 != null;
                this.x0y1.traverse(depth + 1, traversal);
                assert this.x1y1 != null;
                this.x1y1.traverse(depth + 1, traversal);
            }
        }
    }

    /**
     * Construct a new quadtree with the given size and position.
     *
     * @param size The size.
     * @param position The position.
     * @param size_minimum The minimum quadrant size.
     *
     * @return A new quadtree.
     * @param <T> The precise type of quadtree members.
     */
    public static <T extends QuadTreeMemberType<T>> QuadTreeSDType<T> newQuadTree(
            final VectorRW size,
            final VectorRW position,
            final VectorRW size_minimum) {
        return new QuadTreeSDLimit<T>(position, size, size_minimum);
    }

    private final VectorRW minimum_size;
    private final SortedSet<T> objects_all_dynamic;
    private final SortedSet<T> objects_all_static;
    private final Quadrant root;

    private QuadTreeSDLimit(
            final VectorRW position,
            final VectorRW size,
            final VectorRW size_minimum) {
        CObjects.ensureNotNull(position, "Position");
        CObjects.ensureNotNull(size, "Size");
        CObjects.ensureNotNull(size_minimum, "Minimum size");

        QuadTreeChecks.checkSize("Quadtree size", size.get(0), size.get(1));
        QuadTreeChecks.checkSize(
                "Minimum quadrant size",
                size_minimum.get(0),
                size_minimum.get(1));

        if (size_minimum.get(0) > size.get(0)) {
            final String s
                    = String
                            .format(
                                    "Minimum quadrant width (%f) is greater than the quadtree width (%f)",
                                    size_minimum.get(0),
                                    size.get(0));
            throw new InvalidArgumentException(s);
        }
        if (size_minimum.get(1) > size.get(1)) {
            final String s
                    = String
                            .format(
                                    "Minimum quadrant height (%f) is greater than the quadtree height (%f)",
                                    size_minimum.get(1),
                                    size.get(1));
            throw new InvalidArgumentException(s);
        }

        this.objects_all_dynamic = new TreeSet<T>();
        this.objects_all_static = new TreeSet<T>();
        this.minimum_size = VectorNf64.create(size_minimum);

        final VectorRW lower = VectorNf64.create(position);
        final VectorRW upper
                = new Vector2f64(
                        (position.get(0) + size.get(0)) - 1,
                        (position.get(1) + size.get(1)) - 1);
        this.root = new Quadrant(lower, upper);
    }

    @Override
    public void quadTreeClear() {
        this.root.clear();
        this.objects_all_static.clear();
        this.objects_all_dynamic.clear();
    }

    @Override
    public double quadTreeGetPositionX() {
        return this.root.getLower().get(0);
    }

    @Override
    public double quadTreeGetPositionY() {
        return this.root.getLower().get(1);
    }

    @Override
    public double quadTreeGetSizeX() {
        return this.root.quadrant_size_x;
    }

    @Override
    public double quadTreeGetSizeY() {
        return this.root.quadrant_size_y;
    }

    @Override
    public boolean quadTreeInsert(
            final T item) {
        BoundingAreaCheck.checkWellFormed(item);
        return this.root.insert(item, SDType.SD_DYNAMIC);
    }

    @Override
    public boolean quadTreeInsertSD(
            final T item,
            final SDType type) {
        BoundingAreaCheck.checkWellFormed(item);
        CObjects.ensureNotNull(type, "Type");
        return this.root.insert(item, type);
    }

    @Override
    public void quadTreeIterateObjects(final Predicate f) throws Exception {
        CObjects.ensureNotNull(f, "Function");

        for (final T item : this.objects_all_static) {
            assert item != null;
            final Boolean next = f.evaluate(item);
            if (next.booleanValue() == false) {
                break;
            }
        }

        for (final T item : this.objects_all_dynamic) {
            assert item != null;
            final Boolean next = f.evaluate(item);
            if (next.booleanValue() == false) {
                break;
            }
        }
    }

    @Override
    public void quadTreeQueryAreaContaining(
            final BBox area,
            final SortedSet<T> items) {
        BoundingAreaCheck.checkWellFormed(area);
        CObjects.ensureNotNull(items, "Items");
        this.root.areaContaining(area, items);
    }

    @Override
    public void quadTreeQueryAreaOverlapping(
            final BBox area,
            final SortedSet<T> items) {
        BoundingAreaCheck.checkWellFormed(area);
        CObjects.ensureNotNull(items, "Items");
        this.root.areaOverlapping(area, items);
    }

    @Override
    public void quadTreeQueryRaycast(
            final Ray ray,
            final SortedSet<QuadTreeRaycastResult<T>> items) {
        CObjects.ensureNotNull(ray, "Ray");
        CObjects.ensureNotNull(items, "Items");
        this.root.raycast(ray, items);
    }

    @Override
    public void quadTreeQueryRaycastQuadrants(
            final Ray ray,
            final SortedSet<QuadTreeRaycastResult<QuadrantType>> items) {
        CObjects.ensureNotNull(ray, "Ray");
        CObjects.ensureNotNull(items, "Items");
        this.root.raycastQuadrants(ray, items);
    }

    @Override
    public boolean quadTreeRemove(
            final T item) {
        BoundingAreaCheck.checkWellFormed(item);
        return this.root.remove(item);
    }

    @Override
    public void quadTreeSDClearDynamic() {
        this.root.clearDynamic();
        this.objects_all_dynamic.clear();
    }

    @Override
    public <E extends Throwable> void quadTreeTraverse(
            final QuadTreeTraversalType<E> traversal)
            throws E {
        CObjects.ensureNotNull(traversal, "Traversal");
        this.root.traverse(0, traversal);
    }

    @Override
    public Chars toChars() {
        final CharBuffer b = new CharBuffer();
        b.append("[QuadTree ");
        b.append(this.root.getLower());
        b.append(" ");
        b.append(this.root.getUpper());
        b.append(" ");
        b.append(this.objects_all_static);
        b.append(" ");
        b.append(this.objects_all_dynamic);
        b.append("]");
        return b.toChars();
    }
}
