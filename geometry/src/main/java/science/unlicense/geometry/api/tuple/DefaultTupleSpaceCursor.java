
package science.unlicense.geometry.api.tuple;

import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.DecoratedVector;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class DefaultTupleSpaceCursor extends DecoratedVector implements TupleSpaceCursor {

    private final TupleSpace base;
    private final TupleRW buffer;
    private final VectorRW coordinate;

    public DefaultTupleSpaceCursor(TupleSpace base) {
        super(base.createTuple());
        this.base = base;
        this.buffer = (TupleRW) tuple;
        this.coordinate = VectorNf64.createDouble(base.getCoordinateSystem().getNumComponents());
    }

    @Override
    public Tuple samples() {
        return this;
    }

    @Override
    public Tuple coordinate() {
        return coordinate;
    }

    @Override
    public void moveTo(Tuple coordinate) {
        this.coordinate.set(coordinate);
        base.getTuple(this.coordinate, buffer);
    }

}
