
package science.unlicense.geometry.api;

import science.unlicense.common.api.collection.Collection;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_Surface
 *   An ST_Surface value is a 2-dimensional geometry that consists of a
 *   single connected interior that is associated with one exterior ring
 *   and zero or more interior rings
 *
 * @author Johann Sorel
 */
public interface Surface2D extends Surface, PlanarGeometry {

    @Override
    Curve getExterior();

    @Override
    Collection getInteriors();

}
