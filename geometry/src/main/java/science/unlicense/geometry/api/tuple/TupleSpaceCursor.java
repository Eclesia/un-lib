
package science.unlicense.geometry.api.tuple;

import science.unlicense.math.api.Tuple;

/**
 * Tuple iterator.
 *
 * @author Johann Sorel
 */
public interface TupleSpaceCursor {

    /**
     * Get samples at current cursor position.
     * @return Tuple
     */
    Tuple samples();

    /**
     * Get the current tuple coodinate.
     * @return Tuple, returns always the same container
     */
    Tuple coordinate();

    /**
     * Move cursor to given coordinate.
     *
     * @param coordinate
     */
    void moveTo(Tuple coordinate);

}
