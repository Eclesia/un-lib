
package science.unlicense.geometry.api;

import science.unlicense.geometry.api.tuple.TupleGrid1D;

/**
 * Lines is a serie of line segments not connected next to each other.
 *
 * Specification :
 * - OpenGL : one of the opengl primitives
 *
 * @author Johann Sorel
 */
public interface Lines extends Geometry {

    TupleGrid1D getCoordinates();

}
