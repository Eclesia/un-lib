
package science.unlicense.geometry.api.index.octtrees;

import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;


/**
 * The default implementation of the {@link OctTreeBuilderType}.
 *
 * @param <T>
 *          The precise type of octtree members.
 * @author Mark Raynsford
 */
public final class OctTreeBuilder<T extends OctTreeMemberType<T>> implements
  OctTreeBuilderType<T>
{
  /**
   * @return A new octtree builder.
   *
   * @param <T>
   *          The precise type of octtree members.
   */

  public static
    <T extends OctTreeMemberType<T>>
    OctTreeBuilderType<T>
    newBuilder()
  {
    return new OctTreeBuilder<T>();
  }

  private boolean         limit;
  private final VectorRW limit_size;
  private final VectorRW position;
  private boolean         prune;
  private final VectorRW size;

  private OctTreeBuilder()
  {
    this.limit_size = VectorNf64.createDouble(3);
    this.size = VectorNf64.createDouble(3);
    this.position = VectorNf64.createDouble(3);
  }

  @Override public OctTreeType<T> build()
  {
    if (this.prune) {
      if (this.limit) {
        return OctTreePruneLimit.newOctTree(
          this.size,
          this.position,
          this.limit_size);
      }
      return OctTreePrune.newOctTree(this.size, this.position);
    }

    if (this.limit) {
      return OctTreeLimit.newOctTree(
        this.size,
        this.position,
        this.limit_size);
    }

    return OctTreeBasic.newOctTree(this.size, this.position);
  }

  @Override public OctTreeSDType<T> buildWithSD()
  {
    if (this.prune) {
      if (this.limit) {
        return OctTreeSDPruneLimit.newOctTree(
          this.size,
          this.position,
          this.limit_size);
      }
      return OctTreeSDPrune.newOctTree(this.size, this.position);
    }
    if (this.limit) {
      return OctTreeSDLimit.newOctTree(
        this.size,
        this.position,
        this.limit_size);
    }
    return OctTreeSDBasic.newOctTree(this.size, this.position);
  }

  @Override public void disableLimitedOctantSizes()
  {
    this.limit = false;
  }

  @Override public void disablePruning()
  {
    this.prune = false;
  }

  @Override public void enableLimitedOctantSizes(
    final int x,
    final int y,
    final int z)
  {
    OctTreeChecks.checkSize("Octant minimum size", x, y, z);
    this.limit = true;
    this.limit_size.setXYZ(x, y, z);
  }

  @Override public void enablePruning()
  {
    this.prune = true;
  }

  @Override public void setPosition3i(
    final int x,
    final int y,
    final int z)
  {
    this.position.setXYZ(x, y, z);
  }

  @Override public void setSize3i(
    final int x,
    final int y,
    final int z)
  {
    OctTreeChecks.checkSize("Octtree size", x, y, z);
    this.size.setXYZ(x, y, z);
  }

}
