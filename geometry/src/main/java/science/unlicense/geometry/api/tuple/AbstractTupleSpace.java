
package science.unlicense.geometry.api.tuple;

import science.unlicense.common.api.number.NumberType;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractTupleSpace implements TupleSpace {

    protected SampleSystem coordinateSystem;
    protected SampleSystem sampleSystem;
    protected NumberType numericType;

    public AbstractTupleSpace(SampleSystem coordinateSystem, SampleSystem sampleSystem, NumberType numericType) {
        this.coordinateSystem = coordinateSystem;
        this.sampleSystem = sampleSystem;
    }

    @Override
    public SampleSystem getCoordinateSystem() {
        return coordinateSystem;
    }

    @Override
    public SampleSystem getSampleSystem() {
        return sampleSystem;
    }

    @Override
    public NumberType getNumericType() {
        return numericType;
    }

    @Override
    public Geometry getCoordinateGeometry() {
        return null;
    }

    @Override
    public TupleRW createTuple() {
        return Vectors.create(sampleSystem, numericType);
    }

    @Override
    public TupleSpaceCursor cursor() {
        return new DefaultTupleSpaceCursor(this);
    }

}
