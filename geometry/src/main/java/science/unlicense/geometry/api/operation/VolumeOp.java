
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 * Calculate geometry volume.
 *
 * @author Johann Sorel
 */
public class VolumeOp extends AbstractSingleOperation {

    public VolumeOp(Geometry geometry) {
        super(geometry);
    }

    @Override
    public Double execute() throws OperationException {
        return (Double) super.execute();
    }

}
