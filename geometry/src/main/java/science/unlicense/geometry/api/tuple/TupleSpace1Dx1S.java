
package science.unlicense.geometry.api.tuple;

/**
 * One Dimension Coordiante System with One Sample System.
 *
 * @author Johann Sorel
 */
public interface TupleSpace1Dx1S extends TupleSpace {

    /**
     * Evaluate value at given coordinate.
     *
     * @param coordinate : coordinate to evaluate
     * @return evaluated tuple, null if point was outside space geometry
     */
    double getSample(double coordinate);

}
