
package science.unlicense.geometry.api.tuple;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.number.Bits;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int16;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int64;
import science.unlicense.common.api.number.Int8;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt16;
import science.unlicense.common.api.number.UInt32;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;

/**
 *
 * Bank is expected to be ordered sample blocks next to another, and line by line within each block :
 * Samples 1
 * line 0 : [s1][s1][s1][s1][s1][s1] ...
 * line 1 : [s1][s1][s1][s1][s1][s1] ...
 * ...
 * Samples 2
 * line 0 : [s2][s2][s2][s2][s2][s2] ...
 * line 1 : [s2][s2][s2][s2][s2][s2] ...
 * ...
 *
 * @author Johann Sorel
 */
public class PlanarTupleGrid extends AbstractNumberTupleGrid {

    private final Buffer bank;
    private final int imageHeight;
    private final int imageWidth;

    public PlanarTupleGrid(Buffer bank, Extent.Long extent, NumberType sampleType, SampleSystem sampleSystem) {
        super(extent, sampleType, sampleSystem);
        if (extent.getDimension() != 2) {
            throw new UnsupportedOperationException();
        }
        this.imageWidth = (int) extent.getL(0);
        this.imageHeight = (int) extent.getL(1);
        this.bank = bank;
    }

    private void checkCoordinate(int[] coordinate, int sampleIndex) {
        if (sampleIndex >= nbSample) {
            throw new RuntimeException("Sample " + sampleIndex + " do not exist.");
        }
        if (coordinate.length != 2) {
            throw new RuntimeException("Invalid coordinate dimension, expected 2 but was" + coordinate.length);
        }
        if (coordinate[0] >= imageWidth || coordinate[1] >= imageHeight) {
            throw new RuntimeException("Invalid coordinate, out of image range");
        }
    }

    private void checkCoordinate(Tuple coordinate, int sampleIndex) {
        if (sampleIndex >= nbSample) {
            throw new RuntimeException("Sample " + sampleIndex + " do not exist.");
        }
        if (coordinate.getSampleCount() != 2) {
            throw new RuntimeException("Invalid coordinate dimension, expected 2 but was" + coordinate.getSampleCount());
        }
        if (Math.round(coordinate.get(0)) >= imageWidth || Math.round(coordinate.get(1)) >= imageHeight) {
            throw new RuntimeException("Invalid coordinate, out of image range");
        }
    }

    private int getByteOffsetInByte(Tuple coordinate, int sampleIndex) {
        checkCoordinate(coordinate, sampleIndex);
        final int bytePerSample = bitsPerSample / 8;
        final int c0 = (int) Math.round(coordinate.get(0));
        final int c1 = (int) Math.round(coordinate.get(1));
        return sampleIndex * (imageWidth * imageHeight * bytePerSample) //block offset
         + c1 * imageWidth * bytePerSample + c0 * bytePerSample; //sample offset
    }

    private int getByteOffsetInByte(int[] coordinate, int sampleIndex) {
        checkCoordinate(coordinate, sampleIndex);
        final int bytePerSample = bitsPerSample / 8;
        return sampleIndex * (imageWidth * imageHeight * bytePerSample) //block offset
         + coordinate[1] * imageWidth * bytePerSample + coordinate[0] * bytePerSample; //sample offset
    }

    private int getByteOffsetInBits(int[] coordinate, int sampleIndex) {
        checkCoordinate(coordinate, sampleIndex);
        return sampleIndex * (imageWidth * imageHeight * bitsPerSample) //block offset
         + coordinate[1] * imageWidth * bitsPerSample + coordinate[0] * bitsPerSample; //sample offset
    }

    private int getByteOffsetInBits(Tuple coordinate, int sampleIndex) {
        checkCoordinate(coordinate, sampleIndex);
        final int c0 = (int) Math.round(coordinate.get(0));
        final int c1 = (int) Math.round(coordinate.get(1));
        return sampleIndex * (imageWidth * imageHeight * bitsPerSample) //block offset
         + c1 * imageWidth * bitsPerSample + c0 * bitsPerSample; //sample offset
    }

    @Override
    public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
        if (sampleType == Int8.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                buffer.set(i, bank.readInt8(getByteOffsetInByte(coordinate, i)));
            }
        } else if (sampleType == Bits.TYPE_1_BIT) {
            for (int i = 0; i < nbSample; i++) {
                final int start = getByteOffsetInBits(coordinate, i);
                final int byteOffset = start / 8;
                final int bitoffset = start % 8;
                final byte b = bank.readInt8(byteOffset);
                final int d = 7 - bitoffset;
                buffer.set(i, (b & (1 << d)) >> d );
            }
        } else if (sampleType == Bits.TYPE_2_BIT) {
            throw new RuntimeException("Not implemented yet");
        } else if (sampleType == Bits.TYPE_4_BIT) {
            throw new RuntimeException("Not implemented yet");
        } else if (sampleType == UInt8.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                buffer.set(i, (short) bank.readUInt8(getByteOffsetInByte(coordinate, i)));
            }
        } else if (sampleType == Int16.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                buffer.set(i, bank.readInt16(getByteOffsetInByte(coordinate, i)));
            }
        } else if (sampleType == UInt16.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                buffer.set(i, bank.readUInt16(getByteOffsetInByte(coordinate, i)));
            }
        } else if (sampleType == Int32.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                buffer.set(i, bank.readInt32(getByteOffsetInByte(coordinate, i)));
            }
        } else if (sampleType == UInt32.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                buffer.set(i, bank.readUInt32(getByteOffsetInByte(coordinate, i)));
            }
        } else if (sampleType == Int64.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                buffer.set(i, bank.readInt64(getByteOffsetInByte(coordinate, i)));
            }
        } else if (sampleType == Float32.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                buffer.set(i, bank.readFloat32(getByteOffsetInByte(coordinate, i)));
            }
        } else if (sampleType == Float64.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                buffer.set(i, bank.readFloat64(getByteOffsetInByte(coordinate, i)));
            }
        } else {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        return buffer;
    }

    @Override
    public void setTuple(Tuple coordinate, Tuple buffer) {
        if (sampleType == Int8.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                bank.writeInt8((byte) buffer.get(i), getByteOffsetInByte(coordinate, i));
            }
        } else if (sampleType == UInt8.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                bank.writeUInt8((short) buffer.get(i), getByteOffsetInByte(coordinate, i));
            }
        } else if (sampleType == Int16.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                bank.writeInt16((short) buffer.get(i), getByteOffsetInByte(coordinate, i));
            }
        } else if (sampleType == UInt16.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                bank.writeUInt16((int) buffer.get(i), getByteOffsetInByte(coordinate, i));
            }
        } else if (sampleType == Int32.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                bank.writeInt32((int) buffer.get(i), getByteOffsetInByte(coordinate, i));
            }
        } else if (sampleType == UInt32.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                bank.writeUInt32((long) buffer.get(i), getByteOffsetInByte(coordinate, i));
            }
        } else if (sampleType == Int64.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                bank.writeInt64((long) buffer.get(i), getByteOffsetInByte(coordinate, i));
            }
        } else if (sampleType == Float32.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                bank.writeFloat32((float) buffer.get(i), getByteOffsetInByte(coordinate, i));
            }
        } else if (sampleType == Float64.TYPE) {
            for (int i = 0; i < nbSample; i++) {
                bank.writeFloat64(buffer.get(i), getByteOffsetInByte(coordinate, i));
            }
        } else {
            throw new RuntimeException("Unexpected type " + sampleType);
        }
    }
}
