
package science.unlicense.geometry.api.operation;


/**
 *
 * @author Johann Sorel
 */
public interface Operation {

    /**
     * Tolerance for calculations.
     *
     * @return double
     */
    double getEpsilon();

    /**
     *
     * @param epsilon
     */
    void setEpsilon(double epsilon);

    Object execute() throws OperationException;

}
