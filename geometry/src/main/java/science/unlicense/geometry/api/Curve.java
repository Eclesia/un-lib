
package science.unlicense.geometry.api;

/**
 * Geometry with continous points.
 *
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_Curve
 *   An ST_Curve value is a 1-dimensional geometry usually stored as a sequence of points
 *
 * @author Johann Sorel
 */
public interface Curve extends PlanarGeometry {

}
