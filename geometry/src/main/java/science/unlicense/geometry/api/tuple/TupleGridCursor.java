
package science.unlicense.geometry.api.tuple;

import science.unlicense.math.api.TupleRW;

/**
 * Tuple iterator.
 *
 * @author Johann Sorel
 */
public interface TupleGridCursor extends TupleSpaceCursor {

    @Override
    TupleRW samples();

    /**
     * Move cursor to given coordinate.
     *
     * @param coordinate
     */
    void moveTo(int coordinate);

    /**
     * Move to next tuple.
     *
     * @return false if no more tuple
     */
    boolean next();

}
