
package science.unlicense.geometry.api;

import science.unlicense.common.api.collection.Collection;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_Surface is 2D, this surface extend it to geometry of more then 2D.
 *
 * @author Johann Sorel
 */
public interface Surface extends Geometry {

    Geometry getExterior();

    Collection getInteriors();

}
