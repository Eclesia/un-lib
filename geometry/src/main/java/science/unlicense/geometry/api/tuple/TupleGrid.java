
package science.unlicense.geometry.api.tuple;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Tuple;

/**
 * A tuple buffer is a N dimension array of tuples.
 *
 *
 * @author Johann Sorel
 */
public interface TupleGrid extends TupleSpace {

    /**
     * Get buffer dimensions.
     *
     * @return int[]
     */
    Extent.Long getExtent();

    /**
     * Get contour geometry containing all datas.
     * For TupleGird the geometry is never null and is a BBox matching grid extent.
     *
     * @return BBox, never null
     */
    @Override
    BBox getCoordinateGeometry();

    void getTuple(BBox box, TupleGrid buffer);

    /**
     * set the given coordinate with given sample.
     *
     * @param coordinate
     * @param buffer
     */
    void setTuple(Tuple coordinate, Tuple buffer);

    /**
     * Fill the given bounding box with given sample.
     *
     * @param box
     * @param sample
     */
    void setTuple(BBox box, Tuple sample);

    /**
     * Create a cursor over buffer tuples.
     *
     * @return TupleCursor
     */
    @Override
    TupleGridCursor cursor();

}
