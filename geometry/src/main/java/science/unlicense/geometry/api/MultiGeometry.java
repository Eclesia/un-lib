
package science.unlicense.geometry.api;

import science.unlicense.common.api.collection.Sequence;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_GeomCollection
 *   An ST_GeomCollection is a collection of zero or more ST_Geometry values
 *
 * @author Johann Sorel
 */
public interface MultiGeometry extends Geometry {

    Sequence getGeometries();

}
