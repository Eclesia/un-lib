
package science.unlicense.geometry.api.system;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class Direction extends CObject{

    public static final Direction UP        = new Direction(new Chars("UP"));
    public static final Direction DOWN      = new Direction(new Chars("DOWN"));
    public static final Direction LEFT      = new Direction(new Chars("LEFT"));
    public static final Direction RIGHT     = new Direction(new Chars("RIGHT"));
    public static final Direction FORWARD   = new Direction(new Chars("FORWARD"));
    public static final Direction BACKWARD  = new Direction(new Chars("BACKWARD"));
    public static final Direction UNDEFINED = new Direction(new Chars("UNDEFINED"));

    private final Chars name;

    private Direction(Chars direction) {
        CObjects.ensureNotNull(direction);
        this.name = direction;
    }

    public Chars getName() {
        return name;
    }

    public Chars toChars() {
        return name;
    }

    /**
     *
     * @param other
     * @return 1 if same direction, -1 if opposite direction, 0 if incompatible
     */
    public int isCompatible(Direction other){
        if (this==other) return 1;

        if (    (this==UP || this==DOWN) && (other==UP || other==DOWN)
            || (this==LEFT || this==RIGHT) && (other==LEFT || other==RIGHT)
            || (this==FORWARD || this==BACKWARD) && (other==FORWARD || other==BACKWARD)){
            return -1;
        }

        return 0;
    }

    public int getHash() {
        int hash = 7;
        hash = 59 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Direction other = (Direction) obj;
        return !(this.name != other.name && (this.name == null || !this.name.equals(other.name)));
    }

}
