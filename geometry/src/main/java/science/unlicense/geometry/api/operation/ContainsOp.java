
package science.unlicense.geometry.api.operation;

import science.unlicense.geometry.api.Geometry;

/**
 * Test if given geometry is contained in this one.
 *
 * @author Johann Sorel
 */
public class ContainsOp extends AbstractBinaryOperation {

    public ContainsOp(Geometry first, Geometry second) {
        super(first,second);
    }

    @Override
    public Boolean execute() throws OperationException {
        return (Boolean) super.execute();
    }
}
