
package science.unlicense.geometry.api.system;

import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;

/**
 * A coordinate system define a ordered set of axis.
 *
 * In 3D models and animations may be stored in different coordinate system conventions.
 * Those are know as LeftHand or RightHand coordinate systems.
 *
 * TODO, find a proper way to merge with CSUtilities and to declare this information
 * on classes which need it.
 *
 * @author Johann Sorel
 */
public abstract class CoordinateSystem extends CObject implements SampleSystem {

    public int getDimension(){
        return getAxis().length;
    }

    @Override
    public int getNumComponents() {
        return getAxis().length;
    }

    public abstract Axis[] getAxis();

    @Override
    public Chars toChars() {
        final Axis[] axis = getAxis();
        final CharBuffer cb = new CharBuffer();
        cb.append(new Chars("CS["));
        cb.append(axis[0].toChars());
        for (int i=1;i<axis.length;i++){
            cb.append(',').append(axis[i].toChars());
        }
        cb.append(new Chars("]"));
        return cb.toChars();
    }

    @Override
    public int getHash() {
        int hash = 5;
        hash = 41 * hash + Arrays.computeHash(this.getAxis());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CoordinateSystem other = (CoordinateSystem) obj;
        return Arrays.equals(this.getAxis(), other.getAxis());
    }

}
