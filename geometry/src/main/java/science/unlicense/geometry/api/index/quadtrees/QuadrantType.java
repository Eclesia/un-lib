
package science.unlicense.geometry.api.index.quadtrees;

import science.unlicense.geometry.api.BBox;

/**
 * The type of quadrants in quadtrees.
 *
 * @author Mark Raynsford
 */
public abstract class QuadrantType extends BBox{

    public QuadrantType() {
        super(2);
    }

}
