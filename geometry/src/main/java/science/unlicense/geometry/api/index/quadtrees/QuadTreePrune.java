package science.unlicense.geometry.api.index.quadtrees;

import java.util.SortedSet;
import java.util.TreeSet;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.index.BoundingAreaCheck;
import science.unlicense.geometry.api.index.Dimensions;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.VectorNf64;

/**
 * <p>
 * A quadtree implementation based on {@link QuadTreeBasic} but implementing
 * empty node pruning when an object is removed from the tree.
 * </p>
 * <p>
 * An implementation that aggressively removes empty nodes from the tree
 * increases garbage collection pressure, but reduces the number of nodes that
 * must be traversed when enumerating objects. This improves the performance of
 * area and raycast queries.
 * </p>
 *
 * @param <T> The type of objects contained within the tree.
 * @author Mark Raynsford
 */
public final class QuadTreePrune<T extends QuadTreeMemberType<T>> extends CObject implements QuadTreeType<T> {

    final class Quadrant extends QuadrantType {

        private boolean leaf;
        private final Quadrant parent;
        private final SortedSet<T> quadrant_objects;
        private final double quadrant_size_x;
        private final double quadrant_size_y;
        private Quadrant x0y0;
        private Quadrant x0y1;
        private Quadrant x1y0;
        private Quadrant x1y1;

        /**
         * Construct a quadrant defined by the inclusive ranges given by
         * <code>lower</code> and <code>upper</code>.
         */
        Quadrant(
                final Quadrant in_parent,
                final VectorRW in_lower,
                final VectorRW in_upper) {
            this.parent = in_parent;
            this.upper.set(in_upper);
            this.lower.set(in_lower);
            this.x0y0 = null;
            this.x1y0 = null;
            this.x0y1 = null;
            this.x1y1 = null;
            this.leaf = true;
            this.quadrant_objects = new TreeSet<T>();
            this.quadrant_size_x = Dimensions.getSpanSizeX(this.lower, this.upper);
            this.quadrant_size_y = Dimensions.getSpanSizeY(this.lower, this.upper);
        }

        void areaContaining(
                final BBox area,
                final SortedSet<T> items) {
            /**
             * If <code>area</code> completely contains this quadrant, collect
             * everything in this quadrant and all children of this quadrant.
             */

            if (BoundingAreaCheck.containedWithin(area, this)) {
                this.collectRecursive(items);
                return;
            }

            /**
             * Otherwise, <code>area</code> may be overlapping this quadrant and
             * therefore some items may still be contained within
             * <code>area</code>.
             */
            for (final T object : this.quadrant_objects) {
                assert object != null;
                if (BoundingAreaCheck.containedWithin(area, object)) {
                    items.add(object);
                }
            }

            if (this.leaf == false) {
                assert this.x0y0 != null;
                this.x0y0.areaContaining(area, items);
                assert this.x0y1 != null;
                this.x0y1.areaContaining(area, items);
                assert this.x1y0 != null;
                this.x1y0.areaContaining(area, items);
                assert this.x1y1 != null;
                this.x1y1.areaContaining(area, items);
            }
        }

        void areaOverlapping(
                final BBox area,
                final SortedSet<T> items) {
            /**
             * If <code>area</code> overlaps this quadrant, test each object
             * against <code>area</code>.
             */

            if (BoundingAreaCheck.overlapsArea(area, this)) {
                for (final T object : this.quadrant_objects) {
                    assert object != null;
                    if (BoundingAreaCheck.overlapsArea(area, object)) {
                        items.add(object);
                    }
                }

                if (this.leaf == false) {
                    assert this.x0y0 != null;
                    this.x0y0.areaOverlapping(area, items);
                    assert this.x1y0 != null;
                    this.x1y0.areaOverlapping(area, items);
                    assert this.x0y1 != null;
                    this.x0y1.areaOverlapping(area, items);
                    assert this.x1y1 != null;
                    this.x1y1.areaOverlapping(area, items);
                }
            }
        }

        private boolean canSplit() {
            return (this.quadrant_size_x >= 2) && (this.quadrant_size_y >= 2);
        }

        private void collectRecursive(
                final SortedSet<T> items) {
            items.addAll(this.quadrant_objects);
            if (this.leaf == false) {
                assert this.x0y0 != null;
                this.x0y0.collectRecursive(items);
                assert this.x0y1 != null;
                this.x0y1.collectRecursive(items);
                assert this.x1y0 != null;
                this.x1y0.collectRecursive(items);
                assert this.x1y1 != null;
                this.x1y1.collectRecursive(items);
            }
        }

        /**
         * Attempt to insert <code>item</code> into this node, or the children
         * of this node.
         *
         * @return <code>true</code>, if the item was inserted and
         * <code>false</code> otherwise.
         */
        boolean insert(
                final T item) {
            return this.insertBase(item);
        }

        /**
         * Insertion base case: item may or may not fit within node.
         */
        private boolean insertBase(
                final T item) {
            if (QuadTreePrune.this.objects_all.contains(item)) {
                return false;
            }
            if (BoundingAreaCheck.containedWithin(this, item)) {
                return this.insertStep(item);
            }
            return false;
        }

        /**
         * Insert the given object into the current quadrant's object list, and
         * also inserted into the "global" object list.
         */
        private boolean insertObject(
                final T item) {
            QuadTreePrune.this.objects_all.add(item);
            this.quadrant_objects.add(item);
            return true;
        }

        /**
         * Insertion inductive case: item fits within node, but may fit more
         * precisely within a child node.
         */
        private boolean insertStep(
                final T item) {
            /**
             * The object can fit in this node, but perhaps it is possible to
             * fit it more precisely within one of the child nodes.
             */

            /**
             * If this node is a leaf, and is large enough to split, do so.
             */
            if (this.leaf == true) {
                if (this.canSplit()) {
                    this.split();
                } else {

                    /**
                     * The node is a leaf, but cannot be split further. Insert
                     * directly.
                     */
                    return this.insertObject(item);
                }
            }

            /**
             * See if the object will fit in any of the child nodes.
             */
            assert this.leaf == false;

            assert this.x0y0 != null;
            if (BoundingAreaCheck.containedWithin(this.x0y0, item)) {
                assert this.x0y0 != null;
                return this.x0y0.insertStep(item);
            }
            assert this.x1y0 != null;
            if (BoundingAreaCheck.containedWithin(this.x1y0, item)) {
                assert this.x1y0 != null;
                return this.x1y0.insertStep(item);
            }
            assert this.x0y1 != null;
            if (BoundingAreaCheck.containedWithin(this.x0y1, item)) {
                assert this.x0y1 != null;
                return this.x0y1.insertStep(item);
            }
            assert this.x1y1 != null;
            if (BoundingAreaCheck.containedWithin(this.x1y1, item)) {
                assert this.x1y1 != null;
                return this.x1y1.insertStep(item);
            }

            /**
             * Otherwise, insert the object into this node.
             */
            return this.insertObject(item);
        }

        void raycast(
                final Ray ray,
                final SortedSet<QuadTreeRaycastResult<T>> items) {
            if (BoundingAreaCheck.rayBoxIntersects(
                    ray,
                    this.lower.get(0),
                    this.lower.get(1),
                    this.upper.get(0),
                    this.upper.get(1))) {

                for (final T object : this.quadrant_objects) {
                    final Tuple object_lower
                            = object.getLower();
                    final Tuple object_upper
                            = object.getUpper();

                    if (BoundingAreaCheck.rayBoxIntersects(
                            ray,
                            object_lower.get(0),
                            object_lower.get(1),
                            object_upper.get(0),
                            object_upper.get(1))) {

                        final QuadTreeRaycastResult<T> r
                                = new QuadTreeRaycastResult<T>(object, DistanceOp.distance(new Vector2f64(object_lower.get(0), object_lower.get(1)),
                                        ray.getPosition()));
                        items.add(r);
                    }
                }

                if (this.leaf == false) {
                    assert this.x0y0 != null;
                    this.x0y0.raycast(ray, items);
                    assert this.x0y1 != null;
                    this.x0y1.raycast(ray, items);
                    assert this.x1y0 != null;
                    this.x1y0.raycast(ray, items);
                    assert this.x1y1 != null;
                    this.x1y1.raycast(ray, items);
                }
            }
        }

        void raycastQuadrants(
                final Ray ray,
                final SortedSet<QuadTreeRaycastResult<QuadrantType>> quadrants) {
            if (BoundingAreaCheck.rayBoxIntersects(
                    ray,
                    this.lower.get(0),
                    this.lower.get(1),
                    this.upper.get(0),
                    this.upper.get(1))) {

                if (this.leaf) {
                    final QuadTreeRaycastResult<QuadrantType> r
                            = new QuadTreeRaycastResult<QuadrantType>(this, DistanceOp.distance(new Vector2f64(this.lower.get(0), this.lower.get(1)),
                                    ray.getPosition()));
                    quadrants.add(r);
                    return;
                }

                assert this.x0y0 != null;
                this.x0y0.raycastQuadrants(ray, quadrants);
                assert this.x0y1 != null;
                this.x0y1.raycastQuadrants(ray, quadrants);
                assert this.x1y0 != null;
                this.x1y0.raycastQuadrants(ray, quadrants);
                assert this.x1y1 != null;
                this.x1y1.raycastQuadrants(ray, quadrants);
            }
        }

        boolean remove(
                final T item) {
            if (QuadTreePrune.this.objects_all.contains(item) == false) {
                return false;
            }

            /**
             * If an object is in objects_all, then it must be within the bounds
             * of the tree, according to insert().
             */
            assert BoundingAreaCheck.containedWithin(this, item);
            return this.removeStep(item);
        }

        private boolean removeStep(
                final T item) {
            if (this.quadrant_objects.contains(item)) {
                this.quadrant_objects.remove(item);
                QuadTreePrune.this.objects_all.remove(item);
                this.unsplitAttemptRecursive();
                return true;
            }

            this.unsplitAttemptRecursive();

            if (this.leaf == false) {
                assert this.x0y0 != null;
                if (BoundingAreaCheck.containedWithin(this.x0y0, item)) {
                    assert this.x0y0 != null;
                    return this.x0y0.removeStep(item);
                }
                assert this.x1y0 != null;
                if (BoundingAreaCheck.containedWithin(this.x1y0, item)) {
                    assert this.x1y0 != null;
                    return this.x1y0.removeStep(item);
                }
                assert this.x0y1 != null;
                if (BoundingAreaCheck.containedWithin(this.x0y1, item)) {
                    assert this.x0y1 != null;
                    return this.x0y1.removeStep(item);
                }
                assert this.x1y1 != null;
                if (BoundingAreaCheck.containedWithin(this.x1y1, item)) {
                    assert this.x1y1 != null;
                    return this.x1y1.removeStep(item);
                }
            }

            /**
             * The object must be in the tree, according to remove(). Therefore
             * it must be in this node, or one of the child quadrants.
             */
            throw new RuntimeException("Unreachable code");
        }

        /**
         * Split this node into four quadrants.
         */
        private void split() {
            assert this.canSplit();

            final Quadrants q = Quadrants.split(this.lower, this.upper);
            this.x0y0 = new Quadrant(this, q.getX0Y0Lower(), q.getX0Y0Upper());
            this.x0y1 = new Quadrant(this, q.getX0Y1Lower(), q.getX0Y1Upper());
            this.x1y0 = new Quadrant(this, q.getX1Y0Lower(), q.getX1Y0Upper());
            this.x1y1 = new Quadrant(this, q.getX1Y1Lower(), q.getX1Y1Upper());
            this.leaf = false;
        }

        <E extends Throwable> void traverse(
                final int depth,
                final QuadTreeTraversalType<E> traversal)
                throws E {
            traversal.visit(depth, this.lower, this.upper);
            if (this.leaf == false) {
                assert this.x0y0 != null;
                this.x0y0.traverse(depth + 1, traversal);
                assert this.x1y0 != null;
                this.x1y0.traverse(depth + 1, traversal);
                assert this.x0y1 != null;
                this.x0y1.traverse(depth + 1, traversal);
                assert this.x1y1 != null;
                this.x1y1.traverse(depth + 1, traversal);
            }
        }

        /**
         * Attempt to turn this node back into a leaf.
         */
        private void unsplitAttempt() {
            if (this.leaf == false) {
                boolean prunable = true;
                assert this.x0y0 != null;
                prunable &= this.x0y0.unsplitCanPrune();
                assert this.x1y0 != null;
                prunable &= this.x1y0.unsplitCanPrune();
                assert this.x0y1 != null;
                prunable &= this.x0y1.unsplitCanPrune();
                assert this.x1y1 != null;
                prunable &= this.x1y1.unsplitCanPrune();

                if (prunable) {
                    this.leaf = true;
                    this.x0y0 = null;
                    this.x0y1 = null;
                    this.x1y0 = null;
                    this.x1y1 = null;
                }
            }
        }

        /**
         * Attempt to turn this node and as many ancestors if this node back
         * into leaves as possible.
         */
        private void unsplitAttemptRecursive() {
            this.unsplitAttempt();
            if (this.parent != null) {
                this.parent.unsplitAttemptRecursive();
            }
        }

        private boolean unsplitCanPrune() {
            return (this.leaf == true) && this.quadrant_objects.isEmpty();
        }
    }

    /**
     * Construct a new quadtree with the given size and position.
     *
     * @param size The size.
     * @param position The position.
     * @return A new quadtree.
     * @param <T> The precise type of quadtree members.
     */
    public static <T extends QuadTreeMemberType<T>>
            QuadTreeType<T>
            newQuadTree(
                    final VectorRW size,
                    final VectorRW position) {
        return new QuadTreePrune<T>(position, size);
    }

    private final SortedSet<T> objects_all;
    private final VectorRW position;
    private Quadrant root;
    private final VectorRW size;

    private QuadTreePrune(
            final VectorRW in_position,
            final VectorRW in_size) {
        CObjects.ensureNotNull(in_position, "Position");
        CObjects.ensureNotNull(in_size, "Size");
        QuadTreeChecks.checkSize(
                "Quadtree size",
                in_size.get(0),
                in_size.get(1));

        this.position = VectorNf64.create(in_position);
        this.size = VectorNf64.create(in_size);

        this.objects_all = new TreeSet<T>();

        final VectorRW lower = VectorNf64.create(this.position);
        final VectorRW upper
                = new Vector2f64(
                        (this.position.get(0) + this.size.get(0)) - 1,
                        (this.position.get(1) + this.size.get(1)) - 1);
        this.root = new Quadrant(null, lower, upper);
    }

    @Override
    public void quadTreeClear() {
        this.root = new Quadrant(null, this.position, this.size);
        this.objects_all.clear();
    }

    @Override
    public double quadTreeGetPositionX() {
        return this.position.get(0);
    }

    @Override
    public double quadTreeGetPositionY() {
        return this.position.get(1);
    }

    @Override
    public double quadTreeGetSizeX() {
        return this.size.get(0);
    }

    @Override
    public double quadTreeGetSizeY() {
        return this.size.get(1);
    }

    @Override
    public boolean quadTreeInsert(
            final T item) {
        BoundingAreaCheck.checkWellFormed(item);
        return this.root.insert(item);
    }

    @Override
    public void quadTreeIterateObjects(final Predicate f) throws Exception {
        CObjects.ensureNotNull(f, "Function");

        for (final T item : this.objects_all) {
            assert item != null;
            final Boolean next = f.evaluate(item);
            if (next.booleanValue() == false) {
                break;
            }
        }
    }

    @Override
    public void quadTreeQueryAreaContaining(
            final BBox area,
            final SortedSet<T> items) {
        BoundingAreaCheck.checkWellFormed(area);
        CObjects.ensureNotNull(items, "Items");
        this.root.areaContaining(area, items);
    }

    @Override
    public void quadTreeQueryAreaOverlapping(
            final BBox area,
            final SortedSet<T> items) {
        BoundingAreaCheck.checkWellFormed(area);
        CObjects.ensureNotNull(items, "Items");
        this.root.areaOverlapping(area, items);
    }

    @Override
    public void quadTreeQueryRaycast(
            final Ray ray,
            final SortedSet<QuadTreeRaycastResult<T>> items) {
        CObjects.ensureNotNull(ray, "Ray");
        CObjects.ensureNotNull(items, "Items");
        this.root.raycast(ray, items);
    }

    @Override
    public void quadTreeQueryRaycastQuadrants(
            final Ray ray,
            final SortedSet<QuadTreeRaycastResult<QuadrantType>> items) {
        CObjects.ensureNotNull(ray, "Ray");
        CObjects.ensureNotNull(items, "Items");
        this.root.raycastQuadrants(ray, items);
    }

    @Override
    public boolean quadTreeRemove(
            final T item) {
        BoundingAreaCheck.checkWellFormed(item);
        return this.root.remove(item);
    }

    @Override
    public <E extends Throwable> void quadTreeTraverse(
            final QuadTreeTraversalType<E> traversal)
            throws E {
        CObjects.ensureNotNull(traversal, "Traversal");
        this.root.traverse(0, traversal);
    }

    @Override
    public Chars toChars() {
        final CharBuffer b = new CharBuffer();
        b.append("[QuadTree ");
        b.append(this.root.getLower());
        b.append(" ");
        b.append(this.root.getUpper());
        b.append(" ");
        b.append(this.objects_all);
        b.append("]");
        return b.toChars();
    }
}
