
package science.unlicense.geometry.api.index.quadtrees;

import science.unlicense.geometry.api.index.SDType;

/**
 * The interface provided by quadtree implementations that distinguish between
 * static (immovable) and dynamic (movable) objects.
 *
 * @param <T> The precise type of quadtree members.
 * @author Mark Raynsford
 */

public interface QuadTreeSDType<T extends QuadTreeMemberType<T>> extends QuadTreeType<T> {
  /**
   * Insert the object <code>item</code> into the quadtree. The object will be
   * given the categorization <code>type</code>.
   * <p>
   * The function returns <code>false</code> if the object could not be
   * inserted for any reason (perhaps due to being too large).
   * </p>
   *
   * @param item The object to insert
   * @param type The categorization of the object
   * @return <code>true</code> if the object was inserted
   */
  boolean quadTreeInsertSD(
    final T item,
    final SDType type);

  /**
   * Remove all dynamic (movable) objects from the tree.
   */

  void quadTreeSDClearDynamic();
}
