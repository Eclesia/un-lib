
package science.unlicense.geometry.api.tuple;

import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;

/**
 *
 * @author Johann Sorel
 */
public interface TupleSpace {

    /**
     * Get definition of coordinate values system.
     *
     * @return SampleSystem never null
     */
    SampleSystem getCoordinateSystem();

    /**
     * Get definition of contained tuple samples.
     *
     * @return SampleSystem never null
     */
    SampleSystem getSampleSystem();

    /**
     * @return type of samples,.
     */
    ArithmeticType getNumericType();

    /**
     * Get contour geometry containing all datas.
     * If geometry is null space is infinite.
     *
     * @return Geometry, can be null
     */
    Geometry getCoordinateGeometry();

    /**
     * @return create the most efficient tuple for storing samples.
     */
    TupleRW createTuple();

    /**
     * Evaluate value at given coordinate.
     *
     * @param coordinate : coordinate to evaluate
     * @param buffer : to store each sample value, not null
     * @return evaluated tuple, null if point was outside space geometry
     */
    TupleRW getTuple(Tuple coordinate, TupleRW buffer);

    /**
     * Create a cursor to navigate in tuple space.
     *
     * @return TupleCursor
     */
    TupleSpaceCursor cursor();

}
