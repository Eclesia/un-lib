
package science.unlicense.geometry.impl;

import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class IndexedRange {

    //values are inherited from OPENGL specifications
    //copied fromm GLC.Primitives
    public static final int POINTS                  = 0;
    public static final int LINE_STRIP              = 3;
    public static final int LINE_LOOP               = 2;
    public static final int LINES                   = 1;
    public static final int LINE_STRIP_ADJACENCY    = 11;
    public static final int LINES_ADJACENCY         = 10;
    public static final int TRIANGLE_STRIP          = 5;
    public static final int TRIANGLE_FAN            = 6;
    public static final int TRIANGLES               = 4;
    public static final int TRIANGLE_STRIP_ADJACENCY= 13;
    public static final int TRIANGLES_ADJACENCY     = 12;
    public static final int PATCHES                 = 14;


    protected final int mode;
    protected final int patchSize;
    protected final int indexOffset;
    protected final int count;

    public static IndexedRange POINTS(int offset, int count) {
        return new IndexedRange(POINTS, 1, offset, count);
    }

    public static IndexedRange LINE_STRIP(int offset, int count) {
        return new IndexedRange(LINE_STRIP, 2, offset, count);
    }

    public static IndexedRange LINE_LOOP(int offset, int count) {
        return new IndexedRange(LINE_LOOP, 2, offset, count);
    }

    public static IndexedRange LINES(int offset, int count) {
        return new IndexedRange(LINES, 2, offset, count);
    }

    /**
     * OpenGL constraints : GL3
     */
    public static IndexedRange LINE_STRIP_ADJACENCY(int offset, int count) {
        return new IndexedRange(LINE_STRIP_ADJACENCY, 2, offset, count);
    }

    /**
     * OpenGL constraints : GL3
     */
    public static IndexedRange LINES_ADJACENCY(int offset, int count) {
        return new IndexedRange(LINES_ADJACENCY, 2, offset, count);
    }

    public static IndexedRange TRIANGLE_STRIP(int offset, int count) {
        return new IndexedRange(TRIANGLE_STRIP, 3, offset, count);
    }

    public static IndexedRange TRIANGLE_FAN(int offset, int count) {
        return new IndexedRange(TRIANGLE_FAN, 3, offset, count);
    }

    public static IndexedRange TRIANGLES(int offset, int count) {
        return new IndexedRange(TRIANGLES, 3, offset, count);
    }

    /**
     * OpenGL constraints : GL3
     */
    public static IndexedRange TRIANGLE_STRIP_ADJACENCY(int offset, int count) {
        return new IndexedRange(TRIANGLE_STRIP_ADJACENCY, 3, offset, count);
    }

    /**
     * OpenGL constraints : GL3
     */
    public static IndexedRange TRIANGLES_ADJACENCY(int offset, int count) {
        return new IndexedRange(TRIANGLES_ADJACENCY, 3, offset, count);
    }

    /**
     * OpenGL constraints : GL3
     */
    public static IndexedRange PATCHES(int tupleSize, int offset, int count) {
        return new IndexedRange(PATCHES, tupleSize, offset, count);
    }

    /**
     *
     * @param mode POINTS, LINE_STRIP, LINE_LOOP, LINES,
     *      LINE_STRIP_ADJACENCY, LINES_ADJACENCY, TRIANGLE_STRIP,
     *      TRIANGLE_FAN, TRIANGLES, TRIANGLE_STRIP_ADJACENCY,
     *      TRIANGLES_ADJACENCY and PATCHES
     * @param patchSize informative value of the number of element to process at each patch.
     * @param offset offset from the beginning of the index
     *               do not multiply by the number of byte of the index value type.
     *               multiplication will be done automaticaly
     * @param count number of values from offset in the IBO
     */
    public IndexedRange(int mode, int patchSize, int offset, int count) {
        this.mode = mode;
        this.patchSize = patchSize;
        this.indexOffset = offset;
        this.count = count;
    }

    /**
     * Range mode.
     * POINTS, LINE_STRIP, LINE_LOOP, LINES,
     * LINE_STRIP_ADJACENCY, LINES_ADJACENCY, TRIANGLE_STRIP,
     * TRIANGLE_FAN, TRIANGLES, TRIANGLE_STRIP_ADJACENCY,
     * TRIANGLES_ADJACENCY or PATCHES
     * @return int
     */
    public int getMode() {
        return mode;
    }

    /**
     * Informative value of the number of element to process at each patch.
     * @return int
     */
    public int getPatchSize() {
        return patchSize;
    }

    /**
     * Offset from the beginning of the index.
     * Offset is in BYTES.
     *
     * @return int Offset in bytes.
     */
    public int getBytesOffset(NumberType type) {
        return type.getSizeInBytes() * indexOffset;
    }

    /**
     * Offset from the beginning of the index.
     * Offset is an index for the buffer.
     *
     * @return int Offset in elements in the buffer.
     */
    public int getIndexOffset() {
        return indexOffset;
    }

    /**
     * Number of values in the index range.
     *
     * If used with an index, this is the number of values from offset in the IBO.
     *
     * @return int
     */
    public int getCount() {
        return count;
    }

}
