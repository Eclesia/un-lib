
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.Curve;
import science.unlicense.geometry.api.Surface2D;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.impl.path.ConcatenatePathIterator;
import science.unlicense.geometry.api.path.PathIterator;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_CurvePolygon
 *   An ST_CurvePolygon value is a planar surface consisting of a single patch,
 *   defined by one exterior boundary and zero or more interior boundaries
 *
 * @author Johann Sorel
 */
public class CurvePolygon extends AbstractPlanarGeometry implements Surface2D {

    protected final Curve outer;
    protected final Sequence inners;

    public CurvePolygon(Curve outer, Sequence inners) {
        this.outer = outer;
        this.inners = inners;
    }

    public Curve getExterior() {
        return outer;
    }

    public Sequence getInteriors() {
        return inners;
    }

    public PathIterator createPathIterator() {
        final Sequence s = new ArraySequence();
        if (outer!=null) s.add(outer.createPathIterator());
        if (inners!=null){
            for (int i=0,n=inners.getSize();i<n;i++){
                s.add( ((PlanarGeometry) inners.get(i)).createPathIterator() );
            }
        }
        return new ConcatenatePathIterator(s);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CurvePolygon other = (CurvePolygon) obj;
        if (this.outer != other.outer && (this.outer == null || !this.outer.equals(other.outer))) {
            return false;
        }
        if (!this.inners.equals(other.inners)) {
            return false;
        }
        return true;
    }

}
