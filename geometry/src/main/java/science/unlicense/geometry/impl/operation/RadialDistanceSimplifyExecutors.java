
package science.unlicense.geometry.impl.operation;

import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.api.operation.RadialDistanceSimplifyOp;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.api.Point;

/**
 *
 * @author Johann Sorel
 */
public class RadialDistanceSimplifyExecutors {

    private RadialDistanceSimplifyExecutors(){}

    public static final AbstractSingleOperationExecutor POINT =
            new AbstractSingleOperationExecutor(RadialDistanceSimplifyOp.class, Point.class){

        public Object execute(Operation operation) {
            final RadialDistanceSimplifyOp op = (RadialDistanceSimplifyOp) operation;
            final Point p = (Point) op.getGeometry();
            return new DefaultPoint(p.getCoordinate().copy());
        }
    };

}
