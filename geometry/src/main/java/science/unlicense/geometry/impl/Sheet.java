
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.VectorRW;

/**
 * Similar to a plane but like a paper sheet.
 *
 * @author Johann Sorel
 */
public class Sheet extends AbstractGeometry {

    // Plane normal. Points x on the plane satisfy Dot(n,x) = d
    private VectorRW normal;
    // a point on the plane
    private VectorRW point;
    // d = dot(n,p) for a given point p on the plane
    private double d;

    public Sheet(VectorRW normal, VectorRW point) {
        super(normal.getSampleCount());
        this.normal = normal;
        this.point = point;
        this.d = normal.dot(point);
    }

    /**
     * Build plan from 3 points.
     * @param a Tuple, not null
     * @param b Tuple, not null
     * @param c Tuple, not null
     */
    public Sheet(VectorRW a, VectorRW b, VectorRW c) {
        super(a.getSampleCount());
        normal = b.subtract(a).cross(c.subtract(a)).localNormalize();
        point = a;
        d = normal.dot(a);
    }

    public VectorRW getNormal() {
        return normal;
    }

    /**
     * A point on the plane.
     */
    public VectorRW getPoint(){
        return point;
    }

    public double getD() {
        return d;
    }

    public BBox getBoundingBox() {
        //TODO not right, if normal is perfectly on an axis then the bbox is infnite
        //on another axi.
        final BBox bbox = new BBox(getCoordinateSystem());
        for (int i=0,n=getDimension();i<n;i++){
            bbox.setRange(i, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
        }
        return bbox;
    }

    /**
     * Sheet surface is infinite.
     * @return infinite
     */
    public double getArea() {
        return Double.POSITIVE_INFINITY;
    }

    /**
     * Sheet volume is 0.
     * @return infinite
     */
    public double getVolume() {
        return 0;
    }

}
