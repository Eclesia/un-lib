
package science.unlicense.geometry.impl;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.geometry.api.Point;
import science.unlicense.math.api.TupleRW;

/**
 * A fragment is a interpolated point on the surface of an indexed geometry.
 *
 * The name fragment comes from OpenGL and FragmentShader.
 *
 * @author Johann Sorel
 */
public interface Fragment extends Point {

    @Override
    TupleRW getCoordinate();

    Dictionary properties();

}
