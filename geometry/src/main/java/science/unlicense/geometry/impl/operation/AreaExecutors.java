
package science.unlicense.geometry.impl.operation;

import science.unlicense.geometry.api.operation.AreaOp;
import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.Cone;
import science.unlicense.geometry.impl.ConicalFrustrum;
import science.unlicense.geometry.impl.Cylinder;
import science.unlicense.geometry.impl.Disk;
import science.unlicense.geometry.impl.Ellipse;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.impl.OBBox3D;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.impl.Quad;
import science.unlicense.geometry.impl.Sheet;
import science.unlicense.geometry.impl.Sphere;

/**
 *
 * @author Johann Sorel
 */
public class AreaExecutors {

    private AreaExecutors(){}

    public static final AbstractSingleOperationExecutor CIRCLE =
            new AbstractSingleOperationExecutor(AreaOp.class, Circle.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Circle geom = (Circle) op.getGeometry();
            return geom.getArea();
        }
    };

    public static final AbstractSingleOperationExecutor ELLIPSE =
            new AbstractSingleOperationExecutor(AreaOp.class, Ellipse.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Ellipse geom = (Ellipse) op.getGeometry();
            return geom.getArea();
        }
    };

    public static final AbstractSingleOperationExecutor POINT =
            new AbstractSingleOperationExecutor(AreaOp.class, Point.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Point geom = (Point) op.getGeometry();
            return 0.0;
        }
    };

    public static final AbstractSingleOperationExecutor SEGMENT =
            new AbstractSingleOperationExecutor(AreaOp.class, Line.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Line geom = (Line) op.getGeometry();
            return 0.0;
        }
    };

    public static final AbstractSingleOperationExecutor CAPSULE =
            new AbstractSingleOperationExecutor(AreaOp.class, Capsule.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Capsule geom = (Capsule) op.getGeometry();
            return geom.getArea();
        }
    };

    public static final AbstractSingleOperationExecutor CONE =
            new AbstractSingleOperationExecutor(AreaOp.class, Cone.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Cone geom = (Cone) op.getGeometry();
            return geom.getArea();
        }
    };

    public static final AbstractSingleOperationExecutor CONICAL_FRUSTRUM =
            new AbstractSingleOperationExecutor(AreaOp.class, ConicalFrustrum.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final ConicalFrustrum geom = (ConicalFrustrum) op.getGeometry();
            return geom.getArea();
        }
    };

    public static final AbstractSingleOperationExecutor CYLINDER =
            new AbstractSingleOperationExecutor(AreaOp.class, Cylinder.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Cylinder geom = (Cylinder) op.getGeometry();
            return geom.getArea();
        }
    };

    public static final AbstractSingleOperationExecutor DISK =
            new AbstractSingleOperationExecutor(AreaOp.class, Disk.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Disk geom = (Disk) op.getGeometry();
            return geom.getArea();
        }
    };

    public static final AbstractSingleOperationExecutor OBBOX3D =
            new AbstractSingleOperationExecutor(AreaOp.class, OBBox3D.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final OBBox3D geom = (OBBox3D) op.getGeometry();
            return geom.getArea();
        }
    };

    public static final AbstractSingleOperationExecutor PLANE =
            new AbstractSingleOperationExecutor(AreaOp.class, Plane.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Plane geom = (Plane) op.getGeometry();
            return geom.getArea();
        }
    };

    public static final AbstractSingleOperationExecutor QUAD =
            new AbstractSingleOperationExecutor(AreaOp.class, Quad.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Quad geom = (Quad) op.getGeometry();
            return geom.getArea();
        }
    };

    public static final AbstractSingleOperationExecutor SHEET =
            new AbstractSingleOperationExecutor(AreaOp.class, Sheet.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Sheet geom = (Sheet) op.getGeometry();
            return geom.getArea();
        }
    };

    public static final AbstractSingleOperationExecutor SPHERE =
            new AbstractSingleOperationExecutor(AreaOp.class, Sphere.class){

        public Object execute(Operation operation) {
            final AreaOp op = (AreaOp) operation;
            final Sphere geom = (Sphere) op.getGeometry();
            return geom.getArea();
        }
    };

}
