
package science.unlicense.geometry.impl.operation;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.operation.AbstractBinaryOperation;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.operation.IntersectionOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.Operations;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.algorithm.SutherlandHodgman;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public class IntersectionExecutors {

    private IntersectionExecutors(){}


    /**
     * Plan to plan intersection.
     *
     * @param p1
     * @param p2
     * @param epsilon
     * @return
     */
    public static Object intersection(Plane p1, Plane p2, double epsilon) {
        final boolean parallel = p1.getNormal().isParallel(p2.getNormal(), epsilon);
        if (parallel) {
            //plane overlaps test
            final VectorRW a = p1.getPoint();
            final double d = DistanceOp.distance(a, p2);
            if (d < epsilon && d > -epsilon) {
                //they overlaps
                return p1;
            } else {
                //no intersection
                return null;
            }
        }

        //intersection is an infinite line / ray

        // the ray direction is the cross product of both normals, perpendicular to both
        final VectorRW rayDir = p1.getNormal().cross(p2.getNormal());
        //find a point of the ray
        final double det = rayDir.lengthSquare();
        // calculate the final (point, normal)
        final VectorRW pos = rayDir.cross(p2.getNormal()).scale(-p1.getD());
        pos.localAdd(p1.getNormal().cross(rayDir).scale(-p2.getD()));
        pos.localScale(1.0/det);

        return new Ray(pos, rayDir);
    }

    /**
     * Ray to segment intersection.
     *
     * @param ray
     * @param segment
     * @param epsilon
     * @return
     */
    public static Object intersection(Ray ray, Line segment, double epsilon) {

        final double[] segmentStart = segment.getStart().toDouble();
        final double[] segmentEnd = segment.getEnd().toDouble();
        final double[] ray2Pos = segmentStart;
        final double[] ray2Dir = Vectors.subtract(segmentEnd, segmentStart);
        final double[] closest1 = new double[ray2Pos.length];
        final double[] closest2 = new double[ray2Pos.length];
        final double[] ratio = new double[2];

        final double distance = DistanceOp.rayDistanceSquare(
                ray.getPosition().toDouble(), ray.getDirection().toDouble(), closest1,
                ray2Pos, ray2Dir, closest2, ratio, epsilon);

        //check segment distance and inside segment
        if (distance <= epsilon && (ratio[1]) >=0 && (ratio[1]) <=1) {
            return new DefaultPoint(closest2);
        }

        return null;
    }


    public static final AbstractBinaryOperationExecutor RAY_TRIANGLE =
            new AbstractBinaryOperationExecutor(IntersectionOp.class, Ray.class, Triangle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {

            final Ray ray = (Ray) first;
            final Triangle triangle = (Triangle) second;
            final TupleRW t1 = triangle.getFirstCoord();
            final TupleRW t2 = triangle.getSecondCoord();
            final TupleRW t3 = triangle.getThirdCoord();
            final Plane p1 = new Plane(VectorNf64.create(t1), VectorNf64.create(t2), VectorNf64.create(t3));
            final VectorRW n = p1.getNormal();

            if (n.length()==0) {
                //triangle is a point
                return null;
            }

            //intersect with triangle plane
            Object res = Operations.execute(new IntersectionOp(ray, p1, op.epsilon));

            if (res==null) {
                //no intersection
                return null;
            } else if (res instanceof Point) {
                //single point intersection, check if in the triangle

                //todo : there should be a more efficient way, to avoid computing it again
                final Tuple position = ray.getPosition();
                final Tuple direction = ray.getDirection();
                final VectorRW rel = VectorNf64.createDouble(3);
                rel.set(position);
                rel.localSubtract(t1);
                final double a = -n.dot(rel);
                final double b =  n.dot(direction);

                if (Math.abs(b) < 0.000001){
                    return null;
                }

                final double r = a / b;
                return new DefaultPoint(new Vector3f64(
                    position.get(0) + r * direction.get(0),
                    position.get(1) + r * direction.get(1),
                    position.get(2) + r * direction.get(2)
                    ));

            } else if (res instanceof Ray) {
                //ray is in the triangle plane
                Object i1 = intersection(ray, new DefaultLine(t1, t2), op.epsilon);
                Object i2 = intersection(ray, new DefaultLine(t2, t3), op.epsilon);
                Object i3 = intersection(ray, new DefaultLine(t3, t1), op.epsilon);

                if (i1==null) {
                    if (i2==null) {
                        if (i3==null) {
                            return null;
                        } else {
                            return (Point) i3;
                        }
                    } else {
                        if (i3==null) {
                            return (Point) i2;
                        } else {
                            return new DefaultLine(((Point) i2).getCoordinate(),((Point) i3).getCoordinate());
                        }
                    }
                } else {
                    if (i2==null) {
                        if (i3==null) {
                            return (Point) i1;
                        } else {
                            return new DefaultLine(((Point) i1).getCoordinate(),((Point) i3).getCoordinate());
                        }
                    } else {
                        if (i3==null) {
                            return new DefaultLine(((Point) i1).getCoordinate(),((Point) i2).getCoordinate());
                        } else {
                            throw new OperationException("Unexpected case");
                        }
                    }
                }

            } else {
                throw new OperationException("Unexpected case");
            }

        }
    };

    public static final AbstractBinaryOperationExecutor RAY_SEGMENT =
            new AbstractBinaryOperationExecutor(IntersectionOp.class, Ray.class, Line.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {

            final Ray ray = (Ray) first;
            final Line segment = (Line) second;
            return intersection(ray, segment, op.epsilon);
        }
    };

    /**
     * http://en.wikipedia.org/wiki/Line–plane_intersection
     */
    public static final AbstractBinaryOperationExecutor RAY_PLANE =
            new AbstractBinaryOperationExecutor(IntersectionOp.class, Ray.class, Plane.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {

            final Ray ray = (Ray) first;
            final Plane plane = (Plane) second;
            final TupleRW rayPosition = ray.getPosition();
            final VectorRW rayDirection = ray.getDirection();

            final VectorRW n = plane.getNormal();

            final VectorRW rel = VectorNf64.createDouble(3);
            rel.set(rayPosition);
            rel.localSubtract(plane.getPoint());
            final double a = n.dot(rel);
            final double b =  n.dot(rayDirection);

            if (Math.abs(b) <= op.epsilon){
                //ray is parallal
                if (Math.abs(a) <= op.epsilon){
                    //ray is on the plane
                    return new Ray(rayPosition.copy(), rayDirection.copy());
                }

                return null;
            }

            final double r = -a / b;
            if (r<0){
                //ray points the opposite direction
                return null;
            }

            return new DefaultPoint(new Vector3f64(
                rayPosition.get(0) + r * rayDirection.get(0),
                rayPosition.get(1) + r * rayDirection.get(1),
                rayPosition.get(2) + r * rayDirection.get(2)
                ));
        }
    };

    public static final AbstractBinaryOperationExecutor TRIANGLE_TRIANGLE =
            new AbstractBinaryOperationExecutor(IntersectionOp.class, Triangle.class, Triangle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Triangle t1 = (Triangle) first;
            final Triangle t2 = (Triangle) second;

            final Sequence subject = new ArraySequence();
            subject.add(t1.getFirstCoord());
            subject.add(t1.getSecondCoord());
            subject.add(t1.getThirdCoord());

            final Sequence clip = new ArraySequence();
            clip.add(new DefaultLine(t2.getFirstCoord(), t2.getSecondCoord()));
            clip.add(new DefaultLine(t2.getSecondCoord(),t2.getThirdCoord()));
            clip.add(new DefaultLine(t2.getThirdCoord(), t2.getFirstCoord()));

            final Sequence result = SutherlandHodgman.clip(subject, clip);
            final int nb = result.getSize();
            if (nb==0){
                return null;
            } else if (nb==3){
                return new DefaultTriangle((TupleRW) result.get(0), (TupleRW) result.get(1), (TupleRW) result.get(2));
            } else {
                result.add(VectorNf64.create((Tuple) result.get(0)));
                return new Polygon(new Polyline(Geometries.toTupleBuffer(result)), new ArraySequence());
            }

        }
    };

    public static final AbstractBinaryOperationExecutor TRIANGLE_PLANE =
            new AbstractBinaryOperationExecutor(IntersectionOp.class, Triangle.class, Plane.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Triangle t1 = (Triangle) first;
            final Plane p2 = (Plane) second;

            //first convert triangle to a plane
            final Plane p1 = new Plane(VectorNf64.create(t1.getFirstCoord()), VectorNf64.create(t1.getSecondCoord()), VectorNf64.create(t1.getThirdCoord()));
            Object inter = intersection(p1, p2, op.epsilon);
            if (inter==null) {
                //no plane intersection
                return null;
            } else if (inter instanceof Plane) {
                //plane overlaps triangle
                return t1;
            } else if (inter instanceof Ray) {
                return Operations.execute(new IntersectionOp((Ray) inter, t1, op.epsilon));
            } else {
                throw new OperationException("Unexpected case");
            }
        }
    };


    public static final AbstractBinaryOperationExecutor PLANE_PLANE =
            new AbstractBinaryOperationExecutor(IntersectionOp.class, Plane.class, Plane.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Plane p1 = (Plane) first;
            final Plane p2 = (Plane) second;
            return intersection(p1, p2, op.epsilon);
        }

    };

    public static final AbstractBinaryOperationExecutor RECTANGLE_RECTANGLE =
            new AbstractBinaryOperationExecutor(IntersectionOp.class, Rectangle.class, Rectangle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Rectangle rect1 = (Rectangle) first;
            final Rectangle rect2 = (Rectangle) second;
            return rect1.intersection(rect2);
        }
    };

}
