

package science.unlicense.geometry.impl;

/**
 * 3D Oriented BoundingBox.
 *
 * @author Johann Sorel
 */
public class OBBox3D extends OBBox {

    public OBBox3D() {
        super(3);
    }

    /**
     * Calculate bounding box diagonal.
     *
     * @return diagonal
     */
    public double getDiagonal() {
        final double s1 = getSpan(0);
        final double s2 = getSpan(1);
        final double s3 = getSpan(2);
        return Math.sqrt(s1*s1 + s2*s2 + s3*s3);
    }

    public double getArea() {
        final double s1 = getSpan(0);
        final double s2 = getSpan(1);
        final double s3 = getSpan(2);
        return 2 * (s1*s2 + s1*s3 + s2*s3);
    }

    public double getVolume() {
        return getSpan(0) * getSpan(1) * getSpan(2);
    }

}
