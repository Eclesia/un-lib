package science.unlicense.geometry.impl.path;

import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.math.api.TupleRW;

/**
 * Abstract path iterator based on succesive steps.
 * @author Johann Sorel
 */
public abstract class AbstractStepPathIterator implements PathIterator {

    protected PathStep2D currentStep = null;

    @Override
    public int getDimension() {
        return 2;
    }

    public int getType() {
        return currentStep.type;
    }

    public TupleRW getPosition(TupleRW buffer) {
        return currentStep.getPosition(buffer);
    }

    public TupleRW getFirstControl(TupleRW buffer) {
        return currentStep.getFirstControl(buffer);
    }

    public TupleRW getSecondControl(TupleRW buffer) {
        return currentStep.getSecondControl(buffer);
    }

    public TupleRW getArcParameters(TupleRW buffer) {
        return currentStep.getArcParameters(buffer);
    }

    public boolean getLargeArcFlag() {
        return currentStep.largeArcFlag;
    }

    public boolean getSweepFlag() {
        return currentStep.sweepFlag;
    }
}
