
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractFragment extends AbstractPlanarGeometry implements Fragment {

    /**
     * Line bounding box is a box which has as corners both start and end tuples
     * with a width of twice the radius.
     *
     * @return BoundingBox
     */
    @Override
    public BBox getBoundingBox() {
        final BBox bbox = new BBox(getCoordinateSystem());
        final TupleRW coord = getCoordinate();
        for (int i=0,n=getDimension(); i<n; i++) {
            final double d = coord.get(i);
            bbox.setRange(i, d, d);
        }
        return bbox;
    }
}
