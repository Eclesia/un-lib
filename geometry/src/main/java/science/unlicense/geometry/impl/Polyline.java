
package science.unlicense.geometry.impl;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.number.Float64;
import science.unlicense.geometry.api.Curve;
import science.unlicense.geometry.api.LineStrip;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.impl.path.CoordinatePathIterator;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 * A LineString/Polyline.
 *
 * Specification :
 * - SVG v1.1:9.6 :
 *   The ‘polyline’ element defines a set of connected straight line segments.
 *   Typically, ‘polyline’ elements define open shapes.
 * - WKT/WKB ISO 13249-3 : LineString/LinearRing/ST_LineString
 *   An ST_LineString value has linear interpolation between ST_Point values
 *   called LineString or LinearRing if closed
 *
 * @author Johann Sorel
 */
public class Polyline extends AbstractPlanarGeometry implements LineStrip, Curve {

    private TupleGrid1D coords;

    public Polyline() {
        this(new InterleavedTupleGrid1D(Float64.TYPE, new UndefinedSystem(2), 0));
    }

    public Polyline(TupleGrid1D coords) {
        CObjects.ensureNotNull(coords);
        this.coords = coords;
    }

    public TupleGrid1D getCoordinates() {
        return coords;
    }

    /**
     * A Polyline isclosed if first coordinate equals last.
     * @return true if polyline is closed.
     */
    public boolean isClosed(){
        final TupleRW t0 = coords.createTuple();
        final TupleRW tn = coords.createTuple();
        coords.getTuple(0, t0);
        coords.getTuple(coords.getDimension()-1, tn);
        return t0.equals(tn);
    }

    public PathIterator createPathIterator() {
        return new CoordinatePathIterator(coords);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Polyline other = (Polyline) obj;
        if (!this.coords.equals(other.coords)) {
            return false;
        }
        return true;
    }

}
