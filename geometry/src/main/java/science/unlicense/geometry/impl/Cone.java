
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class Cone extends AbstractOrientedGeometry {

    private double radius;
    private double height;

    public Cone(double height, double radius) {
        super(3);
        this.height = height;
        this.radius = radius;
    }

    /**
     * The base radius of the cone.
     */
    public double getRadius() {
        return radius;
    }

    public void setRadius(double r) {
        this.radius = r;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * Calculate the base circle diameter.
     *
     * @return base circle diameter.
     */
    public double getBaseDiameter() {
        return 2*radius;
    }

    /**
     * Calculate the base circle surface area.
     *
     * @return base circle area.
     */
    public double getBaseArea() {
        return Math.PI*radius*radius;
    }

    /**
     * Calculate base circle circumference.
     *
     * @return base circle circumference
     */
    public double getBaseCircumference() {
        return 2*Math.PI*radius;
    }

    /**
     * Calculate the cone lateral height.
     *
     * @return cone lateral height
     */
    public double getLateralHeight(){
        return Math.sqrt( (radius*radius) + (height*height));
    }

    /**
     * Calculate the cone volume.
     *
     * @return cone volume.
     */
    public double getVolume(){
        return (1d/3d) * getBaseArea() * height;
    }

    /**
     * Calculate lateral surface of the cone.
     *
     * @return cone lateral surface.
     */
    public double getLateralSurfaceArea(){
        final double l = getLateralHeight();
        return Math.PI*radius*l;
    }

    /**
     * Calculate surface of the cone.
     * Base + lateral surface
     *
     * @return cone lateral surface.
     */
    public double getArea(){
        return getBaseArea() + getLateralSurfaceArea();
    }

    public BBox getUnorientedBounds() {
        return new BBox(
                new Vector3f64(-radius, -height/2, -radius),
                new Vector3f64(radius, height/2, radius)
        );
    }

}
