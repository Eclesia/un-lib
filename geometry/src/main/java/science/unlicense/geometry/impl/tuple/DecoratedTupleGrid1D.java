
package science.unlicense.geometry.impl.tuple;

import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public abstract class DecoratedTupleGrid1D extends DecoratedTupleGrid implements TupleGrid1D {

    @Override
    protected abstract TupleGrid1D getBase();

    @Override
    public int getDimension() {
        return getBase().getDimension();
    }

    @Override
    public void getTuple(int coordinate, TupleRW buffer) {
        getBase().getTuple(coordinate, buffer);
    }

    @Override
    public void setTuple(int coordinate, Tuple sample) {
        getBase().setTuple(coordinate, sample);
    }

}
