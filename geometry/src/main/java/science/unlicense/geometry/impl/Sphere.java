
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 * Sphere geometry.
 *
 * @author Johann Sorel
 */
public class Sphere extends AbstractOrientedGeometry {

    private double radius;

    public Sphere() {
        this(1);
    }

    public Sphere(double radius) {
        super(3);
        this.radius = radius;
    }

    public Sphere(Tuple center, double radius) {
        super(3);
        this.radius = radius;
        transform.getTranslation().set(center);
        transform.notifyChanged();
    }

    public Point getCentroid() {
        return new DefaultPoint(getCenter().copy());
    }

    public TupleRW getCenter() {
        return transform.getTranslation();
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * Calculate surface of the sphere.
     *
     * @return sphere surface area.
     */
    public double getArea() {
        return 4d*Math.PI*radius*radius;
    }

    /**
     * Calculate the sphere volume.
     *
     * @return sphere volume
     */
    public double getVolume() {
        return (4d/3d)*Math.PI*radius*radius*radius;
    }

    public BBox getUnorientedBounds() {
        final BBox bbox = new BBox(3);
        bbox.setRange(0, -radius, +radius);
        bbox.setRange(1, -radius, +radius);
        bbox.setRange(2, -radius, +radius);
        return bbox;
    }

}
