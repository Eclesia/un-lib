
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.impl.SimilarityNd;

/**
 * Abstract oriented geometry.
 *
 * @author Johann Sorel
 */
public abstract class AbstractOrientedGeometry extends AbstractGeometry implements OrientedGeometry{

    protected final SimilarityRW transform;

    public AbstractOrientedGeometry(int dimension) {
        super(dimension);
        this.dimension = dimension;
        this.transform = SimilarityNd.create(dimension);
    }

    public SimilarityRW getTransform() {
        return transform;
    }

    public BBox getBoundingBox() {
        final BBox bbox = getUnorientedBounds();
        //TODO
        throw new RuntimeException("Not implemented yet.");
    }

}
