
package science.unlicense.geometry.impl.path;

import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public final class PathStep2D {
    public int type;
    public final double[] values;
    public final boolean largeArcFlag;
    public final boolean sweepFlag;

    public PathStep2D(int type) {
        this.type = type;
        this.values = null;
        largeArcFlag = false;
        sweepFlag = false;
    }

    public PathStep2D(int type, double x, double y) {
        this.type = type;
        this.values = new double[]{x, y};
        largeArcFlag = false;
        sweepFlag = false;
    }

    public PathStep2D(int type, double cx, double cy, double x, double y) {
        this.type = type;
        this.values = new double[]{x, y, cx, cy};
        largeArcFlag = false;
        sweepFlag = false;
    }

    public PathStep2D(int type, double cx1, double cy1, double cx2, double cy2, double x, double y) {
        this.type = type;
        this.values = new double[]{x, y, cx1, cy1, cx2, cy2};
        largeArcFlag = false;
        sweepFlag = false;
    }

    public PathStep2D(int type, double rx, double ry, double xrot, boolean largeArcFlag, boolean sweepFlag, double x, double y) {
        this.type = type;
        this.values = new double[]{x, y, rx, ry, xrot};
        this.largeArcFlag = largeArcFlag;
        this.sweepFlag = sweepFlag;
    }

    /**
     * Copy the current iterator step in this PathStep.
     * @param ite
     */
    public PathStep2D(PathIterator ite) {
        this.type = ite.getType();
        this.largeArcFlag = ite.getLargeArcFlag();
        this.sweepFlag = ite.getSweepFlag();

        final Vector3f64 c = new Vector3f64();
        if (PathIterator.TYPE_MOVE_TO == type){
            ite.getPosition(c);
            values = new double[]{c.x,c.y};
        } else if (PathIterator.TYPE_LINE_TO == type){
            ite.getPosition(c);
            values = new double[]{c.get(0),c.get(1)};
        } else if (PathIterator.TYPE_CUBIC == type){
            this.values = new double[6];
            ite.getPosition(c);
            this.values[0] = c.x;
            this.values[1] = c.y;
            ite.getFirstControl(c);
            this.values[2] = c.x;
            this.values[3] = c.y;
            ite.getSecondControl(c);
            this.values[4] = c.x;
            this.values[5] = c.y;
        } else if (PathIterator.TYPE_QUADRATIC == type){
            this.values = new double[4];
            ite.getPosition(c);
            this.values[0] = c.x;
            this.values[1] = c.y;
            ite.getFirstControl(c);
            this.values[2] = c.x;
            this.values[3] = c.y;
        } else if (PathIterator.TYPE_ARC == type){
            this.values = new double[5];
            ite.getPosition(c);
            this.values[0] = c.x;
            this.values[1] = c.y;
            ite.getArcParameters(c);
            this.values[2] = c.x;
            this.values[3] = c.y;
            this.values[4] = c.z;
        } else if (PathIterator.TYPE_CLOSE == type){
            values = null;
        } else {
            throw new RuntimeException("Unexpected step type : "+type);
        }
    }

    public int getType() {
        return type;
    }

    public TupleRW getPosition(TupleRW buffer) {
        buffer.set(0,values[0]);
        buffer.set(1,values[1]);
        return buffer;
    }

    public TupleRW getFirstControl(TupleRW buffer) {
        buffer.set(0,values[2]);
        buffer.set(1,values[3]);
        return buffer;
    }

    public TupleRW getSecondControl(TupleRW buffer) {
        buffer.set(0,values[4]);
        buffer.set(1,values[5]);
        return buffer;
    }

    public TupleRW getArcParameters(TupleRW buffer) {
        buffer.set(0,values[2]);
        buffer.set(1,values[3]);
        buffer.set(2,values[4]);
        return buffer;
    }

    public boolean getLargeArcFlag() {
        return largeArcFlag;
    }

    public boolean getSweepFlag() {
        return sweepFlag;
    }

}
