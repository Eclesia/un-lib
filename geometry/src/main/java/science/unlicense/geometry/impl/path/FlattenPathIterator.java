
package science.unlicense.geometry.impl.path;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;

/**
 * PathIterator which wraps another iterator.
 * Iteration steps are simplified to obtain only types :
 * MOVE_TO, LINE_TO, CLOSE
 *
 * Removing any curves.
 *
 * @author Johann Sorel
 */
public class FlattenPathIterator implements PathIterator{

    //when finding a quad,cubic,arc, it is decomposed in simple steps
    //each step is stored here.
    private final Sequence decomposition = new ArraySequence();

    private PathIterator iterator;
    private double[] resolution;
    private double lastStepX;
    private double lastStepY;
    private PathStep2D currentStep;

    private final Vector3f64 buffer = new Vector3f64();

    public FlattenPathIterator(PathIterator iterator, double[] resolution) {
        this.iterator = iterator;
        this.resolution = resolution;
    }

    @Override
    public int getDimension() {
        return iterator.getDimension();
    }

    public void reset(PathIterator iterator, double[] resolution){
        decomposition.removeAll();
        this.iterator = iterator;
        this.resolution = resolution;
        this.lastStepX = 0;
        this.lastStepY = 0;
    }

    @Override
    public void reset() {
        decomposition.removeAll();
        iterator.reset();
    }

    @Override
    public boolean next() {

        while (decomposition.isEmpty()){
            final boolean subnext = iterator.next();
            if (subnext == false){
                return false;
            } else {
                decomposeStep();
            }
        }
        currentStep = (PathStep2D) decomposition.get(0);
        decomposition.remove(0);
        return true;
    }

    private void decomposeStep(){
        final int stepType = iterator.getType();
        if (PathIterator.TYPE_MOVE_TO == stepType){
            //nothing to decompose
            final PathStep2D step = new PathStep2D(iterator);
            decomposition.add(step);
        } else if (PathIterator.TYPE_LINE_TO == stepType){
            //nothing to decompose
            final PathStep2D step = new PathStep2D(iterator);
            decomposition.add(step);
        } else if (PathIterator.TYPE_CLOSE == stepType){
            //nothing to decompose
            final PathStep2D step = new PathStep2D(iterator);
            decomposition.add(step);
        } else if (PathIterator.TYPE_QUADRATIC == stepType){
            //TODO, decompose based on given resolution
            //for now we consider it a straight line
            if (false){
                final PathStep2D step = new PathStep2D(iterator);
                step.type = PathIterator.TYPE_LINE_TO;
                decomposition.add(step);
            } else {

            //a quadratic can be converted in a cubic to reuse decimation
            final double tt = 2d/3d;
            final VectorRW c0 = new Vector2f64(lastStepX,lastStepY);
            final VectorRW q1 = new Vector2f64();
            final VectorRW c3 = new Vector2f64();
            iterator.getFirstControl(q1);
            iterator.getPosition(c3);
            //CP1 = QP0 + 2/3 *(QP1-QP0)
            //CP2 = QP2 + 2/3 *(QP1-QP2)
            VectorRW c1 = q1.subtract(c0, null).localScale(tt).localAdd(c0);
            VectorRW c2 = q1.subtract(c3, null).localScale(tt).localAdd(c3);
            double cx1 = c1.get(0);
            double cy1 = c1.get(1);
            double cx2 = c2.get(0);
            double cy2 = c2.get(1);
            double endx = c3.get(0);
            double endy = c3.get(1);

//            iterator.getFirstControl(buffer);
//            final double ctrlx = buffer.getX();
//            final double ctrly = buffer.getY();
//            iterator.getPosition(buffer);
//            final double endx = buffer.getX();
//            final double endy = buffer.getY();
//            final double tt = 2d/3d;
//            final double cx1 = lastStepX + (tt * (ctrlx - lastStepX));
//            final double cy1 = lastStepY + (tt * (ctrly - lastStepY));
//            final double cx2 = endx + (tt * (ctrlx - endx));
//            final double cy2 = endy + (tt * (ctrly - endy));
            decomposeCubic(
                    lastStepX, lastStepY,
                    cx1, cy1,
                    cx2, cy2,
                    endx, endy,
                    5);
            }

        } else if (PathIterator.TYPE_CUBIC == stepType){
            //decompose based on given resolution
            iterator.getFirstControl(buffer);
            final double cx1 = buffer.x;
            final double cy1 = buffer.y;
            iterator.getSecondControl(buffer);
            final double cx2 = buffer.x;
            final double cy2 = buffer.y;
            iterator.getPosition(buffer);
            final double endx = buffer.x;
            final double endy = buffer.y;
            decomposeCubic(
                    lastStepX, lastStepY,
                    cx1, cy1,
                    cx2, cy2,
                    endx, endy,
                    5);
        } else if (PathIterator.TYPE_ARC == stepType){
            //TODO, decompose based on given resolution
            //for now we consider it a straight line
            final PathStep2D step = new PathStep2D(iterator);
            step.type = PathIterator.TYPE_LINE_TO;
            decomposition.add(step);
        } else {
            throw new RuntimeException("Unexpected step type : "+stepType);
        }

        if (PathIterator.TYPE_CLOSE != stepType){
            //store coordinate for next iteration
            iterator.getPosition(buffer);
            lastStepX = buffer.x;
            lastStepY = buffer.y;
        }
    }

    @Override
    public int getType() {
        return currentStep.type;
    }

    @Override
    public TupleRW getPosition(TupleRW buffer) {
        return currentStep.getPosition(buffer);
    }

    @Override
    public TupleRW getFirstControl(TupleRW buffer) {
        return currentStep.getFirstControl(buffer);
    }

    @Override
    public TupleRW getSecondControl(TupleRW buffer) {
        return currentStep.getSecondControl(buffer);
    }

    @Override
    public TupleRW getArcParameters(TupleRW buffer) {
        return currentStep.getArcParameters(buffer);
    }

    @Override
    public boolean getLargeArcFlag() {
        return currentStep.getLargeArcFlag();
    }

    @Override
    public boolean getSweepFlag() {
        return currentStep.getSweepFlag();
    }

    /**
     * Naive decimation (Casteljau algorithm)
     * http://fr.wikipedia.org/wiki/Algorithme_de_De_Casteljau
     * TODO improve this, produce too much points.
     */
    private void decomposeCubic(double startx, double starty,double cx1, double cy1,
                                double cx2, double cy2,double endx, double endy, int maxloop) {

        //check distance
        if (maxloop==0 || (Math.abs(startx - endx) < resolution[0] && Math.abs(starty - endy) < resolution[1])) {
            final PathStep2D step = new PathStep2D(TYPE_LINE_TO, endx, endy);
            decomposition.add(step);
            return;
        }

        final double tx = (cx1+cx2)/2;
        final double ty = (cy1+cy2)/2;

        //calculate sub controls and end points
        final double leftcx1    = (startx  + cx1     )/2;
        final double leftcy1    = (starty  + cy1     )/2;
        final double rightcx2   = (cx2     + endx    )/2;
        final double rightcy2   = (cy2     + endy    )/2;
        final double leftcx2    = (leftcx1 + tx      )/2;
        final double leftcy2    = (leftcy1 + ty      )/2;
        final double rightcx1   = (tx      + rightcx2)/2;
        final double rightcy1   = (ty      + rightcy2)/2;
        final double middlex    = (leftcx2 + rightcx1)/2;
        final double middley    = (leftcy2 + rightcy1)/2;

        decomposeCubic(startx,  starty,  leftcx1,  leftcy1,  leftcx2,  leftcy2,  middlex, middley, maxloop-1);
        decomposeCubic(middlex, middley, rightcx1, rightcy1, rightcx2, rightcy2, endx,    endy,    maxloop-1);
    }

}
