
package science.unlicense.geometry.impl;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.AbstractDictionary;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.MultiGeometry;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.path.CoordinateTuplePathIterator;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Scalari32;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vectors;

/**
 * Inherited from GPU and OpengL rendering model.
 *
 * A mesh define a shape with simple primitives such as lines and triangles
 * Each geometry node is called a vertice and may be used multiple times in the index.
 * This allows very compact in memory representation.
 *
 *
 * @author Johann Sorel
 */
public interface Mesh extends MultiGeometry {

    // Name come from GLTF 2.0 specification
    // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md
    public static final Chars ATT_POSITION = Chars.constant("POSITION");
    public static final Chars ATT_NORMAL = Chars.constant("NORMAL");
    public static final Chars ATT_TANGENT = Chars.constant("TANGENT");
    public static final Chars ATT_TEXCOORD_0 = Chars.constant("TEXCOORD_0");
    public static final Chars ATT_TEXCOORD_1 = Chars.constant("TEXCOORD_1");
    public static final Chars ATT_COLOR_0 = Chars.constant("COLOR_0");
    public static final Chars ATT_JOINTS_0 = Chars.constant("JOINTS_0");
    public static final Chars ATT_WEIGHTS_0 = Chars.constant("WEIGHTS_0");

    /**
     * Get name.
     * @return Chars, can be null.
     */
    Chars getName();

    /**
     * Set name.
     * @param name , can be null.
     */
    void setName(Chars name);

    /**
     * Array of vertices containing the points in geometry coordinate system.
     *
     * Shortcut for getAttributes().getValue(ATT_POSITION)
     *
     * @return
     */
    TupleGrid getPositions();

    /**
     * Shortcut for getAttributes().getValue(ATT_NORMAL)
     *
     * @return
     */
    TupleGrid getNormals();

    /**
     * Expects orthogonalised and normalised tangents.
     * x,Y,Z = tangent
     * W = handedness
     *
     * Shortcut for getAttributes().getValue(ATT_TANGENT)
     */
    TupleGrid getTangents();

    /**
     * Shortcut for getAttributes().getValue(ATT_TEXCOORD_0)
     *
     * @return
     */
    TupleGrid getUVs();

    /**
     * Array of indices in the vertice and attachement arrays.
     * @return
     */
    TupleGrid getIndex();

    /**
     * Definition of the data types in the index.
     * @return in index range definition
     */
    IndexedRange[] getRanges();

    /**
     * Dictionnary of TupleGrid instances linked to each vertices.
     * This is commonly used for normals, tangents or color informations for each vertice.
     * The positions tupleGrid is also included in this dictionary.
     *
     * @return
     */
    Dictionary getAttributes();

    /**
     * List morph names.
     * @return Chars[] never null
     */
    Chars[] getMorphNames();

    /**
     * Get morph frame with given name.
     * @param name searched frame name
     * @return MorphFrame or null if no frame with this name exist
     */
    MorphTarget getMorph(Chars name);

    /**
     * Sequence of MorphTargets.
     *
     * @return
     */
    Sequence getMorphs();

    /**
     * Get vertex at index.
     *
     * @param index vertex index.
     * @return Vertex
     */
    Vertex getVertex(int index);

    /**
     * Calculate normals from vertices and indices.
     *
     * TODO : this handle only triangle and normals are not smooth using adjacent
     * triangles.
     */
    public static void calculateNormals(Mesh geom) {
        final TupleGridCursor icursor = createIBOCursor(geom);
        final TupleGrid vertices = geom.getPositions();
        final TupleGridCursor vcursor = vertices.cursor();

        final long size = vertices.getExtent().getL(0);
        final BufferFactory factory = DefaultBufferFactory.INSTANCE;
        final TupleGrid normals = InterleavedTupleGrid1D.create(factory.createFloat32(size * 3).getBuffer(), 3);
        final TupleGridCursor ncursor = normals.cursor();
        final VectorRW nv = Vectors.castOrWrap(ncursor.samples());
        final TupleRW v0 = vertices.createTuple();
        final TupleRW v1 = vertices.createTuple();
        final TupleRW v2 = vertices.createTuple();
        TupleRW normal = null;

        // accumulate normal vectors
        for (IndexedRange r : geom.getRanges()) {
            final int offset = r.getIndexOffset();
            for (int i=0,n=r.getCount();i<n;i+=3) {
                icursor.moveTo(i+offset);
                final int idx0 = (int) icursor.samples().get(0);
                icursor.next();
                final int idx1 = (int) icursor.samples().get(0);
                icursor.next();
                final int idx2 = (int) icursor.samples().get(0);
                vcursor.moveTo(idx0); v0.set(vcursor.samples());
                vcursor.moveTo(idx1); v1.set(vcursor.samples());
                vcursor.moveTo(idx2); v2.set(vcursor.samples());
                normal = Geometries.calculateNormal(v0, v1, v2);

                //a normal may already be set at given index, we accumulate
                //values and we will normalize them in a second loop
                ncursor.moveTo(idx0); nv.localAdd(normal);
                ncursor.moveTo(idx1); nv.localAdd(normal);
                ncursor.moveTo(idx2); nv.localAdd(normal);
            }
        }

        //normalize normals
        for (IndexedRange r : geom.getRanges()) {
            final int offset = r.getIndexOffset();
            for (int i=0,n=r.getCount();i<n;i+=3) {
                icursor.moveTo(i+offset);
                final int idx0 = (int) icursor.samples().get(0);
                icursor.next();
                final int idx1 = (int) icursor.samples().get(0);
                icursor.next();
                final int idx2 = (int) icursor.samples().get(0);
                ncursor.moveTo(idx0); nv.localNormalize();
                ncursor.moveTo(idx1); nv.localNormalize();
                ncursor.moveTo(idx2); nv.localNormalize();
            }
        }

        geom.getAttributes().add(ATT_NORMAL, normals);
    }

    /**
     * Calculate tangents from shell vertices and uvs.
     *
     * Resources :
     * http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping/
     *
     */
    public static void calculateTangents(Mesh geom) {
        final TupleGridCursor icursor = createIBOCursor(geom);
        final TupleGrid vertices = geom.getPositions();
        final TupleGrid normals = (TupleGrid) geom.getAttributes().getValue(ATT_NORMAL);
        final TupleGrid uvs = (TupleGrid) geom.getAttributes().getValue(ATT_TEXCOORD_0);
        final TupleGridCursor vcursor = vertices.cursor();
        final TupleGridCursor ncursor = normals.cursor();
        final TupleGridCursor uvcursor = uvs.cursor();

        //we store tangents + handedness
        final long size = vertices.getExtent().getL(0);
        final BufferFactory factory = DefaultBufferFactory.INSTANCE;
        final Buffer buffert = factory.createFloat32(size*4).getBuffer();
        final Buffer bufferbt = factory.createFloat32(size*3).getBuffer();
        final TupleGrid tangents = InterleavedTupleGrid1D.create(buffert, 4);
        final TupleGrid bitangents = InterleavedTupleGrid1D.create(bufferbt, 3);
        final TupleGridCursor tcursor = tangents.cursor();
        final VectorRW tv = Vectors.castOrWrap(tcursor.samples());
        final TupleGridCursor bcursor = bitangents.cursor();
        final VectorRW bv = Vectors.castOrWrap(bcursor.samples());

        final VectorRW v0 = Vectors.castOrWrap(vertices.createTuple());
        final VectorRW v1 = Vectors.castOrWrap(vertices.createTuple());
        final VectorRW v2 = Vectors.castOrWrap(vertices.createTuple());
        final VectorRW uv0 = Vectors.castOrWrap(uvs.createTuple());
        final VectorRW uv1 = Vectors.castOrWrap(uvs.createTuple());
        final VectorRW uv2 = Vectors.castOrWrap(uvs.createTuple());

        final Vector3f64 deltaPos1 = new Vector3f64();
        final Vector3f64 deltaPos2 = new Vector3f64();
        final Vector2f64 deltaUV1 = new Vector2f64();
        final Vector2f64 deltaUV2 = new Vector2f64();
        final Vector3f64 tangent = new Vector3f64();
        final Vector3f64 bitangent = new Vector3f64();

        // accumulate tangents vectors
        for (IndexedRange range : geom.getRanges()) {
            final int offset = range.getIndexOffset();
            for (int i=0,n=range.getCount();i<n;i+=3) {
                icursor.moveTo(i+offset);
                final int idx0 = (int) icursor.samples().get(0);
                icursor.next();
                final int idx1 = (int) icursor.samples().get(0);
                icursor.next();
                final int idx2 = (int) icursor.samples().get(0);
                vcursor.moveTo(idx0); v0.set(vcursor.samples());
                vcursor.moveTo(idx1); v1.set(vcursor.samples());
                vcursor.moveTo(idx2); v2.set(vcursor.samples());
                uvcursor.moveTo(idx0); uv0.set(uvcursor.samples());
                uvcursor.moveTo(idx1); uv1.set(uvcursor.samples());
                uvcursor.moveTo(idx2); uv2.set(uvcursor.samples());

                // Edges of the triangle : postion delta
                v1.subtract(v0, deltaPos1);
                v2.subtract(v0, deltaPos2);
                // UV delta
                uv1.subtract(uv0, deltaUV1);
                uv2.subtract(uv0, deltaUV2);

                double ra = 1.0 / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
                deltaPos1.scale(deltaUV2.y).subtract(deltaPos2.scale(deltaUV1.y)).scale(ra, tangent);

                //bitangent
                deltaPos2.scale(deltaUV1.x).subtract(deltaPos1.scale(deltaUV2.x)).scale(ra, bitangent);

                VectorRW t4 = tangent.extend(0);

                //a tangent may already be set at given index, we accumulate
                //values and we will normalize them in a second loop
                tcursor.moveTo(idx0); tv.localAdd(t4);
                tcursor.moveTo(idx1); tv.localAdd(t4);
                tcursor.moveTo(idx2); tv.localAdd(t4);
                bcursor.moveTo(idx0); bv.localAdd(bitangent);
                bcursor.moveTo(idx1); bv.localAdd(bitangent);
                bcursor.moveTo(idx2); bv.localAdd(bitangent);
            }
        }

        //normalize tangents + handedness
        final VectorRW normal = Vectors.castOrWrap(normals.createTuple());

        VectorRW t4 = tangent.extend(0);
        final Scalari32 s = new Scalari32();
        for (IndexedRange r : geom.getRanges()) {
            final int offset = r.getIndexOffset();
            for (int i=0,n=r.getCount();i<n;i++) {
                icursor.moveTo(i+offset);
                s.x = (int) icursor.samples().get(0);
                ncursor.moveTo(s.x); normal.set(ncursor.samples());
                tangents.getTuple(s, t4);
                bitangents.getTuple(s, bitangent);

                tangent.set(t4);

                // Gram-Schmidt orthogonalize
                final VectorRW t = tangent.subtract(normal.scale(normal.dot(tangent)) ).normalize();
                // Calculate handedness
                double d = normal.cross(tangent).dot(bitangent);

                final double handedness = (d < 0f) ? -1 : +1;

                tangents.setTuple(s, t.extend(handedness));
            }
        }

        geom.getAttributes().add(ATT_TANGENT, tangents);
    }

    static TupleGridCursor createIBOCursor(Mesh geom) {
        return geom.getIndex().cursor();
    }

    public static class Line implements science.unlicense.geometry.api.Line {

        public final Mesh parent;
        public final int[] indices;

        public Line(Mesh geometry, int[] array) {
            this.parent = geometry;
            this.indices = array;
        }

        @Override
        public TupleRW getStart() {
            return parent.getVertex(indices[0]).getCoordinate();
        }

        @Override
        public TupleRW getEnd() {
            return parent.getVertex(indices[1]).getCoordinate();
        }

        @Override
        public CoordinateSystem getCoordinateSystem() {
            return parent.getCoordinateSystem();
        }

        @Override
        public void setCoordinateSystem(CoordinateSystem cs) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public int getDimension() {
            return parent.getDimension();
        }

        @Override
        public BBox getBoundingBox() {
            final TupleRW first = getStart();
            final BBox bbox = new BBox(first.getSampleCount());
            bbox.getLower().set(first);
            bbox.getUpper().set(first);
            bbox.expand(getEnd());
            return bbox;
        }

        @Override
        public Dictionary getUserProperties() {
            return Collections.emptyDictionary();
        }

        @Override
        public PathIterator createPathIterator() {
            return new CoordinateTuplePathIterator(new Tuple[]{getStart(),getEnd()});
        }
    }

    public static class Triangle implements science.unlicense.geometry.api.Triangle {

        public final Mesh parent;
        public final int[] indices;

        public Triangle(Mesh geometry, int[] array) {
            this.parent = geometry;
            this.indices = array;
        }

        @Override
        public TupleRW getFirstCoord() {
            return parent.getVertex(indices[0]).getCoordinate();
        }

        @Override
        public TupleRW getSecondCoord() {
            return parent.getVertex(indices[1]).getCoordinate();
        }

        @Override
        public TupleRW getThirdCoord() {
            return parent.getVertex(indices[2]).getCoordinate();
        }

        @Override
        public CoordinateSystem getCoordinateSystem() {
            return parent.getCoordinateSystem();
        }

        @Override
        public void setCoordinateSystem(CoordinateSystem cs) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public int getDimension() {
            return parent.getDimension();
        }

        @Override
        public BBox getBoundingBox() {
            final TupleRW first = getFirstCoord();
            final BBox bbox = new BBox(first.getSampleCount());
            bbox.getLower().set(first);
            bbox.getUpper().set(first);
            bbox.expand(getSecondCoord());
            bbox.expand(getThirdCoord());
            return bbox;
        }

        @Override
        public Dictionary getUserProperties() {
            return Collections.emptyDictionary();
        }
    }

    public static class InterpolatedFragment extends AbstractDictionary implements Fragment {

        public final double[] weights;
        public final Vertex[] vertices;

        public InterpolatedFragment(Vertex[] vertices, double[] weights) {
            this.vertices = vertices;
            this.weights = weights;
        }

        @Override
        public TupleRW getCoordinate() {
            final Tuple p0 = vertices[0].getCoordinate();
            if (p0 == null) return null;
            final Tuple p1 = vertices[1].getCoordinate();

            switch (vertices.length) {
                case 2 : {
                    return interpolate2(p0, p1);
                }
                case 3 : {
                    final Tuple p2 = vertices[2].getCoordinate();
                    return interpolate3(p0, p1, p2);
                }
                default :
                    throw new RuntimeException("Not supported");
            }
        }

        @Override
        public Dictionary properties() {
            return this;
        }

        @Override
        public int getSize() {
            return vertices[0].properties().getSize();
        }

        @Override
        public Set getKeys() {
            return vertices[0].properties().getKeys();
        }

        @Override
        public Object getValue(Object key) {
            final Tuple p0 = (Tuple) vertices[0].properties().getValue(key);
            if (p0 == null) return null;
            final Tuple p1 = (Tuple) vertices[1].properties().getValue(key);

            switch (vertices.length) {
                case 2 : {
                    return interpolate2(p0, p1);
                }
                case 3 : {
                    final Tuple p2 = (Tuple) vertices[2].properties().getValue(key);
                    return interpolate3(p0, p1, p2);
                }
                default :
                    throw new RuntimeException("Not supported");
            }
        }

        private TupleRW interpolate2(Tuple t0, Tuple t1) {
            final int dim = t0.getSampleCount();

            switch (dim) {
                case 2 : {
                    return new Vector2f64(
                        t0.get(0) * weights[0]
                      + t1.get(0) * weights[1],
                        t0.get(1) * weights[0]
                      + t1.get(1) * weights[1]
                      );
                }
                case 3 : {
                    return new Vector3f64(
                        t0.get(0) * weights[0]
                      + t1.get(0) * weights[1],
                        t0.get(1) * weights[0]
                      + t1.get(1) * weights[1],
                        t0.get(2) * weights[0]
                      + t1.get(2) * weights[1]
                      );
                }
                default :
                    throw new RuntimeException("Not supported");
            }
        }

        private TupleRW interpolate3(Tuple t0, Tuple t1, Tuple t2) {
            final int dim = t0.getSampleCount();

            switch (dim) {
                case 2 : {
                    return new Vector2f64(
                        t0.get(0) * weights[0]
                      + t1.get(0) * weights[1]
                      + t2.get(0) * weights[2],
                        t0.get(1) * weights[0]
                      + t1.get(1) * weights[1]
                      + t2.get(1) * weights[2]
                      );
                }
                case 3 : {
                    return new Vector3f64(
                        t0.get(0) * weights[0]
                      + t1.get(0) * weights[1]
                      + t2.get(0) * weights[2],
                        t0.get(1) * weights[0]
                      + t1.get(1) * weights[1]
                      + t2.get(1) * weights[2],
                        t0.get(2) * weights[0]
                      + t1.get(2) * weights[1]
                      + t2.get(2) * weights[2]
                      );
                }
                default :
                    throw new RuntimeException("Not supported");
            }
        }

        @Override
        public Collection getValues() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Collection getPairs() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public void add(Object key, Object value) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Object remove(Object key) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public void removeAll() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public PathIterator createPathIterator() {
            return new CoordinateTuplePathIterator(new Tuple[]{getCoordinate()});
        }

        @Override
        public CoordinateSystem getCoordinateSystem() {
            return vertices[0].getCoordinateSystem();
        }

        @Override
        public void setCoordinateSystem(CoordinateSystem cs) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public int getDimension() {
            return getCoordinateSystem().getDimension();
        }

        @Override
        public BBox getBoundingBox() {
            final TupleRW pos = getCoordinate();
            final BBox bbox = new BBox(getCoordinateSystem());
            bbox.getLower().set(pos);
            bbox.getUpper().set(pos);
            return bbox;
        }

        @Override
        public Dictionary getUserProperties() {
            return Collections.emptyDictionary();
        }

    }


}
