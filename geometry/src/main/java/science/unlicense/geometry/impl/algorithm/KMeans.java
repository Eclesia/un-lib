
package science.unlicense.geometry.impl.algorithm;

import science.unlicense.common.api.Arrays;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Vectors;

/**
 * K-means clustering.
 *
 * resources :
 * https://en.wikipedia.org/wiki/K-means_clustering
 *
 * @author Johann Sorel (Adapted to unlicense-lib)
 */
public final class KMeans {

    private KMeans(){}

    /**
     *
     * @param tuples
     * @param nbCluster
     * @param nbCycle
     * @return
     */
    public static double[][] kmeans(TupleGrid tuples, int nbCluster, int nbCycle){

        final double[][] means = randomMeans(tuples, nbCluster);
        final long[] count = new long[nbCluster];
        final double[][] cumul = new double[nbCluster][tuples.getSampleSystem().getNumComponents()];

        double[] tuple = null;
        int closestMean;
        double dist, d;


        for (int i=0;i<nbCycle;i++){
            //reset counters
            Arrays.fill(count, 0);
            for (int k=0;k<cumul.length;k++) Arrays.fill(cumul[k], 0);

            //iterate on tuples
            final TupleGridCursor cursor = tuples.cursor();
            while (cursor.next()) {
                cursor.samples().toDouble(tuple, 0);
                closestMean = 0;
                dist = DistanceOp.distance(means[0], tuple);
                for (int m=1;m<means.length;m++){
                    d = DistanceOp.distance(means[m], tuple);
                    if (d<dist){
                        dist = d;
                        closestMean = m;
                    }
                }

                count[closestMean]++;
                Vectors.add(cumul[closestMean], tuple, cumul[closestMean]);
            }

            //calculate new means
            for (int m=0;m<means.length;m++){
                Vectors.scale(cumul[m], count[m], means[m]);
            }
        }

        return means;
    }

    /**
     * Generating random means.
     */
    private static double[][] randomMeans(TupleGrid tuples, int nbClusters){

        //find min and max values
        final double[] tuple = new double[tuples.getSampleSystem().getNumComponents()];
        final TupleGridCursor cursor = tuples.cursor();
        cursor.next();
        cursor.samples().toDouble(tuple,0);
        final BBox bbox = new BBox(tuples.getSampleSystem().getNumComponents());
        bbox.getLower().set(tuple);
        bbox.getUpper().set(tuple);
        bbox.expand(tuples);

        //generate random means inside bbox extent
        final double[] min = bbox.getLower().toDouble();
        final double[] max = bbox.getUpper().toDouble();
        final double[][] means = new double[nbClusters][tuple.length];
        for (int i=0;i<nbClusters;i++){
            for (int k=0;k<tuple.length;k++){
                means[i][k] = Maths.random(min[k], max[k]);
            }
        }

        return means;
    }

}
