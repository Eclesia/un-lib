
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.system.CompoundCoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystem;

/**
 * An extruded geometry expand a geometry by one dimension.
 * The new geometry coordinate system whil be composed of the original geometry
 * CS and the extrusion CS.
 *
 * @author Johann Sorel
 */
public abstract class ExtrudedGeometry extends AbstractGeometry {

    private Geometry base;
    private double minE;
    private double maxE;

    private ExtrudedGeometry(int dimension) {
        super(dimension);
    }

    private ExtrudedGeometry(Geometry geom, CoordinateSystem extrusionCS, double minE, double maxE) {
        super(new CompoundCoordinateSystem(geom.getCoordinateSystem(), extrusionCS));
        this.base = geom;
        this.minE = minE;
        this.maxE = maxE;
    }

    public Geometry getBase() {
        return base;
    }

    public void setBase(Geometry base) {
        this.base = base;
    }

    public double getMinE() {
        return minE;
    }

    public void setMinE(double minE) {
        this.minE = minE;
    }

    public double getMaxE() {
        return maxE;
    }

    public void setMaxE(double maxE) {
        this.maxE = maxE;
    }

    @Override
    public BBox getBoundingBox() {
        final BBox basebbox = base.getBoundingBox();
        final BBox exbbox = new BBox(dimension);
        for (int i=0;i<dimension-1;i++) {
            exbbox.setRange(i, basebbox.getMin(i), basebbox.getMax(i));
        }
        exbbox.setRange(dimension-1, minE, maxE);
        return exbbox;
    }

    public static TND create(Geometry geom, CoordinateSystem extrusionCS, double minE, double maxE) {
        return new TND(geom, extrusionCS, minE, maxE);
    }

    public static class TND extends ExtrudedGeometry {

        public TND(Geometry geom, CoordinateSystem extrusionCS, double minE, double maxE) {
            super(geom, extrusionCS, minE, maxE);
        }

    }
}
