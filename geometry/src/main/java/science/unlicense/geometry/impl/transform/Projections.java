
package science.unlicense.geometry.impl.transform;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.AffineN;
import science.unlicense.math.impl.Vectors;

/**
 * Methods to create transforms for common transformation cases.
 *
 * @author Johann Sorel
 */
public final class Projections {

    private Projections(){}

    /**
     * Create an Affine that transform the given bbox to be centerd into target bbox.
     *
     * @param source
     * @param target
     * @return
     */
    public static AffineRW centered(BBox source, BBox target){
        final Tuple sourceCenter = source.getMiddle();
        final Tuple targetCenter = target.getMiddle();
        final double[] trs = Vectors.subtract(targetCenter.toDouble(), sourceCenter.toDouble());
        final AffineRW m = AffineN.create(trs.length).setToIdentity();
        m.setCol(trs.length, trs);
        return m;
    }

    /**
     * Create an Affine that transform the given bbox to be centerd into target bbox.
     * The source bbox is scaled with given scale.
     *
     * @param source
     * @param target
     * @param scale wanted scale.
     * @return
     */
    public static AffineRW centeredScaled(BBox source, BBox target, double scale){

        final double[] sourceCenter = source.getMiddle().toDouble();
        final double[] targetCenter = target.getMiddle().toDouble();
        Vectors.scale(sourceCenter, scale, sourceCenter);
        final double[] trs = Vectors.subtract(targetCenter, sourceCenter);

        final AffineRW mt = AffineN.create(trs.length).setToIdentity();
        for (int i=0;i<trs.length;i++){
            mt.set(i, i, scale);
            mt.set(i, trs.length, trs[i]);
        }

        return mt;
    }

    /**
     * Create an Affine that transform the given bbox to fit into target bbox.
     * Dimensions ratio are preserved and will be centered in target bbox.
     *
     * @param source
     * @param target
     * @return
     */
    public static AffineRW scaled(BBox source, BBox target){
        //find min scale
        final int dim =source.getCoordinateSystem().getDimension();
        double scale = target.getSpan(0) / source.getSpan(0);
        for (int i=1;i<dim;i++){
            scale = Maths.min( target.getSpan(i) / source.getSpan(i), scale);
        }
        return centeredScaled(source, target, scale);
    }

    /**
     * Create an Affine that transform the given bbox to fill the all space into target bbox.
     * Dimensions ratio are not preserved.
     *
     * @param source
     * @param target
     * @return
     */
    public static AffineRW stretched(BBox source, BBox target){
        final int dim = source.getCoordinateSystem().getDimension();
        final AffineRW m = AffineN.create(dim).setToIdentity();

        for (int i=0;i<dim;i++){
            final double scale = target.getSpan(i) / source.getSpan(i);
            final double trs = target.getMin(i) - source.getMin(i) * scale;
            m.set(i, i, scale);
            m.set(i, dim, trs);
        }

        return m;
    }

    /**
     * Create an Affine that transform the given bbox to fill the all space into target bbox.
     * The source bbox is scaled preserving ratio and centering source bbox.
     *
     * @param source
     * @param target
     * @return
     */
    public static AffineRW zoomed(BBox source, BBox target){
        //find max scale
        final int dim =source.getCoordinateSystem().getDimension();
        double scale = target.getSpan(0) / source.getSpan(0);
        for (int i=1;i<dim;i++){
            scale = Maths.max( target.getSpan(i) / source.getSpan(i), scale);
        }
        return centeredScaled(source, target, scale);
    }

    /**
     * Create an Affine that transform the given bbox to fit space into target bbox on one
     * specified dimension.
     * The source bbox is scaled preserving ratio and will be centered on other axis.
     *
     * @param source
     * @param target
     * @param axis index to fit
     * @return
     */
    public static AffineRW fit(BBox source, BBox target, int axis){
        final double scale = target.getSpan(axis) / source.getSpan(axis);
        return centeredScaled(source, target, scale);
    }

    /**
     * Create an Affine that transform the given bbox to align it on each axis.
     * The source bbox is scaled preserving ratio.
     *
     * @param source
     * @param target
     * @param ratios at 0 the box will be align to the minimum of the target box
     *               and at 1 it will be align to the minimum of the target box
     * @return
     */
    public static AffineRW align(BBox source, BBox target, double[] ratios){
        final int dim = source.getCoordinateSystem().getDimension();
        final AffineRW affine = AffineN.create(dim);
        affine.setToIdentity();
        for (int i=0;i<dim;i++) {
            final double sourceSpan = source.getSpan(i);
            final double targetSpan = target.getSpan(i);
            final double sourceMin = source.getMin(i);
            final double targetMin = target.getMin(i);
            final double diff = targetSpan-sourceSpan;
            affine.set(i, dim, target.getMin(i) - sourceMin + (ratios[i]*diff) );
        }
        return affine;
    }

}
