
package science.unlicense.geometry.impl.operation;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.DoubleSequence;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.TriangulateOp;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.impl.ArrayMultiGeometry;
import science.unlicense.geometry.impl.ArrayTriangleStrip;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Cone;
import science.unlicense.geometry.impl.Cylinder;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Disk;
import science.unlicense.geometry.impl.Ellipsoid;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.Quad;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.geometry.impl.triangulate.EarClipping;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class TriangulateExecutors {

    private TriangulateExecutors(){}

    public static final AbstractSingleOperationExecutor PLANAR =
            new AbstractSingleOperationExecutor(TriangulateOp.class, PlanarGeometry.class){

        public Object execute(Operation operation) throws OperationException {
            final TriangulateOp op = (TriangulateOp) operation;
            final PlanarGeometry p = (PlanarGeometry) op.getGeometry();
            final double res = op.getResolution();

            final EarClipping clipping = new EarClipping();
            Sequence triangles = clipping.triangulate(p);
            return new ArrayMultiGeometry(triangles);
        }
    };

    public static final AbstractSingleOperationExecutor BBOX =
            new AbstractSingleOperationExecutor(TriangulateOp.class, BBox.class){

        public Object execute(Operation operation) throws OperationException {
            final TriangulateOp op = (TriangulateOp) operation;
            final BBox geom = (BBox) op.getGeometry();

            final TupleRW lower = geom.getLower();
            final TupleRW upper = geom.getUpper();

            final int dim = lower.getSampleCount();

            if (dim == 2) {
                /*
                +--+
                |\ |
                | \|
                +--+
                */
                double minx = lower.get(0);
                double miny = lower.get(1);
                double maxx = upper.get(0);
                double maxy = upper.get(1);
                final double[] values = new double[]{
                    minx,miny,
                    maxx,miny,
                    minx,maxy,
                    maxx,maxy
                };

                TupleGrid1D tg = new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(values), Float64.TYPE, geom.getCoordinateSystem(), 4);
                return new ArrayTriangleStrip(tg);
            } else if (dim == 3) {
                double minx = lower.get(0);
                double miny = lower.get(1);
                double minz = lower.get(2);
                double maxx = upper.get(0);
                double maxy = upper.get(1);
                double maxz = upper.get(2);

                final double[][] vertices = new double[8][3];
                vertices[0] = new double[]{minx, miny, minz};
                vertices[1] = new double[]{maxx, miny, minz};
                vertices[2] = new double[]{maxx, maxy, minz};
                vertices[3] = new double[]{minx, maxy, minz};
                vertices[4] = new double[]{maxx, miny, maxz};
                vertices[5] = new double[]{minx, miny, maxz};
                vertices[6] = new double[]{maxx, maxy, maxz};
                vertices[7] = new double[]{minx, maxy, maxz};

                final DoubleSequence vertexCursor = new DoubleSequence();
                //back
                vertexCursor.put(vertices[0]);
                vertexCursor.put(vertices[1]);
                vertexCursor.put(vertices[2]);
                vertexCursor.put(vertices[3]);

                //front
                vertexCursor.put(vertices[4]);
                vertexCursor.put(vertices[5]);
                vertexCursor.put(vertices[7]);
                vertexCursor.put(vertices[6]);

                //rigth
                vertexCursor.put(vertices[1]);
                vertexCursor.put(vertices[4]);
                vertexCursor.put(vertices[6]);
                vertexCursor.put(vertices[2]);

                //left
                vertexCursor.put(vertices[5]);
                vertexCursor.put(vertices[0]);
                vertexCursor.put(vertices[3]);
                vertexCursor.put(vertices[7]);

                //up
                vertexCursor.put(vertices[2]);
                vertexCursor.put(vertices[6]);
                vertexCursor.put(vertices[7]);
                vertexCursor.put(vertices[3]);

                //down
                vertexCursor.put(vertices[0]);
                vertexCursor.put(vertices[5]);
                vertexCursor.put(vertices[4]);
                vertexCursor.put(vertices[1]);

                final int[] indices = {
                    2, 1, 0, //back
                    3, 2, 0,
                    6, 5, 4, //front
                    7, 6, 4,
                    10, 9, 8, //sides
                    11, 10, 8,
                    14, 13, 12,
                    15, 14, 12,
                    18, 17, 16,
                    19, 18, 16,
                    22, 21, 20,
                    23, 22, 20 };

                final TupleGrid1D index = new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(indices),
                        Int32.TYPE, UndefinedSystem.create(1), 36);
                final IndexedRange range = IndexedRange.TRIANGLES(0, 36);

                final TupleGrid1D verts = new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(vertexCursor.toArrayDouble()),
                        Float64.TYPE, geom.getCoordinateSystem(), 24);

                final DefaultMesh idxGeom = new DefaultMesh(geom.getCoordinateSystem());
                idxGeom.setIndex(index);
                idxGeom.setRanges(new IndexedRange[]{range});
                idxGeom.setPositions(verts);
                return idxGeom;
            } else {
                throw new OperationException("Only 2D and 3D BBOX supported.");
            }
        }
    };

    public static final AbstractSingleOperationExecutor DISK =
            new AbstractSingleOperationExecutor(TriangulateOp.class, Disk.class){

        public Object execute(Operation operation) throws OperationException {
            final TriangulateOp op = (TriangulateOp) operation;
            final Disk geom = (Disk) op.getGeometry();
            final double res = op.getResolution();
            final int nbRing = op.getNbRing();
            final int nbSector = op.getNbSector();
            final CoordinateSystem cs = geom.getCoordinateSystem();
            final SampleSystem ss = UndefinedSystem.create(3);

            final float hAngle = 1f / (nbSector);
            final float radius = (float) geom.getRadius();

            //build a triangle fan
            final int nbVertice = (2+nbSector)*3;
            final Buffer.Float32 bufferVertices = DefaultBufferFactory.INSTANCE.createFloat32(nbVertice);
            final Buffer.Float32 bufferNormals = DefaultBufferFactory.INSTANCE.createFloat32(nbVertice);
            final Buffer.Int32 bufferIndices = DefaultBufferFactory.INSTANCE.createInt32(2+nbSector);

            int index = 0;

            final Float32Cursor vertexCursor = bufferVertices.cursor();
            final Float32Cursor normalCursor = bufferNormals.cursor();
            final Int32Cursor indiceCursor = bufferIndices.cursor();

            //center point
            vertexCursor.write(0).write(0).write(0);
            indiceCursor.write(index++);

            for (int s=0; s<nbSector+1; s++) {
                final double half = Math.PI /2;
                final double leftha = 2*Math.PI * s * hAngle;

                //first point : top left
                final float[] tl = point(half,leftha,radius, 0);
                vertexCursor.write(tl);
                normalCursor.write(tl);
                indiceCursor.write(index++);
            }

            final DefaultMesh idxGeom = new DefaultMesh(cs);
            idxGeom.getAttributes().add(Mesh.ATT_POSITION, new InterleavedTupleGrid1D(bufferVertices.getBuffer(), Float32.TYPE, ss, nbVertice/3));
            idxGeom.getAttributes().add(Mesh.ATT_NORMAL, new InterleavedTupleGrid1D(bufferNormals.getBuffer(), Float32.TYPE, ss, nbVertice/3));
            idxGeom.setIndex(new InterleavedTupleGrid1D(bufferIndices.getBuffer(), Int32.TYPE, UndefinedSystem.create(1), bufferIndices.getSize()));
            idxGeom.setRanges(new IndexedRange[]{IndexedRange.TRIANGLE_FAN(0, (int) bufferIndices.getSize())});
            return idxGeom;
        }
    };

    public static final AbstractSingleOperationExecutor SPHERE =
            new AbstractSingleOperationExecutor(TriangulateOp.class, Sphere.class){

        public Object execute(Operation operation) throws OperationException {
            final TriangulateOp op = (TriangulateOp) operation;
            final Sphere geom = (Sphere) op.getGeometry();
            final double res = op.getResolution();
            final int nbRing = op.getNbRing();
            final int nbSector = op.getNbSector();
            final CoordinateSystem cs = geom.getCoordinateSystem();
            final SampleSystem ss = UndefinedSystem.create(3);
            final float radius = (float) geom.getRadius();
            final float vAngle = 1f / (nbRing-1);
            final float hAngle = 1f / (nbSector-1);

            final Buffer.Float32 bufferVertices = DefaultBufferFactory.INSTANCE.createFloat32(nbRing*nbSector*3);
            final Buffer.Float32 bufferNormals = DefaultBufferFactory.INSTANCE.createFloat32(nbRing*nbSector*3);
            final Buffer.Int32 bufferIndices = DefaultBufferFactory.INSTANCE.createInt32((nbRing-1)*(nbSector-1)*6);

            final Float32Cursor vertexCursor = bufferVertices.cursor();
            final Float32Cursor normalCursor = bufferNormals.cursor();
            final Int32Cursor indiceCursor = bufferIndices.cursor();

            final double[] center = geom.getCenter().toDouble();
            for (int v = 0; v < nbRing; v++) {
                final double va = Math.PI * v * vAngle;
                for (int h = 0; h < nbSector; h++) {
                    final double ha = 2*Math.PI * h * hAngle;
                    final float y = (float) Math.sin(- Math.PI/2d + va);
                    final float x = (float) (Math.cos(ha) * Math.sin(va));
                    final float z = (float) (Math.sin(ha) * Math.sin(va));
                    vertexCursor.write(x*radius+(float) center[0]);
                    vertexCursor.write(y*radius+(float) center[1]);
                    vertexCursor.write(z*radius+(float) center[2]);
                    normalCursor.write(x);
                    normalCursor.write(y);
                    normalCursor.write(z);

                    if (v!=nbRing-1 && h!=nbSector-1) {
                        //last ring, we don't create triangles
                        //first triangle
                        indiceCursor.write( v * nbSector + h );
                        indiceCursor.write( (v+1) * nbSector + h );
                        indiceCursor.write( v * nbSector + (h+1) );
                        //second triangle
                        indiceCursor.write( v * nbSector + (h+1) );
                        indiceCursor.write( (v+1) * nbSector + h );
                        indiceCursor.write( (v+1) * nbSector + (h+1) );
                    }
                }
            }

            final DefaultMesh idxGeom = new DefaultMesh(cs);
            idxGeom.getAttributes().add(Mesh.ATT_POSITION, new InterleavedTupleGrid1D(bufferVertices.getBuffer(), Float32.TYPE, ss, nbRing*nbSector));
            idxGeom.getAttributes().add(Mesh.ATT_NORMAL, new InterleavedTupleGrid1D(bufferNormals.getBuffer(), Float32.TYPE, ss, nbRing*nbSector));
            idxGeom.setIndex(new InterleavedTupleGrid1D(bufferIndices.getBuffer(), Int32.TYPE, UndefinedSystem.create(1), bufferIndices.getSize()));
            idxGeom.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) bufferIndices.getSize())});
            return idxGeom;
        }
    };

    public static final AbstractSingleOperationExecutor CAPSULE =
            new AbstractSingleOperationExecutor(TriangulateOp.class, Capsule.class){

        public Object execute(Operation operation) throws OperationException {
            final TriangulateOp op = (TriangulateOp) operation;
            final Capsule capsule = (Capsule) op.getGeometry();
            final double res = op.getResolution();
            final int nbRing = op.getNbRing();
            final int nbSector = op.getNbSector();
            final CoordinateSystem cs = capsule.getCoordinateSystem();
            final SampleSystem ss = UndefinedSystem.create(3);

            final float height = (float) capsule.getHeight();
            final float radius = (float) capsule.getRadius();
            final float upper = height/2;
            final float lower = -height/2;
            final float vAngle = 1f / (nbRing);
            final float hAngle = 1f / (nbSector);

            final int nbVertice = nbRing*nbSector*3*4
                                + nbSector*3*4
                                + nbRing*nbSector*3*4;
            final Buffer.Float32 bufferVertices = DefaultBufferFactory.INSTANCE.createFloat32(nbVertice);
            final Buffer.Float32 bufferNormals = DefaultBufferFactory.INSTANCE.createFloat32(nbVertice);
            final Buffer.Int32 bufferIndices = DefaultBufferFactory.INSTANCE.createInt32(
                    nbRing*nbSector*6 //top half-sphere
                   +nbSector*6 //cylinder
                   +nbRing*nbSector*6 //bottom half sphere
            );

            final Float32Cursor vertexCursor = bufferVertices.cursor();
            final Float32Cursor normalCursor = bufferNormals.cursor();
            final Int32Cursor indiceCursor = bufferIndices.cursor();

            //TODO we duplicate many points here

            int index = 0;
            final float[] tl = new float[3];
            final float[] tr = new float[3];
            final float[] bl = new float[3];
            final float[] br = new float[3];
            final float[] tln = new float[3];
            final float[] trn = new float[3];
            final float[] bln = new float[3];
            final float[] brn = new float[3];

            //TOP half-sphere
            for (int v = 0; v < nbRing; v++) {
                final double topva = Math.PI/2 * v * vAngle;
                final double bottomva = Math.PI/2 * (v+1) * vAngle;

                for (int s = 0; s < nbSector; s++) {
                    final double leftha = 2*Math.PI * s * hAngle;
                    final double righttha = 2*Math.PI * (s+1) * hAngle;

                    point(topva,leftha,radius, upper, tl, tln);
                    point(topva,righttha,radius, upper, tr, trn);
                    point(bottomva,leftha,radius, upper, bl, bln);
                    point(bottomva,righttha,radius, upper, br, brn);

                    vertexCursor.write(tl).write(tr).write(bl).write(br);
                    normalCursor.write(tln).write(trn).write(bln).write(brn);

                    int itl = index++;
                    int itr = index++;
                    int ibl = index++;
                    int ibr = index++;

                    indiceCursor.write(itl).write(itr).write(ibl);
                    indiceCursor.write(itr).write(ibr).write(ibl);
                }
            }

            //Cylinder
            for (int s = 0; s < nbSector; s++) {
                final double half = Math.PI /2;
                final double leftha = 2*Math.PI * s * hAngle;
                final double righttha = 2*Math.PI * (s+1) * hAngle;

                point(half,leftha,radius, upper, tl, tln);
                point(half,righttha,radius, upper, tr, trn);
                point(half,leftha,radius, lower, bl, bln);
                point(half,righttha,radius, lower, br, brn);

                vertexCursor.write(tl).write(tr).write(bl).write(br);
                normalCursor.write(tln).write(trn).write(bln).write(brn);

                int itl = index++;
                int itr = index++;
                int ibl = index++;
                int ibr = index++;

                indiceCursor.write(itl).write(itr).write(ibl);
                indiceCursor.write(itr).write(ibr).write(ibl);
            }

            //Bottom half-sphere
            for (int v = 0; v < nbRing; v++) {
                double topva = Math.PI/2 * v * vAngle + Math.PI/2;
                double bottomva = Math.PI/2 * (v+1) * vAngle + Math.PI/2;

                for (int s = 0; s < nbSector; s++) {
                    final double leftha = 2*Math.PI * s * hAngle;
                    final double righttha = 2*Math.PI * (s+1) * hAngle;

                    point(topva,leftha,radius, lower, tl, tln);
                    point(topva,righttha,radius, lower, tr, trn);
                    point(bottomva,leftha,radius, lower, bl, bln);
                    point(bottomva,righttha,radius, lower, br, brn);

                    vertexCursor.write(tl).write(tr).write(bl).write(br);
                    normalCursor.write(tln).write(trn).write(bln).write(brn);

                    int itl = index++;
                    int itr = index++;
                    int ibl = index++;
                    int ibr = index++;

                    indiceCursor.write(itl).write(itr).write(ibl);
                    indiceCursor.write(itr).write(ibr).write(ibl);
                }
            }

            final DefaultMesh idxGeom = new DefaultMesh(cs);
            idxGeom.getAttributes().add(Mesh.ATT_POSITION, new InterleavedTupleGrid1D(bufferVertices.getBuffer(), Float32.TYPE, ss, nbVertice/3));
            idxGeom.getAttributes().add(Mesh.ATT_NORMAL, new InterleavedTupleGrid1D(bufferNormals.getBuffer(), Float32.TYPE, ss, nbVertice/3));
            idxGeom.setIndex(new InterleavedTupleGrid1D(bufferIndices.getBuffer(), Int32.TYPE, UndefinedSystem.create(1), bufferIndices.getSize()));
            idxGeom.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) bufferIndices.getSize())});
            return idxGeom;
        }
    };

    public static final AbstractSingleOperationExecutor ELLIPSOID =
            new AbstractSingleOperationExecutor(TriangulateOp.class, Ellipsoid.class){

        public Object execute(Operation operation) throws OperationException {
            final TriangulateOp op = (TriangulateOp) operation;
            final Ellipsoid ellipsoid = (Ellipsoid) op.getGeometry();
            final double res = op.getResolution();
            final int nbRing = op.getNbRing();
            final int nbSector = op.getNbSector();
            final CoordinateSystem cs = ellipsoid.getCoordinateSystem();
            final SampleSystem ss = UndefinedSystem.create(3);

            final float radiusX = (float) ellipsoid.getRadiusX();
            final float radiusY = (float) ellipsoid.getRadiusY();
            final float radiusZ = (float) ellipsoid.getRadiusZ();
            final float vAngle = 1f / (nbRing-1);
            final float hAngle = 1f / (nbSector-1);

            final Buffer.Float32 bufferVertices = DefaultBufferFactory.INSTANCE.createFloat32(nbRing*nbSector*3);
            final Buffer.Float32 bufferNormals = DefaultBufferFactory.INSTANCE.createFloat32(nbRing*nbSector*3);
            final Buffer.Int32 bufferIndices = DefaultBufferFactory.INSTANCE.createInt32(nbRing*nbSector*6);

            final Float32Cursor vertexCursor = bufferVertices.cursor();
            final Float32Cursor normalCursor = bufferNormals.cursor();
            final Int32Cursor indiceCursor = bufferIndices.cursor();

            for (int v = 0; v < nbRing; v++) {
                final double va = - Math.PI/2d + Math.PI * v * vAngle;
                for (int h = 0; h < nbSector; h++) {
                    final double ha = 2*Math.PI * h * hAngle;
                    final float x = (float) (Math.cos(va) * Math.sin(ha));
                    final float y = (float) (Math.sin(va) * Math.sin(ha));
                    final float z = (float) (Math.cos(ha));
                    vertexCursor.write(x*radiusX);
                    vertexCursor.write(y*radiusY);
                    vertexCursor.write(z*radiusZ);
                    normalCursor.write(x);
                    normalCursor.write(y);
                    normalCursor.write(z);

                    //first triangle
                    indiceCursor.write( v * nbSector + h );
                    indiceCursor.write( v * nbSector + (h+1) );
                    indiceCursor.write( (v+1) * nbSector + h );
                    //second triangle
                    indiceCursor.write( v * nbSector + (h+1) );
                    indiceCursor.write( (v+1) * nbSector + h );
                    indiceCursor.write( (v+1) * nbSector + (h+1) );
                }
            }

            final DefaultMesh idxGeom = new DefaultMesh(cs);
            idxGeom.getAttributes().add(Mesh.ATT_POSITION, new InterleavedTupleGrid1D(bufferVertices.getBuffer(), Float32.TYPE, ss, nbRing*nbSector));
            idxGeom.getAttributes().add(Mesh.ATT_NORMAL, new InterleavedTupleGrid1D(bufferNormals.getBuffer(), Float32.TYPE, ss, nbRing*nbSector));
            idxGeom.setIndex(new InterleavedTupleGrid1D(bufferIndices.getBuffer(), Int32.TYPE, UndefinedSystem.create(1), bufferIndices.getSize()));
            idxGeom.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) bufferIndices.getSize())});
            return idxGeom;
        }
    };

    public static final AbstractSingleOperationExecutor CYLINDER =
            new AbstractSingleOperationExecutor(TriangulateOp.class, Cylinder.class){

        public Object execute(Operation operation) throws OperationException {
            final TriangulateOp op = (TriangulateOp) operation;
            final Cylinder cylinder = (Cylinder) op.getGeometry();
            final double res = op.getResolution();
            final int nbRing = op.getNbRing();
            final int nbSector = op.getNbSector();
            final CoordinateSystem cs = cylinder.getCoordinateSystem();
            final SampleSystem ss = UndefinedSystem.create(3);

            final float height = (float) cylinder.getHeight();
            final float radius = (float) cylinder.getRadius();
            final float upper = height/2;
            final float lower = -height/2;
            final float hAngle = 1f / (nbSector);

            final int fannb = 2+nbSector;
            final int cylindernb = nbSector*6;
            final int nbVertice = (2+nbSector)*3 //top disk
                                + nbSector*3*4 //cylinder
                                + (2+nbSector)*3; //bottom disk
            final Buffer.Float32 bufferVertices = DefaultBufferFactory.INSTANCE.createFloat32(nbVertice);
            final Buffer.Float32 bufferNormals = DefaultBufferFactory.INSTANCE.createFloat32(nbVertice);
            final Buffer.Int32 bufferIndices = DefaultBufferFactory.INSTANCE.createInt32(
                    fannb //top disk
                   +nbSector*6 //cylinder
                   +fannb //bottom disk
            );

            final Float32Cursor vertexCursor = bufferVertices.cursor();
            final Float32Cursor normalCursor = bufferNormals.cursor();
            final Int32Cursor indiceCursor = bufferIndices.cursor();

            int index = 0;

            //top disk
            vertexCursor.write(0).write(upper).write(0);
            indiceCursor.write(index++);
            for (int s=0; s<nbSector+1; s++) {
                final double half = Math.PI /2;
                final double leftha = 2*Math.PI * s * hAngle;

                //first point : top left
                final float[] tl = point(half,leftha,radius, upper);
                vertexCursor.write(tl);
                normalCursor.write(tl);
                indiceCursor.write(index++);
            }

            //Cylinder
            for (int s = 0; s < nbSector; s++) {
                final double half = Math.PI /2;
                final double leftha = 2*Math.PI * s * hAngle;
                final double righttha = 2*Math.PI * (s+1) * hAngle;

                //first point : top left
                final float[] tl = point(half,leftha,radius, upper);
                final float[] tr = point(half,righttha,radius, upper);
                final float[] bl = point(half,leftha,radius, lower);
                final float[] br = point(half,righttha,radius, lower);

                vertexCursor.write(tl).write(tr).write(bl).write(br);
                normalCursor.write(tl).write(tr).write(bl).write(br);

                int itl = index++;
                int itr = index++;
                int ibl = index++;
                int ibr = index++;

                indiceCursor.write(itl).write(itr).write(ibl);
                indiceCursor.write(itr).write(ibr).write(ibl);
            }

            //Bottom disk
            vertexCursor.write(0).write(lower).write(0);
            indiceCursor.write(index++);
            for (int s=0; s<nbSector+1; s++) {
                final double half = Math.PI /2;
                final double leftha = 2*Math.PI * s * hAngle;

                //first point : top left
                final float[] tl = point(half,leftha,radius, lower);
                vertexCursor.write(tl);
                normalCursor.write(tl);
                indiceCursor.write(index++);
            }

            final DefaultMesh idxGeom = new DefaultMesh(cs);
            idxGeom.getAttributes().add(Mesh.ATT_POSITION, new InterleavedTupleGrid1D(bufferVertices.getBuffer(), Float32.TYPE, ss, nbVertice/3));
            idxGeom.getAttributes().add(Mesh.ATT_NORMAL, new InterleavedTupleGrid1D(bufferNormals.getBuffer(), Float32.TYPE, ss, nbVertice/3));
            idxGeom.setIndex(new InterleavedTupleGrid1D(bufferIndices.getBuffer(), Int32.TYPE, UndefinedSystem.create(1), bufferIndices.getSize()));
            idxGeom.setRanges(new IndexedRange[]{
                IndexedRange.TRIANGLE_FAN(0, fannb),
                IndexedRange.TRIANGLES(fannb, cylindernb),
                IndexedRange.TRIANGLE_FAN(fannb+cylindernb, fannb)
            });
            return idxGeom;
        }
    };

    public static final AbstractSingleOperationExecutor CONE =
            new AbstractSingleOperationExecutor(TriangulateOp.class, Cone.class){

        public Object execute(Operation operation) throws OperationException {
            final TriangulateOp op = (TriangulateOp) operation;
            final Cone cone = (Cone) op.getGeometry();
            final double res = op.getResolution();
            final int nbRing = op.getNbRing();
            final int nbSector = op.getNbSector();
            final CoordinateSystem cs = cone.getCoordinateSystem();
            final SampleSystem ss = UndefinedSystem.create(3);

            final float hAngle = 1f / (nbSector);
            final float radius = (float) cone.getRadius();
            final float height = (float) cone.getHeight();
            final float upper = height/2;
            final float lower = -height/2;

            //build 2 triangle fans
            final int fannb = (2+nbSector);
            final int nbVertice = (2+nbSector)*3*2;
            final Buffer.Float32 bufferVertices = DefaultBufferFactory.INSTANCE.createFloat32(nbVertice);
            final Buffer.Float32 bufferNormals = DefaultBufferFactory.INSTANCE.createFloat32(nbVertice);
            final Buffer.Int32 bufferIndices = DefaultBufferFactory.INSTANCE.createInt32(fannb*2);

            final Float32Cursor vertexCursor = bufferVertices.cursor();
            final Float32Cursor normalCursor = bufferNormals.cursor();
            final Int32Cursor indiceCursor = bufferIndices.cursor();

            int index = 0;

            //base disk
            vertexCursor.write(0).write(lower).write(0);
            indiceCursor.write(index++);
            for (int s=0; s<nbSector+1; s++) {
                final double half = Math.PI /2;
                final double leftha = 2*Math.PI * s * hAngle;

                //first point : top left
                final float[] tl = point(half,leftha,radius, lower);
                vertexCursor.write(tl);
                normalCursor.write(tl);
                indiceCursor.write(index++);
            }

            //cone
            vertexCursor.write(0).write(upper).write(0);
            indiceCursor.write(index++);
            for (int s=0; s<nbSector+1; s++) {
                final double half = Math.PI /2;
                final double leftha = 2*Math.PI * s * hAngle;

                //first point : top left
                final float[] tl = point(half,leftha,radius, lower);
                vertexCursor.write(tl);
                normalCursor.write(tl);
                indiceCursor.write(index++);
            }

            final DefaultMesh idxGeom = new DefaultMesh(cs);
            idxGeom.getAttributes().add(Mesh.ATT_POSITION, new InterleavedTupleGrid1D(bufferVertices.getBuffer(), Float32.TYPE, ss, nbVertice/3));
            idxGeom.getAttributes().add(Mesh.ATT_NORMAL, new InterleavedTupleGrid1D(bufferNormals.getBuffer(), Float32.TYPE, ss, nbVertice/3));
            idxGeom.setIndex(new InterleavedTupleGrid1D(bufferIndices.getBuffer(), Int32.TYPE, UndefinedSystem.create(1), bufferIndices.getSize()));
            idxGeom.setRanges(new IndexedRange[]{
                IndexedRange.TRIANGLE_FAN(0, fannb),
                IndexedRange.TRIANGLE_FAN(fannb, fannb)
            });
            return idxGeom;
        }
    };

    public static final AbstractSingleOperationExecutor QUAD =
            new AbstractSingleOperationExecutor(TriangulateOp.class, Quad.class){

        public Object execute(Operation operation) throws OperationException {
            final TriangulateOp op = (TriangulateOp) operation;
            final Quad quad = (Quad) op.getGeometry();
            final double res = op.getResolution();
            final int nbRing = op.getNbRing();
            final int nbSector = op.getNbSector();
            final CoordinateSystem cs = quad.getCoordinateSystem();
            final SampleSystem ss = UndefinedSystem.create(3);

            final VectorRW v1 = new Vector3f64(-quad.getWidth()/2.0,  quad.getHeight()/2.0, 0.0);
            final VectorRW v2 = new Vector3f64(-quad.getWidth()/2.0, -quad.getHeight()/2.0, 0.0);
            final VectorRW v3 = new Vector3f64( quad.getWidth()/2.0, -quad.getHeight()/2.0, 0.0);
            final VectorRW v4 = new Vector3f64( quad.getWidth()/2.0,  quad.getHeight()/2.0, 0.0);
            final Buffer.Float32 bufferVertices = DefaultBufferFactory.INSTANCE.createFloat32(16);
            final Float32Cursor vertexCursor = bufferVertices.cursor();
            vertexCursor.write(v1.toFloat());
            vertexCursor.write(v2.toFloat());
            vertexCursor.write(v3.toFloat());
            vertexCursor.write(v4.toFloat());

            final VectorRW normal = Geometries.calculateNormal(v1, v2, v3);
            final float[] na = normal.toFloat();
            final Buffer.Float32 bufferNormals = DefaultBufferFactory.INSTANCE.createFloat32(16);
            final Float32Cursor normalCursor = bufferNormals.cursor();
            normalCursor.write(na);
            normalCursor.write(na);
            normalCursor.write(na);
            normalCursor.write(na);

            final Buffer.Int32 bufferIndices = DefaultBufferFactory.INSTANCE.createInt32(6);
            final Int32Cursor indiceCursor = bufferIndices.cursor();
            indiceCursor.write(new int[]{
                0,1,2,
                2,3,0
            });

            final DefaultMesh idxGeom = new DefaultMesh(cs);
            idxGeom.getAttributes().add(Mesh.ATT_POSITION, new InterleavedTupleGrid1D(bufferVertices.getBuffer(), Float32.TYPE, ss, 16/3));
            idxGeom.getAttributes().add(Mesh.ATT_NORMAL, new InterleavedTupleGrid1D(bufferNormals.getBuffer(), Float32.TYPE, ss, 16/3));
            idxGeom.setIndex(new InterleavedTupleGrid1D(bufferIndices.getBuffer(), Int32.TYPE, UndefinedSystem.create(1), bufferIndices.getSize()));
            idxGeom.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 6)});
            return idxGeom;
        }
    };

    public static final AbstractSingleOperationExecutor TRIANGLE =
            new AbstractSingleOperationExecutor(TriangulateOp.class, Triangle.class){

        public Object execute(Operation operation) throws OperationException {
            final TriangulateOp op = (TriangulateOp) operation;
            final Triangle tri = (Triangle) op.getGeometry();
            final double res = op.getResolution();
            final int nbRing = op.getNbRing();
            final int nbSector = op.getNbSector();
            final CoordinateSystem cs = tri.getCoordinateSystem();
            final SampleSystem ss = UndefinedSystem.create(3);

            final Buffer.Float32 bufferVertices = DefaultBufferFactory.INSTANCE.createFloat32(3*3);
            final Buffer.Float32 bufferNormals = DefaultBufferFactory.INSTANCE.createFloat32(3*3);
            final Buffer.Int32 bufferIndices = DefaultBufferFactory.INSTANCE.createInt32(3);
            final Float32Cursor vertexCursor = bufferVertices.cursor();
            final Float32Cursor normalCursor = bufferNormals.cursor();

            final float[][] vertices = new float[3][3];
            vertices[0] = tri.getFirstCoord().toFloat();
            vertices[1] = tri.getSecondCoord().toFloat();
            vertices[2] = tri.getThirdCoord().toFloat();

            final float[] normals = new float[3];

            vertexCursor.write(vertices[0]);
            vertexCursor.write(vertices[1]);
            vertexCursor.write(vertices[2]);
            normals[0]=0; normals[1]=0; normals[2]=-1;
            for (int i=0;i<3;i++) normalCursor.write(normals);

            final int[] indices = {0, 1, 2};
            bufferIndices.write(0,indices);

            final DefaultMesh idxGeom = new DefaultMesh(cs);
            idxGeom.getAttributes().add(Mesh.ATT_POSITION, new InterleavedTupleGrid1D(bufferVertices.getBuffer(), Float32.TYPE, ss, 3));
            idxGeom.getAttributes().add(Mesh.ATT_NORMAL, new InterleavedTupleGrid1D(bufferNormals.getBuffer(), Float32.TYPE, ss, 3));
            idxGeom.setIndex(new InterleavedTupleGrid1D(bufferIndices.getBuffer(), Int32.TYPE, UndefinedSystem.create(1), bufferIndices.getSize()));
            idxGeom.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 3)});
            return idxGeom;
        }

    };

    public static final AbstractSingleOperationExecutor LINE =
            new AbstractSingleOperationExecutor(TriangulateOp.class, Line.class){

        public Object execute(Operation operation) throws OperationException {
            final TriangulateOp op = (TriangulateOp) operation;
            final Line segment = (Line) op.getGeometry();
            final double res = op.getResolution();
            final int nbRing = op.getNbRing();
            final int nbSector = op.getNbSector();
            final CoordinateSystem cs = segment.getCoordinateSystem();
            final SampleSystem ss = UndefinedSystem.create(3);

            final Buffer.Float32 bufferVertices = DefaultBufferFactory.INSTANCE.createFloat32(6);
            bufferVertices.cursor().write(segment.getStart().toFloat()).write(segment.getEnd().toFloat());

            final DefaultMesh idxGeom = new DefaultMesh(cs);
            idxGeom.getAttributes().add(Mesh.ATT_POSITION, new InterleavedTupleGrid1D(bufferVertices.getBuffer(), Float32.TYPE, ss, 6));
            idxGeom.setIndex(null);
            idxGeom.setRanges(new IndexedRange[]{IndexedRange.LINES(0, 6)});
            return idxGeom;
        }

    };

    private static float[] point(double va, double ha, float radius, float height) {
        final double sinva = Math.sin(va);
        return new float[]{
                    (float) (Math.cos(ha) * sinva)          *radius,
                    (float) (Math.sin(Math.PI/2d + va))    *radius + height,
                    (float) (Math.sin(ha) * sinva)          *radius
                };
    }

    private static void point(double va, double ha, float radius, float height, float[] position, float[] normals) {
        final double sinva = Math.sin(va);

        normals[0] = (float) (Math.cos(ha) * sinva);
        normals[1] = (float) Math.sin(Math.PI/2d + va);
        normals[2] = (float) (Math.sin(ha) * sinva);

        position[0] = normals[0] * radius;
        position[1] = normals[1] * radius + height;
        position[2] = normals[2] * radius;
    }

}
