
package science.unlicense.geometry.impl;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.geometry.api.system.CoordinateSystem;

/**
 *
 * @author Johann Sorel
 */
public class DefaultFragment extends DefaultPoint implements Fragment {

    private final Dictionary attributes = new HashDictionary();

    public DefaultFragment(CoordinateSystem cs) {
        super(cs);
    }

    @Override
    public Dictionary properties() {
        return attributes;
    }

}
