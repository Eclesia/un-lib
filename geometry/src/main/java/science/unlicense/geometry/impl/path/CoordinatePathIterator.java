
package science.unlicense.geometry.impl.path;

import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGridCursor;

/**
 * Path iterator over a coordinate array.
 * @author Johann Sorel
 */
public class CoordinatePathIterator extends AbstractStepPathIterator{

    private final TupleGrid1D coords;
    private final TupleGridCursor cursor;
    private final boolean close;
    private final int length;
    private int index = 0;

    public CoordinatePathIterator(TupleGrid1D coords) {
        this(coords,false);
    }

    public CoordinatePathIterator(TupleGrid1D coords,boolean close) {
        this.coords = coords;
        this.cursor = coords.cursor();
        this.close = close;
        this.length = coords.getDimension();
    }

    public void reset() {
        index = 0;
    }

    public boolean next() {
        if (index==length && close){
            currentStep = new PathStep2D(TYPE_CLOSE);
            index++;
            return true;
        }
        if (index>=length){
            return false;
        }

        final int type = (index==0) ? PathIterator.TYPE_MOVE_TO : PathIterator.TYPE_LINE_TO;
        cursor.moveTo(index);
        currentStep = new PathStep2D(type, cursor.samples().get(0), cursor.samples().get(1));

        index++;
        return true;
    }

}
