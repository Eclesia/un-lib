
package science.unlicense.geometry.impl.operation;

import science.unlicense.geometry.api.operation.LengthOp;
import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.Ellipse;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public class LengthExecutors {

    private LengthExecutors(){}

    public static final AbstractSingleOperationExecutor CIRCLE =
            new AbstractSingleOperationExecutor(LengthOp.class, Circle.class){

        public Object execute(Operation operation) {
            final LengthOp op = (LengthOp) operation;
            final Circle geom = (Circle) op.getGeometry();
            return geom.getLength();
        }
    };

    public static final AbstractSingleOperationExecutor ELLIPSE =
            new AbstractSingleOperationExecutor(LengthOp.class, Ellipse.class){

        public Object execute(Operation operation) {
            final LengthOp op = (LengthOp) operation;
            final Ellipse geom = (Ellipse) op.getGeometry();
            return geom.getLength();
        }
    };

    public static final AbstractSingleOperationExecutor POINT =
            new AbstractSingleOperationExecutor(LengthOp.class, Point.class){

        public Object execute(Operation operation) {
            return 0.0;
        }
    };

    public static final AbstractSingleOperationExecutor SEGMENT =
            new AbstractSingleOperationExecutor(LengthOp.class, Line.class){

        public Object execute(Operation operation) {
            final LengthOp op = (LengthOp) operation;
            final Line geom = (Line) op.getGeometry();
            final TupleRW start = geom.getStart();
            final TupleRW end = geom.getEnd();
            return Vectors.length(Vectors.subtract(end.toDouble(), start.toDouble()));
        }
    };

}
