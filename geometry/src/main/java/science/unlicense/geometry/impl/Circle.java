
package science.unlicense.geometry.impl;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.impl.path.AbstractStepPathIterator;
import science.unlicense.geometry.impl.path.PathStep2D;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2f64;


/**
 * A Circle.
 *
 * Specification :
 * - SVG specification v1.1:9.3 : Circle
 *   The ‘circle’ element defines a circle based on a center point and a radius.
 *
 * @author Johann Sorel
 */
public class Circle extends AbstractPlanarGeometry {

    private TupleRW center;
    private double radius;

    public Circle() {
        this(new Vector2f64(),0);
    }

    public Circle(final double cx, final double cy, final double r) {
        this(new Vector2f64(cx, cy), r);
    }

    public Circle(double radius) {
        this(new Vector2f64(),radius);
    }

    public Circle(TupleRW center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    /**
     * The circle center.
     */
    public TupleRW getCenter() {
        return center;
    }

    public double getCenterX() {
        return center.get(0);
    }

    public double getCenterY() {
        return center.get(1);
    }

    public void setCenter(final TupleRW center) {
        this.center = center;
    }

    public void setCenter(final double cx, final double cy) {
        this.center = new Vector2f64(cx, cy);
    }

    public void setCenterX(double cx) {
        this.center.set(0, cx);
    }

    public void setCenterY(double cy) {
        this.center.set(1, cy);
    }

    /**
     * The radius of the circle.
     */
    public double getRadius() {
        return radius;
    }

    public void setRadius(double r) {
        this.radius = r;
    }

    /**
     * Calculate the circle diameter.
     *
     * @return circle diameter.
     */
    public double getDiameter() {
        return 2*radius;
    }

    /**
     * Calculate the circle surface area.
     *
     * @return circle area.
     */
    public double getArea() {
        return Math.PI*radius*radius;
    }

    /**
     * Calculate circle circumference.
     *
     * @return circle circumference
     */
    public double getLength() {
        return 2*Math.PI*radius;
    }

    /**
     * Circle bounding box is a box centered on circle center
     * with a width of twice the radius.
     *
     * @return BoundingBox
     */
    public BBox getBoundingBox() {
        final BBox bbox = new BBox(2);
        bbox.setRange(0, center.get(0)-radius, center.get(0)+radius);
        bbox.setRange(1, center.get(1)-radius, center.get(1)+radius);
        return bbox;
    }

    /**
     * Bounding circle is a copy of this circle.
     *
     * @return Circle
     */
    public Circle getBoundingCircle() {
        return new Circle(center.copy(), radius);
    }

    /**
     * Centroid is circle center.
     *
     * @return Point
     */
    public Point getCentroid() {
        return new DefaultPoint(center.copy());
    }

    public PathIterator createPathIterator() {
        return new CircleIterator();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Circle other = (Circle) obj;
        if (this.center != other.center && (this.center == null || !this.center.equals(other.center))) {
            return false;
        }
        if (Double.doubleToLongBits(this.radius) != Double.doubleToLongBits(other.radius)) {
            return false;
        }
        return true;
    }

    @Override
    public Chars toChars() {
        return new Chars("Circle").concat(CObjects.toChars(center)).concat(new Chars(" "+radius));
    }

    private class CircleIterator extends AbstractStepPathIterator{

        private int index = 0;

        public void reset() {
            index = -1;
        }

        public boolean next() {

            //TODO not right but at least it has a close shape of a circle.
            index++;
            if (index==1){
                currentStep = new PathStep2D(PathIterator.TYPE_MOVE_TO,
                        center.get(0), center.get(1)-radius);
            } else if (index==2){
               currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC,
                       center.get(0)+radius, center.get(1)-radius,
                       center.get(0)+radius, center.get(1)       );
            } else if (index==3){
                currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC,
                       center.get(0)+radius, center.get(1)+radius,
                       center.get(0),        center.get(1)+radius);
            } else if (index==4){
                currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC,
                       center.get(0)-radius, center.get(1)+radius,
                       center.get(0)-radius, center.get(1));
            } else if (index==5){
                currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC,
                       center.get(0)-radius, center.get(1)-radius,
                       center.get(0),        center.get(1)-radius);
            } else if (index==6){
                currentStep = new PathStep2D(PathIterator.TYPE_CLOSE);
            } else {
                return false;
            }

            return true;
        }

    }

}
