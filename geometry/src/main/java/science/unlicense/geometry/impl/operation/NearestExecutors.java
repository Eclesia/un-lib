
package science.unlicense.geometry.impl.operation;

import science.unlicense.common.api.Arrays;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.operation.AbstractBinaryOperation;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.operation.NearestOp;
import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.OBBox;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class NearestExecutors {

    private NearestExecutors(){}

    public abstract static class NearestExecutor extends AbstractBinaryOperationExecutor{

        public NearestExecutor() {
            super(null, null, null, false);
        }

        public NearestExecutor(Class firstGeomClass, Class secondGeomClass) {
            super(NearestOp.class,firstGeomClass,secondGeomClass,true);
        }

        public Object execute(Operation operation) throws OperationException{
            final AbstractBinaryOperation op = (AbstractBinaryOperation) operation;
            Object first = op.getFirst();
            Object second = op.getSecond();

            if (canInvert && !(firstGeomClass.isInstance(first) && secondGeomClass.isInstance(second))){
                //set geometries in expected order
                Object temp = first;
                first = second;
                second = temp;
                //flip result
                Geometry[] res = (Geometry[]) execute(op, first, second);
                temp = res[0];
                res[0] = res[1];
                res[1] = (Geometry) temp;
                return res;
            }

            return execute(op, first, second);
        }

    }

    public static final NearestExecutor POINT_POINT =
            new NearestExecutor(Point.class, Point.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point) first;
            final Point b = (Point) second;
            return new Geometry[]{a,b};
        }
    };

    public static final NearestExecutor POINT_PLANE =
            new NearestExecutor(Point.class, Plane.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a  = (Point) first;
            final Plane b = (Plane) second;
            return new Geometry[]{a, new DefaultPoint(Geometries.projectPointOnPlan(
                    a.getCoordinate().toDouble(),
                    b.getNormal().toDouble(),
                    b.getD()))};
        }
    };

    public static final NearestExecutor POINT_TRIANGLE =
            new NearestExecutor(Point.class, Triangle.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Point a = (Point) first;
            final Triangle b = (Triangle) second;

            final double[] coord = a.getCoordinate().toDouble();

            final double[] t0 = b.getFirstCoord().toDouble();
            final double[] t1 = b.getSecondCoord().toDouble();
            final double[] t2 = b.getThirdCoord().toDouble();
            final double[] planNormal = Geometries.calculateNormal(t0,t1,t2);
            final double planD = Geometries.calculatePlanD(planNormal, t0);

            final double[] pointOnPlan = Geometries.projectPointOnPlan(coord, planNormal, planD);

            if (Geometries.inTriangle(t0, t1, t2, pointOnPlan)){
                //point projection is in in the triangle
                return new Geometry[]{a, new DefaultPoint(pointOnPlan)};
            } else {
                //calculate distance to edges
                double distanceSqt;
                final double[] res = new double[coord.length];
                final double[] buffer = new double[coord.length];
                final double[] ratio = new double[1];

                //distance to first edge
                double distSqt = DistanceOp.distanceSquare(t0, t1, buffer, coord, ratio, op.epsilon);
                distanceSqt = distSqt;
                Arrays.copy(buffer,0,buffer.length,res,0);

                //distance to second edge
                distSqt = DistanceOp.distanceSquare(t1, t2, buffer, coord, ratio, op.epsilon);
                if (distSqt<distanceSqt){
                    distanceSqt = distSqt;
                    Arrays.copy(buffer,0,buffer.length,res,0);
                }

                //distance to third edge
                distSqt = DistanceOp.distanceSquare(t2, t0, buffer, coord, ratio, op.epsilon);
                if (distSqt<distanceSqt){
                    distanceSqt = distSqt;
                    Arrays.copy(buffer,0,buffer.length,res,0);
                }

                return new Geometry[]{a,new DefaultPoint(res)};
            }
        }
    };

    public static final NearestExecutor POINT_LINE =
            new NearestExecutor(Point.class, Line.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Point a = (Point) first;
            final Line b = (Line) second;
            final Line la = new DefaultLine(a.getCoordinate(), a.getCoordinate());
            return (Geometry[]) LINE_LINE.execute(op, la, b);
        }
    };

    public static final NearestExecutor POINT_RECTANGLE =
            new NearestExecutor(Point.class, Rectangle.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Rectangle b = (Rectangle) second;
            return (Geometry[]) POINT_BBOX.execute(op, first, b.getBoundingBox());
        }
    };

    public static final NearestExecutor POINT_BBOX =
            new NearestExecutor(Point.class, BBox.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point) first;
            final BBox b = (BBox) second;
            final int dim = a.getDimension();
            final TupleRW nearest = VectorNf64.createDouble(dim);
            for (int i=0;i<dim;i++){
                double val = a.getCoordinate().get(i);
                final double minx = b.getMin(i);
                final double maxx = b.getMax(i);
                if (val<minx) val = minx;
                if (val>maxx) val = maxx;
                nearest.set(i, val);
            }
            return new Geometry[]{a,new DefaultPoint(nearest)};
        }
    };

    public static final NearestExecutor POINT_OBBOX =
            new NearestExecutor(Point.class, OBBox.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Point a = (Point) first;
            final OBBox b = (OBBox) second;

            //we move the point in the OBBox space
            Similarity rt = b.getTransform();
            Point pt = new DefaultPoint(rt.invert().transform(a.getCoordinate(),null));
            final BBox bbox = new BBox(b.getLower(),b.getUpper());
            final Geometry[] geoms = (Geometry[]) POINT_BBOX.execute(op, pt, bbox);

            //convert point back in world space
            pt = (Point) geoms[1];
            rt.transform(pt.getCoordinate(), pt.getCoordinate());

            return new Geometry[]{a,pt};
        }
    };

    public static final NearestExecutor LINE_LINE =
            new NearestExecutor(Line.class, Line.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Line line1 = (Line) first;
            final Line line2 = (Line) second;
            final double[] ratio = new double[2];
            final double[] c1 = new double[line1.getStart().getSampleCount()];
            final double[] c2 = new double[line1.getStart().getSampleCount()];
            final double dist = DistanceOp.distance(
                                    line1.getStart().toDouble(), line1.getEnd().toDouble(), c1,
                                    line2.getStart().toDouble(), line2.getEnd().toDouble(), c2,
                                    ratio,op.epsilon);
            return new Geometry[]{new DefaultPoint(c1),new DefaultPoint(c2)};
        }
    };

    public static final NearestExecutor LINE_TRIANGLE =
            new NearestExecutor(Line.class, Triangle.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Line segment = (Line) first;
            final Triangle triangle  = (Triangle) second;
            final double[] segmentStart = segment.getStart().toDouble();
            final double[] segmentEnd = segment.getEnd().toDouble();

            final double[] t0 = triangle.getFirstCoord().toDouble();
            final double[] t1 = triangle.getSecondCoord().toDouble();
            final double[] t2 = triangle.getThirdCoord().toDouble();

            final double[] p0 = new double[segmentStart.length];
            final double[] p1 = new double[segmentStart.length];
            final double distance = DistanceOp.distanceSquareSegmentTriangle(segmentStart, segmentEnd,
                    p0, t0, t1, t2, p1, op.epsilon);

            return new Geometry[]{new DefaultPoint(p0), new DefaultPoint(p1)};
        }
    };

    public static final NearestExecutor TRIANGLE_TRIANGLE =
            new NearestExecutor(Triangle.class, Triangle.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Triangle triangle1 = (Triangle) first;
            final Triangle triangle2  = (Triangle) second;

            final double[] t0 = triangle2.getFirstCoord().toDouble();
            final double[] t1 = triangle2.getSecondCoord().toDouble();
            final double[] t2 = triangle2.getThirdCoord().toDouble();

            double distance;
            final double[] res0 = new double[t0.length];
            final double[] res1 = new double[t0.length];

            final double[] p0 = new double[t0.length];
            final double[] p1 = new double[t0.length];

            //fisrt edge test
            double[] segmentStart = triangle1.getFirstCoord().toDouble();
            double[] segmentEnd = triangle1.getSecondCoord().toDouble();
            double dist = DistanceOp.distanceSquareSegmentTriangle(segmentStart, segmentEnd,
                    p0, t0, t1, t2, p1, op.epsilon);

            distance = dist;
            Arrays.copy(p0, 0, p0.length, res0, 0);
            Arrays.copy(p1, 0, p1.length, res1, 0);

            //second edge test
            segmentStart = triangle1.getSecondCoord().toDouble();
            segmentEnd = triangle1.getThirdCoord().toDouble();
            dist = DistanceOp.distanceSquareSegmentTriangle(segmentStart, segmentEnd,
                    p0, t0, t1, t2, p1, op.epsilon);

            if (dist<distance){
                distance = dist;
                Arrays.copy(p0, 0, p0.length, res0, 0);
                Arrays.copy(p1, 0, p1.length, res1, 0);
            }

            //third edge test
            segmentStart = triangle1.getThirdCoord().toDouble();
            segmentEnd = triangle1.getFirstCoord().toDouble();
            dist = DistanceOp.distanceSquareSegmentTriangle(segmentStart, segmentEnd,
                    p0, t0, t1, t2, p1, op.epsilon);

            if (dist<distance){
                distance = dist;
                Arrays.copy(p0, 0, p0.length, res0, 0);
                Arrays.copy(p1, 0, p1.length, res1, 0);
            }


            return new Geometry[]{new DefaultPoint(res0), new DefaultPoint(res1)};
        }
    };


    public static final NearestExecutor POLYLINE_POLYLINE =
            new NearestExecutor(Polyline.class, Polyline.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Polyline line1 = (Polyline) first;
            final Polyline line2 = (Polyline) second;

            final double[] c1 = new double[line1.getDimension()];
            final double[] c2 = new double[line1.getDimension()];
            double distance = Double.MAX_VALUE;

            final double[] tempRatio = new double[2];
            final double[] tempC1 = new double[line1.getDimension()];
            final double[] tempC2 = new double[line1.getDimension()];

            final TupleGrid1D line1Coords = line1.getCoordinates();
            final TupleGrid1D line2Coords = line2.getCoordinates();
            final TupleGridCursor cursor1 = line1Coords.cursor();
            final TupleGridCursor cursor2 = line2Coords.cursor();
            final int nb1 = line1Coords.getDimension()-1;
            final int nb2 = line2Coords.getDimension()-1;

            final double[] s1 = new double[line1.getDimension()];
            final double[] e1 = new double[line1.getDimension()];
            final double[] s2 = new double[line2.getDimension()];
            final double[] e2 = new double[line2.getDimension()];
            for (int i=0;i<nb1;i++){
                cursor1.moveTo(i);
                cursor1.samples().toDouble(s1, 0);
                cursor1.moveTo(i+1);
                cursor1.samples().toDouble(e1, 0);
                for (int k=0;k<nb2;k++){
                    cursor2.moveTo(k);
                    cursor2.samples().toDouble(s2, 0);
                    cursor2.moveTo(k+1);
                    cursor2.samples().toDouble(e2, 0);

                    final double dist = DistanceOp.distance(
                                    s1, e1, tempC1,
                                    s2, e2, tempC2,
                                    tempRatio,op.epsilon);
                    if (dist<distance){
                        //keep informations
                        distance = dist;
                        Arrays.copy(tempC1, 0, tempC1.length, c1, 0);
                        Arrays.copy(tempC2, 0, tempC2.length, c2, 0);
                    }
                }
            }

            return new Geometry[]{new DefaultPoint(c1), new DefaultPoint(c2)};
        }
    };

    public static final NearestExecutor PLANE_SEGMENT =
            new NearestExecutor(Plane.class, Line.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Plane plan  = (Plane) first;
            final Line segment = (Line) second;

            final VectorRW vb1 = VectorNf64.create(segment.getStart());
            double d1 = plan.getNormal().dot(vb1.subtract(plan.getNormal().scale(plan.getD())) );
            final VectorRW vb2 = VectorNf64.create(segment.getEnd());
            double d2 = plan.getNormal().dot(vb2.subtract(plan.getNormal().scale(plan.getD())) );

            final VectorRW pointOnPlan;
            final VectorRW segmentPoint;
            if (d1<d2){
                segmentPoint = vb1;
                pointOnPlan = vb1.subtract(plan.getNormal().scale(d1));
            } else {
                segmentPoint = vb2;
                pointOnPlan = vb2.subtract(plan.getNormal().scale(d2));
            }

            return new Geometry[]{new DefaultPoint(pointOnPlan), new DefaultPoint(segmentPoint)};
        }
    };

    public static final NearestExecutor BBOX_SEGMENT =
            new NearestExecutor(BBox.class, Line.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final BBox bbox  = (BBox) first;
            final Line segment = (Line) second;
            final double[] segmentStart = segment.getStart().toDouble();
            final double[] segmentEnd = segment.getEnd().toDouble();

            final double[] l = bbox.getLower().toDouble();
            final double[] u = bbox.getUpper().toDouble();
            final double[][] edges = {
                {l[0], l[1], l[2]},
                {l[0], l[1], u[2]},
                {l[0], u[1], l[2]},
                {l[0], u[1], u[2]},
                {u[0], l[1], l[2]},
                {u[0], l[1], u[2]},
                {u[0], u[1], l[2]},
                {u[0], u[1], u[2]}
            };
            //12 edges
            //bottom face   :  0,1  1,3  3,2  2,0
            //top face      :  4,5  5,7  7,6  6,4
            //contour faces :  0,4  1,5  2,6  3,7
            final int[][] indexEdge = {
                {0,1},{1,3},{3,2},{2,0},
                {4,5},{5,7},{7,6},{6,4},
                {0,4},{1,5},{2,6},{3,7}
            };

            final double[] p0 = new double[segmentStart.length];
            final double[] p1 = new double[segmentStart.length];
            final double[] ratio = new double[2];
            final double[] nearestPointSegment = new double[segmentStart.length];
            final double[] nearestPointBBox = new double[segmentStart.length];

            //test each edge distance to segment
            double distance = Double.POSITIVE_INFINITY;
            double dist;
            for (int i=0;i<indexEdge.length;i++){
                dist = DistanceOp.distanceSquare(segmentStart, segmentEnd, p0, edges[indexEdge[i][0]], edges[indexEdge[i][1]], p1, ratio, op.epsilon);
                if (dist<distance){
                    distance = dist;
                    Arrays.copy(p0,0,p0.length,nearestPointSegment,0);
                    Arrays.copy(p1,0,p1.length,nearestPointBBox,0);
                }
            }

            //test the two segment ends
            //TODO
            //test plan intersection
            //TODO

            return new Geometry[]{new DefaultPoint(nearestPointBBox), new DefaultPoint(nearestPointSegment)};
        }
    };

}
