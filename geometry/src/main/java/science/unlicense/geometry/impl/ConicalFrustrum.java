
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;

/**
 * A conical frustrum is a volume defined
 * by 2 circles aligned on the same axis
 *
 * @author Johann Sorel
 */
public class ConicalFrustrum extends AbstractOrientedGeometry {

    private double height;
    private double radius1;
    private double radius2;

    /**
     *
     * @param height
     * @param radius1 top radius
     * @param radius2 bottom radius
     */
    public ConicalFrustrum(double height, double radius1, double radius2) {
        super(3);
        this.height = height;
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getRadius1() {
        return radius1;
    }

    public void setRadius1(double radius) {
        this.radius1 = radius;
    }

    public double getRadius2() {
        return radius2;
    }

    public void setRadius2(double radius) {
        this.radius2 = radius;
    }

    public VectorRW getFirst(){
        return new Vector3f64(0, -height/2, 0);
    }

    public VectorRW getSecond(){
        return new Vector3f64(0, height/2, 0);
    }

    /**
     * Calculate the volume.
     *
     * @return volume.
     */
    public double getVolume(){
        return (1.0/3.0) * Math.PI * height * ( radius1*radius1 + radius2*radius2 + (radius1*radius2));
    }

    /**
     * Calculate the cone lateral height.
     *
     * @return cone lateral height
     */
    public double getLateralHeight(){
        return Math.sqrt( Math.pow(radius1-radius2,2) + (height*height));
    }

    /**
     * Calculate top circle surface area.
     *
     * @return base circle area.
     */
    public double getTopArea() {
        return Math.PI*radius1*radius1;
    }

    /**
     * Calculate the top circumference.
     *
     * @return circumference.
     */
    public double getTopCircumference(){
        return 2 * Math.PI * radius1;
    }

    /**
     * Calculate top circle surface area.
     *
     * @return base circle area.
     */
    public double getBottomArea() {
        return Math.PI*radius2*radius2;
    }

    /**
     * Calculate the bottom circumference.
     *
     * @return circumference.
     */
    public double getBottomCircumference(){
        return 2 * Math.PI * radius2;
    }

    /**
     * Calculate lateral surface of the frustrum.
     *
     * @return lateral surface.
     */
    public double getLateralSurfaceArea(){
        return Math.PI * (radius1+radius2) * getLateralHeight();
    }

    /**
     * Calculate the surface area.
     *
     * @return area.
     */
    public double getArea() {
        return getTopArea() + getBottomArea() + getLateralSurfaceArea();
    }

    public BBox getUnorientedBounds() {
        final double maxRadius = Maths.max(radius1, radius2);
        return new BBox(
                new Vector3f64(-maxRadius, -height/2 - maxRadius, -maxRadius),
                new Vector3f64(maxRadius, height/2 + maxRadius, maxRadius)
        );
    }

}
