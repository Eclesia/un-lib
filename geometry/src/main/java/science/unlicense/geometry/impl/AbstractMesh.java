
package science.unlicense.geometry.impl;

import science.unlicense.common.api.collection.AbstractDictionary;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.path.CoordinateTuplePathIterator;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Scalari32;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractMesh extends AbstractGeometry implements Mesh {

    public AbstractMesh(int dim) {
        super(dim);
    }

    public AbstractMesh(CoordinateSystem cs) {
        super(cs);
    }

    @Override
    public Sequence getGeometries() {
        final Sequence geometries = new ArraySequence();

        final TupleGridCursor cursor = getIndex().cursor();
        final IndexedRange[] ranges = getRanges();

        int idx0;
        int idx1;
        int idx2;
        IndexedRange range;
        int mode;
        int offset;
        int cnt;
        int i;
        int n;
        for (int m=0; m<ranges.length; m++) {
            range  = ranges[m];
            mode   = range.getMode();
            offset = range.getIndexOffset();
            cnt    = range.getCount();

            switch (mode) {
                case IndexedRange.POINTS:
                    for (i=0; i<cnt; i++) {
                        cursor.moveTo(i+offset);
                        idx0 = (int) cursor.samples().get(0);
                        geometries.add(getVertex(idx0));
                    }   break;
                case IndexedRange.LINES:
                    for (i=0; i<cnt; i+=2) {
                        cursor.moveTo(i+offset+0);
                        idx0 = (int) cursor.samples().get(0);
                        cursor.moveTo(i+offset+1);
                        idx1 = (int) cursor.samples().get(0);
                        geometries.add(new Mesh.Line(this, new int[]{idx0,idx1}));
                    }   break;
                case IndexedRange.LINES_ADJACENCY:
                    throw new UnimplementedException("TODO");
                case IndexedRange.LINE_LOOP:
                    throw new UnimplementedException("TODO");
                case IndexedRange.LINE_STRIP:
                    throw new UnimplementedException("TODO");
                case IndexedRange.LINE_STRIP_ADJACENCY:
                    throw new UnimplementedException("TODO");
                case IndexedRange.TRIANGLES:
                    for (n=offset+cnt; offset<n;offset+=3) {
                        cursor.moveTo(offset);
                        idx0 = (int) cursor.samples().get(0);
                        cursor.next();
                        idx1 = (int) cursor.samples().get(0);
                        cursor.next();
                        idx2 = (int) cursor.samples().get(0);
                        geometries.add(new Mesh.Triangle(this, new int[]{idx0,idx1,idx2}));
                    }   break;
                case IndexedRange.TRIANGLES_ADJACENCY:
                    //TODO make a real type triangleadjency, same for fan and strip
                    for (i=0; i<cnt; i+=6) {
                        cursor.moveTo(i+offset+0);
                        idx0 = (int) cursor.samples().get(0);
                        cursor.moveTo(i+offset+2);
                        idx1 = (int) cursor.samples().get(0);
                        cursor.moveTo(i+offset+4);
                        idx2 = (int) cursor.samples().get(0);
                        geometries.add(new Mesh.Triangle(this, new int[]{idx0,idx1,idx2}));
                    }   break;
                case IndexedRange.TRIANGLE_FAN:
                    cursor.moveTo(offset+0);
                    idx0 = (int) cursor.samples().get(0);
                    for (i=1,n=cnt-1; i<n; i++) {
                        cursor.moveTo(i+offset+0);
                        idx1 = (int) cursor.samples().get(0);
                        cursor.moveTo(i+offset+1);
                        idx2 = (int) cursor.samples().get(0);
                        geometries.add(new Mesh.Triangle(this, new int[]{idx0,idx1,idx2}));
                    }   break;
                case IndexedRange.TRIANGLE_STRIP:
                    cursor.moveTo(offset+0);
                    idx0 = (int) cursor.samples().get(0);
                    cursor.moveTo(offset+1);
                    idx1 = (int) cursor.samples().get(0);
                    for (i=2,n=cnt-1; i<n; i++) {
                        cursor.moveTo(i+offset);
                        idx2 = (int) cursor.samples().get(0);
                        geometries.add(new Mesh.Triangle(this, new int[]{idx0,idx1,idx2}));
                        idx0 = idx1;
                        idx1 = idx2;
                    }   break;
                case IndexedRange.TRIANGLE_STRIP_ADJACENCY:
                    throw new UnimplementedException("TODO");
                case IndexedRange.PATCHES:
                    throw new UnimplementedException("TODO");
                default:
                    throw new UnimplementedException("TODO");
            }
        }

        return geometries;
    }

    @Override
    public Vertex getVertex(int index) {
        return new IndexedVertex(this, index);
    }

    public static class IndexedVertex extends AbstractDictionary implements Vertex {

        private final Mesh parent;
        private final Scalari32 index;

        public IndexedVertex(Mesh geometry, int index) {
            this.parent = geometry;
            this.index = new Scalari32(index);
        }

        @Override
        public TupleRW getCoordinate() {
            final TupleGridCursor cursor = parent.getPositions().cursor();
            cursor.moveTo(index);
            return cursor.samples();
        }

        @Override
        public int getIndex() {
            return index.x;
        }

        public void setIndex(int index) {
            this.index.x = index;
        }

        @Override
        public Dictionary properties() {
            return this;
        }

        @Override
        public int getSize() {
            return parent.getAttributes().getSize();
        }

        @Override
        public Set getKeys() {
            return parent.getAttributes().getKeys();
        }

        @Override
        public Collection getValues() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Collection getPairs() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Object getValue(Object key) {
            final TupleGrid tupleGrid = (TupleGrid) parent.getAttributes().getValue(key);
            if (tupleGrid == null) return null;
            final TupleGridCursor cursor = tupleGrid.cursor();
            cursor.moveTo(index);
            return cursor;
        }

        @Override
        public void add(Object key, Object value) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Object remove(Object key) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public void removeAll() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public PathIterator createPathIterator() {
            return new CoordinateTuplePathIterator(new Tuple[]{getCoordinate()});
        }

        @Override
        public CoordinateSystem getCoordinateSystem() {
            return parent.getCoordinateSystem();
        }

        @Override
        public void setCoordinateSystem(CoordinateSystem cs) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public int getDimension() {
            return getCoordinateSystem().getDimension();
        }

        @Override
        public BBox getBoundingBox() {
            final TupleRW pos = getCoordinate();
            final BBox bbox = new BBox(getCoordinateSystem());
            bbox.getLower().set(pos);
            bbox.getUpper().set(pos);
            return bbox;
        }

        @Override
        public Dictionary getUserProperties() {
            throw new UnsupportedOperationException("Not supported.");
        }

    }

}
