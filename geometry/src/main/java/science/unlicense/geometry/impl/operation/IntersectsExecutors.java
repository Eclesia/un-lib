
package science.unlicense.geometry.impl.operation;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.operation.AbstractBinaryOperation;
import science.unlicense.geometry.api.operation.CentroidOp;
import science.unlicense.geometry.api.operation.DistanceOp;
import static science.unlicense.geometry.api.operation.DistanceOp.distance;
import static science.unlicense.geometry.api.operation.DistanceOp.distanceSquare;
import science.unlicense.geometry.api.operation.IntersectsOp;
import science.unlicense.geometry.api.operation.NearestOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.Frustrum;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public class IntersectsExecutors {

    private IntersectsExecutors(){}

    public static final AbstractBinaryOperationExecutor POINT_POINT =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Point.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point) first;
            final Point b = (Point) second;
            return a.getCoordinate().equals(b.getCoordinate());
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_LINE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Point.class, Line.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a  = (Point) first;
            final Line b = (Line) second;
            final double d = distance(b.getStart(), b.getEnd(), a.getCoordinate());
            return d < op.epsilon  && d > -op.epsilon;
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_CIRCLE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Point.class, Circle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point) first;
            final Circle b = (Circle) second;
            return (distance(a.getCoordinate(), b.getCenter()) - b.getRadius()) < 0;
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_RECTANGLE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Point.class, Rectangle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Tuple pt    = ((Point) first).getCoordinate();
            final Rectangle rect = (Rectangle) second;
            final double x = pt.get(0);
            if (x<rect.getX() || x>rect.getX()+rect.getWidth()) return false;
            final double y = pt.get(1);
            if (y<rect.getY() || y>rect.getY()+rect.getHeight()) return false;
            return true;
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_PLANE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Point.class, Plane.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point) first;
            final Plane b = (Plane) second;
            final double d = DistanceOp.distance(a.getCoordinate(), b);
            return d < op.epsilon  && d > -op.epsilon;
        }
    };

    public static final AbstractBinaryOperationExecutor RECTANGLE_RECTANGLE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Rectangle.class, Rectangle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Rectangle rect1 = (Rectangle) first;
            final Rectangle rect2 = (Rectangle) second;
            return rect1.intersects(rect2);
        }
    };

    public static final AbstractBinaryOperationExecutor BBOX_BBOX =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, BBox.class, BBox.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final BBox box1 = (BBox) first;
            final BBox box2 = (BBox) second;
            final int dim = box1.getDimension();
            for (int i=0;i<dim;i++){
                if (box1.getMax(i) < box2.getMin(i) || box1.getMin(i) > box2.getMax(i)) return false;
            }
            return true;
        }
    };

    /**
     * Two planes intersects if they are not parallel nore overlaps.
     */
    public static final AbstractBinaryOperationExecutor PLANE_PLANE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Plane.class, Plane.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Plane p1 = (Plane) first;
            final Plane p2 = (Plane) second;

            final boolean parallel = p1.getNormal().isParallel(p2.getNormal(), op.epsilon);
            if (parallel) {
                //plane overlaps test
                final VectorRW a = p1.getPoint();
                final double d = DistanceOp.distance(a, p2);
                return d < op.epsilon && d > -op.epsilon;
            }
            return !parallel;
        }
    };


    public static final AbstractBinaryOperationExecutor BBOX_FRUSTRUM =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, BBox.class, Frustrum.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final BBox a = (BBox) first;
            final Frustrum b = (Frustrum) second;

            final VectorRW[] corners = new VectorRW[]{
                new Vector3f64(),new Vector3f64(),new Vector3f64(),new Vector3f64(),
                new Vector3f64(),new Vector3f64(),new Vector3f64(),new Vector3f64()
            };

            final double[] l = a.getLower().toDouble();
            final double[] u = a.getUpper().toDouble();
            corners[0].setXYZ(l[0], l[1], l[2]);
            corners[1].setXYZ(l[0], l[1], u[2]);
            corners[2].setXYZ(l[0], u[1], l[2]);
            corners[3].setXYZ(l[0], u[1], u[2]);
            corners[4].setXYZ(u[0], l[1], l[2]);
            corners[5].setXYZ(u[0], l[1], u[2]);
            corners[6].setXYZ(u[0], u[1], l[2]);
            corners[7].setXYZ(u[0], u[1], u[2]);

            //TODO algo can be split and optimized in 2 for intersect and within operations

            int nbIn = 0;
            for (Plane p : b.getPlanes()) {
                int nbCornerIn = 0;

                for (int i=0;i<8;i++){
                    if (DistanceOp.distance(corners[i], p) <= 0) {
                        //corner is in the plane
                        nbCornerIn++;
                    }
                }

                if (nbCornerIn==0){
                    //all points are outside the plane
                    return false;
                } else if (nbCornerIn==8){
                    nbIn++;
                    //we are just searching for intersection, we can return now
                    return true;
                }
            }

            return true;
//            if (nbIn == 6){
//                //all corners are in, bbox is within
//                return true;
//            } else {
//                //some corners are in the plane, bbox is intersection
//                return true;
//            }
        }
    };

    public static final AbstractBinaryOperationExecutor LINE_LINE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Line.class, Line.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Line line1 = (Line) first;
            final Line line2 = (Line) second;
            final double[] ratio = new double[2];
            final double[] c1 = new double[line1.getStart().getSampleCount()];
            final double[] c2 = new double[line1.getStart().getSampleCount()];
            final double dist = distance(
                                    line1.getStart().toDouble(), line1.getEnd().toDouble(), c1,
                                    line2.getStart().toDouble(), line2.getEnd().toDouble(), c2,
                                    ratio,op.epsilon);
            return dist == 0;
        }
    };

    public static final AbstractBinaryOperationExecutor SPHERE_SPHERE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Sphere.class, Sphere.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Sphere s1 = (Sphere) first;
            final Sphere s2 = (Sphere) second;
            // square distance between sphere center
            final double[] d = Vectors.subtract(s1.getCenter().toDouble(), s2.getCenter().toDouble());
            final double d2 = Vectors.dot(d, d);
            // square sum of radius
            final double rs = s1.getRadius() + s2.getRadius();
            return d2 <= rs * rs;
        }
    };

    public static final AbstractBinaryOperationExecutor SPHERE_CAPSULE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Sphere.class, Capsule.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Sphere sphere = (Sphere) first;
            final Capsule capsule = (Capsule) second;
            // get square distance between sphere center and capsule segment
            final double dist = distanceSquare(capsule.getFirst(), capsule.getSecond(), sphere.getCenter());
            final double radius = sphere.getRadius() + capsule.getRadius();
            return dist <= radius*radius;
        }
    };

    public static final AbstractBinaryOperationExecutor SPHERE_PLANE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Sphere.class, Plane.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Sphere a = (Sphere) first;
            final Plane b = (Plane) second;
            double d = DistanceOp.distance(a.getCenter(), b);
            d = Math.abs(d);
            return d < a.getRadius();
        }
    };

    public static final AbstractBinaryOperationExecutor SPHERE_BBOX =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Sphere.class, BBox.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Sphere a = (Sphere) first;
            final BBox b = (BBox) second;

            //find the nearest point on the bbox
            final Geometry[] nearest = new NearestOp(b, new DefaultPoint(a.getCenter())).execute();

            //check distance
            final double distance = distance(
                    new CentroidOp(nearest[0]).execute().getCoordinate(),
                    new CentroidOp(nearest[1]).execute().getCoordinate());
            return distance <= a.getRadius();
        }
    };

    public static final AbstractBinaryOperationExecutor SPHERE_FRUSTRUM =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Sphere.class, Frustrum.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Sphere a = (Sphere) first;
            final Frustrum b = (Frustrum) second;
            final double radius = a.getRadius();

            //check distance to each plane
            for (Plane p : b.getPlanes()){
                // calculate distance to plane
                final double dist = p.getNormal().dot(a.getCenter())+p.getD();

                if (dist<radius){
                    //outside
                    return false;
                } else if (Math.abs(dist)<radius){
                    //intersect
                    return true;
                }
            }

            //inside
            return true;
        }
    };


    /**
     * Adapted from book : Real-TimeCollision Detection by Christer Ericson
     */
    public static final AbstractBinaryOperationExecutor CAPSULE_CAPSULE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Capsule.class, Capsule.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Capsule capsule1 = (Capsule) first;
            final Capsule capsule2 = (Capsule) second;
            // Compute (squared) distance between the inner structures of the capsules
            final double[] ratio = new double[2];
            final double[] c1 = new double[capsule1.getFirst().getSampleCount()];
            final double[] c2 = new double[capsule1.getFirst().getSampleCount()];
            final double dist = distance(capsule1.getFirst().toDouble(), capsule1.getSecond().toDouble(), c1,
                                    capsule2.getFirst().toDouble(), capsule2.getSecond().toDouble(), c2,
                                    ratio,op.epsilon);
            // If (squared) distance smaller than (squared) sum of radii, they collide
            double radius = capsule1.getRadius() + capsule2.getRadius();
            return dist <= radius * radius;
        }
    };

    public static final AbstractBinaryOperationExecutor CIRCLE_CIRCLE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Circle.class, Circle.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Circle a = (Circle) first;
            final Circle b = (Circle) second;
            double r = a.getRadius() + b.getRadius();
            r *= r;
            final double[] c1 = a.getCenter().toDouble();
            final double[] c2 = b.getCenter().toDouble();
            final double sx = c1[0]-c2[0];
            final double sy = c1[1]-c2[1];
            return r > (sx*sx + sy*sy);
        }
    };

    public static final AbstractBinaryOperationExecutor TRIANGLE_BBOX =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Triangle.class, BBox.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Triangle triangle = (Triangle) first;
            final BBox bbox = (BBox) second;

            //highly modified version of algo from book : Real time collision detection, page 169
            //TODO make OBBox version, should be nearly the same
            final VectorRW[] bboxNormals = {
                new Vector3f64(1,0,0),
                new Vector3f64(0,1,0),
                new Vector3f64(0,0,1)
            };
            final double[] bboxLower = bbox.getLower().toDouble();
            final double[] bboxUpper = bbox.getUpper().toDouble();
            final Tuple[] bboxCoords = {
                new Vector3f64(bboxLower[0],bboxLower[1],bboxLower[2]),
                new Vector3f64(bboxLower[0],bboxLower[1],bboxUpper[2]),
                new Vector3f64(bboxLower[0],bboxUpper[1],bboxLower[2]),
                new Vector3f64(bboxLower[0],bboxUpper[1],bboxUpper[2]),
                new Vector3f64(bboxUpper[0],bboxLower[1],bboxLower[2]),
                new Vector3f64(bboxUpper[0],bboxLower[1],bboxUpper[2]),
                new Vector3f64(bboxUpper[0],bboxUpper[1],bboxLower[2]),
                new Vector3f64(bboxUpper[0],bboxUpper[1],bboxUpper[2])
            };

            final Tuple[] triangleCoords = {
                triangle.getFirstCoord(),
                triangle.getSecondCoord(),
                triangle.getThirdCoord()
            };
            final VectorRW[] triangleEdges = {
                VectorNf64.create(triangleCoords[0]).localSubtract(triangleCoords[1]),
                VectorNf64.create(triangleCoords[1]).localSubtract(triangleCoords[2]),
                VectorNf64.create(triangleCoords[2]).localSubtract(triangleCoords[0])
            };

            //check 9 edges
            for (int i=0; i<3; i++){
                for (int k=0; k<3; k++){
                    final VectorRW axis = triangleEdges[i].cross(bboxNormals[k]);
                    final double[] rangeBbox = project(bboxCoords, axis);
                    final double[] rangeTriangle = project(triangleCoords, axis);
                    if (rangeBbox[1] < rangeTriangle[0] || rangeBbox[0] > rangeTriangle[1]){
                        return false;
                    }
                }
            }

            //check against bbox normals
            for (int i=0; i<3; i++){
//                if (!sat(triangleCoords,bboxNormals[i], bbox.getMin(i), bbox.getMax(i))){
//                    return false;
//                }
                final double[] range = project(triangleCoords, bboxNormals[i]);
                if (range[1] < bbox.getMin(i) || range[0] > bbox.getMax(i)){
                    return false;
                }
            }

            //check triangle normal
            final VectorRW triangleNormal = ((DefaultTriangle) triangle).getNormal();
            final double[] triangleSpan = project(triangleCoords, triangleNormal);
            final double[] range = project(bboxCoords, triangleNormal);
            if (range[1]<triangleSpan[0] || range[0]>triangleSpan[1]){
                return false;
            }

            return true;
        }

    };

    /**
     * Separating axis theorem.
     * http://en.wikipedia.org/wiki/Hyperplane_separation_theorem
     *
     * @param coords Coordinates to project on axis
     * @param axis projection axis
     * @param min minimum value on axis for intersection
     * @param max maximum value on axis for intersection
     * @return false if there is no intersection
     */
    private static boolean sat(Tuple[] coords, VectorRW axis, double min, double max){
        final double[] range = project(coords, axis);
        return !(range[1] < min || range[0] > max);
    }

    private static double[] project(Tuple[] coords, VectorRW axis){
        final double[] range = {Double.POSITIVE_INFINITY,Double.NEGATIVE_INFINITY};
        for (int i=0;i<coords.length;i++){
            final double val = axis.dot(coords[i]);
            range[0] = Maths.min(range[0], val);
            range[1] = Maths.max(range[1], val);
        }
        return range;
    }


}
