
package science.unlicense.geometry.impl;


/**
 * A vertex is a node/corner/joint in an indexed geometry.
 * Vertices may store more then just position informations.
 *
 * @author Johann Sorel
 */
public interface Vertex extends Fragment {

    int getIndex();

}
