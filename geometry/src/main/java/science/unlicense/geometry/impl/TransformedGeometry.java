
package science.unlicense.geometry.impl;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.impl.path.TransformedPathIterator;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public abstract class TransformedGeometry extends AbstractGeometry {

    protected Geometry base;
    protected Transform transform;

    private TransformedGeometry(PlanarGeometry base, Transform transform) {
        super(base.getCoordinateSystem());
        CObjects.ensureNotNull(base);
        CObjects.ensureNotNull(transform);
        if (base.getDimension() != transform.getInputDimensions()) {
            throw new InvalidArgumentException("PathIterator and Transform do not have the same dimension.");
        }
        if (transform.getInputDimensions() != transform.getOutputDimensions()) {
            throw new InvalidArgumentException("Transform input and output dimensions are different.");
        }
        this.base = base;
        this.transform = transform;
    }

    public Geometry getBase() {
        return base;
    }

    public void setBase(Geometry base) {
        this.base = base;
    }

    public Transform getTransform() {
        return transform;
    }

    public void setTransform(Transform transform) {
        this.transform = transform;
    }

    public static T2D create(PlanarGeometry geom, Transform transform) {
        return new T2D(geom, transform);
    }

    public static class T2D extends TransformedGeometry implements PlanarGeometry {

        public T2D(PlanarGeometry geom, Transform transform) {
            super(geom, transform);
        }

        public PathIterator createPathIterator() {
            return new TransformedPathIterator( ((PlanarGeometry) base).createPathIterator(), transform);
        }

        @Override
        public BBox getBoundingBox() {
            final BBox bbox = new BBox(2);
            final PathIterator ite = createPathIterator();
            final Vector2f64 coord = new Vector2f64();
            boolean first = true;
            while (ite.next()){
                //TODO this is not accurate, it doesnt take in consideration the curve types.
                if (ite.getType() == PathIterator.TYPE_CLOSE) continue;
                ite.getPosition(coord);
                if (first){
                    first = false;
                    bbox.setRange(0, coord.x, coord.x);
                    bbox.setRange(1, coord.y, coord.y);
                } else {
                    if (bbox.getMin(0)>coord.x) bbox.setRange(0, coord.x,  bbox.getMax(0));
                    if (bbox.getMax(0)<coord.x) bbox.setRange(0, bbox.getMin(0),coord.x);
                    if (bbox.getMin(1)>coord.y) bbox.setRange(1, coord.y,  bbox.getMax(1));
                    if (bbox.getMax(1)<coord.y) bbox.setRange(1, bbox.getMin(1),coord.y);
                }

            }
            return bbox;
        }

    }

}
