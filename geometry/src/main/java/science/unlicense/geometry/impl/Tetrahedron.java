
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public class Tetrahedron extends AbstractGeometry {

    private final TupleRW first;
    private final TupleRW second;
    private final TupleRW third;
    private final TupleRW fourth;

    public Tetrahedron(TupleRW first, TupleRW second, TupleRW third, TupleRW fourth) {
        super(3);
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
    }

    public TupleRW getFirstCoord(){
        return first;
    }

    public TupleRW getSecondCoord(){
        return second;
    }

    public TupleRW getThirdCoord(){
        return third;
    }

    public TupleRW getFourthCoord(){
        return fourth;
    }

    @Override
    public BBox getBoundingBox() {
        final BBox bbox = new BBox(cs);
        bbox.getLower().set(first);
        bbox.getUpper().set(first);
        bbox.expand(second);
        bbox.expand(third);
        bbox.expand(fourth);
        return bbox;
    }

}
