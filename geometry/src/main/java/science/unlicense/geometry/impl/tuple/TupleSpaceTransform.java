
package science.unlicense.geometry.impl.tuple;

import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.common.api.number.Float64;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.Operations;
import science.unlicense.geometry.api.operation.TransformOp;
import science.unlicense.geometry.api.tuple.DefaultTupleSpaceCursor;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public class TupleSpaceTransform implements TupleSpace {

    private Transform sampleTransform;
    private Transform coordinateTransform;
    private TupleSpace source;
    private Geometry cachedGeometry;

    public TupleSpaceTransform() {
    }

    public TupleSpaceTransform(TupleSpace source, Transform sampleTransform, Transform coordinateTransform) {
        this.sampleTransform = sampleTransform;
        this.coordinateTransform = coordinateTransform;
        this.source = source;
    }

    public TupleSpace getSource() {
        return source;
    }

    public void setSource(TupleSpace source) {
        this.source = source;
    }

    public Transform getSampleTransform() {
        return sampleTransform;
    }

    public void setSampleTransform(Transform sampleTransform) {
        this.sampleTransform = sampleTransform;
    }

    public Transform getCoordinateTransform() {
        return coordinateTransform;
    }

    public void setCoordinateTransform(Transform coordinateTransform) {
        this.coordinateTransform = coordinateTransform;
        cachedGeometry = null;
    }

    @Override
    public SampleSystem getCoordinateSystem() {
        if (coordinateTransform != null) {
            return coordinateTransform.getOutputSystem();
        } else {
            return source.getCoordinateSystem();
        }
    }

    @Override
    public SampleSystem getSampleSystem() {
        if (sampleTransform != null) {
            return sampleTransform.getOutputSystem();
        } else {
            return source.getSampleSystem();
        }
    }

    @Override
    public ArithmeticType getNumericType() {
        return source.getNumericType();
    }

    @Override
    public Geometry getCoordinateGeometry() {
        if (coordinateTransform != null) {
            if (cachedGeometry == null) {
                final TransformOp trs = new TransformOp(source.getCoordinateGeometry(), coordinateTransform);
                try {
                    cachedGeometry = (Geometry) Operations.execute(trs);
                } catch (OperationException ex) {
                    throw new RuntimeException(ex.getMessage(), ex);
                }
            }
            return cachedGeometry;
        } else {
            return source.getCoordinateGeometry();
        }
    }

    @Override
    public TupleRW createTuple() {
        if (sampleTransform != null) {
            return Vectors.create(sampleTransform.getOutputSystem(), source.getNumericType());
        } else {
            return source.createTuple();
        }
    }

    @Override
    public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
        //transform coordinate
        if (coordinateTransform != null) {
            TupleRW coordBuffer = Vectors.create(coordinateTransform.getOutputSystem(), Float64.TYPE);
            coordinateTransform.transform(coordinate, coordBuffer);
            coordinate = coordBuffer;
        }
        //transform value
        if (sampleTransform != null) {
            TupleRW valueBuffer = Vectors.create(source.getSampleSystem(), Float64.TYPE);
            source.getTuple(coordinate, valueBuffer);
            sampleTransform.transform(valueBuffer, buffer);
        } else {
            source.getTuple(coordinate, buffer);
        }
        return buffer;
    }

    @Override
    public TupleSpaceCursor cursor() {
        return new DefaultTupleSpaceCursor(this);
    }

}
