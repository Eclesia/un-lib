
package science.unlicense.geometry.impl;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.transform.BBoxCalculator;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractGeometry extends CObject implements Geometry {

    private Dictionary userProperties = null;

    protected CoordinateSystem cs = CoordinateSystems.UNDEFINED_2D;
    protected int dimension;

    protected AbstractGeometry(int dimension) {
        this.dimension = dimension;
        this.cs = CoordinateSystems.undefined(dimension);
    }

    protected AbstractGeometry(CoordinateSystem cs) {
        CObjects.ensureNotNull(cs);
        this.cs = cs;
        this.dimension = cs.getDimension();
    }

    @Override
    public final CoordinateSystem getCoordinateSystem() {
        return cs;
    }

    @Override
    public final void setCoordinateSystem(CoordinateSystem cs) {
        CObjects.ensureNotNull(cs);
        this.cs = cs;
        this.dimension = cs.getDimension();
    }

    @Override
    public final int getDimension() {
        return dimension;
    }

    @Override
    public synchronized Dictionary getUserProperties() {
        if (userProperties==null) userProperties = new HashDictionary();
        return userProperties;
    }

    @Override
    public BBox getBoundingBox() {
        return new BBoxCalculator().computeBBox(this);
    }

}
