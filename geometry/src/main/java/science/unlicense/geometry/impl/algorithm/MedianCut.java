
package science.unlicense.geometry.impl.algorithm;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Hasher;
import science.unlicense.common.api.collection.HasherDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.math.api.Maths;

/**
 *
 * Algorithm infos :
 * http://en.literateprograms.org/Median_cut_algorithm_%28C_Plus_Plus%29
 * https://en.wikipedia.org/wiki/Median_cut
 *
 *
 * @author Johann Sorel
 */
public final class MedianCut {

    private static final Sorter WIDTH_SORTER = new Sorter() {
        public int sort(Object first, Object second) {
            return Double.compare(((MBBox) second).getMaxSpan(), ((MBBox) first).getMaxSpan());
        }
    };

    private MedianCut(){}

    private static class TupleHasher implements Hasher {

        private final int size;

        public TupleHasher(int size) {
            this.size = size;
        }

        public int getHash(Object candidate) {
            final double[] d = (double[]) candidate;
            double s = 0;
            for (int i=0;i<size;i++){
                s += d[i];
            }
            return (int) s;
        }

        public boolean equals(Object a, Object b) {
            return Arrays.equals((double[]) a,0,size,(double[]) b,0);
        }
    };

    private static class ByAxisSort implements Sorter{

        private final int index;

        public ByAxisSort(int index) {
            this.index = index;
        }

        public int sort(Object first, Object second) {
            return Double.compare(((double[]) first)[index], ((double[]) second)[index]);
        }
    }

    /**
     * Create a sequence of nbBbox BBox for given tuples.
     *
     * @param tuples
     * @param nbBbox number of wished bbox
     * @return Sequence of BBox equals nbBox or less if not enough points.
     */
    public static Sequence medianCut(TupleGrid tuples, int nbBbox){

        final Sequence bboxes = new ArraySequence(nbBbox);

        //we add 1 to store the number of values
        final int nbSample = tuples.getSampleSystem().getNumComponents()+1;
        final Dictionary dico = new HasherDictionary(new TupleHasher(nbSample-1));
        final MBBox root = new MBBox(nbSample-1);
        final TupleGridCursor cursor = tuples.cursor();
        while (cursor.next()) {
            final double[] t = new double[nbSample];
            cursor.samples().toDouble(t, 0);
            final double[] value = (double[]) dico.getValue(t);
            if (value!=null){
                value[nbSample-1]++;
            } else {
                t[nbSample-1] = 1;
                dico.add(t, t);
            }
        }

        final Iterator tite = dico.getKeys().createIterator();
        while (tite.hasNext()){
            root.add((double[]) tite.next());
        }

        bboxes.add(root);

        for (nbBbox--;nbBbox>0;nbBbox--){
            //split the largest bbox
            Collections.sort(bboxes, WIDTH_SORTER);
            final MBBox bbox = (MBBox) bboxes.get(0);
            final MBBox half = split(bbox);
            bboxes.add(half);
        }

        return bboxes;
    }

    private static MBBox split(MBBox bbox){
        if (bbox.points.getSize()<2){
            throw new RuntimeException("Can not split box with less then 2 points");
        }

        final int dimension = bbox.getDimension();
        final int axisIndex = bbox.getMaxSpanDim();
        //sort points by position on this axis
        Collections.sort(bbox.points, new ByAxisSort(axisIndex));
        final int half = bbox.nbTuple/2;

        final MBBox splitBbox = new MBBox(bbox.getDimension());
        int count = 0;
        for (int n=bbox.points.getSize()-1;;n--){
            final double[] c = (double[]) bbox.points.get(n);
            if (count>0 && count+c[dimension] > half){
                break;
            }
            count += c[dimension];
            splitBbox.add(c);
            bbox.points.remove(n);
        }
        bbox.recompute();
        return splitBbox;
    }

    private static class MBBox extends BBox {

        private final Sequence points = new ArraySequence();
        private int nbTuple=0;

        public MBBox(int dim) {
            super(dim);
        }

        public void add(double[] t){
            if (points.isEmpty()){
                lower.set(t);
                upper.set(t);
            } else {
                expand(t);
            }
            points.add(t);
            nbTuple += (int) t[dimension];
        }

        public void recompute(){
            double[] t = (double[]) points.get(0);
            lower.set(t);
            upper.set(t);
            nbTuple = (int) t[dimension];
            for (int i=1,n=points.getSize();i<n;i++){
                t = (double[]) points.get(i);
                expand(t);
                nbTuple += (int) t[dimension];
            }
        }

        public int getMaxSpanDim(){
            int index = 0;
            double maxSpan = getSpan(0);
            double d;
            for (int i=1;i<dimension;i++){
                d = getSpan(i);
                if (d>maxSpan){
                    maxSpan = d;
                    index = i;
                }
            }
            return index;
        }

        public double getMaxSpan(){
            double maxSpan = getSpan(0);
            for (int i=1;i<dimension;i++){
                maxSpan = Maths.max(maxSpan, getSpan(i));
            }
            return maxSpan;
        }

    }

}
