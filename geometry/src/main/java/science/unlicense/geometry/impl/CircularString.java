
package science.unlicense.geometry.impl;

import science.unlicense.common.api.number.Float64;
import science.unlicense.geometry.api.Curve;
import science.unlicense.geometry.api.operation.BoundingCircleOp;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.path.AbstractStepPathIterator;
import science.unlicense.geometry.impl.path.PathStep2D;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_CircularString
 *   An ST_CircularString value has circular interpolation between ST_Point values
 *
 * @author Johann Sorel
 */
public class CircularString extends AbstractPlanarGeometry implements Curve {

    private final TupleGrid1D coords;

    public CircularString() {
        this(new InterleavedTupleGrid1D(Float64.TYPE, new UndefinedSystem(2), 0));
    }

    public CircularString(TupleGrid1D coords) {
        this.coords = coords;
    }

    public TupleGrid1D getCoordinates() {
        return coords;
    }

    /**
     * A CircularString isclosed if first coordinate equals last.
     * @return true if string is closed.
     */
    public boolean isClosed(){
        final TupleRW t0 = coords.createTuple();
        final TupleRW tn = coords.createTuple();
        coords.getTuple(0, t0);
        coords.getTuple(coords.getDimension()-1, tn);
        return t0.equals(tn);
    }

    public PathIterator createPathIterator() {
        return new CircularPathIterator();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CircularString other = (CircularString) obj;
        if (!this.coords.equals(other.coords)) {
            return false;
        }
        return true;
    }

    private class CircularPathIterator extends AbstractStepPathIterator{

        private TupleGridCursor cursor;
        private final double[] a = new double[2];
        private final double[] b = new double[2];
        private final double[] c = new double[2];
        private int index = -1;

        public CircularPathIterator() {
            cursor = coords.cursor();
        }

        public void reset() {
            index = -1;
            cursor = coords.cursor();
        }

        public boolean next() {

            //TODO not right but at least it has a close shape of a circle.
            index++;
            if (index>=coords.getDimension()){
                return false;
            } else if (index==0){
                cursor.moveTo(index);
                currentStep = new PathStep2D(PathIterator.TYPE_MOVE_TO, cursor.samples().get(0), cursor.samples().get(1));
            } else if (index==1){
                cursor.moveTo(index-1); cursor.samples().toDouble(a, 0);
                cursor.moveTo(index  ); cursor.samples().toDouble(b, 0);
                cursor.moveTo(index+1); cursor.samples().toDouble(c, 0);
                final double[] center = BoundingCircleOp.calculateCircleCenter(a, b, c);
                final double[] sym = Geometries.calculateAxisSymmetry(a, b, center);
                currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC, sym[0], sym[1], b[0], b[1]);
            } else {
                cursor.moveTo(index-2); cursor.samples().toDouble(a, 0);
                cursor.moveTo(index-1); cursor.samples().toDouble(b, 0);
                cursor.moveTo(index  ); cursor.samples().toDouble(c, 0);
                final double[] center = BoundingCircleOp.calculateCircleCenter(a, b, c);
                final double[] sym = Geometries.calculateAxisSymmetry(b, c, center);
                currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC, sym[0], sym[1], c[0], c[1]);
            }

            return true;
        }

    }

}
