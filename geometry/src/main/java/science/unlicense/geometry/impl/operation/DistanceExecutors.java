
package science.unlicense.geometry.impl.operation;

import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.operation.AbstractBinaryOperation;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.impl.OBBox;
import science.unlicense.geometry.api.Point;
import static science.unlicense.geometry.api.operation.DistanceOp.distance;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Sheet;
import science.unlicense.geometry.impl.Sphere;

/**
 *
 * @author Johann Sorel
 */
public class DistanceExecutors {

    private DistanceExecutors(){}

    public static final AbstractBinaryOperationExecutor POINT_POINT =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Point.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point) first;
            final Point b = (Point) second;
            return distance(a.getCoordinate(), b.getCoordinate());
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_CIRCLE =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Point.class, Circle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a  = (Point) first;
            final Circle b = (Circle) second;
            return distance(a.getCoordinate(), b.getCenter()) - b.getRadius();
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_LINE =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Point.class, Line.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a  = (Point) first;
            final Line b = (Line) second;
            return distance(b.getStart(), b.getEnd(), a.getCoordinate());
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_RECTANGLE =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Point.class, Rectangle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Geometry[] nearest = (Geometry[]) NearestExecutors.POINT_RECTANGLE.execute(op, first, second);
            return distance(((Point) nearest[0]).getCoordinate(), ((Point) nearest[1]).getCoordinate());
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_BBOX =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Point.class, BBox.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Geometry[] nearest = (Geometry[]) NearestExecutors.POINT_BBOX.execute(op, first, second);
            return distance(((Point) nearest[0]).getCoordinate(), ((Point) nearest[1]).getCoordinate());
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_OBBOX =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Point.class, OBBox.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Geometry[] nearest = (Geometry[]) NearestExecutors.POINT_OBBOX.execute(op, first, second);
            return distance(((Point) nearest[0]).getCoordinate(), ((Point) nearest[1]).getCoordinate());
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_PLANE =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Point.class, Plane.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point) first;
            final Plane b = (Plane) second;
            final double d = DistanceOp.distance(a.getCoordinate(), b);
            //negative ditances are within the plan
            return (d<0) ? 0 : d;
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_SHEET =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Point.class, Sheet.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point) first;
            final Sheet b = (Sheet) second;
            return Math.abs(DistanceOp.distance(a.getCoordinate(), b));
        }
    };

    public static final AbstractBinaryOperationExecutor LINE_LINE =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Line.class, Line.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Line line1 = (Line) first;
            final Line line2 = (Line) second;
            final double[] ratio = new double[2];
            final double[] c1 = new double[line1.getStart().getSampleCount()];
            final double[] c2 = new double[line1.getStart().getSampleCount()];
            final double dist = distance(
                                    line1.getStart().toDouble(), line1.getEnd().toDouble(), c1,
                                    line2.getStart().toDouble(), line2.getEnd().toDouble(), c2,
                                    ratio,op.epsilon);
            return dist;
        }
    };

    public static final AbstractBinaryOperationExecutor CIRCLE_CIRCLE =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Circle.class, Circle.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Circle a = (Circle) first;
            final Circle b = (Circle) second;
            return distance(a.getCenter(), b.getCenter()) - a.getRadius() - b.getRadius();
        }
    };

    public static final AbstractBinaryOperationExecutor SPHERE_SPHERE =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Sphere.class, Sphere.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Sphere s1 = (Sphere) first;
            final Sphere s2 = (Sphere) second;
            // distance between sphere center
            final double dist = distance(s1.getCenter(), s2.getCenter());

            // sum of radius
            final double rs = s1.getRadius() + s2.getRadius();
            return dist - rs;
        }
    };

    public static final AbstractBinaryOperationExecutor SPHERE_PLANE =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Sphere.class, Plane.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Sphere a = (Sphere) first;
            final Plane b = (Plane) second;
            final double d = DistanceOp.distance(a.getCenter(), b);
            final double radius = a.getRadius();
            //TODO how should we consider distance when sphere under ?
            //total penetration ? from closest border to plan ?
            return d - radius;
        }
    };

    public static final AbstractBinaryOperationExecutor CAPSULE_CAPSULE =
            new AbstractBinaryOperationExecutor(DistanceOp.class, Capsule.class, Capsule.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Capsule capsule1 = (Capsule) first;
            final Capsule capsule2 = (Capsule) second;
            final double[] ratio = new double[2];
            final double[] c1 = new double[capsule1.getFirst().getSampleCount()];
            final double[] c2 = new double[capsule1.getFirst().getSampleCount()];
            final double dist = distance(
                                    capsule1.getFirst().toDouble(), capsule1.getSecond().toDouble(), c1,
                                    capsule2.getFirst().toDouble(), capsule2.getSecond().toDouble(), c2,
                                    ratio,op.epsilon);
            return dist - capsule1.getRadius() - capsule2.getRadius();
        }
    };

}
