
package science.unlicense.geometry.impl;

import java.util.Objects;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.impl.path.AbstractStepPathIterator;
import science.unlicense.geometry.impl.path.PathStep2D;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Specification :
 *  - WKT/WKB ISO 13249-3 : ST_Triangle
 *   The ST_Triangle type is a subtype of ST_Polygon with an exterior
 *   boundary having exactly four points (the last point being the same
 *   as the first point) and no interior boundaries
 *
 * @author Johann Sorel
 */
public class DefaultTriangle extends SingularGeometry2D implements Triangle {

    private final TupleRW first;
    private final TupleRW second;
    private final TupleRW third;

    public DefaultTriangle(CoordinateSystem cs) {
        super(cs);
        this.first = VectorNf64.createDouble(dimension);
        this.second = VectorNf64.createDouble(dimension);
        this.third = VectorNf64.createDouble(dimension);
    }

    public DefaultTriangle(TupleRW first, TupleRW second, TupleRW third) {
        super(first.getSampleCount());
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public TupleRW getFirstCoord(){
        return first;
    }

    public TupleRW getSecondCoord(){
        return second;
    }

    public TupleRW getThirdCoord(){
        return third;
    }

    /**
     * Calculate the barycentric value in triangle for given point.
     * @param point
     * @return Vector barycentric values
     */
    public Tuple getBarycentricValue(Tuple p){
        return VectorNf64.create(Triangle.getBarycentricValue(
                first.toDouble(), second.toDouble(), third.toDouble(), p.toDouble()));
    }

    /**
     * Calculate triangle normal.
     *
     * @return normal vector
     */
    public VectorRW getNormal(){
        return Geometries.calculateNormal(first, second, third);
    }

    public Polygon toPolygon() {
        final Polyline poly = new Polyline(
            Geometries.toTupleBuffer(new Tuple[]{
                first.copy(),
                second.copy(),
                third.copy(),
                first.copy()
            }));
        return new Polygon(poly, null);
    }

    public BBox getBoundingBox() {
        final BBox bbox = new BBox(first.getSampleCount());
        bbox.getLower().set(first);
        bbox.getUpper().set(first);
        bbox.expand(second);
        bbox.expand(third);
        return bbox;
    }

    @Override
    public int getHash() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultTriangle other = (DefaultTriangle) obj;
        if (!Objects.equals(this.first, other.first)) {
            return false;
        }
        if (!Objects.equals(this.second, other.second)) {
            return false;
        }
        if (!Objects.equals(this.third, other.third)) {
            return false;
        }
        return true;
    }

    @Override
    public PathIterator createPathIterator() {
        return new TriangleIterator();
    }

    private class TriangleIterator extends AbstractStepPathIterator{

        private int index = -1;

        public void reset() {
            index = -1;
        }

        public boolean next() {
            index++;
            if (index == 0){
                currentStep = new PathStep2D(PathIterator.TYPE_MOVE_TO,first.get(0), first.get(1));
            } else if (index == 1){
                currentStep = new PathStep2D(PathIterator.TYPE_LINE_TO,second.get(0), second.get(1));
            } else if (index == 2){
                currentStep = new PathStep2D(PathIterator.TYPE_LINE_TO,third.get(0), third.get(1));
            } else if (index == 3){
                currentStep = new PathStep2D(PathIterator.TYPE_CLOSE);
            } else {
                currentStep = null;
            }

            return currentStep != null;
        }

    }


}
