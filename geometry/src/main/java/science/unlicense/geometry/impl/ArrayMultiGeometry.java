
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.MultiGeometry;
import science.unlicense.geometry.impl.path.ConcatenatePathIterator;
import science.unlicense.geometry.api.path.PathIterator;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_GeomCollection
 *   An ST_GeomCollection is a collection of zero or more ST_Geometry values
 *
 * @author Johann Sorel
 */
public class ArrayMultiGeometry extends AbstractPlanarGeometry implements MultiGeometry {

    private Sequence geometries;

    public ArrayMultiGeometry() {
    }

    public ArrayMultiGeometry(Sequence geometries) {
        this.geometries = geometries;
    }

    @Override
    public Sequence getGeometries() {
        return geometries;
    }

    @Override
    public PathIterator createPathIterator() {
        final Sequence s = new ArraySequence();
        if (geometries!=null){
            for (int i=0,n=geometries.getSize();i<n;i++){
                s.add( ((PlanarGeometry) geometries.get(i)).createPathIterator() );
            }
        }
        return new ConcatenatePathIterator(s);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ArrayMultiGeometry other = (ArrayMultiGeometry) obj;
        if (this.geometries != other.geometries && (this.geometries == null || !this.geometries.equals(other.geometries))) {
            return false;
        }
        return true;
    }



}
