
package science.unlicense.geometry.impl.triangulate;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.operation.NearestOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.TupleNf64;
import science.unlicense.math.impl.Vector2f64;

/**
 * Triangulation using ear clipping algorithm.
 * http://en.wikipedia.org/wiki/Polygon_triangulation
 *
 * @author Johann Sorel
 */
public class EarClipping {

    private static final Sorter X_SORTER = new Sorter() {
        public int sort(Object first, Object second) {
            final double m1 = ((PlanarGeometry) first).getBoundingBox().getMin(0);
            final double m2 = ((PlanarGeometry) second).getBoundingBox().getMin(0);
            double d = m2-m1;
            return (d<0) ? -1 : (d>0) ? +1 : 0;
        }
    };
    private static final class SimplePolygon{
        private Polyline outter;
        private final Sequence inners = new ArraySequence();
        /**
         * direction of the outter loop
         */
        private boolean outterIsClockwise;
    }

    private final Sequence triangles = new ArraySequence();

    /**
     * A path can be composed of multiple parts.
     * Each part is a closed polyline with inner holes.
     */
    private final Sequence parts = new ArraySequence();

    /** number of coordinates, may be inferior to coords length. */
    private int nbCoords;
    /** current coordinates to triangulate. */
    private TupleRW[] coords;
    /**
     * store the type of angle made by the coordinate
     * avoid recalculate it each time.
     * 0 : not calculated
     * 1 : convex
     * 2 : concave
     */
    private int[] coordType;

    //current segment analyzed
    private int indexPrevious;
    private int index;
    private int indexNext;
    private TupleRW t1; //previous point
    private TupleRW t2; //point at index
    private TupleRW t3; //next point

    private void reset(){
        //reset values
        triangles.removeAll();
        nbCoords = 0;
        coords = null;
        coordType = null;
        parts.removeAll();
    }

    /**
     *
     * @param geometry geometry to triangulate
     * @return Sequence of Triangle
     */
    public Sequence triangulate(PlanarGeometry geometry) throws OperationException{

        if (geometry instanceof DefaultTriangle){
            geometry = ((DefaultTriangle) geometry).toPolygon();
        } else if (geometry instanceof Rectangle){
            geometry = ((Rectangle) geometry).toPolygon();
        }

        //start by collecting all points
        if (geometry instanceof Polygon){
            reset();

            final SimplePolygon part = new SimplePolygon();
            //copy collection to avoid modifications
            TupleGrid1D coords = ((Polygon) geometry).getExterior().getCoordinates();
            TupleGrid1D cp = new InterleavedTupleGrid1D(coords);
            part.outter = new Polyline(cp);

            final Sequence inners = ((Polygon) geometry).getInteriors();
            if (inners != null){
                for (int i=0,n=inners.getSize();i<n;i++){
                    Polyline inner = (Polyline) inners.get(i);
                    TupleGrid1D cpi = new InterleavedTupleGrid1D(inner.getCoordinates());
                    part.inners.add(new Polyline(cpi));
                }
            }

            run(part);

            return triangles;

        } else {
            final PathIterator ite = geometry.createPathIterator();
            return triangulate(ite);
        }

    }

    /**
     *
     * @param ite geometry to triangulate
     * @return Sequence of Triangle
     */
    public Sequence triangulate(PathIterator ite) throws OperationException{
        reset();

        //collect all loops and rebuild simple polygons
        //we expect to find parts ordered correctly :
        // outter1,inner1,inner2,inner3,outter2,inner1,inner2...
        SimplePolygon current = null;
        while (ite.next()){
            final Polyline loop = nextLoop(ite);
            if (loop == null) continue;

            final boolean clockwise = Geometries.isClockWise(loop.getCoordinates());

            if (current == null){
                current = new SimplePolygon();
                current.outterIsClockwise = clockwise;
            }

            if (current.outterIsClockwise == clockwise){
                //this is an outter loop
                if (current.outter != null){
                    //outter is already defined, switch to a new simple polygon
                    parts.add(current);
                    current = new SimplePolygon();
                    current.outterIsClockwise = clockwise;
                }
                current.outter = loop;
            } else {
                //this is an inner loop
                current.inners.add(loop);
            }
        }

        if (current!=null) parts.add(current);

        for (int i=0,n=parts.getSize();i<n;i++){
            run((SimplePolygon) parts.get(i));
        }

        return triangles;
    }

    private Polyline nextLoop(PathIterator ite){
        //start by collecting all points
        final Sequence coords = new ArraySequence();
        final TupleRW start = new Vector2f64();
        do {
            final int type = ite.getType();
            if (type==PathIterator.TYPE_MOVE_TO){
                ite.getPosition(start);
            } else if (type==PathIterator.TYPE_CLOSE){
                coords.add(start);
                break;
            }
            coords.add(ite.getPosition(new Vector2f64()));
        } while (ite.next());

        if (coords.isEmpty()) return null;

        //ensure last point == first point
        if (!coords.get(0).equals(coords.get(coords.getSize()-1))){
            coords.add( ((Tuple) coords.get(0)).copy() );
        }

        return new Polyline(Geometries.toTupleBuffer(coords));
    }


    private void run(SimplePolygon part) throws OperationException{

        //build a single geometry linking inner holes.
        final Sequence borderCoords = new ArraySequence();
        borderCoords.addAll(Geometries.toTupleArray(part.outter.getCoordinates()));
        //sort inner holes by minimum x value
        orderHoles(part);

        //attach holes to the main geometry
        for (int i=0,n=part.inners.getSize();i<n;i++){
            //we must find the minimum x coordinate in the inner loop
            final Sequence loop = new ArraySequence( Geometries.toTupleArray( ((Polyline) part.inners.get(i)).getCoordinates()));
            int index = 0;
            Tuple min = (Tuple) loop.get(index);
            for (int k=1,p=loop.getSize();k<p;k++){
                Tuple candidate = (Tuple) loop.get(1);
                if (candidate.get(0) < min.get(0)){
                    min = candidate;
                    index = k;
                }
            }

            //now find the closest point on the outter loop
            final Sequence line2 = new ArraySequence();
            line2.add(min);line2.add(min);
            final double[] buffer1 = new double[2];
            final double[] buffer2 = new double[2];
            final double[] ratio = new double[2];
            final int[] offset = new int[2];
            NearestOp.nearest(borderCoords, buffer1, line2, buffer2, ratio, offset, 0.000000001);

            //make a cut from outter loop to inner loop
            int insertIndex = offset[0]+1;
            borderCoords.add(insertIndex, new TupleNf64(buffer1));
            insertIndex++;
            //remove the duplicateion inner loop end point
            loop.remove(loop.getSize()-1);
            for (int k=index,p=loop.getSize();k<p;k++,insertIndex++){
                borderCoords.add(insertIndex,loop.get(k));
            }
            for (int k=0;k<=index;k++,insertIndex++){
                borderCoords.add(insertIndex,loop.get(k));
            }
            borderCoords.add(insertIndex, new TupleNf64(buffer1));

        }

        //remove any neighor points overlaping
        Tuple t = (Tuple) borderCoords.get(0);
        for (int i=1,n=borderCoords.getSize();i<n;i++){
            Tuple candidate = (Tuple) borderCoords.get(i);
            if (candidate.equals(t)){
                borderCoords.remove(i);
                i--;
                n--;
            } else {
                t = candidate;
            }
        }



        final boolean clockwise = Geometries.isClockWise(borderCoords);

        nbCoords = borderCoords.getSize();
        coords = new TupleRW[nbCoords];
        coordType = new int[nbCoords];
        Collections.copy(borderCoords, coords, 0);

        //flip coordinates if not clockwise
        if (!clockwise){
            Arrays.reverse(coords, 0, coords.length);
        }

        //now cut ears progressively
        int nbcut = 0;
        while (nbCoords > 3){
            nbcut = 0;
            for (index=0;index<nbCoords-2 && nbCoords>3;){
                indexPrevious = (index==0) ? nbCoords-2 : index-1 ;
                indexNext = index+1;

                t1 = coords[indexPrevious];
                t2 = coords[index];
                t3 = coords[indexNext];

                if (isConvex() && isEar()){
                    //reset angle type, we remove a point so it might change
                    coordType[indexPrevious]=0;
                    coordType[indexNext]=0;

                    triangles.add(new DefaultTriangle(t1,t2,t3));
                    Arrays.removeWithin(coords, index);
                    Arrays.removeWithin(coordType, index);
                    nbCoords--;
                    nbcut++;
                    //we do not increment since we have remove the point
                } else {
                    index++;
                }
            }

            if (nbcut == 0){
                //this should not happen if the geometry is correct
                //System.out.println("Triangulation failed. no ear to cut.");
                return;
            }
        }
    }

    /**
     * Sort inner holes by minimum x value.
     */
    private void orderHoles(SimplePolygon part){
        Collections.sort(part.inners, X_SORTER);
    }


    /**
     * @return true if  currentsegment is convex
     */
    private boolean isConvex(){
        if (coordType[index]==0){
            //calculate angle type
            final double side = Geometries.lineSide(t1.toDouble(), t3.toDouble(), t2.toDouble());
            coordType[index] = side>0 ? 1 : 2;
        }
        return coordType[index] == 1;
    }

    /**
     * @return true if segment is convex
     */
    private boolean isConvex(int idx){
        if (coordType[idx]==0){
            //calculate angle type
            final Tuple s1 = coords[(idx==0) ? (nbCoords-2) : (idx-1)];
            final Tuple s2 = coords[idx];
            final Tuple s3 = coords[idx+1];
            final double side = Geometries.lineSide(s1.toDouble(), s3.toDouble(), s2.toDouble());
            coordType[idx] = side>0 ? 1 : 2;
        }
        return coordType[idx] == 1;
    }

    /**
     * Check this segment triangle is an ear.
     * Does not contain any other point.
     * @return
     * @throws OperationException
     */
    private boolean isEar() throws OperationException {
        final double[] a = t1.toDouble();
        final double[] b = t2.toDouble();
        final double[] c = t3.toDouble();

        for (int i=0; i<nbCoords-1; i++){
            //test only concave points
            if (!isConvex(i)){
                //check it's not one of the current segment
                if (i!=index && i!=indexNext && i!=indexPrevious){
                    //check if it's in the segment triangle
                    if (Geometries.inTriangle(a, b, c, coords[i].toDouble())){
                        return false;
                    }
                }
            }
        }

        return true;
    }

}
