
package science.unlicense.geometry.impl.tuple;

import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;

/**
 *
 * @author Johann Sorel
 */
public abstract class DecoratedTupleGrid implements TupleGrid {

    protected abstract TupleGrid getBase();

    @Override
    public Extent.Long getExtent() {
        return getBase().getExtent();
    }

    @Override
    public ArithmeticType getNumericType() {
        return getBase().getNumericType();
    }

    @Override
    public TupleRW createTuple() {
        return getBase().createTuple();
    }

    @Override
    public void setTuple(Tuple coordinate, Tuple buffer) {
        getBase().setTuple(coordinate, buffer);
    }

    @Override
    public void setTuple(BBox box, Tuple sample) {
        getBase().setTuple(box, sample);
    }

    @Override
    public TupleGridCursor cursor() {
        return getBase().cursor();
    }

    @Override
    public SampleSystem getSampleSystem() {
        return getBase().getSampleSystem();
    }

    @Override
    public void getTuple(BBox box, TupleGrid buffer) {
        getBase().getTuple(box, buffer);
    }
}
