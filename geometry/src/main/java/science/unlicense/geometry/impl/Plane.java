
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.MatrixNxN;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vectors;
import science.unlicense.math.impl.malgebra.SingularValueDecomposition;

/**
 * Cuts the universe in 2 half.
 * Any point 'under' the plane is consider within the plane.
 * Use Sheet to have a paper thin plane.
 *
 * TODO Consideration : should this be named HalfUniverse ?
 *
 * @author Johann Sorel
 */
public class Plane extends AbstractGeometry{

    // Plane normal. Points x on the plane satisfy Dot(n,x) = d
    private VectorRW normal;
    // a point on the plane
    private VectorRW point;
    // d = dot(n,p) for a given point p on the plane
    private double d;

    public Plane(VectorRW normal, VectorRW point) {
        super(normal.getSampleCount());
        this.normal = normal;
        this.point = point;
        this.d = normal.dot(point);
    }

    /**
     * Build plan from 3 points.
     * @param a Tuple, not null
     * @param b Tuple, not null
     * @param c Tuple, not null
     */
    public Plane(VectorRW a, VectorRW b, VectorRW c) {
        super(a.getSampleCount());
        normal = Geometries.calculateNormal(a, b, c);
        point = a;
        d = Geometries.calculatePlanD(normal.toDouble(), a.toDouble());
    }

    public VectorRW getNormal() {
        return normal;
    }

    /**
     * A point on the plane.
     */
    public VectorRW getPoint(){
        return point;
    }

    /**
     *
     * @return constant of the plane
     */
    public double getD() {
        return d;
    }

    public BBox getBoundingBox() {
        //TODO not right, if normal is perfectly on an axis then the bbox is infnite
        //on another axi.
        final BBox bbox = new BBox(getCoordinateSystem());
        for (int i=0,n=getDimension();i<n;i++){
            bbox.setRange(i, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
        }
        return bbox;
    }

    /**
     * Plane surface is infinite.
     * @return infinite
     */
    public double getArea() {
        return Double.POSITIVE_INFINITY;
    }

    /**
     * Plane volume is infinite.
     * @return infinite
     */
    public double getVolume() {
        return Double.POSITIVE_INFINITY;
    }

    /**
     * Compute the best fitting plane using singular value decomposition.
     *
     * NOTE JSorel : I'm not a math expert, this needs to be confirmed by a real one.
     *
     * Resources :
     * http://en.wikipedia.org/wiki/Singular_value_decomposition
     * http://stackoverflow.com/questions/9243645/weighted-least-square-fit-a-plane-to-3d-point-set
     * http://math.stackexchange.com/questions/923851/plane-fitting-using-svd
     *
     * @param points
     * @return
     */
    public static Plane computeBestFittingPlan(Tuple[] points){

        final int size = points[0].getSampleCount();

        //compute centroid of points
        final VectorRW centroid = VectorNf64.createDouble(size);
        for (Tuple t : points) centroid.localAdd(t);
        centroid.localScale(1.0/points.length);

        //create matrix
        final MatrixRW m = MatrixNxN.create(points.length, size);
        for (int i=0;i<points.length;i++){
            m.setRow(i, Vectors.subtract(points[i].toDouble(),centroid.toDouble()) );
            //m.set(i, size, 1);
        }

        final SingularValueDecomposition svd = new SingularValueDecomposition(m);

        //normal is the column of the V matrix : called right singular vector
        //with the smallest singulare values
        final double[] singularValues = svd.getSingularValues();
        final MatrixRW v = svd.getV();

        int index = 0;
        for (int i=1;i<singularValues.length;i++){
            if (singularValues[i]<singularValues[index]){
                index = i;
            }
        }

        final VectorRW normal = v.getColumnTuple(index).localNormalize();
        return new Plane(normal, centroid);
    }

    /**
     * Calculate the angle made from this plane with the given vector.
     *
     * @param vector
     * @return angle in radians
     */
    public double calculateAngle(VectorRW vector){
        double l = vector.length();
        final double a = normal.getX()*vector.getX() + normal.getY()*vector.getY() + normal.getZ()*vector.getZ();
        return Math.asin(Math.abs(a)/l);
    }

}
