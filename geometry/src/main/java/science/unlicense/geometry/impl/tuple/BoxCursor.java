
package science.unlicense.geometry.impl.tuple;

import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * A cursor which moves inside a defined BBox.
 *
 * @author Johann Sorel
 */
public class BoxCursor implements TupleGridCursor {

    private final TupleGridCursor base;
    private final BBox bbox;

    private final int dimension;
    protected final VectorRW coordinateD;
    private final int[] dimStart;
    private final int[] dimEnd;
    protected boolean hasNext = true;

    public BoxCursor(TupleGridCursor base, BBox bbox) {
        this.base = base;
        this.bbox = bbox;

        this.dimension = bbox.getDimension();
        coordinateD = VectorNf64.createDouble(dimension);

        this.dimStart = new int[this.dimension];
        this.dimEnd = new int[this.dimension];
        for (int i=0;i<this.dimension;i++){
            dimStart[i] = (int) bbox.getMin(i);
            dimEnd[i] = (int) bbox.getMax(i);
            coordinateD.set(i, dimStart[i]);
        }
        coordinateD.set(0, coordinateD.get(0)-1); //place just before first coordinate

    }

    @Override
    public TupleRW samples() {
        return base.samples();
    }

    @Override
    public Tuple coordinate() {
        return coordinateD;
    }

    @Override
    public void moveTo(Tuple tuple) {
        //verify coordinate
        hasNext = false;
        for (int i=0;i<this.dimension;i++){
            int v = Math.round( (int) tuple.get(i));
            if (v<dimStart[0] || v >= dimEnd[i]) {
                throw new InvalidIndexException("Invalid coordinate, outside of data range");
            }
            hasNext |= (v != dimEnd[i]-1);
            coordinateD.set(i, v);
        }
        base.moveTo(coordinateD);
    }

    @Override
    public void moveTo(int coordinate) {
        if (dimStart.length != 1) {
            throw new InvalidIndexException("Tuple coordinate have more then one coordinate");
        }
        //verify coordinate
        if (coordinate<dimStart[0] || coordinate >= dimEnd[0]) {
            throw new InvalidIndexException("Invalid coordinate, outside of data range");
        }
        hasNext = (coordinate != dimEnd[0]-1);

        //TODO : where is 0,0 ? boox corner or grid corner ?
        this.coordinateD.set(0, coordinate);
        base.moveTo(coordinate);
    }

    @Override
    public boolean next() {
        if (!hasNext) return false;

        coordinateD.set(0, coordinateD.get(0)+1);
        for (int i=0; i<this.dimension; i++) {
            if (coordinateD.get(i) >= dimEnd[i]) {
                //get back to this dimension start
                coordinateD.set(i, dimStart[i]);
                if (i < dimension-1) {
                    //iterator next dimension
                    coordinateD.set(i+1, coordinateD.get(i+1)+1);
                } else {
                    //we have finish
                    hasNext = false;
                }
            }
        }
        if (hasNext) base.moveTo(coordinateD);
        return hasNext;
    }

}
