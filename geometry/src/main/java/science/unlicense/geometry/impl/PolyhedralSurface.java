
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.Curve;
import science.unlicense.geometry.api.Surface2D;
import science.unlicense.common.api.collection.Collection;

/**
 * Specification :
 *  - WKT/WKB ISO 13249-3 : ST_PolyhdrlSurface
 *    The ST_PolyhdrlSurface type is a subtype of ST_Surface composed
 *    of contiguous polygon surfaces (ST_Polygon) connected along
 *    their common boundary curves
 *
 * @author Johann Sorel
 */
public class PolyhedralSurface extends AbstractPlanarGeometry implements Surface2D {

    private Polygon[] polygons;

    public PolyhedralSurface() {
    }

    public PolyhedralSurface(Polygon[] polygons) {
        this.polygons = polygons;
    }

    public Polygon[] getPolygons() {
        return polygons;
    }

    @Override
    public Curve getExterior() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Collection getInteriors() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
