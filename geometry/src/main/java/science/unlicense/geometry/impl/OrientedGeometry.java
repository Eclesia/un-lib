
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.math.api.SimilarityRW;

/**
 * An oriented geometry is a geometry with specific properties
 * and additional rotation,translation properties.
 *
 * @author Johann Sorel
 */
public interface OrientedGeometry extends Geometry {

    /**
     * Get geometry transform.
     * @return NodeTransform, never null
     */
    SimilarityRW getTransform();

    /**
     * Get the bounding box without the orientation rotation and translation
     * applied.
     * @return Bounding box
     */
    BBox getUnorientedBounds();

}
