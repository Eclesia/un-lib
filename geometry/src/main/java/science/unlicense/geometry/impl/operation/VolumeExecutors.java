
package science.unlicense.geometry.impl.operation;

import science.unlicense.geometry.api.operation.VolumeOp;
import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Cone;
import science.unlicense.geometry.impl.ConicalFrustrum;
import science.unlicense.geometry.impl.Cylinder;
import science.unlicense.geometry.impl.Disk;
import science.unlicense.geometry.impl.Ellipsoid;
import science.unlicense.geometry.impl.OBBox3D;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Quad;
import science.unlicense.geometry.impl.Sheet;
import science.unlicense.geometry.impl.Sphere;

/**
 *
 * @author Johann Sorel
 */
public class VolumeExecutors {

    private VolumeExecutors(){}

    public static final AbstractSingleOperationExecutor CAPSULE =
            new AbstractSingleOperationExecutor(VolumeOp.class, Capsule.class){

        public Object execute(Operation operation) {
            final VolumeOp op = (VolumeOp) operation;
            final Capsule geom = (Capsule) op.getGeometry();
            return geom.getVolume();
        }
    };

    public static final AbstractSingleOperationExecutor CONE =
            new AbstractSingleOperationExecutor(VolumeOp.class, Cone.class){

        public Object execute(Operation operation) {
            final VolumeOp op = (VolumeOp) operation;
            final Cone geom = (Cone) op.getGeometry();
            return geom.getVolume();
        }
    };

    public static final AbstractSingleOperationExecutor ELLIPSOID =
            new AbstractSingleOperationExecutor(VolumeOp.class, Ellipsoid.class){

        public Object execute(Operation operation) {
            final VolumeOp op = (VolumeOp) operation;
            final Ellipsoid geom = (Ellipsoid) op.getGeometry();
            return geom.getVolume();
        }
    };

    public static final AbstractSingleOperationExecutor CONICAL_FRUSTRUM =
            new AbstractSingleOperationExecutor(VolumeOp.class, ConicalFrustrum.class){

        public Object execute(Operation operation) {
            final VolumeOp op = (VolumeOp) operation;
            final ConicalFrustrum geom = (ConicalFrustrum) op.getGeometry();
            return geom.getVolume();
        }
    };

    public static final AbstractSingleOperationExecutor CYLINDER =
            new AbstractSingleOperationExecutor(VolumeOp.class, Cylinder.class){

        public Object execute(Operation operation) {
            final VolumeOp op = (VolumeOp) operation;
            final Cylinder geom = (Cylinder) op.getGeometry();
            return geom.getVolume();
        }
    };

    public static final AbstractSingleOperationExecutor DISK =
            new AbstractSingleOperationExecutor(VolumeOp.class, Disk.class){

        public Object execute(Operation operation) {
            final VolumeOp op = (VolumeOp) operation;
            final Disk geom = (Disk) op.getGeometry();
            return geom.getVolume();
        }
    };

    public static final AbstractSingleOperationExecutor OBBOX3D =
            new AbstractSingleOperationExecutor(VolumeOp.class, OBBox3D.class){

        public Object execute(Operation operation) {
            final VolumeOp op = (VolumeOp) operation;
            final OBBox3D geom = (OBBox3D) op.getGeometry();
            return geom.getVolume();
        }
    };

    public static final AbstractSingleOperationExecutor PLANE =
            new AbstractSingleOperationExecutor(VolumeOp.class, Plane.class){

        public Object execute(Operation operation) {
            final VolumeOp op = (VolumeOp) operation;
            final Plane geom = (Plane) op.getGeometry();
            return geom.getVolume();
        }
    };

    public static final AbstractSingleOperationExecutor QUAD =
            new AbstractSingleOperationExecutor(VolumeOp.class, Quad.class){

        public Object execute(Operation operation) {
            final VolumeOp op = (VolumeOp) operation;
            final Quad geom = (Quad) op.getGeometry();
            return geom.getVolume();
        }
    };

    public static final AbstractSingleOperationExecutor SHEET =
            new AbstractSingleOperationExecutor(VolumeOp.class, Sheet.class){

        public Object execute(Operation operation) {
            final VolumeOp op = (VolumeOp) operation;
            final Sheet geom = (Sheet) op.getGeometry();
            return geom.getVolume();
        }
    };

    public static final AbstractSingleOperationExecutor SPHERE =
            new AbstractSingleOperationExecutor(VolumeOp.class, Sphere.class){

        public Object execute(Operation operation) {
            final VolumeOp op = (VolumeOp) operation;
            final Sphere geom = (Sphere) op.getGeometry();
            return geom.getVolume();
        }
    };

}
