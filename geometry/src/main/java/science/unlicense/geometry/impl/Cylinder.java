
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class Cylinder extends AbstractOrientedGeometry {

    private double radius;
    private double height;

    public Cylinder() {
        this(1,1);
    }

    public Cylinder(double height, double radius) {
        super(3);
        this.height = height;
        this.radius = radius;
    }

    /**
     * The base radius of the cylinder.
     */
    public double getRadius() {
        return radius;
    }

    public void setRadius(double r) {
        this.radius = r;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * Calculate the base circle diameter.
     *
     * @return base circle diameter.
     */
    public double getBaseDiameter() {
        return 2*radius;
    }

    /**
     * Calculate the base circle surface area.
     *
     * @return base circle area.
     */
    public double getBaseArea() {
        return Math.PI*radius*radius;
    }

    /**
     * Calculate base circle circumference.
     *
     * @return base circle circumference
     */
    public double getBaseCircumference() {
        return 2*Math.PI*radius;
    }

    /**
     * Calculate lateral surface of the cylinder.
     *
     * @return lateral surface.
     */
    public double getLateralSurfaceArea(){
        return 2d*Math.PI*radius*height;
    }

    /**
     * Calculate surface of the cylinder.
     * Base + Top + lateral surface
     *
     * @return lateral surface.
     */
    public double getArea(){
        return getBaseArea()*2 + getLateralSurfaceArea();
    }

    /**
     * Calculate the cylinder volume.
     *
     * @return volume.
     */
    public double getVolume(){
        return getBaseArea() * height;
    }

    public BBox getUnorientedBounds() {
        return new BBox(
                new Vector3f64(-radius, -height/2, -radius),
                new Vector3f64(radius, height/2, radius)
        );
    }

}
