
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.impl.path.CoordinateTuplePathIterator;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.TupleNf64;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vectors;

/**
 * A Line segment .
 *
 * Specification :
 * - SVG v1.1:9.5 :
 *   The ‘line’ element defines a line segment that starts at one point and ends at another.
 *
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public class DefaultLine extends AbstractPlanarGeometry implements Line {

    private TupleRW start;
    private TupleRW end;

    public DefaultLine(int dimension) {
        this.start = VectorNf64.createDouble(dimension);
        this.end = VectorNf64.createDouble(dimension);
    }

    public DefaultLine(CoordinateSystem cs) {
        super(cs);
        this.start = VectorNf64.createDouble(dimension);
        this.end = VectorNf64.createDouble(dimension);
    }
    public DefaultLine(TupleRW start, TupleRW end) {
        this.start = start;
        this.end = end;
    }

    public DefaultLine(final double x1, final double y1,
                  final double x2, final double y2) {
        this.start = new Vector2f64(x1,y1);
        this.end = new Vector2f64(x2,y2);
    }

    public TupleRW getStart() {
        return start;
    }

    public void setStart(TupleRW start) {
        this.start = start;
    }

    public TupleRW getEnd() {
        return end;
    }

    public void setEnd(TupleRW end) {
        this.end = end;
    }

    // ===== Commun ============================================================

    /**
     * Calculate the line surface area.
     *
     * @return 0.
     */
    public double getArea() {
        return 0.0;
    }

    /**
     * Calculate line length.
     *
     * @return the line's length.
     */
    public double getLength() {
        return Vectors.length(Vectors.subtract(this.end.toDouble(), this.start.toDouble()));
    }

    /**
     * Line bounding box is a box which has as corners both start and end tuples
     * with a width of twice the radius.
     *
     * @return BoundingBox
     */
    public BBox getBoundingBox() {
        final BBox bbox = new BBox(2);
        bbox.setRange( 0,
                Maths.min(this.start.get(0), this.end.get(0)),
                Maths.max(this.start.get(0), this.end.get(0)));
        bbox.setRange( 1,
                Maths.min(this.start.get(1), this.end.get(1)),
                Maths.max(this.start.get(1), this.end.get(1)));
        return bbox;
    }

    /**
     * Bounding circle is centered on the middle of the line and has as radius
     * half-length of the line.
     *
     * @return
     */
    public Circle getBoundingCircle() {
        Tuple lineVector = new TupleNf64( Vectors.subtract(this.end.toDouble(), this.start.toDouble()));
        TupleRW center = new TupleNf64( Vectors.add( this.start.toDouble(), Vectors.scale(lineVector.toDouble(), 0.5)));
        double radius = Vectors.length(lineVector.toDouble())/2.;
        return new Circle(center,radius);
    }

    // Polygon getConvexhull() method defined in AbstractGeometry2D abstract class

    // Geometry buffer(double distance) method  method defined in AbstractGeometry abstract class

    /**
     * Centroid is line's center.
     *
     * @return Point
     */
    public Point getCentroid() {
        Tuple lineVector = new TupleNf64( Vectors.subtract(this.end.toDouble(), this.start.toDouble()));
        TupleRW center = new TupleNf64( Vectors.add( this.start.toDouble(), Vectors.scale(lineVector.toDouble(), 0.5)));
        return new DefaultPoint(center);
    }

    // =========================================================================

    @Override
    public PathIterator createPathIterator() {
        return new CoordinateTuplePathIterator(new Tuple[]{start,end});
    }

}
