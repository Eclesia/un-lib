
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Point;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;

/**
 * A Capsule is a cylinder with half spheres at each end.
 * It can be described as a buffer around a line segment.
 * Also called sphere-swept line.
 * The capsule rotates around the Y axis.
 *
 * @author Johann Sorel
 */
public class Capsule extends AbstractOrientedGeometry {

    private double height;
    private double radius;

    public Capsule(double height, double radius) {
        super(3);
        this.height = height;
        this.radius = radius;
    }

    public Capsule(Tuple first, Tuple second, double radius) {
        super(3);
        final VectorRW temp = VectorNf64.create(second);
        temp.localSubtract(first);

        this.height = temp.length();
        this.radius = radius;

        //calculate rotation
        transform.getRotation().set(Matrix3x3.createRotation(new Vector3f64(0, 1, 0), temp));
        //calculate translation
        temp.set(second);
        temp.localAdd(first);
        temp.localScale(0.5);
        transform.getTranslation().set(temp);
        transform.notifyChanged();

    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * Calculate the bottom point of the capsule axis.
     * Including orientation
     * @return capsule axis bottom point
     */
    public VectorRW getFirst(){
        final VectorRW v = new Vector3f64(0, -height/2, 0);
        return (VectorRW) transform.transform(v, v);
    }

    /**
     * Calculate the top point of the capsule axis.
     * Including orientation
     * @return capsule axis top point
     */
    public VectorRW getSecond(){
        final VectorRW v = new Vector3f64(0, +height/2, 0);
        return (VectorRW) transform.transform(v, v);
    }

    /**
     * Calculate the capsule volume.
     *
     * @return volume.
     */
    public double getVolume(){
        return Math.PI * (radius*radius) * ((4.0/3.0)*radius + height);
    }

    /**
     * Calculate the capsule surface area.
     *
     * @return area.
     */
    public double getArea() {
        return 2 * Math.PI * radius * (2 * radius + height);
    }

    /**
     * Calculate the capsule circumference.
     *
     * @return circumference.
     */
    public double getCircumference(){
        return 2 * Math.PI * radius;
    }

    public Point getCentroid() {
        final VectorRW v = new Vector3f64(0, 0, 0);
        return new DefaultPoint(transform.transform(v, v));
    }

    public BBox getUnorientedBounds() {
        return new BBox(
                new Vector3f64(-radius, -height/2 - radius, -radius),
                new Vector3f64(radius, height/2 + radius, radius)
        );
    }

}
