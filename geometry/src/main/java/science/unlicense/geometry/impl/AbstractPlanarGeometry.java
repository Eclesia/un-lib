
package science.unlicense.geometry.impl;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.impl.path.TransformedPathIterator;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractPlanarGeometry extends AbstractGeometry implements PlanarGeometry {

    protected AbstractPlanarGeometry() {
        super(2);
    }

    protected AbstractPlanarGeometry(int dimension) {
        super(dimension);
    }

    protected AbstractPlanarGeometry(CoordinateSystem cs) {
        super(cs);
    }

    public PathIterator createPathIterator() {
        throw new UnimplementedException("Not supported.");
    }

    public PathIterator createPathIterator(Transform transform) {
        return new TransformedPathIterator(createPathIterator(), transform);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer();
        sb.append(getClass().getSimpleName());
        sb.append(' ');

        final Vector3f64 c = new Vector3f64();
        final PathIterator ite = createPathIterator();
        while (ite.next()){
            if (ite.getType() != PathIterator.TYPE_CLOSE){
                ite.getPosition(c);
                sb.append(CObjects.toChars(c));
                sb.append(',');
            } else {
                sb.append("[CLOSE]");
                sb.append(',');
            }
        }
        return sb.toChars();
    }

}
