
package science.unlicense.geometry.impl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid;

/**
 * A morph target is used in mesh animation.
 * Also called :
 * - Blend Shapes
 * - Blend Keys
 * - Morph target
 *
 * Unlike skining a morph modify the vertices position directly.
 *
 * Morph target are relative to base vertex position.
 * Therefor there value must be added to the original value.
 *
 * @author Johann Sorel
 */
public class MorphTarget {

    private Chars name;
    private TupleGrid vertices;
    private float factor = 0f;

    public MorphTarget() {
    }

    public MorphTarget(TupleGrid vertices) {
        this.vertices = vertices;
    }

    public MorphTarget(Chars name, TupleGrid vertices) {
        this.name = name;
        this.vertices = vertices;
    }

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        this.name = name;
    }

    public TupleGrid getVertices() {
        return vertices;
    }

    public void setVertices(TupleGrid vertices) {
        this.vertices = vertices;
    }

    public float getFactor() {
        return factor;
    }

    public void setFactor(float factor) {
        this.factor = factor;
    }

    public MorphTarget copy() {
        final MorphTarget mt = new MorphTarget();
        mt.name = this.name;
        mt.factor = this.factor;
        mt.vertices = this.vertices;
        return mt;
    }

    public MorphTarget deepCopy() {
        final MorphTarget mt = new MorphTarget();
        mt.name = this.name;
        mt.factor = this.factor;
        mt.vertices = this.vertices==null ? null : new InterleavedTupleGrid1D(this.vertices);
        return mt;
    }

}
