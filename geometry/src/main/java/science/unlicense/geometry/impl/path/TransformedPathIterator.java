
package science.unlicense.geometry.impl.path;

import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class TransformedPathIterator implements PathIterator{

    private final PathIterator sub;
    private final Transform trs;
    private final TupleRW temp;

    public TransformedPathIterator(PathIterator ite, Transform trs) {
        if (ite.getDimension() != trs.getInputDimensions()) {
            throw new InvalidArgumentException("PathIterator and Transform do not have the same dimension.");
        }
        if (trs.getInputDimensions() != trs.getOutputDimensions()) {
            throw new InvalidArgumentException("Transform input and output dimensions are different.");
        }

        this.sub = ite;
        this.trs = trs;
        this.temp = VectorNf64.createDouble(trs.getInputDimensions());
    }

    @Override
    public int getDimension() {
        return trs.getOutputDimensions();
    }

    public void reset() {
        sub.reset();
    }

    public boolean next() {
        return sub.next();
    }

    public int getType() {
        return sub.getType();
    }

    public TupleRW getPosition(final TupleRW buffer) {
        temp.setAll(0.0);
        sub.getPosition(temp);
        trs.transform(temp, temp);
        if (buffer.getSampleCount() == temp.getSampleCount()) {
            buffer.set(temp);
        } else {
            for (int i=0,n=Maths.min(buffer.getSampleCount(), temp.getSampleCount()); i<n; i++) {
                buffer.set(i, temp.get(i));
            }
        }
        return buffer;
    }

    public TupleRW getFirstControl(TupleRW buffer) {
        temp.setAll(0.0);
        sub.getFirstControl(temp);
        trs.transform(temp, temp);
        if (buffer.getSampleCount() == temp.getSampleCount()) {
            buffer.set(temp);
        } else {
            for (int i=0,n=Maths.min(buffer.getSampleCount(), temp.getSampleCount()); i<n; i++) {
                buffer.set(i, temp.get(i));
            }
        }
        return buffer;
    }

    public TupleRW getSecondControl(TupleRW buffer) {
        temp.setAll(0.0);
        sub.getSecondControl(temp);
        trs.transform(temp, temp);
        if (buffer.getSampleCount() == temp.getSampleCount()) {
            buffer.set(temp);
        } else {
            for (int i=0,n=Maths.min(buffer.getSampleCount(), temp.getSampleCount()); i<n; i++) {
                buffer.set(i, temp.get(i));
            }
        }
        return buffer;
    }

    public TupleRW getArcParameters(TupleRW buffer) {
        sub.getArcParameters(buffer);
        buffer = trs.transform(buffer, buffer.copy());
        return buffer;
    }

    public boolean getLargeArcFlag() {
        return sub.getLargeArcFlag();
    }

    public boolean getSweepFlag() {
        return sub.getSweepFlag();
    }

}
