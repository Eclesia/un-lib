

package science.unlicense.geometry.impl.algorithm;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Clipping algorithm.
 *
 * doc :
 * https://en.wikipedia.org/wiki/Sutherland–Hodgman_algorithm
 *
 * @author Johann Sorel
 */
public class SutherlandHodgman {

    private SutherlandHodgman(){}

    /**
     *
     * @param subjectPolygon : sequence of Tuples
     * @param clipPolygon : sequence of Segment
     * @return Sequence of tuple for the result polygon
     */
    public static Sequence clip(Sequence subjectPolygon, Sequence clipPolygon){
        final Sequence outputList = new ArraySequence(subjectPolygon);
        for (int i=0,n=clipPolygon.getSize();i<n;i++){
            final Line clipEdge = (Line) clipPolygon.get(i);
            final Sequence inputList = new ArraySequence(outputList);
            if (inputList.isEmpty()) break;
            outputList.removeAll();

            Tuple start = (Tuple) inputList.get(inputList.getSize()-1);
            for (int k=0,kn=inputList.getSize();k<kn;k++){
                final Tuple end = (Tuple) inputList.get(k);

                if (isInside(clipEdge, end)){
                    if (!isInside(clipEdge, start)){
                        outputList.add(computeIntersection(clipEdge, start, end));
                    }
                    outputList.add(end.copy());
                } else if (isInside(clipEdge, start)){
                    outputList.add(computeIntersection(clipEdge, start, end));
                }
                start = end;
            }
        }

        return outputList;
    }

    private static boolean isInside(Line edge, Tuple point){
        return Geometries.isAtRightOf(
                edge.getStart().toDouble(),
                edge.getEnd().toDouble(),
                point.toDouble());
    }

    private static Tuple computeIntersection(Line edge, Tuple start, Tuple end){
        final TupleRW buffer1 = VectorNf64.createDouble(start.getSampleCount());
        final TupleRW buffer2 = VectorNf64.createDouble(start.getSampleCount());
        final double[] ratio = new double[2];
        DistanceOp.distance(edge.getStart(), edge.getEnd(), buffer1,
                          start, end, buffer2, ratio, 0.000000000000001);
        return buffer1;
    }

}
