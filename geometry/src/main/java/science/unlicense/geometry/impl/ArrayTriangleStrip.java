
package science.unlicense.geometry.impl;

import science.unlicense.common.api.collection.AbstractIterator;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.geometry.api.TriangleStrip;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public class ArrayTriangleStrip extends SingularGeometry2D implements TriangleStrip {

    private TupleGrid1D coordinates;

    public ArrayTriangleStrip(TupleGrid1D coordinates) {
        super(coordinates.getSampleSystem().getNumComponents());
        this.coordinates = coordinates;
    }

    @Override
    public TupleGrid1D getCoordinates() {
        return coordinates;
    }

    @Override
    public Polygon toPolygon() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Iterator decompose() {
        if (coordinates.getDimension() == 0) {
            return Collections.emptyIterator();
        }
        return new Decomposition();
    }

    private final class Decomposition extends AbstractIterator {

        private final TupleGridCursor cursor = coordinates.cursor();
        private TupleRW t1;
        private TupleRW t2;
        private TupleRW t3;

        public Decomposition() {
            cursor.next();
            t1 = cursor.samples().copy();
            cursor.next();
            t2 = cursor.samples().copy();
        }

        @Override
        protected void findNext() {
            if (!cursor.next()) return;
            t3 = cursor.samples().copy();
            nextValue = new DefaultTriangle(t1, t2, t3);

            //prepare next iteration
            t1 = t2;
            t2 = t3;
            t3 = null;
        }
    }

}
