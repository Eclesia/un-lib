

package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class Quad extends AbstractOrientedGeometry {

    private double width;
    private double height;

    /**
     *
     * @param width along X axis
     * @param height along Y axis
     */
    public Quad(double width, double height) {
        super(3);
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public BBox getUnorientedBounds() {
        return new BBox(
                new Vector3f64(-width/2.0, 0.0, -height/2.0),
                new Vector3f64(width/2.0, 0.0, height/2.0)
        );
    }

    /**
     * Calculate quad surface area.
     * width * height
     *
     * @return surface
     */
    public double getArea() {
        return width * height;
    }

    /**
     * A Quad has no volume.
     *
     * @return always 0
     */
    public double getVolume() {
        return 0;
    }

}
