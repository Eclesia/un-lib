
package science.unlicense.geometry.impl.transform;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.geometry.api.Curve;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.MultiGeometry;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.Surface;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.TriangleFan;
import science.unlicense.geometry.api.TriangleStrip;
import science.unlicense.geometry.api.Volume;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.TriangulateOp;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.Arc;
import science.unlicense.geometry.impl.CubicCurve;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.QuadraticCurve;
import science.unlicense.geometry.impl.path.FlattenPathIterator;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public abstract class Decomposer {

    public void process(Geometry geom) {

        if (geom instanceof MultiGeometry) {
            final Iterator ite = ((MultiGeometry) geom).getGeometries().createIterator();
            while (ite.hasNext()) {
                process((Geometry) ite.next());
            }
            return;
        }

        if (geom instanceof Point) {
            processPoint((Point) geom);
        } else if (geom instanceof Line) {
            processLine((Line) geom);
        } else if (geom instanceof CubicCurve) {
            processCubicCurve((CubicCurve) geom);
        } else if (geom instanceof QuadraticCurve) {
            processQuadraticCurve((QuadraticCurve) geom);
        } else if (geom instanceof Arc) {
            processArc((Arc) geom);
        } else if (geom instanceof Triangle) {
            processTriangle((Triangle) geom);
        } else if (geom instanceof TriangleFan) {
            processTriangleFan((TriangleFan) geom);
        } else if (geom instanceof TriangleStrip) {
            processTriangleStrip((TriangleStrip) geom);
        }

        else if (geom instanceof Curve) {
            processCurve((Curve) geom);
        } else if (geom instanceof Surface) {
            processSurface((Surface) geom);
        } else if (geom instanceof Volume) {
            processVolume((Volume) geom);
        } else {
            processOther(geom);
        }
    }

    protected abstract void processPoint(Point geometry);

    protected abstract void processLine(Line geometry);

    protected abstract void processTriangle(Triangle geometry);

    protected void processTriangleFan(TriangleFan geometry) {
        final TupleGridCursor cursor = geometry.getCoordinates().cursor();
        final TupleRW t1;
        TupleRW t2;
        TupleRW t3;

        cursor.next();
        t1 = cursor.samples().copy();
        cursor.next();
        t2 = cursor.samples().copy();

        while (cursor.next()) {
            t3 = cursor.samples().copy();
            process(new DefaultTriangle(t1, t2, t3));
            //prepare next iteration
            t2 = t3;
            t3 = null;
        }

    }

    protected void processTriangleStrip(TriangleStrip geometry) {
        final TupleGridCursor cursor = geometry.getCoordinates().cursor();
        TupleRW t1;
        TupleRW t2;
        TupleRW t3;

        cursor.next();
        t1 = cursor.samples().copy();
        cursor.next();
        t2 = cursor.samples().copy();

        while (cursor.next()) {
            t3 = cursor.samples().copy();
            process(new DefaultTriangle(t1, t2, t3));
            //prepare next iteration
            t1 = t2;
            t2 = t3;
            t3 = null;
        }
    }

    protected abstract void processCubicCurve(CubicCurve geometry);

    protected abstract void processQuadraticCurve(QuadraticCurve geometry);

    protected abstract void processArc(Arc geometry);

    protected abstract void processCurve(Curve geometry);

    protected abstract void processSurface(Surface geometry);

    protected abstract void processVolume(Volume geometry);

    protected abstract void processOther(Geometry geometry);

    protected void decomposeVolumeSurfaces(Volume volume) {
        process(volume.getExterior());
        final Iterator ite = volume.getInteriors().createIterator();
        while (ite.hasNext()) {
            process((Geometry) ite.next());
        }
    }

    protected void decomposeSurfaceCurves(Surface surface) {
        process(surface.getExterior());
        final Iterator ite = surface.getInteriors().createIterator();
        while (ite.hasNext()) {
            process((Geometry) ite.next());
        }
    }

    protected void decomposeCurve(Curve curve) {
        decompose(curve.createPathIterator());
    }

    protected void decomposeToLines(PlanarGeometry geometry, double[] resolution) {
        decompose(new FlattenPathIterator(geometry.createPathIterator(), resolution));
    }

    protected void decomposeToTriangles(Geometry geometry, double resolution) throws OperationException {
        TriangulateOp op = new TriangulateOp(geometry, resolution);
        Geometry result = op.execute();
        process(result);
    }

    protected void decompose(PathIterator ite) {

        final VectorRW current = VectorNf64.createDouble(3);
        final VectorRW previous = VectorNf64.createDouble(3);
        final VectorRW first = VectorNf64.createDouble(3);
        final VectorRW control1 = VectorNf64.createDouble(3);
        final VectorRW control2 = VectorNf64.createDouble(3);
        final VectorRW arcParams = VectorNf64.createDouble(4);

        while (ite.next()) {
            int type = ite.getType();
            if (PathIterator.TYPE_MOVE_TO==type) {
                ite.getPosition(first);
                ite.getPosition(current);

            } else if (PathIterator.TYPE_LINE_TO==type) {
                ite.getPosition(current);
                process(new DefaultLine(previous, current));

            } else if (PathIterator.TYPE_CLOSE==type) {
                process(new DefaultLine(previous, first));

            } else if (PathIterator.TYPE_QUADRATIC==type) {
                ite.getPosition(current);
                ite.getFirstControl(control1);
                process(new QuadraticCurve(previous, current, control1));

            } else if (PathIterator.TYPE_CUBIC==type) {
                ite.getPosition(current);
                ite.getFirstControl(control1);
                ite.getSecondControl(control2);
                process(new CubicCurve(previous, current, control1, control2));

            } else if (PathIterator.TYPE_ARC==type) {
                ite.getPosition(current);
                ite.getFirstControl(control1);
                ite.getSecondControl(control2);
                ite.getArcParameters(arcParams);
                process(new Arc(previous, current, arcParams.getXY(), arcParams.getY(), arcParams.getZ(), ite.getLargeArcFlag(), ite.getSweepFlag()));

            } else {
                throw new RuntimeException("Unknown type " + type);
            }

            previous.set(current);
        }
    }

}
