
package science.unlicense.geometry.impl;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.IntSequence;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Int32;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.TriangulateOp;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class DefaultMesh extends AbstractMesh {

    protected final Dictionary attachments = new HashDictionary();
    protected final Sequence morphs = new ArraySequence();
    protected Chars name = Chars.EMPTY;
    protected TupleGrid index;
    protected IndexedRange[] ranges;

    public DefaultMesh(CoordinateSystem cs) {
        super(cs);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Chars getName() {
        return name;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setName(Chars name) {
        this.name = name;
    }

    @Override
    public TupleGrid getPositions() {
        return (TupleGrid) attachments.getValue(ATT_POSITION);
    }

    public void setPositions(TupleGrid positions) {
        attachments.add(ATT_POSITION, positions);
    }

    @Override
    public TupleGrid getNormals() {
        return (TupleGrid) attachments.getValue(ATT_NORMAL);
    }

    public void setNormals(TupleGrid normals) {
        attachments.add(ATT_NORMAL, normals);
    }

    @Override
    public TupleGrid getTangents() {
        return (TupleGrid) attachments.getValue(ATT_TANGENT);
    }

    public void setTangents(TupleGrid tangents) {
        attachments.add(ATT_TANGENT, tangents);
    }

    @Override
    public TupleGrid getUVs() {
        return (TupleGrid) attachments.getValue(ATT_TEXCOORD_0);
    }

    public void setUVs(TupleGrid uvs) {
        attachments.add(ATT_TEXCOORD_0, uvs);
    }

    @Override
    public TupleGrid getIndex() {
        return index;
    }

    public void setIndex(TupleGrid index) {
        this.index = index;
    }

    @Override
    public IndexedRange[] getRanges() {
        return ranges;
    }

    public void setRanges(IndexedRange[] ranges) {
        this.ranges = ranges;
    }

    @Override
    public Dictionary getAttributes() {
        return attachments;
    }

    @Override
    public Chars[] getMorphNames() {
        final Chars[] names = new Chars[morphs.getSize()];
        for (int i=0;i<names.length;i++) {
            final MorphTarget mf = (MorphTarget) morphs.get(i);
            names[i] = mf.getName();
        }
        return names;
    }

    @Override
    public MorphTarget getMorph(Chars name) {
        for (int i=0,n=morphs.getSize();i<n;i++) {
            final MorphTarget mf = (MorphTarget) morphs.get(i);
            if (name.equals(mf.getName())) {
                return mf;
            }
        }
        return null;
    }

    @Override
    public Sequence getMorphs() {
        return morphs;
    }

    public static Mesh createIcosphere(double m_radius) {

        final float phi = (float) ((1.0 + Math.sqrt(5.0)) / 2.0);
        final float X = (float) (m_radius / Math.sqrt( phi * Math.sqrt( 5.0)));
        final float Z = X * phi;
        final Buffer.Float32 bufferPosition = DefaultBufferFactory.INSTANCE.createFloat32(12*3);
        final Buffer.Float32 bufferNormal = DefaultBufferFactory.INSTANCE.createFloat32(12*3);
        final Buffer.Int32 bufferIndice = DefaultBufferFactory.INSTANCE.createInt32(20*3);

        final Float32Cursor vertexCursor = bufferPosition.cursor();
        final Float32Cursor normalCursor = bufferNormal.cursor();
        final Int32Cursor indiceCursor = bufferIndice.cursor();


        VectorRW vec = VectorNf64.createDouble(3);
        // on crée les 12 points le composant
        vec.setXYZ(-X,  0,  Z);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());
        vec.setXYZ( X,  0,  Z);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());
        vec.setXYZ(-X,  0, -Z);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());
        vec.setXYZ( X,  0, -Z);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());
        vec.setXYZ( 0,  Z,  X);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());
        vec.setXYZ( 0,  Z, -X);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());
        vec.setXYZ( 0, -Z,  X);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());
        vec.setXYZ( 0, -Z, -X);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());
        vec.setXYZ( Z,  X,  0);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());
        vec.setXYZ(-Z,  X,  0);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());
        vec.setXYZ( Z, -X,  0);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());
        vec.setXYZ(-Z, -X,  0);
        vertexCursor.write(vec.toFloat());
        normalCursor.write(vec.localNormalize().toFloat());

        indiceCursor.write( 0).write( 1).write( 4);
        indiceCursor.write( 9).write( 0).write( 4);
        indiceCursor.write( 9).write( 4).write( 5);
        indiceCursor.write( 4).write( 8).write( 5);
        indiceCursor.write( 4).write( 1).write( 8);
        indiceCursor.write( 1).write(10).write( 8);
        indiceCursor.write( 3).write( 8).write(10);
        indiceCursor.write( 3).write( 5).write( 8);
        indiceCursor.write( 2).write( 5).write( 3);
        indiceCursor.write( 7).write( 2).write( 3);
        indiceCursor.write( 3).write(10).write( 7);
        indiceCursor.write( 6).write( 7).write(10);
        indiceCursor.write( 6).write(11).write( 7);
        indiceCursor.write(11).write( 6).write( 0);
        indiceCursor.write( 1).write( 0).write( 6);
        indiceCursor.write( 1).write( 6).write(10);
        indiceCursor.write( 0).write( 9).write(11);
        indiceCursor.write(11).write( 9).write( 2);
        indiceCursor.write( 9).write( 5).write( 2);
        indiceCursor.write( 2).write( 7).write(11);

        final TupleGrid positions = new InterleavedTupleGrid1D(bufferPosition.getBuffer(), Float32.TYPE, UndefinedSystem.create(3), 12);
        final TupleGrid normals = new InterleavedTupleGrid1D(bufferNormal.getBuffer(), Float32.TYPE, UndefinedSystem.create(3), 12);
        final TupleGrid indices = new InterleavedTupleGrid1D(bufferIndice.getBuffer(), Int32.TYPE, UndefinedSystem.create(1), 20*3);

        final DefaultMesh geom = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        geom.setIndex(indices);
        geom.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 60)});

        geom.attachments.add(ATT_POSITION, positions);
        geom.attachments.add(ATT_NORMAL, normals);

        return geom;
    }

    public static Mesh createTerrain(TupleGrid tb){
        final TupleRW samples = tb.createTuple();
        final Vector2i32 coords = new Vector2i32();
        final int width = (int) tb.getExtent().getL(0);
        final int height = (int) tb.getExtent().getL(1);

        //TODO we could use less points in the IBO by using triangle strips.
        final int nbVertex = width*height;
        final float[] vertices = new float[nbVertex*3];
        final IntSequence ib = new IntSequence();
        int k=0;
        for (int y=0;y<height;y++){
            for (int x=0;x<width;x++){
                coords.x = x; coords.y = y;
                tb.getTuple(coords, samples);
                vertices[k++]=x;
                vertices[k++]=(float) (samples.get(0));
                vertices[k++]=y;

                if (y<height-1 && x<width-1){
                    int i1 = y*width + x;
                    int i2 = y*width + x + 1;
                    int i3 = (y+1)*width + x;
                    int i4 = (y+1)*width + x + 1;
                    ib.put(i1);
                    ib.put(i3);
                    ib.put(i2);
                    ib.put(i3);
                    ib.put(i4);
                    ib.put(i2);
                }
            }
        }

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.undefined(3));
        shell.setPositions(new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(vertices), Float32.TYPE, UndefinedSystem.create(3), vertices.length/3));
        shell.setIndex(new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(ib.toArrayInt()), Int32.TYPE, UndefinedSystem.create(1), ib.getSize()));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, ib.getSize())});

        return shell;
    }

    public static DefaultMesh createFromGeometry(Geometry geometry) throws OperationException {
        return createFromGeometry(geometry, 10, 10);
    }

    public static DefaultMesh createFromGeometry(Geometry geometry, int nbRing, int nbSector) throws OperationException {
        final TriangulateOp op = new TriangulateOp(geometry, 0);
        op.setNbRing(nbRing);
        op.setNbSector(nbSector);
        final Mesh ig = (Mesh) op.execute();

        final DefaultMesh shell = new DefaultMesh(ig.getCoordinateSystem());
        shell.getAttributes().addAll(ig.getAttributes());
        shell.setIndex(ig.getIndex());
        shell.setRanges(ig.getRanges());
        return shell;
    }

    public static DefaultMesh createPlan() {
        return createPlan(new Vector3f64(-0.5, -0.5, 0),
             new Vector3f64(-0.5, +0.5, 0),
             new Vector3f64(+0.5, +0.5, 0),
             new Vector3f64(+0.5, -0.5, 0));
    }

    public static DefaultMesh createPlan(VectorRW v1,VectorRW v2,VectorRW v3,VectorRW v4) {
        final Buffer.Float32 vertices = DefaultBufferFactory.INSTANCE.createFloat32(16);
        final Float32Cursor vc = vertices.cursor();
        vc.write(v1.toFloat());
        vc.write(v2.toFloat());
        vc.write(v3.toFloat());
        vc.write(v4.toFloat());

        final Buffer.Int32 indices = DefaultBufferFactory.INSTANCE.createInt32(6);
        indices.write(0, new int[]{
            0,1,2,
            2,3,0
        });

        final DefaultMesh mesh = new DefaultMesh(CoordinateSystems.CARTESIAN3D_METRIC_RIGH_HANDED);
        mesh.setPositions(InterleavedTupleGrid1D.create(vertices.getBuffer(),3));
        mesh.setIndex(InterleavedTupleGrid1D.create(indices.getBuffer(),1));
        mesh.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 6)});
        Mesh.calculateNormals(mesh);
        return mesh;
    }

}
