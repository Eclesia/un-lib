
package science.unlicense.geometry.impl.path;

import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.math.api.TupleRW;

/**
 * Path iterator concatenate several iterator.
 *
 * @author Johann Sorel
 */
public class ConcatenatePathIterator implements PathIterator{

    private final Sequence seq;
    private PathIterator ite;
    private int index = 0;

    public ConcatenatePathIterator(Sequence seq) {
        this.seq = seq;
    }

    @Override
    public int getDimension() {
        return ((PathIterator) seq.get(0)).getDimension();
    }

    public void reset() {
        ite = null;
        index=0;
    }

    public boolean next() {
        if (ite != null && ite.next()){
            return true;
        }
        //move to next iterator
        ite = null;
        do {
            if (seq.getSize()>index){
                ite = (PathIterator) seq.get(index);
                if (!ite.next()){
                    ite = null;
                } else {
                    return true;
                }
            } else {
                break;
            }
            index++;
        } while (ite == null);
        return false;
    }

    public int getType() {
        return ite.getType();
    }

    public TupleRW getPosition(TupleRW buffer) {
        return ite.getPosition(buffer);
    }

    public TupleRW getFirstControl(TupleRW buffer) {
        return ite.getFirstControl(buffer);
    }

    public TupleRW getSecondControl(TupleRW buffer) {
        return ite.getSecondControl(buffer);
    }

    public TupleRW getArcParameters(TupleRW buffer) {
        return ite.getArcParameters(buffer);
    }

    public boolean getLargeArcFlag() {
        return ite.getLargeArcFlag();
    }

    public boolean getSweepFlag() {
        return ite.getSweepFlag();
    }

}
