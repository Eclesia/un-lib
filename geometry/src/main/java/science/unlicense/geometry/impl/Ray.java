
package science.unlicense.geometry.impl;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * A ray is a one direction infinite line.
 * It is build from a position and a direction.
 *
 * @author Johann Sorel
 */
public class Ray extends AbstractGeometry{

    private TupleRW position;
    private VectorRW direction;

    public Ray(int dimension) {
        this(VectorNf64.createDouble(dimension),VectorNf64.createDouble(dimension));
    }

    public Ray(TupleRW position, VectorRW direction) {
        super(position.getSampleCount());
        this.position = position;
        this.direction = direction;
    }

    public TupleRW getPosition() {
        return position;
    }

    public void setPosition(TupleRW position) {
        this.position = position;
    }

    public VectorRW getDirection() {
        return direction;
    }

    public void setDirection(VectorRW direction) {
        this.direction = direction;
    }

    public BBox getBoundingBox() {
        final int dim = direction.getSampleCount();
        final BBox bbox = new BBox(dim);
        for (int i=0;i<dim;i++){
            double d = direction.get(i);
            if (d<0){
                bbox.setRange(i, Double.NEGATIVE_INFINITY, position.get(i));
            } else if (d>0){
                bbox.setRange(i, position.get(i), Double.POSITIVE_INFINITY);
            } else {
                bbox.setRange(i, position.get(i), position.get(i));
            }
        }
        return bbox;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ray other = (Ray) obj;
        if (this.position != other.position && (this.position == null || !this.position.equals(other.position))) {
            return false;
        }
        return this.direction == other.direction || (this.direction != null && this.direction.equals(other.direction));
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("Ray(");
        cb.append(position.toString());
        cb.append(direction.toChars());
        cb.append(')');
        return cb.toChars();
    }

}
