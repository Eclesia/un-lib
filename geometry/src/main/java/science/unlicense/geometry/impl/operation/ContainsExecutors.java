
package science.unlicense.geometry.impl.operation;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Curve;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.operation.AbstractBinaryOperation;
import science.unlicense.geometry.api.operation.ContainsOp;
import static science.unlicense.geometry.api.operation.DistanceOp.distance;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.CurvePolygon;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.api.Tuple;

/**
 *
 * @author Johann Sorel
 */
public class ContainsExecutors {

    private ContainsExecutors(){}

    /**
     * Test if point is within polygon using Winding Number algorithm.
     *
     * http://geomalgorithms.com/a03-_inclusion.html
     * http://en.wikipedia.org/wiki/Point_in_polygon
     *
     * @param pl expected closed polyline, start point == end point
     * @param b point to test
     * @return true if point is inside polygon
     */
    private static boolean isInside(Polyline pl, Point b){
        final TupleGrid1D coords = pl.getCoordinates();
        final TupleGridCursor cursor = coords.cursor();
        final double[] point = b.getCoordinate().toDouble();

        //check invalid polygons
        if (coords.getDimension()< 4) return false;

        int windingNumber=0;
        double[] current = new double[coords.getSampleSystem().getNumComponents()];
        double[] previous = new double[current.length];
        cursor.moveTo(0);
        cursor.samples().toDouble(current,0);
        for (int i=1,n=coords.getDimension();i<n;i++){
            Arrays.copy(current, 0, current.length, previous,0);
            cursor.moveTo(i);
            cursor.samples().toDouble(current,0);

            if (previous[1] <= point[1]){
                if (current[1] > point[1]){
                    if (Geometries.lineSide(previous, current, point) > 0){
                        windingNumber++;
                    }
                }
            } else {
                if (current[1] <= point[1]){
                    if (Geometries.lineSide(previous, current, point) < 0){
                        windingNumber--;
                    }
                }
            }
        }

        //if 0 point is outside
        return windingNumber != 0;
    }


    public static final AbstractBinaryOperationExecutor RECTANGLE_POINT =
            new AbstractBinaryOperationExecutor(ContainsOp.class, Rectangle.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Rectangle rect = (Rectangle) first;
            final Tuple pt       = ((Point) second).getCoordinate();
            final double x = pt.get(0);
            if (x<rect.getX() || x>rect.getX()+rect.getWidth()) return false;
            final double y = pt.get(1);
            if (y<rect.getY() || y>rect.getY()+rect.getHeight()) return false;
            return true;
        }
    };

    public static final AbstractBinaryOperationExecutor BBOX_POINT =
            new AbstractBinaryOperationExecutor(ContainsOp.class, BBox.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final BBox bbox = (BBox) first;
            final Point pt  = (Point) second;
            final int dim = bbox.getDimension();
            for (int i=0;i<dim;i++){
                final double v = pt.getCoordinate().get(i);
                if (v<bbox.getMin(i) || v>bbox.getMax(i)) return false;
            }
            return true;
        }
    };

    public static final AbstractBinaryOperationExecutor TRIANGLE_POINT =
            new AbstractBinaryOperationExecutor(ContainsOp.class, Triangle.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final DefaultTriangle a = (DefaultTriangle) first;
            final Point b = (Point) second;
            final Tuple bary = a.getBarycentricValue(b.getCoordinate());
            return bary.get(1) >= 0.0 && bary.get(2) >= 0.0 && (bary.get(1) + bary.get(2)) <= 1.0;
        }
    };

    public static final AbstractBinaryOperationExecutor CIRCLE_POINT =
            new AbstractBinaryOperationExecutor(ContainsOp.class, Circle.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Circle a  = (Circle) first;
            final Point b = (Point) second;
            final double d = distance(a.getCenter(), b.getCoordinate());
            return d-a.getRadius() < 0;
        }
    };

    public static final AbstractBinaryOperationExecutor CURVEPOLYGON_POINT =
            new AbstractBinaryOperationExecutor(ContainsOp.class, CurvePolygon.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final CurvePolygon a  = (CurvePolygon) first;
            final Point b = (Point) second;

            final Curve c = a.getExterior();
            if (!(c instanceof Polyline)){
                throw new RuntimeException("Curve types not supported yet.");
            }

            final Polyline pl = (Polyline) c;
            boolean inside = isInside(pl, b);

            if (inside){
                //check the holes
                final Sequence holes = a.getInteriors();
                for (int i=0,n=holes.getSize();i<n;i++){
                    final Polyline hole = (Polyline) holes.get(i);
                    if (isInside(hole, b)){
                        //point in a hole
                        return false;
                    }
                }
            }

            return inside;
        }
    };

}
