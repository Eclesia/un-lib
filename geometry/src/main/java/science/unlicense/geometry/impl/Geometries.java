
package science.unlicense.geometry.impl;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.path.FlattenPathIterator;
import science.unlicense.geometry.impl.triangulate.EarClipping;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.api.transform.RearrangeTransform;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vectors;
import static science.unlicense.math.impl.Vectors.cross;
import static science.unlicense.math.impl.Vectors.dot;
import static science.unlicense.math.impl.Vectors.subtract;

/**
 * Utilities for geometries.
 *
 * @author Johann Sorel
 */
public class Geometries {

    private Geometries() {
    }

    /**
     * Calculate normal of triangle made of given 3 points.
     *
     * @param a
     * @param b
     * @param c
     * @return normal
     */
    public static VectorRW calculateNormal(Tuple a, Tuple b, Tuple c) {
        return VectorNf64.create(calculateNormal(a.toDouble(), b.toDouble(), c.toDouble()));
    }

    /**
     * Calculate normal of triangle made of given 3 points.
     *
     * @param a
     * @param b
     * @param c
     * @return normal
     */
    public static double[] calculateNormal(double[] a, double[] b, double[] c) {
        final double[] ab = Vectors.subtract(b, a);
        final double[] ac = Vectors.subtract(c, a);
        final double[] res = Vectors.cross(ab, ac);
        Vectors.normalize(res, res);
        return res;
    }

    /**
     * Calculate normal of triangle made of given 3 points.
     *
     * @param a
     * @param b
     * @param c
     * @param buffer
     * @return normal
     */
    public static float[] calculateNormal(float[] a, float[] b, float[] c, float[] buffer) {
        final float[] ab = Vectors.subtract(b, a);
        final float[] ac = Vectors.subtract(c, a);
        buffer = Vectors.cross(ab, ac, buffer);
        return Vectors.normalize(buffer, buffer);
    }

    /**
     * @param c symmetry center
     * @param p point to reflect
     * @return reflected point
     */
    public static double[] calculatePointSymmetry(double[] c, double[] p) {
        final double[] r = new double[c.length];
        for (int i = 0; i < r.length; i++) r[i] = (2 * c[i]) - p[i];
        return r;
    }

    /**
     * @param a symmetry axis a
     * @param b symmetry axis b
     * @param p point to reflect
     * @return reflected point
     */
    public static double[] calculateAxisSymmetry(double[] a, double[] b, double[] p) {
        final double[] ratio = new double[2];
        final double[] c1 = new double[a.length];
        final double[] c2 = new double[a.length];
        DistanceOp.distance(a, b, c1,
                p, p, c2,
                ratio, 0.000000001);

        return calculatePointSymmetry(c1, p);
    }

    /**
     * Test if given a,b,c,d quad is convex.
     *
     * @param a
     * @param b
     * @param c
     * @param d
     * @return true if convex, false otherwise.
     */
    public static boolean isConvex(Tuple a, Tuple b, Tuple c, Tuple d) {
        final double[] bda = cross(subtract(d.toDouble(), b.toDouble()), subtract(a.toDouble(), b.toDouble()));
        final double[] bdc = cross(subtract(d.toDouble(), b.toDouble()), subtract(c.toDouble(), b.toDouble()));
        if (dot(bda, bdc) >= 0.0) return false;
        final double[] acd = cross(subtract(c.toDouble(), a.toDouble()), subtract(d.toDouble(), a.toDouble()));
        final double[] acb = cross(subtract(c.toDouble(), a.toDouble()), subtract(b.toDouble(), a.toDouble()));
        return dot(acd, acb) < 0.0;
    }

    /**
     * Test if the Point p is on the line porting the edge
     *
     * @param a line origine
     * @param b line end
     * @param p Point to test
     * @return true/false
     */
    public static boolean isOnLine(Point a, Point b, Point p) {
        // test if the vector product is zero
        return lineSide(a.getCoordinate().toDouble(),
                b.getCoordinate().toDouble(),
                p.getCoordinate().toDouble()
        ) == 0;
    }

    /**
     * Test if the Point P is at the right of the line A-B.
     *
     * @param a line origine
     * @param b line end
     * @param p Point to test
     * @return true/false
     */
    public static boolean isAtRightOf(Point a, Point b, Point p) {
        return isCounterClockwise(p, b, a);
    }

    /**
     * Test if the Point P is at the right of the line A-B.
     *
     * @param a line origine
     * @param b line end
     * @param p Point to test
     * @return true/false
     */
    public static boolean isAtRightOf(double[] a, double[] b, double[] p) {
        return isCounterClockwise(p, b, a);
    }

    /**
     * return true if a, b and c turn in Counter Clockwise direction
     *
     * @param a,b,c the 3 points to test
     * @return true if a, b and c turn in Counter Clockwise direction
     */
    public static boolean isCounterClockwise(Point a, Point b, Point c) {
        return isCounterClockwise(
                a.getCoordinate().toDouble(),
                b.getCoordinate().toDouble(),
                c.getCoordinate().toDouble());
    }

    /**
     * return true if a, b and c turn in Counter Clockwise direction
     *
     * @param a,b,c the 3 points to test
     * @return true if a, b and c turn in Counter Clockwise direction
     */
    public static boolean isCounterClockwise(Tuple a, Tuple b, Tuple c) {
        // test the sign of the determinant of ab x cb
        return lineSide(a, b, c) > 0;
    }

    /**
     * return true if a, b and c turn in Counter Clockwise direction
     *
     * @param a,b,c the 3 points to test
     * @return true if a, b and c turn in Counter Clockwise direction
     */
    public static boolean isCounterClockwise(double[] a, double[] b, double[] c) {
        // test the sign of the determinant of ab x cb
        return lineSide(a, b, c) > 0;
    }

    /**
     * return true if a, b and c turn in Counter Clockwise direction
     *
     * @param a,b,c the 3 points to test
     * @return true if a, b and c turn in Counter Clockwise direction
     */
    public static boolean isCounterClockwise(float[] a, float[] b, float[] c) {
        // test the sign of the determinant of ab x cb
        return lineSide(a, b, c) > 0;
    }

    /**
     * Test the side of a point compare to a line.
     *
     * @param a line start
     * @param b line end
     * @param c to test
     * @return > 0 if point is on the left side
     * = 0 if point is on the line
     * < 0 if point is on the right side
     */
    public static double lineSide(Tuple a, Tuple b, Tuple c) {
        double a0 = a.get(0);
        double a1 = a.get(1);
        return (b.get(0) - a0) * (c.get(1) - a1) - (c.get(0) - a0) * (b.get(1) - a1);
    }

    /**
     * Test the side of a point compare to a line.
     *
     * @param a line start
     * @param b line end
     * @param c to test
     * @return > 0 if point is on the left side
     * = 0 if point is on the line
     * < 0 if point is on the right side
     */
    public static double lineSide(double[] a, double[] b, double[] c) {
        return (b[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (b[1] - a[1]);
    }

    /**
     * Test the side of a point compare to a line.
     *
     * @param a line start
     * @param b line end
     * @param c to test
     * @return > 0 if point is on the left side
     * = 0 if point is on the line
     * < 0 if point is on the right side
     */
    public static double lineSide(float[] a, float[] b, float[] c) {
        return (b[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (b[1] - a[1]);
    }

    /**
     * Test the side of a point compare to a line.
     *
     * @param ax line start X
     * @param ay line start Y
     * @param bx line end X
     * @param by line end Y
     * @param cx X to test
     * @param cy Y to test
     * @return > 0 if point is on the left side
     * = 0 if point is on the line
     * < 0 if point is on the right side
     */
    public static double lineSide(double ax, double ay, double bx, double by, double cx, double cy) {
        return (bx - ax) * (cy - ay) - (cx - ax) * (by - ay);
    }

    /**
     * Test the side of a point compare to a line.
     *
     * @param ax line start X
     * @param ay line start Y
     * @param bx line end X
     * @param by line end Y
     * @param cx X to test
     * @param cy Y to test
     * @return > 0 if point is on the left side
     * = 0 if point is on the line
     * < 0 if point is on the right side
     */
    public static double lineSide(float ax, float ay, float bx, float by, float cx, float cy) {
        return (bx - ax) * (cy - ay) - (cx - ax) * (by - ay);
    }

    /**
     * The Delaunay criteria:
     * <p>
     * test if the point d is inside the circumscribed circle of triangle p1,p2,p3
     *
     * @param p1,p2,p3 triangle points
     * @param pd       point to test
     * @return true if point p0 inside circle and false, if point p0 is outside of circle or on border
     */
    public static boolean inCircle(Point p1, Point p2, Point p3, Point pd) {
        final double x1 = p1.getCoordinate().get(0);
        final double y1 = p1.getCoordinate().get(1);
        final double x2 = p2.getCoordinate().get(0);
        final double y2 = p2.getCoordinate().get(1);
        final double x3 = p3.getCoordinate().get(0);
        final double y3 = p3.getCoordinate().get(1);
        final double x = pd.getCoordinate().get(0);
        final double y = pd.getCoordinate().get(1);

        double p1Square = x1 * x1 + y1 * y1;
        double p2Square = x2 * x2 + y2 * y2;
        double p3Square = x3 * x3 + y3 * y3;
        double pdSquare = x  * x  + y  * y;

        double a = det33(x1, y1,1, x2, y2, 1, x3, y3, 1);
        double b = det33(p1Square, y1, 1, p2Square, y2, 1, p3Square, y3, 1);
        double c = det33(p1Square, x1, 1, p2Square, x2, 1, p3Square, x3, 1);
        double d = det33(p1Square, x1, y1, p2Square, x2, y2, p3Square, x3, y3);

        double det44 = Math.signum(a) * (a * pdSquare - b * x + c * y - d);

        if (det44 < 0) {
            return true;
        }
        return false;
    }

    /**
     * Test if a point is inside given triangle.
     *
     * @param a
     * @param b
     * @param c
     * @param p
     * @return
     */
    public static boolean inTriangle(double[] a, double[] b, double[] c, double[] p) {
        final double[] bary = Triangle.getBarycentricValue(a, b, c, p);
        return bary[1] >= 0.0 && bary[2] >= 0.0 && (bary[1] + bary[2]) <= 1.0;
    }

    /**
     * Calculate constant D of a plan.
     * Same as normal.dot(point).
     *
     * @param normal
     * @param pointOnPlan
     * @return
     */
    public static double calculatePlanD(double[] normal, double[] pointOnPlan) {
        return Vectors.dot(normal, pointOnPlan);
    }

    /**
     * Calculate projection of a point on a plan.
     *
     * @param point      point to project
     * @param planNormal plan normal
     * @param planD
     * @return projected point
     */
    public static double[] projectPointOnPlan(double[] point, double[] planNormal, double planD) {
        double[] va = Vectors.subtract(point, Vectors.scale(planNormal, planD));
        double d = Vectors.dot(planNormal, va);
        return Vectors.subtract(point, Vectors.scale(planNormal, d));
    }

    /**
     * Test if given sequence of tuple is in clockwise direction.
     * This method expect the coordinates to be a closed line.
     *
     * @param coordinates
     * @return
     */
    public static boolean isClockWise(Sequence coordinates) {
        final double area = calculateArea(coordinates);
        return area > 0;
    }

    public static boolean isClockWise(TupleGrid1D coordinates) {
        final double area = calculateArea(coordinates);
        return area > 0;
    }

    /**
     * Test if given path iterator is in clockwise direction.
     * This method expect the coordinates to be a closed line.
     *
     * @param pathIterator
     * @return true if clockwise
     */
    public static boolean isClockWise(PathIterator pathIterator) {
        final Sequence outter = new ArraySequence();
        Vector2f64 start = new Vector2f64();
        while (pathIterator.next()) {
            final int type = pathIterator.getType();
            if (type == PathIterator.TYPE_MOVE_TO) {
                pathIterator.getPosition(start);
            } else if (type == PathIterator.TYPE_CLOSE) {
                outter.add(start);
                break;
            }
            outter.add(pathIterator.getPosition(new Vector2f64()));
        }
        final double area = calculateArea(outter);
        return area > 0;
    }

    /**
     * Test if given sequence of tuple is in clockwise direction.
     * This method expect the coordinates to be a closed line.
     *
     * @param coordinates
     * @return
     */
    public static double calculateArea(Sequence coordinates) {
        double area = 0;
        final int numPoints = coordinates.getSize();
        for (int i = 0; i < numPoints - 1; i++) {
            final Tuple start = (Tuple) coordinates.get(i);
            final Tuple end = (Tuple) coordinates.get(i + 1);
            area += (start.get(0) + end.get(0)) * (start.get(1) - end.get(1));
        }
        return area / 2.0;
    }

    /**
     * Test if given sequence of tuple is in clockwise direction.
     * This method expect the coordinates to be a closed line.
     *
     * @param coordinates
     * @return
     */
    public static double calculateArea(TupleGrid1D coordinates) {
        double area = 0;
        final int numPoints = coordinates.getDimension();
        final TupleRW start = coordinates.createTuple();
        final TupleRW end = coordinates.createTuple();
        for (int i = 0; i < numPoints - 1; i++) {
            coordinates.getTuple(i, start);
            coordinates.getTuple(i + 1, end);
            area += (start.get(0) + end.get(0)) * (start.get(1) - end.get(1));
        }
        return area / 2.0;
    }

    private static double det33(double... m) {
        double det33 = 0;
        det33 += m[0] * (m[4] * m[8] - m[5] * m[7]);
        det33 -= m[1] * (m[3] * m[8] - m[5] * m[6]);
        det33 += m[2] * (m[3] * m[7] - m[4] * m[6]);
        return det33;
    }

    public static BBox transform(BBox bbox, Transform trs, BBox buffer) {
        if (buffer == null) {
            buffer = new BBox(bbox.getDimension());
        }

        if (bbox.getDimension()==2) {
            double minX = bbox.getMin(0);
            double minY = bbox.getMin(1);
            double maxX = bbox.getMax(0);
            double maxY = bbox.getMax(1);

            final Vector2f64 c = new Vector2f64(minX, minY);
            trs.transform(c, c);
            buffer.getLower().set(c);
            buffer.getUpper().set(c);

            c.x = minX; c.y = maxY;
            trs.transform(c, c);
            buffer.expand(c);

            c.x = maxX; c.y = maxY;
            trs.transform(c, c);
            buffer.expand(c);

            c.x = maxX; c.y = minY;
            trs.transform(c, c);
            buffer.expand(c);

            return buffer;
        } else {
            throw new UnsupportedOperationException("TODO");
        }
    }

    public static PlanarGeometry flatten(PlanarGeometry geom, double[] resolution) {
        final FlattenPathIterator ite = new FlattenPathIterator(
                geom.createPathIterator(), resolution);
        final Path path = new Path();
        path.append(ite);
        return path;
    }

    public static TupleGrid1D toTupleBuffer(Tuple[] tuples) {
        final TupleGrid1D buffer = new InterleavedTupleGrid1D(
                Float64.TYPE, new UndefinedSystem(tuples[0].getSampleCount()), tuples.length);
        for (int i = 0; i < tuples.length; i++) {
            buffer.setTuple(i, tuples[i]);
        }
        return buffer;
    }

    public static TupleGrid1D toTupleBuffer(Sequence tuples) {
        final Tuple tuple = (Tuple) tuples.get(0);
        final TupleGrid1D buffer = new InterleavedTupleGrid1D(
                Float64.TYPE, new UndefinedSystem(tuple.getSampleCount()), tuples.getSize());
        for (int i = 0, n = tuples.getSize(); i < n; i++) {
            buffer.setTuple(i, (Tuple) tuples.get(i));
        }
        return buffer;
    }

    public static Tuple[] toTupleArray(TupleGrid1D buffer) {
        final Tuple[] tuples = new Tuple[buffer.getDimension()];
        for (int i = 0; i < tuples.length; i++) {
            tuples[i] = buffer.createTuple();
            buffer.getTuple(i, (TupleRW) tuples[i]);
        }
        return tuples;
    }

    public static int[] toInt32(TupleGrid grid) {
        if (grid instanceof InterleavedTupleGrid) {
            final Buffer buffer = ((InterleavedTupleGrid) grid).getPrimitiveBuffer();
            return buffer.toIntArray();
        }

        final Extent.Long extent = grid.getExtent();
        final int nbSample = grid.getSampleSystem().getNumComponents();
        long count = nbSample;
        for (int i=0,n=extent.getDimension();i<n;i++) {
            count *= extent.getL(i);
        }
        final int[] all = new int[(int) count];
        int offset = 0;
        final TupleGridCursor cursor = grid.cursor();
        while (cursor.next()) {
            cursor.samples().toInt(all, offset);
            offset += nbSample;
        }
        return all;
    }

    public static float[] toFloat32(TupleGrid grid) {
        if (grid instanceof InterleavedTupleGrid) {
            final Buffer buffer = ((InterleavedTupleGrid) grid).getPrimitiveBuffer();
            return buffer.toFloatArray();
        }

        final Extent.Long extent = grid.getExtent();
        final int nbSample = grid.getSampleSystem().getNumComponents();
        long count = nbSample;
        for (int i=0,n=extent.getDimension();i<n;i++) {
            count *= extent.getL(i);
        }
        final float[] all = new float[(int) count];
        int offset = 0;
        final TupleGridCursor cursor = grid.cursor();
        while (cursor.next()) {
            cursor.samples().toFloat(all, offset);
            offset += nbSample;
        }
        return all;
    }

    public static double[] toFloat64(TupleGrid grid) {
        if (grid instanceof InterleavedTupleGrid) {
            final Buffer buffer = ((InterleavedTupleGrid) grid).getPrimitiveBuffer();
            return buffer.toDoubleArray();
        }

        final Extent.Long extent = grid.getExtent();
        final int nbSample = grid.getSampleSystem().getNumComponents();
        long count = nbSample;
        for (int i=0,n=extent.getDimension();i<n;i++) {
            count *= extent.getL(i);
        }
        final double[] all = new double[(int) count];
        int offset = 0;
        final TupleGridCursor cursor = grid.cursor();
        while (cursor.next()) {
            cursor.samples().toDouble(all, offset);
            offset += nbSample;
        }
        return all;
    }

    /**
     * NOTE : this is not a perfect algorithm,
     * it search the smallest dimension and and triangulate in 2D.
     * NOTE : we could improve it by projection on the most efficient plane.
     *
     * @param vertices
     * @return triangles indices int[n][3]
     */
    public static int[][] triangulate(Tuple[] vertices) throws OperationException{

        final BBox bbox = new BBox(3);
        bbox.getLower().set(vertices[0]);
        bbox.getUpper().set(vertices[0]);
        for (int i=1;i<vertices.length;i++) {
            bbox.expand(vertices[i]);
        }

        //find thinest dimension
        int smallIndex = 0;
        for (int i=1;i<3;i++) {
            if (bbox.getSpan(i)< bbox.getSpan(smallIndex)) {
                smallIndex = i;
            }
        }

        //convert to 2d
        final Tuple[] v2d = new Tuple[vertices.length];
        for (int i=0;i<vertices.length;i++) {
            v2d[i] = FLATTEN[smallIndex].transform(vertices[i], null);
        }

        //TODO : improve triangulation classes to return indexes if possible
        final EarClipping clip = new EarClipping();
        final Sequence res = clip.triangulate(new Polygon(new Polyline(Geometries.toTupleBuffer(v2d)), null));

        //find indexes back
        final int[][] trs = new int[res.getSize()][3];
        for (int i=0;i<trs.length;i++) {
            final science.unlicense.geometry.api.Triangle triangle = (science.unlicense.geometry.api.Triangle) res.get(i);
            trs[i][0] = Arrays.getFirstOccurence(v2d, 0, v2d.length, triangle.getFirstCoord());
            trs[i][1] = Arrays.getFirstOccurence(v2d, 0, v2d.length, triangle.getSecondCoord());
            trs[i][2] = Arrays.getFirstOccurence(v2d, 0, v2d.length, triangle.getThirdCoord());
        }

        return trs;
    }


    private static final Transform[] FLATTEN = new Transform[]{
        RearrangeTransform.create(new int[]{-1, 0, 1}),
        RearrangeTransform.create(new int[]{ 0,-1, 1}),
        RearrangeTransform.create(new int[]{ 0, 1,-1})
    };
}
