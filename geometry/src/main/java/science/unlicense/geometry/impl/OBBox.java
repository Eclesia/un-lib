

package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Oriented BoundingBox.
 *
 * @author Johann Sorel
 */
public class OBBox extends AbstractOrientedGeometry {

    private final TupleRW upper;
    private final TupleRW lower;

    public OBBox(int dimension) {
        super(dimension);
        lower = VectorNf64.createDouble(dimension);
        upper = VectorNf64.createDouble(dimension);
    }

    public OBBox(TupleRW min, TupleRW max) {
        super(min.getSampleCount());
        this.lower = min;
        this.upper = max;
    }

    public OBBox(double[] min, double[] max) {
        super(min.length);
        this.lower = VectorNf64.create(min);
        this.upper = VectorNf64.create(max);
    }

    public TupleRW getUpper() {
        return upper;
    }

    public TupleRW getLower() {
        return lower;
    }

    public Tuple getMiddle() {
        return VectorNf64.create(upper).localAdd(lower).localScale(0.5);
    }

    public double getMin(int ordinate){
        return lower.get(ordinate);
    }

    public double getMiddle(int ordinate){
        return (upper.get(ordinate) + lower.get(ordinate)) / 2;
    }

    public double getMax(int ordinate){
        return upper.get(ordinate);
    }

    public double getSpan(int ordinate){
        return getMax(ordinate)-getMin(ordinate);
    }

    public BBox getUnorientedBounds() {
        return new BBox(lower.copy(), upper.copy());
    }

}
