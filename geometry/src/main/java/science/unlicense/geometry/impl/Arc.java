
package science.unlicense.geometry.impl;

import science.unlicense.math.api.Tuple;

/**
 *
 * @author Johann Sorel
 */
public class Arc extends AbstractPlanarGeometry {

    private final Tuple start;
    private final Tuple end;
    private final Tuple center;
    private final double radius;
    private final double rotation;
    private final boolean largeArc;
    private final boolean sweep;

    public Arc(Tuple start, Tuple end, Tuple center, double radius, double rotation, boolean largeArc, boolean sweep) {
        this.start = start;
        this.end = end;
        this.center = center;
        this.radius = radius;
        this.rotation = rotation;
        this.largeArc = largeArc;
        this.sweep = sweep;
    }

    public Tuple getStart() {
        return start;
    }

    public Tuple getEnd() {
        return end;
    }

    public Tuple getArcCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }

    public double getRotation() {
        return rotation;
    }

    public boolean getLargeArcFlag() {
        return largeArc;
    }

    public boolean getSweepFlag() {
        return sweep;
    }

}
