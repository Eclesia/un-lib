

package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.system.CoordinateSystem;

/**
 * There are numerous named geometry types.
 * If all types extend Polygon the relative constraint of each type may be
 * altered after creation. For exemple a fourth point could be added to a triangle.
 *
 * Therefor singular geometry even if they mathematicaly are polygons will not
 * extend the Polygon class to enforce each type constraint.
 *
 * @author Johann Sorel
 */
public abstract class SingularGeometry2D extends AbstractPlanarGeometry {

    public SingularGeometry2D(int dim) {
        super(dim);
    }

    public SingularGeometry2D(CoordinateSystem cs) {
        super(cs);
    }

    /**
     * Convert this singular geometry to it's polygon equivalent.
     */
    public abstract Polygon toPolygon();

}
