package science.unlicense.geometry.impl.operation;

import science.unlicense.geometry.api.operation.AbstractBinaryOperation;
import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.OperationExecutor;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractBinaryOperationExecutor implements OperationExecutor {

    protected final Class operationClass;
    protected final Class firstGeomClass;
    protected final Class secondGeomClass;
    protected final boolean canInvert;

    public AbstractBinaryOperationExecutor(Class operationClass,
            Class firstGeomClass, Class secondGeomClass, boolean canInvert) {
        this.operationClass = operationClass;
        this.firstGeomClass = firstGeomClass;
        this.secondGeomClass = secondGeomClass;
        this.canInvert = canInvert;
    }

    @Override
    public Class getOperationClass() {
        return operationClass;
    }

    @Override
    public boolean canHandle(Operation operation) {
        if (!(operationClass.isInstance(operation))) return false;
        final AbstractBinaryOperation op = (AbstractBinaryOperation) operation;
        final Object first = op.getFirst();
        final Object second = op.getSecond();
        if (canInvert){
            return (firstGeomClass.isInstance(first)  && secondGeomClass.isInstance(second))
                || (firstGeomClass.isInstance(second) && secondGeomClass.isInstance(first));
        } else {
            return (firstGeomClass.isInstance(first)  && secondGeomClass.isInstance(second));
        }
    }

    @Override
    public Object execute(Operation operation) throws OperationException{
        final AbstractBinaryOperation op = (AbstractBinaryOperation) operation;
        Object first = op.getFirst();
        Object second = op.getSecond();
        if (canInvert && !(firstGeomClass.isInstance(first) && secondGeomClass.isInstance(second))){
            //set geometries in expected order
            final Object temp = first;
            first = second;
            second = temp;
        }
        return execute(op, first, second);
    }

    protected abstract Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException;

}
