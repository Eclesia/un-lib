
package science.unlicense.geometry.impl;

import science.unlicense.common.api.CObjects;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.impl.path.AbstractStepPathIterator;
import science.unlicense.geometry.impl.path.PathStep2D;

/**
 * A Rounded Rectangle.
 *
 * Specification :
 * - SVG v1.1:9.2 :
 *   The ‘rect’ element defines a rectangle which is axis-aligned with the
 *   current user coordinate system. Rounded rect-angles can be achieved by
 *   setting appropriate values for attributes ‘rx’ and ‘ry’.
 *
 * @author Johann Sorel
 */
public class RoundedRectangle extends AbstractPlanarGeometry {

    protected double x;
    protected double y;
    protected double width;
    protected double height;
    private double rx;
    private double ry;

    public RoundedRectangle() {
    }

    public RoundedRectangle(double x, double y, double width, double height) {
        this(x, y, width, height,0,0);
    }

    public RoundedRectangle(double x, double y, double width, double height,double rx, double ry) {
        CObjects.ensurePositive(width);
        CObjects.ensurePositive(height);
        CObjects.ensurePositive(rx);
        CObjects.ensurePositive(ry);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.rx = rx;
        this.ry = ry;
    }

    /**
     * The x-axis coordinate of the side of the rectangle which has the smaller
     * x-axis coordinate value in the current user coordinate system.
     */
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    /**
     * The y-axis coordinate of the side of the rectangle which has the smaller
     * y-axis coordinate value in the current user coordinate system.
     */
    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    /**
     * The width of the rectangle.
     */
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        CObjects.ensurePositive(width);
        this.width = width;
    }

    /**
     * The height of the rectangle.
     */
    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        CObjects.ensurePositive(height);
        this.height = height;
    }

    /**
     * For rounded rectangles, the x-axis radius of the ellipse used
     * to round off the corners of the rectangle.
     */
    public double getRx() {
        return rx;
    }

    public void setRx(double rx) {
        CObjects.ensurePositive(rx);
        this.rx = rx;
    }

    /**
     * For rounded rectangles, the y-axis radius of the ellipse used
     * to round off the corners of the rectangle.
     */
    public double getRy() {
        return ry;
    }

    public void setRy(double ry) {
        CObjects.ensurePositive(ry);
        this.ry = ry;
    }

    public BBox getBoundingBox() {
        return new BBox(
                new double[]{x,y},
                new double[]{x+width,y+height}
        );
    }

    public PathIterator createPathIterator() {
        return new RoundedRectangle2DIterator();
    }

    private class RoundedRectangle2DIterator extends AbstractStepPathIterator{

        private int index = -1;
        private final double frx;
        private final double fry;

        public RoundedRectangle2DIterator() {
            frx = (rx>width/2) ? width/2 : rx;
            fry = (ry>height/2) ? height/2 : ry;
        }

        public void reset() {
            index = -1;
        }

        public boolean next() {
            index++;
            if (index == 0){
                currentStep = new PathStep2D(PathIterator.TYPE_MOVE_TO,x,y+fry);
            } else if (index == 1){
                currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC,x,y,x+frx,y);
            } else if (index == 2){
                currentStep = new PathStep2D(PathIterator.TYPE_LINE_TO,x+width-frx,y);
            } else if (index == 3){
                currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC,x+width,y,x+width,y+fry);
            } else if (index == 4){
                currentStep = new PathStep2D(PathIterator.TYPE_LINE_TO,x+width,y+height-fry);
            } else if (index == 5){
                currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC,x+width,y+height,x+width-frx,y+height);
            } else if (index == 6){
                currentStep = new PathStep2D(PathIterator.TYPE_LINE_TO,x+frx,y+height);
            } else if (index == 7){
                currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC,x,y+height,x,y+height-fry);
            } else if (index == 8){
                currentStep = new PathStep2D(PathIterator.TYPE_CLOSE);
            } else {
                currentStep = null;
            }
            return currentStep != null;
        }

    }


}
