
package science.unlicense.geometry.impl.operation;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.operation.CentroidOp;
import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.Ellipse;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.TupleNf64;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public class CentroidExecutors {

    private CentroidExecutors(){}

    public static final AbstractSingleOperationExecutor BBOX =
            new AbstractSingleOperationExecutor(CentroidOp.class, BBox.class){

        public Object execute(Operation operation) {
            final CentroidOp op = (CentroidOp) operation;
            final BBox geom = (BBox) op.getGeometry();
            return geom.getCentroid();
        }
    };

    public static final AbstractSingleOperationExecutor POINT =
            new AbstractSingleOperationExecutor(CentroidOp.class, Point.class){

        public Object execute(Operation operation) {
            final CentroidOp op = (CentroidOp) operation;
            final Point geom = (Point) op.getGeometry();
            return new DefaultPoint(geom.getCoordinateSystem(), geom.getCoordinate().copy());
        }
    };

    public static final AbstractSingleOperationExecutor CIRCLE =
            new AbstractSingleOperationExecutor(CentroidOp.class, Circle.class){

        public Object execute(Operation operation) {
            final CentroidOp op = (CentroidOp) operation;
            final Circle geom = (Circle) op.getGeometry();
            return geom.getCentroid();
        }
    };

    public static final AbstractSingleOperationExecutor CAPSULE =
            new AbstractSingleOperationExecutor(CentroidOp.class, Capsule.class){

        public Object execute(Operation operation) {
            final CentroidOp op = (CentroidOp) operation;
            final Capsule geom = (Capsule) op.getGeometry();
            return geom.getCentroid();
        }
    };

    public static final AbstractSingleOperationExecutor SPHERE =
            new AbstractSingleOperationExecutor(CentroidOp.class, Sphere.class){

        public Object execute(Operation operation) {
            final CentroidOp op = (CentroidOp) operation;
            final Sphere geom = (Sphere) op.getGeometry();
            return geom.getCentroid();
        }
    };

    public static final AbstractSingleOperationExecutor SEGMENT =
            new AbstractSingleOperationExecutor(CentroidOp.class, Line.class){

        public Object execute(Operation operation) {
            final CentroidOp op = (CentroidOp) operation;
            final Line geom = (Line) op.getGeometry();
            final TupleRW start = geom.getStart();
            final TupleRW end = geom.getEnd();
            final Tuple lineVector = new TupleNf64( Vectors.subtract(end.toDouble(), start.toDouble()));
            final TupleRW center = new TupleNf64( Vectors.add( start.toDouble(), Vectors.scale(lineVector.toDouble(), 0.5)));
            return new DefaultPoint(center);
        }
    };

    public static final AbstractSingleOperationExecutor ELLIPSE =
            new AbstractSingleOperationExecutor(CentroidOp.class, Ellipse.class){

        public Object execute(Operation operation) {
            final CentroidOp op = (CentroidOp) operation;
            final Ellipse geom = (Ellipse) op.getGeometry();
            return geom.getCentroid();
        }
    };
}
