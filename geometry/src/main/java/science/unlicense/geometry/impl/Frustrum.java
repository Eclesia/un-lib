
package science.unlicense.geometry.impl;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.geometry.api.BBox;

/**
 *
 * @author Johann Sorel
 */
public class Frustrum extends AbstractGeometry {

    private final Plane[] planes;

    public Frustrum(Plane[] planes) {
        super(planes[0].getCoordinateSystem());
        this.planes = planes;
    }

    public Plane[] getPlanes() {
        return planes;
    }

    @Override
    public BBox getBoundingBox() {
        throw new UnimplementedException("Not supported yet.");
    }

}
