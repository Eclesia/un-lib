
package science.unlicense.geometry.impl;

import science.unlicense.geometry.impl.path.AbstractStepPathIterator;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.impl.path.PathStep2D;
import science.unlicense.math.api.Tuple;

/**
 * Single segment cubic curve.
 *
 * @author Johann Sorel
 */
public class CubicCurve extends AbstractPlanarGeometry {

    private final Tuple start;
    private final Tuple end;
    private final Tuple control1;
    private final Tuple control2;

    public CubicCurve(Tuple start, Tuple end, Tuple control1, Tuple control2) {
        this.start = start;
        this.end = end;
        this.control1 = control1;
        this.control2 = control2;
    }

    public Tuple getStart() {
        return start;
    }

    public Tuple getEnd() {
        return end;
    }

    public Tuple getFirstControlPoint() {
        return control1;
    }

    public Tuple getSecondControlPoint() {
        return control2;
    }

    @Override
    public PathIterator createPathIterator() {
        return new CubicCurve2DIterator();
    }

    private final class CubicCurve2DIterator extends AbstractStepPathIterator{

        private int index = -1;

        public void reset() {
            index = -1;
        }

        public boolean next() {
            index++;
            if (index == 0){
                currentStep = new PathStep2D(PathIterator.TYPE_MOVE_TO,start.get(0),start.get(1));
            } else if (index == 1){
                currentStep = new PathStep2D(PathIterator.TYPE_CUBIC,
                        control1.get(0),control1.get(1),
                        control2.get(0),control2.get(1),
                        end.get(0),end.get(1));
            } else {
                currentStep = null;
            }

            return currentStep != null;
        }

    }

}
