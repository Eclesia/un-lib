
package science.unlicense.geometry.impl.operation;

import science.unlicense.geometry.api.operation.AbstractSingleOperation;
import science.unlicense.geometry.api.operation.OperationExecutor;
import science.unlicense.geometry.api.operation.Operation;

/**
 * An operation executor which works against a single geometry.
 *
 * @author Johann Sorel
 */
public abstract class AbstractSingleOperationExecutor implements OperationExecutor {

    protected final Class operationClass;
    protected final Class geomClass;

    public AbstractSingleOperationExecutor(Class operationClass,
            Class firstGeomClass) {
        this.operationClass = operationClass;
        this.geomClass = firstGeomClass;
    }

    public Class getOperationClass() {
        return operationClass;
    }

    public boolean canHandle(Operation operation) {
        if (!(operationClass.isInstance(operation))) return false;
        final AbstractSingleOperation op = (AbstractSingleOperation) operation;
        final Object first = op.getGeometry();
        return geomClass.isInstance(first);
    }

}
