
package science.unlicense.geometry.impl.operation;

import science.unlicense.geometry.api.operation.BufferOp;
import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.api.Point;

/**
 *
 * @author Johann Sorel
 */
public class BufferExecutors {

    private BufferExecutors(){}

    public static final AbstractSingleOperationExecutor POINT =
            new AbstractSingleOperationExecutor(BufferOp.class, Point.class){

        public Object execute(Operation operation) {
            final BufferOp op = (BufferOp) operation;
            final Point p = (Point) op.getGeometry();
            final double distance = op.getDistance();
            if (distance <=0){
                return new DefaultPoint(p.getCoordinate().copy());
            } else {
                return new Circle(p.getCoordinate().copy(), distance);
            }
        }
    };

    public static final AbstractSingleOperationExecutor CIRCLE =
            new AbstractSingleOperationExecutor(BufferOp.class, Circle.class){

        public Object execute(Operation operation) {
            final BufferOp op = (BufferOp) operation;
            final Circle p = (Circle) op.getGeometry();
            double distance = op.getDistance();
            distance = p.getRadius()+distance;
            if (distance <=0){
                return new DefaultPoint(p.getCenter().copy());
            } else {
                return new Circle(p.getCenter().copy(), distance);
            }
        }
    };

}
