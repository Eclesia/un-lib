
package science.unlicense.geometry.impl.transform;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Curve;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.Surface;
import science.unlicense.geometry.api.Volume;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.impl.Arc;
import science.unlicense.geometry.impl.CubicCurve;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.impl.QuadraticCurve;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.math.api.Tuple;

/**
 * Decompose a geometry to compute it's bounding box.
 *
 * @author Johann Sorel
 */
public class BBoxCalculator extends Decomposer {

    private CoordinateSystem cs;
    private BBox bbox;

    public BBox computeBBox(Geometry geom) {
        cs = geom.getCoordinateSystem();
        bbox = null;
        process(geom);
        return bbox;
    }

    private void expand(Tuple tuple) {
        if (bbox == null) {
            bbox = new BBox(cs);
            bbox.getLower().set(tuple);
            bbox.getUpper().set(tuple);
        } else {
            bbox.expand(tuple);
        }
    }

    @Override
    protected void processPoint(Point geometry) {
        expand(geometry.getCoordinate());
    }

    @Override
    protected void processLine(Line geometry) {
        expand(geometry.getStart());
        expand(geometry.getEnd());
    }

    @Override
    protected void processCubicCurve(CubicCurve geometry) {
        //TODO compute real curve bbox
        expand(geometry.getStart());
        expand(geometry.getEnd());
        expand(geometry.getFirstControlPoint());
        expand(geometry.getSecondControlPoint());
    }

    @Override
    protected void processQuadraticCurve(QuadraticCurve geometry) {
        //TODO compute real curve bbox
        expand(geometry.getStart());
        expand(geometry.getEnd());
        expand(geometry.getControlPoint());
    }

    @Override
    protected void processArc(Arc geometry) {
        //TODO compute real curve bbox
        expand(geometry.getStart());
        expand(geometry.getEnd());
        expand(geometry.getArcCenter());
    }

    @Override
    protected void processTriangle(Triangle geometry) {
        expand(geometry.getFirstCoord());
        expand(geometry.getSecondCoord());
        expand(geometry.getThirdCoord());
    }

    @Override
    protected void processCurve(Curve geometry) {
        decomposeCurve(geometry);
    }

    @Override
    protected void processSurface(Surface geometry) {
        decomposeSurfaceCurves(geometry);
    }

    @Override
    protected void processVolume(Volume geometry) {
        decomposeVolumeSurfaces(geometry);
    }

    @Override
    protected void processOther(Geometry geometry) {
        if (geometry instanceof PlanarGeometry) {
            decompose(((PlanarGeometry) geometry).createPathIterator());
        } else {
            throw new IllegalArgumentException("Unsupported geometry type : "+geometry);
        }
    }

}
