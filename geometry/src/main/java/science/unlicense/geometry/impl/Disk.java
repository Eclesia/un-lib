

package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.impl.Vector3f64;

/**
 * A filled disk defined by a radius.
 * The disk rotates around the Y axis.
 *
 * @author Johann Sorel
 */
public class Disk extends AbstractOrientedGeometry {

    private double radius;

    public Disk(double radius) {
        super(3);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public BBox getUnorientedBounds() {
        return new BBox(
                new Vector3f64(-radius, 0, -radius),
                new Vector3f64(radius, 0, radius)
        );
    }

    /**
     * Calculate disk surface area.
     * Since a disk has 2 sides :
     * 2 * (Math.PI*radius*radius)
     *
     * @return surface
     */
    public double getArea() {
        return 2 * (Math.PI*radius*radius);
    }

    /**
     * A Disk has no volume.
     *
     * @return always 0
     */
    public double getVolume() {
        return 0;
    }

}
