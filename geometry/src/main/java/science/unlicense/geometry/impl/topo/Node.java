
package science.unlicense.geometry.impl.topo;

import science.unlicense.common.api.graph.DefaultVertex;
import science.unlicense.common.api.graph.Graph;

/**
 * Topologic Node, extends the Graph API.
 *
 * @author Johann Sorel
 */
public class Node extends DefaultVertex{

    public Node(Graph graph) {
        super(graph);
    }

}
