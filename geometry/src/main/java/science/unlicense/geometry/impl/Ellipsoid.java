
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.Tuple;

/**
 * Ellipsoid defined by 3 radius.
 * http://math.wikia.com/wiki/Ellipsoid
 *
 * @author Johann Sorel
 */
public class Ellipsoid extends AbstractOrientedGeometry {

    private double radiusX;
    private double radiusY;
    private double radiusZ;

    public Ellipsoid(double radiusX, double radiusY, double radiusZ) {
        super(3);
        this.radiusX = radiusX;
        this.radiusY = radiusY;
        this.radiusZ = radiusZ;
    }

    public Tuple getCenter() {
        return transform.getTranslation();
    }

    public double getRadiusX() {
        return radiusX;
    }

    public void setRadiusX(double radiusX) {
        this.radiusX = radiusX;
    }

    public double getRadiusY() {
        return radiusY;
    }

    public void setRadiusY(double radiusY) {
        this.radiusY = radiusY;
    }

    public double getRadiusZ() {
        return radiusZ;
    }

    public void setRadiusZ(double radiusZ) {
        this.radiusZ = radiusZ;
    }

    public BBox getUnorientedBounds() {
        final BBox bbox = new BBox(3);
        bbox.setRange(0, -radiusX, +radiusX);
        bbox.setRange(1, -radiusY, +radiusY);
        bbox.setRange(2, -radiusZ, +radiusZ);
        return bbox;
    }

    public double getVolume() {
        return (4.0/3.0) * Math.PI * radiusX * radiusY * radiusZ;
    }

}
