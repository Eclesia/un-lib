
package science.unlicense.geometry.impl.operation;

import science.unlicense.geometry.api.operation.TransformOp;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.api.operation.OperationExecutor;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.OrientedGeometry;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.math.impl.VectorNf64;

/**
 * Transform executors.
 *
 * @author Johann Sorel
 */
public class TransformExecutors {

    private TransformExecutors(){}

    private abstract static class TransformExecutor implements OperationExecutor{

        private final Class clazz;

        public TransformExecutor(Class geometryClass) {
            this.clazz = geometryClass;
        }

        @Override
        public Class getOperationClass() {
            return TransformOp.class;
        }

        @Override
        public Geometry execute(Operation operation) {
            final TransformOp transform = (TransformOp) operation;
            final science.unlicense.math.api.transform.Transform matrix = transform.getTransform();
            final Geometry geom = transform.getGeometry();
            return execute(geom, matrix);
        }

        protected abstract Geometry execute(Geometry geom, science.unlicense.math.api.transform.Transform transform);

        @Override
        public boolean canHandle(Operation operation) {
            return operation instanceof TransformOp &&
                    clazz.isInstance( ((TransformOp) operation).getGeometry());
        }

    }

    public static final OperationExecutor POINT =
            new TransformExecutor(Point.class){

        @Override
        protected Point execute(Geometry geom, science.unlicense.math.api.transform.Transform transform) {
            final Point candidate = (Point) geom;
            return new DefaultPoint(transform.transform(candidate.getCoordinate(),null));
        }
    };

    public static final OperationExecutor RECTANGLE =
            new TransformExecutor(Rectangle.class){

        @Override
        protected Rectangle execute(Geometry geom, science.unlicense.math.api.transform.Transform transform) {
            final Rectangle candidate = (Rectangle) geom;
            double[] ul = new double[]{
                candidate.getX(),
                candidate.getY()
                };
            transform.transform(ul, 0, ul, 0, 1);
            return new Rectangle(ul[0], ul[1], candidate.getWidth(), candidate.getHeight());
        }
    };

    public static final OperationExecutor CIRCLE =
            new TransformExecutor(Circle.class){

        @Override
        protected Circle execute(Geometry geom, science.unlicense.math.api.transform.Transform transform) {
            final Circle candidate = (Circle) geom;
            return new Circle(
                    transform.transform(candidate.getCenter(),null),
                    candidate.getRadius());
        }
    };

    public static final OperationExecutor BBOX =
            new TransformExecutor(BBox.class){

        @Override
        protected BBox execute(Geometry geom, science.unlicense.math.api.transform.Transform transform) {
            final BBox bbox = (BBox) geom;
            return new BBox(
                transform.transform(bbox.getLower(),null),
                transform.transform(bbox.getUpper(),null)
                );
        }
    };

    public static final OperationExecutor SPHERE =
            new TransformExecutor(Sphere.class){

        @Override
        protected Sphere execute(Geometry geom, science.unlicense.math.api.transform.Transform transform) {
            final Sphere sphere = (Sphere) geom;
            final Sphere copy = new Sphere();
            copy.getCenter().set(sphere.getCenter());
            copy.setRadius(sphere.getRadius());

            transform.transform(copy.getCenter(),copy.getCenter());
            //make a transform on x axis, TODO this is not right, just a quick simplification
            //real result should be an ellipsoid
            final VectorRW v = new Vector4f64(copy.getRadius(), 0, 0,0);
            ((Affine) transform).toMatrix().transform(v, v);
            copy.setRadius(v.length());

            return copy;
        }
    };

    public static final OperationExecutor CAPSULE =
            new TransformExecutor(Capsule.class){

        @Override
        protected Capsule execute(Geometry geom, science.unlicense.math.api.transform.Transform transform) {
            final Capsule capsule = (Capsule) geom;
            final Capsule copy = new Capsule(capsule.getHeight(),capsule.getRadius());

            final Affine m = capsule.getTransform();
            copy.getTransform().set(m.multiply((Affine) transform));

            //make a transform on x axis, TODO this is not right, just a quick simplification
            final VectorRW v = new Vector3f64(copy.getRadius(), 0, 0);
            transform.transform(v, v);
            copy.setRadius(v.length());

            return copy;
        }
    };

    public static final OperationExecutor PLANE =
            new TransformExecutor(Plane.class){

        @Override
        protected Plane execute(Geometry geom, science.unlicense.math.api.transform.Transform transform) {
            final Plane plane = (Plane) geom;
            final VectorRW normal = plane.getNormal();
            final int dim = normal.getSampleCount();
            final VectorRW cpNormal = VectorNf64.createDouble(dim+1);
            for (int i=0; i<dim; i++) cpNormal.set(i, normal.get(i));
            final VectorRW cpPoint = plane.getPoint().copy();
            transform.transform(cpPoint,cpPoint);
            ((Affine) transform).toMatrix().transform(cpNormal,cpNormal);
            return new Plane(cpNormal.getXYZ(),cpPoint);
        }
    };

    public static final OperationExecutor ORIENTED =
            new TransformExecutor(OrientedGeometry.class){

        @Override
        protected Capsule execute(Geometry geom, science.unlicense.math.api.transform.Transform transform) {
            final OrientedGeometry oriented = (OrientedGeometry) geom;
            //TODO
            throw new RuntimeException("TODO");
        }
    };

}
