
package science.unlicense.geometry.impl;

import science.unlicense.math.api.Tuple;

/**
 *
 * @author Johann Sorel
 */
public class QuadraticCurve extends AbstractPlanarGeometry {

    private final Tuple start;
    private final Tuple end;
    private final Tuple control;

    public QuadraticCurve(Tuple start, Tuple end, Tuple control) {
        this.start = start;
        this.end = end;
        this.control = control;
    }

    public Tuple getStart() {
        return start;
    }

    public Tuple getEnd() {
        return end;
    }

    public Tuple getControlPoint() {
        return control;
    }

}
