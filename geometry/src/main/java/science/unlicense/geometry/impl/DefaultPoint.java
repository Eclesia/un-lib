
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.impl.path.CoordinateTuplePathIterator;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;

/**
 * A Point.
 *
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_Point
 *   An ST_Point value is a 0-dimensional geometry and represents a single location.
 *
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public class DefaultPoint extends AbstractPlanarGeometry implements Point {

    private TupleRW coord;

    public DefaultPoint(CoordinateSystem cs) {
        super(cs);
        coord = VectorNf64.createDouble(cs.getDimension());
    }

    public DefaultPoint(CoordinateSystem cs, TupleRW coord) {
        super(cs);
        this.coord = coord;
    }

    public DefaultPoint(int dimension) {
        this(VectorNf64.createDouble(dimension));
    }

    public DefaultPoint(double[] coords){
        this(VectorNf64.create(coords));
    }

    public DefaultPoint(double x, double y){
        this(new Vector2f64(x, y));
    }

    public DefaultPoint(double x, double y, double z){
        this(new Vector3f64(x, y, z));
    }

    public DefaultPoint(double x, double y, double z, double w){
        this(new Vector4f64(x, y, z, w));
    }

    public DefaultPoint(TupleRW coord) {
        super(coord.getSampleCount());
        this.coord = coord;
    }

    public TupleRW getCoordinate() {
        return coord;
    }

    public double getX() {
        return coord.get(0);
    }

    public double getY() {
        return coord.get(1);
    }

    public double getZ() {
        return coord.get(2);
    }

    public double getW() {
        return coord.get(3);
    }

    public void setCoordinate(final TupleRW center) {
        this.coord = center;
    }

    public void setX(double cx) {
        this.coord.set(0, cx);
    }

    public void setY(double cy) {
        this.coord.set(1, cy);
    }

    public void setZ(double cz) {
        this.coord.set(2, cz);
    }

    public void setW(double cw) {
        this.coord.set(3, cw);
    }

    // ===== Commun ============================================================

    /**
     * Calculate the line surface area.
     *
     * @return circle area.
     */
    public double getArea() {
        return 0.0;
    }

    /**
     * Calculate line length.
     *
     * @return circle circumference
     */
    public double getLength() {
        return 0.0;
    }

    /**
     * Line bounding box is a box which has as corners both start and end tuples
     * with a width of twice the radius.
     *
     * @return BoundingBox
     */
    public BBox getBoundingBox() {
        final BBox bbox = new BBox(getCoordinateSystem());
        for (int i=0,n=getDimension(); i<n; i++) {
            final double d = coord.get(i);
            bbox.setRange(i, d, d);
        }
        return bbox;
    }

    /**
     * Bounding circle.
     * half-length of the line.
     *
     * @return Circle
     */
    public Circle getBoundingCircle() {
        return new Circle(getCoordinate().copy(), 0.0);
    }

    // Polygon getConvexhull() method defined in AbstractGeometry2D abstract class

    // Geometry buffer(double distance) method  method defined in AbstractGeometry abstract class

    /**
     * Centroid is circle center.
     *
     * @return Point
     */
    public DefaultPoint getCentroid() {
        return new DefaultPoint( this.coord );
    }

    // =========================================================================

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultPoint other = (DefaultPoint) obj;
        if (this.coord != other.coord && (this.coord == null || !this.coord.equals(other.coord))) {
            return false;
        }
        return true;
    }

    public PathIterator createPathIterator() {
        return new CoordinateTuplePathIterator(new Tuple[]{coord});
    }

}
