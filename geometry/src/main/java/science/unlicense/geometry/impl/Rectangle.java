
package science.unlicense.geometry.impl;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.impl.path.AbstractStepPathIterator;
import science.unlicense.geometry.impl.path.PathStep2D;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 * A Rectangle.
 *
 * Specification :
 * - SVG v1.1:9.2 :
 *   The ‘rect’ element defines a rectangle which is axis-aligned
 *   with the current user coordinate system.
 *
 * @author Johann Sorel
 */
public class Rectangle extends SingularGeometry2D {

    protected double x;
    protected double y;
    protected double width;
    protected double height;

    public Rectangle() {
        super(2);
    }

    public Rectangle(Rectangle rectangle) {
        this(rectangle.x,rectangle.y,rectangle.width,rectangle.height);
    }

    public Rectangle(Extent extent) {
        this(0,0, extent.get(0), extent.get(1));
    }

    public Rectangle(BBox bbox) {
        this(bbox.getMin(0), bbox.getMin(1), bbox.getSpan(0), bbox.getSpan(1));
    }

    public Rectangle(double x, double y, double width, double height) {
        super(2);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        if (width < 0) {
            throw new IllegalArgumentException("Negative width "+width);
        }
        if (height < 0) {
            throw new IllegalArgumentException("Negative height "+height);
        }
    }

    /**
     * The x-axis coordinate of the side of the rectangle which has the smaller
     * x-axis coordinate value in the current user coordinate system.
     */
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    /**
     * The y-axis coordinate of the side of the rectangle which has the smaller
     * y-axis coordinate value in the current user coordinate system.
     */
    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    /**
     * The width of the rectangle.
     */
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * Copy given rectangle position and size.
     *
     * @param bounds Rectangle to copy
     */
    public void set(Rectangle bounds) {
        this.x = bounds.x;
        this.y = bounds.y;
        this.width = bounds.width;
        this.height = bounds.height;
    }

    public void set(Extent ext){
        x = 0;
        y = 0;
        width = ext.get(0);
        height = ext.get(1);
    }

    public void set(BBox bbox){
        x = bbox.getMin(0);
        y = bbox.getMin(1);
        width = bbox.getSpan(0);
        height = bbox.getSpan(1);
    }

    /**
     * The height of the rectangle.
     */
    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public PathIterator createPathIterator() {
        return new Rectangle2DIterator();
    }

    public Polygon toPolygon() {
        final Buffer buffer = DefaultBufferFactory.wrap(new double[]{
            x,       y,
            x+width, y,
            x+width, y+height,
            x,       y+height,
            x,       y
        });
        final Polyline poly = new Polyline(new InterleavedTupleGrid1D(
                buffer, Float64.TYPE, UndefinedSystem.create(2), 5));
        return new Polygon(poly, null);
    }

    public BBox getBoundingBox() {
        return new BBox(
                new double[]{x,y},
                new double[]{x+width,y+height}
        );
    }

    public boolean intersects(Rectangle r){
        //check for intersections
        double t;
        if ((t = x - r.x) > r.width || -t > width) return false;
        if ((t = y - r.y) > r.height|| -t > height) return false;
        return true;
    }

    public Rectangle intersection(Rectangle r){
        //check for intersections
        double t;
        if ((t = x - r.x) > r.width || -t > width) return null;
        if ((t = y - r.y) > r.height|| -t > height) return null;

        final double minX = Maths.max(x, r.x);
        final double maxX = Maths.min(x+width, r.x+r.width);
        final double minY = Maths.max(y, r.y);
        final double maxY = Maths.min(y+height, r.y+r.height);

        return new Rectangle(minX, minY, maxX-minX, maxY-minY);
    }

    @Override
    public Chars toChars() {
        return new Chars("Rectangle, x="+x+" y="+y+" w="+width+" h="+height);
    }

    @Override
    public int getHash() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rectangle other = (Rectangle) obj;
        if (Double.doubleToLongBits(this.x) != Double.doubleToLongBits(other.x)) {
            return false;
        }
        if (Double.doubleToLongBits(this.y) != Double.doubleToLongBits(other.y)) {
            return false;
        }
        if (Double.doubleToLongBits(this.width) != Double.doubleToLongBits(other.width)) {
            return false;
        }
        if (Double.doubleToLongBits(this.height) != Double.doubleToLongBits(other.height)) {
            return false;
        }
        return true;
    }



    private class Rectangle2DIterator extends AbstractStepPathIterator{

        private int index = -1;

        public void reset() {
            index = -1;
        }

        public boolean next() {
            index++;
            if (index == 0){
                currentStep = new PathStep2D(PathIterator.TYPE_MOVE_TO,x,      y);
            } else if (index == 1){
                currentStep = new PathStep2D(PathIterator.TYPE_LINE_TO,x+width,y);
            } else if (index == 2){
                currentStep = new PathStep2D(PathIterator.TYPE_LINE_TO,x+width,y+height);
            } else if (index == 3){
                currentStep = new PathStep2D(PathIterator.TYPE_LINE_TO,x,      y+height);
            } else if (index == 4){
                currentStep = new PathStep2D(PathIterator.TYPE_CLOSE);
            } else {
                currentStep = null;
            }

            return currentStep != null;
        }

    }

}
