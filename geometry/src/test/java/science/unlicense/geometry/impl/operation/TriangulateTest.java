
package science.unlicense.geometry.impl.operation;

import org.junit.Test;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.TriangulateOp;

/**
 *
 * @author Johann Sorel
 */
public class TriangulateTest {

    @Test
    public void testBbox() throws OperationException{

        final BBox bbox = new BBox(3);
        bbox.setRange(0,   0, +10);
        bbox.setRange(1, -10, +20);
        bbox.setRange(2, -30, +30);

        final TriangulateOp op = new TriangulateOp(bbox, 0);
        final Geometry geom = op.execute();
        System.out.println(geom);

    }
}
