

package science.unlicense.geometry.impl.algorithm;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class SutherlandHodgmanTest {

    @Test
    public void testRectangleClip(){

        final Sequence subject = new ArraySequence();
        subject.add(new Vector2f64(0,0));
        subject.add(new Vector2f64(10,0));
        subject.add(new Vector2f64(10,20));
        subject.add(new Vector2f64(0,20));

        final Sequence clip = new ArraySequence();
        clip.add(new DefaultLine( 5, 8, 5, 24));
        clip.add(new DefaultLine( 5,24,16, 24));
        clip.add(new DefaultLine(16,24,16,  8));
        clip.add(new DefaultLine(15, 8, 5,  8));

        final Sequence result = SutherlandHodgman.clip(subject, clip);
        Assert.assertEquals(4,result.getSize());
        Assert.assertEquals(new Vector2f64( 5,  8),result.get(0));
        Assert.assertEquals(new Vector2f64(10,  8),result.get(1));
        Assert.assertEquals(new Vector2f64(10, 20),result.get(2));
        Assert.assertEquals(new Vector2f64( 5, 20),result.get(3));

    }

}
