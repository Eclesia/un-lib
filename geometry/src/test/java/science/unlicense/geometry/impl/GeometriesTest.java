
package science.unlicense.geometry.impl;

import science.unlicense.geometry.api.Point;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector3f64;

/**
 * @author Johann Sorel
 * @author Izyumov Konstantin
 */
public class GeometriesTest {
    private static final double DELTA = 0.000000001;
    private static final double[] lineStart = new double[]{2, 4};
    private static final double[] lineEnd = new double[]{6, 1};
    private static final double[] pointOnLine = new double[]{4, 2.5};
    private static final double[] pointLeft = new double[]{3, 5};
    private static final double[] pointRight = new double[]{5, 0.1};

    @Test
    public void testLineSide() {

        double side;

        side = Geometries.lineSide(lineStart, lineEnd, pointOnLine);
        Assert.assertTrue(side == 0);

        side = Geometries.lineSide(lineStart, lineEnd, pointLeft);
        Assert.assertTrue(side > 0);

        side = Geometries.lineSide(lineStart, lineEnd, pointRight);
        Assert.assertTrue(side < 0);

    }

    @Test
    public void testIsOnLine() {
        Assert.assertTrue(Geometries.isOnLine(new DefaultPoint(lineStart), new DefaultPoint(lineEnd), new DefaultPoint(pointOnLine)));
        Assert.assertFalse(Geometries.isOnLine(new DefaultPoint(lineStart), new DefaultPoint(lineEnd), new DefaultPoint(pointLeft)));
        Assert.assertFalse(Geometries.isOnLine(new DefaultPoint(lineStart), new DefaultPoint(lineEnd), new DefaultPoint(pointRight)));
    }

    @Test
    public void testIsCounterClockwise() {
        //on line is false
        Assert.assertFalse(Geometries.isCounterClockwise(new DefaultPoint(lineStart), new DefaultPoint(lineEnd), new DefaultPoint(pointOnLine)));
        Assert.assertTrue(Geometries.isCounterClockwise(new DefaultPoint(lineStart), new DefaultPoint(lineEnd), new DefaultPoint(pointLeft)));
        Assert.assertFalse(Geometries.isCounterClockwise(new DefaultPoint(lineStart), new DefaultPoint(lineEnd), new DefaultPoint(pointRight)));
        //on line is false
        Assert.assertFalse(Geometries.isCounterClockwise(lineStart, lineEnd, pointOnLine));
        Assert.assertTrue(Geometries.isCounterClockwise(lineStart, lineEnd, pointLeft));
        Assert.assertFalse(Geometries.isCounterClockwise(lineStart, lineEnd, pointRight));
    }

    @Test
    public void testIsAtRightOf() {
        //on line is false
        Assert.assertFalse(Geometries.isAtRightOf(new DefaultPoint(lineStart), new DefaultPoint(lineEnd), new DefaultPoint(pointOnLine)));
        Assert.assertFalse(Geometries.isAtRightOf(new DefaultPoint(lineStart), new DefaultPoint(lineEnd), new DefaultPoint(pointLeft)));
        Assert.assertTrue(Geometries.isAtRightOf(new DefaultPoint(lineStart), new DefaultPoint(lineEnd), new DefaultPoint(pointRight)));
        //on line is false
        Assert.assertFalse(Geometries.isAtRightOf(lineStart, lineEnd, pointOnLine));
        Assert.assertFalse(Geometries.isAtRightOf(lineStart, lineEnd, pointLeft));
        Assert.assertTrue(Geometries.isAtRightOf(lineStart, lineEnd, pointRight));
    }

    @Test
    public void testProjectPointOnPlan() {
        final Plane plane = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(0, 4, 0));
        final VectorRW center = new Vector3f64(0, 5, 0);
        double[] proj = Geometries.projectPointOnPlan(center.toDouble(), plane.getNormal().toDouble(), plane.getD());

        Assert.assertEquals(0, proj[0], DELTA);
        Assert.assertEquals(4, proj[1], DELTA);
        Assert.assertEquals(0, proj[2], DELTA);
    }

    @Test
    public void testInCircle1() {
        final Point a = new DefaultPoint(-1, 0);
        final Point b = new DefaultPoint(0, 1);
        final Point c = new DefaultPoint(1, 0);
        final Point d = new DefaultPoint(0, 0);

        Assert.assertTrue(Geometries.inCircle(a, b, c, d));
    }

    @Test
    public void testInCircle2() {
        final Point a = new DefaultPoint(1, 0);
        final Point b = new DefaultPoint(0, 1);
        final Point c = new DefaultPoint(-1, 0);
        final Point d = new DefaultPoint(0, 0);

        Assert.assertTrue(Geometries.inCircle(a, b, c, d));
    }

    @Test
    public void testInCircle3() {
        final Point a = new DefaultPoint(1, 0);
        final Point b = new DefaultPoint(0, -1);
        final Point c = new DefaultPoint(0, 1);
        final Point d = new DefaultPoint(0, 0);

        Assert.assertTrue(Geometries.inCircle(a, b, c, d));
    }

    @Test
    public void testInCircle4() {
        final Point a = new DefaultPoint(1, 0);
        final Point b = new DefaultPoint(0, 1);
        final Point c = new DefaultPoint(0, -1);
        final Point d = new DefaultPoint(0, 0);

        Assert.assertTrue(Geometries.inCircle(a, b, c, d));
    }

    @Test
    public void testInCircle5() {
        final Point a = new DefaultPoint(-1, 0);
        final Point b = new DefaultPoint(0, 1);
        final Point c = new DefaultPoint(1, 0);
        final Point d = new DefaultPoint(10, 10);

        Assert.assertFalse(Geometries.inCircle(a, b, c, d));
    }

    @Test
    public void testInCircle6() {
        final Point a = new DefaultPoint(-1, 0);
        final Point b = new DefaultPoint(0, 1);
        final Point c = new DefaultPoint(1, 0);
        final Point d = new DefaultPoint(0, 1+DELTA);

        Assert.assertFalse(Geometries.inCircle(a, b, c, d));
    }

    @Test
    public void testBBoxTransform() {

        BBox bbox = new BBox(new double[]{10,20}, new double[]{100,200});
        Affine2 trs = new Affine2(1, 0, 50, 0, 1, -50);
        BBox result = Geometries.transform(bbox, trs, null);

        Assert.assertEquals(10+50,result.getMin(0),DELTA);
        Assert.assertEquals(100+50,result.getMax(0),DELTA);
        Assert.assertEquals(20-50,result.getMin(1),DELTA);
        Assert.assertEquals(200-50,result.getMax(1),DELTA);


    }

}
