

package science.unlicense.geometry.impl.s3d;

import science.unlicense.geometry.impl.Capsule;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.impl.Vector3f64;

/**
 * capsule class tests.
 *
 * @author Johann Sorel
 */
public class CapsuleTest {

    private static final double DELTA = 0.0000000001;

    @Test
    public void testPoints(){

        Capsule capsule = new Capsule(10, 1);
        Assert.assertArrayEquals(new double[]{0, -5, 0}, capsule.getFirst().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{0, +5, 0}, capsule.getSecond().toDouble(), DELTA);

        capsule = new Capsule(new Vector3f64(-5, 3, 7), new Vector3f64(8, -9, 2), 1);
        Assert.assertArrayEquals(new double[]{-5, 3, 7}, capsule.getFirst().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{8, -9, 2}, capsule.getSecond().toDouble(), DELTA);

    }

}
