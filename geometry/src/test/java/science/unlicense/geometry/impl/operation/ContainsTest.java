
package science.unlicense.geometry.impl.operation;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.operation.ContainsOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.Operations;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class ContainsTest {

    @Test
    public void testRectangle_point() throws OperationException{
        final Rectangle geom1 = new Rectangle(5, 10, 4, 4);
        Point geom2 = new DefaultPoint(6, 13);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new ContainsOp(geom1, geom2)));
        geom2 = new DefaultPoint(10, 13);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new ContainsOp(geom1, geom2)));
        geom2 = new DefaultPoint(6, 8);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new ContainsOp(geom1, geom2)));
    }

    @Test
    public void testBbox_point() throws OperationException{
        final BBox geom1 = new BBox(new double[]{5,10}, new double[]{9,14});
        Point geom2 = new DefaultPoint(6, 13);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new ContainsOp(geom1, geom2)));
        geom2 = new DefaultPoint(10, 13);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new ContainsOp(geom1, geom2)));
        geom2 = new DefaultPoint(6, 8);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new ContainsOp(geom1, geom2)));
    }

    @Test
    public void testTriangle_point() throws OperationException{
        Triangle geom1 = new DefaultTriangle(new Vector2f64(5, 10), new Vector2f64(10, 10), new Vector2f64(10,15));
        Point geom2 = new DefaultPoint(9, 12);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new ContainsOp(geom1, geom2)));
        geom2 = new DefaultPoint(6, 13);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new ContainsOp(geom1, geom2)));
        geom2 = new DefaultPoint(2, 4);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new ContainsOp(geom1, geom2)));
    }

    @Test
    public void testCircle_point() throws OperationException{
        Circle geom1 = new Circle(8, 11, 2);
        Point geom2 = new DefaultPoint(9, 12);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new ContainsOp(geom1, geom2)));
        geom2 = new DefaultPoint(6, 13);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new ContainsOp(geom1, geom2)));
        geom2 = new DefaultPoint(2, 4);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new ContainsOp(geom1, geom2)));
    }

    @Test
    public void testCurvepolygon_point() throws OperationException{

        final Polyline exterior = new Polyline(
            InterleavedTupleGrid1D.create(new double[]{
                0,0,
                5,0,
                5,3,
                0,3,
                0,0
            }, 2));
        final Polygon geom1 = new Polygon(exterior, new ArraySequence());

        Point geom2 = new DefaultPoint(1.3, 2.8);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new ContainsOp(geom1, geom2)));
        geom2 = new DefaultPoint(-2, 2.8);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new ContainsOp(geom1, geom2)));
        geom2 = new DefaultPoint(1.3, 3.1);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new ContainsOp(geom1, geom2)));
    }
}
