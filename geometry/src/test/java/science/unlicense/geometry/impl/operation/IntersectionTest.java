

package science.unlicense.geometry.impl.operation;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.operation.IntersectionOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.Operations;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class IntersectionTest {

    private static final double DELTA = 0.00000001;

    @Test
    public void testRay_plane() throws OperationException{
        Ray ray;
        Plane plane;

        //simple test
        ray   = new Ray(new Vector3f64(-10, 0, 0), new Vector3f64(1, 0, 0));
        plane = new Plane(new Vector3f64(-1,0,0), new Vector3f64(0,0,0));
        Assert.assertEquals(new DefaultPoint(0, 0, 0), new IntersectionOp(ray, plane).execute());

        //offset
        ray   = new Ray(new Vector3f64(-10, 0, 0), new Vector3f64(1, 0, 0));
        plane = new Plane(new Vector3f64(-1,0,0), new Vector3f64(15,-8,7));
        Assert.assertEquals(new DefaultPoint(15, 0, 0), new IntersectionOp(ray, plane).execute());

        //ray pointing opposite direction
        ray   = new Ray(new Vector3f64(-10, 0, 0), new Vector3f64(-1, 0, 0));
        plane = new Plane(new Vector3f64(-1,0,0), new Vector3f64(0,0,0));
        Assert.assertEquals(null, new IntersectionOp(ray, plane).execute());

        //ray offset and parallal to plane
        ray   = new Ray(new Vector3f64(-10, 0, 0), new Vector3f64(0, 1, 0));
        plane = new Plane(new Vector3f64(-1,0,0), new Vector3f64(0,0,0));
        Assert.assertEquals(null, new IntersectionOp(ray, plane).execute());

        //ray parallal and on the plane
        ray   = new Ray(new Vector3f64(0, 0, 0), new Vector3f64(0, 1, 0));
        plane = new Plane(new Vector3f64(-1,0,0), new Vector3f64(0,0,0));
        Assert.assertEquals(new Ray(new Vector3f64(0, 0, 0), new Vector3f64(0, 1, 0)), new IntersectionOp(ray, plane).execute());

    }

    @Test
    public void testRay_segment() throws OperationException{
        Ray ray;
        Line segment;

        //simple test
        ray   = new Ray(new Vector3f64(-10, 0, 0), new Vector3f64(1, 0, 0));
        segment = new DefaultLine(new Vector3f64(0,1,0), new Vector3f64(0,-1,0));
        Assert.assertEquals(new DefaultPoint(0, 0, 0), new IntersectionOp(ray, segment).execute());
    }

    @Test
    public void testRectangle_rectangle() throws OperationException{
        final Rectangle r1 = new Rectangle(2, 2, 4, 4);
        final Rectangle r2 = new Rectangle(3, 4, 4, 4);

        final Rectangle r3 = (Rectangle) r1.intersection(r2);
        Assert.assertEquals(3, r3.getX(),DELTA);
        Assert.assertEquals(4, r3.getY(),DELTA);
        Assert.assertEquals(3, r3.getWidth(),DELTA);
        Assert.assertEquals(2, r3.getHeight(),DELTA);

    }

    @Test
    public void testPlane_plane() throws OperationException{

        //plane parallel, no intersection
        Plane geom1 = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(5, 6, 7));
        Plane geom2 = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(5, 9, 7));
        Assert.assertEquals(null, Operations.execute(new IntersectionOp(geom1, geom2)));

        //plane overlaps, result should be same as plane1
        geom2 = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(5, 6, 7));
        Assert.assertEquals(geom1, Operations.execute(new IntersectionOp(geom1, geom2)));

        //plane intersect
        geom1 = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(0, 6, 0));
        geom2 = new Plane(new Vector3f64(0, 0, 1), new Vector3f64(0, 6, 0));
        Object res = Operations.execute(new IntersectionOp(geom1, geom2));
        Assert.assertTrue(res instanceof Ray);
        Ray ray = (Ray) res;
        Assert.assertEquals(new Vector3f64(1,0,0), ray.getDirection());
        Assert.assertEquals(new Vector3f64(0,6,0), ray.getPosition());

    }

}
