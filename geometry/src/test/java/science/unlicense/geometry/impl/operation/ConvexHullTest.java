package science.unlicense.geometry.impl.operation;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.operation.ConvexHullOp;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.api.Point;

public class ConvexHullTest {

    // Simple test of triangle
    // all elements in triangle
    @Test
    public void testConvexHull1() throws Exception {
        Point[] points = new Point[]{
                new DefaultPoint(0, 0),
                new DefaultPoint(0, 1),
                new DefaultPoint(1, 0)
        };
        Point[] convexPoints = ConvexHullOp.convexHull(points);
        Assert.assertTrue(convexPoints.length == 3);
        Assert.assertTrue(points[0].equals(convexPoints[0]));
        Assert.assertTrue(points[1].equals(convexPoints[1]));
        Assert.assertTrue(points[2].equals(convexPoints[2]));
    }

    // ConvexHull test of rectangle
    // all elements is convexHull
    @Test
    public void testConvexHull2() throws Exception {
        Point[] points = new Point[]{
                new DefaultPoint(0, 0),
                new DefaultPoint(0, 1),
                new DefaultPoint(1, 0),
                new DefaultPoint(1, 1),
        };
        Point[] convexPoints = ConvexHullOp.convexHull(points);
        Assert.assertTrue(convexPoints.length == 4);
        Assert.assertTrue(points[0].equals(convexPoints[0]));
        Assert.assertTrue(points[1].equals(convexPoints[1]));
        Assert.assertTrue(points[3].equals(convexPoints[2]));
        Assert.assertTrue(points[2].equals(convexPoints[3]));
    }

    @Test
    public void testConvexHull3() throws Exception {
        Point[] points = new Point[]{
                new DefaultPoint(0, 0),
                new DefaultPoint(0, 1),
                new DefaultPoint(1, 0),
                new DefaultPoint(1, 1),
                new DefaultPoint(0.5,0.5)
        };
        Point[] convexPoints = ConvexHullOp.convexHull(points);
        Assert.assertTrue(convexPoints.length == 4);
        Assert.assertTrue(points[0].equals(convexPoints[0]));
        Assert.assertTrue(points[1].equals(convexPoints[1]));
        Assert.assertTrue(points[2].equals(convexPoints[3]));
        Assert.assertTrue(points[3].equals(convexPoints[2]));
    }

    @Test
    public void testConvexHull4() {
        Point[] points = new Point[]{
                new DefaultPoint(34, 90),
                new DefaultPoint(99, 47),
                new DefaultPoint(34, 53),
                new DefaultPoint(88, 31),
                new DefaultPoint(1, 92)
        };
        Point[] convexPoints = ConvexHullOp.convexHull(points);
        Assert.assertTrue(convexPoints.length == 5);
        Assert.assertTrue(points[4].equals(convexPoints[0]));
        Assert.assertTrue(points[0].equals(convexPoints[1]));
        Assert.assertTrue(points[1].equals(convexPoints[2]));
        Assert.assertTrue(points[3].equals(convexPoints[3]));
        Assert.assertTrue(points[2].equals(convexPoints[4]));
    }

    @Test
    public void testConvexHull5() {
        Point[] points = new Point[]{
                new DefaultPoint(51, 34),
                new DefaultPoint(0, 31),
                new DefaultPoint(51, 26),
                new DefaultPoint(51, 67),
                new DefaultPoint(5, 93)
        };
        Point[] convexPoints = ConvexHullOp.convexHull(points);
        Assert.assertTrue(convexPoints.length == 5);
        Assert.assertTrue(points[1].equals(convexPoints[0]));
        Assert.assertTrue(points[4].equals(convexPoints[1]));
        Assert.assertTrue(points[3].equals(convexPoints[2]));
        Assert.assertTrue(points[0].equals(convexPoints[3]));
        Assert.assertTrue(points[2].equals(convexPoints[4]));
    }
}
