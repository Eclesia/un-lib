
package science.unlicense.geometry.impl.s3d;

import science.unlicense.geometry.impl.Plane;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class PlaneTest {

    private static final double DELTA = 0.0000001;

    @Test
    public void testComputePlaneFromPoints(){

        final Plane plane = Plane.computeBestFittingPlan(new Tuple[]{
            new Vector3f64(-1, 0, 0),
            new Vector3f64(0, -1, 0),
            new Vector3f64(0, +1, 0),
            new Vector3f64(+1, 0, 0)
        });

        Assert.assertEquals(new Vector3f64(0, 0, -1), plane.getNormal());
        Assert.assertEquals(new Vector3f64(0, 0, 0), plane.getPoint());
    }

    @Test
    public void testCalculateAngle(){

        final Plane plane = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(0, 0, 0));

        Assert.assertEquals(Maths.HALF_PI, plane.calculateAngle(new Vector3f64(0, 15, 0)), DELTA);
        Assert.assertEquals(0.0, plane.calculateAngle(new Vector3f64(0.5, 0, 0)), DELTA);
        Assert.assertEquals(0.0, plane.calculateAngle(new Vector3f64(0, 0, 10)), DELTA);
        Assert.assertEquals(Maths.QUATER_PI, plane.calculateAngle(new Vector3f64(1, 1, 0)), DELTA);
    }

}
