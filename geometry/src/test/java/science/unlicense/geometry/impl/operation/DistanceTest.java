
package science.unlicense.geometry.impl.operation;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.Operations;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.OBBox;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.Sheet;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class DistanceTest {

    private static final double DELTA = 0.00000001;

    @Test
    public void testPoint_point() throws OperationException{
        Point geom1 = new DefaultPoint(10, 20);
        Point geom2 = new DefaultPoint(10, 20);
        Assert.assertEquals(0d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(0d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom2 = new DefaultPoint(11, 20);
        Assert.assertEquals(1d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(1d, Operations.execute(new DistanceOp(geom2, geom1)));
    }

    @Test
    public void testPoint_line() throws OperationException{
        Point geom1 = new DefaultPoint(10, 20);
        Line geom2 = new DefaultLine(10, 20, 15, 20);
        Assert.assertEquals(0d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(0d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom2 = new DefaultLine(11, 20, 12, 20);
        Assert.assertEquals(1d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(1d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom2 = new DefaultLine(5, 20, 15, 20);
        Assert.assertEquals(0d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(0d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom2 = new DefaultLine(5, 22, 15, 22);
        Assert.assertEquals(2d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(2d, Operations.execute(new DistanceOp(geom2, geom1)));
    }

    @Test
    public void testPoint_circle() throws OperationException{
        Point geom1 = new DefaultPoint(10, 20);
        Circle geom2 = new Circle(10, 2, 19);
        Assert.assertEquals(-1d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(-1d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom2 = new Circle(10, -1, 5);
        Assert.assertEquals(16d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(16d, Operations.execute(new DistanceOp(geom2, geom1)));
    }

    @Test
    public void testPoint_plane() throws OperationException{
        Point geom1 = new DefaultPoint(0, 10, 0);
        Plane geom2 = new Plane(new Vector3f64(1,0,0), new Vector3f64(0,0,0), new Vector3f64(0,0,1));
        Assert.assertEquals(10d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(10d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom1 = new DefaultPoint(35, 8, -12);
        Assert.assertEquals(8d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(8d, Operations.execute(new DistanceOp(geom2, geom1)));

        //point is in the plan
        geom1 = new DefaultPoint(35, -6, -12);
        Assert.assertEquals(0.0, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(0.0, Operations.execute(new DistanceOp(geom2, geom1)));
    }

    @Test
    public void testPoint_sheet() throws OperationException{
        Point geom1 = new DefaultPoint(0, 10, 0);
        Sheet geom2 = new Sheet(new Vector3f64(1,0,0), new Vector3f64(0,0,0), new Vector3f64(0,0,1));
        Assert.assertEquals(10d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(10d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom1 = new DefaultPoint(35, 8, -12);
        Assert.assertEquals(8d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(8d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom1 = new DefaultPoint(35, -6, -12);
        Assert.assertEquals(6d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(6d, Operations.execute(new DistanceOp(geom2, geom1)));
    }

    @Test
    public void testPoint_rectangle() throws OperationException{
        Point geom1 = new DefaultPoint(0, 0);
        Rectangle geom2 = new Rectangle(5,10,20,5);
        double result = (Double) Operations.execute(new DistanceOp(geom1, geom2));
        double expected = Math.sqrt(5*5 + 10*10);
        Assert.assertEquals(expected, result, DELTA);

        //point inside
        geom1 = new DefaultPoint(10,12);
        result = (Double) Operations.execute(new DistanceOp(geom1, geom2));
        Assert.assertEquals(0.0, result, DELTA);
    }

    @Test
    public void testPoint_bbox() throws OperationException{
        Point geom1 = new DefaultPoint(0, 0);
        BBox geom2 = new BBox(new double[]{5,10},new double[]{25,15});
        double result = (Double) Operations.execute(new DistanceOp(geom1, geom2));
        double expected = Math.sqrt(5*5 + 10*10);
        Assert.assertEquals(expected, result, DELTA);

        //point inside
        geom1 = new DefaultPoint(10,12);
        result = (Double) Operations.execute(new DistanceOp(geom1, geom2));
        Assert.assertEquals(0.0, result, DELTA);
    }

    @Test
    public void testPoint_obbox() throws OperationException{
        Point geom1 = new DefaultPoint(0, 0);
        OBBox geom2 = new OBBox(new double[]{0,0},new double[]{20,5});
        geom2.getTransform().getTranslation().setXY(5, 10);
        geom2.getTransform().notifyChanged();
        double result = (Double) Operations.execute(new DistanceOp(geom1, geom2));
        double expected = Math.sqrt(5*5 + 10*10);
        Assert.assertEquals(expected, result, DELTA);

        //point inside
        geom1 = new DefaultPoint(10,12);
        result = (Double) Operations.execute(new DistanceOp(geom1, geom2));
        Assert.assertEquals(0.0, result, DELTA);
    }

    @Test
    public void testLine_line() throws OperationException{
        Line geom1 = new DefaultLine(10, 20, 15, 20);
        Line geom2 = new DefaultLine(10, 20, 15, 20);
        Assert.assertEquals(0d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(0d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom2 = new DefaultLine(-5, -2, 15, 20);
        Assert.assertEquals(0d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(0d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom2 = new DefaultLine(10, 21, 15, 21);
        Assert.assertEquals(1d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(1d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom2 = new DefaultLine(16, 20, 17, 20);
        Assert.assertEquals(1d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(1d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom2 = new DefaultLine(19, 5, 19, 50);
        Assert.assertEquals(4d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(4d, Operations.execute(new DistanceOp(geom2, geom1)));
    }

    @Test
    public void testCircle_circle() throws OperationException{
        Circle geom1 = new Circle(10, 5, 2);
        Circle geom2 = new Circle(10, 2, 2);
        Assert.assertEquals(-1d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(-1d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom2 = new Circle(10, 2, 0.99);
        Assert.assertEquals(0.01d, (Double) Operations.execute(new DistanceOp(geom1, geom2)),DELTA);
        Assert.assertEquals(0.01d, (Double) Operations.execute(new DistanceOp(geom2, geom1)),DELTA);
        geom2 = new Circle(3, 5, 2);
        Assert.assertEquals(3d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(3d, Operations.execute(new DistanceOp(geom2, geom1)));
    }

    @Test
    public void testSphere_sphere() throws OperationException{
        Sphere geom1 = new Sphere(1); geom1.getTransform().getTranslation().setXYZ(2, -3, 5);
        Sphere geom2 = new Sphere(2.2); geom2.getTransform().getTranslation().setXYZ(2, -1, 5);
        Assert.assertTrue((Double) Operations.execute(new DistanceOp(geom1, geom2))<=0);
        Assert.assertTrue((Double) Operations.execute(new DistanceOp(geom1, geom2))<=0);
        geom2 = new Sphere(1.5); geom2.getTransform().getTranslation().setXYZ(5, -3, 5);
        Assert.assertEquals(0.5, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(0.5, Operations.execute(new DistanceOp(geom2, geom1)));
    }

    @Test
    public void testSphere_plane() throws OperationException{
        Sphere geom1 = new Sphere(3); geom1.getTransform().getTranslation().setXYZ(0, 10, 0);
        Plane geom2 = new Plane(new Vector3f64(1,0,0), new Vector3f64(0,0,0), new Vector3f64(0,0,1));
        Assert.assertEquals(7d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(7d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom1 = new Sphere(3); geom1.getTransform().getTranslation().setXYZ(35, 8, -12);
        Assert.assertEquals(5d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(5d, Operations.execute(new DistanceOp(geom2, geom1)));
        geom1 = new Sphere(3); geom1.getTransform().getTranslation().setXYZ(35, -6, -12);
        Assert.assertEquals(-9d, Operations.execute(new DistanceOp(geom1, geom2)));
        Assert.assertEquals(-9d, Operations.execute(new DistanceOp(geom2, geom1)));
    }

    @Test
    public void testCcapsule_capsule() throws OperationException{
        Capsule geom1 = new Capsule(new Vector3f64(10, 20, 5), new Vector3f64(15, 20, 5), 1);
        Capsule geom2 = new Capsule(new Vector3f64(10, 20, 5), new Vector3f64(15, 20, 5), 1);
        Assert.assertTrue((Double) Operations.execute(new DistanceOp(geom1, geom2))<=0);
        Assert.assertTrue((Double) Operations.execute(new DistanceOp(geom1, geom2))<=0);

        geom2 = new Capsule(new Vector3f64(-5, -2, 5), new Vector3f64(15, 20, 5), 1);
        Assert.assertTrue((Double) Operations.execute(new DistanceOp(geom1, geom2))<=0);
        Assert.assertTrue((Double) Operations.execute(new DistanceOp(geom1, geom2))<=0);

        geom1 = new Capsule(new Vector3f64(10, 20, 5), new Vector3f64(15, 20, 5), 0.2);
        geom2 = new Capsule(new Vector3f64(10, 21, 5), new Vector3f64(15, 21, 5), 0.3);
        Assert.assertEquals(0.5d, (Double) Operations.execute(new DistanceOp(geom1, geom2)), DELTA);
        Assert.assertEquals(0.5d, (Double) Operations.execute(new DistanceOp(geom2, geom1)), DELTA);

        geom2 = new Capsule(new Vector3f64(16, 20, 5), new Vector3f64(17, 20, 5), 0.3);
        Assert.assertEquals(0.5d, (Double) Operations.execute(new DistanceOp(geom1, geom2)), DELTA);
        Assert.assertEquals(0.5d, (Double) Operations.execute(new DistanceOp(geom2, geom1)), DELTA);

        geom2 = new Capsule(new Vector3f64(19, 5, 5), new Vector3f64(19, 50, 5), 0.3);
        //19 - 15 - 0.2 - 0.3 = 3.5
        Assert.assertEquals(3.5d, (Double) Operations.execute(new DistanceOp(geom1, geom2)), DELTA);
        Assert.assertEquals(3.5d, (Double) Operations.execute(new DistanceOp(geom2, geom1)), DELTA);
    }

    @Test
    public void testDistanceLineAndPoint() {
        {
            final Point a = new DefaultPoint(0, 0);
            final Point b = new DefaultPoint(0, 1);
            final Point c = new DefaultPoint(1, 0);
            Assert.assertTrue(DistanceOp.distanceLineAndPoint(a, b, c) - 1 < 1e-8);
        }
        {
            final Point a = new DefaultPoint(0, 0);
            final Point b = new DefaultPoint(1, 0);
            final Point c = new DefaultPoint(0, 1);
            Assert.assertTrue(DistanceOp.distanceLineAndPoint(a, b, c) - 1 < 1e-8);
        }
        {
            final Point a = new DefaultPoint(0, 0);
            final Point b = new DefaultPoint(1, 1);
            final Point c = new DefaultPoint(0, 1);
            Assert.assertTrue(DistanceOp.distanceLineAndPoint(a, b, c) > 0);
        }
    }
}
