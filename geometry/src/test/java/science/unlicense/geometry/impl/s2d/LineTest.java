
package science.unlicense.geometry.impl.s2d;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.operation.AreaOp;
import science.unlicense.geometry.api.operation.CentroidOp;
import science.unlicense.geometry.api.operation.LengthOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author bertrand
 */
public class LineTest {

    private static final int startIndex = 0;
    private static final int endIndex = 1;
    private static final int areaLengthIndex = 2;
    private static final int bboxMinIndex = 3;
    private static final int bboxMaxIndex = 4;
    private static final int boundingCircleIndex = 5;
    private static final int centroidIndex = 6;

    private static final double[][][] lineParametersTest = new double[][][] {
        // { { startX, startY }, { endX, endY }, { area, length }, { bboxXMin, bboxYMin }, { boxXMax, bboxYMax }, { boundingCircleCx, boundingCircleCy, radius }, { centroidX, centroidY } },
        { {  1, 2 }, { -3, -4 }, { 0, Math.sqrt(52.) }, { -3, -4 }, { 1, 2 }, { -1,  -1, Math.sqrt(52.)/2. }, { -1,  -1 } },
        { { -3, 5 }, {  2, -1 }, { 0, Math.sqrt(61.) }, { -3, -1 }, { 2, 5 }, { -0.5, 2, Math.sqrt(61.)/2. }, { -0.5, 2 } },
    };

    private static final Line[] lineTest;

    static {
        lineTest = new Line[lineParametersTest.length];
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            lineTest[i] = new DefaultLine( lineParametersTest[i][startIndex][0], lineParametersTest[i][startIndex][1], lineParametersTest[i][endIndex][0], lineParametersTest[i][endIndex][1]);
        }
    }

    public LineTest() {
    }

    /**
     * Test of getStart method, of class Line.
     */
    @Test
    public void testGetStart() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            Line instance = lineTest[i];
            Tuple expResult = new Vector2f64( lineParametersTest[i][startIndex][0], lineParametersTest[i][startIndex][1] );
            Tuple result = instance.getStart();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of setStart method, of class Line.
     */
    @Test
    public void testSetStart() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            DefaultLine instance = new DefaultLine(2);
            instance.setStart(new Vector2f64( lineParametersTest[i][startIndex][0], lineParametersTest[i][startIndex][1] ) );
            Tuple expResult = new Vector2f64( lineParametersTest[i][startIndex][0], lineParametersTest[i][startIndex][1] );
            Tuple result = instance.getStart();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of getStartX method, of class Line.
     */
    @Test
    public void testGetStartX() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            Line instance = lineTest[i];
            double expResult = lineParametersTest[i][startIndex][0];
            double result = instance.getStart().get(0);
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setStartX method, of class Line.
     */
    @Test
    public void testSetStartX() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            DefaultLine instance = new DefaultLine(2);
            instance.getStart().set(0,lineParametersTest[i][startIndex][0]);
            double expResult = lineParametersTest[i][startIndex][0];
            double result = instance.getStart().get(0);
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getStartY method, of class Line.
     */
    @Test
    public void testGetStartY() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            Line instance = lineTest[i];
            double expResult = lineParametersTest[i][startIndex][1];
            double result = instance.getStart().get(1);
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setStartY method, of class Line.
     */
    @Test
    public void testSetStartY() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            DefaultLine instance = new DefaultLine(2);
            instance.getStart().set(1,lineParametersTest[i][startIndex][1]);
            double expResult = lineParametersTest[i][startIndex][1];
            double result = instance.getStart().get(1);
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getEnd method, of class Line.
     */
    @Test
    public void testGetEnd() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            Line instance = lineTest[i];
            Tuple expResult = new Vector2f64( lineParametersTest[i][endIndex][0], lineParametersTest[i][endIndex][1] );
            Tuple result = instance.getEnd();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of setEnd method, of class Line.
     */
    @Test
    public void testSetEnd() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            DefaultLine instance = new DefaultLine(2);
            instance.setEnd(new Vector2f64( lineParametersTest[i][endIndex][0], lineParametersTest[i][endIndex][1] ) );
            Tuple expResult = new Vector2f64( lineParametersTest[i][endIndex][0], lineParametersTest[i][endIndex][1] );
            Tuple result = instance.getEnd();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of getEndX method, of class Line.
     */
    @Test
    public void testGetEndX() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            Line instance = lineTest[i];
            double expResult = lineParametersTest[i][endIndex][0];
            double result = instance.getEnd().get(0);
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setEndX method, of class Line.
     */
    @Test
    public void testSetEndX() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            DefaultLine instance = new DefaultLine(2);
            instance.getEnd().set(0,lineParametersTest[i][endIndex][0]);
            double expResult = lineParametersTest[i][endIndex][0];
            double result = instance.getEnd().get(0);
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getEndY method, of class Line.
     */
    @Test
    public void testGetEndY() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            Line instance = lineTest[i];
            double expResult = lineParametersTest[i][endIndex][1];
            double result = instance.getEnd().get(1);
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setEndY method, of class Line.
     */
    @Test
    public void testSetEndY() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            DefaultLine instance = new DefaultLine(2);
            instance.getEnd().set(1,lineParametersTest[i][startIndex][1]);
            double expResult = lineParametersTest[i][startIndex][1];
            double result = instance.getEnd().get(1);
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getArea method, of class Line.
     */
    @Test
    public void testGetArea() throws OperationException {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            Line instance = new DefaultLine(2);
            double expResult = lineParametersTest[i][areaLengthIndex][0];
            double result = new AreaOp(instance).execute();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getLength method, of class Line.
     */
    @Test
    public void testGetLength() throws OperationException {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            Line instance = lineTest[i];
            double expResult = lineParametersTest[i][areaLengthIndex][1];
            double result = new LengthOp(instance).execute();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getBoundingBox method, of class Line.
     */
    @Test
    public void testGetBoundingBox() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            Line instance = lineTest[i];
            BBox expResult = new BBox( lineParametersTest[i][bboxMinIndex], lineParametersTest[i][bboxMaxIndex]);
            BBox result = instance.getBoundingBox();
            Assert.assertTrue(expResult.equals(result));
        }
    }

    /**
     * Test of getBoundingCircle method, of class Line.
     */
    @Test
    public void testGetBoundingCircle() {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            DefaultLine instance = (DefaultLine) lineTest[i];
            Circle expResult = new Circle( lineParametersTest[i][boundingCircleIndex][0], lineParametersTest[i][boundingCircleIndex][1], lineParametersTest[i][boundingCircleIndex][2]);
            Circle result = instance.getBoundingCircle();
            Assert.assertTrue(expResult.equals(result));
        }
    }

    /**
     * Test of getCentroid method, of class Line.
     */
    @Test
    public void testGetCentroid() throws OperationException {
        for ( int i=0; i<lineParametersTest.length; i++ ) {
            Line instance = lineTest[i];
            Point expResult = new DefaultPoint( lineParametersTest[i][centroidIndex][0], lineParametersTest[i][boundingCircleIndex][1]);
            Point result = new CentroidOp(instance).execute();
            Assert.assertTrue(expResult.equals(result));
        }
    }

}
