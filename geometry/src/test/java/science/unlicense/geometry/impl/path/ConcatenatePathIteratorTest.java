
package science.unlicense.geometry.impl.path;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class ConcatenatePathIteratorTest {

    @Test
    public void testConcatenate(){

        final Tuple i1 = new Vector2f64(5, 10);
        final Tuple i2 = new Vector2f64(10, 15);
        final Tuple i3 = new Vector2f64(15, 20);
        final Tuple i4 = new Vector2f64(20, 25);
        final Tuple i5 = new Vector2f64(25, 30);
        final Tuple i6 = new Vector2f64(30, 35);

        final CoordinateTuplePathIterator ite1 = new CoordinateTuplePathIterator(new Tuple[]{i1,i2,i3});
        final CoordinateTuplePathIterator ite2 = new CoordinateTuplePathIterator(new Tuple[]{i4,i5,i6});
        final Sequence seq = new ArraySequence();
        seq.add(ite1);
        seq.add(ite2);
        final ConcatenatePathIterator ite = new ConcatenatePathIterator(seq);


        TupleRW c = new Vector2f64();
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO,ite.getType());
        Assert.assertEquals(i1, ite.getPosition(c));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO,ite.getType());
        Assert.assertEquals(i2, ite.getPosition(c));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO,ite.getType());
        Assert.assertEquals(i3, ite.getPosition(c));

        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO,ite.getType());
        Assert.assertEquals(i4, ite.getPosition(c));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO,ite.getType());
        Assert.assertEquals(i5, ite.getPosition(c));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO,ite.getType());
        Assert.assertEquals(i6, ite.getPosition(c));

        Assert.assertFalse(ite.next());


    }

}
