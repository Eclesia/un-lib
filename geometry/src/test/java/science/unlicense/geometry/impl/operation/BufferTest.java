
package science.unlicense.geometry.impl.operation;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.operation.BufferOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.Operations;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.api.Point;

/**
 *
 * @author Johann Sorel
 */
public class BufferTest {

    @Test
    public void testPoint() throws OperationException{
        final Point point = new DefaultPoint(5, 10);

        Object result = Operations.execute(new BufferOp(point, 3));
        Assert.assertEquals(new Circle(5, 10, 3), result);

        result = Operations.execute(new BufferOp(point, -2));
        Assert.assertEquals(new DefaultPoint(5, 10), result);

    }

    @Test
    public void testCircle() throws OperationException{
        final Circle circle = new Circle(5, 10, 6);

        Object result = Operations.execute(new BufferOp(circle, 3));
        Assert.assertEquals(new Circle(5, 10, 9), result);

        result = Operations.execute(new BufferOp(circle, -6));
        Assert.assertEquals(new DefaultPoint(5, 10), result);

        result = Operations.execute(new BufferOp(circle, -9));
        Assert.assertEquals(new DefaultPoint(5, 10), result);

    }

}
