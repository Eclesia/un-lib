
package science.unlicense.geometry.impl.triangulate;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class EarClippingTest {

    /**
     * Test a triangle geometry.
     */
    @Test
    public void testTriangle() throws OperationException{

        final VectorRW v1 = new Vector2f64(1,2);
        final VectorRW v2 = new Vector2f64(10,20);
        final VectorRW v3 = new Vector2f64(5,5);

        //test correct rotation : clockwise
        DefaultTriangle geometry = new DefaultTriangle(v1, v2, v3);
        EarClipping ec = new EarClipping();
        Sequence triangles = ec.triangulate(geometry);
        Assert.assertEquals(1, triangles.getSize());
        Triangle t = (Triangle) triangles.get(0);
        Assert.assertEquals(new DefaultTriangle(v3,v1,v2), t);

        //test reverse rotation : counter-clockwise
        geometry = new DefaultTriangle(v1, v3, v2);
        ec = new EarClipping();
        triangles = ec.triangulate(geometry);
        Assert.assertEquals(1, triangles.getSize());
        t = (Triangle) triangles.get(0);
        Assert.assertEquals(new DefaultTriangle(v3,v1,v2), t);

    }

    /**
     * Test a square geometry.
     */
    @Test
    public void testQuad() throws OperationException{

        final VectorRW v1 = new Vector2f64(0,0);
        final VectorRW v2 = new Vector2f64(0,10);
        final VectorRW v3 = new Vector2f64(10,10);
        final VectorRW v4 = new Vector2f64(10,0);

        //test correct rotation : clockwise
        Polygon geometry = new Polygon(new Polyline(Geometries.toTupleBuffer(new Tuple[]{v1,v2,v3,v4,v1})), null);
        EarClipping ec = new EarClipping();
        Sequence triangles = ec.triangulate(geometry);
        Assert.assertEquals(2, triangles.getSize());
        Triangle t1 = (Triangle) triangles.get(0);
        Triangle t2 = (Triangle) triangles.get(1);
        Assert.assertEquals(new DefaultTriangle(v4,v1,v2), t1);
        Assert.assertEquals(new DefaultTriangle(v4,v2,v3), t2);

        //test reverse rotation : counter-clockwise
        geometry = new Polygon(new Polyline(Geometries.toTupleBuffer(new Tuple[]{v1,v4,v3,v2,v1})), null);
        ec = new EarClipping();
        triangles = ec.triangulate(geometry);
        Assert.assertEquals(2, triangles.getSize());
        t1 = (Triangle) triangles.get(0);
        t2 = (Triangle) triangles.get(1);
        Assert.assertEquals(new DefaultTriangle(v4,v1,v2), t1);
        Assert.assertEquals(new DefaultTriangle(v4,v2,v3), t2);

    }

    /**
     * Test a V shape geometry.
     */
    @Test
    public void testV() throws OperationException{

        final VectorRW v1 = new Vector2f64(5,0);
        final VectorRW v2 = new Vector2f64(0,10);
        final VectorRW v3 = new Vector2f64(5,5);
        final VectorRW v4 = new Vector2f64(10,10);

        //test correct rotation : clockwise
        Polygon geometry = new Polygon(new Polyline(Geometries.toTupleBuffer(new Tuple[]{v1,v2,v3,v4,v1})), null);
        EarClipping ec = new EarClipping();
        Sequence triangles = ec.triangulate(geometry);
        Assert.assertEquals(2, triangles.getSize());
        Triangle t1 = (Triangle) triangles.get(0);
        Triangle t2 = (Triangle) triangles.get(1);
        Assert.assertEquals(new DefaultTriangle(v1,v2,v3), t1);
        Assert.assertEquals(new DefaultTriangle(v1,v3,v4), t2);

        //test reverse rotation : counter-clockwise
        geometry = new Polygon(new Polyline(Geometries.toTupleBuffer(new Tuple[]{v1,v4,v3,v2,v1})), null);
        ec = new EarClipping();
        triangles = ec.triangulate(geometry);
        Assert.assertEquals(2, triangles.getSize());
        t1 = (Triangle) triangles.get(0);
        t2 = (Triangle) triangles.get(1);
        Assert.assertEquals(new DefaultTriangle(v1,v2,v3), t1);
        Assert.assertEquals(new DefaultTriangle(v1,v3,v4), t2);

    }

}
