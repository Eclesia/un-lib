
package science.unlicense.geometry.impl.operation;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.operation.IntersectsOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.Operations;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class IntersectsTest {

    @Test
    public void testPoint_point() throws OperationException{
        Point geom1 = new DefaultPoint(10, 20);
        Point geom2 = new DefaultPoint(10, 20);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new DefaultPoint(11, 20);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testPoint_line() throws OperationException{
        Point geom1 = new DefaultPoint(10, 20);
        Line geom2 = new DefaultLine(10, 20, 15, 20);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new DefaultLine(11, 20, 12, 20);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new DefaultLine(5, 20, 15, 20);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new DefaultLine(5, 22, 15, 22);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testPoint_rectangle() throws OperationException{
        Point geom1 = new DefaultPoint(new Vector2f64(10, 20));
        Rectangle geom2 = new Rectangle(8, 18, 4, 4);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));

        geom2 = new Rectangle(16, 18, 4, 4);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testPoint_circle() throws OperationException{
        Point geom1 = new DefaultPoint(10, 20);
        Circle geom2 = new Circle(10, 2, 19);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new Circle(3, -1, 5);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testPoint_plane() throws OperationException{
        Point geom1 = new DefaultPoint(0, 10, 0);
        Plane geom2 = new Plane(new Vector3f64(1,0,0), new Vector3f64(0,0,0), new Vector3f64(0,0,1));
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom1 = new DefaultPoint(35, 8, -12);
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom1 = new DefaultPoint(35, -6, -12);
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom1 = new DefaultPoint(35, 0, -12);
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testLine_line() throws OperationException{
        Line geom1 = new DefaultLine(10, 20, 15, 20);
        Line geom2 = new DefaultLine(10, 20, 15, 20);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new DefaultLine(-5, -2, 15, 20);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new DefaultLine(10, 21, 15, 21);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new DefaultLine(16, 20, 17, 20);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new DefaultLine(19, 5, 19, 50);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testRectangle_rectangle() throws OperationException{
        Rectangle geom1 = new Rectangle(8, 18, 4, 4);
        Rectangle geom2 = new Rectangle(8, 18, 4, 4);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new Rectangle(2, 20, 7, 1);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));

        geom2 = new Rectangle(1, 4, 1, 3);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testBbox_bbox() throws OperationException{
        BBox geom1 = new BBox(new double[]{8, 18}, new double[]{12, 22});
        BBox geom2 = new BBox(new double[]{8, 18}, new double[]{12, 22});
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new BBox(new double[]{2, 20}, new double[]{9, 21});
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));

        geom2 = new BBox(new double[]{1, 4}, new double[]{2, 7});
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testPlane_plane() throws OperationException{

        Plane geom1 = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(5, 6, 7));
        Plane geom2 = new Plane(new Vector3f64(0, 0, 1), new Vector3f64(5, 6, 7));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));

        //plane overlaps
        geom2 = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(5, 6, 7));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));

        //plane parallel
        geom2 = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(5, 9, 7));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
    }

    @Test
    public void testSphere_sphere() throws OperationException{
        Sphere geom1 = new Sphere(1); geom1.getTransform().getTranslation().setXYZ(2, -3, 5);
        Sphere geom2 = new Sphere(2.2); geom2.getTransform().getTranslation().setXYZ(2, -1, 5);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new Sphere(1.5); geom2.getTransform().getTranslation().setXYZ(5, -3, 5);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testSphere_capsule() throws OperationException{
        final Capsule geom1 = new Capsule(new Vector3f64(1, 1, 5), new Vector3f64(1, 1, 10), 1);

        Sphere geom2 = new Sphere(new Vector3f64(1, 1, 5), 2);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));

        geom2 = new Sphere(new Vector3f64(1, 2, 8), 1.5);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));

        geom2 = new Sphere(new Vector3f64(5, -3, 5), 2);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));

        geom2 = new Sphere(new Vector3f64(1, 1, 12.9), 2);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));

        geom2 = new Sphere(new Vector3f64(1, 1, 13.1), 2);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testSphere_plane() throws OperationException{
        Sphere geom1 = new Sphere(new Vector3f64(0, 2, 0),1);
        Plane geom2 = new Plane(new Vector3f64(1,0,0), new Vector3f64(0,0,0), new Vector3f64(0,0,1));
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom1 = new Sphere(new Vector3f64(35, 8, -12),1);
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom1 = new Sphere(new Vector3f64(35, -6, -12),1);
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom1 = new Sphere(new Vector3f64(35, 0.7, -12),1);
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom1 = new Sphere(new Vector3f64(35, -0.3, -12),1);
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom1 = new Sphere(new Vector3f64(35, -5.0000001, -12),5);
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom1 = new Sphere(new Vector3f64(35, -4.999999, -12),5);
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testSphere_bbox() throws OperationException{
        BBox geom2 = new BBox(new Vector3f64(6,6,6), new Vector3f64(10,10,10));

        //inside bbox
        Sphere geom1 = new Sphere(new Vector3f64(8, 8, 8),1);
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom2, geom1)));
        //englobe bbox
        geom1 = new Sphere(new Vector3f64(8, 8, 8),20);
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom2, geom1)));
        //above no intersect
        geom1 = new Sphere(new Vector3f64(8, 12.5, 8),2);
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new IntersectsOp(geom2, geom1)));
        //above intersect
        geom1 = new Sphere(new Vector3f64(8, 11.5, 8),2);
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testCircle_circle() throws OperationException{
        Circle geom1 = new Circle(10, 5, 2);
        Circle geom2 = new Circle(10, 2, 2);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new Circle(10, 2, 0.99);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
        geom2 = new Circle(3, -1, 2);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    @Test
    public void testCapsule_capsule() throws OperationException{
        Capsule geom1 = new Capsule(new Vector3f64(10, 20, 5), new Vector3f64(15, 20, 5), 1);

        Capsule geom2 = new Capsule(new Vector3f64(10, 20, 5), new Vector3f64(15, 20, 5), 1);
        Assert.assertEquals(Boolean.TRUE,  Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));

        geom2 = new Capsule(new Vector3f64(-5, -2, 5), new Vector3f64(15, 20, 5), 1);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new IntersectsOp(geom1, geom2)));

        geom1 = new Capsule(new Vector3f64(10, 20, 5), new Vector3f64(15, 20, 5), 0.2);

        geom2 = new Capsule(new Vector3f64(10, 21, 5), new Vector3f64(15, 21, 5), 0.3);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));

        geom2 = new Capsule(new Vector3f64(16, 20, 5), new Vector3f64(17, 20, 5), 0.3);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));

        geom2 = new Capsule(new Vector3f64(19, 5, 5), new Vector3f64(19, 50, 5), 0.3);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new IntersectsOp(geom2, geom1)));
    }

    /**
     * Test againt a [0,0,0,1,1,1] bbox.
     *
     * @throws OperationException
     */
    @Test
    public void testTriangle_bbox() throws OperationException{

        final BBox bbox = new BBox(3);
        bbox.setRange(0, 0, 1);
        bbox.setRange(1, 0, 1);
        bbox.setRange(2, 0, 1);

        final VectorRW v0 = new Vector3f64();
        final VectorRW v1 = new Vector3f64();
        final VectorRW v2 = new Vector3f64();
        final Triangle triangle = new DefaultTriangle(v0, v1, v2);

        final IntersectsOp intersects = new IntersectsOp(triangle, bbox);

        //triangle cross bbox on x plan
        v0.setXYZ(0.5, +10, -10);
        v1.setXYZ(0.5, -10, -10);
        v2.setXYZ(0.5, -10, +10);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));

        //triangle cross bbox on y plan
        v0.setXYZ(+10, 0.5, -10);
        v1.setXYZ(-10, 0.5, -10);
        v2.setXYZ(-10, 0.5, +10);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));

        //triangle cross bbox on z plan
        v0.setXYZ(+10, -10, 0.5);
        v1.setXYZ(-10, -10, 0.5);
        v2.setXYZ(-10, +10, 0.5);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));

        //triangle within bbox
        v0.setXYZ(0.2,0.2,0.2);
        v1.setXYZ(0.2,0.4,0.2);
        v2.setXYZ(0.2,0.2,0.4);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));

        //triangle one corner in bbox
        v0.setXYZ(0.5,0.5,0.5);
        v1.setXYZ(+10,+10,+10);
        v2.setXYZ(-10,-10,-10);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));

    }

    /**
     * Test against a [10,20,30,11,22,33] bbox.
     *
     * @throws OperationException
     */
    @Test
    public void testTriangle_bbox2() throws OperationException{

        final BBox bbox = new BBox(3);
        bbox.setRange(0, 10, 11);
        bbox.setRange(1, 20, 22);
        bbox.setRange(2, 30, 33);

        final VectorRW v0 = new Vector3f64();
        final VectorRW v1 = new Vector3f64();
        final VectorRW v2 = new Vector3f64();
        final Triangle triangle = new DefaultTriangle(v0, v1, v2);

        final IntersectsOp intersects = new IntersectsOp(triangle, bbox);

        //triangle cross bbox on x plan
        v0.setXYZ(10.5, +100, 0);
        v1.setXYZ(10.5, 0, 0);
        v2.setXYZ(10.5, 0, +100);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));

        //triangle cross bbox on y plan
        v0.setXYZ(+100, 21, 0);
        v1.setXYZ(0, 21, 0);
        v2.setXYZ(0, 21, +100);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));

        //triangle cross bbox on z plan
        v0.setXYZ(+100, 0, 31.5);
        v1.setXYZ(0, 0, 31.5);
        v2.setXYZ(0, +100, 31.5);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));

        //triangle within bbox
        v0.setXYZ(10.2,21.2,32.2);
        v1.setXYZ(10.2,21.4,32.2);
        v2.setXYZ(10.2,21.2,32.4);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));

        //triangle one corner in bbox 1
        v0.setXYZ(10.5,21.5,32.5);
        v1.setXYZ(10.5,+100,+100);
        v2.setXYZ(10.5,-100,-100);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));

        //triangle one corner in bbox 2
        v0.setXYZ(10.5,21.5,32.5);
        v1.setXYZ(+100,+100,+100);
        v2.setXYZ(-100,-100,-100);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));

        //from a real case ----------
        //one corner outside bbox
        bbox.setRange(0, 25, 26);
        bbox.setRange(1, 14, 15);
        bbox.setRange(2, 48, 49);
        v0.setXYZ(24.793482276873835,14.662215379777855,48.69177254509293);
        v1.setXYZ(25.419224087367553,14.907598565732481,48.710409708649166);
        v2.setXYZ(25.404734553018685,14.97108488632958,48.052672859715464);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
    }

}
