
package science.unlicense.geometry.impl.operation;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.NearestOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.Operations;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.impl.OBBox;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class NearestTest {

    private static final double DELTA = 0.00000001;

    @Test
    public void testPoint_point() throws OperationException{
        Point geom1 = new DefaultPoint(10, 20);
        Point geom2 = new DefaultPoint(10, 25);
        Geometry[] result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(geom2,result[1]);
    }

    @Test
    public void testPoint_plane() throws OperationException{
        Point geom1 = new DefaultPoint(0, 10, 0);
        Plane geom2 = new Plane(new Vector3f64(1,0,0), new Vector3f64(0,0,0), new Vector3f64(0,0,1));
        Geometry[] result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new DefaultPoint(0, 0, 0),result[1]);

        geom1 = new DefaultPoint(35, -8, -12);
        result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new DefaultPoint(35, 0, -12),result[1]);
    }

    @Test
    public void testPoint_triangle() throws OperationException{
        Point geom1 = new DefaultPoint(0, 10, 0);
        Triangle geom2 = new DefaultTriangle(new Vector3f64(1,0,0), new Vector3f64(0,0,0), new Vector3f64(0,0,1));
        Geometry[] result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new DefaultPoint(0, 0, 0),result[1]);

        geom1 = new DefaultPoint(20, -3, 0);
        result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new DefaultPoint(1, 0, 0),result[1]);
    }

    @Test
    public void testPoint_rectangle() throws OperationException{
        Point geom1 = new DefaultPoint(0, 0);
        Rectangle geom2 = new Rectangle(5,10,20,5);
        Geometry[] result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new DefaultPoint(5, 10),result[1]);

        //point inside
        geom1 = new DefaultPoint(10,12);
        result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new DefaultPoint(10, 12),result[1]);
    }

    @Test
    public void testPoint_bbox() throws OperationException{
        Point geom1 = new DefaultPoint(0, 0);
        BBox geom2 = new BBox(new double[]{5,10},new double[]{25,15});
        Geometry[] result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new DefaultPoint(5, 10),result[1]);

        //point inside
        geom1 = new DefaultPoint(10,12);
        result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new DefaultPoint(10, 12),result[1]);
    }

    @Test
    public void testPoint_obbox() throws OperationException{
        Point geom1 = new DefaultPoint(0, 0);
        OBBox geom2 = new OBBox(new double[]{0,0},new double[]{20,5});
        geom2.getTransform().getTranslation().setXY(5, 10);
        geom2.getTransform().notifyChanged();
        Geometry[] result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new DefaultPoint(5, 10),result[1]);

        //point inside
        geom1 = new DefaultPoint(10,12);
        result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new DefaultPoint(10, 12),result[1]);
    }

    @Test
    public void testSegment_triangle() throws OperationException{
        //segment straight over the triangle, both point project on it
        Line geom1 = new DefaultLine(new Vector3f64(0.1, 12, 0.1), new Vector3f64(0.3, 10, 0.3));
        Triangle geom2 = new DefaultTriangle(new Vector3f64(1,0,0), new Vector3f64(0,0,0), new Vector3f64(0,0,1));
        Geometry[] result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(new DefaultPoint(0.3, 10, 0.3),result[0]);
        Assert.assertEquals(new DefaultPoint(0.3,  0, 0.3),result[1]);

        //segment do not project on triangle
        geom1 = new DefaultLine(new Vector3f64(14, -5, 3), new Vector3f64(17, -8, 6));
        result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(new DefaultPoint(14, -5, 3),result[0]);
        Assert.assertEquals(new DefaultPoint(1, 0, 0),result[1]);
    }

    @Test
    public void testTriangle_triangle() throws OperationException{
        //parallal triangles
        Triangle geom1 = new DefaultTriangle(new Vector3f64(1,1,0), new Vector3f64(1,0,0), new Vector3f64(1,0,1));
        Triangle geom2 = new DefaultTriangle(new Vector3f64(2,1,0), new Vector3f64(2,0,0), new Vector3f64(2,0,1));
        Geometry[] result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(new DefaultPoint(1, 1, 0),result[0]);
        Assert.assertEquals(new DefaultPoint(2, 1, 0),result[1]);

        //perpendicular triangles
        geom1 = new DefaultTriangle(new Vector3f64(10,3,0), new Vector3f64(0,3,0), new Vector3f64(0,3,10));
        result = (Geometry[]) Operations.execute(new NearestOp(geom1, geom2));
        Assert.assertEquals(new DefaultPoint(2, 3, 0),result[0]);
        Assert.assertEquals(new DefaultPoint(2, 1, 0),result[1]);
    }

}
