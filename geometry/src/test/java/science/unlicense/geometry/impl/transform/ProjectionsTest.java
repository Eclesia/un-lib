
package science.unlicense.geometry.impl.transform;

import science.unlicense.geometry.impl.transform.Projections;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.Affine;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class ProjectionsTest {

    @Test
    public void testCenteredMatrix(){
        final BBox source = new BBox(new Vector2f64(10, 10), new Vector2f64(30, 50));
        final BBox target = new BBox(new Vector2f64(100, 200), new Vector2f64(200, 400));

        final Affine trs = Projections.centered(source, target);
        final BBox result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector2f64(140, 280), new Vector2f64(160, 320)), result);
    }

    @Test
    public void testScaledMatrix(){
        final BBox source = new BBox(new Vector2f64(10, 10), new Vector2f64(30, 50));
        final BBox target = new BBox(new Vector2f64(60, 20), new Vector2f64(110, 40));

        final Affine trs = Projections.scaled(source, target);
        final BBox result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector2f64(80, 20), new Vector2f64(90, 40)), result);
    }

    @Test
    public void testStretchedMatrix(){
        final BBox source = new BBox(new Vector2f64(10, 10), new Vector2f64(30, 50));
        final BBox target = new BBox(new Vector2f64(100, 200), new Vector2f64(200, 400));

        final Affine trs = Projections.stretched(source, target);
        final BBox result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(target, result);
    }

    @Test
    public void testZoomedMatrix(){
        final BBox source = new BBox(new Vector2f64(10, 10), new Vector2f64(30, 50));
        final BBox target = new BBox(new Vector2f64(60, 20), new Vector2f64(110, 40));

        final Affine trs = Projections.zoomed(source, target);
        final BBox result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector2f64(60, -20), new Vector2f64(110, 80)), result);
    }

    @Test
    public void testFitMatrix(){
        final BBox source = new BBox(new Vector2f64(10, 10), new Vector2f64(30, 50));
        final BBox target = new BBox(new Vector2f64(60, 20), new Vector2f64(110, 40));

        Affine trs = Projections.fit(source, target, 0);
        BBox result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector2f64(60, -20), new Vector2f64(110, 80)), result);

        trs = Projections.fit(source, target, 1);
        result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector2f64(80, 20), new Vector2f64(90, 40)), result);
    }

    @Test
    public void testAlignMatrix(){
        final BBox source = new BBox(new Vector2f64(10, 10), new Vector2f64(20, 20));
        final BBox target = new BBox(new Vector2f64(5, 5), new Vector2f64(50, 50));

        Affine trs = Projections.align(source, target, new double[]{0,0});
        Assert.assertEquals(new Affine2(1, 0, -5, 0, 1, -5), trs);
        BBox result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector2f64(5, 5), new Vector2f64(15, 15)), result);

        trs = Projections.align(source, target, new double[]{1,1});
        Assert.assertEquals(new Affine2(1, 0, 30, 0, 1, 30), trs);
        result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector2f64(40, 40), new Vector2f64(50, 50)), result);

        trs = Projections.align(source, target, new double[]{0.5,1});
        Assert.assertEquals(new Affine2(1, 0, 12.5, 0, 1, 30), trs);
        result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector2f64(22.5, 40), new Vector2f64(32.5, 50)), result);
    }

}
