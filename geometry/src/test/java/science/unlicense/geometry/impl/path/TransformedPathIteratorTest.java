
package science.unlicense.geometry.impl.path;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.impl.Matrix4x4;

/**
 *
 * @author Johann Sorel
 */
public class TransformedPathIteratorTest {

    @Test
    public void testMismatchDimension() {

        final Rectangle rect = new Rectangle();
        final Matrix4x4 m = new Matrix4x4();

        try {
            new TransformedPathIterator(rect.createPathIterator(), m);
            Assert.fail("Creating a transformed geometry with a different size transform should have failed");
        } catch(InvalidArgumentException ex) {
            //ok
        }
    }

}
