package science.unlicense.geometry.api.index.octtrees;

import java.util.SortedSet;
import java.util.TreeSet;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.api.index.Cuboid;
import science.unlicense.geometry.api.index.SDType;
import science.unlicense.geometry.api.index.utilities.TestUtilities;
import science.unlicense.math.impl.Vector3f64;

/**
 * @author Mark Raynsford
 */
public final class OctTreeSDLimitTest extends OctTreeCommonTests {

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct128() {
        try {
            return OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                    new Vector3f64(0, 0, 0),
                    new Vector3f64(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct128Offset64() {
        try {
            return OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                    new Vector3f64(64, 64, 64),
                    new Vector3f64(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct128OffsetM64() {
        try {
            return OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                    new Vector3f64(-64, -64, -64),
                    new Vector3f64(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct16() {
        try {
            return OctTreeSDLimit.newOctTree(new Vector3f64(16, 16, 16),
                    new Vector3f64(0, 0, 0),
                    new Vector3f64(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2() {
        try {
            return OctTreeSDLimit.newOctTree(new Vector3f64(2, 2, 2), new Vector3f64(
                    0,
                    0,
                    0), new Vector3f64(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_2_4() {
        try {
            return OctTreeSDLimit.newOctTree(new Vector3f64(2, 2, 4), new Vector3f64(
                    0,
                    0,
                    0), new Vector3f64(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_4_2() {
        try {
            return OctTreeSDLimit.newOctTree(new Vector3f64(2, 4, 2), new Vector3f64(
                    0,
                    0,
                    0), new Vector3f64(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_4_4() {
        try {
            return OctTreeSDLimit.newOctTree(new Vector3f64(2, 4, 4), new Vector3f64(
                    0,
                    0,
                    0), new Vector3f64(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_2_2() {
        try {
            return OctTreeSDLimit.newOctTree(new Vector3f64(4, 2, 2), new Vector3f64(
                    0,
                    0,
                    0), new Vector3f64(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_2_4() {
        try {
            return OctTreeSDLimit.newOctTree(new Vector3f64(4, 2, 4), new Vector3f64(
                    0,
                    0,
                    0), new Vector3f64(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_4_2() {
        try {
            return OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 2), new Vector3f64(
                    0,
                    0,
                    0), new Vector3f64(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Test
    public void testClearDynamic() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));
        final Cuboid[] dynamics = TestUtilities.makeCuboids(0, 128);
        final Cuboid[] statics = TestUtilities.makeCuboids(10, 128);

        for (final Cuboid c : dynamics) {
            {
                final boolean in = q.octTreeInsertSD(c, SDType.SD_DYNAMIC);
                Assert.assertTrue(in);
            }
            {
                final boolean in = q.octTreeInsertSD(c, SDType.SD_DYNAMIC);
                Assert.assertFalse(in);
            }
        }

        for (final Cuboid c : statics) {
            {
                final boolean in = q.octTreeInsertSD(c, SDType.SD_STATIC);
                Assert.assertTrue(in);
            }
            {
                final boolean in = q.octTreeInsertSD(c, SDType.SD_STATIC);
                Assert.assertFalse(in);
            }
        }

        {
            final IterationCounter counter = new IterationCounter();
            q.octTreeIterateObjects(counter);
            Assert.assertEquals(16, counter.count);
        }

        q.octTreeSDClearDynamic();

        {
            final IterationCounter counter = new IterationCounter();
            q.octTreeIterateObjects(counter);
            Assert.assertEquals(8, counter.count);
        }
    }

    @Test
    public void testCreateLimitOddX() {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(3, 4, 4));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitOddY() {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(4, 3, 4));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitOddZ() {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(4, 4, 3));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooLargeX() {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(6, 4, 4));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooLargeY() {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(4, 6, 4));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooLargeZ() {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(4, 4, 6));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooSmallX() {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(1, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooSmallY() {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 1, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooSmallZ() {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 1));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateOddX() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(3, 2, 2),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateOddY() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(2, 3, 2),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateOddZ() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(2, 2, 3),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateSDOddX() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(5, 4, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateSDOddY() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 5, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateSDOddZ() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 5),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateSDTooSmallX() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(0, 4, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateSDTooSmallY() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 0, 4),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateSDTooSmallZ() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(4, 4, 0),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateTooSmallX() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(1, 2, 2),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateTooSmallY() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(2, 1, 2),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateTooSmallZ() throws Exception {
        try {
            OctTreeSDLimit.newOctTree(new Vector3f64(2, 2, 1),
                new Vector3f64(0,0,0),
                new Vector3f64(2, 2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public final void testInsertLeafNoSplit() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(1, c.count);
    }

    @Test
    public final void testInsertSplit() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct128();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(49, c.count);
    }

    @Test
    public final void testInsertSplitNot() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(1, c.count);
    }

    @Test
    public final void testInsertSplitXNotYNotZ() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct4_2_2();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(1, c.count);
    }

    @Test
    public final void testInsertSplitXYNotZ() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct4_4_2();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(1, c.count);
    }

    @Test
    public final void testInsertSplitXZNotY() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct4_2_4();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(1, c.count);
    }

    @Test
    public final void testInsertSplitYNotZNotZ() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2_4_2();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(1, c.count);
    }

    @Test
    public final void testInsertSplitYZNotX() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2_4_4();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(1, c.count);
    }

    @Test
    public final void testInsertSplitZNotXNotY() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2_2_4();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(1, c.count);
    }

    @Test
    public void testIterateEarlyEndDynamic() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));
        final Cuboid cubes[] = TestUtilities.makeCuboids(0, 128);

        for (final Cuboid c : cubes) {
            q.octTreeInsertSD(c, SDType.SD_DYNAMIC);
        }

        final IterationChecker0 checker = new IterationChecker0() {
            Integer count = Integer.valueOf(0);

            @Override
            public Boolean evaluate(final Object candidate) {
                this.count = Integer.valueOf(this.count.intValue() + 1);
                this.got.add((Cuboid) candidate);

                if (this.count.intValue() == 4) {
                    return Boolean.FALSE;
                }

                return Boolean.TRUE;
            }
        };

        q.octTreeIterateObjects(checker);

        Assert.assertEquals(4, checker.got.size());

        int count = 0;
        for (final Cuboid c : cubes) {
            if (checker.got.contains(c)) {
                ++count;
            }
        }

        Assert.assertEquals(4, count);
    }

    @Test
    public void testIterateEarlyEndStatic() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));
        final Cuboid cubes[] = TestUtilities.makeCuboids(0, 128);

        for (final Cuboid c : cubes) {
            q.octTreeInsertSD(c, SDType.SD_STATIC);
        }

        final IterationChecker0 checker = new IterationChecker0() {
            Integer count = Integer.valueOf(0);

            @Override
            public Boolean evaluate(final Object candidate) {
                this.count = Integer.valueOf(this.count.intValue() + 1);
                this.got.add((Cuboid) candidate);

                if (this.count.intValue() == 4) {
                    return Boolean.FALSE;
                }

                return Boolean.TRUE;
            }
        };

        q.octTreeIterateObjects(checker);

        Assert.assertEquals(4, checker.got.size());

        int count = 0;
        for (final Cuboid c : cubes) {
            if (checker.got.contains(c)) {
                ++count;
            }
        }

        Assert.assertEquals(4, count);
    }

    @Test
    public void testQueryContainingDynamic() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));
        final Cuboid[] dynamics = TestUtilities.makeCuboids(0, 128);

        for (final Cuboid c : dynamics) {
            final boolean in = q.octTreeInsertSD(c, SDType.SD_DYNAMIC);
            Assert.assertTrue(in);
        }

        {
            final SortedSet<Cuboid> items = new TreeSet<Cuboid>();
            q.octTreeQueryVolumeContaining(new Cuboid(
                    0,
                    new Vector3f64(0, 0, 0),
                    new Vector3f64(127, 127, 127)), items);

            Assert.assertEquals(8, items.size());
        }
    }

    @Test
    public void testQueryContainingDynamicNot() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));

        final boolean in
                = q.octTreeInsertSD(new Cuboid(
                                0,
                                new Vector3f64(66, 66, 66),
                                new Vector3f64(127, 127, 127)), SDType.SD_DYNAMIC);
        Assert.assertTrue(in);

        {
            final SortedSet<Cuboid> items = new TreeSet<Cuboid>();
            q.octTreeQueryVolumeContaining(new Cuboid(
                    0,
                    new Vector3f64(0, 0, 0),
                    new Vector3f64(65, 65, 65)), items);

            Assert.assertEquals(0, items.size());
        }
    }

    @Test
    public void testQueryContainingDynamicOK() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));

        final boolean in
                = q.octTreeInsertSD(new Cuboid(0, new Vector3f64(2, 2, 2), new Vector3f64(
                                        60,
                                        60,
                                        60)), SDType.SD_DYNAMIC);
        Assert.assertTrue(in);

        {
            final SortedSet<Cuboid> items = new TreeSet<Cuboid>();
            q.octTreeQueryVolumeContaining(new Cuboid(
                    0,
                    new Vector3f64(1, 1, 1),
                    new Vector3f64(61, 61, 61)), items);

            Assert.assertEquals(1, items.size());
        }
    }

    @Test
    public void testQueryContainingStatic() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));
        final Cuboid[] statics = TestUtilities.makeCuboids(0, 128);

        for (final Cuboid c : statics) {
            final boolean in = q.octTreeInsertSD(c, SDType.SD_STATIC);
            Assert.assertTrue(in);
        }

        {
            final SortedSet<Cuboid> items = new TreeSet<Cuboid>();
            q.octTreeQueryVolumeContaining(new Cuboid(
                    0,
                    new Vector3f64(0, 0, 0),
                    new Vector3f64(127, 127, 127)), items);

            Assert.assertEquals(8, items.size());
        }
    }

    @Test
    public void testQueryContainingStaticNot() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));

        final boolean in
                = q.octTreeInsertSD(new Cuboid(
                                0,
                                new Vector3f64(66, 66, 66),
                                new Vector3f64(127, 127, 127)), SDType.SD_STATIC);
        Assert.assertTrue(in);

        {
            final SortedSet<Cuboid> items = new TreeSet<Cuboid>();
            q.octTreeQueryVolumeContaining(new Cuboid(
                    0,
                    new Vector3f64(0, 0, 0),
                    new Vector3f64(65, 65, 65)), items);

            Assert.assertEquals(0, items.size());
        }
    }

    @Test
    public void testQueryContainingStaticOK() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));

        final boolean in
                = q.octTreeInsertSD(new Cuboid(0, new Vector3f64(2, 2, 2), new Vector3f64(
                                        60,
                                        60,
                                        60)), SDType.SD_STATIC);
        Assert.assertTrue(in);

        {
            final SortedSet<Cuboid> items = new TreeSet<Cuboid>();
            q.octTreeQueryVolumeContaining(new Cuboid(
                    0,
                    new Vector3f64(1, 1, 1),
                    new Vector3f64(61, 61, 61)), items);

            Assert.assertEquals(1, items.size());
        }
    }

    @Test
    public void testQueryOverlappingDynamic() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));
        final Cuboid[] dynamics = TestUtilities.makeCuboids(0, 128);

        for (final Cuboid c : dynamics) {
            final boolean in = q.octTreeInsertSD(c, SDType.SD_DYNAMIC);
            Assert.assertTrue(in);
        }

        {
            final SortedSet<Cuboid> items = new TreeSet<Cuboid>();
            q.octTreeQueryVolumeOverlapping(new Cuboid(
                    0,
                    new Vector3f64(16, 16, 16),
                    new Vector3f64(80, 80, 80)), items);

            Assert.assertEquals(8, items.size());
        }
    }

    @Test
    public void testQueryOverlappingDynamicNot() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));
        final Cuboid[] dynamics = TestUtilities.makeCuboids(0, 128);

        for (final Cuboid c : dynamics) {
            final boolean in = q.octTreeInsertSD(c, SDType.SD_DYNAMIC);
            Assert.assertTrue(in);
        }

        {
            final SortedSet<Cuboid> items = new TreeSet<Cuboid>();
            q.octTreeQueryVolumeOverlapping(new Cuboid(
                    0,
                    new Vector3f64(0, 0, 0),
                    new Vector3f64(1, 1, 1)), items);

            Assert.assertEquals(0, items.size());
        }
    }

    @Test
    public void testQueryOverlappingStatic() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));
        final Cuboid statics[] = TestUtilities.makeCuboids(0, 128);

        for (final Cuboid c : statics) {
            final boolean in = q.octTreeInsertSD(c, SDType.SD_STATIC);
            Assert.assertTrue(in);
        }

        {
            final SortedSet<Cuboid> items = new TreeSet<Cuboid>();
            q.octTreeQueryVolumeOverlapping(new Cuboid(
                    0,
                    new Vector3f64(16, 16, 16),
                    new Vector3f64(80, 80, 80)), items);

            Assert.assertEquals(8, items.size());
        }
    }

    @Test
    public void testQueryOverlappingStaticNot() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));
        final Cuboid statics[] = TestUtilities.makeCuboids(0, 128);

        for (final Cuboid c : statics) {
            final boolean in = q.octTreeInsertSD(c, SDType.SD_STATIC);
            Assert.assertTrue(in);
        }

        {
            final SortedSet<Cuboid> items = new TreeSet<Cuboid>();
            q.octTreeQueryVolumeOverlapping(new Cuboid(
                    0,
                    new Vector3f64(0, 0, 0),
                    new Vector3f64(1, 1, 1)), items);

            Assert.assertEquals(0, items.size());
        }
    }

    @Test
    public void testRemoveSubDynamic() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));
        final Cuboid[] dynamics = TestUtilities.makeCuboids(0, 128);

        for (final Cuboid c : dynamics) {
            {
                final boolean in = q.octTreeInsertSD(c, SDType.SD_DYNAMIC);
                Assert.assertTrue(in);
            }
            {
                final boolean in = q.octTreeInsertSD(c, SDType.SD_DYNAMIC);
                Assert.assertFalse(in);
            }
        }

        for (final Cuboid c : dynamics) {
            {
                final boolean removed = q.octTreeRemove(c);
                Assert.assertTrue(removed);
            }
            {
                final boolean removed = q.octTreeRemove(c);
                Assert.assertFalse(removed);
            }
        }
    }

    @Test
    public void testRemoveSubStatic() throws Exception {
        final OctTreeSDType<Cuboid> q
                = OctTreeSDLimit.newOctTree(new Vector3f64(128, 128, 128),
                        new Vector3f64(0,0,0),
                        new Vector3f64(2, 2, 2));
        final Cuboid[] dynamics = TestUtilities.makeCuboids(0, 128);

        for (final Cuboid c : dynamics) {
            {
                final boolean in = q.octTreeInsertSD(c, SDType.SD_STATIC);
                Assert.assertTrue(in);
            }
            {
                final boolean in = q.octTreeInsertSD(c, SDType.SD_STATIC);
                Assert.assertFalse(in);
            }
        }

        for (final Cuboid c : dynamics) {
            {
                final boolean removed = q.octTreeRemove(c);
                Assert.assertTrue(removed);
            }
            {
                final boolean removed = q.octTreeRemove(c);
                Assert.assertFalse(removed);
            }
        }
    }
}
