package science.unlicense.geometry.api.index.examples;

import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.index.octtrees.OctTreeBasic;
import science.unlicense.geometry.api.index.octtrees.OctTreeMemberType;
import science.unlicense.geometry.api.index.octtrees.OctTreeRaycastResult;
import science.unlicense.geometry.api.index.octtrees.OctTreeType;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;

/**
 * @author Mark Raynsford
 */
public final class TrivialOct {

    /**
     * An extremely simple class that implements {@link OctTreeMemberType}.
     */
    static class Something extends OctTreeMemberType<Something> {

        /**
         * A "pool" of unique identifiers, shared between all objects of type <code>Something</code>.
         */
        private static final AtomicLong pool = new AtomicLong(0);
        /**
         * The unique identifier of this object.
         */
        private final long id;

        Something(final VectorRW in_lower,final VectorRW in_upper) {
            this.id = Something.pool.incrementAndGet();
            this.lower.set(in_lower);
            this.upper.set(in_upper);
        }

        @Override
        public int compareTo(final Something other) {
            if (this.id > other.id) {
                return 1;
            }
            if (this.id < other.id) {
                return -1;
            }
            return 0;
        }

    }

    @Test
    public void testExample() {
        /**
         * Create a octtree of width, height, and depth 128, using the simplest implementation the package provides.
         */
        final OctTreeType<Something> tree = OctTreeBasic.newOctTree(new Vector3f64(128, 128, 128), new Vector3f64(0, 0, 0));

        /**
         * Insert eight objects into the tree. The sizes and positions of the object will place one in each corner of the volume described by the tree.
         */
        Something s0;
        Something s1;
        Something s2;
        Something s3;
        Something s4;
        Something s5;
        Something s6;
        Something s7;

        {
            final VectorRW lower = new Vector3f64(0, 0, 0);
            final VectorRW upper = new Vector3f64(31, 31, 31);
            s0 = new Something(lower, upper);
        }

        {
            final VectorRW lower = new Vector3f64(64, 0, 0);
            final VectorRW upper = new Vector3f64(64 + 31, 31, 31);
            s1 = new Something(lower, upper);
        }

        {
            final VectorRW lower = new Vector3f64(0, 64, 0);
            final VectorRW upper = new Vector3f64(31, 64 + 31, 31);
            s2 = new Something(lower, upper);
        }

        {
            final VectorRW lower = new Vector3f64(64, 64, 0);
            final VectorRW upper = new Vector3f64(64 + 31, 64 + 31, 31);
            s3 = new Something(lower, upper);
        }

        /**
         * Upper Z...
         */
        {
            final VectorRW lower = new Vector3f64(0, 0, 64);
            final VectorRW upper = new Vector3f64(31, 31, 64 + 31);
            s4 = new Something(lower, upper);
        }

        {
            final VectorRW lower = new Vector3f64(64, 0, 64);
            final VectorRW upper = new Vector3f64(64 + 31, 31, 64 + 31);
            s5 = new Something(lower, upper);
        }

        {
            final VectorRW lower = new Vector3f64(0, 64, 64);
            final VectorRW upper = new Vector3f64(31, 64 + 31, 64 + 31);
            s6 = new Something(lower, upper);
        }

        {
            final VectorRW lower = new Vector3f64(64, 64, 64);
            final VectorRW upper = new Vector3f64(64 + 31, 64 + 31, 64 + 31);
            s7 = new Something(lower, upper);
        }

        boolean inserted = true;
        inserted &= tree.octTreeInsert(s0);
        inserted &= tree.octTreeInsert(s1);
        inserted &= tree.octTreeInsert(s2);
        inserted &= tree.octTreeInsert(s3);
        inserted &= tree.octTreeInsert(s4);
        inserted &= tree.octTreeInsert(s5);
        inserted &= tree.octTreeInsert(s6);
        inserted &= tree.octTreeInsert(s7);

        Assert.assertTrue(inserted);

        /**
         * Now, select a volume of the tree and check that the expected objects were contained within the volume.
         */
        {
            final BBox volume = new BBox(
                    new Vector3f64(0,0,0),
                    new Vector3f64(40, 128, 40)
            );

            final TreeSet<Something> objects = new TreeSet<Something>();
            tree.octTreeQueryVolumeContaining(volume, objects);

            Assert.assertEquals(2, objects.size());
            Assert.assertTrue(objects.contains(s0));
            Assert.assertFalse(objects.contains(s1));
            Assert.assertTrue(objects.contains(s2));
            Assert.assertFalse(objects.contains(s3));
        }

        /**
         * Now, select another volume of the tree and check that the expected objects were overlapped by the volume.
         */
        {
            final BBox volume = new BBox(
                    new Vector3f64(0,0,0),
                    new Vector3f64(80, 80, 128)
            );

            final TreeSet<Something> objects = new TreeSet<Something>();
            tree.octTreeQueryVolumeOverlapping(volume, objects);

            Assert.assertEquals(4, objects.size());
            Assert.assertTrue(objects.contains(s4));
            Assert.assertTrue(objects.contains(s5));
            Assert.assertTrue(objects.contains(s6));
            Assert.assertTrue(objects.contains(s7));
        }

        /**
         * Now, cast a ray from (16,16,16) towards (128,128,16), and check that the expected objects were intersected by the ray.
         *
         * Note that objects are returned in order of increasing distance: The nearest object intersected by the ray will be the first in the returned set.
         */
        {

            final VectorRW origin = new Vector3f64(16, 16, 16);
            final VectorRW direction = new Vector3f64(128, 128, 16).normalize();
            final Ray ray = new Ray(origin, direction);

            final TreeSet<OctTreeRaycastResult<Something>> objects = new TreeSet<OctTreeRaycastResult<Something>>();
            tree.octTreeQueryRaycast(ray, objects);

            Assert.assertEquals(2, objects.size());
            Assert.assertIdentity(objects.first().getObject(), s0);
            Assert.assertIdentity(objects.last().getObject(), s3);
        }
    }

}
