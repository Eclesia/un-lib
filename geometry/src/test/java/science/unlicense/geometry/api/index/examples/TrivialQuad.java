package science.unlicense.geometry.api.index.examples;

import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.index.quadtrees.QuadTreeBasic;
import science.unlicense.geometry.api.index.quadtrees.QuadTreeMemberType;
import science.unlicense.geometry.api.index.quadtrees.QuadTreeRaycastResult;
import science.unlicense.geometry.api.index.quadtrees.QuadTreeType;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;

/**
 * @author Mark Raynsford
 */
public final class TrivialQuad {

    /**
     * An extremely simple class that implements {@link QuadTreeMemberType}.
     */
    static class Something extends QuadTreeMemberType<Something> {

        /**
         * A "pool" of unique identifiers, shared between all objects of type <code>Something</code>.
         */

        private static final AtomicLong pool = new AtomicLong(0);
        /**
         * The unique identifier of this object.
         */

        private final long id;

        Something(final VectorRW in_lower,final VectorRW in_upper) {
            this.id = Something.pool.incrementAndGet();
            this.lower.set(in_lower);
            this.upper.set(in_upper);
        }

        @Override
        public int compareTo(
                final Something other) {
            if (this.id > other.id) {
                return 1;
            }
            if (this.id < other.id) {
                return -1;
            }
            return 0;
        }

    }

    @Test
    public void testExample() {
        /**
         * Create a quadtree of width and height 128, using the simplest implementation the package provides.
         */
        final QuadTreeType<Something> tree
                = QuadTreeBasic.newQuadTree(new Vector2f64(128, 128), new Vector2f64(0, 0));

        /**
         * Insert four objects into the tree. The sizes and positions of the object will place one in each corner of the area described by the tree.
         */
        Something s0;
        Something s1;
        Something s2;
        Something s3;

        {
            final VectorRW lower = new Vector2f64(0, 0);
            final VectorRW upper = new Vector2f64(31, 31);
            s0 = new Something(lower, upper);
        }

        {
            final VectorRW lower = new Vector2f64(64, 0);
            final VectorRW upper = new Vector2f64(64 + 31, 31);
            s1 = new Something(lower, upper);
        }

        {
            final VectorRW lower = new Vector2f64(0, 64);
            final VectorRW upper = new Vector2f64(0 + 31, 64 + 31);
            s2 = new Something(lower, upper);
        }

        {
            final VectorRW lower = new Vector2f64(64, 64);
            final VectorRW upper = new Vector2f64(64 + 31, 64 + 31);
            s3 = new Something(lower, upper);
        }

        boolean inserted = true;
        inserted &= tree.quadTreeInsert(s0);
        inserted &= tree.quadTreeInsert(s1);
        inserted &= tree.quadTreeInsert(s2);
        inserted &= tree.quadTreeInsert(s3);

        Assert.assertTrue(inserted);

        /**
         * Now, select an area of the tree and check that the expected objects were contained within the area.
         */
        {
            final BBox area = new BBox(
                    new Vector2f64(0,0),
                    new Vector2f64(40, 128)
            );

            final TreeSet<Something> objects = new TreeSet<Something>();
            tree.quadTreeQueryAreaContaining(area, objects);

            Assert.assertEquals(2, objects.size());
            Assert.assertTrue(objects.contains(s0));
            Assert.assertFalse(objects.contains(s1));
            Assert.assertTrue(objects.contains(s2));
            Assert.assertFalse(objects.contains(s3));
        }

        /**
         * Now, select another area of the tree and check that the expected objects were overlapped by the area.
         */
        {
            final BBox area = new BBox(
                    new Vector2f64(0,0),
                    new Vector2f64(80, 80)
            );

            final TreeSet<Something> objects = new TreeSet<Something>();
            tree.quadTreeQueryAreaOverlapping(area, objects);

            Assert.assertEquals(4, objects.size());
            Assert.assertTrue(objects.contains(s0));
            Assert.assertTrue(objects.contains(s1));
            Assert.assertTrue(objects.contains(s2));
            Assert.assertTrue(objects.contains(s3));
        }

        /**
         * Now, cast a ray from (16,16) towards (128,128), and check that the expected objects were intersected by the ray.
         *
         * Note that objects are returned in order of increasing distance: The nearest object intersected by the ray will be the first in the returned set.
         */
        {
            final VectorRW origin = new Vector2f64(16, 16);
            final VectorRW direction = new Vector2f64(128, 128).normalize();
            final Ray ray = new Ray(origin, direction);

            final TreeSet<QuadTreeRaycastResult<Something>> objects
                    = new TreeSet<QuadTreeRaycastResult<Something>>();
            tree.quadTreeQueryRaycast(ray, objects);

            Assert.assertEquals(2, objects.size());
            Assert.assertIdentity(objects.first().getObject(), s0);
            Assert.assertIdentity(objects.last().getObject(), s3);
        }
    }
}
