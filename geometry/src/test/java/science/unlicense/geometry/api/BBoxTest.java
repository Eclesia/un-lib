
package science.unlicense.geometry.api;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class BBoxTest {

    @Test
    public void testIntersect() {

        //test intersection bounding box

        testIntersect(new BBox(new Vector2f64( 0,  0), new Vector2f64(10, 10)),
                new BBox(new Vector2f64( 4,  5), new Vector2f64( 7,  8)),
                new BBox(new Vector2f64( 4,  5), new Vector2f64( 7,  8)));
        testIntersect(new BBox(new Vector2f64( 0,  0), new Vector2f64(10, 10)),
                new BBox(new Vector2f64(-2,  5), new Vector2f64(15,  8)),
                new BBox(new Vector2f64( 0,  5), new Vector2f64(10,  8)));
        testIntersect(new BBox(new Vector2f64( 0,  0), new Vector2f64(10, 10)),
                new BBox(new Vector2f64(10, 10), new Vector2f64(15, 16)),
                new BBox(new Vector2f64(10, 10), new Vector2f64(10, 10)));
        testIntersect(new BBox(new Vector2f64( 0,  0), new Vector2f64(10, 10)),
                new BBox(new Vector2f64(12, 13), new Vector2f64(15, 16)),
                new BBox(new Vector2f64(Double.NaN, Double.NaN), new Vector2f64(Double.NaN, Double.NaN)));


    }

    private void testIntersect(BBox bbox1, BBox bbox2, BBox result) {
        bbox1.localIntersect(bbox2);
        Assert.assertEquals(result, bbox1);
    }

}
