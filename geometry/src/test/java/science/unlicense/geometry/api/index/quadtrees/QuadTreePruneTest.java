package science.unlicense.geometry.api.index.quadtrees;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.index.Rectangle;
import science.unlicense.geometry.api.index.utilities.TestUtilities;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.math.impl.Vector2f64;

/**
 * @author Mark Raynsford
 */
public final class QuadTreePruneTest extends QuadTreeCommonTests {

    private static final double DELTA = 0.0000001;

  @Override <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad128()
  {
    try {
      return QuadTreePrune.newQuadTree(new Vector2f64(128, 128),
        new Vector2f64(0,0));
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }

    throw new RuntimeException("Unreachable code");
  }

  @Override <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad16()
  {
    try {
      return QuadTreePrune.newQuadTree(new Vector2f64(16, 16), new Vector2f64(0,0));
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }

    throw new RuntimeException("Unreachable code");
  }

  @Override <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad2()
  {
    try {
      return QuadTreePrune.newQuadTree(new Vector2f64(2, 2), new Vector2f64(0,0));
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }

    throw new RuntimeException("Unreachable code");
  }

  @Override <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad32()
  {
    try {
      return QuadTreePrune.newQuadTree(new Vector2f64(32, 32), new Vector2f64(0,0));
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }

    throw new RuntimeException("Unreachable code");
  }

  @Override <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad512()
  {
    try {
      return QuadTreePrune.newQuadTree(new Vector2f64(512, 512),
        new Vector2f64(0,0));
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }

    throw new RuntimeException("Unreachable code");
  }

  @Test
  public void testCreateOddX()
  {
    try {
        QuadTreePrune.newQuadTree(new Vector2f64(3, 2), new Vector2f64(0,0));
        Assert.fail("Exception expected");
        } catch (Exception ex) {}
  }

  @Test
  public void testCreateOddY()
  {
    try {
        QuadTreePrune.newQuadTree(new Vector2f64(2, 3), new Vector2f64(0,0));
        Assert.fail("Exception expected");
        } catch (Exception ex) {}
  }

  @Test
  public void testCreateTooSmallX()
  {
    try {
        QuadTreePrune.newQuadTree(new Vector2f64(1, 2), new Vector2f64(0,0));
        Assert.fail("Exception expected");
        } catch (Exception ex) {}
  }

  @Test
  public void testCreateTooSmallY()
  {
    try {
        QuadTreePrune.newQuadTree(new Vector2f64(2, 1), new Vector2f64(0,0));
        Assert.fail("Exception expected");
        } catch (Exception ex) {}
  }

  @Test
  public void testInsertSplitNotX() throws Exception
  {
    final QuadTreeType<Rectangle> q =
      QuadTreePrune.newQuadTree(new Vector2f64(2, 4), new Vector2f64(0,0));

    final Rectangle r =
      new Rectangle(0, new Vector2f64(0, 0), new Vector2f64(0, 0));

    final boolean in = q.quadTreeInsert(r);
    Assert.assertTrue(in);

    final Counter counter = new Counter();
    q.quadTreeTraverse(counter);
    Assert.assertEquals(5, counter.count);
  }

  @Test
  public void testInsertSplitNotY() throws Exception
  {
    final QuadTreeType<Rectangle> q =
      QuadTreePrune.newQuadTree(new Vector2f64(4, 2), new Vector2f64(0,0));

    final Rectangle r =
      new Rectangle(0, new Vector2f64(0, 0), new Vector2f64(0, 0));

    final boolean in = q.quadTreeInsert(r);
    Assert.assertTrue(in);

    final Counter counter = new Counter();
    q.quadTreeTraverse(counter);
    Assert.assertEquals(5, counter.count);
  }

  @Test
  public void testRaycastQuadrants()
  {
    final QuadTreeType<Rectangle> q =
      QuadTreePrune.newQuadTree(new Vector2f64(512, 512), new Vector2f64(0,0));

    q.quadTreeInsert(new Rectangle(0, new Vector2f64(32, 32), new Vector2f64(
      80,
      80)));
    q.quadTreeInsert(new Rectangle(1, new Vector2f64(400, 400), new Vector2f64(
      480,
      480)));

    final Ray ray =
      new Ray(new Vector2f64(0,0), new Vector2f64(511, 511).normalize());
    final SortedSet<QuadTreeRaycastResult<QuadrantType>> items =
      new TreeSet<QuadTreeRaycastResult<QuadrantType>>();
    q.quadTreeQueryRaycastQuadrants(ray, items);

    Assert.assertEquals(6, items.size());

    final Iterator<QuadTreeRaycastResult<QuadrantType>> iter
                = items.iterator();

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(0, quad.getLower().get(0),DELTA);
            Assert.assertEquals(0, quad.getLower().get(1),DELTA);
            Assert.assertEquals(63.875, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(63.875, quad.getUpper().get(1),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(63.875, quad.getLower().get(0),DELTA);
            Assert.assertEquals(63.875, quad.getLower().get(1),DELTA);
            Assert.assertEquals(127.75, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(127.75, quad.getUpper().get(1),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(127.75, quad.getLower().get(0),DELTA);
            Assert.assertEquals(127.75, quad.getLower().get(1),DELTA);
            Assert.assertEquals(255.5, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(255.5, quad.getUpper().get(1),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(255.5, quad.getLower().get(0),DELTA);
            Assert.assertEquals(255.5, quad.getLower().get(1),DELTA);
            Assert.assertEquals(383.25, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(383.25, quad.getUpper().get(1),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(383.25, quad.getLower().get(0),DELTA);
            Assert.assertEquals(383.25, quad.getLower().get(1),DELTA);
            Assert.assertEquals(447.125, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(447.125, quad.getUpper().get(1),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(447.125, quad.getLower().get(0),DELTA);
            Assert.assertEquals(447.125, quad.getLower().get(1),DELTA);
            Assert.assertEquals(511, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(511, quad.getUpper().get(1),DELTA);
        }

    Assert.assertFalse(iter.hasNext());
  }

  @Test
  public void testRaycastQuadrantsNegativeRay()
  {
    final QuadTreeType<Rectangle> q =
      QuadTreePrune.newQuadTree(new Vector2f64(512, 512), new Vector2f64(0,0));

    final Ray ray =
      new Ray(new Vector2f64(512, 512), new Vector2f64(
        -0.5,
        -0.5).normalize());
    final SortedSet<QuadTreeRaycastResult<QuadrantType>> items =
      new TreeSet<QuadTreeRaycastResult<QuadrantType>>();
    q.quadTreeQueryRaycastQuadrants(ray, items);

    Assert.assertEquals(1, items.size());
  }

  @Test
  public void testRemovePrune() throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad128();
    final Rectangle[] rectangles =
      TestUtilities.makeRectangles(0, q.quadTreeGetSizeX());

    for (final Rectangle r : rectangles) {
      {
        final boolean in = q.quadTreeInsert(r);
        Assert.assertTrue(in);
      }

      {
        final boolean in = q.quadTreeInsert(r);
        Assert.assertFalse(in);
      }
    }

    {
      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(21, counter.count);
    }

    {
      final boolean removed = q.quadTreeRemove(rectangles[0]);
      Assert.assertTrue(removed);

      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(17, counter.count);
    }

    {
      final boolean removed = q.quadTreeRemove(rectangles[1]);
      Assert.assertTrue(removed);

      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(13, counter.count);
    }

    {
      final boolean removed = q.quadTreeRemove(rectangles[2]);
      Assert.assertTrue(removed);

      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(9, counter.count);
    }

    {
      final boolean removed = q.quadTreeRemove(rectangles[3]);
      Assert.assertTrue(removed);

      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(1, counter.count);
    }

    {
      final IterationCounter counter = new IterationCounter();
      q.quadTreeIterateObjects(counter);
      Assert.assertEquals(0, counter.count);
    }

    for (final Rectangle r : rectangles) {
      {
        final boolean in = q.quadTreeInsert(r);
        Assert.assertTrue(in);
      }

      {
        final boolean in = q.quadTreeInsert(r);
        Assert.assertFalse(in);
      }
    }

    {
      final IterationCounter counter = new IterationCounter();
      q.quadTreeIterateObjects(counter);
      Assert.assertEquals(4, counter.count);
    }

    {
      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(21, counter.count);
    }
  }

  @Test
  public void testRemovePruneNotLeafNotEmpty() throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();

    /**
     * The rectangles are larger than the smallest quadrant size, and as a
     * result, will not be inserted into leaf nodes. Removing one of them will
     * trigger an attempt to prune nodes, which will fail due to a non-empty
     * non-leaf node.
     */

    final Rectangle r0 =
      new Rectangle(0, new Vector2f64(1, 1), new Vector2f64(7, 7));
    final Rectangle r1 =
      new Rectangle(1, new Vector2f64(1, 1), new Vector2f64(7, 7));

    {
      final boolean in = q.quadTreeInsert(r0);
      Assert.assertTrue(in);
    }

    {
      final boolean in = q.quadTreeInsert(r1);
      Assert.assertTrue(in);
    }

    {
      // (4 ^ 0) + (4 ^ 1) + 4
      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(9, counter.count);
    }

    {
      final boolean removed = q.quadTreeRemove(r0);
      Assert.assertTrue(removed);
    }

    {
      // (4 ^ 0) + (4 ^ 1)
      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(5, counter.count);
    }
  }
}
