package science.unlicense.geometry.api.index.quadtrees;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.api.index.Dimensions;
import science.unlicense.geometry.api.index.Rectangle;
import science.unlicense.geometry.api.index.SDType;
import science.unlicense.geometry.api.index.utilities.TestUtilities;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;

/**
 * @author Mark Raynsford
 */
public final class QuadTreeSDPruneLimitTest extends QuadTreeCommonTests {

    private static final double DELTA = 0.0000001;

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad128() {
        try {
            final VectorRW size = new Vector2f64(128, 128);
            final VectorRW position = new Vector2f64(0, 0);
            final VectorRW limit = new Vector2f64(2, 2);
            return QuadTreeSDPruneLimit.newQuadTree(size, position, limit);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad16() {
        try {
            final VectorRW size = new Vector2f64(16, 16);
            final VectorRW position = new Vector2f64(0, 0);
            final VectorRW limit = new Vector2f64(2, 2);
            return QuadTreeSDPruneLimit.newQuadTree(size, position, limit);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad2() {
        try {
            final VectorRW size = new Vector2f64(2, 2);
            final VectorRW position = new Vector2f64(0, 0);
            final VectorRW limit = new Vector2f64(2, 2);
            return QuadTreeSDPruneLimit.newQuadTree(size, position, limit);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad32() {
        try {
            final VectorRW size = new Vector2f64(32, 32);
            final VectorRW position = new Vector2f64(0, 0);
            final VectorRW limit = new Vector2f64(2, 2);
            return QuadTreeSDPruneLimit.newQuadTree(size, position, limit);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad512() {
        try {
            final VectorRW size = new Vector2f64(512, 512);
            final VectorRW position = new Vector2f64(0, 0);
            final VectorRW limit = new Vector2f64(2, 2);
            return QuadTreeSDPruneLimit.newQuadTree(size, position, limit);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Test
    public void testClearDynamic() throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(32, 32),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));
        final Rectangle[] dynamics = TestUtilities.makeRectangles(0, 32);
        final Rectangle[] statics = TestUtilities.makeRectangles(10, 32);

        for (final Rectangle r : dynamics) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
            Assert.assertTrue(in);
        }
        for (final Rectangle r : statics) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
            Assert.assertTrue(in);
        }

        {
            final IterationCounter counter = new IterationCounter();
            q.quadTreeIterateObjects(counter);
            Assert.assertEquals(8, counter.count);
        }

        q.quadTreeSDClearDynamic();

        {
            final IterationCounter counter = new IterationCounter();
            q.quadTreeIterateObjects(counter);
            Assert.assertEquals(4, counter.count);
        }
    }

    @Test
    public void testCreateLimitXOdd() {
        final VectorRW size = new Vector2f64(4, 4);
        final VectorRW position = new Vector2f64(0, 0);
        final VectorRW size_minimum = new Vector2f64(3, 2);
        try {
            QuadTreeSDPruneLimit.newQuadTree(size, position, size_minimum);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }
    }

    @Test
    public void testCreateLimitXTooBig() {
        final VectorRW size = new Vector2f64(2, 2);
        final VectorRW position = new Vector2f64(0, 0);
        final VectorRW size_minimum = new Vector2f64(4, 2);
        try {
            QuadTreeSDPruneLimit.newQuadTree(size, position, size_minimum);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }
    }

    @Test
    public void testCreateLimitXTooSmall() {
        final VectorRW size = new Vector2f64(2, 2);
        final VectorRW position = new Vector2f64(0, 0);
        final VectorRW size_minimum = new Vector2f64(1, 2);
        try {
            QuadTreeSDPruneLimit.newQuadTree(size, position, size_minimum);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }
    }

    @Test
    public void testCreateLimitYOdd() {
        final VectorRW size = new Vector2f64(4, 4);
        final VectorRW position = new Vector2f64(0, 0);
        final VectorRW size_minimum = new Vector2f64(2, 3);
        try {
            QuadTreeSDPruneLimit.newQuadTree(size, position, size_minimum);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }
    }

    @Test
    public void testCreateLimitYTooBig() {
        final VectorRW size = new Vector2f64(2, 2);
        final VectorRW position = new Vector2f64(0, 0);
        final VectorRW size_minimum = new Vector2f64(2, 4);
        try {
            QuadTreeSDPruneLimit.newQuadTree(size, position, size_minimum);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }
    }

    @Test
    public void testCreateLimitYTooSmall() {
        final VectorRW size = new Vector2f64(2, 2);
        final VectorRW position = new Vector2f64(0, 0);
        final VectorRW size_minimum = new Vector2f64(2, 1);
        try {
            QuadTreeSDPruneLimit.newQuadTree(size, position, size_minimum);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }
    }

    @Test
    public void testCreateSDXOdd() {
        try {
            QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(3, 2),
                    new Vector2f64(0, 0),
                    new Vector2f64(2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }

    }

    @Test
    public void testCreateSDXTooSmall() {
        try {
            QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(1, 2),
                    new Vector2f64(0, 0),
                    new Vector2f64(2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }

    }

    @Test
    public void testCreateSDYOdd() {
        try {
            QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(4, 3),
                    new Vector2f64(0, 0),
                    new Vector2f64(2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }

    }

    @Test
    public void testCreateSDYTooSmall() {
        try {
            QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(2, 1),
                    new Vector2f64(0, 0),
                    new Vector2f64(2, 2));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}

    }

    @Test
    public void testInsertLimit() throws Exception {
        final QuadTreeType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(16, 16),
                        new Vector2f64(0, 0),
                        new Vector2f64(8, 8));

        final Rectangle r
                = new Rectangle(0, new Vector2f64(0, 0), new Vector2f64(0, 0));

        final boolean in = q.quadTreeInsert(r);
        Assert.assertTrue(in);

        q.quadTreeTraverse(new QuadTreeTraversalType<Exception>() {
            @Override
            public void visit(
                    final int depth,
                    final Tuple lower,
                    final Tuple upper)
                    throws Exception {
                Assert.assertTrue(Dimensions.getSpanSizeX(lower, upper) >= 8);
                Assert.assertTrue(Dimensions.getSpanSizeX(lower, upper) >= 8);
            }
        });
    }

    @Test
    public void testInsertSplitNotX() throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(2, 4),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        final Rectangle r
                = new Rectangle(0, new Vector2f64(0, 0), new Vector2f64(0, 0));

        final boolean in = q.quadTreeInsert(r);
        Assert.assertTrue(in);

        final Counter counter = new Counter();
        q.quadTreeTraverse(counter);
        Assert.assertEquals(1, counter.count);
    }

    @Test
    public void testInsertSplitNotY() throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(4, 2),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        final Rectangle r
                = new Rectangle(0, new Vector2f64(0, 0), new Vector2f64(0, 0));

        final boolean in = q.quadTreeInsert(r);
        Assert.assertTrue(in);

        final Counter counter = new Counter();
        q.quadTreeTraverse(counter);
        Assert.assertEquals(1, counter.count);
    }

    @Test
    public void testInsertTypeDynamicCollision() throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(16, 16),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        final Rectangle r
                = new Rectangle(0, new Vector2f64(0, 0), new Vector2f64(12, 12));

        boolean in = false;
        in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
        Assert.assertTrue(in);
        in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
        Assert.assertFalse(in);
    }

    @Test
    public void testInsertTypeStaticCollision() throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(16, 16),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        final Rectangle r
                = new Rectangle(0, new Vector2f64(0, 0), new Vector2f64(12, 12));

        boolean in = false;
        in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
        Assert.assertTrue(in);
        in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
        Assert.assertFalse(in);
    }

    @Test
    public void testIterateStopEarlyDynamic() throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(16, 16),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        final Rectangle[] rectangles = TestUtilities.makeRectangles(0, 16);

        for (final Rectangle r : rectangles) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
            Assert.assertTrue(in);
        }

        final IterationChecker1 counter = new IterationChecker1() {
            @Override
            public Boolean evaluate(final Object candidate) {
                ++this.count;
                if (this.count >= 2) {
                    return Boolean.FALSE;
                }
                return Boolean.TRUE;
            }
        };

        q.quadTreeIterateObjects(counter);

        Assert.assertEquals(2, counter.count);
    }

    @Test
    public void testIterateStopEarlyStatic() throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(16, 16),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        final Rectangle[] rectangles = TestUtilities.makeRectangles(0, 16);

        for (final Rectangle r : rectangles) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
            Assert.assertTrue(in);
        }

        final IterationChecker1 counter = new IterationChecker1() {
            @Override
            public Boolean evaluate(final Object candidate) {
                ++this.count;
                if (this.count >= 2) {
                    return Boolean.FALSE;
                }
                return Boolean.TRUE;
            }
        };

        q.quadTreeIterateObjects(counter);

        Assert.assertEquals(2, counter.count);
    }

    @Test
    public void testQueryContainingStatic() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(128, 128),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        final boolean in
                = q.quadTreeInsertSD(new Rectangle(
                        0,
                        new Vector2f64(66, 66),
                        new Vector2f64(127, 127)), SDType.SD_STATIC);
        Assert.assertTrue(in);

        {
            final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
            q.quadTreeQueryAreaContaining(new Rectangle(
                    0,
                    new Vector2f64(66, 66),
                    new Vector2f64(127, 127)), items);

            Assert.assertEquals(1, items.size());
        }
    }

    @Test
    public void testQueryContainingStaticNot() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(128, 128),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        final boolean in
                = q.quadTreeInsertSD(new Rectangle(
                        0,
                        new Vector2f64(66, 66),
                        new Vector2f64(127, 127)), SDType.SD_STATIC);
        Assert.assertTrue(in);

        {
            final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
            q.quadTreeQueryAreaContaining(new Rectangle(
                    0,
                    new Vector2f64(0, 0),
                    new Vector2f64(65, 65)), items);

            Assert.assertEquals(0, items.size());
        }
    }

    @Test
    public void testQueryOverlappingStatic() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(128, 128),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        final Rectangle[] dynamics = TestUtilities.makeRectangles(0, 128);
        final Rectangle[] statics = TestUtilities.makeRectangles(10, 128);

        for (final Rectangle r : dynamics) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
            Assert.assertTrue(in);
        }
        for (final Rectangle r : statics) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
            Assert.assertTrue(in);
        }

        {
            final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
            q.quadTreeQueryAreaOverlapping(new Rectangle(
                    0,
                    new Vector2f64(16, 16),
                    new Vector2f64(80, 80)), items);

            Assert.assertEquals(8, items.size());
        }
    }

    @Test
    public void testQueryOverlappingStaticNot() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(128, 128),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        final boolean in
                = q.quadTreeInsertSD(new Rectangle(
                        0,
                        new Vector2f64(66, 66),
                        new Vector2f64(127, 127)), SDType.SD_STATIC);
        Assert.assertTrue(in);

        {
            final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
            q.quadTreeQueryAreaOverlapping(new Rectangle(
                    0,
                    new Vector2f64(0, 0),
                    new Vector2f64(65, 65)), items);

            Assert.assertEquals(0, items.size());
        }
    }

    @Test
    public void testRaycastQuadrants() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(512, 512),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        q.quadTreeInsert(new Rectangle(0, new Vector2f64(32, 32), new Vector2f64(
                80,
                80)));
        q.quadTreeInsert(new Rectangle(1, new Vector2f64(400, 400), new Vector2f64(
                480,
                480)));

        final Ray ray
                = new Ray(new Vector2f64(0, 0), new Vector2f64(511, 511).normalize());
        final SortedSet<QuadTreeRaycastResult<QuadrantType>> items
                = new TreeSet<QuadTreeRaycastResult<QuadrantType>>();
        q.quadTreeQueryRaycastQuadrants(ray, items);

        Assert.assertEquals(6, items.size());

        final Iterator<QuadTreeRaycastResult<QuadrantType>> iter
                = items.iterator();

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(0, quad.getLower().get(0), DELTA);
            Assert.assertEquals(0, quad.getLower().get(1), DELTA);
            Assert.assertEquals(63.875, quad.getUpper().get(0), DELTA);
            Assert.assertEquals(63.875, quad.getUpper().get(1), DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(63.875, quad.getLower().get(0), DELTA);
            Assert.assertEquals(63.875, quad.getLower().get(1), DELTA);
            Assert.assertEquals(127.75, quad.getUpper().get(0), DELTA);
            Assert.assertEquals(127.75, quad.getUpper().get(1), DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(127.75, quad.getLower().get(0), DELTA);
            Assert.assertEquals(127.75, quad.getLower().get(1), DELTA);
            Assert.assertEquals(255.5, quad.getUpper().get(0), DELTA);
            Assert.assertEquals(255.5, quad.getUpper().get(1), DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(255.5, quad.getLower().get(0), DELTA);
            Assert.assertEquals(255.5, quad.getLower().get(1), DELTA);
            Assert.assertEquals(383.25, quad.getUpper().get(0), DELTA);
            Assert.assertEquals(383.25, quad.getUpper().get(1), DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(383.25, quad.getLower().get(0), DELTA);
            Assert.assertEquals(383.25, quad.getLower().get(1), DELTA);
            Assert.assertEquals(447.125, quad.getUpper().get(0), DELTA);
            Assert.assertEquals(447.125, quad.getUpper().get(1), DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(447.125, quad.getLower().get(0), DELTA);
            Assert.assertEquals(447.125, quad.getLower().get(1), DELTA);
            Assert.assertEquals(511, quad.getUpper().get(0), DELTA);
            Assert.assertEquals(511, quad.getUpper().get(1), DELTA);
        }

        Assert.assertFalse(iter.hasNext());
    }

    @Test
    public void testRaycastQuadrantsNegativeRay() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(512, 512),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));

        final Ray ray
                = new Ray(new Vector2f64(512, 512), new Vector2f64(
                        -0.5,
                        -0.5).normalize());
        final SortedSet<QuadTreeRaycastResult<QuadrantType>> items
                = new TreeSet<QuadTreeRaycastResult<QuadrantType>>();
        q.quadTreeQueryRaycastQuadrants(ray, items);

        Assert.assertEquals(1, items.size());
    }

    @Test
    public void testRemovePrune() throws Exception {
        final QuadTreeType<Rectangle> q = this.makeQuad128();
        final Rectangle[] rectangles
                = TestUtilities.makeRectangles(0, q.quadTreeGetSizeX());

        for (final Rectangle r : rectangles) {
            {
                final boolean in = q.quadTreeInsert(r);
                Assert.assertTrue(in);
            }

            {
                final boolean in = q.quadTreeInsert(r);
                Assert.assertFalse(in);
            }
        }

        {
            final Counter counter = new Counter();
            q.quadTreeTraverse(counter);
            Assert.assertEquals(21, counter.count);
        }

        {
            final boolean removed = q.quadTreeRemove(rectangles[0]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.quadTreeTraverse(counter);
            Assert.assertEquals(17, counter.count);
        }

        {
            final boolean removed = q.quadTreeRemove(rectangles[1]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.quadTreeTraverse(counter);
            Assert.assertEquals(13, counter.count);
        }

        {
            final boolean removed = q.quadTreeRemove(rectangles[2]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.quadTreeTraverse(counter);
            Assert.assertEquals(9, counter.count);
        }

        {
            final boolean removed = q.quadTreeRemove(rectangles[3]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.quadTreeTraverse(counter);
            Assert.assertEquals(1, counter.count);
        }

        {
            final IterationCounter counter = new IterationCounter();
            q.quadTreeIterateObjects(counter);
            Assert.assertEquals(0, counter.count);
        }

        for (final Rectangle r : rectangles) {
            {
                final boolean in = q.quadTreeInsert(r);
                Assert.assertTrue(in);
            }

            {
                final boolean in = q.quadTreeInsert(r);
                Assert.assertFalse(in);
            }
        }

        {
            final IterationCounter counter = new IterationCounter();
            q.quadTreeIterateObjects(counter);
            Assert.assertEquals(4, counter.count);
        }

        {
            final Counter counter = new Counter();
            q.quadTreeTraverse(counter);
            Assert.assertEquals(21, counter.count);
        }
    }

    @Test
    public void testRemovePruneNotLeafNotEmpty() throws Exception {
        final QuadTreeType<Rectangle> q = this.makeQuad16();

        /**
         * The rectangles are larger than the smallest quadrant size, and as a
         * result, will not be inserted into leaf nodes. Removing one of them
         * will trigger an attempt to prune nodes, which will fail due to a
         * non-empty non-leaf node.
         */
        final Rectangle r0
                = new Rectangle(0, new Vector2f64(1, 1), new Vector2f64(7, 7));
        final Rectangle r1
                = new Rectangle(1, new Vector2f64(1, 1), new Vector2f64(7, 7));

        {
            final boolean in = q.quadTreeInsert(r0);
            Assert.assertTrue(in);
        }

        {
            final boolean in = q.quadTreeInsert(r1);
            Assert.assertTrue(in);
        }

        {
            // (4 ^ 0) + (4 ^ 1) + 4
            final Counter counter = new Counter();
            q.quadTreeTraverse(counter);
            Assert.assertEquals(9, counter.count);
        }

        {
            final boolean removed = q.quadTreeRemove(r0);
            Assert.assertTrue(removed);
        }

        {
            // (4 ^ 0) + (4 ^ 1)
            final Counter counter = new Counter();
            q.quadTreeTraverse(counter);
            Assert.assertEquals(5, counter.count);
        }
    }

    @Test
    public void testRemoveSubDynamic() throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(32, 32),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));
        final Rectangle[] rectangles = TestUtilities.makeRectangles(0, 32);

        for (final Rectangle r : rectangles) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
            Assert.assertTrue(in);
        }

        for (final Rectangle r : rectangles) {
            final boolean removed = q.quadTreeRemove(r);
            Assert.assertTrue(removed);
        }
    }

    @Test
    public void testRemoveSubStatic() throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDPruneLimit.newQuadTree(new Vector2f64(32, 32),
                        new Vector2f64(0, 0),
                        new Vector2f64(2, 2));
        final Rectangle[] rectangles = TestUtilities.makeRectangles(0, 32);

        for (final Rectangle r : rectangles) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
            Assert.assertTrue(in);
        }

        for (final Rectangle r : rectangles) {
            final boolean removed = q.quadTreeRemove(r);
            Assert.assertTrue(removed);
        }
    }

}
