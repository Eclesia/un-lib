package science.unlicense.geometry.api.index;

import science.unlicense.common.api.CObjects;
import science.unlicense.geometry.api.index.quadtrees.QuadTreeMemberType;
import science.unlicense.math.api.VectorRW;

/**
 * @author Mark Raynsford
 */
public final class Rectangle extends QuadTreeMemberType<Rectangle> {

    private final long id;

    public Rectangle(
            final long in_id,
            final VectorRW in_lower,
            final VectorRW in_upper) {
        this.id = in_id;
        CObjects.ensureNotNull(in_lower);
        CObjects.ensureNotNull(in_upper);
        this.lower.set(in_lower);
        this.upper.set(in_upper);
    }

    @Override
    public int compareTo(
            final Rectangle other) {
        CObjects.ensureNotNull(other);
        final Rectangle o = other;

        if (o.id < this.id) {
            return -1;
        }
        if (o.id > this.id) {
            return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(
            final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Rectangle other = (Rectangle) obj;
        if (!this.lower.equals(other.lower)) {
            return false;
        }
        if (!this.upper.equals(other.upper)) {
            return false;
        }
        return true;
    }

//  @Override public int hashCode()
//  {
//    final int prime = 31;
//    int result = 1;
//    result = (prime * result) + this.lower.hashCode();
//    result = (prime * result) + this.upper.hashCode();
//    return result;
//  }
//
//  @Override public String toString()
//  {
//    final StringBuilder b = new StringBuilder();
//    b.append("[Rectangle lower=");
//    b.append(this.lower);
//    b.append(", upper=");
//    b.append(this.upper);
//    b.append("]");
//    final String r = b.toString();
//    assert r != null;
//    return r;
//  }
}
