package science.unlicense.geometry.api.index;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import static science.unlicense.geometry.api.index.RayI3DTest.DELTA;
import science.unlicense.math.impl.Vector2f64;

/**
 * @author Mark Raynsford
 */
public class RayI2DTest {

    @Test
    public void testRayEqualsNotCase0() {
        final Ray ray0 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        Assert.assertFalse(ray0.equals(null));
    }

    @Test
    public void testRayEqualsNotCase1() {
        final Ray ray0 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        Assert.assertFalse(ray0.equals(Integer.valueOf(23)));
    }

    @Test
    public void testRayEqualsNotCase2() {
        final Ray ray0 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        final Ray ray1 = new Ray(new Vector2f64(1, 2), new Vector2f64(0, 0));
        Assert.assertFalse(ray0.equals(ray1));
    }

    @Test
    public void testRayEqualsNotCase3() {
        final Ray ray0 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        final Ray ray1 = new Ray(new Vector2f64(0, 0), new Vector2f64(1, 2));
        Assert.assertFalse(ray0.equals(ray1));
    }

    @Test
    public void testRayEqualsReflexive() {
        final Ray ray0 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        Assert.assertEquals(ray0, ray0);
    }

    @Test
    public void testRayEqualsSymmetric() {
        final Ray ray0 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        final Ray ray1 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        Assert.assertEquals(ray0, ray1);
        Assert.assertEquals(ray1, ray0);
    }

    @Test
    public void testRayEqualsTransitive() {
        final Ray ray0 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        final Ray ray1 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        final Ray ray2 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        Assert.assertEquals(ray0, ray1);
        Assert.assertEquals(ray1, ray2);
        Assert.assertEquals(ray0, ray2);
    }

    //TODO remove this test ? Un Vectors are modifiable
    @Ignore
    @Test
    public void testSkipRayHashCodeEquals() {
        final Ray ray0 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        final Ray ray1 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        Assert.assertTrue(ray0.hashCode() == ray1.hashCode());
    }

    @Test
    public void testRayToStringEquals() {
        final Ray ray0 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        final Ray ray1 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        Assert.assertTrue(ray0.toString().equals(ray1.toString()));
    }

    @Test
    public void testRayToStringNotEquals() {
        final Ray ray0 = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));
        final Ray ray1 = new Ray(new Vector2f64(0, 0), new Vector2f64(1, 2));
        Assert.assertFalse(ray0.toString().equals(ray1.toString()));
    }

    @Test
    public void testRayZero() {
        final Ray ray = new Ray(new Vector2f64(0, 0), new Vector2f64(0, 0));

        final Tuple ray_origin = ray.getPosition();
        final VectorRW ray_direction = ray.getDirection();
//    final Vector ray_direction_inv = ray.getDirectionInverse();

        Assert.assertEquals(ray_origin.get(0),ray_origin.get(1),DELTA);
        Assert.assertEquals(ray_direction.getX(),ray_direction.getY(),DELTA);

//    Assert.assertTrue(ray_direction_inv.getX() == Double.POSITIVE_INFINITY);
//    Assert.assertTrue(ray_direction_inv.getY() == Double.POSITIVE_INFINITY);
    }
}
