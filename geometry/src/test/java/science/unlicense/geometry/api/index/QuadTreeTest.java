
package science.unlicense.geometry.api.index;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;

/**
 *
 *@author Johann Sorel
 */
public class QuadTreeTest {

    private static final double DELTA = 0.00001;

    @Test
    public void test1D(){
        final QuadTree tree = new QuadTree(1, 1, new BBox(new double[]{10}, new double[]{20}));
        final QuadNode[] children = tree.getRoot().getChildrenCreate();
        Assert.assertEquals(2, children.length);
        Assert.assertArrayEquals(new double[]{10}, children[0].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{15}, children[0].getUpper().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{15}, children[1].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{20}, children[1].getUpper().toDouble(), DELTA);
    }

    @Test
    public void test2D(){
        final QuadTree tree = new QuadTree(2, 1, new BBox(new double[]{10,100}, new double[]{20,200}));
        final QuadNode[] children = tree.getRoot().getChildrenCreate();
        Assert.assertEquals(4, children.length);
        Assert.assertArrayEquals(new double[]{10,100}, children[0].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{15,150}, children[0].getUpper().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{10,150}, children[1].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{15,200}, children[1].getUpper().toDouble(), DELTA);

        Assert.assertArrayEquals(new double[]{15,100}, children[2].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{20,150}, children[2].getUpper().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{15,150}, children[3].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{20,200}, children[3].getUpper().toDouble(), DELTA);
    }

    @Test
    public void test3D(){
        final QuadTree tree = new QuadTree(3, 1, new BBox(new double[]{10,100,1000}, new double[]{20,200,2000}));
        final QuadNode[] children = tree.getRoot().getChildrenCreate();
        Assert.assertEquals(8, children.length);
        Assert.assertArrayEquals(new double[]{10,100,1000}, children[0].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{15,150,1500}, children[0].getUpper().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{10,100,1500}, children[1].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{15,150,2000}, children[1].getUpper().toDouble(), DELTA);

        Assert.assertArrayEquals(new double[]{10,150,1000}, children[2].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{15,200,1500}, children[2].getUpper().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{10,150,1500}, children[3].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{15,200,2000}, children[3].getUpper().toDouble(), DELTA);

        Assert.assertArrayEquals(new double[]{15,100,1000}, children[4].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{20,150,1500}, children[4].getUpper().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{15,100,1500}, children[5].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{20,150,2000}, children[5].getUpper().toDouble(), DELTA);

        Assert.assertArrayEquals(new double[]{15,150,1000}, children[6].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{20,200,1500}, children[6].getUpper().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{15,150,1500}, children[7].getLower().toDouble(), DELTA);
        Assert.assertArrayEquals(new double[]{20,200,2000}, children[7].getUpper().toDouble(), DELTA);
    }

}
