
package science.unlicense.geometry.api.index.quadtrees;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;

/**
 * @author Mark Raynsford
 */
public final class QuadrantsTest{

    private static final double DELTA = 0.0000001;

  @Test
  public void testQuadrantsSimple() {
    final VectorRW lower = new Vector2f64(8, 8);
    final VectorRW upper = new Vector2f64(15, 15);
    final Quadrants q = Quadrants.split(lower, upper);

    Assert.assertEquals(8, q.getX0Y0Lower().get(0),DELTA);
    Assert.assertEquals(8, q.getX0Y0Lower().get(1),DELTA);
    Assert.assertEquals(11.5, q.getX0Y0Upper().get(0),DELTA);
    Assert.assertEquals(11.5, q.getX0Y0Upper().get(1),DELTA);

    Assert.assertEquals(11.5, q.getX1Y0Lower().get(0),DELTA);
    Assert.assertEquals(8, q.getX1Y0Lower().get(1),DELTA);
    Assert.assertEquals(15, q.getX1Y0Upper().get(0),DELTA);
    Assert.assertEquals(11.5, q.getX1Y0Upper().get(1),DELTA);

    Assert.assertEquals(8, q.getX0Y1Lower().get(0),DELTA);
    Assert.assertEquals(11.5, q.getX0Y1Lower().get(1),DELTA);
    Assert.assertEquals(11.5, q.getX0Y1Upper().get(0),DELTA);
    Assert.assertEquals(15, q.getX0Y1Upper().get(1),DELTA);

    Assert.assertEquals(11.5, q.getX1Y1Lower().get(0),DELTA);
    Assert.assertEquals(11.5, q.getX1Y1Lower().get(1),DELTA);
    Assert.assertEquals(15, q.getX1Y1Upper().get(0),DELTA);
    Assert.assertEquals(15, q.getX1Y1Upper().get(1),DELTA);
  }
}
