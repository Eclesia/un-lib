
package science.unlicense.geometry.api.system;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.api.unitold.Units;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class CoordinateSystemsTest {

    private static final double DELTA = 0.0000001;

    @Test
    public void testTransform(){

        final CoordinateSystem source = new DefaultCoordinateSystem(
            new Axis[]{
                new Axis(Direction.BACKWARD, Units.CENTIMETER),
                new Axis(Direction.LEFT, Units.CENTIMETER),
                new Axis(Direction.UP, Units.CENTIMETER),
            }
        );
        final CoordinateSystem target = new DefaultCoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.METER),
                new Axis(Direction.UP, Units.METER),
                new Axis(Direction.FORWARD, Units.METER)
            }
        );

        final Transform trs = CoordinateSystems.createTransform(source, target);

        Tuple tuple = new Vector3f64(1, 2, 3);
        tuple = trs.transform(tuple, null);

        Assert.assertEquals(-0.02, tuple.get(0), DELTA);
        Assert.assertEquals(+0.03, tuple.get(1), DELTA);
        Assert.assertEquals(-0.01, tuple.get(2), DELTA);

    }

    @Test
    public void testReduceTransform(){

        final CoordinateSystem source = new DefaultCoordinateSystem(
            new Axis[]{
                new Axis(Direction.FORWARD, Units.METER),
                new Axis(Direction.UP, Units.METER),
                new Axis(Direction.RIGHT, Units.METER)
            }
        );
        final CoordinateSystem target = new DefaultCoordinateSystem(
            new Axis[]{
                new Axis(Direction.FORWARD, Units.METER),
                new Axis(Direction.RIGHT, Units.METER)
            }
        );

        final Transform trs = CoordinateSystems.createTransform(source, target);
        Assert.assertEquals(3, trs.getInputDimensions());
        Assert.assertEquals(2, trs.getOutputDimensions());

        Tuple tuple = new Vector3f64(1, 2, 3);
        tuple = trs.transform(tuple, null);
        Assert.assertEquals(2, tuple.getSampleCount());

        Assert.assertEquals(1, tuple.get(0), DELTA);
        Assert.assertEquals(3, tuple.get(1), DELTA);
    }

}
