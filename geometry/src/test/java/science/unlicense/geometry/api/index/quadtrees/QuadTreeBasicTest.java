package science.unlicense.geometry.api.index.quadtrees;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.api.index.Rectangle;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;

/**
 * @author Mark Raynsford
 */
public final class QuadTreeBasicTest extends QuadTreeCommonTests {

    private static final double DELTA = 0.0000001;

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad128() {
        try {
            final VectorRW size = new Vector2f64(128, 128);
            final VectorRW position = new Vector2f64(0, 0);
            return QuadTreeBasic.newQuadTree(size, position);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad16() {
        try {
            final VectorRW size = new Vector2f64(16, 16);
            final VectorRW position = new Vector2f64(0, 0);
            return QuadTreeBasic.newQuadTree(size, position);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad2() {
        try {
            final VectorRW size = new Vector2f64(2, 2);
            final VectorRW position = new Vector2f64(0, 0);
            return QuadTreeBasic.newQuadTree(size, position);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad32() {
        try {
            final VectorRW size = new Vector2f64(32, 32);
            final VectorRW position = new Vector2f64(0, 0);
            return QuadTreeBasic.newQuadTree(size, position);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad512() {
        try {
            final VectorRW size = new Vector2f64(512, 512);
            final VectorRW position = new Vector2f64(0, 0);
            return QuadTreeBasic.newQuadTree(size, position);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Test
    public void testCreateOddX() throws Exception {
        final VectorRW size = new Vector2f64(3, 2);
        final VectorRW position = new Vector2f64(0, 0);
        try {
            QuadTreeBasic.newQuadTree(size, position);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateOddY() throws Exception {
        final VectorRW size = new Vector2f64(2, 3);
        final VectorRW position = new Vector2f64(0, 0);
        try {
            QuadTreeBasic.newQuadTree(size, position);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateTooSmallX() throws Exception {
        final VectorRW size = new Vector2f64(1, 2);
        final VectorRW position = new Vector2f64(0, 0);
        try {
            QuadTreeBasic.newQuadTree(size, position);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateTooSmallY() throws Exception {
        final VectorRW size = new Vector2f64(2, 1);
        final VectorRW position = new Vector2f64(0, 0);
        try {
            QuadTreeBasic.newQuadTree(size, position);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testInsertSplitNotX() throws Exception {
        final QuadTreeType<Rectangle> q
                = QuadTreeBasic.newQuadTree(new Vector2f64(2, 4), new Vector2f64(0, 0));

        final Rectangle r
                = new Rectangle(0, new Vector2f64(0, 0), new Vector2f64(0, 0));

        final boolean in = q.quadTreeInsert(r);
        Assert.assertTrue(in);

        final Counter counter = new Counter();
        q.quadTreeTraverse(counter);
        Assert.assertEquals(5, counter.count);
    }

    @Test
    public void testInsertSplitNotY() throws Exception {
        final QuadTreeType<Rectangle> q
                = QuadTreeBasic.newQuadTree(new Vector2f64(4, 2), new Vector2f64(0, 0));

        final Rectangle r
                = new Rectangle(0, new Vector2f64(0, 0), new Vector2f64(0, 0));

        final boolean in = q.quadTreeInsert(r);
        Assert.assertTrue(in);

        final Counter counter = new Counter();
        q.quadTreeTraverse(counter);
        Assert.assertEquals(5, counter.count);
    }

    @Test
    public void testRaycastQuadrants() throws Exception {
        final QuadTreeType<Rectangle> q
                = QuadTreeBasic.newQuadTree(new Vector2f64(512, 512), new Vector2f64(0, 0));

        q.quadTreeInsert(new Rectangle(0, new Vector2f64(32, 32), new Vector2f64(80,80)));
        q.quadTreeInsert(new Rectangle(1, new Vector2f64(400, 400), new Vector2f64(480,480)));

        final Ray ray = new Ray(new Vector2f64(0, 0), new Vector2f64(511, 511).normalize());
        final SortedSet<QuadTreeRaycastResult<QuadrantType>> items
                = new TreeSet<QuadTreeRaycastResult<QuadrantType>>();
        q.quadTreeQueryRaycastQuadrants(ray, items);

        Assert.assertEquals(6, items.size());

        final Iterator<QuadTreeRaycastResult<QuadrantType>> iter
                = items.iterator();

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(0, quad.getLower().get(0),DELTA);
            Assert.assertEquals(0, quad.getLower().get(1),DELTA);
            Assert.assertEquals(63.875, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(63.875, quad.getUpper().get(1),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(63.875, quad.getLower().get(0),DELTA);
            Assert.assertEquals(63.875, quad.getLower().get(1),DELTA);
            Assert.assertEquals(127.75, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(127.75, quad.getUpper().get(1),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(127.75, quad.getLower().get(0),DELTA);
            Assert.assertEquals(127.75, quad.getLower().get(1),DELTA);
            Assert.assertEquals(255.5, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(255.5, quad.getUpper().get(1),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(255.5, quad.getLower().get(0),DELTA);
            Assert.assertEquals(255.5, quad.getLower().get(1),DELTA);
            Assert.assertEquals(383.25, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(383.25, quad.getUpper().get(1),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(383.25, quad.getLower().get(0),DELTA);
            Assert.assertEquals(383.25, quad.getLower().get(1),DELTA);
            Assert.assertEquals(447.125, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(447.125, quad.getUpper().get(1),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(447.125, quad.getLower().get(0),DELTA);
            Assert.assertEquals(447.125, quad.getLower().get(1),DELTA);
            Assert.assertEquals(511, quad.getUpper().get(0),DELTA);
            Assert.assertEquals(511, quad.getUpper().get(1),DELTA);
        }

        Assert.assertFalse(iter.hasNext());
    }

    @Test
    public void testRaycastQuadrantsNegativeRay() throws Exception {
        final QuadTreeType<Rectangle> q
                = QuadTreeBasic.newQuadTree(new Vector2f64(512, 512), new Vector2f64(0, 0));

        final Ray ray
                = new Ray(new Vector2f64(512, 512), new Vector2f64(
                                -0.5,
                                -0.5).normalize());
        final SortedSet<QuadTreeRaycastResult<QuadrantType>> items
                = new TreeSet<QuadTreeRaycastResult<QuadrantType>>();
        q.quadTreeQueryRaycastQuadrants(ray, items);

        Assert.assertEquals(1, items.size());
    }

}
