package science.unlicense.geometry.api.index.octtrees;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.api.index.Cuboid;

/**
 * @author Mark Raynsford
 */
public final class OctTreeBuilderTest {

    private static final double DELTA = 0.0000001;

    @Test
    public void testBasic() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        b.disableLimitedOctantSizes();
        b.disablePruning();
        b.setPosition3i(0, 4, 8);
        b.setSize3i(128, 64, 32);

        final OctTreeType<Cuboid> r = b.build();
        Assert.assertTrue(r instanceof OctTreeBasic);
        Assert.assertEquals(128, r.octTreeGetSizeX(),DELTA);
        Assert.assertEquals(64, r.octTreeGetSizeY(),DELTA);
        Assert.assertEquals(32, r.octTreeGetSizeZ(),DELTA);
        Assert.assertEquals(0, r.octTreeGetPositionX(),DELTA);
        Assert.assertEquals(4, r.octTreeGetPositionY(),DELTA);
        Assert.assertEquals(8, r.octTreeGetPositionZ(),DELTA);
    }

    @Test
    public void testLimit() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        b.enableLimitedOctantSizes(2, 4, 8);
        b.disablePruning();
        b.setPosition3i(0, 4, 8);
        b.setSize3i(128, 64, 32);

        final OctTreeType<Cuboid> r = b.build();
        Assert.assertTrue(r instanceof OctTreeLimit);
        Assert.assertEquals(128, r.octTreeGetSizeX(),DELTA);
        Assert.assertEquals(64, r.octTreeGetSizeY(),DELTA);
        Assert.assertEquals(32, r.octTreeGetSizeZ(),DELTA);
        Assert.assertEquals(0, r.octTreeGetPositionX(),DELTA);
        Assert.assertEquals(4, r.octTreeGetPositionY(),DELTA);
        Assert.assertEquals(8, r.octTreeGetPositionZ(),DELTA);
    }

    @Test
    public void testPrune() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        b.disableLimitedOctantSizes();
        b.enablePruning();
        b.setPosition3i(0, 4, 8);
        b.setSize3i(128, 64, 32);

        final OctTreeType<Cuboid> r = b.build();
        Assert.assertTrue(r instanceof OctTreePrune);
        Assert.assertEquals(128, r.octTreeGetSizeX(),DELTA);
        Assert.assertEquals(64, r.octTreeGetSizeY(),DELTA);
        Assert.assertEquals(32, r.octTreeGetSizeZ(),DELTA);
        Assert.assertEquals(0, r.octTreeGetPositionX(),DELTA);
        Assert.assertEquals(4, r.octTreeGetPositionY(),DELTA);
        Assert.assertEquals(8, r.octTreeGetPositionZ(),DELTA);
    }

    @Test
    public void testPruneLimit() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        b.enableLimitedOctantSizes(2, 4, 8);
        b.enablePruning();
        b.setPosition3i(0, 4, 8);
        b.setSize3i(128, 64, 32);

        final OctTreeType<Cuboid> r = b.build();
        Assert.assertTrue(r instanceof OctTreePruneLimit);
        Assert.assertEquals(128, r.octTreeGetSizeX(),DELTA);
        Assert.assertEquals(64, r.octTreeGetSizeY(),DELTA);
        Assert.assertEquals(32, r.octTreeGetSizeZ(),DELTA);
        Assert.assertEquals(0, r.octTreeGetPositionX(),DELTA);
        Assert.assertEquals(4, r.octTreeGetPositionY(),DELTA);
        Assert.assertEquals(8, r.octTreeGetPositionZ(),DELTA);
    }

    @Test
    public void testSDBasic() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        b.disableLimitedOctantSizes();
        b.disablePruning();
        b.setPosition3i(0, 4, 8);
        b.setSize3i(128, 64, 32);

        final OctTreeType<Cuboid> r = b.buildWithSD();
        Assert.assertTrue(r instanceof OctTreeSDBasic);
        Assert.assertEquals(128, r.octTreeGetSizeX(),DELTA);
        Assert.assertEquals(64, r.octTreeGetSizeY(),DELTA);
        Assert.assertEquals(32, r.octTreeGetSizeZ(),DELTA);
        Assert.assertEquals(0, r.octTreeGetPositionX(),DELTA);
        Assert.assertEquals(4, r.octTreeGetPositionY(),DELTA);
        Assert.assertEquals(8, r.octTreeGetPositionZ(),DELTA);
    }

    @Test
    public void testSDLimit() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        b.enableLimitedOctantSizes(2, 4, 8);
        b.disablePruning();
        b.setPosition3i(0, 4, 8);
        b.setSize3i(128, 64, 32);

        final OctTreeType<Cuboid> r = b.buildWithSD();
        Assert.assertTrue(r instanceof OctTreeSDLimit);
        Assert.assertEquals(128, r.octTreeGetSizeX(),DELTA);
        Assert.assertEquals(64, r.octTreeGetSizeY(),DELTA);
        Assert.assertEquals(32, r.octTreeGetSizeZ(),DELTA);
        Assert.assertEquals(0, r.octTreeGetPositionX(),DELTA);
        Assert.assertEquals(4, r.octTreeGetPositionY(),DELTA);
        Assert.assertEquals(8, r.octTreeGetPositionZ(),DELTA);
    }

    @Test
    public void testSDPrune() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        b.disableLimitedOctantSizes();
        b.enablePruning();
        b.setPosition3i(0, 4, 8);
        b.setSize3i(128, 64, 32);

        final OctTreeType<Cuboid> r = b.buildWithSD();
        Assert.assertTrue(r instanceof OctTreeSDPrune);
        Assert.assertEquals(128, r.octTreeGetSizeX(),DELTA);
        Assert.assertEquals(64, r.octTreeGetSizeY(),DELTA);
        Assert.assertEquals(32, r.octTreeGetSizeZ(),DELTA);
        Assert.assertEquals(0, r.octTreeGetPositionX(),DELTA);
        Assert.assertEquals(4, r.octTreeGetPositionY(),DELTA);
        Assert.assertEquals(8, r.octTreeGetPositionZ(),DELTA);
    }

    @Test
    public void testSDPruneLimit() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        b.enableLimitedOctantSizes(2, 4, 8);
        b.enablePruning();
        b.setPosition3i(0, 4, 8);
        b.setSize3i(128, 64, 32);

        final OctTreeType<Cuboid> r = b.buildWithSD();
        Assert.assertTrue(r instanceof OctTreeSDPruneLimit);
        Assert.assertEquals(128, r.octTreeGetSizeX(),DELTA);
        Assert.assertEquals(64, r.octTreeGetSizeY(),DELTA);
        Assert.assertEquals(32, r.octTreeGetSizeZ(),DELTA);
        Assert.assertEquals(0, r.octTreeGetPositionX(),DELTA);
        Assert.assertEquals(4, r.octTreeGetPositionY(),DELTA);
        Assert.assertEquals(8, r.octTreeGetPositionZ(),DELTA);
    }

    @Test
    public void testTooSmallLimitX() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        try {
            b.enableLimitedOctantSizes(0, 2, 2);
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testTooSmallLimitY() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        try {
            b.enableLimitedOctantSizes(2, 0, 2);
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testTooSmallLimitZ() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        try {
            b.enableLimitedOctantSizes(2, 2, 0);
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testTooSmallX() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        try {
            b.setSize3i(0, 2, 2);
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testTooSmallY() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        try {
            b.setSize3i(2, 0, 2);
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testTooSmallZ() {
        final OctTreeBuilderType<Cuboid> b = OctTreeBuilder.newBuilder();
        try {
            b.setSize3i(2, 2, 0);
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }
}
