package science.unlicense.geometry.api.index;

import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 * @author Mark Raynsford
 */
public class DimensionsTest{

    private static final double DELTA = 0.0000001;

  @Test
  public void testSpan1D() {
    final double spans[] = new double[4];

    Dimensions.split1D(0, 3, spans);
    Assert.assertEquals(0, spans[0],DELTA);
    Assert.assertEquals(1.5, spans[1],DELTA);
    Assert.assertEquals(1.5, spans[2],DELTA);
    Assert.assertEquals(3, spans[3],DELTA);
  }

  @Test
  public void testSpan1DVarious() {
    final double spans[] = new double[4];

    for (int i = 2; i < 30; ++i) {
        double size = (1 << i) - 1;
      Dimensions.split1D(0, size, spans);

      final double lower_lower = 0;
      final double lower_upper = size /2.0;
      final double upper_lower = size /2.0;
      final double upper_upper = size;

      System.err.print("testSpan1DVarious: [");
      for (final double k : spans) {
        System.err.print(k);
        System.err.print(" ");
      }
      System.err.println("]");

      Assert.assertEquals(lower_lower, spans[0],DELTA);
      Assert.assertEquals(lower_upper, spans[1],DELTA);
      Assert.assertEquals(upper_lower, spans[2],DELTA);
      Assert.assertEquals(upper_upper, spans[3],DELTA);
    }
  }

}
