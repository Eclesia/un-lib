
package science.unlicense.geometry.api.tuple;

import org.junit.Test;
import static science.unlicense.common.api.Assert.*;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultInt8Buffer;
import science.unlicense.common.api.number.Int8;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 *
 * @author Johann Sorel
 */
public class DefaultTupleCursorTest {

    @Test
    public void testCursor() {

        final Buffer bank = new DefaultInt8Buffer(new byte[]{0,1,2,3,4,5,6,7,8,9});
        final TupleGrid buffer = InterleavedTupleGrid.create(bank, Int8.TYPE, new UndefinedSystem(2), new Extent.Long(5, 1));
        final TupleGridCursor cursor = buffer.cursor();
        final byte[] tuple = new byte[2];

        assertTrue(cursor.next());
        cursor.samples().toByte(tuple, 0);
        assertEquals(0, tuple[0]);
        assertEquals(1, tuple[1]);

        assertTrue(cursor.next());
        cursor.samples().toByte(tuple, 0);
        assertEquals(2, tuple[0]);
        assertEquals(3, tuple[1]);

        assertTrue(cursor.next());
        cursor.samples().toByte(tuple, 0);
        assertEquals(4, tuple[0]);
        assertEquals(5, tuple[1]);

        assertTrue(cursor.next());
        cursor.samples().toByte(tuple, 0);
        assertEquals(6, tuple[0]);
        assertEquals(7, tuple[1]);

        assertTrue(cursor.next());
        cursor.samples().toByte(tuple, 0);
        assertEquals(8, tuple[0]);
        assertEquals(9, tuple[1]);

        assertFalse(cursor.next());

    }

    @Test
    public void testCursorByte1D() {

        final Buffer bank = new DefaultInt8Buffer(new byte[]{0,1,2,3,4});
        final TupleGrid buffer = InterleavedTupleGrid.create(bank, Int8.TYPE, new UndefinedSystem(1), new Extent.Long(5, 1));
        final TupleGridCursor cursor = buffer.cursor();
        final byte[] tuple = new byte[1];

        assertTrue(cursor.next());
        cursor.samples().toByte(tuple, 0);
        assertEquals(0, tuple[0]);

        assertTrue(cursor.next());
        cursor.samples().toByte(tuple, 0);
        assertEquals(1, tuple[0]);

        assertTrue(cursor.next());
        cursor.samples().toByte(tuple, 0);
        assertEquals(2, tuple[0]);

        assertTrue(cursor.next());
        cursor.samples().toByte(tuple, 0);
        assertEquals(3, tuple[0]);

        assertTrue(cursor.next());
        cursor.samples().toByte(tuple, 0);
        assertEquals(4, tuple[0]);

        assertFalse(cursor.next());

    }

}
