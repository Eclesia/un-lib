package science.unlicense.geometry.api.index.octtrees;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.api.index.Cuboid;
import science.unlicense.math.impl.Vector3f64;

/**
 * @author Mark Raynsford
 */
public final class OctTreeBasicTest extends OctTreeCommonTests {

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct128() {
        try {
            return OctTreeBasic.newOctTree(new Vector3f64(128, 128, 128),
                    new Vector3f64(0,0,0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct128Offset64() {
        try {
            return OctTreeBasic.newOctTree(new Vector3f64(128, 128, 128),
                    new Vector3f64(64, 64, 64));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct128OffsetM64() {
        try {
            return OctTreeBasic.newOctTree(new Vector3f64(128, 128, 128),
                    new Vector3f64(-64, -64, -64));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct16() {
        try {
            return OctTreeBasic.newOctTree(new Vector3f64(16, 16, 16),
                    new Vector3f64(0,0,0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2() {
        try {
            return OctTreeBasic.newOctTree(new Vector3f64(2, 2, 2), new Vector3f64(0,0,0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_2_4() {
        try {
            return OctTreeBasic.newOctTree(new Vector3f64(2, 2, 4), new Vector3f64(0,0,0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_4_2() {
        try {
            return OctTreeBasic.newOctTree(new Vector3f64(2, 4, 2), new Vector3f64(0,0,0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_4_4() {
        try {
            return OctTreeBasic.newOctTree(new Vector3f64(2, 4, 4), new Vector3f64(0,0,0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_2_2() {
        try {
            return OctTreeBasic.newOctTree(new Vector3f64(4, 2, 2), new Vector3f64(0,0,0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_2_4() {
        try {
            return OctTreeBasic.newOctTree(new Vector3f64(4, 2, 4), new Vector3f64(0,0,0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_4_2() {
        try {
            return OctTreeBasic.newOctTree(new Vector3f64(4, 4, 2), new Vector3f64(0,0,0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Test
    public void testCreateBasicOddX() {
        try {
            OctTreeBasic.newOctTree(new Vector3f64(3, 4, 4), new Vector3f64(0,0,0));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateBasicOddY() {
        try {
            OctTreeBasic.newOctTree(new Vector3f64(4, 3, 4), new Vector3f64(0,0,0));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateBasicOddZ() {
        try {
            OctTreeBasic.newOctTree(new Vector3f64(4, 4, 3), new Vector3f64(0,0,0));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateBasicTooSmallX() {
        try {
            OctTreeBasic.newOctTree(new Vector3f64(0, 4, 4), new Vector3f64(0,0,0));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateBasicTooSmallY() {
        try {
            OctTreeBasic.newOctTree(new Vector3f64(4, 0, 4), new Vector3f64(0,0,0));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateBasicTooSmallZ() {
        try {
            OctTreeBasic.newOctTree(new Vector3f64(4, 4, 0), new Vector3f64(0,0,0));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateOddX() {
        try {
            OctTreeBasic.newOctTree(new Vector3f64(3, 2, 2), new Vector3f64(0,0,0));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateOddY() {
        try {
            OctTreeBasic.newOctTree(new Vector3f64(2, 3, 2), new Vector3f64(0,0,0));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateOddZ() {
        try {
            OctTreeBasic.newOctTree(new Vector3f64(2, 2, 3), new Vector3f64(0,0,0));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public final void testInsertLeafNoSplit() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2();

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplit() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct128();

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(57, c.count);
    }

    @Test
    public final void testInsertSplitNot() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2();

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitXNotYNotZ() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct4_2_2();

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitXYNotZ() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct4_4_2();

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitXZNotY() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct4_2_4();

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitYNotZNotZ() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2_4_2();

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitYZNotX() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2_4_4();

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitZNotXNotY() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2_2_4();

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector3f64(0, 0, 0), new Vector3f64(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }
}
