package science.unlicense.geometry.api.index.utilities;

import science.unlicense.geometry.api.index.Cuboid;
import science.unlicense.geometry.api.index.Rectangle;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;

/**
 * @author Mark Raynsford
 */
public final class TestUtilities
{
  private static final Object z = null;

  public static <A> A actuallyNull() {
    return (A) TestUtilities.z;
  }

  public static Cuboid[] makeCuboids(
    final long id_first,
    final double container_size)
  {
    final Cuboid[] cubes = new Cuboid[8];
    final double half_size = container_size / 2.0;
    final double cube_size = half_size - 3;

    double x_root = 2;
    double y_root = 2;
    double z_root = 2;
    cubes[0] =
      new Cuboid(
        id_first,
        new Vector3f64(x_root, y_root, z_root),
        new Vector3f64(x_root + cube_size, y_root + cube_size, z_root
          + cube_size));

    x_root = half_size + 2;
    y_root = 2;
    z_root = 2;
    cubes[1] =
      new Cuboid(
        id_first + 1,
        new Vector3f64(x_root, y_root, z_root),
        new Vector3f64(x_root + cube_size, y_root + cube_size, z_root
          + cube_size));

    x_root = 2;
    y_root = half_size + 2;
    z_root = 2;
    cubes[2] =
      new Cuboid(
        id_first + 2,
        new Vector3f64(x_root, y_root, z_root),
        new Vector3f64(x_root + cube_size, y_root + cube_size, z_root
          + cube_size));

    x_root = half_size + 2;
    y_root = half_size + 2;
    z_root = 2;
    cubes[3] =
      new Cuboid(
        id_first + 3,
        new Vector3f64(x_root, y_root, z_root),
        new Vector3f64(x_root + cube_size, y_root + cube_size, z_root
          + cube_size));

    //
    // Upper Z
    //

    x_root = 2;
    y_root = 2;
    z_root = half_size + 2;
    cubes[4] =
      new Cuboid(
        id_first + 4,
        new Vector3f64(x_root, y_root, z_root),
        new Vector3f64(x_root + cube_size, y_root + cube_size, z_root
          + cube_size));

    x_root = half_size + 2;
    y_root = 2;
    z_root = half_size + 2;
    cubes[5] =
      new Cuboid(
        id_first + 5,
        new Vector3f64(x_root, y_root, z_root),
        new Vector3f64(x_root + cube_size, y_root + cube_size, z_root
          + cube_size));

    x_root = 2;
    y_root = half_size + 2;
    z_root = half_size + 2;
    cubes[6] =
      new Cuboid(
        id_first + 6,
        new Vector3f64(x_root, y_root, z_root),
        new Vector3f64(x_root + cube_size, y_root + cube_size, z_root
          + cube_size));

    x_root = half_size + 2;
    y_root = half_size + 2;
    z_root = half_size + 2;
    cubes[7] =
      new Cuboid(
        id_first + 7,
        new Vector3f64(x_root, y_root, z_root),
        new Vector3f64(x_root + cube_size, y_root + cube_size, z_root
          + cube_size));

    return cubes;
  }

  public static Rectangle[] makeRectangles(
    final long id_first,
    final double container_size)
  {
    final Rectangle[] rectangles = new Rectangle[4];
    final double half_size = container_size / 2.0;
    final double rectangle_size = half_size - 3;

    double x_root = 2;
    double y_root = 2;
    rectangles[0] =
      new Rectangle(id_first,
              new Vector2f64(x_root, y_root),
              new Vector2f64(x_root + rectangle_size,y_root + rectangle_size));

    x_root = half_size + 2;
    y_root = 2;
    rectangles[1] =
      new Rectangle(
        id_first + 1,
        new Vector2f64(x_root, y_root),
        new Vector2f64(x_root + rectangle_size, y_root + rectangle_size));

    x_root = 2;
    y_root = half_size + 2;
    rectangles[2] =
      new Rectangle(
        id_first + 2,
        new Vector2f64(x_root, y_root),
        new Vector2f64(x_root + rectangle_size, y_root + rectangle_size));

    x_root = half_size + 2;
    y_root = half_size + 2;
    rectangles[3] =
      new Rectangle(
        id_first + 3,
        new Vector2f64(x_root, y_root),
        new Vector2f64(x_root + rectangle_size, y_root + rectangle_size));

    return rectangles;
  }
}
