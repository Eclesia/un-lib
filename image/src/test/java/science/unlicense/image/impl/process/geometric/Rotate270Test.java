

package science.unlicense.image.impl.process.geometric;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector3f64;

/**
 * Rotate 270° operator test
 *
 * @author Johann Sorel
 */
public class Rotate270Test {

    @Test
    public void test2x2(){

        Image image = Images.create(new Extent.Long(2, 2),Images.IMAGE_TYPE_RGB);
        TupleGrid intb = image.getRawModel().asTupleBuffer(image);
        intb.setTuple(new Vector2i32(0,0), new Vector3f64(0,0,0));
        intb.setTuple(new Vector2i32(1,0), new Vector3f64(1,1,1));
        intb.setTuple(new Vector2i32(0,1), new Vector3f64(2,2,2));
        intb.setTuple(new Vector2i32(1,1), new Vector3f64(3,3,3));

        image = new Rotate270Operator().execute(image);
        intb = image.getRawModel().asTupleBuffer(image);
        TupleGridCursor cursor = intb.cursor();
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));
        cursor.moveTo(new Vector2i32(0,0)); Assert.assertArrayEquals(new int[]{1,1,1}, cursor.samples().toInt());
        cursor.moveTo(new Vector2i32(1,0)); Assert.assertArrayEquals(new int[]{3,3,3}, cursor.samples().toInt());
        cursor.moveTo(new Vector2i32(0,1)); Assert.assertArrayEquals(new int[]{0,0,0}, cursor.samples().toInt());
        cursor.moveTo(new Vector2i32(1,1)); Assert.assertArrayEquals(new int[]{2,2,2}, cursor.samples().toInt());
    }

    @Test
    public void test3x2(){

        Image image = Images.create(new Extent.Long(3, 2),Images.IMAGE_TYPE_RGB);
        TupleGrid intb = image.getRawModel().asTupleBuffer(image);
        intb.setTuple(new Vector2i32(0,0), new Vector3f64(0,0,0));
        intb.setTuple(new Vector2i32(1,0), new Vector3f64(1,1,1));
        intb.setTuple(new Vector2i32(2,0), new Vector3f64(2,2,2));
        intb.setTuple(new Vector2i32(0,1), new Vector3f64(3,3,3));
        intb.setTuple(new Vector2i32(1,1), new Vector3f64(4,4,4));
        intb.setTuple(new Vector2i32(2,1), new Vector3f64(5,5,5));

        image = new Rotate270Operator().execute(image);
        intb = image.getRawModel().asTupleBuffer(image);
        TupleGridCursor cursor = intb.cursor();
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(3, image.getExtent().getL(1));
        cursor.moveTo(new Vector2i32(0,0)); Assert.assertArrayEquals(new int[]{2,2,2}, cursor.samples().toInt());
        cursor.moveTo(new Vector2i32(0,1)); Assert.assertArrayEquals(new int[]{1,1,1}, cursor.samples().toInt());
        cursor.moveTo(new Vector2i32(0,2)); Assert.assertArrayEquals(new int[]{0,0,0}, cursor.samples().toInt());
        cursor.moveTo(new Vector2i32(1,0)); Assert.assertArrayEquals(new int[]{5,5,5}, cursor.samples().toInt());
        cursor.moveTo(new Vector2i32(1,1)); Assert.assertArrayEquals(new int[]{4,4,4}, cursor.samples().toInt());
        cursor.moveTo(new Vector2i32(1,2)); Assert.assertArrayEquals(new int[]{3,3,3}, cursor.samples().toInt());
    }

}
