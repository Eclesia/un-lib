
package science.unlicense.image.impl.process.analyze;

import org.junit.Test;
import static science.unlicense.common.api.Assert.*;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.MultiPolygon;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.TupleNf64;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class ContourOperatorTest {

    private static final double DELTA = 0;

    private static final Predicate MATCHER = new Predicate() {
        @Override
        public Boolean evaluate(Object candidate) {
            return ((Tuple) candidate).get(0) == 7;
        }
    };
    private static final Tuple VALID = new TupleNf64(new double[]{7});
    private static final Tuple INVALID = new TupleNf64(new double[]{5});

    /**
     * .
     */
    @Test
    public void testSingleFalsePixel() {

        final Image img = Images.createCustomBand(new Extent.Long(1, 1), 1, Int32.TYPE);
        img.getRawModel().asTupleBuffer(img).setTuple(new Vector2i32(0,0), INVALID);

        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, ImageModel.MODEL_RAW);
        assertNotNull(geom);
        assertEquals(0, geom.getGeometries().getSize());
    }

    /**
     * █
     */
    @Test
    public void testSingleTruePixel() {

        final Image img = Images.createCustomBand(new Extent.Long(1, 1), 1, Int32.TYPE);
        img.getRawModel().asTupleBuffer(img).setTuple(new Vector2i32(0,0), VALID);

        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, ImageModel.MODEL_RAW);
        assertNotNull(geom);
        assertEquals(1, geom.getGeometries().getSize());
        final Polygon polygon = (Polygon) geom.getGeometries().get(0);
        final Polyline exterior = polygon.getExterior();
        assertNotNull(exterior);
        assertNull(polygon.getInteriors());

        final TupleGrid1D coordinates = exterior.getCoordinates();
        assertEquals(5l, coordinates.getExtent().getL(0));

        VectorRW tuple = new Vector2f64();
        coordinates.getTuple(0, tuple);
        assertEquals(new Vector2f64(1,1), tuple);
        coordinates.getTuple(1, tuple);
        assertEquals(new Vector2f64(0,1), tuple);
        coordinates.getTuple(2, tuple);
        assertEquals(new Vector2f64(0,0), tuple);
        coordinates.getTuple(3, tuple);
        assertEquals(new Vector2f64(1,0), tuple);
        coordinates.getTuple(4, tuple);
        assertEquals(new Vector2f64(1,1), tuple);
    }

    /**
     * ██
     * ██
     */
    @Test
    public void testSquare() {

        final Image img = Images.createCustomBand(new Extent.Long(2, 2), 1, Int32.TYPE);
        final TupleGrid tb = img.getRawModel().asTupleBuffer(img);
        tb.setTuple(new Vector2i32(0,0), VALID);
        tb.setTuple(new Vector2i32(1,0), VALID);
        tb.setTuple(new Vector2i32(1,1), VALID);
        tb.setTuple(new Vector2i32(0,1), VALID);

        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, ImageModel.MODEL_RAW);
        assertNotNull(geom);
        assertEquals(1, geom.getGeometries().getSize());
        final Polygon polygon = (Polygon) geom.getGeometries().get(0);
        final Polyline exterior = polygon.getExterior();
        assertNotNull(exterior);
        assertNull(polygon.getInteriors());

        final TupleGrid1D coordinates = exterior.getCoordinates();
        assertEquals(5l, coordinates.getExtent().getL(0));

        VectorRW tuple = new Vector2f64();
        coordinates.getTuple(0, tuple);
        assertEquals(new Vector2f64(2,2), tuple);
        coordinates.getTuple(1, tuple);
        assertEquals(new Vector2f64(0,2), tuple);
        coordinates.getTuple(2, tuple);
        assertEquals(new Vector2f64(0,0), tuple);
        coordinates.getTuple(3, tuple);
        assertEquals(new Vector2f64(2,0), tuple);
        coordinates.getTuple(4, tuple);
        assertEquals(new Vector2f64(2,2), tuple);
    }

    /**
     * ██
     * █.
     */
    @Test
    public void testAngle1() {

        final Image img = Images.createCustomBand(new Extent.Long(2, 2), 1, Int32.TYPE);
        final TupleGrid tb = img.getRawModel().asTupleBuffer(img);
        tb.setTuple(new Vector2i32(0,0), VALID);
        tb.setTuple(new Vector2i32(1,0), VALID);
        tb.setTuple(new Vector2i32(1,1), INVALID);
        tb.setTuple(new Vector2i32(0,1), VALID);

        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, ImageModel.MODEL_RAW);
        assertNotNull(geom);
        assertEquals(1, geom.getGeometries().getSize());
        final Polygon polygon = (Polygon) geom.getGeometries().get(0);
        final Polyline exterior = polygon.getExterior();
        assertNotNull(exterior);
        assertNull(polygon.getInteriors());

        final TupleGrid1D coordinates = exterior.getCoordinates();
        assertEquals(7l, coordinates.getExtent().getL(0));
        final double[] coords = Geometries.toFloat64(coordinates);

        final double[] expected = {
            1,2,
            0,2,
            0,0,
            2,0,
            2,1,
            1,1,
            1,2};

        assertArrayEquals(expected, coords, DELTA);
    }

    /**
     * .█
     * ██
     */
    @Test
    public void testAngle2() {

        final Image img = Images.createCustomBand(new Extent.Long(2, 2), 1, Int32.TYPE);
        final TupleGrid tb = img.getRawModel().asTupleBuffer(img);
        tb.setTuple(new Vector2i32(0,0), INVALID);
        tb.setTuple(new Vector2i32(1,0), VALID);
        tb.setTuple(new Vector2i32(1,1), VALID);
        tb.setTuple(new Vector2i32(0,1), VALID);

        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, ImageModel.MODEL_RAW);
        assertNotNull(geom);
        assertEquals(1, geom.getGeometries().getSize());
        final Polygon polygon = (Polygon) geom.getGeometries().get(0);
        final Polyline exterior = polygon.getExterior();
        assertNotNull(exterior);
        assertNull(polygon.getInteriors());

        final TupleGrid1D coordinates = exterior.getCoordinates();
        assertEquals(7l, coordinates.getExtent().getL(0));
        final double[] coords = Geometries.toFloat64(coordinates);

        final double[] expected = {
            2,2,
            0,2,
            0,1,
            1,1,
            1,0,
            2,0,
            2,2};

        assertArrayEquals(expected, coords, DELTA);
    }

    /**
     * .█
     * █.
     */
    @Test
    public void testMedian1() {

        final Image img = Images.createCustomBand(new Extent.Long(2, 2), 1, Int32.TYPE);
        final TupleGrid tb = img.getRawModel().asTupleBuffer(img);
        tb.setTuple(new Vector2i32(0,0), INVALID);
        tb.setTuple(new Vector2i32(1,0), VALID);
        tb.setTuple(new Vector2i32(1,1), INVALID);
        tb.setTuple(new Vector2i32(0,1), VALID);

        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, ImageModel.MODEL_RAW);
        assertNotNull(geom);
        assertEquals(2, geom.getGeometries().getSize());
        final Polygon poly1 = (Polygon) geom.getGeometries().get(0);
        final Polygon poly2 = (Polygon) geom.getGeometries().get(1);
        assertNull(poly1.getInteriors());
        assertNull(poly2.getInteriors());

        final double[] coords1 = Geometries.toFloat64(poly1.getExterior().getCoordinates());
        final double[] coords2 = Geometries.toFloat64(poly2.getExterior().getCoordinates());

        final double[] exp1 = {
            2,1,
            1,1,
            1,0,
            2,0,
            2,1};
        assertArrayEquals(exp1, coords1, DELTA);

        final double[] exp2 = {
            1,2,
            0,2,
            0,1,
            1,1,
            1,2};
        assertArrayEquals(exp2, coords2, DELTA);
    }

    /**
     * █.
     * .█
     */
    @Test
    public void testMedian2() {

        final Image img = Images.createCustomBand(new Extent.Long(2, 2), 1, Int32.TYPE);
        final TupleGrid tb = img.getRawModel().asTupleBuffer(img);
        tb.setTuple(new Vector2i32(0,0), VALID);
        tb.setTuple(new Vector2i32(1,0), INVALID);
        tb.setTuple(new Vector2i32(1,1), VALID);
        tb.setTuple(new Vector2i32(0,1), INVALID);

        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, ImageModel.MODEL_RAW);
        assertNotNull(geom);
        assertEquals(2, geom.getGeometries().getSize());
        final Polygon poly1 = (Polygon) geom.getGeometries().get(0);
        final Polygon poly2 = (Polygon) geom.getGeometries().get(1);
        assertNull(poly1.getInteriors());
        assertNull(poly2.getInteriors());

        final double[] coords1 = Geometries.toFloat64(poly1.getExterior().getCoordinates());
        final double[] coords2 = Geometries.toFloat64(poly2.getExterior().getCoordinates());

        final double[] exp1 = {
            1,1,
            0,1,
            0,0,
            1,0,
            1,1};
        assertArrayEquals(exp1, coords1, DELTA);

        final double[] exp2 = {
            2,2,
            1,2,
            1,1,
            2,1,
            2,2};
        assertArrayEquals(exp2, coords2, DELTA);
    }

}
