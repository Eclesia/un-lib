
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Assert;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.math.api.transform.Transform;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractColorSpaceTest {

    private static final double DELTA = 0.0001;

    protected static void testEqual(ColorSpace sourceCs, ColorSpace targetCs, double[] sourceValues, double[] targetValues) {

        final Transform tranform = sourceCs.getTranform(targetCs);

        // test with double precision
        final double[] resd = new double[tranform.getOutputDimensions()];
        tranform.transform(sourceValues, 0, resd, 0, 1);
        Assert.assertArrayEquals(targetValues, resd, DELTA);

        // test with float precision
        final float[] srcf = Arrays.reformatToFloat(sourceValues);
        final float[] tgtf = Arrays.reformatToFloat(targetValues);
        final float[] resf = new float[tranform.getOutputDimensions()];
        tranform.transform(srcf, 0, resf, 0, 1);
        Assert.assertArrayEquals(tgtf, resf, (float)DELTA);


    }

}
