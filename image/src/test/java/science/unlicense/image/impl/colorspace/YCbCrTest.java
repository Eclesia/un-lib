
package science.unlicense.image.impl.colorspace;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.image.api.color.Colors;

/**
 *
 * @author Johann Sorel
 */
public class YCbCrTest extends AbstractColorSpaceTest {

    private static final float DELTA = 0.0000001f;

    /**
     * Check YCbCr to RGB conversion.
     * TODO BUG
     */
    @Ignore
    @Test
    public void testYCbCrToRGB(){
        final float[] ycc = new float[]{1f,0.5f,0.5f};
        final float[] rgb = Colors.YCbCrtoRGB(ycc);
        Assert.assertEquals(1f, rgb[0], DELTA);
        Assert.assertEquals(1f, rgb[1], DELTA);
        Assert.assertEquals(1f, rgb[2], DELTA);
    }

    @Test
    public void testYCbCrToRGBInt(){
        final int[] ycc = new int[]{255,128,128};
        final int[] rgb = Colors.YCbCrtoRGB(ycc);
        Assert.assertEquals(255f, rgb[0], DELTA);
        Assert.assertEquals(255f, rgb[1], DELTA);
        Assert.assertEquals(255f, rgb[2], DELTA);
    }

}
