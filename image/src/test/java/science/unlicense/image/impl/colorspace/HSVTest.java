
package science.unlicense.image.impl.colorspace;

import org.junit.Test;
import science.unlicense.image.api.colorspace.ColorSpace;
import static science.unlicense.image.impl.colorspace.AbstractColorSpaceTest.testEqual;

/**
 *
 * @author Johann Sorel
 */
public class HSVTest extends AbstractColorSpaceTest {

    private static final ColorSpace HSV = science.unlicense.image.impl.colorspace.HSV.INSTANCE;
    private static final ColorSpace SRGB = science.unlicense.image.impl.colorspace.SRGB.INSTANCE;

    @Test
    public void testToRGB() {
        testEqual(HSV, SRGB, new double[]{  0,0,0}, new double[]{0,0,0});
        testEqual(HSV, SRGB, new double[]{  0,1,1}, new double[]{1,0,0});
        testEqual(HSV, SRGB, new double[]{120,1,1}, new double[]{0,1,0});
        testEqual(HSV, SRGB, new double[]{240,1,1}, new double[]{0,0,1});
        testEqual(HSV, SRGB, new double[]{  0,0,1}, new double[]{1,1,1});
    }

    @Test
    public void testToSpace() {
        testEqual(SRGB, HSV, new double[]{0,0,0}, new double[]{  0,0,0});
        testEqual(SRGB, HSV, new double[]{1,0,0}, new double[]{  0,1,1});
        testEqual(SRGB, HSV, new double[]{0,1,0}, new double[]{120,1,1});
        testEqual(SRGB, HSV, new double[]{0,0,1}, new double[]{240,1,1});
        testEqual(SRGB, HSV, new double[]{1,1,1}, new double[]{  0,0,1});
    }
}

