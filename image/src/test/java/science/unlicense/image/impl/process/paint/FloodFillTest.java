
package science.unlicense.image.impl.process.paint;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.predicate.AbstractPredicate;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Scalarf64;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class FloodFillTest {

    /**
     * Check a full flood fill.
     */
    @Test
    public void testAllMatch(){

        final Image image = Images.createCustomBand(new Extent.Long(10, 10), 1, Int32.TYPE);
        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        sm.setTuple(new BBox(new double[]{0,0}, new double[]{10,10}), new VectorNf64(new double[]{16}));

        final FloodFillOperator op = new FloodFillOperator();
        op.execute(image, new Vector2f64(5, 5), new AbstractPredicate() {
            public Boolean evaluate(Object candidate) {
                return ((Tuple) candidate).get(0) == 16;
            }
        }, new Scalarf64(21));

        final TupleRW sample = sm.createTuple();
        for (int y=0;y<10;y++){
            for (int x=0;x<10;x++){
                sm.getTuple(new Vector2i32(x,y), sample);
                Assert.assertEquals(21,(int)sample.get(0),x+","+y);
            }
        }

    }

    /**
     * Check a square flood fill.
     */
    @Test
    public void testSquareMatch(){

        final Image image = Images.createCustomBand(new Extent.Long(10, 10), 1, Int32.TYPE);
        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        sm.setTuple(new BBox(new double[]{2,3}, new double[]{9,7}), new VectorNf64(new double[]{16}));

        final FloodFillOperator op = new FloodFillOperator();
        op.execute(image, new Vector2f64(5, 5), new AbstractPredicate() {
            public Boolean evaluate(Object candidate) {
                return ((Tuple) candidate).get(0) == 16;
            }
        }, new Scalarf64(21));

        final TupleRW sample = sm.createTuple();
        for (int y=0;y<10;y++){
            for (int x=0;x<10;x++){
                sm.getTuple(new Vector2i32(x,y), sample);
                if (x<2||x>=9||y<3||y>=7){
                    Assert.assertEquals(0,(int)sample.get(0),x+","+y);
                } else {
                    Assert.assertEquals(21,(int)sample.get(0),x+","+y);
                }
            }
        }

    }


}
