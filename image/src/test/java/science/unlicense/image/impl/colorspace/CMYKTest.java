
package science.unlicense.image.impl.colorspace;

import org.junit.Test;
import science.unlicense.image.api.colorspace.ColorSpace;

/**
 *
 * @author Johann Sorel
 */
public class CMYKTest extends AbstractColorSpaceTest {

    private static final ColorSpace CMYK = science.unlicense.image.impl.colorspace.CMYK.INSTANCE;
    private static final ColorSpace SRGB = science.unlicense.image.impl.colorspace.SRGB.INSTANCE;

    @Test
    public void testToRGB() {

        testEqual(CMYK, SRGB, new double[]{0.0, 0.0, 0.0, 0.0}, new double[]{1.0, 1.0, 1.0});
        testEqual(CMYK, SRGB, new double[]{1.0, 0.0, 0.0, 0.0}, new double[]{0.0, 1.0, 1.0});
        testEqual(CMYK, SRGB, new double[]{0.0, 1.0, 0.0, 0.0}, new double[]{1.0, 0.0, 1.0});
        testEqual(CMYK, SRGB, new double[]{0.0, 0.0, 1.0, 0.0}, new double[]{1.0, 1.0, 0.0});
        //various vays to write black
        testEqual(CMYK, SRGB, new double[]{0.0, 0.0, 0.0, 1.0}, new double[]{0.0, 0.0, 0.0});
        testEqual(CMYK, SRGB, new double[]{1.0, 1.0, 1.0, 1.0}, new double[]{0.0, 0.0, 0.0});
        testEqual(CMYK, SRGB, new double[]{1.0, 1.0, 1.0, 0.0}, new double[]{0.0, 0.0, 0.0});
        //random colors
        testEqual(CMYK, SRGB, new double[]{0.2, 0.6, 0.3, 0.5}, new double[]{0.4, 0.2, 0.35});
        testEqual(CMYK, SRGB, new double[]{0.0, 0.5, 0.125,0.6}, new double[]{0.4, 0.2, 0.35});

    }

    @Test
    public void testToSpace() {

        testEqual(SRGB, CMYK, new double[]{1.0, 1.0, 1.0}, new double[]{0.0, 0.0, 0.0, 0.0});
        testEqual(SRGB, CMYK, new double[]{0.0, 1.0, 1.0}, new double[]{1.0, 0.0, 0.0, 0.0});
        testEqual(SRGB, CMYK, new double[]{1.0, 0.0, 1.0}, new double[]{0.0, 1.0, 0.0, 0.0});
        testEqual(SRGB, CMYK, new double[]{1.0, 1.0, 0.0}, new double[]{0.0, 0.0, 1.0, 0.0});
        //black
        testEqual(SRGB, CMYK, new double[]{0.0, 0.0, 0.0}, new double[]{0.0, 0.0, 0.0, 1.0});
        //random colors
        testEqual(SRGB, CMYK, new double[]{0.4, 0.2, 0.35}, new double[]{0.0, 0.5, 0.125, 0.6});

    }
}
