
package science.unlicense.image.api.color;

import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 * ColorRGBA tests.
 *
 * @author Johann Sorel
 */
public class ColorRGBATest {

    private static final double DELTA = 0.000001;
    private static final double DELTAI = 0.02;
    private static final float VR = 200f/255f;
    private static final float VG = 150f/255f;
    private static final float VB = 100f/255f;
    private static final float VA =  50f/255f;

    @Test
    public void testContructorInt(){
        Color color;

        color = new ColorRGB(200, 150, 100);
        Assert.assertEquals(VR, color.getRed(),    DELTA);
        Assert.assertEquals(VG, color.getGreen(),  DELTA);
        Assert.assertEquals(VB, color.getBlue(),   DELTA);
        Assert.assertEquals(1f, color.getAlpha(),  DELTA);

        color = new ColorRGB(200, 150, 100, 50);
        Assert.assertEquals(VR, color.getRed(),    DELTA);
        Assert.assertEquals(VG, color.getGreen(),  DELTA);
        Assert.assertEquals(VB, color.getBlue(),   DELTA);
        Assert.assertEquals(VA, color.getAlpha(),  DELTA);

        color = new ColorRGB(200, 150, 100, 50, false);
        Assert.assertEquals(VR, color.getRed(),    DELTA);
        Assert.assertEquals(VG, color.getGreen(),  DELTA);
        Assert.assertEquals(VB, color.getBlue(),   DELTA);
        Assert.assertEquals(VA, color.getAlpha(),  DELTA);

        color = new ColorRGB((int) (200*VA), (int) (150*VA), (int) (100*VA), 50, true);
        Assert.assertEquals(VR, color.getRed(),    DELTAI);
        Assert.assertEquals(VG, color.getGreen(),  DELTAI);
        Assert.assertEquals(VB, color.getBlue(),   DELTAI);
        Assert.assertEquals(VA, color.getAlpha(),  DELTA);

        color = new ColorRGB(Colors.toARGB(50, 200, 150, 100));
        Assert.assertEquals(VR, color.getRed(),    DELTA);
        Assert.assertEquals(VG, color.getGreen(),  DELTA);
        Assert.assertEquals(VB, color.getBlue(),   DELTA);
        Assert.assertEquals(VA, color.getAlpha(),  DELTA);
    }

    @Test
    public void testContructorFloat(){
        Color color;

        color = new ColorRGB(VR, VG, VB);
        Assert.assertEquals(VR, color.getRed(),    DELTA);
        Assert.assertEquals(VG, color.getGreen(),  DELTA);
        Assert.assertEquals(VB, color.getBlue(),   DELTA);
        Assert.assertEquals(1f, color.getAlpha(),  DELTA);

        color = new ColorRGB(VR, VG, VB, VA);
        Assert.assertEquals(VR, color.getRed(),    DELTA);
        Assert.assertEquals(VG, color.getGreen(),  DELTA);
        Assert.assertEquals(VB, color.getBlue(),   DELTA);
        Assert.assertEquals(VA, color.getAlpha(),  DELTA);

        color = new ColorRGB(VR, VG, VB, VA, false);
        Assert.assertEquals(VR, color.getRed(),    DELTA);
        Assert.assertEquals(VG, color.getGreen(),  DELTA);
        Assert.assertEquals(VB, color.getBlue(),   DELTA);
        Assert.assertEquals(VA, color.getAlpha(),  DELTA);

        color = new ColorRGB(VR*VA, VG*VA, VB*VA, VA, true);
        Assert.assertEquals(VR, color.getRed(),    DELTA);
        Assert.assertEquals(VG, color.getGreen(),  DELTA);
        Assert.assertEquals(VB, color.getBlue(),   DELTA);
        Assert.assertEquals(VA, color.getAlpha(),  DELTA);

    }

}
