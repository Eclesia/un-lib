
package science.unlicense.image.api.process;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.number.Int8;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class ExtrapolatorTest {

    private static final double DELTA = 0.000001;

    @Test
    public void testConstant() {

        final Buffer buffer = DefaultBufferFactory.wrap(new byte[]{
            0,1,2,
            3,4,5,
            6,7,8});
        final TupleGrid grid = InterleavedTupleGrid.create(buffer, Int8.TYPE, UndefinedSystem.create(1), new Extent.Long(3, 3));

        final TupleSpace extrapolator = new ExtrapolatorConstant(grid, 100);

        /*
        X X X X X
        X 0 1 2 X
        X 3 4 5 X
        X 6 7 8 X
        X X X X X
        */
        //check inner image values are unchanged
        tupleEquals(0, 0, 0, extrapolator);
        tupleEquals(1, 0, 1, extrapolator);
        tupleEquals(2, 0, 2, extrapolator);
        tupleEquals(0, 1, 3, extrapolator);
        tupleEquals(1, 1, 4, extrapolator);
        tupleEquals(2, 1, 5, extrapolator);
        tupleEquals(0, 2, 6, extrapolator);
        tupleEquals(1, 2, 7, extrapolator);
        tupleEquals(2, 2, 8, extrapolator);
        //check out of image extrapolation
        //left
        tupleEquals(-1, -1, 100, extrapolator);
        tupleEquals(-1,  0, 100, extrapolator);
        tupleEquals(-1,  1, 100, extrapolator);
        tupleEquals(-1,  2, 100, extrapolator);
        tupleEquals(-1,  3, 100, extrapolator);
        //right
        tupleEquals(3, -1, 100, extrapolator);
        tupleEquals(3,  0, 100, extrapolator);
        tupleEquals(3,  1, 100, extrapolator);
        tupleEquals(3,  2, 100, extrapolator);
        tupleEquals(3,  3, 100, extrapolator);
        //top
        tupleEquals(0, -1, 100, extrapolator);
        tupleEquals(1, -1, 100, extrapolator);
        tupleEquals(2, -1, 100, extrapolator);
        //bottom
        tupleEquals(0, 3, 100, extrapolator);
        tupleEquals(1, 3, 100, extrapolator);
        tupleEquals(2, 3, 100, extrapolator);
    }

    @Test
    public void testCopy() {

        final Buffer buffer = DefaultBufferFactory.wrap(new byte[]{
            0,1,2,
            3,4,5,
            6,7,8});
        final TupleGrid grid = InterleavedTupleGrid.create(buffer, Int8.TYPE, UndefinedSystem.create(1), new Extent.Long(3, 3));

        final TupleSpace extrapolator = new ExtrapolatorCopy(grid);

        /*
        0 0 1 2 2
        0 0 1 2 2
        3 3 4 5 5
        6 6 7 8 8
        6 6 7 8 8
        */
        //check inner image values are unchanged
        tupleEquals(0, 0, 0, extrapolator);
        tupleEquals(1, 0, 1, extrapolator);
        tupleEquals(2, 0, 2, extrapolator);
        tupleEquals(0, 1, 3, extrapolator);
        tupleEquals(1, 1, 4, extrapolator);
        tupleEquals(2, 1, 5, extrapolator);
        tupleEquals(0, 2, 6, extrapolator);
        tupleEquals(1, 2, 7, extrapolator);
        tupleEquals(2, 2, 8, extrapolator);
        //check out of image extrapolation
        //left
        tupleEquals(-1, -1, 0, extrapolator);
        tupleEquals(-1,  0, 0, extrapolator);
        tupleEquals(-1,  1, 3, extrapolator);
        tupleEquals(-1,  2, 6, extrapolator);
        tupleEquals(-1,  3, 6, extrapolator);
        //right
        tupleEquals(3, -1, 2, extrapolator);
        tupleEquals(3,  0, 2, extrapolator);
        tupleEquals(3,  1, 5, extrapolator);
        tupleEquals(3,  2, 8, extrapolator);
        tupleEquals(3,  3, 8, extrapolator);
        //top
        tupleEquals(0, -1, 0, extrapolator);
        tupleEquals(1, -1, 1, extrapolator);
        tupleEquals(2, -1, 2, extrapolator);
        //bottom
        tupleEquals(0, 3, 6, extrapolator);
        tupleEquals(1, 3, 7, extrapolator);
        tupleEquals(2, 3, 8, extrapolator);
    }

    @Test
    public void testWrap() {

        final Buffer buffer = DefaultBufferFactory.wrap(new byte[]{
            0,1,2,
            3,4,5,
            6,7,8});
        final TupleGrid grid = InterleavedTupleGrid.create(buffer, Int8.TYPE, UndefinedSystem.create(1), new Extent.Long(3, 3));

        final TupleSpace extrapolator = new ExtrapolatorWrap(grid);

        /*
        8 6 7 8 6
        2 0 1 2 0
        5 3 4 5 3
        8 6 7 8 6
        2 0 1 2 0
        */
        //check inner image values are unchanged
        tupleEquals(0, 0, 0, extrapolator);
        tupleEquals(1, 0, 1, extrapolator);
        tupleEquals(2, 0, 2, extrapolator);
        tupleEquals(0, 1, 3, extrapolator);
        tupleEquals(1, 1, 4, extrapolator);
        tupleEquals(2, 1, 5, extrapolator);
        tupleEquals(0, 2, 6, extrapolator);
        tupleEquals(1, 2, 7, extrapolator);
        tupleEquals(2, 2, 8, extrapolator);
        //check out of image extrapolation
        //left
        tupleEquals(-1, -1, 8, extrapolator);
        tupleEquals(-1,  0, 2, extrapolator);
        tupleEquals(-1,  1, 5, extrapolator);
        tupleEquals(-1,  2, 8, extrapolator);
        tupleEquals(-1,  3, 2, extrapolator);
        //right
        tupleEquals(3, -1, 6, extrapolator);
        tupleEquals(3,  0, 0, extrapolator);
        tupleEquals(3,  1, 3, extrapolator);
        tupleEquals(3,  2, 6, extrapolator);
        tupleEquals(3,  3, 0, extrapolator);
        //top
        tupleEquals(0, -1, 6, extrapolator);
        tupleEquals(1, -1, 7, extrapolator);
        tupleEquals(2, -1, 8, extrapolator);
        //bottom
        tupleEquals(0, 3, 0, extrapolator);
        tupleEquals(1, 3, 1, extrapolator);
        tupleEquals(2, 3, 2, extrapolator);


        tupleEquals(0, -1, 6, extrapolator);
        tupleEquals(0, -2, 3, extrapolator);
        tupleEquals(0, -3, 0, extrapolator);
        tupleEquals(0, -4, 6, extrapolator);
        tupleEquals(0, -5, 3, extrapolator);
        tupleEquals(0, -6, 0, extrapolator);


    }

    private static void tupleEquals(double x, double y, double expected, TupleSpace space) {

        final TupleRW tuple = space.createTuple();
        space.getTuple(new Vector2f64(x,y), tuple);
        double value = tuple.get(0);

        Assert.assertEquals(expected, value, DELTA);

    }

}
