
package science.unlicense.image.api.color;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.image.impl.colorspace.HSV;

/**
 * Test color methods.
 *
 * @author Johann Sorel
 */
public class ColorTest {

    private static final float DELTA = 0.0000001f;

    /**
     * Check float array to ARGB conversion.
     */
    @Test
    public void testFloatToARGB(){
        final float[] rgba = new float[]{1f,0f,0f,1f};
        final Color color = new ColorRGB(rgba);

        int argb = color.toARGB();
        int a = (argb >>> 24) & 0xFF;
        int r = (argb >>> 16) & 0xFF;
        int g = (argb >>>  8) & 0xFF;
        int b = (argb >>>  0) & 0xFF;
        Assert.assertEquals(255, a);
        Assert.assertEquals(255, r);
        Assert.assertEquals(0, g);
        Assert.assertEquals(0, b);
    }

    /**
     * Check ARGB to float array conversion.
     */
    @Test
    public void testARGBToFloat(){
        int argb = -65536; //red
        final Color color = new ColorRGB(argb);
        Assert.assertEquals(1f, color.getAlpha(), DELTA);
        Assert.assertEquals(1f, color.getRed(), DELTA);
        Assert.assertEquals(0f, color.getGreen(), DELTA);
        Assert.assertEquals(0f, color.getBlue(), DELTA);
    }

    @Test
    public void testC4toC8(){
        Assert.assertEquals(0,     Colors.C4toC8(0));
        Assert.assertEquals(255,   Colors.C4toC8(15));
    }

    @Test
    public void testC5toC8(){
        Assert.assertEquals(0,     Colors.C5toC8(0));
        Assert.assertEquals(255,   Colors.C5toC8(31));
    }

    @Test
    public void testC6toC8(){
        Assert.assertEquals(0,     Colors.C6toC8(0));
        Assert.assertEquals(255,   Colors.C6toC8(63));
    }

}
