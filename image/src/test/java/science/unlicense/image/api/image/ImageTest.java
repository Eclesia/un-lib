
package science.unlicense.image.api.image;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.number.Bits;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class ImageTest {

    @Test
    public void testBitMaskImage(){

        final Image image = Images.createCustomBand(new Extent.Long(100, 40), 1, Bits.TYPE_1_BIT);
        Assert.assertNotNull(image);
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(100, image.getExtent().getL(0));
        Assert.assertEquals(40, image.getExtent().getL(1));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final Vector2i32 coordinate = new Vector2i32();
        final TupleRW storage = sm.createTuple();

        coordinate.x = 0; coordinate.y = 0;
        sm.getTuple(coordinate, storage);
        Assert.assertTrue(storage.get(0)==0.0);

        coordinate.x = 10; coordinate.y = 20;
        sm.getTuple(coordinate, storage);
        Assert.assertTrue(storage.get(0)==0.0);

        storage.set(0, 1);
        sm.setTuple(coordinate, storage);
        sm.getTuple(coordinate, storage);
        Assert.assertFalse(storage.get(0)==0.0);

    }

}
