
package science.unlicense.image.api.image.sample;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultInt8Buffer;
import science.unlicense.common.api.number.Bits;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Vector2i32;

/**
 * Test interleaved image sampel model.
 *
 * @author Johann Sorel
 */
public class InterleavedTest {

    @Test
    public void test1Bit1Band(){

        final byte[] data = new byte[]{(byte) 0xAA,0x55};
        final Buffer bank = new DefaultInt8Buffer(data);
        final InterleavedModel sm = new InterleavedModel(new UndefinedSystem(1), Bits.TYPE_1_BIT);
        final Image image = new DefaultImage(bank, new Extent.Long(8, 2), sm);
        final TupleGrid tb = sm.asTupleBuffer(image);
        final TupleGridCursor cursor = tb.cursor();

        cursor.moveTo(new Vector2i32(0,0)); Assert.assertEquals(1, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(1,0)); Assert.assertEquals(0, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(2,0)); Assert.assertEquals(1, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(3,0)); Assert.assertEquals(0, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(4,0)); Assert.assertEquals(1, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(5,0)); Assert.assertEquals(0, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(6,0)); Assert.assertEquals(1, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(7,0)); Assert.assertEquals(0, (int)cursor.samples().get(0));

        cursor.moveTo(new Vector2i32(0,1)); Assert.assertEquals(0, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(1,1)); Assert.assertEquals(1, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(2,1)); Assert.assertEquals(0, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(3,1)); Assert.assertEquals(1, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(4,1)); Assert.assertEquals(0, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(5,1)); Assert.assertEquals(1, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(6,1)); Assert.assertEquals(0, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(7,1)); Assert.assertEquals(1, (int)cursor.samples().get(0));
    }

    @Test
    public void test2Bit1Band(){

        final byte[] data = new byte[]{(byte) 0x1B,(byte) 0xE4};
        final Buffer bank = new DefaultInt8Buffer(data);
        final InterleavedModel sm = new InterleavedModel(new UndefinedSystem(1),Bits.TYPE_2_BIT);
        final Image image = new DefaultImage(bank, new Extent.Long(4, 2), sm);
        final TupleGrid tb = sm.asTupleBuffer(image);
        final TupleGridCursor cursor = tb.cursor();

        cursor.moveTo(new Vector2i32(0,0)); Assert.assertEquals(0, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(1,0)); Assert.assertEquals(1, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(2,0)); Assert.assertEquals(2, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(3,0)); Assert.assertEquals(3, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(0,1)); Assert.assertEquals(3, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(1,1)); Assert.assertEquals(2, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(2,1)); Assert.assertEquals(1, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(3,1)); Assert.assertEquals(0, (int)cursor.samples().get(0));
    }

    @Test
    public void test4Bit1Band(){

        final byte[] data = new byte[]{(byte) 0x52,(byte) 0xC1};
        final Buffer bank = new DefaultInt8Buffer(data);
        final InterleavedModel sm = new InterleavedModel(new UndefinedSystem(1), Bits.TYPE_4_BIT);
        final Image image = new DefaultImage(bank, new Extent.Long(2, 2), sm);
        final TupleGrid tb = sm.asTupleBuffer(image);
        final TupleGridCursor cursor = tb.cursor();

        cursor.moveTo(new Vector2i32(0,0)); Assert.assertEquals(5, (int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(1,0)); Assert.assertEquals(2, (int)cursor.samples().get(0));

        cursor.moveTo(new Vector2i32(0,1)); Assert.assertEquals(12,(int)cursor.samples().get(0));
        cursor.moveTo(new Vector2i32(1,1)); Assert.assertEquals(1, (int)cursor.samples().get(0));
    }

}
