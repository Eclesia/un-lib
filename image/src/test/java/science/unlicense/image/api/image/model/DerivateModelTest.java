
package science.unlicense.image.api.image.model;

import org.junit.Test;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public class DerivateModelTest {

    @Test
    public void mappingTest() {

        final int[] data = new int[]{0,1,2,3,4,5};
        final Extent.Long extent = new Extent.Long(2, 0);

        //final Image image = new DefaultImage(new DefaultIntBuffer(data), extent, null );


    }


//    @Test
//    public void testRGB565(){
//
//        final Extent.Long ext = new Extent.Long(1, 4);
//        final Buffer buffer = DefaultBufferFactory.create(8, NumericType.TYPE_UBYTE, Endianness.BIG_ENDIAN);
//        buffer.writeUShort(0, 0); //0,0,0
//        buffer.writeUShort(0xFFFF, 2);//1,1,1
//        buffer.writeUShort(0x07E0, 4);//0,1,0
//        buffer.writeUShort(0xF81F, 6);//1,0,1
//
//        final ImageModel rm = new InterleavedModel(new DefaultSampleSystem(1),NumericType.TYPE_USHORT);
//        final DerivateModel cm = new DerivateModel(rm,new int[]{0,5,11}, new int[]{5,6,5});
//        final Image image = new DefaultImage(buffer, ext, rm, cm);
//
//        final TupleBuffer pb = image.getColorModel().asTupleBuffer(image);
//        Assert.assertEquals(new ColorRGB(0, 0, 0),pb.getColor(new int[]{0,0}));
//        Assert.assertEquals(new ColorRGB(1f, 1f, 1f),pb.getColor(new int[]{0,1}));
//        Assert.assertEquals(new ColorRGB(0f, 1f, 0f),pb.getColor(new int[]{0,2}));
//        Assert.assertEquals(new ColorRGB(1f, 0f, 1f),pb.getColor(new int[]{0,3}));
//
//    }

}
