
package science.unlicense.image.api.color;

/**
 * Blending is a technique to merge two colors.
 * The most known blending and AlphaBlending using Porter-Duff rules.
 *
 * @author Johann Sorel
 */
public interface Blending {

    /**
     * Blend colors.
     *
     * @param src Source color.
     * @param dst Destination color.
     * @return blended color
     */
    Color blend(Color src, Color dst);

    /**
     * Blend tuple.
     *
     * @param src Source tuple.
     * @param dst Destination tuple.
     * @param buffer result storeage buffer.
     * @return blended tuple
     */
    //TupleRW blend(Tuple src, Tuple dst, TupleRW buffer);

    /**
     * Blend ARGB colors.
     * Alpha not premultiplied in source and destination.
     *
     * @param src Source color.
     * @param dst Destination color.
     * @return blended color
     */
    int blend(int src, int dst);

    /**
     * Blend ARGB colors.
     * Alpha premultiplied in source and destination.
     *
     * @param src Source color ARGB.
     * @param dst Destination color ARGB.
     * @return blended color
     */
    int blendPreMul(int src, int dst);

    /**
     * Blend a single color.
     * Alpha not premultiplied in source and destination.
     *
     * @param src RGBA source color, not null
     * @param dst RGBA destination color, not null
     * @param buffer RGBA result buffer, not null
     * @return buffer same as given buffer.
     */
    float[] blend(float[] src, float[] dst, float[] buffer);

    /**
     * Blend a single color.
     * Alpha premultiplied in source and destination.
     *
     * @param src RGBA source color, not null
     * @param dst RGBA destination color, not null
     * @param buffer RGBA result buffer, not null
     * @return buffer same as given buffer.
     */
    float[] blendPreMul(float[] src, float[] dst, float[] buffer);

    /**
     * Blend multiple colors in one call.
     * Alpha not premultiplied in source and destination.
     *
     * @param src RGBA source color, not null
     * @param srcOffset start index in source array
     * @param dst RGBA destination color, not null
     * @param dstOffset start index in destination array
     * @param buffer RGBA result buffer, not null
     * @param bufOffset start index in result buffer array
     * @param nbElement number of color to transform
     * @return buffer same as given buffer.
     */
    float[] blend(float[] src, int srcOffset,
                  float[] dst, int dstOffset,
                  float[] buffer, int bufOffset,
                  int nbElement);

    /**
     * Blend multiple colors in one call.
     * Alpha premultiplied in source and destination.
     *
     * @param src RGBA source color, not null
     * @param srcOffset start index in source array
     * @param dst RGBA destination color, not null
     * @param dstOffset start index in destination array
     * @param buffer RGBA result buffer, not null
     * @param bufOffset start index in result buffer array
     * @return buffer same as given buffer.
     */
    float[] blendPreMul(float[] src, int srcOffset,
                        float[] dst, int dstOffset,
                        float[] buffer, int bufOffset,
                        int nbElement);

}
