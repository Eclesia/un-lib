
package science.unlicense.image.api.color;

import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public interface ColorRW extends Color,TupleRW {

    /**
     * Copy color values from given color.
     * if the color to not have the same ColorSystem it will be converted.
     * transformation may cause some information loss.
     *
     * @param color
     */
    void set(Color color);

    void fromARGB(int argb);

}
