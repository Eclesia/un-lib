
package science.unlicense.image.api.model;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.AbstractNumberTupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;

/**
 *
 * @author Johann Sorel
 */
public class DerivateModel extends AbstractImageModel {

    private final ImageModel base;
    private final int[] mapping;
    private final int[] bitOffset;
    private final int[] bitCount;
    private final int[] bitMasks;
    private final int nbSampleBase;
    private final int nbSampleDerivate;

    private boolean applyMapping = false;

    public static ImageModel create(ImageModel base, int[] mapping, int[] bitOffset, int[] bitCount, SampleSystem sampleSystem) {

        final SampleSystem ssBase = base.getSampleSystem();
        final int nbSampleBase = ssBase.getNumComponents();
        final int nbSampleDerivate = sampleSystem.getNumComponents();

        if (mapping != null && mapping.length != nbSampleDerivate) {
            throw new InvalidArgumentException("Mapping length do not match sample system size, expected "+nbSampleDerivate+" but was"+ mapping.length);
        } else if (mapping == null && nbSampleBase != nbSampleDerivate) {
            throw new InvalidArgumentException("base model length do not match new sample system size, mapping must be specified");
        }
        if ( (bitOffset != null && bitCount == null) || (bitOffset == null && bitCount != null) ) {
            throw new InvalidArgumentException("Both bitoffset and bitcount must be defined");
        }
        if (bitOffset != null && bitOffset.length != nbSampleDerivate) {
            throw new InvalidArgumentException("BitOffset length do not match sample system size, expected "+nbSampleDerivate+" but was"+ bitOffset.length);
        }
        if (bitCount != null && bitOffset.length != bitCount.length) {
            throw new InvalidArgumentException("BitOffset and BitCount length do not match, expected "+nbSampleDerivate+" but was"+ bitCount.length);
        }

        //remove mapping if useless
        mapCheck:
        if (mapping != null) {
            if (mapping.length != nbSampleBase) break mapCheck;
            //check if indexes are reordered
            for (int i=0;i<mapping.length;i++) {
                if (mapping[i] != i) break mapCheck;
            }
            mapping = null;
        }

        //remove bitOffset and count if useless
        bitCheck:
        if (bitOffset != null) {
            //TODO, we need each column type
        }

        if (mapping == null && bitOffset == null) {
            //try to create a more direct model
            if (base instanceof InterleavedModel) {
                final InterleavedModel parent = (InterleavedModel) base;
                final NumberType numericType = parent.getNumericType();
                return new InterleavedModel(sampleSystem, numericType);
            } else if (base instanceof PlanarModel) {
                final PlanarModel parent = (PlanarModel) base;
                final NumberType numericType = parent.getNumericType();
                return new PlanarModel(sampleSystem, numericType);
            }
        }

        //can not optimize model, use default implementation
        return new DerivateModel(base, mapping, bitOffset, bitCount, sampleSystem);
    }

    /**
     *
     * @param base
     * @param mapping size must equal new sample system sample count, if null values ar copied unchanged
     * @param bitOffset size must equal new sample system sample count, can be null
     * @param bitCount size must equal new sample system sample count, can be null
     * @param sampleSystem
     */
    private DerivateModel(ImageModel base, int[] mapping, int[] bitOffset, int[] bitCount, SampleSystem sampleSystem) {
        super(sampleSystem);
        this.nbSampleBase = base.getSampleSystem().getNumComponents();
        this.nbSampleDerivate = sampleSystem.getNumComponents();

        if (mapping != null && mapping.length != nbSampleDerivate) {
            throw new InvalidArgumentException("Mapping length do not match sample system size, expected "+nbSampleDerivate+" but was"+ mapping.length);
        } else if (mapping == null && nbSampleBase != nbSampleDerivate) {
            throw new InvalidArgumentException("base model length do not match new sample system size, mapping must be specified");
        }
        if ( (bitOffset != null && bitCount == null) || (bitOffset == null && bitCount != null) ) {
            throw new InvalidArgumentException("Both bitoffset and bitcount must be defined");
        }
        if (bitOffset != null && bitOffset.length != nbSampleDerivate) {
            throw new InvalidArgumentException("BitOffset length do not match sample system size, expected "+nbSampleDerivate+" but was"+ bitOffset.length);
        }
        if (bitCount != null && bitOffset.length != bitCount.length) {
            throw new InvalidArgumentException("BitOffset and BitCount length do not match, expected "+nbSampleDerivate+" but was"+ bitCount.length);
        }

        simplify:
        if (mapping != null) {
            applyMapping = true;
            if (nbSampleBase == nbSampleDerivate) {
                //check if mapping is really necessary
                for (int i=0;i<nbSampleBase;i++) {
                    if (mapping[i] != i) break simplify;
                }
                //mapping is unnecessary
                applyMapping = false;
            }
        }

        this.base = base;
        this.mapping = mapping;
        this.bitOffset = bitOffset;
        this.bitCount = bitCount;

        if (bitOffset != null) {
            this.bitMasks = new int[bitCount.length];
            for (int i=0;i<bitCount.length;i++){
                bitMasks[i] = ((~0) >> bitOffset[i]) & bitCount[i];
            }
        } else {
            this.bitMasks = null;
        }
    }

    public int[] getMapping() {
        return mapping == null ? null : mapping.clone();
    }

    public int[] getBitOffset() {
        return bitOffset == null ? null : bitOffset.clone();
    }

    public int[] getBitCount() {
        return bitCount == null ? null : bitCount.clone();
    }

    @Override
    public Buffer createBuffer(Extent.Long dimensions, BufferFactory factory) {
        return base.createBuffer(dimensions, factory);
    }

    @Override
    public NumberType getNumericType() {
        return base.getNumericType();
    }

    @Override
    public TupleGrid asTupleBuffer(Image image) {
        TupleGrid buffer = base.asTupleBuffer(image);
        return new DerivateTupleGrid(buffer);

    }

    private class DerivateTupleGrid extends AbstractNumberTupleGrid {

        private final TupleGrid base;

        public DerivateTupleGrid(TupleGrid base) {
            super(base.getExtent(), (NumberType) base.getNumericType(), DerivateModel.this.sampleSystem);
            this.base = base;
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW derivate) {

            if (applyMapping) {
                TupleRW src = base.createTuple();
                base.getTuple(coordinate, src);
                switch (mapping.length) {
                    case 4 : if (mapping[3] >= 0) derivate.set(3,src.get(mapping[3]));
                    case 3 : if (mapping[2] >= 0) derivate.set(2,src.get(mapping[2]));
                    case 2 : if (mapping[1] >= 0) derivate.set(1,src.get(mapping[1]));
                    case 1 : if (mapping[0] >= 0) derivate.set(0,src.get(mapping[0]));
                        break;
                    default :
                        for (int i=0; i<mapping.length; i++) {
                        final int idx = mapping[i];
                        if (idx >= 0) {
                            derivate.set(i,src.get(idx));
                        }
                    }
                }
            } else {
                base.getTuple(coordinate, derivate);
            }

            if (bitMasks != null) {
                for (int i=0; i<bitMasks.length; i++) {
                    long v = (long) derivate.get(i);
                    v = (v & bitMasks[i]) >> bitOffset[i];
                    derivate.set(i,v);
                }
            }

            return derivate;
        }

        @Override
        public void setTuple(Tuple coordinate, Tuple buffer) {

            double[] derivate = buffer.toDouble();
            double[] src = new double[nbSampleBase];

            if (applyMapping) {
                for (int i=0;i<mapping.length;i++) {
                    final int idx = mapping[i];
                    if (idx >= 0) {
                        src[idx] = derivate[i];
                    }
                }
            } else {
                src = derivate;
            }

            if (bitMasks != null) {
                for (int i=0; i<bitMasks.length; i++) {
                    long v = (long) derivate[i];
                    v = v & bitCount[i];
                    v = v << bitOffset[i];
                    src[i] = v & (long) src[i];
                }
            }

            final TupleRW tout = base.createTuple();
            tout.set(src);
            base.setTuple(coordinate, tout);
        }

    }

}
