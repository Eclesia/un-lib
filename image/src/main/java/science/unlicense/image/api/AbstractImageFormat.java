
package science.unlicense.image.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractImageFormat extends DefaultFormat implements ImageFormat {

    public static final Chars ADDRESS = Chars.constant("address");

    public AbstractImageFormat(Chars identifier) {
        super(identifier);
        resourceTypes.add(ImageResource.class);
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

}
