
package science.unlicense.image.api.process;

import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2f64;

/**
 * Extend image filled pixels with a repetition of the image.
 *
 * For example if on a one channel image we obtain ;
 * <pre>
 *              e f d e f d e
 *              h i g h i g h
 *   a b c      b c a b c a b
 *   d e f  ===>e f d e f d e
 *   g h i      h i g h i g h
 *              b c a b c a b
 *              e f d e f d e
 * </pre>
 *
 * @author Humbert Florent
 * @author Johann Sorel
 */
public class ExtrapolatorWrap extends AbstractExtrapolator {

    public ExtrapolatorWrap(TupleGrid parent) {
        super(parent);
    }

    @Override
    protected TupleRW evaluateOutside(TupleRW coordinate, TupleRW buffer) {

        //we need to restore old values after, better avoid modifying them
        int oldx, x = oldx = (int) Math.round(coordinate.get(0));
        int oldy, y = oldy = (int) Math.round(coordinate.get(1));
        if (x < 0) x = ((Math.abs(x) / dims[0])+1)*dims[0] + x;
        if (y < 0) y = ((Math.abs(y) / dims[1])+1)*dims[1] + y;
        if (x >= dims[0]) x = x % dims[0];
        if (y >= dims[1]) y = y % dims[1];

        coordinate.set(0, x);
        coordinate.set(1, y);
        parent.getTuple(coordinate, buffer);
        Maths.wrap(-1, 0, dims[1]);
        //restore old value
        coordinate.set(0, oldx);
        coordinate.set(1, oldy);
        return buffer;
    }

    @Override
    public TupleSpaceCursor cursor() {
        return new Cursor(parent.cursor());
    }

    private class Cursor extends ExtrapolatorCursor {

        private final TupleRW buffer;
        private final Vector2f64 coord = new Vector2f64();

        public Cursor(TupleSpaceCursor base) {
            super(base);
            buffer = parent.createTuple();
        }

        @Override
        protected Tuple getOutsideTuple(Tuple coordinate) {
            coord.x = coordinate.get(0);
            coord.y = coordinate.get(1);
            if (coord.x<0) coord.x =  dims[0] - ( Math.abs(coord.x) % dims[0] );
            else if (coord.x>=dims[0]) coord.x = (coord.x % dims[0]) -1;
            if (coord.y<0) coord.y = dims[1] - ( Math.abs(coord.y) % dims[1] );
            else if (coord.y>=dims[1]) coord.y = (coord.y % dims[1]) -1;

            parent.getTuple(coord, buffer);
            return buffer;
        }

    }
}
