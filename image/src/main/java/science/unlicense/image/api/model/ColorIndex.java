
package science.unlicense.image.api.model;

import science.unlicense.image.api.color.Color;

/**
 * Color index, also called color palette.
 *
 * @author Johann Sorel
 */
public class ColorIndex {

    public final Color[] palette;

    public ColorIndex(Color[] palette) {
        this.palette = palette;
    }

}
