
package science.unlicense.image.api.color;

import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.VectorNf32;

/**
 *
 * @author Johann Sorel
 */
public class DecoratedColorRW extends AbstractColor implements ColorRW {

    private final TupleRW tuple;

    public DecoratedColorRW(TupleRW tuple, ColorSystem colorSystem) {
        super(colorSystem);
        if (tuple.getSampleCount() != colorSystem.getNumComponents()) {
            throw new InvalidArgumentException("Tuple size do not match color system dimension, expected "+colorSystem.getNumComponents()+" but was "+tuple.getSampleCount());
        }
        this.tuple = tuple;
    }

    @Override
    public ArithmeticType getNumericType() {
        return tuple.getNumericType();
    }

    @Override
    public double get(int indice) throws InvalidIndexException {
        return tuple.get(indice);
    }

    @Override
    public Arithmetic getNumber(int indice) throws InvalidIndexException {
        return tuple.getNumber(indice);
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        tuple.toNumber(buffer, offset);
    }

    @Override
    public float[] getSamples(float[] buffer) {
        if (buffer == null) buffer = new float[colorSystem.getNumComponents()];
        tuple.toFloat(buffer, 0);
        return buffer;
    }

    @Override
    public void setAll(double v) {
        tuple.setAll(v);
    }

    @Override
    public void set(int indice, double value) throws InvalidIndexException {
        tuple.set(indice, value);
    }

    @Override
    public void set(int indice, Arithmetic value) throws InvalidIndexException {
        tuple.set(indice, value);
    }

    @Override
    public void set(Tuple toCopy) {
        tuple.set(toCopy);
    }

    @Override
    public void set(double[] values) {
        tuple.set(values);
    }

    @Override
    public void set(float[] values) {
        tuple.set(values);
    }

    @Override
    public void set(Color color) {
        final ColorSystem ccs = color.getSampleSystem();
        if (colorSystem.equals(ccs)) {
            tuple.set(color);
        } else {
            final Transform tr = ccs.getTranform(colorSystem);
            tr.transform(color, tuple);
        }
    }

    @Override
    public void fromARGB(int argb) {
        set(new ColorRGB(argb));
    }

    @Override
    public TupleRW create(int size) {
        return VectorNf32.createFloat(size);
    }

}
