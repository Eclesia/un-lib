
package science.unlicense.image.api.process;

import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2f64;

/**
 * Extend image filled pixels with closest pixel value.
 *
 * For example if on a one channel image we obtain ;
 * <pre>
 *              a a a b c c c
 *              a a a b c c c
 *   a b c      a a a b c c c
 *   d e f  ===>d d d e f f f
 *   g h i      g g g h i i i
 *              g g g h i i i
 *              g g g h i i i
 * </pre>
 *
 * @author Humbert Florent
 * @author Johann Sorel
 */
public class ExtrapolatorCopy extends AbstractExtrapolator {

    public ExtrapolatorCopy(TupleGrid parent) {
        super(parent);
    }

    @Override
    protected TupleRW evaluateOutside(TupleRW coordinate, TupleRW buffer) {

        //we need to restore old values after, better avoid modifying them
        final int oldx = (int) Math.round(coordinate.get(0));
        final int oldy = (int) Math.round(coordinate.get(1));
        if (oldx<0) coordinate.set(0, 0);
        else if (oldx >= dims[0]) coordinate.set(0, dims[0]-1);
        if (oldy<0) coordinate.set(1, 0);
        else if (oldy >= dims[1]) coordinate.set(1, dims[1]-1);

        parent.getTuple(coordinate, buffer);

        //restore old value
        coordinate.set(0, oldx);
        coordinate.set(1, oldy);
        return buffer;
    }

    @Override
    public TupleSpaceCursor cursor() {
        return new Cursor(parent.cursor());
    }

    private class Cursor extends ExtrapolatorCursor {

        private final TupleRW buffer;
        private final Vector2f64 coord = new Vector2f64();

        public Cursor(TupleSpaceCursor base) {
            super(base);
            buffer = parent.createTuple();
        }

        @Override
        protected Tuple getOutsideTuple(Tuple coordinate) {
            coord.x = coordinate.get(0);
            coord.y = coordinate.get(1);
            if (coord.x<0) coord.x = 0;
            else if (coord.x>=dims[0]) coord.x = dims[0]-1;
            if (coord.y<0) coord.y = 0;
            else if (coord.y>=dims[1]) coord.y = dims[1]-1;

            parent.getTuple(coord, buffer);
            return buffer;
        }

    }
}
