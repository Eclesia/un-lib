
package science.unlicense.image.api.color;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.UnimplementedException;

/**
 * Porter Duff alpha rules for color blending.
 * http://en.wikipedia.org/wiki/Alpha_blending#Alpha_blending
 * http://dev.w3.org/fxtf/compositing-1/#porterduffcompositingoperators
 *
 * @author Johann Sorel
 */
public abstract class AlphaBlending extends AbstractBlending{

    /**
     * Default blending method.
     * Equivalent to SRC_OVER with alpha at 1.0 .
     */
    public static final AlphaBlending DEFAULT = AlphaBlending.create(AlphaBlending.SRC_OVER, 1.0f);

    public static final int CLEAR      = 1;
    public static final int SRC        = 2;
    public static final int DST        = 3;
    public static final int XOR        = 4;
    public static final int LIGHTER    = 5;

    public static final int SRC_OVER   = 6;
    public static final int SRC_IN     = 7;
    public static final int SRC_OUT    = 8;
    public static final int SRC_ATOP   = 9;

    public static final int DST_OVER   = 10;
    public static final int DST_IN     = 11;
    public static final int DST_OUT    = 12;
    public static final int DST_ATOP   = 13;

    protected final float alpha;

    private AlphaBlending(float alpha) {
        this.alpha = alpha;
    }

    /**
     * Get blending type,
     * @return blending type
     */
    public abstract int getType();

    /**
     * Get global alpha blending used on source color.
     * @return float from 0 to 1.
     */
    public float getAlpha(){
        return alpha;
    }

    public static AlphaBlending create(final int type, final float alpha){
             if (type==CLEAR)        return new Clear(alpha);
        else if (type==SRC)          return new Src(alpha);
        else if (type==DST)          return new Dst(alpha);
        else if (type==XOR)          return new Xor(alpha);
        else if (type==LIGHTER)      return new Lighter(alpha);
        else if (type==SRC_OVER)     return new SrcOver(alpha);
        else if (type==SRC_IN)       return new SrcIn(alpha);
        else if (type==SRC_OUT)      return new SrcOut(alpha);
        else if (type==SRC_ATOP)     return new SrcAtop(alpha);
        else if (type==DST_OVER)     return new DstOver(alpha);
        else if (type==DST_IN)       return new DstIn(alpha);
        else if (type==DST_OUT)      return new DstOut(alpha);
        else if (type==DST_ATOP)     return new DstAtop(alpha);
        throw new InvalidArgumentException("Type "+type+" unknowned.");
    }

    /**
     * No regions are enabled.
     *
     * Fa = 0; Fb = 0
     * co = 0
     * αo = 0
     */
    public static final class Clear extends AlphaBlending{
        private Clear(float alpha) {
            super(alpha);
        }
        public int getType() {
            return CLEAR;
        }
        public Color blend(Color source, Color dest) {
            return new ColorRGB(0, 0, 0, 0, false);
        }
        public int blend(int src, int dst) {
            return 0;
        }
        public int blendPreMul(int src, int dst) {
            return 0;
        }
        public float[] blend(float[] source, float[] dst, float[] buffer) {
            buffer[0]=0;buffer[1]=0;buffer[2]=0;buffer[3]=0;
            return buffer;
        }
        public float[] blendPreMul(float[] source, float[] dst, float[] buffer) {
            buffer[0]=0;buffer[1]=0;buffer[2]=0;buffer[3]=0;
            return buffer;
        }
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            Arrays.fill(buffer, bufOffset, 4*nbElement, 0f);
            return buffer;
        }
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            Arrays.fill(buffer, bufOffset, 4*nbElement, 0f);
            return buffer;
        }
    }

    /**
     * Only the source will be present.
     *
     * Fa = 1; Fb = 0
     * co = αs x Cs
     * αo = αs
     */
    public static final class Src extends AlphaBlending{
        private Src(float alpha) {
            super(alpha);
        }
        public int getType() {
            return SRC;
        }
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs;
                final float Fa = 1;
                final float Fb = 0;
                buffer[bufOffset+off+0] = porterDuff(αs, Fa, src[srcOffset+off+0], αb, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuff(αs, Fa, src[srcOffset+off+1], αb, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuff(αs, Fa, src[srcOffset+off+2], αb, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;

                //result value is premultiplied, return it to non-multiplied
                if (αo!=0){
                    buffer[bufOffset+off+0] /= αo;
                    buffer[bufOffset+off+1] /= αo;
                    buffer[bufOffset+off+2] /= αo;
                }
            }
            return buffer;
        }
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs;
                final float Fa = 1;
                final float Fb = 0;
                buffer[bufOffset+off+0] = porterDuffPreMul(Fa, src[srcOffset+off+0]*alpha, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuffPreMul(Fa, src[srcOffset+off+1]*alpha, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuffPreMul(Fa, src[srcOffset+off+2]*alpha, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;
            }
            return buffer;
        }
    }

    /**
     * Only the destination will be present.
     *
     * Fa = 0; Fb = 1
     * co = αb x Cb
     * αo = αb
     */
    public static final class Dst extends AlphaBlending{
        private Dst(float alpha) {
            super(alpha);
        }
        public int getType() {
            return DST;
        }
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αb;
                final float Fa = 0;
                final float Fb = 1;
                buffer[bufOffset+off+0] = porterDuff(αs, Fa, src[srcOffset+off+0], αb, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuff(αs, Fa, src[srcOffset+off+1], αb, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuff(αs, Fa, src[srcOffset+off+2], αb, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;

                //result value is premultiplied, return it to non-multiplied
                if (αo!=0){
                    buffer[bufOffset+off+0] /= αo;
                    buffer[bufOffset+off+1] /= αo;
                    buffer[bufOffset+off+2] /= αo;
                }

            }

            return buffer;
        }
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αb;
                final float Fa = 0;
                final float Fb = 1;
                buffer[bufOffset+off+0] = porterDuffPreMul(Fa, src[srcOffset+off+0]*alpha, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuffPreMul(Fa, src[srcOffset+off+1]*alpha, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuffPreMul(Fa, src[srcOffset+off+2]*alpha, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;
            }
            return buffer;
        }
    }

    /**
     * The non-overlapping regions of source and destination are combined.
     *
     * Fa = 1 - αb; Fb = 1 – αs
     * co = αs x Cs x (1 - αb) + αb x Cb x (1 – αs)
     * αo = αs x (1 - αb) + αb x (1 – αs)
     */
    public static final class Xor extends AlphaBlending{
        private Xor(float alpha) {
            super(alpha);
        }
        public int getType() {
            return XOR;
        }
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * (1 - αb) + αb * (1 - αs);
                final float Fa = 1 - αb;
                final float Fb = 1 - αs;
                buffer[bufOffset+off+0] = porterDuff(αs, Fa, src[srcOffset+off+0], αb, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuff(αs, Fa, src[srcOffset+off+1], αb, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuff(αs, Fa, src[srcOffset+off+2], αb, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;

                //result value is premultiplied, return it to non-multiplied
                if (αo!=0){
                    buffer[bufOffset+off+0] /= αo;
                    buffer[bufOffset+off+1] /= αo;
                    buffer[bufOffset+off+2] /= αo;
                }
            }

            return buffer;
        }
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * (1 - αb) + αb * (1 - αs);
                final float Fa = 1 - αb;
                final float Fb = 1 - αs;
                buffer[bufOffset+off+0] = porterDuffPreMul(Fa, src[srcOffset+off+0]*alpha, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuffPreMul(Fa, src[srcOffset+off+1]*alpha, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuffPreMul(Fa, src[srcOffset+off+2]*alpha, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;
            }
            return buffer;
        }
    }

    /**
     * Display the sum of the source image and destination image.
     * It is defined in the Porter Duff paper [3] as the ‘plus’ operator.
     *
     * Fa = 1; Fb = 1
     * co = αs x Cs + αb x Cb;
     * αo = αs + αb
     */
    public static final class Lighter extends AlphaBlending{
        private Lighter(float alpha) {
            super(alpha);
        }
        public int getType() {
            return LIGHTER;
        }
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     * Source is placed over the destination.
     *
     * Fa = 1; Fb = 1 – αs
     * co = αs x Cs + αb x Cb x (1 – αs)
     * αo = αs + αb x (1 – αs)
     */
    public static final class SrcOver extends AlphaBlending{
        private SrcOver(float alpha) {
            super(alpha);
        }
        public int getType() {
            return SRC_OVER;
        }

        @Override
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs + αb * (1 - αs);
                final float Fa = 1;
                final float Fb = 1 - αs;
                buffer[bufOffset+off+0] = porterDuff(αs, Fa, src[srcOffset+off+0], αb, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuff(αs, Fa, src[srcOffset+off+1], αb, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuff(αs, Fa, src[srcOffset+off+2], αb, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;

                //result value is premultiplied, return it to non-multiplied
                if (αo!=0){
                    buffer[bufOffset+off+0] /= αo;
                    buffer[bufOffset+off+1] /= αo;
                    buffer[bufOffset+off+2] /= αo;
                }

            }

            return buffer;
        }

        @Override
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs + αb * (1 - αs);
                final float Fb = 1 - αs;
                buffer[bufOffset+off+0] = src[srcOffset+off+0]*alpha + Fb * dst[dstOffset+off+0];
                buffer[bufOffset+off+1] = src[srcOffset+off+1]*alpha + Fb * dst[dstOffset+off+1];
                buffer[bufOffset+off+2] = src[srcOffset+off+2]*alpha + Fb * dst[dstOffset+off+2];
                buffer[bufOffset+off+3] = αo;
            }
            return buffer;
        }
    }

    /**
     * The source that overlaps the destination, replaces the destination.
     *
     * Fa = αb; Fb = 0
     * co = αs x Cs x αb
     * αo = αs x αb
     */
    public static final class SrcIn extends AlphaBlending{
        private SrcIn(float alpha) {
            super(alpha);
        }
        public int getType() {
            return SRC_IN;
        }

        @Override
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * αb;
                final float Fa = αb;
                final float Fb = 0;
                buffer[bufOffset+off+0] = porterDuff(αs, Fa, src[srcOffset+off+0], αb, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuff(αs, Fa, src[srcOffset+off+1], αb, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuff(αs, Fa, src[srcOffset+off+2], αb, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;

                //result value is premultiplied, return it to non-multiplied
                if (αo!=0){
                    buffer[bufOffset+off+0] /= αo;
                    buffer[bufOffset+off+1] /= αo;
                    buffer[bufOffset+off+2] /= αo;
                }

            }

            return buffer;
        }

        @Override
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * αb;
                final float Fa = αb;
                final float Fb = 0;
                buffer[bufOffset+off+0] = porterDuffPreMul(Fa, src[srcOffset+off+0]*alpha, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuffPreMul(Fa, src[srcOffset+off+1]*alpha, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuffPreMul(Fa, src[srcOffset+off+2]*alpha, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;
            }
            return buffer;
        }
    }

    /**
     * Source is placed, where it falls outside of the destination.
     *
     * Fa = 1 – αb; Fb = 0
     * co = αs x Cs x (1 – αb)
     * αo = αs x (1 – αb)
     */
    public static final class SrcOut extends AlphaBlending{
        private SrcOut(float alpha) {
            super(alpha);
        }
        public int getType() {
            return SRC_OUT;
        }
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * (1 - αb);
                final float Fa = 1 - αb;
                final float Fb = 0;
                buffer[bufOffset+off+0] = porterDuff(αs, Fa, src[srcOffset+off+0], αb, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuff(αs, Fa, src[srcOffset+off+1], αb, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuff(αs, Fa, src[srcOffset+off+2], αb, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;

                //result value is premultiplied, return it to non-multiplied
                if (αo!=0){
                    buffer[bufOffset+off+0] /= αo;
                    buffer[bufOffset+off+1] /= αo;
                    buffer[bufOffset+off+2] /= αo;
                }
            }

            return buffer;
        }
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * (1 - αb);
                final float Fa = 1 - αb;
                final float Fb = 0;
                buffer[bufOffset+off+0] = porterDuffPreMul(Fa, src[srcOffset+off+0]*alpha, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuffPreMul(Fa, src[srcOffset+off+1]*alpha, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuffPreMul(Fa, src[srcOffset+off+2]*alpha, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;
            }
            return buffer;
        }
    }

    /**
     * Source which overlaps the destination, replaces the destination. Destination is placed elsewhere.
     *
     * Fa = αb; Fb = 1 – αs
     * co = αs x Cs x αb + αb x Cb x (1 – αs)
     * αo = αs x αb + αb x (1 – αs)
     */
    public static final class SrcAtop extends AlphaBlending{
        private SrcAtop(float alpha) {
            super(alpha);
        }
        public int getType() {
            return SRC_ATOP;
        }
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * αb + αb * (1 - αs);
                final float Fa = αb;
                final float Fb = 1 - αs;
                buffer[bufOffset+off+0] = porterDuff(αs, Fa, src[srcOffset+off+0], αb, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuff(αs, Fa, src[srcOffset+off+1], αb, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuff(αs, Fa, src[srcOffset+off+2], αb, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;

                //result value is premultiplied, return it to non-multiplied
                if (αo!=0){
                    buffer[bufOffset+off+0] /= αo;
                    buffer[bufOffset+off+1] /= αo;
                    buffer[bufOffset+off+2] /= αo;
                }
            }

            return buffer;
        }
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * αb + αb * (1 - αs);
                final float Fa = αb;
                final float Fb = 1 - αs;
                buffer[bufOffset+off+0] = porterDuffPreMul(Fa, src[srcOffset+off+0]*alpha, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuffPreMul(Fa, src[srcOffset+off+1]*alpha, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuffPreMul(Fa, src[srcOffset+off+2]*alpha, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;
            }
            return buffer;
        }
    }

    /**
     * Destination is placed over the source.
     *
     * Fa = 1 – αb; Fb = 1
     * co = αs x Cs x (1 – αb) + αb x Cb
     * αo = αs x (1 – αb) + αb
     */
    public static final class DstOver extends AlphaBlending{
        private DstOver(float alpha) {
            super(alpha);
        }
        public int getType() {
            return DST_OVER;
        }

        @Override
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * (1 - αb) + αb;
                final float Fa = 1 - αb;
                final float Fb = 1;
                buffer[bufOffset+off+0] = porterDuff(αs, Fa, src[srcOffset+off+0], αb, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuff(αs, Fa, src[srcOffset+off+1], αb, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuff(αs, Fa, src[srcOffset+off+2], αb, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;

                //result value is premultiplied, return it to non-multiplied
                if (αo!=0){
                    buffer[bufOffset+off+0] /= αo;
                    buffer[bufOffset+off+1] /= αo;
                    buffer[bufOffset+off+2] /= αo;
                }

            }

            return buffer;
        }

        @Override
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * (1 - αb) + αb;
                final float Fa = 1 - αb;
                final float Fb = 1;
                buffer[bufOffset+off+0] = porterDuffPreMul(Fa, src[srcOffset+off+0]*alpha, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuffPreMul(Fa, src[srcOffset+off+1]*alpha, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuffPreMul(Fa, src[srcOffset+off+2]*alpha, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;
            }
            return buffer;
        }
    }

    /**
     * Destination which overlaps the source, replaces the source.
     *
     * Fa = 0; Fb = αs
     * co = αb x Cb x αs
     * αo = αb x αs
     */
    public static final class DstIn extends AlphaBlending{
        private DstIn(float alpha) {
            super(alpha);
        }
        public int getType() {
            return DST_IN;
        }
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αb * αs;
                final float Fa = 0;
                final float Fb = αs;
                buffer[bufOffset+off+0] = porterDuff(αs, Fa, src[srcOffset+off+0], αb, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuff(αs, Fa, src[srcOffset+off+1], αb, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuff(αs, Fa, src[srcOffset+off+2], αb, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;

                //result value is premultiplied, return it to non-multiplied
                if (αo!=0){
                    buffer[bufOffset+off+0] /= αo;
                    buffer[bufOffset+off+1] /= αo;
                    buffer[bufOffset+off+2] /= αo;
                }
            }

            return buffer;
        }
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αb * αs;
                final float Fa = 0;
                final float Fb = αs;
                buffer[bufOffset+off+0] = porterDuffPreMul(Fa, src[srcOffset+off+0]*alpha, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuffPreMul(Fa, src[srcOffset+off+1]*alpha, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuffPreMul(Fa, src[srcOffset+off+2]*alpha, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;
            }
            return buffer;
        }
    }

    /**
     * Destination is placed, where it falls outside of the source.
     *
     * Fa = 0; Fb = 1 – αs
     * co = αb x Cb x (1 – αs)
     * αo = αb x (1 – αs)
     */
    public static final class DstOut extends AlphaBlending{
        private DstOut(float alpha) {
            super(alpha);
        }
        public int getType() {
            return DST_OUT;
        }
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αb * (1 - αs);
                final float Fa = 0;
                final float Fb = 1 - αs;
                buffer[bufOffset+off+0] = porterDuff(αs, Fa, src[srcOffset+off+0], αb, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuff(αs, Fa, src[srcOffset+off+1], αb, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuff(αs, Fa, src[srcOffset+off+2], αb, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;

                //result value is premultiplied, return it to non-multiplied
                if (αo!=0){
                    buffer[bufOffset+off+0] /= αo;
                    buffer[bufOffset+off+1] /= αo;
                    buffer[bufOffset+off+2] /= αo;
                }
            }

            return buffer;
        }
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αb * (1 - αs);
                final float Fa = 0;
                final float Fb = 1 - αs;
                buffer[bufOffset+off+0] = porterDuffPreMul(Fa, src[srcOffset+off+0]*alpha, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuffPreMul(Fa, src[srcOffset+off+1]*alpha, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuffPreMul(Fa, src[srcOffset+off+2]*alpha, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;
            }
            return buffer;
        }
    }

    /**
     * Destination which overlaps the source replaces the source. Source is placed elsewhere.
     *
     * Fa = 1 - αb; Fb = αs
     * co = αs x Cs x (1 - αb) + αb x Cb x αs
     * αo = αs x (1 - αb) + αb x αs
     */
    public static final class DstAtop extends AlphaBlending{
        private DstAtop(float alpha) {
            super(alpha);
        }
        public int getType() {
            return DST_ATOP;
        }
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * (1 - αb) + αb * αs;
                final float Fa = 1 - αb;
                final float Fb = αs;
                buffer[bufOffset+off+0] = porterDuff(αs, Fa, src[srcOffset+off+0], αb, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuff(αs, Fa, src[srcOffset+off+1], αb, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuff(αs, Fa, src[srcOffset+off+2], αb, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;

                //result value is premultiplied, return it to non-multiplied
                if (αo!=0){
                    buffer[bufOffset+off+0] /= αo;
                    buffer[bufOffset+off+1] /= αo;
                    buffer[bufOffset+off+2] /= αo;
                }
            }

            return buffer;
        }
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for (int i=0,off=0;i<nbElement;i++,off+=4){
                final float αs = src[srcOffset+off+3]*alpha;
                final float αb = dst[dstOffset+off+3];
                final float αo = αs * (1 - αb) + αb * αs;
                final float Fa = 1 - αb;
                final float Fb = αs;
                buffer[bufOffset+off+0] = porterDuffPreMul(Fa, src[srcOffset+off+0]*alpha, Fb, dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = porterDuffPreMul(Fa, src[srcOffset+off+1]*alpha, Fb, dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = porterDuffPreMul(Fa, src[srcOffset+off+2]*alpha, Fb, dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = αo;
            }
            return buffer;
        }
    }



    /**
     * Only the source will be present.
     */
    public static Color copy(Color a, Color b){
        //Fa = 1; Fb = 0
        //co = αs x Cs
        //αo = αs
        return a;
    }

    /**
     * Only the source will be present.
     */
    public static Color src(Color a, Color b){
        return a;
    }

    /**
     * Only the destination will be present.
     */
    public static Color dst(Color a, Color b){
        //Fa = 0; Fb = 1
        //co = αb x Cb
        //αo = αb
        return b;
    }

    /**
     * Source is placed over the destination
     */
    public static Color srcOver(Color a, Color b){
        //Fa = 1; Fb = 1 – αs
        //co = αs x Cs + αb x Cb x (1 – αs)
        //αo = αs + αb x (1 – αs)
        final float αs = a.getAlpha();
        final float αb = b.getAlpha();
        final float Fa = 1;
        final float Fb = 1 - αs;
        return new ColorRGB(
                porterDuff(αs, Fa, a.getRed(),   αb, Fb, b.getRed()  ),
                porterDuff(αs, Fa, a.getGreen(), αb, Fb, b.getGreen()),
                porterDuff(αs, Fa, a.getBlue(),  αb, Fb, b.getBlue() ),
                αs + αb * (1 - αs)
                );
    }

    /**
     * Destination is placed over the source.
     */
    public static Color dstOver(Color a, Color b){
        //Fa = 1 – αb; Fb = 1
        //co = αs x Cs x (1 – αb) + αb x Cb
        //αo = αs x (1 – αb) + αb
        final float αs = a.getAlpha();
        final float αb = b.getAlpha();
        final float Fa = 1 - αs;
        final float Fb = 1;
        return new ColorRGB(
                porterDuff(αs, Fa, a.getRed(),   αb, Fb, b.getRed()  ),
                porterDuff(αs, Fa, a.getGreen(), αb, Fb, b.getGreen()),
                porterDuff(αs, Fa, a.getBlue(),  αb, Fb, b.getBlue() ),
                αs * (1 - αs) + αb
                );
    }

    /**
     * The source that overlaps the destination, replaces the destination.
     */
    public static Color srcIn(Color a, Color b){
        //Fa = αb; Fb = 0
        //co = αs x Cs x αb
        //αo = αs x αb
        final float αs = a.getAlpha();
        final float αb = b.getAlpha();
        final float Fa = αb;
        final float Fb = 0;
        return new ColorRGB(
                porterDuff(αs, Fa, a.getRed(),   αb, Fb, b.getRed()  ),
                porterDuff(αs, Fa, a.getGreen(), αb, Fb, b.getGreen()),
                porterDuff(αs, Fa, a.getBlue(),  αb, Fb, b.getBlue() ),
                αs * αb
                );
    }

    /**
     * Destination which overlaps the source, replaces the source.
     */
    public static Color dstIn(Color a, Color b){
        //Fa = 0; Fb = αs
        //co = αb x Cb x αs
        //αo = αb x αs
        final float αs = a.getAlpha();
        final float αb = b.getAlpha();
        final float Fa = 0;
        final float Fb = αs;
        return new ColorRGB(
                porterDuff(αs, Fa, a.getRed(),   αb, Fb, b.getRed()  ),
                porterDuff(αs, Fa, a.getGreen(), αb, Fb, b.getGreen()),
                porterDuff(αs, Fa, a.getBlue(),  αb, Fb, b.getBlue() ),
                αb * αs
                );
    }

    /**
     * Source is placed, where it falls outside of the destination.
     */
    public static Color srcOut(Color a, Color b){
        //Fa = 1 – αb; Fb = 0
        //co = αs x Cs x (1 – αb)
        //αo = αs x (1 – αb)
        final float αs = a.getAlpha();
        final float αb = b.getAlpha();
        final float Fa = 1 - αb;
        final float Fb = 0;
        return new ColorRGB(
                porterDuff(αs, Fa, a.getRed(),   αb, Fb, b.getRed()  ),
                porterDuff(αs, Fa, a.getGreen(), αb, Fb, b.getGreen()),
                porterDuff(αs, Fa, a.getBlue(),  αb, Fb, b.getBlue() ),
                αs * (1- αb)
                );
    }

    /**
     * Destination is placed, where it falls outside of the source.
     */
    public static Color dstOut(Color a, Color b){
        //Fa = 0; Fb = 1 – αs
        //co = αb x Cb x (1 – αs)
        //αo = αb x (1 – αs)
        final float αs = a.getAlpha();
        final float αb = b.getAlpha();
        final float Fa = 0;
        final float Fb = 1 - αs;
        return new ColorRGB(
                porterDuff(αs, Fa, a.getRed(),   αb, Fb, b.getRed()  ),
                porterDuff(αs, Fa, a.getGreen(), αb, Fb, b.getGreen()),
                porterDuff(αs, Fa, a.getBlue(),  αb, Fb, b.getBlue() ),
                αb * (1- αs)
                );
    }

    /**
     * Source which overlaps the destination, replaces the destination. Destination is placed elsewhere.
     */
    public static Color srcAtop(Color a, Color b){
        //Fa = αb; Fb = 1 – αs
        //co = αs x Cs x αb + αb x Cb x (1 – αs)
        //αo = αs x αb + αb x (1 – αs)
        final float αs = a.getAlpha();
        final float αb = b.getAlpha();
        final float Fa = αb;
        final float Fb = 1 - αs;
        return new ColorRGB(
                porterDuff(αs, Fa, a.getRed(),   αb, Fb, b.getRed()  ),
                porterDuff(αs, Fa, a.getGreen(), αb, Fb, b.getGreen()),
                porterDuff(αs, Fa, a.getBlue(),  αb, Fb, b.getBlue() ),
                αs * αb + αb * (1 - αs)
                );
    }

    /**
     * Destination which overlaps the source replaces the source. Source is placed elsewhere.
     */
    public static Color dstAtop(Color a, Color b){
        //Fa = 1 - αb; Fb = αs
        //co = αs x Cs x (1 - αb) + αb x Cb x αs
        //αo = αs x (1 - αb) + αb x αs
        final float αs = a.getAlpha();
        final float αb = b.getAlpha();
        final float Fa = 1 - αb;
        final float Fb = αs;
        return new ColorRGB(
                porterDuff(αs, Fa, a.getRed(),   αb, Fb, b.getRed()  ),
                porterDuff(αs, Fa, a.getGreen(), αb, Fb, b.getGreen()),
                porterDuff(αs, Fa, a.getBlue(),  αb, Fb, b.getBlue() ),
                αs * (1 - αb) + αb * αs
                );
    }

    /**
     * The non-overlapping regions of source and destination are combined.
     */
    public static Color xor(Color a, Color b){
        //Fa = 1 - αb; Fb = 1 – αs
        //co = αs x Cs x (1 - αb) + αb x Cb x (1 – αs)
        //αo = αs x (1 - αb) + αb x (1 – αs)
        final float αs = a.getAlpha();
        final float αb = b.getAlpha();
        final float Fa = 1 - αb;
        final float Fb = 1 - αs;
        return new ColorRGB(
                porterDuff(αs, Fa, a.getRed(),   αb, Fb, b.getRed()  ),
                porterDuff(αs, Fa, a.getGreen(), αb, Fb, b.getGreen()),
                porterDuff(αs, Fa, a.getBlue(),  αb, Fb, b.getBlue() ),
                αs * (1 - αb) + αb * (1 - αs)
                );
    }

    /**
     * Porter-Duff formula for premultiplied color component.
     * co = Fa x Cs + Fb x Cb
     *
     * Where :
     * co is the output color pre-multiplied with the output alpha [0 <= co <= 1]
     * Fa is defined by the operator and controls inclusion of the source
     * Cs is the color of the source (multiplied by alpha)
     * Fb is defined by the operator and controls inclusion of the destination
     * Cb is the color of the destination (multiplied by alpha)
     */
    public static float porterDuffPreMul(float Fa, float Cs, float Fb, float Cb){
        return Fa * Cs + Fb * Cb;
    }

    public static void porterDuffPreMul(float Fa, float[] Cs, int cso, float Fb, float[] Cb, int cbo, float[] buffer, int bo){
        buffer[bo+0] = Fa * Cs[cso+0] + Fb * Cb[cbo+0];
        buffer[bo+1] = Fa * Cs[cso+1] + Fb * Cb[cbo+1];
        buffer[bo+2] = Fa * Cs[cso+2] + Fb * Cb[cbo+2];
    }

    /**
     * Porter-Duff formula for not premultiplied color component.
     * co = αs x Fa x Cs + αb x Fb x Cb
     *
     * Where :
     * co is the output color pre-multiplied with the output alpha [0 <= co <= 1]
     * αs is the coverage of the source
     * Fa is defined by the operator and controls inclusion of the source
     * Cs is the color of the source (not multiplied by alpha)
     * αb is the coverage of the destination
     * Fb is defined by the operator and controls inclusion of the destination
     * Cb is the color of the destination (not multiplied by alpha)
     */
    public static float porterDuff(float αs, float Fa, float Cs, float αb, float Fb, float Cb){
        return αs * Fa * Cs + αb * Fb * Cb;
    }

    public static void porterDuff(float αs, float Fa, float[] Cs, int cso, float αb, float Fb, float Cb[], int cbo, float[] buffer, int bo){
        buffer[bo+0] = αs * Fa * Cs[cso+0] + αb * Fb * Cb[cbo+0];
        buffer[bo+1] = αs * Fa * Cs[cso+1] + αb * Fb * Cb[cbo+1];
        buffer[bo+2] = αs * Fa * Cs[cso+2] + αb * Fb * Cb[cbo+2];
    }

}
