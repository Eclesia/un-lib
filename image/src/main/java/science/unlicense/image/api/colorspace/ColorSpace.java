
package science.unlicense.image.api.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.transform.Transform;

/**
 * Defines a 'range' of colors.
 * has fallback conversions methods to sRGB color space.
 *
 * https://en.wikipedia.org/wiki/Color_science
 * https://en.wikipedia.org/wiki/Color_space
 * https://en.wikipedia.org/wiki/Absolute_color_space
 * https://en.wikipedia.org/wiki/CIE_1931_color_space
 * https://en.wikipedia.org/wiki/SRGB
 * https://en.wikipedia.org/wiki/Lab_color_space
 * https://en.wikipedia.org/wiki/YCbCr
 *
 * @author Johann Sorel
 */
public interface ColorSpace extends SampleSystem {

    /**
     * Color space name.
     *
     * @return Chars, never null
     */
    Chars getName();

    /**
     * Get colorspace components.
     * @return Arrayn never null
     */
    ColorSpaceComponent[] getComponents();

    /**
     * Create transform from this color space to given one.
     * In the worst case a fallback transformation using sRGB as pivot colorspace
     * will be used.
     *
     * Implementations are required to implement at least transform to sRGB and inverse.
     *
     * @param cs
     * @return Transformation never null.
     */
    Transform getTranform(ColorSpace cs);

}
