
package science.unlicense.image.api.process;

import science.unlicense.geometry.api.tuple.TupleSpace;


/**
 *
 * @author Johann Sorel
 */
public interface DerivateSpace extends TupleSpace {

    /**
     * Sampling are often stacked.
     * For example : image + extrapolator + interpolator
     *
     * @return parent sampling, can be null
     */
    TupleSpace getParent();

}
