
package science.unlicense.image.api.model;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.geometry.api.tuple.AbstractNumberTupleGrid;
import science.unlicense.geometry.api.tuple.PlanarTupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.common.api.number.NumberType;

/**
 * Usual two dimension images mostly share a common image structure once
 * uncompressed. This class should suffice for image with planar samples.
 *
 * ImageBank is expected to be ordered sample blocks next to another, and line by line within each block :
 * Samples 1
 * line 0 : [s1][s1][s1][s1][s1][s1] ...
 * line 1 : [s1][s1][s1][s1][s1][s1] ...
 * ...
 * Samples 2
 * line 0 : [s2][s2][s2][s2][s2][s2] ...
 * line 1 : [s2][s2][s2][s2][s2][s2] ...
 * ...
 *
 * @author Johann Sorel
 */
public class PlanarModel extends AbstractImageModel{

    private final NumberType sampleType;

    public PlanarModel(SampleSystem ss, NumberType sampleType) {
        super(ss);
        this.sampleType = sampleType;
    }

    public NumberType getNumericType(){
        return sampleType;
    }

    public Buffer createBuffer(Extent.Long dimensions,BufferFactory factory) {
        return AbstractNumberTupleGrid.createPrimitiveBuffer(dimensions, sampleType, sampleSystem.getNumComponents(), factory);
    }

    public TupleGrid create(Extent.Long dimensions) {
        return new PlanarTupleGrid(createBuffer(dimensions), dimensions, sampleType, sampleSystem);
    }

    @Override
    public TupleGrid asTupleBuffer(Image image) {
        return new PlanarTupleGrid(image.getDataBuffer(), image.getExtent(), sampleType, sampleSystem);
    }

}
