
package science.unlicense.image.api.process;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.geometry.api.Extent;
import science.unlicense.task.api.AbstractTask;
import science.unlicense.math.api.Maths;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import static science.unlicense.common.api.number.Primitive.*;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.geometry.api.tuple.TupleGrid;

/**
 * Image operator which preserve the image size and models.
 *
 * This ensure both input images have the same dimensions.
 *
 * @author Johann Sorel
 */
public abstract class AbstractPairPreservingOperator extends AbstractTask{

    public AbstractPairPreservingOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    protected Extent.Long extent;
    protected Image inputImage1;
    protected ImageModel inputRawModel1;
    protected TupleGrid inputTuples1;
    protected ImageModel inputColorModel1;

    protected Image inputImage2;
    protected ImageModel inputRawModel2;
    protected TupleGrid inputTuples2;
    protected ImageModel inputColorModel2;

    protected Image outputImage;
    protected ImageModel outputRawModel;
    protected TupleGrid outputTuples;
    protected ImageModel outputColorModel;

    protected int outNbSample;
    protected ArithmeticType outSampleType;

    public Document perform() {
        inputImage1 = (Image) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        extent = inputImage1.getExtent();
        inputRawModel1 = inputImage1.getRawModel();
        inputTuples1 = inputRawModel1.asTupleBuffer(inputImage1);
        inputColorModel1 = inputImage1.getColorModel();

        inputImage2 = (Image) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE2.getId());
        inputRawModel2 = inputImage2.getRawModel();
        inputTuples2 = inputRawModel2.asTupleBuffer(inputImage2);
        inputColorModel2 = inputImage2.getColorModel();

        if (!inputImage1.getExtent().equals(inputImage2.getExtent())){
            throw new RuntimeException("Image extent mismatch");
        }


        //calculate the result image
        outputColorModel = getOutputColorModel();
        outputRawModel = getOutputSampleModel();
        final Buffer buffer = outputRawModel.createBuffer(extent);
        outputImage = new DefaultImage(buffer, extent, outputRawModel, outputColorModel);
        outNbSample = outputTuples.getSampleSystem().getNumComponents();
        outSampleType = outputTuples.getNumericType();

        outputParameters.setPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),outputImage);
        return outputParameters;
    }

    /**
     *
     * @return same image sample model as first image
     */
    protected ImageModel getOutputSampleModel(){
        return inputRawModel1;
    }

    /**
     *
     * @return same image color model as first image
     */
    protected ImageModel getOutputColorModel(){
        return inputColorModel1;
    }

    protected void adjustInOutSamples(double[] inSamples, Object outSamples){
        switch(outSampleType.getPrimitiveCode()){
            case BITS1 :
                for (int i=0;i<outNbSample;i++) ((boolean[]) outSamples)[i] = inSamples[i] != 0;
                break;
            case BITS2 :
            case BITS4 :
            case INT8 :
                for (int i=0;i<outNbSample;i++) ((byte[]) outSamples)[i] = (byte) Maths.clamp(inSamples[i],-128,127);
                break;
            case UINT8 :
                for (int i=0;i<outNbSample;i++) ((int[]) outSamples)[i] = (int) Maths.clamp(inSamples[i],0,255);
                break;
            case INT16 :
                for (int i=0;i<outNbSample;i++) ((short[]) outSamples)[i] = (short) Maths.clamp(inSamples[i],Short.MIN_VALUE,Short.MAX_VALUE);
                break;
            case UINT16 :
                for (int i=0;i<outNbSample;i++) ((int[]) outSamples)[i] = (int) Maths.clamp(inSamples[i],0,65535);
                break;
            case INT32 :
                for (int i=0;i<outNbSample;i++) ((int[]) outSamples)[i] = (int) inSamples[i];
                break;
            case UINT32 :
                for (int i=0;i<outNbSample;i++) ((long[]) outSamples)[i] = (long) inSamples[i];
                break;
            case INT64 :
                for (int i=0;i<outNbSample;i++) ((long[]) outSamples)[i] = (long) inSamples[i];
                break;
            case FLOAT32 :
                for (int i=0;i<outNbSample;i++) ((float[]) outSamples)[i] = (float) inSamples[i];
                break;
            case FLOAT64 :
                outSamples = inSamples;
                break;
            default: throw new UnimplementedException("Unusual data type.");
        }
    }

}
