
package science.unlicense.image.api.color;

/**
 * Abstract blending.
 * Redirect all calls to blend and blendPremul methods with arrays.
 *
 * @author Johann Sorel
 */
public abstract class AbstractBlending implements Blending{

    /**
     * {@inheritDoc }
     */
    @Override
    public Color blend(Color src, Color dst){
        return new ColorRGB(blend(src.toRGBA(), dst.toRGBA(), new float[4]), false);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int blend(int src, int dst){
        final float[] srcRgba = Colors.toRGBA(src, (float[]) null);
        final float[] dstRgba = Colors.toRGBA(dst, (float[]) null);
        final float[] resRgba = blend(srcRgba, dstRgba, new float[4]);
        return Colors.toARGB(resRgba);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int blendPreMul(int src, int dst){
        final float[] srcRgba = Colors.toRGBA(src, (float[]) null);
        final float[] dstRgba = Colors.toRGBA(dst, (float[]) null);
        final float[] resRgba = blendPreMul(srcRgba, dstRgba, new float[4]);
        return Colors.toARGB(resRgba);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public float[] blend(float[] src, float[] dst, float[] buffer){
        return blend(src,0,dst,0,buffer,0,1);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public float[] blendPreMul(float[] src, float[] dst, float[] buffer){
        return blendPreMul(src,0,dst,0,buffer,0,1);
    }

}
