
package science.unlicense.image.api.model;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.math.api.system.SampleSystem;

/**
 * Raw model where samples do not use regular sizes, such as RGB565 with model
 * sample 0 : 5bits
 * sample 1 : 6bits
 * sample 2 : 5bits
 *
 * The sample type is mapped to UINT.
 *
 * @author Johann Sorel
 */
public class BitPackModel extends AbstractImageModel {

    private final int tupleBitSize;
    private final int[] sampleOffsets;
    private final int[] sampleNbBits;

    public BitPackModel(SampleSystem ss, int tupleBitSize, int[] sampleOffsets, int[] sampleNbBits) {
        super(ss);
        this.tupleBitSize = tupleBitSize;
        this.sampleOffsets = sampleOffsets;
        this.sampleNbBits = sampleNbBits;
    }

    public int getSampleCount() {
        return sampleOffsets.length;
    }

    @Override
    public NumberType getNumericType() {
        return Int32.TYPE;
    }

    @Override
    public Buffer createBuffer(Extent.Long dimensions, BufferFactory factory) {
        return createBuffer(dimensions, tupleBitSize, factory);
    }

    @Override
    public TupleGrid asTupleBuffer(Image image) {
        return new BitPackTupleGrid(image.getDataBuffer(), image.getExtent(), tupleBitSize, sampleOffsets, sampleNbBits, sampleSystem);
    }

    private static long getSize(Extent.Long dimensions, int tupleBitSize){
        long size = dimensions.getL(0);
        for (int i=1;i<dimensions.getDimension();i++){
            size *= dimensions.getL(i);
        }
        size *= tupleBitSize;
        return (size+7)/8;
    }

    static Buffer createBuffer(Extent.Long dimensions, int tupleBitSize, BufferFactory factory){
        return factory.createInt8(getSize(dimensions, tupleBitSize)).getBuffer();
    }

}
