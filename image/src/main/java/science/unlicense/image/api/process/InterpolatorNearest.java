
package science.unlicense.image.api.process;

import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.tuple.DefaultTupleSpaceCursor;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.impl.VectorNf64;


/**
 * Interpolator are used to average samples between pixels.
 * When scaling images, this result in smoother images.
 *
 * @author Johann Sorel
 */
public class InterpolatorNearest implements TupleSpace {

    private final TupleSpace parent;

    public InterpolatorNearest(TupleSpace parent) {
        this.parent = parent;
    }

    @Override
    public ArithmeticType getNumericType() {
        return parent.getNumericType();
    }

    @Override
    public Geometry getCoordinateGeometry() {
        return parent.getCoordinateGeometry();
    }

    @Override
    public SampleSystem getSampleSystem() {
        return parent.getSampleSystem();
    }

    @Override
    public SampleSystem getCoordinateSystem() {
        return parent.getCoordinateSystem();
    }

    @Override
    public TupleRW createTuple() {
        return parent.createTuple();
    }

    @Override
    public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
        final VectorRW icoord = VectorNf64.create(coordinate);
        final int cnt = icoord.getSampleCount();
        switch (cnt) {
            case 5 : icoord.set(5, (int) (coordinate.get(5)+0.5));
            case 4 : icoord.set(4, (int) (coordinate.get(4)+0.5));
            case 3 : icoord.set(3, (int) (coordinate.get(3)+0.5));
            case 2 : icoord.set(2, (int) (coordinate.get(2)+0.5));
            case 1 : icoord.set(1, (int) (coordinate.get(1)+0.5));
                     break;
            default :
                for (int i=0;i<cnt;i++) {
                    icoord.set(i, (int) (coordinate.get(i)+0.5));
                }
        }
        return parent.getTuple(icoord, buffer);
    }

    @Override
    public TupleSpaceCursor cursor() {
        return new DefaultTupleSpaceCursor(this);
    }

}
