
package science.unlicense.image.api.color;

import science.unlicense.math.api.Tuple;

/**
 * Color object, composed of separate red, green, blue and alpha components.
 *
 * @author Johann Sorel
 */
public interface Color extends Tuple {

    public static final Color WHITE         = new ColorRGB(1f, 1f, 1f, 1f);
    public static final Color GRAY_LIGHT    = new ColorRGB(0.75f, 0.75f, 0.75f, 1f);
    public static final Color GRAY_NORMAL   = new ColorRGB(0.5f, 0.5f, 0.5f, 1f);
    public static final Color GRAY_DARK     = new ColorRGB(0.25f, 0.25f, 0.25f, 1f);
    public static final Color BLACK         = new ColorRGB(0f, 0f, 0f, 1f);
    public static final Color RED           = new ColorRGB(1f, 0f, 0f, 1f);
    public static final Color GREEN         = new ColorRGB(0f, 1f, 0f, 1f);
    public static final Color BLUE          = new ColorRGB(0f, 0f, 1f, 1f);
    public static final Color TRS_WHITE     = new ColorRGB(1f, 1f, 1f, 0f);
    public static final Color TRS_BLACK     = new ColorRGB(0f, 0f, 0f, 0f);

    /**
     * Color color system.
     * @return ColorSystem never null
     */
    @Override
    ColorSystem getSampleSystem();

    /**
     * Color samples in color space units.
     * @param buffer if null a new array will be created
     * @return float[]
     */
    float[] getSamples(float[] buffer);

    /**
     * Get red component.
     * @return red component [0...1]
     */
    float getRed();

    /**
     * Get green component.
     * @return green component [0...1]
     */
    float getGreen();

    /**
     * Get blue component.
     * @return blue component [0...1]
     */
    float getBlue();

    /**
     * Get alpha component.
     * @return alpha component [0...1]
     */
    float getAlpha();

    /**
     * Get RGB components as float array.
     * Alpha value is ignored.
     *
     * @return float array RGB
     */
    float[] toRGB();

    /**
     * Get RGB components as float array.
     * Alpha value is ignored.
     *
     * @param buffer to write into, not null
     * @param offset start index where to write color
     * @return float array RGB
     */
    float[] toRGB(float[] buffer, int offset);

    /**
     * Get RGBA components as float array.
     * Alpha not premultiplied.
     *
     * @return float array RGBA
     */
    float[] toRGBA();

    /**
     * Get RGBA components as float array.
     * Alpha not premultiplied.
     *
     * @param buffer to write into, not null
     * @param offset start index where to write color
     * @return float array RGB
     */
    float[] toRGBA(float[] buffer, int offset);

    /**
     * Get RGBA components as float array.
     * Alpha premultiplied.
     *
     * @return float array RGBA
     */
    float[] toRGBAPreMul();

    /**
     * Get RGBA components as float array.
     * Alpha premultiplied.
     *
     * @param buffer to write into, not null
     * @param offset start index where to write color
     * @return float array RGB
     */
    float[] toRGBAPreMul(float[] buffer, int offset);

    /**
     * Encode color on an int, 8 bits per component in order ARGB.
     * Alpha not premultiplied.
     *
     * @return int ARGB
     */
    int toARGB();

    /**
     * Encode color on an int, 8 bits per component in order ARGB.
     * Alpha premultiplied.
     *
     * @return int ARGB
     */
    int toARGBPreMul();

    /**
     * Convert color samples to target color system.
     *
     * @param cs target system
     * @return converted color
     */
    Color toColorSystem(ColorSystem cs);

    /**
     *
     * @param other
     * @param cs compare ColorSystem, if null use current color color sytem.
     * @param tolerance
     * @return
     */
    boolean equals(Color other, ColorSystem cs, double tolerance);

    /**
     * Test exact color equality including color system and samples.
     *
     * @param other
     * @return
     */
    boolean equalsExact(Color other);
}
