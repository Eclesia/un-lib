
package science.unlicense.image.api.color;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.VectorNf32;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class DefaultColorRW extends AbstractColor implements ColorRW {

    private final float[] components;

    public DefaultColorRW(ColorSystem colorSystem) {
        this(new float[colorSystem.getNumComponents()], colorSystem);
    }

    public DefaultColorRW(float[] values, ColorSystem colorSystem) {
        super(colorSystem);
        if (values.length != colorSystem.getNumComponents()) {
            throw new InvalidArgumentException("ColorSystem number of components ("+this.colorSystem.getNumComponents()+")differ from values length ("+values.length+")");
        }
        this.components = values;
    }

    @Override
    public NumberType getNumericType() {
        return Float32.TYPE;
    }

    @Override
    public double get(int indice) throws InvalidIndexException {
        return components[indice];
    }

    @Override
    public Number getNumber(int indice) throws InvalidIndexException {
        return new Float32(components[indice]);
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float[] getSamples(float[] buffer) {
        if (buffer == null) {
            return components.clone();
        } else {
            System.arraycopy(components, 0, buffer, 0, components.length);
            return buffer;
        }
    }

    @Override
    public void set(Color color) {
        final ColorSystem ccs = color.getSampleSystem();
        if (colorSystem.equals(ccs)) {
            set((Tuple) color);
        } else {
            final Transform tr = ccs.getTranform(colorSystem);
            tr.transform(color, this);
        }
    }

    @Override
    public void fromARGB(int argb) {
        set(new ColorRGB(argb));
    }

    @Override
    public void setAll(double v) {
        Arrays.fill(components, (float) v);
    }

    @Override
    public void set(int indice, double value) throws InvalidIndexException {
        components[indice] = (float) value;
    }

    @Override
    public void set(int indice, Arithmetic value) throws InvalidIndexException {
        components[indice] = ((Number) value).toFloat();
    }

    @Override
    public void set(Tuple toCopy) {
        for (int i=0;i<components.length;i++) {
            components[i] = (float) toCopy.get(i);
        }
    }

    @Override
    public void set(double[] values) {
        for (int i=0;i<components.length;i++) {
            components[i] = (float) values[i];
        }
    }

    @Override
    public void set(float[] values) {
        System.arraycopy(components, 0, values, 0, components.length);
    }

    @Override
    public TupleRW create(int size) {
        return VectorNf32.createFloat(size);
    }

}
