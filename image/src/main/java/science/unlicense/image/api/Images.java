
package science.unlicense.image.api;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.TextTable;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.concurrent.api.Formats;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Resources;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.AbstractTupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.image.api.model.InterpolatedColorModel;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.system.Work;
import science.unlicense.system.WorkUnits;

/**
 * Convinient methods for image manipulation.
 *
 * @author Johann Sorel
 */

public final class Images {

    public static final int IMAGE_TYPE_RGB                  = 0;
    public static final int IMAGE_TYPE_RGBA                 = 1;
    public static final int IMAGE_TYPE_RGBA_PREMULTIPLIED   = 2;
    public static final int IMAGE_TYPE_RGBA_FLOAT           = 4;

    private Images(){}

    /**
     * Create a ND image with an arbitrary number of bands.
     *
     * @param extent image size, must be positive
     * @param nbSample number of bands/samples in the image
     * @param sampleType samples type.
     * @return Image, never null
     */
    public static Image createCustomBand(Extent.Long extent, int nbSample, NumberType sampleType){
        return createCustomBand(extent, nbSample, sampleType, null);
    }

    /**
     * Create a ND image with an arbitrary number of bands.
     *
     * @param extent image size, must be positive
     * @param nbSample number of bands/samples in the image
     * @param sampleType samples type.
     * @param bufferFactory image buffer factory, can be null
     * @return Image, never null
     */
    public static Image createCustomBand(Extent.Long extent, int nbSample, NumberType sampleType, BufferFactory bufferFactory){
        if (bufferFactory==null) bufferFactory = DefaultBufferFactory.INSTANCE;
        final ImageModel rm = new InterleavedModel(new UndefinedSystem(nbSample), sampleType);
        final Buffer buffer = rm.createBuffer(extent,bufferFactory);
        final ImageModel cm = DerivateModel.create(rm, new int[]{0,0,0}, null, null,ColorSystem.RGB_8BITS);
        return new DefaultImage(buffer, extent, rm, cm);
    }

    public static Image create(Extent.Long extent, int imageType){
        return create(extent,imageType,DefaultBufferFactory.INSTANCE);
    }

    public static Image create(Extent.Long extent, int imageType, BufferFactory factory){
        if (factory==null) factory = DefaultBufferFactory.INSTANCE;

        final ImageModel rm;
        final ImageModel cm;
        switch(imageType){
            case IMAGE_TYPE_RGB :
                rm = new InterleavedModel(ColorSystem.RGB_8BITS, UInt8.TYPE);
                cm = rm;
                break;
            case IMAGE_TYPE_RGBA :
                rm = new InterleavedModel(ColorSystem.RGBA_8BITS, UInt8.TYPE);
                cm = rm;
                break;
            case IMAGE_TYPE_RGBA_PREMULTIPLIED :
                rm = new InterleavedModel(ColorSystem.RGBA_8BITS_PREMUL, UInt8.TYPE);
                cm = rm;
                break;
            case IMAGE_TYPE_RGBA_FLOAT :
                rm = new InterleavedModel(ColorSystem.RGBA_FLOAT, Float32.TYPE);
                cm = rm;
                break;
            default: throw new InvalidArgumentException("Unknowned image type : "+imageType);
        }
        final Buffer buffer = rm.createBuffer(extent,factory);
        return new DefaultImage(buffer, extent, rm, cm);
    }

    /**
     * Copy an image.
     * @return Image
     */
    public static Image copy(Image image){
        final Extent.Long extent = image.getExtent().copy();
        final Buffer buffer = image.getDataBuffer().copy();
        return new DefaultImage(buffer, extent, image.getModels(), image.getMetadatas());
    }

    /**
     * Create a new image with the same properties as given image but with a different
     * size.
     * @param image
     * @param dimensions
     * @return Image
     */
    public static Image create(Image image, Extent dimensions){
        final long[] ext = new long[dimensions.getDimension()];
        for (int i=0;i<ext.length;i++) ext[i] = (int) dimensions.get(i);
        return create(image, ext);
    }
    /**
     * Create a new image with the same properties as given image but with a different
     * size.
     * @param image
     * @param dimensions
     * @return Image
     */
    public static Image create(Image image, long[] dimensions){
        final Extent.Long extent = new Extent.Long(dimensions);

        //TODO find a way to preserve buffer type.
        final ImageModel rm = image.getRawModel();
        final Buffer buffer = rm.createBuffer(extent);
        return new DefaultImage(buffer, extent, image.getModels(), image.getMetadatas());
    }

    /**
     * Lists available image formats.
     * @return array of ImageFormat, never null but can be empty.
     */
    public static ImageFormat[] getFormats() {
        return (ImageFormat[]) Formats.getFormatsOfType(ImageFormat.class);
    }

    public static ImageFormat getFormat(Chars name) throws InvalidArgumentException{
        final ImageFormat[] formats = getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i].getIdentifier().equals(name)){
                return formats[i];
            }
        }
        throw new InvalidArgumentException("Format "+name+" not found.");
    }

    public static ImageFormat getFormatForExtension(Chars ext) throws InvalidArgumentException{
        final ImageFormat[] formats = getFormats();
        for (int i=0;i<formats.length;i++){
            final Iterator ite = formats[i].getExtensions().createIterator();
            while (ite.hasNext()) {
                final Chars e = (Chars) ite.next();
                if (e.equals(ext, true, true)){
                    return formats[i];
                }
            }
        }
        throw new InvalidArgumentException("Format "+ext+" not found.");
    }

    /**
     * Convinient method to open a image of unknowned format.
     * The method will loop on available formats until one can decode the source.
     *
     * @param input
     * @return Store, never null
     * @throws IOException if not format could support the source.
     */
    public static Store open(Object input) throws IOException {
        return Formats.open(input, new ArraySequence(getFormats()));
    }

    static BacktrackInputStream toInputStream(Object input, boolean[] mustClose) throws IOException{
        BacktrackInputStream stream;
        if (input instanceof ByteInputStream){
            stream = new BacktrackInputStream((ByteInputStream) input);
            //we did not create the stream, we do not close it.
            mustClose[0] = false;
            stream.mark();
            return stream;
        } else if (input instanceof Path){
            stream = new BacktrackInputStream(((Path) input).createInputStream());
            mustClose[0] = true;
            stream.mark();
            return stream;
        } else {
            throw new IOException("Unsupported input : "+input);
        }
    }

    static ByteOutputStream toOutputStream(Object output, boolean[] mustClose) throws IOException{
        ByteOutputStream stream;
        if (output instanceof ByteOutputStream){
            stream = (ByteOutputStream) output;
            //we did not create the stream, we do not close it.
            mustClose[0] = false;
            return stream;
        } else if (output instanceof Path){
            stream = ((Path) output).createOutputStream();
            mustClose[0] = true;
            return stream;
        } else {
            throw new IOException("Unsupported output : "+output);
        }
    }

    /**
     * Convinient method to read an image source of unknowned format.
     * The method will loop on available formats until one can decode the source.
     *
     * @param input
     * @return Image, never null
     * @throws IOException if not format could support the source.
     */
    public static Image read(Object input) throws IOException {
        final Store store = open(input);
        final ImageResource imgRes;
        try {
            imgRes = (ImageResource) Resources.findFirst(store, ImageResource.class);
        } catch (StoreException ex) {
            throw new IOException(ex.getMessage(), ex);
        }
        if (imgRes == null) {
            throw new IOException("No image resource found.");
        }

        final ImageReader reader = imgRes.createReader();
        try {
            return reader.read(null);
        } finally {
            reader.dispose();
        }
    }

    /**
     * Convenient method to write an image.
     * This method will search for the format with given name and trye to write
     * the image.
     *
     * @param image
     * @param format
     * @param output
     * @throws IOException
     */
    public static void write(Image image, Chars format, Object output) throws IOException {
        final ImageFormat[] formats = getFormats();
        if (formats.length == 0) {
            throw new IOException("No image formats available.");
        }

        final boolean[] mustclose = new boolean[1];
        final ByteOutputStream stream = toOutputStream(output, mustclose);
        try {
            for (int i=0;i<formats.length;i++){
                if (formats[i].supportWriting() && formats[i].getIdentifier().equals(format, true, true)) {
                    final Store store = formats[i].open(output);
                    ImageWriter writer = null;
                    try {
                        final ImageResource res = (ImageResource) Resources.findFirst(store, ImageResource.class);
                        writer = res.createWriter();
                        final ImageWriteParameters params = writer.createParameters();
                        writer.setOutput(stream);
                        writer.write(image, params);
                    } finally {
                        if (writer != null) {
                            writer.dispose();
                        }
                        store.dispose();
                    }
                    return;
                }
            }
        } catch(StoreException ex) {
            throw new IOException(ex.getMessage(), ex);
        } finally {
            if (mustclose[0]) {
                stream.close();
            }
        }

        throw new IOException("No writer available for given format.");
    }

    /**
     * Create a printable grid of the image data in bounding box.
     * Ensure the region isn't to large.
     *
     * @param image 2D image only
     * @param ext printed area
     * @return
     */
    public static Chars toChars(Image image, Rectangle ext){

        if (ext==null){
            ext = new Rectangle(0, 0, image.getExtent().get(0), image.getExtent().get(1));
        }

        final TextTable writer = new TextTable();

        final ImageModel sm = image.getRawModel();
        final TupleGrid tb = sm.asTupleBuffer(image);
        final TupleRW buffer = tb.createTuple();
        final Vector2i32 coord = new Vector2i32();
        int xn = (int) ext.getWidth();
        int yn = (int) ext.getHeight();
        for (coord.x=0;coord.x<xn;coord.x++){
            writer.newLine();
            for (coord.y=0;coord.y<yn;coord.y++){
                tb.getTuple(coord, buffer);
                writer.appendCell(Arrays.toChars(buffer));
            }
        }

        return writer.toChars();
    }

    public static TupleGrid gridView(TupleSpace space, Extent.Long extent) {
        return new AbstractTupleGrid(extent, space.getNumericType(), space.getSampleSystem()) {

            @Override
            public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
                return space.getTuple(coordinate, buffer);
            }

            @Override
            public void setTuple(Tuple coordinate, Tuple buffer) {
                throw new UnsupportedOperationException("This grid is not writable.");
            }
        };
    }

    /**
     * Sample tuple space in target grid.
     *
     * @param source
     * @param target
     */
    public static void sample(TupleSpace source, TupleGrid target) {

        final Extent.Long extent = target.getExtent();
        if (extent.getDimension() != 2) {
            throw new InvalidArgumentException("Target size must be 2D.");
        }

        long sizeX = extent.getL(0);
        long sizeY = extent.getL(1);

        final WorkUnits workers = science.unlicense.system.System.get().createWorkUnits(50);
        workers.submit(sizeX*sizeY, new Work() {
            @Override
            public void run(long index) {
                final long x = (index % sizeX);
                final long y = (index / sizeX);
                final Vector2f64 pos = new Vector2f64(x, y);
                final TupleRW px = source.getTuple(pos, source.createTuple());
                target.setTuple(pos, px);
            }
        });
        workers.dispose();
    }

    /**
     * Copy tuple spaces values in a new Image.
     * if space is not a color model, one will be created automaticaly
     * by finding the minimum and maximum values in the space.
     *
     * If tuple space type is not a number type then datas will be
     * evaluated to Float64 type.
     *
     * @param space
     * @param extent
     * @return
     */
    public static Image copyToImage(TupleSpace space, Extent.Long extent) {
        final ArithmeticType tstype = space.getNumericType();
        final NumberType type;
        if (tstype instanceof NumberType) {
            type = (NumberType) tstype;
        } else {
            type = Float64.TYPE;
        }

        final SampleSystem ss = space.getSampleSystem();
        long size = ss.getNumComponents();
        for (int i=0; i < extent.getDimension(); i++) {
            size *= extent.getL(i);
        }

        final ImageModel rm = new InterleavedModel(ss, type);
        ImageModel cm = null;
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        if (ss instanceof ColorSystem) {
            cm = rm;
        }

        final Buffer buffer = DefaultBufferFactory.INSTANCE.create(size, type, Endianness.BIG_ENDIAN);
        final Image image = new DefaultImage(buffer, extent, rm);

        //fill values
        final TupleGridCursor cursor = image.getTupleBuffer(rm).cursor();
        while (cursor.next()) {
            final Tuple c = cursor.coordinate();
            space.getTuple(c, cursor.samples());
            if (cm == null) {
                double v = cursor.samples().get(0);
                if (!Double.isNaN(v)) {
                    min = Maths.min(v, min);
                    max = Maths.max(v, max);
                }
            }
        }

        if (cm == null) {
            cm = new InterpolatedColorModel(rm, new Color[]{ColorRGB.BLACK,ColorRGB.WHITE}, new float[]{(float) min,(float) max});
        }
        image.getModels().add(ImageModel.MODEL_COLOR, cm);

        return image;
    }
}
