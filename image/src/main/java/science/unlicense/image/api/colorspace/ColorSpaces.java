
package science.unlicense.image.api.colorspace;

import science.unlicense.system.ModuleSeeker;

/**
 * Utilities methodes for colorspaces.
 *
 * @author Johann Sorel
 */
public final class ColorSpaces {

    private ColorSpaces(){}

    /**
     * Lists available color spaces.
     * @return array of ColorSpace, never null but can be empty.
     */
    public static ColorSpace[] getColorSpaces(){
        return (ColorSpace[]) ModuleSeeker.findServices(ColorSpace.class);
    }

}
