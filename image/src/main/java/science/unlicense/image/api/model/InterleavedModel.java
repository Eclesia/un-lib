
package science.unlicense.image.api.model;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.geometry.api.tuple.AbstractNumberTupleGrid;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.common.api.number.NumberType;

/**
 * Usual two dimension images mostly share a common image structure once
 * uncompressed. This class should suffice for image with interleaved samples.
 *
 * ImageBank is expected to be ordered line by line, each sample next to another :
 * line 0 : [s1][s2][sN][s1][s2][sN] ...
 * line 1 : [s1][s2][sN][s1][s2][sN] ...
 * ...
 *
 * If image has more then 2 dimensions, datas are expected to be ordered by successive
 * slices of the two first dimensions starting from the first dimension. 4D example :
 * Slice : [0,0,0,0]
 * Slice : [0,0,1,0]
 * Slice : [0,0,Z,0]
 * Slice : [0,0,0,1]
 * Slice : [0,0,1,1]
 * Slice : [0,0,Z,1]
 * Slice : [0,0,0,W]
 * Slice : [0,0,1,W]
 * Slice : [0,0,Z,W]
 *
 * @author Johann Sorel
 */
public class InterleavedModel extends AbstractImageModel {

    private final NumberType sampleType;
    private final int nbSample;

    public InterleavedModel(SampleSystem ss, NumberType sampleType) {
        super(ss);
        this.sampleType = sampleType;
        this.nbSample = ss.getNumComponents();
    }

    @Override
    public NumberType getNumericType(){
        return sampleType;
    }

    public int getSampleCount() {
        return nbSample;
    }

    public TupleGrid asTupleBuffer(Image image) {
        return InterleavedTupleGrid.create(image.getDataBuffer(),sampleType,sampleSystem,image.getExtent());
    }

    public Buffer createBuffer(Extent.Long dimensions, BufferFactory factory) {
        return AbstractNumberTupleGrid.createPrimitiveBuffer(dimensions, sampleType, nbSample,factory);
    }

}
