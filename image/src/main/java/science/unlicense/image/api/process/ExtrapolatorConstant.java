
package science.unlicense.image.api.process;

import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 * Extend image filled pixels with a constant value.
 * For example if we add value 5 to a one channel image we obtain ;
 * <pre>
 *              5 5 5 5 5
 * 3 3 3        5 3 3 3 5
 * 3 3 3  ===>  5 3 3 3 5
 * 3 3 3        5 3 3 3 5
 *              5 5 5 5 5
 * </pre>
 *
 * @author Humbert Florent
 * @author Johann Sorel
 */
public class ExtrapolatorConstant extends AbstractExtrapolator {

    private final Tuple values;

    public ExtrapolatorConstant(TupleGrid parent, double value) {
        super(parent);
        values = parent.createTuple();
        ((TupleRW) values).setAll(value);
    }

    public ExtrapolatorConstant(TupleGrid parent, Tuple values) {
        super(parent);
        this.values = values;
    }

    @Override
    protected TupleRW evaluateOutside(TupleRW coordinate, TupleRW buffer) {
        buffer.set(values);
        return buffer;
    }

    @Override
    public TupleSpaceCursor cursor() {
        return new Cursor(parent.cursor());
    }

    private class Cursor extends ExtrapolatorCursor {

        public Cursor(TupleSpaceCursor base) {
            super(base);
        }

        @Override
        protected Tuple getOutsideTuple(Tuple coordinate) {
            return values;
        }

    }
}
