
package science.unlicense.image.api.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.AbstractNumberTupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.Colors;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;

/**
 * Interpolated color model.
 * Interpolate colors from a palette similar to a linear gradiant.
 *
 * @author Johann Sorel
 */
public class InterpolatedColorModel extends AbstractImageModel {

    private final ImageModel base;
    private final NumberType sampleType;
    private final Color[] palette;
    private final float[] steps;

    public InterpolatedColorModel(ImageModel base, Color[] palette, float[] steps) {
        super(palette[0].getSampleSystem());
        this.base = base;
        this.sampleType = Float32.TYPE;
        this.palette = palette;
        this.steps = steps;
    }

    /**
     * Colors palette.
     * @return array never null, copy of the palette.
     */
    public Color[] getPalette() {
        return Arrays.copy(palette, new Color[palette.length]);
    }

    /**
     * Colors steps.
     * @return array never null, copy of the steps.
     */
    public float[] getSteps() {
        return Arrays.copy(steps, new float[steps.length]);
    }

    @Override
    public NumberType getNumericType() {
        return sampleType;
    }

    @Override
    public Buffer createBuffer(Extent.Long dimensions, BufferFactory factory) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public TupleGrid asTupleBuffer(Image image) {
        return new Grid(base.asTupleBuffer(image));
    }

    private class Grid extends AbstractNumberTupleGrid {

        private final TupleGrid tbase;

        public Grid(TupleGrid tbase) {
            this.tbase = tbase;
            updateModel(tbase.getExtent(), sampleType, InterpolatedColorModel.this.sampleSystem.getNumComponents());
        }

        @Override
        public SampleSystem getSampleSystem() {
            return InterpolatedColorModel.this.getSampleSystem();
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final TupleRW tuple = tbase.createTuple();
            tbase.getTuple(coordinate, tuple);

            double d = tuple.get(0);
            //find interval
            //avoid under/above out of range
            d = Maths.clamp(d, steps[0], steps[steps.length-1]);
            int index=0;
            for (;index<steps.length-1;index++){
                if (d>= steps[index] && d<=steps[index+1]){
                    break;
                }
            }
            d = (d-steps[index]) / (steps[index+1]-steps[index]);

            Color interpolate = Colors.interpolate(palette[index], palette[index+1], (float) d);
            buffer.set(interpolate);
            return buffer;
        }

        @Override
        public void setTuple(Tuple coordinate, Tuple buffer) {
            throw new UnsupportedOperationException("Not supported.");
        }

    }

}
