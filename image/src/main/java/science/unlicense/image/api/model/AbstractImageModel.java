
package science.unlicense.image.api.model;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.system.SampleSystem;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractImageModel implements ImageModel {

    protected SampleSystem sampleSystem;

    public AbstractImageModel(SampleSystem sampleSystem) {
        CObjects.ensureNotNull(sampleSystem, "Sample system");
        this.sampleSystem = sampleSystem;
    }

    @Override
    public Chars getName() {
        return null;
    }

    @Override
    public SampleSystem getSampleSystem() {
        return sampleSystem;
    }

    @Override
    public Buffer createBuffer(Extent.Long dimensions) {
        return createBuffer(dimensions, DefaultBufferFactory.INSTANCE);
    }

}
