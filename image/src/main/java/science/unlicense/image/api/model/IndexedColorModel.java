
package science.unlicense.image.api.model;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.number.NumberType;
import static science.unlicense.common.api.number.Primitive.*;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.AbstractNumberTupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 * Indexed color model.
 * Each possible value in the image is mapped to a color.
 *
 * @author Johann Sorel
 */
public class IndexedColorModel extends AbstractImageModel {

    private final ImageModel baseModel;
    private final NumberType ntype;
    private final int sampleType;
    private final ColorIndex index;

    public IndexedColorModel(ImageModel baseModel, NumberType sampleType, ColorIndex index) {
        super(index.palette[0].getSampleSystem());
        this.baseModel = baseModel;
        this.ntype = sampleType;
        this.sampleType = sampleType.getPrimitiveCode();
        this.index = index;
    }

    /**
     * Colors palette.
     * @return array never null, copy of the palette.
     */
    public ColorIndex getPalette() {
        return index;
    }

    public Color toColor(Object sample) {
        int index;
        if (   BITS1 == sampleType || BITS2 == sampleType
           || BITS4 == sampleType || INT8 == sampleType){
            final byte[] samples = (byte[]) sample;
            index = samples[0] & 0xFF;
        } else if (UINT8 == sampleType ){
            final int[] samples = (int[]) sample;
            index = samples[0] & 0xFF;
        } else if (INT16 == sampleType){
            final short[] samples = (short[]) sample;
            index = samples[0] & 0xFF;
        } else if (UINT16 == sampleType ||INT32 == sampleType){
            final int[] samples = (int[]) sample;
            index = samples[0];
        } else if (UINT32 == sampleType ||INT64 == sampleType){
            final long[] samples = (long[]) sample;
            index = (int) samples[0];
        } else if (FLOAT32 == sampleType){
            final float[] samples = (float[]) sample;
            index = (int) samples[0];
        } else if (FLOAT64 == sampleType){
            final double[] samples = (double[]) sample;
            index = (int) samples[0];
        } else {
            throw new RuntimeException("Unsupported sample type "+sampleType);
        }

        return this.index.palette[index];
    }

    public int toColorARGB(Object sample) {
        return toColor(sample).toARGB();
    }

    public Object toSample(Color color) {
        for (int i=0;i<this.index.palette.length;i++){
            if (this.index.palette[i].equals(color)){
                return i;
            }
        }
        throw new RuntimeException("Sample value is not in available palette");
    }

    public Object toSample(int color) {
        return toSample(new ColorRGB(color));
    }

    @Override
    public Buffer createBuffer(Extent.Long dimensions, BufferFactory factory) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public NumberType getNumericType() {
        return ntype;
    }

    @Override
    public TupleGrid asTupleBuffer(Image image) {
        return new IndexedTupleGrid(baseModel.asTupleBuffer(image));
    }

    private class IndexedTupleGrid extends AbstractNumberTupleGrid {

        private final TupleGrid base;

        public IndexedTupleGrid(TupleGrid base) {
            super(base.getExtent(), IndexedColorModel.this.ntype, IndexedColorModel.this.sampleSystem);
            this.base = base;
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            final TupleRW t = base.createTuple();
            base.getTuple(coordinate, t);
            buffer.set(IndexedColorModel.this.index.palette[(int) t.get(0)]);
            return buffer;
        }

        @Override
        public void setTuple(Tuple coordinate, Tuple buffer) {
            for (int i=0;i<IndexedColorModel.this.index.palette.length;i++){
                if (IndexedColorModel.this.index.palette[i].equals(buffer)){
                    final TupleRW t = base.createTuple();
                    base.getTuple(coordinate, t);
                    t.set(0, i);
                    base.setTuple(coordinate, t);
                    return;
                }
            }
            throw new RuntimeException("Sample value is not in available palette");
        }

    }

}
