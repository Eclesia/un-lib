
package science.unlicense.image.api.process;

import science.unlicense.common.api.model.doc.Document;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Image operator where output sample value can be calculated with
 * only the input sample value.
 *
 * Override evaluate(TupleRW sample, TupleRW result) to work by pixel
 * Override evaluate(double sample) to work by sample
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public abstract class AbstractPointOperator extends AbstractPreservingOperator{

    public AbstractPointOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    /// when iterating
    private VectorRW inSamples;
    private VectorRW inSamplesProcessed;
    private TupleRW outSamples;

    public Document perform() {
        super.perform();
        prepareInputs();

        //loop on all pixels
        int k;
        inSamples = VectorNf64.createDouble(nbSample);
        inSamplesProcessed = VectorNf64.createDouble(nbSample);
        outSamples = inputTuples.createTuple();
        final TupleGridCursor cursor = inputTuples.cursor();
        while (cursor.next()) {
            final Tuple coord = cursor.coordinate();
            evaluate(cursor.samples(), inSamplesProcessed);

            //clip values to image sample type
            //adjustInOutSamples(inSamplesProcessed, outSamples);

            //fill out image pixel
            outputTuples.setTuple(coord, outSamples);
        }

        return outputParameters;
    }

    protected void prepareInputs(){}

    protected void evaluate(TupleRW sample, TupleRW result){
        for (int k=0;k<nbSample;k++){
            inSamplesProcessed.set(k, evaluate(inSamples.get(k)));
        }
    }

    protected double evaluate(double value){
        return value;
    }

}
