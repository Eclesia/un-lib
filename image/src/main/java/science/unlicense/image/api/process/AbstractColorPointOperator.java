
package science.unlicense.image.api.process;

import science.unlicense.common.api.model.doc.Document;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ImageModel;

/**
 * Image operator where output color can be calculated with the input color.
 *
 * @author Johann Sorel
 */
public abstract class AbstractColorPointOperator extends AbstractPreservingOperator{

    public AbstractColorPointOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    public Document perform() {
        workingModel = ImageModel.MODEL_COLOR;
        super.perform();
        prepareInputs();


        final ColorSystem cs = (ColorSystem) inputModel.getSampleSystem();

        //loop on all pixels
        final TupleGridCursor cursor = inputTuples.cursor();
        while (cursor.next()) {
            Color color = new ColorRGB(cursor.samples(), cs);
            color = evaluate(color);

            //fill out image pixel
            outputTuples.setTuple(cursor.coordinate(), color);
        }

        return outputParameters;
    }

    protected void prepareInputs(){
    }

    protected abstract Color evaluate(Color color);

}
