
package science.unlicense.image.api.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.math.api.unitold.Unit;

/**
 * Define a colorspace component.
 *
 * @author Johann Sorel
 */
public class ColorSpaceComponent {

    /**
     * Opacity,Alpha,Translucent defined the physical transparency of a surface.
     * This property is not part of existing color spaces and should used only
     * by ColorCoding as a pivot point to transform alpha values.
     */
    public static final ColorSpaceComponent ALPHA = new ColorSpaceComponent(new Chars("a"), null, Primitive.FLOAT32, 0, +1);

    private final Chars name;
    private final Unit unit;
    private final int dataType;
    private final double minValue;
    private final double maxValue;

    public ColorSpaceComponent(Chars name, Unit unit, int dataType, double minValue, double maxValue) {
        this.name = name;
        this.unit = unit;
        this.dataType = dataType;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public Chars getName() {
        return name;
    }

    public Unit getUnit() {
        return unit;
    }

    public int getDataType() {
        return dataType;
    }

    public double getMinValue() {
        return minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

}
