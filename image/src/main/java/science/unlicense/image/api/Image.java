package science.unlicense.image.api;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.Tuple;

/**
 * An image is a regular multidimentional array of values.
 * Usual images are in 2 dimensions and have 3 or 4 bands mapping RGBA colors.
 *
 * @author Johann Sorel
 */
public interface Image {

    /**
     * Get image extent.
     * An image is a N dimension grid.
     *
     * @return number and size of the image
     */
    Extent.Long getExtent();

    /**
     * Images store different metadata models whitin them.
     * Each often being specific to it's own storage design.
     *
     * @return Dictionary, Chars -> TypedNode
     */
    Dictionary getMetadatas();

    /**
     * Images are N dimension raw byte buffers.
     * ImageModel produce views of the image for a given purpose.
     * See {@link ImageModel}
     *
     * @return Dictionary, Chars -> ImageModel
     */
    Dictionary getModels();

    /**
     * Get mandatory raw model.
     *
     * @return ImageModel, never null.
     */
    ImageModel getRawModel();

    /**
     * Get optional color model.
     *
     * @return ImageModel, can be null.
     */
    ImageModel getColorModel();

    /**
     * Get tuple buffer for an image model.
     *
     * @return TupleBuffer, can be null if model is null
     */
    TupleGrid getTupleBuffer(ImageModel model);

    Tuple getTuple(Tuple coordinate, ImageModel model);

    Color getColor(Tuple coordinate);

    /**
     * Raw image data buffer.
     *
     * @return Buffer
     */
    Buffer getDataBuffer();

}
