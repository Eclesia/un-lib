
package science.unlicense.image.api.process;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.Extent;
import science.unlicense.task.api.AbstractTask;
import science.unlicense.image.api.Image;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.Images;
import science.unlicense.geometry.api.tuple.TupleGrid;

/**
 * Image operator which preserve the image size and models.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public abstract class AbstractPreservingOperator extends AbstractTask{

    protected Chars workingModel = ImageModel.MODEL_RAW;

    protected Extent.Long extent;
    protected Image inputImage;
    protected ImageModel inputModel;
    protected TupleGrid inputTuples;

    protected Image outputImage;
    protected ImageModel outputModel;
    protected TupleGrid outputTuples;

    protected int nbSample;
    protected ArithmeticType sampleType;

    public AbstractPreservingOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    public Document perform() {
        inputImage = (Image) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        extent = inputImage.getExtent();
        inputModel = (ImageModel) inputImage.getModels().getValue(workingModel);
        inputTuples = inputModel.asTupleBuffer(inputImage);
        nbSample = inputTuples.getSampleSystem().getNumComponents();
        sampleType = inputTuples.getNumericType();

        //calculate the result image
        outputImage = Images.create(inputImage, extent);
        outputModel = (ImageModel) outputImage.getModels().getValue(workingModel);
        outputTuples = outputModel.asTupleBuffer(outputImage);

        outputParameters.setPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),outputImage);
        return outputParameters;
    }

//    protected void adjustInOutSamples(double[] inSamples, TupleRW outSamples){
//        switch(sampleType.getPrimitiveCode()){
//            case TYPE_1_BIT :
//                for (int i=0;i<nbSample;i++) outSamples)[i] = inSamples[i] != 0;
//                break;
//            case TYPE_2_BIT :
//            case TYPE_4_BIT :
//            case TYPE_BYTE :
//                for (int i=0;i<nbSample;i++) ((byte[]) outSamples)[i] = (byte) Maths.clamp(inSamples[i],-128,127);
//                break;
//            case TYPE_UBYTE :
//                for (int i=0;i<nbSample;i++) ((int[]) outSamples)[i] = (int) Maths.clamp(inSamples[i],0,255);
//                break;
//            case TYPE_SHORT :
//                for (int i=0;i<nbSample;i++) ((short[]) outSamples)[i] = (short) Maths.clamp(inSamples[i],Short.MIN_VALUE,Short.MAX_VALUE);
//                break;
//            case TYPE_USHORT :
//                for (int i=0;i<nbSample;i++) ((int[]) outSamples)[i] = (int) Maths.clamp(inSamples[i],0,65535);
//                break;
//            case TYPE_INT :
//                for (int i=0;i<nbSample;i++) ((int[]) outSamples)[i] = (int) inSamples[i];
//                break;
//            case TYPE_UINT :
//                for (int i=0;i<nbSample;i++) ((long[]) outSamples)[i] = (long) inSamples[i];
//                break;
//            case TYPE_LONG :
//                for (int i=0;i<nbSample;i++) ((long[]) outSamples)[i] = (long) inSamples[i];
//                break;
//            case TYPE_FLOAT :
//                for (int i=0;i<nbSample;i++) ((float[]) outSamples)[i] = (float) inSamples[i];
//                break;
//            case TYPE_DOUBLE :
//                outSamples = inSamples;
//                break;
//            default: throw new UnimplementedException("Unusual data type.");
//        }
//    }

}
