
package science.unlicense.image.api.process;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.impl.DecoratedVector;
import science.unlicense.math.impl.VectorNf64;

/**
 * Abstract Extrapolator.
 * Redirects double[] coordinates to nearest int[] coordinate.
 *
 * @author Johann Sorel
 */
public abstract class AbstractExtrapolator implements TupleSpace {

    protected final TupleGrid parent;
    protected final int[] dims;

    public AbstractExtrapolator(TupleGrid parent) {
        CObjects.ensureNotNull(parent);
        this.parent = parent;
        final Extent.Long extent = parent.getExtent();
        dims = new int[extent.getDimension()];
        for (int i=0;i<dims.length;i++){
            dims[i] = (int) extent.getL(i);
        }
    }

    @Override
    public ArithmeticType getNumericType() {
        return parent.getNumericType();
    }

    @Override
    public Geometry getCoordinateGeometry() {
        //extrapolate to infinite
        return null;
    }

    @Override
    public SampleSystem getSampleSystem() {
        return parent.getSampleSystem();
    }

    @Override
    public SampleSystem getCoordinateSystem() {
        return parent.getCoordinateSystem();
    }

    @Override
    public TupleRW createTuple() {
        return parent.createTuple();
    }

    @Override
    public final TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
        if (isOutsideImage(coordinate)){
            return evaluateOutside(VectorNf64.create(coordinate), buffer);
        } else {
            parent.getTuple(coordinate, buffer);
            return buffer;
        }
    }

    protected abstract TupleRW evaluateOutside(TupleRW coordinate, TupleRW buffer);

    protected boolean isOutsideImage(double[] coord){
        for (int i=0;i<coord.length;i++){
            int v = (int) Math.round(coord[i]);
            if (v<0 || v>dims[i]-1) return true;
        }
        return false;
    }

    protected boolean isOutsideImage(int[] coord){
        switch (dims.length) {
            case 4 : if (coord[3]<0 || coord[3]>=dims[3]) return true;
            case 3 : if (coord[2]<0 || coord[2]>=dims[2]) return true;
            case 2 : if (coord[1]<0 || coord[1]>=dims[1]) return true;
            case 1 : if (coord[0]<0 || coord[0]>=dims[0]) return true;
                    break;
            default:
                for (int i=0;i<coord.length;i++){
                    if (coord[i]<0 || coord[i]>dims[i]-1) return true;
                }
        }
        return false;
    }

    protected boolean isOutsideImage(Tuple coord){
        for (int i=0,n=coord.getSampleCount();i<n;i++){
            double c = coord.get(i);
            if (c < 0 || c > dims[i]-1) return true;
        }
        return false;
    }

    protected abstract class ExtrapolatorCursor extends DecoratedVector implements TupleSpaceCursor {

        private final TupleSpaceCursor base;
        private final VectorRW inCoord;

        public ExtrapolatorCursor(TupleSpaceCursor base) {
            super(base.samples());
            this.base = base;
            inCoord = VectorNf64.createDouble(parent.getExtent().getDimension());
        }

        @Override
        public Tuple samples() {
            return this;
        }

        @Override
        public Tuple coordinate() {
            return inCoord;
        }

        @Override
        public void moveTo(Tuple coordinate) {
            inCoord.set(coordinate);
            if (isOutsideImage(coordinate)) {
                tuple = getOutsideTuple(coordinate);
            } else {
                base.moveTo(coordinate);
                tuple = base.samples();
            }
        }

        protected abstract Tuple getOutsideTuple(Tuple coordinate);

    }
}
