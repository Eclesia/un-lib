
package science.unlicense.image.api.color;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.impl.colorspace.HSL;
import science.unlicense.image.impl.colorspace.SRGB;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.transform.AbstractTransform;
import science.unlicense.math.api.transform.DimStackTransform;
import science.unlicense.math.api.transform.IdentityTransform;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Affine1;

/**
 *
 * DRAFT color coding.
 *
 * Store colorspace components range and alpha component informations.
 *
 * @author Johann Sorel
 */
public final class ColorSystem implements SampleSystem {

    public static final ColorSystem RGB_8BITS = new ColorSystem(SRGB.INSTANCE, false, false, new Affine1[]{
        new Affine1(1.0/255.0, 0),
        new Affine1(1.0/255.0, 0),
        new Affine1(1.0/255.0, 0)
    });
    public static final ColorSystem RGB_FLOAT = new ColorSystem(SRGB.INSTANCE, false, false, new Affine1[]{
        new Affine1(1.0, 0),
        new Affine1(1.0, 0),
        new Affine1(1.0, 0)
    });
    public static final ColorSystem RGBA_8BITS = new ColorSystem(SRGB.INSTANCE, true, false, new Affine1[]{
        new Affine1(1.0/255.0, 0),
        new Affine1(1.0/255.0, 0),
        new Affine1(1.0/255.0, 0),
        new Affine1(1.0/255.0, 0)
    });
    public static final ColorSystem RGBA_8BITS_PREMUL = new ColorSystem(SRGB.INSTANCE, true, true, new Affine1[]{
        new Affine1(1.0/255.0, 0),
        new Affine1(1.0/255.0, 0),
        new Affine1(1.0/255.0, 0),
        new Affine1(1.0/255.0, 0)
    });
    public static final ColorSystem RGBA_FLOAT = new ColorSystem(SRGB.INSTANCE, true, false, new Affine1[]{
        new Affine1(1, 0),
        new Affine1(1, 0),
        new Affine1(1, 0),
        new Affine1(1, 0)
    });
    public static final ColorSystem RGBA_FLOAT_PREMUL = new ColorSystem(SRGB.INSTANCE, true, true, new Affine1[]{
        new Affine1(1, 0),
        new Affine1(1, 0),
        new Affine1(1, 0),
        new Affine1(1, 0)
    });
    public static final ColorSystem HSL_FLOAT = new ColorSystem(HSL.INSTANCE, true, false, new Affine1[]{
        new Affine1(1, 0),
        new Affine1(1, 0),
        new Affine1(1, 0),
        new Affine1(1, 0)
    });

    private static final Affine1 IDENTITY = new Affine1(1.0, 0.0);

    private final ColorSpace colorspace;
    private final boolean hasAlpha;
    private final boolean isPremul;
    private final Affine1[] sampleToCs;

    //no need for synchronisation, worst case scenario transform would be build
    //a few times with any consequences.
    private Affine samplesToCs;
    private Affine csToSamples;

    /**
     *
     * @param colorspace
     * @param hasAlpha
     * @param isPremul
     * @param sampleToCs in colorspace component order,
     *        alpha transform last,
     *        if null transform is linear for each axis
     */
    public ColorSystem(ColorSpace colorspace, boolean hasAlpha, boolean isPremul, Affine1[] sampleToCs) {
        CObjects.ensureNotNull(colorspace);
        this.colorspace = colorspace;
        this.hasAlpha = hasAlpha;
        this.isPremul = isPremul;
        if (sampleToCs == null) {
            this.sampleToCs = new Affine1[colorspace.getNumComponents() + (hasAlpha ? 1 : 0)];
            Arrays.fill(this.sampleToCs, IDENTITY);
        } else {
            if (colorspace.getNumComponents() + (hasAlpha ? 1 : 0) != sampleToCs.length) {
                throw new InvalidArgumentException("Transforms size do not match color system dimension, expected "+colorspace.getNumComponents() + (hasAlpha ? 1 : 0)+" but was "+sampleToCs.length);
            }
            this.sampleToCs = sampleToCs.clone();
        }
    }

    /**
     * Color color space.
     * @return ColorSpace never null
     */
    public ColorSpace getColorSpace() {
        return colorspace;
    }

    @Override
    public int getNumComponents() {
        return sampleToCs.length;
    }

    public boolean hasAlpha() {
        return hasAlpha;
    }

    public boolean isAlphaPremultiplied() {
        return isPremul;
    }

    public Affine1[] getSampleToCs() {
        return sampleToCs;
    }

    /**
     * Get samples to color space as one transform.
     * @return transform
     */
    public Affine getSamplesToCs() {
        if (samplesToCs == null) {
            samplesToCs = (Affine) DimStackTransform.create(sampleToCs);
        }
        return samplesToCs;
    }

    /**
     * Get color space to samples as one transform.
     * @return transform
     */
    public Affine getCsToSamples() {
        if (csToSamples == null) {
            csToSamples = getSamplesToCs().invert();
        }
        return csToSamples;
    }

    public Transform getTranform(ColorSystem out) {
        if (this.equals(out)) {
            return new IdentityTransform(sampleToCs.length);
        }

        return new ColorCodingTransform(this, out);

        //TODO optimized version
//        final int nbIn = in.getNumComponents();
//        final int nbOut = out.getNumComponents();
//        final double[] temp = new double[nbIn];
//        Arrays.copy(source, 0, temp.length, temp, 0);
//
//        //apply component transforms
//        final Transform toInCs = DimStackTransform.create(sampleToCs);
//
//
//
//        //unset alpha
//        double alpha = 1.0;
//        if (hasAlpha()) {
//
//            final int[] mapping = new int[colorspace.getDimension()];
//            for (int i=0;i<mapping.length;i++) mapping[i] = i;
//            final Transform rea = RearrangeTransform.create(mapping);
//
//            if (isAlphaPremultiplied()) {
//                final Transform ap = new AlphaInverseTransform4();
//            }
//        }
//
//
//        final Transform csTrs = getColorSpace().getTranform(out.getColorSpace());
//
//        //reset alpha
//        if (out.hasAlpha()) {
//            if (out.isAlphaPremultiplied()) {
//                Vectors.scale(dest, alpha);
//            }
//            dest[nbOut-1] = alpha;
//        }
//
//        //apply component transforms
//        final Transform toOutCc = DimStackTransform.create(out.getSampleToCs()).invert();
//        toOutCc.transform(dest, dest);


    }

    /**
     * TODO doesn't feel right, should not be here.
     *
     * @param samples
     * @return
     */
    public int toARGB(float[] samples) {
        return new ColorRGB(samples, this).toARGB();
    }

    private static class AlphaMultiplyTransform4 extends AbstractTransform {

        public AlphaMultiplyTransform4() {
            super(4);
        }

        @Override
        public double[] transform(double[] source, double[] dest) {
            double alpha = source[3];
            dest[0] = source[0] * alpha;
            dest[1] = source[1] * alpha;
            dest[2] = source[2] * alpha;
            return dest;
        }

        @Override
        public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
            double alpha;
            for (int i=0,off=0;i<nbTuple;i++,off+=4) {
                alpha = source[sourceOffset+off+3];
                dest[destOffset+off+0] = source[sourceOffset+off+0] * alpha;
                dest[destOffset+off+1] = source[sourceOffset+off+1] * alpha;
                dest[destOffset+off+2] = source[sourceOffset+off+2] * alpha;
            }
        }

        @Override
        public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
            float alpha;
            for (int i=0,off=0;i<nbTuple;i++,off+=4) {
                alpha = source[sourceOffset+off+3];
                dest[destOffset+off+0] = source[sourceOffset+off+0] * alpha;
                dest[destOffset+off+1] = source[sourceOffset+off+1] * alpha;
                dest[destOffset+off+2] = source[sourceOffset+off+2] * alpha;
            }
        }

        @Override
        public Transform invert() {
            return new AlphaInverseTransform4();
        }

    }

    private static class AlphaInverseTransform4 extends AbstractTransform {

        public AlphaInverseTransform4() {
            super(4);
        }

        @Override
        public double[] transform(double[] source, double[] dest) {
            double alpha = source[3];
            dest[0] = source[0] * alpha;
            dest[1] = source[1] * alpha;
            dest[2] = source[2] * alpha;
            return dest;
        }

        @Override
        public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
            double alpha;
            for (int i=0,off=0;i<nbTuple;i++,off+=4) {
                alpha = source[sourceOffset+off+3];
                dest[destOffset+off+0] = source[sourceOffset+off+0] / alpha;
                dest[destOffset+off+1] = source[sourceOffset+off+1] / alpha;
                dest[destOffset+off+2] = source[sourceOffset+off+2] / alpha;
            }
        }

        @Override
        public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
            float alpha;
            for (int i=0,off=0;i<nbTuple;i++,off+=4) {
                alpha = source[sourceOffset+off+3];
                dest[destOffset+off+0] = source[sourceOffset+off+0] / alpha;
                dest[destOffset+off+1] = source[sourceOffset+off+1] / alpha;
                dest[destOffset+off+2] = source[sourceOffset+off+2] / alpha;
            }
        }

        @Override
        public Transform invert() {
            return new AlphaMultiplyTransform4();
        }

    }

}
