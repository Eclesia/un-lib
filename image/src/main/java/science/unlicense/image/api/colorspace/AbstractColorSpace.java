
package science.unlicense.image.api.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.image.impl.colorspace.SRGB;
import science.unlicense.math.api.transform.ConcatenateTransform;
import science.unlicense.math.api.transform.Transform;

/**
 * Abstract ColorSpace implementation, will fall back on single tuple
 * conversion methods.
 *
 * @author Johann Sorel
 */
public abstract class AbstractColorSpace implements ColorSpace {

    protected final Chars name;
    protected final ColorSpaceComponent[] components;
    protected final int dimension;

    /**
     *
     *
     * @param name colorspace name
     * @param components colorspace components
     */
    public AbstractColorSpace(Chars name, ColorSpaceComponent[] components) {
        this.name = name;
        this.components = components;
        this.dimension = components.length;
    }

    @Override
    public Chars getName() {
       return name;
    }

    @Override
    public int getNumComponents() {
        return dimension;
    }

    @Override
    public ColorSpaceComponent[] getComponents() {
        return components;
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) {
            throw new InvalidArgumentException("Colorspace classes must implement a transform to sRGB colorspace");
        }
        final Transform spaceToRgb = getTranform(SRGB.INSTANCE);
        final Transform rgbToTarget = cs.getTranform(SRGB.INSTANCE).invert();
        return ConcatenateTransform.create(spaceToRgb, rgbToTarget);
    }

}
