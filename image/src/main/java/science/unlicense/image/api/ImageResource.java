
package science.unlicense.image.api;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Identifiable;
import science.unlicense.encoding.api.store.Resource;

/**
 * An image resource of a single image in a Store.
 *
 * @author Johann Sorel
 */
public interface ImageResource extends Resource, Identifiable {

    /**
     * Indicate if this resource can support reading.
     *
     * @return true if reading is supported.
     */
    boolean supportReading();

    /**
     * @return true if writing is supported.
     */
    boolean supportWriting();

    /**
     * @return new Image Reader, can be null if reading is not supported
     */
    ImageReader createReader() throws IOException;

    /**
     * @return new Image Writer, can be null if writing is not supported
     */
    ImageWriter createWriter() throws IOException;


}
