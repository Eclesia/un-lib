
package science.unlicense.image.api.process;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocumentType;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.image.api.Image;
import science.unlicense.task.api.AbstractTaskDescriptor;

/**
 * Description of an operator.
 *
 * @author Johann Sorel
 */
public abstract class AbstractImageTaskDescriptor extends AbstractTaskDescriptor {

    public static final Chars EXTRAPOLATE_CONSTANTE = Chars.constant("constant");
    public static final Chars EXTRAPOLATE_COPY = Chars.constant("copy");
    public static final Chars EXTRAPOLATE_WRAP = Chars.constant("wrap");

    public static final Chars INTERPOLATE_LINEAR = Chars.constant("linear");
    public static final Chars INTERPOLATE_NEAREST = Chars.constant("nearest");

    public static final FieldType INPUT_IMAGE         = new FieldTypeBuilder().id(new Chars("Image")).title(new Chars("Input image")).valueClass(Image.class).build();
    public static final FieldType INPUT_IMAGE2        = new FieldTypeBuilder().id(new Chars("Image2")).title(new Chars("Input image 2")).valueClass(Image.class).build();
    public static final FieldType INPUT_EXTRAPOLATOR  = new FieldTypeBuilder().id(new Chars("Extender")).title(new Chars("Input extrapolator")).valueClass(Chars.class).build();
    public static final FieldType OUTPUT_IMAGE        = new FieldTypeBuilder().id(new Chars("Image")).title(new Chars("Input image")).valueClass(Image.class).build();
    public static final FieldType INPUT_WORKINGMODEL  = new FieldTypeBuilder().id(new Chars("WorkingModel")).title(new Chars("Input model name used to work on, often RAW or COLOR")).valueClass(Chars.class).build();

    public AbstractImageTaskDescriptor(Chars id, CharArray name, CharArray description,
            DocumentType inputType, DocumentType outputType) {
        super(id, name, description, inputType, outputType);
    }

    public AbstractImageTaskDescriptor(Chars id, CharArray name, CharArray description,
            FieldType[] inputs, FieldType[] outputs) {
        super(id, name, description,
                new DefaultDocumentType(INPUT,null,null,true,inputs,null),
                new DefaultDocumentType(OUTPUT,null,null,true,outputs,null));
    }

    public static TupleSpace extrapolate(TupleGrid grid, Chars extrapolationName) {
        if (AbstractImageTaskDescriptor.EXTRAPOLATE_COPY.equals(extrapolationName)) {
            return new ExtrapolatorCopy((TupleGrid) grid);
        } else if (AbstractImageTaskDescriptor.EXTRAPOLATE_WRAP.equals(extrapolationName)) {
            return new ExtrapolatorWrap((TupleGrid) grid);
        } else {
            return new ExtrapolatorConstant((TupleGrid) grid, 0);
        }
    }

}
