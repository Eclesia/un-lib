
package science.unlicense.image.api.model;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.common.api.number.NumberType;

/**
 * Images are N dimension raw byte buffers.
 * ImageModel produce views of the image for a given purpose.
 * They add meaning to the image by providing an interpretation of the byte buffer
 * content as tuples with units.
 * additionally they may transform the buffer values or ignore some parts of the byte buffer.
 *
 * There are typically 3 types of image models :
 * - Raw model : access to the datas with no transformation
 * - Color model : interpretation of the datas as colors
 * - Physic model : interpretation of the datas as their real values
 *
 * The Raw model is mandatory.
 * Image may not necessarily have a color or physic model.
 * Other types of image models may be available.
 *
 * @author Johann Sorel
 */
public interface ImageModel {

    public static final Chars MODEL_RAW = Chars.constant("RAW");
    public static final Chars MODEL_COLOR = Chars.constant("COLOR");
    public static final Chars MODEL_PHYSIC = Chars.constant("PHYSIC");

    /**
     * Get model name, this is a common name such as Planar,Interleaved,DXT,...
     *
     * @return model common name, can be null
     */
    Chars getName();

    SampleSystem getSampleSystem();

    /**
     * @return type of samples, see NumericType.TYPE_X
     */
    NumberType getNumericType();

    TupleGrid asTupleBuffer(Image image);

    /**
     * Create a primitive buffer.
     *
     * @param dimensions
     * @return
     */
    Buffer createBuffer(Extent.Long dimensions);

    /**
     * Create a primitive buffer.
     *
     * @param dimensions
     * @param factory
     * @return
     */
    Buffer createBuffer(Extent.Long dimensions, BufferFactory factory);

}
