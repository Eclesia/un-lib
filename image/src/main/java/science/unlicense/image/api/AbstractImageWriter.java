
package science.unlicense.image.api;

import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractImageWriter extends AbstractWriter implements ImageWriter{


    public ImageWriteParameters createParameters() {
        return new DefaultImageWriteParameters();
    }

    public final void write(Image image, ImageWriteParameters parameters) throws IOException {
        final ByteOutputStream stream = getOutputAsByteStream();
        write(image, parameters, stream);
    }

    protected abstract void write(Image image, ImageWriteParameters params, ByteOutputStream stream) throws IOException;


}
