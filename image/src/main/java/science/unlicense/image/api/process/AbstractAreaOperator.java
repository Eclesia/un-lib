
package science.unlicense.image.api.process;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 * Image operator where output sample value can be calculated with
 * a given range around the pixel.
 *
 * @author Johann Sorel
 * @author Florent Humbert
 */
public abstract class AbstractAreaOperator extends AbstractPreservingOperator{

    /// when iterating
    private double[] inSamples;
    private TupleRW outSamples;

    public AbstractAreaOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    protected abstract int getTopPadding();

    protected abstract int getBottomPadding();

    protected abstract int getLeftPadding();

    protected abstract int getRightPadding();

    @Override
    public Document perform() {
        super.perform();
        prepareInputs();

        TupleSpace grid = inputImage.getTupleBuffer(inputImage.getRawModel());

        final Chars extrapolator = (Chars) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_EXTRAPOLATOR.getId());
        grid = AbstractImageTaskDescriptor.extrapolate((TupleGrid) grid, extrapolator);

        //prepare the work space
        final int width     = (int) extent.get(0);
        final int height    = (int) extent.get(1);
        final int toppad    = getTopPadding();
        final int bottompad = getBottomPadding();
        final int leftpad   = getLeftPadding();
        final int rightpad  = getRightPadding();
        final int wsHeight  = toppad+bottompad+1;
        final int wsWidth   = leftpad+rightpad+1;
        final double[][][] workspaces = new double[toppad+bottompad+1][leftpad+rightpad+1][nbSample];

        //cache the current x, this way we know when we move only a single pixel aside
        //we can avoid getting the all matrix values, only grab the last column
        int lastX = -10;

        //loop on all pixels
        int x,y,k,maxx,maxy;
        final Vector2i32 xy = new Vector2i32();
        inSamples = new double[nbSample];
        outSamples = inputTuples.createTuple();
        final TupleGridCursor cursor = inputTuples.cursor();
        final TupleGridCursor cursor2 = inputTuples.cursor();
        final TupleRW temp = inputTuples.createTuple();
        while (cursor.next()) {
            cursor.samples().toDouble(inSamples, 0);
            final Vector2i32 coord = new Vector2i32();
            coord.set(cursor.coordinate());
            maxx = coord.x + rightpad+1;
            maxy = coord.y + bottompad+1;
            if (coord.x == lastX+1) {
                //moved one pixel on x, we can grab only the last column values
                for (x=1;x<wsWidth;x++){
                    for (y=0;y<wsHeight;y++){
                        for (k=0;k<nbSample;k++){
                            workspaces[y][x-1][k] = workspaces[y][x][k];
                        }
                    }
                }
                //grave the last column values
                xy.x = maxx-1;
                x=wsWidth-1;
                y=0;
                for (xy.y=-toppad+coord.y;xy.y<maxy;xy.y++,y++){
                    //check if we are outside image
                    if (xy.x<0 || xy.x>=width || xy.y<0 || xy.y>=height){
                        grid.getTuple(coord, temp);
                        temp.toDouble(workspaces[y][x],0);
                    } else {
                        cursor2.moveTo(xy);
                        cursor2.samples().toDouble(workspaces[y][x], 0);
                    }
                }

            } else {
                //fill matrices
                x=0;
                for (xy.x=-leftpad+coord.x;xy.x<maxx;xy.x++,x++){
                    y=0;
                    for (xy.y=-toppad+coord.y;xy.y<maxy;xy.y++,y++){
                        //check if we are outside image
                        if (xy.x<0 || xy.x>=width || xy.y<0 || xy.y>=height){
                            grid.getTuple(coord, temp);
                            temp.toDouble(workspaces[y][x],0);
                        } else {
                            cursor2.moveTo(xy);
                            cursor2.samples().toDouble(workspaces[y][x], 0);
                        }
                    }
                }
            }
            lastX = coord.x;

            evaluate(workspaces,inSamples);

            //clip values to image sample type
            //adjustInOutSamples(inSamples,outSamples);
            outSamples.set(inSamples);

            //fill out image pixel
            outputTuples.setTuple(coord, outSamples);
        }

        return outputParameters;
    };

    protected void prepareInputs(){}

    //TODO we could give the displacement information here, it might avoid recalculation some cell values
    // would greatly improve oil painting operator for example
    protected abstract void evaluate(double[][][] workspace, double[] result);

}
