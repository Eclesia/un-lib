package science.unlicense.image.api;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRW;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.DefaultColorRW;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public class DefaultImage implements Image {

    private final Dictionary metadatas = new HashDictionary();
    private final Dictionary models = new HashDictionary() {
        @Override
        public void add(Object key, Object value) {
            super.add(key, value);
            clearCache();

            if (ImageModel.MODEL_COLOR.equals(key) && value != null) {
                final SampleSystem ss = ((ImageModel) value).getSampleSystem();
                if (!(ss instanceof ColorSystem)) {
                    throw new InvalidArgumentException("Color model must have a ColorSystem as SampleSystem.");
                }
            }
        }

        @Override
        public void addAll(Dictionary index) {
            final ImageModel cm = (ImageModel) index.getValue(ImageModel.MODEL_COLOR);
            if (cm != null) {
                final SampleSystem ss = cm.getSampleSystem();
                if (!(ss instanceof ColorSystem)) {
                    throw new InvalidArgumentException("Color model must have a ColorSystem as SampleSystem.");
                }
            }

            super.addAll(index);
            clearCache();
        }

        @Override
        public Object remove(Object key) {
            clearCache();
            return super.remove(key);
        }

        @Override
        public void removeAll() {
            clearCache();
            super.removeAll();
        }

    };
    private final Buffer dataBuffer;
    private final Extent.Long extent;

    private ImageModel rawModel;
    private ImageModel colorModel;
    private TupleGrid rawBuffer;
    private TupleGrid pixelBuffer;

    public DefaultImage(Buffer dataBuffer, Extent.Long extent,
            ImageModel rm) {
        models.add(ImageModel.MODEL_RAW, rm);
        this.dataBuffer = dataBuffer;
        this.extent = extent;
    }

    public DefaultImage(Buffer dataBuffer, Extent.Long extent,
            ImageModel rm, ImageModel cm) {
        models.add(ImageModel.MODEL_RAW, rm);
        models.add(ImageModel.MODEL_COLOR, cm);
        this.dataBuffer = dataBuffer;
        this.extent = extent;
    }

    public DefaultImage(Buffer dataBuffer, Extent.Long extent,
            Dictionary models, Dictionary metadatas) {
        if (metadatas!=null) this.metadatas.addAll(metadatas);
        if (models!=null) this.models.addAll(models);
        this.dataBuffer = dataBuffer;
        this.extent = extent;
    }

    public Extent.Long getExtent() {
        return extent;
    }

    public Buffer getDataBuffer() {
        return dataBuffer;
    }

    public Dictionary getMetadatas() {
        return metadatas;
    }

    public Dictionary getModels() {
        return models;
    }

    public ImageModel getRawModel(){
        if (rawModel != null) return rawModel;
        rawModel = (ImageModel) models.getValue(ImageModel.MODEL_RAW);
        return rawModel;
    }

    public ImageModel getColorModel(){
        if (colorModel != null) return colorModel;
        colorModel = (ImageModel) models.getValue(ImageModel.MODEL_COLOR);
        return colorModel;
    }

    public TupleGrid getTupleBuffer(ImageModel model) {
        if (model == getColorModel()) {
            if (pixelBuffer == null) {
                pixelBuffer = model.asTupleBuffer(this);
            }
            return pixelBuffer;
        } else if (model == getRawModel()) {
            if (rawBuffer == null) {
                rawBuffer = model.asTupleBuffer(this);
            }
            return rawBuffer;
        } else {
            return model.asTupleBuffer(this);
        }
    }

    @Override
    public Tuple getTuple(Tuple coordinate, ImageModel model) {
        final TupleGrid tb = getTupleBuffer(model);
        final TupleRW tuple = tb.createTuple();
        tb.getTuple(coordinate, tuple);
        return tuple;
    }

    @Override
    public Color getColor(Tuple coordinate) {
        final TupleGrid tb = getTupleBuffer(getColorModel());
        final ColorRW tuple = new DefaultColorRW((ColorSystem) tb.getSampleSystem());
        tb.getTuple(coordinate, tuple);
        return tuple;
    }

    private void clearCache() {
        rawModel = null;
        colorModel = null;
        rawBuffer = null;
        pixelBuffer = null;
    }
}
