
package science.unlicense.image.api;

import science.unlicense.encoding.api.store.Format;

/**
 * Define an image format.
 *
 * @author Johann Sorel
 */
public interface ImageFormat extends Format {

    /**
     * Indicate if this format can support reading.
     *
     * @return true if this format has reading capabilities.
     */
    boolean supportReading();

    /**
     * Indicate if this format can support writing.
     *
     * This does not ensure all created ImageResource will support writing.
     * Writing support is also based on the input type and various other conditions.
     * Use the supportWriting method on ImageResource for a more accurate information.
     *
     * @return true if this format has writing capabilities.
     */
    boolean supportWriting();

}
