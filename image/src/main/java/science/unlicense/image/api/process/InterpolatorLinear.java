
package science.unlicense.image.api.process;

import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.tuple.DefaultTupleSpaceCursor;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.math.api.EasingMethods;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.impl.Vector2f64;


/**
 * Interpolator are used to average samples between pixels.
 * When scaling images, this result in smoother images.
 *
 * @author Johann Sorel
 */
public class InterpolatorLinear implements TupleSpace {

    private final TupleSpace parent;

    public InterpolatorLinear(TupleSpace parent) {
        this.parent = parent;
    }

    @Override
    public ArithmeticType getNumericType() {
        return parent.getNumericType();
    }

    @Override
    public Geometry getCoordinateGeometry() {
        return parent.getCoordinateGeometry();
    }

    @Override
    public SampleSystem getSampleSystem() {
        return parent.getSampleSystem();
    }

    @Override
    public SampleSystem getCoordinateSystem() {
        return parent.getCoordinateSystem();
    }

    @Override
    public TupleRW createTuple() {
        return parent.createTuple();
    }

    @Override
    public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {

        final double x = coordinate.get(0);
        final double y = coordinate.get(1);
        final double minx = Math.floor(x);
        final double maxx = Math.ceil(x);
        final double miny = Math.floor(y);
        final double maxy = Math.ceil(y);

        if (minx == maxx && miny == maxy) {
            return parent.getTuple(coordinate, buffer);
        }

        final double ratiox = x-minx;
        final double ratioy = y-miny;

        //get the 4 corners
        final TupleRW buffer1 = parent.createTuple();
        final TupleRW buffer2 = parent.createTuple();
        final TupleRW buffer3 = parent.createTuple();
        final TupleRW buffer4 = parent.createTuple();


        final Vector2f64 crd = new Vector2f64(minx, miny);
        parent.getTuple(crd, buffer1);
        crd.x = maxx;
        crd.y = miny;
        parent.getTuple(crd, buffer2);
        crd.x = minx;
        crd.y = maxy;
        parent.getTuple(crd, buffer3);
        crd.x = maxx;
        crd.y = maxy;
        parent.getTuple(crd, buffer4);

        for (int i=0,n=buffer.getSampleCount();i<n;i++){
            final double interMiny = EasingMethods.linear(ratiox, buffer1.get(i), buffer2.get(i));
            final double interMaxY = EasingMethods.linear(ratiox, buffer3.get(i), buffer4.get(i));
            buffer.set(i, EasingMethods.linear(ratioy, interMiny, interMaxY));
        }

        return buffer;
    }

    @Override
    public TupleSpaceCursor cursor() {
        return new DefaultTupleSpaceCursor(this);
    }

}
