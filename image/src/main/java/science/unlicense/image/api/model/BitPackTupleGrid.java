
package science.unlicense.image.api.model;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DataCursor;
import science.unlicense.common.api.number.UInt32;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.AbstractNumberTupleGrid;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;

/**
 *
 * @author Johann Sorel
 */
final class BitPackTupleGrid extends AbstractNumberTupleGrid {

    private final Buffer buffer;
    private final Extent.Long extent;
    private final int tupleBitSize;
    private final int[] sampleOffsets;
    private final int[] sampleNbBits;
    private final DataCursor cursor;

    public BitPackTupleGrid(Buffer buffer, Extent.Long extent, int tupleBitSize, int[] sampleOffsets, int[] sampleNbBits, SampleSystem ss) {
        super(extent, UInt32.TYPE, ss);
        this.buffer = buffer;
        this.extent = extent;
        this.tupleBitSize = tupleBitSize;
        this.sampleOffsets = sampleOffsets;
        this.sampleNbBits = sampleNbBits;
        this.cursor = buffer.dataCursor();
    }

    private long getOffsetInBits(int[] coordinate) {
        long offset = coordinate[1] * extent.getL(0) + coordinate[0];
        return offset * tupleBitSize;
    }

    private long getOffsetInBits(Tuple coordinate) {
        long offset = Math.round(coordinate.get(1)) * extent.getL(0) + Math.round(coordinate.get(0));
        return offset * tupleBitSize;
    }

    public Buffer getPrimitiveBuffer() {
        return buffer;
    }

    @Override
    public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
        long offsetBits = getOffsetInBits(coordinate);
        long sampleOffset;
        for (int i = 0; i < nbSample; i++) {
            sampleOffset = offsetBits + sampleOffsets[i];
            cursor.setByteOffset(sampleOffset / 8);
            cursor.setBitOffset((int) (sampleOffset % 8));
            buffer.set(i, cursor.readBit(sampleNbBits[i]));
        }
        return buffer;
    }

    @Override
    public void setTuple(Tuple coordinate, Tuple buffer) {
        long offsetBits = getOffsetInBits(coordinate);
        long sampleOffset;
        for (int i = 0; i < nbSample; i++) {
            sampleOffset = offsetBits + sampleOffsets[i];
            cursor.setByteOffset(sampleOffset / 8);
            cursor.setBitOffset((int) (sampleOffset % 8));
            cursor.writeBit((int) buffer.get(i), sampleNbBits[i]);
        }
    }

}
