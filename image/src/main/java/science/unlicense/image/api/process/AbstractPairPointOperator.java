
package science.unlicense.image.api.process;

import science.unlicense.common.api.model.doc.Document;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 * Image operator where output sample value can be calculated with
 * only the input sample value.
 *
 * Override evaluate(double[] sample, double[] result) to work by pixel
 * Override evaluate(double sample) to work by sample
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public abstract class AbstractPairPointOperator extends AbstractPairPreservingOperator{

    public AbstractPairPointOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    /// when iterating
    private int size;

    public Document perform() {
        super.perform();
        prepareInputs();

        //loop on all pixels
        size = outputTuples.getSampleSystem().getNumComponents();
        final TupleGridCursor cursor1 = inputTuples1.cursor();
        final TupleGridCursor cursor2 = inputTuples2.cursor();
        final TupleGridCursor outCursor = outputTuples.cursor();
        while (cursor1.next()) {
            final Tuple coord = cursor1.coordinate();
            cursor2.moveTo(coord);
            outCursor.moveTo(coord);
            evaluate(cursor1.samples(), cursor2.samples(), outCursor.samples());

            //clip values to image sample type
            //adjustInOutSamples(inSamplesProcessed,outSamples);
        }

        return outputParameters;
    }

    protected void prepareInputs(){}

    protected void evaluate(TupleRW tuple1, TupleRW tuple2, TupleRW result){
        for (int k=0; k<size; k++) {
            result.set(k, evaluate(tuple1.get(k), tuple2.get(k)));
        }
    }

    protected abstract double evaluate(double value1, double value2);

}
