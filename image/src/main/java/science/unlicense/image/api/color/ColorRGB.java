
package science.unlicense.image.api.color;

import java.util.Objects;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.api.AbstractTuple;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.VectorNf32;
import science.unlicense.common.api.number.NumberType;

/**
 * Color object, composed of separate red, green, blue and alpha components.
 *
 * @author Johann Sorel
 */
public final class ColorRGB extends AbstractTuple implements Color {

    //color samples in colorspace
    private final float[] samples;
    private final ColorSystem colorSystem;

    //cache RBB values for the color, whatever it's colorspace
    //non premultiplied colors
    private final float red;
    private final float green;
    private final float blue;
    //premultiplied colors value.
    private final float redMul;
    private final float greenMul;
    private final float blueMul;

    private final float alpha;

    /**
     * Create color from int value.
     * Expect alpha not to be premultiplied.
     *
     * @param argb
     */
    public ColorRGB(int argb) {
        this(argb,false);
    }

    /**
     * Create color from int value.
     *
     * @param argb color
     * @param preMul if alpha is premultiplied
     */
    public ColorRGB(int argb, boolean preMul) {
        this(((argb >>> 16) & 0xFF)/255f,
             ((argb >>>  8) & 0xFF)/255f,
             ((argb >>>  0) & 0xFF)/255f,
             ((argb >>> 24) & 0xFF)/255f,
             preMul);
    }

    /**
     * Create color from R,G,B components.
     *
     * @param red compoent [0...255]
     * @param green compoent [0...255]
     * @param blue compoent [0...255]
     */
    public ColorRGB(int red, int green, int blue) {
        this(red/255f,green/255f,blue/255f,1f,false);
    }

    /**
     * Create color from R,G,B,A components.
     * Expect alpha not to be premultiplied.
     *
     * @param red compoent [0...255]
     * @param green compoent [0...255]
     * @param blue compoent [0...255]
     * @param alpha compoent [0...255]
     */
    public ColorRGB(int red, int green, int blue, int alpha) {
        this(red/255f,green/255f,blue/255f,alpha/255f,false);
    }

    /**
     * Create color from R,G,B,A components.
     *
     * @param red compoent [0...255]
     * @param green compoent [0...255]
     * @param blue compoent [0...255]
     * @param alpha compoent [0...255]
     * @param preMul if alpha is premultiplied
     */
    public ColorRGB(int red, int green, int blue, int alpha, boolean preMul) {
        this(red/255f,green/255f,blue/255f,alpha/255f,preMul);
    }

    /**
     * Create color from R,G,B components.
     *
     * @param red compoent [0...1]
     * @param green compoent [0...1]
     * @param blue compoent [0...1]
     */
    public ColorRGB(float red, float green, float blue) {
        this(red,green,blue,1f,false);
    }

    /**
     * Create color from R,G,B,A components.
     * Expect alpha not to be premultiplied.
     *
     * @param red compoent [0...1]
     * @param green compoent [0...1]
     * @param blue compoent [0...1]
     * @param alpha compoent [0...1]
     */
    public ColorRGB(float red, float green, float blue, float alpha) {
        this(red,green,blue,alpha,false);
    }

    /**
     * Create color from float array, [R,G,B,A?]
     * Aplha is optional
     * Expect alpha not to be premultiplied.
     *
     * @param rgba 3 or 4 dimension array
     */
    public ColorRGB(float[] rgba) {
        this(rgba[0],rgba[1],rgba[2],(rgba.length>3) ? rgba[3] : 1f, false);
    }

    /**
     * Create color from float array, [R,G,B,A]
     *
     * @param rgba 4 dimension array
     * @param preMul if alpha is premultiplied
     */
    public ColorRGB(float[] rgba, boolean preMul) {
        this(rgba[0],rgba[1],rgba[2],rgba[3], preMul);
    }

    /**
     * Create color from R,G,B,A components.
     *
     * @param red compoent [0...1]
     * @param green compoent [0...1]
     * @param blue compoent [0...1]
     * @param alpha compoent [0...1]
     * @param preMul if alpha is premultiplied
     */
    public ColorRGB(float red, float green, float blue, float alpha, boolean preMul) {

        if (preMul){
            this.colorSystem = ColorSystem.RGBA_FLOAT_PREMUL;
            if (alpha != 0f){
                this.red        = red/alpha;
                this.green      = green/alpha;
                this.blue       = blue/alpha;
            } else {
                //everything to zero
                this.red        = 0f;
                this.green      = 0f;
                this.blue       = 0f;
            }
            this.alpha      = alpha;
            this.redMul     = red;
            this.greenMul   = green;
            this.blueMul    = blue;
        } else {
            this.colorSystem = ColorSystem.RGBA_FLOAT;
            this.red        = red;
            this.green      = green;
            this.blue       = blue;
            this.alpha      = alpha;
            this.redMul     = this.red * alpha;
            this.greenMul   = this.green * alpha;
            this.blueMul    = this.blue * alpha;
        }

        this.samples = new float[]{red,green,blue,alpha};
    }

    public ColorRGB(float[] samples, ColorSystem cc){
        this.colorSystem = cc;
        this.samples = Arrays.copy(samples);

        samples = new float[4];
        cc.getTranform(ColorSystem.RGBA_FLOAT).transform(this.samples, 0, samples, 0, 1);
        this.red        = samples[0];
        this.green      = samples[1];
        this.blue       = samples[2];
        this.alpha      = samples[3];
        this.redMul     = this.red * alpha;
        this.greenMul   = this.green * alpha;
        this.blueMul    = this.blue * alpha;
    }

    public ColorRGB(int[] isamples, ColorSystem cc){
        float[] samples = Arrays.reformatToFloat(isamples);

        this.colorSystem = cc;
        this.samples = Arrays.copy(samples);

        samples = new float[4];
        cc.getTranform(ColorSystem.RGBA_FLOAT).transform(this.samples, 0, samples, 0, 1);
        this.red        = samples[0];
        this.green      = samples[1];
        this.blue       = samples[2];
        this.alpha      = samples[3];
        this.redMul     = this.red * alpha;
        this.greenMul   = this.green * alpha;
        this.blueMul    = this.blue * alpha;
    }

    public ColorRGB(Tuple source, ColorSystem cc){
        this.colorSystem = cc;
        this.samples = source.toFloat();

        final float[] rgba = new float[4];
        cc.getTranform(ColorSystem.RGBA_FLOAT).transform(this.samples, 0, rgba, 0, 1);
        this.red        = rgba[0];
        this.green      = rgba[1];
        this.blue       = rgba[2];
        this.alpha      = rgba[3];
        this.redMul     = this.red * alpha;
        this.greenMul   = this.green * alpha;
        this.blueMul    = this.blue * alpha;
    }

    /**
     * Color color system.
     * @return ColorSystem never null
     */
    public ColorSystem getSampleSystem() {
        return colorSystem;
    }

    /**
     * Color samples in color space units.
     * @param buffer if null a new array will be created
     * @return float[]
     */
    public float[] getSamples(float[] buffer){
        if (buffer==null){
            buffer = Arrays.copy(samples);
        } else {
            Arrays.copy(samples, 0, samples.length,buffer,0);
        }
        return buffer;
    }

    /**
     * Get red component.
     * @return red component [0...1]
     */
    public float getRed() {
        return red;
    }

    /**
     * Get green component.
     * @return green component [0...1]
     */
    public float getGreen() {
        return green;
    }

    /**
     * Get blue component.
     * @return blue component [0...1]
     */
    public float getBlue() {
        return blue;
    }

    /**
     * Get alpha component.
     * @return alpha component [0...1]
     */
    public float getAlpha() {
        return alpha;
    }

    /**
     * Get RGB components as float array.
     * Alpha value is ignored.
     *
     * @return float array RGB
     */
    public float[] toRGB(){
        return new float[]{red,green,blue};
    }

    /**
     * Get RGB components as float array.
     * Alpha value is ignored.
     *
     * @param buffer to write into, not null
     * @param offset start index where to write color
     * @return float array RGB
     */
    public float[] toRGB(float[] buffer, int offset){
        buffer[offset] = red;
        buffer[offset+1] = green;
        buffer[offset+2] = blue;
        return buffer;
    }

    /**
     * Get RGBA components as float array.
     * Alpha not premultiplied.
     *
     * @return float array RGBA
     */
    public float[] toRGBA(){
        return new float[]{red,green,blue,alpha};
    }

    /**
     * Get RGBA components as float array.
     * Alpha not premultiplied.
     *
     * @param buffer to write into, not null
     * @param offset start index where to write color
     * @return float array RGB
     */
    public float[] toRGBA(float[] buffer, int offset){
        buffer[offset] = red;
        buffer[offset+1] = green;
        buffer[offset+2] = blue;
        buffer[offset+3] = alpha;
        return buffer;
    }

    /**
     * Get RGBA components as float array.
     * Alpha premultiplied.
     *
     * @return float array RGBA
     */
    public float[] toRGBAPreMul(){
        return new float[]{redMul,greenMul,blueMul,alpha};
    }

    /**
     * Get RGBA components as float array.
     * Alpha premultiplied.
     *
     * @param buffer to write into, not null
     * @param offset start index where to write color
     * @return float array RGB
     */
    public float[] toRGBAPreMul(float[] buffer, int offset){
        buffer[offset] = redMul;
        buffer[offset+1] = greenMul;
        buffer[offset+2] = blueMul;
        buffer[offset+3] = alpha;
        return buffer;
    }

    /**
     * Encode color on an int, 8 bits per component in order ARGB.
     * Alpha not premultiplied.
     *
     * @return int ARGB
     */
    public int toARGB() {
        return ((int) (alpha*255)) << 24
             | ((int) (red*255))   << 16
             | ((int) (green*255)) <<  8
             | ((int) (blue*255))  <<  0;
    }

    /**
     * Encode color on an int, 8 bits per component in order ARGB.
     * Alpha premultiplied.
     *
     * @return int ARGB
     */
    public int toARGBPreMul() {
        return ((int) (alpha*255))    << 24
             | ((int) (redMul*255))   << 16
             | ((int) (greenMul*255)) <<  8
             | ((int) (blueMul*255))  <<  0;
    }

    /**
     * Convert color samples to target color system.
     *
     * @param cs target system
     * @return converted color
     */
    public ColorRGB toColorSystem(ColorSystem cs) {
        final Transform tr = this.colorSystem.getTranform(cs);
        float[] res = new float[tr.getOutputDimensions()];
        tr.transform(samples, 0, res, 0, 1);
        return new ColorRGB(res,cs);
    }

    /**
     * {@inheritDoc }
     */
    public Chars toChars() {
        return new Chars("["+(int) (red*255)+","+(int) (green*255)+","+(int) (blue*255)+","+(int) (alpha*255)+"]");
    }

    @Override
    public NumberType getNumericType() {
        return Float32.TYPE;
    }

    @Override
    public int getSampleCount() {
        return samples.length;
    }

    @Override
    public double get(int indice) throws InvalidIndexException {
        return samples[indice];
    }

    @Override
    public Number getNumber(int indice) throws InvalidIndexException {
        return new Float32(samples[indice]);
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        for (int i=0;i<this.samples.length;i++){
            buffer[offset+i] = new Float32(samples[i]);
        }
    }

    @Override
    public final int getHash() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.colorSystem);
        return hash;
    }


    @Override
    public final boolean equals(Object obj) {
        if (obj instanceof Color) {
            return equals((Color) obj, ColorSystem.RGBA_FLOAT, 0);
        }
        return false;
    }

    @Override
    public final boolean equalsExact(Color obj) {
        if (obj instanceof Color) {
            final Color other = (Color) obj;
            if (colorSystem.equals(other.getSampleSystem())) {
                for (int i=0,n=colorSystem.getNumComponents();i<n;i++) {
                    if (get(i) != other.get(i)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public boolean equals(Color other, ColorSystem cs, double tolerance) {
        Color base = this;
        if (cs == null){
            cs = colorSystem;
            other = other.toColorSystem(cs);
        } else {
            base = base.toColorSystem(cs);
            other = other.toColorSystem(cs);
        }

        for (int i=0,n=cs.getNumComponents();i<n;i++) {
            final double diff = base.get(i) - other.get(i);
            if ( diff<-tolerance || diff>tolerance) {
                return false;
            }
        }
        return true;
    }

    @Override
    public TupleRW create(int size) {
        return VectorNf32.createFloat(size);
    }

}
