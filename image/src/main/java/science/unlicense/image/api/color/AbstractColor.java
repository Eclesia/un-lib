
package science.unlicense.image.api.color;

import java.util.Objects;
import science.unlicense.math.api.AbstractTuple;
import science.unlicense.math.api.transform.Transform;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractColor extends AbstractTuple implements Color {

    protected final ColorSystem colorSystem;

    public AbstractColor(ColorSystem colorSystem) {
        this.colorSystem = colorSystem;
    }

    @Override
    public int getSampleCount() {
        return colorSystem.getNumComponents();
    }

    @Override
    public ColorSystem getSampleSystem() {
        return colorSystem;
    }

    @Override
    public float getRed() {
        return toRGB()[0];
    }

    @Override
    public float getGreen() {
        return toRGB()[1];
    }

    @Override
    public float getBlue() {
        return toRGB()[2];
    }

    @Override
    public float getAlpha() {
        return toRGBA()[3];
    }

    @Override
    public float[] toRGB() {
        final float[] rgb = new float[3];
        toRGB(rgb, 0);
        return rgb;
    }

    @Override
    public float[] toRGB(float[] buffer, int offset) {
        final Transform tr = this.colorSystem.getTranform(ColorSystem.RGB_FLOAT);
        tr.transform(getSamples(null), 0, buffer, offset, 1);
        return buffer;
    }

    @Override
    public float[] toRGBA() {
        final float[] rgba = new float[4];
        toRGBA(rgba, 0);
        return rgba;
    }

    @Override
    public float[] toRGBA(float[] buffer, int offset) {
        final Transform tr = this.colorSystem.getTranform(ColorSystem.RGBA_FLOAT);
        float[] data;
        int soff;
        if (tr.getInputDimensions() <= tr.getOutputDimensions()) {
            toFloat(buffer, offset);
            data = buffer;
            soff = offset;
        } else {
            data = getSamples(null);
            soff = 0;
        }
        tr.transform(data, soff, buffer, offset, 1);
        return buffer;
    }

    @Override
    public float[] toRGBAPreMul() {
        final float[] rgba = new float[4];
        toRGBAPreMul(rgba, 0);
        return rgba;
    }

    @Override
    public float[] toRGBAPreMul(float[] buffer, int offset) {
        buffer = toRGBA(buffer, offset);
        float alpha = buffer[offset+3];
        buffer[offset+0] *= alpha;
        buffer[offset+1] *= alpha;
        buffer[offset+2] *= alpha;
        return buffer;
    }

    @Override
    public int toARGB() {
        float[] rgba = toRGBA();
        return ((int) (rgba[3]*255)) << 24
             | ((int) (rgba[0]*255)) << 16
             | ((int) (rgba[1]*255)) <<  8
             | ((int) (rgba[2]*255)) ;
    }

    @Override
    public int toARGBPreMul() {
        float[] rgba = toRGBAPreMul();
        return ((int) (rgba[3]*255)) << 24
             | ((int) (rgba[0]*255)) << 16
             | ((int) (rgba[1]*255)) <<  8
             | ((int) (rgba[2]*255)) ;
    }

    @Override
    public Color toColorSystem(ColorSystem cs) {
        if (cs.equals(colorSystem)) return this;
        final Transform tr = this.colorSystem.getTranform(cs);
        float[] samples = getSamples(null);
        float[] res = new float[tr.getOutputDimensions()];
        tr.transform(samples, 0, res, 0, 1);
        return new ColorRGB(res,cs);
    }

    @Override
    public final int getHash() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.colorSystem);
        return hash;
    }

    @Override
    public final boolean equals(Object obj) {
        if (obj instanceof Color) {
            return equals((Color) obj, ColorSystem.RGBA_FLOAT, 0);
        }
        return false;
    }

    @Override
    public final boolean equalsExact(Color obj) {
        if (obj instanceof Color) {
            final Color other = (Color) obj;
            if (colorSystem.equals(other.getSampleSystem())) {
                for (int i=0,n=colorSystem.getNumComponents();i<n;i++) {
                    if (get(i) != other.get(i)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param other
     * @param cs compare ColorSystem, if null use current color color sytem.
     * @param tolerance
     * @return
     */
    public boolean equals(Color other, ColorSystem cs, double tolerance) {
        Color base = this;
        if (cs == null){
            cs = colorSystem;
            other = other.toColorSystem(cs);
        } else {
            base = base.toColorSystem(cs);
            other = other.toColorSystem(cs);
        }

        for (int i=0,n=cs.getNumComponents();i<n;i++) {
            final double diff = base.get(i) - other.get(i);
            if ( diff<-tolerance || diff>tolerance) {
                return false;
            }
        }
        return true;
    }

}
