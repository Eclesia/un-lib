
package science.unlicense.image.api;

import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public interface Coverage {

    /**
     * Get the boundary of the coverage.
     *
     * @return Geometry, not null.
     */
    Geometry getGeometry();

    /**
     *
     * @param coord
     * @return
     */
    double[] evaluate(double[] coord);


}
