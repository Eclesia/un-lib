package science.unlicense.image.api;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.Writer;

/**
 *
 * @author Johann Sorel
 */
public interface ImageWriter extends Writer {

    /**
     * Create a default write parameters.
     *
     * @return ImageReadParameters
     */
    ImageWriteParameters createParameters();

    /**
     * Write an image in the file.
     *
     * @param params, can be null.
     * @throws IOException
     */
    void write(Image image, ImageWriteParameters params) throws IOException;

}
