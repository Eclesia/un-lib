
package science.unlicense.image.api.color;

import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector4f32;

/**
 *
 * @author Johann Sorel
 */
public class DecoratedColor extends AbstractColor {

    private final Tuple tuple;
    private Vector4f32 rgbaf;

    public DecoratedColor(Tuple tuple, ColorSystem colorSystem) {
        super(colorSystem);
        if (tuple.getSampleCount() != colorSystem.getNumComponents()) {
            throw new InvalidArgumentException("Tuple size do not match color system dimension, expected "+colorSystem.getNumComponents()+" but was "+tuple.getSampleCount());
        }
        this.tuple = tuple;
    }

    private Vector4f32 getRGBAf() {
        if (rgbaf == null) {
            if (colorSystem == ColorSystem.RGBA_FLOAT) {
                if (tuple instanceof Vector4f32) {
                    rgbaf = (Vector4f32) tuple;
                } else {
                    rgbaf = new Vector4f32(
                        (float) tuple.get(0),
                        (float) tuple.get(1),
                        (float) tuple.get(2),
                        (float) tuple.get(3) );
                }
            } else {
                rgbaf = new Vector4f32();
                colorSystem.getTranform(ColorSystem.RGBA_FLOAT).transform(tuple, rgbaf);
            }
        }
        return rgbaf;
    }

    @Override
    public ArithmeticType getNumericType() {
        return tuple.getNumericType();
    }

    @Override
    public double get(int indice) throws InvalidIndexException {
        return tuple.get(indice);
    }

    @Override
    public Arithmetic getNumber(int indice) throws InvalidIndexException {
        return tuple.getNumber(indice);
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        tuple.toNumber(buffer, offset);
    }

    @Override
    public float[] getSamples(float[] buffer) {
        if (buffer == null) buffer = new float[colorSystem.getNumComponents()];
        tuple.toFloat(buffer, 0);
        return buffer;
    }

    @Override
    public float getRed() {
        return getRGBAf().x;
    }

    /**
     * Get green component.
     * @return green component [0...1]
     */
    @Override
    public float getGreen() {
        return getRGBAf().y;
    }

    @Override
    public float getBlue() {
        return getRGBAf().z;
    }

    @Override
    public float getAlpha() {
        return getRGBAf().w;
    }

    @Override
    public float[] toRGB() {
        getRGBAf();
        return new float[]{rgbaf.x, rgbaf.y, rgbaf.z};
    }

    @Override
    public float[] toRGB(float[] buffer, int offset) {
        getRGBAf();
        buffer[offset  ] = rgbaf.x;
        buffer[offset+1] = rgbaf.y;
        buffer[offset+2] = rgbaf.z;
        return buffer;
    }

    @Override
    public float[] toRGBA() {
        getRGBAf();
        return new float[]{rgbaf.x, rgbaf.y, rgbaf.z, rgbaf.w};
    }

    @Override
    public float[] toRGBA(float[] buffer, int offset) {
        getRGBAf();
        buffer[offset  ] = rgbaf.x;
        buffer[offset+1] = rgbaf.y;
        buffer[offset+2] = rgbaf.z;
        buffer[offset+3] = rgbaf.w;
        return buffer;
    }

    @Override
    public float[] toRGBAPreMul() {
        getRGBAf();
        return new float[]{
            rgbaf.x * rgbaf.w,
            rgbaf.y * rgbaf.w,
            rgbaf.z * rgbaf.w,
            rgbaf.w};
    }

    @Override
    public float[] toRGBAPreMul(float[] buffer, int offset) {
        getRGBAf();
        buffer[offset  ] = rgbaf.x * rgbaf.w;
        buffer[offset+1] = rgbaf.y * rgbaf.w;
        buffer[offset+2] = rgbaf.z * rgbaf.w;
        buffer[offset+3] = rgbaf.w;
        return buffer;
    }

    @Override
    public int toARGB() {
        getRGBAf();
        return ((int) (rgbaf.w*255)) << 24
             | ((int) (rgbaf.x*255)) << 16
             | ((int) (rgbaf.y*255)) <<  8
             | ((int) (rgbaf.z*255)) ;
    }

    @Override
    public int toARGBPreMul() {
        getRGBAf();
        return ((int) (rgbaf.w        *255)) << 24
             | ((int) (rgbaf.x*rgbaf.w*255)) << 16
             | ((int) (rgbaf.y*rgbaf.w*255)) <<  8
             | ((int) (rgbaf.z*rgbaf.w*255)) ;
    }

    @Override
    public Color toColorSystem(ColorSystem cs) {
        if (cs == ColorSystem.RGBA_FLOAT) {
            return new DecoratedColor(getRGBAf(), cs);
        }
        return super.toColorSystem(cs);
    }

    @Override
    public TupleRW create(int size) {
        return tuple.create(size);
    }

}
