package science.unlicense.image.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.Metadata;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.model.tree.DefaultNodeCardinality;
import science.unlicense.common.api.model.tree.DefaultNodeType;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeCardinality;
import science.unlicense.common.api.model.tree.NodeType;
import science.unlicense.common.api.model.tree.TypedNode;

/**
 * Default image meta model.
 *
 * @author Johann Sorel
 */
public final class ImageSetMetadata extends DefaultTypedNode implements Metadata{

    public static final NodeType MD_IMAGESET;
    public static final NodeCardinality MD_IMAGE;
    public static final NodeCardinality MD_IMAGE_DIMENSION;
    public static final NodeCardinality MD_IMAGE_DIMENSION_ID;
    public static final NodeCardinality MD_IMAGE_DIMENSION_EXTEND;
    public static final NodeCardinality MD_IMAGE_BAND;
    public static final NodeCardinality MD_IMAGE_BAND_TYPE;

    static {

        MD_IMAGE_BAND_TYPE = new DefaultNodeCardinality(new Chars("type"),null, null, Integer.class, false, null);
        MD_IMAGE_BAND = new DefaultNodeCardinality(
                new Chars("band"),null, null,Sequence.class, null, false, 0,-1,new NodeCardinality[]{
                    MD_IMAGE_BAND_TYPE
                });

        MD_IMAGE_DIMENSION_ID = new DefaultNodeCardinality(new Chars("id"),null, null,String.class, false, null);
        MD_IMAGE_DIMENSION_EXTEND = new DefaultNodeCardinality(new Chars("extent"),null, null,Integer.class, false, null);
        MD_IMAGE_DIMENSION = new DefaultNodeCardinality(
                new Chars("dimension"),null, null,Sequence.class, null, false, 0,-1,new NodeCardinality[]{
                    MD_IMAGE_DIMENSION_ID,
                    MD_IMAGE_DIMENSION_EXTEND
                });

        MD_IMAGE = new DefaultNodeCardinality(
                new Chars("image"),null, null,Sequence.class, null, false, 0,-1,new NodeCardinality[]{
                    MD_IMAGE_DIMENSION,
                    MD_IMAGE_BAND
                });

        MD_IMAGESET = new DefaultNodeType(
                new Chars("imageset"),null, null,Sequence.class, false, new NodeCardinality[]{
                    MD_IMAGE
                });
    }

    public ImageSetMetadata(){
        super(MD_IMAGESET);
    }

    public ImageSetMetadata(Image image){
        super(MD_IMAGESET);
        children.add(image);
    }

    public Sequence getImages(){
        return Collections.filter(children, new Predicate() {
            public Boolean evaluate(Object candidate) {
                return MD_IMAGE.equals(((TypedNode) candidate).getCardinality());
            }
        });
    }

    public Node toNode() {
        return this;
    }

    public static class Image extends DefaultTypedNode implements Metadata{

        public Image() {
            super(MD_IMAGE);
        }

        public Image(int width, int height) {
            super(MD_IMAGE);
            children.add(new Dimension(new Chars("x"), width));
            children.add(new Dimension(new Chars("y"), height));
        }

        public Sequence getBands(){
            return Collections.filter(children, new Predicate() {
                public Boolean evaluate(Object candidate) {
                    return MD_IMAGE_BAND.equals(((TypedNode) candidate).getCardinality());
                }
            });
        }

        public Sequence getDimensions(){
            return Collections.filter(children, new Predicate() {
                public Boolean evaluate(Object candidate) {
                    return MD_IMAGE_DIMENSION.equals(((TypedNode) candidate).getCardinality());
                }
            });
        }

        public Node toNode() {
            return this;
        }
    }

    public static class Dimension extends DefaultTypedNode{

        public Dimension() {
            super(MD_IMAGE_DIMENSION);
        }

        public Dimension(Chars id, int extent) {
            this();
            setId(id);
            setExtent(extent);
        }

        public Chars getId(){
            final TypedNode node = getChild(MD_IMAGE_DIMENSION_ID);
            if (node==null) return null;
            return (Chars) node.getValue();
        }

        public void setId(Chars id){
            TypedNode node = getChild(MD_IMAGE_DIMENSION_ID);
            if (node==null){
                node = new DefaultTypedNode(MD_IMAGE_DIMENSION_ID);
                children.add(node);
            }
            node.setValue(value);
        }

        public Integer geExtent(){
            final TypedNode node = getChild(MD_IMAGE_DIMENSION_EXTEND);
            if (node==null) return null;
            return (Integer) node.getValue();
        }

        public void setExtent(Integer value){
            TypedNode node = getChild(MD_IMAGE_DIMENSION_EXTEND);
            if (node==null){
                node = new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND);
                children.add(node);
            }
            node.setValue(value);
        }

    }

    public static class Band extends DefaultTypedNode{

        public Band() {
            super(MD_IMAGE_BAND);
        }

        public Band(Integer type) {
            this();
            setType(type);
        }

        public Integer geType(){
            final TypedNode node = getChild(MD_IMAGE_BAND_TYPE);
            if (node==null) return null;
            return (Integer) node.getValue();
        }

        public void setType(Integer value){
            TypedNode node = getChild(MD_IMAGE_BAND_TYPE);
            if (node==null){
                node = new DefaultTypedNode(MD_IMAGE_BAND_TYPE);
                children.add(node);
            }
            node.setValue(value);
        }

    }

}
