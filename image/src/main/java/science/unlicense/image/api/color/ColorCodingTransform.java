
package science.unlicense.image.api.color;

import science.unlicense.common.api.Arrays;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.transform.AbstractTransform;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public class ColorCodingTransform extends AbstractTransform {

    private final ColorSystem in;
    private final ColorSystem out;
    private final Transform csTrs;
    private final Transform inCctoCs;
    private final Transform outCstoCc;

    public ColorCodingTransform(ColorSystem in, ColorSystem out) {
        super(in, out);
        this.in = in;
        this.out = out;
        csTrs = in.getColorSpace().getTranform(out.getColorSpace());
        inCctoCs = in.getSamplesToCs();
        outCstoCc = out.getCsToSamples();
    }

    @Override
    public void transform(double[] source, int srcOff, double[] dest, int dstOff, int nbTuple) {

        final double[] tempIn = new double[inSize];
        final double[] tempOut = new double[outSize];
        double alpha;
        for (int i=0;i<nbTuple;i++,srcOff+=inSize,dstOff+=outSize) {
            Arrays.copy(source, srcOff, inSize, tempIn, 0);

            //apply component transforms
            inCctoCs.transform(tempIn, 0, tempIn, 0, 1);

            //unset alpha
            alpha = 1.0;
            if (in.hasAlpha()) {
                alpha = tempIn[inSize-1];
                if (in.isAlphaPremultiplied()) {
                    Vectors.scale(tempIn, 1.0/alpha);
                }
            }

            //transform colorspace
            csTrs.transform(tempIn, 0, tempOut, 0, 1);

            //reset alpha
            if (out.hasAlpha()) {
                if (out.isAlphaPremultiplied()) {
                    Vectors.scale(tempOut, alpha);
                }
                tempOut[outSize-1] = alpha;
            }

            //apply component transforms
            outCstoCc.transform(tempOut, 0, tempOut, 0, 1);

            Arrays.copy(tempOut, 0, outSize, dest, dstOff);
        }
    }

    @Override
    public void transform(float[] source, int srcOff, float[] dest, int dstOff, int nbTuple) {

        final float[] tempIn = new float[inSize];
        final float[] tempOut = new float[outSize];
        float alpha;
        for (int i=0;i<nbTuple;i++,srcOff+=inSize,dstOff+=outSize) {
            Arrays.copy(source, srcOff, inSize, tempIn, 0);

            //apply component transforms
            inCctoCs.transform(tempIn, 0, tempIn, 0, 1);

            //unset alpha
            alpha = 1.0f;
            if (in.hasAlpha()) {
                alpha = tempIn[inSize-1];
                if (in.isAlphaPremultiplied()) {
                    Vectors.scale(tempIn, 1.0f/alpha);
                }
            }

            //transform colorspace
            csTrs.transform(tempIn, 0, tempOut, 0, 1);

            //reset alpha
            if (out.hasAlpha()) {
                if (out.isAlphaPremultiplied()) {
                    Vectors.scale(tempOut, alpha);
                }
                tempOut[outSize-1] = alpha;
            }

            //apply component transforms
            outCstoCc.transform(tempOut, 0, tempOut, 0, 1);

            Arrays.copy(tempOut, 0, outSize, dest, dstOff);
        }
    }

    @Override
    public TupleRW transform(Tuple source, TupleRW dest) {
        return super.transform(source, dest);
    }

    @Override
    public Transform invert() {
        return new ColorCodingTransform(out, in);
    }

}
