
package science.unlicense.image.impl.process.distance;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.common.api.number.Float64;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.task.api.AbstractTask;

/**
 * Brute force implementation of distance map.
 *
 * @author Johann Sorel
 */
public class DistanceOperator extends AbstractTask{

    public static final FieldType INPUT_MAXDISTANCE = new FieldTypeBuilder(new Chars("dist")).valueClass(Integer.class).defaultValue(1f).build();

    public static DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("distance"), new Chars("distance"), Chars.EMPTY, DistanceOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_MAXDISTANCE},
                new FieldType[]{OUTPUT_IMAGE});

    public DistanceOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image, int maxDistance) {
        inputParameters.setPropertyValue(INPUT_IMAGE.getId(),image);
        inputParameters.setPropertyValue(INPUT_MAXDISTANCE.getId(),maxDistance);
        return (Image) perform().getPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }

    @Override
    public Document perform() {
        final Image img = (Image) inputParameters.getPropertyValue(INPUT_IMAGE.getId());
        final int maxDist = (Integer) inputParameters.getPropertyValue(INPUT_MAXDISTANCE.getId());

        final Extent.Long extent = img.getExtent();

        //create distance image
        final Image distanceMap = Images.createCustomBand(extent, 1, Float64.TYPE);
        final double[] data = (double[]) distanceMap.getDataBuffer().getBackEnd();
        Arrays.fill(data, maxDist);

        //TODO
        throw new RuntimeException("TODO");

    }

}
