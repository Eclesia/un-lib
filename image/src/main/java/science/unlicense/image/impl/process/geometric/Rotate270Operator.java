
package science.unlicense.image.impl.process.geometric;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.task.api.AbstractTask;


/**
 * Rotate 270° clockwise operator.
 *
 * @author Johann Sorel
 */
public class Rotate270Operator extends AbstractTask {

   public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
           new Chars("rotate-270"), new Chars("rotate-270"), Chars.EMPTY, Rotate270Operator.class,
                new FieldType[]{INPUT_IMAGE},
                new FieldType[]{OUTPUT_IMAGE});

    public Rotate270Operator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image){
        inputParameters.setPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),image);
        return(Image) perform().getPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }

   @Override
    public Document perform() {
        final Image image = (Image) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());

        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final TupleGridCursor tb = image.getRawModel().asTupleBuffer(image).cursor();

        final Image result = Images.create(image, new Extent.Long(height, width));
        final TupleGridCursor rsm = result.getRawModel().asTupleBuffer(result).cursor();

        final Vector2i32 coord1 = new Vector2i32();
        final Vector2i32 coord2 = new Vector2i32();
        for (coord1.y=0;coord1.y<height;coord1.y++){
            for (coord1.x=0;coord1.x<width;coord1.x++){
                coord2.x = coord1.y;
                coord2.y = width-coord1.x-1;

                tb.moveTo(coord1);
                rsm.moveTo(coord2);
                rsm.samples().set(tb.samples());
            }
        }

        outputParameters.setPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),result);
        return outputParameters;
    }

}
