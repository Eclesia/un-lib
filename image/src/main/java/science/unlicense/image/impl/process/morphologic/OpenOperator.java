
package science.unlicense.image.impl.process.morphologic;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_EXTRAPOLATOR;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.task.api.AbstractTask;


/**
 * Morphological opening operator.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public class OpenOperator extends AbstractTask {

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("open"), new Chars("Open"), Chars.EMPTY, OpenOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_EXTRAPOLATOR},
                new FieldType[]{OUTPUT_IMAGE});

    public OpenOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image, Chars extrapolatorType) {
        inputParameters.setPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),image);
        inputParameters.setPropertyValue(AbstractImageTaskDescriptor.INPUT_EXTRAPOLATOR.getId(),extrapolatorType);
        return (Image) perform().getPropertyValue(OUTPUT_IMAGE.getId());
    }

    @Override
    public Document perform() {
        Image image = (Image) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final Chars extrapolator = (Chars) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_EXTRAPOLATOR.getId());

        image = new ErodeOperator().execute(image, null, extrapolator);
        image = new DilateOperator().execute(image, null, extrapolator);

        outputParameters.setPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),image);
        return outputParameters;
    }

}
