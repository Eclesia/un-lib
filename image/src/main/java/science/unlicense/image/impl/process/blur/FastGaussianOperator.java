
package science.unlicense.image.impl.process.blur;

import science.unlicense.common.api.character.Chars;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.AbstractPreservingOperator;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;

/**
 * Binary threshold of an image.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public class FastGaussianOperator extends AbstractPreservingOperator {

    public static final FieldType INPUT_RADIUS = new FieldTypeBuilder(new Chars("radius")).title(new Chars("Input blur radius")).valueClass(Integer.class).build();

    public static DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("fastgaussianblur"), new Chars("Fast Gaussian blur"), Chars.EMPTY, FastGaussianOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_RADIUS},
                new FieldType[]{OUTPUT_IMAGE});

    private int radius;

    public FastGaussianOperator() {
        super(DESCRIPTOR);
    }

    public Document perform() {
        super.perform();
        radius = (Integer) inputParameters.getPropertyValue(INPUT_RADIUS.getId());

        //TODO must generalize this for N samples, allow somehow a way to work on colors or samples.


//        ImageProcessor inputP = new ImageProcessor(input);
//        ImageProcessor outputP = new ImageProcessor(output);
//        final boolean withAlpha = input.getColorModel().hasAlpha();
//
//        final int width = inputImage.getExtent().get(0);
//        final int height = inputImage.getExtent().get(1);
//        final int maxWH = Math.max(width, height);
//
//        final int widthm = width - 1;
//        final int heightm = height - 1;
//        final int aire = width * height;
//        final int div = 2 * radius + 1;
//        final int r[] = new int[aire];
//        final int g[] = new int[aire];
//        final int b[] = new int[aire];
//        final int a[] = withAlpha ? new int[aire] : null;
//        int rsum, gsum, bsum, asum, p, yp, yi = 0, yw = 0;
//        final int vmin[] = new int[maxWH];
//
//        final int[] pixels = outputP.getPixels();
//        System.arraycopy(inputP.getPixels(), 0, pixels, 0, pixels.length);
//
//        int divsum = (div + 1) >> 1;
//        divsum *= divsum;
//        final int dv[] = new int[256 * divsum];
//        for (int i = 0; i < 256 * divsum; i++) {
//            dv[i] = (i / divsum);
//        }
//
//        final int[][] stack = withAlpha ? new int[div][4] : new int[div][3] ;
//        int stackpointer;
//        int stackstart;
//        int[] sir;
//        int rbs;
//        int r1 = radius + 1;
//        int routsum, goutsum, boutsum, aoutsum;
//        int rinsum, ginsum, binsum, ainsum;
//
//        for (int y = 0; y < height; y++) {
//            rinsum = ginsum = binsum = ainsum = routsum = goutsum = boutsum = aoutsum = rsum = gsum = bsum = asum = 0;
//            for (int i = -radius; i <= radius; i++) {
//                p = pixels[yi + Math.min(widthm, Math.max(i, 0))];
//                sir = stack[i + radius];
//                sir[0] = (p & 0x00ff0000) >> 16;
//                sir[1] = (p & 0x0000ff00) >> 8;
//                sir[2] = (p & 0x000000ff);
//                if (withAlpha) {
//                    sir[3] = (p >> 24)&0xff;
//                }
//
//                rbs = r1 - Math.abs(i);
//                rsum += sir[0] * rbs;
//                gsum += sir[1] * rbs;
//                bsum += sir[2] * rbs;
//                if (withAlpha)
//                    asum += sir[3] * rbs;
//                if (i > 0) {
//                    rinsum += sir[0];
//                    ginsum += sir[1];
//                    binsum += sir[2];
//                    if (withAlpha)
//                        ainsum += sir[3];
//                } else {
//                    routsum += sir[0];
//                    goutsum += sir[1];
//                    boutsum += sir[2];
//                    if (withAlpha)
//                        aoutsum += sir[3];
//                }
//            }
//            stackpointer = radius;
//
//            for (int x = 0; x < width; x++) {
//
//                r[yi] = dv[rsum];
//                g[yi] = dv[gsum];
//                b[yi] = dv[bsum];
//                if (withAlpha) {
//                    a[yi] = dv[asum];
//                }
//
//                rsum -= routsum;
//                gsum -= goutsum;
//                bsum -= boutsum;
//                if (withAlpha)
//                    asum -= aoutsum;
//
//                stackstart = stackpointer - radius + div;
//                sir = stack[stackstart % div];
//
//                routsum -= sir[0];
//                goutsum -= sir[1];
//                boutsum -= sir[2];
//                if (withAlpha)
//                    aoutsum -= sir[3];
//
//                if (y == 0) {
//                    vmin[x] = Math.min(x + radius + 1, widthm);
//                }
//                p = pixels[yw + vmin[x]];
//
//                sir[0] = (p & 0xff0000) >> 16;
//                sir[1] = (p & 0x00ff00) >> 8;
//                sir[2] = (p & 0x0000ff);
//                if (withAlpha)
//                    sir[3] =  (p >> 24)&0xff;
//
//                rinsum += sir[0];
//                ginsum += sir[1];
//                binsum += sir[2];
//                if (withAlpha)
//                    ainsum += sir[3];
//
//                rsum += rinsum;
//                gsum += ginsum;
//                bsum += binsum;
//                if (withAlpha)
//                    asum += ainsum;
//
//                stackpointer = (stackpointer + 1) % div;
//                sir = stack[(stackpointer) % div];
//
//                routsum += sir[0];
//                goutsum += sir[1];
//                boutsum += sir[2];
//
//                rinsum -= sir[0];
//                ginsum -= sir[1];
//                binsum -= sir[2];
//
//                if (withAlpha) {
//                    aoutsum +=sir[3];
//                    ainsum -= sir[3];
//                }
//                yi++;
//            }
//            yw += width;
//        }
//        for (int x = 0; x < width; x++) {
//            rinsum = ginsum = binsum = ainsum = aoutsum = asum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
//            yp = -radius * width;
//            for (int i = -radius; i <= radius; i++) {
//                yi = Math.max(0, yp) + x;
//
//                sir = stack[i + radius];
//
//                sir[0] = r[yi];
//                sir[1] = g[yi];
//                sir[2] = b[yi];
//                if (withAlpha)
//                    sir[3]  = a[yi];
//
//                rbs = r1 - Math.abs(i);
//
//                rsum += r[yi] * rbs;
//                gsum += g[yi] * rbs;
//                bsum += b[yi] * rbs;
//                if (withAlpha)
//                    asum += a[yi] * rbs;
//
//                if (i > 0) {
//                    rinsum += sir[0];
//                    ginsum += sir[1];
//                    binsum += sir[2];
//                    if (withAlpha)
//                        ainsum += sir[3];
//                } else {
//                    routsum += sir[0];
//                    goutsum += sir[1];
//                    boutsum += sir[2];
//                    if (withAlpha)
//                        aoutsum += sir[3];
//                }
//
//                if (i < heightm) {
//                    yp += width;
//                }
//            }
//            yi = x;
//            stackpointer = radius;
//            for (int y = 0; y < height; y++) {
//                if (withAlpha) {
//                    pixels[yi] = dv[asum]<<24 | (dv[rsum] << 16) | (dv[gsum] << 8)
//                    | dv[bsum];
//                }
//                else {
//                    pixels[yi] = 0xff000000 | (dv[rsum] << 16) | (dv[gsum] << 8)
//                            | dv[bsum];
//                }
//                rsum -= routsum;
//                gsum -= goutsum;
//                bsum -= boutsum;
//                if (withAlpha)
//                    asum -= aoutsum;
//
//                stackstart = stackpointer - radius + div;
//                sir = stack[stackstart % div];
//
//                routsum -= sir[0];
//                goutsum -= sir[1];
//                boutsum -= sir[2];
//                if (withAlpha)
//                    aoutsum -= sir[3];
//
//                if (x == 0) {
//                    vmin[y] = Math.min(y + r1, heightm) * width;
//                }
//                p = x + vmin[y];
//
//                sir[0] = r[p];
//                sir[1] = g[p];
//                sir[2] = b[p];
//                if (withAlpha)
//                    sir[3] = a[p];
//
//                rinsum += sir[0];
//                ginsum += sir[1];
//                binsum += sir[2];
//                if (withAlpha)
//                    ainsum += sir[3];
//
//                rsum += rinsum;
//                gsum += ginsum;
//                bsum += binsum;
//                if (withAlpha)
//                    asum += ainsum;
//
//                stackpointer = (stackpointer + 1) % div;
//                sir = stack[stackpointer];
//
//                routsum += sir[0];
//                goutsum += sir[1];
//                boutsum += sir[2];
//                if (withAlpha)
//                    aoutsum += sir[3];
//
//                rinsum -= sir[0];
//                ginsum -= sir[1];
//                binsum -= sir[2];
//                if (withAlpha)
//                    ainsum -= sir[3];
//                yi += width;
//            }
//        }


        return outputParameters;
    }

}
