
package science.unlicense.image.impl.process.morphologic;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.process.AbstractAreaOperator;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_EXTRAPOLATOR;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.image.impl.process.ConvolutionMatrix;
import science.unlicense.math.api.Maths;


/**
 * Morphological erosion operator.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public class ErodeOperator extends AbstractAreaOperator {

    public static final FieldType INPUT_CONV_MATRIX = new FieldTypeBuilder(new Chars("ConvolutionMatrix")).title(new Chars("Input convolution matrix")).valueClass(ConvolutionMatrix.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("erode"), new Chars("Erode"), Chars.EMPTY,ErodeOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_EXTRAPOLATOR,INPUT_CONV_MATRIX},
                new FieldType[]{OUTPUT_IMAGE});

    private static final ConvolutionMatrix DEFAULT_KERNEL = new ConvolutionMatrix(1, 1,
            new double[][]{
                {0,1,0},
                {1,1,1},
                {0,1,0},
            });

    private ConvolutionMatrix kernel;
    private double[][] matrix;
    private int height;
    private int width;
    private int originx;
    private int originy;
    private double[] buffer;

    public ErodeOperator(){
        super(DESCRIPTOR);
    }

    public Image execute(Image image, ConvolutionMatrix kernel, Chars extrapolatorType){
        inputParameters.setPropertyValue(INPUT_IMAGE.getId(),image);
        inputParameters.setPropertyValue(INPUT_EXTRAPOLATOR.getId(),extrapolatorType);
        inputParameters.setPropertyValue(INPUT_CONV_MATRIX.getId(),kernel);
        return (Image) perform().getPropertyValue(OUTPUT_IMAGE.getId());
    }

    @Override
    protected void prepareInputs() {
        kernel = (ConvolutionMatrix) inputParameters.getPropertyValue(INPUT_CONV_MATRIX.getId());
        if (kernel==null){
            //default matrix
            kernel = DEFAULT_KERNEL;
        }
        matrix = kernel.getValuesCopy();
        height = matrix.length;
        width = matrix[0].length;
        buffer = new double[height * width];
        originx = kernel.getOriginX();
        originy = kernel.getOriginY();
    }

    @Override
    protected int getTopPadding() {
        return kernel.getTopPadding();
    }

    @Override
    protected int getBottomPadding() {
        return kernel.getBottomPadding();
    }

    @Override
    protected int getLeftPadding() {
        return kernel.getLeftPadding();
    }

    @Override
    protected int getRightPadding() {
        return kernel.getRightPadding();
    }

    @Override
    protected void evaluate(double[][][] workspace, double[] result) {

        for (int canal = 0; canal < result.length; canal++) {

            for (int kj = 0; kj < height; kj++) {
                for (int ki = 0; ki < width; ki++) {
                    buffer[width * kj + ki] =
                            workspace[originy][originx][canal]
                            - matrix[kj][ki] * workspace[kj][ki][canal];
                }
            }

            result[canal] = Maths.min(buffer) + workspace[originy][originx][canal];
        }
    }

}
