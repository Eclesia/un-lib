
package science.unlicense.image.impl.process.analyze;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.DoubleSequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.MishandleException;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.task.api.AbstractTask;
import science.unlicense.geometry.impl.MultiPolygon;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.math.api.TupleRW;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.math.impl.Vector2i32;

/**
 * This algorithm extract a contour geometry from given image.
 *
 * This operator outputs one multipolygon per predicate.
 *
 * @author Johann Sorel
 */
public class ContourOperator extends AbstractTask {

    public static final FieldType INPUT_PREDICATES = new FieldTypeBuilder(new Chars("Predicates")).title(new Chars("Predicates")).valueClass(Predicate[].class).build();
    public static final FieldType INPUT_MODEL = new FieldTypeBuilder(new Chars("Model")).title(new Chars("Model")).valueClass(Chars.class).defaultValue(ImageModel.MODEL_COLOR).build();
    public static final FieldType OUTPUT_GEOMETRIES = new FieldTypeBuilder(new Chars("Geometries")).title(new Chars("Geometries")).valueClass(MultiPolygon[].class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("contour"), new Chars("Contour"), Chars.EMPTY,ContourOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_PREDICATES,INPUT_MODEL},
                new FieldType[]{OUTPUT_GEOMETRIES});

    public ContourOperator() {
        super(DESCRIPTOR);
    }

    public MultiPolygon execute(Image image, Predicate predicate, Chars modelName) {
        inputs().setPropertyValue(INPUT_IMAGE.getId(), image);
        inputs().setPropertyValue(INPUT_PREDICATES.getId(), new Predicate[]{predicate});
        inputs().setPropertyValue(INPUT_MODEL.getId(), modelName);
        final Document doc = perform();
        return ((MultiPolygon[]) doc.getPropertyValue(OUTPUT_GEOMETRIES.getId()))[0];
    }

    public MultiPolygon[] execute(Image image, Predicate[] predicates, Chars modelName) {
        inputs().setPropertyValue(INPUT_IMAGE.getId(), image);
        inputs().setPropertyValue(INPUT_PREDICATES.getId(), predicates);
        inputs().setPropertyValue(INPUT_MODEL.getId(), modelName);
        final Document doc = perform();
        return (MultiPolygon[]) doc.getPropertyValue(OUTPUT_GEOMETRIES.getId());
    }

    public Document perform() {
        final Image inputImage = (Image) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final Predicate[] predicates = (Predicate[]) inputParameters.getPropertyValue(INPUT_PREDICATES.getId());
        final Chars modelName = (Chars) inputParameters.getPropertyValue(INPUT_MODEL.getId());

        final ImageModel model = (ImageModel) inputImage.getModels().getValue(modelName);
        final TupleGrid tb = model.asTupleBuffer(inputImage);

        final TupleRW sample = tb.createTuple();
        final long[] dimensions = inputImage.getExtent().toArray();

        final ContourBuilder builder[] = new ContourBuilder[predicates.length];
        for (int i=0;i<builder.length;i++){
            builder[i] = new ContourBuilder((int) dimensions[0], (int) dimensions[1]);
        }

        final Vector2i32 coord = new Vector2i32();
        for (coord.y=0;coord.y<dimensions[1];coord.y++){
            for (int i=0;i<builder.length;i++){
                builder[i].nextLine();
            }
            for (coord.x=0;coord.x<dimensions[0];coord.x++){
                tb.getTuple(coord, sample);

                for (int i=0;i<builder.length;i++){
                    builder[i].put(predicates[i].evaluate(sample));
                }
            }
        }

        //get the result
        final MultiPolygon[] polygons = new MultiPolygon[predicates.length];
        for (int i=0;i<polygons.length;i++) polygons[i] = builder[i].build();
        outputParameters.setPropertyValue(OUTPUT_GEOMETRIES.getId(),polygons);
        return outputParameters;
    }

    private static class ContourBuilder{

        private int x = 0;
        private int y = 0;
        private GeomRef[] previous;
        private GeomRef[] current;
        private final Sequence polygons = new ArraySequence();

        private ContourBuilder(int width, int height){
            previous = new GeomRef[width+1];
            current = new GeomRef[width+1];
        }

        private void nextLine(){

            final GeomRef[] temp = previous;
            previous = current;
            current = temp;
            Arrays.fill(current, null);
        }

        private void put(boolean state){

            final boolean top = y!=0 && previous[x]!=null;
            final boolean left = x!=0 && current[x-1]!=null;
            final boolean topleft = x!=0 && y!=0 && previous[x-1]!=null;

            if (state) {
                if (topleft && top && left) {
                    // █ █
                    //
                    // █ █
                    // do nothing
                    current[x] = current[x-1];
                } else if (topleft && top) {
                    // █ █
                    // ─┐
                    // o║█
                    current[x] = previous[x-1];
                    current[x].pushDown(x, y);
                } else if (topleft && left) {
                    // █│o
                    //  └═
                    // █ █
                    current[x] = previous[x-1];
                    current[x].pushRight(x, y);
                } else if (top && left) {
                    // o│█
                    // ─╝
                    // █ █

                    //in this case, previous[x] and current[x] can be different
                    //geometries, we must merge them
                    current[x] = current[x-1];
                    current[x].merge(x, y, previous[x]);
                    current[x].geom.merge(x, y);
                } else if (topleft) {
                    // █│o
                    // ─┼═
                    // o║█
                    previous[x-1].geom.merge(x, y);
                    current[x] = new GeomRef(new TempGeometry(this));
                    current[x].newLoop(x, y);
                } else if (top) {
                    // o│█
                    //  │
                    // o║█
                    current[x] = previous[x];
                    current[x].pushDown(x, y);
                } else if (left) {
                    // o o
                    // ──═
                    // █ █
                    current[x] = current[x-1];
                    current[x].pushRight(x, y);
                } else {
                    // o o
                    //  ╔═
                    // o║█
                    current[x] = new GeomRef(new TempGeometry(this));
                    current[x].newLoop(x, y);
                }
            } else {
                if (topleft && top && left) {
                    // █ █
                    //  ╔═
                    // █║o
                    ensureSame(previous[x-1], previous[x], current[x-1]);
                    previous[x].newLoop(x, y);
                } else if (topleft && top) {
                    // █ █
                    // ──═
                    // o o
                    ensureSame(previous[x-1], previous[x]);
                    previous[x-1].pushRight(x, y);

                } else if (topleft && left) {
                    // █│o
                    //  │
                    // █║o
                    ensureSame(previous[x-1], current[x-1]);
                    current[x-1].pushDown(x, y);
                } else if (top && left) {
                    // o│█
                    // ─┼═
                    // █║o
                    previous[x].pushRight(x, y);
                    current[x-1].pushDown(x, y);
                } else if (topleft) {
                    // █│o
                    // ═┘
                    // o o
                    previous[x-1].geom.merge(x, y);
                } else if (top) {
                    // o│█
                    //  └═
                    // o o
                    previous[x].pushRight(x, y);
                } else if (left) {
                    // o o
                    // ─┐
                    // █║o
                    current[x-1].pushDown(x, y);
                } else {
                    // o o
                    //
                    // o o
                    // do nothing
                }
            }

            //prepare next iteration
            x++;
            if (x==previous.length-1) {
                //add a false last column
                put(false);
                x = 0;
                y++;
            }
        }

        private MultiPolygon build(){
            //add one false last line;
            nextLine();
            for (int i=0;i<current.length-1;i++) put(false);
            return new MultiPolygon(polygons);
        }

    }

    private static class GeomRef {

        private TempGeometry geom;

        private GeomRef(TempGeometry geom) {
            this.geom = geom;
        }

        private Loop newLoop(int x, int y) {
            return geom.newLoop(x,y);
        }

        private void pushRight(int x, int y) {
            geom.getEnd(x, y).pushRight();
        }

        private void pushDown(int x, int y) {
            geom.getEnd(x, y).pushDown();
        }

        private void merge(int x, int y, GeomRef other){
            if (geom == other.geom) return;
            geom.merge(x, y, other.geom);
            other.geom = this.geom;
        }
    }

    private static class TempGeometry {

        private final ContourBuilder builder;
        private final Sequence loops = new ArraySequence();

        public TempGeometry(ContourBuilder builder) {
            this.builder = builder;
        }

        Loop newLoop(int x, int y) {
            final Loop loop = new Loop(this,x,y);
            loops.add(loop);
            return loop;
        }

        Loop.End getEnd(int x, int y) {
            for (int i=0,n=loops.getSize();i<n;i++) {
                final Loop loop = (Loop) loops.get(i);
                if (loop.start.match(x, y)) return loop.start;
                if (loop.end.match(x, y)) return loop.end;
            }
            throw new MishandleException("Should not happen");
        }

        private void finish(){
            if (loops.getSize()>1) return;
            final Loop loop = (Loop) loops.get(0);
            final int size = loop.coords.getSize();
            if (loop.coords.read(0)==loop.coords.read(size-2) && loop.coords.read(1)==loop.coords.read(size-1)) {
                final Polyline outer = new Polyline(InterleavedTupleGrid1D.create(loop.coords.toArrayDouble(), 2));
                loops.removeAll();
                builder.polygons.add(new Polygon(outer, null));
            }
        }

        private void merge(int x, int y, TempGeometry other){
            this.loops.addAll(other.loops);
            merge(x, y);
        }

        private void merge(int x, int y){
            if (loops.getSize()<2) return;
            Loop loop1 = null;
            Loop loop2 = null;
            int i=0;
            int n=loops.getSize();
            for (;i<n;i++) {
                final Loop loop = (Loop) loops.get(i);
                if (loop.start.match(x, y) || loop.end.match(x, y)) {
                    loop1 = loop;
                    break;
                }
            }
            for (i++;i<n;i++) {
                final Loop loop = (Loop) loops.get(i);
                if (loop.start.match(x, y) || loop.end.match(x, y)) {
                    loop2 = loop;
                    break;
                }
            }
            if (loop1==null || loop2==null) return;

            loops.remove(loop2);
            final double[] data = loop2.coords.toArrayDouble();
            if (loop1.end.match(loop2.start)) {
                loop1.coords.moveTo(loop1.coords.getSize());
                loop1.coords.put(data, 2, data.length-2);
            } else if (loop1.end.match(loop2.end)) {
                Arrays.reverse(data, 0, data.length, 2);
                loop1.coords.moveTo(loop1.coords.getSize());
                loop1.coords.put(data, 2, data.length-2);
            } else if (loop1.start.match(loop2.start)) {
                Arrays.reverse(data, 0, data.length, 2);
                loop1.coords.moveTo(0);
                loop1.coords.insert(data, 0, data.length-2);
            } else if (loop1.start.match(loop2.end)) {
                loop1.coords.moveTo(0);
                loop1.coords.insert(data, 0, data.length-2);
            } else {
                throw new MishandleException("Should not happen");
            }

            loop1.start.goingRight = null;
            loop1.end.goingRight = null;
            finish();
        }

    }

    private static class Loop {

        private final TempGeometry geom;
        private final DoubleSequence coords = new DoubleSequence();
        private final double[] buffer = new double[2];
        private final End start = new End() {
            int getPreviousIndex() { return 2; }
            int getIndex() { return 0; }
            int getNextIndex() { return 0; }
        };
        private final End end = new End() {
            int getPreviousIndex() { return coords.getSize()-4; }
            int getIndex() { return coords.getSize()-2; }
            int getNextIndex() { return coords.getSize(); }
        };

        public Loop(TempGeometry geom, int x, int y) {
            this.geom = geom;
            //add the 2 first segments
            coords.put(x);
            coords.put(y+1);
            coords.put(x);
            coords.put(y);
            coords.put(x+1);
            coords.put(y);
            start.goingRight = false;
            end.goingRight = true;
        }

        public String toString() {
            return new Polyline(InterleavedTupleGrid1D.create(coords.toArrayDouble(), 2)).toString();
        }

        private abstract class End {
            Boolean goingRight=null;
            abstract int getPreviousIndex();
            abstract int getIndex();
            abstract int getNextIndex();
            private Loop getLoop() {
                return Loop.this;
            }

            void computeDirection(){
                if (goingRight!=null) return;
                final int pidx = getIndex();
                final int idx = getIndex();
                if (pidx<0 ||pidx>coords.getSize()) {
                   goingRight = null;
                } else {
                    goingRight = coords.read(idx) == coords.read(pidx)+1;
                }
            }

            void pushRight() {
                computeDirection();
                int index = getIndex();
                if (Boolean.TRUE.equals(goingRight)) {
                    //move coordinate
                    coords.moveTo(index);
                    coords.put(coords.read(index)+1);
                } else {
                    //add a new point
                    coords.read(index,buffer,0,2);
                    buffer[0]++;
                    coords.moveTo(getNextIndex());
                    coords.insert(buffer);
                }
                geom.finish();
                goingRight = true;
            }
            void pushDown() {
                computeDirection();
                int index = getIndex();
                if (Boolean.FALSE.equals(goingRight)) {
                    //move coordinate
                    coords.moveTo(index+1);
                    coords.put(coords.read(index+1)+1);
                } else {
                    //add a new point
                    coords.read(index,buffer,0,2);
                    buffer[1]++;
                    coords.moveTo(getNextIndex());
                    coords.insert(buffer);
                }
                geom.finish();
                goingRight = false;
            }
            boolean match(double x, double y) {
                coords.read(getIndex(),buffer,0,2);
                return buffer[0]==x && buffer[1]==y;
            }
            boolean match(End end) {
                final int index = end.getIndex();
                final double x = end.getLoop().coords.read(index);
                final double y = end.getLoop().coords.read(index+1);
                return match(x, y);
            }
        }
    }


    private static void ensureSame(GeomRef a, GeomRef b) throws InvalidArgumentException {
        if (a.geom != b.geom){
            throw new InvalidArgumentException("Objects are different");
        }
    }

    private static void ensureSame(GeomRef a, GeomRef b, GeomRef c) throws InvalidArgumentException {
        if (a.geom != b.geom || b.geom != c.geom){
            throw new InvalidArgumentException("Objects are different");
        }
    }

}
