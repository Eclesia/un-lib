
package science.unlicense.image.impl.process.analyze;

import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.transform.AbstractTransform;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class ThresholdTransform extends AbstractTransform {

    private final Tuple under;
    private final Tuple above;
    private final Predicate limit;

    public ThresholdTransform(Tuple under, Tuple above, Predicate limit) {
        super(under.getSampleSystem());
        this.under = under;
        this.above = above;
        this.limit = limit;
    }

    @Override
    public TupleRW transform(Tuple source, TupleRW dest) {
        if (dest == null) dest = VectorNf64.create(under);
        if (limit.evaluate(source)) {
            dest.set(above);
        } else {
            dest.set(under);
        }
        return dest;
    }

    @Override
    public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Transform invert() {
        throw new UnsupportedOperationException("Not invertable.");
    }

}
