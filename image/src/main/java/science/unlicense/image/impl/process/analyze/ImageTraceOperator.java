
package science.unlicense.image.impl.process.analyze;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.Image;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.task.api.AbstractTask;
import science.unlicense.geometry.impl.algorithm.PathSmoothing;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ColorIndex;
import science.unlicense.math.api.TupleRW;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.math.impl.Vector2i32;

/**
 *   ImageTracer.java (Desktop version with javax.imageio. See ImageTracerAndroid.java for the Android version.)
 *   Simple raster image tracer and vectorizer written in Java. This is a port of imagetracer.js.
 *   by András Jankovics 2015
 *   andras@jankovics.net
 *
 *   Tips:
 *    - color quantization uses randomization to recycle little used colors when colorquantcycles > 1 ,
 *      which means that output will be different every time. Use colorquantcycles = 1 to get deterministic output.
 *    - palette generation uses randomization to make some of the colors. Set numberofcolors to a qubic number
 *      (e.g. 8, 27 or 64) to get a deterministic palette, or use a custom palette.
 *    - ltres,qtres : lower linear (ltres) and quadratic spline (qtres) error tresholds result
 *      more details at the cost of longer paths (more segments). Usually 0.5 or 1 is good for both. When tracing
 *      round shapes, lower ltres, e.g. ltres=0.2 qtres=1 will result more curves, thus maybe better quality. Similarly,
 *      ltres=1 qtres=0 is better for polygonal shapes. Values greater than 2 will usually result inaccurate paths.
 *    - pathomit : the length of the shortest path is 4, four corners around one pixel. The default pathomit=8 filters out
 *      isolated one and two pixels for noise reduction. This can be deactivated by setting pathomit=0.
 *    - pal, numberofcolors : custom palette in the format pal=[colornum][4]; or automatic palette
 *      with the given length. Many colors will result many layers, so longer processing time and more paths, but better
 *      quality. When using few colors, more colorquantcycles can improve quality.
 *    - mincolorratio : minimum ratio of pixels, below this the color will be randomized. mincolorratio=0.02 for a 10*10 image
 *      means that if a color has less than 10*10 * 0.02 = 2 pixels, it will be randomized.
 *    - colorquantcycles : color quantization will be repeated this many times. When using many colors, this can be a lower value.
 *    - scale : optionally, the SVG output can be scaled, scale=2 means the SVG will have double height and width
 *
 *   Process overview
 *   ----------------
 *
 *   // 1. Color quantization
 *   // 2. Layer separation and edge detection
 *   // 3. Batch pathscan
 *   // 4. Batch interpollation
 *   // 5. Batch tracing
 *   // 6. Creating SVG string
 *
 * Origin :
 * https://github.com/jankovicsandras/imagetracerjava
 *
 * @author jankovics andras (andras@jankovics.net)
 * @author Johann Sorel (Adapted to Unlicense-lib)
 */
public class ImageTraceOperator extends AbstractTask{

    // Tracing
    public static final FieldType INPUT_LTRES = new FieldTypeBuilder(new Chars("ltres")).valueClass(Float.class).defaultValue(1f).build();
    public static final FieldType INPUT_QTRES = new FieldTypeBuilder(new Chars("qtres")).valueClass(Float.class).defaultValue(1f).build();
    public static final FieldType INPUT_PATHOMIT = new FieldTypeBuilder(new Chars("pathomit")).valueClass(Integer.class).defaultValue(8).build();

    // Color quantization
    public static final FieldType INPUT_NUMBER_OF_COLORS = new FieldTypeBuilder(new Chars("numberofcolors")).valueClass(Integer.class).defaultValue(16).build();

    // SVG rendering
    public static final FieldType INPUT_LCPR = new FieldTypeBuilder(new Chars("lcpr")).valueClass(Float.class).defaultValue(0f).build();
    public static final FieldType INPUT_QCPR = new FieldTypeBuilder(new Chars("qcpr")).valueClass(Float.class).defaultValue(0f).build();


    public static final FieldType OUTPUT_SVG = new FieldTypeBuilder(new Chars("svg")).valueClass(String.class).build();

    public static DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("imagetrace"), new Chars("ImageTrace"), Chars.EMPTY, ThresholdOperator.class,
                new FieldType[]{INPUT_IMAGE,
                    INPUT_LTRES,INPUT_QTRES,INPUT_PATHOMIT,
                    INPUT_NUMBER_OF_COLORS,
                    INPUT_LCPR,INPUT_QCPR
                },
                new FieldType[]{OUTPUT_SVG});


    public ImageTraceOperator(){
        super(DESCRIPTOR);
    }

    @Override
    public Document perform() {
        final Image img = (Image) inputParameters.getPropertyValue(INPUT_IMAGE.getId());
        try {
            String svg = imageToSVG(img, null);
            outputParameters.setPropertyValue(OUTPUT_SVG.getId(),svg);
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

        return outputParameters;
    }

    // Container for the color-indexed image before and tracedata after vectorizing
    public static class IndexedImage{
        public int [][] array; // array[x][y] of palette colors
        public byte [][] palette;// array[palettelength][4] RGBA color palette
        public int background;// palette index of background color
        public ArrayList<ArrayList<ArrayList<double[]>>> layers;// tracedata

        public IndexedImage(int [][] marray, byte [][] mpalette, int mbackground){
            array = marray;
            palette = mpalette;
            background = mbackground;
        }
    }

    /**
     * The bitshift method in loadImageData creates signed bytes where
     * -1 -> 255 unsigned ;
     * -128 -> 128 unsigned ;
     * 127 -> 127 unsigned ;
     * 0 -> 0 unsigned ;
     * These will be converted to -128 (representing 0 unsigned) ...
     * 127 (representing 255 unsigned) and torgbstr will add +128 to create RGB values 0..255
     */
    private static byte bytetrans(byte b){
        if (b<0){
            return (byte) (b+128);
        } else {
            return (byte) (b-128);
        }
    }

    ////////////////////////////////////////////////////////////
    //
    //  User friendly functions
    //
    ////////////////////////////////////////////////////////////

    // Tracing ImageData, then returning the SVG String
    private String imageToSVG (Image imgd, byte[][] palette){
        IndexedImage ii = imagedataToTracedata(imgd,palette);

        return getsvgstring(imgd.getExtent().get(0), imgd.getExtent().get(1),ii);
    }

    // Tracing ImageData, then returning IndexedImage with tracedata in layers
    private IndexedImage imagedataToTracedata (Image imgd, byte[][] palette){
        final int numberofcolors = (Integer) inputParameters.getPropertyValue(INPUT_NUMBER_OF_COLORS.getId());
        final int pathomit = (Integer) inputParameters.getPropertyValue(INPUT_PATHOMIT.getId());
        final float ltres = (Float) inputParameters.getPropertyValue(INPUT_LTRES.getId());
        final float qtres = (Float) inputParameters.getPropertyValue(INPUT_QTRES.getId());

        // 1. Color quantization
        IndexedImage ii = colorquantization(imgd, palette, numberofcolors);
        // 2. Layer separation and edge detection
        int[][][] rawlayers = layering(ii);
        // 3. Batch pathscan
        ArrayList<ArrayList<ArrayList<int[]>>> bps = batchpathscan(rawlayers,pathomit);
        // 4. Batch interpollation
        ArrayList<ArrayList<ArrayList<double[]>>> bis = batchinternodes(bps);
        // 5. Batch tracing
        ii.layers = batchtracelayers(bis,ltres,qtres);
        return ii;
    }


    ////////////////////////////////////////////////////////////
    //
    //  Vectorizing functions
    //
    ////////////////////////////////////////////////////////////

    // 1. Color quantization
    private IndexedImage colorquantization(Image imgd, byte[][] palette, int numberofcolors){
        final int width = (int) imgd.getExtent().get(0);
        final int height = (int) imgd.getExtent().get(1);
        final TupleGrid tb = imgd.getColorModel().asTupleBuffer(imgd);
        final ColorSystem cs = (ColorSystem) imgd.getColorModel().getSampleSystem();

        final Vector2i32 coord = new Vector2i32();

        // Creating indexed color array arr which has a boundary filled with -1 in every direction
        final int[][] arr = new int[height+2][width+2];
        for (int j=0; j<height+2; j++){
            arr[j][0] = -1;
            arr[j][width+1 ] = -1;
        }
        for (int i=0; i<width+2 ; i++){
            arr[0][i] = -1;
            arr[height+1][i] = -1;
        }

        int cd,cdl,ci,c1,c2,c3;

        if (palette==null){
            palette = generatepalette(imgd, numberofcolors);
        }

        // loop through all pixels
        final TupleRW tuple = tb.createTuple();
        for (int j=0;j<height;j++){
            for (int i=0;i<width;i++){
                coord.x = i; coord.y = j;
                tb.getTuple(coord,tuple);
                final Color color = new ColorRGB(tuple,cs);
                final byte r = bytetrans((byte) (color.getRed()*255));
                final byte g = bytetrans((byte) (color.getGreen()*255));
                final byte b = bytetrans((byte) (color.getBlue()*255));

                // find closest color from palette
                cdl = 256+256+256; ci=0;
                for (int k=0;k<palette.length;k++){
                    // In my experience, https://en.wikipedia.org/wiki/Rectilinear_distance works better than https://en.wikipedia.org/wiki/Euclidean_distance
                    c1 = Math.abs(palette[k][0]-r);
                    c2 = Math.abs(palette[k][1]-g);
                    c3 = Math.abs(palette[k][2]-b);
                    cd = c1+c2+c3;
                    if (cd<cdl){
                        cdl = cd; ci = k;
                    }
                }

                arr[j+1][i+1] = ci;
            }
        }

        return new IndexedImage(arr, palette, arr[1][1]);
    }

    /**
     * Generating a palette with numberofcolors, array[numberofcolors][4] where [i][0] = R ; [i][1] = G ; [i][2] = B ; [i][3] = A
     */
    private byte[][] generatepalette(Image img, int numberofcolors){
        final ColorIndex index = new CreateColorIndexOperator().executeNow(img, numberofcolors);
        byte[][] p = new byte[index.palette.length][4];
        for (int i=0;i<p.length;i++){
            final Color c = index.palette[i];
            p[i][0] = (byte) (-128+(c.getRed()*255));
            p[i][1] = (byte) (-128+(c.getGreen()*255));
            p[i][2] = (byte) (-128+(c.getBlue()*255));
            p[i][3] = (byte) 255;
        }
        return p;
    }

    // 2. Layer separation and edge detection
    // Edge node types ( ▓:light or 1; ░:dark or 0 )
    // 12  ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓
    // 48  ░░  ░░  ░░  ░░  ░▓  ░▓  ░▓  ░▓  ▓░  ▓░  ▓░  ▓░  ▓▓  ▓▓  ▓▓  ▓▓
    //     0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15
    //
    private int[][][] layering(IndexedImage im){
        // Creating layers for each indexed color in arr
        int val;
        int ah = im.array.length;
        int aw = im.array[0].length;
        int n1,n2,n3,n4,n5,n6,n7,n8;
        int[][][] layers = new int[im.palette.length][ah][aw];

        for (int j=1;j<ah-1;j++){
            for (int i=1;i<aw-1;i++){

                // This pixel"s indexed color
                val = im.array[j][i];

                // Are neighbor pixel colors the same?
                if ((j>0)    && (i>0))   { n1 = im.array[j-1][i-1]==val?1:0; } else { n1 = 0; }
                if (j>0)                { n2 = im.array[j-1][i  ]==val?1:0; } else { n2 = 0; }
                if ((j>0)    && (i<aw-1)){ n3 = im.array[j-1][i+1]==val?1:0; } else { n3 = 0; }
                if (i>0)                { n4 = im.array[j  ][i-1]==val?1:0; } else { n4 = 0; }
                if (i<aw-1)             { n5 = im.array[j  ][i+1]==val?1:0; } else { n5 = 0; }
                if ((j<ah-1) && (i>0))   { n6 = im.array[j+1][i-1]==val?1:0; } else { n6 = 0; }
                if (j<ah-1)             { n7 = im.array[j+1][i  ]==val?1:0; } else { n7 = 0; }
                if ((j<ah-1) && (i<aw-1)){ n8 = im.array[j+1][i+1]==val?1:0; } else { n8 = 0; }

                // this pixel"s type and looking back on previous pixels
                layers[val][j+1][i+1] = 1 + n5 * 2 + n8 * 4 + n7 * 8 ;
                if (n4==0){ layers[val][j+1][i  ] = 0 + 2 + n7 * 4 + n6 * 8 ; }
                if (n2==0){ layers[val][j  ][i+1] = 0 + n3*2 + n5 * 4 + 8 ; }
                if (n1==0){ layers[val][j  ][i  ] = 0 + n2*2 + 4 + n4 * 8 ; }

            }
        }

        return layers;
    }

    // 3. Walking through an edge node array, discarding edge node types 0 and 15 and creating paths from the rest.
    // Walk directions (dir): 0 > ; 1 ^ ; 2 < ; 3 v
    // Edge node types ( ▓:light or 1; ░:dark or 0 )
    // ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓  ░░  ▓░  ░▓  ▓▓
    // ░░  ░░  ░░  ░░  ░▓  ░▓  ░▓  ░▓  ▓░  ▓░  ▓░  ▓░  ▓▓  ▓▓  ▓▓  ▓▓
    // 0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15
    //
    private ArrayList<ArrayList<int[]>> pathscan(int[][] arr, float pathomit){
        ArrayList<ArrayList<int[]>> paths = new ArrayList<ArrayList<int[]>>();
        ArrayList<int[]> thispath;
        int pacnt=0, px=0,py=0,w=arr[0].length,h=arr.length,dir=0,stepcnt=0,maxsteps=w*h*2;
        boolean pathfinished=true, holepath = false;

        for (int j=0;j<h;j++){
            for (int i=0;i<w;i++){
                if ((arr[j][i]==0)||(arr[j][i]==15)){// Discard
                    stepcnt++;
                } else {// Follow path

                    // Init
                    px = i; py = j;
                    paths.add(new ArrayList<int[]>());
                    thispath = paths.get(paths.size()-1);
                    pathfinished = false;
                    // fill paths will be drawn, but hole paths are also required to remove unnecessary edge nodes
                    if (arr[py][px]==1){dir = 0;}
                    if (arr[py][px]==2){dir = 3;}
                    if (arr[py][px]==3){dir = 0;}
                    if (arr[py][px]==4){dir = 1; holepath = false;}
                    if (arr[py][px]==5){dir = 0;}
                    if (arr[py][px]==6){dir = 3;}
                    if (arr[py][px]==7){dir = 0; holepath = true;}
                    if (arr[py][px]==8){dir = 0;}
                    if (arr[py][px]==9){dir = 3;}
                    if (arr[py][px]==10){dir = 3;}
                    if (arr[py][px]==11){dir = 1; holepath = true;}
                    if (arr[py][px]==12){dir = 0;}
                    if (arr[py][px]==13){dir = 3; holepath = true;}
                    if (arr[py][px]==14){dir = 0; holepath = true;}
                    // Path points loop
                    while (!pathfinished){

                        // New path point
                        thispath.add(new int[3]);
                        thispath.get(thispath.size()-1)[0] = px-1;
                        thispath.get(thispath.size()-1)[1] = py-1;
                        thispath.get(thispath.size()-1)[2] = arr[py][px];

                        // Node types
                        if (arr[py][px]==1){
                            arr[py][px] = 0;
                            if (dir==0){
                                py--;dir=1;
                            } else if (dir==3){
                                px--;dir=2;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        else if (arr[py][px]==2){
                            arr[py][px] = 0;
                            if (dir==3){
                                px++;dir=0;
                            } else if (dir==2){
                                py--;dir=1;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        else if (arr[py][px]==3){
                            arr[py][px] = 0;
                            if (dir==0){
                                px++;
                            } else if (dir==2){
                                px--;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        else if (arr[py][px]==4){
                            arr[py][px] = 0;
                            if (dir==1){
                                px++;dir=0;
                            } else if (dir==2){
                                py++;dir=3;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        else if (arr[py][px]==5){
                            if (dir==0){
                                arr[py][px] = 13;py++;dir=3;
                            } else if (dir==1){
                                arr[py][px] = 13;px--;dir=2;
                            } else if (dir==2){
                                arr[py][px] = 7;py--;dir=1;
                            } else if (dir==3){
                                arr[py][px] = 7;px++;dir=0;
                            }
                        }

                        else if (arr[py][px]==6){
                            arr[py][px] = 0;
                            if (dir==1){
                                py--;
                            } else if (dir==3){
                                py++;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        else if (arr[py][px]==7){
                            arr[py][px] = 0;
                            if (dir==0){
                                py++;dir=3;
                            } else if (dir==1){
                                px--;dir=2;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        else if (arr[py][px]==8){
                            arr[py][px] = 0;
                            if (dir==0){
                                py++;dir=3;
                            } else if (dir==1){
                                px--;dir=2;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        else if (arr[py][px]==9){
                            arr[py][px] = 0;
                            if (dir==1){
                                py--;
                            } else if (dir==3){
                                py++;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        else if (arr[py][px]==10){
                            if (dir==0){
                                arr[py][px] = 11;py--;dir=1;
                            } else if (dir==1){
                                arr[py][px] = 14;px++;dir=0;
                            } else if (dir==2){
                                arr[py][px] = 14;py++;dir=3;
                            } else if (dir==3){
                                arr[py][px] = 11;px--;dir=2;
                            }
                        }

                        else if (arr[py][px]==11){
                            arr[py][px] = 0;
                            if (dir==1){
                                px++;dir=0;
                            } else if (dir==2){
                                py++;dir=3;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        else if (arr[py][px]==12){
                            arr[py][px] = 0;
                            if (dir==0){
                                px++;
                            } else if (dir==2){
                                px--;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        else if (arr[py][px]==13){
                            arr[py][px] = 0;
                            if (dir==2){
                                py--;dir=1;
                            } else if (dir==3){
                                px++;dir=0;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        else if (arr[py][px]==14){
                            arr[py][px] = 0;
                            if (dir==0){
                                py--;dir=1;
                            } else if (dir==3){
                                px--;dir=2;
                            } else {pathfinished=true;paths.remove(thispath);}
                        }

                        // Close path
                        if ((px-1==thispath.get(0)[0])&&(py-1==thispath.get(0)[1])){
                            pathfinished = true;
                            pacnt++;
                            // Discarding "hole" type paths
                            if (holepath){
                                paths.remove(thispath);pacnt--;
                            }
                            // Discarding if path is shorter than pathomit
                            if (thispath.size()<pathomit){
                                paths.remove(thispath);pacnt--;
                            }
                        }

                        // Error: path going out of image
                        if ((px<0)||(px>=w)||(py<0)||(py>=h)){
                            pathfinished = true;
                            System.out.println("path "+pacnt+" error w "+w+" h "+h+" px "+px+" py "+py);
                            paths.remove(thispath);
                        }

                        // Error: stepcnt>maxsteps
                        if (stepcnt>maxsteps){
                            pathfinished = true;
                            System.out.println("path "+pacnt+" error stepcnt "+stepcnt+" maxsteps "+maxsteps+" px "+px+" py "+py);
                            paths.remove(thispath);
                        }

                        stepcnt++;

                    }// End of Path points loop

                }// End of Follow path

            }// End of i loop
        }// End of j loop

        return paths;
    }


    // 3. Batch pathscan
    private ArrayList<ArrayList<ArrayList<int[]>>> batchpathscan (int[][][] layers, float pathomit){
        ArrayList<ArrayList<ArrayList<int[]>>> bpaths = new ArrayList<ArrayList<ArrayList<int[]>>>();
        for (int k=0; k<layers.length; k++){
            bpaths.add(pathscan(layers[k],pathomit));
        }
        return bpaths;
    }

    // 4. interpolating between path points for nodes with 8 directions ( East, SouthEast, S, SW, W, NW, N, NE )
    private ArrayList<ArrayList<double[]>> internodes(ArrayList<ArrayList<int[]>> paths){
        final ArrayList<ArrayList<double[]>> ins = new ArrayList<ArrayList<double[]>>();
        final double[] nextpoint = new double[2];
        int[] pp1, pp2, pp3;

        // paths loop
        for (int pacnt=0; pacnt<paths.size(); pacnt++){
            final ArrayList<int[]> path = paths.get(pacnt);
            final ArrayList<double[]> thisinp = new ArrayList<double[]>();
            ins.add(thisinp);

            // pathpoints loop
            for (int pcnt=0,palen=path.size();pcnt<palen;pcnt++){

                // interpolate between two path points
                final double[] thispoint = new double[3];
                thisinp.add(thispoint);
                pp1 = path.get(pcnt);
                pp2 = path.get((pcnt+1)%palen);
                pp3 = path.get((pcnt+2)%palen);
                thispoint[0] = (pp1[0]+pp2[0]) / 2.0;
                thispoint[1] = (pp1[1]+pp2[1]) / 2.0;
                nextpoint[0] = (pp2[0]+pp3[0]) / 2.0;
                nextpoint[1] = (pp2[1]+pp3[1]) / 2.0;


                // line segment direction to the next point
                if (thispoint[0] < nextpoint[0]){
                    if     (thispoint[1] < nextpoint[1]){ thispoint[2] = 1.0; }// SouthEast
                    else if (thispoint[1] > nextpoint[1]){ thispoint[2] = 7.0; }// NE
                    else                                { thispoint[2] = 0.0; } // E
                } else if (thispoint[0] > nextpoint[0]){
                    if     (thispoint[1] < nextpoint[1]){ thispoint[2] = 3.0; }// SW
                    else if (thispoint[1] > nextpoint[1]){ thispoint[2] = 5.0; }// NW
                    else                                { thispoint[2] = 4.0; }// N
                } else {
                    if     (thispoint[1] < nextpoint[1]){ thispoint[2] = 2.0; }// S
                    else if (thispoint[1] > nextpoint[1]){ thispoint[2] = 6.0; }// N
                    else                                { thispoint[2] = 8.0; }// center, this should not happen
                }
            }
        }

        return ins;
    }

    // 4. Batch interpollation
    private ArrayList<ArrayList<ArrayList<double[]>>> batchinternodes(ArrayList<ArrayList<ArrayList<int[]>>> bpaths){
        ArrayList<ArrayList<ArrayList<double[]>>> binternodes = new ArrayList<ArrayList<ArrayList<double[]>>>();
        for (int k=0; k<bpaths.size(); k++) {
            binternodes.add(internodes(bpaths.get(k)));
        }
        return binternodes;
    }

    // 5. tracepath() : recursively trying to fit straight and quadratic spline segments on the 8 direction internode path

    // 5.1. Find sequences of points with only 2 segment types
    // 5.2. Fit a straight line on the sequence
    // 5.3. If the straight line fails (an error>ltreshold), find the point with the biggest error
    // 5.4. Fit a quadratic spline through errorpoint (project this to get controlpoint), then measure errors on every point in the sequence
    // 5.5. If the spline fails (an error>qtreshold), find the point with the biggest error, set splitpoint = (fitting point + errorpoint)/2
    // 5.6. Split sequence and recursively apply 5.2. - 5.7. to startpoint-splitpoint and splitpoint-endpoint sequences
    // 5.7. TODO? If splitpoint-endpoint is a spline, try to add new points from the next sequence

    // This returns an SVG Path segment as a double[7] where
    // segment[0] ==1.0 linear  ==2.0 quadratic interpolation
    // segment[1] , segment[2] : x1 , y1
    // segment[3] , segment[4] : x2 , y2 ; middle point of Q curve, endpoint of L line
    // segment[5] , segment[6] : x3 , y3 for Q curve, should be 0.0 , 0.0 for L line
    //
    // path type is discarded, no check for path.size < 3 , which should not happen

    private ArrayList<double[]> tracepath(ArrayList<double[]> path, float ltreshold, float qtreshold){
        int pcnt=0;
        int seqend=0;
        double segtype1;
        double segtype2;

        final ArrayList<double[]> smp = new ArrayList<double[]>();
        //double [] thissegment;
        int pathlength = path.size();

        while (pcnt<pathlength){
            // 5.1. Find sequences of points with only 2 segment types
            segtype1 = path.get(pcnt)[2];
            segtype2 = -1;
            seqend=pcnt+1;

            while (((path.get(seqend)[2]==segtype1) || (path.get(seqend)[2]==segtype2) || (segtype2==-1)) && (seqend<pathlength-1)){
                if ((path.get(seqend)[2]!=segtype1) && (segtype2==-1)){
                    segtype2 = path.get(seqend)[2];
                }
                seqend++;
            }
            if (seqend==pathlength-1){
                seqend = 0;
            }

            // 5.2. - 5.6. Split sequence and recursively apply 5.2. - 5.6. to startpoint-splitpoint and splitpoint-endpoint sequences
            smp.addAll(PathSmoothing.fitseq(path,ltreshold,qtreshold,pcnt,seqend));
            // 5.7. TODO? If splitpoint-endpoint is a spline, try to add new points from the next sequence

            // forward pcnt;
            if (seqend>0){
                pcnt = seqend;
            } else {
                pcnt = pathlength;
            }
        }

        return smp;
    }


    // 5. Batch tracing paths
    private ArrayList<ArrayList<double[]>> batchtracepaths(ArrayList<ArrayList<double[]>> internodepaths, float ltres,float qtres){
        ArrayList<ArrayList<double[]>> btracedpaths = new ArrayList<ArrayList<double[]>>();
        for (int k=0;k<internodepaths.size();k++){
            btracedpaths.add(tracepath(internodepaths.get(k),ltres,qtres) );
        }
        return btracedpaths;
    }

    // 5. Batch tracing layers
    private ArrayList<ArrayList<ArrayList<double[]>>> batchtracelayers(ArrayList<ArrayList<ArrayList<double[]>>> binternodes, float ltres, float qtres){
        ArrayList<ArrayList<ArrayList<double[]>>> btbis = new ArrayList<ArrayList<ArrayList<double[]>>>();
        for (int k=0; k<binternodes.size(); k++){
            btbis.add( batchtracepaths( binternodes.get(k),ltres,qtres) );
        }
        return btbis;
    }

    ////////////////////////////////////////////////////////////
    //
    //  SVG Drawing functions
    //
    ////////////////////////////////////////////////////////////

    // Getting SVG path element string from a traced path
    private void svgpathstring(CharBuffer sb, ArrayList<double[]> segments, String fillcolor, float lcpr, float qcpr){
        // Path
        sb.append("<path fill=\"").append(fillcolor).append("\" stroke=\"").append(fillcolor).append("\" stroke-width=\"").append(1).append("\" d=\"" ).append(
                "M").append(segments.get(0)[1]).append(" ").append(segments.get(0)[2]).append(" ");

        for (int pcnt=0;pcnt<segments.size();pcnt++){
            if (segments.get(pcnt)[0]==1.0){
                sb.append("L ").append(segments.get(pcnt)[3]).append(" ").append(segments.get(pcnt)[4]).append(" ");
            } else {
                sb.append("Q ").append(segments.get(pcnt)[3]).append(" ").append(segments.get(pcnt)[4]).append(" ").append(segments.get(pcnt)[5]).append(" ").append(segments.get(pcnt)[6]).append(" ");
            }
        }
        sb.append("Z \" />");

        // Rendering control points
        if ((lcpr>0)&&(qcpr>0)){
            for (int pcnt=0;pcnt<segments.size();pcnt++){
                if (segments.get(pcnt)[0]==2.0){
                    sb.append( "<circle cx=\"").append(segments.get(pcnt)[3]).append("\" cy=\"").append(segments.get(pcnt)[4]).append("\" r=\"").append(qcpr).append("\" fill=\"cyan\" stroke-width=\"").append(qcpr*0.2).append("\" stroke=\"black\" />");
                    sb.append( "<circle cx=\"").append(segments.get(pcnt)[5]).append("\" cy=\"").append(segments.get(pcnt)[6]).append("\" r=\"").append(qcpr).append("\" fill=\"white\" stroke-width=\"").append(qcpr*0.2).append("\" stroke=\"black\" />");
                    sb.append( "<line x1=\"").append(segments.get(pcnt)[1]).append("\" y1=\"").append(segments.get(pcnt)[2]).append("\" x2=\"").append(segments.get(pcnt)[3]).append("\" y2=\"").append(segments.get(pcnt)[4]).append("\" stroke-width=\"").append(qcpr*0.2).append("\" stroke=\"cyan\" />");
                    sb.append( "<line x1=\"").append(segments.get(pcnt)[3]).append("\" y1=\"").append(segments.get(pcnt)[4]).append("\" x2=\"").append(segments.get(pcnt)[5]).append("\" y2=\"").append(segments.get(pcnt)[6]).append("\" stroke-width=\"").append(qcpr*0.2).append("\" stroke=\"cyan\" />");
                } else {
                    sb.append( "<circle cx=\"").append(segments.get(pcnt)[3]).append("\" cy=\"").append(segments.get(pcnt)[4]).append("\" r=\"").append(lcpr).append("\" fill=\"white\" stroke-width=\"").append(lcpr*0.2).append("\" stroke=\"black\" />");
                }
            }
        } else if (lcpr>0){
            for (int pcnt=0;pcnt<segments.size();pcnt++){
                if (segments.get(pcnt)[0]==1.0){
                    sb.append( "<circle cx=\"").append(segments.get(pcnt)[3]).append("\" cy=\"").append(segments.get(pcnt)[4]).append("\" r=\"").append(lcpr).append("\" fill=\"white\" stroke-width=\"").append(lcpr*0.2).append("\" stroke=\"black\" />");
                }
            }
        } else if (qcpr>0){
            for (int pcnt=0;pcnt<segments.size();pcnt++){
                if (segments.get(pcnt)[0]==2.0){
                    sb.append( "<circle cx=\"").append(segments.get(pcnt)[3]).append("\" cy=\"").append(segments.get(pcnt)[4]).append("\" r=\"").append(qcpr).append("\" fill=\"cyan\" stroke-width=\"").append(qcpr*0.2).append("\" stroke=\"black\" />");
                    sb.append( "<circle cx=\"").append(segments.get(pcnt)[5]).append("\" cy=\"").append(segments.get(pcnt)[6]).append("\" r=\"").append(qcpr).append("\" fill=\"white\" stroke-width=\"").append(qcpr*0.2).append("\" stroke=\"black\" />");
                    sb.append( "<line x1=\"").append(segments.get(pcnt)[1]).append("\" y1=\"").append(segments.get(pcnt)[2]).append("\" x2=\"").append(segments.get(pcnt)[3]).append("\" y2=\"").append(segments.get(pcnt)[4]).append("\" stroke-width=\"").append(qcpr*0.2).append("\" stroke=\"cyan\" />");
                    sb.append( "<line x1=\"").append(segments.get(pcnt)[3]).append("\" y1=\"").append(segments.get(pcnt)[4]).append("\" x2=\"").append(segments.get(pcnt)[5]).append("\" y2=\"").append(segments.get(pcnt)[6]).append("\" stroke-width=\"").append(qcpr*0.2).append("\" stroke=\"cyan\" />");
                }
            }
        }

    }


    // Converting tracedata to an SVG string, paths are drawn according to a Z-index
    // the optional lcpr and qcpr are linear and quadratic control point radiuses
    private String getsvgstring(double w, double h, IndexedImage ii){
        final float lcpr=(Float) inputParameters.getPropertyValue(INPUT_LCPR.getId());
        final float qcpr=(Float) inputParameters.getPropertyValue(INPUT_QCPR.getId());
        // SVG start
        final CharBuffer svgstr = new CharBuffer();
        svgstr.append("<svg width=\""+w+"px\" height=\""+h+"px\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" desc=\"Created with ImageTracer.java\" >");

        // Background
        svgstr.append("<rect x=\"0\" y=\"0\" width=\""+w+"px\" height=\""+h+"px\" fill=\""+torgbstr(ii.palette[ii.background])+"\" />");

        // creating Z-index
        TreeMap<Double,int[]> zindex = new TreeMap<Double,int[]>();
        double label;
        // Layer loop
        for (int k=0; k<ii.layers.size(); k++) {

            // Path loop
            for (int pcnt=0; pcnt<ii.layers.get(k).size(); pcnt++){

                // Label (Z-index key) is the startpoint of the path, linearized
                label = ii.layers.get(k).get(pcnt).get(0)[2] * w + ii.layers.get(k).get(pcnt).get(0)[1];
                // Creating new list if required
                if (!zindex.containsKey(label)){
                    zindex.put(label,new int[2]);
                }
                // Adding layer and path number to list
                zindex.get(label)[0] = k;
                zindex.get(label)[1] = pcnt;
            }
        }

        // Drawing
        // Z-index loop
        for (Entry<Double, int[]> entry : zindex.entrySet()) {
            svgpathstring(svgstr,
                    ii.layers.get(entry.getValue()[0]).get(entry.getValue()[1]),
                    torgbstr(ii.palette[entry.getValue()[0]]),lcpr,qcpr);
        }

        svgstr.append("</svg>");
        return svgstr.toString();
    }

    private static String torgbstr(byte[] c){
        return "rgb("+(c[0]+128)+","+(c[1]+128)+","+(c[2]+128)+")";
    }

}
