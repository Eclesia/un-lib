
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import static science.unlicense.image.impl.colorspace.CieXYZ.RGBtoXYZTemp;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 * RGB colorspace defined by CIE in 1931, different from sRGB.
 *
 * Resource:
 * https://fr.wikipedia.org/wiki/CIE_RGB
 *
 * @author Johann Sorel
 */
public class CieRGB extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("CIE-RGB");
    public static final ColorSpace INSTANCE = new CieRGB();

    private final Transform rgbToSpace = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            double r = source[srcOff  ];
            double g = source[srcOff+1];
            double b = source[srcOff+2];
            r = RGBtoXYZTemp(r);
            g = RGBtoXYZTemp(g);
            b = RGBtoXYZTemp(b);
            dest[dstOff  ] = 0.4360747*r + 0.3850649 *g + 0.1430804*b;
            dest[dstOff+1] = 0.2225045*r + 0.7168786 *g + 0.0606169*b;
            dest[dstOff+2] = 0.0139322*r + 0.0971045 *g + 0.7141733*b;
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private CieRGB() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("R"), null, Primitive.FLOAT32, 0, 1),
            new ColorSpaceComponent(new Chars("G"), null, Primitive.FLOAT32, 0, 1),
            new ColorSpaceComponent(new Chars("B"), null, Primitive.FLOAT32, 0, 1)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }

}
