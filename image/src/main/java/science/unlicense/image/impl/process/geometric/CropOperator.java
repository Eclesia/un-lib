

package science.unlicense.image.impl.process.geometric;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.tuple.BoxCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.task.api.AbstractTask;

/**
 *
 * @author Johann Sorel
 */
public class CropOperator extends AbstractTask {

    public static final FieldType INPUT_BBOX = new FieldTypeBuilder(new Chars("BBox")).title(new Chars("Input bbox to crop")).valueClass(BBox.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("crop"), new Chars("crop"), Chars.EMPTY,CropOperator.class,
                new FieldType[]{INPUT_IMAGE, INPUT_BBOX},
                new FieldType[]{OUTPUT_IMAGE});

    public CropOperator() {
        super(DESCRIPTOR);
    }

    public Document perform() {
        final Image inputImage = (Image) inputParameters
                .getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final BBox bbox = (BBox) inputParameters
                .getPropertyValue(INPUT_BBOX.getId());

        final Image outputImage = execute(inputImage, bbox);

        outputParameters.setPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),outputImage);
        return outputParameters;
    }

    public static Image execute(Image inputImage, BBox bbox){

        final Extent ext = bbox.getExtent();

        final TupleGrid inputTuples = inputImage.getRawModel().asTupleBuffer(inputImage);
        final Image outputImage = Images.create(inputImage, ext);
        final TupleGrid outTuples = outputImage.getRawModel().asTupleBuffer(outputImage);

        TupleGridCursor cursor = new BoxCursor(inputTuples.cursor(),bbox);
        while (cursor.next() ){
            outTuples.setTuple(cursor.coordinate(), cursor.samples());
        }

        return outputImage;
    }

}
