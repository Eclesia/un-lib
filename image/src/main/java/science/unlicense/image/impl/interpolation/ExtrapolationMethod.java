
package science.unlicense.image.impl.interpolation;

/**
 *
 * @author Johann Sorel
 */
public interface ExtrapolationMethod {


    /**
     *
     * @param interpolation
     * @param x
     * @param y
     * @param band
     * @return
     */
    double extrapolate(Interpolation2D interpolation, double x, double y, int band);

}
