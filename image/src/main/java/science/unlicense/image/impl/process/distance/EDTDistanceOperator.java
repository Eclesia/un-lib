
package science.unlicense.image.impl.process.distance;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.number.Float64;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterpolatedColorModel;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.task.api.AbstractTask;

/**
 * 8SSEDT implementation of distance map.
 *
 * This implementation produce an image with distances positive outside the mask
 * and negative distances inside the mask.
 *
 * resources :
 * http://www.lems.brown.edu/vision/people/leymarie/Refs/CompVision/DT/DTpaper.pdf
 * http://www.ee.bgu.ac.il/~dinstein/stip2002/LeymarieLevineDistTrans_cvgip92.pdf
 * http://www.codersnotes.com/notes/signed-distance-fields/
 *
 * @author Johann Sorel
 */
public class EDTDistanceOperator extends AbstractTask{

    public static DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("edtdistance"), new Chars("edtdistance"), Chars.EMPTY, EDTDistanceOperator.class,
                new FieldType[]{INPUT_IMAGE},
                new FieldType[]{OUTPUT_IMAGE});

    //variable used for far pixel, used for uncalculated pixels.
    private final Cell far = new Cell();

    public EDTDistanceOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image) {
        inputParameters.setPropertyValue(INPUT_IMAGE.getId(),image);
        return (Image) perform().getPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }

    @Override
    public Document perform() {
        final Image img = (Image) inputParameters.getPropertyValue(INPUT_IMAGE.getId());

        final Extent.Long extent = img.getExtent();

        final int height = (int) extent.getL(1);
        final int width = (int) extent.getL(0);
        //a far coordinate, ensured to always be farther then any in image pixel
        far.x = height*2 + width*2;
        far.y = far.x;
        far.d2 = distSqrt(far.x, far.y);

        //this grid contains 0 for pixels inside geometry
        final Cell[][] outterDistance = new Cell[height][width];
        //this grid contains 0 for pixels outside geometry
        final Cell[][] innerDistance = new Cell[height][width];

        //fill grids with 0 or far if pixel is inside or outside
        final TupleGridCursor cursor = img.getRawModel().asTupleBuffer(img).cursor();
        boolean[] tuple = new boolean[1];
        Tuple coord = cursor.coordinate();
        final int coordX = (int) coord.get(0);
        final int coordY = (int) coord.get(1);
        while (cursor.next()) {
            cursor.samples().toBoolean(tuple, 0);
            outterDistance[coordY][coordX] = new Cell();
            innerDistance[coordY][coordX] = new Cell();
            if (tuple[0]){
                outterDistance[coordY][coordX].x = 0;
                outterDistance[coordY][coordX].y = 0;
                innerDistance[coordY][coordX].x = far.x;
                innerDistance[coordY][coordX].y = far.y;
                innerDistance[coordY][coordX].d2 = far.d2;
            } else {
                outterDistance[coordY][coordX].x = far.x;
                outterDistance[coordY][coordX].y = far.y;
                outterDistance[coordY][coordX].d2 = far.d2;
                innerDistance[coordY][coordX].x = 0;
                innerDistance[coordY][coordX].y = 0;
            }
        }

        // Calculate distances
        computeDistances(outterDistance);
        computeDistances(innerDistance);

        //Create distance result image
        final Image result = Images.createCustomBand(extent, 1, Float64.TYPE);
        final TupleGrid tb = result.getRawModel().asTupleBuffer(result);
        final TupleRW sample = tb.createTuple();
        final Vector2i32 crd = new Vector2i32();
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        double distIn, distOut;
        Cell c1,c2;
        for (crd.y=0;crd.y<height;crd.y++){
            for (crd.x=0;crd.x<width;crd.x++){
                c1 = outterDistance[crd.y][crd.x];
                c2 = innerDistance[crd.y][crd.x];
                distOut = Math.sqrt(distSqrt(c1.x,c1.y));
                distIn = -Math.sqrt(distSqrt(c2.x,c2.y));
                min = Maths.min(min, distOut);
                min = Maths.min(min, distIn);
                max = Maths.max(max, distOut);
                max = Maths.max(max, distIn);
                sample.set(0, distIn<0 ? distIn : distOut);
                tb.setTuple(crd, sample);
            }
        }

        //create a grayscale palette between min and max
        final ImageModel cm = new InterpolatedColorModel(img.getRawModel(),
                new Color[]{Color.BLACK,Color.WHITE}, new float[]{(float) min,(float) max});
        result.getModels().add(ImageModel.MODEL_COLOR, cm);

        outputParameters.setPropertyValue(OUTPUT_IMAGE.getId(),result);
        return outputParameters;
    }

    private Cell get(Cell[][] g, int x, int y ){
        if (x>=0 && y>=0 && x<g[0].length && y<g.length ){
            return g[y][x];
        } else {
            return far;
        }
    }

    private void compare(Cell[][] g, int x, int y, int offsetx, int offsety){
        final Cell p = g[y][x];
        final Cell other = get(g, x+offsetx, y+offsety);
        int d2 = distSqrt(other.x+offsetx, other.y+offsety);
        if (d2 < p.d2){
            p.x = other.x+offsetx;
            p.y = other.y+offsety;
            p.d2 = d2;
        }
    }

    private void computeDistances(Cell[][] g){
        final int height = g.length;
        final int width = g[0].length;

        // Pass 0
        for (int y=0;y<height;y++){
            for (int x=0;x<width;x++){
                compare(g, x, y, -1,  0);
                compare(g, x, y,  0, -1);
                compare(g, x, y, -1, -1);
                compare(g, x, y,  1, -1);
            }
            for (int x=width-1;x>=0;x--){
                compare(g, x, y, 1, 0);
            }
        }

        // Pass 1
        for (int y=height-1;y>=0;y--){
            for (int x=width-1;x>=0;x--){
                compare(g, x, y,  1,  0);
                compare(g, x, y,  0,  1);
                compare(g, x, y, -1,  1);
                compare(g, x, y,  1,  1);
            }
            for (int x=0;x<width;x++){
                compare(g, x, y, -1, 0);
            }
        }
    }

    private static int distSqrt(int x, int y){
        return x*x + y*y;
    }

    private static class Cell{
        private int x,y;
        private int d2;
    }

}
