
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.transform.IdentityTransform;
import science.unlicense.math.api.transform.Transform;

/**
 *
 * @author Johann Sorel
 */
public class SRGB extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("sRGB");
    public static final ColorSpace INSTANCE = new SRGB();

    private static final Transform IDENTITY = new IdentityTransform(3);

    private SRGB() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("r"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("g"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("b"), null, Primitive.FLOAT32, 0, +1)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (INSTANCE.equals(cs)) return IDENTITY;
        return super.getTranform(cs);
    }

}
