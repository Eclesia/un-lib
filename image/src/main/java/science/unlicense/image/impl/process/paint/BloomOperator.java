
package science.unlicense.image.impl.process.paint;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.process.AbstractAreaOperator;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.*;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.math.api.Maths;

/**
 * Bloom operator.
 *
 * Acts as a blur for bright fragments.
 *
 * @author Johann Sorel
 */
public class BloomOperator extends AbstractAreaOperator {

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("bloom"), new Chars("Bloom"), Chars.EMPTY, BloomOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_EXTRAPOLATOR},
                new FieldType[]{OUTPUT_IMAGE});

    private final double bloomFactor = 0.2;
    private int originx;
    private int originy;

    public BloomOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image, Chars extrapolatorType){
        inputParameters.setPropertyValue(INPUT_IMAGE.getId(),image);
        inputParameters.setPropertyValue(INPUT_EXTRAPOLATOR.getId(),extrapolatorType);
        return(Image) perform().getPropertyValue(OUTPUT_IMAGE.getId());
    }

    @Override
    public Document perform() {
        workingModel = ImageModel.MODEL_COLOR;
        return super.perform();
    }

    @Override
    protected void prepareInputs() {
        originx = 4;
        originy = 4;
    }

    @Override
    protected int getTopPadding() {
        return 4;
    }

    @Override
    protected int getBottomPadding() {
        return 4;
    }

    @Override
    protected int getLeftPadding() {
        return 4;
    }

    @Override
    protected int getRightPadding() {
        return 4;
    }

    @Override
    protected void evaluate(double[][][] workspace, double[] result) {

        //calculate the average color of near pixels
        //TODO use a gaussian matrix ? allow custom size ?
        final double[] avg = new double[3];
        int x;
        int y;
        for (y=-4;y<4;y++){
            for (x=-4;x<4;x++){
                avg[0] += workspace[originx+x][originy+y][0];
                avg[1] += workspace[originx+x][originy+y][1];
                avg[2] += workspace[originx+x][originy+y][2];
            }
        }
        avg[0] *= bloomFactor;
        avg[1] *= bloomFactor;
        avg[2] *= bloomFactor;

        //calculate current fragment lightness
        //we use the same function as is RGB to HSL
        double[] tex = workspace[originx][originy];
        double lightness = ( Maths.max(Maths.max(tex[0],tex[1]),tex[2]) + Maths.min(Maths.min(tex[0],tex[1]),tex[2]) ) *0.5;

        //arbitrary linear interpolation to calculate factor
        double scale = 0.005 + (1.0-lightness)*0.006;

        avg[0] = avg[0] * avg[0] * scale + tex[0];
        avg[1] = avg[1] * avg[1] * scale + tex[1];
        avg[2] = avg[2] * avg[2] * scale + tex[2];

        result[0] = Maths.clamp(avg[0], 0, 1);
        result[1] = Maths.clamp(avg[1], 0, 1);
        result[2] = Maths.clamp(avg[2], 0, 1);
        result[3] = 1.0;

    }

}
