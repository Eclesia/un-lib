
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 *
 * @author Johann Sorel
 */
public class YCbCr extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("YCbCr");
    public static final ColorSpace INSTANCE = new YCbCr();

    private final Transform rgbToSpace = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            double r = source[srcOff];
            double g = source[srcOff+1];
            double b = source[srcOff+2];
            dest[dstOff  ] = Maths.clamp(  0 + (int) (0.299*r)    + (int) (0.587*g)    + (int) (0.114*b),    0f, 1f);
            dest[dstOff+1] = Maths.clamp(128 - (int) (0.168736*r) - (int) (0.331264*g) + (int) (0.5*b),      0f, 1f);
            dest[dstOff+2] = Maths.clamp(128 + (int) (0.5*r)      - (int) (0.418688*g) - (int) (0.081312*b), 0f, 1f);
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            double r = source[srcOff];
            double g = source[srcOff+1];
            double b = source[srcOff+2];
            dest[dstOff  ] = Maths.clamp(  0 + (int) (0.299*r)    + (int) (0.587*g)    + (int) (0.114*b),    0f, 1f);
            dest[dstOff+1] = Maths.clamp(128 - (int) (0.168736*r) - (int) (0.331264*g) + (int) (0.5*b),      0f, 1f);
            dest[dstOff+2] = Maths.clamp(128 + (int) (0.5*r)      - (int) (0.418688*g) - (int) (0.081312*b), 0f, 1f);
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            double y  = source[srcOff  ];
            double cb = source[srcOff+1];
            double cr = source[srcOff+2];
            dest[dstOff  ] = Maths.clamp(y + (1.402  *(cr-0.5)), 0.0, 1.0);
            dest[dstOff+1] = Maths.clamp(y - (0.34414*(cb-0.5)) - (0.71414*(cr-0.5)), 0.0, 1.0);
            dest[dstOff+2] = Maths.clamp(y + (1.772  *(cb-0.5)), 0.0, 1.0);
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            float y  = source[srcOff  ];
            float cb = source[srcOff+1];
            float cr = source[srcOff+2];
            dest[dstOff  ] = Maths.clamp(y + (1.402f  *(cr-0.5f)), 0f, 1f);
            dest[dstOff+1] = Maths.clamp(y - (0.34414f*(cb-0.5f)) - (0.71414f*(cr-0.5f)), 0f, 1f);
            dest[dstOff+2] = Maths.clamp(y + (1.772f  *(cb-0.5f)), 0f, 1f);
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private YCbCr() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("Y"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("Cb"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("Cr"), null, Primitive.FLOAT32, 0, +1)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }

}
