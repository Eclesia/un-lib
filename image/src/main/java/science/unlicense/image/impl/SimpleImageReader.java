

package science.unlicense.image.impl;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGESET;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION_ID;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.image.api.model.PlanarModel;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class SimpleImageReader extends AbstractImageReader {

    private final NumberType sampleType;
    private final Endianness encoding;
    private final int[] dimensionSize;
    private final int nbSample;
    private final boolean interlaced;

    private DefaultTypedNode mdImage;


    public SimpleImageReader(NumberType sampleType, Endianness encoding, int nbSample, int[] dimensionSize, boolean interlaced) {
        this.sampleType = sampleType;
        this.encoding = encoding;
        this.nbSample = nbSample;
        this.dimensionSize = dimensionSize;
        this.interlaced = interlaced;
    }


    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        mdImage = new DefaultTypedNode(MD_IMAGE.getType());
        for (int i=0;i<dimensionSize.length;i++){
            final TypedNode tn = new DefaultTypedNode(MD_IMAGE_DIMENSION.getType(),new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID.getType(),"d"+i),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND.getType(),dimensionSize[i])});
            mdImage.getChildren().add(tn);
        }

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        readMetadatas(stream);

        if (dimensionSize.length!=2){
            throw new IOException("dimension size different from 2 not supported yet.");
        }

        final DataInputStream ds = new DataInputStream(stream,encoding);

        final int bitPerSample = sampleType.getSizeInBits();
        if (bitPerSample<8){
            throw new IOException("Bit per sample under 8 not supported yet.");
        }

        final Buffer bank = parameters.getBufferFactory().createInt8((bitPerSample/8)*nbSample*dimensionSize[0]*dimensionSize[1]).getBuffer();
        ds.readFully(bank);
        final ImageModel sm;
        if (interlaced){
            sm = new InterleavedModel(new UndefinedSystem(nbSample), sampleType);
        } else {
            sm = new PlanarModel(new UndefinedSystem(nbSample), sampleType);
        }

        final ImageModel cm = DerivateModel.create(sm, new int[]{0,0,0}, null,null, ColorSystem.RGB_8BITS);

        return new DefaultImage(bank, new Extent.Long(dimensionSize[0], dimensionSize[1]), sm, cm);
    }

}
