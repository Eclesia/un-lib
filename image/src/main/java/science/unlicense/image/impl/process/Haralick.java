package science.unlicense.image.impl.process;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 * Calculate Haralick matrix values.
 * More details at :
 * http://www.developpez.net/forums/d416724/autres-langages/algorithmes/contribuez/image-haralick-texture-features
 *
 * @author Xavier Philippeau
 * @author Johann Sorel (UN project adaptation)
 */
public class Haralick extends CObject{

    // spatial density matrix (aka GLCM)
    private int MSIZE = 256;
    private double[][] matrix = new double[MSIZE][MSIZE];
    // image source
    private Image image = null;
    // precalculs
    private double[] px_y = new double[MSIZE];  // Px-y(i)
    private double[] pxy = new double[2 * MSIZE]; // Px+y(i)
    // valeurs specifiques de la texture
    public double[] features = new double[15];

    public Haralick(Image img) {
        this.image = img;

        int n = 4;
        int[] dx = new int[]{1, 1, 0, -1};
        int[] dy = new int[]{0, 1, 1, 1};

        for (int k = 0; k < n; k++) {
            computeMatrix(dx[k], dy[k]);

            precomputation();

            this.features[1] += getF1();
            this.features[2] += getF2();
            this.features[3] += getF3();
            this.features[4] += getF4();
            this.features[5] += getF5();
            this.features[6] += getF6();
            this.features[7] += getF7(this.features[6]);
            this.features[8] += getF8();
            this.features[9] += getF9();
            this.features[10] += getF10();
            this.features[11] += getF11();
        }

        for (int f = 1; f < this.features.length; f++) {
            this.features[f] /= n;
        }
    }

    private void precomputation() {
        // Px-y(i)
        Arrays.fill(px_y, 0);
        for (int j = 0; j < MSIZE; j++) {
            for (int i = 0; i < MSIZE; i++) {
                px_y[Math.abs(i - j)] += matrix[i][j];
            }
        }

        // Px+y(i)
        Arrays.fill(pxy, 0);
        for (int j = 0; j < MSIZE; j++) {
            for (int i = 0; i < MSIZE; i++) {
                pxy[i + j] += matrix[i][j];
            }
        }
    }

    public Chars toChars() {
        CharBuffer cb = new CharBuffer();
        for (int j = 0; j < MSIZE; j++) {
            for (int i = 0; i < MSIZE; i++) {
                cb.append(matrix[i][j] + " ");
            }
            cb.append("\n");
        }
        return cb.toChars();
    }

    private void computeMatrix(int dx, int dy) {
        // raz matrice
        for (int i = 0; i < MSIZE; i++) {
            Arrays.fill(matrix[i], 0);
        }

        // calcul des co-occurences
        int sum = 0;
        int height = (int) image.getExtent().getL(1);
        int width = (int) image.getExtent().getL(0);
        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final TupleRW pixel = sm.createTuple();
        final Vector2i32 coords = new Vector2i32();

        for (int y0 = 0; y0 < height; y0++) {
            for (int x0 = 0; x0 < width; x0++) {
                coords.x = x0; coords.y = y0;
                // pour chaque pixel
                sm.getTuple(coords, pixel);
                int v0 = (int) (MSIZE * pixel.get(0) / 256.0);

                // on cherche le voisin
                int x1 = x0 + dx;
                if (x1 < 0 || x1 >= width) {
                    continue;
                }
                int y1 = y0 + dy;
                if (y1 < 0 || y1 >= height) {
                    continue;
                }
                coords.x = x1; coords.y = y1;
                int v1 = (int) (MSIZE * pixel.get(0) / 256.0);

                // on incremente la matrice
                matrix[v0][v1]++;
                matrix[v1][v0]++;
                sum += 2;
            }
        }

        if (sum == 0) {
            throw new InvalidArgumentException("Provided parameters resulted in a zero division factor.");
        }

        // normalisation
        for (int j = 0; j < MSIZE; j++) {
            for (int i = 0; i < MSIZE; i++) {
                matrix[i][j] /= sum;
            }
        }
    }

    // F1 - Second moment angulaire (homogeneite)
    private double getF1() {
        double h = 0;
        for (int j = 0; j < MSIZE; j++) {
            for (int i = 0; i < MSIZE; i++) {
                h += matrix[i][j] * matrix[i][j];
            }
        }
        return h;
    }

    // F2 - contraste
    private double getF2() {
        double contrast = 0;
        for (int j = 0; j < MSIZE; j++) {
            for (int i = 0; i < MSIZE; i++) {
                contrast += (i - j) * (i - j) * matrix[i][j];
            }
        }
        return contrast;
    }

    // F3 - correlation
    private double getF3() {

        // optimisation car matrice symetrique
        // ==> distribution marginale sur X = distribution marginale sur Y

        // moyenne = somme { p(i,j) * i }
        double mean = 0;
        for (int i = 0; i < MSIZE; i++) {
            for (int j = 0; j < MSIZE; j++) {
                mean += i * matrix[i][j];
            }
        }

        // variance = somme { p(i,j) * (i-moyenne)^2 }
        double var = 0;
        for (int i = 0; i < MSIZE; i++) {
            for (int j = 0; j < MSIZE; j++) {
                var += matrix[i][j] * (i - mean) * (i - mean);
            }
        }
        if (var <= 0) {
            return 1;
        }

        // correlation = somme { (i-moyenne) * (j-moyenne) * p(i,j) / variance^2 }
        double sum = 0;
        for (int j = 0; j < MSIZE; j++) {
            for (int i = 0; i < MSIZE; i++) {
                sum += matrix[i][j] * (i - mean) * (j - mean);
            }
        }
        double r = sum / (var * var);

        return r;
    }

    // F4 - Variance
    private double getF4() {
        double mean = 0;
        for (int j = 0; j < MSIZE; j++) {
            for (int i = 0; i < MSIZE; i++) {
                mean += i * matrix[i][j];
            }
        }

        double variance = 0;
        for (int j = 0; j < MSIZE; j++) {
            for (int i = 0; i < MSIZE; i++) {
                variance += (i - mean) * (i - mean) * matrix[i][j];
            }
        }
        return variance;
    }

    // F5 - Moment des différences inverses OK
    private double getF5() {
        double invdiff = 0;
        for (int j = 0; j < MSIZE; j++) {
            for (int i = 0; i < MSIZE; i++) {
                double coef = 1.0 / (1.0 + (i - j) * (i - j));
                invdiff += coef * matrix[i][j];
            }
        }
        return invdiff;
    }

    // F6 - Moyenne des sommes
    private double getF6() {
        double sumavg = 0;
        for (int k = 0; k <= 2 * (MSIZE - 1); k++) {
            sumavg += k * pxy[k];
        }
        return sumavg;
    }

    // F7 - Variance des sommes
    private double getF7(double f6) {
        double sumavg = f6;
        int sumvar = 0;
        for (int k = 0; k <= 2 * (MSIZE - 1); k++) {
            sumvar += (k - sumavg) * (k - sumavg) * pxy[k];
        }
        return sumvar;
    }

    // F8 - Entropie des sources
    private double getF8() {
        double entropysrc = 0;
        for (int k = 0; k <= 2 * (MSIZE - 1); k++) {
            if (pxy[k] == 0) {
                continue;
            }
            entropysrc += pxy[k] * Math.log(pxy[k]);
        }
        return -entropysrc;
    }

    // F9 - Entropie
    private double getF9() {
        double entropy = 0;
        for (int j = 0; j < MSIZE; j++) {
            for (int i = 0; i < MSIZE; i++) {
                if (matrix[i][j] == 0) {
                    continue;
                }
                entropy += matrix[i][j] * Math.log(matrix[i][j]);
            }
        }
        return -entropy;
    }

    // F10 - variance des differences
    private double getF10() {
        double mean = 0;
        for (int k = 0; k <= MSIZE - 1; k++) {
            mean += k * px_y[k];
        }
        double var = 0;
        for (int k = 0; k <= MSIZE - 1; k++) {
            var += (k - mean) * (k - mean) * px_y[k];
        }
        return var;
    }

    // F11 - Entropie des differences
    private double getF11() {
        double entropydiff = 0;
        for (int k = 0; k <= MSIZE - 1; k++) {
            if (px_y[k] == 0) {
                continue;
            }
            entropydiff += px_y[k] * Math.log(px_y[k]);
        }
        return -entropydiff;
    }
}
