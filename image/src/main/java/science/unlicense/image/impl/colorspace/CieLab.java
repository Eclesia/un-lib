
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 *
 * @author Johann Sorel
 */
public class CieLab extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("CIE-LAB");
    public static final ColorSpace INSTANCE = new CieLab();

    private final Transform rgbToSpace = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private CieLab() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("L*"), null, Primitive.INT32,    0, +100),
            new ColorSpaceComponent(new Chars("a*"), null, Primitive.INT32, -300, +299),
            new ColorSpaceComponent(new Chars("b*"), null, Primitive.INT32, -300, +299)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }

}
