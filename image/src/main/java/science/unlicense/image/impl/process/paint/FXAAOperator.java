
package science.unlicense.image.impl.process.paint;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.process.AbstractAreaOperator;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.*;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Vectors;

/**
 * FXAA operator.
 *
 * FXAA is a post processing antialising.
 * In a nutshell it detects edges from luminance variation and apply a local blur.
 *
 * TODO : could not find the original shader reference, blog has been cleaned up.
 * Since the work has been derivated on multiple projects, games and engine I believe it is Free.
 * Yet still have to find a way to confirm this somehow ...
 *
 * Explication resources :
 * http://www.ngohq.com/images/articles/fxaa/FXAA_WhitePaper.pdf
 * http://timothylottes.blogspot.fr
 * http://timothylottes.blogspot.com/2011/04/nvidia-fxaa-ii-for-console.html
 * http://www.geeks3d.com/20110405/fxaa-fast-approximate-anti-aliasing-demo-glsl-opengl-test-radeon-geforce
 *
 * @author Johann Sorel
 */
public class FXAAOperator extends AbstractAreaOperator {

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("fxaa"), new Chars("FXAA"), Chars.EMPTY, FXAAOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_EXTRAPOLATOR},
                new FieldType[]{OUTPUT_IMAGE});


    private final double[] luma = new double[]{0.299, 0.587, 0.114};
    private final int FXAA_SPAN_MAX   = 8;
    private final float FXAA_REDUCE_MUL = 1.0f / 8.0f;
    private final float FXAA_REDUCE_MIN = 1.0f / 128.0f;

    private int originx;
    private int originy;

    public FXAAOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image, Chars extrapolatorType) {
        inputParameters.setPropertyValue(INPUT_IMAGE.getId(),image);
        inputParameters.setPropertyValue(INPUT_EXTRAPOLATOR.getId(),extrapolatorType);
        return(Image) perform().getPropertyValue(OUTPUT_IMAGE.getId());
    }

    @Override
    public Document perform() {
        workingModel = ImageModel.MODEL_COLOR;
        return super.perform();
    }

    @Override
    protected void prepareInputs() {
        originx = 8;
        originy = 8;
    }

    @Override
    protected int getTopPadding() {
        return FXAA_SPAN_MAX;
    }

    @Override
    protected int getBottomPadding() {
        return FXAA_SPAN_MAX;
    }

    @Override
    protected int getLeftPadding() {
        return FXAA_SPAN_MAX;
    }

    @Override
    protected int getRightPadding() {
        return FXAA_SPAN_MAX;
    }

    @Override
    protected void evaluate(double[][][] workspace, double[] result) {

        double[] rgbNW = workspace[originx-1][originy-1];
        double[] rgbNE = workspace[originx+1][originy-1];
        double[] rgbM  = workspace[originx  ][originy  ];
        double[] rgbSW = workspace[originx-1][originy+1];
        double[] rgbSE = workspace[originx+1][originy+1];

        double lumaNW = Vectors.dot(luma, rgbNW);
        double lumaNE = Vectors.dot(luma, rgbNE);
        double lumaM  = Vectors.dot(luma, rgbM );
        double lumaSW = Vectors.dot(luma, rgbSW);
        double lumaSE = Vectors.dot(luma, rgbSE);

        double lumaMin = Maths.min(lumaM, Maths.min(Maths.min(lumaNW, lumaNE), Maths.min(lumaSW, lumaSE)));
        double lumaMax = Maths.max(lumaM, Maths.max(Maths.max(lumaNW, lumaNE), Maths.max(lumaSW, lumaSE)));

        double[] dir = new double[2];
        dir[0] = -((lumaNW + lumaNE) - (lumaSW + lumaSE));
        dir[1] =  ((lumaNW + lumaSW) - (lumaNE + lumaSE));

        double dirReduce = Maths.max((lumaNW + lumaNE + lumaSW + lumaSE) * (0.25 * FXAA_REDUCE_MUL), FXAA_REDUCE_MIN);

        double rcpDirMin = 1.0 / (Maths.min(Math.abs(dir[0]), Math.abs(dir[1])) + dirReduce);

        dir[0] *= rcpDirMin;
        dir[1] *= rcpDirMin;
        dir[0] = Maths.clamp(dir[0],-FXAA_SPAN_MAX,FXAA_SPAN_MAX);
        dir[1] = Maths.clamp(dir[1],-FXAA_SPAN_MAX,FXAA_SPAN_MAX);

        double dx0 = dir[0] * (0.0/3.0);
        double dx1 = dir[0] * (1.0/3.0);
        double dx2 = dir[0] * (2.0/3.0);
        double dx3 = dir[0] * (3.0/3.0);
        double dy0 = dir[1] * (0.0/3.0);
        double dy1 = dir[1] * (1.0/3.0);
        double dy2 = dir[1] * (2.0/3.0);
        double dy3 = dir[1] * (3.0/3.0);

        double[] rgbA = new double[3];
        double[] rgbB = new double[3];
        for (int i=0;i<3;i++) {
            rgbA[i] = 0.5 * (
                    workspace[originx + (int) dx1][originy + (int) dy1][i] +
                    workspace[originx + (int) dx2][originy + (int) dy2][i]);
            rgbB[i] = rgbA[i] * 0.5 + 0.25 * (
                    workspace[originx + (int) dx0][originy + (int) dy0][i] +
                    workspace[originx + (int) dx3][originy + (int) dy3][i]);
        }

        double lumaB = Vectors.dot(rgbB, luma);

        if ((lumaB < lumaMin) || (lumaB > lumaMax)) {
            result[0] = rgbA[0];
            result[1] = rgbA[1];
            result[2] = rgbA[2];
        } else {
            result[0] = rgbB[0];
            result[1] = rgbB[1];
            result[2] = rgbB[2];
        }
        result[3] = 1;

    }

}
