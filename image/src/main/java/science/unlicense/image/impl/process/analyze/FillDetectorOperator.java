
package science.unlicense.image.impl.process.analyze;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.model.ImageModel;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.math.impl.util.LinearTransformDetector;
import science.unlicense.task.api.AbstractTask;

/**
 * This operator try to detect a possible paint in an area of an image.
 *
 * Aiming to detect :
 * - plain value fill
 * - TODO gradient values fill
 * - TODO radial values fill
 * - TODO pattern repetition fill
 *
 * @author Johann Sorel
 */
public class FillDetectorOperator extends AbstractTask {

    public static final FieldType INPUT_MASK = new FieldTypeBuilder(new Chars("Mask")).valueClass(Image.class).build();
    public static final FieldType INPUT_MODEL = new FieldTypeBuilder(new Chars("Model")).valueClass(Chars.class).defaultValue(ImageModel.MODEL_RAW).build();
    public static final FieldType OUTPUT_FILL = new FieldTypeBuilder(new Chars("Fill")).valueClass(Object.class).build();
    public static final FieldType OUTPUT_PROBABILITY = new FieldTypeBuilder(new Chars("Probability")).valueClass(Double.class).defaultValue(0.0).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("contour"), new Chars("Contour"), Chars.EMPTY,ContourOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_MASK,INPUT_MODEL},
                new FieldType[]{OUTPUT_FILL,OUTPUT_PROBABILITY});

    public FillDetectorOperator() {
        super(DESCRIPTOR);
    }

    public Document perform() {
        final Image inputImage = (Image) inputParameters.getPropertyValue(INPUT_IMAGE.getId());
        final Image inputMask = (Image) inputParameters.getPropertyValue(INPUT_MASK.getId());
        final Chars model = (Chars) inputParameters.getPropertyValue(INPUT_MODEL.getId());

        final ImageModel imgModel = (ImageModel) inputImage.getModels().getValue(model);
        final TupleGrid tupleBuffer = imgModel.asTupleBuffer(inputImage);
        final TupleGridCursor cursor = tupleBuffer.cursor();
        final int width = (int) tupleBuffer.getExtent().get(0);
        final int height = (int) tupleBuffer.getExtent().get(1);
        final int nbSample = tupleBuffer.getSampleSystem().getNumComponents();

        //extract each line gradient, [line, band, ranges]
        final LinearTransformDetector dectector = new LinearTransformDetector(5.0);
        final LinearTransformDetector.Range1S[][][] ranges = new LinearTransformDetector.Range1S[height][nbSample][0];
        final LinearTransformDetector.RangeNS[][] rangeNs = new LinearTransformDetector.RangeNS[height][0];

        final double[][] bandes = new double[nbSample][width];
        for (int y=0;y<height;y++) {
            for (int x=0;x<width;x++) {
                cursor.next();
                for (int i=0;i<nbSample;i++) {
                    bandes[i][x] = cursor.samples().get(i);
                }
            }
            for (int i=0;i<nbSample;i++) {
                ranges[y][i] = dectector.detect(bandes[i], 0, width);
            }
            //intersect each sample ranges
            rangeNs[y] = LinearTransformDetector.combine(ranges[y]);
        }

        //rebuild geometries






//        if (inputMask!=null){
//            cursor = new MaskCursor(cursor, inputMask);
//        }
//
//        final Statistics[] stats = new Statistics[tupleBuffer.getSampleCount()];
//        int i;
//        final double[] tuple = new double[stats.length];
//        while (cursor.next()) {
//            cursor.toDouble(tuple, 0);
//            for (i=0;i<stats.length;i++){
//                stats[i].add(tuple[i]);
//            }
//        }
//
//        //average fill value
//        final double[] fill = new double[stats.length];
//        for (i=0;i<stats.length;i++){
//            fill[i] = stats[i].getMean();
//        }
//
        final Document params = new DefaultDocument(false,DESCRIPTOR.getOutputType());
//        params.setFieldValue(OUTPUT_FILL.getId(),fill);
        return params;
    }

}
