
package science.unlicense.image.impl.process.paint;

import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.geometry.api.tuple.DefaultTupleSpaceCursor;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.image.api.process.ExtrapolatorCopy;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class SSAO implements TupleSpace {

    private static final SampleSystem OCCLUSION = UndefinedSystem.create(1);

    //TODO replace by a configurable value
    private static final int KERNEL_SIZE = 32;
    private static final Vector2f64[] KERNEL_VALUES = new Vector2f64[KERNEL_SIZE];
    private static final Vector2f64[][] RANDOM_TEXTURE = new Vector2f64[64][64];

    static {
        for (int i=0;i<KERNEL_SIZE;i++){
            // generate a random vector
            KERNEL_VALUES[i] = new Vector2f64();
            KERNEL_VALUES[i].x = (Math.random() * 2.0 - 1.0); // [-1..+1]
            KERNEL_VALUES[i].y = (Math.random() * 2.0 - 1.0); // [-1..+1]
            KERNEL_VALUES[i].localNormalize();
            KERNEL_VALUES[i].localScale(Math.random());
        }

        for (int x=0;x<64;x++) {
            for (int y=0;y<64;y++) {
                RANDOM_TEXTURE[x][y] = new Vector2f64();
                RANDOM_TEXTURE[x][y].x = Math.random() * 2.0 - 1.0;
                RANDOM_TEXTURE[x][y].y = Math.random() * 2.0 - 1.0;
                RANDOM_TEXTURE[x][y].localNormalize();
            }
        }
    }

    private TupleGrid rgb;
    private TupleGrid positionsView;
    private TupleSpace extrapolatePositionsView;
    private TupleGrid normalsView;
    private int width;
    private int height;

    //work constants
    public Vector2f64[] samplesVec = new Vector2f64[128];
    public final int samplesNb = KERNEL_SIZE;
    public float samplesRadius = 1.5f;
    public float intensity = 2.0f;
    public float scale = 1.0f;
    public float bias = 0.5f;

    public SSAO() {
        for (int i=0;i<samplesVec.length;i++) {
            samplesVec[i] = new Vector2f64();
        }
    }

    public void setRgb(TupleGrid rgb) {
        this.rgb = rgb;
        width = (int) rgb.getExtent().get(0);
        height = (int) rgb.getExtent().get(1);
    }

    public void setPositionsView(TupleGrid positionsView) {
        this.positionsView = positionsView;
        this.extrapolatePositionsView = new ExtrapolatorCopy(positionsView);
    }

    public void setNormalsView(TupleGrid normalsView) {
        this.normalsView = normalsView;
    }

    @Override
    public SampleSystem getCoordinateSystem() {
        return rgb.getCoordinateSystem();
    }

    @Override
    public SampleSystem getSampleSystem() {
        return rgb.getSampleSystem();
    }

    @Override
    public ArithmeticType getNumericType() {
        return rgb.getNumericType();
    }

    @Override
    public Geometry getCoordinateGeometry() {
        return rgb.getCoordinateGeometry();
    }

    @Override
    public TupleRW createTuple() {
        return rgb.createTuple();
    }

    @Override
    public TupleRW getTuple(Tuple uv, TupleRW buffer) {
        final TupleRW co = rgb.getTuple(uv, rgb.createTuple());
        double occ = computeOcclusion(uv);
        buffer.set(0, co.get(0));
        buffer.set(1, Maths.clamp(co.get(1)-occ, 0,255));
        buffer.set(2, Maths.clamp(co.get(2)-occ, 0,255));
        buffer.set(3, Maths.clamp(co.get(3)-occ, 0,255));
        return buffer;
    }

    private double computeOcclusion(Tuple uv) {

        // current fragment position and normal
        final Vector3f64 positionView = new Vector3f64();
        final Vector3f64 normalView = new Vector3f64();
        extrapolatePositionsView.getTuple(uv, positionView);
        normalsView.getTuple(uv, normalView);
        normalView.localNormalize();
        if (!normalView.isValid()) return 0.0;

        int rx = (int) uv.get(0) % 64;
        int ry = (int) uv.get(1) % 64;
        final Vector2f64 rand = RANDOM_TEXTURE[rx][ry];

        double rad = samplesRadius / -positionView.z;
        if (Double.isNaN(rad) || Double.isInfinite(rad)) rad = 0.1;
        double occ = 0.0;

        for (int j = 0; j < samplesNb; j++) {
            Vector2f64 coord1 = KERNEL_VALUES[j].reflect(rand);
            coord1.x = coord1.x * rad * width + uv.get(0);
            coord1.y = coord1.y * rad * height + uv.get(1);
            occ += computeOcclusionPart(coord1, positionView, normalView);
        }
        occ /= samplesNb;
        return occ * 255.0;
    }

    private double computeOcclusionPart(VectorRW tcoord, Vector3f64 p, Vector3f64 cnorm){
        final Vector3f64 diff = new Vector3f64();
        extrapolatePositionsView.getTuple(tcoord, diff);
        diff.localSubtract(p);
        double length = diff.length();
        VectorRW v = diff.localNormalize();
        if (!v.isValid()) {
            //can be NaN if it degenerate in the same point
            return 0.0;
        }
        double d = length * scale;
        return Maths.max(0.0, cnorm.dot(v) - bias) * (1.0 / (1.0 + d)) * intensity;
    }

    @Override
    public TupleSpaceCursor cursor() {
        return new DefaultTupleSpaceCursor(this);
    }

}
