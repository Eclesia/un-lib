

package science.unlicense.image.impl.process.geometric;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.impl.tuple.TupleSpaceTransform;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.task.api.AbstractTask;

/**
 *
 * @author Johann Sorel
 */
public class TransformOperator extends AbstractTask {

    public static final FieldType INPUT_TRANSFORM = new FieldTypeBuilder(new Chars("Transformation")).title(new Chars("Input target to source transform")).valueClass(Transform.class).build();

    public static final FieldType INPUT_TARGET_EXTENT = new FieldTypeBuilder(new Chars("Extent")).title(new Chars("Input target image extent")).valueClass(Extent.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("transform"), new Chars("transform"), Chars.EMPTY,TransformOperator.class,
                new FieldType[]{INPUT_IMAGE, INPUT_TRANSFORM, INPUT_TARGET_EXTENT},
                new FieldType[]{OUTPUT_IMAGE});

    public TransformOperator() {
        super(DESCRIPTOR);
    }

    public Document perform() {
        final Image inputImage = (Image) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final Transform transform = (Transform) inputParameters.getPropertyValue(INPUT_TRANSFORM.getId());
        final Extent outputExtent = (Extent) inputParameters.getPropertyValue(INPUT_TARGET_EXTENT.getId());

        final Image outputImage = execute(inputImage, transform, outputExtent);

        outputParameters.setPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),outputImage);
        return outputParameters;
    }

    public static Image execute(Image inputImage, Transform transform, Extent outputExtent){

        final TupleGrid inputSM = inputImage.getRawModel().asTupleBuffer(inputImage);
        final Image outputImage = Images.create(inputImage, outputExtent);
        final TupleGrid outputSM = outputImage.getRawModel().asTupleBuffer(outputImage);
        final TupleSpaceTransform tst = new TupleSpaceTransform(inputSM, null, transform);
        Images.sample(tst, outputSM);

        return outputImage;
    }

}
