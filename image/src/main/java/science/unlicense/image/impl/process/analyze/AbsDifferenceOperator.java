
package science.unlicense.image.impl.process.analyze;

import science.unlicense.common.api.character.Chars;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE2;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.AbstractPairPointOperator;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.common.api.model.doc.FieldType;

/**
 * Calculate absolute difference between 2 images.
 *
 * @author Johann Sorel
 */
public class AbsDifferenceOperator extends AbstractPairPointOperator {

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("absdifference"), new Chars("Absulte difference"), Chars.EMPTY,AbsDifferenceOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_IMAGE2},
                new FieldType[]{OUTPUT_IMAGE});

    public AbsDifferenceOperator() {
        super(DESCRIPTOR);
    }

    @Override
    protected double evaluate(double value1, double value2) {
        return Math.abs(value1-value2);
    }

}
