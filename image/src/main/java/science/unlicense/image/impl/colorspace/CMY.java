
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 *
 * @author Johann Sorel
 */
public final class CMY extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("CMY");
    public static final ColorSpace INSTANCE = new CMY();

    private final Transform rgbToSpace = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int sourceOffset, double[] dest, int destOffset) {
            dest[destOffset  ] = 1.0 - source[sourceOffset  ];
            dest[destOffset+1] = 1.0 - source[sourceOffset+1];
            dest[destOffset+2] = 1.0 - source[sourceOffset+2];
        }

        @Override
        public void transform1(float[] source, int sourceOffset, float[] dest, int destOffset) {
            dest[destOffset  ] = 1.0f - source[sourceOffset  ];
            dest[destOffset+1] = 1.0f - source[sourceOffset+1];
            dest[destOffset+2] = 1.0f - source[sourceOffset+2];
        }

        @Override
        public Transform invert() {
            return this;
        }
    };

    private CMY() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("C"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("M"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("Y"), null, Primitive.FLOAT32, 0, +1)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return rgbToSpace;
        return super.getTranform(cs);
    }

}
