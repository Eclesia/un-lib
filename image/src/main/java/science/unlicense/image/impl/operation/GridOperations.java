

package science.unlicense.image.impl.operation;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.operation.AbstractBinaryOperation;
import science.unlicense.geometry.api.operation.IntersectsOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.geometry.impl.operation.AbstractBinaryOperationExecutor;
import science.unlicense.geometry.impl.tuple.BoxCursor;
import science.unlicense.image.impl.s3d.Grid;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.VectorNi32;

/**
 * Geometry operations for grid.
 *
 * @author Johann Sorel
 */
public class GridOperations {

    private GridOperations(){}

    public static double[] toGridCoord(Grid grid, Tuple position){
        double[] pos = position.toDouble();
        grid.getGeomToGrid().transform(pos, 0, pos, 0, 1);
        return pos;
    }

    public static int[] toInts(double[] array){
        final int[] coord = new int[array.length];
        for (int i=0;i<coord.length;i++){
            coord[i] = (int) array[i];
        }
        return coord;
    }

    public static final AbstractBinaryOperationExecutor INTERSECTS_POINT =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Point.class, Grid.class, true){

        @Override
        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point) first;
            final Grid b = (Grid) second;
            final TupleGrid tb = b.getTuples();
            final double[] coord = toGridCoord(b, a.getCoordinate());
            //check if point is outside grid
            for (int i=0;i<coord.length;i++){
                if (coord[i]<0 || coord[i]>=tb.getExtent().get(i)) return false;
            }

            //check if cell is filled at this coordinate
            final TupleRW tuple = tb.createTuple();
            tb.getTuple(VectorNi32.create(coord), tuple);
            return tuple.get(0) != 0;
        }
    };

    public static final AbstractBinaryOperationExecutor INTERSECTS_SPHERE =
            new AbstractBinaryOperationExecutor(IntersectsOp.class, Sphere.class, Grid.class, true){

        @Override
        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Sphere a = (Sphere) first;
            final Grid b = (Grid) second;
            final double radius = a.getRadius();
            final TupleGrid tb = b.getTuples();
            final double[] coord = toGridCoord(b, a.getCenter());
            //check if point is outside grid
            for (int i=0;i<coord.length;i++){
                if (coord[i]+radius<0 || coord[i]-radius>=tb.getExtent().get(i)) return false;
            }

            //check all intersecting blocks
            final double[] lower = new double[coord.length];
            final double[] upper = new double[coord.length];
            for (int i=0;i<coord.length;i++){
                lower[i] = (int) (coord[i]-radius);
                upper[i] = (int) (coord[i]+radius);
            }

            final TupleGridCursor cursor = new BoxCursor(tb.cursor(),new BBox(lower, upper));
            final BBox voxel = new BBox(lower.length);
            boolean[] sample = new boolean[1];
            while (cursor.next()) {
                cursor.samples().toBoolean(sample, 0);
                if (sample[0]){
                    //check intersection with sphere
                    final Tuple pixelCoord = cursor.coordinate();
                    for (int i=0,n=pixelCoord.getSampleCount();i<n;i++){
                        voxel.setRange(i, pixelCoord.get(i), pixelCoord.get(i)+1);
                    }
                    if (new IntersectsOp(voxel, a).execute()) return true;
                }
            }

            return false;
        }
    };

}
