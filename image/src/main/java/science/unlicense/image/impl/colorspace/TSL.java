
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 * Doc ;
 * http://en.wikipedia.org/wiki/TSL_color_space
 *
 * @author Johann Sorel
 */
public class TSL extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("TSL");
    public static final ColorSpace INSTANCE = new TSL();

    private final Transform rgbToSpace = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            double r = source[srcOff];
            double g = source[srcOff+1];
            double b = source[srcOff+2];
            final double sum = r+g+b;
            final double sr = g / sum;
            final double sg = b / sum;
            final double r1 = sr - 1.0/3.0;
            final double g1 = sg - 1.0/3.0;

            if (g1>0){
                dest[dstOff] = ((1.0/2*Maths.PI) * Math.atan(r1/g1) + 1.0/4.0);
            } else if (g1<0){
                dest[dstOff] = ((1.0/2*Maths.PI) * Math.atan(r1/g1) + 3.0/4.0);
            } else {
                dest[dstOff] = 0.0f;
            }

            dest[dstOff+1] = Math.sqrt(9.0/5.0 * (r1*r1 + g1*g1));
            dest[dstOff+2] = (0.299*r + 0.587*g + 0.114*b);
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            float r = source[srcOff];
            float g = source[srcOff+1];
            float b = source[srcOff+2];
            final double sum = r+g+b;
            final double sr = g / sum;
            final double sg = b / sum;
            final double r1 = sr - 1.0/3.0;
            final double g1 = sg - 1.0/3.0;

            if (g1>0){
                dest[dstOff] = (float) ((1.0/2*Maths.PI) * Math.atan(r1/g1) + 1.0/4.0);
            } else if (g1<0){
                dest[dstOff] = (float) ((1.0/2*Maths.PI) * Math.atan(r1/g1) + 3.0/4.0);
            } else {
                dest[dstOff] = 0.0f;
            }

            dest[dstOff+1] = (float) Math.sqrt(9.0/5.0 * (r1*r1 + g1*g1));
            dest[dstOff+2] = (float) (0.299*r + 0.587*g + 0.114*b);
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            double t = source[srcOff];
            double s = source[srcOff+1];
            double l = source[srcOff+2];

            final double x = -Math.cos(2.0*Maths.PI*t);
            double g;
            if (t==0){
                g=0;
            } else {
                g = Math.sqrt(5.0/(9.0*(x*x+1))) * s;
                if (t>1.0/2.0) g = -g;
            }
            final double r;
            if (t==0){
                r = Math.sqrt(5)/3.0 * s;
            } else {
                r = x*g +1.0/3.0;
            }
            final double k = 1.0 / (0.185*r+0.473*g+0.114);

            dest[dstOff  ] = (k*r);
            dest[dstOff+1] = (k*g);
            dest[dstOff+2] = (k*(1-r-g));
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            double t = source[srcOff];
            double s = source[srcOff+1];
            double l = source[srcOff+2];

            final double x = -Math.cos(2.0*Maths.PI*t);
            double g;
            if (t==0){
                g=0;
            } else {
                g = Math.sqrt(5.0/(9.0*(x*x+1))) * s;
                if (t>1.0/2.0) g = -g;
            }
            final double r;
            if (t==0){
                r = Math.sqrt(5)/3.0 * s;
            } else {
                r = x*g +1.0/3.0;
            }
            final double k = 1.0 / (0.185*r+0.473*g+0.114);

            dest[dstOff  ] = (float) (k*r);
            dest[dstOff+1] = (float) (k*g);
            dest[dstOff+2] = (float) (k*(1-r-g));
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private TSL() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("t"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("s"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("l"), null, Primitive.FLOAT32, 0, +1)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }

}
