
package science.unlicense.image.impl.process.geometric;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.task.api.AbstractTask;


/**
 * FishEye operator.
 *
 * @author Johann Sorel
 */
public class FishEyeOperator extends AbstractTask {

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("fisheye"), new Chars("fisheye"), Chars.EMPTY, FishEyeOperator.class,
                new FieldType[]{INPUT_IMAGE},
                new FieldType[]{OUTPUT_IMAGE});

    public FishEyeOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image){
        inputParameters.setPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),image);
        return (Image) perform().getPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }

    @Override
    public Document perform() {
        final Image image = (Image) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());

        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);

        final int[] coord1 = new int[2];
        final int[] coord2 = new int[2];
        for (int n=height/2;coord1[1]<n;coord1[1]++){
            for (coord1[0]=0;coord1[0]<width;coord1[0]++){
                coord2[0] = coord1[0];
                coord2[1] = height-coord1[1]-1;

                //TODO
                throw new RuntimeException("Not implemented yet.");
            }
        }

        outputParameters.setPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),image);
        return outputParameters;
    }

}
