
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 * Resources :
 * https://fr.wikipedia.org/wiki/YIQ
 *
 * @author Johann Sorel
 */
public class YIQ extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("YIQ");
    public static final ColorSpace INSTANCE = new YIQ();

    private final Transform rgbToSpace = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            double r = source[srcOff  ];
            double g = source[srcOff+1];
            double b = source[srcOff+2];
            dest[dstOff  ] = 0.299000*r +  0.587000*g +  0.114000*b;
            dest[dstOff+1] = 0.595716*r + -0.274453*g + -0.321263*b;
            dest[dstOff+2] = 0.211456*r + -0.522591*g +  0.311135*b;
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            float r = source[srcOff  ];
            float g = source[srcOff+1];
            float b = source[srcOff+2];
            dest[dstOff  ] = 0.299000f*r +  0.587000f*g +  0.114000f*b;
            dest[dstOff+1] = 0.595716f*r + -0.274453f*g + -0.321263f*b;
            dest[dstOff+2] = 0.211456f*r + -0.522591f*g +  0.311135f*b;
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            double y = source[srcOff  ];
            double i = source[srcOff+1];
            double q = source[srcOff+2];
            dest[dstOff  ] = 1.0*y +  0.9563*i +  0.6210*q;
            dest[dstOff+1] = 1.0*y + -0.2721*i + -0.6474*q;
            dest[dstOff+2] = 1.0*y + -1.1070*i +  1.7026*q;
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            float y = source[srcOff  ];
            float i = source[srcOff+1];
            float q = source[srcOff+2];
            dest[dstOff  ] = 1.0f*y +  0.9563f*i +  0.6210f*q;
            dest[dstOff+1] = 1.0f*y + -0.2721f*i + -0.6474f*q;
            dest[dstOff+2] = 1.0f*y + -1.1070f*i +  1.7026f*q;
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private YIQ() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("y"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("i"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("q"), null, Primitive.FLOAT32, 0, +1)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }

}
