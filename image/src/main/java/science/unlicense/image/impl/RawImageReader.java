
package science.unlicense.image.impl;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.BitPackModel;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.image.impl.colorspace.SRGB;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Affine1;

/**
 * The raw image reader is not associated to a format because such images
 * are often stored a binary files with metadata or specific extensions.
 *
 * This reader support several uncompressed raw formats.
 *
 * @author Johann Sorel
 */
public class RawImageReader extends AbstractImageReader {

    public static final ColorSystem RGBA_4444;
    public static final ColorSystem RGB_565;
    public static final ColorSystem A_8;
    static {
        final Affine1 a0 = new Affine1(0.0, 0);
        final Affine1 a4 = new Affine1(1.0/15.0, 0);
        final Affine1 a5 = new Affine1(1.0/31.0, 0);
        final Affine1 a6 = new Affine1(1.0/63.0, 0);
        final Affine1 a8 = new Affine1(1.0/255.0, 0);
        RGBA_4444 = new ColorSystem(SRGB.INSTANCE, true, false, new Affine1[]{a4,a4,a4,a4});
        RGB_565 = new ColorSystem(SRGB.INSTANCE, false, false, new Affine1[]{a5,a6,a5});
        A_8 = new ColorSystem(SRGB.INSTANCE, true, false, new Affine1[]{a0,a0,a0,a8});
    }

    public static final int IMAGE_TYPE_RGB24    = 1;

    public static final int IMAGE_TYPE_ARGB32   = 2;
    public static final int IMAGE_TYPE_ARGB4444 = 3;
    public static final int IMAGE_TYPE_BGRA32   = 4;
    public static final int IMAGE_TYPE_RGBA32   = 5;
    public static final int IMAGE_TYPE_RGBA4444 = 6;
    public static final int IMAGE_TYPE_RGB565   = 7;

    public static final int IMAGE_TYPE_ALPHA8   = 8;



    private final Extent.Long size;
    private final int format;

    public RawImageReader(Extent.Long size, int format) {
        this.size = size;
        this.format = format;
    }

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        return new HashDictionary();
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {

        final long nbPixel = size.getL(0)*size.getL(1);
        final BufferFactory bf = parameters.getBufferFactory();

        final Buffer buffer;
        final ImageModel rm;
        final ImageModel cm;
        if (format==IMAGE_TYPE_RGB24){
            buffer = bf.createInt8(nbPixel*3).getBuffer();
            rm = new InterleavedModel(new UndefinedSystem(3), UInt8.TYPE);
            cm = DerivateModel.create(rm, new int[]{0,1,2}, null, null, ColorSystem.RGB_8BITS);
        } else if (format==IMAGE_TYPE_ARGB32){
            buffer = bf.createInt8(nbPixel*4).getBuffer();
            rm = new InterleavedModel(new UndefinedSystem(4), UInt8.TYPE);
            cm = DerivateModel.create(rm, new int[]{3,0,1,2}, null, null, ColorSystem.RGBA_8BITS);
        } else if (format==IMAGE_TYPE_ARGB4444){
            buffer = bf.createInt8(nbPixel*2).getBuffer();
            rm = new BitPackModel(new UndefinedSystem(4), 16, new int[]{0,4,8,12}, new int[]{4,4,4,4});
            cm = DerivateModel.create(rm, new int[]{1,2,3,0}, null, null, RGBA_4444);
        } else if (format==IMAGE_TYPE_BGRA32){
            buffer = bf.createInt8(nbPixel*4).getBuffer();
            rm = new InterleavedModel(new UndefinedSystem(4), UInt8.TYPE);
            cm = DerivateModel.create(rm, new int[]{2,1,0,3}, null, null, ColorSystem.RGBA_8BITS);
        } else if (format==IMAGE_TYPE_RGBA32){
            buffer = bf.createInt8(nbPixel*4).getBuffer();
            rm = new InterleavedModel(new UndefinedSystem(4), UInt8.TYPE);
            cm = DerivateModel.create(rm, new int[]{0,1,2,3}, null, null, ColorSystem.RGBA_8BITS);
        } else if (format==IMAGE_TYPE_RGBA4444){
            buffer = bf.createInt8(nbPixel*2).getBuffer();
            rm = new BitPackModel(new UndefinedSystem(4), 16, new int[]{0,4,8,12}, new int[]{4,4,4,4});
            cm = DerivateModel.create(rm, new int[]{0,1,2,3}, null, null, RGBA_4444);
        } else if (format==IMAGE_TYPE_RGB565){
            buffer = bf.createInt8(nbPixel*2).getBuffer();
            rm = new BitPackModel(new UndefinedSystem(3),16, new int[]{0,5,11}, new int[]{5,6,5});
            cm = DerivateModel.create(rm, new int[]{0,1,2}, null, null, RGB_565);
        } else if (format==IMAGE_TYPE_ALPHA8){
            buffer = bf.createInt8(nbPixel).getBuffer();
            rm = new InterleavedModel(new UndefinedSystem(1), UInt8.TYPE);
            cm = DerivateModel.create(rm, new int[]{-1,-1,-1,0}, null, null, ColorSystem.RGBA_8BITS);
        } else {
            throw new IOException("Unsupported format");
        }

        final DataInputStream ds = new DataInputStream(stream);
        ds.readFully(buffer);

        return new DefaultImage(buffer, size, rm, cm);
    }

}
