
package science.unlicense.image.impl.interpolation;

import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class NearestNeighborInterpolation2D implements Interpolation2D {

    private final TupleGrid tupleBuffer;
    private final ExtrapolationMethod extrapolationMethod;
    private final Vector2i32 coord = new Vector2i32();
    private final TupleRW tuple;

    public NearestNeighborInterpolation2D(TupleGrid tupleBuffer, ExtrapolationMethod method) {
        this.tupleBuffer = tupleBuffer;
        this.extrapolationMethod = method;
        this.tuple = tupleBuffer.createTuple();
    }

    @Override
    public ExtrapolationMethod getExtrapolationMethod() {
        return extrapolationMethod;
    }

    @Override
    public double evaluate(double x, double y, int band) {
        coord.x = (int) Maths.round(x);
        coord.y = (int) Maths.round(y);
        tupleBuffer.getTuple(coord, tuple);
        return tuple.get(band);
    }

    @Override
    public double[] evaluate(double x, double y) {
        coord.x = (int) Maths.round(x);
        coord.y = (int) Maths.round(y);
        tupleBuffer.getTuple(coord, tuple);
        return tuple.toDouble();
    }

}
