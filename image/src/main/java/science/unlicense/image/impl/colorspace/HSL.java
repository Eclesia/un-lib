
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 * Hue,Saturation,Lighting components.
 * http://en.wikipedia.org/wiki/HSL_and_HSV
 *
 * HUE : 0-360
 * SATURATION : 0-1
 * LIGHTNING : 0-1
 *
 * @author Johann Sorel
 */
public class HSL extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("HSL");
    public static final HSL INSTANCE = new HSL();

    private final Transform rgbToSpace = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            final double red   = source[srcOff  ];
            final double green = source[srcOff+1];
            final double blue  = source[srcOff+2];

            final double max = Maths.max(Maths.max(blue, red), green);
            final double min = Maths.min(Maths.min(blue, red), green);
            final double c = max - min;

            //compute H
            double h1 = 0;
            if (c == 0) {
                h1 = 0;
            } else if (max == red) {
                h1 = ((green - blue ) / c) % 6;
            } else if (max == green) {
                h1 = ((blue  - red  ) / c) + 2;
            } else if (max == blue) {
                h1 = ((red   - green) / c) + 4;
            }
            dest[dstOff  ] = Maths.wrap((int) (60 * h1), 0, 360);

            //compute L
            final double l = 0.5f * (max + min);
            dest[dstOff+2] = l;

            //compute S
            double s;
            if (c == 0) {
                s = 0;
            } else {
                s = c / (1f - Math.abs(2*l-1f) );
            }
            dest[dstOff+1] = s;
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            final float red   = source[srcOff  ];
            final float green = source[srcOff+1];
            final float blue  = source[srcOff+2];

            final float max = Maths.max(Maths.max(blue, red), green);
            final float min = Maths.min(Maths.min(blue, red), green);
            final float c = max - min;

            //compute H
            float h1 = 0;
            if (c == 0) {
                h1 = 0;
            } else if (max == red) {
                h1 = ((green - blue ) / c) % 6;
            } else if (max == green) {
                h1 = ((blue  - red  ) / c) + 2;
            } else if (max == blue) {
                h1 = ((red   - green) / c) + 4;
            }
            dest[dstOff  ] = Maths.wrap((int) (60 * h1), 0, 360);

            //compute L
            final float l = 0.5f * (max + min);
            dest[dstOff+2] = l;

            //compute S
            float s;
            if (c == 0) {
                s = 0;
            } else {
                s = c / (1f - Math.abs(2*l-1f) );
            }
            dest[dstOff+1] = s;
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            final double h = source[srcOff  ];
            final double s = source[srcOff+1];
            final double l = source[srcOff+2];

            final double c = (1- Math.abs(2*l-1)) * s;
            final double h1 = h / 60;
            final double x = c * (1 - Math.abs((h1 % 2) - 1));
            HSV.hue2rgb(h, h1, c, x, dest, dstOff);
            final double m = l - c/2;
            dest[dstOff  ] += m;
            dest[dstOff+1] += m;
            dest[dstOff+2] += m;
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            final float h = source[srcOff  ];
            final float s = source[srcOff+1];
            final float l = source[srcOff+2];

            final float c = (1- Math.abs(2*l-1)) * s;
            final float h1 = h / 60;
            final float x = c * (1 - Math.abs((h1 % 2) - 1));
            HSV.hue2rgb(h, h1, c, x, dest, dstOff);
            final float m = l - c/2;
            dest[dstOff  ] += m;
            dest[dstOff+1] += m;
            dest[dstOff+2] += m;
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private HSL() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("h"), null, Primitive.INT32,   0, +360),
            new ColorSpaceComponent(new Chars("s"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("l"), null, Primitive.FLOAT32, 0, +1),
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }
}
