
package science.unlicense.image.impl.process.paint;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.process.AbstractColorPointOperator;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.image.impl.colorspace.HSL;
import science.unlicense.image.impl.colorspace.SRGB;
import science.unlicense.math.api.Maths;

/**
 * Rotate color.
 *
 * @author Johann Sorel
 */
public class RotateColorOperator extends AbstractColorPointOperator {

    /**
     * Adjust color hue, default is 0.
     * This value will be added to the pixel hue and wrapped to 0 - 360
     */
    public static final FieldType INPUT_HUE = new FieldTypeBuilder(new Chars("Hue")).title(new Chars("Hue")).valueClass(Float.class).defaultValue(0f).build();

    /**
     * Adjust color saturation, default is 1.
     * This value will be multiplied to the pixel saturation and clipped to 0 - 1
     */
    public static final FieldType INPUT_SATURATION = new FieldTypeBuilder(new Chars("Saturation")).title(new Chars("Saturation")).valueClass(Float.class).defaultValue(1f).build();

    /**
     * Adjust color lightning, default is 1.
     * This value will be multiplied to the pixel lightning and clipped to 0 - 1
     */
    public static final FieldType INPUT_LIGHTNING = new FieldTypeBuilder(new Chars("Ligthning")).title(new Chars("Ligthning")).valueClass(Float.class).defaultValue(1f).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("rotatecolor"), new Chars("RotateColor"), Chars.EMPTY, RotateColorOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_HUE,INPUT_SATURATION,INPUT_LIGHTNING},
                new FieldType[]{OUTPUT_IMAGE});

    private final float[] hsl = new float[3];
    private final float[] bufferRGB = new float[4];
    private final float[] bufferHSL = new float[3];

    public RotateColorOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image, float h, float s, float l) {
        inputParameters.setPropertyValue(INPUT_IMAGE.getId(),image);
        inputParameters.setPropertyValue(INPUT_HUE.getId(),h);
        inputParameters.setPropertyValue(INPUT_SATURATION.getId(),s);
        inputParameters.setPropertyValue(INPUT_LIGHTNING.getId(),l);
        return (Image) perform().getPropertyValue(OUTPUT_IMAGE.getId());
    }

    @Override
    protected void prepareInputs() {
        hsl[0] = (Float) inputParameters.getPropertyValue(INPUT_HUE.getId());
        hsl[1] = (Float) inputParameters.getPropertyValue(INPUT_SATURATION.getId());
        hsl[2] = (Float) inputParameters.getPropertyValue(INPUT_LIGHTNING.getId());
    }

    @Override
    protected Color evaluate(Color color) {
        color.toRGBA(bufferRGB, 0);
        final float alpha = bufferRGB[3];
        SRGB.INSTANCE.getTranform(HSL.INSTANCE).transform(bufferRGB, bufferHSL);
        bufferHSL[0] += hsl[0];
        bufferHSL[1] *= hsl[1];
        bufferHSL[2] *= hsl[2];
        //ensure correct limits
        bufferHSL[0] = Maths.wrap(bufferHSL[0], 0, 360);
        bufferHSL[1] = Maths.clamp(bufferHSL[1], 0, 1);
        bufferHSL[2] = Maths.clamp(bufferHSL[2], 0, 1);
        HSL.INSTANCE.getTranform(SRGB.INSTANCE).transform(bufferHSL, bufferRGB);
        bufferRGB[3] = alpha;
        return new ColorRGB(bufferRGB);
    }

    public static Color rotate(Color color, double h, double s, double l){
        final float[] bufferRGB = new float[4];
        final float[] bufferHSL = new float[3];
        color.toRGBA(bufferRGB, 0);
        final float alpha = bufferRGB[3];
        SRGB.INSTANCE.getTranform(HSL.INSTANCE).transform(bufferRGB, bufferHSL);
        bufferHSL[0] += h;
        bufferHSL[1] *= s;
        bufferHSL[2] *= l;
        //ensure correct limits
        bufferHSL[0] = Maths.wrap(bufferHSL[0], 0, 360);
        bufferHSL[1] = Maths.clamp(bufferHSL[1], 0, 1);
        bufferHSL[2] = Maths.clamp(bufferHSL[2], 0, 1);
        HSL.INSTANCE.getTranform(SRGB.INSTANCE).transform(bufferHSL, bufferRGB);
        bufferRGB[3] = alpha;
        return new ColorRGB(bufferRGB);
    }
}
