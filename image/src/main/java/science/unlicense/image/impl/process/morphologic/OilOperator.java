
package science.unlicense.image.impl.process.morphologic;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.process.AbstractAreaOperator;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_EXTRAPOLATOR;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.math.api.Maths;


/**
 * Oil paint operator.
 *
 * @author Johann Sorel
 */
public class OilOperator extends AbstractAreaOperator{

    public static final FieldType INPUT_RADIUS = new FieldTypeBuilder(new Chars("Radius")).title(new Chars("Input radius")).valueClass(Integer.class).defaultValue(5).build();
    public static final FieldType INPUT_NBLEVEL = new FieldTypeBuilder(new Chars("NbLevel")).title(new Chars("Input number of intensity levels")).valueClass(Integer.class).defaultValue(20).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("oil"), new Chars("Oil"), Chars.EMPTY, OilOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_EXTRAPOLATOR,INPUT_RADIUS,INPUT_NBLEVEL},
                new FieldType[]{OUTPUT_IMAGE});

    private int radius;
    private int nbLevel;
    private int size;
    private double[][] average;
    private int[] intensityCount;

    public OilOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image, Chars extrapolatorType, int radius, int nbLevel) {
        inputParameters.setPropertyValue(INPUT_IMAGE.getId(),image);
        inputParameters.setPropertyValue(INPUT_EXTRAPOLATOR.getId(),extrapolatorType);
        inputParameters.setPropertyValue(INPUT_RADIUS.getId(),radius);
        inputParameters.setPropertyValue(INPUT_NBLEVEL.getId(),nbLevel);
        return (Image) perform().getPropertyValue(OUTPUT_IMAGE.getId());
    }

    @Override
    protected void prepareInputs() {
        super.prepareInputs();
        radius = (Integer) inputParameters.getPropertyValue(INPUT_RADIUS.getId());
        nbLevel = (Integer) inputParameters.getPropertyValue(INPUT_NBLEVEL.getId());
        average = new double[nbLevel][nbSample];
        intensityCount = new int[nbLevel];
        size = 1 + radius + radius;
    }

    @Override
    protected int getTopPadding() {
        return radius;
    }

    @Override
    protected int getBottomPadding() {
        return radius;
    }

    @Override
    protected int getLeftPadding() {
        return radius;
    }

    @Override
    protected int getRightPadding() {
        return radius;
    }

    @Override
    protected void evaluate(double[][][] workspace, double[] result) {
        Arrays.fill(intensityCount, 0);
        for (int i=0;i<average.length;i++){
            Arrays.fill(average[i], 0.0);
        }

        int maxIntensity = 0;
        int maxCount = -1;

        for (int kj = 0; kj < size; kj++){
            for (int ki = 0; ki < size; ki++) {
                final double avg = Maths.sum(workspace[kj][ki])/nbSample;
                final int curIntensity = (int) (avg*nbLevel)/256;
                intensityCount[curIntensity]++;
                if (intensityCount[curIntensity]> maxCount){
                    maxCount = intensityCount[curIntensity];
                    maxIntensity = curIntensity;
                }

                for (int k=0;k<nbSample;k++){
                    average[curIntensity][k] += workspace[kj][ki][k];
                }
            }
        }

        //calculate final color
        for (int i=0;i<nbSample;i++){
            result[i] = average[maxIntensity][i] / maxCount;
        }

    }

}
