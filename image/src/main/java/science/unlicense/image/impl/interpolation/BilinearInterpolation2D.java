
package science.unlicense.image.impl.interpolation;

import science.unlicense.geometry.api.tuple.TupleGrid;
import static science.unlicense.math.api.EasingMethods.linear;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class BilinearInterpolation2D implements Interpolation2D {

    private final TupleGrid tupleBuffer;
    private final ExtrapolationMethod extrapolationMethod;
    private final Vector2i32 coord = new Vector2i32();
    private final TupleRW tuple1;
    private final TupleRW tuple2;
    private final TupleRW tuple3;
    private final TupleRW tuple4;
    private final double[] result;

    public BilinearInterpolation2D(TupleGrid tupleBuffer, ExtrapolationMethod method) {
        this.tupleBuffer = tupleBuffer;
        this.extrapolationMethod = method;
        this.tuple1 = tupleBuffer.createTuple();
        this.tuple2 = tupleBuffer.createTuple();
        this.tuple3 = tupleBuffer.createTuple();
        this.tuple4 = tupleBuffer.createTuple();
        this.result = new double[tupleBuffer.getSampleSystem().getNumComponents()];
    }

    @Override
    public ExtrapolationMethod getExtrapolationMethod() {
        return extrapolationMethod;
    }

    @Override
    public double evaluate(double x, double y, int band) {
        return evaluate(x, y)[band];
    }

    @Override
    public double[] evaluate(double x, double y) {
        int floorx = (int) Math.floor(x);
        int ceilx = (int) Math.ceil(x);
        int floory = (int) Math.floor(y);
        int ceily = (int) Math.ceil(y);
        double lx = (x-floorx);
        double ly = (y-floory);

        coord.x = floorx;
        coord.y = floory;
        tupleBuffer.getTuple(coord, tuple1);
        coord.x = ceilx;
        coord.y = floory;
        tupleBuffer.getTuple(coord, tuple2);
        coord.x = floorx;
        coord.y = ceily;
        tupleBuffer.getTuple(coord, tuple3);
        coord.x = ceilx;
        coord.y = ceily;
        tupleBuffer.getTuple(coord, tuple4);

        for (int i=0,n=tupleBuffer.getSampleSystem().getNumComponents(); i<n; i++) {
            result[i] = linear(ly,
                            linear(lx, tuple1.get(i), tuple2.get(i)),
                            linear(lx, tuple3.get(i), tuple4.get(i))
                        );
        }

        return result;
    }

}
