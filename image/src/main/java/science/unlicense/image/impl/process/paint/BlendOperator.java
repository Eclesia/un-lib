

package science.unlicense.image.impl.process.paint;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRW;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.task.api.AbstractTask;

/**
 * Paint using a blend operator an image over another.
 *
 * @author Johann Sorel
 */
public class BlendOperator extends AbstractTask{

    public static final FieldType INPUT_BLENDIMAGE = new FieldTypeBuilder(new Chars("BlendImage")).title(new Chars("Image to paint over")).valueClass(Image.class).build();
    public static final FieldType INPUT_BLENDALPHA = new FieldTypeBuilder(new Chars("AlphaBlending")).title(new Chars("AlphaBlending")).valueClass(AlphaBlending.class).build();
    public static final FieldType INPUT_TRANSFORM = new FieldTypeBuilder(new Chars("Transform")).title(new Chars("Transform")).valueClass(Affine.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("blend"), new Chars("Blend"), Chars.EMPTY, BlendOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_BLENDIMAGE,INPUT_BLENDALPHA,INPUT_TRANSFORM},
                new FieldType[]{OUTPUT_IMAGE});

    public BlendOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image base, Image over, AlphaBlending blend, Affine trs){
        inputParameters.setPropertyValue(INPUT_IMAGE.getId(),base);
        inputParameters.setPropertyValue(INPUT_BLENDIMAGE.getId(),over);
        inputParameters.setPropertyValue(INPUT_BLENDALPHA.getId(),blend);
        inputParameters.setPropertyValue(INPUT_TRANSFORM.getId(),trs);
        return(Image) perform().getPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }

    @Override
    public Document perform() {
        final Image base = (Image) inputParameters.getPropertyValue(INPUT_IMAGE.getId());
        final Image image = (Image) inputParameters.getPropertyValue(INPUT_BLENDIMAGE.getId());
        final AlphaBlending alphaBlending = (AlphaBlending) inputParameters.getPropertyValue(INPUT_BLENDALPHA.getId());
        final Affine transform = (Affine) inputParameters.getPropertyValue(INPUT_TRANSFORM.getId());

        final int width = (int) base.getExtent().getL(0);
        final int height = (int) base.getExtent().getL(1);

        final TupleGridCursor dstCursor = base.getColorModel().asTupleBuffer(base).cursor();
        final ColorSystem dstCs = (ColorSystem) base.getColorModel().getSampleSystem();
        final ColorRW dstColor = Colors.castOrWrap(dstCursor.samples(), dstCs);

        final TupleGrid srcTuples = image.getColorModel().asTupleBuffer(image);
        final TupleGridCursor srcCursor = srcTuples.cursor();
        final ColorSystem srcCs = (ColorSystem) image.getColorModel().getSampleSystem();
        final Color srcColor = Colors.castOrWrap(srcCursor.samples(), srcCs);

        final TupleRW targetcoord = VectorNf64.createDouble(image.getExtent().getDimension());

        final double[] coord = new double[2];
        while (srcCursor.next()) {
            //convert in image coordinate
            final int[] gridCoord = srcCursor.coordinate().toInt();
            coord[0] = gridCoord[0];
            coord[1] = gridCoord[1];
            transform.transform(coord, 0, coord, 0, 1);
            targetcoord.set(0, (int) coord[0]);
            targetcoord.set(1, (int) coord[1]);
            if (targetcoord.get(0)<0 || targetcoord.get(0)>width || targetcoord.get(1)<0 || targetcoord.get(1)>height){
                //out of image
                continue;
            }
            dstCursor.moveTo(targetcoord);

            //blend color
            dstColor.set(alphaBlending.blend(srcColor,dstColor));
        }


        outputParameters.setPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),base);
        return outputParameters;
    }

}
