
package science.unlicense.image.impl.process.analyze;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.image.api.color.Color;
import science.unlicense.geometry.api.BBox;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.task.api.AbstractTask;
import science.unlicense.geometry.impl.algorithm.MedianCut;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ColorIndex;
import science.unlicense.geometry.api.tuple.TupleGrid;

/**
 * Operator which extract a color palette for an image.
 * This operator use the median cut algorithm.
 *
 * @author Johann Sorel
 */
public class CreateColorIndexOperator extends AbstractTask {

    public static final FieldType INPUT_NBCOLOR = new FieldTypeBuilder(new Chars("NbColor")).title(new Chars("Number of colors")).valueClass(Integer.class).build();
    public static final FieldType OUTPUT_PALETTE = new FieldTypeBuilder(new Chars("Palette")).title(new Chars("Palette")).valueClass(ColorIndex.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("extractpalette"), new Chars("ExtractPalette"), Chars.EMPTY,CreateColorIndexOperator.class,
                new FieldType[]{AbstractImageTaskDescriptor.INPUT_IMAGE,INPUT_NBCOLOR},
                new FieldType[]{OUTPUT_PALETTE});

    public CreateColorIndexOperator() {
        super(DESCRIPTOR);
    }

    public ColorIndex executeNow(Image image, int nbColor){
        inputParameters.setPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),image);
        inputParameters.setPropertyValue(INPUT_NBCOLOR.getId(),nbColor);
        return (ColorIndex) perform().getPropertyValue(OUTPUT_PALETTE.getId());
    }

    public Document perform() {
        final Image inputImage = (Image) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final int nbColor = (Integer) inputParameters.getPropertyValue(INPUT_NBCOLOR.getId());

        final ImageModel colorModel = inputImage.getColorModel();
        final ColorSystem cs = (ColorSystem) colorModel.getSampleSystem();
        final TupleGrid tb = colorModel.asTupleBuffer(inputImage);
        final Sequence bboxes = MedianCut.medianCut(tb, nbColor);

        final Color[] palette = new Color[bboxes.getSize()];
        for (int i=0;i<palette.length;i++){
            final BBox bbox = (BBox) bboxes.get(i);
            palette[i] = new ColorRGB(bbox.getMiddle().toFloat(), cs);
        }

        outputParameters.setPropertyValue(OUTPUT_PALETTE.getId(),new ColorIndex(palette));
        return outputParameters;
    }

}
