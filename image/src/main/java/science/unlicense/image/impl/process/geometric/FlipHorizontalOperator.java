
package science.unlicense.image.impl.process.geometric;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.task.api.AbstractTask;


/**
 * Horizontal flip operator.
 *
 * @author Johann Sorel
 */
public class FlipHorizontalOperator extends AbstractTask {

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("flip-horizontal"), new Chars("flip-horizontal"), Chars.EMPTY, FlipHorizontalOperator.class,
                new FieldType[]{INPUT_IMAGE},
                new FieldType[]{OUTPUT_IMAGE});

    public FlipHorizontalOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image){
        inputParameters.setPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),image);
        return (Image) perform().getPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }

    @Override
    public Document perform() {
        final Image image = (Image) inputParameters.getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());

        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final TupleGrid tb = image.getRawModel().asTupleBuffer(image);
        final TupleRW storage1 = tb.createTuple();
        final TupleRW storage2 = tb.createTuple();

        final Vector2i32 coord1 = new Vector2i32();
        final Vector2i32 coord2 = new Vector2i32();
        final int n = width/2;
        for (coord1.y=0;coord1.y<height;coord1.y++){
            for (coord1.x=0;coord1.x<n;coord1.x++){
                coord2.x = width-coord1.x-1;
                coord2.y = coord1.y;

                tb.getTuple(coord1, storage1);
                tb.getTuple(coord2, storage2);
                tb.setTuple(coord1, storage2);
                tb.setTuple(coord2, storage1);
            }
        }

        outputParameters.setPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),image);
        return outputParameters;
    }

}
