
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 * Hue,Saturation,Intensity components.
 * http://en.wikipedia.org/wiki/HSL_and_HSV
 *
 * HUE : 0-360
 * SATURATION : 0-1
 * INTENSITY : 0-1
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class HSI extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("HSI");
    public static final HSI INSTANCE = new HSI();

    private final Transform rgbToSpace = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private HSI() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("h"), null, Primitive.INT32,   0, +360),
            new ColorSpaceComponent(new Chars("s"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("i"), null, Primitive.FLOAT32, 0, +1)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }

}
