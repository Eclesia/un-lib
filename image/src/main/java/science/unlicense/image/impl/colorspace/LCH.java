
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import static science.unlicense.image.impl.colorspace.HSV.hue2rgb;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 * Luma(Y601),Chroma,Hue components
 * http://en.wikipedia.org/wiki/HSL_and_HSV
 *
 * LUMA : 0-1
 * CHROMA : 0-1
 * HUE : 0-360
 *
 * @author Johann Sorel
 */
public class LCH extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("LCH");
    public static final LCH INSTANCE = new LCH();

    private final Transform rgbToSpace = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            throw new UnimplementedException("TODO");
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            final double l = source[srcOff  ];
            final double c = source[srcOff+1];
            final double h = source[srcOff+2];

            final double h1 = h / 60;
            final double x = c * (1 - Math.abs((h1 % 2) - 1));
            hue2rgb(h, h1, c, x, dest, dstOff);
            final double m =  l - (0.3f*dest[dstOff] + 0.59f*dest[dstOff+1] + 0.11f*dest[dstOff+2]);
            dest[0] += m;
            dest[1] += m;
            dest[2] += m;
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            final float l = source[srcOff  ];
            final float c = source[srcOff+1];
            final float h = source[srcOff+2];

            final float h1 = h / 60;
            final float x = c * (1 - Math.abs((h1 % 2) - 1));
            hue2rgb(h, h1, c, x, dest, dstOff);
            final float m =  l - (0.3f*dest[dstOff] + 0.59f*dest[dstOff+1] + 0.11f*dest[dstOff+2]);
            dest[0] += m;
            dest[1] += m;
            dest[2] += m;
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private LCH() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("l"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("c"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("h"), null, Primitive.INT32,   0, +360)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }

}
