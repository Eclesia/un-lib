
package science.unlicense.image.impl.process.analyze;

import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.tuple.DefaultTupleSpaceCursor;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public class Threshold implements TupleSpace {

    private final TupleSpace space;
    private final Tuple under;
    private final Tuple above;
    private final Predicate limit;

    public Threshold(TupleSpace space, Tuple under, Tuple above, Predicate limit) {
        this.space = space;
        this.under = under;
        this.above = above;
        this.limit = limit;
    }

    @Override
    public SampleSystem getCoordinateSystem() {
        return space.getCoordinateSystem();
    }

    @Override
    public SampleSystem getSampleSystem() {
        return under.getSampleSystem();
    }

    @Override
    public ArithmeticType getNumericType() {
        return under.getNumericType();
    }

    @Override
    public Geometry getCoordinateGeometry() {
        return space.getCoordinateGeometry();
    }

    @Override
    public TupleRW createTuple() {
        return Vectors.create(getSampleSystem(), getNumericType());
    }

    @Override
    public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
        space.getTuple(coordinate, buffer);
        if (limit.evaluate(buffer)) {
            buffer.set(above);
        } else {
            buffer.set(under);
        }
        return buffer;
    }

    @Override
    public TupleSpaceCursor cursor() {
        return new DefaultTupleSpaceCursor(this);
    }

}
