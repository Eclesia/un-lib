

package science.unlicense.image.impl.process.paint;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.task.api.AbstractTask;

/**
 *
 * @author Johann Sorel
 */
public class FloodFillOperator extends AbstractTask{

    public static final FieldType INPUT_POINT = new FieldTypeBuilder(new Chars("Point")).title(new Chars("Point")).valueClass(Tuple.class).build();
    public static final FieldType INPUT_PREDICATE = new FieldTypeBuilder(new Chars("Predicate")).title(new Chars("Predicate")).valueClass(Predicate.class).build();
    public static final FieldType INPUT_FILLSAMPLE = new FieldTypeBuilder(new Chars("FillSample")).title(new Chars("FillSample")).valueClass(Tuple.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("floodfill"), new Chars("FloodFill"), Chars.EMPTY, FloodFillOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_POINT,INPUT_PREDICATE,INPUT_FILLSAMPLE},
                new FieldType[]{OUTPUT_IMAGE});

    private final Sequence stack = new ArraySequence();
    private Predicate predicate;
    private TupleGrid tb;
    private int width;
    private int height;
    private TupleRW sample;
    private boolean topSet = false;
    private boolean bottomSet = false;

    public FloodFillOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image base, Tuple point, Predicate predicate, Object fill){
        inputParameters.setPropertyValue(INPUT_IMAGE.getId(),base);
        inputParameters.setPropertyValue(INPUT_POINT.getId(),point);
        inputParameters.setPropertyValue(INPUT_PREDICATE.getId(),predicate);
        inputParameters.setPropertyValue(INPUT_FILLSAMPLE.getId(),fill);
        return (Image) perform().getPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }

    @Override
    public Document perform() {
        final Image base = (Image) inputParameters.getPropertyValue(INPUT_IMAGE.getId());
        final Tuple point = (Tuple) inputParameters.getPropertyValue(INPUT_POINT.getId());
        predicate = (Predicate) inputParameters.getPropertyValue(INPUT_PREDICATE.getId());
        final Tuple fillSample = (Tuple) inputParameters.getPropertyValue(INPUT_FILLSAMPLE.getId());

        tb = base.getRawModel().asTupleBuffer(base);
        width = (int) base.getExtent().getL(0);
        height = (int) base.getExtent().getL(1);
        sample = tb.createTuple();

        int x = (int) point.get(0);
        int y = (int) point.get(1);

        stack.add(new Vector2i32(x,y));

        while (!stack.isEmpty()){
            final int index = stack.getSize()-1;
            final Vector2i32 coord = (Vector2i32) stack.get(index);
            stack.remove(index);

            bottomSet=false;
            topSet=false;

            final int startX = coord.x;

            //search left
            for (;coord.x>=0;coord.x--){
                tb.getTuple(coord, sample);
                if (predicate.evaluate(sample)){
                    tb.setTuple(coord, fillSample);
                    checkTopBottom(coord);
                } else {
                    break;
                }
            }
            //search right
            for (coord.x=startX+1;coord.x<width;coord.x++){
                tb.getTuple(coord, sample);
                if (predicate.evaluate(sample)){
                    tb.setTuple(coord, fillSample);
                    checkTopBottom(coord);
                } else {
                    break;
                }
            }
        }
        outputParameters.setPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),base);
        return outputParameters;
    }

    private void checkTopBottom(Vector2i32 coord){
        //check under
        if (coord.y>0){
            coord.y--;
            tb.getTuple(coord, sample);
            if (predicate.evaluate(sample)){
                if (!bottomSet){
                    stack.add(new Vector2i32(coord.x,coord.y));
                    bottomSet = true;
                }
            } else {
                bottomSet=false;
            }
            coord.y++;
        }

        //check above
        if (coord.y<height-1){
            coord.y++;
            tb.getTuple(coord, sample);
            if (predicate.evaluate(sample)){
                if (!topSet){
                    stack.add(new Vector2i32(coord.x,coord.y));
                    topSet = true;
                }
            } else {
                topSet=false;
            }
            coord.y--;
        }
    }

}
