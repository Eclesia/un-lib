

package science.unlicense.image.impl.s3d;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.number.Bits;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.impl.AbstractGeometry;
import science.unlicense.math.api.Affine;
import science.unlicense.math.impl.Affine3;


/**
 * A grid is an N dimension image used to fill space.
 * Values in the image should be interpreted as boolean values.
 * A value of true means the voxel is filled and can be used as if it was a cube.
 *
 * TODO uncomplete class, just a draft for physics engine
 *
 * @author Johann Sorel
 */
public class Grid extends AbstractGeometry {

    private final BBox sceneBbox;
    private final TupleGrid image;
    private final Affine geomToGrid;
    private final Affine gridToGeom;

    public Grid(BBox bbox, Extent.Long imageSize) {
        super(imageSize.getDimension());
        this.sceneBbox = bbox;
        final Buffer buffer = InterleavedTupleGrid.createPrimitiveBuffer(imageSize, Bits.TYPE_1_BIT, 1, DefaultBufferFactory.INSTANCE);
        this.image = InterleavedTupleGrid.create(buffer, Bits.TYPE_1_BIT, CoordinateSystems.undefined(1), imageSize);

        final double scaleX = bbox.getSpan(0)/(imageSize.get(0));
        final double scaleY = bbox.getSpan(1)/(imageSize.get(1));
        final double scaleZ = bbox.getSpan(2)/(imageSize.get(2));
        this.gridToGeom = new Affine3(
                scaleX, 0, 0, sceneBbox.getMin(0),
                0, scaleY, 0, sceneBbox.getMin(1),
                0, 0, scaleZ, sceneBbox.getMin(2));
        this.geomToGrid = gridToGeom.invert();
    }

    /**
     *
     * @param tuples
     * @param geomToGrid transform at voxel corner
     */
    public Grid(TupleGrid tuples, Affine geomToGrid) {
        super(tuples.getExtent().getDimension());
        this.image = tuples;
        this.geomToGrid = geomToGrid;
        this.gridToGeom = geomToGrid.invert();
        //TODO
        this.sceneBbox = null;
    }

    public TupleGrid getTuples() {
        return image;
    }

    public Affine getGeomToGrid() {
        return geomToGrid;
    }

    public Affine getGridToGeom() {
        return gridToGeom;
    }

    @Override
    public BBox getBoundingBox() {
        return new BBox(sceneBbox);
    }

}
