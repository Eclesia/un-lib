
package science.unlicense.image.impl.interpolation;

/**
 *
 * @author Johann Sorel
 */
public interface Interpolation2D {

    /**
     *
     * @return
     */
    ExtrapolationMethod getExtrapolationMethod();

    /**
     *
     * @param x
     * @param y
     * @param band
     * @return
     */
    double evaluate(double x, double y, int band);

    /**
     *
     * @param x
     * @param y
     * @return
     */
    double[] evaluate(double x, double y);

}
