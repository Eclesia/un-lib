
package science.unlicense.image.impl.process.noise;

import java.util.Random;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.AbstractPointOperator;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;

/**
 * Add gaussian noise to image.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public class GaussianNoiseOperator extends AbstractPointOperator {

    public static final FieldType INPUT_SIGMA = new FieldTypeBuilder(new Chars("Sigma")).valueClass(Double.class).build();
    public static final FieldType INPUT_MEAN = new FieldTypeBuilder(new Chars("Mean")).valueClass(Double.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("gaussiannoise"), new Chars("Gaussian Noise"), Chars.EMPTY, GaussianNoiseOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_SIGMA,INPUT_MEAN},
                new FieldType[]{OUTPUT_IMAGE});

    private final Random gauss = new Random();
    private final Random sign = new Random();
    private double sigma;
    private double mean;

    public GaussianNoiseOperator() {
        super(DESCRIPTOR);
    }

    private double getGaussian() {
        return gauss.nextGaussian() * sigma + mean;
    }

    protected void prepareInputs() {
        sigma = (Double) inputParameters.getPropertyValue(INPUT_SIGMA.getId());
        mean = (Double) inputParameters.getPropertyValue(INPUT_MEAN.getId());
    }

    protected double evaluate(double value) {
        int s = sign.nextInt(2);
        if (s==0) s--;
        return value + getGaussian() * s;
    }

}
