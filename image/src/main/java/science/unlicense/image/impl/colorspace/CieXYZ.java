
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 *
 * Resources :
 * https://en.wikipedia.org/wiki/SRGB#The_sRGB_transfer_function_.28.22gamma.22.29
 *
 * @author Johann Sorel
 */
public class CieXYZ extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("CIE-XYZ");
    public static final ColorSpace INSTANCE = new CieXYZ();

    private final Transform rgbToSpace = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            double r = source[srcOff  ];
            double g = source[srcOff+1];
            double b = source[srcOff+2];
            r = RGBtoXYZTemp(r);
            g = RGBtoXYZTemp(g);
            b = RGBtoXYZTemp(b);
            dest[dstOff  ] = 0.4360747*r + 0.3850649 *g + 0.1430804*b;
            dest[dstOff+1] = 0.2225045*r + 0.7168786 *g + 0.0606169*b;
            dest[dstOff+2] = 0.0139322*r + 0.0971045 *g + 0.7141733*b;
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            double r = source[srcOff  ];
            double g = source[srcOff+1];
            double b = source[srcOff+2];
            r = RGBtoXYZTemp(r);
            g = RGBtoXYZTemp(g);
            b = RGBtoXYZTemp(b);
            dest[dstOff  ] = (float) (0.4360747*r + 0.3850649 *g + 0.1430804*b);
            dest[dstOff+1] = (float) (0.2225045*r + 0.7168786 *g + 0.0606169*b);
            dest[dstOff+2] = (float) (0.0139322*r + 0.0971045 *g + 0.7141733*b);
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            double x = source[srcOff  ];
            double y = source[srcOff+1];
            double z = source[srcOff+2];
            double r =  3.1338561*x + -1.6168667*y + -0.4906146*z;
            double g = -0.9787684*x +  1.9161415*y +  0.0334540*z;
            double b =  0.0719453*x + -0.2289914*y +  1.4052427*z;
            dest[dstOff  ] = XYZtoRGBTemp(r);
            dest[dstOff+1] = XYZtoRGBTemp(g);
            dest[dstOff+2] = XYZtoRGBTemp(b);
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            double x = source[srcOff  ];
            double y = source[srcOff+1];
            double z = source[srcOff+2];
            double r =  3.1338561*x + -1.6168667*y + -0.4906146*z;
            double g = -0.9787684*x +  1.9161415*y +  0.0334540*z;
            double b =  0.0719453*x + -0.2289914*y +  1.4052427*z;
            dest[dstOff  ] = (float) XYZtoRGBTemp(r);
            dest[dstOff+1] = (float) XYZtoRGBTemp(g);
            dest[dstOff+2] = (float) XYZtoRGBTemp(b);
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private CieXYZ() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("X"), null, Primitive.FLOAT32, 0, 1),
            new ColorSpaceComponent(new Chars("Y"), null, Primitive.FLOAT32, 0, 1),
            new ColorSpaceComponent(new Chars("Z"), null, Primitive.FLOAT32, 0, 1)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }

    static double RGBtoXYZTemp(double c) {
        if (c <= 0.04045){
            c /= 12.92;
        } else {
            c = Math.pow((c+0.055)/1.055,2.4);
        }
        return c;
    }

    static double XYZtoRGBTemp(double c) {
        if (c<0.0031308){
            return 12.92*c;
        } else {
            return 1.055*Math.pow(c, 1.0/2.4)-0.055;
        }
    }
}
