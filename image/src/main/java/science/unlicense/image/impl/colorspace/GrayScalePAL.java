
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 * Grayscale using PAL luma components.
 * http://en.wikipedia.org/wiki/Grayscale
 *
 * @author Johann Sorel
 */
public class GrayScalePAL extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("Grayscale-PAL");
    public static final ColorSpace INSTANCE = new GrayScalePAL();

    private final Transform rgbToSpace = new SimplifiedTransform(3,1) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 1;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            dest[dstOff] = source[srcOff] * 0.299f + source[srcOff+1] * 0.587f + source[srcOff+2] * 0.114f;
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            dest[dstOff] = source[srcOff] * 0.299f + source[srcOff+1] * 0.587f + source[srcOff+2] * 0.114f;
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(1,3) {
        @Override
        public int getInputDimensions() {
            return 1;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            dest[dstOff  ] = source[srcOff];
            dest[dstOff+1] = source[srcOff];
            dest[dstOff+2] = source[srcOff];
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            dest[dstOff  ] = source[srcOff];
            dest[dstOff+1] = source[srcOff];
            dest[dstOff+2] = source[srcOff];
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private GrayScalePAL() {
        super(NAME, new ColorSpaceComponent[]{new ColorSpaceComponent(new Chars("G"), null, Primitive.FLOAT32, 0, 1)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }

}
