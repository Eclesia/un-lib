
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 *
 * @author Johann Sorel
 */
public final class CMYK extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("CMYK");
    public static final ColorSpace INSTANCE = new CMYK();

    private final Transform rgbToSpace = new SimplifiedTransform(3,4) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 4;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            double k = 1.0 - Maths.max(Maths.max(source[srcOff],source[srcOff+1]), source[srcOff+2]);
            final double ik = (1.0-k);
            if (ik == 0.0) {
                //full black
                dest[dstOff+0] = 0.0;
                dest[dstOff+1] = 0.0;
                dest[dstOff+2] = 0.0;
                dest[dstOff+3] = k;
            } else {
                dest[dstOff+0] = (1.0-source[srcOff  ]-k) / ik;
                dest[dstOff+1] = (1.0-source[srcOff+1]-k) / ik;
                dest[dstOff+2] = (1.0-source[srcOff+2]-k) / ik;
                dest[dstOff+3] = k;
            }
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            float k = 1.0f - Maths.max(Maths.max(source[srcOff],source[srcOff+1]), source[srcOff+2]);
            final float ik = (1.0f-k);
            if (ik == 0.0f) {
                //full black
                dest[dstOff+0] = 0.0f;
                dest[dstOff+1] = 0.0f;
                dest[dstOff+2] = 0.0f;
                dest[dstOff+3] = k;
            } else {
                dest[dstOff+0] = (1.0f-source[srcOff  ]-k) / ik;
                dest[dstOff+1] = (1.0f-source[srcOff+1]-k) / ik;
                dest[dstOff+2] = (1.0f-source[srcOff+2]-k) / ik;
                dest[dstOff+3] = k;
            }
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(4,3) {
        @Override
        public int getInputDimensions() {
            return 4;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            final double ik = (1.0-source[srcOff+3]);
            dest[dstOff  ]  = (1.0-source[srcOff  ]) * ik;
            dest[dstOff+1]  = (1.0-source[srcOff+1]) * ik;
            dest[dstOff+2]  = (1.0-source[srcOff+2]) * ik;
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            final float ik = (1.0f-source[srcOff+3]);
            dest[dstOff  ] = (1.0f-source[srcOff  ]) * ik;
            dest[dstOff+1] = (1.0f-source[srcOff+1]) * ik;
            dest[dstOff+2] = (1.0f-source[srcOff+2]) * ik;
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private CMYK() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("C"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("M"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("Y"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("K"), null, Primitive.FLOAT32, 0, +1)
        });
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }
}
