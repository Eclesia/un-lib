
package science.unlicense.image.impl.colorspace;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.image.api.colorspace.AbstractColorSpace;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 * Hue,Saturation,Value components.
 * http://en.wikipedia.org/wiki/HSL_and_HSV
 *
 * HUE : 0-360
 * SATURATION : 0-1
 * VALUE : 0-1
 *
 * @author Johann Sorel
 */
public class HSV extends AbstractColorSpace{

    public static final Chars NAME = Chars.constant("HSV");
    public static final HSV INSTANCE = new HSV();

    private final Transform rgbToSpace = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            final double red   = source[srcOff  ];
            final double green = source[srcOff+1];
            final double blue  = source[srcOff+2];

            final double max = Maths.max(Maths.max(blue, red), green);
            final double min = Maths.min(Maths.min(blue, red), green);

            final double c = max - min;

            //compute H
            double h1 = 0;
            if (c == 0) {
                h1 = 0;
            } else if (max == red) {
                h1 = ((green - blue ) / c) % 6;
            } else if (max == green) {
                h1 = ((blue  - red  ) / c) + 2;
            } else if (max == blue) {
                h1 = ((red   - green) / c) + 4;
            }
            dest[dstOff  ] = Maths.wrap((int) (60 * h1), 0, 360);

            //compute L
            final double v = max;
            dest[dstOff+2] = v;

            //compute V
            double s;
            if (c == 0) {
                s = 0;
            } else {
                s = c / v;
            }
            dest[dstOff+1] = s;
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            final float red   = source[srcOff  ];
            final float green = source[srcOff+1];
            final float blue  = source[srcOff+2];

            final float max = Maths.max(Maths.max(blue, red), green);
            final float min = Maths.min(Maths.min(blue, red), green);

            final float c = max - min;

            //compute H
            float h1 = 0;
            if (c == 0) {
                h1 = 0;
            } else if (max == red) {
                h1 = ((green - blue ) / c) % 6;
            } else if (max == green) {
                h1 = ((blue  - red  ) / c) + 2;
            } else if (max == blue) {
                h1 = ((red   - green) / c) + 4;
            }
            dest[dstOff  ] = Maths.wrap((int) (60 * h1), 0, 360);

            //compute L
            final float v = max;
            dest[dstOff+2] = v;

            //compute V
            float s;
            if (c == 0) {
                s = 0;
            } else {
                s = c / v;
            }
            dest[dstOff+1] = s;
        }

        @Override
        public Transform invert() {
            return spaceToRgb;
        }
    };

    private final Transform spaceToRgb = new SimplifiedTransform(3) {
        @Override
        public int getInputDimensions() {
            return 3;
        }

        @Override
        public int getOutputDimensions() {
            return 3;
        }

        @Override
        public void transform1(double[] source, int srcOff, double[] dest, int dstOff) {
            final double h = source[srcOff  ];
            final double s = source[srcOff+1];
            final double v = source[srcOff+2];

            final double c = v * s;
            final double h1 = h / 60;
            final double x = c * (1 - Math.abs((h1 % 2) - 1));
            hue2rgb(h, h1, c, x, dest, dstOff);
            final double m = v - c;
            dest[dstOff  ] += m;
            dest[dstOff+1] += m;
            dest[dstOff+2] += m;
        }

        @Override
        public void transform1(float[] source, int srcOff, float[] dest, int dstOff) {
            final float h = source[srcOff  ];
            final float s = source[srcOff+1];
            final float v = source[srcOff+2];
            final float c = v * s;
            final float h1 = h / 60;
            final float x = c * (1 - Math.abs((h1 % 2) - 1));
            hue2rgb(h, h1, c, x, dest, dstOff);
            final float m = v - c;
            dest[dstOff  ] += m;
            dest[dstOff+1] += m;
            dest[dstOff+2] += m;
        }

        @Override
        public Transform invert() {
            return rgbToSpace;
        }
    };

    private HSV() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("h"), null, Primitive.INT32,   0, +360),
            new ColorSpaceComponent(new Chars("s"), null, Primitive.FLOAT32, 0, +1),
            new ColorSpaceComponent(new Chars("v"), null, Primitive.FLOAT32, 0, +1)
        });
    }

    static void hue2rgb(double h, double h1, double c, double x, double[] rgb, int rgbOff) {
        //if (h==0)     { rgba[0] = 0; rgba[1]=0; rgba[2]=0; }
        if (h1<1)     { rgb[rgbOff] = c; rgb[rgbOff+1]=x; rgb[rgbOff+2]=0; }
        else if (h1<2){ rgb[rgbOff] = x; rgb[rgbOff+1]=c; rgb[rgbOff+2]=0; }
        else if (h1<3){ rgb[rgbOff] = 0; rgb[rgbOff+1]=c; rgb[rgbOff+2]=x; }
        else if (h1<4){ rgb[rgbOff] = 0; rgb[rgbOff+1]=x; rgb[rgbOff+2]=c; }
        else if (h1<5){ rgb[rgbOff] = x; rgb[rgbOff+1]=0; rgb[rgbOff+2]=c; }
        else          { rgb[rgbOff] = c; rgb[rgbOff+1]=0; rgb[rgbOff+2]=x; }
    }

    static void hue2rgb(float h, float h1, float c, float x, float[] rgb, int rgbOff) {
        //if (h==0)     { rgba[0] = 0; rgba[1]=0; rgba[2]=0; }
        if (h1<1)     { rgb[rgbOff] = c; rgb[rgbOff+1]=x; rgb[rgbOff+2]=0; }
        else if (h1<2){ rgb[rgbOff] = x; rgb[rgbOff+1]=c; rgb[rgbOff+2]=0; }
        else if (h1<3){ rgb[rgbOff] = 0; rgb[rgbOff+1]=c; rgb[rgbOff+2]=x; }
        else if (h1<4){ rgb[rgbOff] = 0; rgb[rgbOff+1]=x; rgb[rgbOff+2]=c; }
        else if (h1<5){ rgb[rgbOff] = x; rgb[rgbOff+1]=0; rgb[rgbOff+2]=c; }
        else          { rgb[rgbOff] = c; rgb[rgbOff+1]=0; rgb[rgbOff+2]=x; }
    }

    @Override
    public Transform getTranform(ColorSpace cs) {
        if (SRGB.INSTANCE.equals(cs)) return spaceToRgb;
        return super.getTranform(cs);
    }

}
