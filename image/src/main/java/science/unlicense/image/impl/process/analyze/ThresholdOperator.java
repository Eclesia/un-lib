
package science.unlicense.image.impl.process.analyze;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.geometry.api.tuple.TupleSpace;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.AbstractPointOperator;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.image.impl.process.ConvolutionMatrix;

/**
 * Binary threshold of an image.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public class ThresholdOperator extends AbstractPointOperator {

    public static final FieldType INPUT_THRESHOLD = new FieldTypeBuilder(new Chars("threshold")).title(new Chars("Input threshold")).valueClass(ConvolutionMatrix.class).build();

    public static DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("threshold"), new Chars("Threshold"), Chars.EMPTY, ThresholdOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_THRESHOLD},
                new FieldType[]{OUTPUT_IMAGE});

    private double limit;

    public ThresholdOperator() {
        super(DESCRIPTOR);
    }

    @Override
    protected void prepareInputs() {
        super.prepareInputs();
        limit = (Double) inputParameters.getPropertyValue(INPUT_THRESHOLD.getId());
    }

    @Override
    protected double evaluate(double value) {
        return (value<limit) ? 0 : 1;
    }

    public static TupleSpace transform(TupleSpace space, double limit) {
        throw new UnsupportedOperationException("Not supported.");
    }

}
