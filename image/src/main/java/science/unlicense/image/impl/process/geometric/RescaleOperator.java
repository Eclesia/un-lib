

package science.unlicense.image.impl.process.geometric;

import science.unlicense.common.api.character.Chars;
import science.unlicense.task.api.AbstractTask;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.image.api.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class RescaleOperator extends AbstractTask {

    public static final FieldType INPUT_TARGET_EXTENT = new FieldTypeBuilder(new Chars("Extent")).title(new Chars("Input target image extent")).valueClass(Extent.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("rescale"), new Chars("rescale"), Chars.EMPTY, RescaleOperator.class,
                new FieldType[]{INPUT_IMAGE, INPUT_TARGET_EXTENT},
                new FieldType[]{OUTPUT_IMAGE});

    public RescaleOperator() {
        super(DESCRIPTOR);
    }

    public Document perform() {
        final Image inputImage = (Image) inputParameters
                .getPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final Extent outputExtent = (Extent) inputParameters
                .getPropertyValue(INPUT_TARGET_EXTENT.getId());

        final Image outputImage = execute(inputImage, outputExtent);

        outputParameters.setPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),outputImage);
        return outputParameters;
    }

    public static Image execute(Image inputImage, Extent outputExtent) {

        //TODO support Nd transforms
        final Affine2 trs = new Affine2();
        trs.setToIdentity();

        final double scalex = inputImage.getExtent().getL(0) / outputExtent.get(0);
        final double scaley = inputImage.getExtent().getL(1) / outputExtent.get(1);
        trs.set(0, 0, scalex);
        trs.set(1, 1, scaley);

        return TransformOperator.execute(inputImage, trs, outputExtent);

    }

}
