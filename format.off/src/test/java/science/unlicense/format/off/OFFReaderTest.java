
package science.unlicense.format.off;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 *
 * @author Johann Sorel
 */
public class OFFReaderTest {

    @Test
    public void testRead() throws Exception{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/off/cube.off"));

        final OFFStore store = new OFFStore(path);

        final Collection elements = store.getElements();
        Assert.assertEquals(1, elements.getSize());

        final Object obj = elements.createIterator().next();
        Assert.assertTrue(obj instanceof DefaultModel);
        final DefaultModel mesh = (DefaultModel) obj;

        final DefaultMesh shell = (DefaultMesh) mesh.getShape();

        //TODO


    }

}
