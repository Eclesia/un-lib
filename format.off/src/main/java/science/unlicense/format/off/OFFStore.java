
package science.unlicense.format.off;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.Techniques;
import static science.unlicense.format.off.OFFConstants.*;

/**
 *
 * @author Johann Sorel
 */
public class OFFStore extends AbstractModel3DStore {

    /** file vertices : Vector 3 */
    public VectorRW[] vertexCoord;
    /** file faces */
    public OFFFace[] faces;

    public OFFStore(Object input) {
        super(OFFFormat.INSTANCE,input);
    }

    public Collection getElements() throws StoreException {

        if (vertexCoord==null){
            try {
                read();
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }

        final Collection col = new ArraySequence();
        col.add(rebuildMesh());
        return col;
    }

    private void read() throws IOException{
        final ByteInputStream stream = getSourceAsInputStream();
        final CharInputStream charStream = new CharInputStream(stream,CharEncodings.US_ASCII);

        int vinc = 0;
        int finc = 0;
        Chars cs = null;
        while ( (cs=charStream.readLine()) != null){
            cs = cs.trim();
            if (cs.isEmpty() || cs.startsWith(COMMENT) || cs.startsWith(SIGNATURE)){
                continue;
            }

            if (vertexCoord==null){
                //this is the sizes line, contain number of vertice, number of face, number of edge
                Chars[] parts = cs.split(' ');
                int nbVertice = Int32.decode(parts[0]);
                int nbFace = Int32.decode(parts[1]);
                vertexCoord = new VectorRW[nbVertice];
                faces = new OFFFace[nbFace];
            } else if (vinc<vertexCoord.length){
                vertexCoord[vinc] = VectorNf64.createDouble(3);
                for (int i=0;i<3;i++){
                    int off = cs.getFirstOccurence(' ');
                    if (off<0) off = cs.getCharLength();
                    Chars txt = cs.truncate(0, off);
                    vertexCoord[vinc].set(i, Float64.decode(txt));
                    cs = cs.truncate(off, cs.getCharLength()).trim();
                }
                vinc++;
            } else if (finc<faces.length){
                faces[finc] = new OFFFace();
                int off = cs.getFirstOccurence(' ');
                    if (off<0) off = cs.getCharLength();
                Chars txt = cs.truncate(0, off);
                faces[finc].vertexIdx = new int[Int32.decode(txt)];
                cs = cs.truncate(off, cs.getCharLength()).trim();

                for (int i=0;i<3;i++){
                    off = cs.getFirstOccurence(' ');
                    if (off<0) off = cs.getCharLength();
                    txt = cs.truncate(0, off);
                    faces[finc].vertexIdx[i] = Int32.decode(txt);
                    cs = cs.truncate(off, cs.getCharLength()).trim();
                }
                finc++;
            }
        }
    }

    private DefaultModel rebuildMesh(){
        final DefaultModel mesh = new DefaultModel();
        mesh.getTechniques().add(new SimpleBlinnPhong());

        //count number of triangles
        //some files contain quad or polygons in face definition
        int nbPoint=0;
        int nbIndices=0;
        for (int i=0,n=faces.length;i<n;i++){
            final OFFFace face = faces[i];
            nbPoint += face.vertexIdx.length;
            nbIndices += (face.vertexIdx.length-2)*3;
        }

        //prepare buffers
        final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(nbPoint*3).cursor();
        final Int32Cursor indices = DefaultBufferFactory.INSTANCE.createInt32(nbIndices).cursor();
        final VBO meshVertex = new VBO(vertices.getBuffer(),3);
        final IBO meshIndice = new IBO(indices.getBuffer());

        int indice = 0;
        for (int i=0,n=faces.length;i<n;i++){
            final OFFFace face = faces[i];

            final int[] temp = new int[face.vertexIdx.length];
            for (int k=0;k<temp.length;k++){
                temp[k] = indice++;
            }

            for (int k=2,kn=face.vertexIdx.length;k<kn;k++){
                final boolean firstTriangle = k==2;
                final Tuple v1 = vertexCoord[face.vertexIdx[0  ]];
                final Tuple v2 = vertexCoord[face.vertexIdx[k-1]];
                final Tuple v3 = vertexCoord[face.vertexIdx[k  ]];
                if (firstTriangle){
                    vertices.write(v1.toFloat());
                    vertices.write(v2.toFloat());
                }
                vertices.write(v3.toFloat());

                indices.write(temp[0  ]);
                indices.write(temp[k-1]);
                indices.write(temp[k  ]);
            }
        }

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(meshVertex);
        shell.setIndex(meshIndice);
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, meshIndice.getCapacity())});
        mesh.setShape(shell);
        Mesh.calculateNormals(shell);
        mesh.updateBoundingBox();

        Techniques.styleCAD(mesh);

        return mesh;
    }

}
