
package science.unlicense.format.off;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Object File Format.
 * Old 3D model format, only contain vertices and faces.
 *
 * resource :
 * http://en.wikipedia.org/wiki/OFF_%28file_format%29
 * http://pgl10.chez.com/format_off.html
 *
 * @author Johann Sorel
 */
public class OFFFormat extends AbstractModel3DFormat {

    public static final OFFFormat INSTANCE = new OFFFormat();

    private OFFFormat() {
        super(new Chars("OFF"));
        shortName = new Chars("OFF");
        longName = new Chars("OFF");
        extensions.add(new Chars("off"));
    }

    public Store open(Object input) throws IOException {
        return new OFFStore(input);
    }

}
