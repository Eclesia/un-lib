
package science.unlicense.format.off;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class OFFConstants {

    public static final Chars SIGNATURE = Chars.constant(new byte[]{'O','F','F'});
    public static final Chars COMMENT = Chars.constant(new byte[]{'#'});

    private OFFConstants(){}

}
