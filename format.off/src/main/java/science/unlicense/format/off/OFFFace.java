
package science.unlicense.format.off;


/**
 *
 * @author Johann Sorel
 */
public class OFFFace {

    /**
     * sequence of int[], face is composed of 3 to N vertices
     */
    public int[] vertexIdx;

}
