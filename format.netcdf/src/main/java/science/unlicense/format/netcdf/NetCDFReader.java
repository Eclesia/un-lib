
package science.unlicense.format.netcdf;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int16;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int8;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import static science.unlicense.format.netcdf.NetCDFMetaModel.*;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import static science.unlicense.image.api.ImageSetMetadata.*;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 * NetCDF reader.
 * Specification : http://www.unidata.ucar.edu/software/netcdf/docs/netcdf/File-Format-Specification.html
 *
 * @author Johann Sorel
 */
public class NetCDFReader extends AbstractImageReader{

    private TypedNode mdNetCDF = null;
    private TypedNode mdImageSet = null;

    @Override
    protected Dictionary readMetadatas(final BacktrackInputStream stream) throws IOException {
        final DataInputStream ds = new DataInputStream(stream);
        final byte[] signature = new byte[3];
        ds.readFully(signature);

        final String str = new String(signature);
        if (!str.equals("CDF")){
            throw new IOException(ds, "Stream is not a NetCDF.");
        }

        mdNetCDF = new DefaultTypedNode(MD_NETCDF);
        final int version = ds.readUByte();
        mdNetCDF.addChild(MD_NETCDF_VERSION, version);
        mdNetCDF.addChild(MD_NETCDF_NBREC, ds.readInt());

        //parse dimensions
        final TypedNode[] dimensions = readDimensions(ds);
        //parse global attributes
        final TypedNode[] gatts = readAttributs(ds);
        //parse variables
        final TypedNode[] vars = readVariables(version,dimensions,ds);

        mdNetCDF.getChildren().addAll(dimensions);
        mdNetCDF.getChildren().addAll(gatts);
        mdNetCDF.getChildren().addAll(vars);

        //rebuild the standard image metamodel
        mdImageSet = new DefaultTypedNode(MD_IMAGESET);
        for (int i=0;i<vars.length;i++){
            final TypedNode var = vars[i];
            final TypedNode mdImage = new DefaultTypedNode(MD_IMAGE);

            //add band type
            final int ncType = (Integer) var.getChild(MD_VARIABLE_TYPE).getValue();
            final NumberType sampleType = toSampleType(ncType);
            final TypedNode mdband= new DefaultTypedNode(MD_IMAGE_BAND);
            mdband.addChild(MD_IMAGE_BAND_TYPE, sampleType);
            mdImage.getChildren().add(mdband);

            //add dimensions
            final TypedNode[] vardims = var.getChildren(MD_DIMENSION);
            final int dimsize;
            if (ncType == NC_CHAR){
                //last dimension is the size of the char sequence, we don't count it
                dimsize = vardims.length-1;
            } else {
                dimsize = vardims.length;
            }
            for (int k=0;k<dimsize;k++){
                final TypedNode dim = vardims[k];
                final TypedNode mddim = new DefaultTypedNode(MD_IMAGE_DIMENSION);
                mddim.addChild(MD_IMAGE_DIMENSION_ID, dim.getChild(MD_DIMENSION_NAME).getValue());
                mddim.addChild(MD_IMAGE_DIMENSION_EXTEND, dim.getChild(MD_DIMENSION_SIZE).getValue());
                mdImage.getChildren().add(mddim);
            }

            mdImageSet.getChildren().add(mdImage);
        }

        final Dictionary metas = new HashDictionary();
        metas.add(mdNetCDF.getType().getId(), mdNetCDF);
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    @Override
    protected Image read(final ImageReadParameters parameters, final BacktrackInputStream stream) throws IOException {
        if (mdNetCDF == null){
            //read metas
            readMetadatas(stream);
            stream.rewind();
        }

        //get variable definition
        final int varIndex = parameters.getImageIndex();
        final TypedNode mdImage = mdImageSet.getChildren(MD_IMAGE)[varIndex];
        final TypedNode mdVar = mdNetCDF.getChildren(MD_VARIABLE)[varIndex];
        final long offset = (Long) mdVar.getChild(MD_VARIABLE_BEGIN).getValue();
        final int nctype = (Integer) mdVar.getChild(MD_VARIABLE_TYPE).getValue();
        final int size = (Integer) mdVar.getChild(MD_VARIABLE_SIZE).getValue();
        final NumberType sampleType = toSampleType(nctype);
        final TypedNode[] mdDims = mdVar.getChildren(MD_DIMENSION);

        //move to location and read datas
        final DataInputStream vds = new DataInputStream(stream);
        vds.skip(offset);

        final Buffer bank = parameters.getBufferFactory().createInt8(size).getBuffer();
        vds.readFully(bank);

        //build sample model
        final ImageModel sm;
        if (nctype == NC_CHAR){
            //TODO special sample model needed
            throw new IOException(stream, "Sample model of char sequence not implemented yet.");
        } else {
            sm = new InterleavedModel(new UndefinedSystem(1), sampleType);
        }

        final Extent.Long bbox = new Extent.Long(mdDims.length);
        for (int i=0;i<mdDims.length;i++){
            bbox.set(i,(Integer) mdDims[i].getChild(MD_DIMENSION_SIZE).getValue());
        }

        //TODO grayscale one band color model needed
        final ImageModel cm = null;

        return new DefaultImage(bank,bbox,sm,cm);
    }

    private static NumberType toSampleType(int ncType) throws IOException{
        if (ncType == NC_BYTE){
            return Int8.TYPE;
        } else if (ncType == NC_CHAR){
            return NumberType.TYPE_UNKNOWNED;
        } else if (ncType == NC_SHORT){
            return Int16.TYPE;
        } else if (ncType == NC_INT){
            return Int32.TYPE;
        } else if (ncType == NC_FLOAT){
            return Float32.TYPE;
        } else if (ncType == NC_DOUBLE){
            return Float64.TYPE;
        } else {
            throw new IOException(null, "Unknowned NC_TYPE = "+ncType);
        }
    }

    private static TypedNode[] readDimensions(final DataInputStream ds) throws IOException{
        final int dimDefined = ds.readInt();
        final int nbDim = ds.readInt();
        final TypedNode[] dimensions = new TypedNode[nbDim];

        if (dimDefined == NC_DIMENSION){
            for (int i=0;i<nbDim;i++){
                final DefaultTypedNode node = new DefaultTypedNode(MD_DIMENSION);
                node.addChild(MD_DIMENSION_NAME, readString(ds));
                node.addChild(MD_DIMENSION_SIZE, ds.readInt());
                dimensions[i] = node;
            }
        }

        return dimensions;
    }

    private static TypedNode[] readAttributs(final DataInputStream ds) throws IOException{
        final int attDefined = ds.readInt();
        final int nb = ds.readInt();
        final TypedNode[] atts = new TypedNode[nb];

        if (attDefined == NC_ATTRIBUTE){
            for (int i=0;i<nb;i++){
                final DefaultTypedNode node = new DefaultTypedNode(MD_ATTRIBUTE);
                node.addChild(MD_ATTRIBUTE_NAME, readString(ds));
                final int type = ds.readInt();
                final int nbele = ds.readInt();
                final Object[] value = readValues(type, 1, nbele, ds);
                node.addChild(MD_ATTRIBUTE_TYPE, type);
                node.addChild(MD_ATTRIBUTE_VALUE, value);
                atts[i] = node;
            }
        }

        return atts;
    }

    private static TypedNode[] readVariables(final int version,
            final TypedNode[] dimensions, final DataInputStream ds) throws IOException{

        final int varDefined = ds.readInt();
        final int nbVar = ds.readInt();
        final TypedNode[] vars = new TypedNode[nbVar];
        if (varDefined == NC_VARIABLE){
            for (int i=0;i<nbVar;i++){
                final DefaultTypedNode node = new DefaultTypedNode(MD_VARIABLE);
                node.addChild(MD_VARIABLE_NAME, readString(ds));

                final int nbdim = ds.readInt();
                for (int k=0;k<nbdim;k++){
                    node.getChildren().add(dimensions[ds.readInt()]);
                }
                node.getChildren().addAll(readAttributs(ds));
                node.addChild(MD_VARIABLE_TYPE, ds.readInt());
                node.addChild(MD_VARIABLE_SIZE, ds.readInt());

                if (version == 1){
                    node.addChild(MD_VARIABLE_BEGIN, ds.readUInt());
                } else if (version == 2){
                    node.addChild(MD_VARIABLE_BEGIN, ds.readLong());
                } else {
                    throw new IOException(ds, "Unknowned version : "+version);
                }
                vars[i] = node;
            }
        }

        return vars;
    }

    private static Object[] readValues(final int type, final int nb,
            final int charLength, final DataInputStream ds) throws IOException{
        final Object[] array = new Object[nb];
        if (type == NC_BYTE){
            for (int i=0;i<nb;i++){
                array[i] = ds.readByte();
            }
            int padding = nb % 4;
            if (padding>0){
                ds.readFully(new byte[4-padding]);
            }
        } else if (type == NC_CHAR){
            final byte[] buffer = new byte[charLength];
            for (int i=0;i<nb;i++){
                ds.readFully(buffer);
                array[i] = new String(buffer);
            }
            int padding = (nb*charLength) % 4;
            if (padding>0){
                ds.readFully(new byte[4-padding]);
            }

        } else if (type == NC_SHORT){
            for (int i=0;i<nb;i++){
                array[i] = ds.readShort();
            }
            int padding = (nb*2) % 4;
            if (padding>0){
                ds.readFully(new byte[4-padding]);
            }
        } else if (type == NC_INT){
            for (int i=0;i<nb;i++){
                array[i] = ds.readInt();
            }
        } else if (type == NC_FLOAT){
            for (int i=0;i<nb;i++){
                array[i] = ds.readFloat();
            }
        } else if (type == NC_DOUBLE){
            for (int i=0;i<nb;i++){
                array[i] = ds.readDouble();
            }
        } else {
            throw new IOException(ds, "Unknowned type :"+type);
        }

        return array;
    }

    private static String readString(final DataInputStream ds) throws IOException {
        final int length = ds.readInt();
        final byte[] buffer = new byte[length];
        ds.readFully(buffer);

        int padding = length % 4;
        if (padding>0){
            ds.readFully(new byte[4-padding]);
        }

        return new String(buffer, 0, length);
    }

}
