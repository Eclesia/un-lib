
package science.unlicense.format.netcdf;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * @author Johann Sorel
 */
public class NetCDFFormat extends AbstractImageFormat {

    public static final NetCDFFormat INSTANCE = new NetCDFFormat();

    private NetCDFFormat() {
        super(new Chars("netcdf"));
        shortName = new Chars("NetCDF");
        longName = new Chars("Network Common Data Form");
        mimeTypes.add(new Chars("application/netcdf"));
        mimeTypes.add(new Chars("application/x-netcdf"));
        extensions.add(new Chars("nc"));
        extensions.add(new Chars("cdf"));
        signatures.add(NetCDFMetaModel.SIGNATURE);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new NetCDFStore(this, source);
    }

}
