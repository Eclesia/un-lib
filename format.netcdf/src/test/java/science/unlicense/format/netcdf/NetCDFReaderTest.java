package science.unlicense.format.netcdf;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import science.unlicense.image.api.ImageReader;

/**
 *
 * @author Johann Sorel
 */
public class NetCDFReaderTest {

    private static final float DELTA = 0.000000001f;
    @Test
    public void testSample() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/netcdf/testrh.nc")).createInputStream();

        final ImageReader reader = new NetCDFReader();
        reader.setInput(bs);

        //check metadatas
        final Chars[] names = reader.getMetadataNames();
        Assert.assertEquals(2, names.length);
        Assert.assertEquals(new Chars("netcdf"),names[0]);
        Assert.assertEquals(new Chars("imageset"),names[1]);

        final ImageReadParameters params = reader.createParameters();
        final Image image = reader.read(params);
        Assert.assertEquals(1, image.getExtent().getDimension());
        Assert.assertEquals(10000, image.getExtent().getL(0));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final TupleGridCursor cursor = sm.cursor();

        //values are floats
        cursor.moveTo(0);
        Assert.assertArrayEquals(new float[]{420f}, cursor.samples().toFloat(), DELTA);
        cursor.moveTo(1);
        Assert.assertArrayEquals(new float[]{197}, cursor.samples().toFloat(), DELTA);
        cursor.moveTo(9999);
        Assert.assertArrayEquals(new float[]{444}, cursor.samples().toFloat(), DELTA);

    }

}
