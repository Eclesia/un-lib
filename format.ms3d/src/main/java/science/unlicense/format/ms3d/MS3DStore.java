
package science.unlicense.format.ms3d;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author Johann Sorel
 */
public class MS3DStore extends AbstractModel3DStore{

    public Path base;

    //header
    public int version;

    //mesh
    public MS3DVertex[] vertices;
    public MS3DTriangle[] triangles;
    public MS3DGroup[] groups;
    public MS3DMaterial[] materials;

    //animation
    public float fAnimationFPS;
    public float fCurrentTime;
    public int iTotalFrames;
    public MS3DJoint[] joints;

    //comment extension
    public int commentSubversion;
    public Chars comment;

    //vertex extension
    public int vertexSubversion;

    //joint extension
    public int jointSubversion;

    //model extension
    public int modelSubversion;
    // joint size, since subVersion == 1
    public float jointSize;
    // 0 = simple, 1 = depth buffered with alpha ref, 2 = depth sorted triangles, since subVersion == 1
    public int transparencyMode;
    // alpha reference value for transparencyMode = 1, since subVersion == 1
    public float alphaRef;


    public MS3DStore(Object input) {
        super(MS3DFormat.INSTANCE,input);
    }

    @Override
    public Collection getElements() {
        final MotionModel root = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);

        // Mesh Transformation:
        // 0. Build the transformation matrices from the rotation and position
        // 1. Multiply the vertices by the inverse of local reference matrix (lmatrix0)
        // 2. then translate the result by (lmatrix0 * keyFramesTrans)
        // 3. then multiply the result by (lmatrix0 * keyFramesRot)
        // For normals skip step 2.

        //TODO work by group to create part mesh rather then triangle
        for (int i=0;i<groups.length;i++){
            final MS3DGroup group = groups[i];
            final DefaultModel mesh = buildGroup(group);
            root.getChildren().add(mesh);
        }

        final Collection col = new ArraySequence();
        col.add(root);
        return col;
    }

    private DefaultModel buildGroup(final MS3DGroup group){
        final DefaultModel mesh = new DefaultModel();
        mesh.getTechniques().add(new SimpleBlinnPhong());
        final Float32Cursor vertexBuffer = DefaultBufferFactory.INSTANCE.createFloat32(group.numtriangles*3*3).cursor();
        final Float32Cursor normalBuffer = DefaultBufferFactory.INSTANCE.createFloat32(group.numtriangles*3*3).cursor();
        final Int32Cursor indexBuffer = DefaultBufferFactory.INSTANCE.createInt32(group.numtriangles*3).cursor();
        final Float32Cursor uvBuffer = DefaultBufferFactory.INSTANCE.createFloat32(group.numtriangles*3*2).cursor();

        int k=-1;
        for (int i=0;i<group.numtriangles;i++){
            final MS3DTriangle triangle = triangles[group.triangleIndices[i]];
            final MS3DVertex v0 = vertices[triangle.vertexIndices[0]];
            final MS3DVertex v1 = vertices[triangle.vertexIndices[1]];
            final MS3DVertex v2 = vertices[triangle.vertexIndices[2]];
            vertexBuffer.write(v0.position.toFloat());
            vertexBuffer.write(v1.position.toFloat());
            vertexBuffer.write(v2.position.toFloat());
            normalBuffer.write(triangle.normals[0].toFloat());
            normalBuffer.write(triangle.normals[1].toFloat());
            normalBuffer.write(triangle.normals[2].toFloat());
            indexBuffer.write(++k);
            indexBuffer.write(++k);
            indexBuffer.write(++k);
            uvBuffer.write(triangle.s[0]);
            uvBuffer.write(triangle.t[0]);
            uvBuffer.write(triangle.s[1]);
            uvBuffer.write(triangle.t[1]);
            uvBuffer.write(triangle.s[2]);
            uvBuffer.write(triangle.t[2]);
        }

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(new VBO(vertexBuffer.getBuffer(),3));
        shell.setNormals(new VBO(normalBuffer.getBuffer(),3));
        shell.setIndex(new IBO(indexBuffer.getBuffer()));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) indexBuffer.getBuffer().getNumbersSize())});
        shell.setUVs(new VBO(uvBuffer.getBuffer(), 2));

        //build the material
        final SimpleBlinnPhong.Material mat = SimpleBlinnPhong.newMaterial();
        mesh.getMaterials().add(mat);
        try{
            final MS3DMaterial material = materials[group.materialIndex];
            final Image image = Images.read(base.resolve(material.texture));
            mat.setDiffuseTexture(new TextureMapping(new Texture2D(image)));
        }catch(IOException ex){
            ex.printStackTrace();
        }

        mesh.setShape(shell);
        mesh.updateBoundingBox();
        return mesh;
    }

}
