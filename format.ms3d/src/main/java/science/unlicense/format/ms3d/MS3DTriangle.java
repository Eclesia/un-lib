
package science.unlicense.format.ms3d;

import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class MS3DTriangle {

    // SELECTED | SELECTED2 | HIDDEN
    public int flags;
    // int 3
    public int[] vertexIndices;
    //3 float 3
    public VectorRW[] normals;
    //3
    public float[] s;
    //3
    public float[] t;
    // 1 - 32
    public byte smoothingGroup;
    public byte groupIndex;

}
