
package science.unlicense.format.ms3d;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class MS3DFormat extends AbstractModel3DFormat{

    public static final MS3DFormat INSTANCE = new MS3DFormat();

    private MS3DFormat() {
        super(new Chars("MS3D"));
        shortName = new Chars("MS3D");
        longName = new Chars("MilkShape 3D");
        extensions.add(new Chars("ms3d"));
        signatures.add(MS3DConstants.SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        final MS3DStore store = new MS3DStore(input);
        final MS3DReader reader = new MS3DReader();
        reader.read(store);
        return store;
    }

}
