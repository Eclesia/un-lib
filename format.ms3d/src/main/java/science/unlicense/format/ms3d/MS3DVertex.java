
package science.unlicense.format.ms3d;

import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class MS3DVertex {

    // SELECTED | SELECTED2 | HIDDEN
    public byte flags;
    // 3 floats
    public VectorRW position;
    // -1 = no bone
    public byte boneId;
    public byte referenceCount;

    //vertex extension
    // index of joint or -1, if -1, then that weight is ignored, since subVersion 1, size 3 byte
    public int[] boneIds;
    // vertex weight ranging from 0 - 255, last weight is computed by 1.0 - sum(all weights), since subVersion 1, size 3
    // weight[0] is the weight for boneId in ms3d_vertex_t
    // weight[1] is the weight for boneIds[0]
    // weight[2] is the weight for boneIds[1]
    // 1.0f - weight[0] - weight[1] - weight[2] is the weight for boneIds[2] , size 3 byte
    public int[] weights;
    // vertex extra, which can be used as color or anything else, since subVersion 2, unsigned int
    public long extra;
}
