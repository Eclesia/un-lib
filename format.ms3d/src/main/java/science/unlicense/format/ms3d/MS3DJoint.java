
package science.unlicense.format.ms3d;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class MS3DJoint {
    // SELECTED | DIRTY
    public byte  flags;
    //32 bytes
    public Chars name;
    //32 bytes
    public Chars parentName;
    // local reference matrix , size 3
    public VectorRW rotation;
    public VectorRW position;

    //2 bytes
    public int numKeyFramesRot;
    //2 bytes
    public int numKeyFramesTrans;

    // local animation matrices, size = numKeyFramesRot
    public RotationFrame[] keyFramesRot;
    // local animation matrices, size = numKeyFramesTrans
    public PositionFrame[] keyFramesTrans;

    //comment
    public Chars comment;

    //extension, color
    public float[] rgb;

    public static class RotationFrame{
        public float time;
        public VectorRW rotation;
    }

    public static class PositionFrame{
        public float time;
        public VectorRW position;
    }

}
