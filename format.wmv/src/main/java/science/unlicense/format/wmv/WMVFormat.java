

package science.unlicense.format.wmv;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaCapabilities;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaCapabilities;

/**
 *
 * @author Johann Sorel
 */
public class WMVFormat extends AbstractMediaFormat {

    public static final WMVFormat INSTANCE = new WMVFormat();

    public WMVFormat() {
        super(new Chars("wmv"));
        shortName = new Chars("WMV");
        longName = new Chars("Windows Media Video");
        mimeTypes.add(new Chars("video/x-ms-wmv"));
        extensions.add(new Chars("wmv"));
        resourceTypes.add(Media.class);
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    @Override
    public Store open(Object input) throws IOException {
        throw new RuntimeException("not yet");
    }

}
