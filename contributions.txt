This list holds the dates, names and a short description of all direct external contributions.
(direct is the way the author sended code or explicitly gives his authorisation to harvest/copy from his ressources)

- 23/03/2014 by Thibaut Cuvelier
PBKDF2 Cryptography
https://bitbucket.org/Eclesia/unlicense/issue/1/pbkdf2-algorithm

- 11/09/2013 by Florant Humbert
permission granted to derivate code from project Millie on developpez.com
http://projets.developpez.com/projects/millie/
multiple image processing algorithms.

- 06/05/2013 by Thibaut Cuvelier
Several sorting code, and a binary search algorithm

- 27/04/2013 by Xavier Philippeau
Give the rigth to copy all his algorithms published on developpez.com
mainly image processing : http://xphilipp.developpez.com

X REMOVED original code was not his property
X - 31/03/2013 by Matteo Hausner
X Inversion matrix algprithm (originaly in c++ from http://code.google.com/p/eq-bsp)
X permission granted to copy inverse matrix code as public domain.

- 16/02/2013 by Yann D'Isanto
Arrays utils , Collections, predicate api


