
package science.unlicense.db.postgresql.wktraster;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.ImageWriter;

/**
 *
 * Specification :
 * https://trac.osgeo.org/postgis/wiki/WKTRaster/Documentation01
 *
 * @author Johann Sorel
 */
public class WKTRasterFormat extends AbstractImageFormat {

    public WKTRasterFormat() {
        super(new Chars("wktraster"));
        shortName = new Chars("wktraster");
        longName = new Chars("wktraster");
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new WKTRasterReader();
    }

    public ImageWriter createWriter() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
