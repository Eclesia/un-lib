
package science.unlicense.db.postgresql.message;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.FormatEncodingException;

/**
 *
 * Message : Terminate(F)
 *
 * @author Johann Sorel
 */
public class Terminate extends Message {

    public static final int CODE = 'X';

    public void read(DataInputStream ds) throws IOException, IOException {
        int size = ds.readInt();
        if (size != 4) throw new FormatEncodingException(ds, "Expected message size 4, but was "+size);
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeInt(4);
    }

}
