
package science.unlicense.db.postgresql.message;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : CopyBothResponse(B)
 *
 * @author Johann Sorel
 */
public class CopyBothResponse extends Message {

    public static final int CODE = 'W';

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
