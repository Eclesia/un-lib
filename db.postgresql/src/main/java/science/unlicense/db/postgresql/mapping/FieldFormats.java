
package science.unlicense.db.postgresql.mapping;

import science.unlicense.db.postgresql.message.RowDescription;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public final class FieldFormats {

    private static final FieldFormat[] FORMATS = new FieldFormat[]{
        CommonFormats._INT2,
        CommonFormats._INT4,
        CommonFormats._INT8,
        CommonFormats._FLOAT8,
        CommonFormats._TEXT,
        CommonFormats._VARCHAR,
        PostgisFormats._GEOMETRY
    };

    private FieldFormats(){}

    public static FieldFormat getFormat(RowDescription.Field field) throws IOException {
        for (FieldFormat ff : FORMATS) {
            if (ff.canDecode(field)) {
                return ff;
            }
        }
        throw new RuntimeException("Unexpected type : " + field.dataTypeId);

    }

    public static Object decode(RowDescription.Field field, byte[] fieldData) throws IOException {
        if (fieldData == null) return null;
        final FieldFormat format = getFormat(field);
        return format.createDecoder(field).decode(field, fieldData);
    }

}
