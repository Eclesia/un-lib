
package science.unlicense.db.postgresql.message;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : DataRow(B)
 *
 * @author Johann Sorel
 */
public class DataRow extends Message {

    public static final int CODE = 'D';

    public byte[][] fields;

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        final int nbCol = ds.readUShort();
        fields = new byte[nbCol][0];
        for (int i=0; i<nbCol; i++) {
            final int fieldSize = ds.readInt();
            if (fieldSize != -1) {
                fields[i] = ds.readBytes(fieldSize);
            }
        }
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
