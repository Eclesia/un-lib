
package science.unlicense.db.postgresql;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.DocumentTypeBuilder;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.db.postgresql.mapping.FieldFormat;
import science.unlicense.db.postgresql.mapping.FieldFormats;
import science.unlicense.db.postgresql.message.Authentication;
import science.unlicense.db.postgresql.message.BackendKeyData;
import science.unlicense.db.postgresql.message.BindComplete;
import science.unlicense.db.postgresql.message.CloseComplete;
import science.unlicense.db.postgresql.message.CommandComplete;
import science.unlicense.db.postgresql.message.CopyBothResponse;
import science.unlicense.db.postgresql.message.CopyData;
import science.unlicense.db.postgresql.message.CopyDone;
import science.unlicense.db.postgresql.message.CopyInResponse;
import science.unlicense.db.postgresql.message.CopyOutResponse;
import science.unlicense.db.postgresql.message.DataRow;
import science.unlicense.db.postgresql.message.EmptyQueryResponse;
import science.unlicense.db.postgresql.message.ErrorResponse;
import science.unlicense.db.postgresql.message.FunctionCallResponse;
import science.unlicense.db.postgresql.message.Message;
import science.unlicense.db.postgresql.message.NegotiateProtocolVersion;
import science.unlicense.db.postgresql.message.NoData;
import science.unlicense.db.postgresql.message.NoticeResponse;
import science.unlicense.db.postgresql.message.NotificationResponse;
import science.unlicense.db.postgresql.message.ParameterDescription;
import science.unlicense.db.postgresql.message.ParameterStatus;
import science.unlicense.db.postgresql.message.ParseComplete;
import science.unlicense.db.postgresql.message.PortalSuspended;
import science.unlicense.db.postgresql.message.Query;
import science.unlicense.db.postgresql.message.ReadyForQuery;
import science.unlicense.db.postgresql.message.RowDescription;
import science.unlicense.db.postgresql.message.StartupMessage;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.system.ClientSocket;

/**
 *
 * @author Johann Sorel
 */
public class PostgresClient {

    private ClientSocket socket;
    private DataInputStream inDs;
    private DataOutputStream outDs;

    private Dictionary globalParameters = new HashDictionary();

    public PostgresClient() {
    }

    public void connect(Chars host, int port, Chars user, Chars password, Chars database) throws IOException, PostgresException {
        if (socket != null) {
            throw new IOException(null, "Client is already connected");
        }
        socket = science.unlicense.system.System.get().getSocketManager().createClientSocket(host, port);
        final ByteOutputStream bo = socket.getOutputStream();
        outDs = new DataOutputStream(bo,Endianness.BIG_ENDIAN);
        final ByteInputStream is = socket.getInputStream();
        inDs = new DataInputStream(is, Endianness.BIG_ENDIAN);

        //send startup message
        final StartupMessage msg = new StartupMessage();
        msg.parameters.add(StartupMessage.KEY_USER, user);
        msg.parameters.add(StartupMessage.KEY_DATABASE, database);
        msg.write(outDs);
        outDs.flush();

        //read messages until a ReadyForQuery
        for (;;) {
            final Message message = readMessage();

            if (message instanceof Authentication) {
                final Authentication auth = (Authentication) message;
                switch (auth.authType) {
                    case 0 : continue;
                    default : throw new IOException(socket, "Unsupported authentication "+auth.authType);
                }
            } else if (message instanceof ReadyForQuery) {
                break;
            } else if (message instanceof ParameterStatus) {
                final ParameterStatus ps = (ParameterStatus) message;
                globalParameters.add(ps.parameterName, ps.parameterValue);
                break;
            } else {
                throw new IOException(socket, "Unexpected message "+message);
            }
        }
    }

    public Sequence query(Chars request) throws IOException, PostgresException {
        final Query query = new Query();
        query.query = request;
        query.write(outDs);
        outDs.flush();

        RowDescription desc = null;
        FieldFormat.Decoder[] decoders = null;
        DocumentType docType = null;

        final Sequence seq = new ArraySequence();

        for (;;) {
            final Message message = readMessage();
            if (message instanceof RowDescription) {
                desc = (RowDescription) message;

                //create document type
                final DocumentTypeBuilder dtb = new DocumentTypeBuilder(new Chars("query"));
                decoders = new FieldFormat.Decoder[desc.fields.length];
                for (int i=0;i<desc.fields.length;i++) {
                    final FieldFormat format = FieldFormats.getFormat(desc.fields[i]);
                    decoders[i] = format.createDecoder(desc.fields[i]);
                    dtb.addField(desc.fields[i].name).valueClass(decoders[i].expectedType());
                }

            } else if (message instanceof DataRow) {
                final DataRow row = (DataRow) message;
                final Document doc = new DefaultDocument(docType);

                for (int i=0;i<desc.fields.length;i++) {
                    Object value = null;
                    if (row.fields[i] != null) {
                         value = decoders[i].decode(desc.fields[i], row.fields[i]);
                    }
                    doc.setPropertyValue(desc.fields[i].name, value);
                }
                seq.add(doc);

            } else if (message instanceof CommandComplete) {
                break;
            }
        }

        return seq;
    }

    private Message readMessage() throws IOException, PostgresException {
        final int code = inDs.read();
        final Message message;
        switch (code) {
            case Authentication.CODE : message = new Authentication(); break;
            case BackendKeyData.CODE : message = new BackendKeyData(); break;
            case BindComplete.CODE : message = new BindComplete(); break;
            case CloseComplete.CODE : message = new CloseComplete(); break;
            case CommandComplete.CODE : message = new CommandComplete(); break;
            case CopyData.CODE : message = new CopyData(); break;
            case CopyDone.CODE : message = new CopyDone(); break;
            case CopyInResponse.CODE : message = new CopyInResponse(); break;
            case CopyOutResponse.CODE : message = new CopyOutResponse(); break;
            case CopyBothResponse.CODE : message = new CopyBothResponse(); break;
            case DataRow.CODE : message = new DataRow(); break;
            case EmptyQueryResponse.CODE : message = new EmptyQueryResponse(); break;
            case FunctionCallResponse.CODE : message = new FunctionCallResponse(); break;
            case NegotiateProtocolVersion.CODE : message = new NegotiateProtocolVersion(); break;
            case NoData.CODE : message = new NoData(); break;
            case NoticeResponse.CODE : message = new NoticeResponse(); break;
            case NotificationResponse.CODE : message = new NotificationResponse(); break;
            case ParameterDescription.CODE : message = new ParameterDescription(); break;
            case ParameterStatus.CODE : message = new ParameterStatus(); break;
            case ParseComplete.CODE : message = new ParseComplete(); break;
            case PortalSuspended.CODE : message = new PortalSuspended(); break;
            case ReadyForQuery.CODE : message = new ReadyForQuery(); break;
            case RowDescription.CODE : message = new RowDescription(); break;
            case ErrorResponse.CODE : message = new ErrorResponse(); break;
            default: throw new IOException(socket, "Unexpected response code "+code);
        }
        message.read(inDs);

        if (message instanceof ErrorResponse) {
            throw PostgresException.forError((ErrorResponse) message);
        }
        return message;
    }

    public void disconnect() throws IOException {
        if (socket != null) {
            socket.close();
            socket = null;
            outDs = null;
            inDs = null;
        }
    }
}
