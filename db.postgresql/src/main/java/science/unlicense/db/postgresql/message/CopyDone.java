
package science.unlicense.db.postgresql.message;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : CopyDone(F&B)
 *
 * @author Johann Sorel
 */
public class CopyDone extends Message {

    public static final int CODE = 'c';

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(CODE);
        ds.writeInt(4);
    }

}
