
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : StartupMessage(F)
 *
 * @author Johann Sorel
 */
public class StartupMessage extends Message {

    public static final Chars KEY_USER = Chars.constant("user");
    public static final Chars KEY_DATABASE = Chars.constant("database");
    public static final Chars KEY_OPTIONS = Chars.constant("options");
    public static final Chars KEY_REPLICATION = Chars.constant("replication");

    public int majorVersion = 3;
    public int minorVersion = 0;
    public final Dictionary parameters = new HashDictionary();

    public void read(DataInputStream ds) throws IOException, IOException {
        final int size = ds.readInt();
        final long version = ds.readUInt();
        majorVersion = (int) (version >> 16);
        minorVersion = (int) (version & 0xFFFFl);

        for (;;) {
            final Chars key = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
            if (key.getByteLength() == 0) break;
            final Chars value = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
            parameters.add(key, value);
        }

    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream dds = new DataOutputStream(out, Endianness.BIG_ENDIAN);
        dds.writeInt(majorVersion << 16 | minorVersion);
        final Iterator ite = parameters.getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            final Chars key = (Chars) pair.getValue1();
            final Chars value = (Chars) pair.getValue2();
            dds.writeZeroTerminatedChars(key, 0, CharEncodings.UTF_8);
            dds.writeZeroTerminatedChars(value, 0, CharEncodings.UTF_8);
        }
        dds.writeUByte(0);

        final byte[] buffer = out.getBuffer().toArrayByte();
        ds.writeInt(buffer.length+4);
        ds.write(buffer);
    }

}
