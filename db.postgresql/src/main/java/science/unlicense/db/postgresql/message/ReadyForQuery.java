
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : ReadyForQuery(B)
 *
 * @author Johann Sorel
 */
public class ReadyForQuery extends Message {

    public static final int CODE = 'Z';

    public int transactionStatus;

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        transactionStatus = ds.readUByte();
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(CODE);
        ds.writeInt(5);
        ds.writeUByte(transactionStatus);
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("ReadyForQuery ");
        cb.append(Int32.encode(transactionStatus));
        return cb.toChars();
    }
}
