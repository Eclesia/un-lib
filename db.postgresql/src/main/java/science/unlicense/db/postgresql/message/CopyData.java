
package science.unlicense.db.postgresql.message;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : CopyData(F&B)
 *
 * @author Johann Sorel
 */
public class CopyData extends Message {

    public static final int CODE = 'd';

    public byte[] datas;

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        datas = ds.readBytes(size-4);
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(CODE);
        ds.writeInt(datas.length+4);
        ds.write(datas);
    }

}
