
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : Bind(F)
 *
 * @author Johann Sorel
 */
public class Bind extends Message {

    public static final int CODE = 'B';

    public Chars destPortal;
    public Chars sourceStmt;

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        destPortal = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
        sourceStmt = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
