
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : CommandComplete(B)
 *
 * @author Johann Sorel
 */
public class CommandComplete extends Message {

    public static final int CODE = 'C';

    public Chars command;

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        command = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(CODE);
        ds.writeInt(4+command.getByteLength()+1);
        ds.writeBlockZeroTerminatedChars(command, 0, CharEncodings.UTF_8);
    }

}
