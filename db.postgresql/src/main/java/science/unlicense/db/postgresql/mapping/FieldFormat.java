
package science.unlicense.db.postgresql.mapping;

import science.unlicense.db.postgresql.message.RowDescription;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface FieldFormat {

    boolean canDecode(RowDescription.Field field);

    Decoder createDecoder(RowDescription.Field field);

    public static interface Decoder {

        Class expectedType();

        Object decode(RowDescription.Field field, byte[] fieldData) throws IOException;

    }

}
