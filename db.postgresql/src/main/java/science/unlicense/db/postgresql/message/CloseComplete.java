
package science.unlicense.db.postgresql.message;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : CloseComplete(B)
 *
 * @author Johann Sorel
 */
public class CloseComplete extends Message {

    public static final int CODE = '3';

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(CODE);
        ds.writeInt(4);
    }

}
