
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : ParameterStatus(B)
 *
 * @author Johann Sorel
 */
public class ParameterStatus extends Message {

    public static final int CODE = 'S';

    public Chars parameterName;
    public Chars parameterValue;

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        parameterName = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
        parameterValue = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(CODE);
        ds.writeInt(4
                +parameterName.getByteLength()+1
                +parameterValue.getByteLength()+1);
        ds.writeZeroTerminatedChars(parameterName, 0, CharEncodings.UTF_8);
        ds.writeZeroTerminatedChars(parameterValue, 0, CharEncodings.UTF_8);
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("ParameterStatus ");
        cb.append(parameterName);
        cb.append("=");
        cb.append(parameterValue);
        return cb.toChars();
    }

}
