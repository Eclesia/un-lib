
package science.unlicense.db.postgresql.wktraster;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;

/**
 *
 * @author Johann Sorel
 */
public class WKTRasterReader extends AbstractImageReader {

    @Override
    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {

        final DataInputStream ds = new DataInputStream(stream);
        final int version = ds.readUShort();
        final int nbBand = ds.readUShort();
        final int width = ds.readUShort();
        final int height = ds.readUShort();
        final double pixelsizex = ds.readDouble();
        final double pixelsizey = ds.readDouble();
        final double upperleftx = ds.readDouble();
        final double upperlefty = ds.readDouble();
        final double rotationx = ds.readDouble();
        final double rotationy = ds.readDouble();
        final int srid = ds.readInt();

        for (int i=0;i<nbBand;i++) {
            int isOffline = ds.readBits(1);
            int hasNoDataValue = ds.readBits(1);
            int pixelType = ds.readBits(4);
            int noDataValue = ds.readBits(2);

        }


        throw new IOException(stream, "TODO");
    }


}
