
package science.unlicense.db.postgresql.message;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : SSLrequest(F)
 *
 * @author Johann Sorel
 */
public class SSLRequest extends Message {

    public int requestCode;

    public void read(DataInputStream ds) throws IOException, IOException {
        int size = ds.readInt();
        requestCode = ds.readInt();
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeInt(8);
        ds.writeInt(requestCode);
    }

}
