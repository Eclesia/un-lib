
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Messages :
 * - AuthenticationOk(B)
 * - AuthenticationKerberosV5(B)
 * - AuthenticationCleartextPassword(B)
 * - AuthenticationMD5Password(B)
 * - AuthenticationSCMCredential(B)
 * - AuthenticationGSS(B)
 * - AuthenticationSSPI(B)
 * - AuthenticationGSSContinue(B)
 * - AuthenticationSASL(B)
 * - AuthenticationSASLContinue(B)
 * - AuthenticationSASLFinal(B)
 *
 * @author Johann Sorel
 */
public class Authentication extends Message {

    public static final int CODE = 'R';
    /**
     * Type of authentication, defined avec length.
     * - 0 : Ok
     * - 2 : KerberosV5
     * - 3 : CleartextPassword
     * - 5 : MD5Password
     * - 6 : SCMCredential
     * - 7 : GSS
     * - 8 : GSSContinue
     * - 9 : SSPI
     * - 10 : SASL
     * - 11 : SASLContinue
     * - 12 : SASLFinal
     */
    public int authType;

    /**
     * Associated authentication value based on authentication type.
     * - 5 : Byte4 (The salt to use when encrypting the password.)
     * - 8 : Byten (GSSAPI or SSPI authentication data.)
     * - 10 : Sequence of Chars (Name of a SASL authentication mechanism.)
     * - 11 : Byten (SASL data, specific to the SASL mechanism being used.)
     * - 12 : Byten (SASL outcome "additional data", specific to the SASL mechanism being used.)
     */
    public Object value;

    @Override
    public void read(DataInputStream ds) throws IOException {
        final int length = ds.readInt();
        authType = ds.readInt();

        switch (authType) {
            case 5 : value = ds.readBytes(4); break;
            case 8 : value = ds.readBytes(length-8); break;
            case 10 : {
                Sequence seq = new ArraySequence();
                for (;;) {
                    Chars str = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
                    if (str.getCharLength() == 0) break;
                    seq.add(str);
                }
                value = seq;
            } break;
            case 11 : value = ds.readBytes(length-8); break;
            case 12 : value = ds.readBytes(length-8); break;
        }

    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("Authentication ");
        cb.append(Int32.encode(authType));
        cb.append(":");
        cb.append(value);
        return cb.toChars();
    }
}
