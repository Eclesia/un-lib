
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ErrorResponse extends Message {

    public static final int CODE = 'E';

    public static final int CODE_SEVERITY_LOCAL = 'S';
    public static final int CODE_SEVERITY_NOLOCAL = 'V';
    public static final int CODE_CODE = 'C';
    public static final int CODE_MESSAGE = 'M';
    public static final int CODE_DETAIL = 'D';
    public static final int CODE_HINT = 'H';
    public static final int CODE_POSITION = 'P';
    public static final int CODE_INTERNAL_POSITION = 'p';
    public static final int CODE_INTERNAL_QUERY = 'q';
    public static final int CODE_WHERE = 'W';
    public static final int CODE_SCHEMA = 's';
    public static final int CODE_TABLE = 't';
    public static final int CODE_COLUMN = 'c';
    public static final int CODE_DATATYPE = 'd';
    public static final int CODE_CONSTRAINT = 'n';
    public static final int CODE_FILE = 'F';
    public static final int CODE_LINE = 'L';
    public static final int CODE_ROUTINE = 'R';

    public final Dictionary fields = new HashDictionary();

    @Override
    public void read(DataInputStream ds) throws IOException {
        int size = ds.readInt(Endianness.LITTLE_ENDIAN);
        for (;;) {
            final int fieldCode = ds.readUByte();
            if (fieldCode==0) break;
            final Chars fieldValue = ds.readZeroTerminatedChars(-1, CharEncodings.UTF_8);
            fields.add(fieldCode, fieldValue);
            System.out.println(fieldValue);
        }
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("Error"), fields.getPairs());
    }

    public static Chars commonName(int code) {
        switch (code) {
            case CODE_SEVERITY_LOCAL : return new Chars("Severity localized");
            case CODE_SEVERITY_NOLOCAL : return new Chars("Severity");
            case CODE_CODE : return new Chars("Code");
            case CODE_MESSAGE : return new Chars("Message");
            case CODE_DETAIL : return new Chars("Detail");
            case CODE_HINT : return new Chars("Hint");
            case CODE_POSITION : return new Chars("Position");
            case CODE_INTERNAL_POSITION : return new Chars("Position internal");
            case CODE_INTERNAL_QUERY : return new Chars("Query internal");
            case CODE_WHERE : return new Chars("Where");
            case CODE_SCHEMA : return new Chars("Schema");
            case CODE_TABLE : return new Chars("Table");
            case CODE_COLUMN : return new Chars("Column");
            case CODE_DATATYPE : return new Chars("Data type");
            case CODE_CONSTRAINT : return new Chars("Constraint");
            case CODE_FILE : return new Chars("File");
            case CODE_LINE : return new Chars("Line");
            case CODE_ROUTINE : return new Chars("Routine");
            default : return Chars.EMPTY;
        }
    }
}
