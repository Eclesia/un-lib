
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : Query(F)
 *
 * @author Johann Sorel
 */
public class Query extends Message {

    public static final int CODE = 'Q';

    public Chars query;

    public void read(DataInputStream ds) throws IOException {
        int size = ds.readInt();
        query = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(CODE);
        ds.writeInt(4+query.getByteLength()+1);
        ds.writeZeroTerminatedChars(query, 0, CharEncodings.UTF_8);
    }

}
