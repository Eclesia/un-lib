
package science.unlicense.db.postgresql;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.db.postgresql.message.ErrorResponse;

/**
 *
 * @author Johann Sorel
 */
public class PostgresException extends Exception {

    private final ErrorResponse error;

    private PostgresException(Chars message, ErrorResponse error) {
        super(CObjects.toChars(message).toString());
        this.error = error;

    }

    public static PostgresException forError(ErrorResponse error) {
        final Chars message = (Chars) error.fields.getValue(ErrorResponse.CODE_MESSAGE);
        return new PostgresException(message, error);
    }
}
