
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : CopyFail(F)
 *
 * @author Johann Sorel
 */
public class CopyFail extends Message {

    public static final int CODE = 'f';

    public Chars message;

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        message = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(CODE);
        ds.writeInt(4+message.getByteLength()+1);
        ds.writeZeroTerminatedChars(message, 0, CharEncodings.UTF_8);
    }

}
