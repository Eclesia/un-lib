
package science.unlicense.db.postgresql.message;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : CancelRequest(F)
 *
 * @author Johann Sorel
 */
public class CancelRequest extends Message {

    public int cancelRequestCode;
    public int processId;
    public int secretKey;

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        cancelRequestCode = ds.readInt();
        processId = ds.readInt();
        secretKey = ds.readInt();
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeInt(16);
        ds.writeInt(cancelRequestCode);
        ds.writeInt(processId);
        ds.writeInt(secretKey);
    }

}
