
package science.unlicense.db.postgresql.message;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : Describe(F)
 *
 * @author Johann Sorel
 */
public class Describe extends Message {

    public static final int CODE = 'D';

    public void read(DataInputStream ds) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
