
package science.unlicense.db.postgresql.mapping;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int64;
import science.unlicense.db.postgresql.message.RowDescription;

/**
 *
 * @author Johann Sorel
 */
public final class CommonFormats {

    private CommonFormats(){}

    public static final FieldFormat _INT2 = new _Int2();
    public static final FieldFormat _INT4 = new _Int4();
    public static final FieldFormat _INT8 = new _Int8();
    public static final FieldFormat _FLOAT8 = new _Float8();
    public static final FieldFormat _TEXT = new _Text();
    public static final FieldFormat _VARCHAR = new _Varchar();


    public static class _Int2 implements FieldFormat, FieldFormat.Decoder {

        @Override
        public boolean canDecode(RowDescription.Field field) {
            return field.dataTypeId == 21;
        }

        @Override
        public Decoder createDecoder(RowDescription.Field field) {
            return this;
        }

        @Override
        public Class expectedType() {
            return Short.class;
        }

        @Override
        public Object decode(RowDescription.Field field, byte[] fieldData) {
            if (field.format == 0) {
                return Int32.decode(new Chars(fieldData));
            } else {
                return Endianness.LITTLE_ENDIAN.readUInt(fieldData, 0);
            }
        }
    }

    public static class _Int8 implements FieldFormat, FieldFormat.Decoder {

        @Override
        public boolean canDecode(RowDescription.Field field) {
            return field.dataTypeId == 20;
        }

        @Override
        public Decoder createDecoder(RowDescription.Field field) {
            return this;
        }

        @Override
        public Class expectedType() {
            return Long.class;
        }

        @Override
        public Object decode(RowDescription.Field field, byte[] fieldData) {
            if (field.format == 0) {
                return Int64.decode(new Chars(fieldData));
            } else {
                return Endianness.LITTLE_ENDIAN.readLong(fieldData, 0);
            }
        }
    }

    public static class _Int4 implements FieldFormat, FieldFormat.Decoder {

        @Override
        public boolean canDecode(RowDescription.Field field) {
            return field.dataTypeId == 23;
        }

        @Override
        public Decoder createDecoder(RowDescription.Field field) {
            return this;
        }

        @Override
        public Class expectedType() {
            return Integer.class;
        }

        @Override
        public Object decode(RowDescription.Field field, byte[] fieldData) {
            if (field.format == 0) {
                return Int32.decode(new Chars(fieldData));
            } else {
                return Endianness.LITTLE_ENDIAN.readInt(fieldData, 0);
            }
        }
    }

    public static class _Float8 implements FieldFormat, FieldFormat.Decoder {

        @Override
        public boolean canDecode(RowDescription.Field field) {
            return field.dataTypeId == 701;
        }

        @Override
        public Decoder createDecoder(RowDescription.Field field) {
            return this;
        }

        @Override
        public Class expectedType() {
            return Double.class;
        }

        @Override
        public Object decode(RowDescription.Field field, byte[] fieldData) {
            if (field.format == 0) {
                return Float64.decode(new Chars(fieldData));
            } else {
                return Endianness.LITTLE_ENDIAN.readDouble(fieldData, 0);
            }
        }
    }

    public static class _Text implements FieldFormat, FieldFormat.Decoder {

        @Override
        public boolean canDecode(RowDescription.Field field) {
            return field.dataTypeId == 25;
        }

        @Override
        public Decoder createDecoder(RowDescription.Field field) {
            return this;
        }

        @Override
        public Class expectedType() {
            return Chars.class;
        }

        @Override
        public Object decode(RowDescription.Field field, byte[] fieldData) {
            return new Chars(fieldData);
        }
    }

    public static class _Varchar implements FieldFormat, FieldFormat.Decoder {

        @Override
        public boolean canDecode(RowDescription.Field field) {
            return field.dataTypeId == 1043;
        }

        @Override
        public Decoder createDecoder(RowDescription.Field field) {
            return this;
        }

        @Override
        public Class expectedType() {
            return Chars.class;
        }

        @Override
        public Object decode(RowDescription.Field field, byte[] fieldData) {
            return new Chars(fieldData);
        }
    }
}
