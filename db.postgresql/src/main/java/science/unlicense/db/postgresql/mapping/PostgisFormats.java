
package science.unlicense.db.postgresql.mapping;

import science.unlicense.db.postgresql.message.RowDescription;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.HexInputStream;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.format.wkt.EWKBReader;

/**
 *
 * @author Johann Sorel
 */
public final class PostgisFormats {

    private PostgisFormats(){}

    public static final FieldFormat _GEOMETRY = new _Geometry();


    public static class _Geometry implements FieldFormat, FieldFormat.Decoder {

        @Override
        public boolean canDecode(RowDescription.Field field) {
            return field.dataTypeId == 16391;
        }

        @Override
        public Decoder createDecoder(RowDescription.Field field) {
            return this;
        }

        @Override
        public Class expectedType() {
            return Geometry.class;
        }

        @Override
        public Object decode(RowDescription.Field field, byte[] fieldData) throws IOException {
            final HexInputStream hi = new HexInputStream(new ArrayInputStream(fieldData));
            final EWKBReader reader = new EWKBReader();
            reader.setInput(hi);
            return reader.read();
        }
    }

}
