
package science.unlicense.db.postgresql.message;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : BindComplete(B)
 *
 * @author Johann Sorel
 */
public class BindComplete extends Message {

    public static final int CODE = '2';

    public void read(DataInputStream ds) throws IOException {
        ds.readInt();
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(CODE);
        ds.writeInt(4);
    }

}
