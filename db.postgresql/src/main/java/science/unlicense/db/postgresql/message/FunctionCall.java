
package science.unlicense.db.postgresql.message;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : FunctionCall(F)
 *
 * @author Johann Sorel
 */
public class FunctionCall extends Message {

    public static final int CODE = 'F';

    public void read(DataInputStream ds) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
