
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : RowDescription(B)
 *
 * @author Johann Sorel
 */
public class RowDescription extends Message {

    public static final int CODE = 'T';

    public Field[] fields;

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        final int nbField = ds.readUShort();

        fields = new Field[nbField];

        for (int i=0;i<nbField;i++) {
            fields[i] = new Field();
            fields[i].name = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
            fields[i].tableId = ds.readInt();
            fields[i].columnId = ds.readUShort();
            fields[i].dataTypeId = ds.readInt();
            fields[i].dataTypeSize = ds.readUShort();
            fields[i].typeModifier = ds.readInt();
            fields[i].format = ds.readUShort();
        }

    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("RowDescription"), fields);
    }

    public static class Field extends CObject {
        public Chars name;
        public int tableId;
        public int columnId;
        public int dataTypeId;
        public int dataTypeSize;
        public int typeModifier;
        /**
         * 0 : text format
         * 1 : binary
         */
        public int format;

        @Override
        public Chars toChars() {
            final CharBuffer cb = new CharBuffer();
            cb.append(name);
            cb.append(" tableId: ");
            cb.append(Int32.encode(tableId));
            cb.append(" columnId: ");
            cb.append(Int32.encode(columnId));
            cb.append(" dataTypeId: ");
            cb.append(Int32.encode(dataTypeId));
            cb.append(" dataTypeSize: ");
            cb.append(Int32.encode(dataTypeSize));
            cb.append(" typeModifier: ");
            cb.append(Int32.encode(typeModifier));
            cb.append(" format: ");
            cb.append(Int32.encode(format));
            return cb.toChars();
        }



    }

}
