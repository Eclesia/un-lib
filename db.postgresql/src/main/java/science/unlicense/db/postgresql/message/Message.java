
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class Message extends CObject {

    /**
     * Read message, identifier byte has already been read.
     *
     * @param ds
     * @throws IOException
     */
    public abstract void read(DataInputStream ds) throws IOException;

    /**
     * Write message, including identifier byte and size.
     *
     * @param ds
     * @throws IOException
     */
    public abstract void write(DataOutputStream ds) throws IOException;

    @Override
    public Chars toChars() {
        return new Chars(getClass().getSimpleName());
    }

}
