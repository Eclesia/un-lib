
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : Close(F)
 *
 * @author Johann Sorel
 */
public class Close extends Message {

    public static final int CODE = 'C';

    public int action;
    public Chars stmtName;

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        action = ds.readUByte();
        stmtName = ds.readZeroTerminatedChars(0, CharEncodings.UTF_8);
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(CODE);
        ds.writeInt(5+stmtName.getByteLength()+1);
        ds.writeUByte(action);
        ds.writeBlockZeroTerminatedChars(stmtName, 0, CharEncodings.UTF_8);
    }

}
