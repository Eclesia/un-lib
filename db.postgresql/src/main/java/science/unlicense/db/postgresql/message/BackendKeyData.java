
package science.unlicense.db.postgresql.message;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Message : BackendKeyData(B)
 *
 * @author Johann Sorel
 */
public class BackendKeyData extends Message {

    public static final int CODE = 'K';

    public int processId;
    public int secrectKey;

    public void read(DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        processId = ds.readInt();
        secrectKey = ds.readInt();
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeInt(12);
        ds.writeInt(processId);
        ds.writeInt(secrectKey);
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("BackendKeyData ");
        cb.append(Int32.encode(processId));
        cb.append(":");
        cb.append(Int32.encode(secrectKey));
        return cb.toChars();
    }
}
