Mark Raynsford moved JSpatial to public domain.

http://io7m.com/publicdomain/


Copy of the message :

-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

2014-12-23

I hereby dedicate the jspatial package (in particular, the commit
d4aa34831ad83b8057f5bb15782e439870413a92 and any commits made since)
to the public domain.

  http://mvn.io7m.com/io7m-jspatial

-----BEGIN PGP SIGNATURE-----

iQIcBAEBCgAGBQJUmeaKAAoJEOgBVbcZgswHpsEP/RBx2+oJfyba0+knfk4OxKoA
z6De8SJIzMTLw8zO+aoFeBrr38F4NkurdRiBasTR9pQZqa+HSvwUoui4I7PllfTr
I2qSie+qlEOkEAwBdjAT6LZtUaOuRavaN8ZezYeX3mi8gCEQ15ovvIHUzpaKps+T
SFaLihHyJsTLNnR6wuAmmd6DaloZeBc3B4ktcXAAWeL6sBmpf8Z3mKT3hHHJ9QLf
j/4wdvtmG/eRhMiPqiC6XOQpkIs3/fC4tkFUf1OOwDvde+X4Q5DnPVDUz/BH82Mz
r0woa/jUP+jb4woXz5uUlmFa5yBsnWgNR8XHv0c+1bDzacbRjpjPGpcoKKmFFJiY
FhNM5dJJ7m3zeh/Y78exrNSZrFzUN4Gvp4AxlHkuBD+Rw6YkhlrD2B0DxqBWW7Ra
oJ2SFkmvOA3ngcDQeL7RPYQ+VWZAoX9zhmjYSc1wrg9PWKMXy9zdPT4+cEWUUyIn
lJTcZlnOtttn9cbugqBY6T/tlIU/C6c4bYCLXyRtiFJ0CVHCRwro+e5cWCH8t6VD
d0zg7JVxww+D5sqSpoc1wttAYmDVlDO2nlKQ95lOWJ4iLV9pGeypDZmckkIqKR49
LYeKd1FvuYGtL9ygVH7P37JTynuLGc8SLK7cTD8BLdj1mLSeHgCB/UOZ2tUC0Rv6
4g45z/0s3wS2eGF60jux
=uXCe
-----END PGP SIGNATURE-----


PGP key :
http://io7m.com/pgp/index.xhtml


PGP 2014
 All items (software, articles) released on this site are signed using PGP. The current key policy is yearly-expiring keys, with each new set of keys being signed by the previous set. Expired/revoked keys are archived here in order to allow for the verification of old releases. Key	Comment
AC7F 360E 1C07 2C47 00EB 566E E801 55B7 1982 CC07	2014 personal key
14F0 A524 8273 4791 55C4 1574 E936 2A12 2FE7 EC66	2014 release key

