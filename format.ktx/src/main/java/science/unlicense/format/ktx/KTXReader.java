
package science.unlicense.format.ktx;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 *
 * @author Johann Sorel
 */
public class KTXReader extends AbstractImageReader{

    private final KTXHeader header = new KTXHeader();
    private final Dictionary metas = new HashDictionary();

    @Override
    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        return new HashDictionary();
    }

    @Override
    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        final DataInputStream ds = new DataInputStream(stream, Endianness.BIG_ENDIAN);
        metas.removeAll();

        //read header
        header.read(ds);

        //read metadatas
        //for each keyValuePair that fits in bytesOfKeyValueData
        //    UInt32   keyAndValueByteSize
        //    Byte     keyAndValue[keyAndValueByteSize]
        //    Byte     valuePadding[3 - ((keyAndValueByteSize + 3) % 4)]
        //end
        int metaSize = header.bytesOfKeyValueData;
        while (metaSize>3){
            final int keyAndValueByteSize = ds.readInt();
            final Chars key = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
            final byte[] value = ds.readFully(new byte[keyAndValueByteSize-(key.getByteLength()+1)]);
            metas.add(key, value);
        }
        //4byte alignment
        ds.realign(4);

        //read images, mipmaps, faces ...
        //for each mipmap_level in numberOfMipmapLevels*
        //    UInt32 imageSize;
        //    for each array_element in numberOfArrayElements*
        //       for each face in numberOfFaces
        //           for each z_slice in pixelDepth*
        //               for each row or row_of_blocks in pixelHeight*
        //                   for each pixel or block_of_pixels in pixelWidth
        //                       Byte data[format-specific-number-of-bytes]**
        //                   end
        //               end
        //           end
        //           Byte cubePadding[0-3]
        //       end
        //    end
        //    Byte mipPadding[3 - ((imageSize + 3) % 4)]
        //end
        final int nbMipmaplLevel = (header.numberOfMipmapLevels==0) ? 1 : header.numberOfMipmapLevels;
        final int nbArrayElement = (header.numberOfArrayElements==0) ? 1 : header.numberOfArrayElements;
        final int pixelDepth     = (header.pixelDepth==0) ? 1 : header.pixelDepth;
        final int pixelHeight    = (header.pixelHeight==0) ? 1 : header.pixelHeight;
        final int pixelWidth     = header.pixelWidth;

        //image structure
        final Buffer[][][][] struct = new Buffer[nbMipmaplLevel][nbArrayElement][header.numberOfFaces][pixelDepth];

        //mipmaps
        for (int m=0;m<nbMipmaplLevel;m++){
            final int imageSize = ds.readInt();
            //array elements
            for (int a=0;a<nbArrayElement;a++){
                //faces
                for (int f=0;f<header.numberOfFaces;f++){
                    //z
                    for (int z=0;z<pixelDepth;z++){
                        final Buffer buffer = parameters.getBufferFactory().createInt8(imageSize).getBuffer();
                        ds.readFully(buffer);
                        struct[m][a][f][z] = buffer;
                    }
                    ds.realign(4);
                }
            }
        }

        if (header.glFormat == GLC.Texture.Format.RGB){
            final ImageModel rm = new InterleavedModel(new UndefinedSystem(3), UInt8.TYPE);
            final ImageModel cm = DerivateModel.create(rm, new int[]{0,1,2}, null, null, ColorSystem.RGB_8BITS);
            final Image img = new DefaultImage(struct[0][0][0][0], new Extent.Long(header.pixelWidth, header.pixelHeight), rm, cm);
            return img;
        }
        //TODO all other OpenGL formats
        //TODO think about a correct API to support mipmaps : use ImageStore ?
        else {
            throw new IOException(ds, "Unsupported image type : "+header.glFormat);
        }
    }

}
