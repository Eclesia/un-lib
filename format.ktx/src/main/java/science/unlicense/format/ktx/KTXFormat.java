
package science.unlicense.format.ktx;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * Specification :
 * https://www.khronos.org/opengles/sdk/tools/KTX/file_format_spec/
 *
 * @author Johann Sorel
 */
public class KTXFormat extends AbstractImageFormat {

    public static final KTXFormat INSTANCE = new KTXFormat();

    private KTXFormat() {
        super(new Chars("ktx"));
        shortName = new Chars("KTX");
        longName = new Chars("KTX");
        extensions.add(new Chars("ktx"));
        signatures.add(KTXConstants.SIGNATURE);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new KTXStore(this, source);
    }

}
