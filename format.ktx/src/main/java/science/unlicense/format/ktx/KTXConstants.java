
package science.unlicense.format.ktx;

/**
 *
 * @author Johann Sorel
 */
public final class KTXConstants {

    public static final byte[] SIGNATURE = new byte[]{
        (byte) 0xAB, (byte) 0x4B, (byte) 0x54, (byte) 0x58, (byte) 0x20, (byte) 0x31,
        (byte) 0x31, (byte) 0xBB, (byte) 0x0D, (byte) 0x0A, (byte) 0x1A, (byte) 0x0A};

    public static final int BIG_ENDIAN = 0x04030201;
    public static final int LITTLE_ENDIAN = 0x01020304;

    private KTXConstants(){}

}
