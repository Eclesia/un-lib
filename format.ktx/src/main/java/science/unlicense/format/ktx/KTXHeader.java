
package science.unlicense.format.ktx;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class KTXHeader {

    public int endianness;
    /**
     * One of GLC.Texture.Type
     */
    public int glType;
    /**
     *
     */
    public int glTypeSize;
    /**
     * One of GLC.Texture.Format
     */
    public int glFormat;
    /**
     * One of GLC.Texture.InternalFormat
     */
    public int glInternalFormat;
    public int glBaseInternalFormat;
    public int pixelWidth;
    public int pixelHeight;
    public int pixelDepth;
    public int numberOfArrayElements;
    /**
     * For cubemap : 6 , in order +X, -X, +Y, -Y, +Z, -Z
     */
    public int numberOfFaces;
    public int numberOfMipmapLevels;
    public int bytesOfKeyValueData;

    public void read(DataInputStream ds) throws IOException {

        //check signature
        final byte[] signature = ds.readFully(new byte[12]);
        if (!Arrays.equals(signature, KTXConstants.SIGNATURE)){
            throw new IOException(ds, "Input is not a KTX image, wrong signature");
        }

        //all values are UInt32
        endianness              = ds.readInt();
        if (endianness==KTXConstants.BIG_ENDIAN){
            ds.setEndianness(Endianness.BIG_ENDIAN);
        } else if (endianness==KTXConstants.LITTLE_ENDIAN){
            ds.setEndianness(Endianness.LITTLE_ENDIAN);
        } else {
            throw new IOException(ds, "Unvalid endianness value : "+endianness);
        }

        glType                  = ds.readInt();
        glTypeSize              = ds.readInt();
        glFormat                = ds.readInt();
        glInternalFormat        = ds.readInt();
        glBaseInternalFormat    = ds.readInt();
        pixelWidth              = ds.readInt();
        pixelHeight             = ds.readInt();
        pixelDepth              = ds.readInt();
        numberOfArrayElements   = ds.readInt();
        numberOfFaces           = ds.readInt();
        numberOfMipmapLevels    = ds.readInt();
        bytesOfKeyValueData     = ds.readInt();
    }

}
