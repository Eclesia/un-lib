
package science.unlicense.format.ktx;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.concurrent.api.Paths;

/**
 * TODO
 *
 * @author Johann Sorel
 */
public class KTXReaderTest {

    @Ignore
    @Test
    public void testRead() throws IOException{
        //TODO find some free files to add in tests.
        final Path path = Paths.resolve(new Chars("mod:..."));
        final Image image = Images.read(path);
        System.out.println(image);

    }

}
