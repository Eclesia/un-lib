
package science.unlicense.engine.css;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public class CSSFormat extends DefaultFormat {

    public CSSFormat() {
        super(new Chars("css"));
        shortName = new Chars("CSS");
        longName = new Chars("Cascading Style Sheets");
        mimeTypes.add(new Chars("text/css"));
        extensions.add(new Chars("css"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
