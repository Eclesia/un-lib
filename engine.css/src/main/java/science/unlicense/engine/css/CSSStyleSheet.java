

package science.unlicense.engine.css;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class CSSStyleSheet {

    private final Sequence classes = new ArraySequence();

    public CSSStyleSheet() {
    }

    public Sequence getClasses() {
        return classes;
    }

}
