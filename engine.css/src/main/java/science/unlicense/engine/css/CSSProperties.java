
package science.unlicense.engine.css;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.Float64;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.Colors;

/**
 *
 * @author Johann Sorel
 */
public class CSSProperties extends HashDictionary{

    private static final Chars NONE = Chars.constant("none");

    public Color getColor(Chars name) {
        final Chars txt = (Chars) getValue(name);
        if (txt==null) return null;
        if (NONE.equals(txt)) return null;
        return Colors.fromHexa(txt);
    }

    public Double getDouble(Chars name) {
        final Chars txt = (Chars) getValue(name);
        if (txt==null) return null;
        return Float64.decode(txt);
    }

}
