
package science.unlicense.task.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.system.ModuleSeeker;

/**
 * Convinient methods for tasks.
 *
 * @author Johann Sorel
 */
public final class Tasks {

    private Tasks(){}

    /**
     * Lists available image tasks
     * @return array of TaskDescriptor, never null but can be empty.
     */
    public static TaskDescriptor[] getDescriptors(){
        return (TaskDescriptor[]) ModuleSeeker.findServices(TaskDescriptor.class);
    }

    /**
     * Get task descriptor for given id.
     * @param id task id
     * @return TaskDescriptor, null if not found
     */
    public static TaskDescriptor getDescriptor(Chars id){
        final TaskDescriptor[] descs = getDescriptors();
        for (int i=0;i<descs.length;i++){
            if (descs[i].getId().equals(id)) return descs[i];
        }
        return null;
    }

}
