
package science.unlicense.task.api;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DocumentType;

/**
 * Abstract task descriptor.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTaskDescriptor extends CObject implements TaskDescriptor {

    public static final Chars INPUT = Chars.constant("input");
    public static final Chars OUTPUT = Chars.constant("output");

    protected final Chars id;
    protected final CharArray name;
    protected final CharArray description;
    protected final DocumentType inputType;
    protected final DocumentType outputType;

    public AbstractTaskDescriptor(Chars id, CharArray name, CharArray description,
            DocumentType inputType, DocumentType outputType) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.inputType = inputType;
        this.outputType = outputType;
    }

    @Override
    public Chars getId(){
        return id;
    }

    @Override
    public CharArray getTitle(){
        return name;
    }

    @Override
    public CharArray getDescription(){
        return description;
    }

    @Override
    public DocumentType getInputType() {
        return inputType;
    }

    @Override
    public DocumentType getOutputType() {
        return outputType;
    }

    @Override
    public abstract Task create();

}
