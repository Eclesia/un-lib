
package science.unlicense.task.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.DefaultEventMessage;
import science.unlicense.common.api.model.doc.Document;

/**
 * Listen to task progress.
 *
 * @author Johann Sorel
 */
public class TaskMessage extends DefaultEventMessage {

    public static final int STATE_START = 0;
    public static final int STATE_PROGRESS = 1;
    public static final int STATE_END = 2;
    public static final int STATE_CANCEL = 3;
    public static final int STATE_FAIL = 4;

    private final int state;
    private final float progress;
    private final Chars message;
    private final Exception error;
    private final Document result;

    /**
     *
     * @param state task execution state, one of STATE_X
     * @param progress execution progress, between 0 and 1
     * @param message progress message, can be null
     * @param result intermediate results, can be null
     * @param error execution exception, can be null
     */
    public TaskMessage(int state, float progress,
            Chars message, Document result, Exception error) {
        super(false);
        this.state = state;
        this.progress = progress;
        this.message = message;
        this.error = error;
        this.result = result;
    }

    /**
     * Task state.
     * @return int
     */
    public int getState() {
        return state;
    }

    /**
     * Task progress.
     * @return float between 0 and 1.
     */
    public float getProgress() {
        return progress;
    }

    /**
     * Task progress message.
     * @return Chars, may be null.
     */
    public Chars getMessage() {
        return message;
    }

    /**
     * Get the intermediate results.
     * Some tasks may start returning result even if they are not finished.
     * @return Document, can be null
     */
    public Document getResult() {
        return result;
    }

    /**
     * Task progress exception if failed or minor error occured.
     * @return Exception, can be null.
     */
    public Exception getError() {
        return error;
    }

}
