
package science.unlicense.task.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;

/**
 * Common model for generic tasks.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTask extends AbstractEventSource implements Task {

    protected final TaskDescriptor descriptor;
    protected boolean canceled = false;
    protected final Document inputParameters;
    protected final Document outputParameters;

    public AbstractTask(final TaskDescriptor descriptor) {
        this.descriptor = descriptor;
        this.inputParameters = new DefaultDocument(false, descriptor.getInputType());
        this.outputParameters = new DefaultDocument(false, descriptor.getOutputType());
    }

    @Override
    public TaskDescriptor getDescriptor() {
        return descriptor;
    }

    @Override
    public final Document inputs() {
        return inputParameters;
    }

    @Override
    public final Document outputs() {
        return outputParameters;
    }

    @Override
    public void cancel() {
        canceled = true;
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[]{TaskMessage.class};
    }

    protected void fireEvent(int state, float progress, Chars message, Exception error) {
        if (hasListeners()) {
            getEventManager().sendEvent(new Event(this, new TaskMessage(state, progress, message, outputParameters, error)));
        }
    }

}
