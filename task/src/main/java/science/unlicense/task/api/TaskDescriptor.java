
package science.unlicense.task.api;

import science.unlicense.common.api.model.Presentable;
import science.unlicense.common.api.model.doc.DocumentType;

/**
 * Description of a Task.
 *
 * @author Johann Sorel
 */
public interface TaskDescriptor extends Presentable {

    /**
     * Input parameters of the task.
     * @return ParameterType , never null
     */
    DocumentType getInputType();

    /**
     * Output parameters of the task.
     * @return ParameterType , never null
     */
    DocumentType getOutputType();

    /**
     * Create a new instance of this task.
     * @return Task, never null.
     */
    Task create();

}
