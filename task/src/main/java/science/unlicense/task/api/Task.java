
package science.unlicense.task.api;

import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.functional.OutFunction;
import science.unlicense.common.api.model.doc.Document;

/**
 * Common model for generic tasks.
 *
 * @author Johann Sorel
 */
public interface Task extends EventSource, OutFunction {

    /**
     * Get task description.
     *
     * @return Task descriptor, should not be null
     */
    TaskDescriptor getDescriptor();

    /**
     * Ask to stop the task if it is running.
     * Task have no guarantee to respect this request.
     * Best effort.
     */
    void cancel();

    /**
     * Get task input parameters.
     * @return parameters document
     */
    Document inputs();

    /**
     * Get task output parameters.
     * The content of the document may change while the process is running.
     * This allow intermediate or partial results to be used.
     *
     * @return parameters document
     */
    Document outputs();

    /**
     * Execute the task.
     * @return result parameters, same instance as outputs method returned object.
     */
    @Override
    Document perform();

}
