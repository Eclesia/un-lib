package science.unlicense.linguistic.impl;


import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.transform.FFT;


/**
 *
 * Origin :
 * http://zynaddsubfx.sourceforge.net/doc/PADsynth/PADsynth.htm
 *
 * @author Nasca Octavian Paul (Original code in C/C++)
 * @author Johann Sorel (Ported to Java and Unlicense-lib)
 */
public class PadSynthetizer{

    private static class FFTFREQS{
        public final double[] c;
        public final double[] s;

        public FFTFREQS(int size) {
            c = new double[size];
            s = new double[size];
        }
    }

    private static final int RAND_MAX = 1;
    private static final int N = 262144;
    private static final int number_harmonics = 64;

    private PadSynthetizer(){}

    /* Random number generator */
    private static double RND(){
        return (double) (Math.random() / (RAND_MAX+1.0));
    }

    /* This is the profile of one harmonic
       In this case is a Gaussian distribution (e^(-x^2))
       The amplitude is divided by the bandwidth to ensure that the harmonic
       keeps the same amplitude regardless of the bandwidth */
    private static double profile(double fi,double bwi){
        double x=fi/bwi;
        x*=x;
        //this avoids computing the e^(-x^2) where it's results are very close to zero
        if (x>14.71280603) return 0.0;
        return Math.exp(-x)/bwi;
    }

    /*
        Inverse Fast Fourier Transform
        You may replace it with any IFFT routine
    */
    private static void IFFT(int N,double[] freq_amp,double[] freq_phase,double[] smp){
        final int n2 = N/2;
        //FFTwrapper fft(N);
        FFTFREQS fftfreqs = new FFTFREQS(n2);

        for (int i=0;i<n2;i++){
            fftfreqs.c[i]=freq_amp[i]*Math.cos(freq_phase[i]);
            fftfreqs.s[i]=freq_amp[i]*Math.sin(freq_phase[i]);
        }
        FFT.ifft(fftfreqs.c, fftfreqs.s);
        for (int i=0;i<n2;i++){
            smp[i] = fftfreqs.c[i];
        }
        //fft.freqs2smps(fftfreqs,smp);
    }

    /*
        Simple normalization function. It normalizes the sound to 1/sqrt(2)
    */
    private static void normalize(int N, double[] smp){
        int i;
        double max=0.0;
        for (i=0;i<N;i++) {
            if (Math.abs(smp[i])>max) max=Math.abs(smp[i]);
        }

        if (max == 0) {
            throw new InvalidArgumentException("Provided parameters resulted in a zero factor.");
        }
        if (max<1e-5) max=1e-5;
        for (i=0;i<N;i++) {
            smp[i]/=max*1.4142;
        }
    }


    /**
     * This is the implementation of PADsynth algorithm.
     *
     * @param N
     * @param samplerate
     * @param f
     * @param bw
     * @param number_harmonics
     * @param A input data
     * @param smp output data
     */
    private static void padsynth_basic_algorithm(int N, int samplerate, double f,
            double bw,int number_harmonics, double[] A, double[] smp) {

        int i,nh;
        double[] freq_amp=new double[N/2];
        double[] freq_phase=new double[N/2];

        //default, all the frequency amplitudes are zero
        for (i=0;i<N/2;i++) freq_amp[i]=0.0;

        for (nh=1;nh<number_harmonics;nh++){//for each harmonic
            double bw_Hz;//bandwidth of the current harmonic measured in Hz
            double bwi;
            double fi;
            bw_Hz=(Math.pow(2.0,bw/1200.0)-1.0)*f*nh;

            bwi=bw_Hz/(2.0*samplerate);
            fi=f*nh/samplerate;
            for (i=0;i<N/2;i++){
                double hprofile;
                hprofile=profile((i/(double) N)-fi,bwi);
                freq_amp[i]+=hprofile*A[nh];
            }
        }

        //Add random phases
        for (i=0;i<N/2;i++){
            freq_phase[i]=RND()*2.0*Maths.PI;
        }

        IFFT(N,freq_amp,freq_phase,smp);
        normalize(N,smp);

    }


    public static void main(String[] args) {
        double[] sample = new double[N];

        double[] A = new double[number_harmonics];
        //A[0] is not used
        A[0]=0.0;

        for (int note=0;note<=24;note+=4){
            double f1=130.81*Math.pow(2,note/12.0);
            System.out.print("Generating frequency: "+(int) f1+" Hz\n");
            for (int i=1;i<number_harmonics;i++) {
                A[i]=1.0/i;
                double formants=
                    Math.exp(-Math.pow((i*f1-600.0)/150.0,2.0))+
                    Math.exp(-Math.pow((i*f1-900.0)/250.0,2.0))+
                    Math.exp(-Math.pow((i*f1-2200.0)/200.0,2.0))+
                    Math.exp(-Math.pow((i*f1-2600.0)/250.0,2.0))+
                    Math.exp(-Math.pow((i*f1)/3000.0,2.0))*0.1;
                A[i]*=formants;
            }
            padsynth_basic_algorithm(N,44100,f1,60.0,number_harmonics,A,sample);

            /* Output the data to the 16 bit, mono raw file */
            short[] isample = new short[N];
            for (int i=0;i<N;i++) {
                isample[i]= (short) (sample[i]*32768.0);
            }

            final Path path = Paths.resolve(new Chars("file:./padsynth.wav"));
            try{
                final DataOutputStream ds = new DataOutputStream(path.createOutputStream());
                ds.setEndianness(Endianness.LITTLE_ENDIAN);

                int bitdepth = 16;
                int byteDepth = 2;
                int channels = 1;
                int sampleRate = 44100;
                int nbSample = isample.length;
                ds.write("RIFF".getBytes());
                ds.writeInt(36+(nbSample*byteDepth));
                ds.write("WAVEfmt ".getBytes());
                ds.writeInt(16);
                ds.writeShort((short) 1);
                ds.writeUShort(channels);
                ds.writeInt(sampleRate);
                ds.writeInt(sampleRate*channels*byteDepth);
                ds.writeShort((short) (channels*byteDepth));
                ds.writeShort((short) (byteDepth*8));
                ds.write("data".getBytes());
                ds.writeInt(nbSample*byteDepth);
                ds.writeShort(isample);
                ds.close();

            }catch(IOException ex){
                ex.printStackTrace();
            }

        }
    }


}
