

package science.unlicense.linguistic.api;

import science.unlicense.common.api.character.Chars;

/**
 * Language : en,fr,br,...
 *
 * @author Johann Sorel
 */
public class Language {

    private final Chars code;

    public Language(Chars code) {
        this.code = code;
    }

    public Chars getCode() {
        return code;
    }

}
