
package science.unlicense.linguistic.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;

/**
 * Basicaly a dictionnary.
 *
 * documents :
 * http://en.wikipedia.org/wiki/Lexicon
 *
 * @author Johann Sorel
 */
public interface Lexicon {

    /**
     * Lexicon language.
     *
     * @return Language never null
     */
    Language getLanguage();

    /**
     * Create an iterator over all words.
     *
     * @return Iterator of Word objects
     */
    Iterator createIterator();

    /**
     * Find word for given text.
     *
     * @param word
     * @return Word or null if not found
     */
    Word getWord(Chars word);

}
