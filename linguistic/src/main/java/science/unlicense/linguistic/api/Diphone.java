
package science.unlicense.linguistic.api;

import science.unlicense.common.api.character.Chars;

/**
 * A pair of phoneme.
 *
 * @author Johann Sorel
 */
public class Diphone {

    private final Phoneme first;
    private final Phoneme second;

    public Diphone(Phoneme first, Phoneme second) {
        this.first = first;
        this.second = second;
    }

    public Phoneme getFirstPhoneme() {
        return first;
    }

    public Phoneme getSecondPhoneme() {
        return second;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Diphone){
            final Diphone other = (Diphone) obj;
            return this.first.equals(other.first) && this.second.equals(other.second);
        }
        return false;
    }

    public int getHash() {
        return first.getHash() + second.getHash();
    }

    public Chars toChars() {
        return first.toChars().concat(' ').concat(second.toChars());
    }

}
