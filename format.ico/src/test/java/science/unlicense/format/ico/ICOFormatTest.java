
package science.unlicense.format.ico;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.Images;
import science.unlicense.format.ico.ico.ICOFormat;

/**
 *
 * @author Johann Sorel
 */
public class ICOFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final ImageFormat[] formats = Images.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof ICOFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("Image format not found.");
    }

}
