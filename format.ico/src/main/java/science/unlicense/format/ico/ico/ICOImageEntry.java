
package science.unlicense.format.ico.ico;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ICOImageEntry {

    /**
     * Specifies image width in pixels. Can be any number between 0 and 255.
     * Value 0 means image width is 256 pixels.
     * Common values are : 16,32,64
     * 1 byte.
     */
    public int width;
    /**
     * Specifies image height in pixels. Can be any number between 0 and 255.
     * Value 0 means image height is 256 pixels.
     * Common values are : 16,32,64
     * 1 byte.
     */
    public int height;
    /**
     * Specifies number of colors in the color palette.
     * Should be 0 if the image does not use a color palette.
     * 1 byte.
     */
    public int nbColor;
    /**
     * Reserved.
     * Should be 0.[Notes 2]
     * 1 byte.
     */
    public byte reserved;
    /**
     * In ICO format: Specifies color planes. Should be 0 or 1.
     * In CUR format: Specifies the horizontal coordinates of the hotspot in number of pixels from the left.
     * 2 bytes.
     */
    public int colorPlane;
    /**
     * In ICO format: Specifies bits per pixel.
     * In CUR format: Specifies the vertical coordinates of the hotspot in number of pixels from the top.
     * Common values are : 1,4,8
     * 2 bytes.
     */
    public int bitsPerPixel;
    /**
     * Specifies the size of the image's data in bytes.
     * 4 bytes.
     */
    public int imageByteSize;
    /**
     * Specifies the offset of BMP or PNG data from the beginning of the ICO/CUR file.
     * 4 bytes.
     */
    public int offset;

    public void read(DataInputStream ds) throws IOException{
        width           = ds.readUByte();
        height          = ds.readUByte();
        nbColor         = ds.readUByte();
        reserved        = ds.readByte();
        colorPlane      = ds.readUShort();
        bitsPerPixel    = ds.readUShort();
        imageByteSize   = ds.readInt();
        offset          = ds.readInt();
    }

}
