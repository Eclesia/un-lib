
package science.unlicense.format.ico.cur;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;
import science.unlicense.image.api.ImageReader;
import science.unlicense.format.ico.ico.ICOConstants;

/**
 *
 * @author Johann Sorel
 */
public class CURFormat extends AbstractImageFormat {

    public static final CURFormat INSTANCE = new CURFormat();

    private CURFormat() {
        super(new Chars("cur"));
        shortName = new Chars("CUR");
        longName = new Chars("CUR file format");
        extensions.add(new Chars("cur"));
        signatures.add(ICOConstants.SIGNATURE_CUR);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new CURStore(this, source);
    }

    @Override
    public boolean canDecode(Object input) throws IOException {
        boolean valid =  super.canDecode(input);
        if (valid) {
            // ICO/CUR signature matches a lot of files, try to read the metas
            final CURStore store = new CURStore(this, input);
            try {
                final ImageReader reader = store.createReader();
                try {
                    reader.getMetadata(reader.getMetadataNames()[0]);
                } catch(IOException ex) {
                    return false;
                } finally {
                    reader.dispose();
                }
            } finally {
                store.dispose();
            }
        }
        return valid;
    }

}
