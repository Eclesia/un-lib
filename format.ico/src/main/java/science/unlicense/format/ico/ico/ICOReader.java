
package science.unlicense.format.ico.ico;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.format.bmp.BMPInfoHeader;
import science.unlicense.format.bmp.BMPReader;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGESET;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION_ID;

/**
 *
 * @author Johann Sorel
 */
public class ICOReader extends AbstractImageReader{

    private final byte[] signature;
    private ICOImageEntry[] entries;
    private TypedNode[] mdImages;

    public ICOReader(byte[] signature) {
        this.signature = signature;
    }

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        final DataInputStream ds = new DataInputStream(stream, Endianness.LITTLE_ENDIAN);

        final byte[] sign = ds.readFully(new byte[4]);
        if (!Arrays.equals(sign, signature)){
            throw new IOException(ds, "Not an ICO/CUR image file.");
        }

        final int nbImage = ds.readUShort();
        if (nbImage==0){
            throw new IOException(ds, "Invalid ICO/CUR file, 0 images defined for input : "+getInput());
        }
        entries = new ICOImageEntry[nbImage];
        mdImages = new TypedNode[nbImage];
        for (int i=0;i<nbImage;i++){
            entries[i] = new ICOImageEntry();
            entries[i].read(ds);

            final TypedNode mdImage =
            new DefaultTypedNode(MD_IMAGE,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                    new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"x"),
                    new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,entries[i].width)}),
                new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                    new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"y"),
                    new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,entries[i].height)})
            });
            mdImages[i] = mdImage;
        }

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,mdImages);
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;

    }

    protected Image read(ImageReadParameters irp, BacktrackInputStream stream) throws IOException {
        stream.mark();
        readMetadatas(stream);
        stream.rewind();

        final DataInputStream ds = new DataInputStream(stream, Endianness.LITTLE_ENDIAN);

        final int index = irp.getImageIndex();
        final ICOImageEntry entry = entries[index];
        final TypedNode mdImage = mdImages[index];

        ds.skipFully(entry.offset);

        //read the bmp header
        final BMPInfoHeader header = new BMPInfoHeader();
        header.read(ds);
        final BMPReader reader = new BMPReader();
        reader.setHeader(header);
        final Image baseImage = reader.readNoSkip(irp, stream);


        return baseImage;
    }



}
