
package science.unlicense.format.ico.ico;

/**
 *
 * @author Johann Sorel
 */
public class ICOConstants {

    public static final byte[] SIGNATURE_ICO = new byte[]{0x00,0x00,0x01,0x00};
    public static final byte[] SIGNATURE_CUR = new byte[]{0x00,0x00,0x02,0x00};

    public static final int TYPE_MONOCHROME = 1;

    private ICOConstants(){}

}
