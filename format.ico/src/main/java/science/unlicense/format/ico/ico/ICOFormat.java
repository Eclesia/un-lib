
package science.unlicense.format.ico.ico;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;
import science.unlicense.image.api.ImageReader;

/**
 * Resources :
 * http://en.wikipedia.org/wiki/ICO_(file_format)
 * http://blogs.msdn.com/b/oldnewthing/archive/2010/10/18/10077133.aspx
 * http://blogs.msdn.com/b/oldnewthing/archive/2010/10/19/10077610.aspx
 * http://blogs.msdn.com/b/oldnewthing/archive/2010/10/21/10078690.aspx
 * http://blogs.msdn.com/b/oldnewthing/archive/2010/10/22/10079192.aspx
 *
 * @author Johann Sorel
 */
public class ICOFormat extends AbstractImageFormat {

    public static final ICOFormat INSTANCE = new ICOFormat();

    public ICOFormat() {
        super(new Chars("ico"));
        shortName = new Chars("ICO");
        longName = new Chars("ICO file format");
        mimeTypes.add(new Chars("image/x-icon"));
        mimeTypes.add(new Chars("image/ico"));
        mimeTypes.add(new Chars("image/icon"));
        mimeTypes.add(new Chars("image/vnd.microsoft.icon"));
        mimeTypes.add(new Chars("text/ico"));
        mimeTypes.add(new Chars("application/ico"));
        extensions.add(new Chars("ico"));
        signatures.add(ICOConstants.SIGNATURE_ICO);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new ICOStore(this, source);
    }

    @Override
    public boolean canDecode(Object input) throws IOException {
        boolean valid =  super.canDecode(input);
        if (valid) {
            // ICO/CUR signature matches a lot of files, try to read the metas
            final ICOStore store = new ICOStore(this, input);
            try {
                final ImageReader reader = store.createReader();
                try {
                    reader.getMetadata(reader.getMetadataNames()[0]);
                } catch(IOException ex) {
                    return false;
                } finally {
                    reader.dispose();
                }
            } finally {
                store.dispose();
            }
        }
        return valid;
    }
}
