

package science.unlicense.format.f4v;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;

/**
 *
 * @author Johann Sorel
 */
public class F4VStore extends AbstractStore implements Media {

    private final Path path;


    public F4VStore(Path path) {
        super(F4VFormat.INSTANCE,path);
        this.path = path;
        try {
            read();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void read() throws IOException{
    }

    @Override
    public Track[] getTracks() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
