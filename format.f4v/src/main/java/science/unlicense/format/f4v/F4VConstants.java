
package science.unlicense.format.f4v;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class F4VConstants {

    /**
     * File signature ?
     */
    public static final Chars SIGNATURE = Chars.constant(new byte[]{'F','4','V'});

    private F4VConstants(){}

}
