

package science.unlicense.format.f4v;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;

/**
 * Specification :
 * http://www.adobe.com/devnet/f4v.html
 * http://download.macromedia.com/f4v/video_file_format_spec_v10_1.pdf
 *
 * @author Johann Sorel
 */
public class F4VFormat extends AbstractMediaFormat {

    public static final F4VFormat INSTANCE = new F4VFormat();

    public F4VFormat() {
        super(new Chars("f4v"));
        shortName = new Chars("F4V");
        longName = new Chars("F4V");
        mimeTypes.add(new Chars("video/x-flv"));
        extensions.add(new Chars("f4v"));
        resourceTypes.add(Media.class);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public Store open(Object input) throws IOException {
        return new F4VStore((Path) input);
    }

}
