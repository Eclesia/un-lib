

package science.unlicense.format.f4v;

import science.unlicense.format.f4v.F4VFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.media.api.MediaFormat;
import science.unlicense.media.api.Medias;

/**
 * Test FLV Format declaration.
 *
 * @author Johann Sorel
 */
public class F4VFormatTest {

    @Test
    public void testFormat(){
        final MediaFormat[] formats = Medias.getFormats();
        for (MediaFormat format : formats){
            if (format instanceof F4VFormat){
                return;
            }
        }

        Assert.fail("F4V Format not found.");
    }

}
