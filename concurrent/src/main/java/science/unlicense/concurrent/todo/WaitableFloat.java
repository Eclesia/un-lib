/*
  File: WaitableFloat.java

  Originally written by Doug Lea and released into the public domain.
  This may be used for any purposes whatsoever without acknowledgment.
  Thanks for the assistance and support of Sun Microsystems Labs,
  and everyone contributing, testing, and using this code.

  History:
  Date       Who                What
  23Jun1998  dl               Create public version
 */
package science.unlicense.concurrent.todo;

import science.unlicense.concurrent.api.SynchronizedFloat32;

/**
 * A class useful for offloading waiting and signalling operations on single
 * float variables.
 * <p>
 * <p>
 * [<a href="http://gee.cs.oswego.edu/dl/classes/EDU/oswego/cs/dl/util/concurrent/intro.html">
 * Introduction to this package. </a>]
 *
 * @author Doug Lea (Original author)
 */
public class WaitableFloat extends SynchronizedFloat32 {

    /**
     * Make a new WaitableFloat with the given initial value, and using its own
     * internal lock.
   *
     */
    public WaitableFloat(float initialValue) {
        super(initialValue);
    }

    /**
     * Make a new WaitableFloat with the given initial value, and using the
     * supplied lock.
   *
     */
    public WaitableFloat(float initialValue, Object lock) {
        super(initialValue, lock);
    }

    public float set(float newValue) {
        synchronized (lock) {
            lock.notifyAll();
            return super.set(newValue);
        }
    }

    public boolean commit(float assumedValue, float newValue) {
        synchronized (lock) {
            boolean success = super.commit(assumedValue, newValue);
            if (success) {
                lock.notifyAll();
            }
            return success;
        }
    }

    public float add(float amount) {
        synchronized (lock) {
            lock.notifyAll();
            return super.add(amount);
        }
    }

    public float subtract(float amount) {
        synchronized (lock) {
            lock.notifyAll();
            return super.subtract(amount);
        }
    }

    public float multiply(float factor) {
        synchronized (lock) {
            lock.notifyAll();
            return super.multiply(factor);
        }
    }

    public float divide(float factor) {
        synchronized (lock) {
            lock.notifyAll();
            return super.divide(factor);
        }
    }

    /**
     * Wait until value equals c, then run action if nonnull. The action is run
     * with the synchronization lock held.
   *
     */
    public void whenEqual(float c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value == c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

    /**
     * wait until value not equal to c, then run action if nonnull. The action
     * is run with the synchronization lock held.
   *
     */
    public void whenNotEqual(float c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value != c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

    /**
     * wait until value less than or equal to c, then run action if nonnull. The
     * action is run with the synchronization lock held.
   *
     */
    public void whenLessEqual(float c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value <= c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

    /**
     * wait until value less than c, then run action if nonnull. The action is
     * run with the synchronization lock held.
   *
     */
    public void whenLess(float c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value < c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

    /**
     * wait until value greater than or equal to c, then run action if nonnull.
     * The action is run with the synchronization lock held.
   *
     */
    public void whenGreaterEqual(float c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value >= c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

    /**
     * wait until value greater than c, then run action if nonnull. The action
     * is run with the synchronization lock held.
   *
     */
    public void whenGreater(float c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value > c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

}
