/*
  File: WaitableShort.java

  Originally written by Doug Lea and released into the public domain.
  This may be used for any purposes whatsoever without acknowledgment.
  Thanks for the assistance and support of Sun Microsystems Labs,
  and everyone contributing, testing, and using this code.

  History:
  Date       Who                What
  23Jun1998  dl               Create public version
  13may2004  dl               Add notifying bit ops
 */
package science.unlicense.concurrent.todo;

import science.unlicense.concurrent.api.SynchronizedInt16;

/**
 * A class useful for offloading waiting and signalling operations on single
 * short variables.
 * <p>
 * <p>
 * [<a href="http://gee.cs.oswego.edu/dl/classes/EDU/oswego/cs/dl/util/concurrent/intro.html">
 * Introduction to this package. </a>]
 *
 * @author Doug Lea (Original author)
 */
public class WaitableShort extends SynchronizedInt16 {

    /**
     * Make a new WaitableShort with the given initial value, and using its own
     * internal lock.
   *
     */
    public WaitableShort(short initialValue) {
        super(initialValue);
    }

    /**
     * Make a new WaitableShort with the given initial value, and using the
     * supplied lock.
   *
     */
    public WaitableShort(short initialValue, Object lock) {
        super(initialValue, lock);
    }

    public short set(short newValue) {
        synchronized (lock) {
            lock.notifyAll();
            return super.set(newValue);
        }
    }

    public boolean commit(short assumedValue, short newValue) {
        synchronized (lock) {
            boolean success = super.commit(assumedValue, newValue);
            if (success) {
                lock.notifyAll();
            }
            return success;
        }
    }

    public short increment() {
        synchronized (lock) {
            lock.notifyAll();
            return super.increment();
        }
    }

    public short decrement() {
        synchronized (lock) {
            lock.notifyAll();
            return super.decrement();
        }
    }

    public short add(short amount) {
        synchronized (lock) {
            lock.notifyAll();
            return super.add(amount);
        }
    }

    public short subtract(short amount) {
        synchronized (lock) {
            lock.notifyAll();
            return super.subtract(amount);
        }
    }

    public short multiply(short factor) {
        synchronized (lock) {
            lock.notifyAll();
            return super.multiply(factor);
        }
    }

    public short divide(short factor) {
        synchronized (lock) {
            lock.notifyAll();
            return super.divide(factor);
        }
    }

    /**
     * Set the value to its complement
     *
     * @return the new value
   *
     */
    public short complement() {
        synchronized (lock) {
            value = (short) (~value);
            lock.notifyAll();
            return value;
        }
    }

    /**
     * Set value to value &amp; b.
     *
     * @return the new value
   *
     */
    public short and(short b) {
        synchronized (lock) {
            value = (short) (value & b);
            lock.notifyAll();
            return value;
        }
    }

    /**
     * Set value to value | b.
     *
     * @return the new value
   *
     */
    public short or(short b) {
        synchronized (lock) {
            value = (short) (value | b);
            lock.notifyAll();
            return value;
        }
    }

    /**
     * Set value to value ^ b.
     *
     * @return the new value
   *
     */
    public short xor(short b) {
        synchronized (lock) {
            value = (short) (value ^ b);
            lock.notifyAll();
            return value;
        }
    }

    /**
     * Wait until value equals c, then run action if nonnull. The action is run
     * with the synchronization lock held.
   *
     */
    public void whenEqual(short c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value == c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

    /**
     * wait until value not equal to c, then run action if nonnull. The action
     * is run with the synchronization lock held.
   *
     */
    public void whenNotEqual(short c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value != c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

    /**
     * wait until value less than or equal to c, then run action if nonnull. The
     * action is run with the synchronization lock held.
   *
     */
    public void whenLessEqual(short c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value <= c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

    /**
     * wait until value less than c, then run action if nonnull. The action is
     * run with the synchronization lock held.
   *
     */
    public void whenLess(short c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value < c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

    /**
     * wait until value greater than or equal to c, then run action if nonnull.
     * The action is run with the synchronization lock held.
   *
     */
    public void whenGreaterEqual(short c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value >= c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

    /**
     * wait until value greater than c, then run action if nonnull. The action
     * is run with the synchronization lock held.
   *
     */
    public void whenGreater(short c, Runnable action) throws InterruptedException {
        synchronized (lock) {
            while (!(value > c)) {
                lock.wait();
            }
            if (action != null) {
                action.run();
            }
        }
    }

}
