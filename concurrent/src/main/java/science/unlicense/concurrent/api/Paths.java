
package science.unlicense.concurrent.api;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.encoding.api.io.BufferedInputStream;
import science.unlicense.encoding.api.io.BufferedOutputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.system.ModuleSeeker;

/**
 * Convinient methods for path manipulation.
 *
 * @author Johann Sorel
 */
public final class Paths {

    private Paths(){}

    /**
     * Lists available path formats.
     *
     * @return array of PathFormat, never null but can be empty.
     */
    public static PathFormat[] getFormats(){
        return (PathFormat[]) ModuleSeeker.findServices(PathFormat.class);
    }

    /**
     * Test if given path is relative.
     * A path is considered relative if any :
     * - starts with a .
     * - does not start with a protocol
     *
     * @param strPath URI expected, otherwise considered a local operation system path
     * @return Path
     */
    public static boolean isRelative(Chars strPath){
        for (PathFormat pf : getFormats()) {
            if (pf.isAbsolute() && strPath.startsWith(pf.getPrefix())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Resolve given string to a path.
     * A least the following protocols must be supported :
     * - mod : internal files
     * - file : operation system files
     *
     * If the path do not match pattern 'resolver:path' then it is interpreted as a local system path.
     *
     * @param strPath URI expected, otherwise considered a local operation system path
     * @return Path
     */
    public static Path resolve(Chars strPath){

        if (strPath.getFirstOccurence(':')<0) {
            //local system path
            return resolveSystem(strPath);
        } else {
            return resolveURI(strPath);
        }
    }

    /**
     * Resolve given string URI to a path.
     * A least the following protocols must be supported :
     * - mod : internal files
     * - file : operation system files
     *
     * @param strPath URI expected
     * @return Path
     */
    public static Path resolveURI(Chars strPath){

        Chars[] parts = strPath.split('!');
        if (parts[0].getFirstOccurence(':')<0) {
            throw new InvalidArgumentException("Invalid string path, path pattern must match : 'resolver:path'  but was "+strPath);
        }

        strPath = parts[0];

        final PathFormat[] formats = getFormats();
        Path path = null;
        for (int i=0;i<formats.length;i++){
            if (formats[i].isAbsolute()){
                try {
                    path = formats[i].createResolver(null).resolve(strPath);
                } catch (IOException ex) {
                    Loggers.get().critical(ex);
                }
                if (path != null) break;
            }
        }

        if (parts.length>1){
            //resolve subpaths
            for (int i=1;i<parts.length;i++){
                path = new CrossingPath(path);
                path = path.resolve(parts[i]);
            }
        }

        return path;
    }

    /**
     * Resolve  a local system path.
     *
     * @param strPath local operation system path
     * @return Path
     */
    public static Path resolveSystem(Chars strPath){
        //TODO convert special characters to URI
        return Paths.resolve(new Chars("file:").concat(strPath));
    }


    /**
     * Remove the extension of a path.
     * example : for entry 'test.png' result is 'test'
     *
     * @param name
     * @return
     */
    public static Chars stripExtension(Chars name){
        final int index = name.getFirstOccurence('.');
        if (index>=0){
            return name.truncate(0, index);
        } else {
            return name;
        }
    }

    /**
     * get the extension of a path.
     * example : for entry 'test.png' result is 'png'
     *
     * @param name
     * @return
     */
    public static Chars getExtension(Chars name){
        final int index = name.getFirstOccurence('.');
        if (index>=0){
            return name.truncate(index+1,name.getCharLength());
        } else {
            return name;
        }
    }

    /**
     *
     *
     * @param base
     * @param child
     * @return
     */
    public static Chars relativePath(Path base, Path child){
        final CharBuffer cb = new CharBuffer();
        final Sequence paths = new ArraySequence();

        while (!child.equals(base)){
            paths.add(child);
            child = child.getParent();
        }

        cb.append('.');
        for (int i=paths.getSize()-1;i>=0;i--){
            final Path cp = (Path) paths.get(i);
            cb.append('/');
            cb.append(cp.getName());
        }

        return cb.toChars();
    }

    /**
     *
     * @param candidate
     * @return local system path or
     */
    public static Chars systemPath(Path candidate) {
        final Chars prefix = candidate.getResolver().getPathFormat().getPrefix();
        if (prefix.equals(new Chars("file"))) {
            Chars localPath = candidate.toURI();
            localPath = localPath.truncate(5, -1);
            return localPath;
        }
        return null;
    }

    /**
     * Copy files and directory from source to target.
     *
     * @param source
     * @param target
     */
    public static void copy(Path source, Path target) throws IOException {

        if (source.isContainer()) {
            target.createContainer();

            final Iterator ite = source.getChildren().createIterator();
            while (ite.hasNext()) {
                final Path c = (Path) ite.next();
                final Path t = target.resolve(c.getName());
                copy(c, t);
            }
        } else {
            ByteInputStream in = new BufferedInputStream(source.createInputStream());
            ByteOutputStream out = new BufferedOutputStream(target.createOutputStream());
            IOUtilities.copy(in, out);
            in.dispose();
            out.close();
        }

    }

}
