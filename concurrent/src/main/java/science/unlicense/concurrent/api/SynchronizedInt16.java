/*
  File: SynchronizedShort.java

  Originally written by Doug Lea and released into the public domain.
  This may be used for any purposes whatsoever without acknowledgment.
  Thanks for the assistance and support of Sun Microsystems Labs,
  and everyone contributing, testing, and using this code.

  History:
  Date       Who                What
  19Jun1998  dl               Create public version
  15Apr2003  dl               Removed redundant "synchronized" for multiply()
 */
package science.unlicense.concurrent.api;

import science.unlicense.common.api.Copyable;
import science.unlicense.common.api.Orderable;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;

/**
 * A class useful for offloading synch for short instance variables.
 *
 * <p>
 * [<a href="http://gee.cs.oswego.edu/dl/classes/EDU/oswego/cs/dl/util/concurrent/intro.html">
 * Introduction to this package. </a>]
 *
 * @author Doug Lea (Original author)
 */
public class SynchronizedInt16 extends Synchronizable implements Orderable, Copyable {

    protected short value;

    /**
     * Make a new SynchronizedShort with the given initial value, and using its
     * own internal lock.
   *
     */
    public SynchronizedInt16(short initialValue) {
        super();
        value = initialValue;
    }

    /**
     * Make a new SynchronizedShort with the given initial value, and using the
     * supplied lock.
   *
     */
    public SynchronizedInt16(short initialValue, Object lock) {
        super(lock);
        value = initialValue;
    }

    /**
     * Return the current value
   *
     */
    public final short get() {
        synchronized (lock) {
            return value;
        }
    }

    /**
     * Set to newValue.
     *
     * @return the old value
   *
     */
    public short set(short newValue) {
        synchronized (lock) {
            short old = value;
            value = newValue;
            return old;
        }
    }

    /**
     * Set value to newValue only if it is currently assumedValue.
     *
     * @return true if successful
   *
     */
    public boolean commit(short assumedValue, short newValue) {
        synchronized (lock) {
            boolean success = (assumedValue == value);
            if (success) {
                value = newValue;
            }
            return success;
        }
    }

    /**
     * Atomically swap values with another SynchronizedShort. Uses
     * identityHashCode to avoid deadlock when two SynchronizedShorts attempt to
     * simultaneously swap with each other. (Note: Ordering via identyHashCode
     * is not strictly guaranteed by the language specification to return
     * unique, orderable values, but in practice JVMs rely on them being
     * unique.)
     *
     * @return the new value
   *
     */
    public short swap(SynchronizedInt16 other) {
        if (other == this) {
            return get();
        }
        SynchronizedInt16 fst = this;
        SynchronizedInt16 snd = other;
        if (System.identityHashCode(fst) > System.identityHashCode(snd)) {
            fst = other;
            snd = this;
        }
        synchronized (fst.lock) {
            synchronized (snd.lock) {
                fst.set(snd.set(fst.get()));
                return get();
            }
        }
    }

    /**
     * Increment the value.
     *
     * @return the new value
   *
     */
    public short increment() {
        synchronized (lock) {
            return ++value;
        }
    }

    /**
     * Decrement the value.
     *
     * @return the new value
   *
     */
    public short decrement() {
        synchronized (lock) {
            return --value;
        }
    }

    /**
     * Add amount to value (i.e., set value += amount)
     *
     * @return the new value
   *
     */
    public short add(short amount) {
        synchronized (lock) {
            return value += amount;
        }
    }

    /**
     * Subtract amount from value (i.e., set value -= amount)
     *
     * @return the new value
   *
     */
    public short subtract(short amount) {
        synchronized (lock) {
            return value -= amount;
        }
    }

    /**
     * Multiply value by factor (i.e., set value *= factor)
     *
     * @return the new value
   *
     */
    public short multiply(short factor) {
        synchronized (lock) {
            return value *= factor;
        }
    }

    /**
     * Divide value by factor (i.e., set value /= factor)
     *
     * @return the new value
   *
     */
    public short divide(short factor) {
        synchronized (lock) {
            return value /= factor;
        }
    }

    /**
     * Set the value to the negative of its old value
     *
     * @return the new value
   *
     */
    public short negate() {
        synchronized (lock) {
            value = (short) (-value);
            return value;
        }
    }

    /**
     * Set the value to its complement
     *
     * @return the new value
   *
     */
    public short complement() {
        synchronized (lock) {
            value = (short) (~value);
            return value;
        }
    }

    /**
     * Set value to value &amp; b.
     *
     * @return the new value
   *
     */
    public short and(short b) {
        synchronized (lock) {
            value = (short) (value & b);
            return value;
        }
    }

    /**
     * Set value to value | b.
     *
     * @return the new value
   *
     */
    public short or(short b) {
        synchronized (lock) {
            value = (short) (value | b);
            return value;
        }
    }

    /**
     * Set value to value ^ b.
     *
     * @return the new value
   *
     */
    public short xor(short b) {
        synchronized (lock) {
            value = (short) (value ^ b);
            return value;
        }
    }

    public int order(short other) {
        short val = get();
        return (val < other) ? -1 : (val == other) ? 0 : 1;
    }

    public int order(SynchronizedInt16 other) {
        return order(other.get());
    }

    public int order(Object other) {
        return order((SynchronizedInt16) other);
    }

    public boolean equals(Object other) {
        if (other != null
                && other instanceof SynchronizedInt16) {
            return get() == ((SynchronizedInt16) other).get();
        } else {
            return false;
        }
    }

    public SynchronizedInt16 copy() {
        return new SynchronizedInt16(get());
    }

    public int getHash() {
        return (int) (get());
    }

    public Chars toChars() {
        return Int32.encode(get());
    }

}
