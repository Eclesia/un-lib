

package science.unlicense.concurrent.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.AbstractPath;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public abstract class VirtualPath extends AbstractPath{

    protected final String vuid = "V"+System.identityHashCode(this);

    protected Path parent;

    private VirtualPath() {

    }

    @Override
    public Chars toURI() {
        if (parent != null) {
            return parent.toURI().concat('/').concat(getName());
        } else {
            return new Chars(vuid+":/"+getName());
        }
    }

    public static VirtualPath.Ram onRam(Chars name) {
        return new Ram(name);
    }

    public static VirtualPath decorate(Path path, Chars name) {
        return new Decorate(name, path);
    }

    public static class Ram extends VirtualPath {

        private final Chars name;
        private final Sequence children = new ArraySequence();

        private boolean isContainer = false;
        private boolean exist = false;
        private ByteSequence data = null;

        public Ram(Chars name) {
            this.name = name;
        }

        public void addChildren(int index, Path[] children) {
            for (Path p : children) {
                ((VirtualPath) p).parent = this;
            }
            this.children.addAll(index, new ArraySequence(children));
        }

        public void removeChild(int index) {
            children.remove(index);
        }

        @Override
        public Sequence getChildren() {
            return Collections.readOnlySequence(children);
        }

        @Override
        public Class[] getEventClasses() {
            return new Class[]{};
        }

        @Override
        public Chars getName() {
            return name;
        }

        @Override
        public Path getParent() {
            return parent;
        }

        @Override
        public boolean isContainer() throws IOException {
            return isContainer;
        }

        @Override
        public boolean exists() throws IOException {
            return exist;
        }

        @Override
        public Object getPathInfo(Chars key) {
            if (!isContainer && Path.INFO_OCTETSIZE.equals(key) && data!=null) {
                return data.getSize();
            }
            return super.getPathInfo(key);
        }

        @Override
        public boolean createContainer() throws IOException {
            if (exist) {
                if (isContainer) {
                    return true;
                } else {
                    throw new IOException(this, "Path already exist and is not a container.");
                }
            }
            exist = true;
            isContainer = true;
            return true;
        }

        @Override
        public boolean createLeaf() throws IOException {
            if (exist) {
                if (isContainer) {
                    throw new IOException(this, "Path already exist and is a container.");
                } else {
                    return true;
                }
            }
            exist = true;
            isContainer = false;
            data = new ByteSequence();
            return true;
        }

        @Override
        public Path resolve(Chars address) {
            Chars[] segments = address.split('/');
            Path base = this;

            loopsegment:
            for (int i=0;i<segments.length;i++){
                if (segments[i].isEmpty() || segments[i].equals(new Chars("."))) {
                    //do nothing
                } else if (segments[i].equals(new Chars(".."))){
                    base = base.getParent();
                } else {
                    //try to find children
                    Iterator ite = base.getChildren().createIterator();
                    while (ite.hasNext()){
                        Path next = (Path) ite.next();
                        if (next.getName().equals(segments[i])){
                            base = next;
                            continue loopsegment;
                        }
                    }
                    //segment do not exist create it.
                    final VirtualPath p = VirtualPath.onRam(segments[i]);
                    p.parent = base;
                    ((VirtualPath.Ram) base).addChildren(0, new Path[]{p});
                    base = p;
                }
            }

            return base;
        }

        @Override
        public PathResolver getResolver() {
            throw new UnimplementedException("Not supported.");
        }

        @Override
        public ByteInputStream createInputStream() throws IOException {
            if (exist && !isContainer){
                return new ArrayInputStream(data.toArrayByte());
            } else {
                throw new IOException(this, "Path do not exist");
            }
        }

        @Override
        public ByteOutputStream createOutputStream() throws IOException {
            if (exist) {
                if (isContainer) {
                    throw new IOException(this, "Path is a container.");
                } else {
                    return new ArrayOutputStream(data);
                }
            } else {
                createLeaf();
                return new ArrayOutputStream(data);
            }
        }
    }

    private static class Decorate extends VirtualPath {

        private final Chars name;
        private final Path path;

        public Decorate(Chars name, Path path) {
            this.name = (name == null) ? path.getName() : name;
            this.path = path;
        }

        @Override
        public Sequence getChildren() {
            return Collections.emptySequence();
        }

        @Override
        public Class[] getEventClasses() {
            return new Class[]{};
        }

        @Override
        public Chars getName() {
            return name;
        }

        @Override
        public Path getParent() {
            return parent;
        }

        @Override
        public boolean isContainer() throws IOException {
            return path.isContainer();
        }

        @Override
        public boolean exists() throws IOException {
            return path.exists();
        }

        @Override
        public Object getPathInfo(Chars key) {
            return path.getPathInfo(key);
        }

        @Override
        public boolean createContainer() throws IOException {
            throw new IOException(this, "Can not modify structure of a decorated virtual path.");
        }

        @Override
        public boolean createLeaf() throws IOException {
            throw new IOException(this, "Can not modify structure of a decorated virtual path.");
        }

        @Override
        public Path resolve(Chars address) {
            throw new RuntimeException("todo");
        }

        @Override
        public PathResolver getResolver() {
            throw new UnimplementedException("Not supported.");
        }

        @Override
        public ByteInputStream createInputStream() throws IOException {
            return path.createInputStream();
        }

        @Override
        public ByteOutputStream createOutputStream() throws IOException {
            return path.createOutputStream();
        }
    }
}
