/*
  File: SynchronizedFloat.java

  Originally written by Doug Lea and released into the public domain.
  This may be used for any purposes whatsoever without acknowledgment.
  Thanks for the assistance and support of Sun Microsystems Labs,
  and everyone contributing, testing, and using this code.

  History:
  Date       Who                What
  19Jun1998  dl               Create public version
  15Apr2003  dl               Removed redundant "synchronized" for multiply()
 */
package science.unlicense.concurrent.api;

import science.unlicense.common.api.Copyable;
import science.unlicense.common.api.Orderable;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;

/**
 * A class useful for offloading synch for float instance variables.
 *
 * <p>
 * [<a href="http://gee.cs.oswego.edu/dl/classes/EDU/oswego/cs/dl/util/concurrent/intro.html">
 * Introduction to this package. </a>]
 *
 * @author Doug Lea (Original author)
 */
public class SynchronizedFloat32 extends Synchronizable implements  Orderable, Copyable {

    protected float value;

    /**
     * Make a new SynchronizedFloat with the given initial value, and using its
     * own internal lock.
   *
     */
    public SynchronizedFloat32(float initialValue) {
        super();
        value = initialValue;
    }

    /**
     * Make a new SynchronizedFloat with the given initial value, and using the
     * supplied lock.
   *
     */
    public SynchronizedFloat32(float initialValue, Object lock) {
        super(lock);
        value = initialValue;
    }

    /**
     * Return the current value
   *
     */
    public final float get() {
        synchronized (lock) {
            return value;
        }
    }

    /**
     * Set to newValue.
     *
     * @return the old value
   *
     */
    public float set(float newValue) {
        synchronized (lock) {
            float old = value;
            value = newValue;
            return old;
        }
    }

    /**
     * Set value to newValue only if it is currently assumedValue.
     *
     * @return true if successful
   *
     */
    public boolean commit(float assumedValue, float newValue) {
        synchronized (lock) {
            boolean success = (assumedValue == value);
            if (success) {
                value = newValue;
            }
            return success;
        }
    }

    /**
     * Atomically swap values with another SynchronizedFloat. Uses
     * identityHashCode to avoid deadlock when two SynchronizedFloats attempt to
     * simultaneously swap with each other. (Note: Ordering via identyHashCode
     * is not strictly guaranteed by the language specification to return
     * unique, orderable values, but in practice JVMs rely on them being
     * unique.)
     *
     * @return the new value
   *
     */
    public float swap(SynchronizedFloat32 other) {
        if (other == this) {
            return get();
        }
        SynchronizedFloat32 fst = this;
        SynchronizedFloat32 snd = other;
        if (System.identityHashCode(fst) > System.identityHashCode(snd)) {
            fst = other;
            snd = this;
        }
        synchronized (fst.lock) {
            synchronized (snd.lock) {
                fst.set(snd.set(fst.get()));
                return get();
            }
        }
    }

    /**
     * Add amount to value (i.e., set value += amount)
     *
     * @return the new value
   *
     */
    public float add(float amount) {
        synchronized (lock) {
            return value += amount;
        }
    }

    /**
     * Subtract amount from value (i.e., set value -= amount)
     *
     * @return the new value
   *
     */
    public float subtract(float amount) {
        synchronized (lock) {
            return value -= amount;
        }
    }

    /**
     * Multiply value by factor (i.e., set value *= factor)
     *
     * @return the new value
   *
     */
    public float multiply(float factor) {
        synchronized (lock) {
            return value *= factor;
        }
    }

    /**
     * Divide value by factor (i.e., set value /= factor)
     *
     * @return the new value
   *
     */
    public float divide(float factor) {
        synchronized (lock) {
            return value /= factor;
        }
    }

    public int order(float other) {
        float val = get();
        return (val < other) ? -1 : (val == other) ? 0 : 1;
    }

    public int order(SynchronizedFloat32 other) {
        return order(other.get());
    }

    public int order(Object other) {
        return order((SynchronizedFloat32) other);
    }

    public boolean equals(Object other) {
        if (other != null
                && other instanceof SynchronizedFloat32) {
            return get() == ((SynchronizedFloat32) other).get();
        } else {
            return false;
        }
    }

    public SynchronizedFloat32 copy() {
        return new SynchronizedFloat32(get());
    }

    public int getHash() {
        return Float.floatToIntBits(get());
    }

    public Chars toChars() {
        return Float64.encode(get());
    }

}
