/*
  File: SynchronizedLong.java

  Originally written by Doug Lea and released into the public domain.
  This may be used for any purposes whatsoever without acknowledgment.
  Thanks for the assistance and support of Sun Microsystems Labs,
  and everyone contributing, testing, and using this code.

  History:
  Date       Who                What
  19Jun1998  dl               Create public version
  15Apr2003  dl               Removed redundant "synchronized" for multiply()
  23jan04    dl               synchronize self-swap case for swap
 */
package science.unlicense.concurrent.api;

import science.unlicense.common.api.Copyable;
import science.unlicense.common.api.Orderable;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int64;

/**
 * A class useful for offloading synch for long instance variables.
 *
 * <p>
 * [<a href="http://gee.cs.oswego.edu/dl/classes/EDU/oswego/cs/dl/util/concurrent/intro.html">
 * Introduction to this package. </a>]
 *
 * @author Doug Lea (Original author)
 */
public class SynchronizedInt64 extends Synchronizable implements Orderable, Copyable {

    protected long value;

    /**
     * Make a new SynchronizedLong with the given initial value, and using its
     * own internal lock.
   *
     */
    public SynchronizedInt64(long initialValue) {
        super();
        value = initialValue;
    }

    /**
     * Make a new SynchronizedLong with the given initial value, and using the
     * supplied lock.
   *
     */
    public SynchronizedInt64(long initialValue, Object lock) {
        super(lock);
        value = initialValue;
    }

    /**
     * Return the current value
   *
     */
    public final long get() {
        synchronized (lock) {
            return value;
        }
    }

    /**
     * Set to newValue.
     *
     * @return the old value
   *
     */
    public long set(long newValue) {
        synchronized (lock) {
            long old = value;
            value = newValue;
            return old;
        }
    }

    /**
     * Set value to newValue only if it is currently assumedValue.
     *
     * @return true if successful
   *
     */
    public boolean commit(long assumedValue, long newValue) {
        synchronized (lock) {
            boolean success = (assumedValue == value);
            if (success) {
                value = newValue;
            }
            return success;
        }
    }

    /**
     * Atomically swap values with another SynchronizedLong. Uses
     * identityHashCode to avoid deadlock when two SynchronizedLongs attempt to
     * simultaneously swap with each other.
     *
     * @return the new value
   *
     */
    public long swap(SynchronizedInt64 other) {
        if (other == this) {
            return get();
        }
        SynchronizedInt64 fst = this;
        SynchronizedInt64 snd = other;
        if (System.identityHashCode(fst) > System.identityHashCode(snd)) {
            fst = other;
            snd = this;
        }
        synchronized (fst.lock) {
            synchronized (snd.lock) {
                fst.set(snd.set(fst.get()));
                return get();
            }
        }
    }

    /**
     * Increment the value.
     *
     * @return the new value
   *
     */
    public long increment() {
        synchronized (lock) {
            return ++value;
        }
    }

    /**
     * Decrement the value.
     *
     * @return the new value
   *
     */
    public long decrement() {
        synchronized (lock) {
            return --value;
        }
    }

    /**
     * Add amount to value (i.e., set value += amount)
     *
     * @return the new value
   *
     */
    public long add(long amount) {
        synchronized (lock) {
            return value += amount;
        }
    }

    /**
     * Subtract amount from value (i.e., set value -= amount)
     *
     * @return the new value
   *
     */
    public long subtract(long amount) {
        synchronized (lock) {
            return value -= amount;
        }
    }

    /**
     * Multiply value by factor (i.e., set value *= factor)
     *
     * @return the new value
   *
     */
    public long multiply(long factor) {
        synchronized (lock) {
            return value *= factor;
        }
    }

    /**
     * Divide value by factor (i.e., set value /= factor)
     *
     * @return the new value
   *
     */
    public long divide(long factor) {
        synchronized (lock) {
            return value /= factor;
        }
    }

    /**
     * Set the value to the negative of its old value
     *
     * @return the new value
   *
     */
    public long negate() {
        synchronized (lock) {
            value = -value;
            return value;
        }
    }

    /**
     * Set the value to its complement
     *
     * @return the new value
   *
     */
    public long complement() {
        synchronized (lock) {
            value = ~value;
            return value;
        }
    }

    /**
     * Set value to value &amp; b.
     *
     * @return the new value
   *
     */
    public long and(long b) {
        synchronized (lock) {
            value = value & b;
            return value;
        }
    }

    /**
     * Set value to value | b.
     *
     * @return the new value
   *
     */
    public long or(long b) {
        synchronized (lock) {
            value = value | b;
            return value;
        }
    }

    /**
     * Set value to value ^ b.
     *
     * @return the new value
   *
     */
    public long xor(long b) {
        synchronized (lock) {
            value = value ^ b;
            return value;
        }
    }

    public int compareTo(long other) {
        long val = get();
        return (val < other) ? -1 : (val == other) ? 0 : 1;
    }

    public int order(SynchronizedInt64 other) {
        return order(other.get());
    }

    public int order(Object other) {
        return order((SynchronizedInt64) other);
    }

    public boolean equals(Object other) {
        if (other != null
                && other instanceof SynchronizedInt64) {
            return get() == ((SynchronizedInt64) other).get();
        } else {
            return false;
        }
    }

    public SynchronizedInt64 copy() {
        return new SynchronizedInt64(get());
    }

    public int getHash() { // same expression as java.lang.Long
        long v = get();
        return (int) (v ^ (v >> 32));
    }

    public Chars toChars() {
        return Int64.encode(get());
    }

}
