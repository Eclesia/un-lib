/*
  File: SynchronizedDouble.java

  Originally written by Doug Lea and released into the public domain.
  This may be used for any purposes whatsoever without acknowledgment.
  Thanks for the assistance and support of Sun Microsystems Labs,
  and everyone contributing, testing, and using this code.

  History:
  Date       Who                What
  19Jun1998  dl               Create public version
  15Apr2003  dl               Removed redundant "synchronized" for multiply()
 */
package science.unlicense.concurrent.api;

import science.unlicense.common.api.Copyable;
import science.unlicense.common.api.Orderable;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;

/**
 * A class useful for offloading synch for double instance variables.
 *
 * <p>
 * [<a href="http://gee.cs.oswego.edu/dl/classes/EDU/oswego/cs/dl/util/concurrent/intro.html">
 * Introduction to this package. </a>]
 *
 * @author Doug Lea (Original author)
 */
public class SynchronizedFloat64 extends Synchronizable implements Orderable, Copyable {

    protected double value;

    /**
     * Make a new SynchronizedDouble with the given initial value, and using its
     * own internal lock.
   *
     */
    public SynchronizedFloat64(double initialValue) {
        super();
        value = initialValue;
    }

    /**
     * Make a new SynchronizedDouble with the given initial value, and using the
     * supplied lock.
   *
     */
    public SynchronizedFloat64(double initialValue, Object lock) {
        super(lock);
        value = initialValue;
    }

    /**
     * Return the current value
   *
     */
    public final double get() {
        synchronized (lock) {
            return value;
        }
    }

    /**
     * Set to newValue.
     *
     * @return the old value
   *
     */
    public double set(double newValue) {
        synchronized (lock) {
            double old = value;
            value = newValue;
            return old;
        }
    }

    /**
     * Set value to newValue only if it is currently assumedValue.
     *
     * @return true if successful
   *
     */
    public boolean commit(double assumedValue, double newValue) {
        synchronized (lock) {
            boolean success = (assumedValue == value);
            if (success) {
                value = newValue;
            }
            return success;
        }
    }

    /**
     * Atomically swap values with another SynchronizedDouble. Uses
     * identityHashCode to avoid deadlock when two SynchronizedDoubles attempt
     * to simultaneously swap with each other. (Note: Ordering via
     * identyHashCode is not strictly guaranteed by the language specification
     * to return unique, orderable values, but in practice JVMs rely on them
     * being unique.)
     *
     * @return the new value
   *
     */
    public double swap(SynchronizedFloat64 other) {
        if (other == this) {
            return get();
        }
        SynchronizedFloat64 fst = this;
        SynchronizedFloat64 snd = other;
        if (System.identityHashCode(fst) > System.identityHashCode(snd)) {
            fst = other;
            snd = this;
        }
        synchronized (fst.lock) {
            synchronized (snd.lock) {
                fst.set(snd.set(fst.get()));
                return get();
            }
        }
    }

    /**
     * Add amount to value (i.e., set value += amount)
     *
     * @return the new value
   *
     */
    public double add(double amount) {
        synchronized (lock) {
            return value += amount;
        }
    }

    /**
     * Subtract amount from value (i.e., set value -= amount)
     *
     * @return the new value
   *
     */
    public double subtract(double amount) {
        synchronized (lock) {
            return value -= amount;
        }
    }

    /**
     * Multiply value by factor (i.e., set value *= factor)
     *
     * @return the new value
   *
     */
    public double multiply(double factor) {
        synchronized (lock) {
            return value *= factor;
        }
    }

    /**
     * Divide value by factor (i.e., set value /= factor)
     *
     * @return the new value
   *
     */
    public double divide(double factor) {
        synchronized (lock) {
            return value /= factor;
        }
    }

    public int order(double other) {
        double val = get();
        return (val < other) ? -1 : (val == other) ? 0 : 1;
    }

    public int order(SynchronizedFloat64 other) {
        return order(other.get());
    }

    public int order(Object other) {
        return order((SynchronizedFloat64) other);
    }

    public boolean equals(Object other) {
        if (other != null
                && other instanceof SynchronizedFloat64) {
            return get() == ((SynchronizedFloat64) other).get();
        } else {
            return false;
        }
    }

    public SynchronizedFloat64 copy() {
        return new SynchronizedFloat64(get());
    }

    public int getHash() { // same hash as Double
        long bits = Double.doubleToLongBits(get());
        return (int) (bits ^ (bits >> 32));
    }

    public Chars toChars() {
        return Float64.encode(get());
    }

}
