/*
  File: SynchronizedInt.java

  Originally written by Doug Lea and released into the public domain.
  This may be used for any purposes whatsoever without acknowledgment.
  Thanks for the assistance and support of Sun Microsystems Labs,
  and everyone contributing, testing, and using this code.

  History:
  Date       Who                What
  19Jun1998  dl               Create public version
  15Apr2003  dl               Removed redundant "synchronized" for multiply()
 */
package science.unlicense.concurrent.api;

import science.unlicense.common.api.Copyable;
import science.unlicense.common.api.Orderable;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;

/**
 * A class useful for offloading synch for int instance variables.
 *
 * <p>
 * [<a href="http://gee.cs.oswego.edu/dl/classes/EDU/oswego/cs/dl/util/concurrent/intro.html">
 * Introduction to this package. </a>]
 *
 * @author Doug Lea (Original author)
 */
public class SynchronizedInt32 extends Synchronizable implements Orderable, Copyable {

    protected int value;

    /**
     * Make a new SynchronizedInt with the given initial value, and using its
     * own internal lock.
   *
     */
    public SynchronizedInt32(int initialValue) {
        super();
        value = initialValue;
    }

    /**
     * Make a new SynchronizedInt with the given initial value, and using the
     * supplied lock.
   *
     */
    public SynchronizedInt32(int initialValue, Object lock) {
        super(lock);
        value = initialValue;
    }

    /**
     * Return the current value
   *
     */
    public final int get() {
        synchronized (lock) {
            return value;
        }
    }

    /**
     * Set to newValue.
     *
     * @return the old value
   *
     */
    public int set(int newValue) {
        synchronized (lock) {
            int old = value;
            value = newValue;
            return old;
        }
    }

    /**
     * Set value to newValue only if it is currently assumedValue.
     *
     * @return true if successful
   *
     */
    public boolean commit(int assumedValue, int newValue) {
        synchronized (lock) {
            boolean success = (assumedValue == value);
            if (success) {
                value = newValue;
            }
            return success;
        }
    }

    /**
     * Atomically swap values with another SynchronizedInt. Uses
     * identityHashCode to avoid deadlock when two SynchronizedInts attempt to
     * simultaneously swap with each other. (Note: Ordering via identyHashCode
     * is not strictly guaranteed by the language specification to return
     * unique, orderable values, but in practice JVMs rely on them being
     * unique.)
     *
     * @return the new value
   *
     */
    public int swap(SynchronizedInt32 other) {
        if (other == this) {
            return get();
        }
        SynchronizedInt32 fst = this;
        SynchronizedInt32 snd = other;
        if (System.identityHashCode(fst) > System.identityHashCode(snd)) {
            fst = other;
            snd = this;
        }
        synchronized (fst.lock) {
            synchronized (snd.lock) {
                fst.set(snd.set(fst.get()));
                return get();
            }
        }
    }

    /**
     * Increment the value.
     *
     * @return the new value
   *
     */
    public int increment() {
        synchronized (lock) {
            return ++value;
        }
    }

    /**
     * Decrement the value.
     *
     * @return the new value
   *
     */
    public int decrement() {
        synchronized (lock) {
            return --value;
        }
    }

    /**
     * Add amount to value (i.e., set value += amount)
     *
     * @return the new value
   *
     */
    public int add(int amount) {
        synchronized (lock) {
            return value += amount;
        }
    }

    /**
     * Subtract amount from value (i.e., set value -= amount)
     *
     * @return the new value
   *
     */
    public int subtract(int amount) {
        synchronized (lock) {
            return value -= amount;
        }
    }

    /**
     * Multiply value by factor (i.e., set value *= factor)
     *
     * @return the new value
   *
     */
    public int multiply(int factor) {
        synchronized (lock) {
            return value *= factor;
        }
    }

    /**
     * Divide value by factor (i.e., set value /= factor)
     *
     * @return the new value
   *
     */
    public int divide(int factor) {
        synchronized (lock) {
            return value /= factor;
        }
    }

    /**
     * Set the value to the negative of its old value
     *
     * @return the new value
   *
     */
    public int negate() {
        synchronized (lock) {
            value = -value;
            return value;
        }
    }

    /**
     * Set the value to its complement
     *
     * @return the new value
   *
     */
    public int complement() {
        synchronized (lock) {
            value = ~value;
            return value;
        }
    }

    /**
     * Set value to value &amp; b.
     *
     * @return the new value
   *
     */
    public int and(int b) {
        synchronized (lock) {
            value = value & b;
            return value;
        }
    }

    /**
     * Set value to value | b.
     *
     * @return the new value
   *
     */
    public int or(int b) {
        synchronized (lock) {
            value = value | b;
            return value;
        }
    }

    /**
     * Set value to value ^ b.
     *
     * @return the new value
   *
     */
    public int xor(int b) {
        synchronized (lock) {
            value = value ^ b;
            return value;
        }
    }

    public int order(int other) {
        int val = get();
        return (val < other) ? -1 : (val == other) ? 0 : 1;
    }

    public int order(SynchronizedInt32 other) {
        return order(other.get());
    }

    public int order(Object other) {
        return order((SynchronizedInt32) other);
    }

    public boolean equals(Object other) {
        if (other != null
                && other instanceof SynchronizedInt32) {
            return get() == ((SynchronizedInt32) other).get();
        } else {
            return false;
        }
    }

    public SynchronizedInt32 copy() {
        return new SynchronizedInt32(get());
    }

    public int getHash() {
        return get();
    }

    public Chars toChars() {
        return Int32.encode(get());
    }

}
