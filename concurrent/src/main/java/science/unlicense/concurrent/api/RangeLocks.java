
package science.unlicense.concurrent.api;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;

/**
 * Draft class to manage range locks.
 * Those locks are used to fine grain parts of collections or buffers.
 *
 *
 * @author Johann Sorel
 */
public class RangeLocks {

    private final Sequence rlocks = new ArraySequence();
    private final Sequence rwlocks = new ArraySequence();

    public Lock request(long start, long end, boolean write) throws InterruptedException {
        final Lock lock = new Lock(start, end, write);
        if (write) {
            //wait for a free space
            wait:
            synchronized (rwlocks) {
                for (;;) {
                    if (overlaps(rwlocks, lock)) {
                        rwlocks.wait();
                    } else {
                        synchronized (rlocks) {
                            for (;;) {
                                if (overlaps(rlocks, lock)) {
                                    rlocks.wait();
                                } else {
                                    rwlocks.add(lock);
                                    break wait;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            //wait for no more write locks
            wait:
            synchronized (rwlocks) {
                for (;;) {
                    if (overlaps(rwlocks, lock)) {
                        rwlocks.wait();
                    } else {
                        synchronized (rlocks) {
                            rlocks.add(lock);
                            break wait;
                        }
                    }
                }
            }
        }
        return lock;
    }

    public void release(Lock lock) {
        if (lock.write) {
            synchronized (rwlocks) {
                rwlocks.remove(lock);
                rwlocks.notifyAll();
                synchronized (rlocks) {
                    rlocks.notifyAll();
                }
            }

        } else {
            synchronized (rlocks) {
                rlocks.remove(lock);
                rlocks.notifyAll();
            }
        }
    }

    private static boolean overlaps(Sequence locks, Lock lock) {
        for (int i=0,n=locks.getSize();i<n;i++) {
            final Lock l = (Lock) locks.get(i);
            if (l!=lock && l.overlaps(lock)) return true;
        }
        return false;
    }

    private static class Lock {
        private final long start;
        private final long end;
        private final boolean write;

        public Lock(long start, long end, boolean write) {
            this.start = start;
            this.end = end;
            this.write = write;
        }

        public boolean overlaps(Lock lock) {
            return !(end < lock.start || start > lock.end);
        }
    }

}
