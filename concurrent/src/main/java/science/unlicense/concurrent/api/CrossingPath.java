
package science.unlicense.concurrent.api;

import science.unlicense.encoding.api.path.AbstractPath;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.AbstractCollection;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.logging.Loggers;

/**
 * A Path which automatically unwrap sub path resolver, like zip,rar,...
 *
 * TODO : find a better name, this will do for now.
 *
 * @author Johann Sorel
 */
public class CrossingPath extends AbstractPath {

    private final Path base;
    private Path effective;

    public CrossingPath(Path base) {
        this.base = base;
    }

    public Object getPathInfo(Chars key) {
        return base.getPathInfo(key);
    }

    private void checkCrossPath(){
        if (effective!=null) return;

        try {
            if (base.isContainer()){
                effective = base;
            } else {
                //test if it's a sub container
                final PathFormat[] formats = Paths.getFormats();
                for (int i=0;i<formats.length;i++){
                    if (!formats[i].isAbsolute() && formats[i].canCreate(base)){
                        effective = formats[i].createResolver(base).resolve(null);
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            Loggers.get().warning(ex);
        }

        if (effective == null){
            effective = base;
        }
    }

    public Collection getChildren() {
        checkCrossPath();
        return new AbstractCollection() {
            public Iterator createIterator() {
                return new Iterator() {
                    private final Iterator ite = effective.getChildren().createIterator();

                    public boolean hasNext() {
                        return ite.hasNext();
                    }
                    public Object next() {
                        return new CrossingPath((Path) ite.next());
                    }
                    public boolean remove() {
                        return ite.remove();
                    }
                    public void close() {
                        ite.close();
                    }
                };
            }
        };
    }

    public Class[] getEventClasses() {
        checkCrossPath();
        return effective.getEventClasses();
    }

    public Chars getName() {
        return base.getName();
    }

    public Path getParent() {
        final Path parent = base.getParent();
        if (parent == null) return null;
        return new CrossingPath(parent);
    }

    public boolean isContainer() throws IOException {
        checkCrossPath();
        return effective.isContainer();
    }

    public boolean exists() throws IOException {
        checkCrossPath();
        return effective.exists();
    }

    public boolean createContainer() throws IOException {
        return base.createContainer();
    }

    public boolean createLeaf() throws IOException {
        return base.createLeaf();
    }

    public Path resolve(Chars address) {
        checkCrossPath();
        return new CrossingPath(effective.resolve(address));
    }

    public PathResolver getResolver() {
        return base.getResolver();
    }

    public ByteInputStream createInputStream() throws IOException {
        return base.createInputStream();
    }

    public ByteOutputStream createOutputStream() throws IOException {
        return base.createOutputStream();
    }

    public Chars toURI() {
        return base.toURI();
    }

    public Chars toChars() {
        return base.toChars();
    }

}
