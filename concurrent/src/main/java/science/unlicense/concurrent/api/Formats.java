
package science.unlicense.concurrent.api;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.predicate.Predicates;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.system.ModuleSeeker;

/**
 * Format utility class.
 *
 * @author Johann Sorel
 */
public final class Formats {

    private Formats() {}

    /**
     * Convinient method to open an unknowned input.
     * The method will search formats until one can decode the source.
     *
     * @param input
     * @return Store, never null
     * @throws IOException if not format could support the source.
     */
    public static Store open(Object input) throws IOException {
        return open(input, new ArraySequence(getFormats()));
    }

    /**
     * Convinient method to open an unknowned input.
     * The method will search formats until one can decode the source.
     *
     * @param input
     * @param formats formats to test.
     * @return Store, never null
     * @throws IOException if not format could support the source.
     */
    public static Store open(Object input, Collection formats) throws IOException {
        final Sequence matches = findFormat(input, formats);

        if (matches.isEmpty()) {
            throw new IOException(input, "No known format signature or extension matches input.");
        }

        final Iterator ite = matches.createIterator();
        while (ite.hasNext()) {
            final Format format = (Format) ite.next();
            //final check, format may do additional verifiations
            if (format.canDecode(input)) {
                try {
                    return format.open(input);
                } catch (Exception ex) {
                    //can be a lot things
                    Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                }
            }
        }

        throw new IOException(input, "No known format could decode input "+ input);
    }

    /**
     * Lists available formats.
     *
     * @return array of Format, never null but can be empty.
     */
    public static Format[] getFormats(){
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/format/"),
                Predicates.instanceOf(Format.class));
        final Format[] formats = new Format[results.getSize()];
        Collections.copy(results, formats, 0);
        return formats;
    }

    public static Format getFormat(Chars name) throws InvalidArgumentException{
        final Format[] formats = getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i].getIdentifier().equals(name)){
                return formats[i];
            }
        }
        throw new InvalidArgumentException("Format "+name+" not found.");
    }

    public static Format getFormatForExtension(Chars ext) throws InvalidArgumentException{
        final Format[] formats = getFormats();
        for (int i=0;i<formats.length;i++){
            final Iterator ite = formats[i].getExtensions().createIterator();
            while (ite.hasNext()) {
                final CharArray e = (CharArray) ite.next();
                if (e.equals(ext, true, true)){
                    return formats[i];
                }
            }
        }
        throw new InvalidArgumentException("Format "+ext+" not found.");
    }

    /**
     * Find formats who implement given class.
     *
     * @param formatClass
     * @return
     * @throws InvalidArgumentException
     */
    public static Format[] getFormatsOfType(Class formatClass) throws InvalidArgumentException{
        final Format[] formats = getFormats();
        final Sequence valids = new ArraySequence();
        for (int i=0;i<formats.length;i++) {
            if (formatClass.isInstance(formats[i])) {
                valids.add(formats[i]);
            }
        }
        return (Format[]) valids.toArray(formatClass);
    }

    /**
     * Find formats who may support given resource type.
     *
     * @param resourceClass
     * @return
     * @throws InvalidArgumentException
     */
    public static Format[] getFormatsForResourceType(Class resourceClass) throws InvalidArgumentException{
        final Format[] formats = getFormats();
        final Sequence valids = new ArraySequence();
        for (int i=0;i<formats.length;i++){
            final Collection types = formats[i].getResourceTypes();
            if (types.isEmpty()) {
                valids.add(formats[i]);
                continue;
            }

            final Iterator ite = types.createIterator();
            while (ite.hasNext()) {
                final Class c = (Class) ite.next();
                if (resourceClass.isAssignableFrom(c)) {
                    valids.add(formats[i]);
                    continue;
                }
            }
        }
        return (Format[]) valids.toArray(Format.class);
    }

    public static boolean canDecode(Object input) throws IOException {
        return canDecode(input, new ArraySequence(getFormats()));
    }

    public static boolean canDecode(Object input, Collection formats) throws IOException {
        return !Formats.findFormat(input, formats).isEmpty();
    }

    /**
     * Search known formats for one which can support the given path.
     *
     * @param path
     * @return Format, can be null
     */
    public static Format findFormat(Path path){
        final Format[] formats = getFormats();
        for (Format f : formats){
            try{
                if (f.canDecode(path)){
                    return f;
                }
            }catch(IOException ex){
                //ignore this
            }
        }
        return null;
    }

    /**
     * Test given list of formats.
     * Returns the formats who might match based on signature and extension.
     * When testing signatures, the extension will only be used to sort formats.
     *
     * The most likely formats will be placed first in the list.
     *
     * @param input
     * @param formats
     * @return
     * @throws IOException
     */
    public static Sequence findFormat(Object input, Collection formats) throws IOException {
        return findFormat(input, formats, true, true);
    }

    /**
     * Test given list of formats.
     * Returns the formats who might match based on signature and/or extension.
     * When testing signatures, the extension will only be used to sort formats.
     *
     * The most likely formats will be placed first in the list.
     *
     * @param input
     * @param formats
     * @param testSignatures
     * @param testExtensions
     * @return
     * @throws IOException
     */
    public static Sequence findFormat(Object input, Collection formats, boolean testSignatures, boolean testExtensions) throws IOException {
        final Sequence matches = new ArraySequence();

        if (testSignatures) {

            //separate formats, those with signatures first
            final Sequence signedMatches = new ArraySequence();
            final Sequence nosigned = new ArraySequence();

            final boolean[] mustClose = new boolean[1];
            final BacktrackInputStream is = toInputStream(input, mustClose);
            try {
                is.mark();
                byte[] sign = new byte[0];
                final Iterator formatsIte = formats.createIterator();
                formatLoop:
                while (formatsIte.hasNext()) {
                    final Format format = (Format) formatsIte.next();
                    final Sequence signatures = format.getSignature();
                    if (signatures.isEmpty()) {
                        nosigned.add(format);
                    } else {
                        //check signatures
                        final Iterator signaturesIte = signatures.createIterator();
                        while (signaturesIte.hasNext()) {
                            final byte[] signature = (byte[]) signaturesIte.next();
                            if (signature.length > sign.length) {
                                is.rewind();
                                try {
                                    sign = new DataInputStream(is).readBytes(signature.length);
                                } catch (IOException ex) {
                                    //signature too long for file
                                    continue;
                                }
                            }
                            if (Arrays.equals(signature, 0, signature.length, sign, 0)) {
                                signedMatches.add(format);
                                continue formatLoop;
                            }
                        }
                    }
                }
            } finally {
                if (mustClose[0]) {
                    is.dispose();
                }
            }

            if (signedMatches.getSize() > 1) {
                //multiple matches, see if one of them might be better using the extensions
                matches.addAll(findFormat(input, signedMatches, false, true));
                //we don't exclude the others, extension is not completely reliable information
                //we only place the most likely one first and remaining ones after
                final Iterator ite = signedMatches.createIterator();
                while (ite.hasNext()) {
                    final Format format = (Format) ite.next();
                    if (!matches.contains(format)) {
                        matches.add(format);
                    }
                }
            } else {
                // 1 or 0 match
                matches.addAll(signedMatches);
            }

            //to be tested for extension
            formats = nosigned;
        }

        if (testExtensions && input instanceof Path) {
            final Chars name = ((Path) input).getName().toLowerCase();
            final Chars dot = new Chars(".");

            final Iterator formatsIte = formats.createIterator();
            formatLoop:
            while (formatsIte.hasNext()) {
                final Format format = (Format) formatsIte.next();
                final Iterator extensionsIte = format.getExtensions().createIterator();
                while (extensionsIte.hasNext()) {
                    final Chars extension = (Chars) extensionsIte.next();
                    if (name.endsWith(dot.concat(extension))) {
                        matches.add(format);
                        continue formatLoop;
                    }
                }
            }
        }

        //make individual tests for subset formats
        for (int i=matches.getSize()-1; i>=0; i--) {
            final Format format = (Format) matches.get(i);
            if (format.isSubsetFormat()) {
                if (!format.canDecode(input)) {
                    matches.remove(i);
                }
            }
        }

        return matches;
    }

    private static BacktrackInputStream toInputStream(Object input, boolean[] mustClose) throws IOException{
        BacktrackInputStream stream;
        if (input instanceof ByteInputStream){
            stream = new BacktrackInputStream((ByteInputStream) input);
            //we did not create the stream, we do not close it.
            mustClose[0] = false;
            stream.mark();
            return stream;
        } else if (input instanceof Path){
            stream = new BacktrackInputStream(((Path) input).createInputStream());
            mustClose[0] = true;
            stream.mark();
            return stream;
        } else {
            throw new IOException(input, "Unsupported input");
        }
    }

}
