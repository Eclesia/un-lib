/*
  File: SynchronizedBoolean.java

  Originally written by Doug Lea and released into the public domain.
  This may be used for any purposes whatsoever without acknowledgment.
  Thanks for the assistance and support of Sun Microsystems Labs,
  and everyone contributing, testing, and using this code.

  History:
  Date       Who                What
  19Jun1998  dl               Create public version
 */
package science.unlicense.concurrent.api;

import science.unlicense.common.api.Copyable;
import science.unlicense.common.api.Orderable;
import science.unlicense.common.api.character.Chars;

/**
 * A class useful for offloading synch for boolean instance variables.
 *
 * <p>
 * [<a href="http://gee.cs.oswego.edu/dl/classes/EDU/oswego/cs/dl/util/concurrent/intro.html">
 * Introduction to this package. </a>]
 *
 *  @author Doug Lea (Original author)
 */
public class SynchronizedBoolean extends Synchronizable implements Orderable, Copyable {

    protected boolean value;

    /**
     * Make a new SynchronizedBoolean with the given initial value, and using
     * its own internal lock.
   *
     */
    public SynchronizedBoolean(boolean initialValue) {
        super();
        value = initialValue;
    }

    /**
     * Make a new SynchronizedBoolean with the given initial value, and using
     * the supplied lock.
   *
     */
    public SynchronizedBoolean(boolean initialValue, Object lock) {
        super(lock);
        value = initialValue;
    }

    /**
     * Return the current value
   *
     */
    public final boolean get() {
        synchronized (lock) {
            return value;
        }
    }

    /**
     * Set to newValue.
     *
     * @return the old value
   *
     */
    public boolean set(boolean newValue) {
        synchronized (lock) {
            boolean old = value;
            value = newValue;
            return old;
        }
    }

    /**
     * Set value to newValue only if it is currently assumedValue.
     *
     * @return true if successful
   *
     */
    public boolean commit(boolean assumedValue, boolean newValue) {
        synchronized (lock) {
            boolean success = (assumedValue == value);
            if (success) {
                value = newValue;
            }
            return success;
        }
    }

    /**
     * Atomically swap values with another SynchronizedBoolean. Uses
     * identityHashCode to avoid deadlock when two SynchronizedBooleans attempt
     * to simultaneously swap with each other. (Note: Ordering via
     * identyHashCode is not strictly guaranteed by the language specification
     * to return unique, orderable values, but in practice JVMs rely on them
     * being unique.)
     *
     * @return the new value
   *
     */
    public boolean swap(SynchronizedBoolean other) {
        if (other == this) {
            return get();
        }
        SynchronizedBoolean fst = this;
        SynchronizedBoolean snd = other;
        if (System.identityHashCode(fst) > System.identityHashCode(snd)) {
            fst = other;
            snd = this;
        }
        synchronized (fst.lock) {
            synchronized (snd.lock) {
                fst.set(snd.set(fst.get()));
                return get();
            }
        }
    }

    /**
     * Set the value to its complement
     *
     * @return the new value
   *
     */
    public boolean complement() {
        synchronized (lock) {
            value = !value;
            return value;
        }
    }

    /**
     * Set value to value &amp; b.
     *
     * @return the new value
   *
     */
    public boolean and(boolean b) {
        synchronized (lock) {
            value = value & b;
            return value;
        }
    }

    /**
     * Set value to value | b.
     *
     * @return the new value
   *
     */
    public boolean or(boolean b) {
        synchronized (lock) {
            value = value | b;
            return value;
        }
    }

    /**
     * Set value to value ^ b.
     *
     * @return the new value
   *
     */
    public boolean xor(boolean b) {
        synchronized (lock) {
            value = value ^ b;
            return value;
        }
    }

    public int order(boolean other) {
        boolean val = get();
        return (val == other) ? 0 : (val) ? 1 : -1;
    }

    public int order(SynchronizedBoolean other) {
        return order(other.get());
    }

    public int order(Object other) {
        return order((SynchronizedBoolean) other);
    }

    public boolean equals(Object other) {
        if (other != null
                && other instanceof SynchronizedBoolean) {
            return get() == ((SynchronizedBoolean) other).get();
        } else {
            return false;
        }
    }

    public SynchronizedBoolean copy() {
        return new SynchronizedBoolean(get());
    }

    public int getHash() {
        boolean b = get();
        return (b) ? 3412688 : 8319343; // entirely arbitrary
    }

    public Chars toChars() {
        return new Chars(""+get());
    }

}
