

package science.unlicense.format.avi;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.avi.chunk.BitMapChunk;
import science.unlicense.format.avi.chunk.BitmapInfoHeader;
import science.unlicense.format.avi.chunk.StrfChunk;
import science.unlicense.format.avi.chunk.StrhChunk;
import science.unlicense.format.riff.RIFFElement;
import science.unlicense.format.riff.RIFFReader;
import science.unlicense.format.riff.model.Chunk;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.media.api.MediaReadStream;

/**
 *
 * @author Johann Sorel
 */
public class AVIMediaReader implements MediaReadStream {

    private final RIFFReader reader;

    private StrfChunk strf;
    private StrhChunk strh;
    private Sequence imageChunks = new ArraySequence();
    private int index=0;

    public AVIMediaReader(RIFFReader reader) throws IOException {
        this.reader = reader;

        //collect all chunks
        while (reader.hasNext()){
            final RIFFElement ele = reader.next();
            if (ele.getType() == RIFFElement.TYPE_CHUNK){
                final Chunk chunk = ele.getChunk();
                if (chunk instanceof StrfChunk){
                    strf = (StrfChunk) chunk;
                } else if (chunk instanceof StrhChunk){
                    strh = (StrhChunk) chunk;
                } else if (chunk instanceof BitMapChunk){
                    imageChunks.add(chunk);
                }
            }
        }

    }

    @Override
    public MediaPacket next() throws IOException {
        if (index < imageChunks.getSize()){
            final BitMapChunk chunk = (BitMapChunk) imageChunks.get(index);

            final byte[] data = chunk.data;
            final BitmapInfoHeader header = strf.bitmapHeader;

            if (AVIConstants.COMPRESSION_NONE.equals(header.biCompression) ||
               AVIConstants.COMPRESSION_RGB.equals( header.biCompression) ||
               AVIConstants.COMPRESSION_RAW.equals( header.biCompression) ){

            } else {
                throw new IOException(reader, "Unsupported compression : "+header.biCompression);
            }


            //prepare next iteration
            index++;
        }
        return null;
    }

    @Override
    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
