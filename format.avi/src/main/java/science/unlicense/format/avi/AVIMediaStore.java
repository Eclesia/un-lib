

package science.unlicense.format.avi;

import science.unlicense.format.riff.RIFFElement;
import science.unlicense.format.riff.RIFFReader;
import science.unlicense.format.riff.model.Chunk;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;
import science.unlicense.format.avi.chunk.AvihChunk;
import science.unlicense.format.avi.chunk.BitMapChunk;
import science.unlicense.format.avi.chunk.StrfChunk;
import science.unlicense.format.avi.chunk.StrhChunk;

/**
 *
 * @author Johann Sorel
 */
public class AVIMediaStore extends AbstractStore implements Media {

    private final Path input;

    public AVIMediaStore(Path input) {
        super(AVIFormat.INSTANCE,input);
        this.input = input;
    }

    private void analyze(){
        try{
            final RIFFReader reader = createRiffReader();
            while (reader.hasNext()){
                final RIFFElement element = reader.next();
                System.out.println(element.getChunk());
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public Track[] getTracks() throws IOException {
        analyze();
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        return new AVIMediaReader(createRiffReader());
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public RIFFReader createRiffReader() throws IOException{
        final Dictionary types = new HashDictionary();
        types.add(AVIConstants.TYPE_AVIHEADER, AvihChunk.class);
        types.add(AVIConstants.TYPE_STREAMHEADER, StrhChunk.class);
        types.add(AVIConstants.TYPE_FORMATHEADER, StrfChunk.class);
        types.add(AVIConstants.TYPE_BITMAP, BitMapChunk.class);
        final AVIRiffReader reader = new AVIRiffReader(types);
        reader.setInput(input);
        return reader;
    }

    private static class AVIRiffReader extends RIFFReader{

        private StrhChunk streamHeader;

        private AVIRiffReader(Dictionary dico){
            super(dico);
        }

        protected Chunk createChunk(Chars fourCC) throws IOException {
            Chunk chunk = super.createChunk(fourCC);
            if (chunk instanceof StrhChunk){
                streamHeader = (StrhChunk) chunk;
            } else if (chunk instanceof StrfChunk){
                ((StrfChunk) chunk).type = streamHeader;
            }
            return chunk;
        }

    }

}
