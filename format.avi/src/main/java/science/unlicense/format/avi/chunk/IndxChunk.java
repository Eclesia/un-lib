

package science.unlicense.format.avi.chunk;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.avi.AVIConstants;

/**
 *
 * @author Johann Sorel
 */
public class IndxChunk extends DefaultChunk {

    public int ckid;
    public int dwFlags;
    public int dwChunkOffset;
    public int dwChunkLength;

    public IndxChunk() {
        super(AVIConstants.TYPE_INDEX);
    }

    public void readInternal(DataInputStream ds) throws IOException {
        ckid = ds.readInt();
        dwFlags = ds.readInt();
        dwChunkOffset = ds.readInt();
        dwChunkLength = ds.readInt();
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeInt(ckid);
        ds.writeInt(dwFlags);
        ds.writeInt(dwChunkOffset);
        ds.writeInt(dwChunkLength);
    }

}
