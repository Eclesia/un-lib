
package science.unlicense.format.avi.chunk;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.avi.AVIConstants;

/**
 * http://msdn.microsoft.com/en-us/library/windows/desktop/dd318183(v=vs.85).aspx
 *
 * @author Johann Sorel
 */
public class StrhChunk extends DefaultChunk {

    public Chars fccType;
    public Chars fccHandler;
    public int dwFlags;
    public short wPriority;
    public short wLanguage;
    public int dwInitialFrames;
    public int dwScale;
    public int dwRate;
    public int dwStart;
    public int dwLength;
    public int dwSuggestedBufferSize;
    public int dwQuality;
    public int dwSampleSize;
    public int rcFrameleft;
    public int rcFrametop;
    public int rcFrameright;
    public int rcFramebottom;

    public StrhChunk() {
        super(AVIConstants.TYPE_STREAMHEADER);
        setSize(56);
    }

    public void readInternal(DataInputStream ds) throws IOException {
        fccType = new Chars(ds.readFully(new byte[4]));
        fccHandler = new Chars(ds.readFully(new byte[4]));
        dwFlags = ds.readInt();
        wPriority = ds.readShort();
        wLanguage = ds.readShort();
        dwInitialFrames = ds.readInt();
        dwScale = ds.readInt();
        dwRate = ds.readInt();
        dwStart = ds.readInt();
        dwLength = ds.readInt();
        dwSuggestedBufferSize = ds.readInt();
        dwQuality = ds.readInt();
        dwSampleSize = ds.readInt();
        rcFrameleft = ds.readUShort();
        rcFrametop = ds.readUShort();
        rcFrameright = ds.readUShort();
        rcFramebottom = ds.readUShort();
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.write(fccType.toBytes(CharEncodings.US_ASCII));
        ds.write(fccHandler.toBytes(CharEncodings.US_ASCII));
        ds.writeInt(dwFlags);
        ds.writeShort(wPriority);
        ds.writeShort(wLanguage);
        ds.writeInt(dwInitialFrames);
        ds.writeInt(dwScale);
        ds.writeInt(dwRate);
        ds.writeInt(dwStart);
        ds.writeInt(dwLength);
        ds.writeInt(dwSuggestedBufferSize);
        ds.writeInt(dwQuality);
        ds.writeInt(dwSampleSize);
        ds.writeUShort(rcFrameleft);
        ds.writeUShort(rcFrametop);
        ds.writeUShort(rcFrameright);
        ds.writeUShort(rcFramebottom);
    }

}
