

package science.unlicense.format.avi.chunk;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.avi.AVIConstants;
import static science.unlicense.format.avi.AVIConstants.*;
import science.unlicense.format.riff.model.DefaultChunk;

/**
 *
 * @author Johann Sorel
 */
public class StrfChunk extends DefaultChunk {

    public BitmapInfoHeader bitmapHeader;
    public WaveFormatex waveHeader;

    public StrhChunk type;

    public StrfChunk() {
        super(AVIConstants.TYPE_FORMATHEADER);
    }

    @Override
    public void computeSize() {
        size = 0;
        if (bitmapHeader!=null){
            size = bitmapHeader.computeSize();
        }
        if (waveHeader!=null){
            size = waveHeader.computeSize();
        }
    }

    @Override
    public void readInternal(DataInputStream ds) throws IOException {

        if (STREAM_VIDEO.equals(type.fccType)){
            bitmapHeader = new BitmapInfoHeader();
            bitmapHeader.read(ds);
        } else if (STREAM_AUDIO.equals(type.fccType)){
            waveHeader = new WaveFormatex();
            waveHeader.read(ds);
        } else if (STREAM_MIDI.equals(type.fccType)){
            throw new IOException(ds, "TODO");
        } else if (STREAM_TEXT.equals(type.fccType)){
            throw new IOException(ds, "TODO");
        } else {
            throw new IOException(ds, "unknwoned type : "+ type.fccType);
        }

    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        if (STREAM_VIDEO.equals(type.fccType)){
            bitmapHeader.write(ds);
        } else if (STREAM_AUDIO.equals(type.fccType)){
            waveHeader.write(ds);
        } else if (STREAM_MIDI.equals(type.fccType)){
            throw new IOException(ds, "TODO");
        } else if (STREAM_TEXT.equals(type.fccType)){
            throw new IOException(ds, "TODO");
        } else {
            throw new IOException(ds, "unknwoned type : "+ type.fccType);
        }
    }

}
