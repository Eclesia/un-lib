
package science.unlicense.format.avi;

import science.unlicense.format.riff.RIFFConstants;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaCapabilities;

/**
 *
 * Resources :
 *
 *
 * @author Johann Sorel
 */
public class AVIFormat extends AbstractMediaFormat {

    public static final AVIFormat INSTANCE = new AVIFormat();

    public AVIFormat() {
        super(new Chars("avi"));
        shortName = new Chars("AVI");
        longName = new Chars("Graphics Interchange Format");
        mimeTypes.add(new Chars("video/avi"));
        mimeTypes.add(new Chars("video/msvideo"));
        mimeTypes.add(new Chars("video/x-msvideo"));
        mimeTypes.add(new Chars("image/avi"));
        mimeTypes.add(new Chars("video/xmpg2"));
        mimeTypes.add(new Chars("application/x-troff-msvideo"));
        mimeTypes.add(new Chars("audio/aiff"));
        mimeTypes.add(new Chars("audio/avi"));
        extensions.add(new Chars("avi"));
        signatures.add(RIFFConstants.CHUNK_RIFF.toBytes());
        signatures.add(RIFFConstants.CHUNK_RIFX.toBytes());
        resourceTypes.add(Media.class);
    }

    @Override
    public MediaCapabilities getCapabilities() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object input) {
        return new AVIMediaStore((Path) input);
    }

}
