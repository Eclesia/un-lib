
package science.unlicense.format.avi.chunk;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.avi.AVIConstants.*;

/**
 * http://msdn.microsoft.com/en-us/library/windows/desktop/dd390970(v=vs.85).aspx
 *
 * @author Johann Sorel
 */
public class WaveFormatex {

    public short wFormatTag;
    public short nChannels;
    public int nSamplesPerSec;
    public int nAvgBytesPerSec;
    public short nBlockAlign;
    public short wBitsPerSample;
    public short cbSize;

    public Object audioExt;

    public long computeSize() {
        throw new UnimplementedException("Not supported yet.");
    }

    public void read(DataInputStream ds) throws IOException {

        wFormatTag = ds.readShort();
        nChannels = ds.readShort();
        nSamplesPerSec = ds.readInt();
        nAvgBytesPerSec = ds.readInt();
        nBlockAlign = ds.readShort();
        wBitsPerSample = ds.readShort();
        cbSize = ds.readShort();

        if (wFormatTag == WAVE_FORMAT_PCM){
            throw new IOException(ds, "TODO");
        } else if (wFormatTag == WAVE_FORMAT_IEEE_FLOAT){
            throw new IOException(ds, "TODO");
        } else if (wFormatTag == WAVE_FORMAT_ALAW){
            throw new IOException(ds, "TODO");
        } else if (wFormatTag == WAVE_FORMAT_MULAW){
            throw new IOException(ds, "TODO");
        } else if (wFormatTag == WAVE_FORMAT_MPEG){
            audioExt = new AudioMpeg1();
            ((AudioMpeg1) audioExt).read(ds);
        } else if (wFormatTag == WAVE_FORMAT_MPEGLAYER3){
            audioExt = new AudioMpeg3();
            ((AudioMpeg3) audioExt).read(ds);
        } else if (wFormatTag == WAVE_FORMAT_EXTENSIBLE){
            throw new IOException(ds, "TODO");
        } else {
            throw new IOException(ds, "Unknwoed audio type : "+wFormatTag);
        }

    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeShort(wFormatTag);
        ds.writeShort(nChannels);
        ds.writeInt(nSamplesPerSec);
        ds.writeInt(nAvgBytesPerSec);
        ds.writeShort(nBlockAlign);
        ds.writeShort(wBitsPerSample);
        ds.writeShort(cbSize);

        if (wFormatTag == WAVE_FORMAT_PCM){
            throw new IOException(ds, "TODO");
        } else if (wFormatTag == WAVE_FORMAT_IEEE_FLOAT){
            throw new IOException(ds, "TODO");
        } else if (wFormatTag == WAVE_FORMAT_ALAW){
            throw new IOException(ds, "TODO");
        } else if (wFormatTag == WAVE_FORMAT_MULAW){
            throw new IOException(ds, "TODO");
        } else if (wFormatTag == WAVE_FORMAT_MPEG){
            audioExt = new AudioMpeg1();
            ((AudioMpeg1) audioExt).write(ds);
        } else if (wFormatTag == WAVE_FORMAT_MPEGLAYER3){
            audioExt = new AudioMpeg3();
            ((AudioMpeg3) audioExt).write(ds);
        } else if (wFormatTag == WAVE_FORMAT_EXTENSIBLE){
            throw new IOException(ds, "TODO");
        } else {
            throw new IOException(ds, "Unknwoed audio type : "+wFormatTag);
        }
    }

    public static class AudioMpeg1 {
        public short fwHeadLayer;
        public int dwHeadBitrate;
        public short fwHeadMode;
        public short fwHeadModeExt;
        public short wHeadEmphasis;
        public short fwHeadFlags;
        public int dwPTSLow;
        public int dwPTSHigh;

        public void read(DataInputStream ds) throws IOException{
            fwHeadLayer     = ds.readShort();
            dwHeadBitrate   = ds.readUShort();
            fwHeadMode      = ds.readShort();
            fwHeadModeExt   = ds.readShort();
            wHeadEmphasis   = ds.readShort();
            fwHeadFlags     = ds.readShort();
            dwPTSLow        = ds.readUShort();
            dwPTSHigh       = ds.readUShort();
        }

        public void write(DataOutputStream ds) throws IOException {
            ds.writeShort(fwHeadLayer);
            ds.writeUShort(dwHeadBitrate);
            ds.writeShort(fwHeadMode);
            ds.writeShort(fwHeadModeExt);
            ds.writeShort(wHeadEmphasis);
            ds.writeShort(fwHeadFlags);
            ds.writeUShort(dwPTSLow);
            ds.writeUShort(dwPTSHigh);
        }
    }

    public static class AudioMpeg3 {
        public short wID;
        public int fdwFlags;
        public short nBlockSize;
        public short nFramesPerBlock;
        public short nCodecDelay;

        public void read(DataInputStream ds) throws IOException{
            wID = ds.readShort();
            fdwFlags = ds.readInt();
            nBlockSize = ds.readShort();
            nFramesPerBlock = ds.readShort();
            nCodecDelay = ds.readShort();
        }

        public void write(DataOutputStream ds) throws IOException {
            ds.writeShort(wID);
            ds.writeInt(fdwFlags);
            ds.writeShort(nBlockSize);
            ds.writeShort(nFramesPerBlock);
            ds.writeShort(nCodecDelay);
        }

    }

}
