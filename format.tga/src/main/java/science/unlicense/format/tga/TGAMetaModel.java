package science.unlicense.format.tga;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.DefaultNodeCardinality;
import science.unlicense.common.api.model.tree.DefaultNodeType;
import science.unlicense.common.api.model.tree.NodeCardinality;

/**
 *
 * @author Johann Sorel
 */
public final class TGAMetaModel {


    public static final int IMAGE_TYPE_NODATA = 0;
    public static final int IMAGE_TYPE_UNCOMPRESSED_COLORMAP = 1;
    public static final int IMAGE_TYPE_UNCOMPRESSED_TRUECOLOR = 2;
    public static final int IMAGE_TYPE_UNCOMPRESSED_BW = 3;
    public static final int IMAGE_TYPE_RLE_COLORMAP = 9;
    public static final int IMAGE_TYPE_RLE_TRUECOLOR = 10;
    public static final int IMAGE_TYPE_RLE_BW = 11;

    // 8 bits : 2 unused, 2 image origin, 4 alpha
    public static final byte IMAGE_ORIGIN_MASK = 0x30;
    public static final byte IMAGE_ORIGIN_BOTTOM_LEFT = 0x00;
    public static final byte IMAGE_ORIGIN_BOTTOM_RIGHT = 0x10;
    public static final byte IMAGE_ORIGIN_TOP_LEFT = 0x20;
    public static final byte IMAGE_ORIGIN_TOP_RIGHT = 0x30;


    public static final DefaultNodeType MD_TGA;
    public static final NodeCardinality MD_TGA_ID                         = new DefaultNodeCardinality(new Chars("id"),null, null,String.class, false, null);
    public static final NodeCardinality MD_TGA_COLORMAP;
    public static final NodeCardinality MD_TGA_COLORMAP_TYPE              = new DefaultNodeCardinality(new Chars("type"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_TGA_COLORMAP_FIRSTENTRYINDEX   = new DefaultNodeCardinality(new Chars("firstEntryIndex"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_TGA_COLORMAP_LENGTH            = new DefaultNodeCardinality(new Chars("length"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_TGA_COLORMAP_SIZE              = new DefaultNodeCardinality(new Chars("size"),null, null,Integer.class, false, null);

    public static final NodeCardinality MD_TGA_IMAGE;
    public static final NodeCardinality MD_TGA_IMAGE_TYPE                 = new DefaultNodeCardinality(new Chars("type"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_TGA_IMAGE_XORIGIN              = new DefaultNodeCardinality(new Chars("xorigin"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_TGA_IMAGE_YORIGIN              = new DefaultNodeCardinality(new Chars("yorigin"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_TGA_IMAGE_WIDTH                = new DefaultNodeCardinality(new Chars("width"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_TGA_IMAGE_HEIGHT               = new DefaultNodeCardinality(new Chars("height"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_TGA_IMAGE_DEPTH                = new DefaultNodeCardinality(new Chars("pixeldepth"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_TGA_IMAGE_DESCRIPTOR           = new DefaultNodeCardinality(new Chars("descriptor"),null, null,Byte.class, false, null);


    static {

        MD_TGA_COLORMAP = new DefaultNodeCardinality(new Chars("colormap"),null, null,Sequence.class, null, false, new NodeCardinality[]{
            MD_TGA_COLORMAP_TYPE,
            MD_TGA_COLORMAP_FIRSTENTRYINDEX,
            MD_TGA_COLORMAP_LENGTH,
            MD_TGA_COLORMAP_SIZE
        });

        MD_TGA_IMAGE = new DefaultNodeCardinality(new Chars("image"),null, null,Sequence.class, null, false, new NodeCardinality[]{
            MD_TGA_IMAGE_TYPE,
            MD_TGA_IMAGE_XORIGIN,
            MD_TGA_IMAGE_YORIGIN,
            MD_TGA_IMAGE_WIDTH,
            MD_TGA_IMAGE_HEIGHT,
            MD_TGA_IMAGE_DEPTH,
            MD_TGA_IMAGE_DESCRIPTOR,
        });

        MD_TGA = new DefaultNodeType(new Chars("tga"),null, null,Sequence.class, null, false, new NodeCardinality[]{
            MD_TGA_ID,
            MD_TGA_COLORMAP,
            MD_TGA_IMAGE
        });

    }

    private TGAMetaModel(){}

}
