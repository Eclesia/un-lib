package science.unlicense.format.tga;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class TGAReaderTest {

    private static final Vector2i32 b00 = new Vector2i32(0,0);
    private static final Vector2i32 b10 = new Vector2i32(1,0);
    private static final Vector2i32 b20 = new Vector2i32(2,0);
    private static final Vector2i32 b30 = new Vector2i32(3,0);
    private static final Vector2i32 b01 = new Vector2i32(0,1);
    private static final Vector2i32 b11 = new Vector2i32(1,1);
    private static final Vector2i32 b21 = new Vector2i32(2,1);
    private static final Vector2i32 b31 = new Vector2i32(3,1);

    /**
     * This image pixels starts at the up left.
     */
    @Test
    public void testReadUncompressedUL() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/tga/sampleUncompressedTL.tga")).createInputStream();

        final ImageReader reader = new TGAReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();
        final TupleRW storage = sm.createTuple();

        //values are in BVR

        //BLUE
        sm.getTuple(new Vector2i32(0,0),storage);
        Assert.assertArrayEquals(new int[]{255,0,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 255, 255), image.getColor(b00));
        //BLACK
        sm.getTuple(new Vector2i32(1,0),storage);
        Assert.assertArrayEquals(new int[]{0,0,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 0, 255), image.getColor(b10));
        //RED
        sm.getTuple(new Vector2i32(0,1),storage);
        Assert.assertArrayEquals(new int[]{0,0,255}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), image.getColor(b01));
        //GREEN
        sm.getTuple(new Vector2i32(1,1),storage);
        Assert.assertArrayEquals(new int[]{0,255,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 255, 0, 255), image.getColor(b11));

    }

    /**
     * This image pixels starts at the bottom left.
     */
    @Test
    public void testReadUncompressedBL() throws Exception {

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/tga/sampleUncompressedBL.tga")).createInputStream();

        final ImageReader reader = new TGAReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();
        final TupleRW storage = sm.createTuple();

        //values are in BVR

        //BLUE
        sm.getTuple(new Vector2i32(0,0),storage);
        Assert.assertArrayEquals(new int[]{255,0,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 255, 255), image.getColor(b00));
        //BLACK
        sm.getTuple(new Vector2i32(1,0),storage);
        Assert.assertArrayEquals(new int[]{0,0,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 0, 255), image.getColor(b10));
        //RED
        sm.getTuple(new Vector2i32(0,1),storage);
        Assert.assertArrayEquals(new int[]{0,0,255}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), image.getColor(b01));
        //GREEN
        sm.getTuple(new Vector2i32(1,1),storage);
        Assert.assertArrayEquals(new int[]{0,255,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 255, 0, 255), image.getColor(b11));

    }

    /**
     * This image pixels starts at the bottom left. 4*2 dimension
     */
    @Test
    public void testReadUncompressedBLLarge() throws Exception {

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/tga/sampleUncompressedBLLarge.tga")).createInputStream();

        final ImageReader reader = new TGAReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(4,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();
        final TupleRW storage = sm.createTuple();

        //values are in BVR

        //BLUE
        sm.getTuple(new Vector2i32(0,0),storage);
        Assert.assertArrayEquals(new int[]{255,0,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 255, 255), image.getColor(b00));
        //BLACK
        sm.getTuple(new Vector2i32(1,0),storage);
        Assert.assertArrayEquals(new int[]{0,0,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 0, 255), image.getColor(b10));
        //WHITE
        sm.getTuple(new Vector2i32(2,0),storage);
        Assert.assertArrayEquals(new int[]{255,255,255}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 255, 255, 255), image.getColor(b20));
        //YELLOW
        sm.getTuple(new Vector2i32(3,0),storage);
        Assert.assertArrayEquals(new int[]{0,255,255}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 255, 0, 255), image.getColor(b30));
        //RED
        sm.getTuple(new Vector2i32(0,1),storage);
        Assert.assertArrayEquals(new int[]{0,0,255}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), image.getColor(b01));
        //GREEN
        sm.getTuple(new Vector2i32(1,1),storage);
        Assert.assertArrayEquals(new int[]{0,255,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 255, 0, 255), image.getColor(b11));
        //CYAN
        sm.getTuple(new Vector2i32(2,1),storage);
        Assert.assertArrayEquals(new int[]{255,255,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 255, 255, 255), image.getColor(b21));
        //GRAY
        sm.getTuple(new Vector2i32(3,1),storage);
        Assert.assertArrayEquals(new int[]{100,100,100}, storage.toInt());
        Assert.assertEquals(new ColorRGB(100, 100, 100, 255), image.getColor(b31));

    }

    @Test
    public void testReadCompressed() throws Exception {

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/tga/sampleCompressed.tga")).createInputStream();

        final ImageReader reader = new TGAReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();
        final TupleRW storage = sm.createTuple();

        //values are in BVR

        //BLUE
        sm.getTuple(new Vector2i32(0,0),storage);
        Assert.assertArrayEquals(new int[]{255,0,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 255, 255), image.getColor(b00));
        //BLACK
        sm.getTuple(new Vector2i32(1,0),storage);
        Assert.assertArrayEquals(new int[]{0,0,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 0, 255), image.getColor(b10));
        //RED
        sm.getTuple(new Vector2i32(0,1),storage);
        Assert.assertArrayEquals(new int[]{0,0,255}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), image.getColor(b01));
        //GREEN
        sm.getTuple(new Vector2i32(1,1),storage);
        Assert.assertArrayEquals(new int[]{0,255,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 255, 0, 255), image.getColor(b11));

    }

    /**
     * Grayscale rle compressed image.
     */
    @Test
    public void testReadBW() throws Exception {

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/tga/sampleBW.tga")).createInputStream();

        final ImageReader reader = new TGAReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();
        final TupleRW storage = sm.createTuple();

        //WHITE
        sm.getTuple(new Vector2i32(0,0),storage);
        Assert.assertArrayEquals(new int[]{255}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 255, 255, 255), image.getColor(b00));
        //DARK GRAY
        sm.getTuple(new Vector2i32(1,0),storage);
        Assert.assertArrayEquals(new int[]{100}, storage.toInt());
        Assert.assertEquals(new ColorRGB(100, 100, 100, 255), image.getColor(b10));
        //BLACK
        sm.getTuple(new Vector2i32(0,1),storage);
        Assert.assertArrayEquals(new int[]{0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 0, 255), image.getColor(b01));
        //LIGHT GRAY
        sm.getTuple(new Vector2i32(1,1),storage);
        Assert.assertArrayEquals(new int[]{200}, storage.toInt());
        Assert.assertEquals(new ColorRGB(200, 200, 200, 255), image.getColor(b11));

    }

    /**
     * Grayscale uncompressed image.
     */
    @Test
    public void testReadUncompressedBW() throws Exception {

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/tga/sampleUncompressedBW.tga")).createInputStream();

        final ImageReader reader = new TGAReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();
        final TupleRW storage = sm.createTuple();

        //WHITE
        sm.getTuple(new Vector2i32(0,0),storage);
        Assert.assertArrayEquals(new int[]{255}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 255, 255, 255), image.getColor(b00));
        //DARK GRAY
        sm.getTuple(new Vector2i32(1,0),storage);
        Assert.assertArrayEquals(new int[]{100}, storage.toInt());
        Assert.assertEquals(new ColorRGB(100, 100, 100, 255), image.getColor(b10));
        //BLACK
        sm.getTuple(new Vector2i32(0,1),storage);
        Assert.assertArrayEquals(new int[]{0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 0, 255), image.getColor(b01));
        //LIGHT GRAY
        sm.getTuple(new Vector2i32(1,1),storage);
        Assert.assertArrayEquals(new int[]{200}, storage.toInt());
        Assert.assertEquals(new ColorRGB(200, 200, 200, 255), image.getColor(b11));

    }

    /**
     * Colormap uncompressed image.
     */
    @Test
    public void testReadUncompressedColorMap() throws Exception {

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/tga/sampleUncompressedColorMap.tga")).createInputStream();

        final ImageReader reader = new TGAReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();
        final TupleRW storage = sm.createTuple();

        //RED
        sm.getTuple(new Vector2i32(0,0),storage);
        Assert.assertArrayEquals(new int[]{2}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), image.getColor(b00));
        //GREEN
        sm.getTuple(new Vector2i32(1,0),storage);
        Assert.assertArrayEquals(new int[]{3}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 255, 0, 255), image.getColor(b10));
        //BLACK
        sm.getTuple(new Vector2i32(0,1),storage);
        Assert.assertArrayEquals(new int[]{0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 0, 255), image.getColor(b01));
        //BLUE
        sm.getTuple(new Vector2i32(1,1),storage);
        Assert.assertArrayEquals(new int[]{1}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 255, 255), image.getColor(b11));

    }

    /**
     * Colormap rle compressed image.
     */
    @Test
    public void testReadColorMap() throws Exception {

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/tga/sampleColorMap.tga")).createInputStream();

        final ImageReader reader = new TGAReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();
        final TupleRW storage = sm.createTuple();

        //RED
        sm.getTuple(new Vector2i32(0,0),storage);
        Assert.assertArrayEquals(new int[]{2}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), image.getColor(b00));
        //GREEN
        sm.getTuple(new Vector2i32(1,0),storage);
        Assert.assertArrayEquals(new int[]{3}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 255, 0, 255), image.getColor(b10));
        //BLACK
        sm.getTuple(new Vector2i32(0,1),storage);
        Assert.assertArrayEquals(new int[]{0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 0, 255), image.getColor(b01));
        //BLUE
        sm.getTuple(new Vector2i32(1,1),storage);
        Assert.assertArrayEquals(new int[]{1}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 255, 255), image.getColor(b11));

    }

}
