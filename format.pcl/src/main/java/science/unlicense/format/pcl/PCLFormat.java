

package science.unlicense.format.pcl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * Printer Command Language.
 *
 * Resource :
 * http://en.wikipedia.org/wiki/Printer_Command_Language
 *
 * @author Johann Sorel
 */
public class PCLFormat extends DefaultFormat {

    public PCLFormat() {
        super(new Chars("pcl"));
        shortName = new Chars("PCL");
        longName = new Chars("Printer Command Language");
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
