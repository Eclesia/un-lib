
package science.unlicense.engine.lwjgl.opengl;

import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL32;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public class LGL3 extends AbstractLGLES implements science.unlicense.gpu.api.opengl.GL33, science.unlicense.gpu.api.opengl.GL3{

    LGL3(LGL gl) {
        super(gl);
    }

    public science.unlicense.gpu.api.opengl.GL3 asGL3() {
        return this;
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 3.0 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glClampColor(int target, int clamp) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBeginConditionalRender(int id, int mode) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glEndConditionalRender() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI1i(int index, int x) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI2i(int index, int x, int y) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI3i(int index, int x, int y, int z) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI1ui(int index, int x) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI2ui(int index, int x, int y) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI3ui(int index, int x, int y, int z) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI1iv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI2iv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI3iv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI1uiv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI2uiv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI3uiv(int index, Buffer.Int32 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4bv(int index, Buffer.Int8 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4sv(int index, Buffer.Int16 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4ubv(int index, Buffer.Int8 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribI4usv(int index, Buffer.Int16 v) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindFragDataLocation(int program, int color, CharArray name) {
        if (base.isES) throw new UnimplementedException("Not supported in ES.");
        else GL30.glBindFragDataLocation(program, color, name.toString());
    }

    @Override
    public void glFramebufferTexture1D(int target, int attachment, int textarget, int texture, int level) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFramebufferTexture2D(int target, int attachment, int textarget, int texture, int level) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFramebufferTexture3D(int target, int attachment, int textarget, int texture, int level, int zoffset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 3.1 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glPrimitiveRestartIndex(int index) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetActiveUniformName(int program, int uniformIndex, Buffer.Int32 length, Buffer.Int8 uniformName) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 3.2 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glMultiDrawElementsBaseVertex(int mode, Buffer.Int32 count, int type, int indices, int drawcount, Buffer.Int32 basevertex) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProvokingVertex(int mode) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexImage2DMultisample(int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations) {
        GL32.glTexImage2DMultisample(target, samples, internalformat, width, height, fixedsamplelocations);
    }

    @Override
    public void glTexImage3DMultisample(int target, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations) {
        GL32.glTexImage3DMultisample(target, samples, internalformat, width, height, depth, fixedsamplelocations);
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 3.3 //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glBindFragDataLocationIndexed(int program, int colorNumber, int index, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetFragDataIndex(int program, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glQueryCounter(int id, int target) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetQueryObjecti64v(int id, int pname, Buffer.Int8 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetQueryObjectui64v(int id, int pname, Buffer.Int8 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP1ui(int index, int type, boolean normalized, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP1uiv(int index, int type, boolean normalized, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP2ui(int index, int type, boolean normalized, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP2uiv(int index, int type, boolean normalized, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP3ui(int index, int type, boolean normalized, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP3uiv(int index, int type, boolean normalized, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP4ui(int index, int type, boolean normalized, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribP4uiv(int index, int type, boolean normalized, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP2ui(int type, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP2uiv(int type, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP3ui(int type, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP3uiv(int type, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP4ui(int type, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexP4uiv(int type, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP1ui(int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP1uiv(int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP2ui(int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP2uiv(int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP3ui(int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP3uiv(int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP4ui(int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexCoordP4uiv(int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP1ui(int texture, int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP1uiv(int texture, int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP2ui(int texture, int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP2uiv(int texture, int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP3ui(int texture, int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP3uiv(int texture, int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP4ui(int texture, int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMultiTexCoordP4uiv(int texture, int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNormalP3ui(int type, int coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glNormalP3uiv(int type, Buffer.Int32 coords) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glColorP3ui(int type, int color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glColorP3uiv(int type, Buffer.Int32 color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glColorP4ui(int type, int color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glColorP4uiv(int type, Buffer.Int32 color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glSecondaryColorP3ui(int type, int color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glSecondaryColorP3uiv(int type, Buffer.Int32 color) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
