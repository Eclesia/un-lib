
package science.unlicense.engine.lwjgl.opengl;

import org.lwjgl.opengles.GLES30;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.exception.UnimplementedException;
import static science.unlicense.gpu.api.GLBuffer.*;
import science.unlicense.gpu.api.opengl.GLES232;

/**
 *
 * @author Johann Sorel
 */
public class LGL2ES3 extends AbstractLGLES implements GLES232,science.unlicense.gpu.api.opengl.GL2ES3{

    LGL2ES3(LGL gl) {
        super(gl);
    }

    public science.unlicense.gpu.api.opengl.GL2ES3 asGL2ES3() {
        return this;
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 2 ES 3.0 /////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glDrawRangeElements(int mode, int start, int end, int count, int type, Buffer.Int8 indices) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexImage3D(int target, int level, int internalformat, int width, int height, int depth, int border, int format, int type, Buffer.Int8 pixels) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, Buffer.Int8 pixels) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCopyTexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCompressedTexImage3D(int target, int level, int internalformat, int width, int height, int depth, int border, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCompressedTexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGenQueries(Buffer.Int32 ids) {
        GLES30.glGenQueries(unwrapOrCopyInt(ids));
    }

    @Override
    public void glGenQueries(int[] ids) {
        GLES30.glGenQueries(ids);
    }

    @Override
    public void glDeleteQueries(Buffer.Int32 ids) {
        GLES30.glDeleteQueries(unwrapOrCopyInt(ids));
    }

    @Override
    public void glDeleteQueries(int[] ids) {
        GLES30.glDeleteQueries(ids);
    }

    @Override
    public boolean glIsQuery(int id) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBeginQuery(int target, int id) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glEndQuery(int target) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetQueryiv(int target, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetQueryObjectuiv(int id, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean glUnmapBuffer(int target) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetBufferPointerv(int target, int pname, Buffer.Int8 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindTransformFeedback(int target, int id) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDeleteTransformFeedbacks(Buffer.Int32 ids) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGenTransformFeedbacks(Buffer.Int32 ids) {
        GLES30.glGenTransformFeedbacks(unwrapOrCopyInt(ids));
    }

    @Override
    public void glGenTransformFeedbacks(int[] ids) {
        GLES30.glGenTransformFeedbacks(ids);
    }

    @Override
    public boolean glIsTransformFeedback(int id) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glPauseTransformFeedback() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glResumeTransformFeedback() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetProgramBinary(int program, Buffer.Int32 length, Buffer.Int32 binaryFormat, Buffer.Int8 binary) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramBinary(int program, int binaryFormat, Buffer.Int8 binary) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramParameteri(int program, int pname, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glInvalidateFramebuffer(int target, Buffer.Int32 attachments) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glInvalidateSubFramebuffer(int target, Buffer.Int32 attachments, int x, int y, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexStorage2D(int target, int levels, int internalformat, int width, int height) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexStorage3D(int target, int levels, int internalformat, int width, int height, int depth) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetInternalformativ(int target, int internalformat, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 2 ES 3.1 /////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glDispatchCompute(int num_groups_x, int num_groups_y, int num_groups_z) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDispatchComputeIndirect(long indirect) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDrawArraysIndirect(int mode, long indirect) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDrawElementsIndirect(int mode, int type, long indirect) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glFramebufferParameteri(int target, int pname, int param) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetFramebufferParameteriv(int target, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetProgramInterfaceiv(int program, int programInterface, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetProgramResourceIndex(int program, int programInterface, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetProgramResourceName(int program, int programInterface, int index, Buffer.Int32 length, Buffer.Int8 name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetProgramResourceiv(int program, int programInterface, int index, Buffer.Int32 props, Buffer.Int32 length, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetProgramResourceLocation(int program, int programInterface, CharArray name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glUseProgramStages(int pipeline, int stages, int program) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glActiveShaderProgram(int pipeline, int program) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glCreateShaderProgramv(int type, Buffer.Int8 strings) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindProgramPipeline(int pipeline) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDeleteProgramPipelines(Buffer.Int32 pipelines) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGenProgramPipelines(Buffer.Int32 pipelines) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean glIsProgramPipeline(int pipeline) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetProgramPipelineiv(int pipeline, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform1i(int program, int location, int v0) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform2i(int program, int location, int v0, int v1) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform3i(int program, int location, int v0, int v1, int v2) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform4i(int program, int location, int v0, int v1, int v2, int v3) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform1ui(int program, int location, int v0) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform2ui(int program, int location, int v0, int v1) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform3ui(int program, int location, int v0, int v1, int v2) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform4ui(int program, int location, int v0, int v1, int v2, int v3) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform1f(int program, int location, float v0) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform2f(int program, int location, float v0, float v1) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform3f(int program, int location, float v0, float v1, float v2) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform4f(int program, int location, float v0, float v1, float v2, float v3) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform1iv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform2iv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform3iv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform4iv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform1uiv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform2uiv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform3uiv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform4uiv(int program, int location, Buffer.Int32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform1fv(int program, int location, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform2fv(int program, int location, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform3fv(int program, int location, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniform4fv(int program, int location, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix2fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix3fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix4fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix2x3fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix3x2fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix2x4fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix4x2fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix3x4fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glProgramUniformMatrix4x3fv(int program, int location, boolean transpose, Buffer.Float32 value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glValidateProgramPipeline(int pipeline) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetProgramPipelineInfoLog(int pipeline, Buffer.Int32 length, Buffer.Int8 infoLog) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindImageTexture(int unit, int texture, int level, boolean layered, int layer, int access, int format) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMemoryBarrier(int barriers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMemoryBarrierByRegion(int barriers) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexStorage2DMultisample(int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTexLevelParameteriv(int target, int level, int pname, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTexLevelParameterfv(int target, int level, int pname, Buffer.Float32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBindVertexBuffer(int bindingindex, int buffer, long offset, int stride) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribFormat(int attribindex, int size, int type, boolean normalized, int relativeoffset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribIFormat(int attribindex, int size, int type, int relativeoffset) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexAttribBinding(int attribindex, int bindingindex) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glVertexBindingDivisor(int bindingindex, int divisor) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL 2 ES 3.2 /////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void glBlendBarrier() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glCopyImageSubData(int srcName, int srcTarget, int srcLevel, int srcX, int srcY, int srcZ, int dstName, int dstTarget, int dstLevel, int dstX, int dstY, int dstZ, int srcWidth, int srcHeight, int srcDepth) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDebugMessageControl(int source, int type, int severity, Buffer.Int32 ids, boolean enabled) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDebugMessageInsert(int source, int type, int id, int severity, int length, CharArray buf) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glDebugMessageCallback(Object callback, long userParam) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetDebugMessageLog(Buffer.Int32 sources, Buffer.Int32 types, Buffer.Int32 ids, Buffer.Int32 severities, Buffer.Int32 lengths, Buffer.Int8 messageLog) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glPushDebugGroup(int source, int id, int length, CharArray message) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glPopDebugGroup() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glObjectLabel(int identifier, int name, int length, CharArray label) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetObjectLabel(int identifier, int name, Buffer.Int32 length, Buffer.Int8 label) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glObjectPtrLabel(long ptr, int length, CharArray label) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetObjectPtrLabel(long ptr, Buffer.Int32 length, Buffer.Int8 label) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetPointerv(int pname, Buffer.Int8 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBlendEquationi(int buf, int mode) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBlendEquationSeparatei(int buf, int modeRGB, int modeAlpha) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBlendFunci(int buf, int src, int dst) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glBlendFuncSeparatei(int buf, int srcRGB, int dstRGB, int srcAlpha, int dstAlpha) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glPrimitiveBoundingBox(float minX, float minY, float minZ, float minW, float maxX, float maxY, float maxZ, float maxW) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int glGetGraphicsResetStatus() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glReadnPixels(int x, int y, int width, int height, int format, int type, int bufSize, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnUniformfv(int program, int location, int bufSize, Buffer.Float32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnUniformiv(int program, int location, int bufSize, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetnUniformuiv(int program, int location, int bufSize, Buffer.Int32 params) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glMinSampleShading(float value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glPatchParameteri(int pname, int value) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexBufferRange(int target, int internalformat, int buffer, long offset, long size) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glTexStorage3DMultisample(int target, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
