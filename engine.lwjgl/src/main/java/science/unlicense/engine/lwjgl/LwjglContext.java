
package science.unlicense.engine.lwjgl;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.gpu.api.SharedContext;

/**
 *
 * @author Johann Sorel
 */
public class LwjglContext implements SharedContext {

    private final Sequence referers = new ArraySequence();

    public Sequence getContexts() {
        return referers;
    }

}
