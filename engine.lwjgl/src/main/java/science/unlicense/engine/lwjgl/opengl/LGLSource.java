
package science.unlicense.engine.lwjgl.opengl;

import org.lwjgl.glfw.GLFW;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_NORMAL;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLCapabilities;
import static org.lwjgl.system.MemoryUtil.NULL;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.engine.lwjgl.LwjglContext;
import science.unlicense.gpu.api.SharedContext;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GLBinding;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;

/**
 *
 * @author Johann Sorel
 */
class LGLSource implements GLSource {

    private final Sequence callbacks = new ArraySequence();
    private final GLFWErrorCallback errorCallback;
    private final LwjglContext context;

    private final LGL ugl = new LGL();
    int x;
    int y;
    int width;
    int height;
    final long window;

    public LGLSource(LwjglContext context) {
        this(false, 100, 100, context);
    }

    public LGLSource(boolean offscreen, int width, int height, LwjglContext context) {
        CObjects.ensureNotNull(context);
        this.context = context;

        // TODO : Print errors, find a better approach
        errorCallback = GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if (!GLFW.glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }

        // Configure our window
        GLFW.glfwDefaultWindowHints(); // optional, the current window hints are already the default
        GLFW.glfwWindowHint(GLFW_VISIBLE, offscreen ? GLFW_FALSE : GLFW_TRUE); // the window will stay hidden after creation
        GLFW.glfwWindowHint(GLFW_RESIZABLE, offscreen ? GLFW_FALSE : GLFW_TRUE); // the window will be resizable
//        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
//        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
//        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

        this.width = width;
        this.height = height;

        // Create the window
        if (context.getContexts().isEmpty()) {
            window = GLFW.glfwCreateWindow(width, height, "", NULL, NULL);
        } else {
            //shared context
            final LGLSource referer = (LGLSource) context.getContexts().get(0);
            window = GLFW.glfwCreateWindow(width, height, "", NULL, referer.window);
        }

        if (window == NULL) {
            throw new RuntimeException("Failed to create the GLFW window");
        }
        context.getContexts().add(this);

        GLFW.glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }

    @Override
    public GLBinding getBinding() {
        return LGLBinding.INSTANCE;
    }

    @Override
    public SharedContext getContext() {
        return context;
    }

    @Override
    public GL getGL() {
        return ugl;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getWidth() {
        int[] min = new int[1];
        int[] max = new int[1];
        GLFW.glfwGetWindowSize(window, min, max);
        return min[0];
    }

    @Override
    public int getHeight() {
        int[] min = new int[1];
        int[] max = new int[1];
        GLFW.glfwGetWindowSize(window, min, max);
        return max[0];
    }

    @Override
    public Sequence getCallbacks() {
        return callbacks;
    }

    private GLCapabilities capa =null;

    /**
     * Synchronized is necessary, a new thread is started by lwjgl when frame is resized.
     */
    @Override
    public synchronized void render() {
        // Make the OpenGL context current
        GLFW.glfwMakeContextCurrent(window);
        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
//        if (capa == null) {
            capa = org.lwjgl.opengl.GL.createCapabilities();
//        }

        final String version = GL11.glGetString(GL11.GL_VERSION).toLowerCase();
        ugl.isES = version.contains("ES");

        for (int i=0,n=callbacks.getSize();i<n;i++) {
            ((GLCallback) callbacks.get(i)).execute(this);
        }

//        // Poll for window events. The key callback above will only be
//        // invoked during this call.
//        glfwPollEvents();
    }

    @Override
    public void dispose() {
        context.getContexts().remove(this);

        for (int i=0,n=callbacks.getSize();i<n;i++) {
            ((GLCallback) callbacks.get(i)).dispose(this);
        }

        //detach glfw callback
        GLFW.glfwSetWindowShouldClose(window, true);
        GLFWErrorCallback callback = GLFW.glfwSetErrorCallback(null);
        if (callback != null) callback.free();

    }

}
