package science.unlicense.engine.lwjgl.opengl;

import org.lwjgl.glfw.*;
import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.desktop.cursor.Cursor;
import science.unlicense.geometry.api.Extent;
import science.unlicense.gpu.api.opengl.GLFrame;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.system.api.desktop.Attachment;
import science.unlicense.system.api.desktop.DefaultAttachment;

/**
 * TODO
 */
public class LWJGLFrame extends AbstractEventSource implements GLFrame, GLFWKeyCallbackI {

    private final LGLSource source;

    private CharArray title;
    private final Vector2f64 mousePos = new Vector2f64();
    private boolean mousePressed = false;

    LWJGLFrame(LGLSource source) {
        this.source = source;
    }

    public void run() {

        try {
            init();
            loop();

            // Free the window callbacks and destroy the window
            glfwFreeCallbacks(source.window);
            glfwDestroyWindow(source.window);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // Terminate GLFW and free the error callback
            glfwTerminate();
            source.dispose();
        }
    }

    private void init() {

        // Make the OpenGL context current
        glfwMakeContextCurrent(source.window);
        glfwSetInputMode(source.window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(source.window, new GLFWKeyCallbackI() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
                if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
                    glfwSetWindowShouldClose(window, true); // We will detect this in our rendering loop
                }
            }
        });

        glfwSetScrollCallback(source.window, new GLFWScrollCallbackI() {
            @Override
            public void invoke(long window, double xoffset, double yoffset) {
                final MouseMessage mm = new MouseMessage(MouseMessage.TYPE_WHEEL, 0, 0, mousePos, mousePos, yoffset, false);
                processEvent(mm, null);
            }
        });
        glfwSetCursorPosCallback(source.window, new GLFWCursorPosCallbackI() {
            @Override
            public void invoke(long window, double xpos, double ypos) {
                mousePos.setXY(xpos, ypos);
                final MouseMessage mm = new MouseMessage(MouseMessage.TYPE_MOVE, 0, 0, mousePos, mousePos, 0, mousePressed);
                processEvent(mm, null);
            }
        });
        glfwSetMouseButtonCallback(source.window, new GLFWMouseButtonCallbackI() {
            @Override
            public void invoke(long window, int button, int action, int mods) {
                int typ = -1;
                int but = -1;
                switch (action) {
                    case GLFW_PRESS :
                        typ = MouseMessage.TYPE_PRESS;
                        mousePressed = true;
                        break;
                    case GLFW_RELEASE :
                        typ = MouseMessage.TYPE_RELEASE;
                        mousePressed = false;
                        break;
                }
                switch (button) {
                    case GLFW_MOUSE_BUTTON_LEFT : but = MouseMessage.BUTTON_1; break;
                    case GLFW_MOUSE_BUTTON_MIDDLE : but = MouseMessage.BUTTON_2; break;
                    case GLFW_MOUSE_BUTTON_RIGHT : but = MouseMessage.BUTTON_3; break;
                }
                final MouseMessage mm = new MouseMessage(typ, but, 1, mousePos, mousePos, 0, false);
                processEvent(mm, null);
            }
        });
        glfwSetDropCallback(source.window, new GLFWDropCallbackI() {
            @Override
            public void invoke(long window, int count, long names) {

                final Sequence drops = science.unlicense.system.System.get().getDragAndDrapBag().getAttachments();

                final Sequence files = new ArraySequence();
                for (int i = 0; i < count; i++ ) {
                    String name = GLFWDropCallback.getName(names, i);
                    files.add(Paths.resolve(new Chars(name)));
                }
                final Attachment att = new DefaultAttachment(files, new Chars("application/x-java-file-list; class=java.util.List"));
                drops.removeAll();
                drops.add(att);

                //drop calls are not linked to mouse event in glfw
                //create a mouse button release at last know position
                final MouseMessage mm = new MouseMessage(MouseMessage.TYPE_RELEASE, MouseMessage.BUTTON_1, 1, mousePos, mousePos, 0, true);
                processEvent(mm, null);
            }
        });


        // Get the resolution of the primary monitor
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        // Center our window
        glfwSetWindowPos(
                source.window,
                (vidmode.width() - source.width) / 2,
                (vidmode.height() - source.height) / 2
        );

        // Enable v-sync
        glfwSwapInterval(1);
    }

    /**
     * A new event from JOGAMP arrived, handle it.
     */
    protected void processEvent(MouseMessage me, KeyMessage ke) {
        //send to listeners
        if (hasListeners()) {
            if (me!=null && !me.isConsumed()) getEventManager().sendEvent(new Event(this, me));
            if (ke!=null && !ke.isConsumed()) getEventManager().sendEvent(new Event(this, ke));
        }
    }

    private void loop() {
        // Make the OpenGL context current
        glfwMakeContextCurrent(source.window);

//        // This line is critical for LWJGL's interoperation with GLFW's
//        // OpenGL context, or any context that is managed externally.
//        // LWJGL detects the context that is current in the current thread,
//        // creates the GLCapabilities instance and makes the OpenGL
//        // bindings available for use.
//        final GLCapabilities capa = GL.createCapabilities();
//        System.out.println(capa);
//
//        // Set the clear color
//        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);
//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
//        glfwSwapBuffers(source.window); // swap the color buffers
//        glfwPollEvents();

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while (!glfwWindowShouldClose(source.window)) {

            // Poll for window events. The key callback above will only be
            // invoked during this call.
            glfwPollEvents();

            source.render();

            glfwSwapBuffers(source.window); // swap the color buffers
        }
    }

    @Override
    public GLSource getSource() {
        return source;
    }

    @Override
    public CharArray getTitle() {
        return title;
    }

    @Override
    public void setTitle(CharArray title) {
        this.title = title;
        if (title == null) title = Chars.EMPTY;
        glfwSetWindowTitle(source.window, title.toString());
    }

    /**
     *
     * @return true is pointer is visible
     */
    @Override
    public boolean isPointerVisible() {
        throw new UnimplementedException("Not supported");
    }

    /**
     *
     * @param visible pointer visible state
     */
    @Override
    public void setPointerVisible(boolean visible) {
        if (visible) {
            glfwSetInputMode(source.window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
        } else {
            glfwSetInputMode(source.window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            //grab the cursor, prevent it going out
            //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
    }

    @Override
    public Cursor getCursor() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setCursor(Cursor cursor) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setOnScreenLocation(Tuple location) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Tuple getOnScreenLocation() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setSize(int width, int height) {
        glfwSetWindowSize(source.window, width, height);
    }

    @Override
    public Extent getSize() {
        int[] min = new int[1];
        int[] max = new int[1];
        glfwGetWindowSize(source.window, min, max);
        return new Extent.Long(min[0], max[0]);
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            glfwShowWindow(source.window);
        } else {
            glfwHideWindow(source.window);
        }
    }

    @Override
    public boolean isVisible() {
        return glfwGetWindowAttrib(source.window, GLFW_VISIBLE) != 0;
    }

    @Override
    public int getState() {
        boolean maximized = glfwGetWindowAttrib(source.window, GLFW_MAXIMIZED) != 0;
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setState(int state) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setMaximizable(boolean maximizable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isMaximizable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setMinimizable(boolean minimizable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isMinimizable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setClosable(boolean closable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isClosable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setAlwaysonTop(boolean ontop) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isAlwaysOnTop() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void dispose() {
    }

    @Override
    public boolean isDisposed() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void waitForDisposal() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setDecorated(boolean decorated) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isDecorated() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isOpaque() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void invoke(long l, int i, int i1, int i2, int i3) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void callback(long l) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getSignature() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long address() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
