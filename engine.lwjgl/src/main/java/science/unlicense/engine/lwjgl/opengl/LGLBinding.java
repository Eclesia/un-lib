
package science.unlicense.engine.lwjgl.opengl;

import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.engine.lwjgl.LwjglContext;
import science.unlicense.gpu.api.GLBufferFactory;
import science.unlicense.gpu.api.SharedContext;
import science.unlicense.gpu.api.SharedContextException;
import science.unlicense.gpu.api.opengl.GLBinding;
import science.unlicense.gpu.api.opengl.GLFrame;
import science.unlicense.gpu.api.opengl.GLSource;

/**
 *
 * @author Johann Sorel
 */
public class LGLBinding implements GLBinding {

    public static final LGLBinding INSTANCE = new LGLBinding();

    @Override
    public GLSource createOffScreen(int width, int height, SharedContext context) {
        if (context != null && !(context instanceof LwjglContext)) {
            throw new SharedContextException();
        } else if (context == null) {
            context = new LwjglContext();
        }
        return new LGLSource(true, width, height, (LwjglContext) context);
    }

    @Override
    public GLFrame createFrame(boolean opaque, SharedContext context) {
        if (context != null && !(context instanceof LwjglContext)) {
            throw new SharedContextException();
        } else if (context == null) {
            context = new LwjglContext();
        }
        LWJGLFrame frame = new LWJGLFrame(new LGLSource(false, 100, 100, (LwjglContext) context));
        new Thread() {
            @Override
            public void run() {
                frame.run();
            }
        }.start();
        return frame;
    }

    @Override
    public Margin getFrameMargin() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public BufferFactory getBufferFactory() {
        return GLBufferFactory.INSTANCE;
    }

}
