
package science.unlicense.engine.lwjgl.opengl;

import org.lwjgl.PointerBuffer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL21;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL33;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;
import org.lwjgl.opengles.GLES32;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.character.CharArray;
import static science.unlicense.gpu.api.GLBuffer.*;
import science.unlicense.gpu.api.opengl.GL1;
import science.unlicense.gpu.api.opengl.GL1ES;
import science.unlicense.gpu.api.opengl.GL2;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GL2ES3;
import science.unlicense.gpu.api.opengl.GL3;
import science.unlicense.gpu.api.opengl.GL4;

/**
 * Shared methods between GL and GLES classes.
 * This class redirect on GL2 or GLES220/GLES232 based on current version.
 *
 * @author Johann Sorel
 */
public class AbstractLGLES {

    protected final LGL base;

    AbstractLGLES(LGL gl) {
        this.base = gl;
    }

    public BufferFactory getBufferFactory() {
        return base.getBufferFactory();
    }

    public boolean isGL1() {
        return base.isGL1();
    }

    public boolean isGL2() {
        return base.isGL2();
    }

    public boolean isGL3() {
        return base.isGL3();
    }

    public boolean isGL4() {
        return base.isGL4();
    }

    public boolean isGL1ES() {
        return base.isGL1ES();
    }

    public boolean isGL2ES2() {
        return base.isGL2ES2();
    }

    public boolean isGL2ES3() {
        return base.isGL2ES3();
    }

    public GL1 asGL1() {
        return base.asGL1();
    }

    public GL2 asGL2() {
        return base.asGL2();
    }

    public GL3 asGL3() {
        return base.asGL3();
    }

    public GL4 asGL4() {
        return base.asGL4();
    }

    public GL1ES asGL1ES() {
        return base.asGL1ES();
    }

    public GL2ES2 asGL2ES2() {
        return base.asGL2ES2();
    }

    public GL2ES3 asGL2ES3() {
        return base.asGL2ES3();
    }


    //GL11

    public void glActiveTexture(int texture) {
        if (base.isES) GLES20.glActiveTexture(texture);
        else GL13.glActiveTexture(texture);
    }

    public void glBindBuffer(int target, int buffer) {
        if (base.isES) GLES20.glBindBuffer(target, buffer);
        else GL15.glBindBuffer(target, buffer);
    }

    public void glBindTexture(int target, int texture) {
        if (base.isES) GLES20.glBindTexture(target, texture);
        else GL11.glBindTexture(target, texture);
    }

    public void glEnable(int cap) {
        if (base.isES) GLES20.glEnable(cap);
        else GL11.glEnable(cap);
    }

    public void glDisable(int cap) {
        if (base.isES) GLES20.glDisable(cap);
        else GL11.glDisable(cap);
    }

    public void glFinish() {
        if (base.isES) GLES20.glFinish();
        else GL11.glFinish();
    }

    public void glFlush() {
        if (base.isES) GLES20.glFlush();
        else GL11.glFlush();
    }

    public void glBlendColor(float red, float green, float blue, float alpha) {
        if (base.isES) GLES20.glBlendColor(red, green, blue, alpha);
        else GL14.glBlendColor(red, green, blue, alpha);
    }

    public void glBlendEquation(int mode) {
        if (base.isES) GLES20.glBlendEquation(mode);
        else GL14.glBlendEquation(mode);
    }

    public void glBlendFunc(int sfactor, int dfactor) {
        if (base.isES) GLES20.glBlendFunc(sfactor, dfactor);
        else GL11.glBlendFunc(sfactor, dfactor);
    }

    public void glBlendFuncSeparate(int sfactorRGB, int dfactorRGB, int sfactorAlpha, int dfactorAlpha) {
        if (base.isES) GLES20.glBlendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha);
        else GL14.glBlendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha);
    }

    public void glClear(int mask) {
        if (base.isES) GLES20.glClear(mask);
        else GL11.glClear(mask);
    }

    public void glClearColor(float red, float green, float blue, float alpha) {
        if (base.isES) GLES20.glClearColor(red, green, blue, alpha);
        else GL11.glClearColor(red, green, blue, alpha);
    }

    public void glClearDepthf(float d) {
        if (base.isES) GLES20.glClearDepthf(d);
        else GL11.glClearDepth(d);
    }

    public void glClearStencil(int s) {
        if (base.isES) GLES20.glClearStencil(s);
        else GL14.glClearStencil(s);
    }

    public void glCullFace(int mode) {
        if (base.isES) GLES20.glCullFace(mode);
        else GL14.glCullFace(mode);
    }

    public void glDepthFunc(int func) {
        if (base.isES) GLES20.glDepthFunc(func);
        else GL14.glDepthFunc(func);
    }

    public void glDepthMask(boolean flag) {
        if (base.isES) GLES20.glDepthMask(flag);
        else GL11.glDepthMask(flag);
    }

    public void glDepthRangef(float n, float f) {
        if (base.isES) GLES20.glDepthRangef(n, f);
        else GL11.glDepthRange(n, f);
    }

    public void glDrawArrays(int mode, int first, int count) {
        if (base.isES) GLES20.glDrawArrays( mode, first, count);
        else GL11.glDrawArrays( mode, first, count);
    }

    public void glDrawElements(int mode, int count, int type, long indices) {
        if (base.isES) GLES20.glDrawElements( mode, count, type, indices);
        else GL11.glDrawElements( mode, count, type, indices);
    }

    public void glFrontFace(int mode) {
        if (base.isES) GLES20.glFrontFace(mode);
        else GL11.glFrontFace(mode);
    }

    public void glLineWidth(float width) {
        if (base.isES) GLES20.glLineWidth( width);
        else GL11.glLineWidth( width);
    }

    public void glScissor(int x, int y, int width, int height) {
        if (base.isES) GLES20.glScissor(x, y, width, height);
        else GL11.glScissor(x, y, width, height);
    }

    public void glStencilFunc(int func, int ref, int mask) {
        if (base.isES) GLES20.glStencilFunc(func, ref, mask);
        else GL14.glStencilFunc(func, ref, mask);
    }

    public void glStencilMask(int mask) {
        if (base.isES) GLES20.glStencilMask(mask);
        else GL11.glStencilMask(mask);
    }

    public void glStencilOp(int fail, int zfail, int zpass) {
        if (base.isES) GLES20.glStencilOp(fail, zfail, zpass);
        else GL11.glStencilOp(fail, zfail, zpass);
    }

    public void glViewport(int x, int y, int width, int height) {
        if (base.isES) GLES20.glViewport(x, y, width, height);
        else GL11.glViewport(x, y, width, height);
    }

    public void glHint(int target, int mode) {
        if (base.isES) GLES20.glHint(target, mode);
        else GL11.glHint(target, mode);
    }

    public boolean glIsEnabled(int cap) {
        if (base.isES) return GLES20.glIsEnabled(cap);
        else return GL11.glIsEnabled(cap);
    }

    public void glPolygonOffset(float factor, float units) {
        if (base.isES) GLES20.glPolygonOffset(factor, units);
        else GL11.glPolygonOffset(factor, units);
    }

    public void glSampleCoverage(float value, boolean invert) {
        if (base.isES) GLES20.glSampleCoverage(value, invert);
        else GL13.glSampleCoverage(value, invert);
    }

    public void glPixelStoref(int pname, float param) {
        if (base.isES) throw new RuntimeException("Not supported in GL ES.");
        else GL11.glPixelStoref(pname, param);
    }

    public void glPixelStorei(int pname, int param) {
        if (base.isES) GL11.glPixelStorei(pname, param);
        else GL11.glPixelStorei(pname, param);
    }

    public void glColorMask(boolean red, boolean green, boolean blue, boolean alpha) {
        if (base.isES) GLES20.glColorMask(red, green, blue, alpha);
        else GL11.glColorMask(red, green, blue, alpha);
    }

    public void glReadBuffer(int src) {
        if (base.isES) GLES30.glReadBuffer(src);
        else GL11.glReadBuffer(src);
    }

    public void glBufferData(int target, Buffer data, int usage) {
        if (base.isES) GLES20.glBufferData(target, unwrapOrCopyByte(data), usage);
        else GL15.glBufferData(target, unwrapOrCopyByte(data), usage);
    }

    public void glBufferSubData(int target, long offset, Buffer data) {
        if (base.isES) GLES20.glBufferSubData(target, offset, unwrapOrCopyByte(data));
        else GL15.glBufferSubData(target, offset, unwrapOrCopyByte(data));
    }

    // GL2 and GLES2

    public void glGenTextures(Buffer.Int32 textures) {
        if (base.isES) GLES20.glGenTextures(unwrapOrCopyInt(textures));
        else GL11.glGenTextures(unwrapOrCopyInt(textures));
    }

    public void glGenTextures(int[] textures) {
        if (base.isES) GLES20.glGenTextures(textures);
        else GL11.glGenTextures(textures);
    }

    public void glUniformMatrix2x3fv(int location, boolean transpose, Buffer.Float32 value) {
        if (base.isES) GLES30.glUniformMatrix2x3fv(location, transpose, unwrapOrCopyFloat(value));
        else GL21.glUniformMatrix2x3fv(location, transpose, unwrapOrCopyFloat(value));
    }

    public void glUniformMatrix3x2fv(int location, boolean transpose, Buffer.Float32 value) {
        if (base.isES) GLES30.glUniformMatrix3x2fv(location, transpose, unwrapOrCopyFloat(value));
        else GL21.glUniformMatrix3x2fv(location, transpose, unwrapOrCopyFloat(value));
    }

    public void glUniformMatrix2x4fv(int location, boolean transpose, Buffer.Float32 value) {
        if (base.isES) GLES30.glUniformMatrix2x4fv(location, transpose, unwrapOrCopyFloat(value));
        else GL21.glUniformMatrix2x4fv(location, transpose, unwrapOrCopyFloat(value));
    }

    public void glUniformMatrix4x2fv(int location, boolean transpose, Buffer.Float32 value) {
        if (base.isES) GLES30.glUniformMatrix4x2fv(location, transpose, unwrapOrCopyFloat(value));
        else GL21.glUniformMatrix4x2fv(location, transpose, unwrapOrCopyFloat(value));
    }

    public void glUniformMatrix3x4fv(int location, boolean transpose, Buffer.Float32 value) {
        if (base.isES) GLES30.glUniformMatrix3x4fv(location, transpose, unwrapOrCopyFloat(value));
        else GL21.glUniformMatrix3x4fv(location, transpose, unwrapOrCopyFloat(value));
    }

    public void glUniformMatrix4x3fv(int location, boolean transpose, Buffer.Float32 value) {
        if (base.isES) GLES30.glUniformMatrix4x3fv(location, transpose, unwrapOrCopyFloat(value));
        else GL21.glUniformMatrix4x3fv(location, transpose, unwrapOrCopyFloat(value));
    }

    public void glBlendEquationSeparate(int modeRGB, int modeAlpha) {
        if (base.isES) GLES20.glBlendEquationSeparate(modeRGB, modeAlpha);
        else GL20.glBlendEquationSeparate(modeRGB, modeAlpha);
    }

    public void glDrawBuffers(Buffer.Int32 bufs) {
        if (base.isES) GLES30.glDrawBuffers(unwrapOrCopyInt(bufs));
        else GL20.glDrawBuffers(unwrapOrCopyInt(bufs));
    }

    public void glDrawBuffers(int[] bufs) {
        if (base.isES) GLES30.glDrawBuffers(bufs);
        else GL20.glDrawBuffers(bufs);
    }

    public void glStencilOpSeparate(int face, int sfail, int dpfail, int dppass) {
        if (base.isES) GLES20.glStencilOpSeparate(face, sfail, dpfail, dppass);
        else GL20.glStencilOpSeparate(face, sfail, dpfail, dppass);
    }

    public void glStencilFuncSeparate(int face, int func, int ref, int mask) {
        if (base.isES) GLES20.glStencilFuncSeparate(face, func, ref, mask);
        else GL20.glStencilFuncSeparate(face, func, ref, mask);
    }

    public void glStencilMaskSeparate(int face, int mask) {
        if (base.isES) GLES20.glStencilMaskSeparate(face, mask);
        else GL20.glStencilMaskSeparate(face, mask);
    }

    public void glAttachShader(int program, int shader) {
        if (base.isES) GLES20.glAttachShader(program, shader);
        else GL20.glAttachShader(program, shader);
    }

    public void glBindAttribLocation(int program, int index, CharArray name) {
        if (base.isES) GLES20.glBindAttribLocation(program, index, name.toString());
        else GL20.glBindAttribLocation(program, index, name.toString());
    }

    public void glCompileShader(int shader) {
        if (base.isES) GLES20.glCompileShader(shader);
        else GL20.glCompileShader(shader);
    }

    public int glCreateProgram() {
        if (base.isES) return GLES20.glCreateProgram();
        else return GL20.glCreateProgram();
    }

    public int glCreateShader(int type) {
        if (base.isES) return GLES20.glCreateShader(type);
        else return GL20.glCreateShader(type);
    }

    public void glDeleteProgram(int program) {
        if (base.isES) GLES20.glDeleteProgram(program);
        else GL20.glDeleteProgram(program);
    }

    public void glDeleteShader(int shader) {
        if (base.isES) GLES20.glDeleteShader(shader);
        else GL20.glDeleteShader(shader);
    }

    public void glDetachShader(int program, int shader) {
        if (base.isES) GLES20.glDetachShader(program, shader);
        else GL20.glDetachShader(program, shader);
    }

    public void glDisableVertexAttribArray(int index) {
        if (base.isES) GLES20.glDisableVertexAttribArray(index);
        else GL20.glDisableVertexAttribArray(index);
    }

    public void glEnableVertexAttribArray(int index) {
        if (base.isES) GLES20.glEnableVertexAttribArray(index);
        else GL20.glEnableVertexAttribArray(index);
    }

    public void glGetActiveAttrib(int program, int index, Buffer.Int32 length, Buffer.Int32 size, Buffer.Int32 type, Buffer.Int8 name) {
        if (base.isES) GLES20.glGetActiveAttrib(program, index, unwrapOrCopyInt(length), unwrapOrCopyInt(size), unwrapOrCopyInt(type), unwrapOrCopyByte(name));
        else GL20.glGetActiveAttrib(program, index, unwrapOrCopyInt(length), unwrapOrCopyInt(size), unwrapOrCopyInt(type), unwrapOrCopyByte(name));
    }

    public void glGetActiveUniform(int program, int index, Buffer.Int32 length, Buffer.Int32 size, Buffer.Int32 type, Buffer.Int8 name) {
        if (base.isES) GLES20.glGetActiveUniform(program, index, unwrapOrCopyInt(length), unwrapOrCopyInt(size), unwrapOrCopyInt(type), unwrapOrCopyByte(name));
        else GL20.glGetActiveUniform(program, index, unwrapOrCopyInt(length), unwrapOrCopyInt(size), unwrapOrCopyInt(type), unwrapOrCopyByte(name));
    }

    public void glGetAttachedShaders(int program, Buffer.Int32 count, Buffer.Int32 shaders) {
        if (base.isES) GLES20.glGetAttachedShaders(program, unwrapOrCopyInt(count), unwrapOrCopyInt(shaders));
        else GL20.glGetAttachedShaders(program, unwrapOrCopyInt(count), unwrapOrCopyInt(shaders));
    }

    public int glGetAttribLocation(int program, CharArray name) {
        if (base.isES) return GLES20.glGetAttribLocation(program, name.toString());
        else return GL20.glGetAttribLocation(program, name.toString());
    }

    public void glGetProgramiv(int program, int pname, Buffer.Int32 params) {
        if (base.isES) GLES20.glGetProgramiv(program, pname, unwrapOrCopyInt(params));
        else GL20.glGetProgramiv(program, pname, unwrapOrCopyInt(params));
    }

    public void glGetProgramiv(int program, int pname, int[] params) {
        if (base.isES) GLES20.glGetProgramiv(program, pname, params);
        else GL20.glGetProgramiv(program, pname, params);
    }

    public void glGetProgramInfoLog(int program, int[] length, Buffer.Int8 infoLog) {
        if (base.isES) GLES20.glGetProgramInfoLog(program, length, unwrapOrCopyByte(infoLog));
        else GL20.glGetProgramInfoLog(program, length, unwrapOrCopyByte(infoLog));
    }

    public void glGetShaderiv(int shader, int pname, Buffer.Int32 params) {
        if (base.isES) GLES20.glGetShaderiv(shader, pname, unwrapOrCopyInt(params));
        else GL20.glGetShaderiv(shader, pname, unwrapOrCopyInt(params));
    }

    public void glGetShaderiv(int shader, int pname, int[] params) {
        if (base.isES) GLES20.glGetShaderiv(shader, pname, params);
        else GL20.glGetShaderiv(shader, pname, params);
    }

    public void glGetShaderInfoLog(int shader, Buffer.Int32 length, Buffer.Int8 infoLog) {
        if (base.isES) GLES20.glGetShaderInfoLog(shader, unwrapOrCopyInt(length), unwrapOrCopyByte(infoLog));
        else GL20.glGetShaderInfoLog(shader, unwrapOrCopyInt(length), unwrapOrCopyByte(infoLog));
    }

    public void glGetShaderSource(int shader, Buffer.Int32 length, Buffer.Int8 source) {
        if (base.isES) GLES20.glGetShaderSource(shader, unwrapOrCopyInt(length), unwrapOrCopyByte(source));
        else GL20.glGetShaderSource(shader, unwrapOrCopyInt(length), unwrapOrCopyByte(source));
    }

    public int glGetUniformLocation(int program, CharArray name) {
        if (base.isES) return GLES20.glGetUniformLocation(program, name.toString());
        else return GL20.glGetUniformLocation(program, name.toString());
    }

    public void glGetUniformfv(int program, int location, Buffer.Float32 params) {
        if (base.isES) GLES20.glGetUniformfv(program, location, unwrapOrCopyFloat(params));
        else GL20.glGetUniformfv(program, location, unwrapOrCopyFloat(params));
    }

    public void glGetUniformiv(int program, int location, Buffer.Int32 params) {
        if (base.isES) GLES20.glGetUniformiv(program, location, unwrapOrCopyInt(params));
        else GL20.glGetUniformiv(program, location, unwrapOrCopyInt(params));
    }

    public void glGetVertexAttribfv(int index, int pname, Buffer.Float32 params) {
        if (base.isES) GLES20.glGetVertexAttribfv(index, pname, unwrapOrCopyFloat(params));
        else GL20.glGetVertexAttribfv(index, pname, unwrapOrCopyFloat(params));
    }

    public void glGetVertexAttribiv(int index, int pname, Buffer.Int32 params) {
        if (base.isES) GLES20.glGetVertexAttribiv(index, pname, unwrapOrCopyInt(params));
        else GL20.glGetVertexAttribiv(index, pname, unwrapOrCopyInt(params));
    }

    public void glGetVertexAttribPointerv(int index, int pname, Buffer.Int8 pointer) {
        PointerBuffer pb = PointerBuffer.create(unwrapOrCopyByte(pointer));
        if (base.isES) GLES20.glGetVertexAttribPointerv(index, pname, pb);
        else GL20.glGetVertexAttribPointerv(index, pname, pb);
    }

    public boolean glIsProgram(int program) {
        if (base.isES) return GLES20.glIsProgram(program);
        else return GL20.glIsProgram(program);
    }

    public boolean glIsShader(int shader) {
        if (base.isES) return GLES20.glIsShader(shader);
        else return GL20.glIsShader(shader);
    }

    public void glLinkProgram(int program) {
        if (base.isES) GLES20.glLinkProgram(program);
        else GL20.glLinkProgram(program);
    }

    public void glShaderSource(int shader, CharArray[] strings) {
        if (base.isES) GLES20.glShaderSource(shader, LGL.toString(strings));
        else GL20.glShaderSource(shader, LGL.toString(strings));
    }

    public void glUseProgram(int program) {
        if (base.isES) GLES20.glUseProgram(program);
        else GL20.glUseProgram(program);
    }

    public void glUniform1f(int location, float v0) {
        if (base.isES) GLES20.glUniform1f(location, v0);
        else GL20.glUniform1f(location, v0);
    }

    public void glUniform2f(int location, float v0, float v1) {
        if (base.isES) GLES20.glUniform2f(location, v0, v1);
        else GL20.glUniform2f(location, v0, v1);
    }

    public void glUniform3f(int location, float v0, float v1, float v2) {
        if (base.isES) GLES20.glUniform3f(location, v0, v1, v2);
        else GL20.glUniform3f(location, v0, v1, v2);
    }

    public void glUniform4f(int location, float v0, float v1, float v2, float v3) {
        if (base.isES) GLES20.glUniform4f(location, v0, v1, v2, v3);
        else GL20.glUniform4f(location, v0, v1, v2, v3);
    }

    public void glUniform1i(int location, int v0) {
        if (base.isES) GLES20.glUniform1i(location, v0);
        else GL20.glUniform1i(location, v0);
    }

    public void glUniform2i(int location, int v0, int v1) {
        if (base.isES) GLES20.glUniform2i(location, v0, v1);
        else GL20.glUniform2i(location, v0, v1);
    }

    public void glUniform3i(int location, int v0, int v1, int v2) {
        if (base.isES) GLES20.glUniform3i(location, v0, v1, v2);
        else GL20.glUniform3i(location, v0, v1, v2);
    }

    public void glUniform4i(int location, int v0, int v1, int v2, int v3) {
        if (base.isES) GLES20.glUniform4i(location, v0, v1, v2, v3);
        else GL20.glUniform4i(location, v0, v1, v2, v3);
    }

    public void glUniform1fv(int location, Buffer.Float32 value) {
        if (base.isES) GLES20.glUniform1fv(location, unwrapOrCopyFloat(value));
        else GL20.glUniform1fv(location, unwrapOrCopyFloat(value));
    }

    public void glUniform1fv(int location, float[] value) {
        if (base.isES) GLES20.glUniform1fv(location, value);
        else GL20.glUniform1fv(location, value);
    }

    public void glUniform2fv(int location, Buffer.Float32 value) {
        if (base.isES) GLES20.glUniform2fv(location, unwrapOrCopyFloat(value));
        else GL20.glUniform2fv(location, unwrapOrCopyFloat(value));
    }

    public void glUniform2fv(int location, float[] value) {
        if (base.isES) GLES20.glUniform2fv(location, value);
        else GL20.glUniform2fv(location, value);
    }

    public void glUniform3fv(int location, Buffer.Float32 value) {
        if (base.isES) GLES20.glUniform3fv(location, unwrapOrCopyFloat(value));
        else GL20.glUniform3fv(location, unwrapOrCopyFloat(value));
    }

    public void glUniform3fv(int location, float[] value) {
        if (base.isES) GLES20.glUniform3fv(location, value);
        else GL20.glUniform3fv(location, value);
    }

    public void glUniform4fv(int location, Buffer.Float32 value) {
        if (base.isES) GLES20.glUniform4fv(location, unwrapOrCopyFloat(value));
        else GL20.glUniform4fv(location, unwrapOrCopyFloat(value));
    }

    public void glUniform4fv(int location, float[] value) {
        if (base.isES) GLES20.glUniform4fv(location, value);
        else GL20.glUniform4fv(location, value);
    }

    public void glUniform1iv(int location, Buffer.Int32 value) {
        if (base.isES) GLES20.glUniform1iv(location, unwrapOrCopyInt(value));
        else GL20.glUniform1iv(location, unwrapOrCopyInt(value));
    }

    public void glUniform1iv(int location, int[] value) {
        if (base.isES) GLES20.glUniform1iv(location, value);
        else GL20.glUniform1iv(location, value);
    }

    public void glUniform2iv(int location, Buffer.Int32 value) {
        if (base.isES) GLES20.glUniform2iv(location, unwrapOrCopyInt(value));
        else GL20.glUniform2iv(location, unwrapOrCopyInt(value));
    }

    public void glUniform2iv(int location, int[] value) {
        if (base.isES) GLES20.glUniform2iv(location, value);
        else GL20.glUniform2iv(location, value);
    }

    public void glUniform3iv(int location, Buffer.Int32 value) {
        if (base.isES) GLES20.glUniform3iv(location, unwrapOrCopyInt(value));
        else GL20.glUniform3iv(location, unwrapOrCopyInt(value));
    }

    public void glUniform3iv(int location, int[] value) {
        if (base.isES) GLES20.glUniform3iv(location, value);
        else GL20.glUniform3iv(location, value);
    }

    public void glUniform4iv(int location, Buffer.Int32 value) {
        if (base.isES) GLES20.glUniform4iv(location, unwrapOrCopyInt(value));
        else GL20.glUniform4iv(location, unwrapOrCopyInt(value));
    }

    public void glUniform4iv(int location, int[] value) {
        if (base.isES) GLES20.glUniform4iv(location, value);
        else GL20.glUniform4iv(location, value);
    }

    public void glUniformMatrix2fv(int location, boolean transpose, Buffer.Float32 value) {
        if (base.isES) GLES20.glUniformMatrix2fv(location, transpose, unwrapOrCopyFloat(value));
        else GL20.glUniformMatrix2fv(location, transpose, unwrapOrCopyFloat(value));
    }

    public void glUniformMatrix2fv(int location, boolean transpose, float[] value) {
        if (base.isES) GLES20.glUniformMatrix2fv(location, transpose, value);
        else GL20.glUniformMatrix2fv(location, transpose, value);
    }

    public void glUniformMatrix3fv(int location, boolean transpose, Buffer.Float32 value) {
        if (base.isES) GLES20.glUniformMatrix3fv(location, transpose, unwrapOrCopyFloat(value));
        else GL20.glUniformMatrix3fv(location, transpose, unwrapOrCopyFloat(value));
    }

    public void glUniformMatrix3fv(int location, boolean transpose, float[] value) {
        if (base.isES) GLES20.glUniformMatrix3fv(location, transpose, value);
        else GL20.glUniformMatrix3fv(location, transpose, value);
    }

    public void glUniformMatrix4fv(int location, boolean transpose, Buffer.Float32 value) {
        if (base.isES) GLES20.glUniformMatrix4fv(location, transpose, unwrapOrCopyFloat(value));
        else GL20.glUniformMatrix4fv(location, transpose, unwrapOrCopyFloat(value));
    }

    public void glUniformMatrix4fv(int location, boolean transpose, float[] value) {
        if (base.isES) GLES20.glUniformMatrix4fv(location, transpose, value);
        else GL20.glUniformMatrix4fv(location, transpose, value);
    }

    public void glValidateProgram(int program) {
        if (base.isES) GLES20.glValidateProgram(program);
        else GL20.glValidateProgram(program);
    }

    public void glVertexAttrib1f(int index, float x) {
        if (base.isES) GLES20.glVertexAttrib1f(index, x);
        else GL20.glVertexAttrib1f(index, x);
    }

    public void glVertexAttrib1fv(int index, Buffer.Float32 v) {
        if (base.isES) GLES20.glVertexAttrib1fv(index, unwrapOrCopyFloat(v));
        else GL20.glVertexAttrib1fv(index, unwrapOrCopyFloat(v));
    }

    public void glVertexAttrib2f(int index, float x, float y) {
        if (base.isES) GLES20.glVertexAttrib2f(index, x, y);
        else GL20.glVertexAttrib2f(index, x, y);
    }

    public void glVertexAttrib2fv(int index, Buffer.Float32 v) {
        if (base.isES) GLES20.glVertexAttrib2fv(index, unwrapOrCopyFloat(v));
        else GL20.glVertexAttrib2fv(index, unwrapOrCopyFloat(v));
    }

    public void glVertexAttrib3f(int index, float x, float y, float z) {
        if (base.isES) GLES20.glVertexAttrib3f(index, x, y, z);
        else GL20.glVertexAttrib3f(index, x, y, z);
    }

    public void glVertexAttrib3fv(int index, Buffer.Float32 v) {
        if (base.isES) GLES20.glVertexAttrib3fv(index, unwrapOrCopyFloat(v));
        else GL20.glVertexAttrib3fv(index, unwrapOrCopyFloat(v));
    }

    public void glVertexAttrib4f(int index, float x, float y, float z, float w) {
        if (base.isES) GLES20.glVertexAttrib4f(index, x, y, z, w);
        else GL20.glVertexAttrib4f(index, x, y, z, w);
    }

    public void glVertexAttrib4fv(int index, Buffer.Float32 v) {
        if (base.isES) GLES20.glVertexAttrib4fv(index, unwrapOrCopyFloat(v));
        else GL20.glVertexAttrib4fv(index, unwrapOrCopyFloat(v));
    }

    public void glVertexAttribPointer(int index, int size, int type, boolean normalized, int stride, long pointer) {
        if (base.isES) GLES20.glVertexAttribPointer(index, size, type, normalized, stride, pointer);
        else GL20.glVertexAttribPointer(index, size, type, normalized, stride, pointer);
    }

    public void glGenSamplers(Buffer.Int32 samplers) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glDeleteSamplers(Buffer.Int32 samplers) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean glIsSampler(int sampler) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glBindSampler(int unit, int sampler) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glSamplerParameteri(int sampler, int pname, int param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glSamplerParameteriv(int sampler, int pname, Buffer.Int32 param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glSamplerParameterf(int sampler, int pname, float param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glSamplerParameterfv(int sampler, int pname, Buffer.Float32 param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glSamplerParameterIiv(int sampler, int pname, Buffer.Int32 param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glSamplerParameterIuiv(int sampler, int pname, Buffer.Int32 param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetSamplerParameteriv(int sampler, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetSamplerParameterIiv(int sampler, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetSamplerParameterfv(int sampler, int pname, Buffer.Float32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetSamplerParameterIuiv(int sampler, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glVertexAttribDivisor(int index, int divisor) {
        if (base.isES) GLES30.glVertexAttribDivisor(index, divisor);
        else GL33.glVertexAttribDivisor(index, divisor);
    }

    public void glDrawElementsBaseVertex(int mode, int count, int type, long indices, int basevertex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glDrawRangeElementsBaseVertex(int mode, int start, int end, int count, int type, long indices, int basevertex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glDrawElementsInstancedBaseVertex(int mode, int count, int type, long indices, int instancecount, int basevertex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public long glFenceSync(int condition, int flags) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean glIsSync(long sync) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glDeleteSync(long sync) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int glClientWaitSync(long sync, int flags, long timeout) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glWaitSync(long sync, int flags, long timeout) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetInteger64v(int pname, Buffer.Int64 data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetInteger64v(int pname, long[] data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetSynciv(long sync, int pname, Buffer.Int32 length, Buffer.Int32 values) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetInteger64i_v(int target, int index, Buffer.Int64 data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetBufferParameteri64v(int target, int pname, Buffer.Int8 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glFramebufferTexture(int target, int attachment, int texture, int level) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetMultisamplefv(int pname, int index, Buffer.Float32 val) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glSampleMaski(int maskNumber, int mask) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glDrawArraysInstanced(int mode, int first, int count, int instancecount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glDrawElementsInstanced(int mode, int count, int type, long indices, int instancecount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glTexBuffer(int target, int internalformat, int buffer) {
        if (base.isES) GLES32.glTexBuffer(target, internalformat, buffer);
        else GL31.glTexBuffer(target, internalformat, buffer);
    }

    public void glCopyBufferSubData(int readTarget, int writeTarget, long readOffset, long writeOffset, long size) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetUniformIndices(int program, int uniformCount, CharArray uniformNames, Buffer.Int32 uniformIndices) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetActiveUniformsiv(int program, Buffer.Int32 uniformIndices, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int glGetUniformBlockIndex(int program, CharArray uniformBlockName) {
        if (base.isES) return GLES30.glGetUniformBlockIndex(program, uniformBlockName.toString());
        else return GL31.glGetUniformBlockIndex(program, uniformBlockName.toString());
    }

    public void glGetActiveUniformBlockiv(int program, int uniformBlockIndex, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetActiveUniformBlockName(int program, int uniformBlockIndex, Buffer.Int32 length, Buffer.Int8 uniformBlockName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glUniformBlockBinding(int program, int uniformBlockIndex, int uniformBlockBinding) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glBindBufferRange(int target, int index, int buffer, long offset, long size) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glBindBufferBase(int target, int index, int buffer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetIntegeri_v(int target, int index, Buffer.Int32 data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glColorMaski(int index, boolean red, boolean green, boolean blue, boolean alpha) {
        if (base.isES) GLES32.glColorMaski(index, red, green, blue, alpha);
        else GL30.glColorMaski(index, red, green, blue, alpha);
    }

    public void glGetBooleani_v(int target, int index, Buffer.Int8 data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glEnablei(int target, int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glDisablei(int target, int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean glIsEnabledi(int target, int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glBeginTransformFeedback(int primitiveMode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glEndTransformFeedback() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glTransformFeedbackVaryings(int program, CharArray[] varyings, int bufferMode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetTransformFeedbackVarying(int program, int index, Buffer.Int32 length, Buffer.Int32 size, Buffer.Int32 type, Buffer.Int8 name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glVertexAttribIPointer(int index, int size, int type, int stride, long pointer) {
        if (base.isES) GLES30.glVertexAttribIPointer(index, size, type, stride, pointer);
        else GL30.glVertexAttribIPointer(index, size, type, stride, pointer);
    }

    public void glGetVertexAttribIiv(int index, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetVertexAttribIuiv(int index, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glVertexAttribI4i(int index, int x, int y, int z, int w) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glVertexAttribI4ui(int index, int x, int y, int z, int w) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glVertexAttribI4iv(int index, Buffer.Int32 v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glVertexAttribI4uiv(int index, Buffer.Int32 v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetUniformuiv(int program, int location, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int glGetFragDataLocation(int program, CharArray name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glUniform1ui(int location, int v0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glUniform2ui(int location, int v0, int v1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glUniform3ui(int location, int v0, int v1, int v2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glUniform4ui(int location, int v0, int v1, int v2, int v3) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glUniform1uiv(int location, Buffer.Int32 value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glUniform2uiv(int location, Buffer.Int32 value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glUniform3uiv(int location, Buffer.Int32 value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glUniform4uiv(int location, Buffer.Int32 value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glTexParameterIiv(int target, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glTexParameterIuiv(int target, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetTexParameterIiv(int target, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetTexParameterIuiv(int target, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glClearBufferiv(int buffer, int drawbuffer, Buffer.Int32 value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glClearBufferuiv(int buffer, int drawbuffer, Buffer.Int32 value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glClearBufferfv(int buffer, int drawbuffer, Buffer.Float32 value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glClearBufferfi(int buffer, int drawbuffer, float depth, int stencil) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public byte glGetStringi(int name, int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean glIsRenderbuffer(int renderbuffer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glBindRenderbuffer(int target, int renderbuffer) {
        if (base.isES) GLES20.glBindRenderbuffer(target, renderbuffer);
        else GL30.glBindRenderbuffer(target, renderbuffer);
    }

    public void glDeleteRenderbuffers(Buffer.Int32 renderbuffers) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGenBuffers(Buffer.Int32 buffers) {
        if (base.isES) GLES20.glGenBuffers(unwrapOrCopyInt(buffers));
        else GL15.glGenBuffers(unwrapOrCopyInt(buffers));
    }

    public void glGenBuffers(int[] buffers) {
        if (base.isES) GLES20.glGenBuffers(buffers);
        else GL15.glGenBuffers(buffers);
    }

    public void glGenRenderbuffers(Buffer.Int32 renderbuffers) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glRenderbufferStorage(int target, int internalformat, int width, int height) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glGetRenderbufferParameteriv(int target, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean glIsFramebuffer(int framebuffer) {
        if (base.isES) return GLES20.glIsFramebuffer(framebuffer);
        else return GL30.glIsFramebuffer(framebuffer);
    }

    public void glBindFramebuffer(int target, int framebuffer) {
        if (base.isES) GLES20.glBindFramebuffer(target, framebuffer);
        else GL30.glBindFramebuffer(target, framebuffer);
    }

    public void glDeleteFramebuffers(int[] framebuffers) {
        if (base.isES) GLES20.glDeleteFramebuffers(framebuffers);
        else GL30.glDeleteFramebuffers(framebuffers);
    }

    public void glDeleteFramebuffers(Buffer.Int32 framebuffers) {
        if (base.isES) GLES20.glDeleteFramebuffers(unwrapOrCopyInt(framebuffers));
        else GL30.glDeleteFramebuffers(unwrapOrCopyInt(framebuffers));
    }

    public void glGenFramebuffers(int[] framebuffers) {
        if (base.isES) GLES20.glGenFramebuffers(framebuffers);
        else GL30.glGenFramebuffers(framebuffers);
    }

    public void glGenFramebuffers(Buffer.Int32 framebuffers) {
        if (base.isES) GLES20.glGenFramebuffers(unwrapOrCopyInt(framebuffers));
        else GL30.glGenFramebuffers(unwrapOrCopyInt(framebuffers));
    }

    public int glCheckFramebufferStatus(int target) {
        if (base.isES) return GLES20.glCheckFramebufferStatus(target);
        else return GL30.glCheckFramebufferStatus(target);
    }

    public void glFramebufferTexture2D(int target, int attachment, int textarget, int texture, int level) {
        if (base.isES) GLES20.glFramebufferTexture2D(target, attachment, textarget, texture, level);
        else GL30.glFramebufferTexture2D(target, attachment, textarget, texture, level);
    }

    public void glFramebufferRenderbuffer(int target, int attachment, int renderbuffertarget, int renderbuffer) {
        if (base.isES) GLES20.glFramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer);
        else GL30.glFramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer);
    }

    public void glGetFramebufferAttachmentParameteriv(int target, int attachment, int pname, Buffer.Int32 params) {
        if (base.isES) GLES20.glGetFramebufferAttachmentParameteriv(target, attachment, pname, unwrapOrCopyInt(params));
        else GL30.glGetFramebufferAttachmentParameteriv(target, attachment, pname, unwrapOrCopyInt(params));
    }

    public void glGenerateMipmap(int target) {
        if (base.isES) GLES20.glGenerateMipmap(target);
        else GL30.glGenerateMipmap(target);
    }

    public void glBlitFramebuffer(int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, int mask, int filter) {
        if (base.isES) GLES30.glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
        else GL30.glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
    }

    public void glRenderbufferStorageMultisample(int target, int samples, int internalformat, int width, int height) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glFramebufferTextureLayer(int target, int attachment, int texture, int level, int layer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glMapBufferRange(int target, long offset, long length, int access) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glFlushMappedBufferRange(int target, long offset, long length) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glBindVertexArray(int array) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void glDeleteVertexArrays(Buffer.Int32 arrays) {
        if (base.isES) GLES30.glDeleteVertexArrays(unwrapOrCopyInt(arrays));
        else GL30.glDeleteVertexArrays(unwrapOrCopyInt(arrays));
    }

    public void glDeleteVertexArrays(int[] arrays) {
        if (base.isES) GLES30.glDeleteVertexArrays(arrays);
        else GL30.glDeleteVertexArrays(arrays);
    }

    public void glGenVertexArrays(Buffer.Int32 arrays) {
        if (base.isES) GLES30.glGenVertexArrays(unwrapOrCopyInt(arrays));
        else GL30.glGenVertexArrays(unwrapOrCopyInt(arrays));
    }

    public void glGenVertexArrays(int[] arrays) {
        if (base.isES) GLES30.glGenVertexArrays(arrays);
        else GL30.glGenVertexArrays(arrays);
    }

    public boolean glIsVertexArray(int array) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
