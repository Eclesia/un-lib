
package science.unlicense.engine.lwjgl.opengl;

import org.lwjgl.opengles.GLES20;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.UnimplementedException;
import static science.unlicense.gpu.api.GLBuffer.*;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GLES220;

/**
 *
 * @author Johann Sorel
 */
public class LGL2ES2 extends AbstractLGLES implements GLES220,science.unlicense.gpu.api.opengl.GL2ES2{

    LGL2ES2(LGL gl) {
        super(gl);
    }

    @Override
    public GL2ES2 asGL2ES2() {
        return this;
    }

    @Override
    public void glCompressedTexImage2D(int target, int level, int internalformat, int width, int height, int border, Buffer data) {
        GLES20.glCompressedTexImage2D(target, level, internalformat, width, height, border, unwrapOrCopyByte(data.asInt8()));
    }

    @Override
    public void glCompressedTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, Buffer data) {
        GLES20.glCompressedTexSubImage2D(target,  level,  xoffset,  yoffset,  width,  height,  format, unwrapOrCopyByte(data.asInt8()));
    }

    @Override
    public void glCopyTexImage2D(int target, int level, int internalformat, int x, int y, int width, int height, int border) {
        GLES20.glCopyTexImage2D(target, level, internalformat, x, y, width, height, border);
    }

    @Override
    public void glCopyTexSubImage2D(int target, int level, int xoffset, int yoffset, int x, int y, int width, int height) {
        GLES20.glCopyTexSubImage2D( target,  level,  xoffset,  yoffset,  x,  y,  width,  height);
    }

    @Override
    public void glDeleteBuffers(Buffer.Int32 buffers) {
        GLES20.glDeleteBuffers(unwrapOrCopyInt(buffers));
    }

    @Override
    public void glDeleteBuffers(int[] buffers) {
        GLES20.glDeleteBuffers(buffers);
    }

    @Override
    public void glDeleteTextures(Buffer.Int32 textures) {
        GLES20.glDeleteTextures(unwrapOrCopyInt(textures));
    }

    @Override
    public void glGetBooleanv(int pname, Buffer.Int8 data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetBufferParameteriv(int target, int pname, Buffer.Int32 params) {
        GLES20.glGetBufferParameteriv( target, pname, unwrapOrCopyInt(params));
    }

    @Override
    public int glGetError() {
        return GLES20.glGetError();
    }

    @Override
    public void glGetFloatv(int pname, Buffer.Float32 data) {
        GLES20.glGetFloatv( pname, unwrapOrCopyFloat(data));
    }

    @Override
    public void glGetFloatv(int pname, float[] data) {
        GLES20.glGetFloatv( pname, data);
    }

    @Override
    public void glGetIntegerv(int pname, Buffer.Int32 data) {
        GLES20.glGetIntegerv( pname, unwrapOrCopyInt(data));
    }

    @Override
    public void glGetIntegerv(int pname, int[] data) {
        GLES20.glGetIntegerv( pname, data);
    }

    @Override
    public void glGetShaderPrecisionFormat(int shadertype, int precisiontype, Buffer.Int32 range, Buffer.Int32 precision) {
        GLES20.glGetShaderPrecisionFormat( shadertype, precisiontype, unwrapOrCopyInt(range), unwrapOrCopyInt(precision));
    }

    @Override
    public byte glGetString(int name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTexParameterfv(int target, int pname, Buffer.Float32 params) {
        GLES20.glGetTexParameterfv( target, pname, unwrapOrCopyFloat(params));
    }

    @Override
    public void glGetTexParameteriv(int target, int pname, Buffer.Int32 params) {
        GLES20.glGetTexParameteriv( target, pname, unwrapOrCopyInt(params));
    }

    @Override
    public boolean glIsBuffer(int buffer) {
        return GLES20.glIsBuffer( buffer);
    }

    @Override
    public boolean glIsTexture(int texture) {
        return GLES20.glIsTexture( texture);
    }

    @Override
    public void glPixelStorei(int pname, int param) {
        GLES20.glPixelStorei( pname, param);
    }

    @Override
    public void glReadPixels(int x, int y, int width, int height, int format, int type, Buffer.Int8 pixels) {
        GLES20.glReadPixels( x, y, width, height, format, type, unwrapOrCopyByte(pixels));
    }

    @Override
    public void glReleaseShaderCompiler() {
        GLES20.glReleaseShaderCompiler();
    }

    @Override
    public void glShaderBinary(Buffer.Int32 shaders, int binaryformat, Buffer.Int8 binary) {
        GLES20.glShaderBinary(unwrapOrCopyInt(shaders), binaryformat, unwrapOrCopyByte(binary));
    }

    @Override
    public void glTexImage2D(int target, int level, int internalformat, int width, int height, int border, int format, int type, Buffer pixels) {
//        if (pixels instanceof Buffer.Int8) GLES20.glTexImage2D(target, level, internalformat, width, height, border, format, type, (Buffer.Int8) pixels);
//        if (pixels instanceof Buffer.Int16) GLES20.glTexImage2D(target, level, internalformat, width, height, border, format, type, (Buffer.Int16) pixels);
//        if (pixels instanceof Buffer.Int32) GLES20.glTexImage2D(target, level, internalformat, width, height, border, format, type, (Buffer.Int32) pixels);
//        if (pixels instanceof Buffer.Float32) GLES20.glTexImage2D(target, level, internalformat, width, height, border, format, type, (Buffer.Float32) pixels);
        throw new InvalidArgumentException("Unsupported buffer "+pixels);
    }

    @Override
    public void glTexParameterf(int target, int pname, float param) {
        GLES20.glTexParameterf(target, pname, param);
    }

    @Override
    public void glTexParameterfv(int target, int pname, Buffer.Float32 params) {
        GLES20.glTexParameterfv(target, pname, unwrapOrCopyFloat(params));
    }

    @Override
    public void glTexParameteri(int target, int pname, int param) {
        GLES20.glTexParameteri(target, pname, param);
    }

    @Override
    public void glTexParameteriv(int target, int pname, Buffer.Int32 params) {
        GLES20.glTexParameteriv(target, pname, unwrapOrCopyInt(params));
    }

    @Override
    public void glTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer.Int8 pixels) {
        GLES20.glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, unwrapOrCopyByte(pixels));
    }

}
