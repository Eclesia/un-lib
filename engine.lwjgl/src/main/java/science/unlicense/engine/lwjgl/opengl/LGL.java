
package science.unlicense.engine.lwjgl.opengl;

import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.gpu.api.GLBufferFactory;
import science.unlicense.gpu.api.opengl.GL1;
import science.unlicense.gpu.api.opengl.GL1ES;
import science.unlicense.gpu.api.opengl.GL2;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GL2ES3;
import science.unlicense.gpu.api.opengl.GL3;
import science.unlicense.gpu.api.opengl.GL4;

/**
 *
 * @author Johann Sorel
 */
public class LGL implements science.unlicense.gpu.api.opengl.GL{

    boolean isES;
    private GL1 gl1;
    private GL2 gl2;
    private GL3 gl3;
    private GL4 gl4;
    private GL1ES gl1es;
    private GL2ES2 gl2es2;
    private GL2ES3 gl2es3;

    public LGL() {
    }

    @Override
    public BufferFactory getBufferFactory() {
        return GLBufferFactory.INSTANCE;
    }

    @Override
    public boolean isGL1() {
        return true; //TODO find correct way to detect version in lwjgl
    }

    @Override
    public boolean isGL2() {
        return true; //TODO find correct way to detect version in lwjgl
    }

    @Override
    public boolean isGL3() {
        return true; //TODO find correct way to detect version in lwjgl
    }

    @Override
    public boolean isGL4() {
        return true; //TODO find correct way to detect version in lwjgl
    }

    @Override
    public boolean isGL1ES() {
        return true; //TODO find correct way to detect version in lwjgl
    }

    @Override
    public boolean isGL2ES2() {
        return true; //TODO find correct way to detect version in lwjgl
    }

    @Override
    public boolean isGL2ES3() {
        return true; //TODO find correct way to detect version in lwjgl
    }

    @Override
    public GL1 asGL1() {
        if (gl1==null) gl1 = new LGL1(this);
        return gl1;
    }

    @Override
    public GL2 asGL2() {
        if (gl2==null) gl2 = new LGL2(this);
        return gl2;
    }

    @Override
    public GL3 asGL3() {
        if (gl3==null) gl3 = new LGL3(this);
        return gl3;
    }

    @Override
    public GL4 asGL4() {
        if (gl4==null) gl4 = new LGL4(this);
        return gl4;
    }

    @Override
    public GL1ES asGL1ES() {
        if (gl1es==null) gl1es = new LGL1ES1(this);
        return gl1es;
    }

    @Override
    public GL2ES2 asGL2ES2() {
        if (gl2es2==null) gl2es2 = new LGL2ES2(this);
        return gl2es2;
    }

    @Override
    public GL2ES3 asGL2ES3() {
        if (gl2es3==null) gl2es3 = new LGL2ES3(this);
        return gl2es3;
    }

    static String[] toString(CharArray[] strings) {
        final String[] jvmStrings = new String[strings.length];
        for (int i=0;i<jvmStrings.length;i++) jvmStrings[i] = strings[i].toString();
        return jvmStrings;
    }


}
