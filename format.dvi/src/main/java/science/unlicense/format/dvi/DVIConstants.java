

package science.unlicense.format.dvi;

/**
 * DVI constants.
 *
 * @author Johann Sorel
 */
public final class DVIConstants {

    private DVIConstants(){}

}
