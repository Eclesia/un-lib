

package science.unlicense.format.dvi;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * Device independent file format.
 *
 * Resource :
 * http://en.wikipedia.org/wiki/Device_independent_file_format
 *
 * @author Johann Sorel
 */
public class DVIFormat extends DefaultFormat {

    public DVIFormat() {
        super(new Chars("dvi"));
        shortName = new Chars("DVI");
        longName = new Chars("Device independent file format");
        mimeTypes.add(new Chars("application/x-dvi"));
        extensions.add(new Chars("dvi"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
