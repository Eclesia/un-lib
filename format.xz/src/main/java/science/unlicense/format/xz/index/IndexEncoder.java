/*
 * IndexEncoder
 *
 * Author: Lasse Collin <lasse.collin@tukaani.org>
 *
 * This file has been put into the public domain.
 * You can do whatever you want with this file.
 */

package science.unlicense.format.xz.index;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.xz.common.EncoderUtil;
import science.unlicense.format.xz.XZIOException;
import science.unlicense.encoding.impl.cryptography.hash.CRC32;
import science.unlicense.encoding.impl.cryptography.hash.HashOutputStream;

public class IndexEncoder extends IndexBase {
    private final Sequence records = new ArraySequence();

    public IndexEncoder() {
        super(new XZIOException("XZ Stream or its Index has grown too big"));
    }

    public void add(long unpaddedSize, long uncompressedSize)
            throws XZIOException {
        super.add(unpaddedSize, uncompressedSize);
        records.add(new IndexRecord(unpaddedSize, uncompressedSize));
    }

    public void encode(ByteOutputStream out) throws IOException {
        CRC32 crc32 = new CRC32();
        HashOutputStream outChecked = new HashOutputStream(crc32,out);

        // Index Indicator
        outChecked.write((byte) 0x00);

        // Number of Records
        EncoderUtil.encodeVLI(outChecked, recordCount);

        // List of Records
        for (Iterator i = records.createIterator(); i.hasNext(); ) {
            IndexRecord record = (IndexRecord) i.next();
            EncoderUtil.encodeVLI(outChecked, record.unpadded);
            EncoderUtil.encodeVLI(outChecked, record.uncompressed);
        }

        // Index Padding
        for (int i = getIndexPaddingSize(); i > 0; --i)
            outChecked.write((byte) 0x00);

        // CRC32
        long value = crc32.getResultLong();
        for (int i = 0; i < 4; ++i)
            out.write((byte) (value >>> (i * 8)));
    }
}
