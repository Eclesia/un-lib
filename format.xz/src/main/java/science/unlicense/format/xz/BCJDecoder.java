/*
 * BCJDecoder
 *
 * Author: Lasse Collin <lasse.collin@tukaani.org>
 *
 * This file has been put into the public domain.
 * You can do whatever you want with this file.
 */

package science.unlicense.format.xz;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.format.xz.simple.ARM;
import science.unlicense.format.xz.simple.ARMThumb;
import science.unlicense.format.xz.simple.IA64;
import science.unlicense.format.xz.simple.PowerPC;
import science.unlicense.format.xz.simple.SPARC;
import science.unlicense.format.xz.simple.SimpleFilter;
import science.unlicense.format.xz.simple.X86;

class BCJDecoder extends BCJCoder implements FilterDecoder {
    private final long filterID;
    private final int startOffset;

    BCJDecoder(long filterID, byte[] props)
            throws UnsupportedOptionsException {
        assert isBCJFilterID(filterID);
        this.filterID = filterID;

        if (props.length == 0) {
            startOffset = 0;
        } else if (props.length == 4) {
            int n = 0;
            for (int i = 0; i < 4; ++i)
                n |= (props[i] & 0xFF) << (i * 8);

            startOffset = n;
        } else {
            throw new UnsupportedOptionsException(
                    "Unsupported BCJ filter properties");
        }
    }

    public int getMemoryUsage() {
        return SimpleInputStream.getMemoryUsage();
    }

    public ByteInputStream getInputStream(ByteInputStream in) {
        SimpleFilter simpleFilter = null;

        if (filterID == X86_FILTER_ID)
            simpleFilter = new X86(false, startOffset);
        else if (filterID == POWERPC_FILTER_ID)
            simpleFilter = new PowerPC(false, startOffset);
        else if (filterID == IA64_FILTER_ID)
            simpleFilter = new IA64(false, startOffset);
        else if (filterID == ARM_FILTER_ID)
            simpleFilter = new ARM(false, startOffset);
        else if (filterID == ARMTHUMB_FILTER_ID)
            simpleFilter = new ARMThumb(false, startOffset);
        else if (filterID == SPARC_FILTER_ID)
            simpleFilter = new SPARC(false, startOffset);
        else
            assert false;

        return new SimpleInputStream(in, simpleFilter);
    }
}
