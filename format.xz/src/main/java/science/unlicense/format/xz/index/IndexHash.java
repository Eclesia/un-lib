/*
 * IndexHash
 *
 * Author: Lasse Collin <lasse.collin@tukaani.org>
 *
 * This file has been put into the public domain.
 * You can do whatever you want with this file.
 */

package science.unlicense.format.xz.index;

import java.nio.ByteBuffer;
import science.unlicense.common.api.Arrays;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.xz.common.DecoderUtil;
import science.unlicense.format.xz.XZIOException;
import science.unlicense.format.xz.CorruptedInputException;
import science.unlicense.encoding.impl.cryptography.hash.CRC32;
import science.unlicense.encoding.impl.cryptography.hash.HashFunction;
import science.unlicense.encoding.impl.cryptography.hash.HashInputStream;
import science.unlicense.encoding.impl.cryptography.hash.SHA256;

public class IndexHash extends IndexBase {
    private HashFunction hash;

    public IndexHash() {
        super(new CorruptedInputException());
        hash = new SHA256();
    }

    public void add(long unpaddedSize, long uncompressedSize)
            throws XZIOException {
        super.add(unpaddedSize, uncompressedSize);

        ByteBuffer buf = ByteBuffer.allocate(2 * 8);
        buf.putLong(unpaddedSize);
        buf.putLong(uncompressedSize);
        hash.update(buf.array());
    }

    public void validate(ByteInputStream in) throws IOException {
        // Index Indicator (0x00) has already been read by BlockInputStream
        // so add 0x00 to the CRC32 here.
        HashFunction crc32 = new CRC32();
        crc32.update('\0');
        HashInputStream inChecked = new HashInputStream(crc32, in);

        // Get and validate the Number of Records field.
        long storedRecordCount = DecoderUtil.decodeVLI(inChecked);
        if (storedRecordCount != recordCount)
            throw new CorruptedInputException("XZ Index is corrupt");

        // Decode and hash the Index field and compare it to
        // the hash value calculated from the decoded Blocks.
        IndexHash stored = new IndexHash();
        for (long i = 0; i < recordCount; ++i) {
            long unpaddedSize = DecoderUtil.decodeVLI(inChecked);
            long uncompressedSize = DecoderUtil.decodeVLI(inChecked);

            try {
                stored.add(unpaddedSize, uncompressedSize);
            } catch (XZIOException e) {
                throw new CorruptedInputException("XZ Index is corrupt");
            }

            if (stored.blocksSum > blocksSum
                    || stored.uncompressedSum > uncompressedSum
                    || stored.indexListSize > indexListSize)
                throw new CorruptedInputException("XZ Index is corrupt");
        }

        byte[] sh = stored.hash.getResultBytes();
        stored.hash.reset();
        byte[] hs = hash.getResultBytes();
        hash.reset();


        if (stored.blocksSum != blocksSum
                || stored.uncompressedSum != uncompressedSum
                || stored.indexListSize != indexListSize
                || !Arrays.equals(sh, hs))
            throw new CorruptedInputException("XZ Index is corrupt");

        // Index Padding
        DataInputStream inData = new DataInputStream(inChecked);
        for (int i = getIndexPaddingSize(); i > 0; --i)
            if (inData.readUByte() != 0x00)
                throw new CorruptedInputException("XZ Index is corrupt");

        // CRC32
        long value = crc32.getResultLong();
        for (int i = 0; i < 4; ++i)
            if (((value >>> (i * 8)) & 0xFF) != inData.readUByte())
                throw new CorruptedInputException("XZ Index is corrupt");
    }
}
