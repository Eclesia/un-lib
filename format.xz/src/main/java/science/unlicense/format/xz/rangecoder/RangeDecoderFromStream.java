/*
 * RangeDecoderFromStream
 *
 * Authors: Lasse Collin <lasse.collin@tukaani.org>
 *          Igor Pavlov <http://7-zip.org/>
 *
 * This file has been put into the public domain.
 * You can do whatever you want with this file.
 */

package science.unlicense.format.xz.rangecoder;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.xz.CorruptedInputException;

public final class RangeDecoderFromStream extends RangeDecoder {
    private final DataInputStream inData;

    public RangeDecoderFromStream(ByteInputStream in) throws IOException {
        inData = new DataInputStream(in);

        if (inData.readUByte() != 0x00)
            throw new CorruptedInputException();

        code = inData.readInt();
        range = 0xFFFFFFFF;
    }

    public boolean isFinished() {
        return code == 0;
    }

    public void normalize() throws IOException {
        if ((range & TOP_MASK) == 0) {
            code = (code << SHIFT_BITS) | inData.readUByte();
            range <<= SHIFT_BITS;
        }
    }
}
