/*
 * FilterDecoder
 *
 * Author: Lasse Collin <lasse.collin@tukaani.org>
 *
 * This file has been put into the public domain.
 * You can do whatever you want with this file.
 */

package science.unlicense.format.xz;

import science.unlicense.encoding.api.io.ByteInputStream;


interface FilterDecoder extends FilterCoder {
    int getMemoryUsage();
    ByteInputStream getInputStream(ByteInputStream in);
}
