/*
 * BlockOutputStream
 *
 * Author: Lasse Collin <lasse.collin@tukaani.org>
 *
 * This file has been put into the public domain.
 * You can do whatever you want with this file.
 */

package science.unlicense.format.xz;

import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.xz.common.EncoderUtil;
import science.unlicense.encoding.impl.cryptography.hash.HashFunction;
import science.unlicense.encoding.impl.io.CounterOutputStream;

class BlockOutputStream extends FinishableOutputStream {

    private final ByteOutputStream out;
    private final CounterOutputStream outCounted;
    private FinishableOutputStream filterChain;
    private final HashFunction check;

    private final int headerSize;
    private final long compressedSizeLimit;
    private long uncompressedSize = 0;

    private final byte[] tempBuf = new byte[1];

    public BlockOutputStream(ByteOutputStream out, FilterEncoder[] filters,
                             HashFunction check) throws IOException {
        this.out = out;
        this.check = check;

        // Initialize the filter chain.
        outCounted = new CounterOutputStream(out);
        filterChain = new FinishableWrapperOutputStream(outCounted);
        for (int i = filters.length - 1; i >= 0; --i)
            filterChain = filters[i].getOutputStream(filterChain);

        // Prepare to encode the Block Header field.
        ArrayOutputStream bufStream = new ArrayOutputStream();

        // Write a dummy Block Header Size field. The real value is written
        // once everything else except CRC32 has been written.
        bufStream.write((byte) 0x00);

        // Write Block Flags. Storing Compressed Size or Uncompressed Size
        // isn't supported for now.
        bufStream.write((byte) (filters.length - 1));

        // List of Filter Flags
        for (int i = 0; i < filters.length; ++i) {
            EncoderUtil.encodeVLI(bufStream, filters[i].getFilterID());
            byte[] filterProps = filters[i].getFilterProps();
            EncoderUtil.encodeVLI(bufStream, filterProps.length);
            bufStream.write(filterProps);
        }

        // Header Padding
        while ((bufStream.getBuffer().getSize() & 3) != 0)
            bufStream.write((byte) 0x00);

        byte[] buf = bufStream.getBuffer().toArrayByte();

        // Total size of the Block Header: Take the size of the CRC32 field
        // into account.
        headerSize = buf.length + 4;

        // This is just a sanity check.
        if (headerSize > EncoderUtil.BLOCK_HEADER_SIZE_MAX)
            throw new UnsupportedOptionsException();

        // Block Header Size
        buf[0] = (byte) (buf.length / 4);

        // Write the Block Header field to the output stream.
        out.write(buf);
        EncoderUtil.writeCRC32(out, buf);

        // Calculate the maximum allowed size of the Compressed Data field.
        // It is hard to exceed it so this is mostly to be pedantic.
        compressedSizeLimit = (EncoderUtil.VLI_MAX & ~3)
                              - headerSize - check.getResultLength()/8;
    }

    public void write(int b) throws IOException {
        tempBuf[0] = (byte) b;
        write(tempBuf, 0, 1);
    }

    public void write(byte[] buf, int off, int len) throws IOException {
        filterChain.write(buf, off, len);
        check.update(buf, off, len);
        uncompressedSize += len;
        validate();
    }

    public void flush() throws IOException {
        filterChain.flush();
        validate();
    }

    public void finish() throws IOException {
        // Finish the Compressed Data field.
        filterChain.finish();
        validate();

        // Block Padding
        for (long i = outCounted.getCounterValue(); (i & 3) != 0; ++i)
            out.write((byte) 0x00);

        // Check
        byte[] rb = check.getResultBytes();
        check.reset();
        out.write(rb);
    }

    private void validate() throws IOException {
        long compressedSize = outCounted.getCounterValue();

        // It is very hard to trigger this exception.
        // This is just to be pedantic.
        if (compressedSize < 0 || compressedSize > compressedSizeLimit
                || uncompressedSize < 0)
            throw new XZIOException("XZ Stream has grown too big");
    }

    public long getUnpaddedSize() {
        return headerSize + outCounted.getCounterValue() + check.getResultLength()/8;
    }

    public long getUncompressedSize() {
        return uncompressedSize;
    }

    @Override
    public void write(byte b) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() throws IOException {
    }
}
