
package science.unlicense.format.xz;

import science.unlicense.encoding.impl.cryptography.hash.CRC32;
import science.unlicense.encoding.impl.cryptography.hash.CRC64_ECMA;
import science.unlicense.encoding.impl.cryptography.hash.HashFunction;
import science.unlicense.encoding.impl.cryptography.hash.None;
import science.unlicense.encoding.impl.cryptography.hash.SHA256;

/**
 *
 * @author Johann Sorel
 */
public class Hashs {

    private Hashs() {}

    public static HashFunction getInstance(int checkType) throws UnsupportedOptionsException {
        switch (checkType) {
            case XZ.CHECK_NONE: return new None();
            case XZ.CHECK_CRC32: return new CRC32();
            case XZ.CHECK_CRC64: return new CRC64_ECMA();
            case XZ.CHECK_SHA256: return new SHA256();
            default: throw new UnsupportedOptionsException("Unknown check type : "+checkType);
        }
    }

}
