/*
 * EncoderUtil
 *
 * Author: Lasse Collin <lasse.collin@tukaani.org>
 *
 * This file has been put into the public domain.
 * You can do whatever you want with this file.
 */

package science.unlicense.format.xz.common;

import java.util.zip.CRC32;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;

public class EncoderUtil extends Util {
    
    public static void writeCRC32(ByteOutputStream out, byte[] buf)
            throws IOException {
        CRC32 crc32 = new CRC32();
        crc32.update(buf);
        long value = crc32.getValue();

        for (int i = 0; i < 4; ++i)
            out.write((byte) (value >>> (i * 8)));
    }

    public static void encodeVLI(ByteOutputStream out, long num)
            throws IOException {
        while (num >= 0x80) {
            out.write((byte) (num | 0x80));
            num >>>= 7;
        }

        out.write((byte) num);
    }
}
