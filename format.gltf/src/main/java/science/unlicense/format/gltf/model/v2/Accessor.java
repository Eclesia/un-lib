
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.Vector;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Accessor extends GltfDocument {

    public static final Chars PROP_BUFFERVIEW = Chars.constant("bufferView");
    public static final Chars PROP_BYTEOFFSET = Chars.constant("byteOffset");
    public static final Chars PROP_COMPONENTTYPE = Chars.constant("componentType");
    public static final Chars PROP_NORMALIZED = Chars.constant("normalized");
    public static final Chars PROP_COUNT = Chars.constant("count");
    public static final Chars PROP_TYPE = Chars.constant("type");
    public static final Chars PROP_MAX = Chars.constant("max");
    public static final Chars PROP_MIN = Chars.constant("min");
    public static final Chars PROP_SPARSE = Chars.constant("sparse");
    public static final Chars PROP_NAME = Chars.constant("name");

    public Integer getBufferView() {
        return getInteger(PROP_BUFFERVIEW, null);
    }

    public void setBufferView(Integer value) {
        setPropertyValue(PROP_BUFFERVIEW, value);
    }

    public Integer getByteOffset() {
        return getInteger(PROP_BYTEOFFSET, 0);
    }

    public void setByteOffset(Integer value) {
        setPropertyValue(PROP_BYTEOFFSET, value);
    }

    public Integer getComponentType() {
        return getInteger(PROP_COMPONENTTYPE, null);
    }

    public void setComponentType(Integer value) {
        setPropertyValue(PROP_COMPONENTTYPE, value);
    }

    public Boolean getNormalized() {
        return getBoolean(PROP_NORMALIZED, Boolean.FALSE);
    }

    public void setNormalized(Boolean value) {
        setPropertyValue(PROP_NORMALIZED, value);
    }

    public Integer getCount() {
        return getInteger(PROP_COUNT, null);
    }

    public void setCount(Integer value) {
        setPropertyValue(PROP_COUNT, value);
    }

    public Chars getAttributeType() {
        return getChars(PROP_TYPE, null);
    }

    public void setAttributeType(Chars value) {
        setPropertyValue(PROP_TYPE, value);
    }

    public Vector getMax() {
        return getVector(PROP_MAX, null);
    }

    public void setMax(Tuple value) {
        setTuple(PROP_MAX, value);
    }

    public Vector getMin() {
        return getVector(PROP_MIN, null);
    }

    public void setMin(Tuple value) {
        setTuple(PROP_MIN, value);
    }

    public Sparse getSparse() {
        return (Sparse) getDocument(PROP_SPARSE, Sparse.class);
    }

    public void setSparse(Sparse value) {
        setPropertyValue(PROP_SPARSE, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }

}
