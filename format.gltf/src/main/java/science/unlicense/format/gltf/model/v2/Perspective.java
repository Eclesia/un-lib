
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Perspective extends GltfDocument {

    public static final Chars PROP_ASPECTRATIO = Chars.constant("aspectRatio");
    public static final Chars PROP_YFOV = Chars.constant("yfov");
    public static final Chars PROP_ZFAR = Chars.constant("zfar");
    public static final Chars PROP_ZNEAR = Chars.constant("znear");

    public Double getAspectRatio() {
        return getNumber(PROP_ASPECTRATIO, null);
    }

    public void setAspectRatio(Double value) {
        setPropertyValue(PROP_ASPECTRATIO, value);
    }

    public Double getYfov() {
        return getNumber(PROP_YFOV, null);
    }

    public void setYfov(Double value) {
        setPropertyValue(PROP_YFOV, value);
    }

    public Double getZfar() {
        return getNumber(PROP_ZFAR, null);
    }

    public void setZfar(Double value) {
        setPropertyValue(PROP_ZFAR, value);
    }

    public Double getZnear() {
        return getNumber(PROP_ZNEAR, null);
    }

    public void setZnear(Double value) {
        setPropertyValue(PROP_ZNEAR, value);
    }

}
