
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class AnimationSampler extends GltfDocument {

    public static final Chars PROP_INPUT = Chars.constant("input");
    public static final Chars PROP_INTERPOLATION = Chars.constant("interpolation");
    public static final Chars PROP_OUTPUT = Chars.constant("output");

    public Integer getInput() {
        return getInteger(PROP_INPUT, null);
    }

    public void setInput(Integer value) {
        setPropertyValue(PROP_INPUT, value);
    }

    public Chars getInterpolation() {
        return getChars(PROP_INTERPOLATION, Gltf2Constants.INTERPOLATION_LINEAR);
    }

    public void setInterpolation(Chars value) {
        setPropertyValue(PROP_INTERPOLATION, value);
    }

    public Integer getOutput() {
        return getInteger(PROP_OUTPUT, null);
    }

    public void setOutput(Integer value) {
        setPropertyValue(PROP_OUTPUT, value);
    }

}
