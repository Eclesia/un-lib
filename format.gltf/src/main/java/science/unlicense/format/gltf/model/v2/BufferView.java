
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class BufferView extends GltfDocument {

    public static final Chars PROP_BUFFER = Chars.constant("buffer");
    public static final Chars PROP_BYTEOFFSET = Chars.constant("byteOffset");
    public static final Chars PROP_BYTELENGTH = Chars.constant("byteLength");
    public static final Chars PROP_BYTESTRIDE = Chars.constant("byteStride");
    public static final Chars PROP_TARGET = Chars.constant("target");
    public static final Chars PROP_NAME = Chars.constant("name");

    public Integer getBuffer() {
        return getInteger(PROP_BUFFER, null);
    }

    public void setBuffer(Integer value) {
        setPropertyValue(PROP_BUFFER, value);
    }

    public Integer getByteOffset() {
        return getInteger(PROP_BYTEOFFSET, 0);
    }

    public void setByteOffset(Integer value) {
        setPropertyValue(PROP_BYTEOFFSET, value);
    }
    public Integer getByteLength() {
        return getInteger(PROP_BYTELENGTH, null);
    }

    public void setByteLength(Integer value) {
        setPropertyValue(PROP_BYTELENGTH, value);
    }

    public Integer getByteStride() {
        return getInteger(PROP_BYTESTRIDE, null);
    }

    public void setByteStride(Integer value) {
        setPropertyValue(PROP_BYTESTRIDE, value);
    }
    public Integer getTarget() {
        return getInteger(PROP_TARGET, null);
    }

    public void setTarget(Integer value) {
        setPropertyValue(PROP_TARGET, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }
}
