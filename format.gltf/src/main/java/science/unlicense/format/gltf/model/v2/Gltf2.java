
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.functional.BinaryOutFunction;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.format.gltf.model.Gltf;

/**
 *
 * @author Johann Sorel
 */
public class Gltf2 extends Gltf {

    private static final Chars ASSET = Chars.constant("asset");
    private static final Chars ACCESSORS = Chars.constant("accessors");
    private static final Chars BUFFERS = Chars.constant("buffers");
    private static final Chars BUFFERVIEWS = Chars.constant("bufferViews");
    private static final Chars NODES = Chars.constant("nodes");
    private static final Chars SCENES = Chars.constant("scenes");
    private static final Chars MESHES = Chars.constant("meshes");
    private static final Chars PRIMITIVES = Chars.constant("primitives");
    private static final Chars SKINS = Chars.constant("skins");
    private static final Chars TEXTURES = Chars.constant("textures");
    private static final Chars IMAGES = Chars.constant("images");
    private static final Chars SAMPLERS = Chars.constant("samplers");
    private static final Chars MATERIALS = Chars.constant("materials");
    private static final Chars PBRMETALLICROUGHNESS = Chars.constant("pbrMetallicRoughness");
    private static final Chars CAMERAS = Chars.constant("cameras");
    private static final Chars ANIMATIONS = Chars.constant("animations");
    private static final Chars CHANNELS = Chars.constant("channels");
    private static final Chars INDICES = Chars.constant("indices");
    private static final Chars BASECOLORTEXTURE = Chars.constant("baseColorTexture");

    public static final BinaryOutFunction BUILDER = new BinaryOutFunction() {
            @Override
            public Object perform(Object parent, Object nam) {
                Chars name = (Chars) nam;
                if (name == null) {
                    return new Gltf2();
                }
                if      (name.equals(ASSET,     true, true)) return new Asset();
                else if (name.equals(ACCESSORS, true, true)) return new Accessor();
                else if (name.equals(BUFFERS,       true, true)) return new Buffer();
                else if (name.equals(BUFFERVIEWS, true, true)) return new BufferView();
                else if (name.equals(NODES, true, true)) return new Node();
                else if (name.equals(SCENES, true, true)) return new Scene();
                else if (name.equals(MESHES, true, true)) return new Mesh();
                else if (name.equals(PRIMITIVES, true, true)) return new Primitive();
                else if (name.equals(SKINS, true, true)) return new Skin();
                else if (name.equals(TEXTURES, true, true)) return new Texture();
                else if (name.equals(IMAGES, true, true)) return new Image();
                else if (name.equals(SAMPLERS, true, true)) return new Sampler();
                else if (name.equals(MATERIALS, true, true)) return new Material();
                else if (name.equals(PBRMETALLICROUGHNESS, true, true)) return new PbrMetallicRoughness();
                else if (name.equals(CAMERAS, true, true)) return new Camera();
                else if (name.equals(ANIMATIONS, true, true)) return new Animation();
                else if (name.equals(CHANNELS, true, true)) return new Channel();
                else if (name.equals(INDICES, true, true)) return new Indices();
                else if (name.equals(BASECOLORTEXTURE, true, true)) return new BaseTextureInfo();

                return new DefaultDocument(true);
            }
        };

    public static final Chars PROP_EXTENSIONSUSED = Chars.constant("extensionsUsed");
    public static final Chars PROP_EXTENSIONSREQUIRED = Chars.constant("extensionsRequired");
    public static final Chars PROP_ACCESSORS = Chars.constant("accessors");
    public static final Chars PROP_ANIMATIONS = Chars.constant("animations");
    public static final Chars PROP_ASSET = Chars.constant("asset");
    public static final Chars PROP_BUFFERS = Chars.constant("buffers");
    public static final Chars PROP_BUFFERVIEWS = Chars.constant("bufferViews");
    public static final Chars PROP_CAMERAS = Chars.constant("cameras");
    public static final Chars PROP_IMAGES = Chars.constant("images");
    public static final Chars PROP_MATERIALS = Chars.constant("materials");
    public static final Chars PROP_MESHES = Chars.constant("meshes");
    public static final Chars PROP_NODES = Chars.constant("nodes");
    public static final Chars PROP_SAMPLERS = Chars.constant("samplers");
    public static final Chars PROP_SCENE = Chars.constant("scene");
    public static final Chars PROP_SCENES = Chars.constant("scenes");
    public static final Chars PROP_SKINS = Chars.constant("skins");
    public static final Chars PROP_TEXTURES = Chars.constant("textures");

    public Chars[] getExtensionsUsed() {
        return getCharsArray(PROP_EXTENSIONSUSED);
    }

    public Chars[] getExtensionsRequired() {
        return getCharsArray(PROP_EXTENSIONSREQUIRED);
    }

    public Accessor[] getAccessors() {
        return (Accessor[]) getDocuments(PROP_ACCESSORS, Accessor.class);
    }

    public Animation[] getAnimations() {
        return (Animation[]) getDocuments(PROP_ANIMATIONS, Animation.class);
    }

    public Asset getAsset() {
        return (Asset) getDocument(PROP_ASSET, Asset.class);
    }

    public Buffer[] getBuffers() {
        return (Buffer[]) getDocuments(PROP_BUFFERS, Buffer.class);
    }

    public BufferView[] getBufferViews() {
        return (BufferView[]) getDocuments(PROP_BUFFERVIEWS, BufferView.class);
    }

    public Camera[] getCameras() {
        return (Camera[]) getDocuments(PROP_CAMERAS, Camera.class);
    }

    public Image[] getImages() {
        return (Image[]) getDocuments(PROP_IMAGES, Image.class);
    }

    public Mesh[] getMeshes() {
        return (Mesh[]) getDocuments(PROP_MESHES, Mesh.class);
    }

    public Node[] getNodes() {
        return (Node[]) getDocuments(PROP_NODES, Node.class);
    }

    public Sampler[] getSamplers() {
        return (Sampler[]) getDocuments(PROP_SAMPLERS, Sampler.class);
    }

    public Integer getScene() {
        return getInteger(PROP_SCENE, null);
    }

    public Scene[] getScenes() {
        return (Scene[]) getDocuments(PROP_SCENES, Scene.class);
    }

    public Skin[] getSkins() {
        return (Skin[]) getDocuments(PROP_SKINS, Skin.class);
    }

    public Texture[] getTextures() {
        return (Texture[]) getDocuments(PROP_TEXTURES, Texture.class);
    }

    public Material[] getMaterials() {
        return (Material[]) getDocuments(PROP_MATERIALS, Material.class);
    }
}
