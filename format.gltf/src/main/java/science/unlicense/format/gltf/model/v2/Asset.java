
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Asset extends GltfDocument {

    public static final Chars PROP_COPYRIGHT = Chars.constant("copyright");
    public static final Chars PROP_GENERATOR = Chars.constant("generator");
    public static final Chars PROP_VERSION = Chars.constant("version");
    public static final Chars PROP_MINVERSION = Chars.constant("minVersion");

    public Chars getCopyright() {
        return getChars(PROP_COPYRIGHT, null);
    }

    public void setCopyright(Chars value) {
        setPropertyValue(PROP_COPYRIGHT, value);
    }

    public Chars getGenerator() {
        return getChars(PROP_GENERATOR, null);
    }

    public void setGenerator(Chars value) {
        setPropertyValue(PROP_GENERATOR, value);
    }

    public Chars getVersion() {
        return getChars(PROP_VERSION, null);
    }

    public void setVersion(Chars value) {
        setPropertyValue(PROP_VERSION, value);
    }

    public Chars getMinVersion() {
        return getChars(PROP_MINVERSION, null);
    }

    public void setMinVersion(Chars value) {
        setPropertyValue(PROP_MINVERSION, value);
    }
}
