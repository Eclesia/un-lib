
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Orthographic extends GltfDocument {

    public static final Chars PROP_XMAG = Chars.constant("xmag");
    public static final Chars PROP_YMAG = Chars.constant("ymag");
    public static final Chars PROP_ZFAR = Chars.constant("zfar");
    public static final Chars PROP_ZNEAR = Chars.constant("znear");


    public Double getXmag() {
        return getNumber(PROP_XMAG, null);
    }

    public void setXmag(Double value) {
        setPropertyValue(PROP_XMAG, value);
    }

    public Double getYmag() {
        return getNumber(PROP_YMAG, null);
    }

    public void setTmag(Double value) {
        setPropertyValue(PROP_YMAG, value);
    }

    public Double getZfar() {
        return getNumber(PROP_ZFAR, null);
    }

    public void setZfar(Double value) {
        setPropertyValue(PROP_ZFAR, value);
    }

    public Double getZnear() {
        return getNumber(PROP_ZNEAR, null);
    }

    public void setZnear(Double value) {
        setPropertyValue(PROP_ZNEAR, value);
    }
}
