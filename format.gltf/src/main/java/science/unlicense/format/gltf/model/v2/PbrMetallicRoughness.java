
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.Vector;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class PbrMetallicRoughness extends GltfDocument {

    public static final Chars PROP_BASECOLORFACTOR = Chars.constant("baseColorFactor");
    public static final Chars PROP_BASECOLORTEXTURE = Chars.constant("baseColorTexture");
    public static final Chars PROP_METALLICFACTOR = Chars.constant("metallicFactor");
    public static final Chars PROP_ROUGHNESSFACTOR = Chars.constant("roughnessFactor");
    public static final Chars PROP_METALLICROUGHNESSTEXTURE = Chars.constant("metallicRoughnessTexture");


    public Vector getBaseColorFactor() {
        return getVector(PROP_BASECOLORFACTOR, new Vector4f64(1, 1, 1, 1));
    }

    public void setBaseColorFactor(Tuple value) {
        setTuple(PROP_BASECOLORFACTOR, value);
    }

    public BaseTextureInfo getBaseColorTexture() {
        return (BaseTextureInfo) getDocument(PROP_BASECOLORTEXTURE, BaseTextureInfo.class);
    }

    public void setBaseColorTexture(BaseTextureInfo value) {
        setPropertyValue(PROP_BASECOLORTEXTURE, value);
    }

    public Double getMetallicFactor() {
        return getNumber(PROP_METALLICFACTOR, 1.0);
    }

    public void setMetallicFactor(Double value) {
        setPropertyValue(PROP_METALLICFACTOR, value);
    }

    public Double getRoughnessFactor() {
        return getNumber(PROP_ROUGHNESSFACTOR, 1.0);
    }

    public void setRoughnessFactor(Double value) {
        setPropertyValue(PROP_ROUGHNESSFACTOR, value);
    }

    public Object getMetallicRoughnessTexture() {
        return getPropertyValue(PROP_METALLICROUGHNESSTEXTURE);
    }

    public void setMetallicRoughnessTexture(Object value) {
        setPropertyValue(PROP_METALLICROUGHNESSTEXTURE, value);
    }
}
