
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.Vector;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Scene extends GltfDocument {

    public static final Chars PROP_NODES = Chars.constant("nodes");
    public static final Chars PROP_NAME = Chars.constant("name");

    public Vector getNodes() {
        return (Vector) getVector(PROP_NODES, null);
    }

    public void setNodes(Tuple value) {
        setTuple(PROP_NODES, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }
}
