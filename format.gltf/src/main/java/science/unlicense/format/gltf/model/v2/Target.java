
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Target extends GltfDocument {

    public static final Chars PROP_NODE = Chars.constant("node");
    public static final Chars PROP_PATH = Chars.constant("path");

    public Integer getNode() {
        return getInteger(PROP_NODE, null);
    }

    public void setNode(Integer value) {
        setPropertyValue(PROP_NODE, value);
    }

    public Chars getPath() {
        return getChars(PROP_PATH, null);
    }

    public void setPath(Chars value) {
        setPropertyValue(PROP_PATH, value);
    }

}
