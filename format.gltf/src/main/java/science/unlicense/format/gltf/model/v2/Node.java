
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.Vector;
import science.unlicense.math.impl.Vector3f32;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Node extends GltfDocument {

    public static final Chars PROP_CAMERA = Chars.constant("camera");
    public static final Chars PROP_CHILDREN = Chars.constant("children");
    public static final Chars PROP_SKIN = Chars.constant("skin");
    public static final Chars PROP_MATRIX = Chars.constant("matrix");
    public static final Chars PROP_MESH = Chars.constant("mesh");
    public static final Chars PROP_ROTATION = Chars.constant("rotation");
    public static final Chars PROP_SCALE = Chars.constant("scale");
    public static final Chars PROP_TRANSLATION = Chars.constant("translation");
    public static final Chars PROP_WEIGHTS = Chars.constant("weights");
    public static final Chars PROP_NAME = Chars.constant("name");

    public Integer getCamera() {
        return getInteger(PROP_CAMERA, null);
    }

    public void setCamera(Integer value) {
        setPropertyValue(PROP_CAMERA, value);
    }

    public Vector getChildren() {
        return getVector(PROP_CHILDREN, null);
    }

    public void setChildren(Tuple value) {
        setTuple(PROP_CHILDREN, value);
    }

    public Integer getSkin() {
        return getInteger(PROP_SKIN, null);
    }

    public void setSkin(Integer value) {
        setPropertyValue(PROP_SKIN, value);
    }

    public Vector getMatrix() {
        return getVector(PROP_MATRIX, null);
    }

    public void setMatrix(Tuple value) {
        setTuple(PROP_MATRIX, value);
    }

    public Integer getMesh() {
        return getInteger(PROP_MESH, null);
    }

    public void setMesh(Integer value) {
        setPropertyValue(PROP_MESH, value);
    }

    public Vector getRotation() {
        return getVector(PROP_ROTATION, new Vector4f64(0, 0, 0, 1));
    }

    public void setRotation(Tuple value) {
        setTuple(PROP_ROTATION, value);
    }

    public Vector getScale() {
        return getVector(PROP_SCALE, new Vector3f32(1, 1, 1));
    }

    public void setScale(Tuple value) {
        setTuple(PROP_SCALE, value);
    }

    public Vector getTranslation() {
        return getVector(PROP_TRANSLATION, new Vector3f32(0, 0, 0));
    }

    public void setTranslation(Tuple value) {
        setTuple(PROP_TRANSLATION, value);
    }

    public Vector getWeights() {
        return getVector(PROP_WEIGHTS, null);
    }

    public void setWeights(Tuple value) {
        setTuple(PROP_WEIGHTS, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }
}
