
package science.unlicense.format.gltf.model;

import java.lang.reflect.Array;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.Vector;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class GltfDocument extends DefaultDocument{

    public static final Chars PROP_EXTENSIONS = Chars.constant("extensions");
    public static final Chars PROP_EXTRAS = Chars.constant("extras");

    public void setTuple(Chars name, Tuple value) {
        if (value == null) {
            setPropertyValue(name, null);
        } else {
            final Object[] arr = new Object[value.getSampleCount()];
            for (int i=0;i<arr.length;i++) arr[i] = value.get(i);
            setPropertyValue(name, arr);
        }
    }

    public Integer getInteger(Chars name, Integer def) {
        return (Integer) getFieldValue(name, false, def, Integer.class);
    }

    public Boolean getBoolean(Chars name, Boolean def) {
        return (Boolean) getFieldValue(name, false, def, Boolean.class);
    }

    public Double getNumber(Chars name, Double def) {
        Object value = getPropertyValue(name);
        if (value == null) {
            value = def;
        }
        if (value instanceof Number && !(value instanceof Double)) {
            value = ((Number) value).doubleValue();
        }
        return (Double) value;
    }

    public Chars getChars(Chars name, Chars def) {
        return (Chars) getFieldValue(name, false, def, Chars.class);
    }
    public Chars[] getCharsArray(Chars name) {
        final Object candidate = getPropertyValue(name);
        if (candidate == null) {
            return new Chars[0];
        } else if (candidate.getClass().isArray()) {
            final int size = Array.getLength(candidate);
            final Chars[] vals = new Chars[size];
            for (int i=0;i<vals.length;i++) vals[i] = (Chars) Array.get(candidate, i);
            return vals;
        } else {
            return new Chars[]{(Chars) candidate};
        }
    }

    public Document getDocument(Chars name, Class docClass) {
        return (Document) getFieldValue(name, false, null, docClass);
    }

    public Object[] getDocuments(Chars name, Class docClass) {
        final Object candidate = getPropertyValue(name);
        if (candidate == null) {
            return (Object[]) Array.newInstance(docClass, 0);
        } else if (candidate.getClass().isArray()) {
            final int size = Array.getLength(candidate);
            final Object[] vals = (Object[]) Array.newInstance(docClass, size);
            for (int i=0;i<vals.length;i++) vals[i] = Array.get(candidate, i);
            return vals;
        } else {
            Object[] vals = (Object[]) Array.newInstance(docClass, 1);
            vals[0] = candidate;
            return vals;
        }
    }

    public VectorRW getVector(Chars name, Vector def) {
        Object candidate = getPropertyValue(name);

        if (candidate != null && candidate.getClass().isArray()) {
            final Sequence s = new ArraySequence();
            for (int i=0,n=Array.getLength(candidate); i<n; i++) {
                s.add(Array.get(candidate, i));
            }
            candidate = s;
        }

        if (candidate instanceof Collection) {
            final Collection c = (Collection) candidate;
            final VectorRW v = VectorNf64.createDouble(c.getSize());
            final Iterator ite = c.createIterator();
            int i=0;
            while (ite.hasNext()) {
                Object obj = ite.next();
                double d;
                if (obj instanceof Number) {
                    d = ((Number) obj).doubleValue();
                } else {
                    throw new RuntimeException("Unexpected type "+obj.getClass());
                }

                v.set(i, d);
                i++;
            }
            return v;
        } else if (candidate == null) {
            return def == null ? null : (VectorRW) def.copy();
        } else {
            throw new RuntimeException("Field value "+name+" is not expected type, found "+candidate.getClass().getSimpleName()+" expected an array.");
        }
    }

    public final Object getFieldValue(Chars name, boolean required, Object def, Class expectedType) {
        Object value = getPropertyValue(name);
        if (required) {
            throw new RuntimeException("Missing field value "+name);
        }
        if (value == null) {
            value = def;
        }

        if (value != null && !expectedType.isInstance(value)) {
            throw new RuntimeException("Field value "+name+" is not expected type, found "+value.getClass().getSimpleName()+" expected "+expectedType.getSimpleName());
        }

        return value;
    }
}
