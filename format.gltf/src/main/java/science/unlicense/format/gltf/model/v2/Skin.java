
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.Vector;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Skin extends GltfDocument {

    public static final Chars PROP_INVERSEBINDMATRICES = Chars.constant("inverseBindMatrices");
    public static final Chars PROP_SKELETON = Chars.constant("skeleton");
    public static final Chars PROP_JOINTS = Chars.constant("joints");
    public static final Chars PROP_NAME = Chars.constant("name");

    public Integer getInverseBindMatrices() {
        return getInteger(PROP_INVERSEBINDMATRICES, null);
    }

    public void setInverseBindMatrices(Integer value) {
        setPropertyValue(PROP_INVERSEBINDMATRICES, value);
    }

    public Integer getSkeleton() {
        return getInteger(PROP_SKELETON, null);
    }

    public void setSkeleton(Integer value) {
        setPropertyValue(PROP_SKELETON, value);
    }

    public Vector getJoints() {
        return (Vector) getVector(PROP_JOINTS, null);
    }

    public void setJoints(Tuple value) {
        setTuple(PROP_JOINTS, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }

}
