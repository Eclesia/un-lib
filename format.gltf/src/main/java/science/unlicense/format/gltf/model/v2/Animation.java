
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Animation extends GltfDocument {

    public static final Chars PROP_CHANNELS = Chars.constant("channels");
    public static final Chars PROP_SAMPLERS = Chars.constant("samplers");
    public static final Chars PROP_NAME = Chars.constant("name");

    public Channel[] getChannels() {
        return (Channel[]) getDocuments(PROP_CHANNELS, Channel.class);
    }

    public void setChannels(Channel[] value) {
        setPropertyValue(PROP_CHANNELS, value);
    }

    public AnimationSampler[] getSamplers() {
        return (AnimationSampler[]) getDocuments(PROP_SAMPLERS, AnimationSampler.class);
    }

    public void setSamplers(AnimationSampler[] value) {
        setPropertyValue(PROP_SAMPLERS, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }
}
