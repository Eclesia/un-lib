
package science.unlicense.format.gltf;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.buffer.DataCursor;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.DefaultInt8Buffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.functional.BinaryOutFunction;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Int16;
import science.unlicense.common.api.number.Int32;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.FormatEncodingException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.gltf.model.Gltf;
import science.unlicense.format.gltf.model.v1.Gltf1;
import science.unlicense.format.gltf.model.v2.Accessor;
import science.unlicense.format.gltf.model.v2.Animation;
import science.unlicense.format.gltf.model.v2.Asset;
import science.unlicense.format.gltf.model.v2.BaseTextureInfo;
import science.unlicense.format.gltf.model.v2.Buffer;
import science.unlicense.format.gltf.model.v2.BufferView;
import science.unlicense.format.gltf.model.v2.Camera;
import science.unlicense.format.gltf.model.v2.Gltf2;
import science.unlicense.format.gltf.model.v2.Gltf2Constants;
import science.unlicense.format.gltf.model.v2.Image;
import science.unlicense.format.gltf.model.v2.Material;
import science.unlicense.format.gltf.model.v2.Mesh;
import science.unlicense.format.gltf.model.v2.Node;
import science.unlicense.format.gltf.model.v2.Orthographic;
import science.unlicense.format.gltf.model.v2.PbrMetallicRoughness;
import science.unlicense.format.gltf.model.v2.Perspective;
import science.unlicense.format.gltf.model.v2.Primitive;
import science.unlicense.format.gltf.model.v2.Sampler;
import science.unlicense.format.gltf.model.v2.Scene;
import science.unlicense.format.gltf.model.v2.Skin;
import science.unlicense.format.gltf.model.v2.Sparse;
import science.unlicense.format.gltf.model.v2.Texture;
import science.unlicense.format.json.JSONUtilities;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.api.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.math.api.Vector;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.SimplePBR;

/**
 *
 * @author Johann Sorel
 */
public class GltfStore extends AbstractModel3DStore {

    public GltfStore(Object input) {
        super(GLTFFormat.INSTANCE, input);
    }

    @Override
    public Collection getElements() throws StoreException {

        Gltf gltf;
        try {
             gltf = readGltf();
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        final ArraySequence col = new ArraySequence();
        if (gltf instanceof Gltf1) {
            throw new UnsupportedOperationException("GLTF 1 not supported yet.");
        } else if (gltf instanceof Gltf2) {
            try {
                col.add(build((Gltf2) gltf));
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        } else {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        return col;
    }

    public Gltf readGltf() throws IOException {
        final BinaryOutFunction docBuilder = detectVersion();
        return (Gltf) JSONUtilities.readAsDocument(source, docBuilder);
    }

    private BinaryOutFunction detectVersion() throws IOException {
        final Dictionary dico = JSONUtilities.readAsDictionary(source);
        final Dictionary asset = (Dictionary) dico.getValue(new Chars("asset"));
        if (asset == null) throw new FormatEncodingException(this, "Missing asset property");
        final Chars version = CObjects.toChars(asset.getValue(new Chars("version"))).trim();
        if (version.isEmpty()) {
            throw new FormatEncodingException(this, "Undefined asset version");
        } else if (version.getUnicode(0) == '1') {
            return Gltf1.BUILDER;
        } else if (version.getUnicode(0) == '2') {
            return Gltf2.BUILDER;
        } else {
            throw new FormatEncodingException(this, "Unsupported gltf version : "+version);
        }
    }

    private SceneNode build(Gltf2 gltf) throws IOException {
        final State state = new State();

        final Asset asset = gltf.getAsset();
        final Chars[] extensionsRequired = gltf.getExtensionsRequired();
        final Chars[] extensionsUsed = gltf.getExtensionsUsed();

        final Buffer[] buffers = gltf.getBuffers();
        final BufferView[] bufferViews = gltf.getBufferViews();
        final Accessor[] accessors = gltf.getAccessors();
        rebuildBuffers( ((Path) getInput()).getParent(), gltf, state);

        final Texture[] textures = gltf.getTextures();
        final Image[] images = gltf.getImages();
        final Sampler[] samplers = gltf.getSamplers();
        rebuildMaterials(((Path) getInput()).getParent(), gltf, state);

        final Camera[] cameras = gltf.getCameras();
        final Mesh[] meshes = gltf.getMeshes();
        final Node[] nodes = gltf.getNodes();
        final Scene[] scenes = gltf.getScenes();
        rebuildCameras(gltf, state);
        rebuildMeshes(gltf, state);
        rebuildNodes(gltf, state);
        rebuildScenes(gltf, state);

        final Animation[] animations = gltf.getAnimations();
        final Skin[] skins = gltf.getSkins();

        final Integer sceneIdx = gltf.getScene();
        final SceneNode scene = state.scenes[sceneIdx];

        return scene;
    }

    private void rebuildBuffers(Path folder, Gltf2 gltf, State state) throws IOException {
        final Buffer[] buffers = gltf.getBuffers();
        final BufferView[] bufferViews = gltf.getBufferViews();
        final Accessor[] accessors = gltf.getAccessors();

        state.buffers = new science.unlicense.common.api.buffer.Buffer[buffers.length];
        for (int i=0; i<buffers.length; i++) {
            Integer byteLength = buffers[i].getByteLength();
            Chars name = buffers[i].getName();
            Chars uri = buffers[i].getUri();

            final Path path;
            if (Paths.isRelative(uri)) {
                path = folder.resolve(uri);
            } else {
                path = Paths.resolve(uri);
            }

            final ByteInputStream bs = path.createInputStream();
            try {
                byte[] datas = IOUtilities.readAll(bs);
                state.buffers[i] = DefaultBufferFactory.wrap(datas);
            } finally {
                bs.dispose();
            }
        }

        state.bufferViews = new science.unlicense.common.api.buffer.Buffer[bufferViews.length];
        for (int i=0; i<bufferViews.length; i++) {
            final Integer buffer = bufferViews[i].getBuffer();
            final Integer byteLength = bufferViews[i].getByteLength();
            final Integer byteOffset = bufferViews[i].getByteOffset();
            final Integer byteStride = bufferViews[i].getByteStride();
            final Integer target = bufferViews[i].getTarget();
            final Chars name = bufferViews[i].getName();

            final byte[] array = new byte[byteLength];
            state.buffers[buffer].readInt8(array, byteOffset);

            state.bufferViews[i] = new DefaultInt8Buffer(array);
        }

        state.accessors = new VBO[accessors.length];
        for (int i=0; i<accessors.length; i++) {
            final Chars attributeType = accessors[i].getAttributeType();
            final Integer bufferView = accessors[i].getBufferView();
            final Integer byteOffset = accessors[i].getByteOffset();
            final Integer componentType = accessors[i].getComponentType();
            final Integer count = accessors[i].getCount();
            final Vector max = accessors[i].getMax();
            final Vector min = accessors[i].getMin();
            final Chars name = accessors[i].getName();
            final Boolean normalized = accessors[i].getNormalized();
            final Sparse sparse = accessors[i].getSparse();

            if (sparse != null) {
                throw new IOException(this, "Sparse accessor not supported");
            }

            final int nbComp;
            if (Gltf2Constants.TYPE_SCALAR.equals(attributeType)) {
                nbComp = 1;
            } else if (Gltf2Constants.TYPE_VEC2.equals(attributeType)) {
                nbComp = 2;
            } else if (Gltf2Constants.TYPE_VEC3.equals(attributeType)) {
                nbComp = 3;
            } else if (Gltf2Constants.TYPE_VEC4.equals(attributeType)) {
                nbComp = 4;
            } else if (Gltf2Constants.TYPE_MAT2.equals(attributeType)) {
                nbComp = 4;
            } else if (Gltf2Constants.TYPE_MAT3.equals(attributeType)) {
                nbComp = 9;
            } else if (Gltf2Constants.TYPE_MAT4.equals(attributeType)) {
                nbComp = 16;
            } else {
                throw new IOException(this, "Unsupported component type " +componentType);
            }

            if (GLC.GL_BYTE == componentType) {
                final byte[] array = new byte[nbComp*count];
                state.bufferViews[bufferView].readInt8(array, byteOffset);
                state.accessors[i] = new VBO(DefaultBufferFactory.wrap(array), nbComp);
            } else if (GLC.GL_UNSIGNED_BYTE == componentType) {
                final byte[] array = new byte[nbComp*count];
                state.bufferViews[bufferView].readInt8(array, byteOffset);
                state.accessors[i] = new VBO(DefaultBufferFactory.wrap(array), nbComp);
            } else if (GLC.GL_SHORT == componentType || GLC.GL_UNSIGNED_SHORT == componentType) {
                final short[] array = new short[nbComp*count];
                DataCursor cursor = state.bufferViews[bufferView].dataCursor(Int16.TYPE, Endianness.LITTLE_ENDIAN);
                cursor.skipBytes(byteOffset);
                cursor.readShort(array);
                state.accessors[i] = new VBO(DefaultBufferFactory.wrap(array), nbComp);
            } else if (GLC.GL_INT == componentType || GLC.GL_UNSIGNED_INT == componentType) {
                final int[] array = new int[nbComp*count];
                DataCursor cursor = state.bufferViews[bufferView].dataCursor(Int32.TYPE, Endianness.LITTLE_ENDIAN);
                cursor.skipBytes(byteOffset);
                cursor.readInt(array);
                state.accessors[i] = new VBO(DefaultBufferFactory.wrap(array), nbComp);
            } else if (GLC.GL_FLOAT == componentType) {
                final float[] array = new float[nbComp*count];
                DataCursor cursor = state.bufferViews[bufferView].dataCursor(Float32.TYPE, Endianness.LITTLE_ENDIAN);
                cursor.skipBytes(byteOffset);
                cursor.readFloat(array);
                state.accessors[i] = new VBO(DefaultBufferFactory.wrap(array), nbComp);
            } else if (GLC.GL_DOUBLE == componentType) {
                final double[] array = new double[nbComp*count];
                state.bufferViews[bufferView].readFloat64(array, byteOffset);
                state.accessors[i] = new VBO(DefaultBufferFactory.wrap(array), nbComp);
            } else {
                throw new IOException(this, "Unsupported component type " +componentType);
            }

        }

    }

    private void rebuildCameras(Gltf2 gltf, State state) {
        final Camera[] cameras = gltf.getCameras();

        state.cameras = new MonoCamera[cameras.length];
        for (int i=0; i<cameras.length; i++) {
            state.cameras[i] = new MonoCamera();

            final Chars cameraType = cameras[i].getCameraType();
            final Chars name = cameras[i].getName();
            final Orthographic orthographic = cameras[i].getOrthographic();
            final Perspective perspective = cameras[i].getPerspective();
            if (orthographic != null) {
                state.cameras[i].setCameraType(MonoCamera.TYPE_ORTHO);
                state.cameras[i].setFarPlane(orthographic.getZfar());
                state.cameras[i].setNearPlane(orthographic.getZnear());
//                state.cameras[i].setFarPlane(orthographic.getZfar());
//                state.cameras[i].setFarPlane(orthographic.getZfar());
            } else if (perspective != null) {

            }

            state.cameras[i].setId(name);

        }
    }

    private void rebuildMaterials(Path folder, Gltf2 gltf, State state) throws IOException {

        final Image[] images = gltf.getImages();
        state.images = new science.unlicense.image.api.Image[images.length];
        for (int i=0; i<images.length; i++) {
            final Chars uri = images[i].getUri();
            final Integer bufferView = images[i].getBufferView();

            if (bufferView != null) {
                state.images[i] = Images.read(state.bufferViews[bufferView].toByteArray());
            } else {
                final Path path;
                if (Paths.isRelative(uri)) {
                    path = folder.resolve(uri);
                } else {
                    path = Paths.resolve(uri);
                }
                state.images[i] = Images.read(path);
            }
        }


        final Texture[] textures = gltf.getTextures();
        state.textures = new Texture2D[textures.length];
        for (int i=0; i<textures.length; i++) {
            final Chars name = textures[i].getName();
            final Integer samplerIdx = textures[i].getSampler();
            final Integer sourceIdx = textures[i].getSource();

            Integer magFilter = null;
            Integer minFilter = null;
            Integer wrapS = null;
            Integer wrapT = null;
            if (samplerIdx != null && samplerIdx >=0) {
                final Sampler sampler = gltf.getSamplers()[samplerIdx];
                magFilter = sampler.getMagFilter();
                minFilter = sampler.getMinFilter();
                wrapS = sampler.getWrapS();
                wrapT = sampler.getWrapT();
            }

            final Texture2D tex = new Texture2D(state.images[sourceIdx]);
            if (magFilter != null) tex.getInfo().setParameter(science.unlicense.gpu.impl.opengl.GLC.Texture.Parameters.MAG_FILTER.KEY, magFilter);
            if (minFilter != null) tex.getInfo().setParameter(science.unlicense.gpu.impl.opengl.GLC.Texture.Parameters.MIN_FILTER.KEY, minFilter);
            if (wrapS != null) tex.getInfo().setParameter(science.unlicense.gpu.impl.opengl.GLC.Texture.Parameters.WRAP_S.KEY, wrapS);
            if (wrapT != null) tex.getInfo().setParameter(science.unlicense.gpu.impl.opengl.GLC.Texture.Parameters.WRAP_T.KEY, wrapT);

            state.textures[i] = tex;
        }


        final Material[] materials = gltf.getMaterials();
        state.materials = new science.unlicense.model3d.impl.material.Material[materials.length];
        for (int i=0; i<materials.length; i++) {

//            Double alphaCutoff = materials[i].getAlphaCutoff();
//            Chars alphaMode = materials[i].getAlphaMode();
//            Boolean doubleSided = materials[i].getDoubleSided();
//            Vector emissiveFactor = materials[i].getEmissiveFactor();
//            Document emissiveTexture = materials[i].getEmissiveTexture();
//            Document normalTexture = materials[i].getNormalTexture();
//            Document occlusionTexture = materials[i].getOcclusionTexture();
//            Chars name = materials[i].getName();

            PbrMetallicRoughness pbrMetallicRoughness = materials[i].getPbrMetallicRoughness();
            Vector baseColorFactor = pbrMetallicRoughness.getBaseColorFactor();
            BaseTextureInfo baseColorTexture = pbrMetallicRoughness.getBaseColorTexture();
            Double metallicFactor = pbrMetallicRoughness.getMetallicFactor();
            Object metallicRoughnessTexture = pbrMetallicRoughness.getMetallicRoughnessTexture();
            Double roughnessFactor = pbrMetallicRoughness.getRoughnessFactor();

            final SimplePBR.Material mat = SimplePBR.newMaterial();
            mat.setBaseColorFactor(new ColorRGB(baseColorFactor, ColorSystem.RGB_8BITS));

            if (baseColorTexture != null) {
                final Integer texIdx = baseColorTexture.getIndex();

                final Texture2D tex = state.textures[texIdx];
                mat.setBaseColorTexture(new TextureMapping(tex));
            }
            if (metallicFactor != null) {
                mat.setMetallicFactor(metallicFactor);
            }
            if (roughnessFactor != null) {
                mat.setRoughnessFactor(roughnessFactor);
            }

            state.materials[i] = mat;
        }


    }

    private void rebuildNodes(Gltf2 gltf, State state) {
        final Node[] nodes = gltf.getNodes();

        state.nodes = new SceneNode[nodes.length];
        for (int i=0;i<nodes.length;i++) {
            state.nodes[i] = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        }

        for (int i=0;i<nodes.length;i++) {

            //set transform
            final Vector matrix = nodes[i].getMatrix();
            final Vector rotation = nodes[i].getRotation();
            final Vector scale = nodes[i].getScale();
            final Vector translation = nodes[i].getTranslation();
            if (matrix != null) {
                final Matrix4x4 m = new Matrix4x4();
                m.set(0, 0, matrix.get(0));m.set(0, 1, matrix.get(4));m.set(0, 2, matrix.get( 8));m.set(0, 3, matrix.get(12));
                m.set(1, 0, matrix.get(1));m.set(1, 1, matrix.get(5));m.set(1, 2, matrix.get( 9));m.set(1, 3, matrix.get(13));
                m.set(2, 0, matrix.get(2));m.set(2, 1, matrix.get(6));m.set(2, 2, matrix.get(10));m.set(2, 3, matrix.get(14));
                m.set(3, 0, matrix.get(3));m.set(3, 1, matrix.get(7));m.set(3, 2, matrix.get(11));m.set(3, 3, matrix.get(15));
                state.nodes[i].getNodeTransform().set(m);
            } else {
                state.nodes[i].getNodeTransform().getRotation().set(new Quaternion(rotation).toMatrix3());
                state.nodes[i].getNodeTransform().getScale().set(scale);
                state.nodes[i].getNodeTransform().getTranslation().set(translation);
            }

            final Chars name = nodes[i].getName();
            state.nodes[i].setId(name);
            state.nodes[i].setTitle(name);

            //set children
            Vector children = nodes[i].getChildren();
            if (children != null) {
                final int[] indexes = children.toInt();
                for (int k=0;k<indexes.length;k++) {
                    state.nodes[i].getChildren().add(state.nodes[k]);
                }
            }

            //set defined object
            Integer camera = nodes[i].getCamera();
            Integer mesh = nodes[i].getMesh();https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#buffers-and-buffer-views
            if (camera != null) {
                state.nodes[i].getChildren().add(state.cameras[camera]);
            }
            if (mesh != null) {
                state.nodes[i].getChildren().add(state.meshes[mesh]);
            }



            Integer skin = nodes[i].getSkin();
            Vector weights = nodes[i].getWeights();
        }
    }

    private void rebuildScenes(Gltf2 gltf, State state) {
        final Scene[] scenes = gltf.getScenes();

        state.scenes = new SceneNode[scenes.length];
        for (int i=0;i<scenes.length;i++) {
            state.scenes[i] = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);

            //set children
            Vector children = scenes[i].getNodes();
            if (children != null) {
                final int[] indexes = children.toInt();
                for (int k=0;k<indexes.length;k++) {
                    state.scenes[i].getChildren().add(state.nodes[k]);
                }
            }
        }
    }

    private void rebuildMeshes(Gltf2 gltf, State state) {
        final Mesh[] meshes = gltf.getMeshes();
        state.meshes = new DefaultMotionModel[meshes.length];

        for (int i=0;i<meshes.length;i++) {
            final Mesh m = meshes[i];
            final Chars name = m.getName();
            final Primitive[] primitives = m.getPrimitives();
            final Vector weights = m.getWeights();

            final MotionModel mpm = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);
            state.meshes[i] = mpm;
            mpm.setId(name);

            for (int k=0;k<primitives.length;k++) {
                final DefaultModel mesh = new DefaultModel();
                final Document attributes = primitives[k].getAttributes();
                final Integer indices = primitives[k].getIndices();
                final Integer material = primitives[k].getMaterial();
                final Integer mode = primitives[k].getMode();
                final Document targets = primitives[k].getTargets();

                final Integer posIdx = (Integer) attributes.getPropertyValue(Gltf2Constants.ATT_POSITION);
                final Integer uvIdx = (Integer) attributes.getPropertyValue(Gltf2Constants.ATT_TEXCOORD_0);

                final DefaultMesh geom = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
                geom.setPositions(state.accessors[posIdx]);
                geom.getAttributes().add(science.unlicense.geometry.impl.Mesh.ATT_TEXCOORD_0, state.accessors[uvIdx]);
                geom.setIndex(state.accessors[indices]);
                geom.setRanges(new IndexedRange[]{
                    new IndexedRange(mode, 0, 0, state.accessors[indices].getTupleCount())
                });

                final science.unlicense.model3d.impl.material.Material mat = state.materials[material];

                mesh.setShape(geom);
                mesh.getMaterials().add(mat);
                mesh.getTechniques().add(new SimplePBR());
                mpm.getChildren().add(mesh);
            }
        }
    }

    private static class State {

        private science.unlicense.common.api.buffer.Buffer[] buffers;
        private science.unlicense.common.api.buffer.Buffer[] bufferViews;
        private science.unlicense.image.api.Image[] images;
        private Texture2D[] textures;
        private science.unlicense.model3d.impl.material.Material[] materials;
        private VBO[] accessors;
        private SceneNode[] nodes;
        private SceneNode[] scenes;
        private MonoCamera[] cameras;
        private MotionModel[] meshes;

    }

}
