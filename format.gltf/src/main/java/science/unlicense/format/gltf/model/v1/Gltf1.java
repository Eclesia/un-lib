
package science.unlicense.format.gltf.model.v1;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.functional.BinaryOutFunction;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.format.gltf.model.Gltf;

/**
 *
 * @author Johann Sorel
 */
public class Gltf1 extends Gltf {

    public static final BinaryOutFunction BUILDER = new BinaryOutFunction() {
            @Override
            public Object perform(Object parent, Object nam) {
                Chars name = (Chars) nam;
                if (name == null) {
                    return new Gltf1();
                }
                return new DefaultDocument(true);
            }
        };
}
