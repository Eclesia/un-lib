
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Sampler extends GltfDocument {

    public static final Chars PROP_MAGFILTER = Chars.constant("magFilter");
    public static final Chars PROP_MINFILTER = Chars.constant("minFilter");
    public static final Chars PROP_WRAPS = Chars.constant("wrapS");
    public static final Chars PROP_WRAPT = Chars.constant("wrapT");
    public static final Chars PROP_NAME = Chars.constant("name");

    public Integer getMagFilter() {
        return getInteger(PROP_MAGFILTER, null);
    }

    public void setMagFilter(Integer value) {
        setPropertyValue(PROP_MAGFILTER, value);
    }

    public Integer getMinFilter() {
        return getInteger(PROP_MINFILTER, null);
    }

    public void setMinFilter(Integer value) {
        setPropertyValue(PROP_MINFILTER, value);
    }

    public Integer getWrapS() {
        return getInteger(PROP_WRAPS, null);
    }

    public void setWrapS(Integer value) {
        setPropertyValue(PROP_WRAPS, value);
    }

    public Integer getWrapT() {
        return getInteger(PROP_WRAPT, null);
    }

    public void setWrapT(Integer value) {
        setPropertyValue(PROP_WRAPT, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }

}
