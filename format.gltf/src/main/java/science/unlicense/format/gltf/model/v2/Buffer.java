
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Buffer extends GltfDocument {

    public static final Chars PROP_URI = Chars.constant("uri");
    public static final Chars PROP_BYTELENGTH = Chars.constant("byteLength");
    public static final Chars PROP_NAME = Chars.constant("name");


    public Chars getUri() {
        return getChars(PROP_URI, null);
    }

    public void setUri(Chars value) {
        setPropertyValue(PROP_URI, value);
    }

    public Integer getByteLength() {
        return getInteger(PROP_BYTELENGTH, null);
    }

    public void setByteLength(Integer value) {
        setPropertyValue(PROP_BYTELENGTH, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }

}
