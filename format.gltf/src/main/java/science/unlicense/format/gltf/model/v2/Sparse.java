
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Sparse extends GltfDocument {

    public static final Chars PROP_COUNT = Chars.constant("count");
    public static final Chars PROP_INDICES = Chars.constant("indices");
    public static final Chars PROP_VALUES = Chars.constant("values");

    public Integer getCount() {
        return getInteger(PROP_COUNT, 0);
    }

    public void setCount(Integer value) {
        setPropertyValue(PROP_COUNT, value);
    }

    public Object getIndices() {
        return getPropertyValue(PROP_INDICES);
    }

    public void setIndices(Integer value) {
        setPropertyValue(PROP_INDICES, value);
    }

    public Object getValues() {
        return getPropertyValue(PROP_VALUES);
    }

    public void setValues(Integer value) {
        setPropertyValue(PROP_VALUES, value);
    }

}
