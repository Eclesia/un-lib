
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Camera extends GltfDocument {

    public static final Chars PROP_ORTHOGRAPHIC = Chars.constant("orthographic");
    public static final Chars PROP_PERSPECTIVE = Chars.constant("perspective");
    public static final Chars PROP_TYPE = Chars.constant("type");
    public static final Chars PROP_NAME = Chars.constant("name");

    public Orthographic getOrthographic() {
        return (Orthographic) getDocument(PROP_ORTHOGRAPHIC, Orthographic.class);
    }

    public void setOrthographic(Orthographic value) {
        setPropertyValue(PROP_ORTHOGRAPHIC, value);
    }

    public Perspective getPerspective() {
        return (Perspective) getDocument(PROP_PERSPECTIVE, Perspective.class);
    }

    public void setPerspective(Perspective value) {
        setPropertyValue(PROP_PERSPECTIVE, value);
    }

    public Chars getCameraType() {
        return getChars(PROP_TYPE, null);
    }

    public void setCameraType(Chars value) {
        setPropertyValue(PROP_TYPE, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }
}
