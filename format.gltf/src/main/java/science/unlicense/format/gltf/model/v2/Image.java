
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Image extends GltfDocument {

    public static final Chars PROP_URI = Chars.constant("uri");
    public static final Chars PROP_MIMETYPE = Chars.constant("mimeType");
    public static final Chars PROP_BUFFERVIEW = Chars.constant("bufferView");
    public static final Chars PROP_NAME = Chars.constant("name");


    public Chars getUri() {
        return getChars(PROP_URI, null);
    }

    public void setUri(Chars value) {
        setPropertyValue(PROP_URI, value);
    }

    public Chars getMimeType() {
        return getChars(PROP_MIMETYPE, null);
    }

    public void setMimeType(Chars value) {
        setPropertyValue(PROP_MIMETYPE, value);
    }

    public Integer getBufferView() {
        return getInteger(PROP_BUFFERVIEW, null);
    }

    public void setBufferView(Integer value) {
        setPropertyValue(PROP_BUFFERVIEW, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }

}
