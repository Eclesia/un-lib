
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Primitive extends GltfDocument {

    public static final Chars PROP_ATTRIBUTES = Chars.constant("attributes");
    public static final Chars PROP_INDICES = Chars.constant("indices");
    public static final Chars PROP_MATERIAL = Chars.constant("material");
    public static final Chars PROP_MODE = Chars.constant("mode");
    public static final Chars PROP_TARGETS = Chars.constant("targets");

    public Document getAttributes() {
        return getDocument(PROP_ATTRIBUTES, Document.class);
    }

    public void setAttributes(Document value) {
        setPropertyValue(PROP_ATTRIBUTES, value);
    }

    public Integer getIndices() {
        return getInteger(PROP_INDICES, null);
    }

    public void setIndices(Integer value) {
        setPropertyValue(PROP_INDICES, value);
    }

    public Integer getMaterial() {
        return getInteger(PROP_MATERIAL, null);
    }

    public void setMaterial(Integer value) {
        setPropertyValue(PROP_MATERIAL, value);
    }

    public Integer getMode() {
        return getInteger(PROP_MODE, 4);
    }

    public void setMode(Integer value) {
        setPropertyValue(PROP_MODE, value);
    }

    public Document getTargets() {
        return getDocument(PROP_TARGETS, Document.class);
    }

    public void setTargets(Document value) {
        setPropertyValue(PROP_TARGETS, value);
    }
}
