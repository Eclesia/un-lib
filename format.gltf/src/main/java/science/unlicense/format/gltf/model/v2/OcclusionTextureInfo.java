
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class OcclusionTextureInfo extends GltfDocument {

    public static final Chars PROP_INDEX = Chars.constant("index");
    public static final Chars PROP_TEXCOORD = Chars.constant("texCoord");
    public static final Chars PROP_STRENGTH = Chars.constant("strength");

    public Integer getIndex() {
        return getInteger(PROP_INDEX, null);
    }

    public void setIndex(Integer value) {
        setPropertyValue(PROP_INDEX, value);
    }

    public Integer getTexCoord() {
        return getInteger(PROP_TEXCOORD, 0);
    }

    public void setTexCoord(Integer value) {
        setPropertyValue(PROP_TEXCOORD, value);
    }

    public Double getStrength() {
        return getNumber(PROP_STRENGTH, 1.0);
    }

    public void setStrength(Double value) {
        setPropertyValue(PROP_STRENGTH, value);
    }

}
