
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Channel extends GltfDocument {

    public static final Chars PROP_SAMPLER = Chars.constant("sampler");
    public static final Chars PROP_TARGET = Chars.constant("target");

    public Integer getSampler() {
        return getInteger(PROP_SAMPLER, null);
    }

    public void setSampler(Integer value) {
        setPropertyValue(PROP_SAMPLER, value);
    }

    //TODO : doc says Object, shouldn't it be integer ?
    public Integer getTarget() {
        return getInteger(PROP_TARGET, null);
    }

    public void setTarget(Integer value) {
        setPropertyValue(PROP_TARGET, value);
    }

}
