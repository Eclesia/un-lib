
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.Vector;
import science.unlicense.math.impl.Vector3f32;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Material extends GltfDocument {

    public static final Chars PROP_NAME = Chars.constant("name");
    public static final Chars PROP_PBRMETALLICROUGHNESS = Chars.constant("pbrMetallicRoughness");
    public static final Chars PROP_NORMALTEXTURE = Chars.constant("normalTexture");
    public static final Chars PROP_OCCLUSIONTEXTURE = Chars.constant("occlusionTexture");
    public static final Chars PROP_EMISSIVETEXTURE = Chars.constant("emissiveTexture");
    public static final Chars PROP_EMISSIVEFACTOR = Chars.constant("emissiveFactor");
    public static final Chars PROP_ALPHAMODE = Chars.constant("alphaMode");
    public static final Chars PROP_ALPHACUTOFF = Chars.constant("alphaCutoff");
    public static final Chars PROP_DOUBLESIDED = Chars.constant("doubleSided");

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }

    public PbrMetallicRoughness getPbrMetallicRoughness() {
        return (PbrMetallicRoughness) getDocument(PROP_PBRMETALLICROUGHNESS, PbrMetallicRoughness.class);
    }

    public void setPrMetallicRoughness(PbrMetallicRoughness value) {
        setPropertyValue(PROP_PBRMETALLICROUGHNESS, value);
    }

    public NormalTextureInfo getNormalTexture() {
        return (NormalTextureInfo) getDocument(PROP_NORMALTEXTURE, NormalTextureInfo.class);
    }

    public void setNormalTexture(NormalTextureInfo value) {
        setPropertyValue(PROP_NORMALTEXTURE, value);
    }

    public OcclusionTextureInfo getOcclusionTexture() {
        return (OcclusionTextureInfo) getDocument(PROP_OCCLUSIONTEXTURE, OcclusionTextureInfo.class);
    }

    public void setOcclusionTexture(OcclusionTextureInfo value) {
        setPropertyValue(PROP_OCCLUSIONTEXTURE, value);
    }

    public Document getEmissiveTexture() {
        return getDocument(PROP_EMISSIVETEXTURE, Document.class);
    }

    public void setEmissiveTexture(Document value) {
        setPropertyValue(PROP_EMISSIVETEXTURE, value);
    }

    public Vector getEmissiveFactor() {
        return getVector(PROP_EMISSIVEFACTOR, new Vector3f32(0, 0, 0));
    }

    public void setEmissiveFactor(Tuple value) {
        setTuple(PROP_EMISSIVEFACTOR, value);
    }

    public Chars getAlphaMode() {
        return getChars(PROP_ALPHAMODE, new Chars("OPAQUE"));
    }

    public void setAlphaMode(Chars value) {
        setPropertyValue(PROP_ALPHAMODE, value);
    }

    public Double getAlphaCutoff() {
        return getNumber(PROP_ALPHACUTOFF, 0.0);
    }

    public void setAlphaCutoff(Double value) {
        setPropertyValue(PROP_ALPHACUTOFF, value);
    }

    public Boolean getDoubleSided() {
        return getBoolean(PROP_DOUBLESIDED, Boolean.FALSE);
    }

    public void setDoubleSided(Boolean value) {
        setPropertyValue(PROP_DOUBLESIDED, value);
    }
}
