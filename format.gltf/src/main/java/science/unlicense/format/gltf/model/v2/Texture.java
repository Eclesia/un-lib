
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Texture extends GltfDocument {

    public static final Chars PROP_SAMPLER = Chars.constant("sampler");
    public static final Chars PROP_SOURCE = Chars.constant("source");
    public static final Chars PROP_NAME = Chars.constant("name");


    public Integer getSampler() {
        return getInteger(PROP_SAMPLER, null);
    }

    public void setSampler(Integer value) {
        setPropertyValue(PROP_SAMPLER, value);
    }

    public Integer getSource() {
        return getInteger(PROP_SOURCE, null);
    }

    public void setSource(Integer value) {
        setPropertyValue(PROP_SOURCE, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }
}
