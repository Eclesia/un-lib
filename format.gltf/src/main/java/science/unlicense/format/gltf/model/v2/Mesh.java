
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.Vector;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class Mesh extends GltfDocument {

    public static final Chars PROP_PRIMITIVES = Chars.constant("primitives");
    public static final Chars PROP_WEIGHTS = Chars.constant("weights");
    public static final Chars PROP_NAME = Chars.constant("name");

    public Primitive[] getPrimitives() {
        return (Primitive[]) getDocuments(PROP_PRIMITIVES, Primitive.class);
    }

    public void setPrimitives(Primitive[] value) {
        setPropertyValue(PROP_PRIMITIVES, value);
    }

    public Vector getWeights() {
        return getVector(PROP_WEIGHTS, null);
    }

    public void setWeights(Tuple value) {
        setTuple(PROP_WEIGHTS, value);
    }

    public Chars getName() {
        return getChars(PROP_NAME, null);
    }

    public void setName(Chars value) {
        setPropertyValue(PROP_NAME, value);
    }
}
