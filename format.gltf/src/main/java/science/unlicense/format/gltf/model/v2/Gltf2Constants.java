
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class Gltf2Constants {

    public static final Chars INTERPOLATION_LINEAR = Chars.constant("LINEAR");
    public static final Chars INTERPOLATION_STEP = Chars.constant("STEP");
    public static final Chars INTERPOLATION_CUBICSPLINE = Chars.constant("CUBICSPLINE");

    public static final Chars TYPE_SCALAR = Chars.constant("SCALAR");
    public static final Chars TYPE_VEC2 = Chars.constant("VEC2");
    public static final Chars TYPE_VEC3 = Chars.constant("VEC3");
    public static final Chars TYPE_VEC4 = Chars.constant("VEC4");
    public static final Chars TYPE_MAT2 = Chars.constant("MAT2");
    public static final Chars TYPE_MAT3 = Chars.constant("MAT3");
    public static final Chars TYPE_MAT4 = Chars.constant("MAT4");

    public static final Chars ATT_POSITION = Chars.constant("POSITION");
    public static final Chars ATT_NORMAL = Chars.constant("NORMAL");
    public static final Chars ATT_TANGENT = Chars.constant("TANGENT");
    public static final Chars ATT_TEXCOORD_0 = Chars.constant("TEXCOORD_0");
    public static final Chars ATT_TEXCOORD_1 = Chars.constant("TEXCOORD_1");
    public static final Chars ATT_COLOR_0 = Chars.constant("COLOR_0");
    public static final Chars ATT_JOINTS_0 = Chars.constant("JOINTS_0");
    public static final Chars ATT_WEIGHTS_0 = Chars.constant("WEIGHTS_0");


    private Gltf2Constants(){}
}
