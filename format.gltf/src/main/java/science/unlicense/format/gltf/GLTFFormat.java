
package science.unlicense.format.gltf;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * GLTF Format.
 *
 * resource :
 * https://www.khronos.org/gltf
 *
 * @author Johann Sorel
 */
public class GLTFFormat extends AbstractModel3DFormat{

    public static final GLTFFormat INSTANCE = new GLTFFormat();

    private GLTFFormat() {
        super(new Chars("GLTF"));
        shortName = new Chars("GLTF");
        longName = new Chars("GLTF");
        extensions.add(new Chars("gltf"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new GltfStore(input);
    }

}
