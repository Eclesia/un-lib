
package science.unlicense.format.gltf.model.v2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.gltf.model.GltfDocument;

/**
 *
 * @author Johann Sorel
 */
public class SparseValues extends GltfDocument {

    public static final Chars PROP_BUFFERVIEW = Chars.constant("bufferView");
    public static final Chars PROP_BYTEOFFSET = Chars.constant("byteOffset");

    public Integer getBufferView() {
        return getInteger(PROP_BUFFERVIEW, null);
    }

    public void setBufferView(Integer value) {
        setPropertyValue(PROP_BUFFERVIEW, value);
    }

    public Integer getByteOffset(){
        return getInteger(PROP_BYTEOFFSET, null);
    }

    public void setByteOffset(Integer value) {
        setPropertyValue(PROP_BYTEOFFSET, value);
    }

}
