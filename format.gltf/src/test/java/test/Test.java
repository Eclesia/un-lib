
package test;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.gltf.GltfStore;
import science.unlicense.concurrent.api.Paths;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author husky
 */
public class Test {

    public static void main(String[] args) throws Exception {

        GltfStore store = new GltfStore(Paths.resolve(new Chars("/home/husky/temp/gltf/Avocado.gltf")));
        store.getElements();

    }

}
