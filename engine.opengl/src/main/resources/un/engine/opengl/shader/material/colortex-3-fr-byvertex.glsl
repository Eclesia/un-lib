#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform float $prefix_ratio = 1.0;

<VARIABLE_IN>
vec4 color;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>
    $produce = $method(inData.color, $produce);
