#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform sampler2D normalSampler0;

<VARIABLE_IN>
vec2 uv;
mat3 tangentMatrix;

<FUNCTION>

<OPERATION>
    // sample the normal map and covert from 0:1 range to -1:1 range
    normal_model = texture (normalSampler0, inData.uv).rgb;
    normal_model = normal_model * 2.0 - 1.0;
    normal_model = normalize(inData.tangentMatrix * normal_model);

