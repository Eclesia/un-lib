#version 330

<STRUCTURE>

<LAYOUT>
layout(triangles_adjacency) in;
layout(line_strip, max_vertices = 12) out;

<UNIFORM>

<VARIABLE_IN>
vec4 position_model;
vec4 normal_model;

<VARIABLE_OUT>

<FUNCTION>
vec4 calculateCenter(vec4 v0, vec4 v1, vec4 v2){
    vec4 c = (v0 + v1 + v2) / 3.0;
    return MVP * c;
}

vec4 calculateIntersect(vec4 v0, vec4 v1){
    vec4 c = (v0 + v1) / 2.0;
    return MVP * c;
}

<OPERATION>
    vec4 c024 = calculateCenter(inData[0].position_model, inData[2].position_model, inData[4].position_model);
    vec4 c012 = calculateCenter(inData[0].position_model, inData[1].position_model, inData[2].position_model);
    vec4 c234 = calculateCenter(inData[2].position_model, inData[3].position_model, inData[4].position_model);
    vec4 c450 = calculateCenter(inData[4].position_model, inData[5].position_model, inData[0].position_model);

    vec4 c02 = calculateIntersect(inData[0].position_model, inData[2].position_model);
    vec4 c24 = calculateIntersect(inData[2].position_model, inData[4].position_model);
    vec4 c40 = calculateIntersect(inData[4].position_model, inData[0].position_model);


    gl_Position = c024; EmitVertex();
    gl_Position = c02; EmitVertex();
    EndPrimitive();
    gl_Position = c02; EmitVertex();
    gl_Position = c012; EmitVertex();
    EndPrimitive();

    gl_Position = c024; EmitVertex();
    gl_Position = c24; EmitVertex();
    EndPrimitive();
    gl_Position = c24; EmitVertex();
    gl_Position = c234; EmitVertex();
    EndPrimitive();

    gl_Position = c024; EmitVertex();
    gl_Position = c40; EmitVertex();
    EndPrimitive();
    gl_Position = c40; EmitVertex();
    gl_Position = c450; EmitVertex();
    EndPrimitive();