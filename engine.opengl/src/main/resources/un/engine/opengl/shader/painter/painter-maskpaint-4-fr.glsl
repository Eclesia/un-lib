#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
uniform isampler2D SAMPLER;
uniform vec4 COLOR;

<VARIABLE_IN>

<VARIABLE_OUT>

<FUNCTION>

<VARIABLE_WS>
//each bit mask value
const int s01 = 1;
const int s02 = 2;
const int s03 = 4;
const int s04 = 8;
const int s05 = 16;
const int s06 = 32;
const int s07 = 64;
const int s08 = 128;
const int s09 = 256;
const int s10 = 512;
const int s11 = 1024;
const int s12 = 2048;
const int s13 = 4096;
const int s14 = 8192;
const int s15 = 16384;
const int s16 = 32768;

const float PI = 3.1415926535897932384626433832795;


<OPERATION>
    ivec2 viewSize = textureSize(SAMPLER,0);
    vec2 coord = vec2(gl_FragCoord.x/ float(viewSize.x), gl_FragCoord.y/ float(viewSize.y) );
    int mask = texture(SAMPLER,coord).r;

    float alpha = 0.0;
    if((mask & s01)!=0) alpha++;
    if((mask & s02)!=0) alpha++;
    if((mask & s03)!=0) alpha++;
    if((mask & s04)!=0) alpha++;
    if((mask & s05)!=0) alpha++;
    if((mask & s06)!=0) alpha++;
    if((mask & s07)!=0) alpha++;
    if((mask & s08)!=0) alpha++;
    if((mask & s09)!=0) alpha++;
    if((mask & s10)!=0) alpha++;
    if((mask & s11)!=0) alpha++;
    if((mask & s12)!=0) alpha++;
    if((mask & s13)!=0) alpha++;
    if((mask & s14)!=0) alpha++;
    if((mask & s15)!=0) alpha++;
    if((mask & s16)!=0) alpha++;

    alpha = alpha /16.0;

    //enhance border
    //alpha = smoothstep(0.0,1.0,alpha);

    outColor = COLOR*alpha;
