#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform sampler2D $prefix_tex;

<VARIABLE_IN>

<VARIABLE_WS>

<FUNCTION>

<OPERATION>
    //reflection vector
    vec3 $prefix_sr = reflect(
        normalize(inData.position_camera.xyz),
        normalize(normal_camera.xyz));
    //convert to texture coordinate
    float $prefix_sd = 2.0 * sqrt($prefix_sr.x*$prefix_sr.x + $prefix_sr.y*$prefix_sr.y + ($prefix_sr.z+1.0)*($prefix_sr.z+1.0) );
    vec2 $prefix_uv = vec2($prefix_sr.x/$prefix_sd+0.5, $prefix_sr.y/$prefix_sd+0.5);
    $produce = $method(texture($prefix_tex, $prefix_uv).rgba,$produce);
