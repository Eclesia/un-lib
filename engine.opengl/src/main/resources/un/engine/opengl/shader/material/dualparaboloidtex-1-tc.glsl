#version 400
<STRUCTURE>

<LAYOUT>

<UNIFORM>

<VARIABLE_IN>
vec3 $prefix_uvz;

<VARIABLE_OUT>
vec3 $prefix_uvz;

<FUNCTION>

<OPERATION>
    outData[gl_InvocationID].$prefix_uvz = inData[gl_InvocationID].$prefix_uvz;