#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform mat4 MVP;

<VARIABLE_OUT>
vec4 position_proj;
vec2 uv;

<VARIABLE_WS>
// triangles 3,0,1, 3,1,2
//const vec4[4] coords = vec4[4](vec4(-1,-1,0,1),vec4(-1,+1,0,1),vec4(+1,+1,0,1),vec4(+1,-1,0,1));
const vec2[4] coords = vec2[4](vec2(-1,+1),vec2(+1,+1),vec2(-1,-1), vec2(+1,-1));

<FUNCTION>

<OPERATION>
    vec4 coord = vec4(coords[gl_VertexID],0,1);
    outData.position_proj = MVP * coord;
    outData.uv = (coord.xy*0.5)+0.5;

    gl_Position = outData.position_proj;