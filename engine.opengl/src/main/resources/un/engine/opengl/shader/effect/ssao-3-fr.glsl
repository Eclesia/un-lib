#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
//sampler0 = texture position world space
//sampler1 = texture normal world space
uniform mat4 V;
uniform mat4 P;
uniform sampler2D randomSampler;

uniform vec2 samplesVec[128];
uniform int samplesNb;
uniform float samplesRadius;
uniform float intensity;
uniform float scale;
uniform float bias;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>
vec3 getPosition(in vec2 uv){
    vec3 data = texture(sampler0,uv).xyz;
    return vec3( V * vec4(data,1));
}

vec3 getNormal(in vec2 uv){
    vec3 data = texture(sampler1,uv).xyz;
    return vec3( V * vec4(data,0));
}

float doAmbientOcclusion(vec2 tcoord, vec2 uv, vec3 p, vec3 cnorm){
    vec3 diff = getPosition(tcoord + uv) - p;
    vec3 v = normalize(diff);
    float d = length(diff)*scale;
    return max(0.0,dot(cnorm,v)-bias)*(1.0/(1.0+d))*intensity;
}

<OPERATION>
    // current fragment position and normal
    vec3 positionView = getPosition(inData.uv);
    vec3 normalView = normalize(getNormal(inData.uv));
    vec2 rand = normalize(texture(randomSampler, PX * inData.uv / 64).xy * 2.0 - 1.0);

    float rad = samplesRadius / positionView.z;
    float occ = 0.0;

    for(int j= 0; j<samplesNb; j++){
        vec2 coord1 = reflect(samplesVec[j],rand)*rad;
        occ += doAmbientOcclusion(inData.uv,coord1,      positionView, normalView);
    }
    occ /= samplesNb;

    outColor = vec4(occ,occ,occ,1);
