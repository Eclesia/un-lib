#version 400

<STRUCTURE>
struct PPatch {
    float ij;
    float jk;
    float ik;
};

<LAYOUT>
layout(vertices=3) out;

<UNIFORM>
uniform int innerfactor;
uniform int outterfactor;

<VARIABLE_IN>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;

<VARIABLE_OUT>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;
PPatch opatch;

<FUNCTION>
float PIi (int i, vec4 q) {
    vec4 q_minus_p = q - inData[i].position_model;
    return q[gl_InvocationID] - dot(q_minus_p.xyz, inData[i].normal_model.xyz) * inData[i].normal_model[gl_InvocationID];
}

<OPERATION>
    outData[gl_InvocationID].position_world  = inData[gl_InvocationID].position_world;
    outData[gl_InvocationID].position_model  = inData[gl_InvocationID].position_model;
    outData[gl_InvocationID].position_camera = inData[gl_InvocationID].position_camera;
    outData[gl_InvocationID].position_proj   = inData[gl_InvocationID].position_proj;
    outData[gl_InvocationID].normal_world    = inData[gl_InvocationID].normal_world;
    outData[gl_InvocationID].normal_model    = inData[gl_InvocationID].normal_model;

    //calculate phong patch
    outData[gl_InvocationID].opatch.ij = PIi(0,inData[1].position_model) + PIi(1,inData[0].position_model);
    outData[gl_InvocationID].opatch.jk = PIi(1,inData[2].position_model) + PIi(2,inData[1].position_model);
    outData[gl_InvocationID].opatch.ik = PIi(2,inData[0].position_model) + PIi(0,inData[2].position_model);

    //tessellation factor
    gl_TessLevelInner[0] = innerfactor;
    gl_TessLevelOuter[0] = outterfactor;
    gl_TessLevelOuter[1] = outterfactor;
    gl_TessLevelOuter[2] = outterfactor;