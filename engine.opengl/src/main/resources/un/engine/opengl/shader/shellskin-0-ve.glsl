#version 330

<STRUCTURE>

<LAYOUT>
layout(location = $IDX) in $weightType l_jointweight;
layout(location = $IDX) in $indextType l_jointindex;

<UNIFORM>
uniform int NBJOINT;
uniform mat4 JOINTS[128]; //128 should be more then enough for a mesh

<VARIABLE_OUT>

<VARIABLE_WS>
vec3 w_position_model = vec3(0,0,0);
float w_position_factor = 1.0;

<FUNCTION>

<OPERATION>
    // morph actor may have set the position before, we don't want to override it
    w_position_model = w_position_model + l_position*w_position_factor;

    mat4 jointMatrix;
    vec4 l_position4 = vec4(w_position_model,1);
    vec4 l_normal4 = vec4(l_normal,0);
    vec4 posModel = vec4(0,0,0,1);
    vec4 norModel = vec4(0,0,0,0);
    for(int i=0;i<NBJOINT;i++){
        jointMatrix = JOINTS[l_jointindex[i]];
        posModel.xyz += ((jointMatrix * l_position4) * l_jointweight[i]).xyz;
        norModel.xyz += ((jointMatrix * l_normal4) * l_jointweight[i]).xyz;
    }
