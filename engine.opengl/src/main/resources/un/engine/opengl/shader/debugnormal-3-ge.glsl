#version 400

<STRUCTURE>

<LAYOUT>
layout(triangles) in;
layout(line_strip, max_vertices = 6) out;

<UNIFORM>
uniform float normalLength = 1.0;

<VARIABLE_IN>
vec4 position_model;
vec3 normal_model;

<VARIABLE_OUT>
vec3 normal_model;

<FUNCTION>

<OPERATION>
    outData.normal_model = vec3(1,0,0);

    gl_Position = MVP * inData[0].position_model;
    EmitVertex();
    gl_Position = MVP * vec4((inData[0].position_model.xyz + inData[0].normal_model*normalLength),1);
    EmitVertex();
    EndPrimitive();

    gl_Position = MVP * inData[1].position_model;
    EmitVertex();
    gl_Position = MVP * vec4((inData[1].position_model.xyz + inData[1].normal_model*normalLength),1);
    EmitVertex();
    EndPrimitive();

    gl_Position = MVP * inData[2].position_model;
    EmitVertex();
    gl_Position = MVP * vec4((inData[2].position_model.xyz + inData[2].normal_model*normalLength),1);
    EmitVertex();
    EndPrimitive();