#version 400
<STRUCTURE>

<LAYOUT>

<UNIFORM>

<VARIABLE_IN>
vec2 uv;

<VARIABLE_OUT>
vec2 uv;

<FUNCTION>

<OPERATION>
    float t0 = gl_TessCoord.x * inData[0].uv.x + gl_TessCoord.y * inData[1].uv.x+ + gl_TessCoord.z * inData[2].uv.x;
    float t1 = gl_TessCoord.x * inData[0].uv.y + gl_TessCoord.y * inData[1].uv.y+ + gl_TessCoord.z * inData[2].uv.y;
    outData.uv = vec2(t0,t1);
