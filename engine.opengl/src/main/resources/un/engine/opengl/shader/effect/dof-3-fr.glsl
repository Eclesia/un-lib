#version 330

<STRUCTURE>
//
// The blur factor calculation was from :
// "Depth of Field" demo for Ogre
// Copyright (C) 2006  Christian Lindequist Larsen
// This code is in the public domain. You may do whatever you want with it.
//
// @author Christian Lindequist Larsen (original blur factor calculation)
// @author Johann Sorel (new blur factor math and texture merge)
//

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
// sampler0 = default RGBA image
// sampler1 = world position
// sampler2 = blured RGBA image
// dofParams coefficients
// x = near blur depth
// y = focal plane min depth
// z = focal plane max depth
// w = far blur depth
uniform vec4 dofParams;
uniform sampler2D texDepth;
uniform mat4 V;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>
vec3 getPosition(){
    vec3 data = texture(sampler1,inData.uv).xyz;
    return vec3( V * vec4(data,1));
}

<OPERATION>
    //depth is negative relative to camera position
    float depth = -getPosition().z;
    float f = 0.0;
    if (depth < dofParams.y){
        // scale depth value between near blur distance and focal distance to [-1, 0] range
        f = ((dofParams.y - depth) / (dofParams.y - dofParams.x));
    }else if (depth > dofParams.z){
        // scale depth value between focal distance and far blur distance to [0, 1] range
        f = (depth - dofParams.z) / (dofParams.w - dofParams.z);
    }
    // clamp the far blur to a maximum blurriness
    f = clamp(f, 0.0, 1.0);

    vec4 colorBase = texture(sampler0, inData.uv);
    vec4 colorBlur = texture(sampler2, inData.uv);

    // scale and bias into [0, 1] range
    outColor = vec4(0);
    outColor += colorBase*(1.0-f);
    outColor += colorBlur*f;
