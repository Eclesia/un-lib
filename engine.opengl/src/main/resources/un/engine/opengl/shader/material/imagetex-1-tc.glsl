#version 400
<STRUCTURE>

<LAYOUT>

<UNIFORM>

<VARIABLE_IN>
vec2 uv;

<VARIABLE_OUT>
vec2 uv;

<FUNCTION>

<OPERATION>
    outData[gl_InvocationID].uv = inData[gl_InvocationID].uv;
