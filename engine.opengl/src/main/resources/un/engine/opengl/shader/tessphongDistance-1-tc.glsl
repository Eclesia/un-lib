#version 400

<STRUCTURE>
struct PPatch {
    float ij;
    float jk;
    float ik;
};

<LAYOUT>
layout(vertices=3) out;

<UNIFORM>
// b - ax  : factor[1] - distance*factor[0]
uniform vec2 UNI_LINEAR;

<VARIABLE_IN>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;

<VARIABLE_OUT>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;
PPatch opatch;

<FUNCTION>
float PIi (int i, vec4 q) {
    vec4 q_minus_p = q - inData[i].position_model;
    return q[gl_InvocationID] - dot(q_minus_p.xyz, inData[i].normal_model) * inData[i].normal_model[gl_InvocationID];
}

int tessFactor(int i1, int i2){
    float z1 = -inData[i1].position_camera.z;
    float z2 = -inData[i2].position_camera.z;
    if(max(z1,z2)>0){
        int v = int(UNI_LINEAR[1] - UNI_LINEAR[0]*min(z1,z2));
        return max(v,1);
    }else{
        return 0;
    }
}

<OPERATION>
    outData[gl_InvocationID].position_world  = inData[gl_InvocationID].position_world;
    outData[gl_InvocationID].position_model  = inData[gl_InvocationID].position_model;
    outData[gl_InvocationID].position_camera = inData[gl_InvocationID].position_camera;
    outData[gl_InvocationID].position_proj   = inData[gl_InvocationID].position_proj;
    outData[gl_InvocationID].normal_world    = inData[gl_InvocationID].normal_world;
    outData[gl_InvocationID].normal_model    = inData[gl_InvocationID].normal_model;

    //calculate phong patch
    outData[gl_InvocationID].opatch.ij = PIi(0,inData[1].position_model) + PIi(1,inData[0].position_model);
    outData[gl_InvocationID].opatch.jk = PIi(1,inData[2].position_model) + PIi(2,inData[1].position_model);
    outData[gl_InvocationID].opatch.ik = PIi(2,inData[0].position_model) + PIi(0,inData[2].position_model);

    //tessellation factor
    // note : outer level index are for the opposite corners, so edge 0 is for corners 1,2
    gl_TessLevelOuter[0] = tessFactor(1,2);
    gl_TessLevelOuter[1] = tessFactor(2,0);
    gl_TessLevelOuter[2] = tessFactor(0,1);
    gl_TessLevelInner[0] = max(max(gl_TessLevelOuter[0],gl_TessLevelOuter[1]),gl_TessLevelOuter[2]);
