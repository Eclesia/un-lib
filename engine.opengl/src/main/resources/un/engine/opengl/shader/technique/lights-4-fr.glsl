#version 330

<STRUCTURE>
struct Light {
    vec4 position; //in world space
    vec3 diffuse;
    vec3 specular;
    float constantAttenuation, linearAttenuation, quadraticAttenuation;
    float spotCutoff, spotExponent;
    vec3 spotDirection;
    //for shadow
    int hasShadowMap;
    mat4 v;
    mat4 p;
};

struct LightFrag {
    vec3 normal_model;
    vec3 normal_world;
    vec3 normal_camera;
    vec3 normal_proj;
    vec3 normal;
    vec3 eyeDirection;
    vec3 diffuse;
    vec3 specular;
};

<LAYOUT>

<UNIFORM>
uniform mat4 MODEL;
uniform mat4 VIEW;
uniform mat4 PROJECTION;
uniform mat4 MODELVIEW;
uniform mat4 MODELVIEWPROJECTION;
uniform vec2 VIEWPORT;

uniform int LIGHT_NB;
uniform vec3 LIGHT_AMBIENT;
uniform float CELLSHADINGSTEPS = 0.0;
uniform Light LIGHTS[5]; //fix size
uniform sampler2D SHADOWMAPS[5]; //fix size


<VARIABLE_IN>
    vec4 position_model;
    vec4 position_world;
    vec4 position_camera;
    vec4 position_proj;



<FUNCTION>
void recalculateNormals(LightFrag frag) {
    frag.normal_model  = normalize(frag.normal_model);
    frag.normal_world  = normalize((MODEL * vec4(frag.normal_model,0)).xyz);
    frag.normal_camera = normalize((MODELVIEW * vec4(frag.normal_model,0)).xyz);
    frag.normal_proj   = normalize((MODELVIEWPROJECTION * vec4(frag.normal_model,0)).xyz);
}

float attenuation(Light light, float distance) {
    return 1.0 / (light.constantAttenuation
                + light.linearAttenuation    * distance
                + light.quadraticAttenuation * distance * distance);
}


float chebyshevUpperBound(vec2 moments, float t) {
    // One-tailed inequality valid if t > moments.x
    float p = float(t <= moments.x);
    // Compute variance.
    float variance = moments.y - (moments.x * moments.x);
    // variance = max(variance, g_MinVariance);
    // Compute probabilistic upper bound.
    float d = t - moments.x;
    float p_max = variance / (variance + d*d);
    return max(p, p_max);
}

float shadowContribution(vec2 lightTexCoord, float distanceToLight, sampler2D shadowSampler) {
    // Read the moments from the variance shadow map.
    vec2 moments = texture(shadowSampler, lightTexCoord).xy;
    // Compute the Chebyshev upper bound.
    return chebyshevUpperBound(moments, distanceToLight);
}

float calculateShadowFactor(Light light, sampler2D shadowMap) {
    //convert fragment position to light space
    vec4 fragPosLightSpace = (light.v * vec4 (inData.position_world.xyz, 1.0));
    vec4 fragPosLightProj = (light.p * fragPosLightSpace);
    float distToLight = -fragPosLightSpace.z;

    //calculate shadow map coordinate
    vec4 shadowCoord = fragPosLightProj;
    shadowCoord.xyz /= shadowCoord.w;
    shadowCoord.xy += 1.0;
    shadowCoord.xy *= 0.5;

    return shadowContribution(shadowCoord.xy, distToLight, shadowMap);
}

float cellShading(float value) {
    if (CELLSHADINGSTEPS > 0) {
        //cell shading, also called Toon shading
        value = int(value * CELLSHADINGSTEPS) / CELLSHADINGSTEPS;
    }
    return value;
}

float calcSpecularFactor(vec3 fragNormal, vec3 toLight, vec3 toEye) {

    // Standard reflection method, slower but more accurate
    vec3 reflection = reflect(-toLight, fragNormal);
    float specularFactor = clamp(dot(reflection, toEye),0.0,1.0);
    specularFactor = pow(specularFactor, 5.0);

    // Blinn-Phong method, faster but less accurate
    //vec3 halfWay = normalize(toEye + toLight);
    //float specularFactor = max(dot(halfWay, fragNormal),0.0);
    //specularFactor = pow(specularFactor, material.shininess);

    if (CELLSHADINGSTEPS > 0) {
        //cell shading, also called Toon shading
        specularFactor = specularFactor < 0.5 ? 0.0 : 1.0;
    }

    return specularFactor;
}

void directionalLight(Light light, LightFrag frag) {
    vec3 fragToLightDirection = vec3(VIEW * vec4(-light.spotDirection.xyz, 0.0));

    float diffuseRatio = clamp(dot(fragToLightDirection,frag.normal), 0.0, 1.0);
    diffuseRatio = cellShading(diffuseRatio);
    frag.diffuse = (light.diffuse * material.diffuse) * diffuseRatio;

    float specularFactor = 0.0;
    if (diffuseRatio > 0.0) {
        specularFactor = calcSpecularFactor(frag.normal,fragToLightDirection,frag.eyeDirection);
    }
    frag.specular = (light.specular * material.specular) * specularFactor;
}

void pointLight(Light light, LightFrag frag) {

    //convert light position to eye space
    vec3 lightCameraSpace = vec3(VIEW * vec4(light.position.xyz, 1.0) );

    // point light or spotlight (or other kind of light)
    vec3  fragToLightVector    = lightCameraSpace - inData.position_camera.xyz;
    float fragToLightDistance  = length(fragToLightVector);
    vec3  fragToLightDirection = normalize(fragToLightVector);
    float attenuation          = attenuation(light, fragToLightDistance);

    float diffuseRatio = max(dot(fragToLightDirection,frag.normal), 0.0);
    diffuseRatio = cellShading(diffuseRatio);
    frag.diffuse = (light.diffuse * material.diffuse) * diffuseRatio;

    float specularFactor = 0.0;
    if (diffuseRatio > 0.0) {
        specularFactor = calcSpecularFactor(frag.normal,fragToLightDirection,frag.eyeDirection);
    }
    frag.specular = (light.specular * material.specular) * specularFactor;

    frag.diffuse  *= attenuation;
    frag.specular *= attenuation;
}

void spotLight(Light light, LightFrag frag) {
    //convert light position to eye space
    vec3 lightCameraSpace = vec3(VIEW * vec4(light.position.xyz, 1.0) );

    vec3  fragToLightVector    = lightCameraSpace - inData.position_camera.xyz;
    float fragToLightDistance  = length(fragToLightVector);
    vec3  fragToLightDirection = normalize(fragToLightVector);
    float attenuation          = attenuation(light, fragToLightDistance);

    vec3 spotDirection = vec3(VIEW * vec4(light.spotDirection.xyz, 0.0) );
    float clampedCosine = max(0.0, dot(-fragToLightDirection, normalize(spotDirection)));
    if (clampedCosine < cos(radians(light.spotCutoff))) {
        // outside of spotlight cone
        attenuation = 0.0;
    } else {
        attenuation = attenuation * pow(clampedCosine, light.spotExponent);
    }

    float diffuseRatio = max(dot(fragToLightDirection, frag.normal), 0.0);
    diffuseRatio = cellShading(diffuseRatio);
    frag.diffuse = (light.diffuse * material.diffuse) * diffuseRatio;

    float specularFactor = 0.0;
    if (diffuseRatio > 0.0) {
        specularFactor = calcSpecularFactor(frag.normal,fragToLightDirection,frag.eyeDirection);
    }
    frag.specular = (light.specular * material.specular) * specularFactor;

    frag.diffuse  *= attenuation;
    frag.specular *= attenuation;
}

void applyShadow(Light light, LightFrag frag, int index) {
    if (light.hasShadowMap > 0) {
        float shadowFactor = calculateShadowFactor(light, SHADOWMAPS[index]);
        frag.diffuse *= shadowFactor;
        frag.specular *= shadowFactor;
    }
}

vec3 computeLight() {

    LightFrag frag = LightFrag(vec3(99.0),vec3(99.0),vec3(99.0),vec3(99.0),vec3(0.0),vec3(0.0),vec3(0.0),vec3(0.0));
    frag.normal_model = inData.normal;
    recalculateNormals(frag);

    //frag.eyeDirection = -normalize((V* vec4(inData.position_world.xyz,0)).xyz);
    frag.eyeDirection = normalize(-inData.position_camera.xyz);
    //frag.normal = normal_camera;

    vec3 totalAmbient = vec3(0.0);
    vec3 totalSpecular = vec3(0.0);
    vec3 totalDiffuse = vec3(0.0);

    // initialize total lighting with ambiant lighting
    totalAmbient = LIGHT_AMBIENT * material.ambiant;

    for (int index = 0; index < LIGHT_NB; index++) {
        Light light = LIGHTS[index];

        //calculate light/material result diffuse and specular
        if (0.0 == light.position.w) {
            // directional
            directionalLight(light, frag);
        } else if (light.spotCutoff <= 90.0) {
            // spotlight
            spotLight(light, frag);
        } else {
            // point light
            pointLight(light, frag);
        }

        //apply shadow factor
        //applyShadow(index);

        totalSpecular += frag.specular.xyz;
        totalDiffuse  += frag.diffuse.xyz;
    }

    return totalAmbient + totalDiffuse + totalSpecular;
}

