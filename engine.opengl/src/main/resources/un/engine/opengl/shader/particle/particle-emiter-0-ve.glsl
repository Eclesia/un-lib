#version 330

<STRUCTURE>

<LAYOUT>
layout(location = $IDX) in vec3 l_velocity;
layout(location = $IDX) in float l_time;

<UNIFORM>
uniform vec3 GRAVITY;
uniform float TIME;
uniform float LIFESPAN;

<VARIABLE_WS>

<VARIABLE_OUT>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;
vec3 normal_camera;
vec3 normal_proj;

<FUNCTION>

<OPERATION>
    float t = TIME - l_time;
    t = mod (t, LIFESPAN);

    vec3 p = (M * vec4(0,0,0,1)).xyz;
    // this is a standard kinematics equation of motion with velocity and acceleration (gravity)
    p += l_velocity * t + 0.5 * GRAVITY * t * t;

    outData.position_model = vec4(p,1);
    outData.position_world = M * vec4(p,1);
    outData.position_camera = MV * vec4(p,1);
    outData.position_proj = MVP * vec4(p,1);

    //always face camera TODO
    vec3 l_normal = vec3(0,0,1);
    outData.normal_model = l_normal;
    outData.normal_world = (M * vec4(l_normal,0)).xyz;
    outData.normal_camera = (MV * vec4(l_normal,0)).xyz;
    outData.normal_proj = (MVP * vec4(l_normal,0)).xyz;

    gl_Position = outData.position_proj;