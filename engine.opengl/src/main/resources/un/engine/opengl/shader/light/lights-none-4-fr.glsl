#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>

<VARIABLE_IN>
vec3 normal_model;

<VARIABLE_WS>

vec3 normal_model = vec3(99,99,99);
vec3 normal_world = vec3(99,99,99);
vec3 normal_camera = vec3(99,99,99);
vec3 normal_proj = vec3(99,99,99);


<FUNCTION>
void recalculateNormals(){
    normal_model = normalize(normal_model);
    normal_world = normalize((M * vec4(normal_model,0)).xyz);
    normal_camera = normalize((MV * vec4(normal_model,0)).xyz);
    normal_proj = normalize((MVP * vec4(normal_model,0)).xyz);
}


<OPERATION>

    if(normal_model.x == 99){
        normal_model = normalize(inData.normal_model.xyz);
    }
    recalculateNormals();

    float alpha = material.color.w;

    //skip transparent and nearly transparent fragments
    if(alpha < 0.01) discard;

    if(material.ambient.x < 0){
        //use diffuse as ambient
        material.ambient = material.color;
    }
    if(material.specular.x < 0){
        //use diffuse as specular
        material.specular = material.color;
    }
