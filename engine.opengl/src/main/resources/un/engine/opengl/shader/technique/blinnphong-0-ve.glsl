#version 330

<STRUCTURE>

<LAYOUT>
layout(location = 0) in vec3 POSITION;
layout(location = 1) in vec3 NORMAL;
layout(location = 2) in vec2 TEXCOORD_0;
layout(location = 3) in ivec4 JOINTS_0;
layout(location = 4) in vec4 WEIGHTS_0;
layout(location = 5) in vec4 JPARAMS_0;

<UNIFORM>
uniform mat4 MODEL;
uniform mat4 VIEW;
uniform mat4 PROJECTION;
uniform mat4 MODELVIEW;
uniform mat4 MODELVIEWPROJECTION;
uniform vec2 VIEWPORT;
uniform int NBJOINT;
uniform samplerBuffer JOINTSAMPLER;

<VARIABLE_OUT>
    vec4 position_model;
    vec4 position_world;
    vec4 position_camera;
    vec4 position_proj;
    vec3 normal;
    vec2 texcoord_0;

<VARIABLE_WS>
vec3 w_position_model = vec3(0,0,0);
float w_position_factor = 1.0;

vec4 l_position4;
vec4 l_normal4;
vec4 posModel;
vec4 norModel;

<FUNCTION>
/** Linear blend skinning */
void lbs(){
    for (int i=0;i<NBJOINT;i++) {
        int index = JOINTS_0[i]*4;
        float weight = WEIGHTS_0[i];
        vec4 col0 = texelFetch(JOINTSAMPLER,index+0);
        vec4 col1 = texelFetch(JOINTSAMPLER,index+1);
        vec4 col2 = texelFetch(JOINTSAMPLER,index+2);
        vec4 col3 = texelFetch(JOINTSAMPLER,index+3);
        mat4 jointMatrix = mat4(col0,col1,col2,col3);
        posModel.xyz += ((jointMatrix * l_position4) * weight).xyz;
        norModel.xyz += ((jointMatrix * l_normal4) * weight).xyz;
    }
}


void sbs(){
    //position and rotation and calculated separately with this method
    //references :
    // - http://www.seas.upenn.edu/~ladislav/kavan05spherical/kavan05spherical.pdf
    // - http://image.diku.dk/projects/media/kasper.amstrup.andersen.07.pdf

    vec3 rotCenter = JPARAMS_0.yzw;
    mat3 rotation = mat3(0);

    for (int i=0;i<NBJOINT;i++) {
        int index = JOINTS_0[i]*4;
        float weight = WEIGHTS_0[i];
        vec4 col0 = texelFetch(JOINTSAMPLER,index+0);
        vec4 col1 = texelFetch(JOINTSAMPLER,index+1);
        vec4 col2 = texelFetch(JOINTSAMPLER,index+2);
        vec4 col3 = texelFetch(JOINTSAMPLER,index+3);
        mat4 jointMatrix = mat4(col0,col1,col2,col3);
        mat3 jointRot = mat3(jointMatrix);
        vec3 jointTrs = jointMatrix[3].xyz;

        posModel.xyz += (jointRot*rotCenter + jointTrs) * weight;
        norModel.xyz += (jointRot*l_normal4.xyz) * weight;
        rotation += jointRot * weight;
    }

    posModel.xyz += rotation * (l_position4.xyz-rotCenter);
}

<OPERATION>
    outData.texcoord_0 = TEXCOORD_0;

    if (NBJOINT == 0) {
        norModel = MODELVIEWPROJECTION * vec4(NORMAL,0);
        posModel = MODELVIEWPROJECTION * vec4(POSITION, 1.0);
    } else {
        l_normal4 = vec4(NORMAL, 0.0);
        l_position4 = vec4(POSITION, 1.0);
        norModel = vec4(0.0, 0.0, 0.0, 0.0);
        posModel = vec4(0.0, 0.0, 0.0, 1.0);

        if (JPARAMS_0.x < 1.0) {
            lbs();
        } else {
            sbs();
        }
        posModel = MODELVIEWPROJECTION * posModel;
    }

    gl_Position = posModel;

    vec3 p = vec3(0,0,0);
    outData.position_model  = vec4(p, 1);
    outData.position_world  = MODEL * vec4(p, 1);
    outData.position_camera = MODELVIEW * vec4(p, 1);
    outData.position_proj   = MODELVIEWPROJECTION * vec4(p, 1);

    vec3 n = vec3(0,0,1);
    outData.normal = n;
