#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform mat4 MVP;
uniform vec2 PX;

<VARIABLE_OUT>
vec2 uv;

<VARIABLE_WS>
// triangles 3,0,1, 3,1,2
//const vec4[4] coords = vec4[4](vec4(-1,-1,0,1),vec4(-1,+1,0,1),vec4(+1,+1,0,1),vec4(+1,-1,0,1));
const vec4[6] coords = vec4[6](vec4(+1,-1,0,1),vec4(-1,-1,0,1),vec4(-1,+1,0,1), vec4(+1,-1,0,1),vec4(-1,+1,0,1),vec4(+1,+1,0,1));

<FUNCTION>

<OPERATION>
    gl_Position = MVP * coords[gl_VertexID];
    //position is [-1...+1] and there is no transform,
    //we convert it to UV coordinates here to avoid a vbo.
    outData.uv = (coords[gl_VertexID].xy*0.5)+0.5;