#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform vec4 CLIP_PLAN;

<VARIABLE_IN>

<VARIABLE_WS>

<VARIABLE_OUT>

<FUNCTION>

<OPERATION>
    float clipPos = dot(inData.position_world.xyz, CLIP_PLAN.xyz) + CLIP_PLAN.w;
    if (clipPos < 0.0) {
        discard;
    }
