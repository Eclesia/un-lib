#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform vec2 blurCenter;
//x=NbSamples , y=Strength, z=Bright
uniform vec3 blurParams = vec3(10,0.1,1.0);

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>
vec4 color = vec4(0);
float ratio;
int nbSamples;

<FUNCTION>

<OPERATION>
    //vector center <-> fragment
    vec2 suv = vec2(inData.uv) - blurCenter;
    
    nbSamples = int(blurParams.x)-1;
    for(float i=0;i<=nbSamples;i++){
        ratio = 1.0 - (i/nbSamples) * blurParams.y;
        color += texture(sampler0, blurCenter + (suv*ratio));
    }

    color /= nbSamples * blurParams.z;

