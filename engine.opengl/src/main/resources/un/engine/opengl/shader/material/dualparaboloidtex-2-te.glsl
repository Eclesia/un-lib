#version 400
<STRUCTURE>

<LAYOUT>

<UNIFORM>

<VARIABLE_IN>
vec3 $prefix_uvz;

<VARIABLE_OUT>
vec3 $prefix_uvz;

<FUNCTION>

<OPERATION>
    float $prefix_t0 = gl_TessCoord.x * inData[0].$prefix_uvz.x + gl_TessCoord.y * inData[1].$prefix_uvz.x+ + gl_TessCoord.z * inData[2].$prefix_uvz.x;
    float $prefix_t1 = gl_TessCoord.x * inData[0].$prefix_uvz.y + gl_TessCoord.y * inData[1].$prefix_uvz.y+ + gl_TessCoord.z * inData[2].$prefix_uvz.y;
    float $prefix_t2 = gl_TessCoord.x * inData[0].$prefix_uvz.z + gl_TessCoord.y * inData[1].$prefix_uvz.z+ + gl_TessCoord.z * inData[2].$prefix_uvz.z;
    outData.uvz = vec3($prefix_t0,$prefix_t1,$prefix_t2);