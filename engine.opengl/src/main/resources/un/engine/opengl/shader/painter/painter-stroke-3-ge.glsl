#version 330

<STRUCTURE>

<LAYOUT>
layout(lines) in;
layout(triangle_strip, max_vertices = 4) out;

<UNIFORM>
uniform mat3 MV;
uniform mat3 P;
uniform float WIDTH = 1.0;

<VARIABLE_IN>
vec4 position_model;
vec4 position_proj;

<VARIABLE_OUT>

<FUNCTION>

void emitEdge(vec4 P0, vec4 P1){
    vec2 V = normalize(P1.xy - P0.xy);
    vec2 N = vec2(-V.y, V.x) * (WIDTH / 2.0);

    mat3 mvp = (P*MV);

    gl_Position = vec4(mvp * vec3(P1.xy - N, 1),1); EmitVertex();
    gl_Position = vec4(mvp * vec3(P1.xy + N, 1),1); EmitVertex();
    gl_Position = vec4(mvp * vec3(P0.xy - N, 1),1); EmitVertex();
    gl_Position = vec4(mvp * vec3(P0.xy + N, 1),1); EmitVertex();
    EndPrimitive();
}

<OPERATION>
    emitEdge(inData[0].position_model,inData[1].position_model);