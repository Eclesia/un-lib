#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform sampler2D $prefix_tex;
uniform vec4 $prefix_sprite = vec4(1,1,0,0);

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<VARIABLE_OUT>

<FUNCTION>

<OPERATION>
    float depth = texture($prefix_tex,glFragment.xy);
    if($produce.x<0){ $produce = texture($prefix_tex, $prefix_suv).rgba; }
    else{             $produce = $method(texture($prefix_tex, $prefix_suv).rgba,$produce); }
