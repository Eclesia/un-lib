#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
uniform vec4 COLOR = vec4(1,0,0,1);

<VARIABLE_OUT>

<FUNCTION>

<OPERATION>
    outColor = COLOR;