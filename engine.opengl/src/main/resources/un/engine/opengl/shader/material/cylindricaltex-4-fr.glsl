#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform sampler2D $prefix_tex;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>
    $produce = $method(texture($prefix_tex, inData.uvz),$produce);
