#version 330

<STRUCTURE>
struct MaterialInfo {
    //visual parameters
    vec4 ambient;
    vec4 color;
    vec4 specular;
    float shininess;

    //volumetric parameters
    // depth of the model, this is the distance between the front and back faces
    float depth_model;
    // depth of the model, this is the distance between the front face and scene depth
    float depth_scene;

};

<LAYOUT>

<UNIFORM>
uniform float SHININESS = 0.0;
uniform vec4 AMBIENT = vec4(1,1,1,1);
uniform vec4 DIFFUSE = vec4(0,0,0,0);
uniform vec4 SPECULAR = vec4(0,0,0,0);

<VARIABLE_IN>

<VARIABLE_OUT>

<FUNCTION>
vec4 methodAddition(vec4 src, vec4 dst){
    return src+dst;
}

vec4 methodMultiply(vec4 src, vec4 dst){
    return src*dst;
}

vec4 methodSrcOver(vec4 src, vec4 dst){
    float ao = src[3] + dst[3] * (1 - src[3]);
    float Fb = 1 - src[3];
    return vec4(src[0] + Fb * dst[0],
                src[1] + Fb * dst[1],
                src[2] + Fb * dst[2],
                ao);
}

<VARIABLE_WS>
MaterialInfo material = MaterialInfo(vec4(-1,-1,-1,-1),vec4(-1,-1,-1,-1),vec4(-1,-1,-1,-1),1.0,0.0,0.0);

<OPERATION>
    material.ambient = AMBIENT;
    material.color = DIFFUSE;
    material.specular = SPECULAR;
    material.shininess = SHININESS;
