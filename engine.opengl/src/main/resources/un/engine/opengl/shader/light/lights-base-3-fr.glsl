#version 330

<STRUCTURE>
struct Light {
    vec4 position; //in world space
    vec4 diffuse;
    vec4 specular;
    float constantAttenuation, linearAttenuation, quadraticAttenuation;
    float spotCutoff, spotExponent;
    vec3 spotDirection;
    //for shadow
    int hasShadowMap;
    mat4 v;
    mat4 p;
};

struct FragLightInfo {
    vec3 normal;
    vec3 eyeDirection;
    vec4 diffuse;
    vec4 specular;
};

<LAYOUT>




<FUNCTION>


<VARIABLE_WS>
Light fragLight = Light(vec4(0),vec4(0),vec4(0),0,0,0,0,0,vec3(0),0,mat4(1.0),mat4(1.0));
FragLightInfo frag = FragLightInfo(vec3(0),vec3(0),vec4(0),vec4(0));

vec3 normal_model = vec3(99,99,99);
vec3 normal_world = vec3(99,99,99);
vec3 normal_camera = vec3(99,99,99);
vec3 normal_proj = vec3(99,99,99);