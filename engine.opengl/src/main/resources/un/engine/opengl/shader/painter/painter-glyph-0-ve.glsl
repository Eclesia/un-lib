#version 330

precision highp float;

layout(location = 0) in vec3 l_position1;
layout(location = 1) in vec3 l_position2;
layout(location = 2) in vec3 l_position3;
layout(location = 3) in vec3 l_position4;
layout(location = 4) in vec3 l_position5;

uniform mat3 MV;
uniform mat3 P;
uniform vec2 SIZE;

out Data {
    flat int quad;
    flat vec2 p0;
    flat vec2 p1;
    flat vec2 p2;
} data;

void main(void){

    mat3 MVP = P*MV;

    int m = int(mod(gl_VertexID, 3.0));
    vec2 l1 = (MVP * vec3(l_position1.x,l_position1.y,1.0)).xy;
    vec2 l2 = (MVP * vec3(l_position2.x,l_position2.y,1.0)).xy;
    vec2 l3 = (MVP * vec3(l_position3.x,l_position3.y,1.0)).xy;
    vec2 l4 = (MVP * vec3(l_position4.x,l_position4.y,1.0)).xy;
    vec2 l5 = (MVP * vec3(l_position5.x,l_position5.y,1.0)).xy;

    data.p0 = vec2(0,0);
    data.p1 = vec2(0,0);
    data.p2 = vec2(0,0);
    if(m==0){
        data.p0 = l3;
        data.p1 = l4;
        data.p2 = l5;
    }else if(m==1){
        data.p0 = l2;
        data.p1 = l3;
        data.p2 = l4;
    }else if(m==2){
        data.p0 = l1;
        data.p1 = l2;
        data.p2 = l3;
    }

    //expand triangle
    vec2 gc = (data.p0+data.p1+data.p2)/3.0;
    vec2 vc0 = normalize(data.p0-gc);
    vec2 vc1 = normalize(data.p1-gc);
    vec2 vc2 = normalize(data.p2-gc);
    vec2 v01 = normalize(data.p1-data.p0);
    vec2 v12 = normalize(data.p2-data.p1);
    vec2 v20 = normalize(data.p0-data.p2);
    float ag0 = acos(dot(vc0,v01));
    float ag1 = acos(dot(vc1,v12));
    float ag2 = acos(dot(vc2,v20));
    
    float length0 = 1.0/ sin(ag0);
    float length1 = 1.0/ sin(ag1);
    float length2 = 1.0/ sin(ag2);

    //float unit = length(2.0/SIZE.x);
    float unit = 1.0;
    vec2 b0 = data.p0 + vc0*length0*unit;
    vec2 b1 = data.p1 + vc1*length1*unit;
    vec2 b2 = data.p2 + vc2*length2*unit;

    //quad and uv used for quadratic triangles
    data.quad = int(l_position3.z+0.4);
    if(m==0){
        gl_Position = vec4(b0,0,1);
    }else if(m==1){
        gl_Position = vec4(b1,0,1);
    }else if(m==2){
        gl_Position = vec4(b2,0,1);
    }

}
