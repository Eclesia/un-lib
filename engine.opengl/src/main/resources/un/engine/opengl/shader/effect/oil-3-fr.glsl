#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
uniform vec2 oilParams;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>
vec4[32] average = vec4[32](vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0),vec4(0));
int[32] intensityCount = int[32](0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
int maxIntensity = 0;
int maxCount = -1;

<FUNCTION>
vec4 getSamples(vec2 coord){
    return texture(sampler0, inData.uv + coord*PX);
}

float itns(vec4 v){
    return (v.x+v.y+v.z+v.w)/4.0;
}

<OPERATION>

    int radius = int(oilParams.x);
    int nbLevel = int(oilParams.y);
    int size = 1 + radius + radius;


    for (int kj = 0; kj < size; kj++){
        for (int ki = 0; ki < size; ki++) {
            vec4 cdt = getSamples(vec2(kj,ki));
            float avg = itns(cdt);
            int curIntensity = int(avg*nbLevel);
            intensityCount[curIntensity]++;
            if(intensityCount[curIntensity] > maxCount){
                maxCount = intensityCount[curIntensity];
                maxIntensity = curIntensity;
            }

            average[curIntensity] += cdt;
        }
    }

    //calculate final color
    outColor = average[maxIntensity] / maxCount;
