#version 330

<STRUCTURE>

<LAYOUT>
layout(location = $IDX) in $weightType l_jointweight;
layout(location = $IDX) in $indextType l_jointindex;
layout(location = $IDX) in vec4 l_deformtype;

<UNIFORM>
uniform int NBJOINT;
uniform samplerBuffer JOINTSAMPLER;


<FUNCTION>
/** Linear blend skinning */
void lbs(){
    for(int i=0;i<NBJOINT;i++){
        int index = l_jointindex[i]*4;
        float weight = l_jointweight[i];
        vec4 col0 = texelFetch(JOINTSAMPLER,index+0);
        vec4 col1 = texelFetch(JOINTSAMPLER,index+1);
        vec4 col2 = texelFetch(JOINTSAMPLER,index+2);
        vec4 col3 = texelFetch(JOINTSAMPLER,index+3);
        jointMatrix = mat4(col0,col1,col2,col3);
        posModel.xyz += ((jointMatrix * l_position4) * weight).xyz;
        norModel.xyz += ((jointMatrix * l_normal4) * weight).xyz;
    }
}


void sbs(){
    //position and rotation and calculated separately with this method
    //references :
    // - http://www.seas.upenn.edu/~ladislav/kavan05spherical/kavan05spherical.pdf
    // - http://image.diku.dk/projects/media/kasper.amstrup.andersen.07.pdf

    vec3 rotCenter = l_deformtype.yzw;
    mat3 rotation = mat3(0);
    
    for(int i=0;i<NBJOINT;i++){
        int index = l_jointindex[i]*4;
        float weight = l_jointweight[i];
        vec4 col0 = texelFetch(JOINTSAMPLER,index+0);
        vec4 col1 = texelFetch(JOINTSAMPLER,index+1);
        vec4 col2 = texelFetch(JOINTSAMPLER,index+2);
        vec4 col3 = texelFetch(JOINTSAMPLER,index+3);
        jointMatrix = mat4(col0,col1,col2,col3);
        mat3 jointRot = mat3(jointMatrix);
        vec3 jointTrs = jointMatrix[3].xyz;

        posModel.xyz += (jointRot*rotCenter + jointTrs) * weight;
        norModel.xyz += (jointRot*l_normal4.xyz) * weight;
        rotation += jointRot * weight;
    }
    
    posModel.xyz += rotation * (l_position4.xyz-rotCenter);
}

<VARIABLE_OUT>

<VARIABLE_WS>
vec3 w_position_model = vec3(0,0,0);
float w_position_factor = 1.0;

mat4 jointMatrix;
vec4 l_position4;
vec4 l_normal4;
vec4 posModel;
vec4 norModel;

<OPERATION>
    // morph actor may have set the position before, we don't want to override it
    w_position_model = w_position_model + l_position*w_position_factor;

    l_position4 = vec4(w_position_model,1);
    l_normal4 = vec4(l_normal,0);
    posModel = vec4(0,0,0,1);
    norModel = vec4(0,0,0,0);

    if(l_deformtype.x < 1.0){
        lbs();
    }else{
        sbs();
    }
