#version 330

<STRUCTURE>

<LAYOUT>
layout(points) in;
layout(triangle_strip, max_vertices=4) out;

<UNIFORM>
//size in XY and coordinate system : 0 scene, 1 screen
uniform vec3 UNI_SIZE;
uniform vec3 UNI_AXIS;
uniform vec2 UNI_ANCHOR;

<VARIABLE_IN>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;
vec3 normal_camera;
vec3 normal_proj;

<VARIABLE_WS>
//generated quad corners
const vec2 corners[4] = vec2[4](vec2(-1.0, 1.0), vec2(-1.0,-1.0), vec2( 1.0, 1.0), vec2( 1.0,-1.0) );
const vec2 uvs[4]     = vec2[4](vec2( 0.0, 0.0), vec2( 0.0, 1.0), vec2( 1.0, 0.0), vec2( 1.0, 1.0) );

<VARIABLE_OUT>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec3 normal_model;
vec3 normal_world;
vec3 normal_camera;
vec3 normal_proj;
vec2 uv;

<FUNCTION>

<OPERATION>
    
    if(length(UNI_AXIS) > 0.1){
        //axis aligned billboard

        vec4 pos = inData[0].position_model;

        vec3 look = normalize((inverse(MV) * vec4(normalize(inData[0].position_camera.xyz),0)).xyz);
        vec3 up = UNI_AXIS;
        vec3 right = -normalize(cross(up,look));

        for(int i=0; i<4; i++){
            vec3 corner = vec3(pos.xyz);
            vec2 offset = UNI_SIZE.xy * (corners[i]-UNI_ANCHOR) * 0.5;
            corner += right*offset.x;
            corner += up*offset.y;
            vec4 np = MVP * vec4(corner,1);

            outData.uv = uvs[i];
            outData.position_model = np; //TODO
            outData.position_world = np; //TODO
            outData.position_camera = np;
            outData.normal_model = inData[0].normal_model;
            outData.normal_world = inData[0].normal_world;
            outData.normal_camera = inData[0].normal_camera;
            outData.normal_proj = inData[0].normal_proj;

            gl_Position = np;

            EmitVertex();
        }


    } else {
        //camera aligned billboard

        vec4 pos = inData[0].position_camera;
        if(UNI_SIZE.z>0.1){
            pos = inData[0].position_proj;
            pos /= inData[0].position_proj.w;
        }

        for(int i=0; i<4; i++){
            vec4 corner = vec4(pos);
            vec2 size = UNI_SIZE.xy * (corners[i]-UNI_ANCHOR) * 0.5;
            corner.xy += size;

            outData.uv = uvs[i];
            outData.position_model = corner; //TODO
            outData.position_world = corner; //TODO
            outData.position_camera = corner;
            outData.normal_model = inData[0].normal_model;
            outData.normal_world = inData[0].normal_world;
            outData.normal_camera = inData[0].normal_camera;
            outData.normal_proj = inData[0].normal_proj;

            if(UNI_SIZE.z<0.1){
                gl_Position = P * corner;
            }else{
                gl_Position = corner;
            }

            EmitVertex();
        }

    }

    EndPrimitive();
