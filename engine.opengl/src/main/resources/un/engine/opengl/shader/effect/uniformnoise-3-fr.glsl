#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
uniform float time;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>
float random(){
    float r=time;
    if(r < 0.2) r = r +0.2;
    if(r > 0.8) r = r -0.2;
    float d = dot(gl_FragCoord.xyz, vec3( 4.125783, 5.812, 3.488) );
    return fract(r*d);
}

<OPERATION>
float noise = random();
vec4 tcolor = texture( sampler0, inData.uv).rgba;
tcolor[0] = tcolor[0] * noise;
tcolor[1] = tcolor[1] * noise;
tcolor[2] = tcolor[2] * noise;
outColor = tcolor;