#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform sampler2D $prefix_tex0;
uniform sampler2D $prefix_tex1;

<VARIABLE_IN>
vec3 $prefix_uvz;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>

    vec3 vR = normalize(inData.$prefix_uvz);
    if(vR.z>0.0){
        vec2 frontUV = (vR.xy / (2.0*(1.0 + vR.z))) + 0.5;
        $produce = $method(texture($prefix_tex0,frontUV),$produce);
    }else{
        vec2 backUV = (vR.xy / (2.0*(1.0 - vR.z))) + 0.5;
        $produce = $method(texture($prefix_tex1,backUV),$produce);
    }