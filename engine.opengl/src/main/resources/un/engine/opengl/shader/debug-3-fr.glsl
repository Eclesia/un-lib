#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
//debug fragment position
uniform vec2 DEBUG_POSITION;
//message offset
uniform int DEBUG_OFFSET;


<VARIABLE_WS>
//indicator of the value type being send to debugger
const float DEBUG_TYPE_BOOL  =  1f/255f;
const float DEBUG_TYPE_INT   =  2f/255f;
const float DEBUG_TYPE_UINT  =  3f/255f;
const float DEBUG_TYPE_FLOAT =  4f/255f;
const float DEBUG_TYPE_VEC2  =  5f/255f;
const float DEBUG_TYPE_VEC3  =  6f/255f;
const float DEBUG_TYPE_VEC4  =  7f/255f;
const float DEBUG_TYPE_MAT2  =  8f/255f;
const float DEBUG_TYPE_MAT3  =  9f/255f;
const float DEBUG_TYPE_MAT4  = 10f/255f;
const float DEBUG_TYPE_CONTINUE = 255f/255f;
//current message value send
int debug_offset = 0;
vec4 color;

<FUNCTION>
bool debugIntersect(){
    //check if fragment is in the area
    float dist = distance(gl_FragCoord.xy, DEBUG_POSITION);
    return dist<=1f ;
}

void debugPush(int value, bool firstHalf, float type){
    //write the type and part of the value
    if(firstHalf){
        float byte1 = (value>>24) & 0xFF;
        float byte2 = (value>>16) & 0xFF;
        color = vec4(type,byte1/255,byte2/255,0);
    }else{
        float byte3 = (value>>8) & 0xFF;
        float byte4 = (value>>0) & 0xFF;
        color = vec4(type,byte3/255,byte4/255,0);
    }
}

void debugPush(float value, bool firstHalf, float type){
    debugPush(floatBitsToInt(value), firstHalf, type);
}

bool debugValue(int value, float type){
    if(!debugIntersect()) return false;

    if(DEBUG_OFFSET==debug_offset){
        debugPush(value, true, type);
        return true;
    }else if(DEBUG_OFFSET==debug_offset+1){
        debugPush(value, false, type);
        return true;
    }else{
        debug_offset+=2;
        return DEBUG_OFFSET<debug_offset;
    }
}

bool debug(bool value){
    if(value){
        return debugValue(1,DEBUG_TYPE_BOOL);
    }else{
        return debugValue(0,DEBUG_TYPE_BOOL);
    }
}

bool debug(int value){
    return debugValue(value,DEBUG_TYPE_INT);
}

bool debug(float value){
    return debugValue(floatBitsToInt(value),DEBUG_TYPE_FLOAT);
}

bool debug(vec2 value){
    if(!debugIntersect()) return DEBUG_OFFSET<debug_offset;;

    int pos = DEBUG_OFFSET - debug_offset;
    switch(pos){
        case 0 : debugPush(value.x, true , DEBUG_TYPE_VEC2);     return true;
        case 1 : debugPush(value.x, false, DEBUG_TYPE_CONTINUE); return true;
        case 2 : debugPush(value.y, true , DEBUG_TYPE_CONTINUE); return true;
        case 3 : debugPush(value.y, false, DEBUG_TYPE_CONTINUE); return true;
        default: debug_offset += 4; return DEBUG_OFFSET<debug_offset;;
    }
}

bool debug(vec3 value){
    if(!debugIntersect()) return DEBUG_OFFSET<debug_offset;;

    int pos = DEBUG_OFFSET - debug_offset;
    switch(pos){
        case 0 : debugPush(value.x, true , DEBUG_TYPE_VEC3);     return true;
        case 1 : debugPush(value.x, false, DEBUG_TYPE_CONTINUE); return true;
        case 2 : debugPush(value.y, true , DEBUG_TYPE_CONTINUE); return true;
        case 3 : debugPush(value.y, false, DEBUG_TYPE_CONTINUE); return true;
        case 4 : debugPush(value.z, true , DEBUG_TYPE_CONTINUE); return true;
        case 5 : debugPush(value.z, false, DEBUG_TYPE_CONTINUE); return true;
        default: debug_offset += 6; return DEBUG_OFFSET<debug_offset;;
    }
}

bool debug(vec4 value){
    if(!debugIntersect()) return DEBUG_OFFSET<debug_offset;;

    int pos = DEBUG_OFFSET - debug_offset;
    switch(pos){
        case 0 : debugPush(value.x, true , DEBUG_TYPE_VEC4);     return true;
        case 1 : debugPush(value.x, false, DEBUG_TYPE_CONTINUE); return true;
        case 2 : debugPush(value.y, true , DEBUG_TYPE_CONTINUE); return true;
        case 3 : debugPush(value.y, false, DEBUG_TYPE_CONTINUE); return true;
        case 4 : debugPush(value.z, true , DEBUG_TYPE_CONTINUE); return true;
        case 5 : debugPush(value.z, false, DEBUG_TYPE_CONTINUE); return true;
        case 6 : debugPush(value.w, true , DEBUG_TYPE_CONTINUE); return true;
        case 7 : debugPush(value.w, false, DEBUG_TYPE_CONTINUE); return true;
        default: debug_offset += 8; return DEBUG_OFFSET<debug_offset;;
    }
}

bool debug(mat2 value){
    if(!debugIntersect()) return DEBUG_OFFSET<debug_offset;;

    int pos = DEBUG_OFFSET - debug_offset;
    switch(pos){
        case 0 : debugPush(value[0][0], true , DEBUG_TYPE_MAT2);     return true;
        case 1 : debugPush(value[0][0], false, DEBUG_TYPE_CONTINUE); return true;
        case 2 : debugPush(value[1][0], true , DEBUG_TYPE_CONTINUE); return true;
        case 3 : debugPush(value[1][0], false, DEBUG_TYPE_CONTINUE); return true;
        case 4 : debugPush(value[0][1], true , DEBUG_TYPE_CONTINUE); return true;
        case 5 : debugPush(value[0][1], false, DEBUG_TYPE_CONTINUE); return true;
        case 6 : debugPush(value[1][1], true , DEBUG_TYPE_CONTINUE); return true;
        case 7 : debugPush(value[1][1], false, DEBUG_TYPE_CONTINUE); return true;
        default: debug_offset += 8; return DEBUG_OFFSET<debug_offset;;
    }
}

bool debug(mat3 value){
    if(!debugIntersect()) return DEBUG_OFFSET<debug_offset;;

    int pos = DEBUG_OFFSET - debug_offset;
    switch(pos){
        case  0 : debugPush(value[0][0], true , DEBUG_TYPE_MAT3);     return true;
        case  1 : debugPush(value[0][0], false, DEBUG_TYPE_CONTINUE); return true;
        case  2 : debugPush(value[1][0], true , DEBUG_TYPE_CONTINUE); return true;
        case  3 : debugPush(value[1][0], false, DEBUG_TYPE_CONTINUE); return true;
        case  4 : debugPush(value[2][0], true , DEBUG_TYPE_CONTINUE); return true;
        case  5 : debugPush(value[2][0], false, DEBUG_TYPE_CONTINUE); return true;

        case  6 : debugPush(value[0][1], true , DEBUG_TYPE_CONTINUE); return true;
        case  7 : debugPush(value[0][1], false, DEBUG_TYPE_CONTINUE); return true;
        case  8 : debugPush(value[1][1], true , DEBUG_TYPE_CONTINUE); return true;
        case  9 : debugPush(value[1][1], false, DEBUG_TYPE_CONTINUE); return true;
        case 10 : debugPush(value[2][1], true , DEBUG_TYPE_CONTINUE); return true;
        case 11 : debugPush(value[2][1], false, DEBUG_TYPE_CONTINUE); return true;

        case 12 : debugPush(value[0][2], true , DEBUG_TYPE_CONTINUE); return true;
        case 13 : debugPush(value[0][2], false, DEBUG_TYPE_CONTINUE); return true;
        case 14 : debugPush(value[1][2], true , DEBUG_TYPE_CONTINUE); return true;
        case 15 : debugPush(value[1][2], false, DEBUG_TYPE_CONTINUE); return true;
        case 16 : debugPush(value[2][2], true , DEBUG_TYPE_CONTINUE); return true;
        case 17 : debugPush(value[2][2], false, DEBUG_TYPE_CONTINUE); return true;
        default: debug_offset += 18; return DEBUG_OFFSET<debug_offset;;
    }
}

bool debug(mat4 value){
    if(!debugIntersect()) return DEBUG_OFFSET<debug_offset;;

    int pos = DEBUG_OFFSET - debug_offset;
    switch(pos){
        case  0 : debugPush(value[0][0], true , DEBUG_TYPE_MAT4);     return true;
        case  1 : debugPush(value[0][0], false, DEBUG_TYPE_CONTINUE); return true;
        case  2 : debugPush(value[1][0], true , DEBUG_TYPE_CONTINUE); return true;
        case  3 : debugPush(value[1][0], false, DEBUG_TYPE_CONTINUE); return true;
        case  4 : debugPush(value[2][0], true , DEBUG_TYPE_CONTINUE); return true;
        case  5 : debugPush(value[2][0], false, DEBUG_TYPE_CONTINUE); return true;
        case  6 : debugPush(value[3][0], true , DEBUG_TYPE_CONTINUE); return true;
        case  7 : debugPush(value[3][0], false, DEBUG_TYPE_CONTINUE); return true;

        case  8 : debugPush(value[0][1], true , DEBUG_TYPE_CONTINUE); return true;
        case  9 : debugPush(value[0][1], false, DEBUG_TYPE_CONTINUE); return true;
        case 10 : debugPush(value[1][1], true , DEBUG_TYPE_CONTINUE); return true;
        case 11 : debugPush(value[1][1], false, DEBUG_TYPE_CONTINUE); return true;
        case 12 : debugPush(value[2][1], true , DEBUG_TYPE_CONTINUE); return true;
        case 13 : debugPush(value[2][1], false, DEBUG_TYPE_CONTINUE); return true;
        case 14 : debugPush(value[3][1], true , DEBUG_TYPE_CONTINUE); return true;
        case 15 : debugPush(value[3][1], false, DEBUG_TYPE_CONTINUE); return true;

        case 16 : debugPush(value[0][2], true , DEBUG_TYPE_CONTINUE); return true;
        case 17 : debugPush(value[0][2], false, DEBUG_TYPE_CONTINUE); return true;
        case 18 : debugPush(value[1][2], true , DEBUG_TYPE_CONTINUE); return true;
        case 19 : debugPush(value[1][2], false, DEBUG_TYPE_CONTINUE); return true;
        case 20 : debugPush(value[2][2], true , DEBUG_TYPE_CONTINUE); return true;
        case 21 : debugPush(value[2][2], false, DEBUG_TYPE_CONTINUE); return true;
        case 22 : debugPush(value[3][2], true , DEBUG_TYPE_CONTINUE); return true;
        case 23 : debugPush(value[3][2], false, DEBUG_TYPE_CONTINUE); return true;

        case 24 : debugPush(value[0][3], true , DEBUG_TYPE_CONTINUE); return true;
        case 25 : debugPush(value[0][3], false, DEBUG_TYPE_CONTINUE); return true;
        case 26 : debugPush(value[1][3], true , DEBUG_TYPE_CONTINUE); return true;
        case 27 : debugPush(value[1][3], false, DEBUG_TYPE_CONTINUE); return true;
        case 28 : debugPush(value[2][3], true , DEBUG_TYPE_CONTINUE); return true;
        case 29 : debugPush(value[2][3], false, DEBUG_TYPE_CONTINUE); return true;
        case 30 : debugPush(value[3][3], true , DEBUG_TYPE_CONTINUE); return true;
        case 31 : debugPush(value[3][3], false, DEBUG_TYPE_CONTINUE); return true;

        default: debug_offset += 32; return DEBUG_OFFSET<debug_offset;;
    }
}


<OPERATION>