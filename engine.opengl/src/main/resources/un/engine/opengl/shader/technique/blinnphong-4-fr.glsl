#version 330

<STRUCTURE>
struct Material {
    vec3 ambiant;
    vec3 diffuse;
    vec3 specular;
    vec3 normal;
    float alpha;
};

<LAYOUT>
out vec4 truc;
out vec4 COLOR;


<UNIFORM>
uniform mat4 PROJECTION;
uniform vec2 VIEWPORT;

uniform vec3 AMBIANT;
uniform vec4 DIFFUSE;
uniform vec3 SPECULAR;
uniform float ALPHA;
uniform float SHININESS;
uniform sampler2D AMBIANT_TEXTURE;
uniform sampler2D DIFFUSE_TEXTURE;
uniform sampler2D SPECULAR_TEXTURE;
uniform sampler2D ALPHA_TEXTURE;
uniform sampler2D NORMAL_TEXTURE;
//store mask for each used texture
uniform int TEXTURES_BITSET;

<VARIABLE_IN>
    vec3 normal;
    vec2 texcoord_0;


<VARIABLE_OUT>


<VARIABLE_WS>
Material material = Material(vec3(0.0),vec3(0.0),vec3(0.0),vec3(0.0),0.0);


<FUNCTION>


<OPERATION>

    //base values
    material.ambiant = vec3(AMBIANT);
    material.diffuse = DIFFUSE.rgb;
    material.specular = vec3(SPECULAR);
    material.normal = vec3(inData.normal);
    material.alpha = ALPHA * DIFFUSE.a;

    //apply textures
    if ((TEXTURES_BITSET & 0x01) != 0) {
        vec4 pixel = texture(AMBIANT_TEXTURE, inData.texcoord_0);
        material.ambiant *= pixel.rgb;
    }
    if ((TEXTURES_BITSET & 0x02) != 0) {
        vec4 pixel = texture(DIFFUSE_TEXTURE, inData.texcoord_0);
        material.diffuse *= pixel.rgb;
        material.alpha *= pixel.a;
    }
    if ((TEXTURES_BITSET & 0x04) != 0) {
        vec4 pixel = texture(SPECULAR_TEXTURE, inData.texcoord_0);
        material.specular *= pixel.rgb;
    }
    if ((TEXTURES_BITSET & 0x08) != 0) {
        vec4 pixel = texture(ALPHA_TEXTURE, inData.texcoord_0);
        material.alpha *= pixel.r;
    }
    if ((TEXTURES_BITSET & 0x0F) != 0) {
        vec4 pixel = texture(NORMAL_TEXTURE, inData.texcoord_0);
        material.normal *= pixel.rgb;
    }

    //vec3 rgb = computeLight();
    COLOR = vec4(material.diffuse, material.alpha);
    truc = vec4(1,0,1,1);