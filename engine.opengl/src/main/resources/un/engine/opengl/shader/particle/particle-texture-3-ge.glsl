#version 330

<STRUCTURE>

<LAYOUT>
layout(points) in;
layout(triangle_strip, max_vertices=4) out;

<UNIFORM>
uniform float globalSize;

<VARIABLE_IN>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;
vec3 normal_camera;
vec3 normal_proj;

<VARIABLE_WS>
//generated quad corners
const vec2 corners[4] = vec2[4](vec2(0.0,1.0), vec2(0.0,0.0), vec2(1.0,1.0), vec2(1.0,0.0) );

<VARIABLE_OUT>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec3 normal_model;
vec3 normal_world;
vec3 normal_camera;
vec3 normal_proj;
vec2 uv;

<FUNCTION>

<OPERATION>
    
    for(int i=0; i<4; i++){
        vec4 corner = inData[0].position_camera;
        corner.xy += globalSize * (corners[i] - vec2(0.5));

        outData.uv = corners[i];
        outData.position_model = corner; //TODO
        outData.position_world = corner; //TODO
        outData.position_camera = corner;
        outData.normal_model = inData[0].normal_model;
        outData.normal_world = inData[0].normal_world;
        outData.normal_camera = inData[0].normal_camera;
        outData.normal_proj = inData[0].normal_proj;

        gl_Position = P * corner;

        EmitVertex();
    }

    EndPrimitive();