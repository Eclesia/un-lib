#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
uniform vec4 COLOR;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>
    outColor = COLOR;