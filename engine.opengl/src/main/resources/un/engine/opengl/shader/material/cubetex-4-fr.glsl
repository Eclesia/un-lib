#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform samplerCube $prefix_tex;

<VARIABLE_IN>
vec3 $prefix_uvz;

<VARIABLE_WS>

<VARIABLE_OUT>

<FUNCTION>

<OPERATION>
    $produce = $method(texture($prefix_tex, inData.$prefix_uvz),$produce);

