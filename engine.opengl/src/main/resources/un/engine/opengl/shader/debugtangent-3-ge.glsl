#version 330

<STRUCTURE>

<LAYOUT>
layout(triangles) in;
layout(line_strip, max_vertices = 6) out;

<UNIFORM>
uniform float tangentLength = 1.0;

<VARIABLE_IN>
vec4 position_model;
vec4 tangent_model;

<VARIABLE_OUT>

<FUNCTION>

<OPERATION>

    gl_Position = MVP * inData[0].position_model;
    EmitVertex();
    gl_Position = MVP * vec4((inData[0].position_model.xyz + inData[0].tangent_model.xyz*tangentLength),1);
    EmitVertex();
    EndPrimitive();

    gl_Position = MVP * inData[1].position_model;
    EmitVertex();
    gl_Position = MVP * vec4((inData[1].position_model.xyz + inData[1].tangent_model.xyz*tangentLength),1);
    EmitVertex();
    EndPrimitive();

    gl_Position = MVP * inData[2].position_model;
    EmitVertex();
    gl_Position = MVP * vec4((inData[2].position_model.xyz + inData[2].tangent_model.xyz*tangentLength),1);
    EmitVertex();
    EndPrimitive();