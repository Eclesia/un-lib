#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 color;

<UNIFORM>
//  1 : clear
//  2 : src
//  3 : dst
//  4 : xor
//  5 : lighter
//  6 : srcover
//  7 : srcin
//  8 : srcout
//  9 : srcatop
// 10 : dstover
// 11 : dstin
// 12 : dstout
// 13 : dstatop
unitform int MODE;
unitform float ALPHA;
// 0 : alpha premultiplied colors
// 1 : not premultiplied colors
unitform int PREMUL;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>
/**
 * Porter-Duff formula for premultiplied color component.
 * co = Fa x Cs + Fb x Cb
 *
 * Where :
 * co is the output color pre-multiplied with the output alpha [0 <= co <= 1]
 * Fa is defined by the operator and controls inclusion of the source
 * Cs is the color of the source (multiplied by alpha)
 * Fb is defined by the operator and controls inclusion of the destination
 * Cb is the color of the destination (multiplied by alpha)
 */
function float porterDuffPreMul(float Fa, float Cs, float Fb, float Cb){
    return Fa * Cs + Fb * Cb;
}

/**
 * Porter-Duff formula for not premultiplied color component.
 * co = αs x Fa x Cs + αb x Fb x Cb
 *
 * Where :
 * co is the output color pre-multiplied with the output alpha [0 <= co <= 1]
 * αs is the coverage of the source
 * Fa is defined by the operator and controls inclusion of the source
 * Cs is the color of the source (not multiplied by alpha)
 * αb is the coverage of the destination
 * Fb is defined by the operator and controls inclusion of the destination
 * Cb is the color of the destination (not multiplied by alpha)
 */
function float porterDuff(float αs, float Fa, float Cs, float αb, float Fb, float Cb){
    return αs * Fa * Cs + αb * Fb * Cb;
}

function float porterDuff(float αs, float Fa, vec3 Cs, float αb, float Fb, vec3 Cb){
    return αs * Fa * Cs + αb * Fb * Cb;
}

function vec4 clear(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 clearPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 src(vec4 src, vec4 dst){
    final float αs = src[3]*alpha;
    final float αb = dst[3];
    final float αo = αs;
    final float Fa = 1;
    final float Fb = 0;
    color[0] = porterDuff(αs, Fa, src[0], αb, Fb, dst[0]);
    color[1] = porterDuff(αs, Fa, src[1], αb, Fb, dst[1]);
    color[2] = porterDuff(αs, Fa, src[2], αb, Fb, dst[2]);
    color[3] = αo;
    //result value is premultiplied, return it to non-multiplied
    if(αo!=0){
        color[0] /= αo;
        color[1] /= αo;
        color[2] /= αo;
    }
}

function vec4 srcPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 dst(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 dstPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 xor(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 xorPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 lighter(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 lighterPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 srcover(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 srcoverPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 srcin(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 srcinPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 srcout(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 srcoutPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 srcatop(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 srcatopPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 dstover(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 dstoverPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 dstin(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 dstinPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 dstout(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 dstoutPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 dstatop(vec4 src, vec4 dst){
    color = vec4(0);
}

function vec4 dstatopPreMul(vec4 src, vec4 dst){
    color = vec4(0);
}

<OPERATION>
    vec4 src = texture(sampler0,inData.uv);
    vec4 dst = texture(sampler1,inData.uv);

    if(PREMUL==0){
        switch(MODE){
            case  1 : clear(src,dst); break;
            case  2 : src(src,dst); break;
            case  3 : dst(src,dst); break;
            case  4 : xor(src,dst); break;
            case  5 : lighter(src,dst); break;
            case  6 : srcover(src,dst); break;
            case  7 : srcin(src,dst); break;
            case  8 : srcout(src,dst); break;
            case  9 : srcatop(src,dst); break;
            case 10 : dstover(src,dst); break;
            case 11 : dstin(src,dst); break;
            case 12 : dstout(src,dst); break;
            case 13 : dstatop(src,dst); break;
        }
    }else{
        switch(MODE){
            case  1 : clearPreMul(src,dst); break;
            case  2 : srcPreMul(src,dst); break;
            case  3 : dstPreMul(src,dst); break;
            case  4 : xorPreMul(src,dst); break;
            case  5 : lighterPreMul(src,dst); break;
            case  6 : srcoverPreMul(src,dst); break;
            case  7 : srcinPreMul(src,dst); break;
            case  8 : srcoutPreMul(src,dst); break;
            case  9 : srcatopPreMul(src,dst); break;
            case 10 : dstoverPreMul(src,dst); break;
            case 11 : dstinPreMul(src,dst); break;
            case 12 : dstoutPreMul(src,dst); break;
            case 13 : dstatopPreMul(src,dst); break;
        }
    }
    