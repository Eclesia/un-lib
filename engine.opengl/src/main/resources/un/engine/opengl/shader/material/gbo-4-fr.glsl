#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;
layout(location=1) out vec4 outSpecular;
layout(location=2) out vec3 outWorldPosition;
layout(location=3) out vec3 outWorldNormal;

<UNIFORM>

<VARIABLE_IN>

<VARIABLE_OUT>

<FUNCTION>

<VARIABLE_WS>

<OPERATION>
    outColor = material.color;
    outSpecular = material.specular;
    outWorldPosition = inData.position_world.xyz;
    outWorldNormal = normal_world;

