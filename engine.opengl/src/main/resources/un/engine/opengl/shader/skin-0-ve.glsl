#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>

<VARIABLE_WS>
vec3 w_position_model = vec3(0,0,0);
float w_position_factor = 1.0;

<FUNCTION>

<OPERATION>
    // morph actor may have set the position before, we don't want to override it
    w_position_model = w_position_model + l_position*w_position_factor;