#version 330

<STRUCTURE>

<LAYOUT>
layout(location = $IDX) in vec2 l_position;

<UNIFORM>
uniform mat3 MV;
uniform mat3 P;
uniform vec4 CORNERS;

<VARIABLE_OUT>
vec4 position_model;
vec4 position_proj;

<FUNCTION>

<VARIABLE_WS>
// triangles 3,0,1, 3,1,2

<OPERATION>
    vec2 pos = vec2(0,0);
         if(gl_VertexID==0) pos = vec2(CORNERS.x,CORNERS.y);
    else if(gl_VertexID==1) pos = vec2(CORNERS.z,CORNERS.y);
    else if(gl_VertexID==2) pos = vec2(CORNERS.z,CORNERS.w);
    else if(gl_VertexID==3) pos = vec2(CORNERS.x,CORNERS.w);

    outData.position_model = vec4(pos,0.0,1.0);
    vec3 test = (P*MV) * vec3(pos.x,pos.y,1.0);
    outData.position_proj = vec4(test,1);
    gl_Position = outData.position_proj;
