#version 330

<STRUCTURE>

<LAYOUT>
layout(location = 0) out vec4 outColor;

<UNIFORM>
uniform samplerCube skybox;

<VARIABLE_IN>
vec3 coord;

<FUNCTION>

<OPERATION>
     outColor = texture(skybox, inData.coord);