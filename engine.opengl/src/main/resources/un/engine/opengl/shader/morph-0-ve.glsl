#version 330

<STRUCTURE>

<LAYOUT>
layout(location = $IDX) in vec3 l_morph0;
layout(location = $IDX) in vec3 l_morph1;
layout(location = $IDX) in vec3 l_morph2;

<UNIFORM>
uniform vec3 MORPH_FACTOR;

<VARIABLE_WS>
vec3 w_position_model = vec3(0,0,0);
float w_position_factor = 1.0;

<FUNCTION>

<OPERATION>
    w_position_model = l_position;
    w_position_factor = 0.0;

    // append morph modifications
    if(MORPH_FACTOR.x>0) w_position_model += l_morph0 * MORPH_FACTOR.x;
    if(MORPH_FACTOR.y>0) w_position_model += l_morph1 * MORPH_FACTOR.y;
    if(MORPH_FACTOR.z>0) w_position_model += l_morph2 * MORPH_FACTOR.z;