#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>

<VARIABLE_WS>

<VARIABLE_IN>
flat mat4 M;

<VARIABLE_OUT>
flat mat4 M;

<VARIABLE_WS>

<OPERATION>
    M = inData.M ;
    outData.M = M ;