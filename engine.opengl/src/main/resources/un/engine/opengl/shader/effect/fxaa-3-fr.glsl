#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>
const vec3 luma = vec3(0.299, 0.587, 0.114);
const float FXAA_SPAN_MAX   = 8.0;
const float FXAA_REDUCE_MUL = 1.0/8.0;
const float FXAA_REDUCE_MIN = 1.0/128.0;
const vec2 size = vec2($width,$height);

<FUNCTION>

<OPERATION>

        vec3 rgbNW = texture(sampler0,inData.uv+(vec2(-1.0,-1.0)/size)).xyz;
        vec3 rgbNE = texture(sampler0,inData.uv+(vec2(+1.0,-1.0)/size)).xyz;
        vec3 rgbM  = texture(sampler0,inData.uv                               ).xyz;
        vec3 rgbSW = texture(sampler0,inData.uv+(vec2(-1.0,+1.0)/size)).xyz;
        vec3 rgbSE = texture(sampler0,inData.uv+(vec2(+1.0,+1.0)/size)).xyz;

        float lumaNW = dot(rgbNW, luma);
        float lumaNE = dot(rgbNE, luma);
        float lumaM  = dot(rgbM,  luma);
        float lumaSW = dot(rgbSW, luma);
        float lumaSE = dot(rgbSE, luma);

        float lumaMin = min(lumaM, min(min(lumaNW, lumaNE), min(lumaSW, lumaSE)));
        float lumaMax = max(lumaM, max(max(lumaNW, lumaNE), max(lumaSW, lumaSE)));

        vec2 dir;
        dir.x = -((lumaNW + lumaNE) - (lumaSW + lumaSE));
        dir.y =  ((lumaNW + lumaSW) - (lumaNE + lumaSE));

        float dirReduce = max((lumaNW + lumaNE + lumaSW + lumaSE) * (0.25 * FXAA_REDUCE_MUL), FXAA_REDUCE_MIN);

        float rcpDirMin = 1.0 / (min(abs(dir.x), abs(dir.y)) + dirReduce);

        dir = min(vec2( FXAA_SPAN_MAX,  FXAA_SPAN_MAX),
                  max(vec2(-FXAA_SPAN_MAX, -FXAA_SPAN_MAX),
                  dir * rcpDirMin)) / size;

        vec3 rgbA = 0.5 * (
                texture(sampler0, inData.uv.xy + dir * (1.0/3.0 - 0.5)).xyz +
                texture(sampler0, inData.uv.xy + dir * (2.0/3.0 - 0.5)).xyz);
        vec3 rgbB = rgbA * 0.5 + 0.25 * (
                texture(sampler0, inData.uv.xy + dir * (0.0/3.0 - 0.5)).xyz +
                texture(sampler0, inData.uv.xy + dir * (3.0/3.0 - 0.5)).xyz);
        float lumaB = dot(rgbB, luma);
        if((lumaB < lumaMin) || (lumaB > lumaMax)){
            outColor.xyz = rgbA;
        }else{
            outColor.xyz = rgbB;
        }
        outColor.w = 1;