#version 130

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform vec4 $prefix_value;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>
    $produce = $method($prefix_value,$produce);