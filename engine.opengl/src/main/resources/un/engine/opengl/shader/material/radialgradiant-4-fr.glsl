#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform mat3 MV;
uniform mat3 P;
uniform int NBSTEP;
uniform float GSTEPS[32];
uniform vec4 GCOLORS[32];
uniform vec2 GCENTER;
uniform float GRADIUS;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>
    //fragment distance to radial distance
    float d = length(gl_FragCoord.xy - (MV*vec3(GCENTER,1)).xy) / GRADIUS;
    d = clamp(d,0.0,1.0);

    //find interval
    material.color = vec4(0);
    for(int index=0;index<NBSTEP-1;index++){
        if(d >= GSTEPS[index] && d <= GSTEPS[index+1]){
            d = (d-GSTEPS[index]) / (GSTEPS[index+1]-GSTEPS[index]);
            vec4 c0 = GCOLORS[index];
            vec4 c1 = GCOLORS[index+1];
            material.color = mix(c0, c1, d);
            break;
        }
    }

