#version 330

<STRUCTURE>

<LAYOUT>
layout(lines_adjacency) in;
layout(triangle_strip, max_vertices = 4) out;

<UNIFORM>
uniform mat3 MV;
uniform mat3 P;
uniform float WIDTH = 1.0;

<VARIABLE_IN>
vec4 position_model;
vec4 position_proj;

<VARIABLE_OUT>

<FUNCTION>

<OPERATION>
    vec2 p0 = inData[0].position_model.xy;
    vec2 p1 = inData[1].position_model.xy;
    vec2 p2 = inData[2].position_model.xy;
    vec2 p3 = inData[3].position_model.xy;
    
    //calculate vector/direction of each segment
    vec2 v01 = normalize(p1-p0);
    vec2 v12 = normalize(p2-p1);
    vec2 v23 = normalize(p3-p2);
    //on line ends the points are the same, we fallback on the middle segment vector
    if(p0==p1) v01 = v12;
    if(p2==p3) v23 = v12;
    
    //perpendicular vector of each segment
    vec2 n01 = vec2(-v01.y, v01.x);
    vec2 n12 = vec2(-v12.y, v12.x);
    vec2 n23 = vec2(-v23.y, v23.x);

    //now calculate the average vector between segments
    vec2 a01 = normalize(n01 + n12);
    vec2 a23 = normalize(n12 + n23);

    //since the width is for the perpendicular vector
    //we need to adjust it on the average vector
    //NOTE : simplified formula from un.impl.math.Vector.project()
    float ratio01 = dot(a01, n12);
    float ratio23 = dot(a23, n12);
    vec2 offset01 = a01 * (WIDTH/2.0) / ratio01;
    vec2 offset23 = a23 * (WIDTH/2.0) / ratio23;

    //calculate final triangle strip
    mat3 mvp = (P*MV);
    gl_Position = vec4(mvp * vec3(p2 - offset23, 1),1); EmitVertex();
    gl_Position = vec4(mvp * vec3(p2 + offset23, 1),1); EmitVertex();
    gl_Position = vec4(mvp * vec3(p1 - offset01, 1),1); EmitVertex();
    gl_Position = vec4(mvp * vec3(p1 + offset01, 1),1); EmitVertex();
    EndPrimitive();
