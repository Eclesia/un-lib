#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>
const float[$size] kernel = float[$size]($vals);

<FUNCTION>


<OPERATION>
vec4 tcolor = vec4(0,0,0,0);

int k=0;
float kx;
float ky;
for(float y=-$radius; y<=$radius; y++) {
    ky = inData.uv.y + y*PX.y;
    for(float x=-$radius; x<=$radius; x++) {
        kx = inData.uv.x + x*PX.x;
        tcolor += texture(sampler0, vec2(kx, ky)) * kernel[k];
        k++;
    }
}

outColor = tcolor;