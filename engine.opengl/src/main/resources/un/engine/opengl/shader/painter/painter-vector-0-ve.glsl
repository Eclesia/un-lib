#version 330

<STRUCTURE>

<LAYOUT>
layout(location = $IDX) in vec2 l_position;

<UNIFORM>
uniform mat3 MV;
uniform mat3 P;

<VARIABLE_OUT>
vec4 position_model;
vec4 position_proj;

<FUNCTION>

<OPERATION>
    outData.position_model = vec4(l_position.x,l_position.y,0,1);
    vec3 test = (P*MV) * vec3(l_position.x,l_position.y,1.0);
    outData.position_proj = vec4(test,1);
    gl_Position = outData.position_proj;