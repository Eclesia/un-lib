#version 400

<STRUCTURE>
struct PPatch {
    float ij;
    float jk;
    float ik;
};

<LAYOUT>
layout(vertices=3) out;

<UNIFORM>
uniform float displacementResolution = 20.0;

<VARIABLE_IN>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;

<VARIABLE_OUT>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;
PPatch opatch;

<FUNCTION>
float PIi (int i, vec4 q) {
    vec4 q_minus_p = q - inData[i].position_model;
    return q[gl_InvocationID] - dot(q_minus_p.xyz, inData[i].normal_model) * inData[i].normal_model[gl_InvocationID];
}

vec2 toScreenSpace(int i1){
    vec4 ss = MVP*inData[i1].position_model;
    ss /= ss.w;
    return ss.xy /= (UNI_PX*displacementResolution);
}

float tessFactor(int i1, int i2){
    vec2 d1 = toScreenSpace(i1);
    vec2 d2 = toScreenSpace(i2);
    float d = distance(d1,d2);
    return clamp(d,1,64);
}

<OPERATION>
    outData[gl_InvocationID].position_world  = inData[gl_InvocationID].position_world;
    outData[gl_InvocationID].position_model  = inData[gl_InvocationID].position_model;
    outData[gl_InvocationID].position_camera = inData[gl_InvocationID].position_camera;
    outData[gl_InvocationID].position_proj   = inData[gl_InvocationID].position_proj;
    outData[gl_InvocationID].normal_world    = inData[gl_InvocationID].normal_world;
    outData[gl_InvocationID].normal_model    = inData[gl_InvocationID].normal_model;

    //calculate phong patch
    outData[gl_InvocationID].opatch.ij = PIi(0,inData[1].position_model) + PIi(1,inData[0].position_model);
    outData[gl_InvocationID].opatch.jk = PIi(1,inData[2].position_model) + PIi(2,inData[1].position_model);
    outData[gl_InvocationID].opatch.ik = PIi(2,inData[0].position_model) + PIi(0,inData[2].position_model);

    //tessellation factor
    // note : outer level index are for the opposite corners, so edge 0 is for corners 1,2
    gl_TessLevelOuter[0] = tessFactor(1,2);
    gl_TessLevelOuter[1] = tessFactor(2,0);
    gl_TessLevelOuter[2] = tessFactor(0,1);
    gl_TessLevelInner[0] = max(max(gl_TessLevelOuter[0],gl_TessLevelOuter[1]),gl_TessLevelOuter[2]);
