#version 400
<STRUCTURE>

<LAYOUT>

<UNIFORM>

<VARIABLE_IN>
vec4 color;
<VARIABLE_OUT>
vec4 color;

<FUNCTION>

<OPERATION>
    outData[gl_InvocationID].color = inData[gl_InvocationID].color;