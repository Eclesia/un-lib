#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform sampler2D $prefix_tex;
//spriteIndex,spacing,width,height
uniform vec4 $prefix_sprite = vec4(0,0,1,1);
uniform vec2 $prefix_scale = vec2(1.0,1.0);
uniform vec2 $prefix_offset = vec2(0.0,0.0);

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<VARIABLE_OUT>

<FUNCTION>
vec2 get$prefix_UV(vec2 uv){
    ivec2 texSize = textureSize($prefix_tex,0);
    float spriteIndex = $prefix_sprite.x;
    float spacing = $prefix_sprite.y;
    float width = $prefix_sprite.z;
    float height = $prefix_sprite.w;
    float currentCol = int(mod(spriteIndex,width));
    float currentRow = int(spriteIndex/width);
    float imgWidth = float(texSize.x);
    float imgHeight = float(texSize.y);

    float trsX = (currentCol*(width+spacing)) / imgWidth;
    float trsY = (currentRow*(height+spacing)) / imgHeight;
    float scaleX = width/imgWidth;
    float scaleY = height/imgHeight;

    return vec2((uv.x*scaleX)+trsX,(uv.y*scaleY)+trsY);
}

<OPERATION>
    vec2 $prefix_suv = get$prefix_UV(inData.uv);
    $prefix_suv -= vec2(0.5,0.5);
    $prefix_suv *= $prefix_scale;
    $prefix_suv += $prefix_offset;
    $prefix_suv += vec2(0.5,0.5);
    $produce = $method(texture($prefix_tex, $prefix_suv).rgba,$produce);
