
package science.unlicense.engine.opengl.tessellation;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.AbstractActor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.math.api.Maths;

/**
 * Subdivides the mesh triangle, using phong interpolation.
 *
 * example :
 * http://perso.telecom-paristech.fr/~boubek/papers/PhongTessellation/
 *
 * @author Johann Sorel
 */
public class PhongTessellator extends AbstractActor implements Tessellator{

    static final ShaderTemplate SHADER_TC;
    static final ShaderTemplate SHADER_TE;
    static {
        try{
            SHADER_TC = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/tessphong-1-tc.glsl"), ShaderTemplate.SHADER_TESS_CONTROL);
            SHADER_TE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/tessphong-2-te.glsl"), ShaderTemplate.SHADER_TESS_EVAL);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private int innerFactor;
    private int outterFactor;
    private boolean dirty = true;

    public PhongTessellator(int innerFactor,int outterFactor) {
        super(null);
        this.innerFactor = innerFactor;
        this.outterFactor = outterFactor;
    }

    @Override
    public int getMinGLSLVersion() {
        return Maths.max(
                SHADER_TC.getMinGLSLVersion(),
                SHADER_TE.getMinGLSLVersion());
    }

    public int getInnerFactor() {
        return innerFactor;
    }

    public void setInnerFactor(int innerFactor) {
        this.innerFactor = innerFactor;
    }

    public int getOutterFactor() {
        return outterFactor;
    }

    public void setOutterFactor(int outterFactor) {
        this.outterFactor = outterFactor;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isDirty() {
        return dirty;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean usesGeometryShader() {
        return false;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean usesTesselationShader() {
        return true;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void initProgram(final RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate tessControlShader = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate tessEvalShader = template.getTesselationEvalShaderTemplate();
        dirty = false;
        tessControlShader.append(SHADER_TC);
        tessEvalShader.append(SHADER_TE);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final Uniform infactor = program.getUniform(new Chars("innerfactor"));
        final Uniform outfactor = program.getUniform(new Chars("outterfactor"));
        infactor.setInt(gl, innerFactor);
        outfactor.setInt(gl, outterFactor);
    }

}
