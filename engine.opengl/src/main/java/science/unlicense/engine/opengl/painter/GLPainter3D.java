
package science.unlicense.engine.opengl.painter;

import science.unlicense.display.api.painter3d.AbstractPainter3D;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.GBO;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.geometry.api.Extent;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;

/**
 *
 * @author Johann Sorel
 */
public class GLPainter3D extends AbstractPainter3D {

    private GLSource source;
    private GBO gbo;
    private Texture2D texColor;
    private DefaultGLProcessContext context;

    private final GLCallback callback = new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);
                //get the result image
                texColor.loadOnSystemMemory(source.getGL());
            }

            @Override
            public void dispose(GLSource source) {
                GLEngineUtils.dispose(scene, context);
                gbo.unloadFromGpuMemory(context.getGL());
                context.dispose(source);

            }
        };

    @Override
    public Image render() {
        final Extent.Long extent = getCanvasSize();
        final int width = (int) extent.getL(0);
        final int height = (int) extent.getL(1);

        if (width < 10 || height < 10) return null;

        if (source == null || (source.getWidth() != width || source.getHeight() != height)) {
            if (source != null) {
                //dispose older version
                source.dispose();
            }

            source = GLUtilities.createOffscreenSource(width,height);
            gbo = new GBO(width, height, 0, true, false, false, false, false, false);
            texColor = (Texture2D) gbo.getDiffuseTexture();

            //render it
            context = new DefaultGLProcessContext();
            context.getPhases().add(new UpdatePhase(scene));
            context.getPhases().add(new ClearPhase(gbo,Color.TRS_BLACK.toRGBAPreMul()));
            context.getPhases().add(new DeferredRenderPhase(scene, camera, gbo));
            source.getCallbacks().add(callback);
        }

        source.render();
        return texColor.getImage();
    }

}
