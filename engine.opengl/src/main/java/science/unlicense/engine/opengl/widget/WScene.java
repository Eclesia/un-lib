

package science.unlicense.engine.opengl.widget;

import science.unlicense.common.api.logging.Loggers;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLUtilities;

/**
 * TODO a widget displaying an offscreen rendering context.
 *
 * @author Johann Sorel
 */
public class WScene extends WContainer{

    //OpenGL offscreen drawable.
    private final GLSource source = GLUtilities.createOffscreenSource(100, 100);
    private final DefaultGLProcessContext context = new DefaultGLProcessContext();
    private volatile boolean running = true;
    private AnimatorThread thread;

    public WScene() {
        source.getCallbacks().add(context);
        startAnimatorThread();
    }

    public GLProcessContext getContext() {
        return context;
    }

    private void startAnimatorThread(){
        if (thread != null) return;
        running = true;
        thread = new AnimatorThread();
        thread.start();
    }

    private void stopAnimatorThread(){
        if (thread == null) return;
        running = false;
        synchronized(thread.LOCK){
            thread.LOCK.notifyAll();
        }
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Loggers.get().log(ex, science.unlicense.common.api.logging.Logger.LEVEL_INFORMATION);
        }
        thread = null;
    }

    @Override
    public void finalize() throws Throwable{
        stopAnimatorThread();
        super.finalize();
    }

    private class AnimatorThread extends Thread{
        private final Object LOCK = new Object();
        @Override
        public void run() {
            while (running){
                source.render();
                try {
                    sleep(500);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

}
