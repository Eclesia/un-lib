

package science.unlicense.engine.opengl.technique;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorExecutor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.Actors;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.engine.opengl.technique.actor.MaterialActor;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Display the normals stored in the mesh shell.
 *
 * @author Johann Sorel
 */
public class DebugFaceNormalTechnique extends AbstractGLTechnique {

    private static final ShaderTemplate NORMAL_GE;
    static {
        try{
            NORMAL_GE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugfacenormal-3-ge.glsl"), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private float normalLength = 1.0f;
    private ActorProgram program;
    private Uniform uniLength;

    public DebugFaceNormalTechnique() {
    }

    public DebugFaceNormalTechnique(float normalLength) {
        this.normalLength = normalLength;
    }

    public float getNormalLength() {
        return normalLength;
    }

    public void setNormalLength(float normalLength) {
        this.normalLength = normalLength;
    }

    @Override
    public void renderInternal(RenderContext context, MonoCamera camera, SceneNode node) {

        final Model model = (Model) node;

        if (program == null) {
            program = new ActorProgram();

            //build material actor
            final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
            material.setDiffuse(Color.RED);
            final MaterialActor materialActor = new MaterialActor(material,false);

            //build shell actor
            final Geometry shape = model.getShape();
            final ActorExecutor shellActor = Actors.buildExecutor(model, shape);
            program.setExecutor(shellActor);

            final DefaultActor defaultActor = new DefaultActor(new Chars("DebugFaceNormal"),false,
                    null, null, null, NORMAL_GE, null, true, false){
                    @Override
                    public void preDrawGL(RenderContext context, ActorProgram program) {
                        super.preDrawGL(context, program);
                        uniLength.setFloat(context.getGL().asGL2ES2(), normalLength);
                    }
                };

            program.getActors().add(shellActor);
            program.getActors().add(defaultActor);
            program.getActors().add(materialActor);
            program.compile(context);
            uniLength = program.getUniform(new Chars("normalLength"));
        }

        program.render(context, camera, model);
    }

    @Override
    public void dispose(GLProcessContext context) {
        if (program!=null){
            program.releaseProgram(context);
        }
        uniLength = null;
    }

}
