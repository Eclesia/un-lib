
package science.unlicense.engine.opengl.phase;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Properties;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.opengl.GLExecutable;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.GLResourceManager;
import science.unlicense.engine.opengl.GLStatistics;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GLSource;

/**
 * Rendering Context, link a GLScene, a Camera and timing.
 * The rendering process is composed of a sequence of Phase, allowing
 * some preprocessing or effects.
 *
 * @author Johann Sorel
 */
public class RenderContext extends Properties implements GLProcessContext {

    private GLProcessContext glcontext;
    private SceneNode scene;

    public RenderContext(GLProcessContext glcontext, SceneNode scene) {
        this.glcontext = glcontext;
        this.scene = scene;
    }

    public void setProcessContext(GLProcessContext ctx){
        this.glcontext = ctx;
    }

    @Override
    public Rectangle getGLRectangle() {
        return glcontext.getGLRectangle();
    }

    @Override
    public Rectangle getViewRectangle() {
        return glcontext.getViewRectangle();
    }

    public SceneNode getScene() {
        return scene;
    }

    public void setScene(SceneNode scene) {
        this.scene = scene;
    }

    @Override
    public long getTimeNano() {
        return glcontext.getTimeNano();
    }

    @Override
    public long getDiffTimeNano() {
        return glcontext.getDiffTimeNano();
    }

    @Override
    public float getTimeSecond() {
        return glcontext.getTimeSecond();
    }

    @Override
    public float getDiffTimeSecond() {
        return glcontext.getDiffTimeSecond();
    }

    @Override
    public GLSource getSource() {
        return glcontext.getSource();
    }

    @Override
    public GL getGL() {
        return glcontext.getGL();
    }

    @Override
    public void addTask(GLExecutable executable) {
        glcontext.addTask(executable);
    }

    @Override
    public Sequence getPhases() {
        return glcontext.getPhases();
    }

    @Override
    public GLResourceManager getResourceManager() {
        return glcontext.getResourceManager();
    }

    @Override
    public GLStatistics getStatistics() {
        return glcontext.getStatistics();
    }

}
