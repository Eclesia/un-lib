

package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.AbstractActor;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture;
import science.unlicense.image.impl.process.ConvolutionMatrix;
import science.unlicense.math.api.Maths;

/**
 * Fast Gaussian Blur.
 * Use a 2 pass technic, first horizontal then vertical.
 * This approach is used in many engines and is well documented on the net.
 *
 * Some useful resources :
 * http://www.gamerendering.com/2008/10/11/gaussian-blur-filter-shader/
 * http://xissburg.com/faster-gaussian-blur-in-glsl/
 *
 *
 * @author Johann Sorel
 */
public class FastGaussianBlurPhase extends AbstractTexturePhase {

    private static final ShaderTemplate SHADER_VE;
    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/fastgaussian-1-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/fastgaussian-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final FGActor actor = new FGActor();
    private float[] gaussianValues;
    private final boolean horizontal;

    /**
     *
     * @param texture
     * @param gaussianValues
     * @param horizontal , true for horizontal blur, false for vertical
     */
    public FastGaussianBlurPhase(Texture texture, float[] gaussianValues, boolean horizontal) {
        this(null,texture,gaussianValues,horizontal);
    }

    /**
     *
     * @param texture
     * @param gaussmatrix
     * @param horizontal , true for horizontal blur, false for vertical
     */
    public FastGaussianBlurPhase(Texture texture, ConvolutionMatrix gaussmatrix, boolean horizontal) {
        this(null,texture,toRamp(gaussmatrix),horizontal);
    }

    /**
     *
     * @param output
     * @param texture
     * @param gaussmatrix, the middle raw will be normalized and used as gaussian values
     * @param horizontal , true for horizontal blur, false for vertical
     */
    public FastGaussianBlurPhase(FBO output,Texture texture, ConvolutionMatrix gaussmatrix, boolean horizontal) {
        this(output,texture,toRamp(gaussmatrix),horizontal);
    }

    /**
     *
     * @param output
     * @param texture
     * @param gaussianValues, the middle raw will be normalized and used as gaussian values
     * @param horizontal, true for horizontal blur, false for vertical
     */
    public FastGaussianBlurPhase(FBO output, Texture texture, float[] gaussianValues, boolean horizontal) {
        super(output,texture);
        this.gaussianValues = gaussianValues;
        this.horizontal = horizontal;
    }

    public void setGaussianValues(ConvolutionMatrix gaussmatrix) {
        setGaussianValues(toRamp(gaussmatrix));
    }

    public void setGaussianValues(float[] gaussianValues) {
        this.gaussianValues = gaussianValues;
    }

    private static float[] toRamp(ConvolutionMatrix matrix){
        final double[] middleRow = matrix.getRow(matrix.getNbRow()/2);
        final float[] ramp = new float[middleRow.length];

        //normalize ramp, sum must equal 1
        double scale = 1.0/Maths.sum(middleRow);

        for (int i=0;i<ramp.length;i++){
            ramp[i] = (float) (middleRow[i] * scale);
        }

        return ramp;
    }

    @Override
    protected FGActor getActor() {
        return actor;
    }

    private final class FGActor extends AbstractActor{

        public FGActor() {
            super(null);
        }

        @Override
        public int getMinGLSLVersion() {
            return Maths.max(SHADER_VE.getMinGLSLVersion(),
                             SHADER_FR.getMinGLSLVersion());
        }

        @Override
        public void initProgram(RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
            super.initProgram(ctx, template, tess, geom);
            final ShaderTemplate vertexShader = template.getVertexShaderTemplate();
            final ShaderTemplate fragmentShader = template.getFragmentShaderTemplate();

            vertexShader.append(SHADER_VE);
            vertexShader.replaceTexts(new Chars("$size"), Int32.encode(gaussianValues.length));
            fragmentShader.append(SHADER_FR);
            fragmentShader.replaceTexts(new Chars("$size"), Int32.encode(gaussianValues.length));

            for (int i=0;i<gaussianValues.length;i++){
                int offset = i-(gaussianValues.length/2);
                if (horizontal){
                    vertexShader.addOperation(new Chars("outData.uvs["+i+"] = outData.uv + vec2("+offset+"*PX.x, 0.0);"));
                } else {
                    vertexShader.addOperation(new Chars("outData.uvs["+i+"] = outData.uv + vec2(0.0,"+offset+"*PX.y);"));
                }
                fragmentShader.addOperation(new Chars("outColor = texture2D(sampler0, inData.uvs["+i+"])*"+gaussianValues[i]+";"));
            }
        }
    }
}
