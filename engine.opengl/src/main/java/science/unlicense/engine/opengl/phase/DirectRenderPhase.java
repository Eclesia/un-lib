
package science.unlicense.engine.opengl.phase;

import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.gpu.impl.opengl.resource.FBO;

/**
 * Render to default output.
 *
 * Output variables :
 * - glColor = Diffuse color
 *
 * @author Johann Sorel
 */
public class DirectRenderPhase extends RenderPhase {

    public DirectRenderPhase(GraphicNode root, MonoCamera camera) {
        super(root, camera);
    }

    public DirectRenderPhase(GraphicNode root, MonoCamera camera, FBO fbo) {
        super(root, camera, fbo);
    }

}
