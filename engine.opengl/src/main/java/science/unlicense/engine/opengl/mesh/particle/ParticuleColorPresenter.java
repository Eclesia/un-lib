

package science.unlicense.engine.opengl.mesh.particle;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.Actor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;

/**
 *
 * @author Johann Sorel
 */
public class ParticuleColorPresenter implements ParticulePresenter{

    private int size;

    public ParticuleColorPresenter() {
        this(1);
    }

    public ParticuleColorPresenter(int particuleSize) {
        this.size = particuleSize;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public Actor createActor() {
        try {
            return new ColorActor();
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    private class ColorActor extends DefaultActor {

        public ColorActor() throws IOException {
            super(new Chars("particuleColor"),false, (ShaderTemplate) null, null, null, null, null,
                    true, true);
        }

        @Override
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            //change point size
            context.getGL().asGL1().glPointSize(size);
        }

    }

}
