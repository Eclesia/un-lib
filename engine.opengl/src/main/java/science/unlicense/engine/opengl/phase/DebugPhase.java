
package science.unlicense.engine.opengl.phase;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.api.opengl.GL2ES2;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.math.api.Tuple;

/**
 * GLSL Shader debugger.
 * Catches informations send by fragment shader at given mouse coordinate.
 *
 * Use like this in the shader :
 * if (debug(value)) return;
 *
 *  Add this in a fragment shader to check debugging is active
 *  if (debug(false)) return;
 *  if (debug(true)) return;
 *  if (debug(21)) return;
 *  if (debug(-48.94e21f)) return;
 *  if (debug(vec2(-15.7,56.1))) return;
 *  if (debug(vec3(14.5,87.1,-2.566))) return;
 *  if (debug(vec4(5.56,6.78,8.21,-63.58))) return;
 *  if (debug(mat2(5.56,6.78,8.21,-63.58))) return;
 *  if (debug(mat3(1,2,3,1,2,3,1,2,3))) return;
 *  if (debug(mat4(1,2,3,4, 1,2,3,4, 1,2,3,4, 1,2,3,4))) return;
 *
 * @author Johann Sorel
 */
public class DebugPhase extends AbstractPhase{

    public static final boolean ACTIVE = false;
    public static final DebugPhase INSTANCE = new DebugPhase();

    public static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debug-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private static final int VALUE_STAT_PART1 = 0;
    private static final int VALUE_STAT_PART2 = 1;

    private static final int VALUE_TYPE_BOOL = 1;
    private static final int VALUE_TYPE_INT = 2;
    private static final int VALUE_TYPE_UINT = 3;
    private static final int VALUE_TYPE_FLOAT = 4;
    private static final int VALUE_TYPE_VEC2 = 5;
    private static final int VALUE_TYPE_VEC3 = 6;
    private static final int VALUE_TYPE_VEC4 = 7;
    private static final int VALUE_TYPE_MAT2 = 8;
    private static final int VALUE_TYPE_MAT3 = 9;
    private static final int VALUE_TYPE_MAT4 = 10;
    private static final int VALUE_TYPE_CONTINUE = 255;

    //image info
    private int width;
    private int height;

    //request
    private boolean debug = false;
    private int counter;
    private int pixelX;
    private int pixelY;
    private final CharBuffer message = new CharBuffer();
    private int valuetype;
    private final byte[] value = new byte[64];
    private int valuecomponents = 0;
    private int valueoffset = 0;

    private DebugPhase(){}

    public void initGL(final RenderContext ctx,
            final ShaderTemplate vertexShader,
            final ShaderTemplate tessControlShader,
            final ShaderTemplate tessEvalShader,
            final ShaderTemplate geometryShader,
            final ShaderTemplate fragmentShader) {

        fragmentShader.append(SHADER_FR);
    }

    public void preDisplayGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final Uniform unfiformPosition = program.getUniform(new Chars("DEBUG_POSITION"));
        final Uniform unfiformOffset = program.getUniform(new Chars("DEBUG_OFFSET"));
        unfiformPosition.setVec2(gl, new float[]{pixelX,pixelY});
        unfiformPosition.setInt(gl, counter);
    }

    public void postDisplayGL(RenderContext context, ActorProgram program) {
    }

    @Override
    public void processInt(GLProcessContext context) throws GLException {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        if (debug){
            final Buffer.Int8 buffer = gl.getBufferFactory().createInt8(4);
            gl.glReadPixels(pixelX, pixelY, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
            final byte[] color = buffer.getBuffer().toByteArray();

            final int valuestat = counter%2;
            if (valuestat==VALUE_STAT_PART1){
                if (!(valueoffset<valuecomponents)){
                    //new value start
                    if (valuetype!=-1){
                        //push previous value in the buffer
                        pushValue();
                        valuetype = -1;
                    }
                    valuetype = color[0];
                    valueoffset=0;

                    if (valuetype<1 || valuetype>10){
                        //we have finish
                        System.out.println("MESSAGE:\n" + message.toChars().toString());
                        debug = false;
                        return;
                    }

                    if (valuetype==VALUE_TYPE_BOOL)      valuecomponents = 1;
                    else if (valuetype==VALUE_TYPE_INT)  valuecomponents = 1;
                    else if (valuetype==VALUE_TYPE_UINT) valuecomponents = 1;
                    else if (valuetype==VALUE_TYPE_FLOAT) valuecomponents = 1;
                    else if (valuetype==VALUE_TYPE_VEC2) valuecomponents = 2;
                    else if (valuetype==VALUE_TYPE_VEC3) valuecomponents = 3;
                    else if (valuetype==VALUE_TYPE_VEC4) valuecomponents = 4;
                    else if (valuetype==VALUE_TYPE_MAT2) valuecomponents = 4;
                    else if (valuetype==VALUE_TYPE_MAT3) valuecomponents = 9;
                    else if (valuetype==VALUE_TYPE_MAT4) valuecomponents = 16;
                }

                value[valueoffset*4+0] = color[1];
                value[valueoffset*4+1] = color[2];


            } else if (valuestat==VALUE_STAT_PART2){
                value[valueoffset*4+2] = color[1];
                value[valueoffset*4+3] = color[2];
                valueoffset++;
            }

            counter++;
        }
    }

    private void pushValue(){
        if (valuetype==VALUE_TYPE_BOOL){
            final boolean v = Endianness.BIG_ENDIAN.readBoolean(value[3]);
            message.append("bool : ");
            message.append(String.valueOf(v));
            message.append("\n");
        } else if (valuetype==VALUE_TYPE_INT){
            final int v = Endianness.BIG_ENDIAN.readInt(value, 0);
            message.append("int  : ");
            message.append(String.valueOf(v));
            message.append("\n");
        } else if (valuetype==VALUE_TYPE_UINT){
            final long v = Endianness.BIG_ENDIAN.readUInt(value, 0);
            message.append("uint : ");
            message.append(String.valueOf(v));
            message.append("\n");
        } else if (valuetype==VALUE_TYPE_FLOAT){
            final float v = Endianness.BIG_ENDIAN.readFloat(value, 0);
            message.append("float: ");
            message.append(String.valueOf(v));
            message.append("\n");
        } else if (valuetype==VALUE_TYPE_VEC2){
            final float v1 = Endianness.BIG_ENDIAN.readFloat(value, 0);
            final float v2 = Endianness.BIG_ENDIAN.readFloat(value, 4);
            message.append("vec2 : ");
            message.append(block(v1, v2));
            message.append("\n");
        } else if (valuetype==VALUE_TYPE_VEC3){
            final float v1 = Endianness.BIG_ENDIAN.readFloat(value, 0);
            final float v2 = Endianness.BIG_ENDIAN.readFloat(value, 4);
            final float v3 = Endianness.BIG_ENDIAN.readFloat(value, 8);
            message.append("vec3 : ");
            message.append(block(v1, v2, v3));
            message.append("\n");
        } else if (valuetype==VALUE_TYPE_VEC4){
            final float v1 = Endianness.BIG_ENDIAN.readFloat(value, 0);
            final float v2 = Endianness.BIG_ENDIAN.readFloat(value, 4);
            final float v3 = Endianness.BIG_ENDIAN.readFloat(value, 8);
            final float v4 = Endianness.BIG_ENDIAN.readFloat(value, 12);
            message.append("vec4 : ");
            message.append(block(v1, v2, v3, v4));
            message.append("\n");
        } else if (valuetype==VALUE_TYPE_MAT2){
            final float v1 = Endianness.BIG_ENDIAN.readFloat(value, 0);
            final float v2 = Endianness.BIG_ENDIAN.readFloat(value, 4);
            final float v3 = Endianness.BIG_ENDIAN.readFloat(value, 8);
            final float v4 = Endianness.BIG_ENDIAN.readFloat(value, 12);
            message.append("mat2 : ");
            message.append(block(v1, v2)).append(",");
            message.append(block(v3, v4)).append("\n");
        } else if (valuetype==VALUE_TYPE_MAT3){
            final float v1 = Endianness.BIG_ENDIAN.readFloat(value, 0);
            final float v2 = Endianness.BIG_ENDIAN.readFloat(value, 4);
            final float v3 = Endianness.BIG_ENDIAN.readFloat(value, 8);
            final float v4 = Endianness.BIG_ENDIAN.readFloat(value, 12);
            final float v5 = Endianness.BIG_ENDIAN.readFloat(value, 16);
            final float v6 = Endianness.BIG_ENDIAN.readFloat(value, 20);
            final float v7 = Endianness.BIG_ENDIAN.readFloat(value, 24);
            final float v8 = Endianness.BIG_ENDIAN.readFloat(value, 28);
            final float v9 = Endianness.BIG_ENDIAN.readFloat(value, 32);
            message.append("mat3 : ");
            message.append(block(v1, v2, v3)).append(",");
            message.append(block(v4, v5, v6)).append(",");
            message.append(block(v7, v8, v9)).append("\n");
        } else if (valuetype==VALUE_TYPE_MAT4){
            final float v1 = Endianness.BIG_ENDIAN.readFloat(value, 0);
            final float v2 = Endianness.BIG_ENDIAN.readFloat(value, 4);
            final float v3 = Endianness.BIG_ENDIAN.readFloat(value, 8);
            final float v4 = Endianness.BIG_ENDIAN.readFloat(value, 12);
            final float v5 = Endianness.BIG_ENDIAN.readFloat(value, 16);
            final float v6 = Endianness.BIG_ENDIAN.readFloat(value, 20);
            final float v7 = Endianness.BIG_ENDIAN.readFloat(value, 24);
            final float v8 = Endianness.BIG_ENDIAN.readFloat(value, 28);
            final float v9 = Endianness.BIG_ENDIAN.readFloat(value, 32);
            final float v10 = Endianness.BIG_ENDIAN.readFloat(value, 36);
            final float v11 = Endianness.BIG_ENDIAN.readFloat(value, 40);
            final float v12 = Endianness.BIG_ENDIAN.readFloat(value, 44);
            final float v13 = Endianness.BIG_ENDIAN.readFloat(value, 48);
            final float v14 = Endianness.BIG_ENDIAN.readFloat(value, 52);
            final float v15 = Endianness.BIG_ENDIAN.readFloat(value, 56);
            final float v16 = Endianness.BIG_ENDIAN.readFloat(value, 60);
            message.append("mat4 : ");
            message.append(block(v1, v2, v3, v4)).append(",");
            message.append(block(v5, v6, v7, v8)).append(",");
            message.append(block(v9, v10,v11,v12)).append(",");
            message.append(block(v13,v14,v15,v16)).append("\n");
        }
    }

    private String block(float v1, float v2){
        return "["+v1+","+v2+"]";
    }

    private String block(float v1, float v2, float v3){
        return "["+v1+","+v2+","+v3+"]";
    }

    private String block(float v1, float v2, float v3, float v4){
        return "["+v1+","+v2+","+v3+","+v4+"]";
    }


    public void debugAt(Tuple position){
        this.valuetype = -1;
        this.counter = 0;
        this.pixelX = (int) position.get(0);
        this.pixelY = height - (int) position.get(1);
        this.debug = true;
        this.message.reset();
        this.valuecomponents = 0;
        this.valueoffset = 0;

    }

}
