
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;

/**
 * TODO
 *
 * @author Johann Sorel
 */
public class SpecularActor extends AbstractActor{

    public SpecularActor() {
        super(new Chars("SpecularMapping"));
    }

    @Override
    public int getMinGLSLVersion() {
        throw new UnimplementedException("Not supported yet.");
    }

}
