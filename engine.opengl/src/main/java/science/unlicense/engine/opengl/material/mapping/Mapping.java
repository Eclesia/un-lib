
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.engine.opengl.GLProcessContext;

/**
 *
 * @author Johann Sorel
 */
public interface Mapping {

    public boolean isDirty();

    public void dispose(GLProcessContext context);

}
