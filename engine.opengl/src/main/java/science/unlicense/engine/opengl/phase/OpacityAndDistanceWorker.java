
package science.unlicense.engine.opengl.phase;

import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.impl.FlattenVisitor;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Sort objects by opacity and distance.
 *
 * Order produces :
 * - Opaque close distance
 * - Opaque far distance
 * - Translucent far distance
 * - Translucent close distance
 *
 * @author Johann Sorel
 */
public class OpacityAndDistanceWorker {

    private final GraphicNode origin;
    private final FlattenVisitor flattener = new FlattenVisitor();
    private final Sequence opaques = new ArraySequence();
    private final Sequence translucents = new ArraySequence();
    private final Dictionary distances = new HashDictionary();
    private final Sequence sorted = new ArraySequence();

    private final Sorter distanceSort = new Sorter() {
            @Override
            public int sort(Object first, Object second) {
                Double d1 = (Double) distances.getValue(first);
                Double d2 = (Double) distances.getValue(second);
                if (d1==null || d1.isNaN()) return -1;
                if (d2==null || d2.isNaN()) return -1;
                if (d1.doubleValue() < d2.doubleValue()){
                    return -1;
                } else if (d1.doubleValue() > d2.doubleValue()){
                    return +1;
                } else {
                    return 0;
                }
            }
        };

    private final Point originPoint;

    public OpacityAndDistanceWorker(GraphicNode origin) {
        this.origin = origin;

        final Affine m = origin.getNodeToRootSpace();
        final VectorRW p = new Vector3f64(0, 0, 0);
        originPoint = new DefaultPoint(m.transform(p,p));
    }

    public Sequence prepare(Sequence objects){
        opaques.removeAll();
        translucents.removeAll();
        flattener.reset();
        distances.removeAll();
        sorted.removeAll();

        final Iterator ite = objects.createIterator();
        while (ite.hasNext()){
            final Object obj = ite.next();
            double dist = Double.NaN;
            if (obj instanceof Model){
                final Model model = (Model) obj;
                final BBox bbox = model.getShape().getBoundingBox();
                if (bbox != null){
                    final Affine m = model.getNodeToRootSpace();
                    final TupleRW wl = m.transform(bbox.getLower(),null);
                    final TupleRW wu = m.transform(bbox.getUpper(),null);
                    final BBox wbbox = new BBox(wl, wl.copy());
                    wbbox.expand(wu);
                    try {
                        dist = new DistanceOp(wbbox,originPoint).execute();
                    } catch (OperationException ex) {
                        ex.printStackTrace();
                    }
                }

                if (SimpleBlinnPhong.view((Material) model.getMaterials().get(0)).isOpaque()){
                    opaques.add(model);
                } else {
                    translucents.add(model);
                }
                distances.add(obj,dist);
            }
        }

        Collections.sort(opaques,distanceSort);
        Collections.sort(translucents,distanceSort);

        final Sequence inv = Collections.reverse(translucents);
        sorted.addAll(opaques);
        sorted.addAll(inv);

        return sorted;
    }



    public Sequence prepare(Node scene){
        opaques.removeAll();
        translucents.removeAll();
        flattener.reset();
        distances.removeAll();


        //flatten the scene tree and remove nodes marked unvisible.
        flattener.visit(scene, null);
        final Sequence result = flattener.getCollection();

        //separate opaque and translucent elements
        for (int i=0,n=result.getSize();i<n;i++){
            Node node = (Node) result.get(i);
            if (node instanceof Model && !SimpleBlinnPhong.view((Material) ((Model) node).getMaterials().get(0)).isOpaque()){
                translucents.add(node);
            } else {
                opaques.add(node);
            }

            //calculate distance, used later
            if (node instanceof Model){
                final BBox bbox = ((Model) node).getShape().getBoundingBox();
                //TODO must handle camera to node transform
            }

        }

        //sort translucent by distance
        //TODO


        return result;
    }


    public int sort(Object first, Object second) {

        if (!(first instanceof Model)) return +1;
        if (!(second instanceof Model)) return -1;

        final boolean m1 = SimpleBlinnPhong.view((Material) ((Model) first).getMaterials().get(0)).isOpaque();
        final boolean m2 = SimpleBlinnPhong.view((Material) ((Model) second).getMaterials().get(0)).isOpaque();

        if (m1 && m2){
            //sort by distance
            final double distance1 = distance((Model) first);
            final double distance2 = distance((Model) second);
            if (distance1<distance2){
                return -1;
            } else if (distance1>distance2){
                return +1;
            } else {
                return 0;
            }
        } else if (m1){
            return -1;
        } else if (m2){
            return +1;
        } else {
            //sort by inverse distance
            final double distance1 = distance((Model) first);
            final double distance2 = distance((Model) second);
            if (distance1>distance2){
                return -1;
            } else if (distance1<distance2){
                return +1;
            } else {
                return 0;
            }
        }
    }

    private double distance(Model mesh){
        //TODO merge oriented geometry rotation+scale with NodeTransform
        //Use neaest points to find if mesh is in front or behind camera
        return 0;
    }

}
