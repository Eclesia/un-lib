
package science.unlicense.engine.opengl.phase;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.Camera;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.gpu.api.GLException;

/**
 * This phase loops on all nodes calling the updaters.
 * Also updates the camera.
 *
 * it should be called first in the rendering phases.
 *
 * @author Johann Sorel
 */
public class UpdatePhase extends AbstractPhase{

    private final NodeVisitor CAMERA_VISITOR = new NodeVisitor(){
        @Override
        public Object visit(final Node node, final Object context) {
            if (node instanceof Camera && node instanceof GraphicNode){
                updateNode((RenderContext) context, (GraphicNode) node);
            }
            return super.visit(node, context);
        }
    };
    private final NodeVisitor NODE_VISITOR = new NodeVisitor(){
        @Override
        public Object visit(final Node node, final Object context) {
            if (node instanceof GraphicNode){
                updateNode((RenderContext) context, (GraphicNode) node);
            }
            return super.visit(node, context);
        }
    };

    private final RenderContext renderContext = new RenderContext(null, null);

    public UpdatePhase(SceneNode root) {
        this(Chars.EMPTY,root);
    }

    public UpdatePhase(Chars id, SceneNode root) {
        super(id);
        this.renderContext.setScene(root);
    }

    public SceneNode getScene(){
        return renderContext.getScene();
    }

    public void setScene(SceneNode scene){
        renderContext.setScene(scene);
    }

    @Override
    public void processInt(GLProcessContext context) throws GLException {
        renderContext.setProcessContext(context);

        //make a first pass to update the cameras only in case the projection changes
        CAMERA_VISITOR.visit(renderContext.getScene(), renderContext);

        //then update the scene nodes
        NODE_VISITOR.visit(renderContext.getScene(), renderContext);
    }

    protected void updateNode(final RenderContext context, final GraphicNode node){
        final Iterator iteup = node.getUpdaters().createIterator();
        while (iteup.hasNext()){
            final Updater up = (Updater) iteup.next();
            if (up==null) continue;
            up.update(context);
        }
    }
}
