
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.mapping.VolumetricScreenSpaceMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GL2ES2;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.shader.Uniform;

/**
 *
 * @author Johann Sorel
 */
public class VolumetricScreenSpaceActor extends AbstractMaterialValueActor{

    private static final ShaderTemplate DIFFUSE_TEXTURE_FR;
    static {
        try{
            DIFFUSE_TEXTURE_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/volumetricscreenspace-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }


    //GL loaded informations
    private final VolumetricScreenSpaceMapping mapping;
    private int[] reservedTexture;
    private Uniform unitTexture;

    public VolumetricScreenSpaceActor(Chars produce, Chars method, Chars uniquePrefix, VolumetricScreenSpaceMapping mapping) {
        super(new Chars("VolumetricScreenSpace"),false,produce, method, uniquePrefix,
                null,null,null,null,DIFFUSE_TEXTURE_FR);
        this.mapping = mapping;
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final Texture2D texture = mapping.getTexture();
        //load texture, image may have changed, no effect if already loaded
        texture.loadOnGpuMemory(gl);

        // Bind textures
        if (unitTexture == null){
            unitTexture = program.getUniform(uniquePrefix.concat(new Chars("tex")));
        }
        reservedTexture = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTexture[0]);
        texture.bind(gl);
        unitTexture.setInt(gl, reservedTexture[1]);

    }

    @Override
    public void postDrawGL(RenderContext context, ActorProgram program) {
        super.postDrawGL(context, program);
        final GL gl = context.getGL();
        GLUtilities.checkGLErrorsFail(gl);

        // unbind textures
        gl.asGL1().glActiveTexture(reservedTexture[0]);
        gl.asGL1().glBindTexture(GL_TEXTURE_2D, 0);
        context.getResourceManager().releaseTextureId(reservedTexture[0]);
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        unitTexture = null;
    }

}
