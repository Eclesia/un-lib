
package science.unlicense.engine.opengl.phase;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.FlattenVisitor;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.technique.GLBlinnPhongTechnique;
import science.unlicense.engine.opengl.technique.GLTechnique;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GL1;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.SubSceneTechnique;
import science.unlicense.model3d.impl.technique.Technique;

/**
 * Rendering phase.
 *
 * Render on the default frame buffer or in FBO.
 *
 * @author Johann Sorel
 */
public class RenderPhase extends AbstractFboPhase{

    private final FlattenVisitor flattener = new FlattenVisitor();
    private final RenderContext renderContext = new RenderContext(null, null);
    private MonoCamera camera;

    private boolean useBlending = false;

    private final GLBlinnPhongTechnique blinnPhongTech = new GLBlinnPhongTechnique();

    public RenderPhase(SceneNode root, MonoCamera camera) {
        this(root,camera,null);
    }

     /**
     * Default defered rendering, only output the diffuse color in fbo.
     * @param camera
     * @param root
     * @param fbo
     */
    public RenderPhase(SceneNode root, MonoCamera camera, FBO fbo) {
        super(fbo);
        this.renderContext.setScene(root);
        this.camera = camera;
    }

    public void setUseBlending(boolean useBlending) {
        this.useBlending = useBlending;
    }

    public boolean isUseBlending() {
        return useBlending;
    }

    public MonoCamera getCamera() {
        return camera;
    }

    public void setCamera(MonoCamera camera) {
        this.camera = camera;
    }

    public SceneNode getRoot() {
        return renderContext.getScene();
    }

    public void setRoot(SceneNode root) {
        renderContext.setScene(root);
    }

    @Override
    protected void processInternal(GLProcessContext glctx) throws GLException {

        renderContext.setProcessContext(glctx);

        //update the camera
        final FBO outFbo = getOutputFbo();
        if (outFbo!=null){
            update(camera,renderContext, glctx.getTimeNano(), new Rectangle(0, 0, outFbo.getWidth(), outFbo.getHeight()));
        } else {
            update(camera,renderContext, glctx.getTimeNano(), renderContext.getViewRectangle());
        }


        final GL1 gl = glctx.getGL().asGL1();
        gl.glSampleCoverage(1.f, true);


        if (useBlending){
            GLUtilities.configureBlending(gl, AlphaBlending.DEFAULT);
        } else {
            //we must disable blending if we are doing a GBO rendering
            gl.glDisable(GL_BLEND);
        }

        process(renderContext.getScene());
    }

    protected void process(Node node) {

        //find all nodes, sort them, opaque first, translucent after to reduce artifacts
        flattener.reset();

        flattener.visit(node, renderContext);
        Sequence nodes = new ArraySequence(flattener.getCollection());
//        Collections.sort(nodes, OPACITY_SORTER);
        //TODO : this works but is really slow
//        final OpacityAndDistanceWorker w = new OpacityAndDistanceWorker(camera);
//        nodes = w.prepare(nodes);

        //do the rendering
        for (int i=0,n=nodes.getSize();i<n;i++){
            processTechniques((GraphicNode) nodes.get(i));
        }
    }

    protected void processTechniques(GraphicNode glNode){
        final Sequence techniques = glNode.getTechniques();
        for (int i=0,n=techniques.getSize();i<n;i++) {
            final Technique technique = (Technique) techniques.get(i);
            if (technique instanceof GLTechnique) {
                final GLTechnique gltech = (GLTechnique) technique;
                if (gltech.getPhasePredicate().evaluate(this)) {
                    gltech.render(renderContext, camera, glNode);
                }
            } else if (technique instanceof SimpleBlinnPhong) {
                blinnPhongTech.setBlinnPhong((SimpleBlinnPhong) technique);
                blinnPhongTech.render(renderContext, camera, glNode);
            } else if (technique instanceof SubSceneTechnique) {
                //TODO cache subscene
//                final SubSceneTechnique sstech = (SubSceneTechnique) technique;
//                final SceneNode subScene = sstech.derivateScene(glNode);
//                process(subScene);
//                GLEngineUtils.dispose(subScene, renderContext);
            } else {
                System.out.println("Unsupported rendering technique "+technique.getClass().getName());
            }
        }
    }

    /**
     *
     * @param context
     * @param nanotime
     * @param renderSize : in view port coordinate system, (0,0) is lower left
     */
    private void update(MonoCamera camera, RenderContext context, long nanotime, Rectangle renderSize) {
        Rectangle renderRectangle = camera.getRenderArea();
        if (!renderRectangle.equals(renderSize)){
            camera.setRenderArea(renderSize);
        }

        //update the paint area
        final GL gl = context.getGL();
        if (gl!=null){
            gl.asGL1().glViewport((int) renderRectangle.getX(), (int) renderRectangle.getY(),
                    (int) renderRectangle.getWidth(), (int) renderRectangle.getHeight());
        }
    }

    @Override
    public void dispose(GLProcessContext ctx) {
        super.dispose(ctx);
        blinnPhongTech.dispose(ctx);
    }

}
