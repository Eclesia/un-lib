
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.LinearGradientPaint;
import science.unlicense.display.api.painter2d.RadialGradientPaint;
import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.engine.opengl.scene.GLGeometry2D;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class StrokeGeometry extends PainterTask{

    private final GLGeometry2D geom;
    private final Object paint;
    private final float width;
    private final MatrixRW mv;
    private final AlphaBlending blending;

    public StrokeGeometry(GLGeometry2D geom, Object paint, Affine2 mv, float width, AlphaBlending blending) {
        this.geom = geom;
        this.paint = paint;
        this.mv = mv.toMatrix();
        this.width = width;
        this.blending = blending;
    }

    @Override
    public void execute(GL3Painter2D worker) {
        configureBlending(worker,blending);

        final VBO vertVBO = geom.getContourVBO();
        final IBO indiceVBO = geom.getContourAdjencyIBO();

        //load vbo
        vertVBO.loadOnGpuMemory(worker.gl);
        indiceVBO.loadOnGpuMemory(worker.gl);

        ActorProgram prog = null;
        if (paint instanceof ColorPaint){
            prog = worker.programs.strokePlainColorProg;
            worker.programs.strokePlainColorProg.plainActor.getMapping().setColor( ((ColorPaint) paint).getColor());
            prog.preExecutionGL(worker.renderContext, null);
            prog.preDrawGL(worker.renderContext);
            worker.programs.strokePlainColorProg.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
            worker.programs.strokePlainColorProg.uniformP.setMat3(worker.gl, worker.pArray);
            worker.programs.strokePlainColorProg.uniformWidth.setFloat(worker.gl, width);
        } else if (paint instanceof Color){
            prog = worker.programs.strokePlainColorProg;
            worker.programs.strokePlainColorProg.plainActor.getMapping().setColor((Color) paint);
            prog.preExecutionGL(worker.renderContext, null);
            prog.preDrawGL(worker.renderContext);
            worker.programs.strokePlainColorProg.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
            worker.programs.strokePlainColorProg.uniformP.setMat3(worker.gl, worker.pArray);
            worker.programs.strokePlainColorProg.uniformWidth.setFloat(worker.gl, width);
        } else if (paint instanceof LinearGradientPaint){
            prog = worker.programs.strokeLinearGradientProg;
            worker.programs.strokeLinearGradientProg.lgActor.setGradient((LinearGradientPaint) paint);
            prog.preExecutionGL(worker.renderContext, null);
            prog.preDrawGL(worker.renderContext);
            worker.programs.strokeLinearGradientProg.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
            worker.programs.strokeLinearGradientProg.uniformP.setMat3(worker.gl, worker.pArray);
            worker.programs.strokeLinearGradientProg.uniformWidth.setFloat(worker.gl, width);
        } else if (paint instanceof RadialGradientPaint){
            prog = worker.programs.strokeRadialGradientProg;
            worker.programs.strokeRadialGradientProg.rgActor.setGradient((RadialGradientPaint) paint);
            prog.preExecutionGL(worker.renderContext, null);
            prog.preDrawGL(worker.renderContext);
            worker.programs.strokeRadialGradientProg.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
            worker.programs.strokeRadialGradientProg.uniformP.setMat3(worker.gl, worker.pArray);
            worker.programs.strokeRadialGradientProg.uniformWidth.setFloat(worker.gl, width);
        }


        worker.gl.glEnableVertexAttribArray(0);
        vertVBO.bind(worker.gl, 0);
        indiceVBO.bind(worker.gl);
        GLUtilities.checkGLErrorsFail(worker.gl);

        worker.gl.glDrawElements(
            GL_LINES_ADJACENCY,
            indiceVBO.getCapacity(),
            GL_UNSIGNED_INT,
            0
        );
        GLUtilities.checkGLErrorsFail(worker.gl);
        worker.gl.glDisableVertexAttribArray(0);

        prog.postDrawGL(worker.renderContext);

    }

}
