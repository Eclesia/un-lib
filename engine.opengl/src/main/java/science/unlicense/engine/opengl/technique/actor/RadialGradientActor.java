
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.painter2d.RadialGradientPaint;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.shader.ShaderUtils;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.image.api.color.Color;

/**
 *
 * @author Johann Sorel
 */
public class RadialGradientActor extends AbstractMaterialValueActor{

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/radialgradiant-4-fr.glsl"), ShaderTemplate.SHADER_TESS_CONTROL);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private static final Chars UNIFORM_GCENTER = Chars.constant("GCENTER");
    private static final Chars UNIFORM_GRADIUS = Chars.constant("GRADIUS");
    private static final Chars UNIFORM_GSTEPS = Chars.constant("GSTEPS");
    private static final Chars UNIFORM_GCOLORS = Chars.constant("GCOLORS");
    private static final Chars UNIFORM_NBSTEP = Chars.constant("NBSTEP");

    private Uniform uniformGcenter;
    private Uniform uniformGradius;
    private Uniform uniformNbStep;

    private RadialGradientPaint gradient;

    public RadialGradientActor(Chars baseReuseId, Chars produce, Chars method, Chars uniquePrefix) {
        super(baseReuseId, false, produce, method, uniquePrefix, null, null, null, null, SHADER_FR);
    }

    public void setGradient(RadialGradientPaint gradient) {
        this.gradient = gradient;
    }

    public RadialGradientPaint getGradient() {
        return gradient;
    }

    @Override
    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom) {
        super.initProgram(ctx, template, tess, geom);
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        uniformGcenter = program.getUniform(UNIFORM_GCENTER);
        uniformGradius = program.getUniform(UNIFORM_GRADIUS);
        uniformNbStep = program.getUniform(UNIFORM_NBSTEP);

        final GL2ES2 gl = context.getGL().asGL2ES2();

        //we need the coordinates in pixel cs, we are going to calculate distance with the
        //gl_fragcoord which is in this cs.
        final float[] center = new float[]{(float) gradient.getCenterX(),(float) gradient.getCenterY()};
        uniformGcenter.setVec2(gl, center);
        uniformGradius.setFloat(gl, (float) gradient.getRadius());

        final Color[] colors = gradient.getStopColors();
        final double[] steps = gradient.getStopOffsets();
        uniformNbStep.setInt(gl, steps.length);
        for (int i=0;i<steps.length;i++){
            Uniform us = program.getUniform(new Chars("GSTEPS["+i+"]"),gl,ShaderUtils.TYPE_FLOAT);
            Uniform uc = program.getUniform(new Chars("GCOLORS["+i+"]"),gl,ShaderUtils.TYPE_VEC4);
            us.setFloat(gl, (float) steps[i]);
            uc.setVec4(gl, colors[i].toRGBAPreMul());
        }
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniformGcenter = null;
        uniformGradius = null;
        uniformNbStep = null;
    }

}
