
package science.unlicense.engine.opengl.phase;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.api.opengl.GL1;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.FBO;

/**
 * The clear phase reset the various buffers using gl.glClear function.
 * This phase is usualy called in the first stages of the pipeline before
 * any rendering.
 *
 * @author Johann Sorel
 */
public class ClearPhase extends AbstractFboPhase{

    private float[] clearColor;
    private int clearBuffers;

    /**
     * Default clear phase which reset the main canvas with a color of {0,0,0,0}
     * All buffers, color, depth and stencil are cleared.
     */
    public ClearPhase() {
        this((FBO) null);
    }

    /**
     * Clear phase which reset given fbo or main canvas.
     *
     * @param output FBO to clear, if null default canvas will be cleared
     */
    public ClearPhase(FBO output){
        this(output, new float[]{0f, 0f, 0f, 0f});
    }

    /**
     * Clear phase which reset the main canvas.
     *
     * @param clearColor not null, array of size 4, colors premultiplied
     */
    public ClearPhase(float[] clearColor){
        this(null, clearColor);
    }

    /**
     * Clear phase which reset given fbo or main canvas.
     *
     * @param output FBO to clear, if null default canvas will be cleared
     * @param clearColor not null, array of size 4, colors premultiplied
     */
    public ClearPhase(FBO output, float[] clearColor){
        this(output,clearColor, GLC.CLEAR.COLOR | GLC.CLEAR.DEPTH | GLC.CLEAR.STENCIL);
    }

    /**
     * Clear phase which reset given fbo or main canvas.
     *
     * @param output FBO to clear, if null default canvas will be cleared
     * @param clearColor not null, array of size 4, colors premultiplied
     * @param clearBuffers buffers to clear, use any of
     *         GLC.CLEAR.COLOR | GLC.CLEAR.DEPTH | GLC.CLEAR.STENCIL
     */
    public ClearPhase(FBO output, float[] clearColor, int clearBuffers){
        super(output);
        this.clearColor = clearColor;
        this.clearBuffers = clearBuffers;
    }

    /**
     * Get clear color.
     *
     * @return float array, never null
     */
    public float[] getClearColor() {
        return clearColor;
    }

    /**
     * Set clear color.
     *
     * @param clearColor not null, array of size 4, colors premultiplied
     */
    public void setClearColor(float[] clearColor) {
        this.clearColor = clearColor;
    }

    /**
     * Set buffer to clear, use :
     * GLC.CLEAR.COLOR | GLC.CLEAR.DEPTH | GLC.CLEAR.STENCIL
     *
     * @param clearBuffers buffers to clear
     */
    public void setClearBuffers(int clearBuffers) {
        this.clearBuffers = clearBuffers;
    }

    /**
     * Get cleared buffers.
     *
     * @return buffers to clear
     */
    public int getClearBuffers() {
        return clearBuffers;
    }

    @Override
    protected void processInternal(GLProcessContext context) throws GLException {
        final GL1 gl = context.getGL().asGL1();
        gl.glClearColor(clearColor[0],clearColor[1],clearColor[2],clearColor[3]);
        gl.glClear(clearBuffers);

        //enable Z buffer
        gl.glDepthMask(true);
        gl.glEnable(GL_DEPTH_TEST);
        // Accept fragment if it closer to the camera than the former one
        gl.glDepthFunc(GL_LESS);
    }

}
