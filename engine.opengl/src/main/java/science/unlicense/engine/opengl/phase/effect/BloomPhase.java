
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture;
import science.unlicense.gpu.impl.opengl.shader.Uniform;

/**
 * Bloom effect.
 *
 * Acts as a blur for bright fragments.
 *
 * @author Johann Sorel
 */
public class BloomPhase extends AbstractTexturePhase {

    private static final Chars UNIFORM_FACTOR = Chars.constant("bloomFactor");

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/bloom-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final BloomActor actor = new BloomActor();
    private float bloomFactor;
    private Uniform uniFactor;

    public BloomPhase(Texture texture) {
        this(null,texture,0.2f);
    }

    public BloomPhase(Texture texture, float bloomFactor) {
        this(null,texture,bloomFactor);
    }

    public BloomPhase(FBO output, Texture texture, float bloomFactor) {
        super(output,texture);
        this.bloomFactor = bloomFactor;
    }

    public float getBloomFactor() {
        return bloomFactor;
    }

    public void setBloomFactor(float bloomFactor) {
        this.bloomFactor = bloomFactor;
    }

    @Override
    protected BloomActor getActor() {
        return actor;
    }

    private final class BloomActor extends DefaultActor{

        public BloomActor() {
            super(new Chars("Bloom"),false,null,null,null,null,SHADER_FR,true,true);
        }

        @Override
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL2ES2 gl = context.getGL().asGL2ES2();

            //get all uniforms
            if (uniFactor == null){
                uniFactor = program.getUniform(UNIFORM_FACTOR);
            }
            uniFactor.setFloat(gl, bloomFactor);
        }

        @Override
        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniFactor = null;
        }

    }

}
