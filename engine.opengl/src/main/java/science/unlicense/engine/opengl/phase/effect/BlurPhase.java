
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture;

/**
 * Box blur. TODO
 *
 * @author Johann Sorel
 */
public class BlurPhase extends AbstractTexturePhase {

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/blur-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final BlurActor actor = new BlurActor();

    public BlurPhase(FBO output,Texture texture1) {
        super(output, texture1);
    }

    @Override
    protected BlurActor getActor() {
        return actor;
    }

    private final class BlurActor extends DefaultActor{

        public BlurActor() {
            super(null,false,null,null,null,null,SHADER_FR,true,true);
        }
    }
}
