

package science.unlicense.engine.opengl.phase.picking;

import science.unlicense.common.api.event.DefaultEventMessage;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class PickMessage extends DefaultEventMessage {
    private final GraphicNode selection;
    private final int primitiveId;

    public PickMessage(GraphicNode pickedObject, int primitiveId) {
        super(false);
        this.selection = pickedObject;
        this.primitiveId = primitiveId;
    }

    /**
     * Get the picked scene object.
     * @return GLNode, usualy a mesh
     */
    public GraphicNode getSelection() {
        return selection;
    }

    /**
     * Get the gl_PrimitiveId.
     * @return
     */
    public int getPrimitiveId() {
        return primitiveId;
    }

    /**
     * Try to find the closest vertex in the selected object.
     * @param ctx
     * @return vertex index or -1 if it could not calculated.
     */
    public int[] findVertexId(GLProcessContext ctx) {
        final Model model = (Model) selection;

        final Geometry shape = model.getShape();
        if (!(shape instanceof Mesh)) return new int[]{-1,-1,-1};

        final Mesh mesh = (Mesh) shape;
        final TupleGrid ibo = mesh.getIndex();
        final TupleGridCursor ib = ibo.cursor();

        final IndexedRange range = mesh.getRanges()[0];
        final int offset = range.getIndexOffset();

        ib.moveTo(offset+primitiveId*3+0);
        final int vid0 = (int) ib.samples().get(0);        
        ib.moveTo(offset+primitiveId*3+1);
        final int vid1 = (int) ib.samples().get(0);        
        ib.moveTo(offset+primitiveId*3+2);
        final int vid2 = (int) ib.samples().get(0);

        return new int[]{vid0,vid1,vid2};
    }

}
