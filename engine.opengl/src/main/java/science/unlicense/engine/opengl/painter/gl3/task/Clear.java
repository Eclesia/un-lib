
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.geometry.api.BBox;
import static science.unlicense.gpu.api.opengl.GLC.*;

/**
 *
 * @author Johann Sorel
 */
public class Clear extends PainterTask{

    private final BBox bbox;
    private final boolean maskFBO;

    public Clear(BBox bbox, boolean maskFBO) {
        this.bbox = bbox;
        this.maskFBO = maskFBO;
    }

    @Override
    public void execute(GL3Painter2D painter) {
        if (maskFBO){
            if (painter.mask!=null) painter.mask.bind(painter.gl);
        } else {
            if (painter.fbo!=null) painter.fbo.bind(painter.gl);
        }

        if (bbox==null){
            painter.gl.glDisable(GL_SCISSOR_TEST);
            painter.gl.glClearColor(0f, 0f, 0f, 0f);
            painter.gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        } else {
            //opengl doesn't like negative or 0 spans
            final int minX = (int) bbox.getMin(0);
            final int minY = (int) bbox.getMin(1);
            final int spanX = (int) Math.ceil(bbox.getMax(0)-minX);
            final int spanY = (int) Math.ceil(bbox.getMax(1)-minY);
            if (spanX<=0 || spanY<=0){
                return;
            }

            painter.gl.glClearColor(0f, 0f, 0f, 0f);
            painter.gl.glEnable(GL_SCISSOR_TEST);
            painter.gl.glScissor(minX,minY,spanX,spanY);
            painter.gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            painter.gl.glDisable(GL_SCISSOR_TEST);
        }

    }

}
