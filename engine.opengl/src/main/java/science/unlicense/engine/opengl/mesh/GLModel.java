
package science.unlicense.engine.opengl.mesh;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.GLActorTechnique;
import science.unlicense.engine.opengl.tessellation.Tessellator;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.Model;

/**
 * Stores mesh model informations.
 *
 * @author Johann Sorel
 */
public class GLModel extends DefaultModel {

    //flags to update the mesh when needed
    private static final int SHAPE_DIRTY = 1 << 2;
    private static final int MATERIAL_DIRTY = 1 << 4;
    private static final int TESSELATOR_DIRTY = 1 << 5;

    //mesh model and texture variables
    private Tessellator tessellator;

    //various environement informations
    private boolean pickable = true;

    //loading state
    private int dirty = 0;
    private boolean loaded = false;


    public GLModel() {
        this(CoordinateSystems.CARTESIAN3D_METRIC_RIGH_HANDED);
    }

    public GLModel(Model model) {
        this(model.getCoordinateSystem());
        setShape(model.getShape());
        setId(model.getId());
        materials.replaceAll(model.getMaterials());
        setMorphTargetWeights(model.getMorphTargetWeights());
        setSkin(model.getSkin());
        getTechniques().addAll(model.getTechniques());
        setTitle(model.getTitle());
        setVisible(model.isVisible());
    }

    public GLModel(CoordinateSystem cs) {
        super(cs);
        getTechniques().add(new GLActorTechnique(this));
    }

    public boolean isDirty() {
        return (!loaded || dirty!=0)
            || (shape !=null && ((GLGeometry) shape).isDirty())
            || (isMaterialDirty())
            || (tessellator!=null && tessellator.isDirty());
    }

    private boolean isMaterialDirty() {
        for (int i=0,n=materials.getSize();i<n;i++) {
            final Material material = (Material) materials.get(i);
            Iterator matIte = material.properties().asDictionary().getValues().createIterator();
            while (matIte.hasNext()) {
                Object cdt = matIte.next();
                if (cdt instanceof Layer) {
                    if (((Layer) cdt).isDirty()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Mesh model informations /////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Reuse all informations form given mesh.
     * This does not duplicate materials or any vbo.
     *
     * @param model
     */
    public void copy(Model model) {
        getNodeTransform().set(model.getNodeTransform());
        setLocalCoordinateSystem(model.getLocalCoordinateSystem());
        setShape(model.getShape());
        setSkin(model.getSkin());
        materials.replaceAll(model.getMaterials());

        if (model instanceof GLModel) {
            final GLModel glm = (GLModel) model;
            this.tessellator = glm.tessellator;
            this.pickable = glm.pickable;
            this.dirty = glm.dirty;
            this.loaded = glm.loaded;
        }
    }

    @Override
    public void setShape(Geometry shell) {
        this.shape = shell;
        dirty |= SHAPE_DIRTY;
    }

    @Override
    public Geometry getShape() {
        return shape;
    }

    public void setTessellator(Tessellator material) {
        this.tessellator = material;
        dirty |= TESSELATOR_DIRTY;
    }

    public Tessellator getTessellator() {
        return tessellator;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Environement informations ///////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Indicate if the mesh is pickable.
     * Default is false.
     * @param pickable
     */
    public void setPickable(boolean pickable) {
        this.pickable = pickable;
    }

    public boolean isPickable() {
        return pickable;
    }

    ////////////////////////////////////////////////////////////////////////////
    // data loading ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    private void gpuCleanResources(final RenderContext ctx) {
        final GL gl = ctx.getGL();
        GLUtilities.checkGLErrorsFail(gl);
        ((GLGeometry) shape).dispose(ctx.getGL());
        GLUtilities.checkGLErrorsFail(gl);
    }

    private void gpuLoadResources(final RenderContext ctx) {
        gpuCleanResources(ctx);
        loaded = true;
        dirty = 0;
    }

    public void updateResources(RenderContext context) {
        gpuLoadResources(context);
    }

    public void dispose(GLProcessContext context) {
        GLEngineUtils.dispose(this, context);
    }

}
