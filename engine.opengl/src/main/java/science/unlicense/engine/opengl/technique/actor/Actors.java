
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.engine.opengl.mesh.BillBoard;
import science.unlicense.engine.opengl.mesh.InstancingShell;
import science.unlicense.engine.opengl.mesh.particle.ParticuleShape;
import science.unlicense.engine.opengl.mesh.particle.ParticuleShapeActor;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class Actors {

    /**
     * Find a shader actor and build an actor for given object.
     *
     * @return ShaderActor
     */
    public static ActorExecutor buildExecutor(Model model, Geometry shape){

        if (model.getSkin() != null) return new SkinShellActor(model);
        else if (shape instanceof InstancingShell) return new InstancingModelActor(model);
        else if (shape instanceof Mesh) {
            return new ModelActor(model);
        }
        else if (shape instanceof ParticuleShape) return new ParticuleShapeActor((ParticuleShape) shape);
        else if (shape instanceof BillBoard) return new BillBoardShapeActor((BillBoard) shape);

        throw new RuntimeException("No executor for : "+shape);
    }
}
