
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.light.Light;
import science.unlicense.display.impl.light.Lights;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.light.SpotLight;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.phase.shadow.ShadowMap;
import science.unlicense.engine.opengl.technique.actor.Actor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.engine.opengl.technique.actor.LightsActor;
import science.unlicense.engine.opengl.technique.actor.StructLight;
import science.unlicense.gpu.api.opengl.GL2ES2;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture;
import science.unlicense.gpu.impl.opengl.shader.ShaderUtils;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.Vector;
import science.unlicense.math.impl.MatrixNxN;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class LightPhase extends AbstractTexturePhase {

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/light/deferred-lights-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final Actor actor = new LightActor();
    private SceneNode scene;
    private MonoCamera camera;

    /**
     *
     * @param outputFBO
     * @param diffuse (vec4)
     * @param specular (vec4)
     * @param position : world space (vec3)
     * @param normal : world space (vec3)
     * @param scene
     * @param camera
     */
    public LightPhase(FBO outputFBO, Texture diffuse, Texture specular,
            Texture position, Texture normal, SceneNode scene, MonoCamera camera) {
        super(outputFBO, new Texture[]{diffuse,specular,position,normal});
        this.scene = scene;
        this.camera = camera;
    }

    public void setCamera(MonoCamera camera) {
        this.camera = camera;
    }

    public MonoCamera getCamera() {
        return camera;
    }

    public void setScene(GraphicNode scene) {
        this.scene = scene;
    }

    public SceneNode getScene() {
        return scene;
    }

    @Override
    protected Actor getActor() {
        return actor;
    }

    private final class LightActor extends DefaultActor{


        //lights informations
        private boolean dirty = true;
        private String base;
        private int nbLight;
        private Uniform uniformV;
        private Uniform uniformLightNb;
        private Uniform uniformLightAmbient;
        private Uniform uniformNbCellShadingSteps;
        private final Uniform[][] uniformLight = new Uniform[5][12];
        private final int[][] shadowMapReserved = new int[5][2];
        private final float[] lightAmbient = new float[4];
        private final float[][] lights_position = new float[5][4];
        private final float[][] lights_diffuse = new float[5][4];
        private final float[][] lights_specular = new float[5][4];
        private final float[] lights_constant = new float[5];
        private final float[] lights_linear = new float[5];
        private final float[] lights_quadratic = new float[5];
        private final float[] lights_falloffangle = new float[5];
        private final float[] lights_falloffexp = new float[5];
        private final float[][] lights_direction = new float[5][3];
        // model matrix, projection matrix, shadowmap
        private final Object[][] lightsExt = new Object[5][3];

        public LightActor() {
            super(new Chars("Lights"),false,null,null,null,null,SHADER_FR,true,true);
        }

        @Override
        public int getMinGLSLVersion() {
            return GLC.GLSL.V330_GL33;
        }

        @Override
        public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom) {
            template.getFragmentShaderTemplate().append(StructLight.TEMPLATE);
            super.initProgram(ctx, template, tess, geom);
            dirty = false;
            base = null;
        }

        @Override
        public void preDrawGL(RenderContext context, ActorProgram program) {

            if (dirty){
                dirty = false;
            }

            final GL2ES2 gl = context.getGL().asGL2ES2();
            GLUtilities.checkGLErrorsFail(gl);

            //grab the uniform ids
            if (base==null){
                uniformV = program.getUniform(new Chars("V"));
                uniformLightNb = program.getUniform(LightsActor.LIGHT_NB);
                uniformLightAmbient = program.getUniform(LightsActor.LIGHT_AMBIENT);
                uniformNbCellShadingSteps = program.getUniform(LightsActor.CELLSHADINGSTEPS);

                for (int i=0;i<5;i++){
                    base = "LIGHTS["+i+"].";
                    uniformLight[i][0] = program.getUniform(new Chars(base+"position"),gl, ShaderUtils.TYPE_VEC4);
                    uniformLight[i][1] = program.getUniform(new Chars(base+"diffuse"),gl, ShaderUtils.TYPE_VEC4);
                    uniformLight[i][2] = program.getUniform(new Chars(base+"specular"),gl, ShaderUtils.TYPE_VEC4);
                    uniformLight[i][3] = program.getUniform(new Chars(base+"constantAttenuation"),gl, ShaderUtils.TYPE_FLOAT);
                    uniformLight[i][4] = program.getUniform(new Chars(base+"linearAttenuation"),gl, ShaderUtils.TYPE_FLOAT);
                    uniformLight[i][5] = program.getUniform(new Chars(base+"quadraticAttenuation"),gl, ShaderUtils.TYPE_FLOAT);
                    uniformLight[i][6] = program.getUniform(new Chars(base+"spotCutoff"),gl, ShaderUtils.TYPE_FLOAT);
                    uniformLight[i][7] = program.getUniform(new Chars(base+"spotExponent"),gl, ShaderUtils.TYPE_FLOAT);
                    uniformLight[i][8] = program.getUniform(new Chars(base+"spotDirection"),gl, ShaderUtils.TYPE_VEC3);
                    uniformLight[i][9] = program.getUniform(new Chars(base+"v"),gl, ShaderUtils.TYPE_MAT4);
                    uniformLight[i][10] = program.getUniform(new Chars(base+"p"),gl, ShaderUtils.TYPE_MAT4);
                    uniformLight[i][11] = program.getUniform(new Chars("SHADOWMAPS["+i+"]"),gl, ShaderUtils.TYPE_SAMPLER2D);
                    GLUtilities.checkGLErrorsFail(gl);
                    lights_position[i] = new float[]{0f,1f,2f,1f};
                    lights_diffuse[i] = new float[]{1f,0f,0f,1f};
                    lights_specular[i] = new float[]{1f,1f,1f,1f};
                    lights_constant[i] = 1f;
                    lights_linear[i] = 1f;
                    lights_quadratic[i] = 1f;
                    lights_falloffangle[i] = 1f;
                    lights_falloffexp[i] = 0f;
                    lights_direction[i] = new float[]{1f,1f,1f};
                }
            }

            //update lights final RenderingContext ctx
            findLights(scene);

            uniformV.setMat4(gl, camera.getRootToNodeSpace().toMatrix().toArrayFloat());
            uniformLightNb.setInt(gl, nbLight);
            uniformLightAmbient.setVec4(gl, lightAmbient);
            uniformNbCellShadingSteps.setFloat(gl, 0);
            GLUtilities.checkGLErrorsFail(gl);

            //TODO popssible improvement using UBO
            // http://www.opengl.org/wiki/Uniform_Buffer_Objects
            for (int i=0;i<nbLight;i++){
                //vec4 position;
                //vec4 diffuse;
                //vec4 specular;
                //float constantAttenuation, linearAttenuation, quadraticAttenuation;
                //float spotCutoff, spotExponent;
                //vec3 spotDirection;
                gl.glUniform4fv(uniformLight[i][0].getGpuId(), lights_position[i]);
                gl.glUniform4fv(uniformLight[i][1].getGpuId(), lights_diffuse[i]);
                gl.glUniform4fv(uniformLight[i][2].getGpuId(), lights_specular[i]);
                gl.glUniform1f(uniformLight[i][3].getGpuId(), lights_constant[i]);
                gl.glUniform1f(uniformLight[i][4].getGpuId(), lights_linear[i]);
                gl.glUniform1f(uniformLight[i][5].getGpuId(), lights_quadratic[i]);
                gl.glUniform1f(uniformLight[i][6].getGpuId(), lights_falloffangle[i]);
                gl.glUniform1f(uniformLight[i][7].getGpuId(), lights_falloffexp[i]);
                gl.glUniform3fv(uniformLight[i][8].getGpuId(), lights_direction[i]);

                if (lightsExt[i][2]!=null){
                    uniformLight[i][ 9].setMat4(gl, ((MatrixNxN) lightsExt[i][0]).toArrayFloat() );
                    uniformLight[i][10].setMat4(gl, ((MatrixNxN) lightsExt[i][1]).toArrayFloat() );
                    shadowMapReserved[i] = context.getResourceManager().reserveTextureId();
                    gl.glActiveTexture(shadowMapReserved[i][0]);
                    ((Texture) lightsExt[i][2]).bind(gl);
                    uniformLight[i][11].setInt(gl, shadowMapReserved[i][1]);
                    GLUtilities.checkGLErrorsFail(gl);
                }
            }

        }

        @Override
        public void postDrawGL(RenderContext context, ActorProgram program) {
            final GL2ES2 gl = context.getGL().asGL2ES2();

            // unbind textures
            for (int i=0;i<nbLight;i++){
                if (lightsExt[i][2]!=null){
                    gl.glActiveTexture(shadowMapReserved[i][0]);
                    gl.glBindTexture(GL_TEXTURE_2D, 0);
                    context.getResourceManager().releaseTextureId(shadowMapReserved[i][0]);
                }
            }
            GLUtilities.checkGLErrorsFail(gl);
        }

        private void findLights(final SceneNode scene){
            final Sequence lightCol = Lights.getLights(scene);
            AmbientLight ambient = null;
            final Sequence others = new ArraySequence();
            if (lightCol!=null){
                for (int i=0,n=lightCol.getSize();i<n;i++){
                    Light light = (Light) lightCol.get(i);
                    if (light instanceof AmbientLight){
                        ambient = (AmbientLight) light;
                    } else if (light instanceof PointLight){
                        others.add(light);
                    } else if (light instanceof DirectionalLight){
                        others.add(light);
                    }
                }
            }

            if (ambient!=null){
                Arrays.fill(lightAmbient, 1.0f);
                ambient.getDiffuse().toRGBAPreMul(lightAmbient,0);
            } else {
                lightAmbient[0] = 1.0f;
                lightAmbient[1] = 1.0f;
                lightAmbient[2] = 1.0f;
                lightAmbient[3] = 1.0f;
            }

            //System.out.println("------");
            //fill light values
            nbLight = Maths.min(others.getSize(),5);
            for (int i=0;i<nbLight;i++){
                final Light light = (Light) others.get(i);
                final Affine ntw = light.getNodeToRootSpace();
                final Tuple worldPosition = ntw.transform(new Vector3f64(0, 0, 0), null);

                //vec4 position;
                lights_position[i][ 0] = (float) worldPosition.get(0);
                lights_position[i][ 1] = (float) worldPosition.get(1);
                lights_position[i][ 2] = (float) worldPosition.get(2);
                lights_position[i][ 3] = 1f;

                //vec4 diffuse;
                light.getDiffuse().toRGBAPreMul(lights_diffuse[i],0);

                //vec4 specular;
                light.getSpecular().toRGBAPreMul(lights_specular[i],0);

                if (light instanceof DirectionalLight){
                    final DirectionalLight direct = (DirectionalLight) light;
                    final Vector direction = direct.getWorldSpaceDirection();
                    lights_direction[i][0] = (float) direction.getX();
                    lights_direction[i][1] = (float) direction.getY();
                    lights_direction[i][2] = (float) direction.getZ();
                    lights_position[i][ 3] = 0f;


                } else if (light instanceof SpotLight){
                    final SpotLight sl = (SpotLight) light;
                    //float constantAttenuation, linearAttenuation, quadraticAttenuation;
                    lights_constant[i] = sl.getAttenuation().getConstant();
                    lights_linear[i] = sl.getAttenuation().getLinear();
                    lights_quadratic[i] = sl.getAttenuation().getquadratic();

                    //float spotCutoff, spotExponent;
                    lights_falloffangle[i] = sl.getFallOffAngle();
                    lights_falloffexp[i] = sl.getFallOffExponent();

                    //vec3 spotDirection;
                    final Vector direction = sl.getWorldSpaceDirection();
                    lights_direction[i][0] = (float) direction.getX();
                    lights_direction[i][1] = (float) direction.getY();
                    lights_direction[i][2] = (float) direction.getZ();

                } else if (light instanceof PointLight){
                    final PointLight pl = (PointLight) light;
                    //float constantAttenuation, linearAttenuation, quadraticAttenuation;
                    lights_constant[i] = pl.getAttenuation().getConstant();
                    lights_linear[i] = pl.getAttenuation().getLinear();
                    lights_quadratic[i] = pl.getAttenuation().getquadratic();
                    lights_falloffangle[i] = 180f;

                } else {
                    throw new RuntimeException("Unknowned light type");
                }

                //shadow map
                final ShadowMap sm = (ShadowMap) light.getUserProperties().getValue(ShadowMap.PROPERTY_SHADOWMAP);
                if (sm!=null){
                    final MonoCamera camera = sm.getCamera();
                    lightsExt[i][0] = camera.getRootToNodeSpace();
                    lightsExt[i][1] = camera.getProjectionMatrix();
                    lightsExt[i][2] = sm.getFbo().getTexture(GLC.FBO.Attachment.COLOR_0);
                }

            }

        }

        @Override
        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniformV = null;
            uniformLightNb = null;
            uniformLightAmbient = null;
            uniformNbCellShadingSteps = null;
            for (int x=0;x<uniformLight.length;x++){
                Arrays.fill(uniformLight[x], null);
            }
        }

    }

}
