
package science.unlicense.engine.opengl.scene;

import science.unlicense.display.api.scene.s2d.ImageNode2D;
import science.unlicense.gpu.impl.opengl.resource.Texture;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;

/**
 *
 * @author Johann Sorel
 */
public class GLImageNode2D extends ImageNode2D{

    private Texture texture;

    @Override
    public void setImage(Image image) {
        if (this.image == image) return;
        super.setImage(image);
        if (texture!=null){
            this.texture.setImage(image);
        }
    }

    @Override
    public Image getImage() {
        return getTexture().getImage();
    }

    public Texture getTexture() {
        if (texture==null && image!=null){
            texture = new Texture2D(image);
            texture.setForgetOnLoad(false);
        }
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

}
