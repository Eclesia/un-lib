
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.common.api.exception.UnimplementedException;

/**
 * TODO
 *
 * @author Johann Sorel
 */
public class AlphaActor extends AbstractActor{

    public AlphaActor() {
        super(null);
    }

    @Override
    public int getMinGLSLVersion() {
        throw new UnimplementedException("Not supported yet.");
    }

}
