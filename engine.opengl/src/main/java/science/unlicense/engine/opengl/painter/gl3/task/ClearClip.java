
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import static science.unlicense.gpu.api.opengl.GLC.*;

/**
 *
 * @author Johann Sorel
 */
public class ClearClip extends PainterTask{

    public ClearClip() {
    }

    @Override
    public void execute(GL3Painter2D worker) {
        //reset the stencil buffer
        worker.gl.glStencilMask(0xFF);
        worker.gl.glClearStencil(0);
        worker.gl.glClear(GL_STENCIL_BUFFER_BIT);
        worker.gl.glEnable(GL_STENCIL_TEST);
        worker.gl.glStencilFunc(GL_ALWAYS, 1, 0xFF);
        worker.gl.glStencilMask(0x00);
    }

}
