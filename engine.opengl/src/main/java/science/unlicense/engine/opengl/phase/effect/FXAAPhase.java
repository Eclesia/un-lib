
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.AbstractActor;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture;

/**
 * FXAA operation shader.
 *
 * FXAA is a post processing antialising.
 * In a nutshell it detects edges from luminance variation and apply a local blur.
 *
 * TODO : could not find the original shader reference, blog has been cleaned up.
 * Since the work has been derivated on multiple projects, games and engine I believe it is Free.
 * Yet still have to find a way to confirm this somehow ...
 *
 * Explication resources :
 * http://www.ngohq.com/images/articles/fxaa/FXAA_WhitePaper.pdf
 * http://timothylottes.blogspot.fr
 * http://timothylottes.blogspot.com/2011/04/nvidia-fxaa-ii-for-console.html
 * http://www.geeks3d.com/20110405/fxaa-fast-approximate-anti-aliasing-demo-glsl-opengl-test-radeon-geforce
 *
 * @author Johann Sorel
 */
public class FXAAPhase extends AbstractTexturePhase {

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/fxaa-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final FXAAActor actor = new FXAAActor();

    public FXAAPhase(Texture texture) {
        this(null,texture);
    }

    public FXAAPhase(FBO output, Texture texture) {
        super(output,texture);
    }

    @Override
    protected FXAAActor getActor() {
        return actor;
    }

    private final class FXAAActor extends AbstractActor{

        public FXAAActor() {
            super(null);
        }

        @Override
        public int getMinGLSLVersion() {
            return SHADER_FR.getMinGLSLVersion();
        }

        @Override
        public void initProgram(RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
            super.initProgram(ctx, template, tess, geom);
            final ShaderTemplate fragmentShader = template.getFragmentShaderTemplate();

            ShaderTemplate copy = new ShaderTemplate(ShaderTemplate.SHADER_FRAGMENT);
            copy.append(SHADER_FR);
            copy.replaceTexts(new Chars("$width"), Int32.encode((int) ctx.getViewRectangle().getWidth()));
            copy.replaceTexts(new Chars("$height"), Int32.encode((int) ctx.getViewRectangle().getHeight()));

            fragmentShader.append(copy);
        }

    }

}
