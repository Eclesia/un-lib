

package science.unlicense.engine.opengl.phase.reflection;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.gpu.impl.opengl.shader.Uniform;

/**
 * Actor which discard fragments under the given plan equation.
 *
 * @author Johann Sorel
 */
public class PlanClipActor extends DefaultActor {

    private static final ShaderTemplate PLANECLIP_FR;
    private static final Chars CLIP_PLAN = Chars.constant("CLIP_PLAN");
    static {
        try{
            PLANECLIP_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/reflectionclip-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final ReflectionVisitor visitor;
    private final float[] planEquation;
    private Uniform planClip;

    public PlanClipActor(ReflectionVisitor visitor)  {
        super(new Chars("PlanClip"), false,null, null, null, null, PLANECLIP_FR, true, true);
        this.visitor = visitor;
        this.planEquation = null;
    }

    public PlanClipActor(float[] planEquation)  {
        super(new Chars("PlanClip"), false,null, null, null, null, PLANECLIP_FR, true, true);
        this.visitor = null;
        this.planEquation = planEquation;
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);

        if (planClip==null){
            planClip = program.getUniform(CLIP_PLAN);
        }
        if (planEquation!=null){
            planClip.setVec4(context.getGL().asGL2ES2(), planEquation);
        } else {
            final float[] pe = visitor.getPlanEquation();
            planClip.setVec4(context.getGL().asGL2ES2(), pe);
        }
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        planClip = null;
    }

}
