
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 * Light structure, fragment shader with Light and fragement structures and methods.
 *
 * @author Johann Sorel
 */
public final class StructLight {

    public static final ShaderTemplate TEMPLATE;
    static {
        try{
            TEMPLATE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/light/lights-base-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    //LIGHT
    /** vec4 : in world space*/
    public static final Chars LIGHT_POSITION = Chars.constant("position");
    /** vec4 */
    public static final Chars LIGHT_DIFFUSE = Chars.constant("diffuse");
    /** vec4 */
    public static final Chars LIGHT_SPECULAR = Chars.constant("specular");
    /** float */
    public static final Chars LIGHT_CONSTANT_ATTENUATION = Chars.constant("constantAttenuation");
    /** float */
    public static final Chars LIGHT_LINEAR_ATTENUATION = Chars.constant("linearAttenuation");
    /** float */
    public static final Chars LIGHT_QUADRATIC_ATTENUATION = Chars.constant("quadraticAttenuation");
    /** float */
    public static final Chars LIGHT_SPOT_CUTOFF = Chars.constant("spotCutoff");
    /** float */
    public static final Chars LIGHT_SPOT_EXPONENT = Chars.constant("spotExponent");
    /** vec3 */
    public static final Chars LIGHT_SPOT_DIRECTION = Chars.constant("spotDirection");
    /** int */
    public static final Chars LIGHT_HAS_SHADOWMAP = Chars.constant("hasShadowMap");
    /** mat4 */
    public static final Chars LIGHT_VIEW = Chars.constant("v");
    /** mat4 */
    public static final Chars LIGHT_PROJECTION = Chars.constant("p");

    //FRAGEMENT
    /** vec3 */
    public static final Chars FRAG_NORMAL = Chars.constant("normal");
    /** vec3 */
    public static final Chars FRAG_EYE_DIRECTION = Chars.constant("eyeDirection");
    /** Light */
    public static final Chars FRAG_LIGHT = Chars.constant("light");
    /** vec4 */
    public static final Chars FRAG_DIFFUSE = Chars.constant("diffuse");
    /** vec4 */
    public static final Chars FRAG_SPECULAR = Chars.constant("specular");

    private StructLight(){}

}
