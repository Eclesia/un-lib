
package science.unlicense.engine.opengl.control;

import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.math.impl.Matrix4x4;

/**
 * Preserve scale, based on canvas size.
 * TODO, a parameter in the scale is missing, ratio is not exact.
 * @author Johann Sorel
 */
public class ScaleLock implements Updater{

    private final GraphicNode target;
    private final double scale;

    public ScaleLock(GraphicNode target, double scale) {
        this.target = target;
        this.scale = scale;
    }

    @Override
    public void update(DisplayTimerState timer) {
        final RenderContext ctx = (RenderContext) timer;
        double scaley = 1.0/ctx.getViewRectangle().getHeight();
        scaley *= scale;
        Matrix4x4 m = new Matrix4x4().setToIdentity();
        m.set(0, 0, scaley);
        m.set(1, 1, scaley);
        m.set(2, 2, scaley);
        target.getNodeTransform().set(m);
    }
}
