
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.common.api.CObjects;
import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.Texture2DMS;
import science.unlicense.image.api.Image;

/**
 *
 * @author Johann Sorel
 */
public class BlitImage extends PainterTask {

    private final Texture2DMS texture;
    private Texture2D blit;
    private Image image;

    public BlitImage(Texture2DMS texture, Texture2D blit) {
        CObjects.ensureNotNull(texture);
        this.texture = texture;
        this.blit = blit;
    }

    public Image getImage() {
        return image;
    }

    public Texture2D getBlit() {
        return blit;
    }

    @Override
    public void execute(GL3Painter2D worker) {
        blit = texture.blit(worker.gl,blit);
    }

}
