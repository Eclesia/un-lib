
package science.unlicense.engine.opengl.technique;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.model3d.impl.technique.AbstractTechnique;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractGLTechnique extends AbstractTechnique implements GLTechnique {

    protected Predicate phasePredicate;
    protected GLRenderState state = new GLRenderState();

    public AbstractGLTechnique() {
        this(Predicate.TRUE);
    }

    public AbstractGLTechnique(Predicate phasePredicate) {
        CObjects.ensureNotNull(phasePredicate);
        this.phasePredicate = phasePredicate;
    }

    @Override
    public Predicate getPhasePredicate() {
        return phasePredicate;
    }

    @Override
    public void setPhasePredicate(Predicate predicate) {
        CObjects.ensureNotNull(predicate);
        this.phasePredicate = predicate;
    }

    @Override
    public GLRenderState getState() {
        return state;
    }

    public void setState(GLRenderState state) {
        CObjects.ensureNotNull(state);
        this.state = state;
    }

    /**
     * Ensure the render state is called before renderInternal.
     *
     * @param context
     * @param camera
     * @param node
     */
    @Override
    public final void render(RenderContext context, MonoCamera camera, SceneNode node) {
        getState().configure(context.getGL());
        renderInternal(context, camera, node);
    }

    protected void renderInternal(RenderContext context, MonoCamera camera, SceneNode node){

    }

}
