
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.DefaultInt8Buffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.FloatSequence;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.font.Font;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontContext;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.painter.gl3.GL3ImagePainter2D;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.technique.AbstractGLTechnique;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.transform.Projections;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.TextureModel;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.gpu.impl.opengl.shader.Shader;
import science.unlicense.gpu.impl.opengl.shader.ShaderProgram;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.gpu.impl.opengl.shader.VertexAttribute;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;

/**
 * Generate a font page image.
 *
 * @author Johann Sorel
 */
public class FontPage {

    public static TextureModel VEC1_USHORT(){
        final TextureModel tm = TextureModel.create2D(GLC.Texture.InternalFormat.R16UI, GLC.Texture.Format.RED_INTEGER, GLC.Texture.Type.UNSIGNED_SHORT);
        return tm;
    }
    private static final Chars P1 = Chars.constant("l_position1");
    private static final Chars P2 = Chars.constant("l_position2");
    private static final Chars P3 = Chars.constant("l_position3");
    private static final Chars P4 = Chars.constant("l_position4");
    private static final Chars P5 = Chars.constant("l_position5");
    private static final Chars MV = Chars.constant("MV");
    private static final Chars P = Chars.constant("P");
    private static final Chars SIZE = Chars.constant("SIZE");

    private final FontChoice fontChoice;
    private int width;
    private int height;
    private Font font;
    private Chars family;
    private Texture2D texture;

    private int[] unicodes;
    private int nbGlpyhPerDim;
    private int glyphwidthi;
    private int glypheighti;
    private double glyphwidth;
    private double glypheight;
    private double scaleX;
    private double scaleY;
    private BBox glyphBox;

    public FontPage(FontChoice font) {
        this.fontChoice = font;
    }

    public FontChoice getFont() {
        return fontChoice;
    }

    public Texture2D getTexture() {
        return texture;
    }

    public Affine2 getGlyphTransform(int unicode){
        final int index = Arrays.getFirstOccurence(unicodes, 0, unicodes.length, unicode);
        final int indeX = index % nbGlpyhPerDim;
        final int indeY = index / nbGlpyhPerDim;

        double scaleX = 1.0/width;
        double scaleY = 1.0/height;

        return new Affine2(
                glyphwidthi*scaleX, 0, indeX*glyphwidthi*scaleX,
                0, glypheighti*scaleY, indeY*glypheighti*scaleY);
    }

    public Texture2D load() throws IOException {
        if (texture!=null){
            return texture;
        }

        //count characters in the font
        family = fontChoice.getFamilies()[0];
        font = FontContext.getResolver(family).getFont(family);
        unicodes = font.listCharacters().toArrayInt();
        Arrays.sort(unicodes);
        final FontMetadata metadata = font.getMetaData();

        glyphBox = metadata.getGlyphBox();
        final double boxwidth = glyphBox.getSpan(0);
        final double boxheight = glyphBox.getSpan(1);
        final double ascent = metadata.getAscent();

        //we compute the most appropriate ratio to obtain at 2x2 samples in size 32
        //since one pixel in the texture contains 4x4 samples
        final double scale = 16.0 / ascent;
        glyphwidth = boxwidth*scale;
        glypheight = boxheight*scale;
        glyphwidthi = (int) Math.ceil(glyphwidth);
        glypheighti = (int) Math.ceil(glypheight);

        nbGlpyhPerDim = (int) Math.ceil(Math.sqrt(unicodes.length));

        width = glyphwidthi * nbGlpyhPerDim;
        height = glypheighti * nbGlpyhPerDim;

        //load from cache if it exist
        final Path cachePath = GL3ImagePainter2D.getFontPageCacheFile(family);
        if (cachePath.exists()){
            final byte[] data = IOUtilities.readAll(cachePath.createInputStream());
            final Image image = new DefaultImage(new DefaultInt8Buffer(data), new Extent.Long(width, height), null);
            texture = new Texture2D(image, VEC1_USHORT());
            return texture;
        }

        //create a texture for rendering
        final GLSource drawable = GLUtilities.createOffscreenSource(16, 16);
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        drawable.getCallbacks().add(context);

        texture = new Texture2D(width, height, VEC1_USHORT());
        final FBO fbo = new FBO(width, height);
        fbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, texture);


        //prepare program
        final ShaderProgram program = new ShaderProgram();
        final Shader vertexShader = new Shader(new Chars(IOUtilities.readAll(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-glyph-0-ve.glsl")).createInputStream())),Shader.SHADER_VERTEX);
        final Shader fragmentShader = new Shader(new Chars(IOUtilities.readAll(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-glyph-4-fr.glsl")).createInputStream())),Shader.SHADER_FRAGMENT);
        program.setShader(vertexShader, Shader.SHADER_VERTEX);
        program.setShader(fragmentShader, Shader.SHADER_FRAGMENT);

        final GraphicNode scene = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);

        scaleX = 2.0/width;
        scaleY = 2.0/height;

        //build the scene
        context.getPhases().add(new RenderPhase(scene,new MonoCamera(),fbo));
        scene.getTechniques().add(new BatchGlyphTechnique(unicodes, program));
        drawable.render();
//        for (int i=0;i<unicodes.length;i++) {
//            scene.getRenderers().add(new SingleGlyphRenderer(unicodes[i],i,program));
//            context.paint();
//            scene.getRenderers().removeAll();
//        }

        //render it
        context.getPhases().removeAll();
        context.getPhases().add(new AbstractFboPhase() {
            protected void processInternal(GLProcessContext ctx) throws GLException {
                //get the result image
                GLEngineUtils.dispose(scene, ctx);
                fbo.removeAttachment(GLC.FBO.Attachment.COLOR_0);
                texture.loadOnSystemMemory(ctx.getGL());
                texture.unloadFromGpuMemory(ctx.getGL());
                fbo.unloadFromGpuMemory(ctx.getGL());
                program.unloadFromGpuMemory(context.getGL());
            }
        });

        drawable.render();
        drawable.dispose();

        //save to file cache
        ByteOutputStream cacheOut = cachePath.createOutputStream();
        cacheOut.write(texture.getImage().getDataBuffer().toByteArray());
        cacheOut.close();
//        texture.getImage().getModels().add(ColorModel.MODEL_COLOR, GeometryMaskRasterizer.COLOR_MODEL);
//        Images.write(GeometryMaskRasterizer.to1BitParSample(texture.getImage()), new Chars("png"), Paths.resolve("/home/eclesia/page"+font.getFamilies()[0]+".png"));

        //load texture in provided gl context
        return texture;
    }

    private class BatchGlyphTechnique extends AbstractGLTechnique{

        private final ShaderProgram program;
        private final MatrixRW p = new Matrix3x3().setToIdentity();

        private final Sequence idx1 = new ArraySequence();
        private final Sequence vbo = new ArraySequence();
        private final Sequence mv = new ArraySequence();

        private BatchGlyphTechnique(int[] unicodes, ShaderProgram program) {
            this.program = program;
            state.setEnable(GLC.GETSET.State.COLOR_LOGIC_OP, true);

            final BBox target = new BBox(2);
            final VectorRW anchor = new Vector3f64(0,0,0);
            final VectorRW current = new Vector3f64();
            final VectorRW previous = new Vector3f64();
            final VectorRW first = new Vector3f64();
            final VectorRW control = new Vector3f64();
            final float[] buffer = new float[3];

            for (int index=0;index<unicodes.length;index++){
                final PlanarGeometry geom = font.getGlyph(unicodes[index]);
                if (geom==null) continue;

                //compute matrix
                final int indeX = index % nbGlpyhPerDim;
                final int indeY = index / nbGlpyhPerDim;
                final int offsetX = glyphwidthi * indeX;
                final int offsetY = glypheighti * indeY;
                target.setRange(0, offsetX*scaleX -1, (offsetX+glyphwidthi)*scaleX -1);
                target.setRange(1, offsetY*scaleY -1, (offsetY+glypheighti)*scaleY -1);
                final Matrix mv = Projections.stretched(glyphBox, target).toMatrix();
                this.mv.add(mv);

                //build vbo and render
                final FloatSequence seq = new FloatSequence();
                final PathIterator ite = geom.createPathIterator();

                previous.toFloat(buffer,0);
                seq.put(buffer);
                previous.toFloat(buffer,0);
                seq.put(buffer);

                while (ite.next()){
                    int type = ite.getType();
                    if (PathIterator.TYPE_MOVE_TO==type){
                        ite.getPosition(first);
                        ite.getPosition(current);
                    } else if (PathIterator.TYPE_LINE_TO==type){
                        ite.getPosition(current);
                        control.setZ(0);
                        current.setZ(0);
                        previous.setZ(0);
                        anchor.toFloat(buffer,0);
                        seq.put(buffer);
                        current.toFloat(buffer,0);
                        seq.put(buffer);
                        previous.toFloat(buffer,0);
                        seq.put(buffer);
                    } else if (PathIterator.TYPE_CLOSE==type){
                        control.setZ(0);
                        current.setZ(0);
                        previous.setZ(0);
                        anchor.toFloat(buffer,0);
                        seq.put(buffer);
                        first.toFloat(buffer,0);
                        seq.put(buffer);
                        previous.toFloat(buffer,0);
                        seq.put(buffer);
                    } else if (PathIterator.TYPE_QUADRATIC==type){
                        control.setZ(0);
                        current.setZ(0);
                        previous.setZ(0);
                        ite.getPosition(current);
                        anchor.toFloat(buffer,0);
                        seq.put(buffer);
                        current.toFloat(buffer,0);
                        seq.put(buffer);
                        previous.toFloat(buffer,0);
                        seq.put(buffer);

                        // special triangle
                        ite.getFirstControl(control);
                        control.setZ(1);
                        current.setZ(1);
                        previous.setZ(1);
                        seq.put(control.toFloat());
                        seq.put(current.toFloat());
                        seq.put(previous.toFloat());

                    } else {
                        throw new RuntimeException("TODO " + type);
                    }

                    previous.set(current);
                }

                float[] coords = seq.toArrayFloat();
                VBO vbo = new VBO(coords,3);
                vbo.setForgetOnLoad(true);
                this.vbo.add(vbo);
                IndexedRange idx1 = IndexedRange.TRIANGLES(0, (coords.length-6)/3);
                this.idx1.add(idx1);
            }

        }

        @Override
        public void renderInternal(RenderContext context, MonoCamera camera, SceneNode node) {
            final GL2ES2 gl = context.getGL().asGL2ES2();

            gl.asGL1().glLogicOp(GLC.LOGIC_OP.XOR);

            program.loadOnGpuMemory(gl);
            program.enable(gl);

            //gl.glEnable(GLC.GETSET.State.SCISSOR_TEST);
            //gl.glScissor(target.get, width, width, width);

            final VertexAttribute va1 = program.getVertexAttribute(P1, gl, 1);
            final VertexAttribute va2 = program.getVertexAttribute(P2, gl, 1);
            final VertexAttribute va3 = program.getVertexAttribute(P3, gl, 1);
            final VertexAttribute va4 = program.getVertexAttribute(P4, gl, 1);
            final VertexAttribute va5 = program.getVertexAttribute(P5, gl, 1);
            final Uniform uniMV = program.getUniform(MV);
            final Uniform uniP = program.getUniform(P);
            final Uniform uniSIZE = program.getUniform(SIZE);
            uniP.setMat3(gl, p.toArrayFloat());
            uniSIZE.setVec2(gl, new float[]{width,height});

            for (int i=0,n=10;i<n;i++){
                final MatrixRW mv = (MatrixRW) this.mv.get(i);
                uniMV.setMat3(gl, mv.toArrayFloat());

                //build vbo and render
                VBO vbo = (VBO) this.vbo.get(i);
                va1.enable(gl, vbo, 0*3*4);
                va2.enable(gl, vbo, 1*3*4);
                va3.enable(gl, vbo, 2*3*4);
                va4.enable(gl, vbo, 3*3*4);
                va5.enable(gl, vbo, 4*3*4);
                GLEngineUtils.draw(gl, ((IndexedRange) idx1.get(i)), null);
                vbo.unbind(gl);
                vbo.unloadFromGpuMemory(context.getGL());
                GLUtilities.checkGLErrorsFail(gl);
            }

            program.disable(gl);
            dispose(context);
        }

        @Override
        public void dispose(GLProcessContext context) {
        }

    }

    private class SingleGlyphTechnique extends AbstractGLTechnique{

        private final ShaderProgram program;
        private final MatrixRW p = new Matrix3x3().setToIdentity();
        private Matrix mv;

        private IndexedRange idx1;
        private VBO vbo;

        private SingleGlyphTechnique(int unicode, int index, ShaderProgram program) {
            this.program = program;
            state.setEnable(GLC.GETSET.State.COLOR_LOGIC_OP, true);

            PlanarGeometry geom = font.getGlyph(unicode);
            if (geom==null) return;

            //compute matrix
            final BBox target = new BBox(2);
            final int indeX = index % nbGlpyhPerDim;
            final int indeY = index / nbGlpyhPerDim;
            final int offsetX = glyphwidthi * indeX;
            final int offsetY = glypheighti * indeY;
            target.setRange(0, offsetX*scaleX -1, (offsetX+glyphwidthi)*scaleX -1);
            target.setRange(1, offsetY*scaleY -1, (offsetY+glypheighti)*scaleY -1);
            mv = Projections.stretched(glyphBox, target).toMatrix();

            final FloatSequence seq = new FloatSequence();
            final PathIterator ite = geom.createPathIterator();

            final VectorRW anchor = new Vector3f64(0,0,0);
            final VectorRW current = new Vector3f64();
            final VectorRW previous = new Vector3f64();
            final VectorRW first = new Vector3f64();
            final VectorRW control = new Vector3f64();
            final float[] buffer = new float[3];

            previous.toFloat(buffer,0);
            seq.put(buffer);
            previous.toFloat(buffer,0);
            seq.put(buffer);

            while (ite.next()){
                int type = ite.getType();
                if (PathIterator.TYPE_MOVE_TO==type){
                    ite.getPosition(first);
                    ite.getPosition(current);
                } else if (PathIterator.TYPE_LINE_TO==type){
                    ite.getPosition(current);
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    anchor.toFloat(buffer,0);
                    seq.put(buffer);
                    current.toFloat(buffer,0);
                    seq.put(buffer);
                    previous.toFloat(buffer,0);
                    seq.put(buffer);
                } else if (PathIterator.TYPE_CLOSE==type){
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    anchor.toFloat(buffer,0);
                    seq.put(buffer);
                    first.toFloat(buffer,0);
                    seq.put(buffer);
                    previous.toFloat(buffer,0);
                    seq.put(buffer);
                } else if (PathIterator.TYPE_QUADRATIC==type){
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    ite.getPosition(current);
                    anchor.toFloat(buffer,0);
                    seq.put(buffer);
                    current.toFloat(buffer,0);
                    seq.put(buffer);
                    previous.toFloat(buffer,0);
                    seq.put(buffer);

                    // special triangle
                    ite.getFirstControl(control);
                    control.setZ(1);
                    current.setZ(1);
                    previous.setZ(1);
                    control.toFloat(buffer,0);
                    seq.put(buffer);
                    current.toFloat(buffer,0);
                    seq.put(buffer);
                    previous.toFloat(buffer,0);
                    seq.put(buffer);

                } else {
                    throw new RuntimeException("TODO " + type);
                }

                previous.set(current);
            }

            float[] coords = seq.toArrayFloat();
            vbo = new VBO(coords,3);
            vbo.setForgetOnLoad(true);
            idx1 = IndexedRange.TRIANGLES(0, (coords.length-6)/3);
        }

        @Override
        public void renderInternal(RenderContext context, MonoCamera camera, SceneNode node) {
            if (vbo==null) return;
            final GL2ES2 gl = context.getGL().asGL2ES2();

            gl.asGL1().glLogicOp(GLC.LOGIC_OP.XOR);

            program.loadOnGpuMemory(gl);
            program.enable(gl);

            //gl.glEnable(GLC.GETSET.State.SCISSOR_TEST);
            //gl.glScissor(target.get, width, width, width);

            final VertexAttribute va1 = program.getVertexAttribute(P1, gl, 1);
            final VertexAttribute va2 = program.getVertexAttribute(P2, gl, 1);
            final VertexAttribute va3 = program.getVertexAttribute(P3, gl, 1);
            final VertexAttribute va4 = program.getVertexAttribute(P4, gl, 1);
            final VertexAttribute va5 = program.getVertexAttribute(P5, gl, 1);
            final Uniform uniMV = program.getUniform(MV);
            final Uniform uniP = program.getUniform(P);
            final Uniform uniSIZE = program.getUniform(SIZE);
            uniMV.setMat3(gl, mv.toArrayFloat());
            uniP.setMat3(gl, p.toArrayFloat());
            uniSIZE.setVec2(gl, new float[]{width,height});

            va1.enable(gl, vbo, 0*3*4);
            va2.enable(gl, vbo, 1*3*4);
            va3.enable(gl, vbo, 2*3*4);
            va4.enable(gl, vbo, 3*3*4);
            va5.enable(gl, vbo, 4*3*4);
            GLEngineUtils.draw(gl, idx1, null);
            vbo.unbind(gl);
            vbo.unloadFromGpuMemory(context.getGL());
            GLUtilities.checkGLErrorsFail(gl);

            program.disable(gl);
            dispose(context);
        }

        @Override
        public void dispose(GLProcessContext context) {
        }

    }

}
