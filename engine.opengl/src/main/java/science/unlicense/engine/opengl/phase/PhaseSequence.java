

package science.unlicense.engine.opengl.phase;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.gpu.api.GLException;

/**
 * Sequence of phases.
 *
 * @author Johann Sorel
 */
public class PhaseSequence extends AbstractPhase {

    private final Sequence phases = new ArraySequence();

    public Sequence getPhases() {
        return phases;
    }

    @Override
    public void processInt(GLProcessContext context) throws GLException {
        for (int i=0,n=phases.getSize();i<n;i++){
            final Phase phase = (Phase) phases.get(i);
            if (!phase.isEnable()) continue;
            phase.process(context);
        }
    }

}
