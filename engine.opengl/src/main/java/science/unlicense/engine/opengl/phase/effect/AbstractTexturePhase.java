
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.AbstractActor;
import science.unlicense.engine.opengl.technique.actor.Actor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.api.opengl.GL1;
import science.unlicense.gpu.api.opengl.GL2ES2;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Matrix4x4;

/**
 * A Phase which work with at multiple input texture.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTexturePhase extends AbstractFboPhase {

    private static final ShaderTemplate SHADER_VE;
    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/phase-texture-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/phase-texture-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    private static final Chars UNI_PIXELSIZE = Chars.constant("PX");
    private static final Chars UNI_MVP = Chars.constant("MVP");

    //default actor
    private final Actor baseActor = new BaseActor();

    // gpu buffers
    private final Sequence textures = new ArraySequence();
    private final Sequence uniTextures = new ArraySequence();
    private final Sequence reservedTexIds = new ArraySequence();
    private ActorProgram program;
    private Uniform uniPixelSize;
    private Uniform uniMvp;

    private Matrix4x4 mvp = new Matrix4x4().setToIdentity();

    public AbstractTexturePhase() {
        this((FBO) null);
    }

    public AbstractTexturePhase(Texture tex0) {
        this((FBO) null,tex0);
    }

    public AbstractTexturePhase(Texture tex0, Texture tex1) {
        this((FBO) null,tex0,tex1);
    }

    public AbstractTexturePhase(Texture tex0, Texture tex1, Texture tex2) {
        this((FBO) null,tex0,tex1,tex2);
    }

    public AbstractTexturePhase(Texture tex0, Texture tex1, Texture tex2, Texture tex3) {
        this((FBO) null,tex0,tex1,tex2,tex3);
    }

    public AbstractTexturePhase(FBO outputFBO) {
        super(outputFBO);
    }

    public AbstractTexturePhase(FBO outputFBO, Texture tex0) {
        super(outputFBO);
        this.textures.add(tex0);
    }

    public AbstractTexturePhase(FBO outputFBO, Texture tex0, Texture tex1) {
        super(outputFBO);
        this.textures.add(tex0);
        this.textures.add(tex1);
    }

    public AbstractTexturePhase(FBO outputFBO, Texture tex0, Texture tex1, Texture tex2) {
        super(outputFBO);
        this.textures.add(tex0);
        this.textures.add(tex1);
        this.textures.add(tex2);
    }

    public AbstractTexturePhase(FBO outputFBO, Texture tex0, Texture tex1, Texture tex2, Texture tex3) {
        super(outputFBO);
        this.textures.add(tex0);
        this.textures.add(tex1);
        this.textures.add(tex2);
        this.textures.add(tex3);
    }

    public AbstractTexturePhase(FBO outputFBO, Texture[] texs) {
        super(outputFBO);
        this.textures.addAll(texs);
    }

    public Sequence getTextures() {
        return textures;
    }

    public Matrix4x4 getTransform(){
        return mvp;
    }

    public void setTransform(Matrix4x4 mvp){
        this.mvp = mvp;
    }

    @Override
    protected void processInternal(GLProcessContext context) throws GLException {
        final GL1 gl = context.getGL().asGL1();
        final RenderContext renderContext = new RenderContext(context, null);

        //disable Z buffer
        gl.glDisable(GL_DEPTH_TEST);
        // Accept fragment if it closer to the camera than the former one
        gl.glDepthFunc(GL_LESS);
        gl.glPolygonMode(GL_FRONT_AND_BACK, GLC.POLYGON_MODE.FILL);


        //load vertex and uv coord
        if (program == null){

            program = new ActorProgram();
            program.getActors().add(baseActor);
            program.getActors().add(getActor());
            program.compile(renderContext);


            //get texture uniforms
            for (int i=0,n=textures.getSize();i<n;i++){
                uniTextures.add(program.getUniform(new Chars("sampler"+i)));
                reservedTexIds.add(new int[2]);
            }

            uniPixelSize = program.getUniform(UNI_PIXELSIZE);
            uniMvp = program.getUniform(UNI_MVP);
        }

        //prepare actors
        program.preExecutionGL(renderContext, null);

        //enable shader program
        program.preDrawGL(renderContext);

        //render
        gl.glDrawArrays(GL_TRIANGLES,0,6);
        GLUtilities.discardErrors(gl);
        GLUtilities.checkGLErrorsFail(gl);

        //cleaning
        program.postDrawGL(renderContext);

    }

    /**
     * Clear used resources on the gpu.
     * @param ctx
     */
    @Override
    public void dispose(GLProcessContext ctx) {
        if (program!=null){
            program.releaseProgram(ctx);
        }
    }

    protected abstract Actor getActor();

    private class BaseActor extends AbstractActor{

        public BaseActor() {
            super(null);
        }

        @Override
        public Chars getReuseUID() {
            return new Chars("AbsTexPhase"+textures.getSize());
        }

        @Override
        public int getMinGLSLVersion() {
            return Maths.max(
                    SHADER_VE.getMinGLSLVersion(),
                    SHADER_FR.getMinGLSLVersion());
        }

        @Override
        public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom) {
            super.initProgram(ctx, template, tess, geom);
            template.getVertexShaderTemplate().append(SHADER_VE);
            template.getFragmentShaderTemplate().append(SHADER_FR);
            //add texture uniforms
            for (int i=0,n=textures.getSize();i<n;i++){
                template.getFragmentShaderTemplate().addUniform(new Chars("uniform sampler2D sampler"+i+";"));
            }
        }

        @Override
        public void preDrawGL(RenderContext context, ActorProgram program) {
            final GL2ES2 gl = context.getGL().asGL2ES2();

            //bind textures
            int textureWidth = 1;
            int textureHeight = 1;
            for (int i=0,n=textures.getSize();i<n;i++){
                final Texture tex = (Texture) textures.get(i);
                final Uniform uniTexture = (Uniform) uniTextures.get(i);

                tex.loadOnGpuMemory(gl);
                final int[] reservedTexture = context.getResourceManager().reserveTextureId();
                gl.glActiveTexture(reservedTexture[0]);
                tex.bind(gl);
                uniTexture.setInt(gl, reservedTexture[1]);
                reservedTexIds.replace(i, reservedTexture);
                textureWidth = (int) tex.getExtent().getL(0);
                textureHeight = (int) tex.getExtent().getL(1);
            }
            uniPixelSize.setVec2(gl, new float[]{1.0f/textureWidth,1.0f/textureHeight});
            uniMvp.setMat4(gl, mvp.toArrayFloat());
            GLUtilities.checkGLErrorsFail(gl);
        }

        @Override
        public void postDrawGL(RenderContext context, ActorProgram program) {
            final GL2ES2 gl = context.getGL().asGL2ES2();

            //clean textures
            for (int i=0,n=textures.getSize();i<n;i++){
                final int[] reservedTexture = (int[]) reservedTexIds.get(i);
                gl.glActiveTexture(reservedTexture[0]);
                gl.glBindTexture(GL_TEXTURE_2D, 0);
                context.getResourceManager().releaseTextureId(reservedTexture[0]);
            }
        }

        @Override
        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniPixelSize = null;
            uniMvp = null;
        }

    }

}
