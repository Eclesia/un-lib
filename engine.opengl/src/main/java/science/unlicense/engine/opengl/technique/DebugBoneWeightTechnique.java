

package science.unlicense.engine.opengl.technique;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorExecutor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.Actors;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.engine.opengl.technique.actor.MaterialActor;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Display the normals stored in the mesh shell.
 *
 * @author Johann Sorel
 */
public class DebugBoneWeightTechnique extends AbstractGLTechnique {

    private static final ShaderTemplate WEIGHT_VE;
    private static final ShaderTemplate WEIGHT_FR;
    static {
        try{
            WEIGHT_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugweight-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            WEIGHT_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugweight-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private int jointIndex = 0;
    private ActorProgram program;
    private Uniform uniJointIndex;

    public DebugBoneWeightTechnique() {
    }

    public DebugBoneWeightTechnique(int jointIndex) {
        this.jointIndex = jointIndex;
    }

    public int getJointIndex() {
        return jointIndex;
    }

    public void setJointIndex(int jointIndex) {
        this.jointIndex = jointIndex;
    }

    @Override
    public void renderInternal(RenderContext context, MonoCamera camera, SceneNode node) {

        final Model model = (Model) node;

        if (program == null) {
            program = new ActorProgram();

            //build material actor
            final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
            //material.setMesh(mesh);
            material.setDiffuse(Color.RED);
            final MaterialActor materialActor = new MaterialActor(material,false);

            //build shell actor
            final Geometry shape = model.getShape();
            final ActorExecutor shellActor = Actors.buildExecutor(model, shape);
            program.setExecutor(shellActor);

            final DefaultActor debugActor = new DefaultActor(new Chars("DebugJointWeight"),false,
                    WEIGHT_VE, null, null, null, WEIGHT_FR, true, true){
                    public void preDrawGL(RenderContext context, ActorProgram program) {
                        super.preDrawGL(context, program);
                        uniJointIndex.setInt(context.getGL().asGL2ES2(), jointIndex);
                    }
                };

            program.getActors().add(shellActor);
            program.getActors().add(materialActor);
            program.getActors().add(debugActor);
            program.compile(context);
            uniJointIndex = program.getUniform(new Chars("debugJointIndex"));
        }

        program.render(context, camera, model);
    }

    @Override
    public void dispose(GLProcessContext context) {
        if (program!=null){
            program.releaseProgram(context);
        }
    }

}
