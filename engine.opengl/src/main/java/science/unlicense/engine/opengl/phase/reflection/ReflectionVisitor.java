

package science.unlicense.engine.opengl.phase.reflection;

import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.mapping.ReflectionMapping;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Affine3;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.model3d.impl.scene.Model;

/**
 * Loop on scene nodes, when a mesh with a reflection mapping is found it will
 * be updated using a reflected camera.
 *
 * @author Johann Sorel
 */
public class ReflectionVisitor extends NodeVisitor{

    //TODO : this should be relative to camera upward axis, or coordinate system up ?
    private static final Affine3 FLIP_MATRIX = new Affine3(
                1, 0, 0, 0,
                0,-1, 0, 0,
                0, 0, 1, 0);

    private final RenderPhase baseRenderPhase;
    private final ClearPhase clearPhase;
    private final DeferredRenderPhase reflectRender;
    private final float[] planEquation = new float[4];
    private final float[] n = new float[4];
    private final float[] v = new float[4];

    public ReflectionVisitor(RenderPhase baseRenderPhase) {
        this.baseRenderPhase = baseRenderPhase;
        clearPhase = new ClearPhase();
        reflectRender = new DeferredRenderPhase(null, null, null);
    }

    public float[] getPlanEquation() {
        return planEquation;
    }

    @Override
    public Object visit(Node node, Object context) {
        if (node instanceof GraphicNode){
            final GraphicNode glr = (GraphicNode) node;
            //skip nodes which are not visible
            if (!glr.isVisible()) return null;
            if (glr instanceof Model) {
                final Model model = (Model) glr;

                throw new RuntimeException("TODO, API changed");
//                final Sequence layers = mesh.getMaterial().getLayers();
//                for (int i=0,n=layers.getSize();i<n;i++) {
//                    final Layer layer = (Layer) layers.get(i);
//                    final Mapping mapping = layer.getMap();
//                    if (mapping instanceof ReflectionMapping) {
//                        try {
//                            visit(mesh, (ReflectionMapping) mapping, (GLProcessContext) context);
//                        } catch (GLException ex) {
//                            ex.printStackTrace();
//                        }
//                        break;
//                    }
//                }
            }
        }

        //loop on childrens
        return super.visit(node, context);
    }

    public void visit(Model model, ReflectionMapping mapping, GLProcessContext context) throws GLException{
        if (!model.isVisible()) return;

        //we expect the texture to be a plan
        //get the normal of the plan
        final Mesh mesh = (Mesh) model.getShape();
        final VectorRW nt = VectorNf64.createDouble(n.length);
        final VectorRW vt = VectorNf64.createDouble(v.length);
        ((VBO) mesh.getNormals()).getTuple(0, nt);
        ((VBO) mesh.getPositions()).getTuple(0, vt);
        nt.toFloat(n, 0);
        vt.toFloat(v, 0);
        n[3] = 0;
        v[3] = 1;

        final Matrix planToRootSpace = model.getRootToNodeSpace().toMatrix();
        planToRootSpace.transform(n, 0, n, 0, 1);
        planToRootSpace.transform(v, 0, v, 0, 1);

        final Plane plan = new Plane(new Vector3f64(n[0],n[1],n[2]), new Vector3f64(v[0],v[1],v[2]));
        final float d = (float) plan.getD();

        planEquation[0] = n[0];
        planEquation[1] = n[1];
        planEquation[2] = n[2];
        planEquation[3] = d;

        //calculate reflection matrix
        // http://en.wikipedia.org/wiki/Transformation_matrix : reflection
        final Affine3 reflectionMatrix = new Affine3(
                1-2*n[0]*n[0],  -2*n[0]*n[1],  -2*n[0]*n[2], -2*n[0]*d,
                 -2*n[0]*n[1], 1-2*n[1]*n[1],  -2*n[1]*n[2], -2*n[1]*d,
                 -2*n[0]*n[2],  -2*n[1]*n[2], 1-2*n[2]*n[2], -2*n[2]*d);

        final MonoCamera camera = baseRenderPhase.getCamera();
        final Affine rootToCamera = camera.getRootToNodeSpace();
        final AffineRW res = FLIP_MATRIX.multiply(rootToCamera);
        res.localMultiply(reflectionMatrix);

        final MonoCamera reflectionCamera = new MonoCamera();
        reflectionCamera.copy(camera);
        reflectionCamera.setRootToNodeSpace(res);

        mapping.update(context,
                (int) context.getViewRectangle().getWidth(),
                (int) context.getViewRectangle().getHeight());

        final SceneNode root = baseRenderPhase.getRoot();

        final FBO fbo = mapping.getGbo();

        clearPhase.setOutputFbo(fbo);
        reflectRender.setRoot(root);
        reflectRender.setCamera(reflectionCamera);
        reflectRender.setOutputFbo(fbo);

        model.setVisible(false);
        clearPhase.process(context);
        reflectRender.process(context);
        model.setVisible(true);

    }

}
