

package science.unlicense.engine.opengl.phase.shadow;

import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.engine.opengl.phase.AbstractPhase;
import science.unlicense.math.api.Maths;

/**
 * Cascaded shadow map.
 *
 * This shadow mapping technique is used for directional lights.
 * It generates a given number of shadow maps for ranges of distance.
 *
 * Resources :
 * https://msdn.microsoft.com/en-us/library/windows/desktop/ee416307(v=vs.85).aspx
 * http://developer.download.nvidia.com/SDK/10.5/opengl/src/cascaded_shadow_maps/doc/cascaded_shadow_maps.pdf
 * http://www.asawicki.info/news_1283_cascaded_shadow_mapping.html
 * ~ http://http.developer.nvidia.com/GPUGems3/gpugems3_ch10.html
 *
 * @author Johann Sorel
 */
public class CascadedShadowMapsPhase extends AbstractPhase {

    private static final double LOG_FACTOR = 0.9;
    private final MonoCamera camera;
    private final DirectionalLight light;
    private final int nbShadows;
    private final double maxDistance;

    /**
     *
     * @param camera scene camera
     * @param light directional light generating the shadows
     * @param nbShadows number of shadow maps to generate
     * @param maxDistance max shadow distance, no shadow map will be generated further
     */
    public CascadedShadowMapsPhase(MonoCamera camera, DirectionalLight light, int nbShadows, double maxDistance){
        this.camera = camera;
        this.light = light;
        this.nbShadows = nbShadows;
        this.maxDistance = maxDistance;
    }

    /**
     * Calculate for each shadow map the near/far values.
     * @return
     */
    private double[] calulateNearFarDepths(){
        final double camNear = camera.getNearPlane();
        final double camFar  = Maths.min(camera.getFarPlane(), maxDistance);
        final double camSpan = camFar-camNear;

        final double[] depths = new double[nbShadows];
        for (int i=0;i<nbShadows-1;i++){
            final double s = ((double) (i+1))/nbShadows;
            depths[i] = Maths.lerp(
                camNear + s*camSpan,
                camNear * Math.pow(camFar/camNear, s), LOG_FACTOR);
        }
        depths[nbShadows-1] = camFar;
        return depths;
    }

    private MonoCamera calulateShadowMapCamera(int i){
        //TODO

        return null;
    }

}
