
package science.unlicense.engine.opengl.mesh.particle;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.engine.opengl.mesh.AbstractGLGeometry;
import science.unlicense.engine.opengl.mesh.GLGeometry;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class ParticuleShape extends AbstractGLGeometry {

    private int nbParticule;
    private float lifeSpan;
    private double spawnRadius;
    private VectorRW gravity = new Vector3f64(0, -1, 0);
    private boolean emitting = false;
    private ParticulePresenter presenter = new ParticuleColorPresenter();

    private VBO vertices;
    private VBO times;
    private IBO index;
    private IndexedRange range;

    public ParticuleShape() {
        this(300,2,1);
    }

    public ParticuleShape(int nbParticule, float lifeSpan, double spawnRadius) {
        this.nbParticule = nbParticule;
        this.lifeSpan = lifeSpan;
        this.spawnRadius = spawnRadius;
    }

    public VectorRW getGravity() {
        return gravity;
    }

    public void setGravity(VectorRW gravity) {
        this.gravity = gravity;
    }

    public int getNbParticule() {
        return nbParticule;
    }

    public void setNbParticule(int nbParticule) {
        this.nbParticule = nbParticule;
    }

    public double getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(float lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    public double getSpawnRadius() {
        return spawnRadius;
    }

    public void setSpawnRadius(double spawnRadius) {
        this.spawnRadius = spawnRadius;
    }

    public boolean isEmitting() {
        return emitting;
    }

    public void setEmitting(boolean emitting) {
        this.emitting = emitting;
    }

    public ParticulePresenter getPresenter() {
        return presenter;
    }

    public void setPresenter(ParticulePresenter presenter) {
        this.presenter = presenter;
    }

    public VBO getVertices() {
        return vertices;
    }

    public void setVertices(VBO vertices) {
        this.vertices = vertices;
    }

    public VBO getTimes() {
        return times;
    }

    public void setTimes(VBO times) {
        this.times = times;
    }

    public IndexedRange getRange() {
        return range;
    }

    public void setRange(IndexedRange range) {
        this.range = range;
    }

    public IBO getIndex() {
        return index;
    }

    public void setIndex(IBO index) {
        this.index = index;
    }

    @Override
    public BBox getBoundingBox() {
        return null;
    }

    @Override
    public boolean isDirty() {
        return false;
    }

    @Override
    public void dispose(GL gl) {
    }

    @Override
    public GLGeometry copy() {
        throw new UnimplementedException("Not supported yet.");
    }

}
