
package science.unlicense.engine.opengl.mesh;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.MorphTarget;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;

/**
 * Defines the boundary of the mesh.
 * Vertices, normals and indices.
 *
 * @author Johann Sorel
 */
public class GLMesh extends DefaultMesh implements GLGeometry {

    public GLMesh() {
        super(CoordinateSystems.UNDEFINED_3D);
    }

    public void set(Mesh shell){
        attachments.addAll(shell.getAttributes());
        index = shell.getIndex();
        ranges = shell.getRanges();
    }

    /**
     * Create a copy using the same resources.
     *
     * @return copy
     */
    @Override
    public GLMesh copy() {
        final GLMesh copy = new GLMesh();
        copy.attachments.addAll(this.attachments);
        copy.index = this.index;
        copy.ranges = this.ranges;
        return copy;
    }

    @Override
    public boolean isDirty() {
        if (index instanceof IBO && ((IBO) index).isDirty()) return true;

        final Iterator aite = attachments.getValues().createIterator();
        while (aite.hasNext()) {
            Object cdt = aite.next();
            if (cdt instanceof VBO && ((VBO) cdt).isDirty()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void dispose(GL gl) {
        final Iterator aite = attachments.getValues().createIterator();
        while (aite.hasNext()) {
            Object cdt = aite.next();
            if (cdt instanceof VBO) {
                ((VBO) cdt).unloadFromGpuMemory(gl);
            }
        }

        if (index instanceof IBO) ((IBO) index).unloadFromGpuMemory(gl);
        for (Iterator ite=morphs.createIterator();ite.hasNext();) {
            final MorphTarget mt = (MorphTarget) ite.next();
            ((VBO) mt.getVertices()).unloadFromGpuMemory(gl);
        }
    }

}
