
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;

/**
 *
 * @author Johann Sorel
 */
public final class SphericalMapping implements Mapping {

    private Texture2D texture;

    public SphericalMapping() {
    }

    public SphericalMapping(Texture2D texture) {
        this.texture = texture;
    }

    public Texture2D getTexture() {
        return texture;
    }

    public void setTexture(Texture2D texture) {
        this.texture = texture;
    }

    @Override
    public boolean isDirty() {
        if (texture!=null) {
            return texture.isDirty();
        }
        return false;
    }

    @Override
    public void dispose(GLProcessContext context) {
        if (texture!=null) {
            texture.unloadFromGpuMemory(context.getGL());
        }
    }

}

