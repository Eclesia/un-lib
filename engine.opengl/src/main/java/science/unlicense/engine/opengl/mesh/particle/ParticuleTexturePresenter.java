

package science.unlicense.engine.opengl.mesh.particle;

import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.Actor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.shader.Uniform;

/**
 *
 * @author Johann Sorel
 */
public class ParticuleTexturePresenter implements ParticulePresenter{

    private float size;

    public ParticuleTexturePresenter(float particuleSize) {
        this.size = particuleSize;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    @Override
    public Actor createActor() {
        try {
            return new TextureActor();
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    private class TextureActor extends DefaultActor {

        private Uniform unisize;

        public TextureActor() throws IOException {
            super(new Chars("particuleTexture"), false,null, null, null,
                    Paths.resolve(new Chars("mod:/un/engine/opengl/shader/particle/particle-texture-3-ge.glsl")),
                    null,
                    true, false);
        }

        @Override
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);

            final GL2ES2 gl = context.getGL().asGL2ES2();

            //set texture size
            if (unisize==null) {
                unisize = program.getUniform(new Chars("globalSize"));
            }
            unisize.setFloat(gl, size);
        }

        @Override
        public void postDrawGL(RenderContext context, ActorProgram program) {
        }

        @Override
        public void dispose(GLProcessContext context) {
            super.dispose(context);
            unisize = null;
        }

    }

}
