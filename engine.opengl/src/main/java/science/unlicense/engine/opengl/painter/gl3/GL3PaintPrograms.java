

package science.unlicense.engine.opengl.painter.gl3;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.Actor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.engine.opengl.technique.actor.LinearGradientActor;
import science.unlicense.engine.opengl.technique.actor.PlainColorActor;
import science.unlicense.engine.opengl.technique.actor.RadialGradientActor;
import science.unlicense.engine.opengl.technique.actor.StructMaterial;
import science.unlicense.gpu.impl.opengl.shader.Shader;
import science.unlicense.gpu.impl.opengl.shader.ShaderProgram;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.gpu.impl.opengl.shader.VertexAttribute;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class GL3PaintPrograms {

    private static final Actor ACTOR_MATERIAL;
    private static final Actor ACTOR_VECTOR;
    private static final Actor ACTOR_BBOX;
    private static final Actor ACTOR_STROKE;
    private static final Actor ACTOR_TEXTURE_2D;
    private static final Actor ACTOR_TEXTURE_2DMS;
    private static final Actor ACTOR_VECTOR_MASK;
    static {
        try{
            ACTOR_MATERIAL = new DefaultActor(new Chars("VectorBase"),true,
                null, null,null,null,StructMaterial.TEMPLATE,
                true, false);
            ACTOR_VECTOR = new DefaultActor(new Chars("VectorBase"),true,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-vector-0-ve.glsl")),
                null,null,null,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-vector-4-fr.glsl")),
                true, false);
            ACTOR_BBOX = new DefaultActor(new Chars("BBoxBase"),true,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-rectangle-0-ve.glsl")),
                null,null,null,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-plaincolor-4-fr.glsl")),
                true, false);
            ACTOR_STROKE = new DefaultActor(new Chars("VectorStroke"),true,
                null,null,null,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-advanced-stroke-3-ge.glsl")),
                null,true, false);
            ACTOR_TEXTURE_2D = new DefaultActor(
                new Chars("Tex2D"),true,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-texture-0-ve.glsl")),
                null,null,null,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-texture2d-3-fr.glsl")),
                true, true);
            ACTOR_TEXTURE_2DMS = new DefaultActor(
                new Chars("Tex2DMS"),true,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-texture-0-ve.glsl")),
                null,null,null,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-texture2dms-3-fr.glsl")),
                true, true);
            ACTOR_VECTOR_MASK = new DefaultActor(new Chars("VectorMask"),true,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-trsmaskpaint-0-ve.glsl")),
                null,null,null,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-trsmaskpaint-4-fr.glsl")),
                true, false);

        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private static final Chars UNIFORM_MVP = Chars.constant("MVP");
    private static final Chars UNIFORM_MV = Chars.constant("MV");
    private static final Chars UNIFORM_P = Chars.constant("P");
    private static final Chars UNIFORM_COLOR = Chars.constant("COLOR");
    private static final Chars UNIFORM_WIDTH = Chars.constant("WIDTH");
    private static final Chars UNIFORM_IMGMVP = Chars.constant("IMGMVP");
    private static final Chars UNIFORM_SAMPLER = Chars.constant("SAMPLER");
    private static final Chars UNIFORM_NBSAMPLE = Chars.constant("NBSAMPLE");

    public StrokePlainProgram          strokePlainColorProg;
    public StrokeLinearGradientProgram strokeLinearGradientProg;
    public StrokeRadialGradientProgram strokeRadialGradientProg;
    public FillPlainColorProgram       fillPlainColorProg;
    public FillLinearGradientProgram   fillLinearGradientProg;
    public FillRadialGradientProgram   fillRadialGradientProg;
    public CreateMaskProgram           createMaskProg;
    public FillMaskProgram             fillMaskProg;
    public FillBBoxColorProgram        fillCLipBBoxProg;
    public Texture2DProgram            texture2DProg;
    public Texture2DMSProgram          texture2DMSProg;

    public static class StrokePlainProgram extends ActorProgram{
        public final Uniform uniformMV;
        public final Uniform uniformP;
        public final Uniform uniformWidth;
        public final PlainColorActor plainActor;

        public StrokePlainProgram(RenderContext renderContext) {
            plainActor = new PlainColorActor(StructMaterial.MAT_DIFFUSE,
                    StructMaterial.METHOD_SRC_OVER,new Chars("pl"),new PlainColorMapping());
            getActors().add(ACTOR_MATERIAL);
            getActors().add(ACTOR_STROKE);
            getActors().add(plainActor);
            getActors().add(ACTOR_VECTOR);
            compile(renderContext);
            uniformMV = getUniform(UNIFORM_MV);
            uniformP = getUniform(UNIFORM_P);
            uniformWidth = getUniform(UNIFORM_WIDTH);
        }
    }

    public static class StrokeLinearGradientProgram extends ActorProgram{
        public final Uniform uniformMV;
        public final Uniform uniformP;
        public final Uniform uniformWidth;
        public final LinearGradientActor lgActor;

        public StrokeLinearGradientProgram(RenderContext renderContext) {
            lgActor = new LinearGradientActor(new Chars("Painter2D-LG"),
                StructMaterial.MAT_DIFFUSE, StructMaterial.METHOD_SRC_OVER, new Chars("lg"));
            getActors().add(ACTOR_MATERIAL);
            getActors().add(ACTOR_STROKE);
            getActors().add(lgActor);
            getActors().add(ACTOR_VECTOR);
            compile(renderContext);
            uniformMV = getUniform(UNIFORM_MV);
            uniformP = getUniform(UNIFORM_P);
            uniformWidth = getUniform(UNIFORM_WIDTH);
        }
    }

    public static class StrokeRadialGradientProgram extends ActorProgram{
        public final Uniform uniformMV;
        public final Uniform uniformP;
        public final Uniform uniformWidth;
        public final RadialGradientActor rgActor;

        public StrokeRadialGradientProgram(RenderContext renderContext) {
            rgActor = new RadialGradientActor(new Chars("Painter2D-RG"),
                StructMaterial.MAT_DIFFUSE, StructMaterial.METHOD_SRC_OVER, new Chars("rg"));
            getActors().add(ACTOR_MATERIAL);
            getActors().add(ACTOR_STROKE);
            getActors().add(rgActor);
            getActors().add(ACTOR_VECTOR);
            compile(renderContext);
            uniformMV = getUniform(UNIFORM_MV);
            uniformP = getUniform(UNIFORM_P);
            uniformWidth = getUniform(UNIFORM_WIDTH);
        }
    }


    public static class FillPlainColorProgram extends ActorProgram{
        public final Uniform uniformMV;
        public final Uniform uniformP;
        public final PlainColorActor plainActor;

        public FillPlainColorProgram(RenderContext renderContext) {
            plainActor = new PlainColorActor(StructMaterial.MAT_DIFFUSE,
                    StructMaterial.METHOD_SRC_OVER,new Chars("pl"),new PlainColorMapping());
            getActors().add(ACTOR_MATERIAL);
            getActors().add(plainActor);
            getActors().add(ACTOR_VECTOR);
            compile(renderContext);
            uniformMV = getUniform(UNIFORM_MV);
            uniformP = getUniform(UNIFORM_P);
        }
    }

    public static class FillBBoxColorProgram extends ActorProgram{
        public final Uniform uniformMV;
        public final Uniform uniformP;
        public final Uniform uniformCOLOR;
        public final Uniform uniformCORNERS;

        public FillBBoxColorProgram(RenderContext renderContext) {
            getActors().add(ACTOR_BBOX);
            compile(renderContext);
            uniformMV = getUniform(UNIFORM_MV);
            uniformP = getUniform(UNIFORM_P);
            uniformCOLOR = getUniform(UNIFORM_COLOR);
            uniformCORNERS = getUniform(new Chars("CORNERS"));
        }
    }

    public static class FillLinearGradientProgram extends ActorProgram{
        public final Uniform uniformMV;
        public final Uniform uniformP;
        public final LinearGradientActor lgActor;

        public FillLinearGradientProgram(RenderContext renderContext) {
            lgActor = new LinearGradientActor(new Chars("Painter2D-LG"),
                StructMaterial.MAT_DIFFUSE, StructMaterial.METHOD_SRC_OVER, new Chars("lg"));
            getActors().add(ACTOR_MATERIAL);
            getActors().add(lgActor);
            getActors().add(ACTOR_VECTOR);
            compile(renderContext);
            uniformMV = getUniform(UNIFORM_MV);
            uniformP = getUniform(UNIFORM_P);
        }
    }

    public static class FillRadialGradientProgram extends ActorProgram{
        public final Uniform uniformMV;
        public final Uniform uniformP;
        public final RadialGradientActor rgActor;

        public FillRadialGradientProgram(RenderContext renderContext) {
            rgActor = new RadialGradientActor(new Chars("Painter2D-RG"),
                StructMaterial.MAT_DIFFUSE, StructMaterial.METHOD_SRC_OVER, new Chars("rg"));
            getActors().add(ACTOR_MATERIAL);
            getActors().add(rgActor);
            getActors().add(ACTOR_VECTOR);
            compile(renderContext);
            uniformMV = getUniform(UNIFORM_MV);
            uniformP = getUniform(UNIFORM_P);
        }
    }

    public static class CreateMaskProgram extends ShaderProgram {
        public final VertexAttribute va;
        public final Uniform uniformMV;
        public final Uniform uniformP;

        public CreateMaskProgram(RenderContext renderContext) throws IOException{
            final Shader vertexShader = new Shader(new Chars(IOUtilities.readAll(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-glyph-0-ve.glsl")).createInputStream())),Shader.SHADER_VERTEX);
            final Shader fragmentShader = new Shader(new Chars(IOUtilities.readAll(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-glyph-4-fr.glsl")).createInputStream())),Shader.SHADER_FRAGMENT);
            setShader(vertexShader, Shader.SHADER_VERTEX);
            setShader(fragmentShader, Shader.SHADER_FRAGMENT);
            loadOnGpuMemory(renderContext.getGL());
            va = getVertexAttribute(new Chars("l_position"), renderContext.getGL(), 1);
            uniformMV = getUniform(UNIFORM_MV);
            uniformP = getUniform(UNIFORM_P);
        }

    }

    public static class FillMaskProgram extends ActorProgram{
        public final Uniform uniformMVP;
        public final Uniform uniformCOLOR;
        public final Uniform uniformMASKTRS;
        public final Uniform uniformMASK;
        public final Uniform uniformBUFFER;

        public FillMaskProgram(RenderContext renderContext) {
            getActors().add(ACTOR_VECTOR_MASK);
            compile(renderContext);
            uniformMVP = getUniform(UNIFORM_MVP);
            uniformCOLOR = getUniform(new Chars("COLOR"));
            uniformMASKTRS = getUniform(new Chars("MASKTRS"));
            uniformMASK = getUniform(new Chars("SAMPLER"));
            uniformBUFFER = getUniform(new Chars("BUFFER"));
        }

    }

    public static class Texture2DProgram extends ActorProgram{
        public final Uniform uniformMVP;
        public final Uniform uniformSampler;

        public Texture2DProgram(RenderContext renderContext) {
            getActors().add(ACTOR_MATERIAL);
            getActors().add(ACTOR_TEXTURE_2D);
            compile(renderContext);
            uniformMVP = getUniform(UNIFORM_MVP);
            uniformSampler = getUniform(UNIFORM_SAMPLER);
        }
    }

    public static class Texture2DMSProgram extends ActorProgram{
        public final Uniform uniformMVP;
        public final Uniform uniformSampler;
        public final Uniform uniformNBSample;

        public Texture2DMSProgram(RenderContext renderContext) {
            getActors().add(ACTOR_MATERIAL);
            getActors().add(ACTOR_TEXTURE_2DMS);
            compile(renderContext);
            uniformMVP = getUniform(UNIFORM_MVP);
            uniformSampler = getUniform(UNIFORM_SAMPLER);
            uniformNBSample = getUniform(UNIFORM_NBSAMPLE);
        }
    }

    public GL3PaintPrograms() {
    }

    public synchronized void init(RenderContext renderContext){
        if (fillPlainColorProg!=null) return;

        try {
            strokePlainColorProg = new StrokePlainProgram(renderContext);
            strokeLinearGradientProg = new StrokeLinearGradientProgram(renderContext);
            strokeRadialGradientProg = new StrokeRadialGradientProgram(renderContext);
            fillPlainColorProg = new FillPlainColorProgram(renderContext);
            fillLinearGradientProg = new FillLinearGradientProgram(renderContext);
            fillRadialGradientProg = new FillRadialGradientProgram(renderContext);
            texture2DProg = new Texture2DProgram(renderContext);
            texture2DMSProg = new Texture2DMSProgram(renderContext);
            createMaskProg = new CreateMaskProgram(renderContext);
            fillMaskProg = new FillMaskProgram(renderContext);
            fillCLipBBoxProg = new FillBBoxColorProgram(renderContext);
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    public synchronized void release(GLProcessContext renderContext){
        if (fillPlainColorProg==null) return;

        try {
            strokePlainColorProg.releaseProgram(renderContext);
            strokePlainColorProg = null;
            strokeLinearGradientProg.releaseProgram(renderContext);
            strokeLinearGradientProg = null;
            strokeRadialGradientProg.releaseProgram(renderContext);
            strokeRadialGradientProg = null;
            fillPlainColorProg.releaseProgram(renderContext);
            fillPlainColorProg = null;
            fillLinearGradientProg.releaseProgram(renderContext);
            fillLinearGradientProg = null;
            fillRadialGradientProg.releaseProgram(renderContext);
            fillRadialGradientProg = null;
            texture2DProg.releaseProgram(renderContext);
            texture2DProg = null;
            texture2DMSProg.releaseProgram(renderContext);
            texture2DMSProg = null;
            createMaskProg.unloadFromGpuMemory(renderContext.getGL());
            createMaskProg = null;
            fillMaskProg.releaseProgram(renderContext);
            fillMaskProg = null;
            fillCLipBBoxProg.releaseProgram(renderContext);
            fillCLipBBoxProg = null;
        } catch (Exception ex) {
            Loggers.get().log(ex, science.unlicense.common.api.logging.Logger.LEVEL_WARNING);
            return;
        }
    }

}
