
package science.unlicense.engine.opengl.painter.gl3;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.MishandleException;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.painter2d.AbstractPainter2D;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Contour;
import science.unlicense.display.api.painter2d.LinearGradientPaint;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.display.api.painter2d.RadialGradientPaint;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.s2d.GraphicNode2D;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.painter.gl3.task.Clear;
import science.unlicense.engine.opengl.painter.gl3.task.ClearClip;
import science.unlicense.engine.opengl.painter.gl3.task.DefineClip;
import science.unlicense.engine.opengl.painter.gl3.task.Dispose;
import science.unlicense.engine.opengl.painter.gl3.task.FillGeometry;
import science.unlicense.engine.opengl.painter.gl3.task.FontPage;
import science.unlicense.engine.opengl.painter.gl3.task.PaintTexture2D;
import science.unlicense.engine.opengl.painter.gl3.task.PaintTexture2DMS;
import science.unlicense.engine.opengl.painter.gl3.task.PainterTask;
import science.unlicense.engine.opengl.painter.gl3.task.Release;
import science.unlicense.engine.opengl.painter.gl3.task.Resize;
import science.unlicense.engine.opengl.painter.gl3.task.StrokeGeometry;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scene.GLGeometry2D;
import science.unlicense.engine.opengl.scene.GLGeometryNode2D;
import science.unlicense.engine.opengl.scene.GLImageNode2D;
import science.unlicense.format.ttf.ttf.TTFFontStore;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.gpu.api.opengl.GL2ES2;
import static science.unlicense.gpu.api.opengl.GLC.GL_BLEND;
import static science.unlicense.gpu.api.opengl.GLC.GL_DEPTH_TEST;
import static science.unlicense.gpu.api.opengl.GLC.GL_DITHER;
import static science.unlicense.gpu.api.opengl.GLC.GL_LESS;
import static science.unlicense.gpu.api.opengl.GLC.GL_MULTISAMPLE;
import static science.unlicense.gpu.api.opengl.GLC.GL_ONE;
import static science.unlicense.gpu.api.opengl.GLC.GL_ONE_MINUS_SRC_ALPHA;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.Texture2DMS;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Affine;
import science.unlicense.math.impl.Affine2;

/**
 * OpenGL 2D painter implementation.
 *
 * @author Johann Sorel
 */
public class GL3Painter2D extends AbstractPainter2D {

    private static final TTFFontStore FONT_STORE = new TTFFontStore();

    private final DefaultGLProcessContext glContext;
    public final RenderContext renderContext;
    public FBO fbo;
    public FBO mask;
    public GL2ES2 gl;
    private boolean disposed = false;

    //programs
    public final GL3PaintPrograms programs = new GL3PaintPrograms();
    //font cache
    private final Dictionary fontPages = new HashDictionary();

    // matrix to convert to pixel coordinate system.
    public final Affine2 p = new Affine2();
    public float[] pArray;

    public final Sequence tasks = new ArraySequence();

    //font glyph cache
    public final Dictionary fontCache = new HashDictionary();

    public GL3Painter2D(int width, int height) {
        this(width,height,0);
    }

    /**
     * GLPainter 2D, providing a drawable.
     * If drawable is not nul, then ensure the flush method is called in the main
     * event loop.
     *
     * @param width
     * @param height
     * @param multisample
     */
    public GL3Painter2D(int width, int height, int multisample) {

        this.glContext = new DefaultGLProcessContext();
        this.renderContext = new RenderContext(glContext, null);

        mask =  new FBO(width,height);
        mask.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height, FontPage.VEC1_USHORT()));

        Resize.updateMatrix(this);
        setClip(null);
    }

    @Override
    public FontStore getFontStore() {
        return FONT_STORE;
    }

    protected Affine2 buildMV(Affine trs){
        if (trs!=null && !trs.isIdentity()) {
            return (Affine2) this.transform.multiply(trs);
        } else {
            return new Affine2(this.transform);
        }
    }

    public void clear(){
        clear(null);
    }

    public void clear(BBox bbox){
        addTask(new Clear(bbox,false));
    }

    public void setSize(Extent.Long size){
        addTask(new Resize((int) size.get(0),(int) size.get(1)));
    }

    public Extent.Long getSize() {
        return new Extent.Long(mask.getWidth(), mask.getHeight());
    }

    @Override
    public void setClip(PlanarGeometry geom) {
        super.setClip(geom);
        // add it in the task queue
        addTask(new DefineClip(geom));
    }

    @Override
    public void fill(PlanarGeometry geom) {
        if (geom == null || fillPaint == null) return;
        final GLGeometry2D glgeom = new GLGeometry2D(geom);
        fill(glgeom,null);
        addTask(new Release(glgeom));
    }

    private void fill(GLGeometry2D glgeom, Affine2 trs) {
        if (glgeom == null || fillPaint == null || !glgeom.calculateFill()) return;

        final Affine2 mv = buildMV(trs);

        //paint
        if (fillPaint instanceof ColorPaint || fillPaint instanceof LinearGradientPaint || fillPaint instanceof RadialGradientPaint){
            // add it in the task queue
            addTask(new FillGeometry(glgeom, fillPaint,mv, alphaBlending));
        } else {
            //TODO find a way to handle other types
            final Color rgba = Color.GREEN;
            // add it in the task queue
            addTask(new FillGeometry(glgeom, rgba,mv, alphaBlending));
        }
    }

    @Override
    public void stroke(PlanarGeometry geom) {
        if (brush == null || geom == null || fillPaint == null) return;

        final GLGeometry2D glgeom = new GLGeometry2D(geom);
        stroke(glgeom,null);
        addTask(new Release(glgeom));
    }

    private void stroke(GLGeometry2D glgeom, Affine2 trs) {
        if (brush == null || glgeom == null || fillPaint == null || !glgeom.calculateContour()) return;

        final Affine2 mv = buildMV(trs);
        final PlainBrush pb = (PlainBrush) brush;

        // add it in the task queue
        if (fillPaint instanceof ColorPaint || fillPaint instanceof LinearGradientPaint || fillPaint instanceof RadialGradientPaint){
            // add it in the task queue
            addTask(new StrokeGeometry(glgeom, fillPaint,mv, (float) pb.getMaxWidth(),alphaBlending));
        } else {
            //TODO find a way to handle other types
            final Color rgba = Color.GREEN;
            // add it in the task queue
            addTask(new StrokeGeometry(glgeom, rgba,mv, (float) pb.getMaxWidth(),alphaBlending));
        }

    }

    @Override
    public void paint(Image image, Affine trs) {
        if (image==null) return;

        final Affine mv = buildMV(trs);
        final Texture2D texture = new Texture2D(image);

        // add it in the task queue
        addTask(new PaintTexture2D(texture, mv, alphaBlending));
        addTask(new Release(texture));
    }

    public void paint(final Texture texture, Affine trs) {
        if (texture==null) return;

        final Affine2 mv = buildMV(trs);

        // add it in the task queue
        if (texture instanceof Texture2DMS){
            addTask(new PaintTexture2DMS((Texture2DMS) texture, mv, alphaBlending));
        } else {
            addTask(new PaintTexture2D((Texture2D) texture, mv, alphaBlending));
        }
    }
//
//    protected void character(int c, Affine2 trs, boolean fill) {
//
//        //calculate transform
//        Affine2 transform = getMV();
//        if (trs!=null && !trs.isIdentity()){
//            transform = (Affine2) transform.multiply(trs);
//        }
//
//        addTask(new FillMask(font, c, fillPaint, transform, alphaBlending));
//    }

    @Override
    public void flush() {
        throw new MishandleException("Use flush(GLSource) method");
    }

    public void flush(FBO fbo, GLSource source) {
        if (disposed) throw new IllegalStateException("Painter has been disposed, it is not usable anymore.");
        this.fbo = fbo;
        glContext.execute(source);

        gl = glContext.getGL().asGL2ES2();
        GLUtilities.checkGLErrorsFail(gl);

        //disable Z buffer test
        gl.glDisable(GL_DEPTH_TEST);
        gl.glDepthFunc(GL_LESS);
        //Porter-Duff SRC-OVER rule by default
        gl.glEnable(GL_BLEND);
        gl.glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        //enable AA
        //TODO improve Texture class to offer configuration parameters
        gl.glSampleCoverage(1.f, true);
        gl.glEnable(GL_DITHER);
        gl.glEnable(GL_MULTISAMPLE);

        gl.glEnable(GLC.GETSET.State.CULL_FACE);
        gl.glCullFace(GLC.CULLING.BACK);

        GLUtilities.checkGLErrorsFail(gl);

        programs.init(renderContext);

        bindFBO(fbo);

        for (int i=0,n=tasks.getSize();i<n;i++){
            final PainterTask task = (PainterTask) tasks.get(i);
            task.execute(this);
            if (task instanceof Dispose){
                disposed = true;
            }
        }
        tasks.removeAll();

        if (fbo!=null){
            fbo.unbind(gl);
        }
    }

    @Override
    public void dispose(){
        throw new MishandleException("Use dispose(GLSource) method");
    }

    public void dispose(GLSource source){
        addTask(new Dispose());
        flush(null, source);
    }

    @Override
    public void render(SceneNode node){
        visitor.visit(node, null);
    }

    private final PainterVisitor visitor = new PainterVisitor();
    private class PainterVisitor extends DefaultPainterVisitor{
        @Override
        public Object visit(Node node, Object context) {
            if (node instanceof GLGeometryNode2D){
                final Affine2 cp = getTransform();

                final GraphicNode2D gn = (GraphicNode2D) node;
                final AlphaBlending blending = gn.getBlending();
                if (blending!=null){
                    setAlphaBlending(blending);
                }

                Affine deriv = cp.multiply(gn.getNodeToRootSpace());
                setTransform(deriv);

                final GLGeometryNode2D cdt = (GLGeometryNode2D) node;
                //fills
                final Paint[] fills = cdt.getFills();
                for (int i=0;i<fills.length;i++){
                    setPaint(fills[i]);
                    fill(cdt.getGlGeometry(),null);
                }

                //contours
                final Contour[] contours = cdt.getContours();
                for (int i=0;i<contours.length;i++){
                    setBrush(contours[i].getValue1());
                    setPaint(contours[i].getValue2());
                    stroke(cdt.getGlGeometry(),null);
                }

                setTransform(cp);
            } else if (node instanceof GLImageNode2D){
                final GLImageNode2D gln = (GLImageNode2D) node;
                final Texture texture = gln.getTexture();
                if (texture!=null && (texture.getImage()!=null || texture.isOnGpuMemory())) {
                    paint(texture, gln.getNodeToRootSpace());
                }
            } else {
                return super.visit(node, context);
            }
            return null;
        }
    }

    public FontPage getFontPage(FontChoice font){
        FontPage page = (FontPage) fontPages.getValue(font.getFamilies()[0]);
        if (page==null){
            page = new FontPage(font);
            fontPages.add(font.getFamilies()[0], page);
        }
        return page;
    }

    public void addTask(PainterTask exec){
        final int size = tasks.getSize();
        if (size>0){
            if (exec instanceof ClearClip){
                //no need to have multiple clear clip next to each other
                final PainterTask previous = (PainterTask) tasks.get(size-1);
                if (previous instanceof ClearClip){
                    return;
                } else if (previous instanceof DefineClip){
                    return;
                }
            } else if (exec instanceof DefineClip){
                //defineClip override any previous clip
                //remove the older one
                final PainterTask previous = (PainterTask) tasks.get(size-1);
                if (previous instanceof DefineClip){
                    tasks.replace(size-1,exec);
                    return;
                }
            }
        }

        tasks.add(exec);
    }

    private void bindFBO(FBO fbo){
        //check the fbo is loaded
        if (fbo!=null){
            fbo.loadOnGpuMemory(gl);
            fbo.bind(gl);
            gl.glViewport(0, 0, fbo.getWidth(), fbo.getHeight());
        }
    }

    /**
     * On slow or older graphic cards, the creation of multiple gl contexts
     * crash the driver. We avoid several cases in this method.
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public static void prepareUI() throws IOException{
//        final Sequence families = FontContext.INSTANCE.getFamilies();
//        for (int i=0,n=families.getSize();i<n;i++) {
//            final Chars family = (Chars) families.get(i);
//            final Path fontPageCacheFile = getFontPageCacheFile(family);
//            if (!fontPageCacheFile.exists()) {
//                final FontPage page = new FontPage(new Font(new Chars[]{family}, 12, Font.WEIGHT_NONE));
//                page.load();
//            }
//        }
    }

    public static Path getFontPageCacheFile(Chars name) throws IOException{
        final NamedNode userHome = science.unlicense.system.System.get().getProperties()
                .getSystemTree().search(new Chars("system/user/home"));
        Path path = Paths.resolve((Chars) userHome.getValue());
        path = path.resolve(new Chars(".unlicense")).resolve(new Chars("fontcache"));
        path.createContainer();
        return path.resolve(name);
    }

}
