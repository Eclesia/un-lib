
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.engine.opengl.scene.GLGeometry2D;

/**
 *
 * @author Johann Sorel
 */
public class Dispose extends PainterTask{

    @Override
    public void execute(GL3Painter2D worker) {
        worker.programs.release(worker.renderContext);
        if (worker.fbo!=null){
            worker.fbo.unbind(worker.gl);
            worker.fbo.unloadFromGpuMemory(worker.gl);
        }

        //dispose font glyph geometries
        final Iterator ite = worker.fontCache.getValues().createIterator();
        while (ite.hasNext()){
            final GLGeometry2D next = (GLGeometry2D) ite.next();
            next.dispose(worker.gl);
        }

    }

}
