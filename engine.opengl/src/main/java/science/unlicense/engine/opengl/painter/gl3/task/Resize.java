
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;

/**
 * Resize the worker FBO, this task also perform a full clear.
 * The clear is necessary to erase the multisampled fbo
 *
 * @author Johann Sorel
 */
public class Resize extends PainterTask{

    private final int width;
    private final int height;

    public Resize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public void execute(GL3Painter2D worker) {

        updateMatrix(worker,width,height);

        //update the fbo
        if (worker.fbo!=null){
            if (worker.fbo.getWidth()==width && worker.fbo.getHeight()==height){
                //same size, no need to resize it
                return;
            }
            worker.fbo.unbind(worker.gl);
            worker.fbo.resize(worker.gl, width, height);
            //resizing cause the fbo to be unloaded, we need to reattach it
            worker.fbo.loadOnGpuMemory(worker.gl);
            if (worker.fbo!=null) worker.fbo.bind(worker.gl);
        }

        //update the mask
        if (worker.mask!=null){
            worker.mask.unbind(worker.gl);
            worker.mask.resize(worker.gl, width, height);
            worker.mask.loadOnGpuMemory(worker.gl);
            if (worker.fbo!=null) worker.fbo.bind(worker.gl);
        }

    }

    /**
     * Update the matrix values after a resize.
     */
    public static void updateMatrix(GL3Painter2D worker){
        updateMatrix(worker,worker.mask.getWidth(),worker.mask.getHeight());
    }

    public static void updateMatrix(GL3Painter2D worker, double width, double height){
        worker.p.set(0, 0, 2.0/width);
        worker.p.set(1, 1, 2.0/height);
        worker.p.set(0, 2, -1);
        worker.p.set(1, 2, -1);
        worker.pArray = worker.p.toMatrix().toArrayFloat();
    }

}
