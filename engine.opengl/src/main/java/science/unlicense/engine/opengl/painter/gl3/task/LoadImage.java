
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.gpu.impl.opengl.resource.Texture;
import science.unlicense.image.api.Image;

/**
 *
 * @author Johann Sorel
 */
public class LoadImage extends PainterTask{

    private final Texture texture;
    private Image image;

    public LoadImage(Texture texture) {
        this.texture = texture;
    }

    public Image getImage() {
        return image;
    }

    @Override
    public void execute(GL3Painter2D worker) {
        texture.loadOnSystemMemory(worker.gl);
        image = texture.getImage();
    }

}
