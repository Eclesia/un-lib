
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;

/**
 * The shader executor execute the shader program.
 * A same program could be executed in a multitude of different ways, like for instancing
 * array rendering, deferred etc... that's why it's separate from the program.
 *
 * @author Johann Sorel
 */
public interface ActorExecutor extends Actor {

    /**
     *
     * @param program
     * @param context current rendering context
     * @param camera camera associate to this context
     * @param node Node to be rendered
     */
    void render(final ActorProgram program, final RenderContext context, final MonoCamera camera, final GraphicNode node);

    /**
     * Dispose renderer and his associated resources.
     * @param context
     */
    @Override
    void dispose(GLProcessContext context);
}
