
package science.unlicense.engine.opengl.mesh;

import science.unlicense.gpu.impl.opengl.resource.VBO;

/**
 *
 * @author Johann Sorel
 */
public class InstancingShell extends GLMesh{

    protected VBO instanceTransforms;

    public InstancingShell() {
    }

    public void set(GLMesh shell) {
        setPositions(shell.getPositions());
        setNormals(shell.getNormals());
        setTangents(shell.getTangents());
        setUVs(shell.getUVs());
        setIndex(shell.getIndex());
        setRanges(shell.getRanges());
        getMorphs().replaceAll(shell.getMorphs());
    }

    public VBO getInstanceTransforms() {
        return instanceTransforms;
    }

    public void setInstanceTransforms(VBO instanceTransforms) {
        this.instanceTransforms = instanceTransforms;
    }

}
