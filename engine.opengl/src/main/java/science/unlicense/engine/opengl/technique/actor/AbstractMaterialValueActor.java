

package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;

/**
 *
 *
 * @author Johann Sorel
 */
public class AbstractMaterialValueActor extends DefaultActor{

    public static final Chars PRODUCE_MARKER = Chars.constant("$produce");
    public static final Chars METHOD_MARKER = Chars.constant("$method");
    public static final Chars PREFIX_MARKER = Chars.constant("$prefix_");

    protected final Chars produce;
    protected final Chars method;
    protected final Chars uniquePrefix;

    public AbstractMaterialValueActor(Chars baseReuseId, boolean concurrent, Chars produce, Chars method, Chars uniquePrefix,
            ShaderTemplate vt, ShaderTemplate tc, ShaderTemplate te, ShaderTemplate ge, ShaderTemplate fr) {
        super(baseReuseId.concat(produce).concat(uniquePrefix), concurrent,
                replace(vt,produce,method,uniquePrefix),
                replace(tc,produce,method,uniquePrefix),
                replace(te,produce,method,uniquePrefix),
                replace(ge,produce,method,uniquePrefix),
                replace(fr,produce,method,uniquePrefix),
                true, true);
        this.produce = produce;
        this.method = method;
        this.uniquePrefix = uniquePrefix;
    }

    protected static ShaderTemplate replace(ShaderTemplate template, Chars produce, Chars method, Chars uniquePrefix){
        if (template==null) return null;
        template = new ShaderTemplate(template);
        template.replaceTexts(PRODUCE_MARKER, produce);
        template.replaceTexts(METHOD_MARKER, method);
        template.replaceTexts(PREFIX_MARKER, uniquePrefix);
        return template;
    }

    public Chars getUniquePrefix() {
        return uniquePrefix;
    }

}
