
package science.unlicense.engine.opengl.phase.picking;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class PickResetPhase extends AbstractFboPhase {

    /**
     * Indicate nothing is at this position,
     */
    public static final int NONE = 0;
    /** Indicate there was on object at this pixel position,
     * it was not pickable */
    public static final int OCCLUSION = 1;


    //next color
    private final Dictionary index = new HashDictionary();
    private int inc = 2;

    private final Sequence pickQueries = new ArraySequence();
    private final Texture2D meshPicktexture;
    private final Texture2D vertexPicktexture;

    public PickResetPhase(Texture2D meshPicktexture, Texture2D vertexPicktexture) {
        this.meshPicktexture = meshPicktexture;
        this.vertexPicktexture = vertexPicktexture;
    }

    public void pickAt(Tuple position,EventListener listener){
        pickQueries.add(new Object[]{position,listener});
    }

    public void reset(){
        inc = 2;
        index.removeAll();
    }

    public Model get(int value){
        return (Model) index.getValue(value);
    }

    public int next(Model mesh){
        //calculate this mesh id and store it in the index
        if (mesh instanceof GLModel && ((GLModel) mesh).isPickable()){
            inc++;
            index.add(inc, mesh);
            return inc;
        } else {
            //render it as occlusion
            return OCCLUSION;
        }
    }

    @Override
    protected void processInternal(GLProcessContext ctx) throws GLException {

        if (!pickQueries.isEmpty()){
            meshPicktexture.loadOnSystemMemory(ctx.getGL());
            final Image meshImage = meshPicktexture.getImage();
            Image vertexImage = null;
            if (vertexPicktexture!=null){
                vertexPicktexture.loadOnSystemMemory(ctx.getGL());
                vertexImage = vertexPicktexture.getImage();
            }

            //answer the pick requests
            final TupleGrid tb = meshImage.getRawModel().asTupleBuffer(meshImage);
            final Iterator ite = pickQueries.createIterator();
            while (ite.hasNext()){
                final Object[] array = (Object[]) ite.next();
                final Tuple mousePose = (Tuple) array[0];
                final EventListener lst = (EventListener) array[1];
                final TupleRW color = tb.createTuple();
                tb.getTuple(new Vector2i32((int) mousePose.get(0),
                    (int) meshPicktexture.getExtent().getL(1) - (int) mousePose.get(1)), color);
                final GraphicNode node = get((int) color.get(0));

                int vid = 0;
                if (vertexImage!=null){
                    final TupleGrid vb = vertexImage.getRawModel().asTupleBuffer(vertexImage);
                    final TupleRW tuplev = vb.createTuple();
                    vb.getTuple(new Vector2i32( (int) mousePose.get(0),
                        (int) meshPicktexture.getExtent().getL(1) - (int) mousePose.get(1)), tuplev);
                    vid = (int) tuplev.get(0);
                }
                lst.receiveEvent(new Event(this, new PickMessage(node,vid)));
            }
            pickQueries.removeAll();
        }

        reset();
    }

}
