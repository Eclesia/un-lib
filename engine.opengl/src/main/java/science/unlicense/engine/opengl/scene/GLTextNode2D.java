
package science.unlicense.engine.opengl.scene;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.display.api.scene.s2d.TextNode2D;

/**
 *
 * @author Johann Sorel
 */
public class GLTextNode2D extends TextNode2D{

    @Override
    public void setText(CharArray text) {
        super.setText(text);
    }

}
