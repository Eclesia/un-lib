

package science.unlicense.engine.opengl;

import science.unlicense.common.api.functional.OutFunction;

/**
 * A GLExecutable is a task run the the OpenGL thread.
 * Those can be used to manipulate resources like allocation or disposal.
 * Could also be use to update the rendering phases and scene.
 *
 * @author Johann Sorel
 */
public abstract class GLExecutable implements OutFunction {

    /**
     * Context which executed this task.
     */
    protected GLProcessContext context;

    public void setContext(GLProcessContext context) {
        this.context = context;
    }

}
