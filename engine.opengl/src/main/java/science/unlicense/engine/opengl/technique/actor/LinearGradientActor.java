
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.painter2d.LinearGradientPaint;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.shader.ShaderUtils;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.image.api.color.Color;

/**
 *
 * @author Johann Sorel
 */
public class LinearGradientActor extends AbstractMaterialValueActor{

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/lineargradiant-4-fr.glsl"), ShaderTemplate.SHADER_TESS_CONTROL);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private static final Chars UNIFORM_GSTART = Chars.constant("GSTART");
    private static final Chars UNIFORM_GEND = Chars.constant("GEND");
    private static final Chars UNIFORM_NBSTEP = Chars.constant("NBSTEP");

    private Uniform uniformGstart;
    private Uniform uniformGend;
    private Uniform uniformNbStep;

    private LinearGradientPaint gradient;

    public LinearGradientActor(Chars baseReuseId, Chars produce, Chars method, Chars uniquePrefix) {
        super(baseReuseId, false, produce, method, uniquePrefix, null, null, null, null, SHADER_FR);
    }

    public void setGradient(LinearGradientPaint gradient) {
        this.gradient = gradient;
    }

    public LinearGradientPaint getGradient() {
        return gradient;
    }

    @Override
    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom) {
        super.initProgram(ctx, template, tess, geom);
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        uniformGstart = program.getUniform(UNIFORM_GSTART);
        uniformGend = program.getUniform(UNIFORM_GEND);
        uniformNbStep = program.getUniform(UNIFORM_NBSTEP);

        final GL2ES2 gl = context.getGL().asGL2ES2();

        //we need the coordinates in pixel cs, we are going to calculate distance with the
        //gl_fragcoord which is in this cs.
        final float[] s = new float[]{(float) gradient.getStartX(),(float) gradient.getStartY(),1};
        final float[] e = new float[]{(float) gradient.getEndX(),(float) gradient.getEndY(),1};
        uniformGstart.setVec2(gl, s);
        uniformGend.setVec2(gl, e);

        final Color[] colors = gradient.getStopColors();
        final double[] steps = gradient.getStopOffsets();
        uniformNbStep.setInt(gl, steps.length);
        for (int i=0;i<steps.length;i++){
            final Uniform us = program.getUniform(new Chars("GSTEPS["+i+"]"),gl,ShaderUtils.TYPE_FLOAT);
            final Uniform uc = program.getUniform(new Chars("GCOLORS["+i+"]"),gl,ShaderUtils.TYPE_VEC4);
            us.setFloat(gl, (float) steps[i]);
            uc.setVec4(gl, colors[i].toRGBAPreMul());
        }
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniformGstart = null;
        uniformGend = null;
        uniformNbStep = null;
    }

}
