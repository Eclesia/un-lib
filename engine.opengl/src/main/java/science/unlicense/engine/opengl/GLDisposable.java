
package science.unlicense.engine.opengl;

/**
 *
 * @author Johann Sorel
 */
public interface GLDisposable {

    /**
     * Dispose direct and associated resources.
     *
     * @param context
     */
    void dispose(GLProcessContext context);

}
