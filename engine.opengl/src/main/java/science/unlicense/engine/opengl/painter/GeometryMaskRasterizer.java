
package science.unlicense.engine.opengl.painter;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.FloatSequence;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.painter.gl3.task.FontPage;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.technique.AbstractGLTechnique;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.transform.Projections;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.api.opengl.GL4;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.gpu.impl.opengl.shader.Shader;
import science.unlicense.gpu.impl.opengl.shader.ShaderProgram;
import science.unlicense.gpu.impl.opengl.shader.VertexAttribute;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector3i32;

/**
 *
 * @author Johann Sorel
 */
public class GeometryMaskRasterizer {

    private static final int width = 512;
    private static final int height = 512;


    public static Image renderGlyph(PlanarGeometry geom) throws IOException{

        final Image[] image = new Image[1];

        final GLSource source = GLUtilities.createOffscreenSource(width, height);
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        source.getCallbacks().add(context);


        final FBO fbo = new FBO(width, height);
        fbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height,FontPage.VEC1_USHORT()));

        //build the scene
        final GraphicNode scene = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        scene.getTechniques().add(new GlyphTechnique(geom));

        context.getPhases().add(new RenderPhase(scene,new MonoCamera(),fbo));
        context.getPhases().add(new AbstractFboPhase() {
            protected void processInternal(GLProcessContext ctx) throws GLException {
                //get the result image
                fbo.getColorTexture().loadOnSystemMemory((GL4) ctx.getGL());
                image[0] = fbo.getColorTexture().getImage();
            }
        });

        //render it
        source.render();

        return image[0];
    }


    public static Image to1BitParSample(Image image) {
        //rebuild 4x4 image
        final TupleGridCursor b = image.getRawModel().asTupleBuffer(image).cursor();
        final long width = image.getExtent().getL(0);
        final long height = image.getExtent().getL(1);
        final Image all = Images.create(new Extent.Long(width*4, height*4), Images.IMAGE_TYPE_RGB);
        final TupleGrid o = all.getRawModel().asTupleBuffer(all);
        for (int x=0;x<width;x++) {
            for (int y=0;y<height;y++) {
                b.moveTo(new Vector2i32(x,y));
                final int v = (int) b.samples().get(0);

                byte r01 = (byte) ((v & 1) !=0 ? 255 : 0);
                byte r02 = (byte) ((v & 2) !=0 ? 255 : 0);
                byte r03 = (byte) ((v & 4) !=0 ? 255 : 0);
                byte r04 = (byte) ((v & 8) !=0 ? 255 : 0);
                byte r05 = (byte) ((v & 16) !=0 ? 255 : 0);
                byte r06 = (byte) ((v & 32) !=0 ? 255 : 0);
                byte r07 = (byte) ((v & 64) !=0 ? 255 : 0);
                byte r08 = (byte) ((v & 128) !=0 ? 255 : 0);
                byte r09 = (byte) ((v & 256) !=0 ? 255 : 0);
                byte r10 = (byte) ((v & 512) !=0 ? 255 : 0);
                byte r11 = (byte) ((v & 1024) !=0 ? 255 : 0);
                byte r12 = (byte) ((v & 2048) !=0 ? 255 : 0);
                byte r13 = (byte) ((v & 4096) !=0 ? 255 : 0);
                byte r14 = (byte) ((v & 8192) !=0 ? 255 : 0);
                byte r15 = (byte) ((v & 16384) !=0 ? 255 : 0);
                byte r16 = (byte) ((v & 32768) !=0 ? 255 : 0);

                o.setTuple(new Vector2i32(x*4+0,y*4+0), new Vector3i32(r01,r01,r01));
                o.setTuple(new Vector2i32(x*4+1,y*4+0), new Vector3i32(r02,r02,r02));
                o.setTuple(new Vector2i32(x*4+2,y*4+0), new Vector3i32(r03,r03,r03));
                o.setTuple(new Vector2i32(x*4+3,y*4+0), new Vector3i32(r04,r04,r04));
                o.setTuple(new Vector2i32(x*4+0,y*4+1), new Vector3i32(r05,r05,r05));
                o.setTuple(new Vector2i32(x*4+1,y*4+1), new Vector3i32(r06,r06,r06));
                o.setTuple(new Vector2i32(x*4+2,y*4+1), new Vector3i32(r07,r07,r07));
                o.setTuple(new Vector2i32(x*4+3,y*4+1), new Vector3i32(r08,r08,r08));
                o.setTuple(new Vector2i32(x*4+0,y*4+2), new Vector3i32(r09,r09,r09));
                o.setTuple(new Vector2i32(x*4+1,y*4+2), new Vector3i32(r10,r10,r10));
                o.setTuple(new Vector2i32(x*4+2,y*4+2), new Vector3i32(r11,r11,r11));
                o.setTuple(new Vector2i32(x*4+3,y*4+2), new Vector3i32(r12,r12,r12));
                o.setTuple(new Vector2i32(x*4+0,y*4+3), new Vector3i32(r13,r13,r13));
                o.setTuple(new Vector2i32(x*4+1,y*4+3), new Vector3i32(r14,r14,r14));
                o.setTuple(new Vector2i32(x*4+2,y*4+3), new Vector3i32(r15,r15,r15));
                o.setTuple(new Vector2i32(x*4+3,y*4+3), new Vector3i32(r16,r16,r16));

            }
        }

        return all;
    }

    private static class GlyphTechnique extends AbstractGLTechnique{

        private final VBO vbo;
        private final ShaderProgram program;
        private final Matrix MV;
        private final MatrixRW P;
        private final IndexedRange idx1;

        private GlyphTechnique(PlanarGeometry geom) throws IOException{
            state.setEnable(GLC.GETSET.State.COLOR_LOGIC_OP, true);

            final BBox geomBBox = geom.getBoundingBox();
            final BBox target = new BBox(2);
            target.setRange(0, -1, 1);
            target.setRange(1, -1, 1);

            MV = Projections.scaled(geomBBox, target).toMatrix();
            P = new Matrix3x3().setToIdentity();


            final FloatSequence seq = new FloatSequence();
            final PathIterator ite = geom.createPathIterator();

            final VectorRW anchor = new Vector3f64(-1,0,0);
            final VectorRW current = new Vector3f64();
            final VectorRW previous = new Vector3f64();
            final VectorRW first = new Vector3f64();
            final VectorRW control = new Vector3f64();

            seq.put(previous.toFloat());
            seq.put(previous.toFloat());

            while (ite.next()) {
                int type = ite.getType();
                if (PathIterator.TYPE_MOVE_TO==type) {
                    ite.getPosition(first);
                    ite.getPosition(current);
                } else if (PathIterator.TYPE_LINE_TO==type) {
                    ite.getPosition(current);
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    seq.put(anchor.toFloat());
                    seq.put(current.toFloat());
                    seq.put(previous.toFloat());
                } else if (PathIterator.TYPE_CLOSE==type) {
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    seq.put(anchor.toFloat());
                    seq.put(first.toFloat());
                    seq.put(previous.toFloat());
                } else if (PathIterator.TYPE_QUADRATIC==type) {
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    ite.getPosition(current);
                    seq.put(anchor.toFloat());
                    seq.put(current.toFloat());
                    seq.put(previous.toFloat());

                    // special triangle
                    ite.getFirstControl(control);
                    control.setZ(1);
                    current.setZ(1);
                    previous.setZ(1);
                    seq.put(control.toFloat());
                    seq.put(current.toFloat());
                    seq.put(previous.toFloat());

                } else {
                    throw new RuntimeException("TODO " + type);
                }

                previous.set(current);
            }

            float[] coords = seq.toArrayFloat();
            vbo = new VBO(coords,3);

            final Shader vertexShader = new Shader(new Chars(IOUtilities.readAll(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-glyph-0-ve.glsl")).createInputStream())),Shader.SHADER_VERTEX);
            final Shader fragmentShader = new Shader(new Chars(IOUtilities.readAll(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-glyph-4-fr.glsl")).createInputStream())),Shader.SHADER_FRAGMENT);
            program = new ShaderProgram();
            program.setShader(vertexShader, Shader.SHADER_VERTEX);
            program.setShader(fragmentShader, Shader.SHADER_FRAGMENT);
            idx1 = IndexedRange.TRIANGLES(0, (coords.length-6)/3);

        }

        @Override
        public void renderInternal(RenderContext context, MonoCamera camera, SceneNode node) {
            final GL4 gl = context.getGL().asGL4();

            gl.asGL1().glLogicOp(GLC.LOGIC_OP.XOR);

            program.loadOnGpuMemory(gl);
            program.enable(gl);

            final VertexAttribute va1 = program.getVertexAttribute(new Chars("l_position1"), gl, 1);
            final VertexAttribute va2 = program.getVertexAttribute(new Chars("l_position2"), gl, 1);
            final VertexAttribute va3 = program.getVertexAttribute(new Chars("l_position3"), gl, 1);
            final VertexAttribute va4 = program.getVertexAttribute(new Chars("l_position4"), gl, 1);
            final VertexAttribute va5 = program.getVertexAttribute(new Chars("l_position5"), gl, 1);
            va1.enable(gl.asGL2ES2(), vbo, 0*3*4);
            va2.enable(gl.asGL2ES2(), vbo, 1*3*4);
            va3.enable(gl.asGL2ES2(), vbo, 2*3*4);
            va4.enable(gl.asGL2ES2(), vbo, 3*3*4);
            va5.enable(gl.asGL2ES2(), vbo, 4*3*4);
            program.getUniform(new Chars("MV")).setMat3(gl.asGL2ES2(), MV.toArrayFloat());
            program.getUniform(new Chars("P")).setMat3(gl.asGL2ES2(), P.toArrayFloat());
            program.getUniform(new Chars("SIZE")).setVec2(gl.asGL2ES2(), new float[]{width,height});

            GLEngineUtils.draw(gl, idx1, null);
            program.disable(gl);
            vbo.unbind(gl);

        }

        @Override
        public void dispose(GLProcessContext context) {
        }

    }

}
