
package science.unlicense.engine.opengl.technique;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.event.Properties;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.phase.RenderContext;
import static science.unlicense.gpu.api.opengl.GLC.GL_LESS;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.shader.Shader;
import science.unlicense.gpu.impl.opengl.shader.ShaderProgram;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author Johann Sorel
 */
public class GLBlinnPhongTechnique extends GLShaderTechnique {

    private static final ShaderTemplate SHADOW_VE;
    private static final ShaderTemplate SHADOW_FR;
    private static final ShaderTemplate LIGHTS_FR;
    static {
        try{
            SHADOW_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/technique/blinnphong-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            SHADOW_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/technique/blinnphong-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
            LIGHTS_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/technique/lights-4-fr.glsl"),     ShaderTemplate.SHADER_FRAGMENT);
            SHADOW_FR.append(LIGHTS_FR);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private static final Chars TEXTURES_BITSET = Chars.constant("TEXTURES_BITSET");
    private static final Chars LIGHT_NB = Chars.constant("LIGHT_NB");
    private static final Chars LIGHT_AMBIENT = Chars.constant("LIGHT_AMBIENT");
    private static final Chars CELLSHADINGSTEPS = Chars.constant("CELLSHADINGSTEPS");

    private SimpleBlinnPhong blinnPhong;

    public GLBlinnPhongTechnique() {
        super(new ShaderProgram());
        program.setShader(new Shader(SHADOW_VE.toCharacters(-1), Shader.SHADER_VERTEX), Shader.SHADER_VERTEX);
        program.setShader(new Shader(SHADOW_FR.toCharacters(Shader.SHADER_VERTEX), Shader.SHADER_FRAGMENT), Shader.SHADER_FRAGMENT);

        state.setEnable(GLC.GETSET.State.DITHER, true);
        state.setEnable(GLC.GETSET.State.MULTISAMPLE, true);
        state.setEnable(GLC.GETSET.State.SAMPLE_ALPHA_TO_COVERAGE, true);
        state.setEnable(GLC.GETSET.State.DEPTH_TEST, true);
        state.setEnable(GLC.GETSET.State.BLEND, true);
        state.setWriteToDepth(true);
        state.setDepthFunc(GL_LESS);
    }

    public void setBlinnPhong(SimpleBlinnPhong blinnPhong) {
        this.blinnPhong = blinnPhong;
    }

    public SimpleBlinnPhong getBlinnPhong() {
        return blinnPhong;
    }

    @Override
    public GLRenderState getState() {
        final GLRenderState state = super.getState();
        state.set(blinnPhong.getState());
        return state;
    }

    @Override
    public Chars getMaterialName() {
        return blinnPhong.getMaterialName();
    }

    @Override
    public void setMaterialName(Chars name) {
        blinnPhong.setMaterialName(name);
    }

    @Override
    public Properties properties() {
        return blinnPhong.properties();
    }

    @Override
    protected void fillRenderingProperties(RenderContext context, MonoCamera camera, SceneNode node, Dictionary renderProperties) {
        super.fillRenderingProperties(context, camera, node, renderProperties);

        //fill mask of available textures
        final Model model = (Model) node;
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.view( (Material) model.getMaterials().get(0));
        int bitset = 0;
        if (material.getAmbiantTexture() != null)  bitset |= 0x01;
        if (material.getDiffuseTexture() != null)  bitset |= 0x02;
        if (material.getSpecularTexture() != null) bitset |= 0x04;
        if (material.getAlphaTexture() != null)    bitset |= 0x08;
        if (material.getNormalTexture() != null)   bitset |= 0x0F;
        renderProperties.add(TEXTURES_BITSET, bitset);
    }


//    //lights informations
//    private boolean dirty = true;
//    private String base;
//    private int nbLight;
//    private Uniform uniformLightNb;
//    private Uniform uniformLightAmbient;
//    private Uniform uniformNbCellShadingSteps;
//    private final Uniform[][] uniformLight = new Uniform[5][13];
//    private final int[][] shadowMapReserved = new int[5][2];
//    private final float[] lightAmbiant = new float[4];
//    private final float[][] lights_position = new float[5][4];
//    private final float[][] lights_diffuse = new float[5][4];
//    private final float[][] lights_specular = new float[5][4];
//    private final float[] lights_constant = new float[5];
//    private final float[] lights_linear = new float[5];
//    private final float[] lights_quadratic = new float[5];
//    private final float[] lights_falloffangle = new float[5];
//    private final float[] lights_falloffexp = new float[5];
//    private final float[][] lights_direction = new float[5][3];
//    // model matrix, projection matrix, shadowmap
//    private final Object[][] lightsExt = new Object[5][3];

//    @Override
//    public void preDrawGL(RenderContext context, ActorProgram program) {
//
//        if (dirty){
//            dirty = false;
//        }
//
//        final GL2ES2 gl = context.getGL().asGL2ES2();
//        GLUtilities.checkGLErrorsFail(gl);
//
//        //grab the uniform ids
//        if (base==null){
//            uniformLightNb = program.getUniform(LIGHT_NB);
//            uniformLightAmbient = program.getUniform(LIGHT_AMBIENT);
//            uniformNbCellShadingSteps = program.getUniform(CELLSHADINGSTEPS);
//
//            for (int i=0;i<5;i++){
//                base = "LIGHTS["+i+"].";
//                uniformLight[i][0] = program.getUniform(new Chars(base+"position"),gl, ShaderUtils.TYPE_VEC4);
//                uniformLight[i][1] = program.getUniform(new Chars(base+"diffuse"),gl, ShaderUtils.TYPE_VEC4);
//                uniformLight[i][2] = program.getUniform(new Chars(base+"specular"),gl, ShaderUtils.TYPE_VEC4);
//                uniformLight[i][3] = program.getUniform(new Chars(base+"constantAttenuation"),gl, ShaderUtils.TYPE_FLOAT);
//                uniformLight[i][4] = program.getUniform(new Chars(base+"linearAttenuation"),gl, ShaderUtils.TYPE_FLOAT);
//                uniformLight[i][5] = program.getUniform(new Chars(base+"quadraticAttenuation"),gl, ShaderUtils.TYPE_FLOAT);
//                uniformLight[i][6] = program.getUniform(new Chars(base+"spotCutoff"),gl, ShaderUtils.TYPE_FLOAT);
//                uniformLight[i][7] = program.getUniform(new Chars(base+"spotExponent"),gl, ShaderUtils.TYPE_FLOAT);
//                uniformLight[i][8] = program.getUniform(new Chars(base+"spotDirection"),gl, ShaderUtils.TYPE_VEC3);
//                uniformLight[i][9] = program.getUniform(new Chars(base+"hasShadowMap"),gl, ShaderUtils.TYPE_INT);
//                uniformLight[i][10] = program.getUniform(new Chars(base+"v"),gl, ShaderUtils.TYPE_MAT4);
//                uniformLight[i][11] = program.getUniform(new Chars(base+"p"),gl, ShaderUtils.TYPE_MAT4);
//                uniformLight[i][12] = program.getUniform(new Chars("SHADOWMAPS["+i+"]"),gl, ShaderUtils.TYPE_SAMPLER2D);
//                GLUtilities.checkGLErrorsFail(gl);
//                lights_position[i] = new float[]{0f,1f,2f,1f};
//                lights_diffuse[i] = new float[]{1f,0f,0f,1f};
//                lights_specular[i] = new float[]{1f,1f,1f,1f};
//                lights_constant[i] = 1f;
//                lights_linear[i] = 1f;
//                lights_quadratic[i] = 1f;
//                lights_falloffangle[i] = 1f;
//                lights_falloffexp[i] = 0f;
//                lights_direction[i] = new float[]{1f,1f,1f};
//            }
//        }
//
//        //update lights final RenderingContext ctx
//        findLights(context);
//
//        uniformLightNb.setInt(gl, nbLight);
//        uniformLightAmbient.setVec4(gl, lightAmbiant);
//        uniformNbCellShadingSteps.setFloat(gl, (int) material.properties().getPropertyValue(CELLSHADINGSTEPS, 0));
//        GLUtilities.checkGLErrorsFail(gl);
//
//        //TODO popssible improvement using UBO
//        // http://www.opengl.org/wiki/Uniform_Buffer_Objects
//        for (int i=0;i<nbLight;i++){
//            //vec4 position;
//            //vec4 diffuse;
//            //vec4 specular;
//            //float constantAttenuation, linearAttenuation, quadraticAttenuation;
//            //float spotCutoff, spotExponent;
//            //vec3 spotDirection;
//            gl.glUniform4fv(uniformLight[i][0].getGpuId(), lights_position[i]);
//            gl.glUniform4fv(uniformLight[i][1].getGpuId(), lights_diffuse[i]);
//            gl.glUniform4fv(uniformLight[i][2].getGpuId(), lights_specular[i]);
//            gl.glUniform1f(uniformLight[i][3].getGpuId(), lights_constant[i]);
//            gl.glUniform1f(uniformLight[i][4].getGpuId(), lights_linear[i]);
//            gl.glUniform1f(uniformLight[i][5].getGpuId(), lights_quadratic[i]);
//            gl.glUniform1f(uniformLight[i][6].getGpuId(), lights_falloffangle[i]);
//            gl.glUniform1f(uniformLight[i][7].getGpuId(), lights_falloffexp[i]);
//            gl.glUniform3fv(uniformLight[i][8].getGpuId(), lights_direction[i]);
//
//            uniformLight[i][ 9].setInt(gl, (lightsExt[i][2]!=null) ? 1:0 );
//            if (lightsExt[i][2]!=null){
//                uniformLight[i][10].setMat4(gl, ((Matrix) lightsExt[i][0]).toArrayFloat() );
//                uniformLight[i][11].setMat4(gl, ((Matrix) lightsExt[i][1]).toArrayFloat() );
//                shadowMapReserved[i] = context.getResourceManager().reserveTextureId();
//                gl.glActiveTexture(shadowMapReserved[i][0]);
//                ((Texture) lightsExt[i][2]).bind(gl);
//                uniformLight[i][12].setInt(gl, shadowMapReserved[i][1]);
//                GLUtilities.checkGLErrorsFail(gl);
//            }
//        }
//
//    }
//
//    @Override
//    public void postDrawGL(RenderContext context, ActorProgram program) {
//        final GL2ES2 gl = context.getGL().asGL2ES2();
//
//        // unbind textures
//        for (int i=0;i<nbLight;i++){
//            if (lightsExt[i][2]!=null){
//                gl.glActiveTexture(shadowMapReserved[i][0]);
//                gl.glBindTexture(GL_TEXTURE_2D, 0);
//                context.getResourceManager().releaseTextureId(shadowMapReserved[i][0]);
//            }
//        }
//        GLUtilities.checkGLErrorsFail(gl);
//    }
//
//    @Override
//    public void dispose(GLProcessContext context) {
//        super.dispose(context);
//        base = null;
//        uniformLightNb = null;
//        uniformLightAmbient = null;
//        uniformNbCellShadingSteps = null;
//        for (int x = 0; x < uniformLight.length; x++) {
//            Arrays.fill(uniformLight[x], null);
//        }
//    }
//
//    private void findLights(final RenderContext ctx){
//        final Sequence lightCol = Lights.getLights(ctx.getScene());
//        AmbientLight ambient = null;
//        final Sequence others = new ArraySequence();
//        if (lightCol!=null){
//            for (int i=0,n=lightCol.getSize();i<n;i++){
//                Light light = (Light) lightCol.get(i);
//                if (light instanceof AmbientLight){
//                    ambient = (AmbientLight) light;
//                } else if (light instanceof PointLight){
//                    others.add(light);
//                } else if (light instanceof DirectionalLight){
//                    others.add(light);
//                }
//            }
//        }
//
//        if (ambient!=null){
//            Arrays.fill(lightAmbiant, 1.0f);
//            ambient.getDiffuse().toRGBAPreMul(lightAmbiant,0);
//        } else {
//            lightAmbiant[0] = 0f;
//            lightAmbiant[1] = 0f;
//            lightAmbiant[2] = 0f;
//            lightAmbiant[3] = 1.0f;
//        }
//
//        //System.out.println("------");
//        //fill light values
//        nbLight = Maths.min(others.getSize(),5);
//        for (int i=0;i<nbLight;i++){
//            final Light light = (Light) others.get(i);
//            final Affine ntw = light.getNodeToRootSpace();
//            final VectorRW worldPosition = new Vector3d(0, 0, 0);
//            ntw.transform(worldPosition,worldPosition);
//
//            //vec4 position;
//            lights_position[i][ 0] = (float) worldPosition.get(0);
//            lights_position[i][ 1] = (float) worldPosition.get(1);
//            lights_position[i][ 2] = (float) worldPosition.get(2);
//            lights_position[i][ 3] = 1f;
//
//            //vec4 diffuse;
//            light.getDiffuse().toRGBAPreMul(lights_diffuse[i],0);
//
//            //vec4 specular;
//            light.getSpecular().toRGBAPreMul(lights_specular[i],0);
//
//            if (light instanceof DirectionalLight){
//                final DirectionalLight direct = (DirectionalLight) light;
//                final Vector direction = direct.getWorldSpaceDirection();
//                lights_direction[i][0] = (float) direction.getX();
//                lights_direction[i][1] = (float) direction.getY();
//                lights_direction[i][2] = (float) direction.getZ();
//                lights_position[i][ 3] = 0f;
//
//
//            } else if (light instanceof SpotLight){
//                final SpotLight sl = (SpotLight) light;
//                //float constantAttenuation, linearAttenuation, quadraticAttenuation;
//                lights_constant[i] = sl.getAttenuation().getConstant();
//                lights_linear[i] = sl.getAttenuation().getLinear();
//                lights_quadratic[i] = sl.getAttenuation().getquadratic();
//
//                //float spotCutoff, spotExponent;
//                lights_falloffangle[i] = sl.getFallOffAngle();
//                lights_falloffexp[i] = sl.getFallOffExponent();
//
//                //vec3 spotDirection;
//                final Vector direction = sl.getWorldSpaceDirection();
//                lights_direction[i][0] = (float) direction.getX();
//                lights_direction[i][1] = (float) direction.getY();
//                lights_direction[i][2] = (float) direction.getZ();
//
//            } else if (light instanceof PointLight){
//                final PointLight pl = (PointLight) light;
//                //float constantAttenuation, linearAttenuation, quadraticAttenuation;
//                lights_constant[i] = pl.getAttenuation().getConstant();
//                lights_linear[i] = pl.getAttenuation().getLinear();
//                lights_quadratic[i] = pl.getAttenuation().getquadratic();
//                lights_falloffangle[i] = 180f;
//
//            } else {
//                throw new RuntimeException("Unknowned light type");
//            }
//
//            //shadow map
//            final ShadowMap sm = (ShadowMap) light.getUserProperties().getValue(ShadowMap.PROPERTY_SHADOWMAP);
//            if (sm!=null && sm.getFbo().isOnGpuMemory()){
//                final MonoCamera camera = sm.getCamera();
//                lightsExt[i][0] = camera.getRootToNodeSpace();
//                lightsExt[i][1] = camera.getProjectionMatrix();
//                lightsExt[i][2] = sm.getFbo().getTexture(GLC.FBO.Attachment.COLOR_0);
//            }
//
//        }
//
//    }








}
