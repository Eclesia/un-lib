
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.LinearGradientPaint;
import science.unlicense.display.api.painter2d.RadialGradientPaint;
import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.engine.opengl.scene.GLGeometry2D;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.impl.Affine2;

/**
 * TODO review this API design.
 * Not satisfying.
 *
 * @author Johann Sorel
 */
public class FillGeometry extends PainterTask{

    private final GLGeometry2D geom;

    private final Object paint;
    private final MatrixRW mv;
    private final AlphaBlending blending;

    public FillGeometry(GLGeometry2D geom, Object paint, Affine2 mv, AlphaBlending blending) {
        this.geom = geom;
        this.paint = paint;
        this.mv = mv.toMatrix();
        this.blending = blending;
    }

    @Override
    public void execute(GL3Painter2D worker) {
        configureBlending(worker,blending);

        final VBO vertVBO = geom.getFillVBO();

        //load vbo
        vertVBO.loadOnGpuMemory(worker.gl);

        ActorProgram prog = null;
        if (paint instanceof ColorPaint){
            prog = worker.programs.fillPlainColorProg;
            worker.programs.fillPlainColorProg.plainActor.getMapping().setColor( ((ColorPaint) paint).getColor());
            prog.preExecutionGL(worker.renderContext, null);
            prog.preDrawGL(worker.renderContext);
            worker.programs.fillPlainColorProg.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
            worker.programs.fillPlainColorProg.uniformP.setMat3(worker.gl, worker.pArray);
        } else if (paint instanceof Color){
            prog = worker.programs.fillPlainColorProg;
            worker.programs.fillPlainColorProg.plainActor.getMapping().setColor((Color) paint);
            prog.preExecutionGL(worker.renderContext, null);
            prog.preDrawGL(worker.renderContext);
            worker.programs.fillPlainColorProg.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
            worker.programs.fillPlainColorProg.uniformP.setMat3(worker.gl, worker.pArray);
        } else if (paint instanceof LinearGradientPaint){
            prog = worker.programs.fillLinearGradientProg;
            worker.programs.fillLinearGradientProg.lgActor.setGradient((LinearGradientPaint) paint);
            prog.preExecutionGL(worker.renderContext, null);
            prog.preDrawGL(worker.renderContext);
            worker.programs.fillLinearGradientProg.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
            worker.programs.fillLinearGradientProg.uniformP.setMat3(worker.gl, worker.pArray);
        } else if (paint instanceof RadialGradientPaint){
            prog = worker.programs.fillRadialGradientProg;
            worker.programs.fillRadialGradientProg.rgActor.setGradient((RadialGradientPaint) paint);
            prog.preExecutionGL(worker.renderContext, null);
            prog.preDrawGL(worker.renderContext);
            worker.programs.fillRadialGradientProg.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
            worker.programs.fillRadialGradientProg.uniformP.setMat3(worker.gl, worker.pArray);
        }

        worker.gl.glEnableVertexAttribArray(0);
        vertVBO.bind(worker.gl, 0);
        GLUtilities.checkGLErrorsFail(worker.gl);

        worker.gl.glDrawArrays(GL_TRIANGLES,0,vertVBO.getTupleCount());
        GLUtilities.checkGLErrorsFail(worker.gl);
        worker.gl.glDisableVertexAttribArray(0);

        prog.postDrawGL(worker.renderContext);

    }

}
