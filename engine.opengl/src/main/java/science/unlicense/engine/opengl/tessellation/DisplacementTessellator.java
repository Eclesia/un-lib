
package science.unlicense.engine.opengl.tessellation;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.AbstractActor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.gpu.api.opengl.GL1;
import science.unlicense.gpu.api.opengl.GL2ES2;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.math.api.Maths;

/**
 * Displacement mapping tessellation.
 * This technique uses a texture in tessellation phase to
 * offset vertices along the normal.
 *
 * @author Johann Sorel
 */
public class DisplacementTessellator extends AbstractActor implements Tessellator{

    static final ShaderTemplate SHADER_TC;
    static final ShaderTemplate SHADER_TE;
    static {
        try{
            SHADER_TC = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/tessdisplacement-1-tc.glsl"), ShaderTemplate.SHADER_TESS_CONTROL);
            SHADER_TE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/tessdisplacement-2-te.glsl"), ShaderTemplate.SHADER_TESS_EVAL);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private Texture2D texture;
    private int[] reservedTexture;
    private boolean dirty = true;
    private float factor = 1.0f;
    private float resolution = 20f;

    public DisplacementTessellator(Texture2D texture) {
        super(new Chars("DisplacementTess"));
        this.texture = texture;
    }

    @Override
    public int getMinGLSLVersion() {
        return Maths.max(
                SHADER_TC.getMinGLSLVersion(),
                SHADER_TE.getMinGLSLVersion());
    }

    public Texture2D getTexture() {
        return texture;
    }

    public void setTexture(Texture2D texture) {
        this.texture = texture;
    }

    public void setFactor(float factor) {
        this.factor = factor;
    }

    public float getFactor() {
        return factor;
    }

    public void setResolution(float resolution) {
        this.resolution = resolution;
    }

    public float getResolution() {
        return resolution;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isDirty() {
        return dirty;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean usesGeometryShader() {
        return false;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean usesTesselationShader() {
        return true;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void initProgram(final RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate tessControlShader = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate tessEvalShader = template.getTesselationEvalShaderTemplate();
        dirty = false;
        tessControlShader.append(SHADER_TC);
        tessEvalShader.append(SHADER_TE);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        //load texture, image may have changed, no effect if already loaded
        texture.loadOnGpuMemory(gl);

        final Uniform uniDispFactor = program.getUniform(new Chars("displacementFactor"));
        uniDispFactor.setFloat(gl, factor);

        final Uniform uniDispResolution = program.getUniform(new Chars("displacementResolution"));
        uniDispResolution.setFloat(gl, resolution);

        // Bind textures
        final Uniform uniDispTex = program.getUniform(new Chars("displacementTex"));
        reservedTexture = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTexture[0]);
        texture.bind(gl);
        uniDispTex.setInt(gl, reservedTexture[1]);

    }

    @Override
    public void postDrawGL(RenderContext context, ActorProgram program) {
        super.postDrawGL(context, program);
        final GL1 gl = context.getGL().asGL1();
        gl.glDisable (GL_BLEND);
        GLUtilities.checkGLErrorsFail(gl);

        // unbind textures
        gl.glActiveTexture(reservedTexture[0]);
        gl.glBindTexture(GL_TEXTURE_2D, 0);
        context.getResourceManager().releaseTextureId(reservedTexture[0]);

    }

}
