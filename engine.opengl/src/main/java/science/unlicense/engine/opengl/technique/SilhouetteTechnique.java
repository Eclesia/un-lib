

package science.unlicense.engine.opengl.technique;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.model3d.impl.MeshUtilities;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorExecutor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.Actors;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.engine.opengl.technique.actor.MaterialActor;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * Nice explication here :
 * http://prideout.net/blog/?p=54
 *
 * Shader is derived from prideout.
 *
 * @author Johann Sorel
 */
public class SilhouetteTechnique extends AbstractGLTechnique {

    private static final ShaderTemplate SILHOUETTE_GE;
    static {
        try{
            SILHOUETTE_GE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/silhouette-3-ge.glsl"), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final Model model;
    private ActorProgram program;
    private final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
    private float borderWidth;
    private Uniform uniWidth;

    /**
     *
     * @param model
     * @param color silhouette border color
     * @param borderWidth silhouette border width
     */
    public SilhouetteTechnique(Model model, Color color, float borderWidth) {
        this.model = model;
        this.borderWidth = borderWidth;

        //TODO find a different way, avoid modifying the base mesh if possible
        final Mesh shape = (Mesh) model.getShape();

        MeshUtilities.convertToTriangleAdjency((DefaultMesh) shape);
        //prepare program

        //build material actor

        material.setDiffuse(color);
        final MaterialActor materialActor = new MaterialActor(material,false);

        //build shell actor
        final ActorExecutor shellActor = Actors.buildExecutor(model, shape);

        //silhouette actor
        final SilhouetteActor silhouetteActor = new SilhouetteActor();

        program = new ActorProgram();
        program.getActors().add(silhouetteActor);
        program.getActors().add(shellActor);
        program.getActors().add(materialActor);
        program.setExecutor(shellActor);
    }

    /**
     * Set silhouette border color.
     *
     * @param c
     */
    public void setColor(Color c){
        material.setDiffuse(c);
    }

    /**
     * Set silhouette border width.
     *
     * @param borderWidth
     */
    public void setBorderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
    }

    @Override
    public void renderInternal(RenderContext context, MonoCamera camera, SceneNode node) {

        if (!program.isOnGpuMemory()){
            program.compile(context);
        }

        program.render(context, camera, model);

    }

    @Override
    public void dispose(GLProcessContext context) {
        if (program!=null){
            program.releaseProgram(context);
        }
    }

    private class SilhouetteActor extends DefaultActor{

        public SilhouetteActor() {
            super(new Chars("Silhouette"),false,null,null,null,SILHOUETTE_GE,null,true,false);
        }

        @Override
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            if (uniWidth==null){
                uniWidth = program.getUniform(new Chars("borderWidth"));
            }
            uniWidth.setFloat(context.getGL().asGL2ES2(), borderWidth);
        }

        @Override
        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniWidth = null;
        }

    }

}
