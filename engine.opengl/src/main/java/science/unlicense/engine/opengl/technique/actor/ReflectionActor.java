
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.mapping.ReflectionMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GL2ES2;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.shader.Uniform;

/**
 *
 * @author Johann Sorel
 */
public class ReflectionActor extends AbstractMaterialValueActor{

    private static final ShaderTemplate DIFFUSE_TEXTURE_FR;
    static {
        try{
            DIFFUSE_TEXTURE_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/reflection-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final ReflectionMapping mapping;


    //GL loaded informations
    private int[] reservedTexture;
    private Uniform unitex;
    private Uniform uniSize;

    public ReflectionActor(Chars produce, Chars method, Chars uniquePrefix, ReflectionMapping mapping) {
        super(new Chars("Reflection"),false, produce, method, uniquePrefix,
                null,null,null,null,DIFFUSE_TEXTURE_FR);
        this.mapping = mapping;
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final FBO reflectionFbo = mapping.getGbo();
        if (reflectionFbo==null) return;

        final Texture2D texture = (Texture2D) reflectionFbo.getColorTexture();
        //load texture, image may have changed, no effect if already loaded
        texture.loadOnGpuMemory(gl);

        // Bind textures
        if (unitex == null){
            unitex = program.getUniform(uniquePrefix.concat(new Chars("tex")));
            uniSize = program.getUniform(uniquePrefix.concat(new Chars("screensize")));
        }
        reservedTexture = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTexture[0]);
        texture.bind(gl);
        unitex.setInt(gl, reservedTexture[1]);
        uniSize.setVec2(gl, new float[]{(float) context.getViewRectangle().getWidth(),(float) context.getViewRectangle().getHeight()});

    }

    @Override
    public void postDrawGL(RenderContext context, ActorProgram program) {
        super.postDrawGL(context, program);
        final GL gl = context.getGL();
        GLUtilities.checkGLErrorsFail(gl);

        // unbind textures
        if (reservedTexture!=null){
            //texture may be null if the reflection fbo isn't here
            //this case can happen when multiply mirrors are in the scene
            //the first mirror will render the next ones even if they don't have a texture.
            gl.asGL1().glActiveTexture(reservedTexture[0]);
            gl.asGL1().glBindTexture(GL_TEXTURE_2D, 0);
            context.getResourceManager().releaseTextureId(reservedTexture[0]);
        }

    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        unitex = null;
        uniSize = null;
    }

}
