

package science.unlicense.engine.opengl.phase.shadow;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.gpu.api.GLException;

/**
 * TODO
 *
 * @author Johann Sorel
 */
public class LightspacePerspectiveShadowMapsPhase extends AbstractFboPhase{

    private final ShadowMapVisitor visitor;
    private SceneNode scene;

    public LightspacePerspectiveShadowMapsPhase(SceneNode scene, MonoCamera camera, int textureSize) {
        this(Chars.EMPTY,scene,camera,textureSize);
    }

    public LightspacePerspectiveShadowMapsPhase(Chars id, SceneNode scene, MonoCamera camera, int textureSize) {
        super(id);
        this.scene = scene;
        visitor = new ShadowMapVisitor(textureSize);
        visitor.setCamera(camera);
    }

    public SceneNode getScene(){
        return scene;
    }

    public void setScene(SceneNode scene) {
        this.scene = scene;
    }

    public MonoCamera getCamera() {
        return visitor.getCamera();
    }

    public void setCamera(MonoCamera camera) {
        visitor.setCamera(camera);
    }

    public void setTextureSize(int size){
        visitor.setTextureSize(size);
    }

    public int getTextureSize(int size){
        return visitor.getTextureSize();
    }

    @Override
    protected void processInternal(GLProcessContext ctx) throws GLException {
        visitor.visit(scene, ctx);
    }

}
