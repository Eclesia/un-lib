

package science.unlicense.engine.opengl.control;

import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.model3d.impl.navmap.NavMap;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Enforce player navigation map.
 *
 * @author Johann Sorel
 */
public class PlayerNavigationMapConstraint implements Updater {

    private final NavMap navMap;
    private final GraphicNode node;
    private final VectorRW lastPosition = VectorNf64.createDouble(3);

    public PlayerNavigationMapConstraint(NavMap navMap, GraphicNode node) {
        this.navMap = navMap;
        this.node = node;
    }

    @Override
    public synchronized void update(DisplayTimerState context) {
        final VectorRW position = VectorNf64.create(node.getNodeTransform().getTranslation());
        final VectorRW buffer = VectorNf64.createDouble(3);

        navMap.navigate(lastPosition, position.subtract(lastPosition), buffer);
        lastPosition.set(buffer);
        if (!buffer.equals(position)) {
            node.getNodeTransform().getTranslation().set(buffer);
            node.getNodeTransform().notifyChanged();
        }
    }

}
