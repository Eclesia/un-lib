

package science.unlicense.engine.opengl.painter;

import science.unlicense.display.api.painter2d.ImagePainter2D;
import science.unlicense.display.api.painter2d.PainterManager;
import science.unlicense.engine.opengl.painter.gl3.GL3ImagePainter2D;

/**
 * OpenGL image painter.
 *
 * @author Johann Sorel
 */
public class GLPainterManager implements PainterManager {

    @Override
    public ImagePainter2D createPainter(int width, int height) {
        return new GL3ImagePainter2D(width, height, null);
    }

}
