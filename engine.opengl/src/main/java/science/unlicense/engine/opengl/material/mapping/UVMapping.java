
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.TextureUtils;
import science.unlicense.image.api.Image;

/**
 * UV buffer is declared on the Mesh.
 * Can handle sprite.
 *
 * @author Johann Sorel
 */
public class UVMapping implements Mapping {

    private volatile Texture2D texture;
    private Boolean opaque = null;
    private SpriteInfo sprite = null;

    public UVMapping() {
    }

    public UVMapping(Texture2D texture) {
        this.texture = texture;
    }

    public SpriteInfo getSprite() {
        return sprite;
    }

    public void setSprite(SpriteInfo sprite) {
        this.sprite = sprite;
    }

    public boolean isOpaque() {
        if (opaque==null) checkOpaque();
        return opaque;
    }

    private void checkOpaque() {
        //check opacity
        opaque = true;
        final Image image = texture.getImage();
        if (image==null) return;
        opaque = TextureUtils.isOpaque(image);
    }

    public Texture2D getTexture() {
        return texture;
    }

    public void setTexture(Texture2D texture) {
        this.texture = texture;
    }

    @Override
    public boolean isDirty() {
        if (texture!=null) {
            return texture.isDirty();
        }
        return false;
    }

    @Override
    public void dispose(GLProcessContext context) {
        if (texture!=null) {
            texture.unloadFromGpuMemory(context.getGL());
        }
    }

}
