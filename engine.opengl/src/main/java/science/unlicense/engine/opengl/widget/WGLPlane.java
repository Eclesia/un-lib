
package science.unlicense.engine.opengl.widget;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Hasher;
import science.unlicense.common.api.collection.HasherDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeMessage;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLDisposable;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.GLMesh;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.operation.IntersectionOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Display a UI container.
 * Will forward the WFrame mouse and keyboard events
 *
 * TODO : reuse/merge with WGLView
 *
 * @author Johann Sorel
 */
public class WGLPlane extends GLModel implements GLDisposable, EventListener{

    private static final Image EMPTY = Images.create(new Extent.Long(1, 1, 1),Images.IMAGE_TYPE_RGBA);
    private final WContainer container = new WContainer();

    //contains children with special 3d requierements
    private final Dictionary children3d = new HasherDictionary(Hasher.IDENTITY);
    private final Collection toDispose = new ArraySequence();

    private final Triangle plan = new DefaultTriangle(
            new Vector3f64(0,0,0),
            new Vector3f64(0,1,0),
            new Vector3f64(1,1,0));
    private Logger logger = Loggers.get();

    //event informations
    private boolean mouseInPlan = false;
    private final MonoCamera camera;

    //texture updater
    private final WGLView.Value link = new WGLView.Value(){

        private Texture2D tex1 = null;
        private Texture2D tex2 = null;

        @Override
        public void swapTexture(Image base) {
//            UVMapping mapping = (UVMapping) getMaterial().getLayer(Layer.TYPE_DIFFUSE).getMapping();
//            mapping.setTexture((Texture2D) base);

            //swap texture buffer
//            tex2 = tex1;
//            tex1 = base;
//            return tex2;
        }
    };

    private WGLView view;

    public WGLPlane(final int width, final int height, MonoCamera camera) {
        this(width,height,4,camera);
    }

    public WGLPlane(final int width, final int height, int nbSample, MonoCamera camera) {
        this.camera = camera;

        //make container listen to the WPlan events
        addEventListener(MouseMessage.PREDICATE, container);
        addEventListener(KeyMessage.PREDICATE, container);

        final Buffer.Float32 vertices = DefaultBufferFactory.INSTANCE.createFloat32(12);
        vertices.write(0, new float[]{
            0,0,0,
            0,1,0,
            1,1,0,
            1,0,0
        });
        final Buffer.Float32 normals = DefaultBufferFactory.INSTANCE.createFloat32(12);
        normals.write(0, new float[]{
            0,1,0,
            0,1,0,
            0,1,0,
            0,1,0
        });

        final Buffer.Int32 indices = DefaultBufferFactory.INSTANCE.createInt32(6);
        indices.write(0, new int[]{
            1,0,3,
            3,2,1
        });

        final Buffer.Float32 uv = DefaultBufferFactory.INSTANCE.createFloat32(8);
        uv.write(0, new float[]{
            0,1,
            0,0,
            1,0,
            1,1
        });

        final GLMesh shell = new GLMesh();
        shell.setPositions(new VBO(vertices.getBuffer(),3));
        shell.setNormals(new VBO(normals.getBuffer(),3));
        shell.setIndex(new IBO(indices.getBuffer()));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 6)});
        shell.setUVs(new VBO(uv.getBuffer(),2));
        setShape(shell);


        final UVMapping texture = new UVMapping(new Texture2D(EMPTY,false,false));
        SimpleBlinnPhong.view((Material) getMaterials().get(0)).setDiffuseTexture(new Layer(texture));

        //listen to container events to update 3d sub nodes
        container.addEventListener(NodeMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                NodeMessage ne = (NodeMessage) event.getMessage();
                while (ne!=null){
                    //TODO bug in events here, no sub events
//                    if (ne.getType() == NodeEvent.TYPE_UPDATE){
//                        final Event sub = ne.getTrigger();
//                        if (sub instanceof NodeEvent){
//                            ne = (NodeEvent) sub;
//                        } else {
//                            ne=null;
//                        }
//                    } else {
                        //update 3d children tree
                        sync=false;
                        break;
//                    }
                }
            }
        });
        container.addEventListener(PropertyMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                if (Widget.PROPERTY_DIRTY.equals(pe.getPropertyName())){
                    link.putDirty((BBox) pe.getNewValue());
                }
            }
        });

        //updater will refresh the image at proper time in rendering cycle
        getUpdaters().add(new PlanUpdater());

        //set the container size
        container.setInlineStyle(new Chars("background:{fill-paint:$color-background;};"));
        container.setEffectiveExtent(new Extent.Double(width,height));
                container.getNodeTransform().setToTranslation(new double[]{width/2.0,height/2.0});
        container.setDirty();
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    /**
     * Get the UI widget.
     * @return WContainer
     */
    public WContainer getContainer() {
        return container;
    }

    /**
     * Convert the mouse event from 3d space to widget space
     * @param me 3d space mouse event
     * @return MouseEvent or null if out of plan
     */
    private void transposeToWidget(Tuple position, MouseMessage me, KeyMessage ke){
        if (position==null) return;

        //adjust position with camera world and render area
        final MonoCamera cam = camera;
        final Rectangle renderArea = cam.getRenderArea();
        final double height = renderArea.getY()+renderArea.getHeight();
        double diffV = 0;
        final GLProcessContext context = null;
        if (context!=null){ //TODO not nice, should find another way to obtain the window size
            diffV = context.getGLRectangle().getHeight()-height;
        }
        final Ray ray = cam.calculateRayWorldCS((int) (position.get(0)-renderArea.getX()), (int) (position.get(1)-diffV));

        //convert ray to plan space
        final Matrix wm = getRootToNodeSpace().toMatrix();
        final VectorRW temp = new VectorNf64(ray.getPosition(),1);
        wm.transform(temp,temp);
        ray.getPosition().set(temp.getXYZ());
        temp.set(ray.getDirection());
        temp.setZ(0);
        wm.transform(temp,temp);
        ray.getDirection().set(temp.getXYZ());

        //calculate intersection point
        TupleRW inter;
        try {
            inter = ((Point) new IntersectionOp(ray, plan).execute()).getCoordinate();
        } catch (OperationException ex) {
            Loggers.get().log(ex, Logger.LEVEL_WARNING);
            return;
        }

        if (inter==null || Double.isNaN(inter.get(0)) || Double.isNaN(inter.get(1)) ){
            //no intersection
            return;
        }

        //this is in 3d plan space
        inter = new Vector3f64(inter.get(0), inter.get(1),1);

        //convert to image space
        final int pixelWidth = (int) container.getEffectiveExtent().get(0);
        final int pixelHeight = (int) container.getEffectiveExtent().get(1);
        inter.set(0,inter.get(0) * pixelWidth);
        inter.set(1,inter.get(1) * -pixelHeight + pixelHeight);
        final Extent extent = container.getEffectiveExtent();

        //convert to container transform space
        Tuple containerCSPoint = container.getNodeTransform().inverseTransform(inter,null);

        if (inter.get(0)<0 || inter.get(0)>extent.get(0) || inter.get(1)<0 || inter.get(1)>extent.get(1)){
            //out of plan
            if (mouseInPlan && me!=null){
                //mouse was in the plan, send a mouse exit event
                final MouseMessage event = new MouseMessage(MouseMessage.TYPE_EXIT, -1, -1, containerCSPoint, me.getMouseScreenPosition(), 0,false);
                mouseInPlan = false;
                if (hasListeners()) getEventManager().sendEvent(new Event(this, event));
            }
        } else {
            if (me!=null){
                if (mouseInPlan){
                    //mouse was already in the plan, just propage the event
                    final MouseMessage event = new MouseMessage(me.getType(), me.getButton(), me.getNbClick(), containerCSPoint, me.getMouseScreenPosition(), me.getWheelOffset(),me.isDragging());
                    if (hasListeners()) getEventManager().sendEvent(new Event(this,event));
                    if (event.isConsumed()) me.consume();
                } else {
                    //mouse was out of the plan, we first generate a mouse enter event
                    mouseInPlan = true;
                    final MouseMessage enterEvent = new MouseMessage(MouseMessage.TYPE_ENTER, -1, -1, containerCSPoint, me.getMouseScreenPosition(),0,me.isDragging());
                    if (hasListeners()) getEventManager().sendEvent(new Event(this,enterEvent));
                    //then propage the true event
                    final MouseMessage event = new MouseMessage(me.getType(), me.getButton(), me.getNbClick(), containerCSPoint, me.getMouseScreenPosition(),me.getWheelOffset(),me.isDragging());
                    if (hasListeners()) getEventManager().sendEvent(new Event(this,event));
                    if (event.isConsumed()) me.consume();
                }
            }
            if (ke!=null){
                if (mouseInPlan && hasListeners()){
                    getEventManager().sendEvent(new Event(this, ke));
                }
            }
        }

    }

    @Override
    public void dispose(GLProcessContext context) {
        view.dispose();
        super.dispose(context);
    }

    // sync possible 3D child nodes ////////////////////////////////////////////

    private boolean sync = false;

    @Override
    protected Object getChildNode(int index) {
        sync3DChildren();
        return super.getChildNode(index);
    }

    private synchronized void sync3DChildren(){
        if (sync) return;
        sync = true;

        final Collection keys = children3d.getKeys();

        final WContainer root = getContainer();
        NodeVisitor visitor = new NodeVisitor() {
            @Override
            public Object visit(Node node, Object context) {
                super.visit(node, context);
                if (node instanceof WGLNode){
                    final WGLNode glnode = (WGLNode) node;
                    SceneNode scaleNode = (SceneNode) children3d.getValue(glnode);
                    if (scaleNode==null){
                        //new node
                        final int pixelWidth = (int) container.getEffectiveExtent().get(0);
                        final int pixelHeight = (int) container.getEffectiveExtent().get(1);
                        final Matrix4x4 nodeToImage3D = new Matrix4x4().setToIdentity();
                        nodeToImage3D.set(0, 0, pixelWidth);
                        nodeToImage3D.set(1, 1, -pixelHeight);
                        nodeToImage3D.set(1, 3, pixelHeight);

                        final GraphicNode inSceneNode = glnode.getInSceneNode();
                        scaleNode = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
                        scaleNode.getChildren().add(inSceneNode);
                        scaleNode.getNodeTransform().set(nodeToImage3D.invert());
                        children3d.add(glnode, scaleNode);
                        getChildren().add(scaleNode);
                    } else {
                        //already existing node
                        keys.remove(glnode);
                    }
                }
                return null;
            }
        };
        visitor.visit(root, null);

        //dispose old childrens
        synchronized(toDispose) {
            toDispose.addAll(keys);
        }
    }

    public Texture2D getTexture(){
        return SimpleBlinnPhong.view((Material) getMaterials().get(0)).getDiffuseTexture().getTexture();
    }

    // events //////////////////////////////////////////////////////////////////

    private Tuple position = null;

    @Override
    public void receiveEvent(Event event) {

        if (!isVisible()) return;

        //catch frame mouse event and propagate to widgets
        final Class eventClass = event.getClass();
        if (eventClass == MouseMessage.class){
            final MouseMessage e = (MouseMessage) event.getMessage();
            position = e.getMousePosition();
            transposeToWidget(position,e,null);
        } else if (eventClass == KeyMessage.class){
            final KeyMessage e = (KeyMessage) event.getMessage();
            transposeToWidget(position,null,e);
        }

    }

    @Override
    public Class[] getEventClasses() {
        return new Class[]{
            MouseMessage.class,
            KeyMessage.class
        };
    }

    private class PlanUpdater implements Updater {

        @Override
        public void update(DisplayTimerState timer) {

            final RenderContext context = (RenderContext) timer;

            if (view == null) {
                final GLSource sharedDrawable = null;//GLUtilities.createDummyDrawable(context.getGLDrawable());
                final DefaultGLProcessContext sharedCtx = new DefaultGLProcessContext();
                sharedDrawable.getCallbacks().add(sharedCtx);

                view = new WGLView(1, 1, 4, sharedCtx, link);
                view.setLogger(logger);
            }

            synchronized (toDispose) {
                for (Iterator ite=toDispose.createIterator();ite.hasNext();){
                    final Object key = ite.next();
                    final SceneNode todisp = (SceneNode) children3d.remove(key);
                    getChildren().remove(todisp);
                    GLEngineUtils.dispose(todisp, context);
                }
                toDispose.removeAll();
            }

            if (!isVisible()) return;

        }
    }

}
