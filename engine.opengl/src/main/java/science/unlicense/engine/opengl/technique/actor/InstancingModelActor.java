
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.InstancingShell;
import science.unlicense.engine.opengl.phase.RenderContext;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_M;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_P;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_PIXELSIZE;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_V;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GL2ES2;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.gpu.impl.opengl.shader.VertexAttribute;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.impl.Affine3;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class InstancingModelActor extends ModelActor implements ActorExecutor {

    public static final Chars UNIFORM_L = Chars.constant("UNI_L");

    private static final Chars UID = Chars.constant("InstancingShell");
    private static final Chars LAYOUT_INSTANCE_TRS = Chars.constant("l_insttrs");
    private static final Affine IDENTITY = new Affine3();

    private static final ShaderTemplate SHADER_VE;
    private static final ShaderTemplate SHADER_GE;
    static {
        try{
            SHADER_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/instancing-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            SHADER_GE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/instancing-3-ge.glsl"), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final InstancingShell shell;
    private VertexAttribute attInstanceTransform;

    public InstancingModelActor(Model mesh) {
        super(mesh);
        this.shell = (InstancingShell) mesh.getShape();
    }

    @Override
    public Chars getReuseUID() {
        return UID;
    }

    @Override
    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate vs = template.getVertexShaderTemplate();
        final ShaderTemplate tc = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate te = template.getTesselationEvalShaderTemplate();
        final ShaderTemplate gs = template.getGeometryShaderTemplate();
        final ShaderTemplate fg = template.getFragmentShaderTemplate();

        vs.append(SHADER_VE);
        if (tess) tc.append(MeshActor.TEMPLATE_TC);
        if (tess) te.append(MeshActor.TEMPLATE_TE);
        if (geom){
            gs.append(MeshActor.TEMPLATE_GE);
            gs.append(SHADER_GE);
        }
        if (fg!=null){
            fg.addUniform(new Chars("uniform mat4 ").concat(MeshActor.UNIFORM_V).concat(';'));
            fg.addOperation(new Chars("    M = inData.M;"));
            fg.addOperation(MeshActor.OP_SET_V);
            fg.append(MeshActor.TEMPLATE_FR);
        }

        vs.append(SKIN_VE);

        // build program
        if (shell.getPositions()!=null){
            //always set the layout, might be used by some other shader actor
            vs.addLayout(PROG_LAYOUT_VERTEX);
            vs.addVariableOut(PROG_OUT_VERTEX_MODEL); vs.addOperation(PROG_OP_VERTEX_MODEL);
            vs.addVariableOut(PROG_OUT_VERTEX_WORLD); vs.addOperation(PROG_OP_VERTEX_WORLD);
            vs.addVariableOut(PROG_OUT_VERTEX_CAMERA); vs.addOperation(PROG_OP_VERTEX_CAMERA);
            vs.addVariableOut(PROG_OUT_VERTEX_PROJ); vs.addOperation(PROG_OP_VERTEX_PROJ);
        }
        if (shell.getNormals()!=null){
            vs.addLayout(PROG_LAYOUT_NORMAL);
            vs.addVariableOut(PROG_OUT_NORMAL_MODEL); vs.addOperation(PROG_OP_NORMAL_MODEL);
            vs.addVariableOut(PROG_OUT_NORMAL_WORLD); vs.addOperation(PROG_OP_NORMAL_WORLD);
            vs.addVariableOut(PROG_OUT_NORMAL_CAMERA); vs.addOperation(PROG_OP_NORMAL_CAMERA);
            vs.addVariableOut(PROG_OUT_NORMAL_PROJ); vs.addOperation(PROG_OP_NORMAL_PROJ);
        }
        if (shell.getUVs()!=null){
            vs.addLayout(PROG_LAYOUT_UV);
            vs.addVariableOut(PROG_OUT_UV);
            vs.addOperation(PROG_OP_UV);
        }
        if (shell.getTangents()!=null){
            vs.addLayout(PROG_LAYOUT_TANGENT);
            vs.addVariableOut(PROG_OUT_TANGENT_MODEL); vs.addOperation(PROG_OP_TANGENT_MODEL);
            vs.addVariableOut(PROG_OUT_TANGENT_WORLD); vs.addOperation(PROG_OP_TANGENT_WORLD);
            vs.addVariableOut(PROG_OUT_TANGENT_CAMERA); vs.addOperation(PROG_OP_TANGENT_CAMERA);
            vs.addVariableOut(PROG_OUT_TANGENT_PROJ); vs.addOperation(PROG_OP_TANGENT_PROJ);
        }

        vs.addOperation(PROG_OP_BUILDIN_PROJ);

    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        final GL2ES2 gl = context.getGL().asGL2ES2();

        if (attInstanceTransform==null){
            attInstanceTransform = program.getVertexAttribute(LAYOUT_INSTANCE_TRS, gl,4);
        }

        //bind buffers
        attInstanceTransform.enable(gl, shell.getInstanceTransforms());
    }

    @Override
    public void postDrawGL(RenderContext context, ActorProgram program) {
        super.postDrawGL(context, program);
        final GL2ES2 gl = context.getGL().asGL2ES2();
        attInstanceTransform.disable(gl);
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        attInstanceTransform = null;
    }

    @Override
    public void render(ActorProgram program, RenderContext context, MonoCamera camera, GraphicNode node) {

        final Model model = (Model) node;
        final GL2ES2 gl = context.getGL().asGL2ES2();
        GLUtilities.checkGLErrorsFail(gl);

        program.preExecutionGL(context, node);
        program.preDrawGL(context);

        ////////////////////////////////////////////////////////////////////////
        // DISPLAY : rendering /////////////////////////////////////////////////

        //set uniforms, Model->World->Projection matrix
        try{
            final Uniform uniformL = program.getUniform(UNIFORM_L);
            final Uniform uniformM = program.getUniform(UNIFORM_M);
            final Uniform uniformV = program.getUniform(UNIFORM_V);
            final Uniform uniformP = program.getUniform(UNIFORM_P);
            final Uniform uniformPX = program.getUniform(UNIFORM_PIXELSIZE);

            uniformL.setMat4(gl,    node.getNodeTransform().viewMatrix().toArrayFloat());
            uniformM.setMat4(gl,    getParentNodeToRootSpace(node).toMatrix().toArrayFloat());
            uniformV.setMat4(gl,    camera.getRootToNodeSpace().toMatrix().toArrayFloat());
            uniformP.setMat4(gl,    camera.getProjectionMatrix().toArrayFloat());
            uniformPX.setVec2(gl, new float[]{
                1.0f/(float) context.getViewRectangle().getWidth(),
                1.0f/(float) context.getViewRectangle().getHeight()});
        }catch(Throwable ex){
            //we may catch java.lang.InvalidArgumentException: Can not inverse
            ex.printStackTrace();
            return;
        }

//        //draw only wanted face
//        int culling = mesh.getCullFace();
//        if (culling==-1){
//            gl.glDisable(GL.GL_CULL_FACE);
//        } else {
//            gl.glEnable(GL.GL_CULL_FACE);
//            //since there are difference coordinate system, clockwise may be inverted
//            //we fix this by reversing the culling
//            if (mesh.getNodeToRootSpace().det()<0) culling = GLUtilities.inverseCulling(culling);
//            gl.glCullFace(culling);
//        }

        render(context, model, program);

        gl.glDisable(GL_CULL_FACE);

        program.postDrawGL(context);

    }

    @Override
    public void render(GLProcessContext context, Model model, ActorProgram program) {

        final InstancingShell is = (InstancingShell) model.getShape();
        final int nbIst = is.getInstanceTransforms().getTupleCount();
        final IBO ibo = (IBO) ((Mesh) model.getShape()).getIndex();

        final GL gl = context.getGL();
        final IndexedRange[] ranges = ((Mesh) model.getShape()).getRanges();
        for (int i=0;i<ranges.length;i++){
            GLEngineUtils.drawInstanced(gl, ranges[i], ibo, nbIst);
        }
        GLUtilities.discardErrors(gl);
        GLUtilities.checkGLErrorsFail(gl);
    }

    private static Affine getParentNodeToRootSpace(SceneNode node){

        final SceneNode parent = node.getParent();
        if (parent==null){
            return IDENTITY;
        } else {
            final AffineRW rootToNode = new Affine3();

            //adjust transform if coordinate system are different
            CoordinateSystem localCs = node.getLocalCoordinateSystem();
            if (localCs!=null){
                CoordinateSystem parentCs = parent.getCoordinateSystem();
                if (parentCs!=null && !localCs.equals(parentCs)){
                    final Affine trs = (Affine) CoordinateSystems.createTransform(parentCs, localCs);
                    rootToNode.localMultiply(trs);
                }
            }

            final Affine pm = parent.getRootToNodeSpace();
            rootToNode.localMultiply(pm);
            rootToNode.localInvert();
            return rootToNode;
        }

    }


}
