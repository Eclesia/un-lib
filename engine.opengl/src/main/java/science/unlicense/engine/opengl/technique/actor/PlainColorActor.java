
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.shader.Uniform;

/**
 * Plain color actor.
 *
 * @author Johann Sorel
 */
public class PlainColorActor extends AbstractMaterialValueActor{

    private static final ShaderTemplate DIFFUSE_COLOR_FR_GLOBAL;
    static {
        try{
            DIFFUSE_COLOR_FR_GLOBAL = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/colortex-3-fr-global.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private static final Chars REUSE_ID_GLOBAL = Chars.constant("ColorGlobal");

    private final PlainColorMapping mapping;

    //GL loaded informations
    private Uniform unicolor;

    public PlainColorActor(Chars produce, Chars method, Chars uniquePrefix, PlainColorMapping mapping) {
        super(Chars.EMPTY,false,produce,method,uniquePrefix,null,null,null,null,null);
        this.mapping = mapping;
    }

    public PlainColorMapping getMapping() {
        return mapping;
    }

    @Override
    public boolean isDirty() {
        return mapping.isDirty();
    }

    @Override
    public Chars getReuseUID() {
        return super.getReuseUID().concat(REUSE_ID_GLOBAL);
    }

    @Override
    public int getMinGLSLVersion() {
        return DIFFUSE_COLOR_FR_GLOBAL.getMinGLSLVersion();
    }

    @Override
    public void initProgram(final RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate fragmentShader = template.getFragmentShaderTemplate();
        fragmentShader.append(DIFFUSE_COLOR_FR_GLOBAL);
        fragmentShader.replaceTexts(PRODUCE_MARKER, produce);
        fragmentShader.replaceTexts(METHOD_MARKER, method);
        fragmentShader.replaceTexts(PREFIX_MARKER, uniquePrefix);
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        //one for all, nothing to do, everything is in the shader
        if (unicolor==null){
            unicolor = program.getUniform(uniquePrefix.concat(new Chars("value")));
        }
        unicolor.setVec4(gl, mapping.getColor().toRGBAPreMul());

    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        unicolor = null;
    }

}
