
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Matrix4x4;

/**
 *
 * @author Johann Sorel
 */
public class PaintTexture2D extends PainterTask {

    private final Texture2D texture;
    private final Matrix imgTrs;
    private final AlphaBlending blending;

    public PaintTexture2D(Texture2D texture, Affine imgTrs, AlphaBlending blending) {
        this.texture = texture;
        this.imgTrs = new Affine2(imgTrs).toMatrix4();
        this.blending = blending;
    }

    @Override
    public void execute(GL3Painter2D worker) {
        configureBlending(worker,blending);

        //load texture and vbo
        texture.loadOnGpuMemory(worker.gl);


        final double halfWidth = texture.getExtent().getL(0)/2;
        final double halfHeight = texture.getExtent().getL(1)/2;
        final Matrix4x4 scaleM = new Matrix4x4(
                halfWidth,0,0,halfWidth,
                0,halfHeight,0,halfHeight,
                0,0,1,0,
                0,0,0,1
        );
        MatrixRW imgTrs = this.imgTrs.multiply(scaleM);

        MatrixRW m = new Matrix4x4().setToIdentity();
        m.set(0, 0, worker.p.get(0, 0));
        m.set(0, 1, worker.p.get(0, 1));
        m.set(1, 0, worker.p.get(1, 0));
        m.set(1, 1, worker.p.get(1, 1));
        m.set(0, 3, worker.p.get(0, 2));
        m.set(1, 3, worker.p.get(1, 2));
        m = m.multiply(imgTrs);

        worker.programs.texture2DProg.enable(worker.gl);
        worker.programs.texture2DProg.uniformMVP.setMat4(worker.gl, m.toArrayFloat());

        //bind texture and sampler
        final int[] reservedTexture = new int[]{33984,0};
        worker.gl.glActiveTexture(reservedTexture[0]);
        texture.bind(worker.gl);
        worker.programs.texture2DProg.uniformSampler.setInt(worker.gl, reservedTexture[1]);
        GLUtilities.checkGLErrorsFail(worker.gl);

        worker.gl.glDisable(GLC.GETSET.State.CULL_FACE);
        worker.gl.glDrawArrays(GL_TRIANGLE_STRIP,0,4);
        GLUtilities.checkGLErrorsFail(worker.gl);
        worker.gl.glEnable(GLC.GETSET.State.CULL_FACE);

        //release sampler
        worker.gl.glActiveTexture(reservedTexture[0]);
        texture.unbind(worker.gl);

        worker.programs.texture2DProg.disable(worker.gl);
    }

}
