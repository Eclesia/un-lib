
package science.unlicense.engine.opengl;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.geometry.impl.Rectangle;

/**
 * Define a serie of rendering phases.
 *
 * @author Johann Sorel
 */
public interface GLProcessContext extends DisplayTimerState {

    /**
     * This rectangle is the opengl context shape as provided
     * by the GLEventListe reshape method.
     *
     * This rectangle is in GL ViewPort coordinate system.
     * (0,0) is at the lower left corner, X increasing right, Y increasing up, all in pixels units.
     *
     * @return Rectangle, never null, do not modify
     */
    Rectangle getGLRectangle();

    /**
     * This rectangle is a subset of the GL rectangle.
     * It must be used by all GL processes.
     *
     * This rectangle is in GL ViewPort coordinate system.
     * (0,0) is at the lower left corner, X increasing right, Y increasing up, all in pixels units.
     *
     * @return Rectangle, never null, do not modify
     */
    Rectangle getViewRectangle();

    /**
     * OpenGL object associated to this context.
     * @return GLSource
     */
    GLSource getSource();

    /**
     * OpenGL object associated to this context.
     * @return GL
     */
    GL getGL();

    /**
     * GLExecutable are tasks run in the OpenGL thread.
     * Unlike phases, GLExecutable are run only once before being forgotten.
     * @param executable task to execute
     */
    void addTask(GLExecutable executable);

    /**
     * Sequence of phases executed in this process.
     * @return Sequence of Process
     */
    Sequence getPhases();

    /**
     * Tracker object to avoid GPU resource index conflicts, memory statistics,
     * or act as a bank of resources.
     *
     * @return GLResourceManager never null.
     */
    GLResourceManager getResourceManager();

    /**
     * Record phases statistics.
     *
     * @return GLStatistics
     */
    GLStatistics getStatistics();

}
