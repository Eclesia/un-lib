

package science.unlicense.engine.opengl.phase.shadow;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.Light;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.impl.opengl.resource.FBO;

/**
 *
 * @author Johann Sorel
 */
public class ShadowMapVisitor extends NodeVisitor{

    private final ClearPhase clearPhase;
    private final RenderPhase depthRender;
    private MonoCamera camera;
    private int textureSize;

    public ShadowMapVisitor(int textureSize) {
        this.textureSize = textureSize;
        clearPhase = new ClearPhase();
        depthRender = new ShadowRenderPhase();
    }

    public void setCamera(MonoCamera camera) {
        this.camera = camera;
    }

    public MonoCamera getCamera() {
        return camera;
    }

    @Override
    public Object visit(Node node, Object context) {
        if (node instanceof GraphicNode){
            final GraphicNode glr = ((GraphicNode) node);
            //skip nodes which are not visible
            if (!glr.isVisible()) return null;
            if (glr instanceof Light){
                try {
                    visit((Light) glr, (GLProcessContext) context);
                } catch (GLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        //loop on childrens
        return super.visit(node, context);
    }

    public void visit(Light light, GLProcessContext context) throws GLException{
        if (!light.isCastShadows()){
            light.getUserProperties().remove(ShadowMap.PROPERTY_SHADOWMAP);
            return;
        }

        final SceneNode root = light.getRoot();

        ShadowMap shadowMap = (ShadowMap) light.getUserProperties().getValue(ShadowMap.PROPERTY_SHADOWMAP);
        if (shadowMap==null){
            shadowMap = new ShadowMap();
        }
        shadowMap.update(camera,light,textureSize);

        //ensure size is correct
        shadowMap.getFbo().resize(context.getGL(), textureSize, textureSize);

        final MonoCamera camera = shadowMap.getCamera();
        final FBO fbo = shadowMap.getFbo();

        clearPhase.setOutputFbo(fbo);
        depthRender.setRoot(root);
        depthRender.setCamera(camera);
        depthRender.setOutputFbo(fbo);

        clearPhase.process(context);
        depthRender.process(context);

//        final Texture2D tex = (Texture2D) fbo.getColorTexture();
//        tex.loadOnSystemMemory(context.getGL());
//        final Image image = tex.getImage();
//        System.out.println(Images.toChars(image, null));
//        System.out.println("la");

    }

    public void setTextureSize(int size) {
        this.textureSize = size;
    }

    public int getTextureSize() {
        return textureSize;
    }

    private static final class ShadowRenderPhase extends RenderPhase{

        private static final ShaderTemplate SHA_FR;
        static {
            try{
                SHA_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/shadowmap-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);

            }catch(IOException ex){
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }

        private final DefaultActor SHA = new DefaultActor(
                new Chars("shadowmap"), false,null, null, null, null, SHA_FR, true, true);

        public ShadowRenderPhase() {
            super(null, null, null);
        }

//        protected ActorProgram getOrCreateProgram(RenderContext context, Mesh mesh) {
//
//            //build a default program
//            ActorProgram program = (ActorProgram) mesh.programs.getValue(programId);
//            if (program == null){
//                program = buildProgram(mesh,context, mesh.getShape(), mesh.getTessellator(), null, null);
//                mesh.programs.add(programId, program);
//            }
//
//            return program;
//        }
//
//        public ActorProgram buildProgram(Mesh mesh,RenderContext ctx, Shape shape,
//            Tessellator tessellator, Material material, Sequence extActors) {
//
//            //build the final collector
//            final FragmentCollectorActor collector = new FragmentCollectorActor(mappings);
//
//            //build shell actor
//            final Actor shellActor = Actors.buildActor(mesh, shape);
//
//            final ActorProgram program = new ActorProgram();
//            final Sequence actors = program.getActors();
//
//            //build morph actor if present
//            if (shape instanceof Shell){
//                final MorphSet ms = ((Shell) shape).getMorphs();
//                if (ms!=null){
//                    actors.add(new MorphActor(ms));
//                }
//            }
//
//            actors.add(shellActor);
//            if (tessellator!=null) actors.add(tessellator);
//            //check additional actors
//            if (extActors!=null) actors.addAll(extActors);
//            actors.add(SHA);
//            actors.add(collector);
//
//            program.buildProgram(ctx);
//
//            return program;
//        }

    }

}
