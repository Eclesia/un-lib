

package science.unlicense.engine.opengl.phase.shadow;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.light.Light;
import science.unlicense.display.impl.light.ProjectiveLight;
import science.unlicense.display.impl.light.SpotLight;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;

/**
 * Describes the shadow map projected by a light.
 *
 * TODO shadow types :
 * - ParallelSplit
 * - PerspectiveShadowMap
 * - TrapezoidalShadowMap
 *
 * @author Johann Sorel
 */
public class ShadowMap {

    public static final Chars PROPERTY_SHADOWMAP = Chars.constant("ShadowMap");

    private FBO fbo;
    private final MonoCamera camera = new MonoCamera(true);

    public ShadowMap() {
    }

    public FBO getFbo() {
        return fbo;
    }

    public void setFbo(FBO fbo) {
        this.fbo = fbo;
    }

    public MonoCamera getCamera() {
        return camera;
    }

    public void update(MonoCamera sceneCamera, Light light, int textureSize) {
        //disable auto resize, this camera will render in an fbo which has a
        //size different from the gl canvas.
        if (light instanceof SpotLight) {
            camera.setNearPlane(0.1);
            camera.setFarPlane(1000);
            camera.setFieldOfView(((SpotLight) light).getFallOffAngle()*2);
        } else if (light instanceof DirectionalLight) {
            final DirectionalLight dl = (DirectionalLight) light;
            final MonoCamera dll = dl.createFittingCamera(sceneCamera);
            camera.setNearPlane(dll.getNearPlane());
            camera.setFarPlane(dll.getFarPlane());
            camera.setFieldOfView(dll.getFieldOfView());
        } else if (light instanceof ProjectiveLight) {
            final ProjectiveLight dl = (ProjectiveLight) light;
            final MonoCamera dll = dl.createFittingCamera(sceneCamera);
            camera.setNearPlane(dll.getNearPlane());
            camera.setFarPlane(dll.getFarPlane());
            camera.setFieldOfView(dll.getFieldOfView());
        } else {
            //point light, TODO
        }
        camera.setRenderArea(new Rectangle(0, 0, textureSize, textureSize));
        if (!light.getChildren().contains(camera)) {
            light.getChildren().add(camera);
        }

        if (fbo==null) {
            fbo = new FBO(textureSize, textureSize);
            final Texture2D tex = new Texture2D(textureSize, textureSize, Texture2D.VEC2_FLOAT());
            fbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, tex);
            fbo.addAttachment(null, GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(textureSize, textureSize, Texture2D.DEPTH24_STENCIL8()));
        }
        light.getUserProperties().add(PROPERTY_SHADOWMAP, this);
    }


}
