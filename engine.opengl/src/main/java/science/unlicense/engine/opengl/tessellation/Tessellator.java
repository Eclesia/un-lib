
package science.unlicense.engine.opengl.tessellation;

import science.unlicense.engine.opengl.technique.actor.Actor;

/**
 * A Tessellator control the tessellation shaders of a mesh.
 *
 * @author Johann Sorel
 */
public interface Tessellator extends Actor {

}
