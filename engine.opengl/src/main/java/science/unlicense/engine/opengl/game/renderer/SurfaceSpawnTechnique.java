
package science.unlicense.engine.opengl.game.renderer;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.AbstractGLTechnique;
import science.unlicense.engine.opengl.technique.actor.ActorExecutor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.Actors;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.engine.opengl.technique.actor.MaterialActor;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.model3d.impl.geometry.ModelVisitor;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.Model;

/**
 * Spawn given mesh at the surface of the rendered object.
 * This can be used to add fine grain details to materials (like dust) or regular
 * mesh patterns (like grass).
 *
 * @author Johann Sorel
 */
public class SurfaceSpawnTechnique extends AbstractGLTechnique {

    private static final Chars CST_SNB = Chars.constant("$SNB");
    private static final Chars CST_SNBC = Chars.constant("$SNBC");
    private static final Chars CST_SCOUNT = Chars.constant("$SMAX");
    private static final Chars CST_SVERTEX = Chars.constant("$S_VERTEX");
    private static final Chars CST_SNORNAL = Chars.constant("$S_NORMAL");
    private static final Chars CST_SUV = Chars.constant("$S_UV");

    private static final ShaderTemplate TEMPLATE_GE;
    static {
        try{
            TEMPLATE_GE = ShaderTemplate.create(new Chars("mod:/un/impl/game/renderer/surfacespawn-3-ge.glsl"), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    //max 5
    private int spawnCount = 3;
    private float size = 0.3f;
    private Model spawnedModel;
    private ActorProgram program;
    private ActorExecutor exec;
    private Uniform uniScale;

    public SurfaceSpawnTechnique() {
    }

    public Model getSpawnedModel() {
        return spawnedModel;
    }

    /**
     * Only simple meshes made of 1 index range of triangles are supported.
     *
     * @param spawnedMesh
     */
    public void setSpawnedModel(Model spawnedMesh) {
        this.spawnedModel = spawnedMesh;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public void setSpawnCount(int spawnCount) {
        this.spawnCount = spawnCount;
    }

    public int getSpawnCount() {
        return spawnCount;
    }

    @Override
    public void renderInternal(RenderContext context, MonoCamera camera, SceneNode node) {
        GLUtilities.checkGLErrorsFail(context.getGL());
        final Model model = (Model) node;

        if (program == null) {

            final ShaderTemplate template = fillTemplate();
            final Material material = (Material) spawnedModel.getMaterials().get(0);
            final MaterialActor materialActor = new MaterialActor(material, true);

            //build shell actor
            final Geometry shape = model.getShape();
            exec = Actors.buildExecutor(model, shape);

            final DefaultActor defaultActor = new DefaultActor(new Chars("SpawnSurface"),false,
                    null, null, null, template, null, true, false) {
                        @Override
                        public void preDrawGL(RenderContext context, ActorProgram program) {
                            super.preDrawGL(context, program);
                            uniScale.setFloat(context.getGL().asGL2ES2(), size);
                        }
                    };

            program = new ActorProgram();
            program.getActors().add(exec);

            if (model instanceof GLModel && ((GLModel) model).getTessellator() != null) {
                program.getActors().add(((GLModel) model).getTessellator());
            }

            program.getActors().add(defaultActor);
            program.getActors().add(materialActor);
            program.compile(context);
            uniScale = program.getUniform(new Chars("spawnScale"));
        }

        GLUtilities.checkGLErrorsFail(context.getGL());
        exec.render(program, context, camera, model);
        GLUtilities.checkGLErrorsFail(context.getGL());
        //TODO, when do we release the program ?
        //program.releaseProgram(context);
    }

    private ShaderTemplate fillTemplate() {
        final ShaderTemplate template = new ShaderTemplate(TEMPLATE_GE);

        final int[] count = new int[1];
        final CharBuffer vbuffer = new CharBuffer();
        final CharBuffer nbuffer = new CharBuffer();
        final CharBuffer uvbuffer = new CharBuffer();

        ModelVisitor visitor = new ModelVisitor(spawnedModel) {
            @Override
            protected void visit(MeshVertex vertex) {

                final TupleRW coord = vertex.getCoordinate();
                final Tuple normal = (Tuple) vertex.properties().getValue(Mesh.ATT_NORMAL);
                final Tuple uv = (Tuple) vertex.properties().getValue(Mesh.ATT_TEXCOORD_0);
                count[0]++;
                if (!vbuffer.isEmpty()) {
                    vbuffer.append(',');
                    nbuffer.append(',');
                    uvbuffer.append(',');
                }
                vbuffer.append("vec3(");
                vbuffer.append(Float64.encode(coord.get(0))).append(',');
                vbuffer.append(Float64.encode(coord.get(1))).append(',');
                vbuffer.append(Float64.encode(coord.get(2))).append(')');
                nbuffer.append("vec3(");
                nbuffer.append(Float64.encode(normal.get(0))).append(',');
                nbuffer.append(Float64.encode(normal.get(1))).append(',');
                nbuffer.append(Float64.encode(normal.get(2))).append(')');
                if (uv == null) {
                    uvbuffer.append("vec2(0,0)");
                } else {
                    uvbuffer.append("vec2(");
                    uvbuffer.append(Float64.encode(uv.get(0))).append(',');
                    uvbuffer.append(Float64.encode(uv.get(1))).append(')');
                }
            }
        };
        visitor.visit();

        template.replaceTexts(CST_SCOUNT, Int32.encode(spawnCount));
        template.replaceTexts(CST_SNBC, Int32.encode(count[0]*spawnCount));
        template.replaceTexts(CST_SNB, Int32.encode(count[0]));
        template.replaceTexts(CST_SVERTEX, vbuffer.toChars());
        template.replaceTexts(CST_SNORNAL, nbuffer.toChars());
        template.replaceTexts(CST_SUV, uvbuffer.toChars());

        return template;
    }

    @Override
    public void dispose(GLProcessContext context) {
        if (program != null) {
            program.releaseProgram(context);
        }
        uniScale = null;
    }

}
