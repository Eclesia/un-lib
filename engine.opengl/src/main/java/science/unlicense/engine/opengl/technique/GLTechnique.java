
package science.unlicense.engine.opengl.technique;

import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.GLDisposable;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.model3d.impl.technique.Technique;

/**
 * A Renderer is an rendering extension used to draw temporary or special
 * elements for a node.
 *
 * Renderers are often used for debugging purpose, like drawing bounding boxes,
 * inner skeleton, invisible objects like lights, path prediction trajectory,
 * mesh normals ...
 *
 * @author Johann Sorel
 */
public interface GLTechnique extends Technique, GLDisposable {

    /**
     * Renderers can be used in various phases of the rendering pipeline, to
     * avoid possible errors this predicate filters the phases to match
     * only the appropriate ones.
     *
     * @return Predicate, never null
     */
    Predicate getPhasePredicate();

    /**
     * Set phase predicate.
     *
     * @param predicate, not null
     */
    void setPhasePredicate(Predicate predicate);

    /**
     * Returns the rendering state configuration.
     * @return RenderState, never null
     */
    @Override
    GLRenderState getState();

    /**
     *
     * @param context current rendering context
     * @param camera camera associate to this context
     * @param node Node to be rendered
     */
    void render(final RenderContext context, final MonoCamera camera, final SceneNode node);

}
