
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.SpriteInfo;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.gpu.api.opengl.GL1;
import science.unlicense.gpu.api.opengl.GL2ES2;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.model3d.impl.material.TextureMapping;

/**
 *
 * @author Johann Sorel
 */
public class DistanceFieldActor extends AbstractMaterialValueActor{

    private static final ShaderTemplate DIFFUSE_TEXTURE_TC;
    private static final ShaderTemplate DIFFUSE_TEXTURE_TE;
    private static final ShaderTemplate DIFFUSE_TEXTURE_FR;
    static {
        try{
            DIFFUSE_TEXTURE_TC = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/imagetex-1-tc.glsl"), ShaderTemplate.SHADER_TESS_CONTROL);
            DIFFUSE_TEXTURE_TE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/imagetex-2-te.glsl"), ShaderTemplate.SHADER_TESS_EVAL);
            DIFFUSE_TEXTURE_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/distancetex-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final TextureMapping mapping;


    //GL loaded informations
    private int[] reservedTexture;
    private Uniform unitTexture;
    private Uniform unitSprite;
    private Uniform unitScale;
    private Uniform unitOffset;

    public DistanceFieldActor(Chars produce, Chars method, Chars uniquePrefix, TextureMapping mapping) {
        super(new Chars("DistanceFieldDiffuse"),false,produce, method, uniquePrefix,
                null,DIFFUSE_TEXTURE_TC,DIFFUSE_TEXTURE_TE,null,DIFFUSE_TEXTURE_FR);
        this.mapping = mapping;
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final Texture2D texture = mapping.getTexture();
        //load texture, image may have changed, no effect if already loaded
        texture.loadOnGpuMemory(gl);

        // Bind textures
        if (unitTexture == null){
            unitTexture = program.getUniform(uniquePrefix.concat(new Chars("tex")));
            unitSprite = program.getUniform(uniquePrefix.concat(new Chars("sprite")));
            unitScale = program.getUniform(uniquePrefix.concat(new Chars("scale")));
            unitOffset = program.getUniform(uniquePrefix.concat(new Chars("offset")));
        }
        reservedTexture = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTexture[0]);
        texture.bind(gl);
        unitTexture.setInt(gl, reservedTexture[1]);
        unitScale.setVec2(gl, mapping.getTransform().getScale().toFloat());
        unitOffset.setVec2(gl, mapping.getTransform().getTranslation().toFloat());

        final SpriteInfo sprite = ((UVMapping) ((Layer) mapping).getMap()).getSprite();
        if (sprite!=null){
            final float[] params = sprite.toParameters();
            unitSprite.setVec4(gl, params);
        } else {
            unitSprite.setVec4(gl, new float[]{0,0,texture.getWidth(), texture.getHeight()});
        }

    }

    @Override
    public void postDrawGL(RenderContext context, ActorProgram program) {
        super.postDrawGL(context, program);
        final GL1 gl = context.getGL().asGL1();
        GLUtilities.checkGLErrorsFail(gl);

        // unbind textures
        gl.glActiveTexture(reservedTexture[0]);
        gl.glBindTexture(GL_TEXTURE_2D, 0);
        context.getResourceManager().releaseTextureId(reservedTexture[0]);

    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        unitTexture = null;
        unitSprite = null;
        unitScale = null;
        unitOffset = null;
    }

}
