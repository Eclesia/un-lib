
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.image.api.color.Color;

/**
 *
 * Docs :
 * http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=15
 * http://codeflow.org/entries/2010/dec/09/minecraft-like-rendering-experiments-in-opengl-4/
 *
 * TODO : improve fog using a half space
 * http://www.terathon.com/lengyel/Lengyel-UnifiedFog.pdf
 *
 * TODO : fix it to use world position or depth instead of camera position
 *
 * @author Johann Sorel
 */
public class FogPhase extends AbstractTexturePhase {

    private static final Chars UNIFORM_PARAMS = Chars.constant("fogParams");
    private static final Chars UNIFORM_COLOR = Chars.constant("fogColor");

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/fog-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final FogActor actor = new FogActor();

    private float density = 0.05f;
    private Color color = Color.GRAY_LIGHT;

    private Uniform uniParams;
    private Uniform uniColor;

    public FogPhase(Texture texture, Texture depthTexture) {
        this(null,texture,depthTexture);
    }

    public FogPhase(FBO output, Texture texture, Texture depthTexture) {
        super(output,texture,depthTexture);
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    protected FogActor getActor() {
        return actor;
    }

    private final class FogActor extends DefaultActor{

        public FogActor() {
            super(new Chars("Fog"),false,null,null,null,null,SHADER_FR,true,true);
        }

        @Override
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL2ES2 gl = context.getGL().asGL2ES2();

            //get all uniforms
            if (uniParams == null){
                uniParams = program.getUniform(UNIFORM_PARAMS);
                uniColor = program.getUniform(UNIFORM_COLOR);
            }
            uniParams.setVec4(gl, new float[]{density,density,density,density});
            uniColor.setVec4(gl, color.toRGBAPreMul());
        }

        @Override
        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniParams = null;
            uniColor = null;
        }

    }

}
