
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_M;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_P;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_PIXELSIZE;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_V;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GL2ES2;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.gpu.impl.opengl.shader.VertexAttribute;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class ModelActor extends AbstractActor implements ActorExecutor{

    public static final ShaderTemplate SKIN_VE;
    static {
        try{
            SKIN_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/skin-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public static final Chars LAYOUT_VERTEX  = Chars.constant("l_position");
    public static final Chars LAYOUT_NORMAL  = Chars.constant("l_normal");
    public static final Chars LAYOUT_UV      = Chars.constant("l_uv");
    public static final Chars LAYOUT_TANGENT = Chars.constant("l_tangent");

    protected static final Chars PROG_LAYOUT_VERTEX  = Chars.constant("layout(location = $IDX) in vec3 l_position;");
    protected static final Chars PROG_LAYOUT_NORMAL  = Chars.constant("layout(location = $IDX) in vec3 l_normal;");
    protected static final Chars PROG_LAYOUT_UV      = Chars.constant("layout(location = $IDX) in vec2 l_uv;");
    protected static final Chars PROG_LAYOUT_TANGENT = Chars.constant("layout(location = $IDX) in vec4 l_tangent;");

    protected static final Chars PROG_OUT_VERTEX_MODEL  = Chars.constant("vec4 ").concat(CV_VERTEX_MODEL).concat(';');
    protected static final Chars PROG_OUT_VERTEX_WORLD  = Chars.constant("vec4 ").concat(CV_VERTEX_WORLD).concat(';');
    protected static final Chars PROG_OUT_VERTEX_CAMERA = Chars.constant("vec4 ").concat(CV_VERTEX_CAMERA).concat(';');
    protected static final Chars PROG_OUT_VERTEX_PROJ   = Chars.constant("vec4 ").concat(CV_VERTEX_PROJ).concat(';');
    protected static final Chars PROG_OUT_NORMAL_MODEL  = Chars.constant("vec3 ").concat(CV_NORMAL_MODEL).concat(';');
    protected static final Chars PROG_OUT_NORMAL_WORLD  = Chars.constant("vec3 ").concat(CV_NORMAL_WORLD).concat(';');
    protected static final Chars PROG_OUT_NORMAL_CAMERA = Chars.constant("vec3 ").concat(CV_NORMAL_CAMERA).concat(';');
    protected static final Chars PROG_OUT_NORMAL_PROJ   = Chars.constant("vec3 ").concat(CV_NORMAL_PROJ).concat(';');

    protected static final Chars PROG_OUT_UV      = Chars.constant("vec2 ").concat(CV_UV).concat(';');
    protected static final Chars PROG_OUT_TANGENT_MODEL  = Chars.constant("vec4 ").concat(CV_TANGENT_MODEL).concat(';');
    protected static final Chars PROG_OUT_TANGENT_WORLD  = Chars.constant("vec4 ").concat(CV_TANGENT_WORLD).concat(';');
    protected static final Chars PROG_OUT_TANGENT_CAMERA = Chars.constant("vec4 ").concat(CV_TANGENT_CAMERA).concat(';');
    protected static final Chars PROG_OUT_TANGENT_PROJ   = Chars.constant("vec4 ").concat(CV_TANGENT_PROJ).concat(';');

    protected static final Chars PROG_OP_VERTEX_MODEL   = Chars.constant("    outData.position_model = vec4("+CV_WORK_VERTEX_MODEL+",1);");
    protected static final Chars PROG_OP_VERTEX_WORLD   = Chars.constant("    outData.position_world = M * vec4("+CV_WORK_VERTEX_MODEL+",1);");
    protected static final Chars PROG_OP_VERTEX_CAMERA  = Chars.constant("    outData.position_camera = MV * vec4("+CV_WORK_VERTEX_MODEL+",1);");
    protected static final Chars PROG_OP_VERTEX_PROJ    = Chars.constant("    outData.position_proj = MVP * vec4("+CV_WORK_VERTEX_MODEL+",1);");
    protected static final Chars PROG_OP_NORMAL_MODEL   = Chars.constant("    outData.normal_model = (vec4(l_normal,0)).xyz;");
    protected static final Chars PROG_OP_NORMAL_WORLD   = Chars.constant("    outData.normal_world = (M * vec4(l_normal,0)).xyz;");
    protected static final Chars PROG_OP_NORMAL_CAMERA  = Chars.constant("    outData.normal_camera = (MV * vec4(l_normal,0)).xyz;");
    protected static final Chars PROG_OP_NORMAL_PROJ    = Chars.constant("    outData.normal_proj = (MVP * vec4(l_normal,0)).xyz;");
    protected static final Chars PROG_OP_TANGENT_MODEL  = Chars.constant("    outData.tangent_model = l_tangent;");
    protected static final Chars PROG_OP_TANGENT_WORLD  = Chars.constant("    outData.tangent_world = M * vec4(l_tangent.xyz,0);");
    protected static final Chars PROG_OP_TANGENT_CAMERA = Chars.constant("    outData.tangent_camera = MV * vec4(l_tangent.xyz,0);");
    protected static final Chars PROG_OP_TANGENT_PROJ   = Chars.constant("    outData.tangent_proj = MVP * vec4(l_tangent.xyz,0);");

    protected static final Chars PROG_OP_UV      = Chars.constant("    outData.uv  = l_uv;");

    protected static final Chars PROG_OP_BUILDIN_PROJ   = Chars.constant("    gl_Position = MVP * vec4("+CV_WORK_VERTEX_MODEL+",1);");

    protected final Model model;
    private VertexAttribute attVertice;
    private VertexAttribute attNormal;
    private VertexAttribute attUv;
    private VertexAttribute attTangent;

    public ModelActor(Model model) {
        super(null);
        this.model = model;
    }

    private Mesh getMesh(){
        return (Mesh) model.getShape();
    }

    @Override
    public int getMinGLSLVersion() {
        return SKIN_VE.getMinGLSLVersion();
    }

    @Override
    public Chars getReuseUID() {
        final Mesh mesh = getMesh();
        final CharBuffer cb = new CharBuffer();
        cb.append(mesh.getPositions()!=null    ? "V1":"V0");
        cb.append(mesh.getNormals()!=null     ? "N1":"N0");
        cb.append(mesh.getIndex()!=null     ? "I1":"I0");
        cb.append(mesh.getUVs()!=null         ? "U1":"U0");
        cb.append(mesh.getTangents()!=null    ? "T1":"T0");
        return cb.toChars();
    }

    @Override
    public boolean isDirty() {
        final Mesh mesh = getMesh();
        return ((VBO) mesh.getPositions()).isDirty()
            || ((VBO) mesh.getNormals()).isDirty()
            || ((IBO) mesh.getIndex()).isDirty();
    }

    @Override
    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate vs = template.getVertexShaderTemplate();
        final ShaderTemplate tc = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate te = template.getTesselationEvalShaderTemplate();
        final ShaderTemplate gs = template.getGeometryShaderTemplate();
        final ShaderTemplate fg = template.getFragmentShaderTemplate();

        vs.append(MeshActor.TEMPLATE_VE);
        if (tess) tc.append(MeshActor.TEMPLATE_TC);
        if (tess) te.append(MeshActor.TEMPLATE_TE);
        if (geom) gs.append(MeshActor.TEMPLATE_GE);
        if (fg!=null){
            fg.addUniform(new Chars("uniform mat4 ").concat(MeshActor.UNIFORM_M).concat(';'));
            fg.addUniform(new Chars("uniform mat4 ").concat(MeshActor.UNIFORM_V).concat(';'));
            fg.addOperation(MeshActor.OP_SET_M);
            fg.addOperation(MeshActor.OP_SET_V);
            fg.append(MeshActor.TEMPLATE_FR);
        }

        vs.append(SKIN_VE);

        final Mesh mesh = getMesh();
        // build program
        if (mesh.getPositions()!=null){
            //always set the layout, might be used by some other shader actor
            vs.addLayout(PROG_LAYOUT_VERTEX);
            vs.addVariableOut(PROG_OUT_VERTEX_MODEL); vs.addOperation(PROG_OP_VERTEX_MODEL);
            vs.addVariableOut(PROG_OUT_VERTEX_WORLD); vs.addOperation(PROG_OP_VERTEX_WORLD);
            vs.addVariableOut(PROG_OUT_VERTEX_CAMERA); vs.addOperation(PROG_OP_VERTEX_CAMERA);
            vs.addVariableOut(PROG_OUT_VERTEX_PROJ); vs.addOperation(PROG_OP_VERTEX_PROJ);
        }
        if (mesh.getNormals()!=null){
            vs.addLayout(PROG_LAYOUT_NORMAL);
            vs.addVariableOut(PROG_OUT_NORMAL_MODEL); vs.addOperation(PROG_OP_NORMAL_MODEL);
            vs.addVariableOut(PROG_OUT_NORMAL_WORLD); vs.addOperation(PROG_OP_NORMAL_WORLD);
            vs.addVariableOut(PROG_OUT_NORMAL_CAMERA); vs.addOperation(PROG_OP_NORMAL_CAMERA);
            vs.addVariableOut(PROG_OUT_NORMAL_PROJ); vs.addOperation(PROG_OP_NORMAL_PROJ);
        }
        if (mesh.getUVs()!=null){
            vs.addLayout(PROG_LAYOUT_UV);
            vs.addVariableOut(PROG_OUT_UV);
            vs.addOperation(PROG_OP_UV);
        }
        if (mesh.getTangents()!=null){
            vs.addLayout(PROG_LAYOUT_TANGENT);
            vs.addVariableOut(PROG_OUT_TANGENT_MODEL); vs.addOperation(PROG_OP_TANGENT_MODEL);
            vs.addVariableOut(PROG_OUT_TANGENT_WORLD); vs.addOperation(PROG_OP_TANGENT_WORLD);
            vs.addVariableOut(PROG_OUT_TANGENT_CAMERA); vs.addOperation(PROG_OP_TANGENT_CAMERA);
            vs.addVariableOut(PROG_OUT_TANGENT_PROJ); vs.addOperation(PROG_OP_TANGENT_PROJ);
        }

        vs.addOperation(PROG_OP_BUILDIN_PROJ);

    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        if (attVertice == null){
            //get all uniform and attributes ids
            attVertice      = program.getVertexAttribute(LAYOUT_VERTEX, gl);
            attNormal       = program.getVertexAttribute(LAYOUT_NORMAL, gl);
            attUv           = program.getVertexAttribute(LAYOUT_UV, gl);
            attTangent      = program.getVertexAttribute(LAYOUT_TANGENT, gl);
        }

        //bind buffers
        final Mesh mesh = getMesh();
        attVertice.enable(gl, (VBO) mesh.getPositions());
        attNormal.enable(gl, (VBO) mesh.getNormals());
        attUv.enable(gl, (VBO) mesh.getUVs());
        attTangent.enable(gl, (VBO) mesh.getTangents());

        //indices
        ((IBO) mesh.getIndex()).loadOnGpuMemory(gl);
        ((IBO) mesh.getIndex()).bind(gl);
    }

    @Override
    public void postDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();
        attVertice.disable(gl);
        attNormal.disable(gl);
        attUv.disable(gl);
        attTangent.disable(gl);
    }

    @Override
    public void render(ActorProgram program, RenderContext context, MonoCamera camera, GraphicNode node) {

        final Model model = (Model) node;

        final GL2ES2 gl = context.getGL().asGL2ES2();
        GLUtilities.checkGLErrorsFail(gl);

        program.preExecutionGL(context, node);
        program.preDrawGL(context);

        ////////////////////////////////////////////////////////////////////////
        // DISPLAY : rendering /////////////////////////////////////////////////


        //set uniforms, Model->World->Projection matrix
        try{
            final Uniform uniformM = program.getUniform(UNIFORM_M);
            final Uniform uniformV = program.getUniform(UNIFORM_V);
            final Uniform uniformP = program.getUniform(UNIFORM_P);
            final Uniform uniformPX = program.getUniform(UNIFORM_PIXELSIZE);

            uniformM.setMat4(gl,    node.getNodeToRootSpace().toMatrix().toArrayFloat());
            uniformV.setMat4(gl,    camera.getRootToNodeSpace().toMatrix().toArrayFloat());
            uniformP.setMat4(gl,    camera.getProjectionMatrix().toArrayFloat());
            uniformPX.setVec2(gl, new float[]{
                1.0f/(float) context.getViewRectangle().getWidth(),
                1.0f/(float) context.getViewRectangle().getHeight()});
        }catch(Throwable ex){
            //we may catch java.lang.InvalidArgumentException: Can not inverse
            ex.printStackTrace();
            return;
        }

//        //draw only wanted face
//        int culling = mesh.getCullFace();
//        if (culling==-1){
//            gl.glDisable(GL.GL_CULL_FACE);
//        } else {
//            gl.glEnable(GL.GL_CULL_FACE);
//            if (mesh.getNodeToRootSpace().det()<0){
//                //since there are difference coordinate system, clockwise may be inverted
//                //we fix this by reversing the culling
//                culling = GLUtilities.inverseCulling(culling);
//            }
//            gl.glCullFace(culling);
//        }

        render(context, model, program);

        //reset values
        gl.glDisable(GL_CULL_FACE);

        program.postDrawGL(context);

    }

    public void render(GLProcessContext context, Model model, ActorProgram program) {
        final GL gl = context.getGL();
        final IndexedRange[] ranges = ((Mesh) model.getShape()).getRanges();
        final IBO ibo = (IBO) ((Mesh) model.getShape()).getIndex();
        for (int i=0;i<ranges.length;i++){
            GLEngineUtils.draw(gl, ranges[i], ibo);
        }
        GLUtilities.discardErrors(gl);
        GLUtilities.checkGLErrorsFail(gl);
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        attVertice      = null;
        attNormal       = null;
        attUv           = null;
        attTangent      = null;
    }

}
