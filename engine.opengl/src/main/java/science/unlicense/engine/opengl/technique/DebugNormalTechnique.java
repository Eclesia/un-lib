

package science.unlicense.engine.opengl.technique;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorExecutor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.Actors;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.model3d.impl.scene.Model;

/**
 * Display the normals stored in the mesh shell.
 *
 * @author Johann Sorel
 */
public class DebugNormalTechnique extends AbstractGLTechnique {

    private static final ShaderTemplate NORMAL_GE;
    private static final ShaderTemplate NORMAL_FR;
    static {
        try{
            NORMAL_GE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugnormal-3-ge.glsl"), ShaderTemplate.SHADER_GEOMETRY);
            NORMAL_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugnormal-4-fr.glsl"), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private float normalLength = 1.0f;
    private ActorProgram program;
    private Uniform uniLength;

    public DebugNormalTechnique() {
    }

    public DebugNormalTechnique(float normalLength) {
        this.normalLength = normalLength;
    }

    public float getNormalLength() {
        return normalLength;
    }

    public void setNormalLength(float normalLength) {
        this.normalLength = normalLength;
    }

    @Override
    public void renderInternal(RenderContext context, MonoCamera camera, SceneNode node) {

        final Model model = (Model) node;

        if (program == null){
            program = new ActorProgram();

            //build shell actor
            final Geometry shape = model.getShape();
            final ActorExecutor shellActor = Actors.buildExecutor(model, shape);
            program.setExecutor(shellActor);

            final DefaultActor defaultActor = new DefaultActor(new Chars("DebugNormal"),false,
                    null, null, null, NORMAL_GE, NORMAL_FR, true, false){
                    @Override
                    public void preDrawGL(RenderContext context, ActorProgram program) {
                        super.preDrawGL(context, program);
                        uniLength.setFloat(context.getGL().asGL2ES2(), normalLength);
                    }
                };

            program.getActors().add(shellActor);
            program.getActors().add(defaultActor);
            program.compile(context);
            uniLength = program.getUniform(new Chars("normalLength"));
        }

        program.render(context, camera, model);

    }

    @Override
    public void dispose(GLProcessContext context) {
        if (program!=null){
            program.releaseProgram(context);
        }
        uniLength = null;
    }
}
