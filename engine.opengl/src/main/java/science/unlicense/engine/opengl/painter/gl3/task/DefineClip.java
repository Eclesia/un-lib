
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.engine.opengl.painter.gl3.GL3PaintPrograms;
import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.engine.opengl.scene.GLGeometry2D;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.Rectangle;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.impl.Matrix3x3;

/**
 *
 * @author Johann Sorel
 */
public class DefineClip extends PainterTask{

    private final PlanarGeometry geom2d;
    private final MatrixRW mv = new Matrix3x3().setToIdentity();

    public DefineClip(PlanarGeometry geom) {
        this.geom2d = geom;
    }

    @Override
    public void execute(GL3Painter2D worker) {
        if (geom2d == null) {
            //reset the stencil buffer
            worker.gl.glStencilMask(0xFF);
            worker.gl.glClearStencil(0);
            worker.gl.glClear(GL_STENCIL_BUFFER_BIT);
            worker.gl.glEnable(GL_STENCIL_TEST);
            worker.gl.glStencilFunc(GL_ALWAYS, 1, 0xFF);
            worker.gl.glStencilMask(0x00);
        } else {

            //disable color and depth writing
            worker.gl.glColorMask(false,false,false,false);
            worker.gl.glDepthMask(false);

            //prepare the stencil mask
            worker.gl.glEnable(GL_STENCIL_TEST);
            worker.gl.glStencilMask(0xFF);
            worker.gl.glClearStencil(0);
            worker.gl.glClear(GL_STENCIL_BUFFER_BIT);
            worker.gl.glStencilFunc(GL_ALWAYS, 1, 0xFF);
            worker.gl.glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

            //render the clip geometry, it will only fill the stencil buffer
            GLGeometry2D geom = null;
            if (geom2d instanceof BBox || geom2d instanceof Rectangle){
                //optimized version for bbox
                final GL3PaintPrograms.FillBBoxColorProgram prog = worker.programs.fillCLipBBoxProg;
                prog.preExecutionGL(worker.renderContext, null);
                prog.preDrawGL(worker.renderContext);

                float[] corners;
                if (geom2d instanceof BBox){
                    final BBox bbox = (BBox) geom2d;
                    corners = new float[]{(float) bbox.getMin(0),(float) bbox.getMin(1),(float) bbox.getMax(0),(float) bbox.getMax(1)};
                } else {
                    final Rectangle bbox = (Rectangle) geom2d;
                    corners = new float[]{(float) bbox.getX(),(float) bbox.getY(),(float) (bbox.getX()+bbox.getWidth()),(float) (bbox.getY()+bbox.getHeight())};
                }


                prog.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
                prog.uniformP.setMat3(worker.gl, worker.pArray);
                prog.uniformCORNERS.setVec4(worker.gl, corners);

                GLUtilities.checkGLErrorsFail(worker.gl);

                worker.gl.glDrawArrays(GL_TRIANGLE_FAN,0,4);
                GLUtilities.checkGLErrorsFail(worker.gl);

                prog.postDrawGL(worker.renderContext);

            } else {
                //load vbo
                geom = new GLGeometry2D(geom2d);
                if (!geom.calculateFill()) return;

                final VBO vertVBO = geom.getFillVBO();
                vertVBO.loadOnGpuMemory(worker.gl);

                final GL3PaintPrograms.FillPlainColorProgram prog = worker.programs.fillPlainColorProg;
                prog.plainActor.getMapping().setColor(Color.GREEN);

                prog.preExecutionGL(worker.renderContext, null);
                prog.preDrawGL(worker.renderContext);

                prog.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
                prog.uniformP.setMat3(worker.gl, worker.pArray);

                worker.gl.glEnableVertexAttribArray(0);
                vertVBO.bind(worker.gl, 0);
                GLUtilities.checkGLErrorsFail(worker.gl);

                worker.gl.glDrawArrays(GL_TRIANGLES, 0, vertVBO.getTupleCount());
                GLUtilities.checkGLErrorsFail(worker.gl);
                worker.gl.glDisableVertexAttribArray(0);

                prog.postDrawGL(worker.renderContext);
            }

            //reactive color and depth buffer
            worker.gl.glColorMask(true,true,true,true);
            worker.gl.glDepthMask(true);

            //active stencil mask
            worker.gl.glEnable(GL_STENCIL_TEST);
            worker.gl.glStencilFunc(GL_EQUAL, 1, 0xFF);
            worker.gl.glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
            //no writing in the stencil buffer
            worker.gl.glStencilMask(0x00);

            if (geom!=null) geom.dispose(worker.gl);
        }

    }

}
