
package science.unlicense.engine.opengl.material;

import science.unlicense.engine.opengl.GLDisposable;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.mapping.Mapping;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Blending;
import science.unlicense.model3d.impl.material.TextureMapping;

/**
 *
 * @author Johann Sorel
 */
public class Layer extends TextureMapping implements GLDisposable {

    /**
     * Material diffuse color.
     */
    public static final int TYPE_DIFFUSE = 1;
    /**
     * Material specular color.
     * Knowned as specular mapping.
     */
    public static final int TYPE_SPECULAR = 2;
    /**
     * Material ambient color.
     * Knowned as ambient mapping.
     */
    public static final int TYPE_AMBIENT = 3;
    /**
     * Used to correct model normals.
     * Knowned as normal/bump mapping.
     */
    public static final int TYPE_NORMAL = 4;
    /**
     * Combined with an opaque diffuse layers to obtain a translucent image.
     */
    public static final int TYPE_ALPHA = 5;

    private Mapping mapping;
    private int type = TYPE_DIFFUSE;
    private Blending method = AlphaBlending.create(AlphaBlending.SRC_OVER, 1.0f);

    public Layer() {}

    public Layer(Mapping mapping) {
        this.mapping = mapping;
    }

    public Layer(Mapping mapping, int type) {
        this.mapping = mapping;
        this.type = type;
    }

    public Layer(Mapping mapping, int type, Blending method) {
        this.mapping = mapping;
        this.type = type;
        this.method = method;
    }

    public Mapping getMap() {
        return mapping;
    }

    public void setMap(Mapping mapping) {
        this.mapping = mapping;
    }

    @Override
    public Texture2D getTexture() {
        if (mapping instanceof UVMapping) {
            return ((UVMapping) mapping).getTexture();
        }
        return null;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Blending getBlending() {
        return method;
    }

    public void setBlending(Blending method) {
        this.method = method;
    }

    public boolean isDirty() {
        if (mapping != null) {
            return mapping.isDirty();
        }
        return false;
    }

    @Override
    public void dispose(GLProcessContext context) {
        if (mapping != null) {
            mapping.dispose(context);
        }
    }

}
