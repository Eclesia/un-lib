

package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.ByVertexColorMapping;
import science.unlicense.engine.opengl.material.mapping.CubeMapping;
import science.unlicense.engine.opengl.material.mapping.CylindricalMapping;
import science.unlicense.engine.opengl.material.mapping.DistanceFieldMapping;
import science.unlicense.engine.opengl.material.mapping.DualParaboloidMapping;
import science.unlicense.engine.opengl.material.mapping.Mapping;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.material.mapping.ReflectionMapping;
import science.unlicense.engine.opengl.material.mapping.SphericalMapping;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.material.mapping.VolumetricScreenSpaceMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Blending;
import science.unlicense.image.api.color.VariousBlending;
import science.unlicense.math.api.Maths;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author Johann Sorel
 */
public class MaterialActor extends AbstractActor{

    public static final Chars AMBIENT = Chars.constant("AMBIENT");
    public static final Chars DIFFUSE = Chars.constant("DIFFUSE");
    public static final Chars SPECULAR = Chars.constant("SPECULAR");
    public static final Chars SHININESS = Chars.constant("SHININESS");

    private static final Chars RATIO = Chars.constant("ratio");
    private static final ShaderTemplate NOLIGHT_FR;
    private static final ShaderTemplate GBO_FR;
    static {
        try{
            NOLIGHT_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/light/lights-none-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
            GBO_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/gbo-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final SimpleBlinnPhong.Material material;
    private final Actor[] actors;
    private Uniform uniformAmbient;
    private Uniform uniformDiffuse;
    private Uniform uniformSpecular;
    private Uniform uniformShininess;

    public MaterialActor(Material material, boolean useLight) {
        super(null);
        this.material = SimpleBlinnPhong.view(material);

        final TextureMapping ambientTex = this.material.getAmbiantTexture();
        final TextureMapping diffuseTex = this.material.getDiffuseTexture();
        final TextureMapping specularTex = this.material.getSpecularTexture();
        final TextureMapping alphaTex = this.material.getAlphaTexture();

        final Sequence actors = new ArraySequence(6);
        int i = -1;
        if (ambientTex != null) {
            actors.add( buildTextureActor(Layer.TYPE_AMBIENT, VariousBlending.MULTIPLY, ++i, ambientTex));
        }
        if (diffuseTex != null) {
            actors.add( buildTextureActor(Layer.TYPE_DIFFUSE, VariousBlending.MULTIPLY, ++i, diffuseTex));
        }
        if (specularTex != null) {
            actors.add( buildTextureActor(Layer.TYPE_SPECULAR, VariousBlending.MULTIPLY, ++i, specularTex));
        }
        if (alphaTex != null) {
            actors.add( buildTextureActor(Layer.TYPE_ALPHA, VariousBlending.MULTIPLY, ++i, alphaTex));
        }


//        final Sequence layers = material.getLayers();
//        lays = new Layer[layers.getSize()+2];
//        Collections.copy(layers, lays, 0);
//        actors = new Actor[lays.length];
//        for (int i=0,n=lays.length-2;i<n;i++){
//            final Layer layer = lays[i];
//            final int type = layer.getType();
//            final Blending method = layer.getBlending();
//            if (Layer.TYPE_DIFFUSE == type){
//                actors[i] = buildTextureActor(type, method, i,layer.getMap());
//            } else if (Layer.TYPE_SPECULAR == type){
//                actors[i] = buildTextureActor(type, method, i,layer.getMap());
//            } else if (Layer.TYPE_AMBIENT == type){
//                actors[i] = buildTextureActor(type, method, i,layer.getMap());
//            } else if (Layer.TYPE_NORMAL == type){
//                final Mapping mapping = layer.getMap();
//                actors[i] = new NormalActor((UVMapping) mapping);
//            } else if (Layer.TYPE_ALPHA == type){
//                throw new RuntimeException("unexpected");
//            } else {
//                throw new RuntimeException("unexpected");
//            }
//        }

        if (useLight){
            actors.add(new LightsActor(material));
        } else {
            actors.add(new DefaultActor(
                    new Chars("NoLight"), false, null, null, null, null, NOLIGHT_FR, true, true));
        }

        //global collector
        actors.add(new DefaultActor(
                    new Chars("gbo"), true, null, null, null, null, GBO_FR, true, true));
        this.actors = (Actor[]) actors.toArray(Actor.class);
    }

    private Actor buildTextureActor(int type, Blending meth, int inc, TextureMapping layer){
        final Chars produce;
        final Chars method;
        if (Layer.TYPE_DIFFUSE == type) produce = StructMaterial.MAT_DIFFUSE;
        else if (Layer.TYPE_SPECULAR == type) produce = StructMaterial.MAT_SPECULAR;
        else produce = StructMaterial.MAT_AMBIENT;

        if (meth == VariousBlending.ADDITIVE) method = StructMaterial.METHOD_ADDITIVE;
        else if (meth == VariousBlending.MULTIPLY) method = StructMaterial.METHOD_MULTIPLY;
        else if (meth instanceof AlphaBlending) method = StructMaterial.METHOD_SRC_OVER;
        else throw new InvalidArgumentException("Unknowned method "+meth);

        final Chars prefix = new Chars("matlayer"+inc+"_");

        final Mapping mapping = ((Layer) layer).getMap();
        if (mapping instanceof PlainColorMapping){
            return new PlainColorActor(produce, method, prefix,(PlainColorMapping) mapping);
        } else if (mapping instanceof ByVertexColorMapping){
            return new ByVertexColorActor(produce, method, prefix,(ByVertexColorMapping) mapping);
        } else if (mapping instanceof DistanceFieldMapping){
            return new DistanceFieldActor(produce, method, prefix, layer);
        } else if (mapping instanceof UVMapping){
            return new TextureActor(produce, method, prefix, layer);
        } else if (mapping instanceof CubeMapping){
            return new TextureCubeActor(produce, method, prefix,(CubeMapping) mapping);
        } else if (mapping instanceof SphericalMapping){
            return new TextureSphericalActor(produce, method, prefix,(SphericalMapping) mapping);
        } else if (mapping instanceof DualParaboloidMapping){
            return new TextureDualParaboloidActor(produce, method, prefix,(DualParaboloidMapping) mapping);
        } else if (mapping instanceof CylindricalMapping){
            return new TextureCylindricalActor(produce, method, prefix,(CylindricalMapping) mapping);
        } else if (mapping instanceof ReflectionMapping){
            return new ReflectionActor(produce, method, prefix,(ReflectionMapping) mapping);
        } else if (mapping instanceof VolumetricScreenSpaceMapping){
            return new VolumetricScreenSpaceActor(produce, method, prefix,(VolumetricScreenSpaceMapping) mapping);
        } else {
            throw new RuntimeException("Unexpected mapping : "+mapping);
        }
    }

    @Override
    public Chars getReuseUID() {
        final CharBuffer cb = new CharBuffer();
        for (int i=0;i<actors.length;i++){
            final Chars id = actors[i].getReuseUID();
            if (id==null) return null;
            if (!cb.isEmpty()) cb.append('*');
            cb.append(id);
        }
        return cb.toChars();
    }

    @Override
    public int getMinGLSLVersion() {
        int min = GLC.GLSL.V110_GL20;
        for (int i=0;i<actors.length;i++){
            min = Maths.max(min,actors[i].getMinGLSLVersion());
        }
        return min;
    }

    @Override
    public boolean isDirty() {
        for (int i=0;i<actors.length;i++){
            if (actors[i].isDirty()) return true;
        }
        return false;
    }

    @Override
    public void initProgram(final RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        template.getFragmentShaderTemplate().append(StructMaterial.TEMPLATE);
        for (int i=0;i<actors.length;i++){
            actors[i].initProgram(ctx, template, tess, geom);
        }
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        if (uniformAmbient==null){
            uniformAmbient = program.getUniform(AMBIENT);
            uniformDiffuse = program.getUniform(DIFFUSE);
            uniformSpecular = program.getUniform(SPECULAR);
            uniformShininess = program.getUniform(SHININESS);
        }

        uniformAmbient.setVec4(gl, material.getAmbient().toRGBAPreMul());
        uniformDiffuse.setVec4(gl, material.getDiffuse().toRGBAPreMul());
        uniformSpecular.setVec4(gl, material.getSpecular().toRGBAPreMul());
        uniformShininess.setFloat(gl, (float) material.getShininess());

        for (int i=0;i<actors.length;i++){
            actors[i].preDrawGL(context, program);
        }
    }

    @Override
    public void postDrawGL(RenderContext context, ActorProgram program) {
        for (int i=0;i<actors.length;i++){
            actors[i].postDrawGL(context, program);
        }
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniformAmbient = null;
        uniformDiffuse = null;
        uniformSpecular = null;
        uniformShininess = null;
        for (int i=0;i<actors.length;i++){
            actors[i].dispose(context);
        }
    }

}
