
package science.unlicense.engine.opengl.mesh.particle;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.Actor;
import science.unlicense.engine.opengl.technique.actor.ActorExecutor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.engine.opengl.technique.actor.MeshActor;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_M;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_P;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_V;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.gpu.impl.opengl.shader.VertexAttribute;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class ParticuleShapeActor extends DefaultActor implements ActorExecutor {

    public static final Chars LAYOUT_VELOCITY = Chars.constant("l_velocity");
    public static final Chars LAYOUT_TIME = Chars.constant("l_time");
    public static final Chars UNIFORM_GRAVITY = Chars.constant("GRAVITY");
    public static final Chars UNIFORM_TIME = Chars.constant("TIME");
    public static final Chars UNIFORM_LIFESPAN = Chars.constant("LIFESPAN");

    private static final ShaderTemplate TEMPLATE;
    static {
        try{
            TEMPLATE = ShaderTemplate.create(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/particle/particle-emiter-0-ve.glsl")), ShaderTemplate.SHADER_VERTEX);
        }catch(IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final ParticuleShape shape;
    private Actor presenterActor;

    //shader variables
    private VertexAttribute attVelocity;
    private VertexAttribute attTime;
    private Uniform uniformM;
    private Uniform uniformV;
    private Uniform uniformP;
    private Uniform uniTime = null;
    private Uniform uniGravity = null;
    private Uniform uniLifeSpan = null;

    public ParticuleShapeActor(ParticuleShape shape) {
        super(new Chars("ParticuleShape"),false,TEMPLATE,null,null,null,null,true,true);
        this.shape = shape;
    }

    private Actor getPresenterActor() {
        if (presenterActor==null) {
            presenterActor = shape.getPresenter().createActor();
        }
        return presenterActor;
    }

    @Override
    public boolean usesGeometryShader() {
        return super.usesGeometryShader() || getPresenterActor().usesGeometryShader();
    }

    @Override
    public boolean usesTesselationShader() {
        return super.usesTesselationShader() || getPresenterActor().usesTesselationShader();
    }

    @Override
    public int getMinGLSLVersion() {
        return TEMPLATE.getMinGLSLVersion();
    }

    @Override
    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom) {
        final ShaderTemplate vs = template.getVertexShaderTemplate();
        final ShaderTemplate tc = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate te = template.getTesselationEvalShaderTemplate();
        final ShaderTemplate gs = template.getGeometryShaderTemplate();
        final ShaderTemplate fg = template.getFragmentShaderTemplate();

        vs.append(MeshActor.TEMPLATE_VE);
        if (tess) tc.append(MeshActor.TEMPLATE_TC);
        if (tess) te.append(MeshActor.TEMPLATE_TE);
        if (geom) gs.append(MeshActor.TEMPLATE_GE);
        if (fg!=null) fg.append(MeshActor.TEMPLATE_FR);

        super.initProgram(ctx, template,tess,geom);

        getPresenterActor().initProgram(ctx, template, tess, geom);
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);

        final GL2ES2 gl = context.getGL().asGL2ES2();
        if (uniTime==null) {
            uniTime = program.getUniform(UNIFORM_TIME);
            uniGravity = program.getUniform(UNIFORM_GRAVITY);
            uniLifeSpan = program.getUniform(UNIFORM_LIFESPAN);
        }
        uniTime.setFloat(gl, (float) context.getTimeNano()/1000000000f);
        uniLifeSpan.setFloat(gl, (float) shape.getLifeSpan());
        uniGravity.setVec3(gl, shape.getGravity().toFloat());

        getPresenterActor().preDrawGL(context, program);
    }

    @Override
    public void postDrawGL(RenderContext context, ActorProgram program) {
        super.postDrawGL(context, program);
        getPresenterActor().postDrawGL(context, program);
    }

    @Override
    public void render(ActorProgram program, RenderContext context, MonoCamera camera, GraphicNode node) {

        final Model model = (Model) node;

        if (uniformM == null) {
            try {
                uniformM = program.getUniform(UNIFORM_M);
                uniformV = program.getUniform(UNIFORM_V);
                uniformP = program.getUniform(UNIFORM_P);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }

        final GL2ES2 gl = context.getGL().asGL2ES2();
        GLUtilities.checkGLErrorsFail(gl);

        //prepare actors
        program.preExecutionGL(context, null);

        program.preDrawGL(context);

        ////////////////////////////////////////////////////////////////////////
        // DISPLAY : rendering /////////////////////////////////////////////////

        //set uniforms, Model->World->Projection matrix
        try{
            uniformM.setMat4(gl,    node.getNodeToRootSpace().toMatrix().toArrayFloat());
            uniformV.setMat4(gl,    camera.getRootToNodeSpace().toMatrix().toArrayFloat());
            uniformP.setMat4(gl,    camera.getProjectionMatrix().toArrayFloat());
        }catch(Throwable ex) {
            //we may catch java.lang.InvalidArgumentException: Can not inverse
            ex.printStackTrace();
            return;
        }

        render(context, model, program);

        program.postDrawGL(context);
    }

    private void render(GLProcessContext context, Model model, ActorProgram program) {

        final GL2ES2 gl = context.getGL().asGL2ES2();
        final ParticuleShape shape = (ParticuleShape) model.getShape();

        if (shape.getVertices() ==null) {
            attVelocity = program.getVertexAttribute(ParticuleShapeActor.LAYOUT_VELOCITY, gl);
            attTime = program.getVertexAttribute(ParticuleShapeActor.LAYOUT_TIME, gl);

            final int nbParticule = shape.getNbParticule();
            final double spawnRadius = shape.getSpawnRadius();
            final float[] velocities = new float[nbParticule*3];
            final float[] starts = new float[nbParticule];
            final int[] idx = new int[nbParticule];

            float t_acum = 0;
            int j=0;

            for (int i=0; i<nbParticule; i+=1, j+=3, t_acum+=shape.getLifeSpan()/nbParticule) {
                idx[i] = i;
                starts[i] = t_acum;
                float randx =  (float) ((Math.random()-0.5) * spawnRadius);
                float randz =  (float) ((Math.random()-0.5) * spawnRadius);
                velocities[j+0] = randx;
                velocities[j+1] = 0.0f;
                velocities[j+2] = randz;
            }

            shape.setVertices(new VBO(velocities, 3));
            shape.setTimes(new VBO(starts, 1));
            shape.setIndex(new IBO(idx));
            shape.setRange(IndexedRange.POINTS(0, idx.length));
        }

        final IBO index = shape.getIndex();
        final IndexedRange range = shape.getRange();

        index.loadOnGpuMemory(gl);

        attVelocity.enable(gl, shape.getVertices());
        attTime.enable(gl, shape.getTimes());

        index.bind(gl);
        GLEngineUtils.draw(gl, range, index);
        GLUtilities.checkGLErrorsFail(gl);

        index.unbind(gl);
        attVelocity.disable(gl);
        attTime.disable(gl);
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        getPresenterActor().dispose(context);
        uniTime = null;
        uniGravity = null;
        uniLifeSpan = null;
    }

}
