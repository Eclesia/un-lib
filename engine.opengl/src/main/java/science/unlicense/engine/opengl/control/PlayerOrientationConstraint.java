

package science.unlicense.engine.opengl.control;

import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;

/**
 * Enforce rotation only on horizontal and vertical axes.
 * This updater will remove any roll element.
 *
 *
 * @author Johann Sorel
 */
public class PlayerOrientationConstraint implements Updater {

    private final double minH;
    private final double maxH;
    private final double minV;
    private final double maxV;
    private final MonoCamera camera;

    /**
     *
     * @param minH min horizontale angle, NaN for none, in radians
     * @param maxH max horizontale angle, NaN for none, in radians
     * @param minV min vertical angle, NaN for none, in radians
     * @param maxV max vertical angle, NaN for none, in radians
     * @param camera
     */
    public PlayerOrientationConstraint(double minH, double maxH, double minV, double maxV, MonoCamera camera) {
        this.minH = minH;
        this.maxH = maxH;
        this.minV = minV;
        this.maxV = maxV;
        this.camera = camera;
    }

    @Override
    public void update(DisplayTimerState context) {

        Matrix3x3 m = new Matrix3x3(camera.getNodeTransform().getRotation());
        VectorRW forward = new Vector3f64(0, 0, 1);
        VectorRW ref = (VectorRW) m.transform(forward);

        double horizontalAngle = forward.shortestAngle(new Vector3f64(ref.getX(), 0, ref.getZ()).localNormalize());
        if (ref.getX() < 0) {
            horizontalAngle = -horizontalAngle;
        }
        if (!Double.isNaN(minH) && !Double.isNaN(maxH)) {
            horizontalAngle = Maths.clamp(horizontalAngle, minH, maxH);
        }

        double verticalAngle = ref.shortestAngle(new Vector3f64(ref.getX(), 0, ref.getZ()).localNormalize());
        if (ref.getY() < 0) {
            verticalAngle = -verticalAngle;
        }
        if (!Double.isNaN(minV) && !Double.isNaN(maxV)) {
            verticalAngle = Maths.clamp(verticalAngle, minV, maxV);
        }

        //rebuild matrix
        m.setToIdentity();
        if (!Double.isNaN(horizontalAngle)) {
            final Matrix3x3 mh = Matrix3x3.createRotation3(horizontalAngle, camera.getUpAxis());
            m.localMultiply(mh);
        }
        if (!Double.isNaN(verticalAngle)) {
            final Matrix3x3 mv = Matrix3x3.createRotation3(verticalAngle, camera.getRightAxis());
            m.localMultiply(mv);
        }
        camera.getNodeTransform().getRotation().set(m);
        camera.getNodeTransform().notifyChanged();
    }

}
