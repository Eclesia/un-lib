
package science.unlicense.engine.opengl.scene;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.FloatSequence;
import science.unlicense.engine.opengl.mesh.AbstractGLGeometry;
import science.unlicense.engine.opengl.mesh.GLGeometry;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.impl.path.FlattenPathIterator;
import science.unlicense.geometry.impl.triangulate.EarClipping;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class GLGeometry2D extends AbstractGLGeometry{

    private static final double[] RESOLUTION = new double[]{1,1};

    private final PlanarGeometry geom;

    private boolean fillSet = false;
    private VBO fillVBO;
    private VBO maskVBO;

    private boolean contourSet = false;
    private VBO contourVBO;
    private IBO contourIBO;
    private IBO contourAdjIBO;

    public GLGeometry2D(PlanarGeometry geom) {
        this.geom = geom;
    }

    public PlanarGeometry getGeometry() {
        return geom;
    }

    @Override
    public BBox getBoundingBox() {
        return geom.getBoundingBox();
    }

    public VBO getFillVBO(){
        return fillVBO;
    }

    public VBO getMaskVBO() {
        return maskVBO;
    }

    public VBO getContourVBO(){
        return contourVBO;
    }

    public IBO getContourIBO(){
        return contourIBO;
    }

    public IBO getContourAdjencyIBO(){
        return contourAdjIBO;
    }

    /**
     *
     * @return true if geometry has a contour
     */
    public boolean calculateContour(){
        if (contourSet || geom==null) return contourVBO!=null;
        contourSet = true;

        final FlattenPathIterator ite = new FlattenPathIterator(
                geom.createPathIterator(), RESOLUTION);

        final Sequence points = new ArraySequence();

        //stroke caches
        final Vector2f64 segmentStart = new Vector2f64();
        final Vector2f64 segmentEnd = new Vector2f64();
        //path first coordinate, used when on a close segment
        final Vector2f64 pathStart = new Vector2f64();

        while (ite.next()){
            final int type = ite.getType();
            if (type==PathIterator.TYPE_MOVE_TO){
                ite.getPosition(segmentEnd);
                ite.getPosition(pathStart);
            } else if (PathIterator.TYPE_LINE_TO == type || PathIterator.TYPE_CLOSE == type){
                if (PathIterator.TYPE_CLOSE == type){
                    //end segment if the first position
                    segmentEnd.set(pathStart);
                } else {
                    ite.getPosition(segmentEnd);
                }
                points.add(segmentStart.toFloat());
                points.add(segmentEnd.toFloat());
            }
            segmentStart.set(segmentEnd);
        }

        if (points.getSize()==0){
            //nothing to paint
            return false;
        } else if (points.getSize()==1){
            //duplicate point
            points.add(points.get(0));
        }

        final int size = points.getSize();
        final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(size*2).cursor();
        final Int32Cursor indices = DefaultBufferFactory.INSTANCE.createInt32(size).cursor();
        final Int32Cursor indiceAdjs = DefaultBufferFactory.INSTANCE.createInt32(size*2).cursor();
        for (int i=0,k=0;i<size-1;i+=2,k+=2){
            final float[] coordStart = (float[]) points.get(i);
            final float[] coordEnd = (float[]) points.get(i+1);
            vertices.write(coordStart).write(coordEnd);
            indices.write(k).write(k+1);

            indiceAdjs.write( i==0 ? k : k-2);
            indiceAdjs.write(k+0);
            indiceAdjs.write(k+1);
            indiceAdjs.write( i==size-2 ? k+1 : k+3);
        }
        contourVBO = new VBO(vertices.getBuffer(), 2);
        contourIBO = new IBO(indices.getBuffer());
        contourAdjIBO = new IBO(indiceAdjs.getBuffer());

        return true;
    }

    public boolean calculateFill(){
        if (fillSet || geom==null) return fillVBO!=null;
        fillSet = true;

        final FlattenPathIterator ite = new FlattenPathIterator(
                geom.createPathIterator(), RESOLUTION);

        final Sequence triangles;
        try {
            final EarClipping triangulator = new EarClipping();
            triangles = triangulator.triangulate(ite);
        } catch (OperationException ex) {
            ex.printStackTrace();
            return false;
        }

        final int size = triangles.getSize();
        final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(size*3*2).cursor();
        int k=0;
        for (int i=0;i<size;i++){
            final Triangle triangle = (Triangle) triangles.get(i);
            vertices.write(triangle.getFirstCoord().toFloat());
            vertices.write(triangle.getThirdCoord().toFloat());
            vertices.write(triangle.getSecondCoord().toFloat());
        }

        fillVBO = new VBO(vertices.getBuffer(),2);

        return true;
    }

    public void calculateMask(){
        if (maskVBO!=null) return;

        final FloatSequence seq = new FloatSequence();
        final PathIterator ite = geom.createPathIterator();

        final VectorRW anchor = new Vector3f64(-1,-1,0);
        final VectorRW current = new Vector3f64();
        final VectorRW previous = new Vector3f64();
        final VectorRW first = new Vector3f64();
        final VectorRW control = new Vector3f64();

        seq.put(previous.toFloat());
        seq.put(previous.toFloat());

        while (ite.next()){
            int type = ite.getType();
            if (PathIterator.TYPE_MOVE_TO==type){
                ite.getPosition(first);
                ite.getPosition(current);
            } else if (PathIterator.TYPE_LINE_TO==type){
                ite.getPosition(current);
                control.setZ(0);
                current.setZ(0);
                previous.setZ(0);
                seq.put(anchor.toFloat());
                seq.put(current.toFloat());
                seq.put(previous.toFloat());
            } else if (PathIterator.TYPE_CLOSE==type){
                control.setZ(0);
                current.setZ(0);
                previous.setZ(0);
                seq.put(anchor.toFloat());
                seq.put(first.toFloat());
                seq.put(previous.toFloat());
            } else if (PathIterator.TYPE_QUADRATIC==type){
                control.setZ(0);
                current.setZ(0);
                previous.setZ(0);
                ite.getPosition(current);
                seq.put(anchor.toFloat());
                seq.put(current.toFloat());
                seq.put(previous.toFloat());

                // special triangle
                ite.getFirstControl(control);
                control.setZ(1);
                current.setZ(1);
                previous.setZ(1);
                seq.put(control.toFloat());
                seq.put(current.toFloat());
                seq.put(previous.toFloat());

            } else {
                throw new RuntimeException("TODO " + type);
            }

            previous.set(current);
        }

        float[] coords = seq.toArrayFloat();
        maskVBO = new VBO(coords,3);
    }

    @Override
    public void dispose(GL gl){
        if (contourIBO!=null){
            contourIBO.unloadFromSystemMemory(gl);
            contourIBO.unloadFromGpuMemory(gl);
        }
        if (contourAdjIBO!=null){
            contourAdjIBO.unloadFromSystemMemory(gl);
            contourAdjIBO.unloadFromGpuMemory(gl);
        }
        if (contourVBO!=null){
            contourVBO.unloadFromSystemMemory(gl);
            contourVBO.unloadFromGpuMemory(gl);
        }
        if (fillVBO!=null){
            fillVBO.unloadFromSystemMemory(gl);
            fillVBO.unloadFromGpuMemory(gl);
        }
    }

    @Override
    public GLGeometry copy() {
        return new GLGeometry2D(geom);
    }

    @Override
    public boolean isDirty() {
        return false;
    }

}
