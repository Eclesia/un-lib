
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.BillBoard;
import science.unlicense.engine.opengl.phase.RenderContext;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_M;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_P;
import static science.unlicense.engine.opengl.technique.actor.MeshActor.UNIFORM_V;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.gpu.impl.opengl.shader.VertexAttribute;
import science.unlicense.math.api.VectorRW;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class BillBoardShapeActor extends DefaultActor implements ActorExecutor{

    public static final Chars UNIFORM_SIZE = Chars.constant("UNI_SIZE");
    public static final Chars UNIFORM_ANCHOR = Chars.constant("UNI_ANCHOR");
    public static final Chars UNIFORM_AXIS = Chars.constant("UNI_AXIS");
    public static final Chars VA_POINT = Chars.constant("l_point");

    private static final ShaderTemplate TEMPLATE_VS;
    private static final ShaderTemplate TEMPLATE_GS;
    static {
        try{
            TEMPLATE_VS = ShaderTemplate.create(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/billboard/billboard-0-ve.glsl")), ShaderTemplate.SHADER_VERTEX);
            TEMPLATE_GS = ShaderTemplate.create(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/billboard/billboard-3-ge.glsl")), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final BillBoard shape;
    //shader variables
    private Uniform uniformM;
    private Uniform uniformV;
    private Uniform uniformP;
    private Uniform uniSize = null;
    private Uniform uniAnchor = null;
    private Uniform uniAxis = null;


    public BillBoardShapeActor(BillBoard shape) {
        super(new Chars("BillBoard"),false,TEMPLATE_VS,null,null,TEMPLATE_GS,null,true,false);
        this.shape = shape;
    }

    @Override
    public int getMinGLSLVersion() {
        return TEMPLATE_VS.getMinGLSLVersion();
    }

    @Override
    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        final ShaderTemplate vs = template.getVertexShaderTemplate();
        final ShaderTemplate tc = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate te = template.getTesselationEvalShaderTemplate();
        final ShaderTemplate gs = template.getGeometryShaderTemplate();
        final ShaderTemplate fg = template.getFragmentShaderTemplate();

        vs.append(MeshActor.TEMPLATE_VE);
        if (tess) tc.append(MeshActor.TEMPLATE_TC);
        if (tess) te.append(MeshActor.TEMPLATE_TE);
        if (geom) gs.append(MeshActor.TEMPLATE_GE);
        if (fg!=null) fg.append(MeshActor.TEMPLATE_FR);

        super.initProgram(ctx, template,tess,geom);
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);

        final GL2ES2 gl = context.getGL().asGL2ES2();
        if (uniSize==null){
            uniSize = program.getUniform(UNIFORM_SIZE);
            uniAnchor = program.getUniform(UNIFORM_ANCHOR);
            uniAxis = program.getUniform(UNIFORM_AXIS);
        }
        uniSize.setVec3(gl, new float[]{shape.getSize()[0],shape.getSize()[1],shape.isScreenCS()?1f:0f});
        uniAnchor.setVec2(gl, shape.getAnchor().toFloat());
        final VectorRW fixedAxis = shape.getFixedAxis();
        if (fixedAxis!=null){
            uniAxis.setVec3(gl, fixedAxis.toFloat());
        } else {
            uniAxis.setVec3(gl, new float[3]);
        }
    }

    @Override
    public void render(ActorProgram program, RenderContext context, MonoCamera camera, GraphicNode node) {

        final Model model = (Model) node;

        if (uniformM == null) {
            try {
                uniformM = program.getUniform(UNIFORM_M);
                uniformV = program.getUniform(UNIFORM_V);
                uniformP = program.getUniform(UNIFORM_P);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }

        final GL2ES2 gl = context.getGL().asGL2ES2();
        GLUtilities.checkGLErrorsFail(gl);

        //prepare actors
        program.preExecutionGL(context, null);

        program.preDrawGL(context);

        ////////////////////////////////////////////////////////////////////////
        // DISPLAY : rendering /////////////////////////////////////////////////

        //set uniforms, Model->World->Projection matrix
        try {
            uniformM.setMat4(gl,    node.getNodeToRootSpace().toMatrix().toArrayFloat());
            uniformV.setMat4(gl,    camera.getRootToNodeSpace().toMatrix().toArrayFloat());
            uniformP.setMat4(gl,    camera.getProjectionMatrix().toArrayFloat());
        }catch(Throwable ex){
            //we may catch java.lang.InvalidArgumentException: Can not inverse
            ex.printStackTrace();
            return;
        }

        render(context, model, program);

        program.postDrawGL(context);
    }

    private void render(GLProcessContext context, Model model, ActorProgram program) {

        final GL2ES2 gl = context.getGL().asGL2ES2();
        final BillBoard shape = (BillBoard) model.getShape();

        final VertexAttribute attPoints = program.getVertexAttribute(VA_POINT, gl);

        final VBO points = shape.getPoints();
        attPoints.enable(gl, points);

        gl.glDrawArrays(GLC.PRIMITIVE.POINTS, 0, points.getTupleCount());
        GLUtilities.checkGLErrorsFail(gl);

        attPoints.disable(gl);
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniSize = null;
        uniAnchor = null;
        uniAxis = null;
    }

}
