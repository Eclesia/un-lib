
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 * Material structure, fragment shader with Material structure and methods.
 *
 * @author Johann Sorel
 */
public final class StructMaterial {

    public static final ShaderTemplate TEMPLATE;
    static {
        try{
            TEMPLATE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/material-base-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    /** vec4 */
    public static final Chars MAT_AMBIENT = Chars.constant("material.ambient");
    /** vec4 */
    public static final Chars MAT_DIFFUSE = Chars.constant("material.color");
    /** vec4 */
    public static final Chars MAT_SPECULAR = Chars.constant("material.specular");
    /** float */
    public static final Chars MAT_SHININESS = Chars.constant("material.shininess");

    public static final Chars METHOD_ADDITIVE = Chars.constant("methodAddition");
    public static final Chars METHOD_MULTIPLY = Chars.constant("methodMultiply");
    public static final Chars METHOD_SRC_OVER = Chars.constant("methodSrcOver");

    private StructMaterial(){}

}
