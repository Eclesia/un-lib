
package science.unlicense.engine.opengl.technique;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorExecutor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.Actors;
import science.unlicense.engine.opengl.technique.actor.MaterialActor;
import science.unlicense.engine.opengl.technique.actor.MorphActor;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.api.opengl.GL;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class GLActorTechnique extends AbstractGLTechnique {

    private final Model model;
    private ActorProgram program;
    //complementary shader actors
    private final Sequence extShaderActors = new ArraySequence();

    protected boolean useLights = false;

    public GLActorTechnique(Model model) {
        this.model = model;

        state.setEnable(GLC.GETSET.State.DITHER, true);
        state.setEnable(GLC.GETSET.State.MULTISAMPLE, true);
        state.setEnable(GLC.GETSET.State.SAMPLE_ALPHA_TO_COVERAGE, true);
        state.setEnable(GLC.GETSET.State.DEPTH_TEST, true);
        state.setEnable(GLC.GETSET.State.BLEND, true);
        state.setWriteToDepth(true);
        state.setDepthFunc(GL_LESS);
    }

    public Sequence getExtShaderActors() {
        return extShaderActors;
    }

    public void setUseLights(boolean useLights) {
        this.useLights = useLights;
    }

    public boolean isUseLights() {
        return useLights;
    }

    @Override
    public void renderInternal(RenderContext context, MonoCamera camera, SceneNode node) {

        final GL gl = context.getGL();
        GLUtilities.checkGLErrorsFail(gl);

        if (program == null){
            buildRenderer(model, context);
        }
        if (!program.isOnGpuMemory()){
            program.compile(context);
        }
        program.render(context, camera, model);
    }

    private void buildRenderer(Model model, RenderContext ctx) {

        program = new ActorProgram();
        final Sequence actors = program.getActors();

        //build shell actor
        final Geometry shape = model.getShape();
        final ActorExecutor executor = Actors.buildExecutor(model, shape);
        actors.add(executor);
        if (shape instanceof Mesh){
            final Sequence ms = ((Mesh) shape).getMorphs();
            if (!ms.isEmpty()) {
                actors.add(new MorphActor(ms));
            }
        }

        if (model instanceof GLModel && ((GLModel) model).getTessellator()!=null) actors.add(((GLModel) model).getTessellator());
        if (!model.getMaterials().isEmpty()) actors.add(new MaterialActor((Material) model.getMaterials().get(0),useLights));
        //check additional actors
        actors.addAll(extShaderActors);

        program.compile(ctx);
        program.setExecutor(executor);
    }

    @Override
    public void dispose(GLProcessContext context) {
        if (program!=null){
            program.releaseProgram(context);
            program = null;
        }
    }

}
