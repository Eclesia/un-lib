
package science.unlicense.engine.opengl.technique;

import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.StackDictionary;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.TBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.UBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.gpu.impl.opengl.shader.ShaderProgram;
import science.unlicense.gpu.impl.opengl.shader.ShaderUtils;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.gpu.impl.opengl.shader.VertexAttribute;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector2f32;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.physics.api.skeleton.Joint;

/**
 *
 * @author Johann Sorel
 */
public class GLShaderTechnique extends AbstractGLTechnique {

    //used by all meshes
    //name convention from GLTF : https://github.com/KhronosGroup/glTF/blob/master/extensions/2.0/Khronos/KHR_techniques_webgl/README.md
    public static final Chars UNIFORM_LOCAL = Chars.constant("LOCAL");
    public static final Chars UNIFORM_MODEL = Chars.constant("MODEL");
    public static final Chars UNIFORM_VIEW = Chars.constant("VIEW");
    public static final Chars UNIFORM_PROJECTION = Chars.constant("PROJECTION");
    public static final Chars UNIFORM_MODELVIEW = Chars.constant("MODELVIEW");
    public static final Chars UNIFORM_MODELVIEWPROJECTION = Chars.constant("MODELVIEWPROJECTION");
    public static final Chars UNIFORM_MODELINVERSE = Chars.constant("MODELINVERSE");
    public static final Chars UNIFORM_VIEWINVERSE = Chars.constant("VIEWINVERSE");
    public static final Chars UNIFORM_PROJECTIONINVERSE = Chars.constant("PROJECTIONINVERSE");
    public static final Chars UNIFORM_MODELVIEWINVERSE = Chars.constant("MODELVIEWINVERSE");
    public static final Chars UNIFORM_MODELVIEWPROJECTIONINVERSE = Chars.constant("MODELVIEWPROJECTIONINVERSE");
    public static final Chars UNIFORM_MODELINVERSETRANSPOSE = Chars.constant("MODELINVERSETRANSPOSE");
    public static final Chars UNIFORM_MODELVIEWINVERSETRANSPOSE = Chars.constant("MODELVIEWINVERSETRANSPOSE");
    public static final Chars UNIFORM_VIEWPORT = Chars.constant("VIEWPORT");
    public static final Chars UNIFORM_JOINTMATRIX = Chars.constant("JOINTMATRIX");
    public static final Chars UNIFORM_ALPHACUTOFF = Chars.constant("ALPHACUTOFF");

    //for skin meshes
    public static final Chars UNIFORM_NBJOINT = Chars.constant("NBJOINT");
    public static final Chars UNIFORM_JOINTSAMPLER = Chars.constant("JOINTSAMPLER");
    private static final Chars GROUP_TBO_UPDATE = Chars.constant("tbo_update");
    private static final Chars GROUP_TBO = Chars.constant("tbo");


    protected final ShaderProgram program;

    public GLShaderTechnique(ShaderProgram program) {
        this.program = program;
    }

    protected void fillRenderingProperties(RenderContext context, MonoCamera camera, SceneNode node, Dictionary renderProperties) {

    }

    @Override
    protected void renderInternal(RenderContext context, MonoCamera camera, SceneNode node) {
        super.renderInternal(context, camera, node);

        final Sequence usedTextures = new ArraySequence();
        final Sequence usedAtts = new ArraySequence();
        final Sequence usedVbos = new ArraySequence();
        final Sequence usedTbos = new ArraySequence();

        final GL2ES2 gl = context.getGL().asGL2ES2();
        program.loadOnGpuMemory(gl);
        program.bindFragDataLocations(gl, new Chars[]{new Chars("COLOR")});
        program.enable(gl);

        final Dictionary renderProperties = new HashDictionary();
        final Matrix MODEL = node.getNodeToRootSpace().toMatrix();
        final Matrix VIEW = camera.getRootToNodeSpace().toMatrix();
        final Matrix PROJECTION = camera.getProjectionMatrix();
        final Matrix MODELVIEW = VIEW.multiply(MODEL);
        final Matrix MODELVIEWPROJECTION = PROJECTION.multiply(VIEW).multiply(MODEL);
        final Tuple VIEWPORT = new Vector2f32(
                1.0f/(float) context.getViewRectangle().getWidth(),
                1.0f/(float) context.getViewRectangle().getHeight());


        renderProperties.add(UNIFORM_MODEL, MODEL);
        renderProperties.add(UNIFORM_VIEW, VIEW);
        renderProperties.add(UNIFORM_PROJECTION, PROJECTION);
        renderProperties.add(UNIFORM_MODELVIEW, MODELVIEW);
        renderProperties.add(UNIFORM_MODELVIEWPROJECTION, MODELVIEWPROJECTION);
        renderProperties.add(UNIFORM_VIEWPORT, VIEWPORT);
        fillRenderingProperties(context, camera, node, renderProperties);

        final Model model = (Model) node;
        final Material material = (Material) model.getMaterials().get(0);
        final Mesh geom = (Mesh) model.getShape();

        //check if we have a skin
        final Skin skin = model.getSkin();
        if (skin != null) {
            final int maxWeightPerVertex = skin.getMaxWeightPerVertex();
            renderProperties.add(UNIFORM_NBJOINT, maxWeightPerVertex);

            //bind TBO
            final MotionModel mpm = (MotionModel) model.getParent();
            final TBO jtTbo = getOrBuildTBO(mpm);
            jtTbo.loadOnGpuMemory(gl);

            //update tbo if needed, it is used by several skins at the same time
            //check update time to update it only once by rendering
            if (getTboUpdate(mpm)!=context.getTimeNano()){
                setTboUpdate(mpm, context.getTimeNano());
                updateTBO(mpm);
                jtTbo.updateData(gl);
            }

            final int[] reservedTexture = context.getResourceManager().reserveTextureId();
            gl.glActiveTexture(reservedTexture[0]);
            jtTbo.bind(gl);
            GLUtilities.checkGLErrorsFail(gl);
            usedTextures.add(reservedTexture);
            renderProperties.add(UNIFORM_JOINTSAMPLER, jtTbo);
        }

        //dictionnary of all available properties
        final Dictionary uniformsProperties = new StackDictionary(new Dictionary[]{
            renderProperties, material.properties().asDictionary(), properties().asDictionary(), context.asDictionary()
        });

        //map all uniforms
        final Iterator iteU = program.getUniformNames().createIterator();
        while (iteU.hasNext()) {
            final Chars name = (Chars) iteU.next();
            final Object value = uniformsProperties.getValue(name);
            if (value != null) {
                final Uniform uniform = program.getUniform(name);
                final int type = uniform.getType();
                if (uniform.getGpuId() < 0) {
                    //this uniform is declared but has no id defined, it is likely
                    //unused by the shader program and the driver removed it
                    //when optimizing.
                    continue;
                }

                if (value instanceof UBO) {
                    final UBO ubo = (UBO) value;

                }

                switch (type) {
                    case ShaderUtils.TYPE_BOOL : uniform.setInt(gl, ((Boolean) value) ? 1 : 0 ); break;
                    case ShaderUtils.TYPE_INT : uniform.setInt(gl, ((Number) value).intValue()); break;
                    case ShaderUtils.TYPE_FLOAT : uniform.setFloat(gl, ((Number) value).floatValue()); break;
                    case ShaderUtils.TYPE_DOUBLE : uniform.setFloat(gl, ((Number) value).floatValue()); break;
                    case ShaderUtils.TYPE_VEC2 : uniform.setVec2(gl, ((Tuple) value).toFloat()); break;
                    case ShaderUtils.TYPE_VEC3 : uniform.setVec3(gl, ((Tuple) value).toFloat()); break;
                    case ShaderUtils.TYPE_VEC4 : uniform.setVec4(gl, ((Tuple) value).toFloat()); break;
                    case ShaderUtils.TYPE_MAT3 : uniform.setMat3(gl, ((Matrix) value).toArrayFloat()); break;
                    case ShaderUtils.TYPE_MAT4 : uniform.setMat4(gl, ((Matrix) value).toArrayFloat()); break;
                    case ShaderUtils.TYPE_SAMPLER2D :
                    case ShaderUtils.TYPE_SAMPLER2DMS :
                    case ShaderUtils.TYPE_SAMPLERCUBE :
                    case ShaderUtils.TYPE_ISAMPLERBUFFER :
                    case ShaderUtils.TYPE_SAMPLERBUFFER :
                    case ShaderUtils.TYPE_ISAMPLER2D :
                    case ShaderUtils.TYPE_USAMPLER2D :
                        final int[] res;
                        if (value instanceof TextureMapping) {
                            res = prepareTexture(context, (TextureMapping) value);
                        } else if (value instanceof TBO) {
                            final TBO tbo = (TBO) value;
                            res = context.getResourceManager().reserveTextureId();
                            gl.glActiveTexture(res[0]);
                            tbo.bind(gl);
                            GLUtilities.checkGLErrorsFail(gl);
                            usedTbos.add(tbo);
                        } else {
                            throw new RuntimeException("Unexpected sampler "+value);
                        }
                        uniform.setInt(gl, res[1]);
                        usedTextures.add(res);
                        break;
                    default : throw new RuntimeException("Unexpected type "+type);
                }
            }

        }

        //map all vertex attributes
        final Iterator iteV = program.getVertexAttributeNames().createIterator();
        while (iteV.hasNext()) {
            final Chars name = (Chars) iteV.next();
            final VBO value = (VBO) geom.getAttributes().getValue(name);
            if (value != null) {
                final VertexAttribute vertexAtt = program.getVertexAttribute(name, gl, -1);
                if (vertexAtt.getLayoutIdx() < 0) {
                    //this attribute is declared but has no id defined, it is likely
                    //unused by the shader program and the driver removed it
                    //when optimizing.
                    continue;
                }
                value.loadOnGpuMemory(gl);
                vertexAtt.enable(gl, value);
                usedVbos.add(value);
                usedAtts.add(vertexAtt);
            }
        }

        //render
        final IBO ibo = ((IBO) geom.getIndex());
        ibo.loadOnGpuMemory(gl);
        ibo.bind(gl);

        final IndexedRange[] ranges = geom.getRanges();
        for (int i=0;i<ranges.length;i++){
            GLEngineUtils.draw(gl, ranges[i], ibo);
        }
        GLUtilities.discardErrors(gl);
        GLUtilities.checkGLErrorsFail(gl);

        //disable vertex attributes
        for (int i=0,n=usedAtts.getSize(); i<n; i++) {
            ((VertexAttribute) usedAtts.get(i)).disable(gl);
        }
        program.disable(gl);
        ibo.unbind(gl);

        //unbind vbo and textures
        for (int i=0,n=usedVbos.getSize(); i<n; i++) {
            ((VBO) usedVbos.get(i)).unbind(gl);
        }
        for (int i=0,n=usedTextures.getSize(); i<n; i++) {
            final int[] res = (int[]) usedTextures.get(i);
            gl.glActiveTexture(res[0]);
            gl.glBindTexture(GLC.GL_TEXTURE_2D, 0);
            context.getResourceManager().releaseTextureId(res[0]);
        }
        for (int i=0,n=usedTbos.getSize(); i<n; i++) {
            final TBO tbo = (TBO) usedTbos.get(i);
            tbo.unbind(gl);
        }

    }

    private int[] prepareTexture(final RenderContext context, TextureMapping mapping) {

        final Texture2D texture = mapping.getTexture();
        if (texture==null) return null;

        final GL2ES2 gl = context.getGL().asGL2ES2();

        //load texture, image may have changed, no effect if already loaded
        texture.loadOnGpuMemory(gl);

        int[] reservedTexture = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTexture[0]);
        texture.bind(gl);
        return reservedTexture;
    }

    private static long getTboUpdate(MotionModel group) {
        Long upd = (Long) group.getUserProperties().getValue(GROUP_TBO_UPDATE);
        return upd == null ? -1 : upd;
    }

    private static void setTboUpdate(MotionModel group, long tboUpdate) {
        group.getUserProperties().add(GROUP_TBO_UPDATE, tboUpdate);
    }

    private static TBO getOrBuildTBO(MotionModel group) {
        TBO jointMatrixTbo = (TBO) group.getUserProperties().getValue(GROUP_TBO);
        if (jointMatrixTbo == null) {
            final int nb = group.getSkeleton().getAllJoints().getSize();
            //4*4 matrix for each joint
            jointMatrixTbo = new TBO(new float[nb*16], nb);
            jointMatrixTbo.setForgetOnLoad(false);
            group.getUserProperties().add(GROUP_TBO, jointMatrixTbo);
        }
        return jointMatrixTbo;
    }

    private static void updateTBO(MotionModel group) {
        TBO jointMatrixTbo = getOrBuildTBO(group);
        final Matrix4x4 matrix = new Matrix4x4();
        final float[] array = new float[16];

        final Sequence joints = group.getSkeleton().getAllJoints();
        final Float32Cursor fb = jointMatrixTbo.getBuffer().asFloat32().cursor();
        for (int i=0,n=joints.getSize();i<n;i++) {
            final Joint jt = (Joint) group.getSkeleton().getJointById(i);
            jt.getBindPose().multiply(jt.getInvertBindPose()).toMatrix(matrix);
            fb.write(matrix.toArrayFloatColOrder(array));
        }
    }

    @Override
    public void dispose(GLProcessContext context) {
        program.disable(context.getGL());
        program.unloadFromGpuMemory(context.getGL());
    }

}
