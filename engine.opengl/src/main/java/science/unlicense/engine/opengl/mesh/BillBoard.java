
package science.unlicense.engine.opengl.mesh;

import science.unlicense.geometry.api.BBox;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;


/**
 * A billboard is an image plan which keeps facing the
 * camera.
 *
 * @author Johann Sorel
 */
public class BillBoard extends AbstractGLGeometry {

    private VBO points = null;
    private float[] size = new float[2];
    private boolean ScreenCS = false;
    private VectorRW fixedAxis = null;
    private VectorRW anchor = new Vector2f64(0, -1);

    public VBO getPoints() {
        return points;
    }

    public void setPoints(VBO points) {
        this.points = points;
    }

    public float[] getSize() {
        return size;
    }

    public void setSize(float[] size) {
        this.size = size;
    }

    public boolean isScreenCS() {
        return ScreenCS;
    }

    public void setScreenCS(boolean ScreenCS) {
        this.ScreenCS = ScreenCS;
    }

    public VectorRW getFixedAxis() {
        return fixedAxis;
    }

    public void setFixedAxis(VectorRW fixedAxis) {
        this.fixedAxis = fixedAxis;
    }

    /**
     * The anchor locates the rotation point of the billboard.
     * An anchor of {0,-1} will locate the point at the centered bottom position.
     * @return
     */
    public VectorRW getAnchor() {
        return anchor;
    }

    public void setAnchor(VectorRW anchor) {
        this.anchor = anchor;
    }

    public BBox getBoundingBox() {
        return new BBox(3);
    }

    public boolean isDirty() {
        return points!=null && points.isDirty();
    }

    public void dispose(GL gl) {
        if (points!=null) {
            points.unloadFromGpuMemory(gl);
        }
    }

    public void calculateBBox() {
    }

    public GLGeometry copy() {
        final BillBoard cp = new BillBoard();
        cp.setName(getName());
        cp.setPoints(points);
        cp.setScreenCS(ScreenCS);
        cp.setFixedAxis(fixedAxis);
        cp.setSize(size);
        return cp;
    }

}
