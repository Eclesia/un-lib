
package science.unlicense.engine.opengl.scene;

import science.unlicense.display.api.scene.s2d.GeometryNode2D;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 *
 * @author Johann Sorel
 */
public class GLGeometryNode2D extends GeometryNode2D{

    private GLGeometry2D glGeometry;

    @Override
    public void setGeometry(PlanarGeometry geometry) {
        super.setGeometry(geometry);
        glGeometry = null;
    }

    public GLGeometry2D getGlGeometry() {
        if (glGeometry==null && geometry!=null){
            glGeometry = new GLGeometry2D(geometry);
        }
        return glGeometry;
    }

}
