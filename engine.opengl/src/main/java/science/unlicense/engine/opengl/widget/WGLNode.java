
package science.unlicense.engine.opengl.widget;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.impl.FlattenVisitor;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.GLTechnique;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.api.Affine;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.model3d.impl.technique.Technique;

/**
 *
 * @author Johann Sorel
 */
public class WGLNode extends WLeaf{

    private final FlattenVisitor visitor = new FlattenVisitor();

    protected final GraphicNode root = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
    /**
     * node transform to map widget position.
     */
    protected final GraphicNode inSceneNode = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
    private final MonoCamera camera = new MonoCamera();

    public WGLNode() {
        camera.setCameraType(MonoCamera.TYPE_FLAT);
        root.getChildren().add(inSceneNode);
        root.getChildren().add(camera);
    }

    public GraphicNode getInSceneNode() {
        return inSceneNode;
    }

    protected void renderGL(GLProcessContext context, Matrix4x4 SceneToWidget){

        // fit camera
        Widget parent = this;
        while (parent.getParent()!=null){
            parent = (Widget) parent.getParent();
        }
        final Extent ext = parent.getEffectiveExtent();
        camera.getNodeTransform().getTranslation().setXY(ext.get(0)/2.0, ext.get(1)/2.0);
        camera.getNodeTransform().getScale().setXY(ext.get(0)/2.0, -ext.get(1)/2.0);
        camera.getNodeTransform().notifyChanged();

        // map widget transform to gl space
        final Affine ttt = getNodeToRootSpace();
        final Matrix4x4 nodeToWg = new Matrix4x4().setToIdentity();
        nodeToWg.set(0, 0, ttt.get(0, 0));
        nodeToWg.set(0, 1, ttt.get(0, 1));
        nodeToWg.set(1, 0, ttt.get(1, 0));
        nodeToWg.set(1, 1, ttt.get(1, 1));
        nodeToWg.set(0, 3, ttt.get(0, 2));
        nodeToWg.set(1, 3, ttt.get(1, 2));
        inSceneNode.getNodeTransform().set(nodeToWg);

        visitor.visit(inSceneNode, context);
        Sequence nodes = visitor.getCollection();
        //do the rendering
        for (int i=0,n=nodes.getSize();i<n;i++){
            processRenderers((RenderContext) context, (GraphicNode) nodes.get(i));
        }
    }

    protected void processRenderers(RenderContext renderContext, GraphicNode glNode){
        final Sequence updaters = glNode.getUpdaters();
        for (int i=0,n=updaters.getSize();i<n;i++){
            ((Updater) updaters.get(i)).update(renderContext);
        }

        final Sequence techniques = glNode.getTechniques();
        for (int i=0,n=techniques.getSize();i<n;i++) {
            final Technique technique = (Technique) techniques.get(i);
            if (technique instanceof GLTechnique) {
                final GLTechnique gltech = (GLTechnique) technique;
                if (gltech.getPhasePredicate().evaluate(this)) {
                    gltech.render(renderContext, camera, glNode);
                }
            } else {
                System.out.println("Unsupported rendering technique "+technique.getClass().getName());
            }
        }
    }

}
