
package science.unlicense.engine.opengl;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.engine.opengl.mesh.GLGeometry;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.tessellation.Tessellator;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.MorphTarget;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GL2ES3;
import science.unlicense.gpu.api.opengl.GLBinding;
import science.unlicense.gpu.api.opengl.GLBindings;
import static science.unlicense.gpu.api.opengl.GLC.GL_PATCH_VERTICES;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Resource;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.Technique;

/**
 *
 * @author Johann Sorel
 */
public class GLEngineUtils {


    /** loop in scene tree and calculate bbox */
    private static final NodeVisitor BBOX_VISITOR = new NodeVisitor() {
        @Override
        public Object visit(Node node, Object context) {
            if (node instanceof Model) {
                final Model model = (Model) node;
                final Geometry shape = model.getShape();
                final Object[] a = (Object[]) context;

                if (a[0] == null) {
                    a[0] = new BBox(shape.getBoundingBox());
                } else {
                    ((BBox) a[0]).expand(shape.getBoundingBox());
                }
            }
            return super.visit(node, context);
        }
    };

    private GLEngineUtils() {}


    /**
     * Loop in scene tree and calculate bbox of all meshes.
     *
     * @param scene
     * @return
     */
    public static BBox calculateBBox(SceneNode scene) {
        final Object[] bboxA = new Object[1];
        BBOX_VISITOR.visit(scene, bboxA);
        return (BBox) bboxA[0];
    }

    /**
     * Loop on scene node and dispose all resources.
     *
     * @param node
     */
    public static void dispose(Object candidate, GLProcessContext context) {

        if (candidate instanceof SceneNode) {
            final SceneNode node = (SceneNode) candidate;

            if (node instanceof GraphicNode) {
                Sequence techniques = ((GraphicNode) node).getTechniques();
                //dispose techniques
                for (int i=0,n=techniques.getSize();i<n;i++){
                    dispose(techniques.get(i), context);
                }
            }

            if (node instanceof Model) {
                final Model model = (Model) node;

                final Iterator ite = model.getMaterials().createIterator();
                while (ite.hasNext()) {
                    dispose(ite.next(), context);
                }
                dispose(model.getShape(), context);

                if (model instanceof GLModel) {
                    dispose(((GLModel) model).getTessellator(), context);
                }
            }

            //check user properties, used by TBO joint matrix
            final Iterator propIte = node.getUserProperties().getValues().createIterator();
            while (propIte.hasNext()) {
                dispose(propIte.next(), context);
            }

            //dispose children
            final Iterator ite = node.getChildren().createIterator();
            while (ite.hasNext()){
                dispose(ite.next(), context);
            }
        }

        if (candidate instanceof Material) {
            final Material material = (Material) candidate;
            final Iterator ite = material.properties().asDictionary().getValues().createIterator();
            while (ite.hasNext()) {
                dispose(ite.next(), context);
            }
        }

        if (candidate instanceof GLGeometry) {
            ((GLGeometry) candidate).dispose(context.getGL());
        }

        if (candidate instanceof Mesh) {
            final Mesh mesh = (Mesh) candidate;

            dispose(mesh.getIndex(), context);

            final Iterator ite = mesh.getAttributes().getValues().createIterator();
            while (ite.hasNext()) {
                dispose(ite.next(), context);
            }

            final Iterator mite = mesh.getMorphs().createIterator();
            while (mite.hasNext()) {
                dispose(mite.next(), context);
            }

        }

        if (candidate instanceof MorphTarget) {
            dispose(((MorphTarget) candidate).getVertices(), context);
        }

        if (candidate instanceof Tessellator) {
            ((Tessellator) candidate).dispose(context);
        }

        if (candidate instanceof GLDisposable) {
            ((GLDisposable) candidate).dispose(context);
        }

        if (candidate instanceof Resource) {
            ((Resource) candidate).unloadFromGpuMemory(context.getGL());
        }
    }

    public static void draw(GL gl, IndexedRange range, IBO ibo){
        final int mode = range.getMode();
        final int indexOffset = range.getIndexOffset();
        final int count = range.getCount();
        final int patchSize = range.getPatchSize();

        if (mode == GLC.PRIMITIVE.PATCHES){
            if (gl.isGL2ES3()){
                //activate patch informations, and change type
                gl.asGL2ES3().glPatchParameteri(GL_PATCH_VERTICES, patchSize);
                GLUtilities.checkGLErrorsFail(gl);
            } else {
                //TODO log, but we should not log too often either, find a solution
                Loggers.get().log(new Chars("glPatchParameteri not supported for gl version < GL3"), Logger.LEVEL_INFORMATION);
            }
        }

        if (ibo != null) {
            final int byteOffset = ibo.getBytePerElement() * indexOffset;
            gl.asGL1().glDrawElements(mode,count,ibo.getGpuType(),byteOffset);
        } else {
            gl.asGL1().glDrawArrays(mode,indexOffset,count);
        }
        GLUtilities.checkGLErrorsFail(gl);
    }

    public static void drawInstanced(GL gl, IndexedRange range, IBO ibo, int nbInstance){
        final int mode = range.getMode();
        final int indexOffset = range.getIndexOffset();
        final int count = range.getCount();
        final int patchSize = range.getPatchSize();

        GL2ES3 gl2es3 = gl.asGL2ES3();

        if (mode == GLC.PRIMITIVE.PATCHES){
            if (gl.isGL2ES3()){
                //activate patch informations, and change type
                gl.asGL2ES3().glPatchParameteri(GL_PATCH_VERTICES, patchSize);
                GLUtilities.checkGLErrorsFail(gl);
            } else {
                //TODO log, but we should not log too often either, find a solution
                Loggers.get().log(new Chars("glPatchParameteri not supported for gl version < GL3"), Logger.LEVEL_INFORMATION);
            }
        }

        if (ibo != null) {
            final int byteOffset = ibo.getBytePerElement() * indexOffset;
            gl2es3.glDrawElementsInstanced(mode,count,ibo.getGpuType(),byteOffset,nbInstance);
        } else {
            gl2es3.glDrawArraysInstanced(mode,indexOffset,count,nbInstance);
        }

    }

    /**
     * Loop on scene nodes and convert base resources to OpenGL objects :
     * - Texture
     * - TupleGrid -> VBO/IBO
     * - BlinnPhongTechnique -> GLBlinnPhongTechnique
     * - SilhouetteBackFaceTechnique -> GLSilhouetteBackFaceTechnique
     *
     * @param node
     */
    public static void makeCompatible(SceneNode node) {

        final GLBinding binding = GLBindings.getBindings()[0];
        final BufferFactory factory = binding.getBufferFactory();

        if (node instanceof Model) {
            final Model model = (Model) node;
            final Geometry shape = model.getShape();
            final Mesh idxGeom = (Mesh) shape;

            //convert index
            TupleGrid index = idxGeom.getIndex();
            if (index != null) {
                
                if (index instanceof InterleavedTupleGrid) {
                    Buffer buffer = ((InterleavedTupleGrid) index).getPrimitiveBuffer();
                    
                    if (buffer.getFactory() != factory) {
                        buffer = buffer.copy(factory, null, Endianness.LITTLE_ENDIAN);
                        final IBO ibo = new IBO(buffer);
                        ((DefaultMesh) idxGeom).setIndex(ibo);
                    } else if (!(index instanceof IBO)) {
                        final IBO ibo = new IBO(buffer);
                        ((DefaultMesh) idxGeom).setIndex(ibo);
                    }
                } else {
                    throw new RuntimeException("TODO");
//                    final Buffer buffer = factory.create(index.getExtent().getL(0), 
//                            (NumberType) index.getNumericType(), Endianness.LITTLE_ENDIAN);
//                    
//                    final TupleGridCursor cursor = index.cursor();
//                    
//                    final IBO ibo = new IBO(buffer);
//                    ((DefaultMesh) idxGeom).setIndex(ibo);
                }
            }

            //convert attributes
            final Iterator keys = idxGeom.getAttributes().getKeys().createIterator();
            while (keys.hasNext()) {
                final Object key = keys.next();
                final TupleGrid tupleGrid = (TupleGrid) idxGeom.getAttributes().getValue(key);

                if (index instanceof InterleavedTupleGrid) {
                    Buffer buffer = ((InterleavedTupleGrid) index).getPrimitiveBuffer();
                    if (buffer.getFactory() != factory) {
                        buffer = buffer.copy(factory, null, Endianness.LITTLE_ENDIAN);
                        final VBO vbo = new VBO(buffer, tupleGrid.getSampleSystem().getNumComponents());
                        idxGeom.getAttributes().add(key, vbo);
                    } else if (!(index instanceof IBO)) {
                        final VBO vbo = new VBO(buffer, tupleGrid.getSampleSystem().getNumComponents());
                        idxGeom.getAttributes().add(key, vbo);
                    }
                } else {
                    throw new RuntimeException("TODO");
                }
            }

            //convert material textures

            //TODO cleanup MeshRenderer and technique, keep only technique
            final Sequence techniques = model.getTechniques();
            for (int i=0,n=techniques.getSize(); i<n; i++) {
                Technique tech = (Technique) techniques.get(i);
//                if (tech instanceof SimpleBlinnPhong) {
//                    GLBlinnPhongTechnique cp = new GLBlinnPhongTechnique();
//                    cp.getProperties().set(tech.getProperties());
//                    cp.getState().set(tech.getState());
//                    techniques.replace(i, cp);
//                }
            }

        }

        Iterator ite = node.getChildren().createIterator();
        while (ite.hasNext()) {
            Object cdt = ite.next();
            if (cdt instanceof SceneNode) {
                makeCompatible((SceneNode) cdt);
            }
        }
    }
}
