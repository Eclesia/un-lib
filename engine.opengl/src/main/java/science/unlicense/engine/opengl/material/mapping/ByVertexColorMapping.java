
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.gpu.impl.opengl.resource.VBO;

/**
 * By vertex color texture.
 *
 * @author Johann Sorel
 */
public final class ByVertexColorMapping implements Mapping{

    private VBO colors;
    private boolean hasAlpha;
    private boolean dirty = true;

    /**
     * Default color mapping.
     * With a red color for all vertices.
     */
    public ByVertexColorMapping() {
        final Buffer.Float32 colors = DefaultBufferFactory.INSTANCE.createFloat32(3);
        colors.write(0, new float[]{1,0,0});
        this.colors = new VBO(colors.getBuffer(), 3);
        hasAlpha = false;
    }

    public ByVertexColorMapping(Buffer colors, boolean hasAlpha) {
        this.colors = new VBO(colors,hasAlpha ? 4 : 3);
        this.hasAlpha = hasAlpha;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    /**
     * Float buffer for colors.
     * 3 or 4 components based on hasAlpha value.
     * @return FloatBuffer
     */
    public VBO getColors() {
        return colors;
    }

    /**
     * Set colors buffer.
     * 3 or 4 components based on hasAlpha value.
     * @param colors
     */
    public void setColors(Buffer colors) {
        this.colors = new VBO(colors,hasAlpha ? 4 : 3);
    }

    /**
     * Get if colors are expressed with 3 or 4 components.
     * RGB or RGBA.
     * @return true if colors are expressed with alpha
     */
    public boolean getHasAlpha() {
        return hasAlpha;
    }

    /**
     * Set colors type, RGB or RGBA.
     * 3 or 4 components by colors.
     * @param hasAlpha
     */
    public void setHasAlpha(boolean hasAlpha) {
        this.hasAlpha = hasAlpha;
    }

    @Override
    public void dispose(GLProcessContext context) {

    }

}
