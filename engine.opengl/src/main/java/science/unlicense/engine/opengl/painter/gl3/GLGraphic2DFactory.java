
package science.unlicense.engine.opengl.painter.gl3;

import science.unlicense.engine.opengl.scene.GLGeometryNode2D;
import science.unlicense.engine.opengl.scene.GLImageNode2D;
import science.unlicense.engine.opengl.scene.GLTextNode2D;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.s2d.GeometryNode2D;
import science.unlicense.display.api.scene.s2d.Graphic2DFactory;
import science.unlicense.display.api.scene.s2d.ImageNode2D;
import science.unlicense.display.api.scene.s2d.TextNode2D;
import science.unlicense.geometry.api.system.CoordinateSystems;

/**
 *
 * @author Johann Sorel
 */
public class GLGraphic2DFactory implements Graphic2DFactory{

    @Override
    public SceneNode createNode() {
        return new DefaultSceneNode(CoordinateSystems.UNDEFINED_2D);
    }

    @Override
    public GeometryNode2D createGeometryNode() {
        return new GLGeometryNode2D();
    }

    @Override
    public ImageNode2D createImageNode() {
        return new GLImageNode2D();
    }

    @Override
    public TextNode2D createTextNode() {
        return new GLTextNode2D();
    }

}
