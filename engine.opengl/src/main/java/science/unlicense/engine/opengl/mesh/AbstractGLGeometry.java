
package science.unlicense.engine.opengl.mesh;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.impl.AbstractGeometry;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractGLGeometry extends AbstractGeometry implements GLGeometry {

    private Chars name = Chars.EMPTY;

    protected AbstractGLGeometry() {
        super(3);
    }

    /**
     * {@inheritDoc }
     */
    public Chars getName() {
        return name;
    }

    /**
     * {@inheritDoc }
     */
    public void setName(Chars name) {
        this.name = name;
    }

}
