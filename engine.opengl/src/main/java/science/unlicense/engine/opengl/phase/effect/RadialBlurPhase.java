
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture;
import science.unlicense.gpu.impl.opengl.shader.Uniform;

/**
 * Radial blur effect.
 *
 * @author Johann Sorel
 */
public class RadialBlurPhase extends AbstractTexturePhase {

    private static final Chars UNIFORM_CENTER = Chars.constant("blurCenter");
    private static final Chars UNIFORM_PARAMS = Chars.constant("blurParams");

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/radialblur-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final RadialBlurActor actor = new RadialBlurActor();
    private float[] center = new float[2];
    private float nbSamples = 10.0f;
    private float strength = 0.1f;
    private float bright = 1.0f;
    private Uniform uniCenter;
    private Uniform uniParams;

    public RadialBlurPhase(Texture texture) {
        this(null,texture,new float[2],10f,0.1f,1.0f);
    }

    public RadialBlurPhase(Texture texture, float[] blurCenter,
            float nbSamples, float strength, float bright) {
        this(null,texture,blurCenter,nbSamples,strength,bright);
    }

    public RadialBlurPhase(FBO output, Texture texture, float[] blurCenter,
            float nbSamples, float strength, float bright) {
        super(output,texture);
        this.center = blurCenter;
        this.nbSamples = nbSamples;
        this.strength = strength;
        this.bright = bright;
    }

    public float[] getCenter() {
        return center;
    }

    public void setCenter(float[] center) {
        this.center = center;
    }

    public float getBright() {
        return bright;
    }

    public void setBright(float bright) {
        this.bright = bright;
    }

    public float getNbSamples() {
        return nbSamples;
    }

    public void setNbSamples(float nbSamples) {
        this.nbSamples = nbSamples;
    }

    public float getStrength() {
        return strength;
    }

    public void setStrength(float strength) {
        this.strength = strength;
    }

    @Override
    protected RadialBlurActor getActor() {
        return actor;
    }

    private final class RadialBlurActor extends DefaultActor{

        public RadialBlurActor() {
            super(new Chars("RadialBlur"),false,null,null,null,null,SHADER_FR,true,true);
        }

        @Override
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL2ES2 gl = context.getGL().asGL2ES2();

            //get all uniforms
            if (uniCenter == null){
                uniCenter = program.getUniform(UNIFORM_CENTER);
                uniParams = program.getUniform(UNIFORM_PARAMS);
            }
            uniCenter.setVec2(gl, center);
            uniParams.setVec3(gl, new float[]{nbSamples,strength,bright});
        }

        @Override
        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniCenter = null;
            uniParams = null;
        }

    }

}
