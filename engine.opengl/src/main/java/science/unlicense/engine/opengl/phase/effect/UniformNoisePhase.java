
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture;
import science.unlicense.gpu.impl.opengl.shader.Uniform;

/**
 *
 * @author Johann Sorel
 */
public class UniformNoisePhase extends AbstractTexturePhase {

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/uniformnoise-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final UNActor actor = new UNActor();
    private Uniform timeUniform;

    public UniformNoisePhase(Texture texture) {
        super(texture);
    }

    public UniformNoisePhase(FBO output, Texture texture) {
        super(output, texture);
    }

    @Override
    protected UNActor getActor() {
        return actor;
    }

    private final class UNActor extends DefaultActor{

        public UNActor() {
            super(new Chars("UniformNoise"),false,null,null,null,null,SHADER_FR,true,true);
        }

        @Override
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL2ES2 gl = context.getGL().asGL2ES2();
            timeUniform = program.getUniform(new Chars("time"));
            timeUniform.setFloat(gl, (float) Math.random());
        }

        @Override
        public void dispose(GLProcessContext context) {
            super.dispose(context);
            timeUniform = null;
        }

    }

}
