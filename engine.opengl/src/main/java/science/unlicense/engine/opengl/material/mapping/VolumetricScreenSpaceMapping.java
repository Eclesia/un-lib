
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;

/**
 * Volumetric is use to simulate color attenuation based on volume/depth information.
 * This is similar to a fog technique where the fragment position is compared to a depth texture.
 * the farther the fragment is from the depth, the greater alpha will be.
 *
 * This mapping should be used with the layer multiply method.
 *
 * @author Johann Sorel
 */
public class VolumetricScreenSpaceMapping implements Mapping {

    private Texture2D texture;

    public VolumetricScreenSpaceMapping() {
    }

    /**
     *
     * @param depthtexture expecting a 1D float texture
     */
    public VolumetricScreenSpaceMapping(Texture2D depthtexture) {
        this.texture = depthtexture;
    }

    public Texture2D getTexture() {
        return texture;
    }

    public void setTexture(Texture2D texture) {
        this.texture = texture;
    }

    @Override
    public boolean isDirty() {
        if (texture!=null) {
            return texture.isDirty();
        }
        return false;
    }

    @Override
    public void dispose(GLProcessContext context) {
        if (texture!=null) {
            texture.unloadFromGpuMemory(context.getGL());
        }
    }

}
