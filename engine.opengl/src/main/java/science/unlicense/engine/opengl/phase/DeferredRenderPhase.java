
package science.unlicense.engine.opengl.phase;

import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;

/**
 * Render to default output.
 *
 * Output attachments :
 * - vec4 out_diffuse = fragment diffuse color
 * - vec4 out_specular = fragment specular color
 * - vec3 out_position = fragment world position
 * - vec3 out_normal = fragment world normal
 * - int out_meshId = fragment parent mesh id
 * - int out_vertexId = fragment parent vertex id
 *
 * @author Johann Sorel
 */
public class DeferredRenderPhase extends RenderPhase {

    public DeferredRenderPhase(SceneNode root, MonoCamera camera, GBO gbo) {
        super(root, camera, gbo);
    }

    @Override
    public GBO getOutputFbo() {
        return (GBO) super.getOutputFbo();
    }

}
