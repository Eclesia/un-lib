
package science.unlicense.engine.opengl.desktop.opengl;

import science.unlicense.display.api.desktop.Frame;
import science.unlicense.display.api.desktop.FrameDecoration;
import science.unlicense.display.api.layout.Margin;

/**
 *
 * @author Johann Sorel
 */
public class GLFrameDecoration implements FrameDecoration{

    private GLUIFrame frame;

    @Override
    public GLUIFrame getFrame() {
        return frame;
    }

    @Override
    public void setFrame(Frame frame) {
        this.frame = (GLUIFrame) frame;
    }

    @Override
    public Margin getMargin() {
        return frame.getGLFrame().getSource().getBinding().getFrameMargin();
    }

}
