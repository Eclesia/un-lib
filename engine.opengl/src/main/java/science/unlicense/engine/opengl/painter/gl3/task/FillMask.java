
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontContext;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.impl.transform.Projections;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Affines;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class FillMask extends PainterTask{

    private static final BBox SHADER_BBOX = new BBox(new double[]{0,0}, new double[]{1,1});

    private final FontChoice font;
    private final int unicode;

    private final Object paint;
    private final Affine2 mv;
    private final AlphaBlending blending;


    public FillMask(FontChoice font, int unicode, Object paint, Affine2 mv, AlphaBlending blending) {
        this.font = font;
        this.unicode = unicode;
        this.paint = paint;
        this.mv = mv;
        this.blending = blending;
    }

    @Override
    public void execute(GL3Painter2D worker) {

        final Color color;
        if (paint instanceof ColorPaint){
            color = ((ColorPaint) paint).getColor();
        } else if (paint instanceof Color){
            color = (Color) paint;
        } else {
            color = Color.BLUE;
        }

        FontStore store = FontContext.getResolver(font.getFamilies()[0]);
        BBox glyphBox = store.getFont(font).getMetaData().getGlyphBox();
        AffineRW stretchedMatrix = Projections.stretched(SHADER_BBOX, glyphBox);

        try {
            final FontPage fontPage = worker.getFontPage(font);
            final Texture2D texture = fontPage.load();
            stretchedMatrix = mv.multiply(stretchedMatrix);

            final Affine2 maskTrs = fontPage.getGlyphTransform(unicode);
            final Affine mvp = worker.p.multiply(stretchedMatrix);

            VectorRW v = new Vector2f64(1,0);
            Affines.transformNormal(stretchedMatrix.invert(), v,v);
            maskTrs.transform(v,v);
            double ratio = v.length();

            //render using the mask
            configureBlending(worker,blending);
            worker.programs.fillMaskProg.enable(worker.gl);
            worker.programs.fillMaskProg.uniformMVP.setMat3(worker.gl, mvp.toMatrix().toArrayFloat());
            worker.programs.fillMaskProg.uniformMASKTRS.setMat3(worker.gl, maskTrs.toMatrix().toArrayFloat());
            worker.programs.fillMaskProg.uniformCOLOR.setVec4(worker.gl, color.toRGBAPreMul());
            worker.programs.fillMaskProg.uniformBUFFER.setFloat(worker.gl, (float) (ratio/2.0));

            //bind texture and sampler
            final int[] reservedTexture = new int[]{33984,0};
            worker.gl.glActiveTexture(reservedTexture[0]);
            texture.loadOnGpuMemory(worker.gl);
            texture.bind(worker.gl);
            worker.programs.fillMaskProg.uniformMASK.setInt(worker.gl, reservedTexture[1]);
            GLUtilities.checkGLErrorsFail(worker.gl);

            worker.gl.glDrawArrays(GL_TRIANGLE_FAN,0,4);
            GLUtilities.checkGLErrorsFail(worker.gl);

            //release sampler
            worker.gl.glActiveTexture(reservedTexture[0]);
            texture.unbind(worker.gl);

            worker.programs.fillMaskProg.disable(worker.gl);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
