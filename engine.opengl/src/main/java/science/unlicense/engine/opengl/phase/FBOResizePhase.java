

package science.unlicense.engine.opengl.phase;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.impl.opengl.resource.FBO;

/**
 * Should be placed first in the context phases.
 * This phase loop on all phases in the context and resize all AbstractPhase FBO
 * to match the context size.
 *
 * @author Johann Sorel
 */
public class FBOResizePhase extends AbstractFboPhase {

    private int resizeWidth = -1;
    private int resizeHeight = -1;

    public FBOResizePhase() {
    }

    public FBOResizePhase(Chars id) {
        super(id);
    }

    /**
     * Set used FBO resolution.
     * Use 0 or negative for dynamic size
     *
     * @param resizeWidth
     */
    public void setResizeWidth(int resizeWidth) {
        this.resizeWidth = resizeWidth;
    }

    public int getResizeWidth() {
        return resizeWidth;
    }

    /**
     * Set used FBO resolution.
     * Use 0 or negative for dynamic size
     *
     * @param resizeHeight
     */
    public void setResizeHeight(int resizeHeight) {
        this.resizeHeight = resizeHeight;
    }

    public int getResizeHeight() {
        return resizeHeight;
    }

    @Override
    protected void processInternal(GLProcessContext context) throws GLException {
        final Sequence seq = context.getPhases();
        for (int i=0,n=seq.getSize();i<n;i++){
            resize(context,(Phase) seq.get(i));
        }
    }

    private void resize(GLProcessContext context, Phase phase){
        if (phase instanceof AbstractFboPhase){
            final FBO fbo = ((AbstractFboPhase) phase).getOutputFbo();
            if (fbo!=null){
                final int width = (resizeWidth<1) ? (int) context.getViewRectangle().getWidth(): resizeWidth;
                final int height = (resizeHeight<1) ? (int) context.getViewRectangle().getHeight(): resizeHeight;
                fbo.resize(context.getGL(), width, height);
            }
        } else if (phase instanceof PhaseSequence){
            final Sequence seq = ((PhaseSequence) phase).getPhases();
            for (int i=0,n=seq.getSize();i<n;i++){
                resize(context, (Phase) seq.get(i));
            }
        }
    }
}
