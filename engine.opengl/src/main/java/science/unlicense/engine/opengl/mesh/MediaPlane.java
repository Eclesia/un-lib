
package science.unlicense.engine.opengl.mesh;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.engine.opengl.GLDisposable;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.media.api.ImagePacket;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.RenderState;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.Technique;

/**
 *
 * @author Johann Sorel
 */
public class MediaPlane extends DefaultGraphicNode implements GLDisposable {

    private final MediaReadStream reader;

    //cache all video images
    //TODO must improve this
    private final Sequence times = new ArraySequence();
    private final Sequence textures = new ArraySequence();

    private final DefaultModel plan;
    private final UVMapping uvmapping;
    private Extent.Long extent;

    public MediaPlane(MediaReadStream reader) {
        this(reader,
             new Vector3f64(-0.5, -0.5, 0),
             new Vector3f64(-0.5, +0.5, 0),
             new Vector3f64(+0.5, +0.5, 0),
             new Vector3f64(+0.5, -0.5, 0));

    }

    public MediaPlane(MediaReadStream reader, VectorRW v1,VectorRW v2,VectorRW v3,VectorRW v4) {
        super(CoordinateSystems.UNDEFINED_3D);
        this.reader = reader;

        uvmapping = new UVMapping();
        final VBO uv = new VBO(new float[]{
            0,1,
            0,0,
            1,0,
            1,1
        },2);
        plan = new DefaultModel();
        plan.setShape(DefaultMesh.createPlan(v1,v2,v3,v4));
        GLEngineUtils.makeCompatible(plan);
        SimpleBlinnPhong.view((Material) plan.getMaterials().get(0)).setDiffuseTexture(new Layer(uvmapping));
        ((DefaultMesh) plan.getShape()).setUVs(uv);
        ((Technique) plan.getTechniques().get(0)).getState().setCulling(RenderState.CULLING_NONE);
        children.add(plan);

        getUpdaters().add(new VideoUpdater());

        try{
            for (MediaPacket pack=reader.next();pack!=null;pack=reader.next()) {
                final Image mi = ((ImagePacket) pack).getImage();
                extent = mi.getExtent();
                final Texture2D tr = new Texture2D(mi);
                times.add(pack.getStartTime());
                textures.add(tr);
            }
        }catch(Exception ex) {
            ex.printStackTrace();
        }

    }

    public Extent.Long getExtent() {
        return extent;
    }

    @Override
    public void dispose(GLProcessContext context) {
        GLEngineUtils.dispose(this, context);

        for (int i=0,n=textures.getSize();i<n;i++) {
            ((Texture2D) textures.get(i)).unloadFromGpuMemory(context.getGL());
        }
    }

    private class VideoUpdater implements Updater{

        long before = -1;
        long beforeNano = 0;

        public void update(DisplayTimerState timer) {
            final RenderContext context = (RenderContext) timer;
            long candidate;
            while (true) {
                if (before==-1) before = context.getTimeNano();
                long time = (context.getTimeNano()-before) / 1000000;

                for (int i=0,n=times.getSize();i<n;i++) {
                    candidate = (Long) times.get(i);
                    if (candidate>=time) {
                        final Texture2D tr = (Texture2D) textures.get(i);
                        if (!tr.isOnGpuMemory()) {
                            tr.loadOnGpuMemory(context.getGL());
                        }
                        uvmapping.setTexture(tr);
                        return;
                    }
                }

                //new loop
                before = -1;
            }

        }

    }

}
