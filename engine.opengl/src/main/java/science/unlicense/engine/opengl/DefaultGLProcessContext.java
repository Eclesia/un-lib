
package science.unlicense.engine.opengl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.engine.opengl.phase.Phase;
import science.unlicense.engine.opengl.phase.TasksPhase;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLUtilities;

/**
 * A process context is a GLCallback which contains additional informations
 * on the rendering.
 *
 * The rendering process is composed of a sequence of Phase, allowing
 * some preprocessing and postprocessing effects.
 *
 * @author Johann Sorel
 */
public class DefaultGLProcessContext implements GLProcessContext, GLCallback {

    private final Sequence phases = new ArraySequence();
    private final TasksPhase tasks = new TasksPhase();
    private final GLResourceManager resourceManager = new GLResourceManager();
    private final GLStatistics statistics = new GLStatistics(this);
    //record time at each rendering, used for animation
    private long diffTime;
    private long time;
    //OpenGL information
    private final Rectangle glRectangle = new Rectangle(0,0,1,1);
    private final Rectangle viewRectangle = new Rectangle(0,0,1,1);
    protected GLSource source;
    private GL gl;

    public DefaultGLProcessContext() {
    }

    /**
     * Attach this context callback to given source.
     *
     * @param source
     */
    public DefaultGLProcessContext(GLSource source) {
        this.source = source;
        // link GL events
        source.getCallbacks().add(this);
    }

    @Override
    public Rectangle getGLRectangle() {
        return glRectangle;
    }

    @Override
    public Rectangle getViewRectangle() {
        return viewRectangle;
    }

    @Override
    public long getTimeNano() {
        return time;
    }

    @Override
    public long getDiffTimeNano() {
        return diffTime;
    }

    @Override
    public float getTimeSecond() {
        return time / 1000000000f;
    }

    @Override
    public float getDiffTimeSecond() {
        return diffTime  / 1000000000f;
    }

    @Override
    public GLSource getSource() {
        return source;
    }

    @Override
    public GL getGL() {
        GLUtilities.checkGLErrorsFail(gl);
        return gl;
    }

    @Override
    public void addTask(GLExecutable executable) {
        tasks.addTask(executable);
    }

    @Override
    public Sequence getPhases() {
        return phases;
    }

    @Override
    public GLResourceManager getResourceManager() {
        return resourceManager;
    }

    @Override
    public GLStatistics getStatistics() {
        return statistics;
    }

    ////////////////////////////////////////////////////////////////////////////
    // GL EVENTS ///////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public void execute(final GLSource source) {
        statistics.start();

        glRectangle.setX(source.getX());
        glRectangle.setY(source.getY());
        glRectangle.setWidth(source.getWidth());
        glRectangle.setHeight(source.getHeight());
        viewRectangle.set(glRectangle);

        long last = this.time;
        this.time = System.nanoTime();
        if (last==0) {
            //first rendering
            last = this.time;
        }
        this.diffTime = this.time - last;
        this.source = source;
        this.gl = source.getGL();

        for (int i=0,n=phases.getSize();i<n;i++) {
            final Phase phase = (Phase) phases.get(i);
            if (!phase.isEnable()) continue;
            try {
                phase.process(this);
            } catch (GLException ex) {
                Loggers.get().log(ex, Logger.LEVEL_WARNING);
            }
            runTasks();
        }
        runTasks();

        statistics.end();
    }

    @Override
    public void dispose(final GLSource glad) {
        this.source = glad;
        this.gl = glad.getGL();
        runTasks();
        for (int i=0,n=phases.getSize();i<n;i++) {
            final Phase phase = (Phase) phases.get(i);
            phase.dispose(this);
        }
        getResourceManager().dispose(this);
    }

    /**
     * Run waiting tasks.
     */
    private void runTasks() {
        statistics.startLog(this, new Chars("Tasks"));
        tasks.runTasks(this);
        statistics.endLog();
    }

}
