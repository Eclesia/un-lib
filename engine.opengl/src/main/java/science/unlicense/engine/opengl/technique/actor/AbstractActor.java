

package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;

/**
 * Shader actor doing nothing.
 *
 * @author Johann Sorel
 */
public abstract class AbstractActor implements Actor{

    /**
     * Keep the current program state.
     * This is to ensure the actor is not used in multiple programs
     * without being disposed between them.
     */
    protected boolean initialized = false;
    protected final boolean concurrent;
    private final Chars reuseId;

    public AbstractActor(Chars reuseId) {
        this(reuseId,false);
    }

    public AbstractActor(Chars reuseId, boolean concurrent) {
        this.reuseId = reuseId;
        this.concurrent = concurrent;
    }

    @Override
    public Chars getReuseUID() {
        return reuseId;
    }

    @Override
    public boolean isDirty() {
        return false;
    }

    @Override
    public boolean usesTesselationShader() {
        return false;
    }

    @Override
    public boolean usesGeometryShader() {
        return false;
    }

    @Override
    public void initProgram(RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom) {
        if (concurrent) return;
        if (initialized) throw new RuntimeException("This actor has been initialized by another program but not disposed before reuse.");
        initialized = true;
    }

    @Override
    public void preExecutionGL(RenderContext context, Object candidate) {
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
    }

    @Override
    public void postDrawGL(RenderContext context, ActorProgram program) {
    }

    @Override
    public void dispose(GLProcessContext context) {
        initialized = false;
    }

}

