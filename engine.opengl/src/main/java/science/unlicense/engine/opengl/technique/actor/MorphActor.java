
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.geometry.impl.MorphTarget;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.gpu.impl.opengl.shader.VertexAttribute;
import science.unlicense.math.api.Maths;

/**
 * Morph target actor.
 *
 * @author Johann Sorel
 */
public class MorphActor extends AbstractActor{

    public static final Chars LAYOUT_MORPH0 = Chars.constant("l_morph0");
    public static final Chars LAYOUT_MORPH1 = Chars.constant("l_morph1");
    public static final Chars LAYOUT_MORPH2 = Chars.constant("l_morph2");

    private static final ShaderTemplate MORPH_VE;
    static {
        try{
            MORPH_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/morph-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private static final Chars MORPH_FACTOR = Chars.constant("MORPH_FACTOR");
    private static final Sorter FACTOR_SORTER = new Sorter() {
        @Override
        public int sort(Object first, Object second) {
            final float f1 = ((MorphTarget) first).getFactor();
            final float f2 = ((MorphTarget) second).getFactor();
            return Double.compare(f2, f1);
        }
    };

    //GL loaded informations
    private Uniform uniFactors;
    private final VertexAttribute[] attMorphs = new VertexAttribute[3];

    private final Sequence morphTargets;

    public MorphActor(Sequence morphset) {
        super(new Chars("Morph"));
        this.morphTargets = morphset;
    }

    @Override
    public int getMinGLSLVersion() {
        return MORPH_VE.getMinGLSLVersion();
    }

    @Override
    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate vs = template.getVertexShaderTemplate();
        vs.append(MORPH_VE);
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        if (uniFactors==null){
            uniFactors = program.getUniform(MORPH_FACTOR);
            attMorphs[0] = program.getVertexAttribute(LAYOUT_MORPH0, gl);
            attMorphs[1] = program.getVertexAttribute(LAYOUT_MORPH1, gl);
            attMorphs[2] = program.getVertexAttribute(LAYOUT_MORPH2, gl);
        }

        //find the morph frames with highest factors, we will only use 3.
        final Object[] ms = morphTargets.toArray();
        Arrays.sort(ms, FACTOR_SORTER);

        final float[] factors = new float[3];
        for (int i=0,n=Maths.min(ms.length,3);i<n;i++){
            final MorphTarget frame = (MorphTarget) ms[i];
            factors[i] = frame.getFactor();
            attMorphs[i].enable(gl, (VBO) frame.getVertices());
        }

        uniFactors.setVec3(gl, factors);
    }

    @Override
    public void postDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();
        attMorphs[0].disable(gl);
        attMorphs[1].disable(gl);
        attMorphs[2].disable(gl);
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniFactors = null;
        Arrays.fill(attMorphs, null);
    }

}
