
package science.unlicense.engine.opengl.desktop.opengl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.desktop.Frame;
import science.unlicense.display.api.desktop.FrameManager;
import science.unlicense.display.api.desktop.cursor.NamedCursor;
import science.unlicense.geometry.api.Extent;
import science.unlicense.gpu.api.opengl.GLBindings;
import science.unlicense.gpu.api.opengl.GLFrame;

/**
 *
 * @author Johann Sorel
 */
public class GLUIFrameManager implements FrameManager{

    public static final GLUIFrameManager INSTANCE = new GLUIFrameManager();
    public static final Chars NAME = Chars.constant("NEWT");

    public GLUIFrameManager() {}

    @Override
    public Chars getName() {
        return NAME;
    }

    @Override
    public boolean isAvailable() {
        return GLBindings.getBindings().length > 0;
    }

    @Override
    public Chars[] getCursorNames() {
        return new Chars[]{NamedCursor.DEFAULT.getName(),NamedCursor.NULL.getName()};
    }

    @Override
    public GLUIFrame createFrame(boolean translucent) {
        return createFrame(null, false, translucent);
    }
    public GLUIFrame createFrame(boolean translucent, boolean withWidget) {
        GLUIFrame frame = createFrame(null, false, translucent);
        frame.setWidgetLayerVisible(withWidget);
        return frame;
    }

    @Override
    public GLUIFrame createFrame(Frame parent, boolean modale, boolean translucent) {
        final GLFrame glFrame = GLBindings.getBindings()[0].createFrame(!translucent, null);

        final GLUIFrame frame = new GLUIFrame((GLUIFrame) parent,glFrame);
        frame.setModale(modale);
        return frame;
    }

    @Override
    public Extent getDisplaySize() {

        //TODO
        return new Extent.Long(1000, 1000);

//        // hack to initialize GL for BCM_IV (Rasp.Pi)
//        GLProfile.initSingleton();
//        // On the Raspberry Pi broadcom screenmode resolution is detected
//        // during GL initialization, using the dispmanx api.
//
//        // A screen may span multiple MonitorDevices representing their combined virtual size.
//        // http://jogamp.org/deployment/jogamp-current/javadoc/jogl/javadoc/com/jogamp/newt/Screen.html
//        // http://jogamp.org/files/screenshots/newt-mmonitor/html/
//        final int screenIdx = 0;
//        final Display dpy = NewtFactory.createDisplay(null); // Get the default display on the system
//        final Screen screen = NewtFactory.createScreen(dpy, screenIdx);
//        screen.addReference();
//        int width = screen.getWidth();
//        int height = screen.getHeight();
//        screen.removeReference();
//        return new Extent.Double(width, height);


         /* List all available monitor mode resolutions supported on the system */
        // http://jogamp.org/deployment/jogamp-current/javadoc/jogl/javadoc/com/jogamp/newt/MonitorMode.html
//        List<MonitorMode> allMonitorModes = screen.getMonitorModes();
//        {
//            int i = 0;
//            for (Iterator<MonitorMode> iMode = allMonitorModes.iterator(); iMode.hasNext(); i++) {
//                System.err.println("All[" + i + "]: " + iMode.next());
//            }
//        }

         /* List all available monitor devices attached to the system
        some machines got more than one monitor */
        // http://jogamp.org/deployment/jogamp-current/javadoc/jogl/javadoc/com/jogamp/newt/MonitorDevice.html
//        List<MonitorDevice> monitors = screen.getMonitorDevices();
//        int j=0;
//        for (Iterator<MonitorDevice> iMonitor=monitors.iterator(); iMonitor.hasNext(); j++) {
//        MonitorDevice monitor = iMonitor.next();
//        System.err.println(j+": "+monitor);
//
//         /* List all supported modes by each attached monitor */
//        List<MonitorMode> modes = monitor.getSupportedModes();
//        int i=0;
//        for (Iterator<MonitorMode> iMode=modes.iterator(); iMode.hasNext(); i++) {
//            System.err.println("["+j+"]["+i+"]: "+iMode.next());
//        }
//
//        MonitorMode sm_o = monitor.getOriginalMode();
//        MonitorMode sm_c = monitor.queryCurrentMode();
//        System.err.println("[0] orig : "+sm_o);
//        System.err.println("[0] current: "+sm_c);
//        }
//
//        screen.removeReference();
    }

    @Override
    public void setIcon(Chars imagePath) {
        System.setProperty("newt.window.icons", imagePath.toString());
    }

}
