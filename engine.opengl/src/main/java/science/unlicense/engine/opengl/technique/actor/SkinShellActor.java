
package science.unlicense.engine.opengl.technique.actor;

import science.unlicense.code.glsl.io.ShaderProgramTemplate;
import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.TBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.gpu.impl.opengl.shader.VertexAttribute;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.physics.api.skeleton.Joint;

/**
 *
 * @author Johann Sorel
 */
public class SkinShellActor extends ModelActor {

    public static final Chars LAYOUT_JOINTWEIGHT = Chars.constant("l_jointweight");
    public static final Chars LAYOUT_JOINTINDEX = Chars.constant("l_jointindex");
    public static final Chars LAYOUT_DEFORMTYPE = Chars.constant("l_deformtype");

    protected static final Chars PROG_OP_VERTEX_MODEL  = Chars.constant("    outData.position_model = posModel;");
    protected static final Chars PROG_OP_VERTEX_WORLD  = Chars.constant("    outData.position_world = M * posModel;");
    protected static final Chars PROG_OP_VERTEX_CAMERA = Chars.constant("    outData.position_camera = MV * posModel;");
    protected static final Chars PROG_OP_VERTEX_PROJ   = Chars.constant("    outData.position_proj = MVP * posModel;");
    protected static final Chars PROG_OP_NORMAL_MODEL  = Chars.constant("    outData.normal_model = norModel.xyz;");
    protected static final Chars PROG_OP_NORMAL_WORLD  = Chars.constant("    outData.normal_world = (M * norModel).xyz;");
    protected static final Chars PROG_OP_NORMAL_CAMERA = Chars.constant("    outData.normal_camera = (MV * norModel).xyz;");
    protected static final Chars PROG_OP_NORMAL_PROJ   = Chars.constant("    outData.normal_proj = (MVP * norModel).xyz;");

    protected static final Chars PROG_OP_BUILDIN_PROJ   = Chars.constant("    gl_Position = MVP * posModel;");

    private static final Chars GROUP_TBO_UPDATE = Chars.constant("tbo_update");
    private static final Chars GROUP_TBO = Chars.constant("tbo");

    private static final ShaderTemplate SHADER_VE;
    static {
        try{
            SHADER_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/shellskin-tbo-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    //shader information, holds weight,index buffer,vertex,normal
    private String base = null;
    private Uniform uniTbo;
    private Uniform uniNbJoint;
    private VertexAttribute attVertice;
    private VertexAttribute attNormal;
    private VertexAttribute attUv;
    private VertexAttribute attTangent;
    private VertexAttribute attJointWeight;
    private VertexAttribute attJointIndex;
    private VertexAttribute attDeformType;
    private int[] reservedTexture;

    public SkinShellActor(Model model) {
        super(model);
    }

    private Mesh getMesh(){
        return (Mesh) model.getShape();
    }

    @Override
    public Chars getReuseUID() {
        Chars uid = super.getReuseUID();
        final int maxWeightPerVertex = model.getSkin().getMaxWeightPerVertex();
        uid = uid.concat(new Chars("_"+maxWeightPerVertex));
        return uid;
    }

    @Override
    public boolean isDirty() {
        final Mesh mesh = getMesh();
        //we don't test the joint vbo because we use a reduced list
        return super.isDirty() || ((VBO) mesh.getAttributes().getValue(Skin.ATT_WEIGHTS_0)).isDirty();
    }

    /**
     * {@inheritDoc }
     *
     * Override direct shell shader.
     *
     * @param ctx
     * @param template
     * @param tess
     * @param geom
     */
    @Override
    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        if (concurrent) return;
        if (initialized) throw new RuntimeException("This actor has been initialized by another program but not disposed before reuse.");
        initialized = true;
        final ShaderTemplate vs = template.getVertexShaderTemplate();
        final ShaderTemplate tc = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate te = template.getTesselationEvalShaderTemplate();
        final ShaderTemplate gs = template.getGeometryShaderTemplate();
        final ShaderTemplate fg = template.getFragmentShaderTemplate();


        vs.append(MeshActor.TEMPLATE_VE);
        if (tess) tc.append(MeshActor.TEMPLATE_TC);
        if (tess) te.append(MeshActor.TEMPLATE_TE);
        if (geom) gs.append(MeshActor.TEMPLATE_GE);
        if (fg!=null){
            fg.addUniform(new Chars("uniform mat4 ").concat(MeshActor.UNIFORM_M).concat(';'));
            fg.addUniform(new Chars("uniform mat4 ").concat(MeshActor.UNIFORM_V).concat(';'));
            fg.addOperation(MeshActor.OP_SET_M);
            fg.addOperation(MeshActor.OP_SET_V);
            fg.append(MeshActor.TEMPLATE_FR);
        }

        //shaders
        final ShaderTemplate cp = new ShaderTemplate(ShaderTemplate.SHADER_VERTEX);
        cp.append(SHADER_VE);

        final Mesh mesh = getMesh();
        final int maxWeightPerVertex = model.getSkin().getMaxWeightPerVertex();
        if (maxWeightPerVertex < 1){
            //should not happen if model is correct but we are tolerant
            cp.replaceTexts(new Chars("$weightType"), new Chars("float[1]"));
            cp.replaceTexts(new Chars("$indextType"), new Chars("int[1]"));
        } else if (maxWeightPerVertex == 1){
            cp.replaceTexts(new Chars("$weightType"), new Chars("float[1]"));
            cp.replaceTexts(new Chars("$indextType"), new Chars("int[1]"));
        } else if (maxWeightPerVertex == 2){
            cp.replaceTexts(new Chars("$weightType"), new Chars("vec2"));
            cp.replaceTexts(new Chars("$indextType"), new Chars("ivec2"));
        } else if (maxWeightPerVertex == 3){
            cp.replaceTexts(new Chars("$weightType"), new Chars("vec3"));
            cp.replaceTexts(new Chars("$indextType"), new Chars("ivec3"));
        } else if (maxWeightPerVertex == 4){
            cp.replaceTexts(new Chars("$weightType"), new Chars("vec4"));
            cp.replaceTexts(new Chars("$indextType"), new Chars("ivec4"));
        } else if (maxWeightPerVertex>4){
            throw new RuntimeException("number of joint per vertex must be between 1 and 4. Was "+maxWeightPerVertex);
        }
        vs.append(cp);

        //load VBOs and build program
        if (mesh.getPositions()!=null){
            //always set the layout, might be used by some other shader actor
            vs.addLayout(PROG_LAYOUT_VERTEX);
            vs.addVariableOut(PROG_OUT_VERTEX_MODEL); vs.addOperation(PROG_OP_VERTEX_MODEL);
            vs.addVariableOut(PROG_OUT_VERTEX_WORLD); vs.addOperation(PROG_OP_VERTEX_WORLD);
            vs.addVariableOut(PROG_OUT_VERTEX_CAMERA); vs.addOperation(PROG_OP_VERTEX_CAMERA);
            vs.addVariableOut(PROG_OUT_VERTEX_PROJ); vs.addOperation(PROG_OP_VERTEX_PROJ);
        }
        if (mesh.getNormals()!=null){
            vs.addLayout(PROG_LAYOUT_NORMAL);
            vs.addVariableOut(PROG_OUT_NORMAL_MODEL); vs.addOperation(PROG_OP_NORMAL_MODEL);
            vs.addVariableOut(PROG_OUT_NORMAL_WORLD); vs.addOperation(PROG_OP_NORMAL_WORLD);
            vs.addVariableOut(PROG_OUT_NORMAL_CAMERA); vs.addOperation(PROG_OP_NORMAL_CAMERA);
            vs.addVariableOut(PROG_OUT_NORMAL_PROJ); vs.addOperation(PROG_OP_NORMAL_PROJ);
        }
        if (mesh.getUVs()!=null){
            vs.addLayout(PROG_LAYOUT_UV);
            vs.addVariableOut(PROG_OUT_UV);
            vs.addOperation(PROG_OP_UV);
        }
        if (mesh.getTangents()!=null){
            vs.addLayout(PROG_LAYOUT_TANGENT);
            vs.addVariableOut(PROG_OUT_TANGENT_MODEL); vs.addOperation(PROG_OP_TANGENT_MODEL);
            vs.addVariableOut(PROG_OUT_TANGENT_WORLD); vs.addOperation(PROG_OP_TANGENT_WORLD);
            vs.addVariableOut(PROG_OUT_TANGENT_CAMERA); vs.addOperation(PROG_OP_TANGENT_CAMERA);
            vs.addVariableOut(PROG_OUT_TANGENT_PROJ); vs.addOperation(PROG_OP_TANGENT_PROJ);
        }

        vs.addOperation(PROG_OP_BUILDIN_PROJ);

    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();
        final Mesh mesh = getMesh();

        //ensure weights and indexes are loaded
        ((VBO) mesh.getAttributes().getValue(Skin.ATT_WEIGHTS_0)).loadOnGpuMemory(gl);
        ((VBO) mesh.getAttributes().getValue(Skin.ATT_JOINTS_0)).loadOnGpuMemory(gl);

        if (base == null){
            //get all uniform and attributes ids
            uniNbJoint      = program.getUniform(new Chars("NBJOINT"));
            uniTbo          = program.getUniform(new Chars("JOINTSAMPLER"));
            attVertice      = program.getVertexAttribute(LAYOUT_VERTEX, gl);
            attNormal       = program.getVertexAttribute(LAYOUT_NORMAL, gl);
            attUv           = program.getVertexAttribute(LAYOUT_UV, gl);
            attTangent      = program.getVertexAttribute(LAYOUT_TANGENT, gl);
            attJointWeight  = program.getVertexAttribute(LAYOUT_JOINTWEIGHT, gl);
            attJointIndex   = program.getVertexAttribute(LAYOUT_JOINTINDEX, gl);
            attDeformType   = program.getVertexAttribute(LAYOUT_DEFORMTYPE, gl);
        }
        final int maxWeightPerVertex = model.getSkin().getMaxWeightPerVertex();
        uniNbJoint.setInt(gl, maxWeightPerVertex);

        //bind TBO
        final MotionModel mpm = (MotionModel) model.getParent();
        final TBO tbo = getOrBuildTBO(mpm);
        tbo.loadOnGpuMemory(gl);

        //update tbo if needed, it is used by several skins at the same time
        //check update time to update it only once by rendering
        if (getTboUpdate(mpm)!=context.getTimeNano()){
            setTboUpdate(mpm, context.getTimeNano());
            updateTBO(mpm);
            tbo.updateData(gl);
        }


        reservedTexture = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTexture[0]);
        tbo.bind(gl);
        GLUtilities.checkGLErrorsFail(gl);
        uniTbo.setInt(gl, reservedTexture[1]);
        GLUtilities.checkGLErrorsFail(gl);

        //bind buffers
        attVertice.enable(gl, (VBO) mesh.getPositions());
        attNormal.enable(gl, (VBO) mesh.getNormals());
        attUv.enable(gl, (VBO) mesh.getUVs());
        attTangent.enable(gl, (VBO) mesh.getTangents());
        attJointWeight.enable(gl, (VBO) mesh.getAttributes().getValue(Skin.ATT_WEIGHTS_0));
        attJointIndex.enable(gl, (VBO) mesh.getAttributes().getValue(Skin.ATT_JOINTS_0));
        attDeformType.enable(gl, (VBO) mesh.getAttributes().getValue(Skin.ATT_JPARAMS_0));

        //indices
        ((IBO) mesh.getIndex()).loadOnGpuMemory(gl);
        ((IBO) mesh.getIndex()).bind(gl);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void postDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        attVertice.disable(gl);
        attNormal.disable(gl);
        attUv.disable(gl);
        attTangent.disable(gl);
        attJointWeight.disable(gl);
        attJointIndex.disable(gl);
        attDeformType.disable(gl);

        // unbind TBO
        final MotionModel mpm = (MotionModel) model.getParent();
        final TBO tbo = getOrBuildTBO(mpm);
        gl.glActiveTexture(reservedTexture[0]);
        tbo.unbind(gl);
        context.getResourceManager().releaseTextureId(reservedTexture[0]);
    }

    private static long getTboUpdate(MotionModel group) {
        Long upd = (Long) group.getUserProperties().getValue(GROUP_TBO_UPDATE);
        return upd == null ? -1 : upd;
    }

    private static void setTboUpdate(MotionModel group, long tboUpdate) {
        group.getUserProperties().add(GROUP_TBO_UPDATE, tboUpdate);
    }

    private static TBO getOrBuildTBO(MotionModel group) {
        TBO jointMatrixTbo = (TBO) group.getUserProperties().getValue(GROUP_TBO);
        if (jointMatrixTbo == null) {
            final int nb = group.getSkeleton().getAllJoints().getSize();
            //4*4 matrix for each joint
            jointMatrixTbo = new TBO(new float[nb*16], nb);
            jointMatrixTbo.setForgetOnLoad(false);
            group.getUserProperties().add(GROUP_TBO, jointMatrixTbo);
        }
        return jointMatrixTbo;
    }

    private static void updateTBO(MotionModel group) {
        TBO jointMatrixTbo = getOrBuildTBO(group);
        final Matrix4x4 matrix = new Matrix4x4();
        final float[] array = new float[16];

        final Sequence joints = group.getSkeleton().getAllJoints();
        final Float32Cursor fb = jointMatrixTbo.getBuffer().asFloat32().cursor();
        for (int i=0,n=joints.getSize();i<n;i++) {
            final Joint jt = (Joint) group.getSkeleton().getJointById(i);
            jt.getBindPose().multiply(jt.getInvertBindPose()).toMatrix(matrix);
            fb.write(matrix.toArrayFloatColOrder(array));
        }
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniTbo = null;
        uniNbJoint = null;
        attVertice = null;
        attNormal = null;
        attUv = null;
        attTangent = null;
        attJointWeight = null;
        attJointIndex = null;
        attDeformType = null;
    }

}
