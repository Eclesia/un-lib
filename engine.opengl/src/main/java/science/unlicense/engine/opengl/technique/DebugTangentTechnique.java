

package science.unlicense.engine.opengl.technique;

import science.unlicense.code.glsl.io.ShaderTemplate;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorExecutor;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.Actors;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.engine.opengl.technique.actor.MaterialActor;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Display the tangents stored in the mesh shell.
 *
 * @author Johann Sorel
 */
public class DebugTangentTechnique extends AbstractGLTechnique {

    private static final ShaderTemplate TANGENT_GE;
    static {
        try{
            TANGENT_GE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugtangent-3-ge.glsl"), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private float tangentLength = 1.0f;
    private ActorProgram program;
    private Uniform uniLength;

    public DebugTangentTechnique() {
    }

    public DebugTangentTechnique(float tangentLength) {
        this.tangentLength = tangentLength;
    }

    public float getTangentLength() {
        return tangentLength;
    }

    public void setTangentLength(float tangentLength) {
        this.tangentLength = tangentLength;
    }

    @Override
    public void renderInternal(RenderContext context, MonoCamera camera, SceneNode node) {

        final Model model = (Model) node;

        if (program==null){
            program = new ActorProgram();

            //build material actor
            final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
            material.setDiffuse(Color.RED);
            final MaterialActor materialActor = new MaterialActor(material,false);

            //build shell actor
            final Geometry shape = model.getShape();
            final ActorExecutor shellActor = Actors.buildExecutor(model, shape);
            program.setExecutor(shellActor);

            final DefaultActor defaultActor = new DefaultActor(new Chars("DebugTangent"),false,
                    null, null, null, TANGENT_GE, null, true, false){
                    @Override
                    public void preDrawGL(RenderContext context, ActorProgram program) {
                        super.preDrawGL(context, program);
                        uniLength.setFloat(context.getGL().asGL2ES2(), tangentLength);
                    }
                };

            program.getActors().add(shellActor);
            program.getActors().add(defaultActor);
            program.getActors().add(materialActor);
            program.compile(context);
            uniLength = program.getUniform(new Chars("tangentLength"));
        }

        program.render(context, camera, model);
    }

    @Override
    public void dispose(GLProcessContext context) {
        if (program!=null){
            program.releaseProgram(context);
        }
        uniLength = null;
    }
}
