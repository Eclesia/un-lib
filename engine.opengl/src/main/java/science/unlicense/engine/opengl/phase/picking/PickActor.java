

package science.unlicense.engine.opengl.phase.picking;

import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.technique.actor.ActorProgram;
import science.unlicense.engine.opengl.technique.actor.DefaultActor;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.shader.Uniform;
import science.unlicense.model3d.impl.scene.Model;

/**
 * Picking actor which can be added on any mesh as additional actor.
 * Don't forget to also add the PickResetPhase in the phases list.
 *
 * @author Johann Sorel
 */
public class PickActor extends DefaultActor{

    private static final Chars UNIFORM_PICKCOLOR = Chars.constant("PICK_COLOR");
    private final PickResetPhase phase;
    private final Model model;

    //GL loaded informations
    private Uniform uniColor;

    public PickActor(PickResetPhase phase, Model model) throws IOException {
        super(new Chars("Picking"),false,null,null,null,null,
                Paths.resolve(new Chars("mod:/un/engine/opengl/shader/picking/picking-4-fr.glsl")),
                true,true);
        this.phase = phase;
        this.model = model;
    }

    @Override
    public int getMinGLSLVersion() {
        return GLC.GLSL.V110_GL20;
    }

    @Override
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);

        if (uniColor==null){
            //the uniforma is declared in the collector shader
            //if it it set on the render phase then the uniform is missing
            //so it won't have any effect
            uniColor = program.getUniform(UNIFORM_PICKCOLOR);
        }

        if (uniColor!=null){
            //calculate this mesh color and store it in the index
            final int color = phase.next(model);
            uniColor.setInt(context.getGL().asGL2ES2(), color);
        }
    }

    @Override
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniColor = null;
    }

}
