

package science.unlicense.engine.opengl.phase.reflection;

import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.gpu.api.GLException;

/**
 * Reflection phase
 *
 * @author Johann Sorel
 */
public class ReflectionPhase extends AbstractFboPhase{

    private final ReflectionVisitor visitor;
    private final RenderPhase baseRenderPhase;

    public ReflectionPhase(RenderPhase baseRenderPhase) {
        this.baseRenderPhase = baseRenderPhase;
        visitor = new ReflectionVisitor(baseRenderPhase);
    }

    @Override
    protected void processInternal(GLProcessContext ctx) throws GLException {
        final SceneNode root = baseRenderPhase.getRoot();
        visitor.visit(root, ctx);
    }

}
