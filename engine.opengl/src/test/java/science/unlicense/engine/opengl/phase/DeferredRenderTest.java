
package science.unlicense.engine.opengl.phase;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.mesh.GLMesh;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.util.GLTest;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.model3d.impl.technique.RenderState;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.Technique;

/**
 *
 * @author Johann Sorel
 */
public class DeferredRenderTest extends GLTest {

    private static final float DELTA = 0.01f;

    @Test
    public void testDiffuseNormalPosition() throws IOException{

        final GLModel plan = createQuadMesh();

        final Image[] images = render(plan,100,100);
        final Image imgColor = images[0];
        final Image imgNormal = images[1];
        final Image imgPosition = images[2];

        final ImageModel cm = imgColor.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();

        final Vector2i32 coord = new Vector2i32();
        final TupleGridCursor cursorColor = imgColor.getRawModel().asTupleBuffer(imgColor).cursor();
        final TupleGridCursor cursorNormal = imgNormal.getRawModel().asTupleBuffer(imgNormal).cursor();
        final TupleGridCursor cursorPos = imgPosition.getRawModel().asTupleBuffer(imgPosition).cursor();
        cursorColor.moveTo(coord);
        cursorNormal.moveTo(coord);
        cursorPos.moveTo(coord);
        Assert.assertTrue(Color.BLUE.equals(new ColorRGB(cursorColor.samples(),cs),null,0));
        Assert.assertArrayEquals(new float[]{0,0,1}, cursorNormal.samples().toFloat(),DELTA);
        Assert.assertArrayEquals(new float[]{-0.5f,-0.5f,-1.0f}, cursorPos.samples().toFloat(),DELTA);
        coord.x = 99; coord.y = 99;
        cursorColor.moveTo(coord);
        cursorNormal.moveTo(coord);
        cursorPos.moveTo(coord);
        Assert.assertTrue(Color.BLUE.equals(new ColorRGB(cursorColor.samples(),cs),null,0));
        Assert.assertArrayEquals(new float[]{0,0,1}, cursorNormal.samples().toFloat(),DELTA);
        Assert.assertArrayEquals(new float[]{0.5f,0.5f,-1.0f}, cursorPos.samples().toFloat(),DELTA);

    }

    public static GLModel createQuadMesh(){
        final GLModel plan = new GLModel();
        final GLMesh mesh = new GLMesh();
        mesh.setPositions(new VBO(new float[]{
            -0.5f,-0.5f,-1.0f,
            +0.5f,-0.5f,-1.0f,
            +0.5f,+0.5f,-1.0f,
            -0.5f,+0.5f,-1.0f
            },3));
        mesh.setNormals(new VBO(new float[]{
            0,0,1,
            0,0,1,
            0,0,1,
            0,0,1
            },3));
        mesh.setUVs(new VBO(new float[]{
            0,0,
            1,0,
            1,1,
            0,1
            },2));
        mesh.setIndex(new IBO(new int[]{0,1,2,2,3,0}));
        mesh.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 6)});
        plan.setShape(mesh);

        final SimpleBlinnPhong.Material mat = SimpleBlinnPhong.newMaterial();
        mat.setDiffuse(Color.BLUE);
        plan.getMaterials().add(mat);

        ((Technique) plan.getTechniques().get(0)).getState().setCulling(RenderState.CULLING_NONE);
        Mesh.calculateTangents(mesh);
        GLEngineUtils.makeCompatible(plan);

        return plan;
    }

    public static Image[] render(GLModel mesh, int width, int height) {

        final GLSource source = GLUtilities.createOffscreenSource(width,height);
        final GBO gbo = new GBO(width, height);
        final Texture2D texColor = (Texture2D) gbo.getDiffuseTexture();
        final Texture2D texNormal = (Texture2D) gbo.getNormalTexture();
        final Texture2D texPosition = (Texture2D) gbo.getPositionTexture();

        //build the scene
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setCameraType(MonoCamera.TYPE_ORTHO);
        scene.getChildren().add(camera);

        scene.getChildren().add(mesh);

        //render it
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(gbo,Color.TRS_BLACK.toRGBAPreMul()));
        context.getPhases().add(new DeferredRenderPhase(scene,camera,gbo));

        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);
                //get the result image
                gbo.loadOnSystemMemory(source.getGL());
            }

            @Override
            public void dispose(GLSource source) {
                context.dispose(source);
                GLEngineUtils.dispose(scene, context);
            }
        });
        source.render();
        source.dispose();

        final Image imgColor = texColor.getImage();
        final Image imgNormal = texNormal.getImage();
        final Image imgPosition = texPosition.getImage();

        return new Image[]{imgColor,imgNormal,imgPosition};
    }

}
