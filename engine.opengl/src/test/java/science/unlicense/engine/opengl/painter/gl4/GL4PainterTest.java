
package science.unlicense.engine.opengl.painter.gl4;

import org.junit.BeforeClass;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.ImagePainter2D;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.opengl.painter.gl3.GL3ImagePainter2D;
import static science.unlicense.engine.opengl.util.GLTest.isGLAvailable;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class GL4PainterTest extends science.unlicense.display.api.painter2d.ImagePainter2DTest{

    private static final double DELTA = 0.00000001;
    private static final Color NONE = new ColorRGB(0, 0, 0, 0);

    @BeforeClass
    public static void beforeClass() {
        org.junit.Assume.assumeTrue(isGLAvailable());
    }

    protected ImagePainter2D createPainter(int width, int height) {
        return new GL3ImagePainter2D(width, height, null);
    }

    @Test
    public void testClip(){
        GL3ImagePainter2D painter;
        Image image;

        //sanity test
        painter = new GL3ImagePainter2D(100, 100, null);
        try{
            testImage(painter.getDepthStencilImage(), 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
            painter.setPaint(new ColorPaint(Color.BLUE));
            painter.fill(new Rectangle(0, 0, 100, 100));
            painter.flush();
            image = painter.getImage();
            testImage(image, 0, 0, 100, 100, NONE, Color.BLUE);
            testImage(painter.getDepthStencilImage(), 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
        }finally{
            painter.dispose();
        }


        painter = new GL3ImagePainter2D(100, 100, null);
        try{
            painter.setPaint(new ColorPaint(Color.BLUE));
            painter.fill(new Rectangle(10, 10, 80, 80));
            painter.flush();
            image = painter.getImage();
            testImage(image, 10, 10, 80, 80, NONE, Color.BLUE);
            testImage(painter.getDepthStencilImage(), 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
        }finally{
            painter.dispose();
        }

        //clip test
        painter = new GL3ImagePainter2D(100, 100, null);
        try{
            painter.setClip(new Rectangle(10, 10, 80, 80));
            painter.flush();
            testImage(painter.getDepthStencilImage(), 10, 10, 80, 80, new double[]{0,0,0,0}, new double[]{1,0,0,0});
            //ensure nothing has been painted
            image = painter.getImage();
            testImage(image, 0, 0, 100, 100, NONE, NONE);

            //paint a geometry, should be clipped
            painter.setPaint(new ColorPaint(Color.BLUE));
            painter.fill(new Rectangle(0, 0, 100, 100));
            painter.flush();
            image = painter.getImage();
            testImage(image, 10, 10, 80, 80, NONE, Color.BLUE);
            testImage(painter.getDepthStencilImage(), 10, 10, 80, 80, new double[]{0,0,0,0}, new double[]{1,0,0,0});

            //remove the clip
            painter.setClip(null);
            painter.flush();
            testImage(painter.getDepthStencilImage(), 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
            //ensure notice has been painted
            image = painter.getImage();
            testImage(image, 10, 10, 80, 80, NONE, Color.BLUE);
        }finally{
            painter.dispose();
        }

        //paint outside of the clip, should be ok since we removed the clip
        painter = new GL3ImagePainter2D(100, 100, null);
        try{
            painter.setPaint(new ColorPaint(Color.RED));
            painter.fill(new Rectangle(0, 0, 100, 100));
            painter.flush();
            image = painter.getImage();
            testImage(image, 0, 0, 100, 100, NONE, Color.RED);
            testImage(painter.getDepthStencilImage(), 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
        }finally{
            painter.dispose();
        }

    }

    @Test
    public void testClipStencilBuffer(){
        Painter2D painter;
        Image depthStencil;

        //sanity test
        final GL3ImagePainter2D glPainter = new GL3ImagePainter2D(100, 100, null);
        painter = glPainter;
        try{
            painter.setClip(new Rectangle(10, 20, 5, 30));
            painter.flush();
            depthStencil = glPainter.getDepthStencilImage();
            Assert.assertNotNull(depthStencil);
            testImage(depthStencil, 10, 20, 5, 30, new double[]{0,0,0,0}, new double[]{1,0,0,0});

            // derivate painter
            final Affine2 m = new Affine2(
                    1, 0, 10,
                    0, 1, 20);
            painter = painter.derivate(true);
            painter.setTransform(m);
            //stencil buffer should still be the same
            painter.flush();
            depthStencil = glPainter.getDepthStencilImage();
            Assert.assertNotNull(depthStencil);
            testImage(depthStencil, 10, 20, 5, 30, new double[]{0,0,0,0}, new double[]{1,0,0,0});

            //change the stencil buffer using derivate painter
            painter.setClip(new Rectangle(10, 20, 5, 30));
            painter.flush();
            depthStencil = glPainter.getDepthStencilImage();
            Assert.assertNotNull(depthStencil);
            testImage(depthStencil, 10, 20, 5, 30, new double[]{0,0,0,0}, new double[]{1,0,0,0});

            //flush the clip
            //this is a derivate painter, the clip would be restored by the main painter
//            painter.setClip(null);
//            painter.flush();
//            depthStencil = glPainter.getDepthStencilImage();
//            Assert.assertNotNull(depthStencil);
//            testImage(depthStencil, 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
        }finally{
            painter.dispose();
        }

    }

    @Test
    public void testClipStencilBuffer2(){
        GL3ImagePainter2D painter;

        //fill the stencil buffer
        painter = new GL3ImagePainter2D(100, 100, null);
        try{
            painter.setClip(new Rectangle(0, 0, 100, 100));
            painter.flush();
            testImage(painter.getDepthStencilImage(), 0, 0, 100, 100, new double[]{0,0,0,0}, new double[]{1,0,0,0});

            //know set a smaller stencil buffer
            painter.setClip(new Rectangle(50, 50, 10, 10));
            painter.flush();
            testImage(painter.getDepthStencilImage(), 50, 50, 10, 10, new double[]{0,0,0,0}, new double[]{1,0,0,0});
            painter.dispose();

            //do both in one call
            painter = new GL3ImagePainter2D(100, 100, null);
            painter.setClip(new Rectangle(0, 0, 100, 100));
            painter.setClip(new Rectangle(50, 50, 10, 10));
            painter.flush();
            testImage(painter.getDepthStencilImage(), 50, 50, 10, 10, new double[]{0,0,0,0}, new double[]{1,0,0,0});
        }finally{
            painter.dispose();
        }

    }


    public static void testImage(Image image, int x, int y, int width, int height, Color outside, Color inside){
        final ImageModel sm = image.getRawModel();
        final TupleGrid cm = image.getColorModel().asTupleBuffer(image);
        final int imgWidth = (int) image.getExtent().getL(0);
        final int imgHeight = (int) image.getExtent().getL(1);

        final Vector2i32 coord = new Vector2i32();
        for (coord.y=0;coord.y<imgHeight;coord.y++){
            for (coord.x=0;coord.x<imgWidth;coord.x++){
                final Color candidate = image.getColor(coord);
                if (coord.x>=x && coord.x<(x+width) &&
                   coord.y>=y && coord.y<(y+height)){
                    Assert.assertTrue(inside.equals(candidate, null, 0), "INSIDE "+coord);
                } else {
                    Assert.assertTrue(outside.equals(candidate, null, 0), "OUTSIDE "+coord);
                }
            }
        }
    }

    public static void testImage(Image image, int x, int y, int width, int height, double[] outside, double[] inside){
        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final TupleRW storage = sm.createTuple();
        final int imgWidth = (int) image.getExtent().getL(0);
        final int imgHeight = (int) image.getExtent().getL(1);

        final Vector2i32 coord = new Vector2i32();
        for (coord.y=0;coord.y<imgHeight;coord.y++){
            for (coord.x=0;coord.x<imgWidth;coord.x++){
                sm.getTuple(coord, storage);
                if (coord.x>=x && coord.x<(x+width) &&
                   coord.y>=y && coord.y<(y+height)){
                    Assert.assertArrayEquals(inside, storage.toDouble(), DELTA, "INSIDE "+coord);
                } else {
                    Assert.assertArrayEquals(outside, storage.toDouble(), DELTA, "OUTSIDE "+coord);
                }
            }
        }
    }

}
