
package science.unlicense.engine.opengl;

import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.GBO;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.util.GLTest;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;

/**
 *
 * @author Johann Sorel
 */
public class RenderingTest extends GLTest {


    /**
     * Make a simple rendering.
     * light active in the rendering pass.
     *
     * @param scene
     * @param width
     * @param height
     * @return
     */
    public static Image renderBasic(final SceneNode scene, MonoCamera camera, int width, int height) {

        final GLSource source = GLUtilities.createOffscreenSource(width,height);
        final GBO gbo = new GBO(width, height);
        final Texture2D texColor = (Texture2D) gbo.getDiffuseTexture();


        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(gbo,Color.TRS_BLACK.toRGBAPreMul()));
        context.getPhases().add(new DeferredRenderPhase(scene,camera,gbo));
        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);
                //get the result image
                gbo.loadOnSystemMemory(source.getGL());
                //release resources
                GLEngineUtils.dispose(scene, context);
                context.dispose(source);
            }

            @Override
            public void dispose(GLSource source) {
                context.dispose(source);
            }

        });
        source.render();

        return texColor.getImage();
    }

}
