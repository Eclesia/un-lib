

package science.unlicense.engine.opengl.material;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.phase.DeferredRenderTest;
import static science.unlicense.engine.opengl.phase.DeferredRenderTest.createQuadMesh;
import science.unlicense.engine.opengl.util.GLTest;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Test materials.
 *
 * @author Johann Sorel
 */
public class MaterialTest extends GLTest {

    @Test
    public void testDiffuseColor(){

        final GLModel plan = createQuadMesh();
        SimpleBlinnPhong.view((Material) plan.getMaterials().get(0)).setDiffuse(Color.BLUE);

        final Image[] images = DeferredRenderTest.render(plan,100,100);
        final Image imgColor = images[0];
        final Image imgNormal = images[1];
        final Image imgPosition = images[2];


        final TupleGrid cm = imgColor.getColorModel().asTupleBuffer(imgColor);

        final Vector2i32 coord = new Vector2i32();
        for (coord.y=0;coord.y<100;coord.y++){
            for (coord.x=0;coord.x<100;coord.x++){
                final Color rgba = imgColor.getColor(coord);
                Assert.assertTrue(Color.BLUE.equals(rgba, null, 0));
            }
        }
    }

    @Test
    public void testDiffuseTexture(){

        //create an image we will use as a texture
        final Image texture = Images.create(new Extent.Long(100, 100),Images.IMAGE_TYPE_RGB);
        final ImageModel textsm = texture.getRawModel();
        final ImageModel cm = texture.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGrid textcm = cm.asTupleBuffer(texture);
        textcm.setTuple(new BBox(new double[]{0,0}, new double[]{100,100}), Color.GREEN.toColorSystem(cs));


        final GLModel plan = createQuadMesh();

        final SimpleBlinnPhong.Material mat = SimpleBlinnPhong.newMaterial();
        final UVMapping mapping = new UVMapping(new Texture2D(texture));
        final Layer layer = new Layer(mapping);
        mat.setDiffuse(Color.WHITE);
        mat.setDiffuseTexture(layer);
        plan.getMaterials().removeAll();
        plan.getMaterials().add(mat);

        final Image[] images = DeferredRenderTest.render(plan,100,100);
        final Image imgColor = images[0];
        final Image imgNormal = images[1];
        final Image imgPosition = images[2];


        final ImageModel cmi = imgColor.getColorModel();
        final TupleGrid tb = cmi.asTupleBuffer(imgColor);

        final Vector2i32 coord = new Vector2i32();
        for (coord.y=0;coord.y<100;coord.y++){
            for (coord.x=0;coord.x<100;coord.x++){
                final Color rgba = imgColor.getColor(coord);
                Assert.assertTrue(Color.GREEN.equals(rgba, null, 0));
            }
        }
    }

    @Ignore
    @Test
    public void testNormalTexture(){

        final float[] normal = new Vector3f64(0.3,0.1,0.6).localNormalize().toFloat();
        //convert from -1:+1 to 0:+1 for image
        final float[] normalAsColor = new float[]{(normal[0]+1)/2,(normal[1]+1)/2,(normal[2]+1)/2};

        //create an image we will use as a texture
        final Image texture = Images.create(new Extent.Long(100, 100),Images.IMAGE_TYPE_RGB);
        final ImageModel cm = texture.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGrid textcm = cm.asTupleBuffer(texture);
        textcm.setTuple(new BBox(new double[]{0,0}, new double[]{100,100}), new ColorRGB(normalAsColor).toColorSystem(cs));


        final GLModel plan = createQuadMesh();

        final SimpleBlinnPhong.Material mat = SimpleBlinnPhong.newMaterial();
        plan.getMaterials().add(mat);

        final UVMapping mapping = new UVMapping(new Texture2D(texture));
        final Layer layer = new Layer(mapping, Layer.TYPE_NORMAL);
        mat.setDiffuseTexture(layer);

        final Image[] images = DeferredRenderTest.render(plan,100,100);
        final Image imgNormal = images[1];


        final TupleGrid tb = imgNormal.getRawModel().asTupleBuffer(imgNormal);
        final TupleRW pixel = tb.createTuple();

        for (int y=0;y<100;y++){
            for (int x=0;x<100;x++){
                tb.getTuple(new Vector2i32(x, y), pixel);
                Assert.assertArrayEquals(normal, pixel.toFloat(),0.1f);
            }
        }
    }

}
