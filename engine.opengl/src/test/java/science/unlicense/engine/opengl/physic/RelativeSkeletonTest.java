

package science.unlicense.engine.opengl.physic;

import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.model3d.impl.physic.RelativeSkeletonPose;
import science.unlicense.model3d.impl.physic.Skeletons;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public class RelativeSkeletonTest {

    /**
     * Convert a skeleton bind pose in a relative pose.
     */
    @Test
    public void testToRelative(){

        //build the structure
        final Skeleton skeleton = new Skeleton();
        final Joint joint0 = new Joint(3);
        final Joint joint1 = new Joint(3);
        final Joint joint2 = new Joint(3);
        joint0.getChildren().add(joint1);
        joint1.getChildren().add(joint2);
        skeleton.getChildren().add(joint0);

        //place all joints
        joint1.getNodeTransform().getTranslation().setXYZ(5, 0, 0);
        joint1.getNodeTransform().notifyChanged();
        joint2.getNodeTransform().getTranslation().setXYZ(5, 2, 0);
        joint2.getNodeTransform().notifyChanged();
        skeleton.reverseWorldPose();
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();
        //sanity check, more exact tests are made in the physics module
        Assert.assertEquals(new Vector3f64(0, 0, 0), joint0.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector3f64(5, 0, 0), joint1.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector3f64(0, 2, 0), joint2.getNodeTransform().getTranslation());

        //move the joints and update bind pose
        joint0.getNodeTransform().getTranslation().localAdd(0, -3, 0);
        joint0.getNodeTransform().notifyChanged();
        final Matrix3x3 rotation = Matrix3x3.createRotation3(Math.toRadians(25), new Vector3f64(0, 0, 1));
        joint1.getNodeTransform().getRotation().set(rotation);
        joint1.getNodeTransform().notifyChanged();
        joint2.getNodeTransform().getTranslation().localAdd(2, 0, 0);
        joint2.getNodeTransform().notifyChanged();
        skeleton.updateBindPose();
        //sanity check, more exact tests are made in the physics module
        Assert.assertEquals(new Vector3f64(0,-3, 0), joint0.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector3f64(5, 0, 0), joint1.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector3f64(2, 2, 0), joint2.getNodeTransform().getTranslation());

        //extract the relative bind pose
        final RelativeSkeletonPose relative = Skeletons.toRelative(skeleton, JointKeyFrame.FROM_BASE);
        final Sequence rjoints = relative.getJointPoses();
        Assert.assertEquals(3, rjoints.getSize());
        final JointKeyFrame rjp0 = (JointKeyFrame) rjoints.get(0);
        final JointKeyFrame rjp1 = (JointKeyFrame) rjoints.get(1);
        final JointKeyFrame rjp2 = (JointKeyFrame) rjoints.get(2);

        Assert.assertEquals(JointKeyFrame.FROM_BASE, rjp0.getFrom());
        Assert.assertEquals(JointKeyFrame.FROM_BASE, rjp1.getFrom());
        Assert.assertEquals(JointKeyFrame.FROM_BASE, rjp2.getFrom());
        Assert.assertEquals(new Vector3f64(0,-3, 0), rjp0.getValue().getTranslation());
        Assert.assertEquals(new Vector3f64(0, 0, 0), rjp1.getValue().getTranslation());
        Assert.assertEquals(new Vector3f64(2, 0, 0), rjp2.getValue().getTranslation());
        Assert.assertEquals(new Matrix3x3().setToIdentity(), rjp0.getValue().getRotation());
        Assert.assertEquals(rotation,    rjp1.getValue().getRotation());
        Assert.assertEquals(new Matrix3x3().setToIdentity(), rjp2.getValue().getRotation());


    }

}
