
package science.unlicense.engine.opengl.material;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.util.GLTest;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Various tests related to alpha blending.
 *
 * @author Johann Sorel
 */
public class AlphaTest extends GLTest {

    @Test
    public void testColor(){

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();

        final Color halfRed = new ColorRGB(1f, 0f, 0f, 0.5f, false);
        material.setDiffuse(halfRed);

        final Image image = render(material,Color.BLACK);
        Assert.assertNotNull(image);
        final TupleGrid cm = image.getColorModel().asTupleBuffer(image);

        //dest = black, src = red . this is the result of a normal SRC_OVER
        final Color resultColor = new ColorRGB(0.5f, 0f, 0f, 1f, false);
        for (int y=0;y<1;y++){
            for (int x=0;x<1;x++){
                final Color rgba = image.getColor(new Vector2i32(x, y));
                //test integer values to avoid rounding issues
                Assert.assertEquals(resultColor.toARGB(), rgba.toARGB());
            }
        }
    }

    //TODO there is a rounding issue here. find where
    @Ignore
    @Test
    public void testTexture(){

        final Color halfRed = new ColorRGB(1f, 0f, 0f, 0.5f, false);

        //create an image we will use as a texture
        final Image texture = Images.create(new Extent.Long(1, 1),Images.IMAGE_TYPE_RGBA);
        final ImageModel cm = texture.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGrid textcm = cm.asTupleBuffer(texture);
        textcm.setTuple(new BBox(new double[]{0,0}, new double[]{100,100}), halfRed.toColorSystem(cs));

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();

        final UVMapping mapping = new UVMapping(new Texture2D(texture));
        final Layer layer = new Layer(mapping, Layer.TYPE_DIFFUSE);
        material.setDiffuseTexture(layer);;

        final Image image = render(material,Color.BLACK);
        Assert.assertNotNull(image);

        //dest = black, src = red . this is the result of a normal SRC_OVER
        final Color resultColor = new ColorRGB(0.5f, 0f, 0f, 1f, false);
        for (int y=0;y<1;y++){
            for (int x=0;x<1;x++){
                final Color rgba = image.getColor(new Vector2i32(x, y));
                //test integer values to avoid rounding issues
                Assert.assertEquals(resultColor.toARGB(), rgba.toARGB());
            }
        }
    }



    private static Image render(Material material, Color background){

        final GLSource source = GLUtilities.createOffscreenSource(1,1);
        final FBO fbo = new FBO(1, 1);
        fbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(1, 1,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo.addAttachment(null, GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(1, 1,Texture2D.DEPTH24_STENCIL8()));

        //build the scene
        final GraphicNode scene = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setCameraType(camera.TYPE_FLAT);
        scene.getChildren().add(camera);

        final GLModel plan = new GLModel();
        plan.setShape(DefaultMesh.createPlan(new Vector3f64(-1, -1, 0),
                new Vector3f64(+1, -1, 0),
                new Vector3f64(+1, +1, 0),
                new Vector3f64(-1, +1, 0)));
        GLEngineUtils.makeCompatible(plan);
        plan.getMaterials().add(material);
        ((DefaultMesh) plan.getShape()).setUVs(new VBO(new float[]{0,0, 1,0, 1,1, 0,1},2));
        scene.getChildren().add(plan);

        //render it
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(fbo,background.toRGBAPreMul()));
        final RenderPhase renderPhase = new RenderPhase(scene,camera,fbo);
        renderPhase.setUseBlending(true);
        context.getPhases().add(renderPhase);

        final Image[] buffer = new Image[1];
        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);

                //get the result image
                fbo.getColorTexture().loadOnSystemMemory(source.getGL());
                final Image image = fbo.getColorTexture().getImage();
                buffer[0] = image;

                //release resources
                GLEngineUtils.dispose(plan, context);
                context.dispose(source);
            }

            @Override
            public void dispose(GLSource source) {
                context.dispose(source);
            }
        });
        source.render();

        return buffer[0];
    }

}
