
package science.unlicense.engine.opengl.util;

import org.junit.BeforeClass;

/**
 * Skip test if GL4 is not available.
 *
 * @author Johann Sorel
 */
public class GLTest {

    @BeforeClass
    public static void beforeClass() {
        org.junit.Assume.assumeTrue(isGLAvailable());
    }

    public static boolean isGLAvailable() {
        return true;
        //TODO
//        boolean glAvailable = false;
//        try {
//            final GLProfile profil = GLProfile.get(GLProfile.GL4);
//
//            final GLCapabilities glCapabilities = new GLCapabilities(profil);
//            final GLDrawableFactory factory = GLDrawableFactory.getFactory(profil);
//            final GLOffscreenAutoDrawable e = factory.createOffscreenAutoDrawable(null,
//                glCapabilities, null, 4, 4);
//            e.destroy();
//            glAvailable = true;
//
//        } catch (Throwable ex){}
//        return glAvailable;
    }

}
