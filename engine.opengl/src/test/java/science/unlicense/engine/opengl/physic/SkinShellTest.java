
package science.unlicense.engine.opengl.physic;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.mesh.GLMesh;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.util.GLTest;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Affine3;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * Test the rendering is changed when the bones are moved.
 *
 * @author Johann Sorel
 */
public class SkinShellTest extends GLTest {

    /**
     * TODO
     * Make a real joint test
     * @throws IOException
     */
    @Ignore
    @Test
    public void testSkin() throws IOException{

        final GLModel mesh = new GLModel();
        SimpleBlinnPhong.view((Material) mesh.getMaterials().get(0)).setDiffuse(Color.RED);

        final Skeleton skeleton = new Skeleton();
        final Joint joint = new Joint(3);
        joint.setRootToNodeSpace(new Affine3(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0));
        skeleton.getChildren().add(joint);
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        final GLMesh shell = new GLMesh();
        final Skin skin = new Skin();
        final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(16).cursor();
        vertices.write(new float[]{-1,-1,0});
        vertices.write(new float[]{ 0,-1,0});
        vertices.write(new float[]{ 0, 0,0});
        vertices.write(new float[]{-1, 0,0});

        final Float32Cursor normals = DefaultBufferFactory.INSTANCE.createFloat32(16).cursor();
        normals.write(new float[]{0,0,1});
        normals.write(new float[]{0,0,1});
        normals.write(new float[]{0,0,1});
        normals.write(new float[]{0,0,1});

        final Buffer indices = DefaultBufferFactory.wrap(new int[]{
            0,1,2,
            2,3,0
        });

        final Buffer jointIndices = DefaultBufferFactory.wrap(new int[]{0,0,0,0});

        final Buffer weights = DefaultBufferFactory.wrap(new float[]{1,1,1,1});

        shell.setPositions(new VBO(vertices.getBuffer(),3));
        shell.setNormals(new VBO(normals.getBuffer(),3));
        shell.setIndex(new IBO(indices));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 6)});
        shell.getAttributes().add(Skin.ATT_JOINTS_0, new VBO(jointIndices,1));
        shell.getAttributes().add(Skin.ATT_WEIGHTS_0, new VBO(weights,1));
        skin.setMaxWeightPerVertex(1);
        skin.setSkeleton(skeleton);
//        shell.updateReducedJointList();
        mesh.setShape(shell);
        mesh.setSkin(skin);

        joint.setRootToNodeSpace(new Affine3(
                1, 0, 0, 1,
                0, 1, 0, 1,
                0, 0, 1, 0));
        skeleton.getChildren().add(joint);
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        Image image = render(mesh, Color.TRS_WHITE);
        //TODO

    }

    private static Image render(GLModel mesh, Color background){

        final GLSource source = GLUtilities.createOffscreenSource(2, 2);
        final FBO fbo = new FBO(2, 2);
        fbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(2, 2,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo.addAttachment(null, GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(2, 2,Texture2D.DEPTH24_STENCIL8()));

        //build the scene
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setCameraType(camera.TYPE_FLAT);
        scene.getChildren().add(camera);

        scene.getChildren().add(mesh);

        //render it
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(background.toRGBAPreMul()));
        context.getPhases().add(new RenderPhase(scene,camera,fbo));

        final Image[] buffer = new Image[1];

        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);

                //get the result image
                fbo.getColorTexture().loadOnSystemMemory(source.getGL());
                final Image image = fbo.getColorTexture().getImage();
                buffer[0] = image;
            }

            @Override
            public void dispose(GLSource source) {
                context.dispose(source);
            }
        });
        source.render();

        return buffer[0];
    }

}
