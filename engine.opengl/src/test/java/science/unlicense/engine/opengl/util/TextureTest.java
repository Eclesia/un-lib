
package science.unlicense.engine.opengl.util;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.number.Int8;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.geometry.api.Extent;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GLBindings;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.TextureUtils;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class TextureTest extends GLTest {

    @Test
    public void testLoadUnloadTexture2D_1B_UBYTE(){

        final byte[] data = new byte[]{47,31,12};
        final Image image = Images.createCustomBand(new Extent.Long(3, 1), 1, UInt8.TYPE);
        image.getDataBuffer().writeInt8(data, 0);

        final Texture2D texture = new Texture2D(image, TextureUtils.toTextureInfo(image, false));

        final GLSource source = GLBindings.getBindings()[0].createOffScreen(1, 1, null);
        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                final GL gl = source.getGL();
                texture.loadOnGpuMemory(gl);
                texture.unloadFromSystemMemory(gl);
                texture.loadOnSystemMemory(gl);
                GLUtilities.checkGLErrorsFail(gl);
            }

            @Override
            public void dispose(GLSource source) {
                texture.unloadFromGpuMemory(source.getGL());
            }
        });
        source.render();
        source.dispose();

        final Image resImage = texture.getImage();
        Assert.assertArrayEquals(data, resImage.getDataBuffer().toByteArray());
    }

    @Test
    public void testLoadUnloadTexture2D_2B_BYTE(){

        final byte[] data = new byte[]{-13,127,-121,46};
        final Image image = Images.createCustomBand(new Extent.Long(2, 1), 2, Int8.TYPE);
        image.getDataBuffer().writeInt8(data, 0);

        final Texture2D texture = new Texture2D(image, TextureUtils.toTextureInfo(image, false));

        final GLSource source = GLBindings.getBindings()[0].createOffScreen(1, 1, null);
        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                final GL gl = source.getGL();
                texture.loadOnGpuMemory(gl);
                texture.unloadFromSystemMemory(gl);
                texture.loadOnSystemMemory(gl);
                GLUtilities.checkGLErrorsFail(gl);
            }

            @Override
            public void dispose(GLSource source) {
                texture.unloadFromGpuMemory(source.getGL());
            }
        });
        source.render();
        source.dispose();

        final Image resImage = texture.getImage();
        Assert.assertArrayEquals(data, resImage.getDataBuffer().toByteArray());
    }

    @Test
    public void testLoadUnloadTexture2D_RGB(){

        final byte[] data = new byte[]{9,9,9, 8,8,8, 7,7,7, 6,6,6};
        final Image image = Images.create(new Extent.Long(4, 1), Images.IMAGE_TYPE_RGB);
        image.getDataBuffer().writeInt8(data, 0);

        final Texture2D texture = new Texture2D(image, TextureUtils.toTextureInfo(image, true));

        final GLSource source = GLBindings.getBindings()[0].createOffScreen(1, 1, null);
        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                final GL gl = source.getGL();
                texture.loadOnGpuMemory(gl);
                texture.unloadFromSystemMemory(gl);
                texture.loadOnSystemMemory(gl);
                GLUtilities.checkGLErrorsFail(gl);
            }

            @Override
            public void dispose(GLSource source) {
                texture.unloadFromGpuMemory(source.getGL());
            }
        });
        source.render();
        source.dispose();

        final Image resImage = texture.getImage();
        Assert.assertArrayEquals(data, resImage.getDataBuffer().toByteArray());
    }

}
