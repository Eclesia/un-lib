
package science.unlicense.engine.opengl.renderer;

import org.junit.Test;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.RenderingTest;
import science.unlicense.engine.opengl.technique.DebugNormalTechnique;
import science.unlicense.engine.opengl.util.GLTest;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.image.api.Image;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 *
 * @author Johann Sorel
 */
public class DebugNormalRendererTest extends GLTest {

    @Test
    public void testRender(){

        final DefaultModel mesh = DefaultModel.createFromGeometry(new BBox(new Vector3f64(-1,-1,-1), new Vector3f64(1, 1, 1)));
        GLEngineUtils.makeCompatible(mesh);
        mesh.getTechniques().add(new DebugNormalTechnique());
        final MonoCamera camera = new MonoCamera();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        scene.getChildren().add(mesh);
        scene.getChildren().add(camera);

        Image img = RenderingTest.renderBasic(scene, camera, 100, 100);

    }

}
