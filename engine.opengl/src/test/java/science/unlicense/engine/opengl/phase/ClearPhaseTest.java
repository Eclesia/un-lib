

package science.unlicense.engine.opengl.phase;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.util.GLTest;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Vector2i32;

/**
 * Test ClearPhase.
 * @author Johann Sorel
 */
public class ClearPhaseTest extends GLTest {

    @Test
    public void testRedClear(){
        final Image image = render(Color.RED);
        Assert.assertNotNull(image);
        final TupleGrid cm = image.getColorModel().asTupleBuffer(image);

        for (int y=0;y<1;y++){
            for (int x=0;x<1;x++){
                final Color rgba = image.getColor(new Vector2i32(x, y));
                //test integer values to avoid rounding issues
                Assert.assertEquals(Color.RED.toARGB(), rgba.toARGB());
            }
        }
    }

    private static Image render(Color clearColor){

        final GLSource source = GLUtilities.createOffscreenSource(1, 1);
        final FBO fbo = new FBO(1, 1);
        fbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(1, 1,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo.addAttachment(null, GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(1, 1,Texture2D.DEPTH24_STENCIL8()));

        //build the scene
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setCameraType(camera.TYPE_FLAT);
        scene.getChildren().add(camera);

        //render it
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(fbo,clearColor.toRGBAPreMul()));

        final Image[] buffer = new Image[1];
        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);
                //get the result image
                fbo.getColorTexture().loadOnSystemMemory(source.getGL());
                final Image image = fbo.getColorTexture().getImage();
                buffer[0] = image;
            }

            @Override
            public void dispose(GLSource source) {
                context.dispose(source);
                GLEngineUtils.dispose(scene, context);
            }
        });
        source.render();
        source.dispose();

        return buffer[0];
    }

}
