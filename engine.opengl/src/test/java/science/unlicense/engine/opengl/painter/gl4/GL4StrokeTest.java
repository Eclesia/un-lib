

package science.unlicense.engine.opengl.painter.gl4;

import org.junit.BeforeClass;
import science.unlicense.display.api.painter2d.ImagePainter2D;
import science.unlicense.display.api.painter2d.StrokeTest;
import science.unlicense.engine.opengl.painter.gl3.GL3ImagePainter2D;
import static science.unlicense.engine.opengl.util.GLTest.isGLAvailable;

/**
 *
 * @author Johann Sorel
 */
public class GL4StrokeTest extends StrokeTest {

    @BeforeClass
    public static void beforeClass() {
        org.junit.Assume.assumeTrue(isGLAvailable());
    }

    @Override
    protected ImagePainter2D createPainter(int width, int height) {
        return new GL3ImagePainter2D(width,height, null);
    }

}
