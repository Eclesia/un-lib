

package science.unlicense.engine.opengl.light;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.Attenuation;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.light.SpotLight;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderTest;
import science.unlicense.engine.opengl.phase.GBO;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.effect.LightPhase;
import science.unlicense.engine.opengl.util.GLTest;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author Johann Sorel
 */
public class LightTest extends GLTest {

    private static final float DELTA = 0.0001f;

    /**
     * No light, the ambient factor should me 1.0, so we expect the diffuse color of the mesh.
     */
    @Test
    public void testNoLight() throws IOException{

        final SceneNode scene = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        final GLModel mesh = DeferredRenderTest.createQuadMesh();
        SimpleBlinnPhong.view((Material) mesh.getMaterials().get(0)).setDiffuse(Color.GREEN);
        scene.getChildren().add(mesh);

        final Image imgColor = render(scene, 100, 100);

        final Vector2i32 coord = new Vector2i32();
        Assert.assertTrue(Color.GREEN.equals(imgColor.getColor(coord),null,0));
        coord.x = 99; coord.y = 99;
        Assert.assertTrue(Color.GREEN.equals(imgColor.getColor(coord),null,0));

    }

    /**
     * Half intensity ambient light, so we expect half the diffuse color of the mesh.
     */
    @Test
    public void testAmbiantLight() throws IOException{

        final SceneNode scene = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        final GLModel mesh = DeferredRenderTest.createQuadMesh();
        SimpleBlinnPhong.view((Material) mesh.getMaterials().get(0)).setDiffuse(Color.GREEN);
        scene.getChildren().add(mesh);

        final AmbientLight light = new AmbientLight(new ColorRGB(0.5f,0.5f,0.5f,1.0f),new ColorRGB(0.5f,0.5f,0.5f,1.0f));
        scene.getChildren().add(light);

        final Image imgColor = render(scene, 100, 100);

        final Color halfGreen = new ColorRGB(0.0f, 0.5f, 0.0f);

        final Vector2i32 coord = new Vector2i32();
        Assert.assertEquals(halfGreen.toARGB(), imgColor.getColor(coord).toARGB());
        coord.x = 99; coord.y = 99;
        Assert.assertEquals(halfGreen.toARGB(), imgColor.getColor(coord).toARGB());

    }

    /**
     *
     */
    @Test
    public void testPointLight() throws IOException{

        final SceneNode scene = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        final GLModel mesh = DeferredRenderTest.createQuadMesh();
        SimpleBlinnPhong.view((Material) mesh.getMaterials().get(0)).setAmbient(Color.WHITE);
        SimpleBlinnPhong.view((Material) mesh.getMaterials().get(0)).setSpecular(Color.BLACK);
        SimpleBlinnPhong.view((Material) mesh.getMaterials().get(0)).setDiffuse(Color.GREEN);
        scene.getChildren().add(mesh);

        final AmbientLight ambLight = new AmbientLight(new ColorRGB(0.0f,0.0f,0.0f,1.0f),new ColorRGB(0.5f,0.5f,0.5f,1.0f));
        scene.getChildren().add(ambLight);
        final PointLight light = new PointLight();
        light.setAttenuation(new Attenuation(1, 1, 0));
        light.getNodeTransform().getTranslation().setXYZ(0, 0, 0);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        final Image imgColor = render(scene, 100, 100);

        final Vector2i32 coord = new Vector2i32();
        Color colorCorner = imgColor.getColor(coord);
        coord.x = 49; coord.y = 49;
        Color colorCenter = imgColor.getColor(coord);

        //red and blue are at zero and green value at corner must be smaller
        Assert.assertEquals(0,colorCenter.getRed(), DELTA);
        Assert.assertEquals(0,colorCorner.getRed(), DELTA);
        Assert.assertEquals(0,colorCenter.getBlue(), DELTA);
        Assert.assertEquals(0,colorCorner.getBlue(), DELTA);
        Assert.assertTrue(colorCenter.getGreen() > colorCorner.getGreen());
    }

    /**
     *
     */
    @Test
    public void testSpotLight() throws IOException{

        final SceneNode scene = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        final GLModel mesh = DeferredRenderTest.createQuadMesh();
        SimpleBlinnPhong.view((Material) mesh.getMaterials().get(0)).setAmbient(Color.WHITE);
        SimpleBlinnPhong.view((Material) mesh.getMaterials().get(0)).setSpecular(Color.BLACK);
        SimpleBlinnPhong.view((Material) mesh.getMaterials().get(0)).setDiffuse(Color.GREEN);
        scene.getChildren().add(mesh);

        final AmbientLight ambLight = new AmbientLight(new ColorRGB(0.0f,0.0f,0.0f,1.0f),new ColorRGB(0.5f,0.5f,0.5f,1.0f));
        scene.getChildren().add(ambLight);
        final SpotLight light = new SpotLight();
        light.setAttenuation(new Attenuation(1, 1, 0));
        light.setFallOffAngle(20f);
        light.setFallOffExponent(1f);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        final Image imgColor = render(scene, 100, 100);

        final Vector2i32 coord = new Vector2i32();
        Color colorCorner = imgColor.getColor(coord);
        coord.x = 49; coord.y = 49;
        Color colorCenter = imgColor.getColor(coord);

        //red and blue are at zero and green value at corner must be 0
        Assert.assertEquals(0,colorCenter.getRed(), DELTA);
        Assert.assertEquals(0,colorCorner.getRed(), DELTA);
        Assert.assertEquals(0,colorCenter.getBlue(), DELTA);
        Assert.assertEquals(0,colorCorner.getBlue(), DELTA);
        Assert.assertEquals(0,colorCorner.getGreen(), DELTA);
        Assert.assertTrue(colorCenter.getGreen() > colorCorner.getGreen());
    }


    public static Image render(final SceneNode scene, int width, int height) {

        final GLSource source = GLUtilities.createOffscreenSource(width,height);
        final GBO gbo = new GBO(width, height);
        final Texture2D texDiffuse = (Texture2D) gbo.getDiffuseTexture();
        final Texture2D texSpecular = (Texture2D) gbo.getSpecularTexture();
        final Texture2D texNormal = (Texture2D) gbo.getNormalTexture();
        final Texture2D texPosition = (Texture2D) gbo.getPositionTexture();

        final FBO lightfbo = new FBO(width, height);
        final Texture2D texColor = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());
        lightfbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0,texColor);


        //build the scene
        final MonoCamera camera = new MonoCamera();
        camera.setCameraType(MonoCamera.TYPE_ORTHO);
        scene.getChildren().add(camera);

        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(gbo,Color.TRS_BLACK.toRGBAPreMul()));
        context.getPhases().add(new DeferredRenderPhase(scene,camera,gbo));
        context.getPhases().add(new LightPhase(lightfbo, texDiffuse, texSpecular, texPosition, texNormal, scene, camera));

        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);
                //get the result image
                lightfbo.loadOnSystemMemory(source.getGL());
                gbo.loadOnSystemMemory(source.getGL());
                //release resources
                GLEngineUtils.dispose(scene, context);
                context.dispose(source);
            }

            @Override
            public void dispose(GLSource source) {
                context.dispose(source);
                gbo.unloadFromGpuMemory(source.getGL());
                lightfbo.unloadFromGpuMemory(source.getGL());
                GLEngineUtils.dispose(scene, context);
            }
        });
        source.render();
        source.dispose();

        return texColor.getImage();
    }

}
