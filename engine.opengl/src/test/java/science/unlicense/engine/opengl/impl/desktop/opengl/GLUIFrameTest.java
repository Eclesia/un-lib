
package science.unlicense.engine.opengl.impl.desktop.opengl;

import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.display.api.desktop.AbstractFrameTest;
import science.unlicense.display.api.desktop.Frame;

/**
 *
 * @author Johann Sorel
 */
public class GLUIFrameTest extends AbstractFrameTest{

    @Override
    protected Frame createFrame() {
        return GLUIFrameManager.INSTANCE.createFrame(false);
    }

}
