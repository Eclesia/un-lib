
package science.unlicense.engine.opengl.util.resource;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.engine.opengl.util.GLTest;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.VBO;

/**
 *
 * @author Johann Sorel
 */
public class VBOTest extends GLTest {

    private static final float DELTA = 0.00001f;

    @Test
    public void testLoadOnSystemMemory(){

        final GLSource source = GLUtilities.createOffscreenSource(1, 1);

        final VBO vbo = new VBO(new float[]{1,2,3,4,5,6}, 3);

        source.getCallbacks().add(new GLCallback() {

            @Override
            public void execute(GLSource source) {
                final GL gl = source.getGL();
                vbo.loadOnGpuMemory(gl);
                vbo.unloadFromSystemMemory(gl);
                Assert.assertNull(vbo.getPrimitiveBuffer());

                vbo.loadOnSystemMemory(gl);
                final Buffer res = vbo.getPrimitiveBuffer();
                Assert.assertNotNull(res);
                Assert.assertArrayEquals(new float[]{1,2,3,4,5,6}, res.toFloatArray(), DELTA);
            }

            @Override
            public void dispose(GLSource source) {
                vbo.unloadFromGpuMemory(source.getGL());
            }
        });

        source.render();
        source.dispose();

    }

}
