

package science.unlicense.engine.opengl.phase;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.phase.picking.PickActor;
import science.unlicense.engine.opengl.phase.picking.PickResetPhase;
import science.unlicense.engine.opengl.technique.GLActorTechnique;
import science.unlicense.engine.opengl.util.GLTest;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.tuple.BoxCursor;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author Johann Sorel
 */
public class PickingTest extends GLTest {

    @Test
    public void testPicking() throws IOException{

        final int width = 10;
        final int height = 10;

        final GLSource source = GLUtilities.createOffscreenSource(width, height);
        final GLProcessContext context = new DefaultGLProcessContext(source);
        final Sequence phases = context.getPhases();

        final GBO fbo = new GBO(width, height, 0, true, false, false, false, true, true);
        final Texture2D texDiffuse  = (Texture2D) fbo.getDiffuseTexture();
        final Texture2D texPickMesh = (Texture2D) fbo.getMeshIdTexture();
        final Texture2D texPickVertex = (Texture2D) fbo.getVertexIdTexture();

        //build the scene
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.getNodeTransform().getTranslation().setXYZ(0,0,30);
        camera.getNodeTransform().notifyChanged();
        scene.getChildren().add(camera);


        final PickResetPhase pickResetPhases = new PickResetPhase(texPickMesh, texPickVertex);

        final GLModel boxOne = new GLModel(DefaultModel.createFromGeometry(new BBox(new double[]{-5,-10,0},new double[]{5,10,1})));
        final SimpleBlinnPhong.Material matOne = SimpleBlinnPhong.newMaterial();
        matOne.setDiffuse(Color.RED);
        boxOne.getMaterials().add(matOne);
        boxOne.setPickable(true);
        ((GLActorTechnique) boxOne.getTechniques().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, boxOne));
        GLEngineUtils.makeCompatible(boxOne);
        scene.getChildren().add(boxOne);

        final GLModel boxTwo = new GLModel(DefaultModel.createFromGeometry(new BBox(new double[]{-5,-5,1},new double[]{5,0,2})));
        final SimpleBlinnPhong.Material matTwo = SimpleBlinnPhong.newMaterial();
        matTwo.setDiffuse(Color.GREEN);
        boxTwo.getMaterials().add(matTwo);
        GLEngineUtils.makeCompatible(boxTwo);
        boxTwo.setPickable(true);
        ((GLActorTechnique) boxTwo.getTechniques().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, boxTwo));
        scene.getChildren().add(boxTwo);

        final GLModel boxThree = new GLModel(DefaultModel.createFromGeometry(new BBox(new double[]{-5,0,1},new double[]{5,5,2})));
        final SimpleBlinnPhong.Material matThree = SimpleBlinnPhong.newMaterial();
        matThree.setDiffuse(Color.BLUE);
        boxThree.getMaterials().add(matThree);
        GLEngineUtils.makeCompatible(boxThree);
        boxThree.setPickable(false);
        ((GLActorTechnique) boxThree.getTechniques().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, boxThree));
        scene.getChildren().add(boxThree);

        final GLModel boxFour = new GLModel(DefaultModel.createFromGeometry(new BBox(new double[]{-5,5,1},new double[]{5,10,2})));
        final SimpleBlinnPhong.Material matFour = SimpleBlinnPhong.newMaterial();
        matFour.setDiffuse(Color.WHITE);
        boxFour.getMaterials().add(matFour);
        GLEngineUtils.makeCompatible(boxFour);
        SimpleBlinnPhong.view((Material) boxFour.getMaterials().get(0)).setDiffuse(Color.WHITE);
        boxFour.setPickable(false);
        ((GLActorTechnique) boxFour.getTechniques().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, boxFour));
        scene.getChildren().add(boxFour);

        final DeferredRenderPhase renderPhase = new DeferredRenderPhase(scene, camera, fbo);


        phases.add(new UpdatePhase(scene));
        phases.add(new ClearPhase(fbo));
        phases.add(pickResetPhases);
        phases.add(renderPhase);
        phases.add(new AbstractFboPhase() {
            protected void processInternal(GLProcessContext ctx) throws GLException {
                //get the result image
                texDiffuse.loadOnSystemMemory(ctx.getGL());
                texPickMesh.loadOnSystemMemory(ctx.getGL());
                texPickVertex.loadOnSystemMemory(ctx.getGL());
                final Image image0 = texDiffuse.getImage();
                final Image image1 = texPickMesh.getImage();
                final Image image2 = texPickVertex.getImage();

                //check diffuse colors
                checkImage(image0, new BBox(new Vector2f64(0, 0), new Vector2f64(10, 1)), new int[]{0,0,0,0});
                checkImage(image0, new BBox(new Vector2f64(3, 1), new Vector2f64(7,  3)), new int[]{255,0,0,255});      //RED
                checkImage(image0, new BBox(new Vector2f64(3, 3), new Vector2f64(7,  5)), new int[]{0,255,0,255});      //GREEN
                checkImage(image0, new BBox(new Vector2f64(3, 5), new Vector2f64(7,  7)), new int[]{0,0,255,255});      //BLUE
                checkImage(image0, new BBox(new Vector2f64(3, 7), new Vector2f64(7,  9)), new int[]{255,255,255,255});  //WHITE
                checkImage(image0, new BBox(new Vector2f64(0, 9), new Vector2f64(10,10)), new int[]{0,0,0,0});
                //check mesh id : picking start at index 3 and increment for each object
                checkImage(image1, new BBox(new Vector2f64(0, 0), new Vector2f64(10, 1)), new int[]{0});
                checkImage(image1, new BBox(new Vector2f64(3, 1), new Vector2f64(7,  3)), new int[]{3});
                checkImage(image1, new BBox(new Vector2f64(3, 3), new Vector2f64(7,  5)), new int[]{4});
                checkImage(image1, new BBox(new Vector2f64(3, 5), new Vector2f64(7,  7)), new int[]{1});    //occlusion = 1
                checkImage(image1, new BBox(new Vector2f64(3, 7), new Vector2f64(7,  9)), new int[]{1});    //occlusion = 1
                checkImage(image1, new BBox(new Vector2f64(0, 9), new Vector2f64(10,10)), new int[]{0});
            }
        });

        //render it
        source.render();

        GLEngineUtils.dispose(scene, context);
        source.dispose();
    }

    private static void checkImage(Image image, BBox rectangle, int[] samples){
        final int[] buffer = new int[samples.length];

        final TupleGridCursor cursor = new BoxCursor(image.getRawModel().asTupleBuffer(image).cursor(),rectangle);
        while (cursor.next()) {
            cursor.samples().toInt(buffer, 0);
            Assert.assertArrayEquals(samples, buffer);
        }

    }

}
