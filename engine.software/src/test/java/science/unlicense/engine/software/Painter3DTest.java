
package science.unlicense.engine.software;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.painter3d.Painter3D;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.RenderState;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Painter3D test.
 *
 * @author Johann Sorel
 */
public abstract class Painter3DTest {

    protected abstract Painter3D create(int width, int height);

    /**
     * Test image is filled with translucent pixels at initialisation.
     */
    @Test
    public void emptyTest() {
        final Painter3D painter = create(100, 100);
        final Image image = painter.render();

        Color color = image.getColor(new Vector2i32(50,50));
        Assert.assertEquals(new ColorRGB(0, 0, 0, 0), color);
    }

    /**
     * Test rendering of a single mesh with blinn phong rendering technique.
     */
    @Test
    public void simpleBlinnPhongMeshTest() {

        //create scene
        final Model model = createQuad(-1, Color.BLUE);
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setCameraType(MonoCamera.TYPE_ORTHO);
        scene.getChildren().add(camera);
        scene.getChildren().add(model);


        //render
        final Painter3D painter = create(100, 100);
        painter.setCamera(camera);
        painter.setScene(scene);
        final Image image = painter.render();

        //tests
        Color color = image.getColor(new Vector2i32(50,50));
        Assert.assertEquals(new ColorRGB(0, 0, 255, 255), color);
    }

    /**
     * Test red mesh is rendered above blue mesh
     */
    @Test
    public void fragmentZorderTest() {

        //create scene
        final Model model1 = createQuad(-2, Color.BLUE);
        final Model model2 = createQuad(-1, Color.RED);
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setCameraType(MonoCamera.TYPE_ORTHO);
        scene.getChildren().add(camera);
        scene.getChildren().add(model1);
        scene.getChildren().add(model2);


        //render
        final Painter3D painter = create(100, 100);
        painter.setCamera(camera);
        painter.setScene(scene);
        final Image image = painter.render();

        //tests
        Color color = image.getColor(new Vector2i32(50,50));
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), color);
    }

    /**
     * Test color blending with two meshes.
     */
    @Test
    public void colorBlendingTest() {

        //create scene
        final Model model1 = createQuad(-2, Color.BLUE);
        final Model model2 = createQuad(-1, new ColorRGB(255, 0, 0, 128));
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setCameraType(MonoCamera.TYPE_ORTHO);
        scene.getChildren().add(camera);
        scene.getChildren().add(model1);
        scene.getChildren().add(model2);


        //render
        final Painter3D painter = create(100, 100);
        painter.setCamera(camera);
        painter.setScene(scene);
        final Image image = painter.render();

        //tests
        Color color = image.getColor(new Vector2i32(50,50));
        Assert.assertEquals(new ColorRGB(128, 0, 126, 255), color);
    }

    private static Model createQuad(float depth, Color color) {

        final DefaultModel mesh = new DefaultModel();
        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(InterleavedTupleGrid1D.create(new float[]{
            -0.5f,-0.5f,depth,
            +0.5f,-0.5f,depth,
            +0.5f,+0.5f,depth,
            -0.5f,+0.5f,depth
            },3));
        shell.setIndex(InterleavedTupleGrid1D.create(new int[]{0,1,2,2,3,0}, 1));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 6)});
        mesh.setShape(shell);

        final SimpleBlinnPhong.Material mat = SimpleBlinnPhong.newMaterial();
        mat.setDiffuse(color);
        mesh.getMaterials().add(mat);

        SimpleBlinnPhong technique = new SimpleBlinnPhong();
        technique.getState().setCulling(RenderState.CULLING_NONE);
        mesh.getTechniques().add(technique);

        return mesh;
    }
}
