
package science.unlicense.engine.software;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.math.api.Vector;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class FragmentGeneratorTest {

    @Test
    public void testTriangleFragments() {

        test(0, 0,
             0, 4,
             4, 0,
             Integer.MIN_VALUE,Integer.MAX_VALUE,Integer.MIN_VALUE,Integer.MAX_VALUE,
             new int[][]{
                {0,0,0,0,0},
                {1,0,0,0,0},
                {1,1,0,0,0},
                {1,1,1,0,0},
                {1,1,1,1,0},
            });

        test(1, 1,
             1, 4,
             4, 1,
             Integer.MIN_VALUE,Integer.MAX_VALUE,Integer.MIN_VALUE,Integer.MAX_VALUE,
             new int[][]{
                {0,0,0,0,0},
                {0,1,0,0,0},
                {0,1,1,0,0},
                {0,1,1,1,0},
                {0,0,0,0,0},
            });

        test(1.4, 0.4,
             1.4, 4.6,
             3.6, 2.5,
             Integer.MIN_VALUE,Integer.MAX_VALUE,Integer.MIN_VALUE,Integer.MAX_VALUE,
             new int[][]{
                {0,1,0,0,0},
                {0,1,1,0,0},
                {0,1,1,1,0},
                {0,1,1,0,0},
                {0,1,0,0,0},
            });

        test(0.6, 0.6,
             0.6, 4.4,
             4.4, 0.6,
             Integer.MIN_VALUE,Integer.MAX_VALUE,Integer.MIN_VALUE,Integer.MAX_VALUE,
             new int[][]{
                {0,0,0,0,0},
                {0,1,0,0,0},
                {0,1,1,0,0},
                {0,1,1,1,0},
                {0,0,0,0,0},
            });

        test(0.6, 0.6,
             0.6, 4.4,
             4.4, 4.4,
             Integer.MIN_VALUE,Integer.MAX_VALUE,Integer.MIN_VALUE,Integer.MAX_VALUE,
             new int[][]{
                {0,0,0,0,0},
                {0,1,1,1,0},
                {0,1,1,0,0},
                {0,1,0,0,0},
                {0,0,0,0,0},
            });

    }

    @Test
    public void testClip() {

        test(1.4, 0.4,
             1.4, 4.6,
             3.6, 2.5,
             1,2,1,2,
             new int[][]{
                {0,0,0,0,0},
                {0,0,0,0,0},
                {0,1,1,0,0},
                {0,1,1,0,0},
                {0,0,0,0,0},
            });

        //test clip
    }

    private void test(
            double v1x, double v1y,
            double v2x, double v2y,
            double v3x, double v3y,
            int clipMinX, int clipMaxX, int clipMinY, int clipMaxY,
            int[][] expected) {


        final Sequence s = new ArraySequence();

        final FragmentGenerator generator = new FragmentGenerator() {
            @Override
            protected void pushFragment(FragmentGenerator.Fragment f) {
                s.add(new Vector2i32(f.x, f.y));
            }
        };

        final Vector v1 = new Vector3f64(v1x, v1y, 1);
        final Vector v2 = new Vector3f64(v2x, v2y, 1);
        final Vector v3 = new Vector3f64(v3x, v3y, 1);

        generator.setClip(clipMinX, clipMaxX, clipMinY, clipMaxY);
        generator.processTriangle(v1, v2, v3);

        for (int x=0; x<expected[0].length; x++) {
            loop:
            for (int vy=0; vy<expected.length; vy++) {
                final int y = expected.length-vy-1;

                if (expected[vy][x] == 0) {
                    for (int i=0; i<s.getSize(); i++) {
                        final Vector2i32 f = (Vector2i32) s.get(i);
                        if (f.x == x && f.y == y) {
                            Assert.fail("fragment should not be set at "+x+ " / "+y);
                        }
                    }
                } else {
                    for (int i=0; i<s.getSize(); i++) {
                        final Vector2i32 f = (Vector2i32) s.get(i);
                        if (f.x == x && f.y == y) {
                            s.remove(i);
                            continue loop;
                        }
                    }
                    Assert.fail("fragment should be set at "+x+ " / "+y);
                }

            }
        }

        if (!s.isEmpty()) {
            Assert.fail("some fragments are duplicated ");
        }

    }

}
