
package science.unlicense.engine.software;

import science.unlicense.display.api.painter3d.Painter3D;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public class SoftwarePainter3DTest extends Painter3DTest {

    @Override
    protected Painter3D create(int width, int height) {
        return new SoftwarePainter3D(new Extent.Long(width, height));
    }

}
