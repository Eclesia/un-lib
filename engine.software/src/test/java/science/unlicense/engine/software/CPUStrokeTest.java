
package science.unlicense.engine.software;

import science.unlicense.engine.software.CPUPainter2D;
import science.unlicense.display.api.painter2d.ImagePainter2D;
import science.unlicense.display.api.painter2d.StrokeTest;

/**
 *
 * @author Johann Sorel
 */
public class CPUStrokeTest extends StrokeTest{

    protected ImagePainter2D createPainter(int width, int height) {
        return new CPUPainter2D(width,height);
    }

}
