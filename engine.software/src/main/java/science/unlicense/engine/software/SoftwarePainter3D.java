
package science.unlicense.engine.software;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.DefaultInt32Buffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Properties;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.display.api.painter3d.AbstractPainter3D;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.FlattenVisitor;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.light.Light;
import science.unlicense.display.impl.light.Lights;
import science.unlicense.display.impl.light.PixelFragment;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.light.ProjectiveLight;
import science.unlicense.display.impl.light.SpotLight;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.geometry.api.Curve;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.Surface;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.Volume;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.TriangulateOp;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.impl.Arc;
import science.unlicense.geometry.impl.CubicCurve;
import science.unlicense.geometry.impl.DefaultFragment;
import science.unlicense.geometry.impl.Fragment;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.QuadraticCurve;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.Vertex;
import science.unlicense.geometry.impl.transform.Decomposer;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Scalarf64;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.math.impl.Vector4f32;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vectors;
import science.unlicense.model3d.impl.effect.AfterEffect;
import science.unlicense.model3d.impl.effect.AttachmentType;
import science.unlicense.model3d.impl.geometry.ModelVisitor;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.SimplePBR;
import science.unlicense.model3d.impl.technique.SubSceneTechnique;
import science.unlicense.model3d.impl.technique.Technique;
import science.unlicense.task.api.Task;
import science.unlicense.task.api.TaskDescriptor;

/**
 *
 * @author Johann Sorel
 */
public class SoftwarePainter3D extends AbstractPainter3D {

    private final FlattenVisitor flattener = new FlattenVisitor();

    private final ModelVisitor shellVisitor = new ModelVisitor() {
        @Override
        protected void visit(ModelVisitor.MeshVertex vertex) {
        }
        @Override
        protected void visit(ModelVisitor.MeshTriangle candidate) {
            processTriangle(candidate);
        }
        @Override
        protected void visit(ModelVisitor.MeshLine candidate) {
            processLine(candidate);
        }
    };
    private final Decomposer geomDecomposer = new Decomposer() {

        @Override
        public void process(Geometry geom) {
            super.process(geom);
        }

        @Override
        protected void processPoint(Point geometry) {}

        @Override
        protected void processLine(Line geometry) {
            SoftwarePainter3D.this.processLine(geometry);
        }

        @Override
        protected void processTriangle(Triangle geometry) {
            SoftwarePainter3D.this.processTriangle(geometry);
        }

        @Override
        protected void processCubicCurve(CubicCurve geometry) {}

        @Override
        protected void processQuadraticCurve(QuadraticCurve geometry) {}

        @Override
        protected void processArc(Arc geometry) {}

        @Override
        protected void processCurve(Curve geometry) {}

        @Override
        protected void processSurface(Surface geometry) {}

        @Override
        protected void processVolume(Volume geometry) {}

        @Override
        protected void processOther(Geometry geometry) {
            TriangulateOp op = new TriangulateOp(geometry, 0);
            try {
                process(op.execute());
            } catch (OperationException ex) {
                ex.printStackTrace();
            }
        }
    };
    private final FragmentGenerator fragGenerator = new FragmentGenerator() {
        @Override
        protected void pushFragment(Fragment fragment) {
            SoftwarePainter3D.this.fragment = fragment;

            int fragArrayIndex = fragment.y * width + fragment.x;

            if (depthTest(fragArrayIndex, fragment.depth)) {
                paintFragment(new Vector2i32(fragment.x, fragment.y), fragArrayIndex);
            }
        }
    };

    private static final ImageModel RM = new InterleavedModel(new UndefinedSystem(4), UInt8.TYPE);
    private static final ImageModel CM = DerivateModel.create(RM, new int[]{1,2,3,0}, null, null, ColorSystem.RGBA_8BITS);

    //global state
    private final Properties context = new Properties();
    private int width;
    private int height;
    private int[] colorArray;
    private float[] depthArray;
    private boolean globalLightning = false;
    private boolean enableShadow = false;
    private int shadowMapSize = 512;
    private boolean enableColor = true;

    // the different textures used
    private Image colorImage;
    private final Dictionary layerTextures = new HashDictionary();
    //cache previous textures, recycle them
    private final Dictionary oldlayerTextures = new HashDictionary();

    //scene variables
    private Sequence lights;
    private LightState[] lightstates;

    //current mesh
    private Similarity M;
    private Similarity V;
    private Matrix P;
    private Similarity MV;
    private Matrix MVP;

    //current mesh informations
    private Triangle visitedTriangle;
    private Line visitedLine;
    private Model model;
    private Material material;
    private Technique technique;

    //current fragment
    private FragmentGenerator.Fragment fragment;
    private final SimpleBlinnPhong.Fragment blinnphongFrag =
            SimpleBlinnPhong.view(new DefaultFragment(CoordinateSystems.UNDEFINED_3D));

    //working variables
    private final Vector4f32 v_proj1 = new Vector4f32();
    private final Vector4f32 v_proj2 = new Vector4f32();
    private final Vector4f32 v_proj3 = new Vector4f32();


    public SoftwarePainter3D(Extent.Long extent) {
        this.extent = new Extent.Long(extent.getL(0), extent.getL(1));
        //all sizes and images will be configured in updateTexture method
        shellVisitor.setApplyJointTransform(true);
        shellVisitor.setApplyMorphTargets(true);
    }

    public Extent.Long getSize() {
        return new Extent.Long(extent.getL(0), extent.getL(1));
    }

    @Override
    public void setCanvasSize(Extent.Long extent) {
        super.setCanvasSize(extent);
    }

    public void setGlobalLightning(boolean enableLight) {
        this.globalLightning = enableLight;
    }

    public boolean isGlobalLightning() {
        return globalLightning;
    }

    public void setEnableShadpw(boolean enableShadow) {
        this.enableShadow = enableShadow;
    }

    public boolean isEnableShadow() {
        return enableShadow;
    }

    @Override
    public Image render() {
        return render(true);
    }

    /**
     *
     * @param copy true to generate to copy of the image, not the raw image used by this instance
     * @return
     */
    public synchronized Image render(boolean copy) {
        prepareTextures();

        //main rendering
        renderScene();

        //render after effects
        final Dictionary inpouts = new HashDictionary();
        inpouts.addAll(layerTextures);

        for (int i = 0,n = afterEffects.getSize(); i < n; i++) {
            final Task effect = (Task) afterEffects.get(i);
            final TaskDescriptor td = effect.getDescriptor();
            final FieldType[] inputs = td.getInputType().getFields();
            final FieldType[] outputs = td.getOutputType().getFields();

            for (int k = 0,kn = inputs.length; k < kn; k++) {
                if (inputs[k] instanceof AttachmentType) {
                    final Chars id = inputs[k].getId();
                    effect.inputs().setPropertyValue(id, layerTextures.getValue(id));
                }
            }
            for (int k = 0,kn = outputs.length; k < kn; k++) {
                if (outputs[k] instanceof AttachmentType) {
                    final Chars id = outputs[k].getId();
                    effect.outputs().setPropertyValue(id, layerTextures.getValue(id));
                }
            }

            effect.perform();
        }

        if (copy) {
            return new DefaultImage(new DefaultInt32Buffer(colorArray.clone()), colorImage.getExtent(), RM, CM);
        } else {
            return colorImage;
        }
    }

    /**
     * Generate all textures which will be needed for rendering and after effects.
     */
    private void prepareTextures() {

        //check if size changed
        if (extent.get(0) != width || extent.get(1) != height) {
            width = (int) extent.get(0);
            height = (int) extent.get(1);
            colorArray = new int[width*height];
            depthArray = new float[width*height];
            colorImage = new DefaultImage(new DefaultInt32Buffer(colorArray), extent, RM, CM);
            fragGenerator.setClip(0, (int) extent.getL(0)-1, 0, (int) extent.getL(1)-1);
            layerTextures.removeAll();
            oldlayerTextures.removeAll();
        }

        oldlayerTextures.removeAll();
        oldlayerTextures.addAll(layerTextures);

        //reset default color and depth textures
        Arrays.fill(colorArray, 0);
        Arrays.fill(depthArray, Float.MAX_VALUE);
        layerTextures.add(AfterEffect.LAYER_COLOR, colorImage.getTupleBuffer(colorImage.getRawModel()));

        for (int i = 0,n = afterEffects.getSize(); i < n; i++) {
            final Task effect = (Task) afterEffects.get(i);
            final TaskDescriptor td = effect.getDescriptor();
            final FieldType[] inputs = td.getInputType().getFields();
            final FieldType[] outputs = td.getOutputType().getFields();

            for (int k = 0,kn = inputs.length; k < kn; k++) {
                if (inputs[k] instanceof AttachmentType) {
                    buildTexture((AttachmentType) inputs[k]);
                }
            }
            for (int k = 0,kn = outputs.length; k < kn; k++) {
                if (outputs[k] instanceof AttachmentType) {
                    buildTexture((AttachmentType) outputs[k]);
                }
            }
        }
    }

    private void buildTexture(AttachmentType layer) {
        if (layerTextures.getValue(layer.getId()) == null) {
            final TupleGrid oldTexture = (TupleGrid) oldlayerTextures.getValue(layer.getId());
            if (oldTexture != null) {
                //clear and recycle texture
                oldTexture.setTuple(oldTexture.getCoordinateGeometry(), oldTexture.createTuple());
                layerTextures.add(layer.getId(), oldTexture);
            } else {
                //create new texture
                final Image image = Images.createCustomBand(extent, layer.getSampleSystem().getNumComponents(), layer.getNumericType());
                final TupleGrid grid = image.getTupleBuffer(image.getRawModel());
                layerTextures.add(layer.getId(), grid);
            }
        }
    }

    private void renderScene() {

        if (camera != null && scene != null) {
            camera.setRenderArea(new Rectangle(colorImage.getExtent()));

            V = camera.getRootToNodeSpace();
            P = camera.getProjectionMatrix();

            // camera projection matrix include near and far values
            // and normalize the distance between -1(near) and +1(far)
            fragGenerator.setZClip(-1, +1);

            flattener.reset();

            flattener.visit(scene, null);
            final Sequence nodes = flattener.getCollection();

            lights = null;

            //do the rendering
            for (int i=0,n=nodes.getSize();i<n;i++) {
                final Object node = nodes.get(i);
                if (node instanceof Model) {
                    render((Model) node);
                } else if (node instanceof GraphicNode) {
                    Sequence techniques = ((GraphicNode) node).getTechniques();
                    Iterator ite = techniques.createIterator();
                    while (ite.hasNext()) {
                        Technique tech = (Technique) ite.next();
                        if (tech instanceof SubSceneTechnique) {
                            SubSceneTechnique subtech = (SubSceneTechnique) tech;
                            FlattenVisitor flattener = new FlattenVisitor();
                            flattener.reset();
                            flattener.visit(subtech.derivateScene((SceneNode) node), null);
                            final Sequence subnodes = flattener.getCollection();
                            for (int k=0,kn=subnodes.getSize();k<kn;k++) {
                                final Object subnode = subnodes.get(k);
                                if (subnode instanceof Model) {
                                    render((Model) subnode);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void updateLights() {
        if (lights != null) return;

        lights = Lights.getLights(scene);
        lightstates = new LightState[lights.getSize()];
        for (int i=0;i<lightstates.length;i++) {
            lightstates[i] = new LightState();
            final Light pl = (Light) lights.get(i);
            if (pl instanceof PointLight) {
                final Vector3f64 lightPose = (Vector3f64) ((PointLight) pl).getPosition();
                V.transform(lightPose, lightPose);
                lightstates[i].position = lightPose;
                if (enableShadow) buildShadowMap(camera, pl, shadowMapSize, lightstates[i]);
            } else if (pl instanceof SpotLight) {
                final Vector3f64 lightPose = (Vector3f64) ((SpotLight) pl).getPosition();
                V.transform(lightPose, lightPose);
                lightstates[i].position = lightPose;
                if (enableShadow) buildShadowMap(camera, pl, shadowMapSize, lightstates[i]);
            }
        }
    }

    public Image getDepthImage() {
        final InterleavedModel dm = new InterleavedModel(UndefinedSystem.create(1), Float32.TYPE);
        final Buffer buffer = DefaultBufferFactory.wrap(depthArray);
        return new DefaultImage(buffer, new Extent.Long(width, height), dm);
    }

    private void render(Model model) {
        this.model = model;
        final Sequence techniques = model.getTechniques();
        for (int i=0,n=techniques.getSize(); i<n; i++) {
            technique = (Technique) techniques.get(i);
            render(model, technique);
        }
    }

    private void render(Model model, Technique tech) {

        if (tech instanceof SubSceneTechnique) {
            final SubSceneTechnique subtech = (SubSceneTechnique) tech;
            FlattenVisitor flattener = new FlattenVisitor();
            flattener.reset();
            flattener.visit(subtech.derivateScene(model), null);
            final Sequence nodes = flattener.getCollection();
            for (int k=0,kn=nodes.getSize();k<kn;k++) {
                final Object node = nodes.get(k);
                if (node instanceof Model) {
                    render((Model) node);
                }
            }
        } else {
            this.model = model;
            technique = tech;

            Chars materialName = technique.getMaterialName();
            if (materialName == null || materialName.isEmpty()) {
                material = (Material) model.getMaterials().get(0);
            } else {
                Iterator ite = model.getMaterials().createIterator();
                while (ite.hasNext()) {
                    Material mat = (Material) ite.next();
                    if (materialName.equals(mat.getName())) {
                        material = mat;
                        break;
                    }
                }
            }
            if (material == null) return;


            Geometry shape = model.getShape();

            //set transform
            M = model.getNodeToRootSpace();
            MV = V.multiply(M);
            MVP = P.multiply(V.toMatrix()).multiply(M.toMatrix());
            fragGenerator.setRenderState(technique.getState());

            //rasterize triangles
            if (shape instanceof Mesh) {
                shellVisitor.reset(model);
                shellVisitor.visit();
            } else if (shape instanceof Geometry) {
                geomDecomposer.process(shape);
            }
        }
    }

    private void processTriangle(Triangle candidate) {
        this.visitedTriangle = candidate;
        this.visitedLine = null;
        toDisplay(candidate.getFirstCoord(), v_proj1);
        toDisplay(candidate.getSecondCoord(), v_proj2);
        toDisplay(candidate.getThirdCoord(), v_proj3);
        fragGenerator.processTriangle(v_proj1, v_proj2, v_proj3);
    }

    private void processLine(Line candidate) {
        this.visitedTriangle = null;
        this.visitedLine = candidate;
        toDisplay(candidate.getStart(), v_proj1);
        toDisplay(candidate.getEnd(), v_proj2);
        fragGenerator.processLine(v_proj1, v_proj2);
    }

    private void toDisplay(Tuple vertice, Vector4f32 buffer) {
        buffer.x = (float) vertice.get(0);
        buffer.y = (float) vertice.get(1);
        buffer.z = (float) vertice.get(2);
        buffer.w = 1;
        MVP.transform(buffer, buffer);
        buffer.x /= buffer.w;
        buffer.y /= buffer.w;
        buffer.z /= buffer.w;
        buffer.x = ((1f+buffer.x)/2f) * width;
        buffer.y = ((1f+buffer.y)/2f) * height;
    }

    private boolean depthTest(int fragArrayIndex, double fragDepth) {
        if (technique.getState().isDepthTest()) {
            //depth clip
            boolean depthTest = fragDepth < depthArray[fragArrayIndex];
            if (depthTest && technique.getState().isWriteToDepth()) {
                //write depth
                depthArray[fragArrayIndex] = (float) fragDepth;
            }
            return depthTest;
        }
        return true;
    }

    private void paintFragment(Vector2i32 xy, int fragArrayIndex) {
        if (!enableColor) return;

        if (visitedTriangle instanceof ModelVisitor.MeshTriangle) {
            final ModelVisitor.MeshTriangle triangle = (ModelVisitor.MeshTriangle) visitedTriangle;
            final Fragment frag = new Mesh.InterpolatedFragment(
                    new Vertex[]{triangle.v0,triangle.v1,triangle.v2}, fragment.baryValues);
            paintFragment(frag, xy, fragArrayIndex);

        } else if (visitedTriangle instanceof Mesh.Triangle) {
            final Mesh.Triangle triangle = (Mesh.Triangle) visitedTriangle;
            final Vertex v0 = triangle.parent.getVertex(triangle.indices[0]);
            final Vertex v1 = triangle.parent.getVertex(triangle.indices[1]);
            final Vertex v2 = triangle.parent.getVertex(triangle.indices[2]);
            final Fragment frag = new Mesh.InterpolatedFragment(new Vertex[]{v0,v1,v2}, fragment.baryValues);
            paintFragment(frag, xy, fragArrayIndex);

        } else if (visitedLine instanceof Mesh.Line) {
            final Mesh.Line triangle = (Mesh.Line) visitedLine;
            final Vertex v0 = triangle.parent.getVertex(triangle.indices[0]);
            final Vertex v1 = triangle.parent.getVertex(triangle.indices[1]);
            final Fragment frag = new Mesh.InterpolatedFragment(new Vertex[]{v0,v1}, fragment.baryValues);
            paintFragment(frag, xy, fragArrayIndex);
        }
    }

    private void paintFragment(Fragment frag, Vector2i32 xy, int fragArrayIndex) {

        float[] color;
        if (technique instanceof SimpleBlinnPhong) {

            final Vector3f64 accAmbient = new Vector3f64();
            final Vector3f64 accDiffuse = new Vector3f64();
            final Vector3f64 accSpecular = new Vector3f64();
            blinnphongFrag.setAccAmbient(accAmbient);
            blinnphongFrag.setAccDiffuse(accDiffuse);
            blinnphongFrag.setAccSpecular(accSpecular);
            blinnphongFrag.setAlpha(0f);

            SimpleBlinnPhong tech = (SimpleBlinnPhong) technique;
            tech.process(context, material, frag, blinnphongFrag);
            color = blinnphongFrag.getBaseDiffuse().toRGBA();
            color[3] = blinnphongFrag.getAlpha();

            //compute position
            TupleRW positionModel = frag.getCoordinate();
            TupleRW positionView = MV.transform(positionModel,null);
            blinnphongFrag.properties().add(PixelFragment.POSITION_MODEL, positionModel);
            blinnphongFrag.properties().add(PixelFragment.POSITION_VIEW, positionView);

            //compute normal
            Tuple fragNormal = (Tuple) frag.properties().getValue(Mesh.ATT_NORMAL);
            if (fragNormal != null) {
                final Vector3f64 normalModel = new Vector3f64(fragNormal);
                normalModel.localNormalize();
                final TupleRW normalView = MV.getRotation().transform(normalModel,null);
                Vectors.castOrWrap(normalView).localNormalize();
                blinnphongFrag.properties().add(PixelFragment.NORMAL_MODEL, normalModel);
                blinnphongFrag.properties().add(PixelFragment.NORMAL_VIEW, normalView);
            }


            //apply lights
            if (globalLightning && tech.isLightEnable()) {
                updateLights();

                final Vector4f64 v4 = new Vector4f64();
                v4.x = positionModel.get(0);
                v4.y = positionModel.get(1);
                v4.z = positionModel.get(2);
                v4.w = 1.0;
                MVP.transform(v4, v4);
                blinnphongFrag.properties().add(PixelFragment.EYE_RAY, v4.getXYZ().localNegate().localNormalize());

                for (int i=0,n=lights.getSize(); i<n; i++) {
                    final Light light = (Light) lights.get(i);

                    double shadowFactor = 1.0;
                    if (lightstates[i].shadowMap != null) {
                        //convert fragment position to light space
                        Vector3f64 fragPosLightSpace = new Vector3f64();
                        M.transform(positionModel, fragPosLightSpace);
                        Similarity lightM = light.getRootToNodeSpace();
                        lightM.transform(fragPosLightSpace, fragPosLightSpace);

                        Vector4f64 fragPosLightProj = new Vector4f64();
                        fragPosLightProj.setXYZ(fragPosLightSpace.x,fragPosLightSpace.y,fragPosLightSpace.z);
                        lightstates[i].camera.getProjectionMatrix().transform(fragPosLightProj, fragPosLightProj);
                        double distToLight = -fragPosLightSpace.z;

                        //calculate shadow map coordinate
                        Vector4f64 shadowCoord = fragPosLightProj;
                        shadowCoord.x /= shadowCoord.w;
                        shadowCoord.y /= shadowCoord.w;
                        shadowCoord.z /= shadowCoord.w;
                        shadowCoord.x += 1.0;
                        shadowCoord.y += 1.0;
                        shadowCoord.x *= 0.5;
                        shadowCoord.y *= 0.5;

                        final Scalarf64 d = new Scalarf64();
                        lightstates[i].shadowMap.getTuple(shadowCoord.getXY(), d);

                        System.out.println(d +"  "+distToLight);
                    }

                    if (light instanceof SpotLight) {
                        final SpotLight dl = (SpotLight) light;
                        final VectorRW lightPose = lightstates[i].position;
                        SimpleBlinnPhong.evaluate(dl, blinnphongFrag, lightPose, V, shadowFactor);

                    } else if (light instanceof PointLight) {
                        final PointLight pl = (PointLight) light;
                        final VectorRW lightPose = lightstates[i].position;
                        SimpleBlinnPhong.evaluate(pl, blinnphongFrag, lightPose, null, shadowFactor);

                    } else if (light instanceof DirectionalLight) {
                        final DirectionalLight dl = (DirectionalLight) light;
                        final VectorRW spotDirection = VectorNf64.create(dl.getWorldSpaceDirection());
//                        V.getRotation().transform(spotDirection.localNegate(), spotDirection);
                        SimpleBlinnPhong.evaluate(dl, blinnphongFrag, null, spotDirection, shadowFactor);

                    } else  if (light instanceof AmbientLight) {
                        final AmbientLight dl = (AmbientLight) light;
                        final Color baseAmbient = blinnphongFrag.getBaseAmbient();
                        final Color baseDiffuse = blinnphongFrag.getBaseDiffuse();
                        accAmbient.x = dl.getDiffuse().getRed() * baseAmbient.getRed() * baseDiffuse.getRed();
                        accAmbient.y = dl.getDiffuse().getGreen() * baseAmbient.getGreen() * baseDiffuse.getGreen();
                        accAmbient.z = dl.getDiffuse().getBlue() * baseAmbient.getBlue() * baseDiffuse.getBlue();
                    }
                }

                color[0] = Maths.clamp((float) (accAmbient.x + accDiffuse.x + accSpecular.x), 0, 1);
                color[1] = Maths.clamp((float) (accAmbient.y + accDiffuse.y + accSpecular.y), 0, 1);
                color[2] = Maths.clamp((float) (accAmbient.z + accDiffuse.z + accSpecular.z), 0, 1);
                color[3] = blinnphongFrag.getAlpha();
            }

            //fill different outputs
            final Iterator layerIte = layerTextures.getPairs().createIterator();
            while (layerIte.hasNext()) {
                final Pair pair = (Pair) layerIte.next();
                final Chars key = (Chars) pair.getValue1();
                Object value = blinnphongFrag.properties().getValue(key);
                if (value instanceof Tuple) {
                    final TupleGrid tg = (TupleGrid) pair.getValue2();
                    tg.setTuple(xy, (Tuple) value);
                }
            }

        } else if (technique instanceof SimplePBR) {
            updateLights();
            final SimplePBR pbr = new SimplePBR();
            final DefaultFragment rf = new DefaultFragment(CoordinateSystems.UNDEFINED_3D);
            pbr.process(context, SimplePBR.view(material), frag, rf);
            Tuple rgb = (Tuple) rf.properties().getValue(SimplePBR.BASECOLOR);
            color = rgb.toFloat();

        } else {
            System.out.println("Unsupported material "+ material);
            color = new float[]{1,0,0,1};
        }

        colorArray[fragArrayIndex] = AlphaBlending.srcOver(
                new ColorRGB(color,false),
                new ColorRGB(colorArray[fragArrayIndex],false)
        ).toARGB();
    }

    private static void buildShadowMap(MonoCamera sceneCamera, Light light, int textureSize, LightState state) {
        final MonoCamera camera = new MonoCamera() {
            @Override
            public SceneNode getParent() {
                return light;
            }
        };

        //disable auto resize, this camera will render in an fbo which has a
        //size different from the gl canvas.
        if (light instanceof SpotLight) {
            camera.setNearPlane(0.1);
            camera.setFarPlane(1000);
            camera.setFieldOfView(((SpotLight) light).getFallOffAngle()*2);
        } else if (light instanceof DirectionalLight) {
            final DirectionalLight dl = (DirectionalLight) light;
            final MonoCamera dll = dl.createFittingCamera(sceneCamera);
            camera.setNearPlane(dll.getNearPlane());
            camera.setFarPlane(dll.getFarPlane());
            camera.setFieldOfView(dll.getFieldOfView());
        } else if (light instanceof ProjectiveLight) {
            final ProjectiveLight dl = (ProjectiveLight) light;
            final MonoCamera dll = dl.createFittingCamera(sceneCamera);
            camera.setNearPlane(dll.getNearPlane());
            camera.setFarPlane(dll.getFarPlane());
            camera.setFieldOfView(dll.getFieldOfView());
        } else {
            //point light, TODO
        }
        camera.setRenderArea(new Rectangle(0, 0, textureSize, textureSize));

        final SoftwarePainter3D painter = new SoftwarePainter3D(new Extent.Long(textureSize, textureSize));
        painter.setGlobalLightning(false);
        painter.setCamera(camera);
        painter.setScene(light.getRoot());
        painter.enableColor = false;
        painter.render();

        final Image depthImage = painter.getDepthImage();
        state.shadowMap = depthImage.getRawModel().asTupleBuffer(depthImage);
        state.camera = camera;
    }

    private static class LightState {
        Light light;
        MonoCamera camera;
        Vector3f64 position;
        TupleSpace shadowMap;
    }

}
