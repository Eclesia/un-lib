
package science.unlicense.engine.software;

import science.unlicense.common.api.Orderable;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.Int8;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.painter2d.AbstractPainter2D;
import science.unlicense.display.api.painter2d.Rasterizer2D;
import science.unlicense.format.ttf.ttf.TTFFontStore;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.geometry.impl.path.FlattenPathIterator;
import science.unlicense.geometry.impl.path.RadialDistanceSimplifyPathIterator;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.impl.process.paint.BlendOperator;
import science.unlicense.math.api.Affine;
import static science.unlicense.math.api.Maths.fract;
import static science.unlicense.math.api.Maths.ifract;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Scalari32;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector2i32;

/**
 * Painter2D rendering on given image.
 * No gpu acceleration is done in this implementation.
 * This type of implementation is often called a Rasterizer.
 *
 * @author Johann Sorel
 */
public class CPUPainter2D extends AbstractPainter2D implements science.unlicense.display.api.painter2d.ImagePainter2D{

    private static final TTFFontStore FONT_STORE = new TTFFontStore();

    private static final Vector2f64 EMPTY = new Vector2f64();

    private Image image;
    private final int width;
    private final int height;

    //variable for rasterizer
    private Rasterizer2D rasterizer;
    private PlanarGeometry currentGeometry;
    private final FlattenPathIterator flattenIterator = new FlattenPathIterator(null, null);
    private final RadialDistanceSimplifyPathIterator decimateIterator = new RadialDistanceSimplifyPathIterator(null, 0d);

    //TODO remove this, used for stroke, shoud rely on the rasterizer
    //serve to delimit the geometries area to reduce iterations over pixels
    private final BBox flagBbox = new BBox(2);
    private final Image flagImage;
    private final TupleGridCursor flagCursor;
    private final Vector2i32 flagCoordinate = new Vector2i32();
    private final double[] decimationResolution = new double[]{1,1};
    private final Vector2f64 segmentStart = new Vector2f64();
    private final Vector2f64 segmentEnd = new Vector2f64();
    //path first coordinate, used when on a close segment
    private final Vector2f64 pathStart = new Vector2f64();

    public CPUPainter2D(final int width, final int height) {
        this(Images.create(new Extent.Long(width, height), Images.IMAGE_TYPE_RGBA));
    }

    public CPUPainter2D(final Image image) {
        if (image == null){
            throw new NullPointerException("Image can not be null.");
        }
        if (image.getExtent().getDimension()!= 2){
            throw new InvalidArgumentException("Image must be 2D.");
        }
        this.image = image;
        this.width = (int) image.getExtent().getL(0);
        this.height = (int) image.getExtent().getL(1);
        //mask is one byte per pixel for alpha
        this.rasterizer = new PlainRasterizer(image.getExtent());

        //TODO remove this
        this.flagImage = Images.createCustomBand(new Extent.Long(width, height), 1, Int8.TYPE);
        this.flagCursor = flagImage.getRawModel().asTupleBuffer(flagImage).cursor();
    }

    @Override
    public FontStore getFontStore() {
        return FONT_STORE;
    }

    public void fill(final PlanarGeometry geom) {
        if (fillPaint == null || geom == null) return;
        this.currentGeometry = geom;

        //reset flag mask;
        flagBbox.set(currentGeometry.getBoundingBox());
        rasterizer.resetBBox(flagBbox);

        if (!transform.isIdentity()){
            currentGeometry = TransformedGeometry.create(currentGeometry, transform);
        }
        rasterizer.rasterize(currentGeometry);
        fillPaint();
    }

    /**
     * Fill image with current paint using bitmask.
     */
    private void fillPaint(){
        final Affine2 matrix = transform;
        Affine2 mt = matrix;
        if (!mt.isIdentity()){
            mt = (Affine2) matrix.invert();
        }
        fillPaint.fill(image, rasterizer.getMask(), flagBbox, mt, alphaBlending);
    }

    public void paint(Image image, Affine transform) {
        final Affine2 matrix = this.transform;
        if (!matrix.isIdentity()){
            transform = transform.multiply(matrix);
        }
        new BlendOperator().execute(this.image, image, alphaBlending, transform);
    }

    public void dispose() {

    }

    public void setSize(Extent.Long size) {
        this.image = Images.create(size,Images.IMAGE_TYPE_RGBA);
        this.rasterizer = new PlainRasterizer(this.image.getExtent());
    }

    public Extent.Long getSize() {
        return image.getExtent().copy();
    }

    public Image getImage() {
        return image;
    }

    ////////////////////////////////////////////////////////////////////////////
    // TODO : REPLACE BY RASTERIZER ////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////


    public void stroke(PlanarGeometry geom) {
        if (brush == null || geom == null || fillPaint == null) return;

        final Affine2 matrix = transform;
        if (!matrix.isIdentity()){
            geom = TransformedGeometry.create(geom, matrix);
        }

        double width = brush.getMaxWidth();

        flagBbox.set(geom.getBoundingBox());
        flagBbox.setRange(0, flagBbox.getMin(0)-width, flagBbox.getMax(0)+width);
        flagBbox.setRange(1, flagBbox.getMin(1)-width, flagBbox.getMax(1)+width);
        resetBitMask();

        //simplify iterator, we only want line segments
        flattenIterator.reset(geom.createPathIterator(), decimationResolution);
        decimateIterator.reset(flattenIterator, decimationResolution[0]);

        if (antialiazing){
            strokeAA();
        } else {
            strokeNoAA(geom);
        }

        fillPaint();
    }

    public void strokeNoAA(PlanarGeometry geom){
        brush.bitMask(geom, decimationResolution, flagImage);
    }

    public void strokeAA(){
        while (decimateIterator.next()){
            final int stepType = decimateIterator.getType();
            draw:
            if (PathIterator.TYPE_MOVE_TO == stepType){
                decimateIterator.getPosition(segmentEnd);
                //Do nothing, just update coordinates
                decimateIterator.getPosition(pathStart);
            } else if (PathIterator.TYPE_LINE_TO == stepType || PathIterator.TYPE_CLOSE == stepType){
                if (PathIterator.TYPE_CLOSE == stepType){
                    //end segment if the first position
                    segmentEnd.set(pathStart);
                } else {
                    decimateIterator.getPosition(segmentEnd);
                }

                // Using Xiaolin Wu line algorithm
                float startX = (float) segmentStart.get(0);
                float startY = (float) segmentStart.get(1);
                float endX   = (float) segmentEnd.get(0);
                float endY   = (float) segmentEnd.get(1);

                final boolean steep = Math.abs(endY - startY) > Math.abs(endX - startX);

                if (steep){
                    float temp = startX;
                    startX = startY;
                    startY = temp;
                    temp = endX;
                    endX = endY;
                    endY = temp;
                }
                if (startX > endX){
                    float temp = startX;
                    startX = endX;
                    endX = temp;
                    temp = startY;
                    startY = endY;
                    endY = temp;
                }

                final float dx = endX - startX;
                final float dy = endY - startY;
                final float gradient = dy / dx;

                // handle first endpoint
                int xend = Math.round(startX);
                float yend = startY + gradient * (xend - startX);
                float xgap = ifract(startX + 0.5f);
                int xpxl1 = xend;   //this will be used in the main loop
                int ypxl1 = (int) yend;
                if (steep){
                    setFlagPixel(ypxl1,   xpxl1, ifract(yend) * xgap);
                    setFlagPixel(ypxl1+1, xpxl1,  fract(yend) * xgap);
                } else {
                    setFlagPixel(xpxl1, ypxl1, ifract(yend) * xgap);
                    setFlagPixel(xpxl1, ypxl1+1,  fract(yend) * xgap);
                 }
                float intery = yend + gradient; // first y-intersection for the main loop

                // handle second endpoint
                xend = Math.round(endX);
                yend = endY + gradient * (xend - endX);
                xgap = fract(endX + 0.5f);
                int xpxl2 = xend; //this will be used in the main loop
                int ypxl2 = (int) yend;
                if (steep){
                    setFlagPixel(ypxl2, xpxl2, ifract(yend) * xgap);
                    setFlagPixel(ypxl2+1, xpxl2,  fract(yend) * xgap);
                } else {
                    setFlagPixel(xpxl2, ypxl2,  ifract(yend) * xgap);
                    setFlagPixel(xpxl2, ypxl2+1, fract(yend) * xgap);
                }

                // main loop
                for (int x=xpxl1 + 1,n=xpxl2-1 ;x<n;x++){
                     if (steep){
                        setFlagPixel((int) intery, x, ifract(intery));
                        setFlagPixel((int) intery+1, x,  fract(intery));
                     } else {
                        setFlagPixel(x, (int) intery,  ifract(intery));
                        setFlagPixel(x, (int) intery+1, fract(intery));
                     }
                    intery += gradient;
                }

            } else {
                throw new RuntimeException("Unexpected step type : "+stepType);
            }
            segmentStart.set(segmentEnd);
        }
    }

    private void resetBitMask(){
        flagImage.getRawModel().asTupleBuffer(flagImage).setTuple(flagBbox, new Scalari32(0));
        flagCoordinate.x = 0;
        flagCoordinate.y = 0;
        segmentStart.set(EMPTY);
        segmentEnd.set(EMPTY);
        pathStart.set(EMPTY);
    }

    private void setFlagPixel(int x, int y, float src){
        if (y<0 || y>=height) return;
        if (x<0 || x>=width) return;

        flagCoordinate.x = x;
        flagCoordinate.y = y;
        flagCursor.moveTo(flagCoordinate);

        float dest = ((int) flagCursor.samples().get(0) & 0xff) / 255f;
        src += dest * (1f - src);
        flagCursor.samples().set(0, (byte) (src*255));
    }


    class Edge implements Orderable{

        //start and end points
        public float sx;
        public float sy;
        public float ex;
        public float ey;

        // equation values : ax + b
        public float a;
        public float b;

        //current work
        public float startx = Float.NaN;
        public float lineRatio = 0f;

        @Override
        public int order(Object other) {
            final Edge o = (Edge) other;

            if (startx == Float.NaN){
                //first sort call : sort by start y
                if (ey < o.ey){
                    return -1;
                } else if (ey > o.ey){
                    return +1;
                }
            } else {
                //scanline calls, sort by start x
                if (startx < o.startx){
                    return -1;
                } else if (startx > o.startx){
                    return +1;
                }
            }

            return 0;
        }

    }

}
