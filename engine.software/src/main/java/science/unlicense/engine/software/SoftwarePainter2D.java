
package science.unlicense.engine.software;

import science.unlicense.common.api.number.Int8;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.painter2d.AbstractPainter2D;
import science.unlicense.display.api.painter2d.ImagePainter2D;
import science.unlicense.format.ttf.ttf.TTFFontStore;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.impl.process.paint.BlendOperator;
import science.unlicense.math.api.Affine;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Scalari32;
import science.unlicense.math.impl.Vector2f64;

/**
 * Draft new software painter.
 *
 * @author Johann Sorel
 */
public class SoftwarePainter2D extends AbstractPainter2D implements ImagePainter2D {

    private static final TTFFontStore FONT_STORE = new TTFFontStore();

    private Image image;
    private Image mask;
    private TupleGrid masksm;
    private final BBox maskBbox = new BBox(2);
    private final byte[] maskSample = new byte[]{(byte) 255};
    private final int[] maskCoordinate = new int[2];

    private final Vector2f64 segmentStart = new Vector2f64();
    private final Vector2f64 segmentEnd = new Vector2f64();
    //path first coordinate, used when on a close segment
    private final Vector2f64 pathStart = new Vector2f64();

    private final Vector2f64 corner = new Vector2f64();
    private final Vector2f64 split = new Vector2f64();
    private Vector2f64 temp;

    public SoftwarePainter2D(Image image) {
        this.image = image;
        this.mask = Images.createCustomBand(image.getExtent(), 1, Int8.TYPE);
        this.masksm = mask.getRawModel().asTupleBuffer(mask);
    }

    @Override
    public FontStore getFontStore() {
        return FONT_STORE;
    }

    @Override
    public void setSize(Extent.Long size) {
        image = Images.create(image, size);
        mask = Images.createCustomBand(image.getExtent(), 1, Int8.TYPE);
        masksm = mask.getRawModel().asTupleBuffer(mask);
    }

    @Override
    public Extent.Long getSize() {
        return image.getExtent();
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public void fill(PlanarGeometry geom) {

        final Affine2 matrix = transform;
        if (!matrix.isIdentity()){
            geom = TransformedGeometry.create(geom, matrix);
        }

        final PathIterator ite = geom.createPathIterator();
        while (ite.next()) {
            final int type = ite.getType();

            if (PathIterator.TYPE_MOVE_TO == type) {
                ite.getPosition(pathStart);
                ite.getPosition(segmentStart);
                ite.getPosition(segmentEnd);
                ite.getPosition(corner);

            } else if (PathIterator.TYPE_LINE_TO == type) {
                segmentStart.set(segmentEnd);
                ite.getPosition(segmentEnd);
                fillTriangle(corner, segmentStart, segmentEnd);

            } else if (PathIterator.TYPE_CUBIC == type) {
                //TODO
                segmentStart.set(segmentEnd);
                ite.getPosition(segmentEnd);
                fillTriangle(corner, segmentStart, segmentEnd);

            } else if (PathIterator.TYPE_QUADRATIC == type) {
                //TODO
                segmentStart.set(segmentEnd);
                ite.getPosition(segmentEnd);
                fillTriangle(corner, segmentStart, segmentEnd);

            } else if (PathIterator.TYPE_ARC == type) {
                //TODO
                segmentStart.set(segmentEnd);
                ite.getPosition(segmentEnd);
                fillTriangle(corner, segmentStart, segmentEnd);

            } else if (PathIterator.TYPE_CLOSE == type) {
                segmentEnd.set(segmentStart);
                segmentStart.set(pathStart);
                fillTriangle(corner, segmentStart, segmentEnd);

            }
        }

        maskBbox.set(geom.getBoundingBox());
        fillPaint();
        //reset mask
        mask.getRawModel().asTupleBuffer(mask).setTuple(maskBbox, new Scalari32(0));
        maskSample[0] = (byte) 255;

    }

    /**
     * Fill image with current paint using bitmask.
     */
    private void fillPaint(){
        final Affine2 matrix = transform;
        Affine2 mt = matrix;
        if (!mt.isIdentity()){
            mt = (Affine2) matrix.invert();
        }
        fillPaint.fill(image, mask, maskBbox, mt, alphaBlending);
    }

    @Override
    public void stroke(PlanarGeometry geom) {

    }

    private void fillTriangle(Vector2f64 v1, Vector2f64 v2, Vector2f64 v3) {

        //sort by Y
        if (v1.y > v2.y) {
            temp = v2;
            v2 = v1;
            v1 = temp;
        }
        if (v2.y > v3.y) {
            temp = v3;
            v3 = v2;
            v2 = temp;
        }
        if (v1.y > v2.y) {
            temp = v2;
            v2 = v1;
            v1 = temp;
        }

        if (v2.y == v3.y) {
            fillUpwardTriangle(v1, v2, v3);
        } else if (v1.y == v2.y) {
            fillDownwardTriangle(v1, v2, v3);
        } else {
            //split in two triangles
            split.x = v1.x + (v2.y-v1.y) / ((v3.y-v1.y)) * (v3.x-v1.x);
            split.y = v2.y;
            fillUpwardTriangle(v1, v2, split);
            fillDownwardTriangle(v2, split, v3);
        }

    }

    private void fillUpwardTriangle(Vector2f64 v1, Vector2f64 v2, Vector2f64 v3) {
        double slope1 = (v2.x - v1.x) / (v2.y - v1.y);
        double slope2 = (v3.x - v1.x) / (v3.y - v1.y);
        if (slope1>slope2) {
            double d = slope1;
            slope1 = slope2;
            slope2 = d;
        }

        double start = v1.x;
        double end = v1.x;

        for (double y = v1.y; y <= v2.y; y++) {
            maskBbox.setRange(0, start, end);
            maskBbox.setRange(1, y, y+1);
            mask.getRawModel().asTupleBuffer(mask).setTuple(maskBbox, new Scalari32(255));
            start += slope1;
            end += slope2;
        }
    }

    private void fillDownwardTriangle(Vector2f64 v1, Vector2f64 v2, Vector2f64 v3) {
        double slope1 = (v3.x - v1.x) / (v3.y - v1.y);
        double slope2 = (v3.x - v2.x) / (v3.y - v2.y);
        if (slope1 < slope2) {
            double d = slope1;
            slope1 = slope2;
            slope2 = d;
        }

        double start = v3.x;
        double end = v3.x;

        for (double y = v3.y; y > v1.y; y--) {
            maskBbox.setRange(0, start, end);
            maskBbox.setRange(1, y, y+1);
            mask.getRawModel().asTupleBuffer(mask).setTuple(maskBbox, new Scalari32(255));
            start -= slope1;
            end -= slope2;
        }
    }

    @Override
    public void paint(Image image, Affine transform) {
        final Affine matrix = transform;
        if (!matrix.isIdentity()) {
            transform = transform.multiply(matrix);
        }
        new BlendOperator().execute(this.image, image, alphaBlending, transform);
    }

    @Override
    public void dispose() {
    }

}
