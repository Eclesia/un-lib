
package science.unlicense.engine.software;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.DefaultInt32Buffer;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.display.api.painter3d.AbstractPainter3D;
import science.unlicense.display.impl.FlattenVisitor;
import science.unlicense.display.impl.light.Light;
import science.unlicense.display.impl.light.Lights;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.operation.IntersectionOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.Vector;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class RayTracer extends AbstractPainter3D {

    private static final ImageModel RM = new InterleavedModel(new UndefinedSystem(4), UInt8.TYPE);
    private static final ImageModel CM = DerivateModel.create(RM, new int[]{1,2,3,0}, null, null, ColorSystem.RGBA_8BITS);

    private final FlattenVisitor flattener = new FlattenVisitor();

    private int[] colorArray;
    private float[] depthArray;
    private boolean useLight = false;

    @Override
    public Image render() {
        final int width = (int) extent.get(0);
        final int height = (int) extent.get(1);
        colorArray = new int[width*height];
        depthArray = new float[width*height];
        Arrays.fill(depthArray, Float.MAX_VALUE);

        final Image image = new DefaultImage(new DefaultInt32Buffer(colorArray), extent, RM, CM);

        flattener.reset();
        flattener.visit(scene, null);
        final GraphicNode[] nodes = (GraphicNode[]) flattener.getCollection().toArray(GraphicNode.class);

        final Light[] lights = (Light[]) Lights.getLights(scene).toArray(Light.class);

        final Similarity V = camera.getRootToNodeSpace();


        int pixelIndex = 0;
        camera.setRenderArea(new Rectangle(extent));
        for (int y=0; y<height; y++) {
            for (int x=0; x<width; x++,pixelIndex++) {
                final Ray primaryRay = camera.calculateRayWorldCS(x, y);

                boolean illuminated = false;

                final RayHit hit = intersect(nodes, primaryRay);

                if (hit == null) continue;

                if (useLight) {
                    for (int i=0;i<lights.length;i++) {
                        Vector lightPos = null; //TODO
                        final Ray shadowRay = null; //TODO ray from fragment in world space toward light
                        final RayHit shadowHit = intersect(nodes, shadowRay);

                        if (shadowHit != null) {
                            illuminated = true;
                            break;
                        }
                    }
                }

                if (!illuminated) {
                    colorArray[pixelIndex] = 255;
                } else {
                    colorArray[pixelIndex] = 0;
                }
            }
        }


        return image;
    }

    private RayHit intersect(GraphicNode[] nodes, Ray ray) {

        final RayHit hit = new RayHit();
        hit.distance = Double.MAX_VALUE;

        final IntersectionOp op = new IntersectionOp(null, null);

        for (int k=0; k<nodes.length; k++) {
            if (nodes[k] instanceof Model) {
                final Model model = (Model) nodes[k];
                final Geometry shape = model.getShape();
                final Similarity rootToNode = model.getNodeToRootSpace();

                //mote efficient to transform the ray then the geometry
                final Ray localRay = transform(ray, rootToNode);

                op.setFirst(shape);
                op.setSecond(shape);

                final Geometry itsc;
                try {
                    itsc = op.execute();
                } catch(OperationException ex) {
                    ex.printStackTrace();
                    continue;
                }

                if (itsc == null) continue;

                Vector fragPosition = null; //TODO

                double distance = DistanceOp.distance(localRay.getPosition(), fragPosition);
                if (distance < hit.distance) {
                    hit.mesh = model;
                    hit.fragPosition = fragPosition;
                    hit.fragNormal = null;
                    hit.distance = distance;
                }
            }
        }

        return (hit.mesh != null) ? hit : null;
    }

    private static Ray transform(Ray ray, Similarity trs) {
        final TupleRW pos = ray.getPosition().copy();
        trs.transform(pos, pos);
        final VectorRW dir = ray.getDirection().extend(0.0);
        trs.toMatrix().transform(dir,dir);
        return new Ray(pos, dir.getXYZ());
    }

    private static class RayHit {
        Model mesh;
        Vector fragPosition;
        Vector fragNormal;
        double distance;
    }

}
