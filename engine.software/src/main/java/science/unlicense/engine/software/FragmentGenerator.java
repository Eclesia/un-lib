
package science.unlicense.engine.software;

import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.math.api.Tuple;
import science.unlicense.model3d.impl.technique.RenderState;

/**
 * The fragment generator acts in a similar way to OpenGL step
 * between vertex and fragment shaders.
 *
 * The purpose of this class is to decompose simple geometries such as points,
 * lines and triangles to fragments. A fragment being is a point aligned on integer
 * coordinates which lay inside the geometry.
 *
 *
 * @author Johann Sorel
 */
public abstract class FragmentGenerator {

    //clipvalues are inclusive
    private int clipMinX = Integer.MIN_VALUE;
    private int clipMaxX = Integer.MAX_VALUE;
    private int clipMinY = Integer.MIN_VALUE;
    private int clipMaxY = Integer.MAX_VALUE;
    //distance clip
    private double nearZ = 0.0;
    private double farZ = Double.POSITIVE_INFINITY;

    // 1 : point
    // 2 : line
    // 3 : triangle
    private int mode = 0;
    private float ov1x;
    private float ov1y;
    private float ov1z;
    private float ov2x;
    private float ov2y;
    private float ov2z;
    private float ov3x;
    private float ov3y;
    private float ov3z;

    private float v1x;
    private float v1y;
    private float v2x;
    private float v2y;
    private float v3x;
    private float v3y;
    private float splitx;
    private float splity;

    //fragments line
    private final Row line = new Row();

    //fragment
    private final Fragment fragment = new Fragment();

    private RenderState renderState = new RenderState();

    public void setRenderState(RenderState renderState) {
        this.renderState = renderState;
    }

    public RenderState getRenderState() {
        return renderState;
    }

    /**
     * Set generator clip.
     * No fragment outside the clip will be generated.
     *
     * @param minx
     * @param maxx
     * @param miny
     * @param maxy
     */
    public void setClip(int minx, int maxx, int miny, int maxy) {
        clipMinX = minx;
        clipMaxX = maxx;
        clipMinY = miny;
        clipMaxY = maxy;
    }

    /**
     * Set near/far plane clipping.
     * This information is often stored by the camera.
     *
     * @param near near plane cliping
     * @param far far plane cliping
     */
    public void setZClip(double near, double far) {
        this.nearZ = near;
        this.farZ = far;
    }

    public void processPoint(Tuple v1) {
        mode = 1;
        fragment.baryValues = new double[]{1};
        process(v1);
    }

    public void processLine(Tuple v1, Tuple v2) {
        mode = 2;
        fragment.baryValues = new double[]{1,0};
        process(v1, v2);
    }

    public void processTriangle(Tuple v1, Tuple v2, Tuple v3) {
        mode = 3;
        fragment.baryValues = new double[3];
        process(v1, v2, v3);
    }

    private void process(Tuple v1) {
        fragment.x = (int) v1.get(0);
        fragment.y = (int) v1.get(1);
        fragment.depth = v1.get(2);
        if (fragment.x < clipMinX || fragment.x > clipMaxX || fragment.y < clipMinY || fragment.y > clipMaxY || fragment.depth < nearZ || fragment.depth > farZ) return;
        pushFragment(fragment);
    }

    private void process(Tuple v1, Tuple v2) {
        ov1x = (float) v1.get(0);
        ov1y = (float) v1.get(1);
        ov1z = (float) v1.get(2);
        ov2x = (float) v2.get(0);
        ov2y = (float) v2.get(1);
        ov2z = (float) v2.get(2);
        v1x = ov1x;
        v1y = ov1y;
        v2x = ov2x;
        v2y = ov2y;

        float minx;
        float maxx;
        float miny;
        float maxy;
        if (v1y < v2y) {
            miny = v1y;
            maxy = v2y;
        } else {
            miny = v2y;
            maxy = v1y;
        }
        if (v1x < v2x) {
            minx = v1x;
            maxx = v2x;
        } else {
            minx = v2x;
            maxx = v1x;
        }

        if (miny == maxy) {
            //horizontal line
            for (float x=minx; x<maxx; x++) {
                fragment.x = (int) x;
                fragment.y = (int) miny;
                if (fragment.x < clipMinX || fragment.x > clipMaxX || fragment.y < clipMinY || fragment.y > clipMaxY) continue;
                computeDepthAndBary();
                pushFragment(fragment);
            }
        } else if (minx == maxx) {
            //vertical line
            for (float y=miny; y<maxy; y++) {
                fragment.x = (int) minx;
                fragment.y = (int) y;
                if (fragment.x < clipMinX || fragment.x > clipMaxX || fragment.y < clipMinY || fragment.y > clipMaxY) continue;
                computeDepthAndBary();
                pushFragment(fragment);
            }
        } else {
            //diagonale line
            final float dx = v2x-v1x;
            final float dy = v2y-v1y;

            if (dx >= dy) {
                final float a = dy / dx;
                float y = v1y;
                for (float x=v1x; x<=v2x; x++,y+=a) {
                    fragment.x = (int) x;
                    fragment.y = (int) y;
                    if (fragment.x < clipMinX || fragment.x > clipMaxX || fragment.y < clipMinY || fragment.y > clipMaxY) continue;
                    //fragment.baryValues[0] = (x-v1x) / dx;
                    //fragment.baryValues[1] = 1.0 - fragment.baryValues[0];
                    computeDepthAndBary();
                    pushFragment(fragment);
                }
            } else {
                final float a = dx / dy;
                float x = v1x;
                for (float y=v1y; y<=v2y; y++,x+=a) {
                    fragment.x = (int) x;
                    fragment.y = (int) y;
                    if (fragment.x < clipMinX || fragment.x > clipMaxX || fragment.y < clipMinY || fragment.y > clipMaxY) continue;
                    //fragment.baryValues[0] = (y-v1y) / dy;
                    //fragment.baryValues[1] = 1.0 - fragment.baryValues[0];
                    computeDepthAndBary();
                    pushFragment(fragment);
                }
            }
        }
    }

    private void process(Tuple v1, Tuple v2, Tuple v3) {

        // test global triangle depth clipping
        ov1z = (float) v1.get(2);
        ov2z = (float) v2.get(2);
        ov3z = (float) v3.get(2);
        if ( (ov1z < nearZ && ov2z < nearZ && ov3z < nearZ) ||
             (ov1z > farZ && ov2z > farZ && ov3z > farZ) ){
            return;
        }

        // test culling
        switch (renderState.getCulling()) {
            case RenderState.CULLING_BACK :
                if (Geometries.isCounterClockwise(v1, v2, v3)) return;
                break;
            case RenderState.CULLING_FRONT :
                if (!Geometries.isCounterClockwise(v1, v2, v3)) return;
                break;
            case RenderState.CULLING_BOTH :
                return;
        }

        //apply polygon mode
        switch (renderState.getPolygonMode()) {
            case RenderState.MODE_LINE :
                process(v1, v2);
                process(v2, v3);
                process(v3, v1);
                return;
            case RenderState.MODE_POINT:
                process(v1);
                process(v2);
                process(v3);
                return;
        }

        //fill polygon mode
        ov1x = (float) v1.get(0);
        ov1y = (float) v1.get(1);
        ov2x = (float) v2.get(0);
        ov2y = (float) v2.get(1);
        ov3x = (float) v3.get(0);
        ov3y = (float) v3.get(1);

        //sort by Y
        v1x = ov1x;
        v1y = ov1y;
        v2x = ov2x;
        v2y = ov2y;
        v3x = ov3x;
        v3y = ov3y;
        float tx, ty;
        if (v1y > v2y) {
            tx  = v2x; ty  = v2y;
            v2x = v1x; v2y = v1y;
            v1x = tx;  v1y = ty;
        }
        if (v2y > v3y) {
            tx  = v3x; ty  = v3y;
            v3x = v2x; v3y = v2y;
            v2x = tx;  v2y = ty;
        }
        if (v1y > v2y) {
            tx  = v2x; ty  = v2y;
            v2x = v1x; v2y = v1y;
            v1x = tx;  v1y = ty;
        }

        if (v2y == v3y) {
            fillUpwardTriangle();
        } else if (v1y == v2y) {
            fillDownwardTriangle();
        } else {
            float t2x = v2x;
            float t2y = v2y;
            float t3x = v3x;
            float t3y = v3y;
            //split in two triangles
            splitx = v1x + (v2y-v1y) / ((v3y-v1y)) * (v3x-v1x);
            splity = v2y;
            //v1,v2,split
            v3x = splitx;
            v3y = splity;
            fillUpwardTriangle();
            //v2,split,v3
            v1x = t2x;
            v1y = t2y;
            v2x = splitx;
            v2y = splity;
            v3x = t3x;
            v3y = t3y;
            fillDownwardTriangle();
        }

    }

    private void fillUpwardTriangle() {
        float slope1 = (v2x - v1x) / (v2y - v1y);
        float slope2 = (v3x - v1x) / (v3y - v1y);
        if (slope1 > slope2) {
            float d = slope1;
            slope1 = slope2;
            slope2 = d;
        }

        float start = v1x;
        float end = v1x;

        //move to first pixel center
        float y = v1y;
        float correction = 0.0f;
        float deci = v1y - ((int) v1y);
        if (deci < 0.5f) {
            correction = 0.5f + deci;
        } else if (deci > 0.5f) {
            correction = deci - 0.5f;
        }
        y -= correction;
        start -= slope1 * correction;
        end -= slope2 * correction;

        //clip lines
//        float endy = Maths.clamp(t2y, clipMinY+0.5f, clipMaxY+0.5f);
        float endy = v2y;

        for (; y <= endy; y++) {
            pushLine(y, start, end);
            start += slope1;
            end += slope2;
        }

    }

    private void fillDownwardTriangle() {

        float slope1 = (v3x - v1x) / (v3y - v1y);
        float slope2 = (v3x - v2x) / (v3y - v2y);
        if (slope1 < slope2) {
            float d = slope1;
            slope1 = slope2;
            slope2 = d;
        }

        float start = v3x;
        float end = v3x;

        //move to first pixel center
        float y = v3y;
        float correction = 0.0f;
        float deci = v3y - ((int) v3y);
        if (deci < 0.5f) {
            correction = 0.5f + deci;
        } else if (deci > 0.5f) {
            correction = deci - 0.5f;
        }
        y -= correction;
        start -= slope1 * correction;
        end -= slope2 * correction;

        //clip lines
//        float endy = Maths.clamp(t1y, clipMinY+0.5f, clipMaxY+0.5f);
        float endy = v1y;

        //loop on each line
        for (; y > endy; y--) {
            pushLine(y, start, end);
            start -= slope1;
            end -= slope2;
        }
    }

    private void pushLine(float y, float startx, float endx) {
        //apply clip
        //TODO do this sooner
        line.y = (int) y;
        if (line.y < clipMinY || line.y > clipMaxY) return;

        line.startx = (int) (startx+0.5);
        if (line.startx > clipMaxX) return;


        line.endx = (int) (endx+0.5);
        if (line.endx < clipMinX) return;

        if (line.startx < clipMinX) line.startx = clipMinX;
        if (line.endx > clipMaxX) line.endx = clipMaxX+1;

        pushRow(line);
    }

    protected void pushRow(Row line) {

        fragment.x = line.startx;
        fragment.y = line.y;
        for (; fragment.x < line.endx; fragment.x++) {
            computeDepthAndBary();
            pushFragment(fragment);
        }
    }

    private void computeDepthAndBary() {
        switch (mode) {
            case 1 :
                fragment.baryValues = new double[]{1};
                fragment.depth = ov1z;
                break;
            case 2 :
                //TODO
                fragment.baryValues = new double[]{2};
                fragment.depth = ov1z * fragment.baryValues[0]
                               + ov2z * fragment.baryValues[1];
                break;
            case 3 :
                fragment.baryValues = Triangle.getBarycentricValue2D(
                    ov1x, ov1y,
                    ov2x, ov2y,
                    ov3x, ov3y,
                    fragment.x+0.5, fragment.y+0.5);
                fragment.depth = ov1z * fragment.baryValues[0]
                               + ov2z * fragment.baryValues[1]
                               + ov3z * fragment.baryValues[2];
                break;
        }
    }

    protected abstract void pushFragment(Fragment fragment);

    public static class Row {
        /** inclusive */
        public int startx;
        /** exclusive */
        public int endx;
        public int y;
    }

    public static class Fragment {
        public int x;
        public int y;
        public double depth;
        public double[] baryValues;
    }

}
