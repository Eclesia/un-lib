package science.unlicense.code.c;

import science.unlicense.encoding.api.io.ByteInputStream;

/**
 * C parser.
 *
 * @author Johann Sorel
 */
public class CParser {

    private ByteInputStream stream;

    public CParser() {}

    public void setInput(ByteInputStream input) {
        this.stream = input;
    }

}
