package science.unlicense.code.c;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;

/**
 * C syntax model.
 *
 * @author Johann Sorel
 */
public final class CMetaModel {

    public static final int[] BLANKS       = new int[]{9,32}; // tab and space
    public static final byte NEWLINE        = 10;    // jump line
    public static final byte SPACE          = ' ';    //
    public static final byte COMMENT        = 42;    // / slash
    public static final byte ADR            = 38;    // & get address
    public static final byte COMMA          = 44;    // ,
    public static final byte COMDOT         = 59;    // ;
    public static final byte PRE            = 35;    // #
    public static final byte LAC            = 123;   // {
    public static final byte RAC            = 125;   // }
    public static final byte LPA            = 40;    // (
    public static final byte RPA            = 41;    // )
    public static final byte LBR            = 91;    // [
    public static final byte RBR            = 93;    // ]
    public static final byte EQU            = 61;    // =
    public static final byte QUE            = 63;    // ?
    public static final byte EXP            = 33;    // !
    public static final byte DDOT           = 58;    // :
    public static final byte SUP            = 60;    // <
    public static final byte INF            = 62;    // >
    public static final byte OR             = 124;   // |
    public static final byte QUO            = 39;    // '
    public static final byte GUI            = 34;    // "

    public static final byte ADD            = 43;    // +
    public static final byte SUB            = 45;    // -
    public static final byte DIV            = 47;    // /
    public static final byte MUL            = 42;    // *
    public static final byte MOD            = 37;    // %

    public static final Chars KW_TYPEDEF     = Chars.constant("typedef", CharEncodings.US_ASCII);
    public static final Chars KW_STRUCT      = Chars.constant("struct", CharEncodings.US_ASCII);
    public static final Chars KW_EXTERN      = Chars.constant("extern", CharEncodings.US_ASCII);
    public static final Chars KW_CONST       = Chars.constant("const", CharEncodings.US_ASCII);
    public static final Chars KW_ENUM        = Chars.constant("enum", CharEncodings.US_ASCII);
    public static final Chars KW_STATIC      = Chars.constant("static", CharEncodings.US_ASCII);
    public static final Chars KW_DEFINE      = Chars.constant("define", CharEncodings.US_ASCII);

    public static final Chars KW_VOID        = Chars.constant("void", CharEncodings.US_ASCII);
    public static final Chars KW_CHAR        = Chars.constant("char", CharEncodings.US_ASCII);
    public static final Chars KW_SHORT       = Chars.constant("short", CharEncodings.US_ASCII);
    public static final Chars KW_INT         = Chars.constant("int", CharEncodings.US_ASCII);
    public static final Chars KW_FLOAT       = Chars.constant("float", CharEncodings.US_ASCII);
    public static final Chars KW_DOUBLE      = Chars.constant("double", CharEncodings.US_ASCII);
    public static final Chars KW_LONG        = Chars.constant("long", CharEncodings.US_ASCII);
    public static final Chars KW_UNSIGNED    = Chars.constant("unsigned", CharEncodings.US_ASCII);

    private CMetaModel() {}

}
