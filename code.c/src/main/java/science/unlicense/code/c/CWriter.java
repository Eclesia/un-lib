
package science.unlicense.code.c;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.code.c.CMetaModel.DIV;
import static science.unlicense.code.c.CMetaModel.KW_DEFINE;
import static science.unlicense.code.c.CMetaModel.MUL;
import static science.unlicense.code.c.CMetaModel.NEWLINE;
import static science.unlicense.code.c.CMetaModel.PRE;
import static science.unlicense.code.c.CMetaModel.SPACE;

/**
 * Draft C writer.
 *
 * @author Johann Sorel
 */
public class CWriter extends AbstractWriter {

    private CharOutputStream cs;

    private CharOutputStream getCharStream() throws IOException{
        if (cs==null) {
            cs = getOutputAsCharStream(CharEncodings.US_ASCII);
        }
        return cs;
    }

    public void writeComment(Chars comment) throws IOException{
        getCharStream();
        cs.write(DIV);
        cs.write(MUL);
        cs.write(SPACE);
        cs.write(comment);
        cs.write(SPACE);
        cs.write(MUL);
        cs.write(DIV);
        cs.write(NEWLINE);
    }

    public void writeDefine(Chars variable, Chars value) throws IOException{
        getCharStream();
        cs.write(PRE);
        cs.write(KW_DEFINE);
        cs.write(SPACE);
        cs.write(variable);
        cs.write(SPACE);
        cs.write(value);
        cs.write(NEWLINE);
    }

}
