
package science.unlicense.code.c;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.parser.Parser;
import science.unlicense.syntax.api.parser.Rule;
import science.unlicense.syntax.api.parser.SyntaxNode;
import science.unlicense.syntax.impl.grammar.io.UNGrammarReader;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class CPreprocessReader extends AbstractReader{


    private static final OrderedHashDictionary TOKENS = new OrderedHashDictionary();
    private static final OrderedHashDictionary RULES = new OrderedHashDictionary();
    private static final Rule RULE_FILE;
    static {
        try{
            //parse grammar
            final UNGrammarReader reader = new UNGrammarReader();
            reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/impl/code/c/c-preprocessor.gr")));
            reader.read(TOKENS, RULES);
            RULE_FILE = (Rule) RULES.getValue(new Chars("file"));


        }catch(IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public SyntaxNode read() throws IOException{
        final Parser parser = new Parser(TOKENS,RULE_FILE,null);
        parser.setInput(getInput());
        final SyntaxNode sn = parser.parse();
        parser.dispose();
        return sn;
    }

}
