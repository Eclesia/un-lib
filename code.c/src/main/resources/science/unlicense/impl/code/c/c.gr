
# C .c/.h text grammar
# author : Johann Sorel

# PREPROCESS token part
$D : [0-9] ;
$L : ([a-z]|[A-Z]|[_$]) ;
$S : (-|\+)? ;

# TOKENS used in lexer
DEFAULT : {
    SEMICOLON       : ; ;
    DOT             : \. ;

    MULTIPLY        : \* ;
    DIVIDE          : \/ ;
    ADD             : \+ ;
    SUBTRACT        : \- ;

    EQUAL1          : = ;
    EQUAL2          : == ;
    GREATER         : > ;
    GREATER_EQUAL   : >= ;
    INFERIOR        : < ;
    INFERIOR_EQUAL  : <= ;

    OR1             : \| ;
    OR2             : \|\| ;
    AND1            : & ;
    AND2            : && ;

    LSHIFT2         : << ;
    RSHIFT2         : >> ;
    RSHIFT3         : >>> ;

    LPAREN          : \( ;
    RPAREN          : \) ;
    LBRACE          : { ;
    RBRACE          : } ;
    LBRACKET        : \[ ;
    RBRACKET        : \] ;

    SPE_AUTO        : auto ;
    SPE_REGISTER    : register ;
    SPE_STATIC      : static ;
    SPE_EXTERN      : extern ;

    PRI_BOOL        : _Bool ;
    PRI_CHAR        : char ;
    PRI_SCHAR       : signed char ;
    PRI_UCHAR       : unsigned char ;
    PRI_SHORT       : short ;
    PRI_USHORT      : unsigned short ;
    PRI_INT         : int ;
    PRI_UINT        : unsigned int ;
    PRI_LONG        : long ;
    PRI_ULONG       : unsigned long ;
    PRI_LONGLONG    : long long ;
    PRI_ULONGLONG   : unsigned long long ;
    PRI_FLOAT       : float ;
    PRI_DOUBLE      : double ;
    PRI_LDOUBLE     : long double ;

    TYPEDEF         : typedef ;
    STRUCT          : struct ;
    ENUM            : enum ;
    UNION           : union ;



    WS              : ( |\t|\n|\r)+ ;
    COMMENT1        : //[^\n]* ;
    COMMENT2        : '/*'([^\*]|(\*[^/]))*'*/' ;
    WORD            : $L($L|$D)* ;
    NUMBER          : $S$D+(\.$D+)?((e|E)$S$D+)? ;
    STRING          : \"[^\"]*\" ;
}

# RULES used in parser

var             : (MULTIPLY)? WORD ;
primtype        : (PRI_BOOL | PRI_CHAR | PRI_SCHAR | PRI_UCHAR | PRI_SHORT | PRI_USHORT | PRI_INT | PRI_UINT | PRI_LONG | PRI_ULONG | PRI_LONGLONG | PRI_ULONGLONG | PRI_FLOAT | PRI_DOUBLE | PRI_LDOUBLE ) ;
vardef          : (primtype|WORD) WS var WS? SEMICOLON ;
struct          : TYPEDEF WS STRUCT (WS? LBRACE RBRACE)? WS? WORD WS? SEMICOLON ;
file            : (WS|COMMENT1|COMMENT2|struct)* ;