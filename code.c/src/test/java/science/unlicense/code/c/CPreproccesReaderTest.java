
package science.unlicense.code.c;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.parser.SyntaxNode;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class CPreproccesReaderTest {

    @Test
    public void testReadInclude() throws IOException{

        final CPreprocessReader reader = new CPreprocessReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/c/preproc_include.h")));
        SyntaxNode read = reader.read();

        System.out.println(read);
    }

    @Test
    public void testReadDefine() throws IOException{

        final CPreprocessReader reader = new CPreprocessReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/c/preproc_define.h")));
        SyntaxNode read = reader.read();

        System.out.println(read);
    }

    @Test
    public void testReadError() throws IOException{

        final CPreprocessReader reader = new CPreprocessReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/c/preproc_error.h")));
        SyntaxNode read = reader.read();

        System.out.println(read);
    }

    @Test
    public void testReadIfElse() throws IOException{

        final CPreprocessReader reader = new CPreprocessReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/c/preproc_ifelse.h")));
        SyntaxNode read = reader.read();
        System.out.println(Nodes.toCharsTree(read, Chars.EMPTY, 10));
    }

}
