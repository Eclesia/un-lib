
package science.unlicense.format.ole;

/**
 * OLE Constants
 * @author Johann Sorel
 */
public final class OLEConstants {

    public static final byte[] SIGNATURE_NEW = new byte[]{(byte) 0xD0,(byte) 0xCF,(byte) 0x11,(byte) 0xE0,(byte) 0xA1,(byte) 0xB1,(byte) 0x1A,(byte) 0xE1};
    public static final byte[] SIGNATURE_OLD = new byte[]{(byte) 0x0E,(byte) 0x11,(byte) 0xFC,(byte) 0x0D,(byte) 0xD0,(byte) 0xCF,(byte) 0x11,(byte) 0x0E};

    public static final int BIG_ENDIAN = 0xFFFE;
    public static final int LITTLE_ENDIAN = 0xFEFF;

    /** Free sector, may exist in the file, but is not part of any stream */
    public static final int SECID_FREE = -1;
    /** Trailing SecID in a SecID chain */
    public static final int SECID_ENDOFCHAIN = -2;
    /** Sector is used by the sector allocation table */
    public static final int SECID_SAT = -3;
    /** Sector is used by the master sector allocation table */
    public static final int SECID_MSAT = -4;



    private OLEConstants() {}

}
