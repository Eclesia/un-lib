

package science.unlicense.format.ole;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.format.ole.model.Header;

/**
 * Resource :
 * http://hdl.loc.gov/loc.gdc/digformat.000006.1
 * http://msdn.microsoft.com/en-us/library/dd942138.aspx
 * http://www.openoffice.org/sc/compdocfileformat.pdf
 * https://googledrive.com/host/0B3fBvzttpiiSS0hEb0pjU2h6a2c/OLE%20Compound%20File%20format.pdf
 *
 * @author Johann Sorel
 */
public class OLEReader extends AbstractReader{

    public void read() throws IOException{

        final BacktrackInputStream bs = getInputAsBacktrackStream();
        bs.mark();
        final DataInputStream ds = new DataInputStream(bs, Endianness.LITTLE_ENDIAN);

        final byte[] sign = ds.readFully(new byte[8]);
        if (!Arrays.equals(OLEConstants.SIGNATURE_NEW, sign) && !Arrays.equals(OLEConstants.SIGNATURE_OLD, sign)) {
            throw new IOException(ds, "File is not a valid OLE stream.");
        }

        final Header header = new Header();
        header.read(ds);

        //loop on all sectors
        final int[] sectors = header.readAllSectorId(bs);
        for (int i=0;i<sectors.length;i++) {
            System.out.println(sectors[i]);
        }

    }

}
