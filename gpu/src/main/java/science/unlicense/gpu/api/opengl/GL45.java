package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;

public interface GL45 extends science.unlicense.gpu.api.opengl.GL44 {

    /**
     * @param origin
     * @param depth
     */
     void glClipControl (int origin, int depth);

    /**
     * @param n
     * @param ids
     */
     void glCreateTransformFeedbacks (int n, Buffer.Int32 ids);

    /**
     * @param xfb
     * @param index
     * @param buffer
     */
     void glTransformFeedbackBufferBase (int xfb, int index, int buffer);

    /**
     * @param xfb
     * @param index
     * @param buffer
     * @param offset
     * @param size ,value from enumeration group BufferSize
     */
     void glTransformFeedbackBufferRange (int xfb, int index, int buffer, long offset, long size);

    /**
     * @param xfb
     * @param pname
     * @param param
     */
     void glGetTransformFeedbackiv (int xfb, int pname, Buffer.Int32 param);

    /**
     * @param xfb
     * @param pname
     * @param index
     * @param param
     */
     void glGetTransformFeedbacki_v (int xfb, int pname, int index, Buffer.Int32 param);

    /**
     * @param xfb
     * @param pname
     * @param index
     * @param param
     */
     void glGetTransformFeedbacki64_v (int xfb, int pname, int index, Buffer.Int8 param);

    /**
     * @param n
     * @param buffers
     */
     void glCreateBuffers (int n, Buffer.Int32 buffers);

    /**
     * @param buffer
     * @param size ,value from enumeration group BufferSize
     * @param data ,length size
     * @param flags
     */
     void glNamedBufferStorage (int buffer, Buffer.Int8 data, int flags);

    /**
     * @param buffer
     * @param size ,value from enumeration group BufferSize
     * @param data
     * @param usage
     */
     void glNamedBufferData (int buffer, long size, Buffer.Int8 data, int usage);

    /**
     * @param buffer
     * @param offset
     * @param size ,value from enumeration group BufferSize
     * @param data
     */
     void glNamedBufferSubData (int buffer, long offset, long size, Buffer.Int8 data);

    /**
     * @param readBuffer
     * @param writeBuffer
     * @param readOffset
     * @param writeOffset
     * @param size ,value from enumeration group BufferSize
     */
     void glCopyNamedBufferSubData (int readBuffer, int writeBuffer, long readOffset, long writeOffset, long size);

    /**
     * @param buffer
     * @param internalformat
     * @param format
     * @param type
     * @param data
     */
     void glClearNamedBufferData (int buffer, int internalformat, int format, int type, Buffer.Int8 data);

    /**
     * @param buffer
     * @param internalformat
     * @param offset
     * @param size ,value from enumeration group BufferSize
     * @param format
     * @param type
     * @param data
     */
     void glClearNamedBufferSubData (int buffer, int internalformat, long offset, long size, int format, int type, long data);

    /**
     * @param buffer
     * @param access
     */
     void glMapNamedBuffer (int buffer, int access);

    /**
     * @param buffer
     * @param offset
     * @param length ,value from enumeration group BufferSize
     * @param access
     */
     void glMapNamedBufferRange (int buffer, long offset, long length, int access);

    /**
     * @param buffer
     */
     boolean glUnmapNamedBuffer (int buffer);

    /**
     * @param buffer
     * @param offset
     * @param length ,value from enumeration group BufferSize
     */
     void glFlushMappedNamedBufferRange (int buffer, long offset, long length);

    /**
     * @param buffer
     * @param pname
     * @param params
     */
     void glGetNamedBufferParameteriv (int buffer, int pname, Buffer.Int32 params);

    /**
     * @param buffer
     * @param pname
     * @param params
     */
     void glGetNamedBufferParameteri64v (int buffer, int pname, Buffer.Int8 params);

    /**
     * @param buffer
     * @param pname
     * @param params
     */
     void glGetNamedBufferPointerv (int buffer, int pname, Buffer.Int8 params);

    /**
     * @param buffer
     * @param offset
     * @param size ,value from enumeration group BufferSize
     * @param data
     */
     void glGetNamedBufferSubData (int buffer, long offset, long size, Buffer.Int8 data);

    /**
     * @param n
     * @param framebuffers
     */
     void glCreateFramebuffers (int n, Buffer.Int32 framebuffers);

    /**
     * @param framebuffer
     * @param attachment
     * @param renderbuffertarget
     * @param renderbuffer
     */
     void glNamedFramebufferRenderbuffer (int framebuffer, int attachment, int renderbuffertarget, int renderbuffer);

    /**
     * @param framebuffer
     * @param pname
     * @param param
     */
     void glNamedFramebufferParameteri (int framebuffer, int pname, int param);

    /**
     * @param framebuffer
     * @param attachment
     * @param texture
     * @param level
     */
     void glNamedFramebufferTexture (int framebuffer, int attachment, int texture, int level);

    /**
     * @param framebuffer
     * @param attachment
     * @param texture
     * @param level
     * @param layer
     */
     void glNamedFramebufferTextureLayer (int framebuffer, int attachment, int texture, int level, int layer);

    /**
     * @param framebuffer
     * @param buf
     */
     void glNamedFramebufferDrawBuffer (int framebuffer, int buf);

    /**
     * @param framebuffer
     * @param n
     * @param bufs
     */
     void glNamedFramebufferDrawBuffers (int framebuffer, int n, Buffer.Int32 bufs);

    /**
     * @param framebuffer
     * @param src
     */
     void glNamedFramebufferReadBuffer (int framebuffer, int src);

    /**
     * @param framebuffer
     * @param numAttachments
     * @param attachments
     */
     void glInvalidateNamedFramebufferData (int framebuffer, int numAttachments, Buffer.Int32 attachments);

    /**
     * @param framebuffer
     * @param numAttachments
     * @param attachments
     * @param x
     * @param y
     * @param width
     * @param height
     */
     void glInvalidateNamedFramebufferSubData (int framebuffer, int numAttachments, Buffer.Int32 attachments, int x, int y, int width, int height);

    /**
     * @param framebuffer
     * @param buffer
     * @param drawbuffer
     * @param value
     */
     void glClearNamedFramebufferiv (int framebuffer, int buffer, int drawbuffer, Buffer.Int32 value);

    /**
     * @param framebuffer
     * @param buffer
     * @param drawbuffer
     * @param value
     */
     void glClearNamedFramebufferuiv (int framebuffer, int buffer, int drawbuffer, Buffer.Int32 value);

    /**
     * @param framebuffer
     * @param buffer
     * @param drawbuffer
     * @param value
     */
     void glClearNamedFramebufferfv (int framebuffer, int buffer, int drawbuffer, Buffer.Float32 value);

    /**
     * @param framebuffer
     * @param buffer
     * @param drawbuffer
     * @param depth
     * @param stencil
     */
     void glClearNamedFramebufferfi (int framebuffer, int buffer, int drawbuffer, float depth, int stencil);

    /**
     * @param readFramebuffer
     * @param drawFramebuffer
     * @param srcX0
     * @param srcY0
     * @param srcX1
     * @param srcY1
     * @param dstX0
     * @param dstY0
     * @param dstX1
     * @param dstY1
     * @param mask
     * @param filter
     */
     void glBlitNamedFramebuffer (int readFramebuffer, int drawFramebuffer, int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, int mask, int filter);

    /**
     * @param framebuffer
     * @param target
     */
     int glCheckNamedFramebufferStatus (int framebuffer, int target);

    /**
     * @param framebuffer
     * @param pname
     * @param param
     */
     void glGetNamedFramebufferParameteriv (int framebuffer, int pname, Buffer.Int32 param);

    /**
     * @param framebuffer
     * @param attachment
     * @param pname
     * @param params
     */
     void glGetNamedFramebufferAttachmentParameteriv (int framebuffer, int attachment, int pname, Buffer.Int32 params);

    /**
     * @param n
     * @param renderbuffers
     */
     void glCreateRenderbuffers (int n, Buffer.Int32 renderbuffers);

    /**
     * @param renderbuffer
     * @param internalformat
     * @param width
     * @param height
     */
     void glNamedRenderbufferStorage (int renderbuffer, int internalformat, int width, int height);

    /**
     * @param renderbuffer
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     */
     void glNamedRenderbufferStorageMultisample (int renderbuffer, int samples, int internalformat, int width, int height);

    /**
     * @param renderbuffer
     * @param pname
     * @param params
     */
     void glGetNamedRenderbufferParameteriv (int renderbuffer, int pname, Buffer.Int32 params);

    /**
     * @param target
     * @param n
     * @param textures
     */
     void glCreateTextures (int target, int n, Buffer.Int32 textures);

    /**
     * @param texture
     * @param internalformat
     * @param buffer
     */
     void glTextureBuffer (int texture, int internalformat, int buffer);

    /**
     * @param texture
     * @param internalformat
     * @param buffer
     * @param offset
     * @param size ,value from enumeration group BufferSize
     */
     void glTextureBufferRange (int texture, int internalformat, int buffer, long offset, long size);

    /**
     * @param texture
     * @param levels
     * @param internalformat
     * @param width
     */
     void glTextureStorage1D (int texture, int levels, int internalformat, int width);

    /**
     * @param texture
     * @param levels
     * @param internalformat
     * @param width
     * @param height
     */
     void glTextureStorage2D (int texture, int levels, int internalformat, int width, int height);

    /**
     * @param texture
     * @param levels
     * @param internalformat
     * @param width
     * @param height
     * @param depth
     */
     void glTextureStorage3D (int texture, int levels, int internalformat, int width, int height, int depth);

    /**
     * @param texture
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     * @param fixedsamplelocations
     */
     void glTextureStorage2DMultisample (int texture, int samples, int internalformat, int width, int height, boolean fixedsamplelocations);

    /**
     * @param texture
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     * @param depth
     * @param fixedsamplelocations
     */
     void glTextureStorage3DMultisample (int texture, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param width
     * @param format
     * @param type
     * @param pixels
     */
     void glTextureSubImage1D (int texture, int level, int xoffset, int width, int format, int type, Buffer.Int8 pixels);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param width
     * @param height
     * @param format
     * @param type
     * @param pixels
     */
     void glTextureSubImage2D (int texture, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer.Int8 pixels);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param width
     * @param height
     * @param depth
     * @param format
     * @param type
     * @param pixels
     */
     void glTextureSubImage3D (int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, Buffer.Int8 pixels);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param width
     * @param format
     * @param imageSize
     * @param data
     */
     void glCompressedTextureSubImage1D (int texture, int level, int xoffset, int width, int format, int imageSize, Buffer.Int8 data);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param width
     * @param height
     * @param format
     * @param imageSize
     * @param data
     */
     void glCompressedTextureSubImage2D (int texture, int level, int xoffset, int yoffset, int width, int height, int format, int imageSize, Buffer.Int8 data);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param width
     * @param height
     * @param depth
     * @param format
     * @param imageSize
     * @param data
     */
     void glCompressedTextureSubImage3D (int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int imageSize, Buffer.Int8 data);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param x
     * @param y
     * @param width
     */
     void glCopyTextureSubImage1D (int texture, int level, int xoffset, int x, int y, int width);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param x
     * @param y
     * @param width
     * @param height
     */
     void glCopyTextureSubImage2D (int texture, int level, int xoffset, int yoffset, int x, int y, int width, int height);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param x
     * @param y
     * @param width
     * @param height
     */
     void glCopyTextureSubImage3D (int texture, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height);

    /**
     * @param texture
     * @param pname
     * @param param
     */
     void glTextureParameterf (int texture, int pname, float param);

    /**
     * @param texture
     * @param pname
     * @param param
     */
     void glTextureParameterfv (int texture, int pname, Buffer.Float32 param);

    /**
     * @param texture
     * @param pname
     * @param param
     */
     void glTextureParameteri (int texture, int pname, int param);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glTextureParameterIiv (int texture, int pname, Buffer.Int32 params);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glTextureParameterIuiv (int texture, int pname, Buffer.Int32 params);

    /**
     * @param texture
     * @param pname
     * @param param
     */
     void glTextureParameteriv (int texture, int pname, Buffer.Int32 param);

    /**
     * @param texture
     */
     void glGenerateTextureMipmap (int texture);

    /**
     * @param unit
     * @param texture
     */
     void glBindTextureUnit (int unit, int texture);

    /**
     * @param texture
     * @param level
     * @param format
     * @param type
     * @param bufSize
     * @param pixels
     */
     void glGetTextureImage (int texture, int level, int format, int type, int bufSize, Buffer.Int8 pixels);

    /**
     * @param texture
     * @param level
     * @param bufSize
     * @param pixels
     */
     void glGetCompressedTextureImage (int texture, int level, int bufSize, Buffer.Int8 pixels);

    /**
     * @param texture
     * @param level
     * @param pname
     * @param params
     */
     void glGetTextureLevelParameterfv (int texture, int level, int pname, Buffer.Float32 params);

    /**
     * @param texture
     * @param level
     * @param pname
     * @param params
     */
     void glGetTextureLevelParameteriv (int texture, int level, int pname, Buffer.Int32 params);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glGetTextureParameterfv (int texture, int pname, Buffer.Float32 params);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glGetTextureParameterIiv (int texture, int pname, Buffer.Int32 params);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glGetTextureParameterIuiv (int texture, int pname, Buffer.Int32 params);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glGetTextureParameteriv (int texture, int pname, Buffer.Int32 params);

    /**
     * @param n
     * @param arrays
     */
     void glCreateVertexArrays (int n, Buffer.Int32 arrays);

    /**
     * @param vaobj
     * @param index
     */
     void glDisableVertexArrayAttrib (int vaobj, int index);

    /**
     * @param vaobj
     * @param index
     */
     void glEnableVertexArrayAttrib (int vaobj, int index);

    /**
     * @param vaobj
     * @param buffer
     */
     void glVertexArrayElementBuffer (int vaobj, int buffer);

    /**
     * @param vaobj
     * @param bindingindex
     * @param buffer
     * @param offset
     * @param stride
     */
     void glVertexArrayVertexBuffer (int vaobj, int bindingindex, int buffer, long offset, int stride);

    /**
     * @param vaobj
     * @param first
     * @param count
     * @param buffers
     * @param offsets
     * @param strides
     */
     void glVertexArrayVertexBuffers (int vaobj, int first, int count, Buffer.Int32 buffers, Buffer.Int8 offsets, Buffer.Int32 strides);

    /**
     * @param vaobj
     * @param attribindex
     * @param bindingindex
     */
     void glVertexArrayAttribBinding (int vaobj, int attribindex, int bindingindex);

    /**
     * @param vaobj
     * @param attribindex
     * @param size
     * @param type
     * @param normalized
     * @param relativeoffset
     */
     void glVertexArrayAttribFormat (int vaobj, int attribindex, int size, int type, boolean normalized, int relativeoffset);

    /**
     * @param vaobj
     * @param attribindex
     * @param size
     * @param type
     * @param relativeoffset
     */
     void glVertexArrayAttribIFormat (int vaobj, int attribindex, int size, int type, int relativeoffset);

    /**
     * @param vaobj
     * @param attribindex
     * @param size
     * @param type
     * @param relativeoffset
     */
     void glVertexArrayAttribLFormat (int vaobj, int attribindex, int size, int type, int relativeoffset);

    /**
     * @param vaobj
     * @param bindingindex
     * @param divisor
     */
     void glVertexArrayBindingDivisor (int vaobj, int bindingindex, int divisor);

    /**
     * @param vaobj
     * @param pname
     * @param param
     */
     void glGetVertexArrayiv (int vaobj, int pname, Buffer.Int32 param);

    /**
     * @param vaobj
     * @param index
     * @param pname
     * @param param
     */
     void glGetVertexArrayIndexediv (int vaobj, int index, int pname, Buffer.Int32 param);

    /**
     * @param vaobj
     * @param index
     * @param pname
     * @param param
     */
     void glGetVertexArrayIndexed64iv (int vaobj, int index, int pname, Buffer.Int8 param);

    /**
     * @param n
     * @param samplers
     */
     void glCreateSamplers (int n, Buffer.Int32 samplers);

    /**
     * @param n
     * @param pipelines
     */
     void glCreateProgramPipelines (int n, Buffer.Int32 pipelines);

    /**
     * @param target
     * @param n
     * @param ids
     */
     void glCreateQueries (int target, int n, Buffer.Int32 ids);

    /**
     * @param id
     * @param buffer
     * @param pname
     * @param offset
     */
     void glGetQueryBufferObjecti64v (int id, int buffer, int pname, long offset);

    /**
     * @param id
     * @param buffer
     * @param pname
     * @param offset
     */
     void glGetQueryBufferObjectiv (int id, int buffer, int pname, long offset);

    /**
     * @param id
     * @param buffer
     * @param pname
     * @param offset
     */
     void glGetQueryBufferObjectui64v (int id, int buffer, int pname, long offset);

    /**
     * @param id
     * @param buffer
     * @param pname
     * @param offset
     */
     void glGetQueryBufferObjectuiv (int id, int buffer, int pname, long offset);

    /**
     * @param barriers
     */
     void glMemoryBarrierByRegion (int barriers);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param width
     * @param height
     * @param depth
     * @param format
     * @param type
     * @param bufSize
     * @param pixels
     */
     void glGetTextureSubImage (int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, int bufSize, Buffer.Int8 pixels);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param width
     * @param height
     * @param depth
     * @param bufSize
     * @param pixels
     */
     void glGetCompressedTextureSubImage (int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int bufSize, Buffer.Int8 pixels);

     int glGetGraphicsResetStatus ();

    /**
     * @param target
     * @param lod
     * @param bufSize
     * @param pixels
     */
     void glGetnCompressedTexImage (int target, int lod, int bufSize, Buffer.Int8 pixels);

    /**
     * @param target
     * @param level
     * @param format
     * @param type
     * @param bufSize
     * @param pixels
     */
     void glGetnTexImage (int target, int level, int format, int type, int bufSize, Buffer.Int8 pixels);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformdv (int program, int location, int bufSize, Buffer.Float64 params);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformfv (int program, int location, int bufSize, Buffer.Float32 params);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformiv (int program, int location, int bufSize, Buffer.Int32 params);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformuiv (int program, int location, int bufSize, Buffer.Int32 params);

    /**
     * @param x
     * @param y
     * @param width
     * @param height
     * @param format
     * @param type
     * @param bufSize
     * @param data
     */
     void glReadnPixels (int x, int y, int width, int height, int format, int type, int bufSize, Buffer.Int8 data);

    /**
     * @param target
     * @param query
     * @param bufSize
     * @param v
     */
     void glGetnMapdv (int target, int query, int bufSize, Buffer.Float64 v);

    /**
     * @param target
     * @param query
     * @param bufSize
     * @param v
     */
     void glGetnMapfv (int target, int query, int bufSize, Buffer.Float32 v);

    /**
     * @param target
     * @param query
     * @param bufSize
     * @param v
     */
     void glGetnMapiv (int target, int query, int bufSize, Buffer.Int32 v);

    /**
     * @param map
     * @param bufSize
     * @param values
     */
     void glGetnPixelMapfv (int map, int bufSize, Buffer.Float32 values);

    /**
     * @param map
     * @param bufSize
     * @param values
     */
     void glGetnPixelMapuiv (int map, int bufSize, Buffer.Int32 values);

    /**
     * @param map
     * @param bufSize
     * @param values
     */
     void glGetnPixelMapusv (int map, int bufSize, Buffer.Int16 values);

    /**
     * @param bufSize
     * @param pattern
     */
     void glGetnPolygonStipple (int bufSize, Buffer.Int8 pattern);

    /**
     * @param target
     * @param format
     * @param type
     * @param bufSize
     * @param table
     */
     void glGetnColorTable (int target, int format, int type, int bufSize, Buffer.Int8 table);

    /**
     * @param target
     * @param format
     * @param type
     * @param bufSize
     * @param image
     */
     void glGetnConvolutionFilter (int target, int format, int type, int bufSize, Buffer.Int8 image);

    /**
     * @param target
     * @param format
     * @param type
     * @param rowBufSize
     * @param row
     * @param columnBufSize
     * @param column
     * @param span
     */
     void glGetnSeparableFilter (int target, int format, int type, int rowBufSize, Buffer.Int8 row, int columnBufSize, Buffer.Int8 column, Buffer.Int8 span);

    /**
     * @param target
     * @param reset
     * @param format
     * @param type
     * @param bufSize
     * @param values
     */
     void glGetnHistogram (int target, boolean reset, int format, int type, int bufSize, Buffer.Int8 values);

    /**
     * @param target
     * @param reset
     * @param format
     * @param type
     * @param bufSize
     * @param values
     */
     void glGetnMinmax (int target, boolean reset, int format, int type, int bufSize, Buffer.Int8 values);

     void glTextureBarrier ();

}
