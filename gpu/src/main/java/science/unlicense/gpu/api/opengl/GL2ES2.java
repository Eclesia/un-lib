
package science.unlicense.gpu.api.opengl;

/**
 *
 * @author Johann Sorel
 */
public interface GL2ES2 extends GL,GLES220 {

}
