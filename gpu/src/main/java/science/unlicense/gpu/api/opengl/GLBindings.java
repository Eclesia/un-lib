
package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.predicate.Predicates;
import science.unlicense.system.ModuleSeeker;

/**
 *
 * @author Johann Sorel
 */
public final class GLBindings {

    private GLBindings(){}

    /**
     * Lists available GL bindings
     * @return array of GLBinding, never null but can be empty.
     */
    public static GLBinding[] getBindings(){
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/science.unlicense.gpu.api.opengl.GLBinding"),
                Predicates.instanceOf(GLBinding.class));
        final GLBinding[] formats = new GLBinding[results.getSize()];
        Collections.copy(results, formats, 0);
        return formats;
    }

}
