package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;

public interface GL40 {

    /**
     * @param value ,value from enumeration group ColorF
     */
     void glMinSampleShading (float value);

    /**
     * @param buf
     * @param mode
     */
     void glBlendEquationi (int buf, int mode);

    /**
     * @param buf
     * @param modeRGB
     * @param modeAlpha
     */
     void glBlendEquationSeparatei (int buf, int modeRGB, int modeAlpha);

    /**
     * @param buf
     * @param src
     * @param dst
     */
     void glBlendFunci (int buf, int src, int dst);

    /**
     * @param buf
     * @param srcRGB
     * @param dstRGB
     * @param srcAlpha
     * @param dstAlpha
     */
     void glBlendFuncSeparatei (int buf, int srcRGB, int dstRGB, int srcAlpha, int dstAlpha);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param indirect
     */
     void glDrawArraysIndirect (int mode, long indirect);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param type
     * @param indirect
     */
     void glDrawElementsIndirect (int mode, int type, long indirect);

    /**
     * @param location
     * @param x
     */
     void glUniform1d (int location, double x);

    /**
     * @param location
     * @param x
     * @param y
     */
     void glUniform2d (int location, double x, double y);

    /**
     * @param location
     * @param x
     * @param y
     * @param z
     */
     void glUniform3d (int location, double x, double y, double z);

    /**
     * @param location
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glUniform4d (int location, double x, double y, double z, double w);

    /**
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1dv (int location, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glUniform2dv (int location, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glUniform3dv (int location, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glUniform4dv (int location, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*4
     */
     void glUniformMatrix2dv (int location, boolean transpose, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*9
     */
     void glUniformMatrix3dv (int location, boolean transpose, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*16
     */
     void glUniformMatrix4dv (int location, boolean transpose, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glUniformMatrix2x3dv (int location, boolean transpose, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glUniformMatrix2x4dv (int location, boolean transpose, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glUniformMatrix3x2dv (int location, boolean transpose, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glUniformMatrix3x4dv (int location, boolean transpose, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glUniformMatrix4x2dv (int location, boolean transpose, Buffer.Float64 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glUniformMatrix4x3dv (int location, boolean transpose, Buffer.Float64 value);

    /**
     * @param program
     * @param location
     * @param params
     */
     void glGetUniformdv (int program, int location, Buffer.Float64 params);

    /**
     * @param program
     * @param shadertype
     * @param name
     */
     int glGetSubroutineUniformLocation (int program, int shadertype, science.unlicense.common.api.character.CharArray name);

    /**
     * @param program
     * @param shadertype
     * @param name
     */
     int glGetSubroutineIndex (int program, int shadertype, science.unlicense.common.api.character.CharArray name);

    /**
     * @param program
     * @param shadertype
     * @param index
     * @param pname
     * @param values
     */
     void glGetActiveSubroutineUniformiv (int program, int shadertype, int index, int pname, Buffer.Int32 values);

    /**
     * @param program
     * @param shadertype
     * @param index
     * @param bufsize
     * @param length
     * @param name ,length bufsize
     */
     void glGetActiveSubroutineUniformName (int program, int shadertype, int index, Buffer.Int32 length, Buffer.Int8 name);

    /**
     * @param program
     * @param shadertype
     * @param index
     * @param bufsize
     * @param length
     * @param name ,length bufsize
     */
     void glGetActiveSubroutineName (int program, int shadertype, int index, Buffer.Int32 length, Buffer.Int8 name);

    /**
     * @param shadertype
     * @param count
     * @param indices ,length count
     */
     void glUniformSubroutinesuiv (int shadertype, Buffer.Int32 indices);

    /**
     * @param shadertype
     * @param location
     * @param params
     */
     void glGetUniformSubroutineuiv (int shadertype, int location, Buffer.Int32 params);

    /**
     * @param program
     * @param shadertype
     * @param pname
     * @param values
     */
     void glGetProgramStageiv (int program, int shadertype, int pname, Buffer.Int32 values);

    /**
     * @param pname
     * @param value
     */
     void glPatchParameteri (int pname, int value);

    /**
     * @param pname
     * @param values
     */
     void glPatchParameterfv (int pname, Buffer.Float32 values);

    /**
     * @param target
     * @param id
     */
     void glBindTransformFeedback (int target, int id);

    /**
     * @param n
     * @param ids ,length n
     */
     void glDeleteTransformFeedbacks (Buffer.Int32 ids);

    /**
     * @param n
     * @param ids ,length n
     */
     void glGenTransformFeedbacks (Buffer.Int32 ids);

    /**
     * Convenient method.
     *
     * @param n
     * @param ids ,length n
     */
     void glGenTransformFeedbacks (int[] ids);

    /**
     * @param id
     */
     boolean glIsTransformFeedback (int id);

     void glPauseTransformFeedback ();

     void glResumeTransformFeedback ();

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param id
     */
     void glDrawTransformFeedback (int mode, int id);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param id
     * @param stream
     */
     void glDrawTransformFeedbackStream (int mode, int id, int stream);

    /**
     * @param target
     * @param index
     * @param id
     */
     void glBeginQueryIndexed (int target, int index, int id);

    /**
     * @param target
     * @param index
     */
     void glEndQueryIndexed (int target, int index);

    /**
     * @param target
     * @param index
     * @param pname
     * @param params
     */
     void glGetQueryIndexediv (int target, int index, int pname, Buffer.Int32 params);

}
