package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;

public interface GLES232 extends science.unlicense.gpu.api.opengl.GLES231 {

     void glBlendBarrier ();

    /**
     * @param srcName
     * @param srcTarget
     * @param srcLevel
     * @param srcX
     * @param srcY
     * @param srcZ
     * @param dstName
     * @param dstTarget
     * @param dstLevel
     * @param dstX
     * @param dstY
     * @param dstZ
     * @param srcWidth
     * @param srcHeight
     * @param srcDepth
     */
     void glCopyImageSubData (int srcName, int srcTarget, int srcLevel, int srcX, int srcY, int srcZ, int dstName, int dstTarget, int dstLevel, int dstX, int dstY, int dstZ, int srcWidth, int srcHeight, int srcDepth);

    /**
     * @param source
     * @param type
     * @param severity
     * @param count
     * @param ids ,length count
     * @param enabled ,value from enumeration group Boolean
     */
     void glDebugMessageControl (int source, int type, int severity, Buffer.Int32 ids, boolean enabled);

    /**
     * @param source
     * @param type
     * @param id
     * @param severity
     * @param length
     * @param buf
     */
     void glDebugMessageInsert (int source, int type, int id, int severity, int length, science.unlicense.common.api.character.CharArray buf);

    /**
     * @param callback
     * @param userParam
     */
     void glDebugMessageCallback (Object callback, long userParam);

    /**
     * @param count
     * @param bufSize
     * @param sources ,length count
     * @param types ,length count
     * @param ids ,length count
     * @param severities ,length count
     * @param lengths ,length count
     * @param messageLog ,length bufSize
     */
     int glGetDebugMessageLog (Buffer.Int32 sources, Buffer.Int32 types, Buffer.Int32 ids, Buffer.Int32 severities, Buffer.Int32 lengths, Buffer.Int8 messageLog);

    /**
     * @param source
     * @param id
     * @param length
     * @param message
     */
     void glPushDebugGroup (int source, int id, int length, science.unlicense.common.api.character.CharArray message);

     void glPopDebugGroup ();

    /**
     * @param identifier
     * @param name
     * @param length
     * @param label
     */
     void glObjectLabel (int identifier, int name, int length, science.unlicense.common.api.character.CharArray label);

    /**
     * @param identifier
     * @param name
     * @param bufSize
     * @param length
     * @param label ,length bufSize
     */
     void glGetObjectLabel (int identifier, int name, Buffer.Int32 length, Buffer.Int8 label);

    /**
     * @param ptr
     * @param length
     * @param label
     */
     void glObjectPtrLabel (long ptr, int length, science.unlicense.common.api.character.CharArray label);

    /**
     * @param ptr
     * @param bufSize
     * @param length
     * @param label ,length bufSize
     */
     void glGetObjectPtrLabel (long ptr, Buffer.Int32 length, Buffer.Int8 label);

    /**
     * @param pname ,value from enumeration group GetPointervPName
     * @param params
     */
     void glGetPointerv (int pname, Buffer.Int8 params);

    /**
     * @param target
     * @param index
     */
     void glEnablei (int target, int index);

    /**
     * @param target
     * @param index
     */
     void glDisablei (int target, int index);

    /**
     * @param buf
     * @param mode
     */
     void glBlendEquationi (int buf, int mode);

    /**
     * @param buf
     * @param modeRGB
     * @param modeAlpha
     */
     void glBlendEquationSeparatei (int buf, int modeRGB, int modeAlpha);

    /**
     * @param buf
     * @param src
     * @param dst
     */
     void glBlendFunci (int buf, int src, int dst);

    /**
     * @param buf
     * @param srcRGB
     * @param dstRGB
     * @param srcAlpha
     * @param dstAlpha
     */
     void glBlendFuncSeparatei (int buf, int srcRGB, int dstRGB, int srcAlpha, int dstAlpha);

    /**
     * @param index
     * @param r ,value from enumeration group Boolean
     * @param g ,value from enumeration group Boolean
     * @param b ,value from enumeration group Boolean
     * @param a ,value from enumeration group Boolean
     */
     void glColorMaski (int index, boolean r, boolean g, boolean b, boolean a);

    /**
     * @param target
     * @param index
     */
     boolean glIsEnabledi (int target, int index);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     * @param basevertex
     */
     void glDrawElementsBaseVertex (int mode, int count, int type, long indices, int basevertex);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param start
     * @param end
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     * @param basevertex
     */
     void glDrawRangeElementsBaseVertex (int mode, int start, int end, int count, int type, long indices, int basevertex);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     * @param instancecount
     * @param basevertex
     */
     void glDrawElementsInstancedBaseVertex (int mode, int count, int type, long indices, int instancecount, int basevertex);

    /**
     * @param target
     * @param attachment
     * @param texture
     * @param level
     */
     void glFramebufferTexture (int target, int attachment, int texture, int level);

    /**
     * @param minX
     * @param minY
     * @param minZ
     * @param minW
     * @param maxX
     * @param maxY
     * @param maxZ
     * @param maxW
     */
     void glPrimitiveBoundingBox (float minX, float minY, float minZ, float minW, float maxX, float maxY, float maxZ, float maxW);

     int glGetGraphicsResetStatus ();

    /**
     * @param x
     * @param y
     * @param width
     * @param height
     * @param format
     * @param type
     * @param bufSize
     * @param data
     */
     void glReadnPixels (int x, int y, int width, int height, int format, int type, int bufSize, Buffer.Int8 data);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformfv (int program, int location, int bufSize, Buffer.Float32 params);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformiv (int program, int location, int bufSize, Buffer.Int32 params);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformuiv (int program, int location, int bufSize, Buffer.Int32 params);

    /**
     * @param value ,value from enumeration group ColorF
     */
     void glMinSampleShading (float value);

    /**
     * @param pname
     * @param value
     */
     void glPatchParameteri (int pname, int value);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params
     */
     void glTexParameterIiv (int target, int pname, Buffer.Int32 params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params
     */
     void glTexParameterIuiv (int target, int pname, Buffer.Int32 params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameterIiv (int target, int pname, Buffer.Int32 params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameterIuiv (int target, int pname, Buffer.Int32 params);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameterIiv (int sampler, int pname, Buffer.Int32 param);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameterIuiv (int sampler, int pname, Buffer.Int32 param);

    /**
     * @param sampler
     * @param pname
     * @param params
     */
     void glGetSamplerParameterIiv (int sampler, int pname, Buffer.Int32 params);

    /**
     * @param sampler
     * @param pname
     * @param params
     */
     void glGetSamplerParameterIuiv (int sampler, int pname, Buffer.Int32 params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param internalformat
     * @param buffer
     */
     void glTexBuffer (int target, int internalformat, int buffer);

    /**
     * @param target
     * @param internalformat
     * @param buffer
     * @param offset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     */
     void glTexBufferRange (int target, int internalformat, int buffer, long offset, long size);

    /**
     * @param target
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     * @param depth
     * @param fixedsamplelocations ,value from enumeration group Boolean
     */
     void glTexStorage3DMultisample (int target, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations);

}
