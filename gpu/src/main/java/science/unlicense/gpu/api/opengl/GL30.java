package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.character.CharArray;

public interface GL30 {

    /**
     * @param index
     * @param r ,value from enumeration group Boolean
     * @param g ,value from enumeration group Boolean
     * @param b ,value from enumeration group Boolean
     * @param a ,value from enumeration group Boolean
     */
     void glColorMaski (int index, boolean r, boolean g, boolean b, boolean a);

    /**
     * @param target
     * @param index
     * @param data ,value from enumeration group Boolean
     */
     void glGetBooleani_v (int target, int index, Buffer.Int8 data);

    /**
     * @param target
     * @param index
     * @param data
     */
     void glGetIntegeri_v (int target, int index, Buffer.Int32 data);

    /**
     * @param target
     * @param index
     */
     void glEnablei (int target, int index);

    /**
     * @param target
     * @param index
     */
     void glDisablei (int target, int index);

    /**
     * @param target
     * @param index
     */
     boolean glIsEnabledi (int target, int index);

    /**
     * @param primitiveMode
     */
     void glBeginTransformFeedback (int primitiveMode);

     void glEndTransformFeedback ();

    /**
     * @param target
     * @param index
     * @param buffer
     * @param offset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     */
     void glBindBufferRange (int target, int index, int buffer, long offset, long size);

    /**
     * @param target
     * @param index
     * @param buffer
     */
     void glBindBufferBase (int target, int index, int buffer);

    /**
     * @param program
     * @param count
     * @param varyings ,length count
     * @param bufferMode
     */
     void glTransformFeedbackVaryings (int program, CharArray[] varyings, int bufferMode);

    /**
     * @param program
     * @param index
     * @param bufSize
     * @param length
     * @param size
     * @param type
     * @param name ,length bufSize
     */
     void glGetTransformFeedbackVarying (int program, int index, Buffer.Int32 length, Buffer.Int32 size, Buffer.Int32 type, Buffer.Int8 name);

    /**
     * @param target ,value from enumeration group ClampColorTargetARB
     * @param clamp ,value from enumeration group ClampColorModeARB
     */
     void glClampColor (int target, int clamp);

    /**
     * @param id
     * @param mode ,value from enumeration group TypeEnum
     */
     void glBeginConditionalRender (int id, int mode);

     void glEndConditionalRender ();

    /**
     * @param index
     * @param size
     * @param type ,value from enumeration group VertexAttribEnum
     * @param stride
     * @param pointer
     */
     void glVertexAttribIPointer (int index, int size, int type, int stride, long pointer);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribEnum
     * @param params
     */
     void glGetVertexAttribIiv (int index, int pname, Buffer.Int32 params);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribEnum
     * @param params
     */
     void glGetVertexAttribIuiv (int index, int pname, Buffer.Int32 params);

    /**
     * @param index
     * @param x
     */
     void glVertexAttribI1i (int index, int x);

    /**
     * @param index
     * @param x
     * @param y
     */
     void glVertexAttribI2i (int index, int x, int y);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     */
     void glVertexAttribI3i (int index, int x, int y, int z);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glVertexAttribI4i (int index, int x, int y, int z, int w);

    /**
     * @param index
     * @param x
     */
     void glVertexAttribI1ui (int index, int x);

    /**
     * @param index
     * @param x
     * @param y
     */
     void glVertexAttribI2ui (int index, int x, int y);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     */
     void glVertexAttribI3ui (int index, int x, int y, int z);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glVertexAttribI4ui (int index, int x, int y, int z, int w);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI1iv (int index, Buffer.Int32 v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI2iv (int index, Buffer.Int32 v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI3iv (int index, Buffer.Int32 v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI4iv (int index, Buffer.Int32 v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI1uiv (int index, Buffer.Int32 v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI2uiv (int index, Buffer.Int32 v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI3uiv (int index, Buffer.Int32 v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI4uiv (int index, Buffer.Int32 v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI4bv (int index, Buffer.Int8 v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI4sv (int index, Buffer.Int16 v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI4ubv (int index, Buffer.Int8 v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI4usv (int index, Buffer.Int16 v);

    /**
     * @param program
     * @param location
     * @param params
     */
     void glGetUniformuiv (int program, int location, Buffer.Int32 params);

    /**
     * @param program
     * @param color
     * @param name
     */
     void glBindFragDataLocation (int program, int color, science.unlicense.common.api.character.CharArray name);

    /**
     * @param program
     * @param name
     */
     int glGetFragDataLocation (int program, science.unlicense.common.api.character.CharArray name);

    /**
     * @param location
     * @param v0
     */
     void glUniform1ui (int location, int v0);

    /**
     * @param location
     * @param v0
     * @param v1
     */
     void glUniform2ui (int location, int v0, int v1);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glUniform3ui (int location, int v0, int v1, int v2);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glUniform4ui (int location, int v0, int v1, int v2, int v3);

    /**
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1uiv (int location, Buffer.Int32 value);

    /**
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glUniform2uiv (int location, Buffer.Int32 value);

    /**
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glUniform3uiv (int location, Buffer.Int32 value);

    /**
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glUniform4uiv (int location, Buffer.Int32 value);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params
     */
     void glTexParameterIiv (int target, int pname, Buffer.Int32 params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params
     */
     void glTexParameterIuiv (int target, int pname, Buffer.Int32 params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameterIiv (int target, int pname, Buffer.Int32 params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameterIuiv (int target, int pname, Buffer.Int32 params);

    /**
     * @param buffer
     * @param drawbuffer ,value from enumeration group DrawBufferName
     * @param value
     */
     void glClearBufferiv (int buffer, int drawbuffer, Buffer.Int32 value);

    /**
     * @param buffer
     * @param drawbuffer ,value from enumeration group DrawBufferName
     * @param value
     */
     void glClearBufferuiv (int buffer, int drawbuffer, Buffer.Int32 value);

    /**
     * @param buffer
     * @param drawbuffer ,value from enumeration group DrawBufferName
     * @param value
     */
     void glClearBufferfv (int buffer, int drawbuffer, Buffer.Float32 value);

    /**
     * @param buffer
     * @param drawbuffer ,value from enumeration group DrawBufferName
     * @param depth
     * @param stencil
     */
     void glClearBufferfi (int buffer, int drawbuffer, float depth, int stencil);

    /**
     * @param name
     * @param index
     */
     byte glGetStringi (int name, int index);

    /**
     * @param renderbuffer
     */
     boolean glIsRenderbuffer (int renderbuffer);

    /**
     * @param target ,value from enumeration group RenderbufferTarget
     * @param renderbuffer
     */
     void glBindRenderbuffer (int target, int renderbuffer);

    /**
     * @param n
     * @param renderbuffers ,length n
     */
     void glDeleteRenderbuffers (Buffer.Int32 renderbuffers);

    /**
     * @param n
     * @param renderbuffers ,length n
     */
     void glGenRenderbuffers (Buffer.Int32 renderbuffers);

    /**
     * @param target ,value from enumeration group RenderbufferTarget
     * @param internalformat
     * @param width
     * @param height
     */
     void glRenderbufferStorage (int target, int internalformat, int width, int height);

    /**
     * @param target ,value from enumeration group RenderbufferTarget
     * @param pname
     * @param params
     */
     void glGetRenderbufferParameteriv (int target, int pname, Buffer.Int32 params);

    /**
     * @param framebuffer
     */
     boolean glIsFramebuffer (int framebuffer);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param framebuffer
     */
     void glBindFramebuffer (int target, int framebuffer);

    /**
     * @param n
     * @param framebuffers ,length n
     */
     void glDeleteFramebuffers (Buffer.Int32 framebuffers);

    /**
     * @param n
     * @param framebuffers ,length n
     */
     void glGenFramebuffers (Buffer.Int32 framebuffers);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     */
     int glCheckFramebufferStatus (int target);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param textarget
     * @param texture
     * @param level
     */
     void glFramebufferTexture1D (int target, int attachment, int textarget, int texture, int level);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param textarget
     * @param texture
     * @param level
     */
     void glFramebufferTexture2D (int target, int attachment, int textarget, int texture, int level);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param textarget
     * @param texture
     * @param level
     * @param zoffset
     */
     void glFramebufferTexture3D (int target, int attachment, int textarget, int texture, int level, int zoffset);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param renderbuffertarget ,value from enumeration group RenderbufferTarget
     * @param renderbuffer
     */
     void glFramebufferRenderbuffer (int target, int attachment, int renderbuffertarget, int renderbuffer);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param pname
     * @param params
     */
     void glGetFramebufferAttachmentParameteriv (int target, int attachment, int pname, Buffer.Int32 params);

    /**
     * @param target
     */
     void glGenerateMipmap (int target);

    /**
     * @param srcX0
     * @param srcY0
     * @param srcX1
     * @param srcY1
     * @param dstX0
     * @param dstY0
     * @param dstX1
     * @param dstY1
     * @param mask ,value from enumeration group ClearBufferMask
     * @param filter
     */
     void glBlitFramebuffer (int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, int mask, int filter);

    /**
     * @param target
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     */
     void glRenderbufferStorageMultisample (int target, int samples, int internalformat, int width, int height);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param texture ,value from enumeration group Texture
     * @param level ,value from enumeration group CheckedInt32
     * @param layer ,value from enumeration group CheckedInt32
     */
     void glFramebufferTextureLayer (int target, int attachment, int texture, int level, int layer);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param offset ,value from enumeration group BufferOffset
     * @param length ,value from enumeration group BufferSize
     * @param access ,value from enumeration group BufferAccessMask
     */
     void glMapBufferRange (int target, long offset, long length, int access);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param offset ,value from enumeration group BufferOffset
     * @param length ,value from enumeration group BufferSize
     */
     void glFlushMappedBufferRange (int target, long offset, long length);

    /**
     * @param array
     */
     void glBindVertexArray (int array);

    /**
     * @param n
     * @param arrays ,length n
     */
     void glDeleteVertexArrays (Buffer.Int32 arrays);

    /**
     * Convenient method.
     *
     * @param n
     * @param arrays ,length n
     */
    void glDeleteVertexArrays (int[] arrays);

    /**
     * @param n
     * @param arrays ,length n
     */
     void glGenVertexArrays (Buffer.Int32 arrays);

    /**
     * @param n
     * @param arrays ,length n
     */
     void glGenVertexArrays (int[] arrays);

    /**
     * @param array
     */
     boolean glIsVertexArray (int array);

}
