
package science.unlicense.gpu.api.opengl;

/**
 *
 * @author Johann Sorel
 */
public interface GL2ES3 extends GL,GLES232 {

}
