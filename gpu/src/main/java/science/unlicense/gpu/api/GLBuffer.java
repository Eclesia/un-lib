
package science.unlicense.gpu.api;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import science.unlicense.common.api.buffer.AbstractBuffer;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class GLBuffer extends AbstractBuffer {

    private static Method cleanerMethod;
    private static Method cleanMethod;
    private static Field attField;

    static {

        //older jvm versions
        try {
            final Class clazzByteBuffer = Class.forName("java.nio.DirectByteBuffer");
            attField = clazzByteBuffer.getDeclaredField("att");
            attField.setAccessible(true);

            cleanerMethod = clazzByteBuffer.getMethod("cleaner");
            cleanerMethod.setAccessible(true);

            final Class clazzCleaner = Class.forName("sun.misc.Cleaner");
            cleanMethod = clazzCleaner.getMethod("clean");
            cleanMethod.setAccessible(true);

        } catch (ClassNotFoundException ex) {
            System.out.println("DirectBuffer class not found");
        } catch (NoSuchFieldException ex) {
            System.out.println("DirectBuffer class not found");
        } catch (SecurityException ex) {
            System.out.println("DirectBuffer class not found");
        } catch (NoSuchMethodException ex) {
            System.out.println("DirectBuffer class not found");
        }
    }

//    private final Exception ex = new Exception();
    private ByteBuffer buffer;

    public GLBuffer(ByteBuffer buffer, NumberType primitiveType) {
        super(primitiveType, buffer.order() == ByteOrder.BIG_ENDIAN ? Endianness.BIG_ENDIAN : Endianness.LITTLE_ENDIAN, GLBufferFactory.INSTANCE);
        this.buffer = buffer;
//        ex.fillInStackTrace();
    }

    @Override
    public boolean isWritable() {
        return !buffer.isReadOnly();
    }

    @Override
    public long getByteSize() {
        return buffer.capacity();
    }

    @Override
    public ByteBuffer getBackEnd() {
        return buffer;
    }

    @Override
    public byte readInt8(long offset) {
        return buffer.get((int) offset);
    }

    @Override
    public void readInt8(byte[] array, int arrayOffset, int length, long offset) {
        final ByteBuffer buffer = this.buffer.duplicate();
        buffer.position((int) offset);
        buffer.get(array, arrayOffset, length);
    }

    @Override
    public void writeInt8(byte value, long offset) {
        buffer.put((int) offset, value);
    }

    @Override
    public void writeInt8(byte[] array, int arrayOffset, int length, long offset) {
        final ByteBuffer buffer = this.buffer.duplicate();
        buffer.position((int) offset);
        buffer.put(array, arrayOffset, length);
    }

    @Override
    public void readInt32(int[] array, int arrayOffset, int length, long offset) {
        if ( (offset%4) == 0) {
            final IntBuffer ib = buffer.asIntBuffer();
            ib.position((int) (offset/4l));
            ib.get(array, arrayOffset, length);
        } else {
            super.readInt32(array, arrayOffset, length, offset);
        }
    }

    @Override
    public float readFloat32(long offset) {
        if ( (offset%4) == 0) {
            final FloatBuffer fb = buffer.asFloatBuffer();
            fb.position((int) (offset/4l));
            return fb.get();
        } else {
            return super.readFloat32(offset);
        }
    }

    @Override
    public void readFloat32(float[] array, int arrayOffset, int length, long offset) {
        if ( (offset%4) == 0) {
            final FloatBuffer fb = buffer.asFloatBuffer();
            fb.position((int) (offset/4l));
            fb.get(array, arrayOffset, length);
        } else {
            super.readFloat32(array, arrayOffset, length, offset);
        }
    }

    @Override
    public double readFloat64(long offset) {
        if ( (offset%8) == 0) {
            final DoubleBuffer fb = buffer.asDoubleBuffer();
            fb.position((int) (offset/8l));
            return fb.get();
        } else {
            return super.readFloat64(offset);
        }
    }

    @Override
    public void readFloat64(double[] array, int arrayOffset, int length, long offset) {
        if ( (offset%8) == 0) {
            final DoubleBuffer fb = buffer.asDoubleBuffer();
            fb.position((int) (offset/8l));
            fb.get(array, arrayOffset, length);
        } else {
            super.readFloat64(array, arrayOffset, length, offset);
        }
    }

    @Override
    public void dispose(){
        if (buffer==null) return;
        dispose(buffer);
        this.buffer = null;
    }

    @Override
    public Buffer copy(BufferFactory factory, NumberType primitive, Endianness encoding) {
        if (  (factory == null || this.factory == factory)
           && (primitive == null || this.numericType == primitive)
           && (encoding == null || this.endianness == encoding)) {
            final Buffer copy = this.factory.createInt8(buffer.capacity()).getBuffer();
            ((GLBuffer) copy).buffer.put(buffer);
            return copy;
        }
        return super.copy(factory, primitive, encoding);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (buffer!=null){
//            Loggers.get().info(new Chars(
//                    "Unreleased direct buffer, the failsafe finalize method will dispose it. Memory "));
            //ex.printStackTrace();
//            JavaNioAccess.BufferPool pool = SharedSecrets.getJavaNioAccess().getDirectBufferPool();
//            Loggers.get().info(new Chars("B "+pool.getMemoryUsed()+"  "+pool.getCount()));
            dispose();
//            Loggers.get().info(new Chars("A "+pool.getMemoryUsed()+"  "+pool.getCount()));
        }
    }


    public static void dispose(java.nio.Buffer buffer){
        //TODO : find method to release buffer in JDK9/10/11
        if (buffer!=null && buffer.isDirect() && cleanerMethod != null && cleanMethod != null) {
            //the one way to clean java direct buffer
            try {
                if (!buffer.getClass().getName().equals("java.nio.DirectByteBuffer")) {
                    buffer = (java.nio.Buffer) attField.get(buffer);
                }
                final Object cleaner = cleanerMethod.invoke(buffer);
                if (cleaner != null) {
                    cleanMethod.invoke(cleaner);
                }
            } catch(Exception ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }
    }

    /**
     * Return the size of given buffer in bytes.
     *
     * @param buffer
     * @return
     */
    public static int byteSize(java.nio.Buffer buffer) {
        final int size = buffer.capacity();
        if (buffer instanceof ByteBuffer) {
            return size;
        } else if (buffer instanceof ShortBuffer) {
            return size*2;
        } else if (buffer instanceof IntBuffer) {
            return size*4;
        } else if (buffer instanceof LongBuffer) {
            return size*8;
        } else if (buffer instanceof FloatBuffer) {
            return size*4;
        } else if (buffer instanceof DoubleBuffer) {
            return size*8;
        } else {
            throw new InvalidArgumentException("Unknowned buffer type : "+buffer);
        }
    }

    public static IntBuffer of(int[] array) {
        final ByteBuffer bb = ByteBuffer.allocateDirect(array.length*4);
        final IntBuffer intBuffer = bb.asIntBuffer();
        intBuffer.put(array);
        bb.position(0);
        return intBuffer;
    }

    public static java.nio.Buffer unwrapOrCopy(Buffer.View view) {
        if (view == null) return null;
        return unwrapOrCopy(view.getBuffer());
    }

    public static ByteBuffer unwrapOrCopyByte(Buffer.View view) {
        if (view == null) return null;
        return unwrapOrCopyByte(view.getBuffer());
    }

    public static ShortBuffer unwrapOrCopyShort(Buffer.View view) {
        if (view == null) return null;
        return unwrapOrCopyShort(view.getBuffer());
    }

    public static IntBuffer unwrapOrCopyInt(Buffer.View view) {
        if (view == null) return null;
        return unwrapOrCopyInt(view.getBuffer());
    }

    public static LongBuffer unwrapOrCopyLong(Buffer.View view) {
        if (view == null) return null;
        return unwrapOrCopyLong(view.getBuffer());
    }

    public static FloatBuffer unwrapOrCopyFloat(Buffer.Float32 view) {
        if (view == null) return null;
        return unwrapOrCopyFloat(view.getBuffer());
    }

    public static DoubleBuffer unwrapOrCopyDouble(Buffer.View view) {
        if (view == null) return null;
        return unwrapOrCopyDouble(view.getBuffer());
    }

    public static java.nio.Buffer unwrapOrCopy(Buffer buffer) {
        if (buffer == null) return null;
        final Object backEnd = buffer.getBackEnd();

        java.nio.Buffer buf;
        if (backEnd instanceof java.nio.Buffer) {
            buf = (java.nio.Buffer) backEnd;
        } else {
            byte[] array = buffer.toByteArray();
            if (Endianness.BIG_ENDIAN.equals(buffer.getEndianness())) {
                //reverse order
                Endianness.reverseOrder(array, buffer.getNumericType().getSizeInBytes());
            }
            buf = ByteBuffer.allocateDirect(array.length);
            buf.position(0);
            ((ByteBuffer) buf).put(array);
            ((ByteBuffer) buf).order(ByteOrder.LITTLE_ENDIAN);
        }
        buf.position(0);
        return buf;
    }

    public static ByteBuffer unwrapOrCopyByte(Buffer buffer) {
        if (buffer == null) return null;
        final Object backEnd = buffer.getBackEnd();

        java.nio.ByteBuffer buf;
        if (backEnd instanceof java.nio.ByteBuffer) {
            buf = (java.nio.ByteBuffer) backEnd;
        } else {
            byte[] array = buffer.toByteArray();
            if (Endianness.BIG_ENDIAN.equals(buffer.getEndianness())) {
                //reverse order
                Endianness.reverseOrder(array, buffer.getNumericType().getSizeInBytes());
            }
            buf = ByteBuffer.allocateDirect(array.length);
            buf.position(0);
            buf.put(array);
        }
        buf.position(0);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        return buf;
    }

    public static ShortBuffer unwrapOrCopyShort(Buffer buffer) {
        if (buffer == null) return null;
        final Object backEnd = buffer.getBackEnd();

        if (backEnd instanceof java.nio.ShortBuffer) {
            ((java.nio.Buffer) backEnd).position(0);
            return (java.nio.ShortBuffer) backEnd;
        }

        java.nio.ByteBuffer buf;
        if (backEnd instanceof java.nio.ByteBuffer) {
            buf = (java.nio.ByteBuffer) backEnd;
        } else {
            byte[] array = buffer.toByteArray();
            if (Endianness.BIG_ENDIAN.equals(buffer.getEndianness())) {
                //reverse order
                Endianness.reverseOrder(array, buffer.getNumericType().getSizeInBytes());
            }
            buf = ByteBuffer.allocateDirect(array.length);
            buf.position(0);
            buf.put(array);
        }
        buf.position(0);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        return buf.asShortBuffer();
    }

    public static IntBuffer unwrapOrCopyInt(Buffer buffer) {
        if (buffer == null) return null;
        final Object backEnd = buffer.getBackEnd();

        if (backEnd instanceof java.nio.IntBuffer) {
            ((java.nio.Buffer) backEnd).position(0);
            return (java.nio.IntBuffer) backEnd;
        }

        java.nio.ByteBuffer buf;
        if (backEnd instanceof java.nio.ByteBuffer) {
            buf = (java.nio.ByteBuffer) backEnd;
        } else {
            byte[] array = buffer.toByteArray();
            if (Endianness.BIG_ENDIAN.equals(buffer.getEndianness())) {
                //reverse order
                Endianness.reverseOrder(array, buffer.getNumericType().getSizeInBytes());
            }
            buf = ByteBuffer.allocateDirect(array.length);
            buf.position(0);
            buf.put(array);
        }
        buf.position(0);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        return buf.asIntBuffer();
    }

    public static LongBuffer unwrapOrCopyLong(Buffer buffer) {
        if (buffer == null) return null;
        final Object backEnd = buffer.getBackEnd();

        if (backEnd instanceof java.nio.LongBuffer) {
            ((java.nio.Buffer) backEnd).position(0);
            return (java.nio.LongBuffer) backEnd;
        }

        java.nio.ByteBuffer buf;
        if (backEnd instanceof java.nio.ByteBuffer) {
            buf = (java.nio.ByteBuffer) backEnd;
        } else {
            byte[] array = buffer.toByteArray();
            if (Endianness.BIG_ENDIAN.equals(buffer.getEndianness())) {
                //reverse order
                Endianness.reverseOrder(array, buffer.getNumericType().getSizeInBytes());
            }
            buf = ByteBuffer.allocateDirect(array.length);
            buf.position(0);
            buf.put(array);
        }
        buf.position(0);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        return buf.asLongBuffer();
    }

    public static FloatBuffer unwrapOrCopyFloat(Buffer buffer) {
        if (buffer == null) return null;
        final Object backEnd = buffer.getBackEnd();

        if (backEnd instanceof java.nio.FloatBuffer) {
            ((java.nio.Buffer) backEnd).position(0);
            return (java.nio.FloatBuffer) backEnd;
        }

        java.nio.ByteBuffer buf;
        if (backEnd instanceof java.nio.ByteBuffer) {
            buf = (java.nio.ByteBuffer) backEnd;
        } else {
            byte[] array = buffer.toByteArray();
            if (Endianness.BIG_ENDIAN.equals(buffer.getEndianness())) {
                //reverse order
                Endianness.reverseOrder(array, buffer.getNumericType().getSizeInBytes());
            }
            buf = ByteBuffer.allocateDirect(array.length);
            buf.position(0);
            buf.put(array);
        }
        buf.position(0);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        return buf.asFloatBuffer();
    }

    public static DoubleBuffer unwrapOrCopyDouble(Buffer buffer) {
        if (buffer == null) return null;
        final Object backEnd = buffer.getBackEnd();

        if (backEnd instanceof java.nio.DoubleBuffer) {
            ((java.nio.Buffer) backEnd).position(0);
            return (java.nio.DoubleBuffer) backEnd;
        }

        java.nio.ByteBuffer buf;
        if (backEnd instanceof java.nio.ByteBuffer) {
            buf = (java.nio.ByteBuffer) backEnd;
        } else {
            byte[] array = buffer.toByteArray();
            if (Endianness.BIG_ENDIAN.equals(buffer.getEndianness())) {
                //reverse order
                Endianness.reverseOrder(array, buffer.getNumericType().getSizeInBytes());
            }
            buf = ByteBuffer.allocateDirect(array.length);
            buf.position(0);
            buf.put(array);
        }
        buf.position(0);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        return buf.asDoubleBuffer();
    }

}
