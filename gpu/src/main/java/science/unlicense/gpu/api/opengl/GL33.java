package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;

public interface GL33 extends science.unlicense.gpu.api.opengl.GL32 {

    /**
     * @param program
     * @param colorNumber
     * @param index
     * @param name
     */
     void glBindFragDataLocationIndexed (int program, int colorNumber, int index, science.unlicense.common.api.character.CharArray name);

    /**
     * @param program
     * @param name
     */
     int glGetFragDataIndex (int program, science.unlicense.common.api.character.CharArray name);

    /**
     * @param count
     * @param samplers ,length count
     */
     void glGenSamplers (Buffer.Int32 samplers);

    /**
     * @param count
     * @param samplers ,length count
     */
     void glDeleteSamplers (Buffer.Int32 samplers);

    /**
     * @param sampler
     */
     boolean glIsSampler (int sampler);

    /**
     * @param unit
     * @param sampler
     */
     void glBindSampler (int unit, int sampler);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameteri (int sampler, int pname, int param);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameteriv (int sampler, int pname, Buffer.Int32 param);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameterf (int sampler, int pname, float param);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameterfv (int sampler, int pname, Buffer.Float32 param);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameterIiv (int sampler, int pname, Buffer.Int32 param);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameterIuiv (int sampler, int pname, Buffer.Int32 param);

    /**
     * @param sampler
     * @param pname
     * @param params
     */
     void glGetSamplerParameteriv (int sampler, int pname, Buffer.Int32 params);

    /**
     * @param sampler
     * @param pname
     * @param params
     */
     void glGetSamplerParameterIiv (int sampler, int pname, Buffer.Int32 params);

    /**
     * @param sampler
     * @param pname
     * @param params
     */
     void glGetSamplerParameterfv (int sampler, int pname, Buffer.Float32 params);

    /**
     * @param sampler
     * @param pname
     * @param params
     */
     void glGetSamplerParameterIuiv (int sampler, int pname, Buffer.Int32 params);

    /**
     * @param id
     * @param target
     */
     void glQueryCounter (int id, int target);

    /**
     * @param id
     * @param pname
     * @param params
     */
     void glGetQueryObjecti64v (int id, int pname, Buffer.Int8 params);

    /**
     * @param id
     * @param pname
     * @param params
     */
     void glGetQueryObjectui64v (int id, int pname, Buffer.Int8 params);

    /**
     * @param index
     * @param divisor
     */
     void glVertexAttribDivisor (int index, int divisor);

    /**
     * @param index
     * @param type
     * @param normalized ,value from enumeration group Boolean
     * @param value
     */
     void glVertexAttribP1ui (int index, int type, boolean normalized, int value);

    /**
     * @param index
     * @param type
     * @param normalized ,value from enumeration group Boolean
     * @param value
     */
     void glVertexAttribP1uiv (int index, int type, boolean normalized, Buffer.Int32 value);

    /**
     * @param index
     * @param type
     * @param normalized ,value from enumeration group Boolean
     * @param value
     */
     void glVertexAttribP2ui (int index, int type, boolean normalized, int value);

    /**
     * @param index
     * @param type
     * @param normalized ,value from enumeration group Boolean
     * @param value
     */
     void glVertexAttribP2uiv (int index, int type, boolean normalized, Buffer.Int32 value);

    /**
     * @param index
     * @param type
     * @param normalized ,value from enumeration group Boolean
     * @param value
     */
     void glVertexAttribP3ui (int index, int type, boolean normalized, int value);

    /**
     * @param index
     * @param type
     * @param normalized ,value from enumeration group Boolean
     * @param value
     */
     void glVertexAttribP3uiv (int index, int type, boolean normalized, Buffer.Int32 value);

    /**
     * @param index
     * @param type
     * @param normalized ,value from enumeration group Boolean
     * @param value
     */
     void glVertexAttribP4ui (int index, int type, boolean normalized, int value);

    /**
     * @param index
     * @param type
     * @param normalized ,value from enumeration group Boolean
     * @param value
     */
     void glVertexAttribP4uiv (int index, int type, boolean normalized, Buffer.Int32 value);

    /**
     * @param type
     * @param value
     */
     void glVertexP2ui (int type, int value);

    /**
     * @param type
     * @param value
     */
     void glVertexP2uiv (int type, Buffer.Int32 value);

    /**
     * @param type
     * @param value
     */
     void glVertexP3ui (int type, int value);

    /**
     * @param type
     * @param value
     */
     void glVertexP3uiv (int type, Buffer.Int32 value);

    /**
     * @param type
     * @param value
     */
     void glVertexP4ui (int type, int value);

    /**
     * @param type
     * @param value
     */
     void glVertexP4uiv (int type, Buffer.Int32 value);

    /**
     * @param type
     * @param coords
     */
     void glTexCoordP1ui (int type, int coords);

    /**
     * @param type
     * @param coords
     */
     void glTexCoordP1uiv (int type, Buffer.Int32 coords);

    /**
     * @param type
     * @param coords
     */
     void glTexCoordP2ui (int type, int coords);

    /**
     * @param type
     * @param coords
     */
     void glTexCoordP2uiv (int type, Buffer.Int32 coords);

    /**
     * @param type
     * @param coords
     */
     void glTexCoordP3ui (int type, int coords);

    /**
     * @param type
     * @param coords
     */
     void glTexCoordP3uiv (int type, Buffer.Int32 coords);

    /**
     * @param type
     * @param coords
     */
     void glTexCoordP4ui (int type, int coords);

    /**
     * @param type
     * @param coords
     */
     void glTexCoordP4uiv (int type, Buffer.Int32 coords);

    /**
     * @param texture
     * @param type
     * @param coords
     */
     void glMultiTexCoordP1ui (int texture, int type, int coords);

    /**
     * @param texture
     * @param type
     * @param coords
     */
     void glMultiTexCoordP1uiv (int texture, int type, Buffer.Int32 coords);

    /**
     * @param texture
     * @param type
     * @param coords
     */
     void glMultiTexCoordP2ui (int texture, int type, int coords);

    /**
     * @param texture
     * @param type
     * @param coords
     */
     void glMultiTexCoordP2uiv (int texture, int type, Buffer.Int32 coords);

    /**
     * @param texture
     * @param type
     * @param coords
     */
     void glMultiTexCoordP3ui (int texture, int type, int coords);

    /**
     * @param texture
     * @param type
     * @param coords
     */
     void glMultiTexCoordP3uiv (int texture, int type, Buffer.Int32 coords);

    /**
     * @param texture
     * @param type
     * @param coords
     */
     void glMultiTexCoordP4ui (int texture, int type, int coords);

    /**
     * @param texture
     * @param type
     * @param coords
     */
     void glMultiTexCoordP4uiv (int texture, int type, Buffer.Int32 coords);

    /**
     * @param type
     * @param coords
     */
     void glNormalP3ui (int type, int coords);

    /**
     * @param type
     * @param coords
     */
     void glNormalP3uiv (int type, Buffer.Int32 coords);

    /**
     * @param type
     * @param color
     */
     void glColorP3ui (int type, int color);

    /**
     * @param type
     * @param color
     */
     void glColorP3uiv (int type, Buffer.Int32 color);

    /**
     * @param type
     * @param color
     */
     void glColorP4ui (int type, int color);

    /**
     * @param type
     * @param color
     */
     void glColorP4uiv (int type, Buffer.Int32 color);

    /**
     * @param type
     * @param color
     */
     void glSecondaryColorP3ui (int type, int color);

    /**
     * @param type
     * @param color
     */
     void glSecondaryColorP3uiv (int type, Buffer.Int32 color);

}
