
package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.BufferFactory;

/**
 *
 * @author Johann Sorel
 */
public interface GL {

    BufferFactory getBufferFactory();

    boolean isGL1();

    boolean isGL2();

    boolean isGL3();

    boolean isGL4();

    boolean isGL1ES();

    boolean isGL2ES2();

    boolean isGL2ES3();

    GL1 asGL1();

    GL2 asGL2();

    GL3 asGL3();

    GL4 asGL4();

    GL1ES asGL1ES();

    GL2ES2 asGL2ES2();

    GL2ES3 asGL2ES3();

}
