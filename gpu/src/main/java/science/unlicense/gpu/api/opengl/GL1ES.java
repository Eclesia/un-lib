
package science.unlicense.gpu.api.opengl;

/**
 *
 * @author Johann Sorel
 */
public interface GL1ES extends GL, GLES110 {

}
