
package science.unlicense.gpu.api;

/**
 * Raised when creating shared context resources fails.
 *
 * @author Johann Sorel
 */
public class SharedContextException extends RuntimeException {

    public SharedContextException() {
    }

}
