package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;

public interface GL32 extends science.unlicense.gpu.api.opengl.GL31 {

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     * @param basevertex
     */
     void glDrawElementsBaseVertex (int mode, int count, int type, long indices, int basevertex);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param start
     * @param end
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     * @param basevertex
     */
     void glDrawRangeElementsBaseVertex (int mode, int start, int end, int count, int type, long indices, int basevertex);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     * @param instancecount
     * @param basevertex
     */
     void glDrawElementsInstancedBaseVertex (int mode, int count, int type, long indices, int instancecount, int basevertex);

    /**
     * @param mode
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     * @param drawcount
     * @param basevertex
     */
     void glMultiDrawElementsBaseVertex (int mode, Buffer.Int32 count, int type, int indices, int drawcount, Buffer.Int32 basevertex);

    /**
     * @param mode
     */
     void glProvokingVertex (int mode);

    /**
     * @param condition
     * @param flags
     */
     long glFenceSync (int condition, int flags);

    /**
     * @param sync ,value from enumeration group sync
     */
     boolean glIsSync (long sync);

    /**
     * @param sync ,value from enumeration group sync
     */
     void glDeleteSync (long sync);

    /**
     * @param sync ,value from enumeration group sync
     * @param flags
     * @param timeout
     */
     int glClientWaitSync (long sync, int flags, long timeout);

    /**
     * @param sync ,value from enumeration group sync
     * @param flags
     * @param timeout
     */
     void glWaitSync (long sync, int flags, long timeout);

    /**
     * @param pname
     * @param data
     */
     void glGetInteger64v (int pname, Buffer.Int64 data);

    /**
     * Convenient method.
     *
     * @param pname
     * @param data
     */
     void glGetInteger64v (int pname, long[] data);

    /**
     * @param sync ,value from enumeration group sync
     * @param pname
     * @param bufSize
     * @param length
     * @param values ,length bufSize
     */
     void glGetSynciv (long sync, int pname, Buffer.Int32 length, Buffer.Int32 values);

    /**
     * @param target
     * @param index
     * @param data
     */
     void glGetInteger64i_v (int target, int index, Buffer.Int64 data);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param pname ,value from enumeration group BufferPNameARB
     * @param params
     */
     void glGetBufferParameteri64v (int target, int pname, Buffer.Int8 params);

    /**
     * @param target
     * @param attachment
     * @param texture
     * @param level
     */
     void glFramebufferTexture (int target, int attachment, int texture, int level);

    /**
     * @param target
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     * @param fixedsamplelocations ,value from enumeration group Boolean
     */
     void glTexImage2DMultisample (int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations);

    /**
     * @param target
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     * @param depth
     * @param fixedsamplelocations ,value from enumeration group Boolean
     */
     void glTexImage3DMultisample (int target, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations);

    /**
     * @param pname
     * @param index
     * @param val
     */
     void glGetMultisamplefv (int pname, int index, Buffer.Float32 val);

    /**
     * @param maskNumber
     * @param mask
     */
     void glSampleMaski (int maskNumber, int mask);

}
