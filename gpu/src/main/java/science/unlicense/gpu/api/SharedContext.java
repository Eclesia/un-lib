
package science.unlicense.gpu.api;

/**
 * The different GPU API, including OpenGL, OpenCL, Vulkan maybe have to
 * exchange datas without copying them to achieve higher performances.
 * Those shared objects are contained in a context which this class represent.
 *
 * @author Johann Sorel
 */
public interface SharedContext {

}
