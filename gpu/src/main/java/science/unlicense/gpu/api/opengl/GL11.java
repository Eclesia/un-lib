package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;

public interface GL11 extends science.unlicense.gpu.api.opengl.GL10 {

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param first
     * @param count
     */
     void glDrawArrays (int mode, int first, int count);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     */
     void glDrawElements (int mode, int count, int type, long indices);

    /**
     * @param pname ,value from enumeration group GetPointervPName
     * @param params
     */
     void glGetPointerv (int pname, Buffer.Int8 params);

    /**
     * @param factor
     * @param units
     */
     void glPolygonOffset (float factor, float units);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group PixelInternalFormat
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param border ,value from enumeration group CheckedInt32
     */
     void glCopyTexImage1D (int target, int level, int internalformat, int x, int y, int width, int border);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group PixelInternalFormat
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     * @param border ,value from enumeration group CheckedInt32
     */
     void glCopyTexImage2D (int target, int level, int internalformat, int x, int y, int width, int height, int border);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     */
     void glCopyTexSubImage1D (int target, int level, int xoffset, int x, int y, int width);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glCopyTexSubImage2D (int target, int level, int xoffset, int yoffset, int x, int y, int width, int height);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glTexSubImage1D (int target, int level, int xoffset, int width, int format, int type, Buffer.Int8 pixels);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glTexSubImage2D (int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer.Int8 pixels);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param texture ,value from enumeration group Texture
     */
     void glBindTexture (int target, int texture);

    /**
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
     */
     void glDeleteTextures (Buffer.Int32 textures);

     /**
      * Convenient method
      *
      * @param textures
      */
     void glDeleteTextures(int[] textures);

    /**
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
     */
     void glGenTextures (Buffer.Int32 textures);

     /**
      * Convenient method
      *
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
      */
     void glGenTextures(int[] textures);

    /**
     * @param texture ,value from enumeration group Texture
     */
     boolean glIsTexture (int texture);

    /**
     * @param i
     */
     void glArrayElement (int i);

    /**
     * @param size
     * @param type ,value from enumeration group ColorPointerType
     * @param stride
     * @param pointer
     */
     void glColorPointer (int size, int type, int stride, Buffer.Int8 pointer);

    /**
     * @param array ,value from enumeration group EnableCap
     */
     void glDisableClientState (int array);

    /**
     * @param stride
     * @param pointer
     */
     void glEdgeFlagPointer (int stride, Buffer.Int8 pointer);

    /**
     * @param array ,value from enumeration group EnableCap
     */
     void glEnableClientState (int array);

    /**
     * @param type ,value from enumeration group IndexPointerType
     * @param stride
     * @param pointer
     */
     void glIndexPointer (int type, int stride, Buffer.Int8 pointer);

    /**
     * @param format ,value from enumeration group InterleavedArrayFormat
     * @param stride
     * @param pointer
     */
     void glInterleavedArrays (int format, int stride, Buffer.Int8 pointer);

    /**
     * @param type ,value from enumeration group NormalPointerType
     * @param stride
     * @param pointer
     */
     void glNormalPointer (int type, int stride, Buffer.Int8 pointer);

    /**
     * @param size
     * @param type ,value from enumeration group TexCoordPointerType
     * @param stride
     * @param pointer
     */
     void glTexCoordPointer (int size, int type, int stride, Buffer.Int8 pointer);

    /**
     * @param size
     * @param type ,value from enumeration group VertexPointerType
     * @param stride
     * @param pointer
     */
     void glVertexPointer (int size, int type, int stride, Buffer.Int8 pointer);

    /**
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
     * @param residences ,value from enumeration group Boolean ,length n
     */
     boolean glAreTexturesResident (Buffer.Int32 textures, Buffer.Int8 residences);

    /**
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
     * @param priorities ,length n
     */
     void glPrioritizeTextures (Buffer.Int32 textures, Buffer.Float32 priorities);

    /**
     * @param c ,value from enumeration group ColorIndexValueUB
     */
     void glIndexub (byte c);

    /**
     * @param c ,value from enumeration group ColorIndexValueUB
     */
     void glIndexubv (Buffer.Int8 c);

     void glPopClientAttrib ();

    /**
     * @param mask ,value from enumeration group ClientAttribMask
     */
     void glPushClientAttrib (int mask);

}
