package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;

public interface GL15 extends science.unlicense.gpu.api.opengl.GL14 {

    /**
     * @param n
     * @param ids ,length n
     */
     void glGenQueries (Buffer.Int32 ids);

    /**
     * Convenient method.
     *
     * @param n
     * @param ids ,length n
     */
     void glGenQueries (int[] ids);

    /**
     * @param n
     * @param ids ,length n
     */
     void glDeleteQueries (Buffer.Int32 ids);

    /**
     * Convenient method.
     *
     * @param n
     * @param ids ,length n
     */
     void glDeleteQueries (int[] ids);

    /**
     * @param id
     */
     boolean glIsQuery (int id);

    /**
     * @param target
     * @param id
     */
     void glBeginQuery (int target, int id);

    /**
     * @param target
     */
     void glEndQuery (int target);

    /**
     * @param target
     * @param pname
     * @param params
     */
     void glGetQueryiv (int target, int pname, Buffer.Int32 params);

    /**
     * @param id
     * @param pname
     * @param params
     */
     void glGetQueryObjectiv (int id, int pname, Buffer.Int32 params);

    /**
     * @param id
     * @param pname
     * @param params
     */
     void glGetQueryObjectuiv (int id, int pname, Buffer.Int32 params);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param buffer
     */
     void glBindBuffer (int target, int buffer);

    /**
     * @param n
     * @param buffers ,length n
     */
     void glDeleteBuffers (Buffer.Int32 buffers);

    /**
     * Convenient method.
     *
     * @param n
     * @param buffers ,length n
     */
     void glDeleteBuffers (int[] buffers);

    /**
     * @param n
     * @param buffers ,length n
     */
     void glGenBuffers (Buffer.Int32 buffers);

    /**
     * Convenient method.
     *
     * @param n
     * @param buffers ,length n
     */
     void glGenBuffers (int[] buffers);

    /**
     * @param buffer
     */
     boolean glIsBuffer (int buffer);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param size ,value from enumeration group BufferSize
     * @param data ,length size
     * @param usage ,value from enumeration group BufferUsageARB
     */
     void glBufferData (int target, Buffer data, int usage);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param offset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     * @param data ,length size
     */
     void glBufferSubData (int target, long offset, Buffer data);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param offset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     * @param data ,length size
     */
     void glGetBufferSubData (int target, long offset, Buffer data);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param access ,value from enumeration group BufferAccessARB
     */
     void glMapBuffer (int target, int access);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     */
     boolean glUnmapBuffer (int target);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param pname ,value from enumeration group BufferPNameARB
     * @param params
     */
     void glGetBufferParameteriv (int target, int pname, Buffer.Int32 params);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param pname ,value from enumeration group BufferPointerNameARB
     * @param params
     */
     void glGetBufferPointerv (int target, int pname, Buffer.Int8 params);

}
