
package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.gpu.api.SharedContext;
import science.unlicense.gpu.api.SharedContextException;

/**
 *
 * @author Johann Sorel
 */
public interface GLBinding {

    GLSource createOffScreen(int width, int height, SharedContext context) throws SharedContextException;

    GLFrame createFrame(boolean opaque, SharedContext context) throws SharedContextException;

    /**
     * Returns the default frame margins.
     * @return
     */
    Margin getFrameMargin();

    BufferFactory getBufferFactory();

}
