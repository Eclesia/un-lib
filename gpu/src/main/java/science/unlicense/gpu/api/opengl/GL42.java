package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;

public interface GL42 extends science.unlicense.gpu.api.opengl.GL41 {

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param first
     * @param count
     * @param instancecount
     * @param baseinstance
     */
     void glDrawArraysInstancedBaseInstance (int mode, int first, int count, int instancecount, int baseinstance);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type
     * @param indices ,length count
     * @param instancecount
     * @param baseinstance
     */
     void glDrawElementsInstancedBaseInstance (int mode, int type, long indices, int instancecount, int baseinstance);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type
     * @param indices ,length count
     * @param instancecount
     * @param basevertex
     * @param baseinstance
     */
     void glDrawElementsInstancedBaseVertexBaseInstance (int mode, int type, long indices, int instancecount, int basevertex, int baseinstance);

    /**
     * @param target
     * @param internalformat
     * @param pname
     * @param bufSize
     * @param params ,length bufSize
     */
     void glGetInternalformativ (int target, int internalformat, int pname, Buffer.Int32 params);

    /**
     * @param program
     * @param bufferIndex
     * @param pname
     * @param params
     */
     void glGetActiveAtomicCounterBufferiv (int program, int bufferIndex, int pname, Buffer.Int32 params);

    /**
     * @param unit
     * @param texture
     * @param level
     * @param layered ,value from enumeration group Boolean
     * @param layer
     * @param access
     * @param format
     */
     void glBindImageTexture (int unit, int texture, int level, boolean layered, int layer, int access, int format);

    /**
     * @param barriers
     */
     void glMemoryBarrier (int barriers);

    /**
     * @param target
     * @param levels
     * @param internalformat
     * @param width
     */
     void glTexStorage1D (int target, int levels, int internalformat, int width);

    /**
     * @param target
     * @param levels
     * @param internalformat
     * @param width
     * @param height
     */
     void glTexStorage2D (int target, int levels, int internalformat, int width, int height);

    /**
     * @param target
     * @param levels
     * @param internalformat
     * @param width
     * @param height
     * @param depth
     */
     void glTexStorage3D (int target, int levels, int internalformat, int width, int height, int depth);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param id
     * @param instancecount
     */
     void glDrawTransformFeedbackInstanced (int mode, int id, int instancecount);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param id
     * @param stream
     * @param instancecount
     */
     void glDrawTransformFeedbackStreamInstanced (int mode, int id, int stream, int instancecount);

}
