
package science.unlicense.gpu.api;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int64;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt8;

/**
 * Buffer factory optimized for OpenGL.
 *
 * @author Johann Sorel
 */
public class GLBufferFactory implements BufferFactory {

    public static final GLBufferFactory INSTANCE = new GLBufferFactory();

    private GLBuffer createByte(long nbValue, NumberType bestType, ByteOrder order) {
        ByteBuffer buffer = ByteBuffer.allocateDirect((int) nbValue);
        buffer = buffer.order(order);
        return new GLBuffer(buffer,bestType);
    }

    @Override
    public GLBuffer.Int8 createInt8(long nbValue) {
        return createByte(nbValue, UInt8.TYPE, ByteOrder.LITTLE_ENDIAN).asInt8();
    }

    @Override
    public GLBuffer.Int32 createInt32(long nbValue) {
        return createByte(nbValue*4, Int32.TYPE, ByteOrder.LITTLE_ENDIAN).asInt32();
    }

    @Override
    public GLBuffer.Int64 createInt64(long nbValue) {
        return createByte(nbValue*8, Int64.TYPE, ByteOrder.LITTLE_ENDIAN).asInt64();
    }

    @Override
    public GLBuffer.Float32 createFloat32(long nbValue) {
        return createByte(nbValue*4, Float32.TYPE, ByteOrder.LITTLE_ENDIAN).asFloat32();
    }

    @Override
    public GLBuffer.Float64 createFloat64(long nbValue) {
        return createByte(nbValue*8, Float64.TYPE, ByteOrder.LITTLE_ENDIAN).asFloat64();
    }

    @Override
    public Buffer create(long nbValue, NumberType numType, Endianness endianness) {
        return createByte(nbValue*numType.getSizeInBytes(), numType,
                Endianness.BIG_ENDIAN.equals(endianness) ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
    }

}
