package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.character.CharArray;

public interface GLES220 {

    /**
     * @param texture ,value from enumeration group TextureUnit
     */
     void glActiveTexture (int texture);

    /**
     * @param program
     * @param shader
     */
     void glAttachShader (int program, int shader);

    /**
     * @param program
     * @param index
     * @param name
     */
     void glBindAttribLocation (int program, int index, science.unlicense.common.api.character.CharArray name);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param buffer
     */
     void glBindBuffer (int target, int buffer);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param framebuffer
     */
     void glBindFramebuffer (int target, int framebuffer);

    /**
     * @param target ,value from enumeration group RenderbufferTarget
     * @param renderbuffer
     */
     void glBindRenderbuffer (int target, int renderbuffer);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param texture ,value from enumeration group Texture
     */
     void glBindTexture (int target, int texture);

    /**
     * @param red ,value from enumeration group ColorF
     * @param green ,value from enumeration group ColorF
     * @param blue ,value from enumeration group ColorF
     * @param alpha ,value from enumeration group ColorF
     */
     void glBlendColor (float red, float green, float blue, float alpha);

    /**
     * @param mode ,value from enumeration group BlendEquationMode
     */
     void glBlendEquation (int mode);

    /**
     * @param modeRGB ,value from enumeration group BlendEquationModeEXT
     * @param modeAlpha ,value from enumeration group BlendEquationModeEXT
     */
     void glBlendEquationSeparate (int modeRGB, int modeAlpha);

    /**
     * @param sfactor ,value from enumeration group BlendingFactorSrc
     * @param dfactor ,value from enumeration group BlendingFactorDest
     */
     void glBlendFunc (int sfactor, int dfactor);

    /**
     * @param sfactorRGB ,value from enumeration group BlendFuncSeparateParameterEXT
     * @param dfactorRGB ,value from enumeration group BlendFuncSeparateParameterEXT
     * @param sfactorAlpha ,value from enumeration group BlendFuncSeparateParameterEXT
     * @param dfactorAlpha ,value from enumeration group BlendFuncSeparateParameterEXT
     */
     void glBlendFuncSeparate (int sfactorRGB, int dfactorRGB, int sfactorAlpha, int dfactorAlpha);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param size ,value from enumeration group BufferSize
     * @param data ,length size
     * @param usage ,value from enumeration group BufferUsageARB
     */
     void glBufferData (int target, Buffer data, int usage);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param offset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     * @param data ,length size
     */
     void glBufferSubData (int target, long offset, Buffer data);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     */
     int glCheckFramebufferStatus (int target);

    /**
     * @param mask ,value from enumeration group ClearBufferMask
     */
     void glClear (int mask);

    /**
     * @param red ,value from enumeration group ColorF
     * @param green ,value from enumeration group ColorF
     * @param blue ,value from enumeration group ColorF
     * @param alpha ,value from enumeration group ColorF
     */
     void glClearColor (float red, float green, float blue, float alpha);

    /**
     * @param d
     */
     void glClearDepthf (float d);

    /**
     * @param s ,value from enumeration group StencilValue
     */
     void glClearStencil (int s);

    /**
     * @param red ,value from enumeration group Boolean
     * @param green ,value from enumeration group Boolean
     * @param blue ,value from enumeration group Boolean
     * @param alpha ,value from enumeration group Boolean
     */
     void glColorMask (boolean red, boolean green, boolean blue, boolean alpha);

    /**
     * @param shader
     */
     void glCompileShader (int shader);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group PixelInternalFormat
     * @param width
     * @param height
     * @param border ,value from enumeration group CheckedInt32
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexImage2D (int target, int level, int internalformat, int width, int height, int border, Buffer data);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexSubImage2D (int target, int level, int xoffset, int yoffset, int width, int height, int format, Buffer data);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group PixelInternalFormat
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     * @param border ,value from enumeration group CheckedInt32
     */
     void glCopyTexImage2D (int target, int level, int internalformat, int x, int y, int width, int height, int border);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glCopyTexSubImage2D (int target, int level, int xoffset, int yoffset, int x, int y, int width, int height);

     int glCreateProgram ();

    /**
     * @param type
     */
     int glCreateShader (int type);

    /**
     * @param mode ,value from enumeration group CullFaceMode
     */
     void glCullFace (int mode);

    /**
     * @param n
     * @param buffers ,length n
     */
     void glDeleteBuffers (Buffer.Int32 buffers);

    /**
     * Convenient method.
     *
     * @param n
     * @param buffers ,length n
     */
     void glDeleteBuffers (int[] buffers);

    /**
     * @param n
     * @param framebuffers ,length n
     */
     void glDeleteFramebuffers (Buffer.Int32 framebuffers);

     /**
      * Convinient method.
      *
      * @param framebuffers
      */
     void glDeleteFramebuffers(int[] framebuffers);

    /**
     * @param program
     */
     void glDeleteProgram (int program);

    /**
     * @param n
     * @param renderbuffers ,length n
     */
     void glDeleteRenderbuffers (Buffer.Int32 renderbuffers);

    /**
     * @param shader
     */
     void glDeleteShader (int shader);

    /**
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
     */
     void glDeleteTextures (Buffer.Int32 textures);

    /**
     * @param func ,value from enumeration group DepthFunction
     */
     void glDepthFunc (int func);

    /**
     * @param flag ,value from enumeration group Boolean
     */
     void glDepthMask (boolean flag);

    /**
     * @param n
     * @param f
     */
     void glDepthRangef (float n, float f);

    /**
     * @param program
     * @param shader
     */
     void glDetachShader (int program, int shader);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     void glDisable (int cap);

    /**
     * @param index
     */
     void glDisableVertexAttribArray (int index);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param first
     * @param count
     */
     void glDrawArrays (int mode, int first, int count);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     */
     void glDrawElements (int mode, int count, int type, long indices);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     void glEnable (int cap);

    /**
     * @param index
     */
     void glEnableVertexAttribArray (int index);

     void glFinish ();

     void glFlush ();

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param renderbuffertarget ,value from enumeration group RenderbufferTarget
     * @param renderbuffer
     */
     void glFramebufferRenderbuffer (int target, int attachment, int renderbuffertarget, int renderbuffer);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param textarget
     * @param texture
     * @param level
     */
     void glFramebufferTexture2D (int target, int attachment, int textarget, int texture, int level);

    /**
     * @param mode ,value from enumeration group FrontFaceDirection
     */
     void glFrontFace (int mode);

    /**
     * @param n
     * @param buffers ,length n
     */
     void glGenBuffers (Buffer.Int32 buffers);

    /**
     * Convenient method.
     *
     * @param n
     * @param buffers ,length n
     */
     void glGenBuffers (int[] buffers);

    /**
     * @param target
     */
     void glGenerateMipmap (int target);

    /**
     * @param n
     * @param framebuffers ,length n
     */
     void glGenFramebuffers (Buffer.Int32 framebuffers);

    /**
     * Convinient method.
     *
     * @param n
     * @param framebuffers ,length n
     */
     void glGenFramebuffers (int[] framebuffers);

    /**
     * @param n
     * @param renderbuffers ,length n
     */
     void glGenRenderbuffers (Buffer.Int32 renderbuffers);

    /**
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
     */
     void glGenTextures (Buffer.Int32 textures);

     /**
      * Convenient method
      *
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
      */
     void glGenTextures(int[] textures);

    /**
     * @param program
     * @param index
     * @param bufSize
     * @param length
     * @param size
     * @param type
     * @param name ,length bufSize
     */
     void glGetActiveAttrib (int program, int index, Buffer.Int32 length, Buffer.Int32 size, Buffer.Int32 type, Buffer.Int8 name);

    /**
     * @param program
     * @param index
     * @param bufSize
     * @param length
     * @param size
     * @param type
     * @param name ,length bufSize
     */
     void glGetActiveUniform (int program, int index, Buffer.Int32 length, Buffer.Int32 size, Buffer.Int32 type, Buffer.Int8 name);

    /**
     * @param program
     * @param maxCount
     * @param count
     * @param shaders ,length maxCount
     */
     void glGetAttachedShaders (int program, Buffer.Int32 count, Buffer.Int32 shaders);

    /**
     * @param program
     * @param name
     */
     int glGetAttribLocation (int program, science.unlicense.common.api.character.CharArray name);

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data ,value from enumeration group Boolean
     */
     void glGetBooleanv (int pname, Buffer.Int8 data);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param pname ,value from enumeration group BufferPNameARB
     * @param params
     */
     void glGetBufferParameteriv (int target, int pname, Buffer.Int32 params);

     int glGetError ();

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetFloatv (int pname, Buffer.Float32 data);

    /**
     * Convenient method.
     *
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetFloatv (int pname, float[] data);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param pname
     * @param params
     */
     void glGetFramebufferAttachmentParameteriv (int target, int attachment, int pname, Buffer.Int32 params);

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetIntegerv (int pname, Buffer.Int32 data);

    /**
     * Convenient method.
     *
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetIntegerv (int pname, int[] data);

    /**
     * @param program
     * @param pname
     * @param params
     */
     void glGetProgramiv (int program, int pname, Buffer.Int32 params);

    /**
     * Convenient method.
     *
     * @param program
     * @param pname
     * @param params
     */
     void glGetProgramiv (int program, int pname, int[] params);

    /**
     * @param program
     * @param bufSize
     * @param length
     * @param infoLog ,length bufSize
     */
     void glGetProgramInfoLog (int program, int[] length, Buffer.Int8 infoLog);

    /**
     * @param target ,value from enumeration group RenderbufferTarget
     * @param pname
     * @param params
     */
     void glGetRenderbufferParameteriv (int target, int pname, Buffer.Int32 params);

    /**
     * @param shader
     * @param pname
     * @param params
     */
     void glGetShaderiv (int shader, int pname, Buffer.Int32 params);

    /**
     * Convenient method.
     *
     * @param shader
     * @param pname
     * @param params
     */
     void glGetShaderiv (int shader, int pname, int[] params);

    /**
     * @param shader
     * @param bufSize
     * @param length
     * @param infoLog ,length bufSize
     */
     void glGetShaderInfoLog (int shader, Buffer.Int32 length, Buffer.Int8 infoLog);

    /**
     * @param shadertype
     * @param precisiontype
     * @param range
     * @param precision
     */
     void glGetShaderPrecisionFormat (int shadertype, int precisiontype, Buffer.Int32 range, Buffer.Int32 precision);

    /**
     * @param shader
     * @param bufSize
     * @param length
     * @param source ,length bufSize
     */
     void glGetShaderSource (int shader, Buffer.Int32 length, Buffer.Int8 source);

    /**
     * @param name ,value from enumeration group StringName
     */
     byte glGetString (int name);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameterfv (int target, int pname, Buffer.Float32 params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameteriv (int target, int pname, Buffer.Int32 params);

    /**
     * @param program
     * @param location
     * @param params
     */
     void glGetUniformfv (int program, int location, Buffer.Float32 params);

    /**
     * @param program
     * @param location
     * @param params
     */
     void glGetUniformiv (int program, int location, Buffer.Int32 params);

    /**
     * @param program
     * @param name
     */
     int glGetUniformLocation (int program, science.unlicense.common.api.character.CharArray name);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribPropertyARB
     * @param params
     */
     void glGetVertexAttribfv (int index, int pname, Buffer.Float32 params);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribPropertyARB
     * @param params
     */
     void glGetVertexAttribiv (int index, int pname, Buffer.Int32 params);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribPointerPropertyARB
     * @param pointer
     */
     void glGetVertexAttribPointerv (int index, int pname, Buffer.Int8 pointer);

    /**
     * @param target ,value from enumeration group HintTarget
     * @param mode ,value from enumeration group HintMode
     */
     void glHint (int target, int mode);

    /**
     * @param buffer
     */
     boolean glIsBuffer (int buffer);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     boolean glIsEnabled (int cap);

    /**
     * @param framebuffer
     */
     boolean glIsFramebuffer (int framebuffer);

    /**
     * @param program
     */
     boolean glIsProgram (int program);

    /**
     * @param renderbuffer
     */
     boolean glIsRenderbuffer (int renderbuffer);

    /**
     * @param shader
     */
     boolean glIsShader (int shader);

    /**
     * @param texture ,value from enumeration group Texture
     */
     boolean glIsTexture (int texture);

    /**
     * @param width ,value from enumeration group CheckedFloat32
     */
     void glLineWidth (float width);

    /**
     * @param program
     */
     void glLinkProgram (int program);

    /**
     * @param pname ,value from enumeration group PixelStoreParameter
     * @param param ,value from enumeration group CheckedInt32
     */
     void glPixelStorei (int pname, int param);

    /**
     * @param factor
     * @param units
     */
     void glPolygonOffset (float factor, float units);

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glReadPixels (int x, int y, int width, int height, int format, int type, Buffer.Int8 pixels);

     void glReleaseShaderCompiler ();

    /**
     * @param target ,value from enumeration group RenderbufferTarget
     * @param internalformat
     * @param width
     * @param height
     */
     void glRenderbufferStorage (int target, int internalformat, int width, int height);

    /**
     * @param value
     * @param invert ,value from enumeration group Boolean
     */
     void glSampleCoverage (float value, boolean invert);

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glScissor (int x, int y, int width, int height);

    /**
     * @param count
     * @param shaders ,length count
     * @param binaryformat
     * @param binary ,length length
     * @param length
     */
     void glShaderBinary (Buffer.Int32 shaders, int binaryformat, Buffer.Int8 binary);

    /**
     * @param shader
     * @param count
     * @param string ,length count
     * @param length ,length count
     */
     void glShaderSource (int shader, CharArray[] strings);

    /**
     * @param func ,value from enumeration group StencilFunction
     * @param ref ,value from enumeration group StencilValue
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilFunc (int func, int ref, int mask);

    /**
     * @param face ,value from enumeration group StencilFaceDirection
     * @param func ,value from enumeration group StencilFunction
     * @param ref ,value from enumeration group StencilValue
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilFuncSeparate (int face, int func, int ref, int mask);

    /**
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilMask (int mask);

    /**
     * @param face ,value from enumeration group StencilFaceDirection
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilMaskSeparate (int face, int mask);

    /**
     * @param fail ,value from enumeration group StencilOp
     * @param zfail ,value from enumeration group StencilOp
     * @param zpass ,value from enumeration group StencilOp
     */
     void glStencilOp (int fail, int zfail, int zpass);

    /**
     * @param face ,value from enumeration group StencilFaceDirection
     * @param sfail ,value from enumeration group StencilOp
     * @param dpfail ,value from enumeration group StencilOp
     * @param dppass ,value from enumeration group StencilOp
     */
     void glStencilOpSeparate (int face, int sfail, int dpfail, int dppass);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group TextureComponentCount
     * @param width
     * @param height
     * @param border ,value from enumeration group CheckedInt32
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glTexImage2D (int target, int level, int internalformat, int width, int height, int border, int format, int type, Buffer pixels);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glTexParameterf (int target, int pname, float param);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glTexParameterfv (int target, int pname, Buffer.Float32 params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param param ,value from enumeration group CheckedInt32
     */
     void glTexParameteri (int target, int pname, int param);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params ,value from enumeration group CheckedInt32
     */
     void glTexParameteriv (int target, int pname, Buffer.Int32 params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glTexSubImage2D (int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer.Int8 pixels);

    /**
     * @param location
     * @param v0
     */
     void glUniform1f (int location, float v0);

    /**
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1fv (int location, Buffer.Float32 value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1fv (int location, float[] value);

    /**
     * @param location
     * @param v0
     */
     void glUniform1i (int location, int v0);

    /**
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1iv (int location, Buffer.Int32 value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1iv (int location, int[] value);

    /**
     * @param location
     * @param v0
     * @param v1
     */
     void glUniform2f (int location, float v0, float v1);

    /**
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glUniform2fv (int location, Buffer.Float32 value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform2fv (int location, float[] value);
    /**
     * @param location
     * @param v0
     * @param v1
     */
     void glUniform2i (int location, int v0, int v1);

    /**
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glUniform2iv (int location, Buffer.Int32 value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform2iv (int location, int[] value);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glUniform3f (int location, float v0, float v1, float v2);

    /**
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glUniform3fv (int location, Buffer.Float32 value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform3fv (int location, float[] value);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glUniform3i (int location, int v0, int v1, int v2);

    /**
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glUniform3iv (int location, Buffer.Int32 value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform3iv (int location, int[] value);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glUniform4f (int location, float v0, float v1, float v2, float v3);

    /**
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glUniform4fv (int location, Buffer.Float32 value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform4fv (int location, float[] value);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glUniform4i (int location, int v0, int v1, int v2, int v3);

    /**
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glUniform4iv (int location, Buffer.Int32 value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform4iv (int location, int[] value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*4
     */
     void glUniformMatrix2fv (int location, boolean transpose, Buffer.Float32 value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*4
     */
     void glUniformMatrix2fv (int location, boolean transpose, float[] value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*9
     */
     void glUniformMatrix3fv (int location, boolean transpose, Buffer.Float32 value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*9
     */
     void glUniformMatrix3fv (int location, boolean transpose, float[] value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*16
     */
     void glUniformMatrix4fv (int location, boolean transpose, Buffer.Float32 value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*16
     */
     void glUniformMatrix4fv (int location, boolean transpose, float[] value);

    /**
     * @param program
     */
     void glUseProgram (int program);

    /**
     * @param program
     */
     void glValidateProgram (int program);

    /**
     * @param index
     * @param x
     */
     void glVertexAttrib1f (int index, float x);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib1fv (int index, Buffer.Float32 v);

    /**
     * @param index
     * @param x
     * @param y
     */
     void glVertexAttrib2f (int index, float x, float y);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib2fv (int index, Buffer.Float32 v);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     */
     void glVertexAttrib3f (int index, float x, float y, float z);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib3fv (int index, Buffer.Float32 v);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glVertexAttrib4f (int index, float x, float y, float z, float w);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4fv (int index, Buffer.Float32 v);

    /**
     * @param index
     * @param size
     * @param type ,value from enumeration group VertexAttribPointerType
     * @param normalized ,value from enumeration group Boolean
     * @param stride
     * @param pointer
     */
     void glVertexAttribPointer (int index, int size, int type, boolean normalized, int stride, long pointer);

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glViewport (int x, int y, int width, int height);

}
