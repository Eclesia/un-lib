
package science.unlicense.gpu.api.opengl;

/**
 *
 * @author Johann Sorel
 */
public interface GLCallback {


    void execute(GLSource source);

    void dispose(GLSource source);

}
