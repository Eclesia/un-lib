
package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.gpu.api.SharedContext;

/**
 *
 * @author Johann Sorel
 */
public interface GLSource {

    /**
     * Returns the original driver/implementation which created this OpenGL instance.
     * @return
     */
    GLBinding getBinding();

    /**
     * Returns the shared objects context.
     * @return
     */
    SharedContext getContext();

    GL getGL();

    int getX();

    int getY();

    int getWidth();

    int getHeight();

    Sequence getCallbacks();

    void render();

    void dispose();

}
