package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;

public interface GL21 extends science.unlicense.gpu.api.opengl.GL20 {

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glUniformMatrix2x3fv (int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glUniformMatrix3x2fv (int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glUniformMatrix2x4fv (int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glUniformMatrix4x2fv (int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glUniformMatrix3x4fv (int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glUniformMatrix4x3fv (int location, boolean transpose, Buffer.Float32 value);

}
