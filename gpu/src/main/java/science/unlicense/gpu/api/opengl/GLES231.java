package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;

public interface GLES231 extends science.unlicense.gpu.api.opengl.GLES230 {

    /**
     * @param num_groups_x
     * @param num_groups_y
     * @param num_groups_z
     */
     void glDispatchCompute (int num_groups_x, int num_groups_y, int num_groups_z);

    /**
     * @param indirect ,value from enumeration group BufferOffset
     */
     void glDispatchComputeIndirect (long indirect);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param indirect
     */
     void glDrawArraysIndirect (int mode, long indirect);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param type
     * @param indirect
     */
     void glDrawElementsIndirect (int mode, int type, long indirect);

    /**
     * @param target
     * @param pname
     * @param param
     */
     void glFramebufferParameteri (int target, int pname, int param);

    /**
     * @param target
     * @param pname
     * @param params
     */
     void glGetFramebufferParameteriv (int target, int pname, Buffer.Int32 params);

    /**
     * @param program
     * @param programInterface
     * @param pname
     * @param params
     */
     void glGetProgramInterfaceiv (int program, int programInterface, int pname, Buffer.Int32 params);

    /**
     * @param program
     * @param programInterface
     * @param name
     */
     int glGetProgramResourceIndex (int program, int programInterface, science.unlicense.common.api.character.CharArray name);

    /**
     * @param program
     * @param programInterface
     * @param index
     * @param bufSize
     * @param length
     * @param name ,length bufSize
     */
     void glGetProgramResourceName (int program, int programInterface, int index, Buffer.Int32 length, Buffer.Int8 name);

    /**
     * @param program
     * @param programInterface
     * @param index
     * @param propCount
     * @param props ,length propCount
     * @param bufSize
     * @param length
     * @param params ,length bufSize
     */
     void glGetProgramResourceiv (int program, int programInterface, int index, Buffer.Int32 props, Buffer.Int32 length, Buffer.Int32 params);

    /**
     * @param program
     * @param programInterface
     * @param name
     */
     int glGetProgramResourceLocation (int program, int programInterface, science.unlicense.common.api.character.CharArray name);

    /**
     * @param pipeline
     * @param stages
     * @param program
     */
     void glUseProgramStages (int pipeline, int stages, int program);

    /**
     * @param pipeline
     * @param program
     */
     void glActiveShaderProgram (int pipeline, int program);

    /**
     * @param type
     * @param count
     * @param strings ,length count
     */
     int glCreateShaderProgramv (int type, Buffer.Int8 strings);

    /**
     * @param pipeline
     */
     void glBindProgramPipeline (int pipeline);

    /**
     * @param n
     * @param pipelines ,length n
     */
     void glDeleteProgramPipelines (Buffer.Int32 pipelines);

    /**
     * @param n
     * @param pipelines ,length n
     */
     void glGenProgramPipelines (Buffer.Int32 pipelines);

    /**
     * @param pipeline
     */
     boolean glIsProgramPipeline (int pipeline);

    /**
     * @param pipeline
     * @param pname
     * @param params
     */
     void glGetProgramPipelineiv (int pipeline, int pname, Buffer.Int32 params);

    /**
     * @param program
     * @param location
     * @param v0
     */
     void glProgramUniform1i (int program, int location, int v0);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     */
     void glProgramUniform2i (int program, int location, int v0, int v1);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glProgramUniform3i (int program, int location, int v0, int v1, int v2);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glProgramUniform4i (int program, int location, int v0, int v1, int v2, int v3);

    /**
     * @param program
     * @param location
     * @param v0
     */
     void glProgramUniform1ui (int program, int location, int v0);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     */
     void glProgramUniform2ui (int program, int location, int v0, int v1);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glProgramUniform3ui (int program, int location, int v0, int v1, int v2);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glProgramUniform4ui (int program, int location, int v0, int v1, int v2, int v3);

    /**
     * @param program
     * @param location
     * @param v0
     */
     void glProgramUniform1f (int program, int location, float v0);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     */
     void glProgramUniform2f (int program, int location, float v0, float v1);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glProgramUniform3f (int program, int location, float v0, float v1, float v2);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glProgramUniform4f (int program, int location, float v0, float v1, float v2, float v3);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count
     */
     void glProgramUniform1iv (int program, int location, Buffer.Int32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glProgramUniform2iv (int program, int location, Buffer.Int32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glProgramUniform3iv (int program, int location, Buffer.Int32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glProgramUniform4iv (int program, int location, Buffer.Int32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count
     */
     void glProgramUniform1uiv (int program, int location, Buffer.Int32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glProgramUniform2uiv (int program, int location, Buffer.Int32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glProgramUniform3uiv (int program, int location, Buffer.Int32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glProgramUniform4uiv (int program, int location, Buffer.Int32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count
     */
     void glProgramUniform1fv (int program, int location, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glProgramUniform2fv (int program, int location, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glProgramUniform3fv (int program, int location, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glProgramUniform4fv (int program, int location, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*4
     */
     void glProgramUniformMatrix2fv (int program, int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*9
     */
     void glProgramUniformMatrix3fv (int program, int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*16
     */
     void glProgramUniformMatrix4fv (int program, int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glProgramUniformMatrix2x3fv (int program, int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glProgramUniformMatrix3x2fv (int program, int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glProgramUniformMatrix2x4fv (int program, int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glProgramUniformMatrix4x2fv (int program, int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glProgramUniformMatrix3x4fv (int program, int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glProgramUniformMatrix4x3fv (int program, int location, boolean transpose, Buffer.Float32 value);

    /**
     * @param pipeline
     */
     void glValidateProgramPipeline (int pipeline);

    /**
     * @param pipeline
     * @param bufSize
     * @param length
     * @param infoLog ,length bufSize
     */
     void glGetProgramPipelineInfoLog (int pipeline, Buffer.Int32 length, Buffer.Int8 infoLog);

    /**
     * @param unit
     * @param texture
     * @param level
     * @param layered ,value from enumeration group Boolean
     * @param layer
     * @param access
     * @param format
     */
     void glBindImageTexture (int unit, int texture, int level, boolean layered, int layer, int access, int format);

    /**
     * @param target
     * @param index
     * @param data ,value from enumeration group Boolean
     */
     void glGetBooleani_v (int target, int index, Buffer.Int8 data);

    /**
     * @param barriers
     */
     void glMemoryBarrier (int barriers);

    /**
     * @param barriers
     */
     void glMemoryBarrierByRegion (int barriers);

    /**
     * @param target
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     * @param fixedsamplelocations ,value from enumeration group Boolean
     */
     void glTexStorage2DMultisample (int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations);

    /**
     * @param pname
     * @param index
     * @param val
     */
     void glGetMultisamplefv (int pname, int index, Buffer.Float32 val);

    /**
     * @param maskNumber
     * @param mask
     */
     void glSampleMaski (int maskNumber, int mask);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexLevelParameteriv (int target, int level, int pname, Buffer.Int32 params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexLevelParameterfv (int target, int level, int pname, Buffer.Float32 params);

    /**
     * @param bindingindex
     * @param buffer
     * @param offset ,value from enumeration group BufferOffset
     * @param stride
     */
     void glBindVertexBuffer (int bindingindex, int buffer, long offset, int stride);

    /**
     * @param attribindex
     * @param size
     * @param type
     * @param normalized ,value from enumeration group Boolean
     * @param relativeoffset
     */
     void glVertexAttribFormat (int attribindex, int size, int type, boolean normalized, int relativeoffset);

    /**
     * @param attribindex
     * @param size
     * @param type
     * @param relativeoffset
     */
     void glVertexAttribIFormat (int attribindex, int size, int type, int relativeoffset);

    /**
     * @param attribindex
     * @param bindingindex
     */
     void glVertexAttribBinding (int attribindex, int bindingindex);

    /**
     * @param bindingindex
     * @param divisor
     */
     void glVertexBindingDivisor (int bindingindex, int divisor);

}
