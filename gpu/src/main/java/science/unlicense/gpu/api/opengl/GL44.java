package science.unlicense.gpu.api.opengl;

import science.unlicense.common.api.buffer.Buffer;

public interface GL44 extends science.unlicense.gpu.api.opengl.GL43 {

    /**
     * @param target
     * @param size
     * @param data ,length size
     * @param flags
     */
     void glBufferStorage (int target, Buffer.Int8 data, int flags);

    /**
     * @param texture
     * @param level
     * @param format
     * @param type
     * @param data
     */
     void glClearTexImage (int texture, int level, int format, int type, Buffer.Int8 data);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param width
     * @param height
     * @param depth
     * @param format
     * @param type
     * @param data
     */
     void glClearTexSubImage (int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, Buffer.Int8 data);

    /**
     * @param target
     * @param first
     * @param count
     * @param buffers ,length count
     */
     void glBindBuffersBase (int target, int first, Buffer.Int32 buffers);

    /**
     * @param target
     * @param first
     * @param count
     * @param buffers ,length count
     * @param offsets ,length count
     * @param sizes ,length count
     */
     void glBindBuffersRange (int target, int first, Buffer.Int32 buffers, Buffer.Int8 offsets, Buffer.Int8 sizes);

    /**
     * @param first
     * @param count
     * @param textures ,length count
     */
     void glBindTextures (int first, Buffer.Int32 textures);

    /**
     * @param first
     * @param count
     * @param samplers ,length count
     */
     void glBindSamplers (int first, Buffer.Int32 samplers);

    /**
     * @param first
     * @param count
     * @param textures ,length count
     */
     void glBindImageTextures (int first, Buffer.Int32 textures);

    /**
     * @param first
     * @param count
     * @param buffers ,length count
     * @param offsets ,length count
     * @param strides ,length count
     */
     void glBindVertexBuffers (int first, Buffer.Int32 buffers, Buffer.Int8 offsets, Buffer.Int32 strides);

}
