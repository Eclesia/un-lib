/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package science.unlicense.gpu.impl.opengl.resource;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;

/**
 * Common interface for OpenGL rendering targets : FBO or frame.
 * This class contain the definition of the available attached buffers.
 *
 * @author Johann Sorel
 */
public interface CanvasBuffer {
/**
     * Returns all fbo attachements.
     *
     * @return FBOAttachment array, never null, can be empty.
     */
    Sequence getAttachements();

    public interface Attachment {

        Chars getName();

    }
}
