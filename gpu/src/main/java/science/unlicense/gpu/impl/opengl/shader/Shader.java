
package science.unlicense.gpu.impl.opengl.shader;

import science.unlicense.common.api.character.Chars;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.AbstractResource;
import science.unlicense.gpu.impl.opengl.resource.ResourceException;

/**
 *
 * @author Johann Sorel
 */
public class Shader extends AbstractResource {

    public static final int SHADER_VERTEX = 0;
    public static final int SHADER_TESS_CONTROL = 1;
    public static final int SHADER_TESS_EVAL = 2;
    public static final int SHADER_GEOMETRY = 3;
    public static final int SHADER_FRAGMENT = 4;
    private static final int[] TYPE_MAPPING = {
        GLC.GLSL.SHADER.VERTEX,
        GLC.GLSL.SHADER.TESS_CONTROL,
        GLC.GLSL.SHADER.TESS_EVAL,
        GLC.GLSL.SHADER.GEOMETRY,
        GLC.GLSL.SHADER.FRAGMENT
    };

    private int glid = -1;
    private Chars code;
    private int type;

    public Shader() {
    }

    public Shader(Chars code, int type) {
        this.code = code;
        this.type = type;
    }

    public Chars getCode() {
        return code;
    }

    public void setCode(Chars code) {
        this.code = code;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getGpuID() {
        return glid;
    }

    public boolean isOnSystemMemory() {
        return code !=null;
    }

    public boolean isOnGpuMemory() {
        return glid >=0;
    }

    public void loadOnSystemMemory(GL gl) throws ResourceException {
        throw new ResourceException("Not supported.");
    }

    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if (isOnGpuMemory()) return;
        glid = ShaderUtils.loadShader(gl, TYPE_MAPPING[type], code);
        if (isForgetOnLoad()){
            unloadFromSystemMemory(gl);
        }
    }

    public void unloadFromSystemMemory(GL gl) throws ResourceException {
        code = null;
    }

    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        if (!isOnGpuMemory()) return;
        gl.asGL2ES2().glDeleteShader(glid);
        glid = -1;
    }

}
