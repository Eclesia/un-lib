

package science.unlicense.gpu.impl.opengl.resource;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.geometry.api.Extent;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.image.api.Image;

/**
 * TODO
 * @author Johann Sorel
 */
public class Texture1D extends AbstractTexture{

    public Texture1D(Image image) {
        super(image, null);
    }

    public Texture1D(int width, int height, TextureModel model) {
        super(new Extent.Long(width, height), model);
    }

    @Override
    public void loadOnSystemMemory(GL gl) throws ResourceException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void loadOnGpuMemory(GL gl) throws ResourceException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public TextureModel getInfo() {
        throw new UnimplementedException("Not supported yet.");
    }

}
