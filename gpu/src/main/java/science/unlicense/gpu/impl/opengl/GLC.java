

package science.unlicense.gpu.impl.opengl;


/**
 * Organized GL constants.
 * Provides a more efficiant way to know which can be used and where.
 *
 * @author Johann Sorel
 */
public interface GLC {

    public static final int I = 0;

    /** http://www.opengl.org/sdk/docs/man/html/glGet.xhtml */
    public static interface GETSET{
        public static final int ACTIVE_TEXTURE                              = 34016;
        public static final int ALIASED_LINE_WIDTH_RANGE                    = 33902;
        public static final int ARRAY_BUFFER_BINDING                        = 34964;
        public static final int BLEND_COLOR                                 = 32773;
        public static final int BLEND_DST_ALPHA                             = 32970;
        public static final int BLEND_DST_RGB                               = 32968;
        public static final int BLEND_EQUATION_RGB                          = 32777;
        public static final int BLEND_EQUATION_ALPHA                        = 34877;
        public static final int BLEND_SRC_ALPHA                             = 32971;
        public static final int BLEND_SRC_RGB                               = 32969;
        public static final int COLOR_CLEAR_VALUE                           = 3106;
        public static final int COLOR_WRITEMASK                             = 3107;
        public static final int COMPRESSED_TEXTURE_FORMATS                  = 34467;
        public static final int MAX_COMPUTE_SHADER_STORAGE_BLOCKS           = 37083;
        public static final int MAX_COMBINED_SHADER_STORAGE_BLOCKS          = 37084;
        public static final int MAX_COMPUTE_UNIFORM_BLOCKS                  = 37307;
        public static final int MAX_COMPUTE_TEXTURE_IMAGE_UNITS             = 37308;
        public static final int MAX_COMPUTE_UNIFORM_COMPONENTS              = 33379;
        public static final int MAX_COMPUTE_ATOMIC_COUNTERS                 = 33381;
        public static final int MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS          = 33380;
        public static final int MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS     = 33382;
        //public static final int MAX_COMPUTE_WORK_GROUP_INVOCATIONS = 37099;
        public static final int MAX_COMPUTE_WORK_GROUP_COUNT                = 37310;
        public static final int MAX_COMPUTE_WORK_GROUP_SIZE                 = 37311;
        public static final int DISPATCH_INDIRECT_BUFFER_BINDING            = 37103;
        public static final int MAX_DEBUG_GROUP_STACK_DEPTH                 = 33388;
        public static final int DEBUG_GROUP_STACK_DEPTH                     = 33389;
        public static final int CONTEXT_FLAGS                               = 33310;
        public static final int CURRENT_PROGRAM                             = 35725;
        public static final int DEPTH_CLEAR_VALUE                           = 2931;
        public static final int DEPTH_FUNC                                  = 2932;
        public static final int DEPTH_RANGE                                 = 2928;
        public static final int DEPTH_WRITEMASK                             = 2930;
        public static final int DOUBLEBUFFER                                = 3122;
        public static final int DRAW_BUFFER                                 = 3073;
        public static final int DRAW_BUFFER_0                               = 34853;
        public static final int DRAW_BUFFER_1                               = 34854;
        public static final int DRAW_BUFFER_2                               = 34855;
        public static final int DRAW_BUFFER_3                               = 34856;
        public static final int DRAW_BUFFER_4                               = 34857;
        public static final int DRAW_BUFFER_5                               = 34858;
        public static final int DRAW_BUFFER_6                               = 34859;
        public static final int DRAW_BUFFER_7                               = 34860;
        public static final int DRAW_BUFFER_8                               = 34861;
        public static final int DRAW_BUFFER_9                               = 34862;
        public static final int DRAW_BUFFER_10                              = 34863;
        public static final int DRAW_BUFFER_11                              = 34864;
        public static final int DRAW_BUFFER_12                              = 34865;
        public static final int DRAW_BUFFER_13                              = 34866;
        public static final int DRAW_BUFFER_14                              = 34867;
        public static final int DRAW_BUFFER_15                              = 34868;
        public static final int DRAW_FRAMEBUFFER_BINDING                    = 36006;
        public static final int READ_FRAMEBUFFER_BINDING                    = 36010;
        public static final int ELEMENT_ARRAY_BUFFER_BINDING                = 34965;
        public static final int FRAGMENT_SHADER_DERIVATIVE_HINT             = 35723;
        public static final int IMPLEMENTATION_COLOR_READ_FORMAT            = 35739;
        public static final int IMPLEMENTATION_COLOR_READ_TYPE              = 35738;
        public static final int LINE_SMOOTH_HINT                            = 3154;
        public static final int LINE_WIDTH                                  = 2849;
        public static final int LAYER_PROVOKING_VERTEX                      = 33374;
        public static final int LOGIC_OP_MODE                               = 3056;
        public static final int MAJOR_VERSION                               = 33307;
        public static final int MAX_3D_TEXTURE_SIZE                         = 32883;
        public static final int MAX_ARRAY_TEXTURE_LAYERS                    = 35071;
        public static final int MAX_CLIP_DISTANCES                          = 3378;
        public static final int MAX_COLOR_TEXTURE_SAMPLES                   = 37134;
        public static final int MAX_COMBINED_ATOMIC_COUNTERS                = 37591;
        public static final int MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS    = 35379;
        public static final int MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS    = 35378;
        public static final int MAX_COMBINED_TEXTURE_IMAGE_UNITS            = 35661;
        public static final int MAX_COMBINED_UNIFORM_BLOCKS                 = 35374;
        public static final int MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS      = 35377;
        public static final int MAX_CUBE_MAP_TEXTURE_SIZE                   = 34076;
        public static final int MAX_DEPTH_TEXTURE_SAMPLES                   = 37135;
        public static final int MAX_DRAW_BUFFERS                            = 34852;
        public static final int MAX_DUAL_SOURCE_DRAW_BUFFERS                = 35068;
        public static final int MAX_ELEMENTS_INDICES                        = 33001;
        public static final int MAX_ELEMENTS_VERTICES                       = 33000;
        public static final int MAX_FRAGMENT_ATOMIC_COUNTERS                = 37590;
        public static final int MAX_FRAGMENT_SHADER_STORAGE_BLOCKS          = 37082;
        public static final int MAX_FRAGMENT_INPUT_COMPONENTS               = 37157;
        public static final int MAX_FRAGMENT_UNIFORM_COMPONENTS             = 35657;
        public static final int MAX_FRAGMENT_UNIFORM_VECTORS                = 36349;
        public static final int MAX_FRAGMENT_UNIFORM_BLOCKS                 = 35373;
        public static final int MAX_FRAMEBUFFER_WIDTH                       = 37653;
        public static final int MAX_FRAMEBUFFER_HEIGHT                      = 37654;
        public static final int MAX_FRAMEBUFFER_LAYERS                      = 37655;
        public static final int MAX_FRAMEBUFFER_SAMPLES                     = 37656;
        public static final int MAX_GEOMETRY_ATOMIC_COUNTERS                = 37589;
        public static final int MAX_GEOMETRY_SHADER_STORAGE_BLOCKS          = 37079;
        public static final int MAX_GEOMETRY_INPUT_COMPONENTS               = 37155;
        public static final int MAX_GEOMETRY_OUTPUT_COMPONENTS              = 37156;
        public static final int MAX_GEOMETRY_TEXTURE_IMAGE_UNITS            = 35881;
        public static final int MAX_GEOMETRY_UNIFORM_BLOCKS                 = 35372;
        public static final int MAX_GEOMETRY_UNIFORM_COMPONENTS             = 36319;
        public static final int MAX_INTEGER_SAMPLES                         = 37136;
        public static final int MIN_MAP_BUFFER_ALIGNMENT                    = 37052;
        public static final int MAX_LABEL_LENGTH                            = 33512;
        public static final int MAX_PROGRAM_TEXEL_OFFSET                    = 35077;
        public static final int MIN_PROGRAM_TEXEL_OFFSET                    = 35076;
        public static final int MAX_RECTANGLE_TEXTURE_SIZE                  = 34040;
        public static final int MAX_RENDERBUFFER_SIZE                       = 34024;
        public static final int MAX_SAMPLES                                 = 36183;
        public static final int MAX_SAMPLE_MASK_WORDS                       = 36441;
        public static final int MAX_SERVER_WAIT_TIMEOUT                     = 37137;
        public static final int MAX_SHADER_STORAGE_BUFFER_BINDINGS          = 37085;
        public static final int MAX_TESS_CONTROL_ATOMIC_COUNTERS            = 37587;
        public static final int MAX_TESS_EVALUATION_ATOMIC_COUNTERS         = 37588;
        public static final int MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS      = 37080;
        public static final int MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS   = 37081;
        public static final int MAX_TEXTURE_BUFFER_SIZE                     = 35883;
        public static final int MAX_TEXTURE_IMAGE_UNITS                     = 34930;
        public static final int MAX_TEXTURE_LOD_BIAS                        = 34045;
        public static final int MAX_TEXTURE_SIZE                            = 3379;
        public static final int MAX_UNIFORM_BUFFER_BINDINGS                 = 35375;
        public static final int MAX_UNIFORM_BLOCK_SIZE                      = 35376;
        public static final int MAX_UNIFORM_LOCATIONS                       = 33390;
        public static final int MAX_VARYING_COMPONENTS                      = 35659;
        public static final int MAX_VARYING_VECTORS                         = 36348;
        public static final int MAX_VARYING_FLOATS                          = 35659;
        public static final int MAX_VERTEX_ATOMIC_COUNTERS                  = 37586;
        public static final int MAX_VERTEX_ATTRIBS                          = 34921;
        public static final int MAX_VERTEX_SHADER_STORAGE_BLOCKS            = 37078;
        public static final int MAX_VERTEX_TEXTURE_IMAGE_UNITS              = 35660;
        public static final int MAX_VERTEX_UNIFORM_COMPONENTS               = 35658;
        public static final int MAX_VERTEX_UNIFORM_VECTORS                  = 36347;
        public static final int MAX_VERTEX_OUTPUT_COMPONENTS                = 37154;
        public static final int MAX_VERTEX_UNIFORM_BLOCKS                   = 35371;
        public static final int MAX_VIEWPORT_DIMS                           = 3386;
        public static final int MAX_VIEWPORTS                               = 33371;
        public static final int MINOR_VERSION                               = 33308;
        public static final int NUM_COMPRESSED_TEXTURE_FORMATS              = 34466;
        public static final int NUM_EXTENSIONS                              = 33309;
        public static final int NUM_PROGRAM_BINARY_FORMATS                  = 34814;
        public static final int NUM_SHADER_BINARY_FORMATS                   = 36345;
        public static final int PACK_ALIGNMENT                              = 3333;
        public static final int PACK_IMAGE_HEIGHT                           = 32876;
        public static final int PACK_LSB_FIRST                              = 3329;
        public static final int PACK_ROW_LENGTH                             = 3330;
        public static final int PACK_SKIP_IMAGES                            = 32875;
        public static final int PACK_SKIP_PIXELS                            = 3332;
        public static final int PACK_SKIP_ROWS                              = 3331;
        public static final int PACK_SWAP_BYTES                             = 3328;
        public static final int PIXEL_PACK_BUFFER_BINDING                   = 35053;
        public static final int PIXEL_UNPACK_BUFFER_BINDING                 = 35055;
        public static final int POINT_FADE_THRESHOLD_SIZE                   = 33064;
        public static final int PRIMITIVE_RESTART_INDEX                     = 36766;
        public static final int PROGRAM_BINARY_FORMATS                      = 34815;
        public static final int PROGRAM_PIPELINE_BINDING                    = 33370;
        public static final int PROVOKING_VERTEX                            = 36431;
        public static final int POINT_SIZE                                  = 2833;
        public static final int POINT_SIZE_GRANULARITY                      = 2835;
        public static final int POINT_SIZE_RANGE                            = 2834;
        public static final int POLYGON_OFFSET_FACTOR                       = 32824;
        public static final int POLYGON_OFFSET_UNITS                        = 10752;
        public static final int POLYGON_SMOOTH_HINT                         = 3155;
        public static final int READ_BUFFER                                 = 3074;
        public static final int RENDERBUFFER_BINDING                        = 36007;
        public static final int SAMPLE_BUFFERS                              = 32936;
        public static final int SAMPLE_COVERAGE_VALUE                       = 32938;
        public static final int SAMPLE_COVERAGE_INVERT                      = 32939;
        public static final int SAMPLER_BINDING                             = 35097;
        public static final int SAMPLES                                     = 32937;
        public static final int SCISSOR_BOX                                 = 3088;
        public static final int SHADER_COMPILER                             = 36346;
        public static final int SHADER_STORAGE_BUFFER_BINDING               = 37075;
        public static final int SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT      = 37087;
        public static final int SHADER_STORAGE_BUFFER_START                 = 37076;
        public static final int SHADER_STORAGE_BUFFER_SIZE                  = 37077;
        public static final int SMOOTH_LINE_WIDTH_RANGE                     = 2850;
        public static final int SMOOTH_LINE_WIDTH_GRANULARITY               = 2851;
        public static final int STENCIL_BACK_FAIL                           = 34817;
        public static final int STENCIL_BACK_FUNC                           = 34816;
        public static final int STENCIL_BACK_PASS_DEPTH_FAIL                = 34818;
        public static final int STENCIL_BACK_PASS_DEPTH_PASS                = 34819;
        public static final int STENCIL_BACK_REF                            = 36003;
        public static final int STENCIL_BACK_VALUE_MASK                     = 36004;
        public static final int STENCIL_BACK_WRITEMASK                      = 36005;
        public static final int STENCIL_CLEAR_VALUE                         = 2961;
        public static final int STENCIL_FAIL                                = 2964;
        public static final int STENCIL_FUNC                                = 2962;
        public static final int STENCIL_PASS_DEPTH_FAIL                     = 2965;
        public static final int STENCIL_PASS_DEPTH_PASS                     = 2966;
        public static final int STENCIL_REF                                 = 2967;
        public static final int STENCIL_VALUE_MASK                          = 2963;
        public static final int STENCIL_WRITEMASK                           = 2968;
        public static final int STEREO                                      = 3123;
        public static final int SUBPIXEL_BITS                               = 3408;
        public static final int TEXTURE_BINDING_1D                          = 32872;
        public static final int TEXTURE_BINDING_1D_ARRAY                    = 35868;
        public static final int TEXTURE_BINDING_2D                          = 32873;
        public static final int TEXTURE_BINDING_2D_ARRAY                    = 35869;
        public static final int TEXTURE_BINDING_2D_MULTISAMPLE              = 37124;
        public static final int TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY        = 37125;
        public static final int TEXTURE_BINDING_3D                          = 32874;
        public static final int TEXTURE_BINDING_BUFFER                      = 35884;
        public static final int TEXTURE_BINDING_CUBE_MAP                    = 34068;
        public static final int TEXTURE_BINDING_RECTANGLE                   = 34038;
        public static final int TEXTURE_COMPRESSION_HINT                    = 34031;
        public static final int TEXTURE_BUFFER_OFFSET_ALIGNMENT             = 37279;
        public static final int TIMESTAMP                                   = 36392;
        public static final int TRANSFORM_FEEDBACK_BUFFER_BINDING           = 35983;
        public static final int TRANSFORM_FEEDBACK_BUFFER_START             = 35972;
        public static final int TRANSFORM_FEEDBACK_BUFFER_SIZE              = 35973;
        public static final int UNIFORM_BUFFER_BINDING                      = 35368;
        public static final int UNIFORM_BUFFER_OFFSET_ALIGNMENT             = 35380;
        public static final int UNIFORM_BUFFER_SIZE                         = 35370;
        public static final int UNIFORM_BUFFER_START                        = 35369;
        public static final int UNPACK_ALIGNMENT                            = 3317;
        public static final int UNPACK_IMAGE_HEIGHT                         = 32878;
        public static final int UNPACK_LSB_FIRST                            = 3313;
        public static final int UNPACK_ROW_LENGTH                           = 3314;
        public static final int UNPACK_SKIP_IMAGES                          = 32877;
        public static final int UNPACK_SKIP_PIXELS                          = 3316;
        public static final int UNPACK_SKIP_ROWS                            = 3315;
        public static final int UNPACK_SWAP_BYTES                           = 3312;
        public static final int VERTEX_ARRAY_BINDING                        = 34229;
        public static final int VERTEX_BINDING_DIVISOR                      = 33494;
        public static final int VERTEX_BINDING_OFFSET                       = 33495;
        public static final int VERTEX_BINDING_STRIDE                       = 33496;
        public static final int MAX_VERTEX_ATTRIB_RELATIVE_OFFSET           = 33497;
        public static final int MAX_VERTEX_ATTRIB_BINDINGS                  = 33498;
        public static final int VIEWPORT                                    = 2978;
        public static final int VIEWPORT_BOUNDS_RANGE                       = 33373;
        public static final int VIEWPORT_INDEX_PROVOKING_VERTEX             = 33375;
        public static final int VIEWPORT_SUBPIXEL_BITS                      = 33372;
        public static final int MAX_ELEMENT_INDEX                           = 36203;

        /**
         * All rendering boolean enabling states.
         *
         * Spec : https://www.opengl.org/sdk/docs/man/html/glEnable.xhtml
         */
        public interface State{
            public static final int BLEND                           = 3042;
            //public static final int CLIP_DISTANCE = GL.GL_CLIP_DISTANCE;
            public static final int COLOR_LOGIC_OP                  = 3058;
            public static final int CULL_FACE                       = 2884;
            public static final int DEBUG_OUTPUT                    = 37600;
            public static final int DEBUG_OUTPUT_SYNCHRONOUS        = 33346;
            public static final int DEPTH_CLAMP                     = 34383;
            public static final int DEPTH_TEST                      = 2929;
            public static final int DITHER                          = 3024;
            public static final int FRAMEBUFFER_SRGB                = 36281;
            public static final int LINE_SMOOTH                     = 2848;
            public static final int MULTISAMPLE                     = 32925;
            public static final int POLYGON_OFFSET_FILL             = 32823;
            public static final int POLYGON_OFFSET_LINE             = 10754;
            public static final int POLYGON_OFFSET_POINT            = 10753;
            public static final int POLYGON_SMOOTH                  = 2881;
            public static final int PRIMITIVE_RESTART               = 36765;
            public static final int PRIMITIVE_RESTART_FIXED_INDEX   = 36201;
            public static final int RASTERIZER_DISCARD              = 35977;
            public static final int SAMPLE_ALPHA_TO_COVERAGE        = 32926;
            public static final int SAMPLE_ALPHA_TO_ONE             = 32927;
            public static final int SAMPLE_COVERAGE                 = 32928;
            public static final int SAMPLE_SHADING                  = 35894;
            public static final int SAMPLE_MASK                     = 36433;
            public static final int SCISSOR_TEST                    = 3089;
            public static final int STENCIL_TEST                    = 2960;
            public static final int TEXTURE_CUBE_MAP_SEAMLESS       = 34895;
            public static final int PROGRAM_POINT_SIZE              = 34370;
            //GL2 TODO : find a way to keep track of GL versions
            //public static final int LINE_STIPPLE                    = 2852;
        }

    }

    public static interface CLEAR{
        public static final int COLOR           = 16384;
        public static final int DEPTH           = 256;
        public static final int STENCIL         = 1024;
    }

    public static interface CULLING{
        public static final int FRONT          = 1028;
        public static final int BACK           = 1029;
        public static final int FRONT_AND_BACK = 1032;
    }

    public static interface POLYGON_MODE{
        public static final int FILL            = 6914;
        public static final int LINE            = 6913;
        public static final int POINT           = 6912;
    }

    /**
     * Possible values for glLogicOp
     */
    public static interface LOGIC_OP{
        /** 0 */
        public static final int CLEAR = 5376;
        /** 1 */
        public static final int SET = 5391;
        /** source */
        public static final int COPY = 5379;
        /** ~source */
        public static final int COPY_INVERTED = 5388;
        /** dest */
        public static final int NOOP = 5381;
        /** ~dest */
        public static final int INVERT = 5386;
        /** source & dest */
        public static final int AND = 5377;
        /** ~(source & dest) */
        public static final int NAND = 5390;
        /** source | dest */
        public static final int OR = 5383;
        /** ~(source | dest) */
        public static final int NOR = 5384;
        /** source ^ dest */
        public static final int XOR = 5382;
        /** ~(source ^ dest) */
        public static final int EQUIV = 5385;
        /** source & ~dest */
        public static final int AND_REVERSE = 5378;
        /** ~source & dest */
        public static final int AND_INVERTED = 5380;
        /** source | ~dest */
        public static final int OR_REVERSE = 5387;
        /** ~source | dest */
        public static final int OR_INVERTED = 5389;
    }

    public static interface FBO{
        public static interface Attachment{
            public static final int COLOR_0         = 36064;
            public static final int COLOR_1         = 36065;
            public static final int COLOR_2         = 36066;
            public static final int COLOR_3         = 36067;
            public static final int COLOR_4         = 36068;
            public static final int COLOR_5         = 36069;
            public static final int COLOR_6         = 36070;
            public static final int COLOR_7         = 36071;
            public static final int COLOR_8         = 36072;
            public static final int COLOR_9         = 36073;
            public static final int COLOR_10        = 36074;
            public static final int COLOR_11        = 36075;
            public static final int COLOR_12        = 36076;
            public static final int COLOR_13        = 36077;
            public static final int COLOR_14        = 36078;
            public static final int COLOR_15        = 36079;
            public static final int COLOR_16        = 36080;
            public static final int COLOR_17        = 36081;
            public static final int COLOR_18        = 36082;
            public static final int COLOR_19        = 36083;
            public static final int DEPTH           = 36096;
            public static final int DEPTH_STENCIL   = 33306;
            public static final int STENCIL         = 36128;
        }
        public static interface Blit{
            public static final int MASK_COLORS     = 16384;
            public static final int MASK_DEPTH      = 256;
            public static final int MASK_STENCIL    = 1024;
            public static final int FILTER_NEAREST  = 9728;
            public static final int FILTER_LINEAR   = 9729;
        }
    }

    public static interface Texture{
        // http://www.opengl.org/sdk/docs/man/xhtml/glTexImage2D.xml
        public interface Target{
            public static final int TEXTURE_1D_ARRAY        = 35864;
            public static final int TEXTURE_2D              = 3553;
            public static final int TEXTURE_2DMS            = 37120;
            public static final int TEXTURE_RECTANGLE       = 34037;
            public static final int CUBE_MAP_POSITIVE_X     = 34069;
            public static final int CUBE_MAP_NEGATIVE_X     = 34070;
            public static final int CUBE_MAP_POSITIVE_Y     = 34071;
            public static final int CUBE_MAP_NEGATIVE_Y     = 34072;
            public static final int CUBE_MAP_POSITIVE_Z     = 34073;
            public static final int CUBE_MAP_NEGATIVE_Z     = 34074;
            public static final int PROXY_TEXTURE_2D        = 32868;
            public static final int PROXY_TEXTURE_1D_ARRAY  = 35865;
            public static final int PROXY_TEXTURE_CUBE_MAP  = 34075;
            public static final int PROXY_TEXTURE_RECTANGLE = 34039;
        }
        public interface InternalFormat{
            public static final int DEPTH_COMPONENT    = 6402;
            public static final int DEPTH_COMPONENT16  = 33189;
            public static final int DEPTH_COMPONENT24  = 33190;
            public static final int DEPTH_COMPONENT32  = 33191;
            public static final int DEPTH_COMPONENT32F = 36012;
            public static final int DEPTH24_STENCIL8   = 35056;
            public static final int DEPTH32F_STENCIL8  = 36013;
            public static final int DEPTH_STENCIL      = 34041;
            public static final int RED             = 6403;
            public static final int RG              = 33319;
            public static final int RGB             = 6407;
            public static final int RGBA            = 6408;
            public static final int R8              = 33321;//  GL_RED  8
            public static final int R8_SNORM        = 36756;//  GL_RED  s8
            public static final int R16             = 33322;//  GL_RED  16
            public static final int R16_SNORM       = 36760;//  GL_RED  s16
            public static final int RG8             = 33323;//  GL_RG  8  8
            public static final int RG8_SNORM       = 36757;//  GL_RG  s8  s8
            public static final int RG16            = 33324;//  GL_RG  16  16
            public static final int RG16_SNORM      = 36761;//  GL_RG  s16  s16
            public static final int R3_G3_B2        = 10768;//  GL_RGB  3  3  2
            public static final int RGB4            = 32847;//  GL_RGB  4  4  4
            public static final int RGB5            = 32848;//  GL_RGB  5  5  5
            public static final int RGB8            = 32849;//  GL_RGB  8  8  8
            public static final int RGB8_SNORM      = 36758;//  GL_RGB  s8  s8  s8
            public static final int RGB10           = 32850;//  GL_RGB  10  10  10
            public static final int RGB12           = 32851;//  GL_RGB  12  12  12
            public static final int RGB16           = 32852;//_SNORM  GL_RGB  16  16  16
            public static final int RGBA2           = 32853;//  GL_RGB  2  2  2  2
            public static final int RGBA4           = 32854;//  GL_RGB  4  4  4  4
            public static final int RGB5_A1         = 32855;//  GL_RGBA  5  5  5  1
            public static final int RGBA8           = 32856;//  GL_RGBA  8  8  8  8
            public static final int RGBA8_SNORM     = 36759;//  GL_RGBA  s8  s8  s8  s8
            public static final int RGB10_A2        = 32857;//  GL_RGBA  10  10  10  2
            public static final int RGB10_A2UI      = 36975;//  GL_RGBA  ui10  ui10  ui10  ui2
            public static final int RGBA12          = 32858;//  GL_RGBA  12  12  12  12
            public static final int RGBA16          = 32859;//  GL_RGBA  16  16  16  16
            public static final int SRGB8           = 35905;//  GL_RGB  8  8  8
            public static final int SRGB8_ALPHA8    = 35907;//  GL_RGBA  8  8  8  8
            public static final int R16F            = 33325;//  GL_RED  f16
            public static final int RG16F           = 33327;//  GL_RG  f16  f16
            public static final int RGB16F          = 34843;//  GL_RGB  f16  f16  f16
            public static final int RGBA16F         = 34842;//  GL_RGBA  f16  f16  f16  f16
            public static final int R32F            = 33326;//  GL_RED  f32
            public static final int RG32F           = 33328;//  GL_RG  f32  f32
            public static final int RGB32F          = 34837;//  GL_RGB  f32  f32  f32
            public static final int RGBA32F         = 34836;//  GL_RGBA  f32  f32  f32  f32
            public static final int R11F_G11F_B10F  = 35898;//  GL_RGB  f11  f11  f10
            public static final int RGB9_E5         = 35901;//  GL_RGB  9  9  9     5
            public static final int R8I             = 33329;//  GL_RED  i8
            public static final int R8UI            = 33330;//  GL_RED  ui8
            public static final int R16I            = 33331;//  GL_RED  i16
            public static final int R16UI           = 33332;//  GL_RED  ui16
            public static final int R32I            = 33333;//  GL_RED  i32
            public static final int R32UI           = 33334;//  GL_RED  ui32
            public static final int RG8I            = 33335;//  GL_RG  i8  i8
            public static final int RG8UI           = 33336;//  GL_RG  ui8  ui8
            public static final int RG16I           = 33337;//  GL_RG  i16  i16
            public static final int RG16UI          = 33338;//  GL_RG  ui16  ui16
            public static final int RG32I           = 33339;//  GL_RG  i32  i32
            public static final int RG32UI          = 33340;//  GL_RG  ui32  ui32
            public static final int RGB8I           = 36239;//  GL_RGB  i8  i8  i8
            public static final int RGB8UI          = 36221;//  GL_RGB  ui8  ui8  ui8
            public static final int RGB16I          = 36233;//  GL_RGB  i16  i16  i16
            public static final int RGB16UI         = 36215;//  GL_RGB  ui16  ui16  ui16
            public static final int RGB32I          = 36227;//  GL_RGB  i32  i32  i32
            public static final int RGB32UI         = 36209;//  GL_RGB  ui32  ui32  ui32
            public static final int RGBA8I          = 36238;//  GL_RGBA  i8  i8  i8  i8
            public static final int RGBA8UI         = 36220;//  GL_RGBA  ui8  ui8  ui8  ui8
            public static final int RGBA16I         = 36232;//  GL_RGBA  i16  i16  i16  i16
            public static final int RGBA16UI        = 36214;//  GL_RGBA  ui16  ui16  ui16  ui16
            public static final int RGBA32I         = 36226;//  GL_RGBA  i32  i32  i32  i32
            public static final int RGBA32UI        = 36208;//  GL_RGBA  ui32  ui32  ui32  ui32
            public static final int COMPRESSED_RED                          = 33317;
            public static final int COMPRESSED_RG                           = 33318;
            public static final int COMPRESSED_RGB                          = 34029;
            public static final int COMPRESSED_RGBA                         = 34030;
            public static final int COMPRESSED_SRGB                         = 35912;
            public static final int COMPRESSED_SRGB_ALPHA                   = 35913;
            public static final int COMPRESSED_RED_RGTC1                    = 36283;
            public static final int COMPRESSED_SIGNED_RED_RGTC1             = 36284;
            public static final int COMPRESSED_RG_RGTC2                     = 36285;
            public static final int COMPRESSED_SIGNED_RG_RGTC2              = 36286;
            public static final int COMPRESSED_RGBA_BPTC_UNORM_ARB          = 36492;
            public static final int CCOMPRESSED_SRGB_ALPHA_BPTC_UNORM_ARB   = 36493;
            public static final int COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB    = 36494;
            public static final int COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_ARB  = 36495;

        }
        /**
         * NOTE-1 : the difference between RED and RED_INTEGER is how opengl will map
         * the value in the shader. for example if you use a type UNSIGNED_BYTE with type RED
         * the shader value will be normalized to [0...1], if you use RED_INTEGER the
         * value will remain [0...255].
         */
        public interface Format{
            public static final int RED             = 6403;
            public static final int RG              = 33319;
            public static final int RGB             = 6407;
            public static final int BGR             = 32992;
            public static final int RGBA            = 6408;
            public static final int BGRA            = 32993;
            public static final int RED_INTEGER     = 36244;
            public static final int RG_INTEGER      = 33320;
            public static final int RGB_INTEGER     = 36248;
            public static final int BGR_INTEGER     = 36250;
            public static final int RGBA_INTEGER    = 36249;
            public static final int BGRA_INTEGER    = 36251;
            public static final int STENCIL_INDEX   = 6401;
            public static final int DEPTH_COMPONENT = 6402;
            public static final int DEPTH_STENCIL   = 34041;
        }
        public interface CompressedFormat{
            //TODO make full list
            public static final int DXT1 = 33777;
            public static final int DXT3 = 33778;
            public static final int DXT5 = 33779;
            //next formats need GL 4.3+
            public static final int RGB8_ETC2                       = 37492;
            public static final int SRGB8_ETC2                      = 37493;
            public static final int RGB8_PUNCHTHROUGH_ALPHA1_ETC2   = 37494;
            public static final int SRGB8_PUNCHTHROUGH_ALPHA1_ETC2  = 37495;
            public static final int RGBA8_ETC2_EAC                  = 37496;
            public static final int SRGB8_ALPHA8_ETC2_EAC           = 37497;
            public static final int R11_EAC                         = 37488;
            public static final int SIGNED_R11_EAC                  = 37489;
            public static final int RG11_EAC                        = 37490;
            public static final int SIGNED_RG11_EAC                 = 37491;
        }
        public interface Type{
            public static final int UNSIGNED_BYTE               = 5121;
            public static final int BYTE                        = 5120;
            public static final int UNSIGNED_SHORT              = 5123;
            public static final int SHORT                       = 5122;
            public static final int UNSIGNED_INT                = 5125;
            public static final int INT                         = 5124;
            public static final int FLOAT                       = 5126;
            public static final int FLOAT_UI                    = 36269;
            public static final int UNSIGNED_BYTE_3_3_2         = 32818;
            public static final int UNSIGNED_BYTE_2_3_3_REV     = 33634;
            public static final int UNSIGNED_SHORT_5_6_5        = 33635;
            public static final int UNSIGNED_SHORT_5_6_5_REV    = 33636;
            public static final int UNSIGNED_SHORT_4_4_4_4      = 32819;
            public static final int UNSIGNED_SHORT_4_4_4_4_REV  = 33637;
            public static final int UNSIGNED_SHORT_5_5_5_1      = 32820;
            public static final int UNSIGNED_SHORT_1_5_5_5_REV  = 33638;
            public static final int UNSIGNED_INT_8_8_8_8        = 32821;
            public static final int UNSIGNED_INT_8_8_8_8_REV    = 33639;
            public static final int UNSIGNED_INT_10_10_10_2     = 36342;
            public static final int UNSIGNED_INT_2_10_10_10_REV = 33640;
            public static final int UNSIGNED_INT_24_8           = 34042;
        }
        public interface Parameters{

            public interface DEPTH_STENCIL_TEXTURE_MODE{
                public static final int KEY = 37098;
            }

            /** Value can be any positive integer */
            public interface BASE_LEVEL{
                public static final int KEY = 33084;
            }

            public interface BORDER_COLOR{
                public static final int KEY = 4100;
            }

            public interface COMPARE_FUNC{
                public static final int KEY     = 34893;
                public static final int LEQUAL  = 515;
                public static final int GEQUAL  = 518;
                public static final int LESS    = 513;
                public static final int GREATER = 516;
                public static final int EQUAL   = 514;
                public static final int NOTEQUAL= 517;
                public static final int ALWAYS  = 519;
                public static final int NEVER   = 512;
            }

            public interface COMPARE_MODE{
                public static final int KEY             = 34892;
                public static final int REF_TO_TEXTURE  = 34894;
                public static final int NONE            = 0;
            }

            /** Value can be any floating point */
            public interface LOD_BIAS{
                public static final int KEY = 34049;
            }

            public interface MIN_FILTER{
                public static final int KEY                     = 10241;
                public static final int NEAREST                 = 9728;
                public static final int LINEAR                  = 9729;
                public static final int NEAREST_MIPMAP_NEAREST  = 9984;
                public static final int LINEAR_MIPMAP_NEAREST   = 9985;
                public static final int NEAREST_MIPMAP_LINEAR   = 9986;
                public static final int LINEAR_MIPMAP_LINEAR    = 9987;
            }

            public interface MAG_FILTER{
                public static final int KEY                     = 10240;
                public static final int NEAREST                 = 9728;
                public static final int LINEAR                  = 9729;
            }

            public interface WRAP_S{
                public static final int KEY                     = 10242;
                public static final int CLAMP_TO_EDGE           = 33071;
                public static final int CLAMP_TO_BORDER         = 33069;
                public static final int MIRRORED_REPEAT         = 33648;
                public static final int REPEAT                  = 10497;
            }

            public interface WRAP_T{
                public static final int KEY                     = 10243;
                public static final int CLAMP_TO_EDGE           = 33071;
                public static final int CLAMP_TO_BORDER         = 33069;
                public static final int MIRRORED_REPEAT         = 33648;
                public static final int REPEAT                  = 10497;
            }

            public interface WRAP_R{
                public static final int KEY                     = 32882;
                public static final int CLAMP_TO_EDGE           = 33071;
                public static final int CLAMP_TO_BORDER         = 33069;
                public static final int MIRRORED_REPEAT         = 33648;
                public static final int REPEAT                  = 10497;
            }

            /** Value can be any floating point */
            public interface MIN_LOD{
                public static final int KEY = 33082;
            }

            /** Value can be any floating point */
            public interface MAX_LOD{
                public static final int KEY = 33083;
            }

            /** Value can be any integer */
            public interface MAX_LEVEL{
                public static final int KEY = 33085;
            }

            public interface SWIZZLE_R{
                public static final int KEY     = 36418;
                public static final int RED     = 6403;
                public static final int GREEN   = 6404;
                public static final int BLUE    = 6405;
                public static final int ALPHA   = 6406;
                public static final int ZERO    = 0;
                public static final int ONE     = 1;
            }

            public interface SWIZZLE_G{
                public static final int KEY     = 36419;
                public static final int RED     = 6403;
                public static final int GREEN   = 6404;
                public static final int BLUE    = 6405;
                public static final int ALPHA   = 6406;
                public static final int ZERO    = 0;
                public static final int ONE     = 1;
            }

            public interface SWIZZLE_B{
                public static final int KEY     = 36420;
                public static final int RED     = 6403;
                public static final int GREEN   = 6404;
                public static final int BLUE    = 6405;
                public static final int ALPHA   = 6406;
                public static final int ZERO    = 0;
                public static final int ONE     = 1;
            }

            public interface SWIZZLE_A{
                public static final int KEY     = 36421;
                public static final int RED     = 6403;
                public static final int GREEN   = 6404;
                public static final int BLUE    = 6405;
                public static final int ALPHA   = 6406;
                public static final int ZERO    = 0;
                public static final int ONE     = 1;
            }

            public interface SWIZZLE_RGBA{
                public static final int KEY     = 36422;
                public static final int RED     = 6403;
                public static final int GREEN   = 6404;
                public static final int BLUE    = 6405;
                public static final int ALPHA   = 6406;
                public static final int ZERO    = 0;
                public static final int ONE     = 1;
            }

            public interface TEXTURE_BORDER_COLOR{
                public static final int KEY     = 4100;
            }

        }
    }

    public static interface GLSL{
        public static final int V110_GL20 = 110;
        public static final int V120_GL21 = 120;
        public static final int V130_GL30 = 130;
        public static final int V140_GL31 = 140;
        public static final int V150_GL32 = 150;
        public static final int V330_GL33 = 330;
        public static final int V400 = 400;
        public static final int V410 = 410;
        public static final int V420 = 420;
        public static final int V430 = 430;
        public static final int V440 = 440;
        public static final int V450 = 450;

        public static interface SHADER{
            public static final int VERTEX      = 35633;
            public static final int TESS_CONTROL= 36488;
            public static final int TESS_EVAL   = 36487;
            public static final int GEOMETRY    = 36313;
            public static final int FRAGMENT    = 35632;
        }
    }

    public static interface TRANSFORM_FEEDBACK{
        public static final int MODE_INTERLEAVED    = 35980;
        public static final int MODE_SEPARATE       = 35981;

        public static interface PRIMITIVE{
            public static final int POINTS      = 0;
            public static final int LINES       = 1;
            public static final int TRIANGLES   = 4;
        }
    }

    public static interface TYPE{
        public static final int UBYTE   = 5121;
        public static final int BYTE    = 5120;
        public static final int USHORT  = 5123;
        public static final int SHORT   = 5122;
        public static final int UINT    = 5125;
        public static final int INT     = 5124;
        public static final int FLOAT   = 5126;
        public static final int DOUBLE  = 5130;
    }

    public static interface PRIMITIVE{
        public static final int POINTS                  = 0;
        public static final int LINE_STRIP              = 3;
        public static final int LINE_LOOP               = 2;
        public static final int LINES                   = 1;
        public static final int LINE_STRIP_ADJACENCY    = 11;
        public static final int LINES_ADJACENCY         = 10;
        public static final int TRIANGLE_STRIP          = 5;
        public static final int TRIANGLE_FAN            = 6;
        public static final int TRIANGLES               = 4;
        public static final int TRIANGLE_STRIP_ADJACENCY= 13;
        public static final int TRIANGLES_ADJACENCY     = 12;
        public static final int PATCHES                 = 14;
    }

}
