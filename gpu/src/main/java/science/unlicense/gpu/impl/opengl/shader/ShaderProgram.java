
package science.unlicense.gpu.impl.opengl.shader;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GL3;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.AbstractResource;
import science.unlicense.gpu.impl.opengl.resource.ResourceException;

/**
 *
 * @author Johann Sorel
 */
public class ShaderProgram extends AbstractResource {

    //program vertex attributes and uniforms
    private static final Chars UNIFORM = Chars.constant("uniform");
    private static final Chars LAYOUT = Chars.constant("layout");
    private final Dictionary uniforms = new HashDictionary();
    private final Dictionary vertexAttributes = new HashDictionary();

    private int glid = -1;
    private Chars[] trsFeedBackVars;
    private int trsFeedBackMode;

    // vertex, tess control, tess eval, geometry, pixel shaders
    private final Shader[] shaders = new Shader[5];

    public ShaderProgram() {
    }

    public ShaderProgram(Chars[] shaderTexts, Chars[] trsFeedBackVars, int trsFeedBackMode){
        for (int i=0;i<shaderTexts.length;i++){
            if (shaderTexts[i]!=null && !shaderTexts[i].isEmpty()){
                setShader(new Shader(shaderTexts[i], i), i);
            }
        }
        setTransferFeedback(trsFeedBackVars, trsFeedBackMode);
    }

    /**
     *
     * @param type one of Shader.SHADER_X
     * @return
     */
    public Shader getShader(int type){
        return shaders[type];
    }

    public void setShader(Shader shader, int type){
        shaders[type] = shader;
    }

    public void setTransferFeedback(Chars[] trsFeedBackVars, int trsFeedBackMode){
        this.trsFeedBackVars = trsFeedBackVars;
        this.trsFeedBackMode = trsFeedBackMode;
    }

    /**
     * Indicate if this program uses the tesselation phases.
     * @return true if tesselation is present.
     */
    public boolean usesTesselationShader(){
        return shaders[1] != null;
    }

    /**
     * Indicate if this program uses the geometry phase.
     * @return true if geometry is present.
     */
    public boolean usesGeometryShader(){
        return shaders[3] != null;
    }

    public Collection getUniformNames() {
        return uniforms.getKeys();
    }

    public Uniform getUniform(Chars name){
        return (Uniform) uniforms.getValue(name);
    }

    /**
     * OpenGL constraints : GL2ES2
     *
     * @param name
     * @param gl
     * @param type
     * @return
     */
    public Uniform getUniform(Chars name, GL gl, int type){
        Uniform uni = (Uniform) uniforms.getValue(name);
        if (uni == null) {
            //search the shader program
            final int uniId = gl.asGL2ES2().glGetUniformLocation(glid, name);
            final int blockIndex = gl.asGL2ES3().glGetUniformBlockIndex(glid, name);
            uni = new Uniform(glid, uniId, type, blockIndex);
            GLUtilities.checkGLErrorsFail(gl);
            uniforms.add(name, uni);
        }
        //check type
        if (type == -1) {
            if (uni.getType() == -1) {
                throw new RuntimeException("Uniform type not defined.");
            }
        } else if (uni.getType() != type) {
            throw new RuntimeException("Uniform type mismatch, caller expected "+type+" but shader is "+uni.getType());
        }

        return uni;
    }

    public Collection getVertexAttributeNames() {
        return vertexAttributes.getKeys();
    }

    /**
     * OpenGL constraints : GL2ES2
     *
     * @param name
     * @param gl
     * @param slotSize
     * @return
     */
    public VertexAttribute getVertexAttribute(Chars name, GL gl, int slotSize) {
        VertexAttribute vertexAtt = (VertexAttribute) vertexAttributes.getValue(name);
        if (vertexAtt == null) {
            final int idx = gl.asGL2ES2().glGetAttribLocation(glid, name);
            //TODO get type back
            vertexAtt = new VertexAttribute(glid, idx, -1,slotSize);
            GLUtilities.checkGLErrorsFail(gl);
            vertexAttributes.add(name, vertexAtt);
        }
        //check type
        if (slotSize == -1) {
            if (vertexAtt.getSlotSize() == -1) {
                throw new RuntimeException("VertexAttribute slot size not defined.");
            }
        } else if (vertexAtt.getSlotSize() == -1) {
            vertexAtt = new VertexAttribute(vertexAtt.getProgramId(), vertexAtt.getLayoutIdx(), vertexAtt.getType(), slotSize);
            vertexAttributes.add(name, vertexAtt);
        } else if (vertexAtt.getSlotSize() != slotSize) {
            throw new RuntimeException("VertexAttribute slot size mismatch, caller expected "+slotSize+" but vertex attribute is "+vertexAtt.getSlotSize());
        }

        return vertexAtt;
    }

    /**
     * Get shader program id.
     *
     * @return shader program id
     */
    @Override
    public int getGpuID() {
        return glid;
    }

    /**
     * Activate the program.
     *
     * OpenGL constraints : GL2ES2
     *
     * @param gl
     */
    public void enable(GL gl){
        if (glid<0) throw new RuntimeException("Program is not loaded.");
        gl.asGL2ES2().glUseProgram(glid);
    }

    /**
     * Desactivate the program.
     *
     * OpenGL constraints : GL2ES2
     *
     * @param gl
     */
    public void disable(GL gl){
        gl.asGL2ES2().glUseProgram(0);
    }

    /**
     * Relink the program setting the output fragment variable to each output.
     *
     * OpenGL constraints : GL3
     *
     * @param names
     */
    public void bindFragDataLocations(GL gl, Chars[] names) {
        loadOnGpuMemory(gl);
        final int programID = getGpuID();

        GL3 gl3 = gl.asGL3();
        for (int i=0;i<names.length;i++) {
            gl3.glBindFragDataLocation(programID, i, names[i] == null ? Chars.EMPTY : names[i]);
        }
        gl.asGL2ES2().glLinkProgram(programID);
    }

    @Override
    public boolean isOnSystemMemory() {
        for (int i=0;i<shaders.length;i++){
            if (shaders[i]!=null && !shaders[i].isOnSystemMemory()){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isOnGpuMemory() {
        return getGpuID()>=0;
    }

    @Override
    public void loadOnSystemMemory(GL gl) throws ResourceException {
        for (int i=0;i<shaders.length;i++){
            if (shaders[i]!=null){
                shaders[i].loadOnSystemMemory(gl);
            }
        }
    }

    @Override
    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if (isOnGpuMemory()) return;

        for (int i=0;i<shaders.length;i++){
            if (shaders[i]!=null){
                shaders[i].loadOnGpuMemory(gl);
            }
        }

        glid = ShaderUtils.loadShaderProgram(gl,
                shaders[0]==null ? -1 : shaders[0].getGpuID(),
                shaders[1]==null ? -1 : shaders[1].getGpuID(),
                shaders[2]==null ? -1 : shaders[2].getGpuID(),
                shaders[3]==null ? -1 : shaders[3].getGpuID(),
                shaders[4]==null ? -1 : shaders[4].getGpuID(),
                trsFeedBackVars, trsFeedBackMode);

        loadAssociates(gl);

        GLUtilities.checkGLErrorsFail(gl);
    }

    @Override
    public void unloadFromSystemMemory(GL gl) throws ResourceException {
        for (int i=0;i<shaders.length;i++){
            if (shaders[i]!=null){
                shaders[i].unloadFromSystemMemory(gl);
            }
        }
    }

    @Override
    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        for (int i=0;i<shaders.length;i++){
            if (shaders[i]!=null){
                shaders[i].unloadFromGpuMemory(gl);
            }
        }

        if (glid!=-1){
            gl.asGL2ES2().glDeleteProgram(glid);
            glid = -1;
            uniforms.removeAll();
            vertexAttributes.removeAll();
        }
    }

    private void loadAssociates(GL gl){
        uniforms.removeAll();
        vertexAttributes.removeAll();

        //find basic uniforms names
        //this might not be all names because of struct types
        findUniforms(gl,shaders[0]);
        findUniforms(gl,shaders[1]);
        findUniforms(gl,shaders[2]);
        findUniforms(gl,shaders[3]);
        findUniforms(gl,shaders[4]);
        //find vertex attributes
        findVertexAttributs(gl, shaders[0]);
    }

    private void findUniforms(GL gl, Shader shader){
        if (shader==null) return;
        final Chars code = shader.getCode();
        if (code==null) return;

        final Chars[] lines = code.split('\n');
        for (int i=0;i<lines.length;i++){
            if (lines[i].startsWith(UNIFORM)){
                final Chars[] words = lines[i].split(' ');
                final Chars name = words[2].replaceAll(';', ' ').trim();
                final Chars type = words[1].trim();
                final Integer itype = ShaderUtils.GLSLtoGLType(type);
                final int nuid = gl.asGL2ES2().glGetUniformLocation(glid, name);
                final int blockIndex = gl.asGL2ES3().glGetUniformBlockIndex(glid, name);
                if (itype == null){
                    //likely a structure type
                    uniforms.add(name, new Uniform(glid, nuid, -1, blockIndex));
                } else {
                    uniforms.add(name, new Uniform(glid, nuid, itype, blockIndex));
                }
            }
        }
    }

    private void findVertexAttributs(GL gl, Shader shader){
        if (shader==null) return;
        final Chars code = shader.getCode();
        if (code==null) return;

        final Chars[] lines = code.split('\n');
        for (int i=0;i<lines.length;i++){
            if (lines[i].startsWith(LAYOUT)){
                final int idx = lines[i].getFirstOccurence(')');
                Chars line = lines[i].truncate(idx+1, -1).trim();
                if (line.endsWith(';')) line = line.truncate(0, line.getCharLength()-1);
                final Chars[] parts = line.split(' ');
                final Chars type = parts[1];
                final Chars name = parts[2];

                final int layoutIdx = gl.asGL2ES2().glGetAttribLocation(glid, name);
                final Integer glType = ShaderUtils.GLSLtoGLType(type);
                if (glType == null) {
                    //structure type
                    final VertexAttribute att = new VertexAttribute(glid, layoutIdx, -1, -1);
                    vertexAttributes.add(name, att);
                } else {
                    final int slotSize = ShaderUtils.VertexAttributeSlotSize(glType);
                    final VertexAttribute att = new VertexAttribute(glid, layoutIdx, glType, slotSize);
                    vertexAttributes.add(name, att);
                }
            }
        }
    }

}
