
package science.unlicense.gpu.impl.opengl.shader;

import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.GLState;

/**
 *
 * @author Johann Sorel
 */
public class Uniform {

    private final int programId;
    private final int gpuId;
    private final int type;
    private final int blockIndex;

    public Uniform(int programId, int gpuId, int type, int blockIndex) {
        this.programId = programId;
        this.gpuId = gpuId;
        this.type = type;
        this.blockIndex = blockIndex;
    }

    public boolean exist(){
        return gpuId >= 0;
    }

    public int getProgramId() {
        return programId;
    }

    public int getGpuId() {
        return gpuId;
    }

    public int getType() {
        return type;
    }

    public void setInt(GL2ES2 gl, int value){
        assert inValidState(gl);
        if (exist()) gl.glUniform1i(gpuId, value);
    }

    public void setFloat(GL2ES2 gl, float value){
        assert inValidState(gl);
        if (exist()) gl.glUniform1f(gpuId, value);
    }

    public void setVec2(GL2ES2 gl, float[] value){
        assert inValidState(gl);
        if (exist()) gl.glUniform2fv(gpuId, value);
    }

    public void setVec3(GL2ES2 gl, float[] value){
        assert inValidState(gl);
        if (exist()) gl.glUniform3fv(gpuId, value);
    }

    public void setVec4(GL2ES2 gl, float[] value){
        assert inValidState(gl);
        if (exist()) gl.glUniform4fv(gpuId, value);
    }

    public void setMat3(GL2ES2 gl, float[] value){
        assert inValidState(gl);
        if (exist()) gl.glUniformMatrix3fv(gpuId, true, value);
    }

    public void setMat4(GL2ES2 gl, float[] value){
        assert inValidState(gl);
        if (exist()) gl.glUniformMatrix4fv(gpuId, true, value);
    }

    private boolean inValidState(GL2ES2 gl) throws ShaderException{
        final int pid = GLState.getCurrentProgramId(gl);
        if (pid!=programId) throw new ShaderException("Uniform is from a different program, current is "+pid+" was expecting program "+ programId);
        return true;
    }

}
