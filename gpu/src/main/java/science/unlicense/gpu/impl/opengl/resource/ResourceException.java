
package science.unlicense.gpu.impl.opengl.resource;

/**
 * Resource loading or unloading exception.
 *
 * @author Johann Sorel
 */
public class ResourceException extends RuntimeException{

    public ResourceException() {
    }

    public ResourceException(String message) {
        super(message);
    }

    public ResourceException(Throwable cause) {
        super(cause);
    }

    public ResourceException(String message, Throwable cause) {
        super(message, cause);
    }

}
