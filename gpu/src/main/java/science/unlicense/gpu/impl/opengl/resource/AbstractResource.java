
package science.unlicense.gpu.impl.opengl.resource;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Loggers;

/**
 * Abstract resouce implementation.
 * It's only purpose is to track unreleased gpu data.
 *
 * @author Johann Sorel
 */
public abstract class AbstractResource extends CObject implements Resource{

    private final Exception ex = new Exception();
    protected CharArray name = Chars.EMPTY;

    private boolean forgetOnLoad = false;

    public AbstractResource(){
        this(false);
    }

    public AbstractResource(boolean forgetOnLoad){
        this.forgetOnLoad = forgetOnLoad;
        ex.fillInStackTrace();
    }

    @Override
    public CharArray getName() {
        return name;
    }

    @Override
    public void setName(CharArray name) {
        CObjects.ensureNotNull(name, "name");
        this.name = name;
    }

    @Override
    public boolean isForgetOnLoad() {
        return forgetOnLoad;
    }

    @Override
    public void setForgetOnLoad(boolean forget) {
        this.forgetOnLoad = forget;
    }

    @Override
    protected void finalize() throws Throwable {
        final int gpuId = getGpuID();
        if (gpuId>=0){
            Loggers.get().info(
                    new Chars("Unreleased GPU resource : "+getClass().getSimpleName()
                    +" with gpu-id="+gpuId));
            ex.printStackTrace();
        }
        super.finalize();
    }

}
