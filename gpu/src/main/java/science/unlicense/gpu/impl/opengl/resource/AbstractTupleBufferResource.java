
package science.unlicense.gpu.impl.opengl.resource;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.tuple.DecoratedTupleGrid;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractTupleBufferResource extends DecoratedTupleGrid implements Resource{

    //store initialization stack trace, only when assertions are enabled
    private Throwable ex;
    protected CharArray name = Chars.EMPTY;
    private boolean forgetOnLoad = false;

    protected TupleGrid base;
    protected Buffer buffer;
    protected boolean dirty = true;
    protected int bytePerElement = 0;
    protected int gpuType;
    protected int size;
    //gpu id
    protected final int[] bufferId = new int[]{-1};
    //contains the steps between each value of a dimension
    private final int[] dimSteps;


    protected SampleSystem sampleSystem;
    protected NumberType sampleType;
    protected int nbSample;
    protected int bitsPerSample;
    /**
     * = nbSample*bitsPerSample
     */
    protected int bitsPerDoxel;
    protected Extent.Long dimensions;
    protected BBox fullBbox;

    public AbstractTupleBufferResource() {
        assert((ex=new Throwable())!=null);
        updateModel(new Extent.Long(0l),UInt8.TYPE,1);

        this.dimSteps = new int[this.dimensions.getDimension()];
        this.dimSteps[0] = 1;
        for (int i=1;i<dimSteps.length;i++){
            this.dimSteps[i] = this.dimSteps[i-1]* (int) dimensions.getL(i-1);
        }
    }

    @Override
    public BBox getCoordinateGeometry() {
        return new BBox(dimensions);
    }

    @Override
    public SampleSystem getCoordinateSystem() {
        return base.getCoordinateSystem();
    }

    public void setBuffer(Buffer buffer, int tupleSize) {
        this.setBuffer(buffer, GLUtilities.primitiveTypeToGlType(buffer.getNumericType()), tupleSize);
    }

    /**
     * Set VBO datas.
     *
     * OpenGL constraints : GL2ES2
     *
     * @param buffer data buffer
     * @param tupleSize number of value for each tuple
     */
    public void setIntBuffer(Buffer buffer, int tupleSize) {
        this.setBuffer(buffer, GLC.TYPE.INT, tupleSize);
    }

    /**
     * Set VBO datas.
     *
     * @param buffer data buffer
     * @param tupleSize number of value for each tuple
     */
    public void setFloatBuffer(Buffer buffer, int tupleSize) {
        this.setBuffer(buffer, GLC.TYPE.FLOAT, tupleSize);
    }

    /**
     * Set datas.
     *
     * @param buffer data buffer
     * @param gpuType gpu data type
     * @param tupleSize number of value for each tuple
     */
    public void setBuffer(Buffer buffer, int gpuType, int tupleSize) {
        this.buffer = buffer;
        this.size = (int) buffer.getNumbersSize();
        this.dimensions = new Extent.Long(buffer.getNumbersSize()/tupleSize);
        updateModel(dimensions, GLUtilities.glTypeToPrimitiveType(gpuType), tupleSize);
        this.dirty = true;

        base = InterleavedTupleGrid.create(buffer, sampleType, sampleSystem, dimensions);
    }

    protected void updateModel(Extent.Long dimensions, NumberType sampleType, int nbSample) {
        this.sampleSystem = new UndefinedSystem(nbSample);
        this.dimensions = dimensions;
        this.sampleType = sampleType;
        this.fullBbox = new BBox(dimensions);
        this.nbSample = sampleSystem.getNumComponents();
        this.bitsPerSample = sampleType.getSizeInBits();
        this.bitsPerDoxel = nbSample*bitsPerSample;
        gpuType = GLUtilities.primitiveTypeToGlType(sampleType);
        bytePerElement = GLUtilities.byteSize(gpuType);
    }

    @Override
    protected TupleGrid getBase() {
        if (base == null) {
            throw new RuntimeException("Resource is not loaded in memory");
        }
        return base;
    }

    @Override
    public TupleGridCursor cursor() {
        if (base == null) {
            throw new RuntimeException("Resource is not loaded in memory");
        }
        return base.cursor();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public CharArray getName() {
        return name;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setName(CharArray name) {
        CObjects.ensureNotNull(name, "name");
        this.name = name;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isForgetOnLoad() {
        return forgetOnLoad;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setForgetOnLoad(boolean forget) {
        this.forgetOnLoad = forget;
    }

    /**
     * Get the number of bytes use for each value in the buffer.
     * This size is derivate from the gpu data type;
     *
     * @return number of byte per value. 0 is data buffer has not been set
     */
    public int getBytePerElement() {
        return bytePerElement;
    }

    public int getGpuType() {
        return gpuType;
    }

    /**
     * Get number of tuples.
     * size / tupleSize
     *
     * @return number of tuples.
     */
    public int getTupleCount(){
        return (int) (size / nbSample);
    }

    /**
     * Get primitive buffer.
     * Can be null.
     *
     * @return Buffer or null if not loaded on system memory
     */
    public Buffer getPrimitiveBuffer() {
        return buffer;
    }

//
//    @Override
//    public void getTuple(int[] coordinate, TupleRW tuple) {
//        switch (sampleType.getPrimitiveCode()) {
//            case Primitive.TYPE_BYTE : {
//                int k = getStartIndexInByte(coordinate);
//                for (int i=0;i<nbSample;i++,k+=1) tuple.set(i,buffer.readByte(k));
//                break; }
//            case Primitive.TYPE_1_BIT : {
//                final int k = getStartIndexInBits(coordinate);
//                final int kb = k/8;
//                final int offset = k%8;
//
//                final DataCursor cursor = buffer.cursor();
//                cursor.setByteOffset(kb);
//                cursor.setBitOffset(offset);
//                for (int i=0;i<nbSample;i++){
//                    tuple.set(i,cursor.readBit(1));
//                }
//                break;}
//            case Primitive.TYPE_2_BIT : {
//                final int k = getStartIndexInBits(coordinate);
//                final int kb = k/8;
//                final int offset = k%8;
//
//                final DataCursor cursor = buffer.cursor();
//                cursor.setByteOffset(kb);
//                cursor.setBitOffset(offset);
//                for (int i=0;i<nbSample;i++){
//                    tuple.set(i,cursor.readBit(2));
//                }
//                break;}
//            case Primitive.TYPE_4_BIT : {
//                final int k = getStartIndexInBits(coordinate);
//                final int kb = k/8;
//                final int offset = k%8;
//
//                final DataCursor cursor = buffer.cursor();
//                cursor.setByteOffset(kb);
//                cursor.setBitOffset(offset);
//                for (int i=0;i<nbSample;i++){
//                    tuple.set(i,cursor.readBit(4));
//                }
//                break;}
//            case Primitive.TYPE_UBYTE : {
//                final int start = getStartIndexInByte(coordinate);
//                for (int i=0,k=start;i<nbSample;i++,k+=1) {
//                    tuple.set(i, buffer.readByte(k) & 0xff);
//                }
//                break;}
//            case Primitive.TYPE_INT : {
//                final int start = getStartIndexInByte(coordinate);
//                for (int i=0,k=start;i<nbSample;i++,k+=4) {
//                    tuple.set(i, buffer.readInt(k));
//                }
//                break;}
//            case Primitive.TYPE_UINT : {
//                final int start = getStartIndexInByte(coordinate);
//                for (int i=0,k=start;i<nbSample;i++,k+=4) {
//                    tuple.set(i, buffer.readUInt(k));
//                }
//                break;}
//            case Primitive.TYPE_LONG : {
//                final int start = getStartIndexInByte(coordinate);
//                for (int i=0,k=start;i<nbSample;i++,k+=8) {
//                    tuple.set(i, buffer.readLong(k));
//                }
//                break;}
//            case Primitive.TYPE_FLOAT : {
//                final int start = getStartIndexInByte(coordinate);
//                for (int i=0,k=start;i<nbSample;i++,k+=4) {
//                    tuple.set(i, buffer.readFloat(k));
//                }
//                break;}
//            case Primitive.TYPE_DOUBLE : {
//                final int start = getStartIndexInByte(coordinate);
//                for (int i=0,k=start;i<nbSample;i++,k+=8) {
//                    tuple.set(i, buffer.readDouble(k));
//                }
//                break;}
//            default: throw new UnsupportedOperationException("Not supported yet.");
//        }
//    }
//
//    @Override
//    public void setTuple(int[] coordinate, Tuple tuple) {
//        if (sampleType == NumericType.TYPE_1_BIT){
//            final int start = getStartIndexInBits(coordinate);
//            int byteOffset = start/8;
//            int bitoffset = start%8;
//            for (int i=0;i<nbSample;i++){
//                byte b = buffer.readByte(byteOffset);
//                boolean v = tuple.get(i) != 0;
//                if (v){
//                    b |= 1 << (7-bitoffset);
//                } else {
//                    b &= ~(1 << (7-bitoffset));
//                }
//
//                this.buffer.writeByte(b, byteOffset);
//                //prepare next iteration
//                if (bitoffset == 7){
//                    byteOffset++;
//                    bitoffset=0;
//                } else {
//                    bitoffset++;
//                }
//            }
//        } else if (sampleType == NumericType.TYPE_2_BIT){
//            throw new RuntimeException("Unexpected type "+ sampleType);
//        } else if (sampleType == NumericType.TYPE_4_BIT){
//            throw new RuntimeException("Unexpected type "+ sampleType);
//        } else if (sampleType == NumericType.TYPE_BYTE){
//            final int start = getStartIndexInByte(coordinate);
//            final byte[] buffer = tuple.toByte();
//            this.buffer.writeByte(buffer, start);
//        } else if (sampleType == NumericType.TYPE_UBYTE){
//            final int start = getStartIndexInByte(coordinate);
//            final int[] buffer = tuple.toInt();
//            this.buffer.writeUByte(buffer, start);
//        } else if (sampleType == NumericType.TYPE_SHORT){
//            final int start = getStartIndexInByte(coordinate);
//            final short[] buffer = tuple.toShort();
//            this.buffer.writeShort(buffer, start);
//        } else if (sampleType == NumericType.TYPE_USHORT){
//            final int start = getStartIndexInByte(coordinate);
//            final int[] buffer = tuple.toInt();
//            this.buffer.writeUShort(buffer, start);
//        } else if (sampleType == NumericType.TYPE_INT){
//            final int start = getStartIndexInByte(coordinate);
//            final int[] buffer = tuple.toInt();
//            this.buffer.writeInt(buffer, start);
//        } else if (sampleType == NumericType.TYPE_UINT){
//            final int start = getStartIndexInByte(coordinate);
//            final long[] buffer = tuple.toLong();
//            this.buffer.writeUInt(buffer, start);
//        } else if (sampleType == NumericType.TYPE_LONG){
//            final int start = getStartIndexInByte(coordinate);
//            final long[] buffer = tuple.toLong();
//            this.buffer.writeLong(buffer, start);
//        } else if (sampleType == NumericType.TYPE_FLOAT){
//            final int start = getStartIndexInByte(coordinate);
//            final float[] buffer = tuple.toFloat();
//            this.buffer.writeFloat(buffer, start);
//        } else if (sampleType == NumericType.TYPE_DOUBLE){
//            final int start = getStartIndexInByte(coordinate);
//            final double[] buffer = tuple.toDouble();
//            this.buffer.writeDouble(buffer, start);
//        } else {
//            throw new RuntimeException("Unexpected type "+ sampleType);
//        }
//    }
//
//    protected int getStartIndexInByte(int[] coordinate){
//        return getStartIndexInBits(coordinate)/8;
//    }
//
//    protected int getStartIndexInByte(int coordinate){
//        return getStartIndexInBits(coordinate)/8;
//    }
//
//    protected int getStartIndexInBits(int coordinate){
//        return coordinate*bitsPerDoxel;
//    }
//
//    protected int getStartIndexInBits(int[] coordinate){
//        //we do not check coordinate
//        int position = 0;
//        for (int i=0;i<coordinate.length;i++){
//            if (coordinate[i]>=dimensions.getL(i)){
//                throw new RuntimeException("Invalid coordinate at index "+i+" value "+coordinate[i]+", out of buffer range");
//            }
//            position += dimSteps[i]*coordinate[i]*bitsPerDoxel;
//        }
//        return position;
//    }

    /**
     * Indicate if the system data buffer values has been changed.
     *
     * @return true if datas has been changed compared to the last time
     *          they were loaded on the gpu.
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     * Declare the buffer to be dirty.
     * Use when direct modification are made to the buffer.
     */
    public void notifyDirty(){
        dirty = true;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int getGpuID() {
        return bufferId[0];
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isOnSystemMemory() {
        return buffer != null;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isOnGpuMemory() {
        return bufferId[0] != -1;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void unloadFromSystemMemory(GL gl) throws ResourceException {
        if (buffer != null) {
            buffer.dispose();
            buffer = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        final int gpuId = getGpuID();
        if (gpuId>=0){
            Loggers.get().info(
                    new Chars("Unreleased GPU resource : "+getClass().getSimpleName()
                    +" with gpu-id="+gpuId));
            if (ex!=null){
                ex.printStackTrace();
            } else {
                Loggers.get().info(new Chars("Enable assertions to view initialization stack trace"));
            }
        }
        super.finalize();
    }

}
