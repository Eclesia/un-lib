
package science.unlicense.gpu.impl.opengl.shader;

import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.impl.opengl.GLState;
import science.unlicense.gpu.impl.opengl.resource.VBO;

/**
 * Vertex shader attribute (Layout).
 *
 * @author Johann Sorel
 */
public class VertexAttribute {

    private final int programId;
    private final int layoutIdx;
    private final int type;
    private final int slotSize;

    public VertexAttribute(int programId, int layoutIdx, int type, int slotSize) {
        this.programId = programId;
        this.layoutIdx = layoutIdx;
        this.type = type;
        this.slotSize = slotSize;
    }

    public int getProgramId() {
        return programId;
    }

    public int getLayoutIdx() {
        return layoutIdx;
    }

    /**
     *
     * @return one of Uniform.TYPE_*
     */
    public int getType() {
        return type;
    }

    /**
     * Slot size is the number of layout index used.
     *
     * @return
     */
    public int getSlotSize() {
        return slotSize;
    }

    /**
     * Since the compiler may exclude some layout because they are not used.
     *
     * @return true if layout exist. (layout index >= 0)
     */
    public boolean exist(){
        return layoutIdx >= 0;
    }

    /**
     * Bind given vbo to this vertex attribute.
     * Has no effect if vbo is null or layout has no index.
     *
     * @param gl
     * @param vbo
     */
    public void enable(GL2ES2 gl, VBO vbo){
        if (vbo!=null && exist() && inValidState(gl)){
            //ensure it is loaded before binding it
            vbo.loadOnGpuMemory(gl);
            for (int i=0;i<slotSize;i++){
                gl.glEnableVertexAttribArray(layoutIdx+i);
            }
            vbo.bind(gl, layoutIdx);
        }
    }

    public void enable(GL2ES2 gl, VBO vbo, int offset){
        if (vbo!=null && exist() && inValidState(gl)){
            //ensure it is loaded before binding it
            vbo.loadOnGpuMemory(gl);
            for (int i=0;i<slotSize;i++){
                gl.glEnableVertexAttribArray(layoutIdx+i);
            }
            vbo.bind(gl, layoutIdx, false, offset);
        }
    }

    public void disable(GL2ES2 gl){
        if (exist() && inValidState(gl)){
            for (int i=0;i<slotSize;i++){
                gl.glDisableVertexAttribArray(layoutIdx+i);
            }
        }
    }

    private boolean inValidState(GL2ES2 gl) throws ShaderException{
        final int pid = GLState.getCurrentProgramId(gl);
        if (pid!=programId) throw new ShaderException("Uniform is from a different program, current is "+pid+" was expecting program "+ programId);
        return true;
    }
}
