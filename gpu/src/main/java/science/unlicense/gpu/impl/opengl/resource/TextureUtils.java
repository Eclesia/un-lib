
package science.unlicense.gpu.impl.opengl.resource;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DataCursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GL3;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLC;
import static science.unlicense.gpu.impl.opengl.GLC.Texture.Type.*;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRW;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.IndexedColorModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.image.impl.colorspace.SRGB;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public final class TextureUtils {

    private static final int[] ORDER_RGB  = new int[]{0,1,2};
    private static final int[] ORDER_RGBA = new int[]{0,1,2,3};
    private static final int[] ORDER_BGR  = new int[]{2,1,0};
    private static final int[] ORDER_BGRA = new int[]{2,1,0,3};

    private TextureUtils() {}

    public static int loadTexture(GL gl, Image image, TextureModel info){

        final int[] textureId = new int[1];
        gl.asGL1().glGenTextures(textureId);
        gl.asGL1().glBindTexture(GL_TEXTURE_2D, textureId[0]);


        final Buffer imageData = image.getDataBuffer();
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        //TODO calculate unpack alignment value correctly
        // http://stackoverflow.com/questions/15052463/given-the-pitch-of-an-image-how-to-calculate-the-gl-unpack-alignment
        gl.asGL1().glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        if (info.isCompressed()){
            gl.asGL1().glCompressedTexImage2D(info.getTarget(), 0, info.getFormat(),
                    width,height, 0, imageData);
            GLUtilities.checkGLErrorsFail(gl);
        } else {
            gl.asGL1().glTexImage2D(info.getTarget(), 0, info.getInternalFormat(),
                    width,height, 0, info.getFormat(), info.getType(), imageData);
            GLUtilities.checkGLErrorsFail(gl);
        }

        for (int i=0,n=info.getParameterSize();i<n;i++){
            final int[] param = info.getParameter(i);
            gl.asGL1().glTexParameterf(GL_TEXTURE_2D, param[0], param[1]);
        }
        if (info.hasMipMap()){
            gl.asGL2ES2().glGenerateMipmap(GL_TEXTURE_2D);
        }

        return textureId[0];
    }

    /**
     * Loads a texture using glTexImage2D.
     * Does not set any parameters, does not bind the texture.
     *
     * It only check and fix the image pixel data properly.
     *
     * @param gltype first parameter of glTexImage2D
     */
    public static void loadTexture(final GL gl, final Image image, final int gltype){

        //GL image info
        Buffer imageData = null;
        int imageInternalFormat = 0;
        int imageFormat = 0;
        boolean compressed = false;

        //UN image
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final ImageModel rm = image.getRawModel();
        final ImageModel cm = image.getColorModel();
        final Chars rmname = rm.getName();

        if (rmname != null) {
            if (rmname.equals(new Chars("DXT1"))) {
                compressed = true;
                imageFormat = GLC.Texture.CompressedFormat.DXT1;
                final Buffer dataBuffer = image.getDataBuffer();
                imageData = dataBuffer;
            } else if (rmname.equals(new Chars("DXT2"))) {
                throw new RuntimeException("DXT2 Not supported yet.");
            } else if (rmname.equals(new Chars("DXT3"))) {
                compressed = true;
                imageFormat = GLC.Texture.CompressedFormat.DXT3;
                final Buffer dataBuffer = image.getDataBuffer();
                imageData = dataBuffer;
            } else if (rmname.equals(new Chars("DXT4"))) {
                throw new RuntimeException("DXT4 Not supported yet.");
            } else if (rmname.equals(new Chars("DXT5"))) {
                compressed = true;
                imageFormat = GLC.Texture.CompressedFormat.DXT5;
                final Buffer dataBuffer = image.getDataBuffer();
                imageData = dataBuffer;
            }
        }

        if (cm instanceof DerivateModel){
            final DerivateModel dcm = (DerivateModel) cm;
            final ColorSystem cs = (ColorSystem) dcm.getSampleSystem();
            final int nbSample = rm.getSampleSystem().getNumComponents();
            final NumberType dataType = rm.getNumericType();
            final int[] mapping = dcm.getMapping();
            final Buffer dataBuffer = image.getDataBuffer();

            if (nbSample == 3
                    && dataType == UInt8.TYPE
                    && Arrays.equals(mapping,ORDER_RGB) ){
                //image is RGBA, we can reuse bit buffer directly
                imageInternalFormat = GL_COMPRESSED_RGB;
                imageFormat = GL_RGB;
                imageData = dataBuffer;

            } else if (nbSample == 4
                    && dataType == UInt8.TYPE
                    && Arrays.equals(mapping,ORDER_RGBA)
                    && cs.isAlphaPremultiplied()){
                //image is RGBA, we can reuse bit buffer directly
                imageInternalFormat = GL_COMPRESSED_RGBA;
                imageFormat = GL_RGBA;
                imageData = dataBuffer;

            } else if (nbSample == 3
                    && dataType == UInt8.TYPE
                    && Arrays.equals(mapping, ORDER_BGR)){
                //image is BGR, we can reuse bit buffer directly
                imageInternalFormat = GL_RGB;
                imageFormat = GL_BGR;
                imageData = dataBuffer;

            } else if (nbSample == 4
                    && dataType == UInt8.TYPE
                    && Arrays.equals(mapping,ORDER_BGRA)
                    && cs.isAlphaPremultiplied()){
                //image is BGRA, we can reuse bit buffer directly
                imageInternalFormat = GL_RGBA;
                imageFormat = GL_BGRA;
                imageData = dataBuffer;
            }
        } else if (cm instanceof InterleavedModel){
            final InterleavedModel dcm = (InterleavedModel) cm;
            final ColorSystem cs = (ColorSystem) dcm.getSampleSystem();
            final int nbSample = cs.getNumComponents();
            final NumberType dataType = rm.getNumericType();
            final Buffer dataBuffer = image.getDataBuffer();
            final ColorSpace colorSpace = cs.getColorSpace();
            //TODO test

            if (nbSample == 3 && dataType == UInt8.TYPE ){
                //image is RGBA, we can reuse bit buffer directly
                imageInternalFormat = GL_COMPRESSED_RGB;
                imageFormat = GL_RGB;
                imageData = dataBuffer;

            } else if (nbSample == 4
                    && dataType == UInt8.TYPE
                    && cs.isAlphaPremultiplied()){
                //image is RGBA, we can reuse bit buffer directly
                imageInternalFormat = GL_COMPRESSED_RGBA;
                imageFormat = GL_RGBA;
                imageData = dataBuffer;
            }
        }

        if (imageData == null){
            //format can not be supported directly in opengl.
            throw new RuntimeException("Image format not supported, use makeCompatible method to transform it.");
        }

        //TODO calculate unpack alignment value correctly
        // http://stackoverflow.com/questions/15052463/given-the-pitch-of-an-image-how-to-calculate-the-gl-unpack-alignment
        gl.asGL1().glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        if (!compressed){
            gl.asGL1().glTexImage2D(gltype, 0,imageInternalFormat, width,height, 0, imageFormat, GL_UNSIGNED_BYTE, imageData);
        } else {
            gl.asGL1().glCompressedTexImage2D(gltype, 0,imageFormat, width,height, 0, imageData);
        }
        GLUtilities.checkGLErrorsFail(gl);

        imageData.dispose();
    }

    /**
     * Download a texture from graphics memory and return it as an image.
     *
     * OpenGL constraints : GL3+
     *
     * @param gl Opengl content
     * @param textureId texture id
     * @param textureType texture type
     * @param extent texture size
     * @param glFormat texture color type
     * @param glSampleType texture sample type
     * @return Image
     */
    public static Image downloadTexture(final GL gl, final int textureId,
            final Extent.Long extent, TextureModel model){
        //TODO we should be able to retrieve most texture information from GL
        final GL3 gl3 = gl.asGL3();

        final int target = model.getTarget();
        final int type = model.getType();
        final int format = model.getFormat();

        final NumberType imageSampleType = GLUtilities.glTypeToPrimitiveType(type);
        final int sampleNbByte = imageSampleType.getSizeInBits() / 8;
        final int nbSample = glFormatToNbSample(format);

        final Buffer.Int8 buffer = gl.getBufferFactory().createInt8((int) (extent.getL(0)*extent.getL(1)*sampleNbByte*nbSample));
        gl3.asGL2ES2().glBindTexture(target,textureId);
        gl3.asGL1().glGetTexImage(GL_TEXTURE_2D, 0, format, type, buffer);
        GLUtilities.checkGLErrorsFail(gl3);

        final science.unlicense.common.api.buffer.Buffer bank = buffer.getBuffer();

        final ImageModel sm = new InterleavedModel(new UndefinedSystem(nbSample), imageSampleType);

        final ImageModel cm;
        if (imageSampleType==UInt8.TYPE){
            switch(format){
                case GLC.Texture.Format.BGR : cm = DerivateModel.create(sm, ORDER_BGR, null, null, ColorSystem.RGB_8BITS); break;
                case GLC.Texture.Format.BGRA : cm = DerivateModel.create(sm, ORDER_BGRA, null, null, ColorSystem.RGBA_8BITS_PREMUL); break;
                case GLC.Texture.Format.RGB : cm = DerivateModel.create(sm, ORDER_RGB, null, null, ColorSystem.RGB_8BITS); break;
                case GLC.Texture.Format.RGBA : cm = DerivateModel.create(sm, ORDER_RGBA, null, null, ColorSystem.RGBA_8BITS_PREMUL); break;
                default : //use first band
                    cm = DerivateModel.create(sm, new int[]{0,0,0}, null,null, ColorSystem.RGB_FLOAT);
            }
        } else {
            final int[] mapping = new int[4];
            Arrays.fill(mapping, -1);
            final Affine1[] t = new Affine1[4];
            final float[] scale = new float[4];
            final float[] offset = new float[4];
            for (int i=0;i<nbSample;i++){
                mapping[i] = i;
                t[i] = new Affine1(0.5,0.5);
            }
            cm = DerivateModel.create(sm, mapping, null, null, new ColorSystem(SRGB.INSTANCE, true, false, t));
        }

        return new DefaultImage(bank, extent, sm, cm);
    }

    public static int glFormatToNbSample(int glFormat){
        if (glFormat == GLC.Texture.Format.RED || glFormat == GLC.Texture.Format.RED_INTEGER){
            return 1;
        } else if (glFormat == GLC.Texture.Format.RG || glFormat == GLC.Texture.Format.RG_INTEGER){
            return 2;
        } else if (glFormat == GLC.Texture.Format.RGB || glFormat == GLC.Texture.Format.RGB_INTEGER
               || glFormat == GLC.Texture.Format.BGR || glFormat == GLC.Texture.Format.BGR_INTEGER){
            return 3;
        } else if (glFormat == GLC.Texture.Format.RGBA || glFormat == GLC.Texture.Format.RGBA_INTEGER
              || glFormat == GLC.Texture.Format.BGRA || glFormat == GLC.Texture.Format.BGRA_INTEGER){
            return 4;
        } else if (glFormat == GLC.Texture.Format.DEPTH_COMPONENT){
            return 1;
        } else if (glFormat == GLC.Texture.Format.DEPTH_STENCIL){
            return 4;
        } else {
            throw new RuntimeException("Unsupported type :" + glFormat);
        }
    }

    /**
     * Check if an image is opaque.
     * Does not contain any alpha component not 1.
     *
     * @param image
     * @return true if image is opaque
     */
    public static boolean isOpaque(Image image){

        final ImageModel cm = image.getColorModel();
        if (cm instanceof IndexedColorModel){
            final IndexedColorModel icm = (IndexedColorModel) cm;
            final Color[] palette = icm.getPalette().palette;
            for (int i=0;i<palette.length;i++){
                if (palette[i].getAlpha() < 1f){
                    return false;
                }
            }
            return true;
        } else if (cm instanceof DerivateModel){
            final DerivateModel dcm = (DerivateModel) cm;
            if (dcm.getMapping()[3] == -1){
                //no alpha band
                return true;
            }
        }

        //we have to check the full image
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor cursor = image.getColorModel().asTupleBuffer(image).cursor();
        final ColorRW pixel = Colors.castOrWrap(cursor.samples(), cs);
        while (cursor.next()) {
            if (pixel.getAlpha() < 1f){
                return false;
            }
        }

        return true;
    }

    /**
     * OpenGL constraints :
     * - GL3+ : texture 2D multisample
     * - GL2ES2+ : texture 3D
     *
     * @param extent
     * @param gl
     * @param info
     */
    public static void createTexture(Extent extent, GL gl, TextureModel info) {
        final int width = (int) extent.get(0);
        final int height = (int) extent.get(1);

        final int target = info.getTarget();
        final int type = info.getType();
        final int format = info.getFormat();
        final int internalFormat = info.getInternalFormat();

        if (target==GL_TEXTURE_2D){
            //create an empty byte buffer for datas
            //we could use GL_ARB_clear_texture but is it not always available
            final NumberType imageSampleType = GLUtilities.glTypeToPrimitiveType(type);
            final int sampleNbByte = imageSampleType.getSizeInBits() / 8;
            final int nbSample = glFormatToNbSample(format);
            //add an extra line : because some graphics expect more or less %2 sizes
            //the extra line ensure there is enough spare bytes.
            // NOTE : find where in the spec we can detect this case
            final Buffer.Int8 buffer = gl.getBufferFactory().createInt8((width+1)*height*sampleNbByte*nbSample);
            gl.asGL1().glTexImage2D(
                    GL_TEXTURE_2D,                          //GLenum target
                    0,                                      //GLint level
                    internalFormat,                         //GLint internalFormat
                    width,                                  //GLsizei width
                    height,                                 //GLsizei height
                    0,                                      //GLint border
                    format,                                 //GLenum format
                    type,                                   //GLenum type
                    buffer.getBuffer());                    //const GLvoid * data
        } else if (target==GL_TEXTURE_2D_MULTISAMPLE){
            final GL3 gl3 = gl.asGL3();
            final int sampling = (int) extent.get(2);
            gl3.glTexImage2DMultisample(
                    target,                                 //GLenum target
                    sampling,                               //GLsizei samples
                    internalFormat,                         //GLint internalformat
                    width,                                  //GLsizei width
                    height,                                 //GLsizei height
                    false);                                 //GLboolean fixedsamplelocations
        } else if (target==GL_TEXTURE_3D){
            //create an empty byte buffer for datas
            //we could use GL_ARB_clear_texture but is it not always available
            final GL2ES2 gl2es2 = gl.asGL2ES2();
            final NumberType imageSampleType = GLUtilities.glTypeToPrimitiveType(type);
            final int sampleNbByte = imageSampleType.getSizeInBits() / 8;
            final int nbSample = glFormatToNbSample(format);
            final int depth = (int) extent.get(2);
            final Buffer.Int8 buffer = gl.getBufferFactory().createInt8(width*height*depth*sampleNbByte*nbSample);
            gl2es2.asGL1().glTexImage3D(
                    target,                                 //GLenum target
                    0,                                      //GLint level,
                    internalFormat,                         //GLint internalformat
                    width,                                  //GLsizei width
                    height,                                 //GLsizei height
                    depth,                                  //GLsizei depth
                    0,                                      //GLint border
                    format,                                 //GLenum format
                    type,                                   //GLenum type
                    buffer);                                //const GLvoid * data
        } else {
            throw new RuntimeException("Unexpected target type : "+target);
        }

        for (int i=0,n=info.getParameterSize();i<n;i++){
            final int[] param = info.getParameter(i);
            gl.asGL1().glTexParameterf(GL_TEXTURE_2D, param[0], param[1]);
        }

    }

    /**
     * All raw models and color models can not be used directly in opengl.
     * This method will transform the image to match opengl capabilities.
     *
     * @param image
     * @param preserveColors
     * @return
     */
    public static Image makeCompatible(Image image, boolean preserveColors){

        final ImageModel rm = image.getRawModel();
        final ImageModel cm = image.getColorModel();

        if (rm.getName() != null && rm.getName().startsWith(new Chars("DXT"))){
            //DXT compression, ok
            return image;
        }

        if (preserveColors && cm!=null){
            if (cm instanceof DerivateModel && rm instanceof InterleavedModel){
                final DerivateModel dcm = (DerivateModel) cm;
                final ColorSystem cs = (ColorSystem) dcm.getSampleSystem();
                final int nbSample = cs.getNumComponents();
                final NumberType dataType = rm.getNumericType();
                final int[] mapping = dcm.getMapping();

                if (nbSample == 3
                        && dataType == UInt8.TYPE
                        && Arrays.equals(mapping,ORDER_RGB) ){
                    //image is RGBA, we can reuse bit buffer directly
                    return image;

                } else if (nbSample == 4
                        && dataType == UInt8.TYPE
                        && Arrays.equals(mapping,ORDER_RGBA)
                        && cs.isAlphaPremultiplied()){
                    //image is RGBA, we can reuse bit buffer directly
                    return image;

                } else if (nbSample == 3
                        && dataType == UInt8.TYPE
                        && Arrays.equals(mapping, ORDER_BGR)){
                    //image is BGR, we can reuse bit buffer directly
                    return image;

                } else if (nbSample == 4
                        && dataType == UInt8.TYPE
                        && Arrays.equals(mapping,ORDER_BGRA)
                        && cs.isAlphaPremultiplied()){
                     //image is BGRA, we can reuse bit buffer directly
                    return image;
                }
            }


            //format can not be supported directly in opengl.
            //convert it to a RGBA buffer.
            final int width = (int) image.getExtent().getL(0);
            final int height = (int) image.getExtent().getL(1);
            final Image rgba = Images.create(new Extent.Long(width, height),
                    Images.IMAGE_TYPE_RGBA_PREMULTIPLIED);

            final DataCursor imageData = rgba.getDataBuffer().dataCursor();
            final Vector2i32 coord = new Vector2i32();
            final float[] rgbapre = new float[4];
            for (coord.y = 0; coord.y <height; coord.y++) {
                for (coord.x = 0; coord.x < width; coord.x++) {
                    final Color color = image.getColor(coord);
                    color.toRGBAPreMul(rgbapre, 0);
                    imageData.writeByte((byte) (rgbapre[0] * 255));
                    imageData.writeByte((byte) (rgbapre[1] * 255));
                    imageData.writeByte((byte) (rgbapre[2] * 255));
                    imageData.writeByte((byte) (rgbapre[3] * 255));
                }
            }

            return rgba;
        } else {
            //opengl support only interleaved images
            if (rm instanceof InterleavedModel){
                final int nbSample = rm.getSampleSystem().getNumComponents();
                if (nbSample>4) throw new InvalidArgumentException("Images with more then 4 samples are not supported by opengl");
                final NumberType primitiveType = rm.getNumericType();
                final int glType = GLUtilities.primitiveTypeToGlType(primitiveType);
                if (glType==0){
                    throw new InvalidArgumentException("Unsupported primitive type "+primitiveType);
                }
            } else {
                //TODO comvert image to interleaved
                throw new InvalidArgumentException("Only interleaved image model are supported");
            }

            return image;
        }
    }

    public static TextureModel toTextureInfo(Image image, boolean preserveColors){
        final int imageInternalFormat;
        final int imageFormat;
        final int imageType;

        final ImageModel rm = image.getRawModel();
        final ImageModel cm = image.getColorModel();
        final Chars rmname = rm.getName();

        if (rmname != null) {
            if (rmname.equals(new Chars("DXT1"))) {
                imageType = GLC.Texture.Type.UNSIGNED_BYTE;
                imageFormat = GLC.Texture.CompressedFormat.DXT1;
                return new TextureModel(GL_TEXTURE_2D,imageFormat, imageType);
            } else if (rmname.equals(new Chars("DXT2"))) {
                throw new RuntimeException("DXT2 Not supported yet.");
            } else if (rmname.equals(new Chars("DXT3"))) {
                imageType = GLC.Texture.Type.UNSIGNED_BYTE;
                imageFormat = GLC.Texture.CompressedFormat.DXT3;
                return new TextureModel(GL_TEXTURE_2D,imageFormat, imageType);
            } else if (rmname.equals(new Chars("DXT4"))) {
                throw new RuntimeException("DXT4 Not supported yet.");
            } else if (rmname.equals(new Chars("DXT5"))) {
                imageType = GLC.Texture.Type.UNSIGNED_BYTE;
                imageFormat = GLC.Texture.CompressedFormat.DXT5;
                return new TextureModel(GL_TEXTURE_2D,imageFormat, imageType);
            }
        }

        if (preserveColors && cm!=null){
            imageType = GLC.Texture.Type.UNSIGNED_BYTE;

            if (cm instanceof DerivateModel){
                final DerivateModel dcm = (DerivateModel) cm;
                final ColorSystem cs = (ColorSystem) dcm.getSampleSystem();
                final int nbSample = cs.getNumComponents();
                final NumberType dataType = rm.getNumericType();
                final int[] mapping = dcm.getMapping();

                if (nbSample == 3
                        && dataType == UInt8.TYPE
                        && Arrays.equals(mapping,ORDER_RGB) ){
                    //image is RGBA, we can reuse bit buffer directly
                    imageInternalFormat = GLC.Texture.InternalFormat.RGB;
                    imageFormat = GLC.Texture.Format.RGB;

                } else if (nbSample == 4
                        && dataType == UInt8.TYPE
                        && Arrays.equals(mapping,ORDER_RGBA)
                        && cs.isAlphaPremultiplied()){
                    //image is RGBA, we can reuse bit buffer directly
                    imageInternalFormat = GLC.Texture.InternalFormat.RGBA;
                    imageFormat = GLC.Texture.Format.RGBA;

                } else if (nbSample == 3
                        && dataType == UInt8.TYPE
                        && Arrays.equals(mapping, ORDER_BGR)){
                    //image is BGR, we can reuse bit buffer directly
                    imageInternalFormat = GLC.Texture.InternalFormat.RGB;
                    imageFormat = GLC.Texture.Format.BGR;

                } else if (nbSample == 4
                        && dataType == UInt8.TYPE
                        && Arrays.equals(mapping,ORDER_BGRA)
                        && cs.isAlphaPremultiplied()){
                     //image is BGRA, we can reuse bit buffer directly
                     imageInternalFormat = GLC.Texture.InternalFormat.RGBA;
                     imageFormat = GLC.Texture.Format.BGRA;

                } else {
                    throw new InvalidArgumentException("Unsupported image color and sample models.");
                }
            } else if (cm instanceof InterleavedModel) {
                final InterleavedModel dcm = (InterleavedModel) cm;
                final ColorSystem cs = (ColorSystem) dcm.getSampleSystem();
                final int nbSample = cs.getNumComponents();
                final NumberType dataType = rm.getNumericType();

                if (nbSample == 3
                        && dataType == UInt8.TYPE
                        && cs.equals(ColorSystem.RGB_8BITS) ){
                    //image is RGBA, we can reuse bit buffer directly
                    imageInternalFormat = GLC.Texture.InternalFormat.RGB;
                    imageFormat = GLC.Texture.Format.RGB;

                } else if (nbSample == 4
                        && dataType == UInt8.TYPE
                        && cs.equals(ColorSystem.RGBA_8BITS_PREMUL)){
                    //image is RGBA, we can reuse bit buffer directly
                    imageInternalFormat = GLC.Texture.InternalFormat.RGBA;
                    imageFormat = GLC.Texture.Format.RGBA;

                } else {
                    throw new InvalidArgumentException("Unsupported image color and sample models.");
                }
            } else {
                throw new InvalidArgumentException("Unsupported image color and sample models.");
            }

        } else {
            //opengl support only interleaved images
            if (rm instanceof InterleavedModel){
                final int nbSample = rm.getSampleSystem().getNumComponents();
                if (nbSample==0 || nbSample>4) throw new InvalidArgumentException("Images with more then 4 samples are not supported by opengl");
                final NumberType primitiveType = rm.getNumericType();
                imageType = GLUtilities.primitiveTypeToGlType(primitiveType);
                if (imageType==0){
                    throw new InvalidArgumentException("Unsupported primitive type "+primitiveType);
                }

                if (nbSample==1){
                    imageFormat = GLC.Texture.Format.RED_INTEGER;
                    switch(imageType){
                        case BYTE :           imageInternalFormat = GLC.Texture.InternalFormat.R8I; break;
                        case UNSIGNED_BYTE :  imageInternalFormat = GLC.Texture.InternalFormat.R8UI; break;
                        case SHORT :          imageInternalFormat = GLC.Texture.InternalFormat.R16I; break;
                        case UNSIGNED_SHORT : imageInternalFormat = GLC.Texture.InternalFormat.R16UI; break;
                        case INT :            imageInternalFormat = GLC.Texture.InternalFormat.R32I; break;
                        case UNSIGNED_INT :   imageInternalFormat = GLC.Texture.InternalFormat.R32UI; break;
                        case FLOAT :          imageInternalFormat = GLC.Texture.InternalFormat.R32F; break;
                        default: throw new InvalidArgumentException("Unsupported primitive type "+imageType);
                    }
                } else if (nbSample==2){
                    imageFormat = GLC.Texture.Format.RG_INTEGER;
                    switch(imageType){
                        case BYTE :           imageInternalFormat = GLC.Texture.InternalFormat.RG8I; break;
                        case UNSIGNED_BYTE :  imageInternalFormat = GLC.Texture.InternalFormat.RG8UI; break;
                        case SHORT :          imageInternalFormat = GLC.Texture.InternalFormat.RG16I; break;
                        case UNSIGNED_SHORT : imageInternalFormat = GLC.Texture.InternalFormat.RG16UI; break;
                        case INT :            imageInternalFormat = GLC.Texture.InternalFormat.RG32I; break;
                        case UNSIGNED_INT :   imageInternalFormat = GLC.Texture.InternalFormat.RG32UI; break;
                        case FLOAT :          imageInternalFormat = GLC.Texture.InternalFormat.RG32F; break;
                        default: throw new InvalidArgumentException("Unsupported primitive type "+imageType);
                    }

                } else if (nbSample==3){
                    imageFormat = GLC.Texture.Format.RGB_INTEGER;
                    switch(imageType){
                        case BYTE :           imageInternalFormat = GLC.Texture.InternalFormat.RGB8I; break;
                        case UNSIGNED_BYTE :  imageInternalFormat = GLC.Texture.InternalFormat.RGB8UI; break;
                        case SHORT :          imageInternalFormat = GLC.Texture.InternalFormat.RGB16I; break;
                        case UNSIGNED_SHORT : imageInternalFormat = GLC.Texture.InternalFormat.RGB16UI; break;
                        case INT :            imageInternalFormat = GLC.Texture.InternalFormat.RGB32I; break;
                        case UNSIGNED_INT :   imageInternalFormat = GLC.Texture.InternalFormat.RGB32UI; break;
                        case FLOAT :          imageInternalFormat = GLC.Texture.InternalFormat.RGB32F; break;
                        default: throw new InvalidArgumentException("Unsupported primitive type "+imageType);
                    }

                } else if (nbSample==4){
                    imageFormat = GLC.Texture.Format.RGBA_INTEGER;
                    switch(imageType){
                        case BYTE :           imageInternalFormat = GLC.Texture.InternalFormat.RGBA8I; break;
                        case UNSIGNED_BYTE :  imageInternalFormat = GLC.Texture.InternalFormat.RGBA8UI; break;
                        case SHORT :          imageInternalFormat = GLC.Texture.InternalFormat.RGBA16I; break;
                        case UNSIGNED_SHORT : imageInternalFormat = GLC.Texture.InternalFormat.RGBA16UI; break;
                        case INT :            imageInternalFormat = GLC.Texture.InternalFormat.RGBA32I; break;
                        case UNSIGNED_INT :   imageInternalFormat = GLC.Texture.InternalFormat.RGBA32UI; break;
                        case FLOAT :          imageInternalFormat = GLC.Texture.InternalFormat.RGBA32F; break;
                        default: throw new InvalidArgumentException("Unsupported primitive type "+imageType);
                    }
                } else {
                    throw new InvalidArgumentException("Unexpected number of samples "+nbSample);
                }

            } else {
                //TODO convert image to interleaved
                throw new InvalidArgumentException("Only interleaved image model are supported");
            }
        }

        //TODO disable interpolation for integer type textures
        //model.setParameter(GLC.Texture.Parameters.MAG_FILTER.KEY, GLC.Texture.Parameters.MAG_FILTER.NEAREST);
        //model.setParameter(GLC.Texture.Parameters.MIN_FILTER.KEY, GLC.Texture.Parameters.MIN_FILTER.NEAREST);

        return TextureModel.create2D(imageInternalFormat, imageFormat, imageType);
    }

}
