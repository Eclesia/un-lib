

package science.unlicense.gpu.impl.opengl.resource;

import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.impl.opengl.GLUtilities;

/**
 * Abstract texture.
 *
 * TODO better merge of Image and Texture API.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTexture extends AbstractResource implements Texture {

    protected TextureModel info;
    protected final Extent.Long size;

    protected Image image;
    protected int texId = -1;
    protected boolean dirty = true;

    public AbstractTexture(Image image, TextureModel info) {
        this.image = image;
        this.size = image.getExtent();
        this.info = info;
    }

    public AbstractTexture(Extent.Long size, TextureModel info) {
        this.size = size.copy();
        this.info = info;
    }

    /**
     * {@inheritDoc }
     */
    public void reformat(GL gl, TextureModel info){
        unloadFromSystemMemory(gl);
        unloadFromGpuMemory(gl);
        this.info = info;
    }

    /**
     * {@inheritDoc }
     */
    public Extent.Long getExtent() {
        return size.copy();
    }

    /**
     * {@inheritDoc }
     */
    public void resize(GL gl, Extent size){
        unloadFromGpuMemory(gl);
        unloadFromSystemMemory(gl);
        this.size.set(size);
        this.dirty = true;
    }

    /**
     * {@inheritDoc }
     */
    public int getTextureType() {
        return info.getTarget();
    }

    /**
     * {@inheritDoc }
     */
    public TextureModel getInfo() {
        return info;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     * {@inheritDoc }
     */
    public Image getImage() {
        return image;
    }

    /**
     * {@inheritDoc }
     */
    public void setImage(Image image) {
        this.size.set(0, image.getExtent().get(0));
        this.size.set(1, image.getExtent().get(1));
        this.image = image;
        this.dirty = true;
    }

    /**
     * {@inheritDoc }
     */
    public int getGpuID() {
        return texId;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isOnSystemMemory() {
        return image != null;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isOnGpuMemory() {
        return texId >= 0;
    }

    /**
     * {@inheritDoc }
     */
    public void unloadFromSystemMemory(GL gl) {
        if (image != null){
            if (image.getDataBuffer() != null) {
                image.getDataBuffer().dispose();
            }
        }
        image = null;
    }

    /**
     * {@inheritDoc }
     */
    public void unloadFromGpuMemory(GL gl) {
        if (texId>=0){
            gl.asGL1().glDeleteTextures(new int[]{texId});
            texId = -1;
            GLUtilities.checkGLErrorsFail(gl);
        }
    }

    /**
     * {@inheritDoc }
     */
    public void bind(GL gl){
        gl.asGL1().glBindTexture(info.getTarget(), texId);
    }

    /**
     * {@inheritDoc }
     */
    public void unbind(GL gl){
        gl.asGL1().glBindTexture(info.getTarget(), 0);
    }

}
