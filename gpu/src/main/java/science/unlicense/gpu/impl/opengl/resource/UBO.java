
package science.unlicense.gpu.impl.opengl.resource;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GLC;

/**
 * Uniform buffer Object resource.
 *
 * http://www.opengl.org/wiki/Uniform_Buffer_Object
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class UBO extends AbstractResource {

    //store initialization stack trace, only when assertions are enabled
    private Throwable ex;

    //gpu id
    private final int[] bufferId = new int[]{-1};
    private Buffer buffer;
    protected boolean dirty = true;

    public UBO() {
        this(null);
    }

    public UBO(Buffer buffer) {
        assert((ex=new Throwable())!=null);
        this.buffer = buffer;
    }

    public Buffer getBuffer() {
        return buffer;
    }

    public void setBuffer(Buffer buffer) {
        this.buffer = buffer;
        this.dirty = true;
    }

    @Override
    public int getGpuID() {
        return bufferId[0];
    }

    @Override
    public boolean isOnSystemMemory() {
        return buffer != null;
    }

    @Override
    public boolean isOnGpuMemory() {
        return bufferId[0] != -1;
    }

    public void bind(GL gl, int pointIndex) {
        gl.asGL3().glBindBufferBase(GLC.GL_UNIFORM_BUFFER, pointIndex, getGpuID());
    }

    public void unbind(GL gl) {
        gl.asGL1().glBindBuffer(GLC.GL_UNIFORM_BUFFER, 0);
    }

    @Override
    public void loadOnSystemMemory(GL gl) throws ResourceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if (bufferId[0] != -1 && !dirty) return; //already loaded
        if (buffer == null) throw new ResourceException("No data buffer to load");
        unloadFromGpuMemory(gl);
        gl.asGL1().glGenBuffers(bufferId);
        gl.asGL1().glBindBuffer(GLC.GL_UNIFORM_BUFFER, bufferId[0]);
        gl.asGL1().glBufferData(GLC.GL_UNIFORM_BUFFER, buffer, GLC.GL_DYNAMIC_DRAW);
        gl.asGL1().glBindBuffer(GLC.GL_UNIFORM_BUFFER, 0);
        dirty = false;
        if (isForgetOnLoad()) {
            buffer = null;
        }
    }

    @Override
    public void unloadFromSystemMemory(GL gl) throws ResourceException {
        buffer = null;
    }

    @Override
    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        if (bufferId[0] != -1) {
            gl.asGL1().glDeleteBuffers(bufferId);
            bufferId[0] = -1;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        final int gpuId = getGpuID();
        if (gpuId>=0){
            Loggers.get().info(
                    new Chars("Unreleased GPU resource : "+getClass().getSimpleName()
                    +" with gpu-id="+gpuId));
            if (ex!=null){
                ex.printStackTrace();
            } else {
                Loggers.get().info(new Chars("Enable assertions to view initialization stack trace"));
            }
        }
        super.finalize();
    }
}
