
package science.unlicense.gpu.impl.opengl.shader;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GL2ES3;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLC;

/**
 *
 * @author Johann Sorel
 */
public final class ShaderUtils {

    public static final int TYPE_UNDEFINED      = -1;
    public static final int TYPE_BOOL           = 0;
    public static final int TYPE_INT            = 1;
    public static final int TYPE_FLOAT          = 2;
    public static final int TYPE_DOUBLE         = 3;
    public static final int TYPE_VEC2           = 4;
    public static final int TYPE_VEC3           = 5;
    public static final int TYPE_VEC4           = 6;
    public static final int TYPE_MAT3           = 7;
    public static final int TYPE_MAT4           = 8;
    public static final int TYPE_SAMPLER2D      = 9;
    public static final int TYPE_SAMPLER2DMS    = 10;
    public static final int TYPE_SAMPLERCUBE    = 11;
    public static final int TYPE_ISAMPLERBUFFER = 12;
    public static final int TYPE_SAMPLERBUFFER  = 13;
    public static final int TYPE_ISAMPLER2D     = 14;
    public static final int TYPE_USAMPLER2D     = 15;
    public static final int TYPE_IVEC2          = 16;
    public static final int TYPE_IVEC3          = 17;
    public static final int TYPE_IVEC4          = 18;

    private static final Chars KW_BOOL           = Chars.constant("bool");
    private static final Chars KW_INT            = Chars.constant("int");
    private static final Chars KW_FLOAT          = Chars.constant("float");
    private static final Chars KW_DOUBLE         = Chars.constant("double");
    private static final Chars KW_VEC2           = Chars.constant("vec2");
    private static final Chars KW_VEC3           = Chars.constant("vec3");
    private static final Chars KW_VEC4           = Chars.constant("vec4");
    private static final Chars KW_IVEC2           = Chars.constant("ivec2");
    private static final Chars KW_IVEC3           = Chars.constant("ivec3");
    private static final Chars KW_IVEC4           = Chars.constant("ivec4");
    private static final Chars KW_MAT3           = Chars.constant("mat3");
    private static final Chars KW_MAT4           = Chars.constant("mat4");
    private static final Chars KW_SAMPLER2D      = Chars.constant("sampler2D");
    private static final Chars KW_ISAMPLER2D     = Chars.constant("isampler2D");
    private static final Chars KW_USAMPLER2D     = Chars.constant("usampler2D");
    private static final Chars KW_SAMPLER2DMS    = Chars.constant("sampler2DMS");
    private static final Chars KW_SAMPLERCUBE    = Chars.constant("samplerCube");
    private static final Chars KW_ISAMPLERBUFFER = Chars.constant("isamplerBuffer");
    private static final Chars KW_SAMPLERBUFFER  = Chars.constant("samplerBuffer");

    private static final Dictionary MAPPING_TYPE = new HashDictionary();
    private static final Dictionary MAPPING_SLOTSIZE = new HashDictionary();
    static {
        MAPPING_TYPE.add(KW_BOOL,            TYPE_BOOL);
        MAPPING_TYPE.add(KW_INT,             TYPE_INT);
        MAPPING_TYPE.add(KW_FLOAT,           TYPE_FLOAT);
        MAPPING_TYPE.add(KW_DOUBLE,          TYPE_DOUBLE);
        MAPPING_TYPE.add(KW_VEC2,            TYPE_VEC2);
        MAPPING_TYPE.add(KW_VEC3,            TYPE_VEC3);
        MAPPING_TYPE.add(KW_VEC4,            TYPE_VEC4);
        MAPPING_TYPE.add(KW_IVEC2,            TYPE_IVEC2);
        MAPPING_TYPE.add(KW_IVEC3,            TYPE_IVEC3);
        MAPPING_TYPE.add(KW_IVEC4,            TYPE_IVEC4);
        MAPPING_TYPE.add(KW_MAT3,            TYPE_MAT3);
        MAPPING_TYPE.add(KW_MAT4,            TYPE_MAT4);
        MAPPING_TYPE.add(KW_SAMPLER2D,       TYPE_SAMPLER2D);
        MAPPING_TYPE.add(KW_ISAMPLER2D,      TYPE_ISAMPLER2D);
        MAPPING_TYPE.add(KW_USAMPLER2D,      TYPE_USAMPLER2D);
        MAPPING_TYPE.add(KW_SAMPLER2DMS,     TYPE_SAMPLER2DMS);
        MAPPING_TYPE.add(KW_SAMPLERCUBE,     TYPE_SAMPLERCUBE);
        MAPPING_TYPE.add(KW_ISAMPLERBUFFER,  TYPE_ISAMPLERBUFFER);
        MAPPING_TYPE.add(KW_SAMPLERBUFFER,   TYPE_SAMPLERBUFFER);
        MAPPING_SLOTSIZE.add(TYPE_BOOL,            1);
        MAPPING_SLOTSIZE.add(TYPE_INT,             1);
        MAPPING_SLOTSIZE.add(TYPE_FLOAT,           1);
        MAPPING_SLOTSIZE.add(TYPE_DOUBLE,          1);
        MAPPING_SLOTSIZE.add(TYPE_VEC2,            1);
        MAPPING_SLOTSIZE.add(TYPE_VEC3,            1);
        MAPPING_SLOTSIZE.add(TYPE_VEC4,            1);
        MAPPING_SLOTSIZE.add(TYPE_IVEC2,           1);
        MAPPING_SLOTSIZE.add(TYPE_IVEC3,           1);
        MAPPING_SLOTSIZE.add(TYPE_IVEC4,           1);
        MAPPING_SLOTSIZE.add(TYPE_MAT3,           -1);
        MAPPING_SLOTSIZE.add(TYPE_MAT4,           -1);
        MAPPING_SLOTSIZE.add(TYPE_SAMPLER2D,       1);
        MAPPING_SLOTSIZE.add(TYPE_ISAMPLER2D,      1);
        MAPPING_SLOTSIZE.add(TYPE_USAMPLER2D,      1);
        MAPPING_SLOTSIZE.add(TYPE_SAMPLER2DMS,     1);
        MAPPING_SLOTSIZE.add(TYPE_SAMPLERCUBE,     1);
        MAPPING_SLOTSIZE.add(TYPE_ISAMPLERBUFFER,  1);
        MAPPING_SLOTSIZE.add(TYPE_SAMPLERBUFFER,   1);
    }

    private ShaderUtils(){}

    /**
     * OpenGL constraints : GL2ES2
     *
     * @param gl
     * @param shaderType : GLC.GLSL.SHADER.X
     * @param shaderCode
     * @return
     */
    public static int loadShader(final GL gl, final int shaderType, final Chars shaderCode) throws ShaderException{
        final GL2ES2 gl2 = gl.asGL2ES2();
        final int shaderId = gl2.glCreateShader(shaderType);

//          TODO optimize shader
//        try{
//            final GLSLReader reader = new GLSLReader();
//            reader.setInput(shaderCode.toBytes(CharEncodings.UTF_8));
//            final SyntaxNode node = reader.read();
//        }catch(Exception ex){
//            throw new ShaderException(ex.getMessage(),ex);
//        }

        // compile the shader
        gl2.glShaderSource(shaderId, new CharArray[]{shaderCode});
        gl2.glCompileShader(shaderId);

        // Check for errors
        final int[] Result = new int[1];
        final int[] InfoLogLength = new int[1];
        gl2.glGetShaderiv(shaderId, GL_COMPILE_STATUS, Result);
        gl2.glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, InfoLogLength);
        final Buffer.Int8 shaderErrorMessage = gl.getBufferFactory().createInt8(InfoLogLength[0]);
        gl2.glGetShaderInfoLog(shaderId, null, shaderErrorMessage);
        final String error = new String(shaderErrorMessage.getBuffer().toByteArray());
        if (error.length()> 1){
            throw new ShaderException(error);
        }

        return shaderId;
    }

    public static int loadShaders(
            GL gl, Chars vertexShaderCode, Chars tessControlShaderCode,
            Chars tessEvalShaderCode, Chars geometryShaderCode, Chars fragmentShaderCode) throws ShaderException{
        return loadShaders(gl, vertexShaderCode, tessControlShaderCode,
                tessEvalShaderCode, geometryShaderCode, fragmentShaderCode,null,-1);
    }

    public static int loadShaders(
            GL gl, Chars vertexShaderCode, Chars tessControlShaderCode,
            Chars tessEvalShaderCode, Chars geometryShaderCode, Chars fragmentShaderCode,
            Chars[] trsFeedbackVars, int trsFeedBAckMode) throws ShaderException{
        final GL2ES2 gl2 = gl.asGL2ES2();
        // Create the shaders
        final int VertexShaderID = loadShader(gl2, GLC.GLSL.SHADER.VERTEX, vertexShaderCode);
        //optional tesselation shaders
        final int TessControlShaderID = (tessControlShaderCode==null) ? -1 :
                loadShader(gl2, GLC.GLSL.SHADER.TESS_CONTROL, tessControlShaderCode);
        final int TessEvalShaderID = (tessEvalShaderCode==null) ? -1 :
                loadShader(gl2, GLC.GLSL.SHADER.TESS_EVAL, tessEvalShaderCode);
        final int GeometryShaderID = (geometryShaderCode==null) ? -1 :
                loadShader(gl2, GLC.GLSL.SHADER.GEOMETRY, geometryShaderCode);
        final int FragmentShaderID = (fragmentShaderCode==null) ? -1 :
                loadShader(gl2, GLC.GLSL.SHADER.FRAGMENT, fragmentShaderCode);


        final int progId = loadShaderProgram(gl, VertexShaderID, TessControlShaderID, TessEvalShaderID,
                GeometryShaderID, FragmentShaderID, trsFeedbackVars, trsFeedBAckMode);

        return progId;
    }

    public static int loadShaderProgram(GL gl, int vertexShaderGlid, int tessControlShaderGlid,
            int tessEvalShaderGlid, int geometryShaderGlid, int fragmentShaderGlid,
            Chars[] trsFeedbackVars, int trsFeedBAckMode) throws ShaderException{

        final GL2ES2 gl2 = gl.asGL2ES2();

        // Link the program
        final int programID = gl2.glCreateProgram();
                                        gl2.glAttachShader(programID, vertexShaderGlid);
        if (tessControlShaderGlid != -1) gl2.glAttachShader(programID, tessControlShaderGlid);
        if (tessEvalShaderGlid    != -1) gl2.glAttachShader(programID, tessEvalShaderGlid);
        if (geometryShaderGlid    != -1) gl2.glAttachShader(programID, geometryShaderGlid);
        if (fragmentShaderGlid    != -1) gl2.glAttachShader(programID, fragmentShaderGlid);

        //set transform feedback
        if (trsFeedbackVars!=null && trsFeedbackVars.length>0){
            final GL2ES3 gl2es3 = gl.asGL2ES3();
            gl2es3.glTransformFeedbackVaryings(programID, trsFeedbackVars, trsFeedBAckMode);
        }

        gl2.glLinkProgram(programID);

        // Check the program
        final int[] Result = new int[1];
        final int[] InfoLogLength = new int[1];
        gl2.glGetProgramiv(programID, GL_LINK_STATUS, Result);
        gl2.glGetProgramiv(programID, GL_INFO_LOG_LENGTH, InfoLogLength);
        final Buffer.Int8 ProgramErrorMessage = gl.getBufferFactory().createInt8(InfoLogLength[0]);
        gl2.glGetProgramInfoLog(programID, null, ProgramErrorMessage);
        final String error = new String(ProgramErrorMessage.getBuffer().toByteArray());
        if (error.length()> 1){
            throw new ShaderException(error);
        }

        return programID;
    }

    /**
     * Reads the given stream and return an array of Strings.
     *
     * @param stream input stream
     * @return String array
     * @throws IOException
     */
    public static Chars getCharsFromStream(final ByteInputStream stream) throws IOException {
        final ByteSequence buffer = new ByteSequence();

        try {
            int b = stream.read();
            while (b!=-1){
                buffer.put((byte) b);
                b = stream.read();
            }
        } finally {
            stream.dispose();
        }
        return new Chars(buffer.toArrayByte());
    }

    public static Integer GLSLtoGLType(Chars typeName){
        return (Integer) MAPPING_TYPE.getValue(typeName);
    }

    public static int VertexAttributeSlotSize(int type){
        return (Integer) MAPPING_SLOTSIZE.getValue(type);
    }

}
