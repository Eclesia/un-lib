
package science.unlicense.gpu.impl.opengl.resource;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int8;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.gpu.api.opengl.GL;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 * Index Buffer Object resource.
 *
 * @author Johann Sorel
 */
public class IBO extends AbstractTupleBufferResource implements TupleGrid {

    private int capacity;

    /**
     * New IBO with no data set.
     */
    public IBO() {}

    /**
     * New IBO from given integer buffer.
     *
     * @param buffer data buffer
     */
    public IBO(Buffer buffer) {
        setBuffer(buffer, 1);
    }

    /**
     * New IBO from given int array.
     *
     * @param array data array
     */
    public IBO(int[] array) {
        this(DefaultBufferFactory.wrap(array));
    }

    /**
     * Number of values in the buffer
     * @return number of values in the buffer
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Set IBO datas.
     *
     * @param buffer data buffer
     * @param tupleSize number of value for each tuple
     */
    public void setBuffer(Buffer buffer, int tupleSize) {
        setBuffer(buffer, GLC.TYPE.UINT, tupleSize);
    }

    public void setBuffer(Buffer buffer, int gpuType, int tupleSize) {
        super.setBuffer(buffer, gpuType, tupleSize);
        this.capacity = (int) buffer.getNumbersSize();
    }

    public int getTuple(int index){
        //TODO change red method based on index value type : byte,short,int
        return buffer.readInt32(index*4);
    }

    /**
     * OpenGL constraints : GL2GL3
     *
     * {@inheritDoc }
     */
    public void loadOnSystemMemory(GL gl) throws ResourceException {
        bind(gl);

        //buffer data are in little endian
        buffer = gl.getBufferFactory().create(capacity*4, Int8.TYPE, Endianness.LITTLE_ENDIAN);

        gl.asGL1().glGetBufferSubData(GL_ELEMENT_ARRAY_BUFFER,0, buffer);
        GLUtilities.checkGLErrorsFail(gl);

        buffer = buffer.copy(Int32.TYPE, Endianness.BIG_ENDIAN);

        unbind(gl);
    }

    /**
     * {@inheritDoc }
     */
    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if (bufferId[0]!=-1) return; //already loaded
        if (buffer==null) throw new ResourceException("No data buffer to load");
        gl.asGL1().glGenBuffers(bufferId);
        gl.asGL1().glBindBuffer(GL_ARRAY_BUFFER, bufferId[0]);
        gl.asGL1().glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
        dirty = false;
        if (isForgetOnLoad()){
            buffer = null;
        }
    }

    /**
     * {@inheritDoc }
     */
    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        if (bufferId[0]!=-1){
            gl.asGL1().glDeleteBuffers(bufferId);
            bufferId[0] = -1;
        }
    }

    /**
     * Bind this index buffer.
     *
     * @param gl OpenGL instance
     */
    public void bind(GL gl){
        gl.asGL1().glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId[0]);
    }

    public void unbind(GL gl){
        gl.asGL1().glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    @Override
    public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
        return getBase().getTuple(coordinate, buffer);
    }

}
