
package science.unlicense.gpu.impl.opengl.resource;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.geometry.api.Extent;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GL2ES2;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;

/**
 * Resource to manipulate a frame buffer object.
 *
 * http://www.opengl.org/wiki/Framebuffer_Object
 *
 * @author Johann Sorel
 */
public class FBO extends AbstractResource implements CanvasBuffer {

    private int width;
    private int height;

    //texture resources
    protected final Sequence attachments = new ArraySequence();

    //gpu ids
    private int fboId = -1;
    //color attachment indexes
    private int[] indexes;

    /**
     *
     * @param width fbo width
     * @param height fbo height
     */
    public FBO(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /**
     * Extract multisampling value from current textures.
     *
     * @return
     */
    public int getMultiSamplingValue(){
        int multisample = 0;
        final Iterator ite = attachments.createIterator();
        while (ite.hasNext()){
            final FBOAttachment att = (FBOAttachment) ite.next();
            final Texture texture = att.getTexture();
            if (texture instanceof Texture2DMS) {
                multisample = ((Texture2DMS) texture).getNbSample();
                break;
            }
        }
        return multisample;
    }

    /**
     * Resize the FBO.Warning : all texture datas will be lost.
     *
     * @param gl
     * @param width newwidth
     * @param height newheight
     */
    public void resize(GL gl, int width, int height){
        if (this.width == width && this.height == height) return;

        unloadFromSystemMemory(gl);
        unloadFromGpuMemory(gl);
        this.width = width;
        this.height = height;
        final Iterator ite = attachments.createIterator();
        while (ite.hasNext()){
            final FBOAttachment att = (FBOAttachment) ite.next();
            final Texture texture = att.getTexture();
            texture.resize(gl, new Extent.Double(width, height));
        }
    }

    /**
     * Change the multisampling value.This method can only be used if the FBO is already multisampled.
     * Warning : all texture datas will be lost.
     *
     * @param gl
     * @param nbSample
     */
    public void resample(GL gl, int nbSample){
        if (nbSample<=0) throw new InvalidArgumentException("Sample value must be positive : "+nbSample);
        unloadFromSystemMemory(gl);
        unloadFromGpuMemory(gl);
        final Iterator ite = attachments.createIterator();
        while (ite.hasNext()){
            final FBOAttachment att = (FBOAttachment) ite.next();
            final Texture texture = att.getTexture();
            if (texture instanceof Texture2DMS){
                ((Texture2DMS) texture).setNbSample(gl,nbSample);
            } else {
                throw new InvalidArgumentException("A texture in the FBO is not multisampled.");
            }
        }
    }

    /**
     * Returns all fbo attachements.
     *
     * @return FBOAttachment array, never null, can be empty.
     */
    @Override
    public Sequence getAttachements() {
        return Collections.readOnlySequence(attachments);
    }

    public void addAttachment(Chars name, int type, Texture texture){
        attachments.add(new FBOAttachment(name, type, texture));
    }

    public void addAttachment(FBOAttachment att){
        attachments.add(att);
    }

    public void removeAttachment(int type){
        final Iterator ite = attachments.createIterator();
        while (ite.hasNext()){
            final FBOAttachment att = (FBOAttachment) ite.next();
            if (att.getAttachment()==type){
                attachments.remove(att);
                break;
            }
        }
    }

    /**
     * @return on gpu fbo id.
     */
    @Override
    public int getGpuID() {
        return fboId;
    }

    /**
     *
     * @param type one of GLC.FBO.Attachment
     * @return
     */
    public Texture getTexture(int type){
        final Iterator ite = attachments.createIterator();
        while (ite.hasNext()){
            final FBOAttachment att = (FBOAttachment) ite.next();
            if (att.getAttachment()==type){
                return att.getTexture();
            }
        }
        return null;
    }

    /**
     * In memory color image.
     *
     * @return Image or null if not loaded on system memory
     */
    public Texture getColorTexture() {
        return getTexture(GLC.FBO.Attachment.COLOR_0);
    }

    /**
     * In memory depth image.
     *
     * @return Image or null if not loaded on system memory
     */
    public Texture getDepthTexture() {
        return getTexture(GLC.FBO.Attachment.DEPTH);
    }

    /**
     * In memory stencil image.
     *
     * @return Image or null if not loaded on system memory
     */
    public Texture getStencilTexture() {
        return getTexture(GLC.FBO.Attachment.STENCIL);
    }

    /**
     * In memory depth stencil image.
     *
     * @return Image or null if not loaded on system memory
     */
    public Texture getDepthStencilTexture() {
        return getTexture(GLC.FBO.Attachment.DEPTH_STENCIL);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isOnSystemMemory() {
        final Iterator ite = attachments.createIterator();
        while (ite.hasNext()){
            final FBOAttachment att = (FBOAttachment) ite.next();
            final Texture texture = att.getTexture();
            if (!texture.isOnSystemMemory()) return false;
        }
        return true;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isOnGpuMemory() {
        return fboId != -1;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void loadOnSystemMemory(GL gl) throws ResourceException {
        if (attachments.isEmpty()) return;
//        final Texture tex = ((FBOAttachment) attachments.get(0)).getTexture();
//        if (tex instanceof Texture2DMS){
//            //blit the full fbo
//            final FBO blit = createBlitFBO();
//            blit(gl, blit);
//            final FBOAttachment[] atts = blit.getAttachements();
//            for (int i=0,n=attachments.getSize();i<n;i++){
//                ((FBOAttachment) attachments.get(i)).getTexture().setImage(atts[i].getTexture().getImage());
//            }
//            blit.unloadFromGpuMemory(gl);
//            blit.unloadFromSystemMemory(gl);
//        } else {
            final Iterator ite = attachments.createIterator();
            while (ite.hasNext()){
                final FBOAttachment att = (FBOAttachment) ite.next();
                final Texture texture = att.getTexture();
                texture.loadOnSystemMemory(gl);
            }
//        }
    }

    /**
     * OpenGL constraints : GL2GL3
     *
     * {@inheritDoc }
     */
    @Override
    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if (fboId != -1){
            //already loaded
            return;
        }

        //load textures
        for (int i=0,n=attachments.getSize();i<n;i++){
            final FBOAttachment att = (FBOAttachment) attachments.get(i);
            final Texture texture = att.getTexture();
            texture.loadOnGpuMemory(gl);
        }

        final GL2ES2 gl2 = gl.asGL2ES2();

        // Create the FBO
        final int[] temp = new int[1];
        gl2.glGenFramebuffers(temp);
        fboId = temp[0];
        gl2.glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        GLUtilities.checkGLErrorsFail(gl2);

        //bind textures, build color indexes
        indexes = new int[0];
        final Iterator typeIte = attachments.createIterator();
        while (typeIte.hasNext()){
            final FBOAttachment att = (FBOAttachment) typeIte.next();
            final Texture texture = att.getTexture();
            final Integer type = att.getAttachment();

            if (texture instanceof Texture1D){
                gl2.asGL3().glFramebufferTexture1D(GL_FRAMEBUFFER, type,
                    texture.getTextureType(), texture.getGpuID(), att.getLevel());
            } else if (texture instanceof Texture2D || texture instanceof Texture2DMS){
                gl2.glFramebufferTexture2D(GL_FRAMEBUFFER, type,
                    texture.getTextureType(), texture.getGpuID(), att.getLevel());
            } else if (texture instanceof Texture3D){
                gl2.asGL3().glFramebufferTexture3D(GL_FRAMEBUFFER, type,
                    texture.getTextureType(), texture.getGpuID(), att.getLevel(),att.getLayer());
            } else {
                throw new RuntimeException("Unexpected texture type : "+texture);
            }

            if (type >= GLC.FBO.Attachment.COLOR_0 && type <=GLC.FBO.Attachment.COLOR_15){
                int index = type-GLC.FBO.Attachment.COLOR_0;
                if (indexes.length-1 < index){
                    indexes = Arrays.resize(indexes, index+1);
                }
                indexes[index] = type;
            }
        }

        // set index for each output
        gl2.asGL2ES3().glDrawBuffers(indexes);

        GLUtilities.checkGLErrorsFail(gl2);

        // Check for FBO errors
        if (gl2.glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
            throw new ResourceException("FBO is not correctly loaded "+this);
        }

        //unbind the fbo
        gl2.glBindFramebuffer(GL_FRAMEBUFFER, 0);

    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void unloadFromSystemMemory(GL gl) throws ResourceException {
        final Iterator ite = attachments.createIterator();
        while (ite.hasNext()){
            final FBOAttachment att = (FBOAttachment) ite.next();
            final Texture texture = att.getTexture();
            texture.unloadFromSystemMemory(gl);
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        if (fboId!=-1) gl.asGL2ES2().glDeleteFramebuffers(new int[]{fboId});
        fboId = -1;
        GLUtilities.checkGLErrorsFail(gl);

        final Iterator ite = attachments.createIterator();
        while (ite.hasNext()){
            final FBOAttachment att = (FBOAttachment) ite.next();
            final Texture texture = att.getTexture();
            texture.unloadFromGpuMemory(gl);
        }
    }

    /**
     * Bind frame buffer.
     *
     * The color attachment are bind using there attachment number.
     * Color attachement 0 must be used with GLSL layout 0
     * Color attachement 1 must be used with GLSL layout 1
     * ...etc...
     *
     * @param gl
     */
    public void bind(GL gl){
//        final Iterator ite = buffers.getValues().createIterator();
//        while (ite.hasNext()){
//            Texture texture = (Texture) ite.next();
//            texture.bind((GL4) gl);
//        }
        gl.asGL2ES2().glBindFramebuffer(GL_FRAMEBUFFER, getGpuID());
    }

    /**
     * Unbind frame buffer.
     * @param gl
     */
    public void unbind(GL gl){
        gl.asGL2ES2().glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    /**
     * Blit (copy removing multisampling) each attachment to target fbo.
     *
     * OpenGL constraints : GL2
     *
     * @param gl
     * @param blitFBO
     */
    public void blit(GL gl, FBO blitFBO){
        final GL2ES2 gl2 = gl.asGL2ES2();
        GLUtilities.checkGLErrorsFail(gl);

        //loop on each attachment and blit them
        Iterator typeIte = attachments.createIterator();
        while (typeIte.hasNext()){
            gl2.glBindFramebuffer(GL_READ_FRAMEBUFFER, getGpuID());
            gl2.glBindFramebuffer(GL_DRAW_FRAMEBUFFER, blitFBO.getGpuID());

            final FBOAttachment att = (FBOAttachment) typeIte.next();
            final Integer type = att.getAttachment();

            GLUtilities.checkGLErrorsFail(gl);
            final int blitType;
            if (type == GLC.FBO.Attachment.DEPTH){
                blitType = GLC.FBO.Blit.MASK_DEPTH;
                //TODO
            } else if (type == GLC.FBO.Attachment.DEPTH_STENCIL){
                blitType = GLC.FBO.Blit.MASK_DEPTH | GLC.FBO.Blit.MASK_STENCIL;
                //TODO
            } else if (type == GLC.FBO.Attachment.STENCIL){
                blitType = GLC.FBO.Blit.MASK_STENCIL;
                //TODO
            } else {
                blitType = GLC.FBO.Blit.MASK_COLORS;
                gl.asGL1().glReadBuffer(type);
                gl.asGL1().glDrawBuffer(type);

                //if the texture type is integer, we must use nearest
                final int interpolation;
                if (att.getTexture().getInfo().isIntegerFormat()){
                    interpolation = GL_NEAREST;
                } else {
                    interpolation = GL_LINEAR;
                }
                gl2.asGL3().glBlitFramebuffer(0, 0, width, height, 0, 0, width, height,
                        blitType, interpolation);
                GLUtilities.checkGLErrorsFail(gl);
            }

            gl2.glBindFramebuffer(GL_FRAMEBUFFER, 0);
            gl2.glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
            gl2.glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        }

//        gl.glBindFramebuffer(GL4.GL_DRAW_FRAMEBUFFER, 0);
//        gl.glBindFramebuffer(GL4.GL_READ_FRAMEBUFFER, blitFBO.getGpuID());
//        typeIte = attachments.createIterator();
//        while (typeIte.hasNext()){
//            final FBOAttachment att = (FBOAttachment) typeIte.next();
//            final Integer type = att.getAttachment();
//            gl4.glReadBuffer(type);
//        }
//        GLUtilities.checkGLErrorsFail(gl);
//
//        gl.glBindFramebuffer(GL4.GL_READ_FRAMEBUFFER, 0);
//        gl.glBindFramebuffer(GL4.GL_DRAW_FRAMEBUFFER, blitFBO.getGpuID());
//        typeIte = attachments.createIterator();
//        while (typeIte.hasNext()){
//            final FBOAttachment att = (FBOAttachment) typeIte.next();
//            final Integer type = att.getAttachment();
//            gl4.glDrawBuffer(type);
//        }
//        GLUtilities.checkGLErrorsFail(gl);

        //unbind buffers
        gl2.glBindFramebuffer(GL_FRAMEBUFFER, 0);
        gl2.glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
        gl2.glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    }


    /**
     * Create an FBO which map all textures.
     * Use only if this FBO uses Multisampling.
     *
     * @return FBO
     */
    public FBO createBlitFBO(){
        final FBO blit = new FBO(width, height);
        final Iterator typeIte = attachments.createIterator();
        while (typeIte.hasNext()){
            final FBOAttachment att = (FBOAttachment) typeIte.next();
            blit.addAttachment(att.createBlitAttachment());
        }
        return blit;
    }

    @Override
    public Chars toChars(){
        return Nodes.toChars(new Chars("FBO"), attachments);
    }

}
