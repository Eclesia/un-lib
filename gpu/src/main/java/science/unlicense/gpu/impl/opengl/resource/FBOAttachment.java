

package science.unlicense.gpu.impl.opengl.resource;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.gpu.api.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLC.FBO.Attachment;

/**
 *
 * @author Johann Sorel
 */
public class FBOAttachment extends CObject implements CanvasBuffer.Attachment {

    private static final Chars[] DEFAULT_NAMES = new Chars[]{
        Chars.constant("COLOR"),
        Chars.constant("COLOR1"),
        Chars.constant("COLOR2"),
        Chars.constant("COLOR3"),
        Chars.constant("COLOR4"),
        Chars.constant("COLOR5"),
        Chars.constant("COLOR6"),
        Chars.constant("COLOR7"),
        Chars.constant("COLOR8"),
        Chars.constant("COLOR9"),
        Chars.constant("COLOR10"),
        Chars.constant("COLOR11"),
        Chars.constant("COLOR12"),
        Chars.constant("COLOR13"),
        Chars.constant("COLOR14"),
        Chars.constant("COLOR15"),
        Chars.constant("DEPTH"),
        Chars.constant("DEPTHSTENCIL"),
        Chars.constant("STENCIL")
    };
    private static Chars defaultName(int attachment) {
        switch(attachment){
            case Attachment.COLOR_0 : return DEFAULT_NAMES[0];
            case Attachment.COLOR_1 : return DEFAULT_NAMES[1];
            case Attachment.COLOR_2 : return DEFAULT_NAMES[2];
            case Attachment.COLOR_3 : return DEFAULT_NAMES[3];
            case Attachment.COLOR_4 : return DEFAULT_NAMES[4];
            case Attachment.COLOR_5 : return DEFAULT_NAMES[5];
            case Attachment.COLOR_6 : return DEFAULT_NAMES[6];
            case Attachment.COLOR_7 : return DEFAULT_NAMES[7];
            case Attachment.COLOR_8 : return DEFAULT_NAMES[8];
            case Attachment.COLOR_9 : return DEFAULT_NAMES[9];
            case Attachment.COLOR_10 : return DEFAULT_NAMES[10];
            case Attachment.COLOR_11 : return DEFAULT_NAMES[11];
            case Attachment.COLOR_12 : return DEFAULT_NAMES[12];
            case Attachment.COLOR_13 : return DEFAULT_NAMES[13];
            case Attachment.COLOR_14 : return DEFAULT_NAMES[14];
            case Attachment.COLOR_15 : return DEFAULT_NAMES[15];
            case Attachment.DEPTH : return DEFAULT_NAMES[16];
            case Attachment.DEPTH_STENCIL : return DEFAULT_NAMES[17];
            case Attachment.STENCIL : return DEFAULT_NAMES[18];
        }
        return Chars.EMPTY;
    }

    private final Chars name;
    private final int attachment;
    private final int textarget;
    private final Texture texture;
    private final int level;
    private final int layer;

    public FBOAttachment(Chars name, int attachment, Texture texture) {
        this(name, attachment, texture.getTextureType(), texture, 0, 0);
    }

    public FBOAttachment(Chars name, int attachment, Texture texture, int level) {
        this(name, attachment, texture.getTextureType(), texture, level, 0);
    }

    public FBOAttachment(Chars name, int attachment, Texture texture, int level, int layer) {
        this(name, attachment, texture.getTextureType(), texture, level, layer);
    }

    public FBOAttachment(Chars name, int attachment, int textarget, Texture texture, int level, int layer) {
        this.name = name == null ? defaultName(attachment) : name;
        this.attachment = attachment;
        this.textarget = textarget;
        this.texture = texture;
        this.level = level;
        this.layer = layer;
    }

    public Chars getName() {
        return name;
    }

    public int getAttachment() {
        return attachment;
    }

    public int getTextarget() {
        return textarget;
    }

    public Texture getTexture() {
        return texture;
    }

    public int getLayer() {
        return layer;
    }

    public int getLevel() {
        return level;
    }

    public FBOAttachment createBlitAttachment(){
        final Integer type = getAttachment();
        final Texture tex = getTexture();

        if (tex instanceof Texture2DMS){
            final Texture2DMS tex2dms = (Texture2DMS) tex;
            final int width = tex2dms.getWidth();
            final int height = tex2dms.getHeight();
            final TextureModel exp = tex2dms.info;
            //create equivalent in 2D
            final TextureModel tm = new TextureModel(GLC.GL_TEXTURE_2D, exp.getInternalFormat(), exp.getFormat(), exp.getType());
            for (int i=0,n=exp.getParameterSize();i<n;i++){
                final int[] p = exp.getParameter(i);
                tm.setParameter(p[0], p[1]);
            }
            final Texture2D tex2d = new Texture2D(width, height, tm);
            return new FBOAttachment(getName(), type, tex2d);
        } else {
            //creating a blit attachement from a texture which is not multisampled,
            // strange but could be used for copy operations
            final Texture2D tex2dOrig = (Texture2D) tex;
            final int width = tex2dOrig.getWidth();
            final int height = tex2dOrig.getHeight();
            final TextureModel exp = tex2dOrig.info;
            //create equivalent in 2D
            final TextureModel tm = new TextureModel(GLC.GL_TEXTURE_2D, exp.getInternalFormat(), exp.getFormat(), exp.getType());
            for (int i=0,n=exp.getParameterSize();i<n;i++){
                final int[] p = exp.getParameter(i);
                tm.setParameter(p[0], p[1]);
            }
            final Texture2D tex2d = new Texture2D(width, height, tm);
            return new FBOAttachment(getName(), type, tex2d);
        }
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("Fbo attachement, ");
        cb.append(name);
        cb.append(texture.toString());
        return cb.toChars();
    }

}
