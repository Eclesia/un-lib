

package science.unlicense.gpu.impl.opengl.resource;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.gpu.impl.opengl.GLC;

/**
 * Informations on the texture.
 *
 * @author Johann Sorel
 */
public class TextureModel {

    private int target;
    private int internalFormat;
    private int format;
    private int type;
    private final Sequence parameters = new ArraySequence();

    public TextureModel(int target, int compressedFormat, int type) {
        this(target,compressedFormat,compressedFormat,type);
    }

    public TextureModel(int target, int internalFormat, int format, int type) {
        this.target = target;
        this.internalFormat = internalFormat;
        this.format = format;
        this.type = type;

        if (isIntegerFormat()){
            //force GL_NEAREST interpolation
            parameters.add(new int[]{GLC.Texture.Parameters.MIN_FILTER.KEY,GLC.Texture.Parameters.MIN_FILTER.NEAREST});
            parameters.add(new int[]{GLC.Texture.Parameters.MAG_FILTER.KEY,GLC.Texture.Parameters.MAG_FILTER.NEAREST});
        }

    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public int getInternalFormat() {
        return internalFormat;
    }

    public void setInternalFormat(int internalFormat) {
        this.internalFormat = internalFormat;
    }

    public int getFormat() {
        return format;
    }

    public void setFormat(int format) {
        this.format = format;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getParameterSize() {
        return parameters.getSize();
    }

    public int[] getParameter(int index){
        return (int[]) parameters.get(index);
    }

    /**
     *
     * @param code one of GLC.Texture.Parameters
     * @return parameter value of -1 if not found
     */
    public int getParameterValue(int code){
        for (int i=0;i<parameters.getSize();i++){
            final int[] p = (int[]) parameters.get(i);
            if (p[0]==code){
                return p[1];
            }
        }
        return -1;
    }

    public void setParameter(int code, int value){
        boolean found = false;
        for (int i=0;i<parameters.getSize();i++){
            final int[] p = (int[]) parameters.get(i);
            if (p[0]==code){
                p[1] = value;
                found = true;
                break;
            }
        }
        if (!found) parameters.add(new int[]{code,value});

        if (isIntegerFormat()){
            if (getParameterValue(GLC.Texture.Parameters.MIN_FILTER.KEY) != GLC.Texture.Parameters.MIN_FILTER.NEAREST
             || getParameterValue(GLC.Texture.Parameters.MAG_FILTER.KEY) != GLC.Texture.Parameters.MAG_FILTER.NEAREST){
                throw new InvalidArgumentException("Integer type texture must have a nearest interpolation");
            }
        }
    }

    public Sequence getParameters() {
        return parameters;
    }

    public boolean hasMipMap(){
        final int val = getParameterValue(GLC.Texture.Parameters.MIN_FILTER.KEY);
        return val == GLC.Texture.Parameters.MIN_FILTER.LINEAR_MIPMAP_LINEAR  ||
               val == GLC.Texture.Parameters.MIN_FILTER.LINEAR_MIPMAP_NEAREST ||
               val == GLC.Texture.Parameters.MIN_FILTER.NEAREST_MIPMAP_LINEAR ||
               val == GLC.Texture.Parameters.MIN_FILTER.NEAREST_MIPMAP_NEAREST;
    }

    public boolean isClipped() {
        final int val = getParameterValue(GLC.Texture.Parameters.WRAP_S.KEY);
        return val == GLC.Texture.Parameters.WRAP_S.CLAMP_TO_EDGE ||
               val == GLC.Texture.Parameters.WRAP_S.CLAMP_TO_BORDER ;
    }

    public boolean isCompressed(){
        return  format == GLC.Texture.CompressedFormat.DXT1 ||
                format == GLC.Texture.CompressedFormat.DXT3 ||
                format == GLC.Texture.CompressedFormat.DXT5 ||
                format == GLC.Texture.CompressedFormat.R11_EAC ||
                format == GLC.Texture.CompressedFormat.RG11_EAC ||
                format == GLC.Texture.CompressedFormat.RGB8_ETC2 ||
                format == GLC.Texture.CompressedFormat.RGB8_PUNCHTHROUGH_ALPHA1_ETC2 ||
                format == GLC.Texture.CompressedFormat.RGBA8_ETC2_EAC ||
                format == GLC.Texture.CompressedFormat.SIGNED_R11_EAC ||
                format == GLC.Texture.CompressedFormat.SIGNED_RG11_EAC ||
                format == GLC.Texture.CompressedFormat.SRGB8_ALPHA8_ETC2_EAC ||
                format == GLC.Texture.CompressedFormat.SRGB8_ETC2 ||
                format == GLC.Texture.CompressedFormat.SRGB8_PUNCHTHROUGH_ALPHA1_ETC2;
    }

    /**
     * Note : integer type texture must not have any interpolation parameters.
     *
     * @return true if texture format is of integer type.
     */
    public boolean isIntegerFormat(){
        return format == GLC.Texture.Format.RED_INTEGER
             ||format == GLC.Texture.Format.RG_INTEGER
             ||format == GLC.Texture.Format.RGB_INTEGER
             ||format == GLC.Texture.Format.RGBA_INTEGER
             ||format == GLC.Texture.Format.BGR_INTEGER
             ||format == GLC.Texture.Format.BGRA_INTEGER;
    }

    public static TextureModel create2D(int internalFormat, int format, int type){
        return new TextureModel(GL_TEXTURE_2D, internalFormat, format, type);
    }

    public static TextureModel create2DMS(int internalFormat, int format, int type){
         return new TextureModel(GL_TEXTURE_2D_MULTISAMPLE, internalFormat, format, type);
    }

    public static TextureModel create3D(int internalFormat, int format, int type){
        return new TextureModel(GL_TEXTURE_3D, internalFormat, format, type);
    }

}
