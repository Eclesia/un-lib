
package science.unlicense.gpu.impl.opengl;

import java.lang.reflect.Field;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import static science.unlicense.common.api.number.Primitive.*;
import science.unlicense.gpu.api.opengl.GL;
import static science.unlicense.gpu.api.opengl.GLC.*;
import static science.unlicense.gpu.impl.opengl.GLC.GETSET.*;

/**
 * Store all GL state variables.
 *
 * @author Johann Sorel
 */
public class GLState extends CObject{

    private static class Value{
        private final int code;
        private final int type;
        private final int nbVal;
        private final boolean settable;
        /** default value */
        private final Object def;

        public Value(int code, int type, int nbVal) {
            this.code = code;
            this.type = type;
            this.nbVal = nbVal;
            this.settable = false;
            this.def = null;
        }

        public Value(int code, int type, int nbVal, Object def) {
            this.code = code;
            this.type = type;
            this.nbVal = nbVal;
            this.settable = true;
            this.def = def;
        }

        public int getNbVal(GL gl) {
            return nbVal;
        }

        public void reset(GL gl){
            if (settable) set(gl,def);
        }

        void set(GL gl, Object value){}

        @Override
        public String toString() {
            final Field[] fields = GLC.GETSET.class.getDeclaredFields();
            try{
                for (Field f : fields){
                    if (f.getInt(null) == code){
                        return f.getName();
                    }
                }
            }catch(IllegalAccessException ex){
                ex.printStackTrace();
            }
            return ""+code;
        }
    }
    private static final Sequence VALUES = new ArraySequence();
    static {
        VALUES.add(new Value(ACTIVE_TEXTURE,                     INT32,   1, GL_TEXTURE0) {void set(GL gl,Object val){gl.asGL1().glActiveTexture((Integer) val);}});
        VALUES.add(new Value(ALIASED_LINE_WIDTH_RANGE,           FLOAT32,2));
        VALUES.add(new Value(ARRAY_BUFFER_BINDING,               INT32,  1));
        VALUES.add(new Value(BLEND_COLOR,                        FLOAT32,4));
        VALUES.add(new Value(BLEND_DST_ALPHA,                    INT32,1));
        VALUES.add(new Value(BLEND_DST_RGB,                      INT32,1));
        VALUES.add(new Value(BLEND_EQUATION_RGB,                 INT32,1));
        VALUES.add(new Value(BLEND_EQUATION_ALPHA,               INT32,1));
        VALUES.add(new Value(BLEND_SRC_ALPHA,                    INT32,1));
        VALUES.add(new Value(BLEND_SRC_RGB,                      INT32,1));
        VALUES.add(new Value(COLOR_CLEAR_VALUE,                  FLOAT32,4));
        VALUES.add(new Value(COLOR_WRITEMASK,                    BITS1,4, new int[]{GL_TRUE,GL_TRUE,GL_TRUE,GL_TRUE}));
        VALUES.add(new Value(COMPRESSED_TEXTURE_FORMATS,         INT32,-1){
            public int getNbVal(GL gl) {
                return getInteger(gl, NUM_COMPRESSED_TEXTURE_FORMATS);
            }
        });
        VALUES.add(new Value(MAX_COMPUTE_SHADER_STORAGE_BLOCKS,  INT32,1));
        VALUES.add(new Value(MAX_COMBINED_SHADER_STORAGE_BLOCKS, INT32,1));
        VALUES.add(new Value(MAX_COMPUTE_UNIFORM_BLOCKS,         INT32,1));
        VALUES.add(new Value(MAX_COMPUTE_TEXTURE_IMAGE_UNITS,    INT32,1));
        VALUES.add(new Value(MAX_COMPUTE_UNIFORM_COMPONENTS,     INT32,1));
        VALUES.add(new Value(MAX_COMPUTE_ATOMIC_COUNTERS,        INT32,1));
        VALUES.add(new Value(MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS, INT32,1));
        VALUES.add(new Value(MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS,INT32,1));
        //VALUES.add(new Value(MAX_COMPUTE_WORK_GROUP_INVOCATIONS,TYPE_INT,1));
//        VALUES.add(new Value(MAX_COMPUTE_WORK_GROUP_COUNT,       TYPE_INT,3));
//        VALUES.add(new Value(MAX_COMPUTE_WORK_GROUP_SIZE,        TYPE_INT,3));
        VALUES.add(new Value(DISPATCH_INDIRECT_BUFFER_BINDING,   INT32,1));
        VALUES.add(new Value(MAX_DEBUG_GROUP_STACK_DEPTH,        INT32,1));
        VALUES.add(new Value(DEBUG_GROUP_STACK_DEPTH,            INT32,1));
        VALUES.add(new Value(CONTEXT_FLAGS,                      INT32,1));
        VALUES.add(new Value(CURRENT_PROGRAM,                    INT32,1));
        VALUES.add(new Value(DEPTH_CLEAR_VALUE,                  FLOAT64,1));
        VALUES.add(new Value(DEPTH_FUNC,                         INT32,1));
        VALUES.add(new Value(DEPTH_RANGE,                        FLOAT64,2));
        VALUES.add(new Value(DEPTH_WRITEMASK,                    BITS1,1));
        VALUES.add(new Value(DOUBLEBUFFER,                       BITS1,1));
        VALUES.add(new Value(DRAW_BUFFER,                        INT32,1));
        VALUES.add(new Value(DRAW_BUFFER_0,                      INT32,1));
        VALUES.add(new Value(DRAW_BUFFER_1,                      INT32,1));
        VALUES.add(new Value(DRAW_BUFFER_2,                      INT32,1));
        VALUES.add(new Value(DRAW_BUFFER_3,                      INT32,1));
        VALUES.add(new Value(DRAW_BUFFER_4,                      INT32,1));
        VALUES.add(new Value(DRAW_BUFFER_5,                      INT32,1));
        VALUES.add(new Value(DRAW_BUFFER_6,                      INT32,1));
        VALUES.add(new Value(DRAW_BUFFER_7,                      INT32,1));
//        VALUES.add(new Value(DRAW_BUFFER_8,                      TYPE_INT,1));
//        VALUES.add(new Value(DRAW_BUFFER_9,                      TYPE_INT,1));
//        VALUES.add(new Value(DRAW_BUFFER_10,                     TYPE_INT,1));
//        VALUES.add(new Value(DRAW_BUFFER_11,                     TYPE_INT,1));
//        VALUES.add(new Value(DRAW_BUFFER_12,                     TYPE_INT,1));
//        VALUES.add(new Value(DRAW_BUFFER_13,                     TYPE_INT,1));
//        VALUES.add(new Value(DRAW_BUFFER_14,                     TYPE_INT,1));
//        VALUES.add(new Value(DRAW_BUFFER_15,                     TYPE_INT,1));
        VALUES.add(new Value(DRAW_FRAMEBUFFER_BINDING,           INT32,1));
        VALUES.add(new Value(READ_FRAMEBUFFER_BINDING,           INT32,1));
        VALUES.add(new Value(ELEMENT_ARRAY_BUFFER_BINDING,       INT32,1));
        VALUES.add(new Value(FRAGMENT_SHADER_DERIVATIVE_HINT,    INT32,1));
        VALUES.add(new Value(IMPLEMENTATION_COLOR_READ_FORMAT,   INT32,1));
        VALUES.add(new Value(IMPLEMENTATION_COLOR_READ_TYPE,     INT32,1));
        VALUES.add(new Value(LINE_SMOOTH_HINT,                   INT32,1));
        VALUES.add(new Value(LINE_WIDTH,                         FLOAT32,1));
        VALUES.add(new Value(LAYER_PROVOKING_VERTEX,             INT32,1));
        VALUES.add(new Value(LOGIC_OP_MODE,                      INT32,1));
        VALUES.add(new Value(MAJOR_VERSION,                      INT32,1));
        VALUES.add(new Value(MAX_3D_TEXTURE_SIZE,                INT32,1));
        VALUES.add(new Value(MAX_ARRAY_TEXTURE_LAYERS,           INT32,1));
        VALUES.add(new Value(MAX_CLIP_DISTANCES,                 INT32,1));
        VALUES.add(new Value(MAX_COLOR_TEXTURE_SAMPLES,          INT32,1));
        VALUES.add(new Value(MAX_COMBINED_ATOMIC_COUNTERS,       INT32,1));
        VALUES.add(new Value(MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS,INT32,1));
        VALUES.add(new Value(MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS,INT32,1));
        VALUES.add(new Value(MAX_COMBINED_TEXTURE_IMAGE_UNITS,   INT32,1));
        VALUES.add(new Value(MAX_COMBINED_UNIFORM_BLOCKS,        INT32,1));
        VALUES.add(new Value(MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS,INT32,1));
        VALUES.add(new Value(MAX_CUBE_MAP_TEXTURE_SIZE,          INT32,1));
        VALUES.add(new Value(MAX_DEPTH_TEXTURE_SAMPLES,          INT32,1));
        VALUES.add(new Value(MAX_DRAW_BUFFERS,                   INT32,1));
        VALUES.add(new Value(MAX_DUAL_SOURCE_DRAW_BUFFERS,       INT32,1));
        VALUES.add(new Value(MAX_ELEMENTS_INDICES,               INT32,1));
        VALUES.add(new Value(MAX_ELEMENTS_VERTICES,              INT32,1));
        VALUES.add(new Value(MAX_FRAGMENT_ATOMIC_COUNTERS,       INT32,1));
        VALUES.add(new Value(MAX_FRAGMENT_SHADER_STORAGE_BLOCKS, INT32,1));
        VALUES.add(new Value(MAX_FRAGMENT_INPUT_COMPONENTS,      INT32,1));
        VALUES.add(new Value(MAX_FRAGMENT_UNIFORM_COMPONENTS,    INT32,1));
        VALUES.add(new Value(MAX_FRAGMENT_UNIFORM_VECTORS,       INT32,1));
        VALUES.add(new Value(MAX_FRAGMENT_UNIFORM_BLOCKS,        INT32,1));
        VALUES.add(new Value(MAX_FRAMEBUFFER_WIDTH,              INT32,1));
        VALUES.add(new Value(MAX_FRAMEBUFFER_HEIGHT,             INT32,1));
        VALUES.add(new Value(MAX_FRAMEBUFFER_LAYERS,             INT32,1));
        VALUES.add(new Value(MAX_FRAMEBUFFER_SAMPLES,            INT32,1));
        VALUES.add(new Value(MAX_GEOMETRY_ATOMIC_COUNTERS,       INT32,1));
        VALUES.add(new Value(MAX_GEOMETRY_SHADER_STORAGE_BLOCKS, INT32,1));
        VALUES.add(new Value(MAX_GEOMETRY_INPUT_COMPONENTS,      INT32,1));
        VALUES.add(new Value(MAX_GEOMETRY_OUTPUT_COMPONENTS,     INT32,1));
        VALUES.add(new Value(MAX_GEOMETRY_TEXTURE_IMAGE_UNITS,   INT32,1));
        VALUES.add(new Value(MAX_GEOMETRY_UNIFORM_BLOCKS,        INT32,1));
        VALUES.add(new Value(MAX_GEOMETRY_UNIFORM_COMPONENTS,    INT32,1));
        VALUES.add(new Value(MAX_INTEGER_SAMPLES,                INT32,1));
        VALUES.add(new Value(MIN_MAP_BUFFER_ALIGNMENT,           INT32,1));
        VALUES.add(new Value(MAX_LABEL_LENGTH,                   INT32,1));
        VALUES.add(new Value(MAX_PROGRAM_TEXEL_OFFSET,           INT32,1));
        VALUES.add(new Value(MIN_PROGRAM_TEXEL_OFFSET,           INT32,1));
        VALUES.add(new Value(MAX_RECTANGLE_TEXTURE_SIZE,         INT32,1));
        VALUES.add(new Value(MAX_RENDERBUFFER_SIZE,              INT32,1));
        VALUES.add(new Value(MAX_SAMPLES,                        INT32,1));
        VALUES.add(new Value(MAX_SAMPLE_MASK_WORDS,              INT32,1));
        VALUES.add(new Value(MAX_SERVER_WAIT_TIMEOUT,            INT32,1));
        VALUES.add(new Value(MAX_SHADER_STORAGE_BUFFER_BINDINGS, INT32,1));
        VALUES.add(new Value(MAX_TESS_CONTROL_ATOMIC_COUNTERS,   INT32,1));
        VALUES.add(new Value(MAX_TESS_EVALUATION_ATOMIC_COUNTERS,INT32,1));
        VALUES.add(new Value(MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS,INT32,1));
        VALUES.add(new Value(MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS,INT32,1));
        VALUES.add(new Value(MAX_TEXTURE_BUFFER_SIZE,            INT32,1));
        VALUES.add(new Value(MAX_TEXTURE_IMAGE_UNITS,            INT32,1));
        VALUES.add(new Value(MAX_TEXTURE_LOD_BIAS,               FLOAT64,1));
        VALUES.add(new Value(MAX_TEXTURE_SIZE,                   INT32,1));
        VALUES.add(new Value(MAX_UNIFORM_BUFFER_BINDINGS,        INT32,1));
        VALUES.add(new Value(MAX_UNIFORM_BLOCK_SIZE,             INT32,1));
        VALUES.add(new Value(MAX_UNIFORM_LOCATIONS,              INT32,1));
        VALUES.add(new Value(MAX_VARYING_COMPONENTS,             INT32,1));
        VALUES.add(new Value(MAX_VARYING_VECTORS,                INT32,1));
        VALUES.add(new Value(MAX_VARYING_FLOATS,                 INT32,1));
        VALUES.add(new Value(MAX_VERTEX_ATOMIC_COUNTERS,         INT32,1));
        VALUES.add(new Value(MAX_VERTEX_ATTRIBS,                 INT32,1));
        VALUES.add(new Value(MAX_VERTEX_SHADER_STORAGE_BLOCKS,   INT32,1));
        VALUES.add(new Value(MAX_VERTEX_TEXTURE_IMAGE_UNITS,     INT32,1));
        VALUES.add(new Value(MAX_VERTEX_UNIFORM_COMPONENTS,      INT32,1));
        VALUES.add(new Value(MAX_VERTEX_UNIFORM_VECTORS,         INT32,1));
        VALUES.add(new Value(MAX_VERTEX_OUTPUT_COMPONENTS,       INT32,1));
        VALUES.add(new Value(MAX_VERTEX_UNIFORM_BLOCKS,          INT32,1));
        VALUES.add(new Value(MAX_VIEWPORT_DIMS,                  INT32,2));
        VALUES.add(new Value(MAX_VIEWPORTS,                      INT32,1));
        VALUES.add(new Value(MINOR_VERSION,                      INT32,1));
        VALUES.add(new Value(NUM_COMPRESSED_TEXTURE_FORMATS,     INT32,1));
        VALUES.add(new Value(NUM_EXTENSIONS,                     INT32,1));
        VALUES.add(new Value(NUM_PROGRAM_BINARY_FORMATS,         INT32,1));
        VALUES.add(new Value(NUM_SHADER_BINARY_FORMATS,          INT32,1));
        VALUES.add(new Value(PACK_ALIGNMENT,                     INT32,1));
        VALUES.add(new Value(PACK_IMAGE_HEIGHT,                  INT32,1));
        VALUES.add(new Value(PACK_LSB_FIRST,                     BITS1,1));
        VALUES.add(new Value(PACK_ROW_LENGTH,                    INT32,1));
        VALUES.add(new Value(PACK_SKIP_IMAGES,                   INT32,1));
        VALUES.add(new Value(PACK_SKIP_PIXELS,                   INT32,1));
        VALUES.add(new Value(PACK_SKIP_ROWS,                     INT32,1));
        VALUES.add(new Value(PACK_SWAP_BYTES,                    BITS1,1));
        VALUES.add(new Value(PIXEL_PACK_BUFFER_BINDING,          INT32,1));
        VALUES.add(new Value(PIXEL_UNPACK_BUFFER_BINDING,        INT32,1));
        VALUES.add(new Value(POINT_FADE_THRESHOLD_SIZE,          INT32,1));
        VALUES.add(new Value(PRIMITIVE_RESTART_INDEX,            INT32,1));
        VALUES.add(new Value(PROGRAM_BINARY_FORMATS,             INT32,-1){
            public int getNbVal(GL gl) {
                return getInteger(gl, NUM_PROGRAM_BINARY_FORMATS);
            }
        });
        VALUES.add(new Value(PROGRAM_PIPELINE_BINDING,           INT32,1));
        VALUES.add(new Value(PROVOKING_VERTEX,                   INT32,1));
        VALUES.add(new Value(POINT_SIZE,                         FLOAT32,1));
        VALUES.add(new Value(POINT_SIZE_GRANULARITY,             INT32,1));
        VALUES.add(new Value(POINT_SIZE_RANGE,                   FLOAT32,2));
        VALUES.add(new Value(POLYGON_OFFSET_FACTOR,              FLOAT32,1));
        VALUES.add(new Value(POLYGON_OFFSET_UNITS,               FLOAT32,1));
        VALUES.add(new Value(POLYGON_SMOOTH_HINT,                INT32,1));
        VALUES.add(new Value(READ_BUFFER,                        INT32,1));
        VALUES.add(new Value(RENDERBUFFER_BINDING,               INT32,1));
        VALUES.add(new Value(SAMPLE_BUFFERS,                     INT32,1));
        VALUES.add(new Value(SAMPLE_COVERAGE_VALUE,              FLOAT32,1));
        VALUES.add(new Value(SAMPLE_COVERAGE_INVERT,             BITS1,1));
        VALUES.add(new Value(SAMPLER_BINDING,                    INT32,1));
        VALUES.add(new Value(SAMPLES,                            INT32,1));
        VALUES.add(new Value(SCISSOR_BOX,                        INT32,4));
        VALUES.add(new Value(SHADER_COMPILER,                    BITS1,1));
        VALUES.add(new Value(SHADER_STORAGE_BUFFER_BINDING,      INT32,1));
        VALUES.add(new Value(SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT,INT32,1));
//        VALUES.add(new Value(SHADER_STORAGE_BUFFER_START,        TYPE_INT,1));
//        VALUES.add(new Value(SHADER_STORAGE_BUFFER_SIZE,         TYPE_INT,1));
        VALUES.add(new Value(SMOOTH_LINE_WIDTH_RANGE,            FLOAT32,2));
        VALUES.add(new Value(SMOOTH_LINE_WIDTH_GRANULARITY,      INT32,1));
        VALUES.add(new Value(STENCIL_BACK_FAIL,                  INT32,1));
        VALUES.add(new Value(STENCIL_BACK_FUNC,                  INT32,1));
        VALUES.add(new Value(STENCIL_BACK_PASS_DEPTH_FAIL,       INT32,1));
        VALUES.add(new Value(STENCIL_BACK_PASS_DEPTH_PASS,       INT32,1));
        VALUES.add(new Value(STENCIL_BACK_REF,                   INT32,1));
        VALUES.add(new Value(STENCIL_BACK_VALUE_MASK,            INT32,1));
        VALUES.add(new Value(STENCIL_BACK_WRITEMASK,             INT32,1));
        VALUES.add(new Value(STENCIL_CLEAR_VALUE,                INT32,1));
        VALUES.add(new Value(STENCIL_FAIL,                       INT32,1));
        VALUES.add(new Value(STENCIL_FUNC,                       INT32,1));
        VALUES.add(new Value(STENCIL_PASS_DEPTH_FAIL,            INT32,1));
        VALUES.add(new Value(STENCIL_PASS_DEPTH_PASS,            INT32,1));
        VALUES.add(new Value(STENCIL_REF,                        INT32,1));
        VALUES.add(new Value(STENCIL_VALUE_MASK,                 INT32,1));
        VALUES.add(new Value(STENCIL_WRITEMASK,                  INT32,1));
        VALUES.add(new Value(STEREO,                             BITS1,1));
        VALUES.add(new Value(SUBPIXEL_BITS,                      INT32,1));
        VALUES.add(new Value(TEXTURE_BINDING_1D,                 INT32,1));
        VALUES.add(new Value(TEXTURE_BINDING_1D_ARRAY,           INT32,1));
        VALUES.add(new Value(TEXTURE_BINDING_2D,                 INT32,1));
        VALUES.add(new Value(TEXTURE_BINDING_2D_ARRAY,           INT32,1));
        VALUES.add(new Value(TEXTURE_BINDING_2D_MULTISAMPLE,     INT32,1));
        VALUES.add(new Value(TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY,INT32,1));
        VALUES.add(new Value(TEXTURE_BINDING_3D,                 INT32,1));
        VALUES.add(new Value(TEXTURE_BINDING_BUFFER,             INT32,1));
        VALUES.add(new Value(TEXTURE_BINDING_CUBE_MAP,           INT32,1));
        VALUES.add(new Value(TEXTURE_BINDING_RECTANGLE,          INT32,1));
        VALUES.add(new Value(TEXTURE_COMPRESSION_HINT,           INT32,1));
        VALUES.add(new Value(TEXTURE_BUFFER_OFFSET_ALIGNMENT,    INT32,1));
        VALUES.add(new Value(TIMESTAMP,                          INT64,1));
        VALUES.add(new Value(TRANSFORM_FEEDBACK_BUFFER_BINDING,  INT32,1));
//        VALUES.add(new Value(TRANSFORM_FEEDBACK_BUFFER_START,    TYPE_INT,1));
//        VALUES.add(new Value(TRANSFORM_FEEDBACK_BUFFER_SIZE,     TYPE_INT,1));
        VALUES.add(new Value(UNIFORM_BUFFER_BINDING,             INT32,1));
        VALUES.add(new Value(UNIFORM_BUFFER_OFFSET_ALIGNMENT,    INT32,1));
//        VALUES.add(new Value(UNIFORM_BUFFER_SIZE,                TYPE_INT,1));
//        VALUES.add(new Value(UNIFORM_BUFFER_START,               TYPE_INT,1));
        VALUES.add(new Value(UNPACK_ALIGNMENT,                   INT32,1));
        VALUES.add(new Value(UNPACK_IMAGE_HEIGHT,                INT32,1));
        VALUES.add(new Value(UNPACK_LSB_FIRST,                   BITS1,1));
        VALUES.add(new Value(UNPACK_ROW_LENGTH,                  INT32,1));
        VALUES.add(new Value(UNPACK_SKIP_IMAGES,                 INT32,1));
        VALUES.add(new Value(UNPACK_SKIP_PIXELS,                 INT32,1));
        VALUES.add(new Value(UNPACK_SKIP_ROWS,                   INT32,1));
        VALUES.add(new Value(UNPACK_SWAP_BYTES,                  BITS1,1));
        VALUES.add(new Value(VERTEX_ARRAY_BINDING,               INT32,1));
//        VALUES.add(new Value(VERTEX_BINDING_DIVISOR,             TYPE_INT,1));
//        VALUES.add(new Value(VERTEX_BINDING_OFFSET,              TYPE_INT,1));
//        VALUES.add(new Value(VERTEX_BINDING_STRIDE,              TYPE_INT,1));
        VALUES.add(new Value(MAX_VERTEX_ATTRIB_RELATIVE_OFFSET,  INT32,1));
        VALUES.add(new Value(MAX_VERTEX_ATTRIB_BINDINGS,         INT32,1));
        VALUES.add(new Value(VIEWPORT,                           INT32,1));
        VALUES.add(new Value(VIEWPORT_BOUNDS_RANGE,              INT32,2));
        VALUES.add(new Value(VIEWPORT_INDEX_PROVOKING_VERTEX,    INT32,1));
        VALUES.add(new Value(VIEWPORT_SUBPIXEL_BITS,             INT32,1));
        VALUES.add(new Value(MAX_ELEMENT_INDEX,                  INT32,1));

        //enabling states
        VALUES.add(new Value(GLC.GETSET.State.BLEND,                BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.COLOR_LOGIC_OP,       BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.CULL_FACE,            BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.DEPTH_TEST,           BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.DITHER,               BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.LINE_SMOOTH,          BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.PROGRAM_POINT_SIZE,   BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.POLYGON_OFFSET_FILL,  BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.POLYGON_OFFSET_LINE,  BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.POLYGON_OFFSET_POINT, BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.POLYGON_SMOOTH,       BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.SCISSOR_TEST,         BITS1,1));
        VALUES.add(new Value(GLC.GETSET.State.STENCIL_TEST,         BITS1,1));

    }

    private final Buffer.Int8 bb;
    private final Buffer.Int32 ib;
    private final Buffer.Int64 lb;
    private final Buffer.Float32 fb;
    private final Buffer.Float64 db;

    //information values
    private final Dictionary INFO_VALUES = new HashDictionary();
    private final Dictionary SETTED_VALUES = new HashDictionary();

    //configurable values

    public GLState(BufferFactory bf) {
        bb = bf.createInt8(1);
        ib = bf.createInt32(1);
        lb = bf.createInt64(1);
        fb = bf.createFloat32(1);
        db = bf.createFloat64(1);
    }

    public Object get(int code){
        final Iterator ite = INFO_VALUES.getPairs().createIterator();
        while (ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final Value v = (Value) pair.getValue1();
            if (v.code==code){
                return pair.getValue2();
            }
        }
        return null;
    }

    public void save(GL gl){
        //TODO
        INFO_VALUES.removeAll();
        SETTED_VALUES.removeAll();
        for (int i=0,n=VALUES.getSize();i<n;i++){
            final Value v = (Value) VALUES.get(i);
            Object value = null;
            final int nbVal = v.getNbVal(gl);
            if (v.type==BITS1){
                value = (nbVal==1) ? getBoolean(gl, v.code) : getBooleans(gl, v.code, nbVal);
            } else if (v.type==INT32){
                value = (nbVal==1) ? getInteger(gl, v.code) : getIntegers(gl, v.code, nbVal);
            } else if (v.type==INT64){
                value = (nbVal==1) ? getLong(gl, v.code) : getLongs(gl, v.code, nbVal);
            } else if (v.type==FLOAT32){
                value = (nbVal==1) ? getFloat(gl, v.code) : getFloats(gl, v.code, nbVal);
            } else if (v.type==FLOAT64){
                value = (nbVal==1) ? getDouble(gl, v.code) : getDoubles(gl, v.code, nbVal);
            }
            INFO_VALUES.add(v,value);
        }
    }

    public void restore(GL gl){
        //TODO
    }

    public boolean getBoolean(GL gl, int code){
        synchronized(bb){
            gl.asGL1().glGetBooleanv(code, bb);
            GLUtilities.checkGLErrorsFail(gl);
            return bb.read(0)!=0;
        }
    }

    public boolean[] getBooleans(GL gl, int code, int nb){
        final boolean[] vals = new boolean[nb];
        for (int i=0;i<nb;i++) vals[i] = getBooleanI(gl, code, i);
        return vals;
    }

    public boolean getBooleanI(GL gl, int code, int index){
        synchronized(bb){
            gl.asGL2ES3().glGetBooleani_v(code, index, bb);
            GLUtilities.checkGLErrorsFail(gl);
            return bb.read(0)!=0;
        }
    }

    public static int getInteger(GL gl, int code){
        final int[] b = new int[1];
        gl.asGL1().glGetIntegerv(code, b);
        GLUtilities.checkGLErrorsFail(gl);
        return b[0];
    }

    public static int[] getIntegers(GL gl, int code, int nb){
        final int[] array = new int[nb];
        gl.asGL1().glGetIntegerv(code, array);
        GLUtilities.checkGLErrorsFail(gl);
        return array;
    }

    public int getIntegerI(GL gl, int code, int index){
        synchronized(ib){
            gl.asGL2ES3().glGetIntegeri_v(code, index, ib);
            GLUtilities.checkGLErrorsFail(gl);
            return ib.read(0);
        }
    }

    public long getLong(GL gl, int code){
        synchronized(lb){
            gl.asGL3().glGetInteger64v(code, lb);
            GLUtilities.checkGLErrorsFail(gl);
            return lb.read(0);
        }
    }

    public static long[] getLongs(GL gl, int code, int nb){
        final long[] array = new long[nb];
        gl.asGL3().glGetInteger64v(code, array);
        GLUtilities.checkGLErrorsFail(gl);
        return array;
    }

    public long getLongI(GL gl, int code, int index){
        synchronized(lb){
            gl.asGL3().glGetInteger64i_v(code, index, lb);
            GLUtilities.checkGLErrorsFail(gl);
            return lb.read(0);
        }
    }

    public float getFloat(GL gl, int code){
        synchronized(fb){
            gl.asGL1().glGetFloatv(code, fb);
            GLUtilities.checkGLErrorsFail(gl);
            return fb.read(0);
        }
    }

    public static float[] getFloats(GL gl, int code, int nb){
        final float[] array = new float[nb];
        gl.asGL1().glGetFloatv(code, array);
        GLUtilities.checkGLErrorsFail(gl);
        return array;
    }

    public float getFloatI(GL gl, int code, int index){
        synchronized(fb){
            gl.asGL4().glGetFloati_v(code, index, fb);
            GLUtilities.checkGLErrorsFail(gl);
            return fb.read(0);
        }
    }

    public double getDouble(GL gl, int code){
        synchronized(db){
            gl.asGL1().glGetDoublev(code, db);
            GLUtilities.checkGLErrorsFail(gl);
            return db.read(0);
        }
    }

    public static double[] getDoubles(GL gl, int code, int nb){
        final double[] array = new double[nb];
        gl.asGL1().glGetDoublev(code, array);
        GLUtilities.checkGLErrorsFail(gl);
        return array;
    }

    public double getDoubleI(GL gl, int code, int index){
        synchronized(db){
            gl.asGL4().glGetDoublei_v(code, index, db);
            GLUtilities.checkGLErrorsFail(gl);
            return db.read(0);
        }
    }


    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        final Iterator ite = INFO_VALUES.getPairs().createIterator();
        while (ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            cb.append(pair.getValue1().toString());
            cb.append(" = ");
            final Object v = pair.getValue2();
            if (v!=null && v.getClass().isArray()){
                cb.append(Arrays.toChars(v));
            } else {
                cb.append(v.toString());
            }
            cb.append('\n');
        }
        return cb.toChars();
    }

    public static int getCurrentProgramId(GL gl){
        return getInteger(gl,GLC.GETSET.CURRENT_PROGRAM);
    }

    /**
     * Get GL viewport.
     *
     * @param gl
     * @return int[]{x,y,width,height}
     */
    public static int[] getViewPort(GL gl){
        return getIntegers(gl, GLC.GETSET.VIEWPORT, 4);
    }

}
