
package science.unlicense.syntax.api.lexer;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.common.api.regex.Regex;

/**
 * Token type expressed as a regular expression.
 *
 * @author Johann Sorel
 */
public class RegexTokenType extends CObject implements TokenType {

    private final Chars name;
    private final Chars regex;
    private final NFAState nfa;

    public RegexTokenType(Chars name,Chars regex) {
        this.name = name;
        this.regex = regex;
        this.nfa = Regex.toNFA(regex,name);
    }

    public Chars getName() {
        return name;
    }

    public Chars getRegex() {
        return regex;
    }

    public NFAState createNFAState() {
        return nfa;
    }

    public Chars toChars() {
        return new Chars("Token:").concat(name);
    }

    public static RegexTokenType decimal(){
        return new RegexTokenType(new Chars("decimal"), new Chars("(-|\\+)?[0-9]+(\\.[0-9]+)?((e|E)( )*(-|\\+)?[0-9]+)?"));
    }

    public static RegexTokenType keyword(Chars tokenTypeName, Chars regex){
        return new RegexTokenType(tokenTypeName, regex);
    }

    public static RegexTokenType space(){
        return new RegexTokenType(new Chars("space"), new Chars("( |\\t|\\r|\\n)+"));
    }

    public static RegexTokenType word(){
        return new RegexTokenType(new Chars("word"), new Chars("([a-z]|[A-Z])+"));
    }

}
