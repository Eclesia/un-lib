
package science.unlicense.syntax.api.parser;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Hasher;
import science.unlicense.common.api.collection.HasherDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.regex.NFAPath;
import science.unlicense.common.api.regex.NFAPathExec;
import science.unlicense.common.api.regex.state.Fork;
import science.unlicense.common.api.regex.state.Match;
import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenGroup;

/**
 *
 * @author Johann Sorel
 */
public class Parser extends AbstractReader {

    private final Dictionary tokenGroups;
    private final Rule root;
    private final Predicate skipedTokens;

    //recycle lexer and state
    private Lexer lexer = null;
    private final NFAPathExec exec;

    public Parser(Rule root) {
        this(root,null);
    }

    public Parser(Rule root, Predicate skip) {
        this(null,root,skip);
    }

    public Parser(Dictionary tokenGroups, Rule root, Predicate skip) {
        CObjects.ensureNotNull(root);
        this.root = root;
        this.skipedTokens = skip;
        if (tokenGroups==null){
            this.tokenGroups = new HashDictionary();
            this.tokenGroups.add(root.getTokens().getName(), root.getTokens());
        } else {
            this.tokenGroups = tokenGroups;
        }

        //prepare execution state
        final NFARuleState state = new NFARuleState(root);
        state.next.state = new Match(root.getName());
        exec = new NFAParserPathExec(state,skipedTokens);
        exec.init();
    }

    public SyntaxNode parse() throws IOException{
        if (tokenGroups.getSize()==1){
            return parseSingleLexer();
        } else {
            return parseMultiLexer();
        }
    }

    private SyntaxNode parseSingleLexer() throws IOException{
        Object input = getInput();
        Lexer lexer = null;
        if (input instanceof Lexer){
            lexer = (Lexer) input;
        } else {
            //recycle lexer
            if (this.lexer==null){
                this.lexer = new Lexer();
            }
            lexer = this.lexer;
            lexer.setInput(input);
        }

        exec.reset();

        final TokenGroup group = root.getTokens();
        if (group==null){
            throw new IOException("Rule has no token group defined : "+root.getName());
        }
        lexer.setTokenGroup(group);
        lexer.init();

        //loop on tokens
        Sequence result = null;
        Token token;
        for (token=lexer.next(); token!=null; token=lexer.next()){
            //System.out.println(token);
            result = exec.process(token);
            if (result.isEmpty()) break;
        }

        if (result==null || result.isEmpty()){
            throw new RuntimeException("no match, last token : "+token);
        }

        //search for the end match
        CharBuffer cb = null;
        NFAPath path = null;
        for (int i=0,n=result.getSize();i<n;i++){
            final NFAPath candidate = (NFAPath) result.get(i);
            if (candidate.state instanceof Match){
                if (path==null){
                    path = (NFAPath) candidate;
                } else {
                    if (cb==null){
                        cb = new CharBuffer();
                        cb.append(path.reverse()).append('\n');
                    }
                    cb.append((NFAPath) candidate.reverse()).append('\n');
                }
            }
        }

        if (path == null){
            throw new RuntimeException("no match.");
        } else if (cb!=null){
            throw new RuntimeException("Multiple match found :\n"+cb.toChars());
        }

        return buildRuleNode(new NFAPath[]{reversePath(path)});
    }

    private SyntaxNode parseMultiLexer() throws IOException{

        exec.reset();

        //create base lexer
        final Dictionary lexers = new HasherDictionary(Hasher.IDENTITY);
        final TokenGroup group = root.getTokens();
        if (group==null){
            throw new IOException("Rule has no token group defined : "+root.getName());
        }
        Lexer lexer = new Lexer(group, CharEncodings.UTF_8);
        lexer.setInput(input);
        lexer.init();
        lexers.add(group, lexer);



        //loop on tokens
        Sequence result = null;
        for (Token token=lexer.next(); token!=null; token=lexer.next()){
            result = exec.process(token);
            if (result.isEmpty()) break;

            //check if we need to change the lexer
            final TokenGroup nexttg = ((NFAParserPath) result.get(0)).getTokenGroup();
            for (int i=1,n=result.getSize();i<n;i++){
                final NFAParserPath p = (NFAParserPath) result.get(i);
                final TokenGroup tg = p.getTokenGroup();
                if (tg!=null && tg!=nexttg){
                    throw new IOException("Only one token group can be active at the same time : "+nexttg.getName()+","+p.getTokenGroup().getName());
                }
            }
            if (nexttg!=lexer.getTokenGroup()){
                //change lexer
                Lexer childLexer = (Lexer) lexers.getValue(nexttg);
                if (childLexer==null){
                    childLexer = lexer.createChild(nexttg);
                    lexers.add(nexttg, childLexer);
                }
                lexer = childLexer;
            }
        }

        if (result==null || result.isEmpty()){
            throw new RuntimeException("no match.");
        }

        //search for the end match
        NFAPath path = null;
        for (int i=0,n=result.getSize();i<n;i++){
            final NFAPath candidate = (NFAPath) result.get(i);
            if (candidate.state instanceof Match){
                path = (NFAPath) candidate;
            }
        }

        if (path == null){
            throw new RuntimeException("no match.");
        }

        //reverse path elements
        final NFAPath reversed = reversePath(path);

        return buildRuleNode(new NFAPath[]{reversed});
    }

    private static NFAPath reversePath(NFAPath path){
        NFAPath child = path;
        NFAPath parent = path.link;
        child.link = null;
        for (;;){
            NFAPath greatParent = parent.link;
            parent.link = child;
            child = parent;
            if (greatParent==null) break;
            parent = greatParent;
        }
        return parent;
    }

    /**
     *
     * @param path current path element, value is change while iterating.
     * @return SyntaxNode
     */
    private SyntaxNode buildRuleNode(final NFAPath[] path){
        final NFARuleState state = (NFARuleState) path[0].state;
        final SyntaxNode node = new SyntaxNode(state.getRule(), null);

        //loop until we find the end state
        for (;;){
            path[0] = path[0].link;
            final NFAState subState = path[0].state;

            if (subState instanceof NFATokenState){
                node.getChildren().add(new SyntaxNode(null, (Token) path[0].value));
            } else if (subState instanceof NFARuleState){
                node.getChildren().add(buildRuleNode(path));
            } else if (subState==state.getEndState()){
                //end of this rule
                break;
            } else if (subState instanceof Fork){
                //skit it
            } else {
                throw new RuntimeException("Unexpected state : "+subState);
            }
        }
        return node;
    }

}
