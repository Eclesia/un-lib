
package science.unlicense.syntax.api.parser;

import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenGroup;
import science.unlicense.syntax.api.parser.NFARuleState.RuleEndState;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.regex.NFAPath;
import science.unlicense.common.api.regex.state.NFAState;

/**
 *
 * @author Johann Sorel
 */
public final class NFAParserPath extends NFAPath {

    private RuleChain chain;
    private final Predicate exclude;

    /** rule before the current active rule start */
    private final NFAParserPath beforeRule;
    /** current active token group */
    private final TokenGroup tokenGroup;
    /** current active lexer */
    Lexer lexer;

    public NFAParserPath(NFAState state, Predicate exclude) {
        super(state);
        this.chain = new RuleChain(null, (NFARuleState) state);
        this.exclude = exclude;
        this.beforeRule = null;

        if (state instanceof NFARuleState){
            this.tokenGroup = ((NFARuleState) state).getRule().getTokens();
        } else {
            this.tokenGroup = null;
        }
    }

    private NFAParserPath(NFAParserPath parent, NFAState state) {
        super(parent,state);
        if (state instanceof NFARuleState){
            //add one level
            this.chain = new RuleChain(parent.chain, (NFARuleState) state);
        } else if (state instanceof RuleEndState){
            //unwrap one level
            this.chain = parent.chain.parent;
        } else {
            this.chain = parent.chain;
        }
        this.exclude = parent.exclude;

        if (state instanceof NFARuleState){
            this.tokenGroup = ((NFARuleState) state).getRule().getTokens();
            this.beforeRule = parent;
        } else if (state instanceof RuleEndState){
            if (parent.beforeRule!=null){
                this.tokenGroup = parent.beforeRule.tokenGroup;
                this.beforeRule = parent.beforeRule.beforeRule;
            } else {
                this.tokenGroup = null;
                this.beforeRule = null;
            }
        } else {
            this.tokenGroup = parent.tokenGroup;
            this.beforeRule = parent.beforeRule;
        }
    }

    /**
     * The current token group which is used.
     * This is the token group from current active rule.
     *
     * @return TokenGroup
     */
    public TokenGroup getTokenGroup() {
        return tokenGroup;
    }


    /**
     * Get the last token.
     *
     * @return last token in the chain
     */
    public Token getLastToken(){
        if (value instanceof Token){
            return (Token) value;
        } else if (link instanceof NFAParserPath){
            return ((NFAParserPath) link).getLastToken();
        } else {
            return null;
        }
    }

    public NFAPath child(NFAState state){
        if (exclude!=null && exclude.evaluate(this.state)){
            //recycle this path
            if (state instanceof NFARuleState){
                //add one level
                this.chain = new RuleChain(chain, (NFARuleState) state);
            } else if (state instanceof RuleEndState){
                //unwrap one level
                this.chain = chain.parent;
            }
            this.state = state;
            return this;
        } else {
            return new NFAParserPath(this, state);
        }
    }

    private NFAState next(NFAState state){
        if (state==null){
            return chain.rule.getEndState();
        } else {
            return state;
        }
    }

    public NFAState getNextState() {
        return next(super.getNextState());
    }

    public NFAState getNextForkState1() {
        return next(super.getNextForkState1());
    }

    public NFAState getNextForkState2() {
        return next(super.getNextForkState2());
    }

}
