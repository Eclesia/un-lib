
package science.unlicense.syntax.api.parser;

import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.regex.NFAPathExec;
import science.unlicense.common.api.regex.NFAPath;
import science.unlicense.common.api.regex.state.NFAState;

/**
 *
 * @author Johann Sorel
 */
public class NFAParserPathExec extends NFAPathExec {

    private final Predicate exclude;

    public NFAParserPathExec(NFAState state, Predicate exclude) {
        super(state);
        this.exclude = exclude;
    }

    protected NFAPath createPath(NFAState state) {
        return new NFAParserPath(state,exclude);
    }

}
