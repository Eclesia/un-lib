

package science.unlicense.syntax.api.lexer;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.regex.state.NFAState;

/**
 * Token type with it's defined executor.
 *
 * @author Johann Sorel
 */
public interface TokenType {

    /**
     * Token tyoe which indicate the stream doesn't not match any token.
     */
    public static final TokenType NO_MATCH = new TokenType() {
        public Chars getName() {
            return new Chars("NO MATCH");
        }
        public NFAState createNFAState() {
            throw new UnimplementedException("Not supported.");
        }
    };

    /**
     * Token type name.
     * @return name
     */
    Chars getName();

    /**
     *
     * @return NFAState never null.
     */
    NFAState createNFAState();

}
