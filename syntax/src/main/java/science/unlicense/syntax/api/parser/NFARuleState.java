
package science.unlicense.syntax.api.parser;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.regex.state.FastForward;
import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.common.api.regex.state.NextRef;

/**
 * Temporary object used when rebuilding parser NFA.
 *
 * @author Johann Sorel
 */
public class NFARuleState extends CObject implements FastForward{

    final Rule rule;
    private final RuleEndState endState = new RuleEndState();
    public final NextRef next = new NextRef(null);

    public NFARuleState(Rule rule) {
        this.rule = rule;
    }

    public Rule getRule() {
        return rule;
    }

    public RuleEndState getEndState() {
        return endState;
    }

    public NFAState getNext(){
        return next.state;
    }

    public NFAState getNextState() {
        return rule.getState();
    }

    public Chars toChars() {
        return new Chars("RuleStart("+rule.getName()+")");
    }


    public final class RuleEndState extends CObject implements FastForward{

        public Rule getRule(){
            return rule;
        }

        public NFAState getNextState() {
            return next.state;
        }

        public Chars toChars() {
            return new Chars("RuleEnd("+rule.getName()+")");
        }
    }

    public static boolean isRuleStart(NFAState state, Chars name){
        return state instanceof NFARuleState &&
                name.equals( ((NFARuleState) state).getRule().getName());
    }

    public static boolean isRuleEnd(NFAState state, Chars name){
        return state instanceof RuleEndState &&
                name.equals( ((RuleEndState) state).getRule().getName());
    }

}
