

package science.unlicense.syntax.api.lexer;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.BacktrackCharIterator;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.common.api.regex.RegexExec;
import science.unlicense.common.api.regex.state.DefaultFork;
import science.unlicense.common.api.regex.state.Match;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.EOSException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;

/**
 *
 * @author Johann Sorel
 */
public class Lexer extends AbstractReader {

    private final ByteSequence byteBuffer = new ByteSequence();
    private TokenGroup tokenGroup = new TokenGroup(Chars.EMPTY);
    private Sequence tokenTypes;

    private State state;
    private boolean finished = false;

    //regex executor
    private Chars[] indexed;
    private RegexExec exec;
    private int[] peek;

    public Lexer() {
        this(CharEncodings.UTF_8);
    }

    public Lexer(CharEncoding encoding) {
        this.state = new State();
        state.encoding = encoding;
    }

    public Lexer(State state) {
        this.state = state;
    }

    public Lexer(TokenGroup tokenGroup, CharEncoding encoding) {
        this.state = new State();
        state.encoding = encoding;
        setTokenGroup(tokenGroup);
    }


    public TokenGroup getTokenGroup() {
        return tokenGroup;
    }

    public void setTokenGroup(TokenGroup tokenGroup) {
        this.tokenGroup = tokenGroup;
    }

    /**
     *
     *
     * @return true if the input has been fully consumed.
     */
    public boolean isInputConsumed(){
        return !state.ite.hasNext();
    }

    public Lexer createChild(TokenGroup tokenGroup) throws IOException{
        final Lexer childLexer = new Lexer();
        childLexer.setInput(input);
        childLexer.state = state;
        childLexer.setTokenGroup(tokenGroup);
        childLexer.init(false);
        return childLexer;
    }

    public void init() throws IOException{
        init(true);
    }

    private void init(boolean initState) throws IOException{
        this.finished = false;
        this.tokenTypes = new ArraySequence(tokenGroup.getValues());

        if (initState) state.init(getInputAsByteStream());

        //prepare regex and variable
        final int nbTypes = tokenTypes.getSize();
        indexed = new Chars[nbTypes];

        //build NFAState with all token types
        TokenType tt = (TokenType) tokenTypes.get(0);
        indexed[0] = tt.getName();
        NFAState state = tt.createNFAState();

        for (int i=1;i<nbTypes;i++){
            tt = (TokenType) tokenTypes.get(i);
            NFAState state2 = tt.createNFAState();
            state = new DefaultFork(state, state2);
            indexed[i] = tt.getName();
        }

        exec = new RegexExec(state);
    }


    /**
     *
     * @return next token, null if no more tokens, token with NO_MATCH type if no matching token.
     * @throws IOException
     * @throws EOSException if method is called after the null(end) token has been send.
     */
    public Token next() throws IOException, EOSException {
        if (finished){
            return null;
        }

        final Token token = new Token();
        token.lineStart = state.currentLine;
        token.lineEnd = state.currentLine;
        token.lineCharStart = state.currentLineChar;
        token.lineCharEnd = state.currentLineChar;
        token.charStart = state.currentChar;
        token.charEnd = state.currentChar;

        int matchNbChar = 0;
        int nbCharBytes = 0;
        byteBuffer.moveTo(0);
        exec.reset();
        while (state.ite.hasNext()){
            //we are not sure this char will match a token, just peek it
            peek = state.ite.peekBulk(byteBuffer);
            final int cp = peek[0];

            final Sequence result = exec.process(cp);
            final int resSize = result.getSize();
            if (resSize==0){
                //no more match possible
                break;
            } else {
                //we have just peek the char, now that we know it match we can move forward
                state.ite.skip();
                nbCharBytes += peek[1];
                state.currentChar++;
                if (cp=='\n'){
                    state.currentLine++;
                    state.currentLineChar=0;
                } else {
                    state.currentLineChar++;
                }

                TokenType matchTT = null;
                int matchIndex = Integer.MAX_VALUE;
                for (int i=0;i<resSize;i++){
                    final NFAState resstate = (NFAState) result.get(i);
                    if (resstate instanceof Match){
                        //we must preserve token type declaration order
                        //if 2 token types match, we must take the first one
                        final Chars ttName = ((Match) resstate).name;
                        final int index = Arrays.getFirstOccurenceIdentity(indexed, 0, indexed.length, ttName);
                        if (index<matchIndex){
                            matchTT = (TokenType) tokenTypes.get(index);
                            matchIndex = index;
                        }
                    }
                }

                if (matchTT!=null){
                    //new longest result
                    token.charEnd = state.currentChar;
                    token.lineEnd = state.currentLine;
                    token.lineCharEnd = state.currentLineChar;
                    token.type = matchTT;
                    matchNbChar = nbCharBytes;
                    state.ite.mark();
                }
            }
        }

        //clip method is slow when working on long strings
        //token.value = text.clip(token.charStart, token.charEnd);
        state.ite.rewind();
        token.value = new Chars(Arrays.copy(byteBuffer.getBackArray(),0,matchNbChar), state.encoding);

        //prepare next iteration
        state.currentChar = token.charEnd;
        state.currentLine = token.lineEnd;
        state.currentLineChar = token.lineCharEnd;

        if (token.type==null){
            finished = true;
            if (state.ite.hasNext()){
                //no token type match
                token.type = TokenType.NO_MATCH;
            } else {
                //end of data
                return null;
            }
        }

        return token;
    }

    private static final class State {

        CharEncoding encoding;
        int currentLine = 0;
        int currentLineChar = 0;
        int currentChar = 0;
        Chars text;
        BacktrackCharIterator ite;

        private void init(ByteInputStream stream) throws IOException{

            //read everything
            final byte[] data = IOUtilities.readAll(stream);
            text = new Chars(data,encoding);
            //reset offsets
            currentLine=0;
            currentLineChar=0;
            currentChar=0;
            ite = new BacktrackCharIterator(text.createIterator());
        }
    }

}
