

package science.unlicense.syntax.api.parser;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.DefaultNode;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenType;

/**
 *
 * @author Johann Sorel
 */
public class SyntaxNode extends DefaultNode {

    private Rule rule;
    private Token token;

    public SyntaxNode() {
        super(true);
    }

    public SyntaxNode(Rule rule, Token token) {
        super(token==null);
        this.rule = rule;
        this.token = token;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    /**
     * Get first child syntax node with given rule
     *
     * @param rule
     * @return SyntaxNode or null if none
     */
    public SyntaxNode getChildByRule(Rule rule){
        for (int i=0,n=children.getSize();i<n;i++){
            final SyntaxNode sn = (SyntaxNode) children.get(i);
            if (CObjects.equals(sn.rule,rule)) return sn;
        }
        return null;
    }

    /**
     * Get all children syntax nodes with given rule
     *
     * @param rule
     * @return SyntaxNode sequence, never null
     */
    public Sequence getChildrenByRule(Rule rule){
        final Sequence seq = new ArraySequence();
        for (int i=0,n=children.getSize();i<n;i++){
            final SyntaxNode sn = (SyntaxNode) children.get(i);
            if (CObjects.equals(sn.rule,rule)) seq.add(sn);
        }
        return seq;
    }

    /**
     * Get first child syntax node with given rule name.
     *
     * @param name
     * @return SyntaxNode or null if none
     */
    public SyntaxNode getChildByRule(Chars name){
        for (int i=0,n=children.getSize();i<n;i++){
            final SyntaxNode sn = (SyntaxNode) children.get(i);
            if (sn.rule==null) continue;
            if (name.equals(sn.rule.getName())) return sn;
        }
        return null;
    }

    /**
     * Get all children syntax nodes with given rule name.
     *
     * @param name
     * @return SyntaxNode sequence, never null
     */
    public Sequence getChildrenByRule(Chars name){
        final Sequence seq = new ArraySequence();
        for (int i=0,n=children.getSize();i<n;i++){
            final SyntaxNode sn = (SyntaxNode) children.get(i);
            if (sn.rule==null) continue;
            if (name.equals(sn.rule.getName())) seq.add(sn);
        }
        return seq;
    }

    /**
     * Get first child syntax node with given token type
     *
     * @param tokenType
     * @return SyntaxNode or null if none
     */
    public SyntaxNode getChildByToken(TokenType tokenType){
        for (int i=0,n=children.getSize();i<n;i++){
            final SyntaxNode sn = (SyntaxNode) children.get(i);
            if (sn.token==null) continue;
            if (CObjects.equals(sn.token.type,tokenType)) return sn;
        }
        return null;
    }

    /**
     * Get all children syntax nodes with given token type
     *
     * @param tokenType
     * @return SyntaxNode sequence, never null
     */
    public Sequence getChildrenByToken(TokenType tokenType){
        final Sequence seq = new ArraySequence();
        for (int i=0,n=children.getSize();i<n;i++){
            final SyntaxNode sn = (SyntaxNode) children.get(i);
            if (sn.token==null) continue;
            if (CObjects.equals(sn.token.type,tokenType)) seq.add(sn);
        }
        return seq;
    }

    /**
     * Get first child syntax node with given token type name.
     *
     * @param name
     * @return SyntaxNode or null if none
     */
    public SyntaxNode getChildByToken(Chars name){
        for (int i=0,n=children.getSize();i<n;i++){
            final SyntaxNode sn = (SyntaxNode) children.get(i);
            if (sn.token==null) continue;
            if (name.equals(sn.token.type.getName())) return sn;
        }
        return null;
    }

    /**
     * Get all children syntax nodes with given token type name.
     *
     * @param name
     * @return SyntaxNode sequence, never null
     */
    public Sequence getChildrenByToken(Chars name){
        final Sequence seq = new ArraySequence();
        for (int i=0,n=children.getSize();i<n;i++){
            final SyntaxNode sn = (SyntaxNode) children.get(i);
            if (sn.token==null) continue;
            if (name.equals(sn.token.type.getName())) seq.add(sn);
        }
        return seq;
    }

    /**
     * Remove all nodes from the syntax tree which match given predicate.
     *
     * @param predicate
     */
    public void trim(Predicate predicate){
        for (int i=children.getSize()-1;i>=0;i--){
            final SyntaxNode sn = (SyntaxNode) children.get(i);
            if (predicate.evaluate(sn)){
                children.remove(i);
            } else {
                sn.trim(predicate);
            }
        }
    }

    /**
     * Rebuild a Chars from all tokens in this syntax node.
     *
     * @return Chars
     */
    public Chars getTokensChars() {
        final CharBuffer cb = new CharBuffer();
        getTokensChars(cb);
        return cb.toChars();
    }

    protected void getTokensChars(CharBuffer cb) {
        if (rule!=null){
            for (Iterator ite=getChildren().createIterator();ite.hasNext();) {
                ((SyntaxNode) ite.next()).getTokensChars(cb);
            }
        } else if (token!=null){
            cb.append(token.value);
        }
    }

    @Override
    public Chars toChars() {
        if (rule!=null) return CObjects.toChars(rule.toString());
        if (token!=null) return CObjects.toChars(token.toString());
        return new Chars("SyntaxNode : EMPTY");
    }

}
