
package science.unlicense.syntax.api.parser;

import science.unlicense.syntax.api.lexer.TokenGroup;
import science.unlicense.syntax.api.lexer.TokenType;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.common.api.regex.NFATemp;
import science.unlicense.common.api.regex.state.Fork;

/**
 *
 * @author Johann Sorel
 */
public final class Rule extends CObject{

    private final Chars name;
    private TokenGroup tokens;
    private NFATemp subStates;

    public Rule(Chars name, TokenGroup tokens) {
        CObjects.ensureNotNull(name);
        this.name = name;
        this.tokens = tokens;
    }

    public Rule setSubState(TokenType tt){
        final NFATokenState ts = new NFATokenState(tt);
        return setSubState(new NFATemp(ts, ts.getNextRef()));
    }

    public Rule setSubState(NFATemp temp){
        this.subStates = temp;
        return this;
    }

    public Chars getName() {
        return name;
    }

    public TokenGroup getTokens() {
        return tokens;
    }

    public void setTokens(TokenGroup tokens) {
        this.tokens = tokens;
    }

    public NFAState getState() {
        return subStates.state;
    }

    public Chars toChars() {
        return name;
    }

    /**
     * Unroll this rule.
     * This solves definitions like :
     * B = (B + B) | A ;
     * solving it to :
     * B = A (+ B)?
     *
     * TODO : is this a right solution ?
     * How do we rebuild the AST as expected after ?
     *
     */
    public void unroll(){
        if (subStates.state instanceof Fork){
            final Fork fork = (Fork) subStates.state;
            final NFAState state1 = fork.getNext1State();
            final NFAState state2 = fork.getNext1State();
        }
    }

}
