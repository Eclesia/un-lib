
package science.unlicense.syntax.api.parser;

/**
 *
 * @author Johann Sorel
 */
public class RuleChain {

    final RuleChain parent;
    final NFARuleState rule;

    public RuleChain(RuleChain parent, NFARuleState rule) {
        this.parent = parent;
        this.rule = rule;
    }

}
