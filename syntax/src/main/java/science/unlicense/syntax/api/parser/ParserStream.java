
package science.unlicense.syntax.api.parser;

import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenGroup;
import science.unlicense.common.api.collection.ArrayStack;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.regex.NFAPath;
import science.unlicense.common.api.regex.NFAPathExec;
import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.common.api.regex.NFAStep;
import science.unlicense.common.api.regex.state.Fork;
import science.unlicense.common.api.regex.state.Match;

/**
 *
 * @author Johann Sorel
 */
public class ParserStream {

    private final Lexer lexer;
    private final Rule root;
    private final Predicate skip;
    private final NFAPathExec exec;
    private final ArrayStack stateStack = new ArrayStack();

    public ParserStream(Lexer lexer, Rule root) throws IOException {
        this(lexer,root,null);
    }

    public ParserStream(Lexer lexer, Rule root, Predicate skip) throws IOException {
        this.lexer = lexer;
        this.root = root;
        this.skip = skip;

        final NFARuleState state = new NFARuleState(root);
        state.next.state = new Match(root.getName());
        exec = new NFAParserPathExec(state,null);
        exec.init();
        exec.reset();

        //prepare lexer
        final TokenGroup group = root.getTokens();
        lexer.setTokenGroup(group);
        lexer.init();

    }

    public NFAStep next() throws IOException{
        if (!stateStack.isEmpty()){
            return (NFAPath) stateStack.pickStart();
        }

        while (stateStack.isEmpty()){
            //loop on tokens
            Sequence result = null;
            Token lastToken = null;
            for (Token token=lexer.next(); token!=null; token=lexer.next()){
                result = exec.process(token);
                final int size = result.getSize();
                if (size==0){
                    lastToken = token;
                    break;
                } else if (size==1){
                    break;
                }
            }

            if (result==null || result.isEmpty()){
                if (lastToken!=null){
                    throw new IOException("No rule match at token : "+lastToken);
                }
                return null;
            }

            //extract all steps
            NFAPath last = (NFAPath) result.get(0);
            //do not stack the last element, it has not been verified yet
            NFAPath path = last.link;
            last.link = null;
            while (path!=null){

                if (path.state instanceof Fork){
                    //do not store it
                } else if ( (skip!=null && skip.evaluate(path)) ){
                    //skip it
                } else {
                    stateStack.add(0,path);
                }
                final NFAPath p = path;
                path = path.link;
                 //clear the path read so far to avoid using too much memory
                p.link = null;
            }
        }


        return (NFAPath) stateStack.pickStart();
    }


}
