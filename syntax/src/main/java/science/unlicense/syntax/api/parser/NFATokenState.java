
package science.unlicense.syntax.api.parser;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.regex.state.AbstractForward;
import science.unlicense.common.api.regex.state.Evaluator;
import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenType;

/**
 *
 * @author Johann Sorel
 */
public class NFATokenState extends AbstractForward implements Evaluator{

    final TokenType tokenType;

    public NFATokenState(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public boolean evaluate(Object candidate) {
        return ((Token) candidate).type == tokenType;
    }

    @Override
    public Chars toChars() {
        return CObjects.toChars(tokenType);
    }

    public static boolean isToken(NFAState state, Chars name){
        return state instanceof NFATokenState &&
                name.equals( ((NFATokenState) state).getTokenType().getName());
    }

}
