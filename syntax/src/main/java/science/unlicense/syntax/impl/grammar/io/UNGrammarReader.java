

package science.unlicense.syntax.impl.grammar.io;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.ArrayStack;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Stack;
import science.unlicense.common.api.regex.NFATemp;
import science.unlicense.common.api.regex.state.DefaultFork;
import science.unlicense.common.api.regex.state.Fork;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.RegexTokenType;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenGroup;
import science.unlicense.syntax.api.lexer.TokenType;
import science.unlicense.syntax.api.parser.NFARuleState;
import science.unlicense.syntax.api.parser.NFATokenState;
import science.unlicense.syntax.api.parser.Rule;

/**
 * TODO : find a name for this grammar syntax.
 * This syntax describe both lexer tokens and parser rules.
 * Inspired from : BNF,AntLR,Regex
 *
 * It is not an official grammar syntax, it is only used in the Unlicense project.
 *
 * @author Johann Sorel
 */
public class UNGrammarReader extends AbstractReader {

    private static final Char NAME = new Char(':');
    private static final Char COMMENT = new Char('#');
    private static final Char PART = new Char('$');
    private static final Char END = new Char(';');
    private static final Chars TOKENS_START = Chars.constant("{");
    private static final Chars TOKENS_END = Chars.constant("}");
    private static final Chars TOKENS_DEFAULT = Chars.constant("DEFAULT");

    //to parse parser rules
    private static final TokenType RULE_RULE = new RegexTokenType(new Chars("rule"), new Chars("[a-z]([a-z]|[0-9]|_|\\-)*"));
    private static final TokenType RULE_TOKEN = new RegexTokenType(new Chars("token"), new Chars("[A-Z]([A-Z]|[0-9]|_|\\-)*"));
    private static final TokenType RULE_0_1 = new RegexTokenType(new Chars("0_1"), new Chars("\\?"));
    private static final TokenType RULE_0_N = new RegexTokenType(new Chars("0_n"), new Chars("\\*"));
    private static final TokenType RULE_1_N = new RegexTokenType(new Chars("1_n"), new Chars("\\+"));
    private static final TokenType RULE_CHOICE = new RegexTokenType(new Chars("choice"), new Chars("\\|"));
    private static final TokenType RULE_GROUP_START = new RegexTokenType(new Chars("groupStart"), new Chars("\\("));
    private static final TokenType RULE_GROUP_END = new RegexTokenType(new Chars("groupEnd"), new Chars("\\)"));
    private static final TokenType RULE_WS = new RegexTokenType(new Chars("ws"), new Chars(" |\t|\n|\r"));
    private static final TokenGroup TOKEN_GROUP = new TokenGroup(Chars.EMPTY);
    static {
        TOKEN_GROUP.addTokenType(RULE_RULE);
        TOKEN_GROUP.addTokenType(RULE_TOKEN);
        TOKEN_GROUP.addTokenType(RULE_0_1);
        TOKEN_GROUP.addTokenType(RULE_0_N);
        TOKEN_GROUP.addTokenType(RULE_1_N);
        TOKEN_GROUP.addTokenType(RULE_CHOICE);
        TOKEN_GROUP.addTokenType(RULE_GROUP_START);
        TOKEN_GROUP.addTokenType(RULE_GROUP_END);
        TOKEN_GROUP.addTokenType(RULE_WS);
    }


    private final Dictionary parts = new OrderedHashDictionary();
    private final Dictionary tokenGroups = new OrderedHashDictionary();
    private final Dictionary rules = new OrderedHashDictionary();
    //rules may be cyclic, they may be used before been defined
    private final Dictionary futurrules = new OrderedHashDictionary();

    //rule text lexer
    private final Lexer lexer;

    public UNGrammarReader() {
        lexer = new Lexer(TOKEN_GROUP, CharEncodings.UTF_8);
    }

    public void read(HashDictionary tokenGroupBuffer, OrderedHashDictionary ruleBuffer) throws IOException{
        parts.removeAll();
        tokenGroups.removeAll();
        rules.removeAll();
        futurrules.removeAll();

        final CharInputStream cs = getInputAsCharStream(CharEncodings.UTF_8);

        for (Chars line=cs.readLine();line!=null;line=cs.readLine()){
            line=line.trim();

            //skip empty and comment lines
            if (line.isEmpty() || line.startsWith(COMMENT)) continue;

            final int split = line.getFirstOccurence(NAME);
            if (split<0) {
                throw new IOException("Invalid grammar for line : "+line);
            }

            final Chars name = line.truncate(0, split).trim();
            Chars value = line.truncate(split+1, line.getCharLength()).trim();

            if (!value.equals(TOKENS_START)){
                while (!value.endsWith(END)){
                    line = cs.readLine();
                    if (line==null){
                        throw new IOException("Unfinished line : "+value);
                    }
                    line = line.trim();
                    //skip empty and comment lines
                    if (line.isEmpty() || line.startsWith(COMMENT)) continue;
                    value = value.concat(' ').concat(line);
                }
                //remove the end ;
                value = value.truncate(0, value.getCharLength()-1);
                value = value.trimEnd();
            }


            if (name.isEmpty() || value.isEmpty()){
                throw new IOException("Invalid grammar for line : "+line);
            }

            if (name.isUpperCase()){
                if (name.startsWith(PART)){
                    parts.add(name, value);
                } else {

                    if (tokenGroups.getValue(name)!=null){
                        throw new IOException("Token group "+name+" has already been defined");
                    }

                    //replace parts
                    if (!value.equals(TOKENS_START)){
                        throw new IOException("Missing '{' for token group "+name);
                    }

                    final TokenGroup tokens = new TokenGroup(name);
                    readTokens(cs, tokens);
                    tokenGroups.add(tokens.getName(), tokens);
                }

            } else if (name.isLowerCase() || name.getFirstOccurence('/')>0){
                final Chars[] cfg = name.split('/');

                if (!cfg[0].isLowerCase()){
                    throw new IOException("Invalid grammar for line : "+line);
                }
                if (rules.getValue(cfg[0])!=null){
                    throw new IOException("Rule "+cfg[0]+" has already been defined");
                }

                final Chars groupName = (cfg.length==1) ? TOKENS_DEFAULT : cfg[1];
                final TokenGroup group = (TokenGroup) tokenGroups.getValue(groupName);
                if (group==null){
                    throw new IOException("Could not find token group : "+groupName);
                }

                final NFATemp state = toNFA(value,group);
                //check if the rule has been already referenced
                Rule r = (Rule) futurrules.remove(cfg[0]);
                if (r==null){
                    r = new Rule(cfg[0],group);
                } else {
                    r.setTokens(group);
                }
                rules.add(cfg[0], r.setSubState(state));

            } else {
                throw new IOException("Invalid grammar for line : "+line);
            }
        }

        if (futurrules.getSize()!=0){
            final CharBuffer cb = new CharBuffer();
            cb.append("Some rules are used be not defined :");
            final Iterator ite = futurrules.getKeys().createIterator();
            while (ite.hasNext()){
                cb.append(' ');
                cb.append(CObjects.toChars(ite.next()));
            }
            throw new IOException(cb.toChars().toString());
        }

        tokenGroupBuffer.addAll(tokenGroups);
        ruleBuffer.addAll(rules);
    }

    private void readTokens(final CharInputStream cs, TokenGroup tokens) throws IOException{

        for (Chars line=cs.readLine();line!=null;line=cs.readLine()){
            line=line.trim();

            //skip empty and comment lines
            if (line.isEmpty() || line.startsWith(COMMENT)) continue;

            if (line.startsWith(TOKENS_END)){
                break;
            }

            final int split = line.getFirstOccurence(NAME);
            if (split<0) {
                throw new IOException("Invalid grammar for line : "+line);
            }

            final Chars name = line.truncate(0, split).trim();
            Chars value = line.truncate(split+1, line.getCharLength()).trim();

            while (!value.endsWith(END)){
                line = cs.readLine().trim();
                //skip empty and comment lines
                if (line.isEmpty() || line.startsWith(COMMENT)) continue;
                value = value.concat(line);
            }

            //remove the end ;
            value = value.truncate(0, value.getCharLength()-1);
            value = value.trimEnd();

            if (name.isEmpty() || value.isEmpty()){
                throw new IOException("Invalid grammar for line : "+line);
            }

            if (name.isUpperCase()){
                if (name.startsWith(PART)){
                    parts.add(name, value);
                } else {
                    //replace parts
                    final Collection pairs = parts.getPairs();
                    for (Iterator ite=pairs.createIterator();ite.hasNext();){
                        final Pair pair = (Pair) ite.next();
                        final Chars key = (Chars) pair.getValue1();
                        final Chars rep = (Chars) pair.getValue2();
                        value = value.replaceAll(key, rep);
                    }
                    if (tokens.getValue(name)!=null){
                        throw new IOException("Token "+name+" has already been defined");
                    }
                    tokens.add(name, new RegexTokenType(name, value));
                }

            } else {
                throw new IOException("Invalid grammar for line : "+line);
            }
        }

    }


    private Token ruleToken = null;

    private NFATemp toNFA(final Chars exp,TokenGroup group) throws IOException{
        lexer.setInput(new ArrayInputStream(exp.toBytes()));
        lexer.init();
        return toNFA(group);
    }

    private NFATemp toNFA(TokenGroup group) throws IOException{

        final Stack stack = new ArrayStack();

        for (ruleToken=(ruleToken==null)?lexer.next():ruleToken;
            ruleToken!=null;
            ruleToken=(ruleToken==null)?lexer.next():ruleToken){

            if (ruleToken.type==TokenType.NO_MATCH){
                //no match
                throw new IOException("No token match for : '"+ruleToken.value+"'");
            }

            //special case for groups
            if (ruleToken.type==RULE_GROUP_END){
                //end of group, return
                break;
            }

            if (ruleToken.type==RULE_WS){
                //skip it
                ruleToken=null;
                continue;

            } else if (ruleToken.type==RULE_GROUP_START){
                ruleToken=null;
                final NFATemp state = toNFA(group);
                //we expect to have a rule end token here
                if (ruleToken==null || ruleToken.type!=RULE_GROUP_END){
                    throw new IOException("Expected a rule end token");
                }
                ruleToken = null;
                stack.add(state);
            } else if (ruleToken.type==RULE_CHOICE){
                //parse what is on the right side of the choice
                final NFATemp left = (NFATemp) stack.pickEnd();
                ruleToken=null;
                final NFATemp right = toNFA(group);
                final Fork state = new DefaultFork(left.state, right.state);
                final Sequence seq = new ArraySequence(left.nexts);
                seq.addAll(right.nexts);
                stack.add(new NFATemp(state, seq));
            } else if (ruleToken.type==RULE_RULE){
                //single rule
                Rule r = (Rule) rules.getValue(ruleToken.value);
                if (r==null) r = (Rule) futurrules.getValue(ruleToken.value);
                if (r==null){
                    //create a futur rule
                    r = new Rule(ruleToken.value,null);
                    futurrules.add(ruleToken.value, r);
                }
                final NFARuleState state = new NFARuleState(r);
                stack.add(new NFATemp(state, state.next));
                ruleToken = null;
            } else if (ruleToken.type==RULE_TOKEN){
                //single token
                final TokenType tt = (TokenType) group.getValue(ruleToken.value);
                if (tt==null) throw new IOException("unknowned token "+ruleToken.value);
                final NFATokenState tokenState = new NFATokenState(tt);
                stack.add(new NFATemp(tokenState, tokenState.getNextRef()));
                ruleToken = null;
            }

            if (ruleToken==null) ruleToken = lexer.next();

            //check for cardinality
            if (ruleToken!=null){
                if (ruleToken.type==RULE_0_1){
                    ruleToken=null;
                    final NFATemp temp = (NFATemp) stack.pickEnd();
                    final DefaultFork state = new DefaultFork(temp.state,null);
                    final Sequence seq = new ArraySequence(temp.nexts);
                    seq.add(state.getNext2Ref());
                    stack.add(new NFATemp(state, seq));
                } else if (ruleToken.type==RULE_0_N){
                    ruleToken=null;
                    final NFATemp temp = (NFATemp) stack.pickEnd();
                    final DefaultFork state = new DefaultFork(temp.state,null);
                    temp.setNext(state);
                    stack.add(new NFATemp(state, state.getNext2Ref()));
                } else if (ruleToken.type==RULE_1_N){
                    ruleToken=null;
                    final NFATemp temp = (NFATemp) stack.pickEnd();
                    final DefaultFork state = new DefaultFork(temp.state,null);
                    temp.setNext(state);
                    stack.add(new NFATemp(temp.state, state.getNext2Ref()));
                }
            }

            //concat with previous in the stack
            if (stack.getSize()>1){
                final NFATemp temp2 = (NFATemp) stack.pickEnd();
                final NFATemp temp1 = (NFATemp) stack.pickEnd();
                temp1.setNext(temp2.state);
                stack.add(new NFATemp(temp1.state, temp2.nexts));
            }

        }

        //the last temp in the stack contains the full graph
        return (NFATemp) stack.pickEnd();
    }


}
