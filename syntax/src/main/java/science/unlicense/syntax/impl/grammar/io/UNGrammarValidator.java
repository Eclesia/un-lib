
package science.unlicense.syntax.impl.grammar.io;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Hasher;
import science.unlicense.common.api.collection.HasherDictionary;
import science.unlicense.common.api.regex.state.Fork;
import science.unlicense.common.api.regex.state.Forward;
import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.syntax.api.parser.Rule;

/**
 * Helper class to find and fix possible incoherencies in grammars.
 * TODO
 *
 * @author Johann Sorel
 */
public final class UNGrammarValidator {

    private Rule rule;

    private UNGrammarValidator(Rule rule){
        this.rule = rule;
    }

    private void validate(){

    }

    /**
     * produce a nice graph like char view.
     *
     * @param rule
     * @return
     */
    public static Chars toChars(Rule rule){
        final Dictionary states = new HasherDictionary(Hasher.IDENTITY);

        final NFAState state = rule.getState();

        if (state instanceof Fork){
            final Fork fork = (Fork) state;
            final NFAState next1 = fork.getNext1State();
            final NFAState next2 = fork.getNext2State();

            if (states.getKeys().contains(next1)){

            }


        } else if (state instanceof Forward){

        }


        return null;
    }

    private static final class Row {



    }

}
