

package science.unlicense.syntax.impl.grammar.io;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.regex.state.Fork;
import science.unlicense.common.api.regex.state.NFAState;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.lexer.RegexTokenType;
import science.unlicense.syntax.api.lexer.TokenGroup;
import science.unlicense.syntax.api.parser.NFARuleState;
import science.unlicense.syntax.api.parser.NFATokenState;
import science.unlicense.syntax.api.parser.Rule;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class GrammarReaderTest {

    @Test
    public void testRead() throws IOException{

        final UNGrammarReader reader = new UNGrammarReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/grammar/io/sample.gr")));

        final OrderedHashDictionary tokenGroups = new OrderedHashDictionary();
        final OrderedHashDictionary rules = new OrderedHashDictionary();
        reader.read(tokenGroups, rules);

        //check tokens
        Assert.assertEquals(1, tokenGroups.getSize());
        final TokenGroup tokens = (TokenGroup) tokenGroups.getValues().createIterator().next();
        Assert.assertEquals(5, tokens.getSize());
        final RegexTokenType tokenImport = (RegexTokenType) tokens.getValue(new Chars("IMPORT"));
        final RegexTokenType tokenWs     = (RegexTokenType) tokens.getValue(new Chars("WS"));
        final RegexTokenType tokenWord   = (RegexTokenType) tokens.getValue(new Chars("WORD"));
        final RegexTokenType tokenCom    = (RegexTokenType) tokens.getValue(new Chars("COM"));
        final RegexTokenType tokenNumber = (RegexTokenType) tokens.getValue(new Chars("NUMBER"));

        Assert.assertEquals(new Chars("IMPORT"), tokenImport.getName());
        Assert.assertEquals(new Chars("WS"),     tokenWs.getName());
        Assert.assertEquals(new Chars("WORD"),   tokenWord.getName());
        Assert.assertEquals(new Chars("COM"),    tokenCom.getName());
        Assert.assertEquals(new Chars("NUMBER"), tokenNumber.getName());

        Assert.assertEquals(new Chars("import"), tokenImport.getRegex());
        Assert.assertEquals(new Chars("( |\\n|\\r|\\t)+"),     tokenWs.getRegex());
        Assert.assertEquals(new Chars("([a-z]|[A-Z])(([a-z]|[A-Z])|[0-9])*"),   tokenWord.getRegex());
        Assert.assertEquals(new Chars("\\;"),    tokenCom.getRegex());
        Assert.assertEquals(new Chars("(-|\\+)?[0-9]+(\\.[0-9]+)?((e|E)(-|\\+)?[0-9]+)?"), tokenNumber.getRegex());

        //check rules
        Assert.assertEquals(3, rules.getSize());
        Rule ruleImport    = (Rule) rules.getValue(new Chars("import"));
        Rule ruleStatement = (Rule) rules.getValue(new Chars("statement"));
        Rule ruleProgram   = (Rule) rules.getValue(new Chars("program"));

        Assert.assertEquals(new Chars("import"),    ruleImport.getName());
        Assert.assertEquals(new Chars("statement"), ruleStatement.getName());
        Assert.assertEquals(new Chars("program"),   ruleProgram.getName());


    }

    @Test
    public void testReadTwoGroup() throws IOException{
        //read the grammar
        final UNGrammarReader reader = new UNGrammarReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/grammar/io/twoTokenGroup.gr")));
        final OrderedHashDictionary tokenGroups = new OrderedHashDictionary();
        final OrderedHashDictionary rules = new OrderedHashDictionary();
        reader.read(tokenGroups, rules);
        Assert.assertEquals(5, rules.getSize());

        //group 1 ---------------------------------------------------------------
        final Rule group1 = (Rule) rules.getValue(new Chars("group1"));
        final NFAState group1State = group1.getState();
        Assert.assertTrue(group1State instanceof Fork);
        final Fork gfork1 = (Fork) group1State;
        final NFATokenState gfork1Next1 = (NFATokenState) gfork1.getNext1State();
        final NFATokenState gfork1Next2 = (NFATokenState) gfork1.getNext2State();
        Assert.assertEquals(new Chars("TOKA"), gfork1Next1.getTokenType().getName());
        Assert.assertEquals(new Chars("TOKB"), gfork1Next2.getTokenType().getName());

        //group 2 ---------------------------------------------------------------
        final Rule group2 = (Rule) rules.getValue(new Chars("group2"));
        final NFAState group2State = group2.getState();
        Assert.assertTrue(group2State instanceof Fork);
        final Fork gfork2 = (Fork) group2State;
        final NFATokenState gfork2Next1 = (NFATokenState) gfork2.getNext1State();
        final NFATokenState gfork2Next2 = (NFATokenState) gfork2.getNext2State();
        Assert.assertEquals(new Chars("TOKC"), gfork2Next1.getTokenType().getName());
        Assert.assertEquals(new Chars("TOKD"), gfork2Next2.getTokenType().getName());

        //rule 1 ---------------------------------------------------------------
        final Rule rule1 = (Rule) rules.getValue(new Chars("rule1"));
        final NFAState rule1State = rule1.getState();
        Assert.assertTrue(rule1State instanceof NFATokenState);
        final NFATokenState fork1Next1 = (NFATokenState) rule1State;
        final NFARuleState fork1Next2 = (NFARuleState) fork1Next1.getNextState();
        Assert.assertEquals(new Chars("TYPE1"), fork1Next1.getTokenType().getName());
        Assert.assertEquals(group1, fork1Next2.getRule());

        //rule 2 ---------------------------------------------------------------
        final Rule rule2 = (Rule) rules.getValue(new Chars("rule2"));
        final NFAState rule2State = rule2.getState();
        Assert.assertTrue(rule2State instanceof NFATokenState);
        final NFATokenState fork2Next1 = (NFATokenState) rule2State;
        final NFARuleState fork2Next2 = (NFARuleState) fork2Next1.getNextState();
        Assert.assertEquals(new Chars("TYPE2"), fork2Next1.getTokenType().getName());
        Assert.assertEquals(group2, fork2Next2.getRule());

        //rule 3 ---------------------------------------------------------------
        final Rule rule3 = (Rule) rules.getValue(new Chars("prog"));
        final NFAState rule3State = rule3.getState();
        Assert.assertTrue(rule3State instanceof Fork);
        final Fork fork = (Fork) rule3State;
        Assert.assertTrue(fork.getNext1State() instanceof NFARuleState);
        Assert.assertTrue(fork.getNext2State() instanceof NFARuleState);
        final NFARuleState forkNext1 = (NFARuleState) fork.getNext1State();
        final NFARuleState forkNext2 = (NFARuleState) fork.getNext2State();
        Assert.assertEquals(rule1,forkNext1.getRule());
        Assert.assertEquals(rule2,forkNext2.getRule());

    }

}
