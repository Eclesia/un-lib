
package science.unlicense.format.pcx;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class PCXReaderTest {

    /**
     * RGB image read test.
     */
    @Test
    public void testReadRGB() throws IOException {

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/pcx/sample.pcx")).createInputStream();

        final ImageReader reader = new PCXReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final TupleGridCursor rawCursor = sm.cursor();
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor colorCursor = cm.asTupleBuffer(image).cursor();

        final TupleRW b00 = sm.createTuple(); sm.getTuple(new Vector2i32(0,0),b00);
        final TupleRW b10 = sm.createTuple(); sm.getTuple(new Vector2i32(1,0),b10);
        final TupleRW b01 = sm.createTuple(); sm.getTuple(new Vector2i32(0,1),b01);
        final TupleRW b11 = sm.createTuple(); sm.getTuple(new Vector2i32(1,1),b11);

        final Vector2i32 coord = new Vector2i32();

        coord.x = 0; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{255,0,0}, b00.toInt()); //RED
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 1; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{255,255,0}, b10.toInt()); //YELLOW
        Assert.assertEquals(new ColorRGB(255, 255, 0, 255), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 0; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,255,255}, b01.toInt()); //CYAN
        Assert.assertEquals(new ColorRGB(0, 255, 255, 255), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 1; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,0,255}, b11.toInt()); //BLUE
        Assert.assertEquals(new ColorRGB(0,   0, 255, 255), new ColorRGB(colorCursor.samples(),cs));
    }

}
