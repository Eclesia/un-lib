package science.unlicense.format.pcx;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import static science.unlicense.image.api.ImageSetMetadata.*;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ColorIndex;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.IndexedColorModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 *
 * @author Johann Sorel
 */
public class PCXReader extends AbstractImageReader{

    private PCXHeader header;
    private int width;
    private int height;
    private DefaultTypedNode mdImage;

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws science.unlicense.encoding.api.io.IOException {
        final DataInputStream ds = new DataInputStream(stream, Endianness.LITTLE_ENDIAN);

        final int signature = ds.readByte();
        if (signature != PCXFormat.SIGNATURE[0]){
            throw new IOException(ds, "Source is not a PCX image.");
        }

        //read header
        header = new PCXHeader();
        header.version = ds.readByte();
        header.encoding = ds.readByte();
        header.bitsPerPixel = ds.readByte();
        header.minX = ds.readUShort();
        header.minY = ds.readUShort();
        header.maxX = ds.readUShort();
        header.maxY = ds.readUShort();
        header.resX = ds.readUShort();
        header.resY = ds.readUShort();
        header.palette = ds.readFully(new byte[48]);
        header.reserved = ds.readByte();
        header.nbColorPlanes = ds.readByte();
        header.bitsPerLine = ds.readUShort();
        header.paletteType = ds.readUShort();
        header.fill = ds.readFully(new byte[58]);

        width = header.maxX - header.minX +1;
        height = header.maxY - header.minY +1;

        mdImage =
        new DefaultTypedNode(MD_IMAGE.getType(),new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION.getType(),new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID.getType(),"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND.getType(),width)}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION.getType(),new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID.getType(),"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND.getType(),height)})
        });

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    protected Image read(ImageReadParameters params, BacktrackInputStream stream) throws science.unlicense.encoding.api.io.IOException {
        readMetadatas(stream);

        final DataInputStream ds = new DataInputStream(stream, Endianness.LITTLE_ENDIAN);

        final Buffer bank;
        final ImageModel sm;
        final ImageModel cm;

        if (header.bitsPerPixel == 8){
            if (header.nbColorPlanes == 1){
                final byte[] data = new byte[width*height];
                uncompress(ds, data);
                bank = DefaultBufferFactory.wrap(data);
                sm = new InterleavedModel(new UndefinedSystem(1), UInt8.TYPE);
                //go read the palette at the end of the file
                //we don't know the file size, so just continue reading until
                //we find the palette flag
                while (true){
                    final byte b = ds.readByte();
                    if (b==PCXFormat.PALETTE_FLAG){
                        break;
                    }
                }
                final byte[] paletteData = ds.readFully(new byte[256]);
                cm = readPalette(sm, data, UInt8.TYPE);
            } else if (header.nbColorPlanes == 3){
                final byte[] data = new byte[3*width*height];
                uncompress(ds, data);
                //we must reorder datas since each the pcx order is RRR,VVV,BBB for each line
                final byte[] ordered = new byte[data.length];
                final int lineSize = width*3;
                for (int y=0;y<height;y++){
                    final int lineOffset = y*lineSize;
                    for (int x=0;x<width;x++){
                        final int colOffset = x*3;
                        ordered[lineOffset +colOffset +0] = data[lineOffset + 0*width +x];
                        ordered[lineOffset +colOffset +1] = data[lineOffset + 1*width +x];
                        ordered[lineOffset +colOffset +2] = data[lineOffset + 2*width +x];
                    }
                }

                bank = DefaultBufferFactory.wrap(ordered);
                sm = new InterleavedModel(new UndefinedSystem(3), UInt8.TYPE);
                cm = DerivateModel.create(sm, new int[]{0,1,2}, null, null, ColorSystem.RGB_8BITS);
            } else {
                throw new IOException(ds, "Number of color planes not supported yet "+header.nbColorPlanes);
            }
        } else {
            throw new IOException(ds, "Number of bits per pixel not supported yet "+header.bitsPerPixel);
        }

        return new DefaultImage(bank, new Extent.Long(width,height), sm, cm);
    }

    /**
     * Decode PCX RLE.
     * @param ds
     * @param data exprected result size
     */
    private static void uncompress(DataInputStream ds, byte[] data) throws IOException{
        int offset = 0;
        while (offset<data.length){
            final int b = ds.readUByte();
            if (b>=192){ //0xCO PCX last 2 bits are use to indicate palette or rle
                final int repetition = b & 0x3F; //last bits indicate the repetition of next byte
                final byte val = ds.readByte();
                Arrays.fill(data, offset,repetition, val);
                offset+=repetition;
            } else {
                //use value directly
                data[offset++] = (byte) b;
            }
        }
    }

    /**
     * Convert 768 bytes palette in an indexed color model.
     * @param data palette data
     * @return IndexedImageColorModel
     */
    private static IndexedColorModel readPalette(ImageModel base, byte[] data, NumberType sampleType){
        final Color[] palette = new Color[256];
        int k=0;
        for (int i=0;i<256;i++){
            palette[i] = new ColorRGB(data[k++]&0xFF,data[k++]&0xFF,data[k++]&0xFF);
        }
        return new IndexedColorModel(base, sampleType, new ColorIndex(palette));
    }

}
