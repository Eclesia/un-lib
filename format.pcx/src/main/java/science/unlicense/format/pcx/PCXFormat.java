
package science.unlicense.format.pcx;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * PCX image format.
 *
 * resources :
 * http://en.wikipedia.org/wiki/PCX
 * http://www.commentcamarche.net/contents/1203-le-format-pcx
 *
 * @author Johann Sorel
 */
public class PCXFormat extends AbstractImageFormat {

    /**
     * File signature.
     */
    public static final byte[] SIGNATURE = new byte[]{0x0A};
    /**
     * Flag indication a 256 color palette.
     * Placed at the end of the file at : Size - 769.
     */
    public static final byte PALETTE_FLAG = 0x0C;

    public static final PCXFormat INSTANCE = new PCXFormat();

    private PCXFormat() {
        super(new Chars("pcx"));
        shortName = new Chars("PCX");
        longName = new Chars("Personal Computer Exchange");
        mimeTypes.add(new Chars("image/x-pcx"));
        extensions.add(new Chars("pcx"));
        signatures.add(SIGNATURE);
    }


    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new PCXStore(this, source);
    }

}
