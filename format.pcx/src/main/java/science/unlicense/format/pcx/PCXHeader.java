
package science.unlicense.format.pcx;

/**
 * PCX image header.
 *
 * @author Johann Sorel
 */
public class PCXHeader {

    /**
     * Image version :
     * 0 = Version 2.5
     * 2 = Version 2.8 with palette
     * 3 = Version 2.8 without palette
     * 5 = Version 3.0
     */
    public byte version;
    /**
     * Encoding method :
     * 1 = RLE encoding
     */
    public byte encoding;
    /**
     * Number of bits per pixel.
     */
    public byte bitsPerPixel;
    /**
     * Unsigned short.
     * Image min x coordinate.
     */
    public int minX;
    /**
     * Unsigned short.
     * Image min y coordinate.
     */
    public int minY;
    /**
     * Unsigned short.
     * Image max x coordinate.
     */
    public int maxX;
    /**
     * Unsigned short.
     * Image max y coordinate.
     */
    public int maxY;
    /**
     * Unsigned short.
     * Image resolution on X.
     */
    public int resX;
    /**
     * Unsigned short.
     * Image resolution on Y.
     */
    public int resY;
    /**
     * Palette. 48bytes
     */
    public byte[] palette;
    /**
     * Reserved.
     */
    public byte reserved;
    /**
     * Number of colors.
     */
    public byte nbColorPlanes;
    /**
     * Unsigned short.
     * Number of bits per line.
     */
    public int bitsPerLine;
    /**
     * Unsigned short.
     * Palette type
     */
    public int paletteType;
    /**
     * Filled spaces. 58bytes
     */
    public byte[] fill;

}
