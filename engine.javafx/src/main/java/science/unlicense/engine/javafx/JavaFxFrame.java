
package science.unlicense.engine.javafx;

import java.util.concurrent.atomic.AtomicBoolean;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.exception.UnimplementedException;
import static science.unlicense.display.api.desktop.Frame.PROP_CLOSABLE;
import science.unlicense.display.api.desktop.FrameDecoration;
import science.unlicense.display.api.desktop.FrameManager;
import science.unlicense.display.api.desktop.Screen;
import science.unlicense.display.api.desktop.cursor.Cursor;
import science.unlicense.display.api.desktop.cursor.NamedCursor;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.desktop.UIFrame;
import static science.unlicense.engine.ui.desktop.UIFrame.PROP_FOCUSED_WIDGET;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Tuple;

/**
 *
 * @author Johann Sorel
 */
public class JavaFxFrame extends AbstractEventSource implements UIFrame {

    private final JavaFxFrame parent;
    private Widget focused = null;
    private Cursor defaultCursor = NamedCursor.DEFAULT;
    private Cursor currentCursor = NamedCursor.DEFAULT;


    private final AtomicBoolean disposed = new AtomicBoolean(false);

    public JavaFxFrame(JavaFxFrame parent, boolean modal, boolean translucent) {
        this.parent = parent;
    }

    @Override
    public Painter2D getPainter() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public Screen getScreen() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public FrameManager getManager() {
        return JavaFxFrameManager.INSTANCE;
    }

    @Override
    public WContainer getContainer() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public Widget getFocusedWidget() {
        return focused;
    }

    @Override
    public void setFocusedWidget(Widget focused) {
        if (CObjects.equals(this.focused, focused)) return;
        final Widget old = this.focused;
        if (old!=null) old.setFocused(false);
        this.focused = focused;
        if (this.focused!=null) this.focused.setFocused(true);
        sendPropertyEvent(PROP_FOCUSED_WIDGET, old, this.focused);
    }

    @Override
    public CharArray getTitle() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public void setTitle(CharArray title) {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public Property varTitle() {
        return getProperty(PROP_TITLE);
    }

    @Override
    public Cursor getCursor() {
        return defaultCursor;
    }

    @Override
    public void setCursor(Cursor defaultCursor) {
        final boolean updateCurrent = currentCursor == this.defaultCursor;
        this.defaultCursor = defaultCursor;
        if (updateCurrent) ;//TODO
    }

    @Override
    public void setOnScreenLocation(Tuple location) {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public Tuple getOnScreenLocation() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public void setSize(int width, int height) {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public Extent getSize() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public void setVisible(boolean visible) {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public boolean isVisible() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public Property varVisible() {
        return getProperty(PROP_VISIBLE);
    }

    @Override
    public FrameDecoration getSystemDecoration() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public void setDecoration(FrameDecoration decoration) {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public FrameDecoration getDecoration() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public void setAlwaysonTop(boolean ontop) {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public boolean isAlwaysOnTop() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public boolean isTranslucent() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void dispose() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public int getState() {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public void setState(int state) {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public void setMaximizable(boolean maximizable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isMaximizable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Property varMaximizable() {
        return getProperty(PROP_MAXIMIZABLE);
    }

    @Override
    public void setMinimizable(boolean minimizable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isMinimizable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Property varMinimizable() {
        return getProperty(PROP_MINIMIZABLE);
    }

    @Override
    public void setClosable(boolean closable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isClosable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Property varClosable() {
        return getProperty(PROP_CLOSABLE);
    }

    @Override
    public UIFrame getParentFrame() {
        return parent;
    }

    @Override
    public Sequence getChildrenFrames() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isModale() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isDisposed() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void waitForDisposal() {
        synchronized(disposed) {
            while (!disposed.get()) {
                try {
                    disposed.wait();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

}
