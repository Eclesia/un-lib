
package science.unlicense.format.craw.dxt;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.ColorRW;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.image.api.model.AbstractImageModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class DXTRawModel extends AbstractImageModel {

    public static final int TYPE_DXT1 = 1;
    public static final int TYPE_DXT2 = 2;
    public static final int TYPE_DXT3 = 3;
    public static final int TYPE_DXT4 = 4;
    public static final int TYPE_DXT5 = 5;

    private final Extent.Long size;
    private final int dxtType;

    //DXT 1-5 use a 4 colour palette
    private final int[] palette = new int[4];
    private final Vector2i32 coord = new Vector2i32();
    //used in DXT 4-5
    private final int[] alphapalette = new int[8];

    //decoding information
    private final int nbBlockX;
    private final int nbBlockY;

    public DXTRawModel(Extent.Long size, final int dxtType) {
        super(ColorSystem.RGBA_8BITS);
        this.size = size;
        this.dxtType = dxtType;

        //number of blocks on X and Y axis
        nbBlockX = (int) Math.ceil(size.get(0)/4);
        nbBlockY = (int) Math.ceil(size.get(1)/4);
    }

    public Chars getName() {
        switch (dxtType) {
            case TYPE_DXT1 : return new Chars("DXT1");
            case TYPE_DXT2 : return new Chars("DXT2");
            case TYPE_DXT3 : return new Chars("DXT3");
            case TYPE_DXT4 : return new Chars("DXT4");
            case TYPE_DXT5 : return new Chars("DXT5");
            default : throw new RuntimeException("Unknown DXT type");
        }
    }

    public int getDxtType() {
        return dxtType;
    }

    public int getSampleCount() {
        return 4;
    }

    public NumberType getNumericType() {
        return UInt8.TYPE;
    }

    public Buffer createBuffer(Extent.Long dimensions, BufferFactory factory) {
        return createBuffer(dimensions, factory, dxtType);
    }

    static Buffer createBuffer(Extent.Long dimensions, BufferFactory factory, int dxtType) {
        if (factory==null) factory = DefaultBufferFactory.INSTANCE;
        final int nbx = (int) Math.ceil(dimensions.get(0)/4);
        final int nby = (int) Math.ceil(dimensions.get(1)/4);
        final int nbBlock = nbx*nby;
        return factory.createInt8(nbBlock * ((dxtType==TYPE_DXT1)?8:16) ).getBuffer();
    }

    public TupleGrid asTupleBuffer(Image image) {
        return new DXTTupleGrid(size,dxtType,image.getDataBuffer(), sampleSystem);
    }

    public Image readDXT1(final DataInputStream ds, BufferFactory bufferFactory) throws IOException{
        final Image image = Images.create(size,Images.IMAGE_TYPE_RGBA, bufferFactory);
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor cursor = cm.asTupleBuffer(image).cursor();
        final ColorRW pixel = Colors.castOrWrap(cursor.samples(), cs);

        final int[][] rgbablock = new int[4][4];

        for (int y=0; y<nbBlockY; y++){
            for (int x=0; x<nbBlockX; x++){
                readBlockDXT1(ds,rgbablock);
                //copie block pixels in image
                for (int by=0;by<4;by++){
                    for (int bx=0;bx<4;bx++){
                        coord.x = x*4 + bx;
                        coord.y = y*4 + by;
                        cursor.moveTo(coord);
                        pixel.fromARGB(rgbablock[by][bx]);
                    }
                }
            }
        }

        return image;
    }

    public Image readDXT2or3(final DataInputStream ds, boolean alphaPremultiplied,
            BufferFactory bufferFactory) throws IOException{
        final Image image = Images.create(size,
                alphaPremultiplied ? Images.IMAGE_TYPE_RGBA_PREMULTIPLIED : Images.IMAGE_TYPE_RGBA, bufferFactory);
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor cursor = cm.asTupleBuffer(image).cursor();
        final ColorRW pixel = Colors.castOrWrap(cursor.samples(), cs);

        final int[][] rgbablock = new int[4][4];

        int x,y,bx,by,yoffset,xoffset;
        for (y=0; y<nbBlockY; y++){
            yoffset = y*4;
            for (x=0; x<nbBlockX; x++){
                xoffset = x*4;
                readBlockDXT3(ds,rgbablock);

                //copie block pixels in image
                for (by=0;by<4;by++){
                    for (bx=0;bx<4;bx++){
                        coord.x = xoffset+bx;
                        coord.y = yoffset+by;
                        cursor.moveTo(coord);
                        pixel.fromARGB(rgbablock[by][bx]);
                    }
                }
            }
        }

        return image;
    }

    public Image readDXT4or5(final DataInputStream ds, boolean alphaPremultiplied, BufferFactory bufferFactory) throws IOException{

        final Image image = Images.create(size,
                alphaPremultiplied ? Images.IMAGE_TYPE_RGBA_PREMULTIPLIED : Images.IMAGE_TYPE_RGBA, bufferFactory);
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor cursor = cm.asTupleBuffer(image).cursor();
        final ColorRW pixel = Colors.castOrWrap(cursor.samples(), cs);

        final int[][] rgbablock = new int[4][4];

        int x,y,yoffset,xoffset;
        for (y=0; y<nbBlockY; y++){
            yoffset = y*4;
            for (x=0; x<nbBlockX; x++){
                xoffset = x*4;
                readBlockDXT5(ds,rgbablock);

                //copie block pixels in image
                coord.x = xoffset+0; coord.y = yoffset+0;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[0][0]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[0][1]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[0][2]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[0][3]);

                coord.x = xoffset; coord.y++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[1][0]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[1][1]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[1][2]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[1][3]);

                coord.x = xoffset; coord.y++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[2][0]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[2][1]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[2][2]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[2][3]);

                coord.x = xoffset; coord.y++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[3][0]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[3][1]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[3][2]);
                coord.x++;
                cursor.moveTo(coord);
                pixel.fromARGB(rgbablock[3][3]);
            }
        }

        return image;
    }


    /**
     * Structure.
     * - 16 bits : color 0
     * - 16 bits : color 1
     * - 16 * 2 bits : pixel color
     *
     * Total : 8 bytes (64 bits).
     * Compression : 1:8
     *
     * @param ds
     * @param block
     * @throws IOException
     */
    private void readBlockDXT1(DataInputStream ds, int[][] block) throws IOException {
        palette[0] = Colors.RGB565toARGB(ds.readUShort());
        palette[1] = Colors.RGB565toARGB(ds.readUShort());
        if (palette[0] > palette[1]){
            palette[2] = Colors.interpolate(palette[0], palette[1], 1f/3f);
            palette[3] = Colors.interpolate(palette[1], palette[0], 1f/3f);
        } else {
            palette[2] = Colors.interpolate(palette[0], palette[1], 0.5f);
            palette[3] = Colors.toARGB(0, 0, 0, 0);
        }

        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                block[y][x] = palette[ds.readBits(2, DataInputStream.LSB)];
            }
        }
    }

    /**
     * Structure.
     * - 16 * 4 bits : each pixel color alpha
     * - 16 bits : color 0
     * - 16 bits : color 1
     * - 16 * 2 bits : each pixel color
     *
     * Total : 16 bytes (128 bits).
     * Compression : 1:4
     *
     * @param ds
     * @param block
     * @throws IOException
     */
    private void readBlockDXT3(DataInputStream ds, int[][] block) throws IOException {
        final int[] alphas = new int[16];
        for (int i=0;i<16;i++){
            // range 0-15 to 0-255
            alphas[i] = (ds.readBits(4, DataInputStream.LSB) * 17);
        }

        palette[0] = Colors.RGB565toARGB(ds.readUShort());
        palette[1] = Colors.RGB565toARGB(ds.readUShort());
        palette[2] = Colors.interpolate(palette[0], palette[1], 1f/3f);
        palette[3] = Colors.interpolate(palette[1], palette[0], 1f/3f);

        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                block[y][x] = (palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphas[4*y+x] << 24);
            }
        }
    }

    /**
     * Structure.
     * - 8 bits : alpha 0
     * - 8 bits : alpha 1
     * - 16 * 3 bits : pixel alpha
     * - 16 bits : color 0
     * - 16 bits : color 1
     * - 16 * 2 bits : pixel color
     *
     * Total : 16 bytes (128 bits).
     *
     * @param ds
     * @param block
     * @throws IOException
     */
    private void readBlockDXT5(DataInputStream ds, int[][] block) throws IOException {
        alphapalette[0] = ds.readUByte();
        alphapalette[1] = ds.readUByte();
        if (alphapalette[0]>alphapalette[1]){
            alphapalette[2] = (6*alphapalette[0] + 1*alphapalette[1])/7;
            alphapalette[3] = (5*alphapalette[0] + 2*alphapalette[1])/7;
            alphapalette[4] = (4*alphapalette[0] + 3*alphapalette[1])/7;
            alphapalette[5] = (3*alphapalette[0] + 4*alphapalette[1])/7;
            alphapalette[6] = (2*alphapalette[0] + 5*alphapalette[1])/7;
            alphapalette[7] = (1*alphapalette[0] + 6*alphapalette[1])/7;
        } else {
            alphapalette[2] = (4*alphapalette[0] + 1*alphapalette[1])/5;
            alphapalette[3] = (3*alphapalette[0] + 2*alphapalette[1])/5;
            alphapalette[4] = (2*alphapalette[0] + 3*alphapalette[1])/5;
            alphapalette[5] = (1*alphapalette[0] + 4*alphapalette[1])/5;
            alphapalette[6] = 0;
            alphapalette[7] = 255;
        }

        final int[] alphas = {
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB),
            ds.readBits(3, DataInputStream.LSB)
        };

        palette[0] = Colors.RGB565toARGB(ds.readUShort());
        palette[1] = Colors.RGB565toARGB(ds.readUShort());
        palette[2] = Colors.interpolate(palette[0], palette[1], 1f/3f);
        palette[3] = Colors.interpolate(palette[1], palette[0], 1f/3f);

        block[0][0] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*0+0]] << 24);
        block[0][1] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*0+1]] << 24);
        block[0][2] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*0+2]] << 24);
        block[0][3] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*0+3]] << 24);

        block[1][0] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*1+0]] << 24);
        block[1][1] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*1+1]] << 24);
        block[1][2] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*1+2]] << 24);
        block[1][3] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*1+3]] << 24);

        block[2][0] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*2+0]] << 24);
        block[2][1] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*2+1]] << 24);
        block[2][2] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*2+2]] << 24);
        block[2][3] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*2+3]] << 24);

        block[3][0] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*3+0]] << 24);
        block[3][1] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*3+1]] << 24);
        block[3][2] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*3+2]] << 24);
        block[3][3] =(palette[ds.readBits(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphas[4*3+3]] << 24);

    }


}
