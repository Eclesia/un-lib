
package science.unlicense.format.craw.astc;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;

/**
 * https://www.khronos.org/registry/gles/extensions/KHR/texture_compression_astc_hdr.txt
 *
 * @author Johann Sorel
 */
public class ASTCReader extends AbstractImageReader{

    private final Extent.Long imgSize;
    private final Extent.Long blockSize;
    private final int nbBlockX;
    private final int nbBlockY;
    private final int nbBlockZ;


    public ASTCReader(Extent.Long imgSize, Extent.Long blockSize) {
        this.imgSize = imgSize;
        this.blockSize = blockSize;

        nbBlockX = (int) Math.ceil(imgSize.get(0)/blockSize.get(0));
        nbBlockY = (int) Math.ceil(imgSize.get(1)/blockSize.get(1));
        if (imgSize.getDimension()>2){
            nbBlockZ = (int) imgSize.getL(2);
        } else {
            nbBlockZ = 1;
        }
    }


    @Override
    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        return new HashDictionary();
    }

    @Override
    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {


        throw new UnimplementedException("No supported yet");

    }

    private void readBlock(DataInputStream ds) throws IOException{

        final int blockMode = ds.readBits(11);

        //-------------------------------------------------------------------------
        //10  9   8   7   6   5   4   3   2   1   0   Width Height Notes
        //-------------------------------------------------------------------------
        //D   H     B       A     R0  0   0   R2  R1  B+4   A+2
        //D   H     B       A     R0  0   1   R2  R1  B+8   A+2
        //D   H     B       A     R0  1   0   R2  R1  A+2   B+8
        //D   H   0   B     A     R0  1   1   R2  R1  A+2   B+6
        //D   H   1   B     A     R0  1   1   R2  R1  B+2   A+2
        //D   H   0   0     A     R0  R2  R1  0   0   12    A+2
        //D   H   0   1     A     R0  R2  R1  0   0   A+2   12
        //D   H   1   1   0   0   R0  R2  R1  0   0   6     10
        //D   H   1   1   0   1   R0  R2  R1  0   0   10    6
        //  B     1   0     A     R0  R2  R1  0   0   A+6   B+6   D=0, H=0
        //x   x   1   1   1   1   1   1   1   0   0   -     -     Void-extent
        //x   x   1   1   1   x   x   x   x   0   0   -     -     Reserved*
        //x   x   x   x   x   x   x   0   0   0   0   -     -     Reserved
        //-------------------------------------------------------------------------

        //        Low Precision Range (H=0)           High Precision Range (H=1)
        //R   Weight Range  Trits  Quints  Bits   Weight Range  Trits  Quints  Bits
        //-------------------------------------------------------------------------
        //000 Invalid                             Invalid
        //001 Invalid                             Invalid
        //010 0..1                          1     0..9                   1      1
        //011 0..2            1                   0..11           1             2
        //100 0..3                          2     0..15                         4
        //101 0..4                   1            0..19                  1      2
        //110 0..5            1             1     0..23           1             3
        //111 0..7                          3     0..31                         5
        //-------------------------------------------------------------------------



        //The "Part" field specifies the number of partitions, minus one.
        final int parts = ds.readBits(2) + 1;

        if (parts==1){
            final int colorEndPointMode = ds.readBits(4);

        } else if (parts==2){

        } else if (parts==3){

        } else if (parts==4){
            //If 4 partitions are specified, the error value is returned for all texels in the block.
        }


    }

}
