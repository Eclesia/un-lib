
package science.unlicense.format.craw.bc;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.model.AbstractImageModel;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class BCRawModel extends AbstractImageModel {

    private final int width;
    private final int height;
    //decoding information
    private final int nbBlockX;
    private final int nbBlockY;

    //deocoding cache
    private final Vector2i32 coord = new Vector2i32();

    //used in BC4 and BC5
    private final float[] bc45 = new float[8];

    public BCRawModel(final int width, final int height) {
        super(new UndefinedSystem(2));
        this.width = width;
        this.height = height;

        //number of blocks on X and Y axis
        nbBlockX = (int) Math.ceil((double) width/4);
        nbBlockY = (int) Math.ceil((double) height/4);
    }

    @Override
    public NumberType getNumericType() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Buffer createBuffer(Extent.Long dimensions, BufferFactory factory) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public TupleGrid asTupleBuffer(Image image) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Image readATI1(final DataInputStream ds) throws IOException{
        final Image image = Images.createCustomBand(new Extent.Long(width, height),1,Float32.TYPE);
        final TupleGridCursor cursor = image.getRawModel().asTupleBuffer(image).cursor();

        final float[][] block = new float[4][4];

        int x,y,bx,by,yoffset,xoffset;
        for (y=0; y<nbBlockY; y++){
            yoffset = y*4;
            for (x=0; x<nbBlockX; x++){
                xoffset = x*4;
                readBlockBC4UNorm(ds,block);

                //copie block pixels in image
                for (by=0;by<4;by++){
                    for (bx=0;bx<4;bx++){
                        coord.x = xoffset + bx;
                        coord.y = yoffset + by;
                        cursor.moveTo(coord);
                        cursor.samples().set(0,block[by][bx]);
                    }
                }
            }
        }

        return image;
    }

    public Image readATI2(final DataInputStream ds) throws IOException{
        final Image image = Images.createCustomBand(new Extent.Long(width, height),2,Float32.TYPE);
        final TupleGridCursor cursor = image.getRawModel().asTupleBuffer(image).cursor();

        final float[][][] block = new float[4][4][];

        int x,y,bx,by,yoffset,xoffset;
        for (y=0; y<nbBlockY; y++){
            yoffset = y*4;
            for (x=0; x<nbBlockX; x++){
                xoffset = x*4;
                readBlockBC5UNorm(ds,block);

                //copie block pixels in image
                for (by=0;by<4;by++){
                    for (bx=0;bx<4;bx++){
                        coord.x = xoffset + bx;
                        coord.y = yoffset + by;
                        cursor.moveTo(coord);
                        cursor.samples().set(block[by][bx]);
                    }
                }
            }
        }

        return image;
    }

    /**
     * Ref :
     * https://msdn.microsoft.com/en-us/library/windows/desktop/bb694531(v=vs.85).aspx#BC4_UNORM
     *
     * Structure.
     * - 1 ubyte : color 0
     * - 1 ubyte : color 1
     * - 16 * 3 bits : pixel
     *
     * Total : 8 bytes (64 bits).
     *
     * @param ds
     * @param block
     * @throws IOException
     */
    private void readBlockBC4UNorm(DataInputStream ds, float[][] block) throws IOException {
        bc45[0] = ds.readUByte();
        bc45[1] = ds.readUByte();
        fillBC45UNorm(bc45);

        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                block[y][x] = bc45[ds.readBits(3, DataInputStream.LSB)];
            }
        }
    }

    /**
     * Ref :
     * https://msdn.microsoft.com/en-us/library/windows/desktop/bb694531(v=vs.85).aspx#BC4_SNORM
     *
     * Structure.
     * - 1 sbyte : color 0
     * - 1 sbyte : color 1
     * - 16 * 3 bits : pixel
     *
     * Total : 8 bytes (64 bits).
     *
     * @param ds
     * @param block
     * @throws IOException
     */
    private void readBlockBC4SNorm(DataInputStream ds, float[][] block) throws IOException {
        bc45[0] = ds.readByte();
        bc45[1] = ds.readByte();
        fillBC45SNorm(bc45);

        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                block[y][x] = bc45[ds.readBits(3, DataInputStream.LSB)];
            }
        }
    }

    /**
     * Ref :
     * https://msdn.microsoft.com/en-us/library/windows/desktop/bb694531(v=vs.85).aspx#BC5_UNORM
     *
     * Structure.
     * - 1 ubyte : color 0 sample 0
     * - 1 ubyte : color 1 sample 0
     * - 16 * 3 bits : pixel sample 0
     * - 1 ubyte : color 0 sample 1
     * - 1 ubyte : color 1 sample 1
     * - 16 * 3 bits : pixel sample 1
     *
     * Total : 16 bytes (128 bits).
     *
     * @param ds
     * @param block
     * @throws IOException
     */
    private void readBlockBC5UNorm(DataInputStream ds, float[][][] block) throws IOException {
        bc45[0] = ds.readUByte();
        bc45[1] = ds.readUByte();
        fillBC45UNorm(bc45);

        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                block[y][x][0] = bc45[ds.readBits(3, DataInputStream.LSB)];
            }
        }

        bc45[0] = ds.readUByte();
        bc45[1] = ds.readUByte();
        fillBC45UNorm(bc45);

        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                block[y][x][1] = bc45[ds.readBits(3, DataInputStream.LSB)];
            }
        }
    }

    /**
     * Ref :
     * https://msdn.microsoft.com/en-us/library/windows/desktop/bb694531(v=vs.85).aspx#BC5_UNORM
     *
     * Structure.
     * - 1 sbyte : color 0 sample 0
     * - 1 sbyte : color 1 sample 0
     * - 16 * 3 bits : pixel sample 0
     * - 1 sbyte : color 0 sample 1
     * - 1 sbyte : color 1 sample 1
     * - 16 * 3 bits : pixel sample 1
     *
     * Total : 16 bytes (128 bits).
     *
     * @param ds
     * @param block
     * @throws IOException
     */
    private void readBlockBC5SNorm(DataInputStream ds, float[][][] block) throws IOException {
        bc45[0] = ds.readByte();
        bc45[1] = ds.readByte();
        fillBC45SNorm(bc45);

        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                block[y][x][0] = bc45[ds.readBits(3, DataInputStream.LSB)];
            }
        }

        bc45[0] = ds.readByte();
        bc45[1] = ds.readByte();
        fillBC45SNorm(bc45);

        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                block[y][x][0] = bc45[ds.readBits(3, DataInputStream.LSB)];
            }
        }
    }

    private static void fillBC45UNorm(float[] bc){
        if (bc[0] > bc[1]){
            bc[2] = (6*bc[0] + 1*bc[1])/7f;
            bc[3] = (5*bc[0] + 2*bc[1])/7f;
            bc[4] = (4*bc[0] + 3*bc[1])/7f;
            bc[5] = (3*bc[0] + 4*bc[1])/7f;
            bc[6] = (2*bc[0] + 5*bc[1])/7f;
            bc[7] = (1*bc[0] + 6*bc[1])/7f;
        } else {
            bc[2] = (4*bc[0] + 1*bc[1])/5f;
            bc[3] = (3*bc[0] + 2*bc[1])/5f;
            bc[4] = (2*bc[0] + 3*bc[1])/5f;
            bc[5] = (1*bc[0] + 4*bc[1])/5f;
            bc[6] = 0f;
            bc[7] = 1f;
        }
    }

    private static void fillBC45SNorm(float[] bc){
        if (bc[0] > bc[1]){
            bc[2] = (6*bc[0] + 1*bc[1])/7f;
            bc[3] = (5*bc[0] + 2*bc[1])/7f;
            bc[4] = (4*bc[0] + 3*bc[1])/7f;
            bc[5] = (3*bc[0] + 4*bc[1])/7f;
            bc[6] = (2*bc[0] + 5*bc[1])/7f;
            bc[7] = (1*bc[0] + 6*bc[1])/7f;
        } else {
            bc[2] = (4*bc[0] + 1*bc[1])/5f;
            bc[3] = (3*bc[0] + 2*bc[1])/5f;
            bc[4] = (2*bc[0] + 3*bc[1])/5f;
            bc[5] = (1*bc[0] + 4*bc[1])/5f;
            bc[6] = -1f;
            bc[7] =  1f;
        }
    }

}
