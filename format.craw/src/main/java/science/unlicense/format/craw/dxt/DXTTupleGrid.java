
package science.unlicense.format.craw.dxt;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DataCursor;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.AbstractNumberTupleGrid;
import science.unlicense.image.api.color.Colors;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;

/**
 *
 * @author Johann Sorel
 */
class DXTTupleGrid extends AbstractNumberTupleGrid {
    //decoding information
    private final int nbBlockX;
    private final int nbBlockY;
    private final Buffer pbuffer;
    private final int dxtType;
    private final int blockByteSize;
    //DXT 1-5 use a 4 colour palette
    private final int[] palette = new int[4];
    private final int[] coord = new int[2];
    //used in DXT 4-5
    private final int[] alphapalette = new int[8];

    public DXTTupleGrid(Extent.Long size, int dxtType, Buffer pbuffer, SampleSystem ss) {
        super(size, UInt8.TYPE, ss);
        this.dxtType = dxtType;
        this.pbuffer = pbuffer;
        this.blockByteSize = dxtType == DXTRawModel.TYPE_DXT1 ? 8 : 16;
        //number of blocks on X and Y axis
        nbBlockX = (int) Math.ceil(size.get(0) / 4);
        nbBlockY = (int) Math.ceil(size.get(1) / 4);
    }

    @Override
    public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
        //calculate block offset
        final int c0 = (int) coordinate.get(0);
        final int c1 = (int) coordinate.get(1);
        final int blockX = c0 / 4;
        final int blockPixX = c0 % 4;
        final int blockY = c1 / 4;
        final int blockPixY = c1 % 4;
        //move and read block
        final int offset = (blockY * nbBlockX + blockX) * blockByteSize;
        final DataCursor cursor = pbuffer.dataCursor(pbuffer.getNumericType(), Endianness.LITTLE_ENDIAN);
        cursor.skipBytes(offset);
        final int[][] rgbablock = new int[4][4];
        if (dxtType == DXTRawModel.TYPE_DXT1) {
            readBlockDXT1(cursor, rgbablock);
        } else if (dxtType == DXTRawModel.TYPE_DXT2) {
            readBlockDXT3(cursor, rgbablock);
        } else if (dxtType == DXTRawModel.TYPE_DXT3) {
            readBlockDXT3(cursor, rgbablock);
        } else if (dxtType == DXTRawModel.TYPE_DXT4) {
            readBlockDXT5(cursor, rgbablock);
        } else if (dxtType == DXTRawModel.TYPE_DXT5) {
            readBlockDXT5(cursor, rgbablock);
        }
        int[] argb = Colors.toARGB(rgbablock[blockPixY][blockPixX],null);
        buffer.set(0, argb[0]);
        buffer.set(1, argb[1]);
        buffer.set(2, argb[2]);
        buffer.set(3, argb[3]);
        return buffer;
    }

    @Override
    public void setTuple(Tuple coordinate, Tuple buffer) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public Buffer getPrimitiveBuffer() {
        return pbuffer;
    }

    /**
     * Structure.
     * - 16 bits : color 0
     * - 16 bits : color 1
     * - 16 * 2 bits : pixel color
     *
     * Total : 8 bytes (64 bits).
     * Compression : 1:8
     *
     * @param ds
     * @param block
     * @throws IOException
     */
    private void readBlockDXT1(DataCursor ds, int[][] block) {
        palette[0] = Colors.RGB565toARGB(ds.readUShort());
        palette[1] = Colors.RGB565toARGB(ds.readUShort());
        if (palette[0] > palette[1]){
            palette[2] = Colors.interpolate(palette[0], palette[1], 1f/3f);
            palette[3] = Colors.interpolate(palette[1], palette[0], 1f/3f);
        } else {
            palette[2] = Colors.interpolate(palette[0], palette[1], 0.5f);
            palette[3] = Colors.toARGB(0, 0, 0, 0);
        }

        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                block[y][x] = palette[ds.readBit(2, DataInputStream.LSB)];
            }
        }
    }

    /**
     * Structure.
     * - 16 * 4 bits : each pixel color alpha
     * - 16 bits : color 0
     * - 16 bits : color 1
     * - 16 * 2 bits : each pixel color
     *
     * Total : 16 bytes (128 bits).
     * Compression : 1:4
     *
     * @param ds
     * @param block
     * @throws IOException
     */
    private void readBlockDXT3(DataCursor ds, int[][] block) {
        final int[] alphas = new int[16];
        for (int i=0;i<16;i++){
            // range 0-15 to 0-255
            alphas[i] = (ds.readBit(4, DataInputStream.LSB) * 17);
        }

        palette[0] = Colors.RGB565toARGB(ds.readUShort());
        palette[1] = Colors.RGB565toARGB(ds.readUShort());
        palette[2] = Colors.interpolate(palette[0], palette[1], 1f/3f);
        palette[3] = Colors.interpolate(palette[1], palette[0], 1f/3f);

        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                block[y][x] = (palette[ds.readBit(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphas[4*y+x] << 24);
            }
        }
    }

    /**
     * Structure.
     * - 8 bits : alpha 0
     * - 8 bits : alpha 1
     * - 16 * 3 bits : pixel alpha
     * - 16 bits : color 0
     * - 16 bits : color 1
     * - 16 * 2 bits : pixel color
     *
     * Total : 16 bytes (128 bits).
     *
     * @param ds
     * @param block
     * @throws IOException
     */
    private void readBlockDXT5(DataCursor ds, int[][] block) {
        alphapalette[0] = ds.readUByte();
        alphapalette[1] = ds.readUByte();
        if (alphapalette[0]>alphapalette[1]){
            alphapalette[2] = (6*alphapalette[0] + 1*alphapalette[1])/7;
            alphapalette[3] = (5*alphapalette[0] + 2*alphapalette[1])/7;
            alphapalette[4] = (4*alphapalette[0] + 3*alphapalette[1])/7;
            alphapalette[5] = (3*alphapalette[0] + 4*alphapalette[1])/7;
            alphapalette[6] = (2*alphapalette[0] + 5*alphapalette[1])/7;
            alphapalette[7] = (1*alphapalette[0] + 6*alphapalette[1])/7;
        } else {
            alphapalette[2] = (4*alphapalette[0] + 1*alphapalette[1])/5;
            alphapalette[3] = (3*alphapalette[0] + 2*alphapalette[1])/5;
            alphapalette[4] = (2*alphapalette[0] + 3*alphapalette[1])/5;
            alphapalette[5] = (1*alphapalette[0] + 4*alphapalette[1])/5;
            alphapalette[6] = 0;
            alphapalette[7] = 255;
        }

        final int[] alphas = new int[16];
        for (int i=0;i<16;i++) alphas[i] = ds.readBit(3, DataInputStream.LSB);

        palette[0] = Colors.RGB565toARGB(ds.readUShort());
        palette[1] = Colors.RGB565toARGB(ds.readUShort());
        palette[2] = Colors.interpolate(palette[0], palette[1], 1f/3f);
        palette[3] = Colors.interpolate(palette[1], palette[0], 1f/3f);

        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                final int alphacode = alphas[4*y+x];
                block[y][x] =(palette[ds.readBit(2, DataInputStream.LSB)] & Colors.MASK_RGB) | (alphapalette[alphacode] << 24);
            }
        }

    }

}
