
package science.unlicense.geospatial.transform.spherical;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geospatial.transform.AbstractTransformTest;
import science.unlicense.math.api.Angles;

/**
 *
 * @author Johann Sorel
 */
public class MercatorTransformTest extends AbstractTransformTest{

    @Test
    public void testTransform(){

        MercatorTransform trs = new MercatorTransform(Angles.degreeToRadian(40));

        double[] coord = new double[]{Angles.degreeToRadian(10),Angles.degreeToRadian(10)};
        trs.transform(coord, 0, coord, 0, 1);
        coord = trs.inverseTransform(coord);
        Assert.assertEquals(10.0,Angles.radianToDegree(coord[0]),DELTA);
        Assert.assertEquals(10.0,Angles.radianToDegree(coord[1]),DELTA);

    }
}
