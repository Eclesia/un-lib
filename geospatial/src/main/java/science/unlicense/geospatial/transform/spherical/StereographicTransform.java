package science.unlicense.geospatial.transform.spherical;

import static java.lang.Math.asin;
import static java.lang.Math.atan;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import science.unlicense.math.api.transform.AbstractTransform;
import science.unlicense.math.api.transform.Transform;

/**
 * Resources: Map Projections - A Working Manual.
 * John P. Snyder U.S. Geological Survey Professional Paper 1395 (21) p. 154
 *
 * @author Samuel Andrés
 */
public class StereographicTransform extends AbstractTransform {

    protected double radius; // R
    protected double standardParallel; // Phi 1
    protected double centralGreenwichLongitude; // Lambda 0
    protected double centralScaleFactor; // k 0

    protected double sinPhi1;
    protected double cosPhi1;

    public StereographicTransform(final double radius, final double standardParallel, final double centralLongitude, final double centralScaleFactor) {
        super(2);
        this.radius = radius;
        this.standardParallel = standardParallel;

        sinPhi1 = sin(standardParallel);
        cosPhi1 = cos(standardParallel);

        this.centralGreenwichLongitude = centralLongitude;
        this.centralScaleFactor = centralScaleFactor;
    }

    @Override
    public double[] transform(double[] src, double[] dst) {
        if (dst==null) dst = new double[2];
        final double lat = src[0];
        final double lon = src[1];
        final double scaleFactor = 2 * centralScaleFactor / (1 + sinPhi1 * sin(lat) + cosPhi1 * cos(lat) * cos(lon - centralGreenwichLongitude));
        dst[0] = radius * scaleFactor * cos(lat) * sin(lon - centralGreenwichLongitude);
        dst[1] = radius * scaleFactor * (cosPhi1 * sin(lat) - sinPhi1 * cos(lat) * cos(lon - centralGreenwichLongitude));
        return dst;
    }

    public double[] inverseTransform(double[] tuple) {

        final double x = tuple[0];
        final double y = tuple[1];
        final double rho = sqrt(x * x + y * y);
        final double c = 2 * atan(rho / (2 * radius * centralScaleFactor));
        final double cosC = cos(c);
        final double sinC = sin(c);
        final double[] unproject = new double[2];
        unproject[0] = asin(cosC * sinPhi1 + (y * sinC * cosPhi1 / rho));
        unproject[1] = centralGreenwichLongitude + atan2(x * sinC, rho * cosPhi1 * cosC - y * sinPhi1 * sinC);
        return unproject;
    }

    @Override
    public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        for (int i=0,off=0;i<nbTuple;i++,off+=2) {
            final double lat = source[sourceOffset+off+0];
            final double lon = source[sourceOffset+off+1];
            final double scaleFactor = 2 * centralScaleFactor / (1 + sinPhi1 * sin(lat) + cosPhi1 * cos(lat) * cos(lon - centralGreenwichLongitude));
            dest[destOffset+off+0] = radius * scaleFactor * cos(lat) * sin(lon - centralGreenwichLongitude);
            dest[destOffset+off+1] = radius * scaleFactor * (cosPhi1 * sin(lat) - sinPhi1 * cos(lat) * cos(lon - centralGreenwichLongitude));
        }
    }

    @Override
    public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        for (int i=0,off=0;i<nbTuple;i++,off+=2) {
            final float lat = source[sourceOffset+off+0];
            final float lon = source[sourceOffset+off+1];
            final double scaleFactor = 2 * centralScaleFactor / (1 + sinPhi1 * sin(lat) + cosPhi1 * cos(lat) * cos(lon - centralGreenwichLongitude));
            dest[destOffset+off+0] = (float) (radius * scaleFactor * cos(lat) * sin(lon - centralGreenwichLongitude));
            dest[destOffset+off+1] = (float) (radius * scaleFactor * (cosPhi1 * sin(lat) - sinPhi1 * cos(lat) * cos(lon - centralGreenwichLongitude)));
        }
    }

    @Override
    public Transform invert() {
        throw new RuntimeException("Uninvertible transform");
    }
}
