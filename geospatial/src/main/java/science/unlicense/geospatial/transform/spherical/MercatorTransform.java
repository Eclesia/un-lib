
package science.unlicense.geospatial.transform.spherical;

import static java.lang.Math.atan;
import static java.lang.Math.log;
import static java.lang.Math.sinh;
import static java.lang.Math.tan;
import static science.unlicense.math.api.Maths.QUATER_PI;
import science.unlicense.math.api.transform.AbstractTransform;
import science.unlicense.math.api.transform.Transform;

/**
 * Resources :
 * http://mathworld.wolfram.com/MercatorProjection.html
 *
 * @author Johann Sorel
 */
public class MercatorTransform extends AbstractTransform {


    private final double lonZero;

    public MercatorTransform(double longitudeZero) {
        super(2);
        this.lonZero = longitudeZero;
    }


    public double[] transform(double[] src, double[] dst) {
        if (dst==null) dst = new double[2];
        final double lon = src[0];
        final double lat = src[1];
        dst[0] = lon - lonZero;
        dst[1] = log(tan(QUATER_PI + 0.5*lat));
        return dst;
    }

    public double[] inverseTransform(double[] tuple) {

        double x = tuple[0];
        double y = tuple[1];
        double lon = x + lonZero;
        double lat = atan(sinh(y));

        return new double[]{lon,lat};
    }

    /**
     *
     * Resources :
     * http://mathworld.wolfram.com/Gudermannian.html
     *
     * @param x
     * @return
     */
    public static double gudermannian(double x) {
        return 2.0 * Math.atan( Math.tanh(0.5*x));
        //return 2.0 * Math.atan(Math.exp(x)) - 0.5 * Math.PI;
    }

    @Override
    public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        for (int i=0,off=0;i<nbTuple;i++,off+=2) {
            double lon = source[sourceOffset+off+0];
            double lat = source[sourceOffset+off+1];
            dest[destOffset+off+0] = lon - lonZero;
            dest[destOffset+off+1] = log(tan(QUATER_PI + 0.5*lat));
        }
    }

    @Override
    public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        for (int i=0,off=0;i<nbTuple;i++,off+=2) {
            float lon = source[sourceOffset+off+0];
            float lat = source[sourceOffset+off+1];
            dest[destOffset+off+0] = (float) (lon - lonZero);
            dest[destOffset+off+1] = (float) log(tan(QUATER_PI + 0.5*lat));
        }
    }

    @Override
    public Transform invert() {
        throw new RuntimeException("Uninvertible transform");
    }
}
