
package science.unlicense.geospatial.transform.spherical;

import static java.lang.Math.atan;
import static java.lang.Math.log;
import static java.lang.Math.sinh;
import static java.lang.Math.tan;
import science.unlicense.common.api.exception.UnimplementedException;
import static science.unlicense.math.api.Maths.QUATER_PI;
import science.unlicense.math.api.transform.AbstractTransform;
import science.unlicense.math.api.transform.Transform;

/**
 * Resources :
 * http://mathworld.wolfram.com/MercatorProjection.html
 *
 * @author Johann Sorel
 */
public class ObliqueMercatorTransform extends AbstractTransform {


    private final double lonZero;

    public ObliqueMercatorTransform(double longitudeZero) {
        super(2);
        this.lonZero = longitudeZero;
    }


    public double[] forwardTransform(double[] tuple) {

        final double lon = tuple[0];
        final double lat = tuple[1];
        final double x = lon - lonZero;
        final double y = log(tan(QUATER_PI + 0.5*lat));

        return new double[]{x,y};
    }

    public double[] inverseTransform(double[] tuple) {

        double x = tuple[0];
        double y = tuple[1];
        double lon = x + lonZero;
        double lat = atan(sinh(y));

        return new double[]{lon,lat};
    }

    @Override
    public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Transform invert() {
        throw new RuntimeException("Uninvertible transform");
    }
}
