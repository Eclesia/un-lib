
package science.unlicense.system.virtual;

import science.unlicense.system.GlobalProperties;
import science.unlicense.system.ModuleManager;
import science.unlicense.system.SocketManager;
import science.unlicense.system.Work;
import science.unlicense.system.WorkUnits;
import science.unlicense.system.api.desktop.TransferBag;

/**
 *
 * @author Johann Sorel
 */
public class VirtualSystem extends science.unlicense.system.System {

    @Override
    public String getType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ModuleManager getModuleManager() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SocketManager getSocketManager() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public GlobalProperties getProperties() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TransferBag getClipBoard() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TransferBag getDragAndDrapBag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public WorkUnits createWorkUnits(float priority) {
        return new VirtualWorkUnits();
    }

    private static class VirtualWorkUnits implements WorkUnits {

        @Override
        public void submit(long range, Work work) {
            for (long i=0;i<range;i++) {
                work.run(i);
            }
        }

        @Override
        public void dispose() {
        }

    }
}
