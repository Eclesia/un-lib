
package science.unlicense.runtime;

import science.unlicense.system.virtual.VirtualSystem;

/**
 *
 * @author Johann Sorel
 */
public final class Runtime {

    private static final VirtualSystem SYSTEM = new VirtualSystem();

    public static void init() {
        science.unlicense.system.System.set(SYSTEM);
    }

    private Runtime(){}

}
