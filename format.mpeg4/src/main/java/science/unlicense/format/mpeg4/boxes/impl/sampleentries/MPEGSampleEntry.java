
package science.unlicense.format.mpeg4.boxes.impl.sampleentries;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;

/**
 * The MPEG sample entry is used in MP4 streams other than video, audio and
 * hint. It contains only one <code>ESDBox</code>.
 *
 * @author Alexander Simm
 */
public class MPEGSampleEntry extends SampleEntry {

    public MPEGSampleEntry() {
        super("MPEG Sample Entry");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        readChildren(in);
    }
}
