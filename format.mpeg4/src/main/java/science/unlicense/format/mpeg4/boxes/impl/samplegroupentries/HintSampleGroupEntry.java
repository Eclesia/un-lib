package science.unlicense.format.mpeg4.boxes.impl.samplegroupentries;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;

/**
 *
 * @author Alexander Simm
 */
public class HintSampleGroupEntry extends SampleGroupDescriptionEntry {

    public HintSampleGroupEntry() {
        super("Hint Sample Group Entry");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
    }
}
