package science.unlicense.format.mpeg4.api;

/**
 * Represents the data type of a <code>Frame</code> or <code>Track</code>.
 *
 * @author Alexander Simm
 */
public enum Type {

    VIDEO,
    AUDIO;
}
