package science.unlicense.format.mpeg4.api.codec;

import science.unlicense.format.mpeg4.api.DecoderInfo;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.AC3SpecificBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.CodecSpecificBox;

/**
 *
 * @author Alexander Simm
 */
public class AC3DecoderInfo extends DecoderInfo {

    private final AC3SpecificBox box;

    public AC3DecoderInfo(CodecSpecificBox box) {
        this.box = (AC3SpecificBox) box;
    }

    public boolean isLfeon() {
        return box.isLfeon();
    }

    public int getFscod() {
        return box.getFscod();
    }

    public int getBsmod() {
        return box.getBsmod();
    }

    public int getBsid() {
        return box.getBsid();
    }

    public int getBitRateCode() {
        return box.getBitRateCode();
    }

    public int getAcmod() {
        return box.getAcmod();
    }
}
