package science.unlicense.format.mpeg4.boxes;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;

/**
 * Box implementation that is used for unknown types.
 *
 * @author Alexander Simm
 */
public class UnknownBox extends Box {

    public UnknownBox() {
        super("unknown");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        //no need to read, box will be skipped
    }
}
