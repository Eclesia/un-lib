package science.unlicense.format.mpeg4.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4Constants;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.impl.HandlerBox;
import science.unlicense.format.mpeg4.boxes.impl.MovieHeaderBox;
import science.unlicense.format.mpeg4.boxes.Box;

/**
 *
 * @author Alexander Simm
 */
public class Movie {

    private final MP4InputStream in;
    private final MovieHeaderBox mvhd;
    private final List<Track> tracks;
    private final MetaData metaData;
    private final List<Protection> protections;

    public Movie(Box moov, MP4InputStream in) {
        this.in = in;

        //create tracks
        mvhd = (MovieHeaderBox) moov.getChild(MP4Constants.BOX_MOVIE_HEADER);
        Sequence trackBoxes = moov.getChildren(MP4Constants.BOX_TRACK);
        tracks = new ArrayList<Track>(trackBoxes.getSize());
        Track track;
        for (int i = 0; i<trackBoxes.getSize(); i++) {
            track = createTrack((Box) trackBoxes.get(i));
            if (track!=null) tracks.add(track);
        }

        //read metadata: moov.meta/moov.udta.meta
        metaData = new MetaData();
        if (moov.hasChild(MP4Constants.BOX_META)) metaData.parse(null, moov.getChild(MP4Constants.BOX_META));
        else if (moov.hasChild(MP4Constants.BOX_USER_DATA)) {
            final Box udta = moov.getChild(MP4Constants.BOX_USER_DATA);
            if (udta.hasChild(MP4Constants.BOX_META)) metaData.parse(udta, udta.getChild(MP4Constants.BOX_META));
        }

        //detect DRM
        protections = new ArrayList<Protection>();
        if (moov.hasChild(MP4Constants.BOX_ITEM_PROTECTION)) {
            final Box ipro = moov.getChild(MP4Constants.BOX_ITEM_PROTECTION);
            final Sequence children = ipro.getChildren(MP4Constants.BOX_PROTECTION_SCHEME_INFORMATION);
            for (int k=0,kn=children.getSize();k<kn;k++) {
                final Box sinf = (Box) children.get(k);
                protections.add(Protection.parse(sinf));
            }
        }
    }

    //TODO: support hint and meta
    private Track createTrack(Box trak) {
        final HandlerBox hdlr = (HandlerBox) trak.getChild(MP4Constants.BOX_MEDIA).getChild(MP4Constants.BOX_HANDLER);
        final Track track;
        switch((int) hdlr.getHandlerType()) {
            case HandlerBox.TYPE_VIDEO:
                track = new VideoTrack(trak, in);
                break;
            case HandlerBox.TYPE_SOUND:
                track = new AudioTrack(trak, in);
                break;
            default:
                track = null;
        }
        return track;
    }

    /**
     * Returns an unmodifiable list of all tracks in this movie. The tracks are
     * ordered as they appeare in the file/stream.
     *
     * @return the tracks contained by this movie
     */
    public List<Track> getTracks() {
        return Collections.unmodifiableList(tracks);
    }

    /**
     * Returns an unmodifiable list of all tracks in this movie with the
     * specified type. The tracks are ordered as they appeare in the
     * file/stream.
     *
     * @return the tracks contained by this movie with the passed type
     */
    public List<Track> getTracks(Type type) {
        final List<Track> l = new ArrayList<Track>();
        for (Track t : tracks) {
            if (t.getType().equals(type)) l.add(t);
        }
        return Collections.unmodifiableList(l);
    }

    /**
     * Returns an unmodifiable list of all tracks in this movie whose samples
     * are encoded with the specified codec. The tracks are ordered as they
     * appeare in the file/stream.
     *
     * @return the tracks contained by this movie with the passed type
     */
    public List<Track> getTracks(Track.Codec codec) {
        final List<Track> l = new ArrayList<Track>();
        for (Track t : tracks) {
            if (t.getCodec().equals(codec)) l.add(t);
        }
        return Collections.unmodifiableList(l);
    }

    /**
     * Indicates if this movie contains metadata. If false the <code>MetaData</code>
     * object returned by <code>getMetaData()</code> will not contain any field.
     *
     * @return true if this movie contains any metadata
     */
    public boolean containsMetaData() {
        return metaData.containsMetaData();
    }

    /**
     * Returns the MetaData object for this movie.
     *
     * @return the MetaData for this movie
     */
    public MetaData getMetaData() {
        return metaData;
    }

    /**
     * Returns the <code>ProtectionInformation</code> objects that contains
     * details about the DRM systems used. If no protection is present the
     * returned list will be empty.
     *
     * @return a list of protection informations
     */
    public List<Protection> getProtections() {
        return Collections.unmodifiableList(protections);
    }

    //mvhd
    /**
     * Returns the time this movie was created.
     * @return the creation time
     */
    public Date getCreationTime() {
        return Utils.getDate(mvhd.getCreationTime());
    }

    /**
     * Returns the last time this movie was modified.
     * @return the modification time
     */
    public Date getModificationTime() {
        return Utils.getDate(mvhd.getModificationTime());
    }

    /**
     * Returns the duration in seconds.
     * @return the duration
     */
    public double getDuration() {
        return (double) mvhd.getDuration()/(double) mvhd.getTimeScale();
    }

    /**
     * Indicates if there are more frames to be read in this movie.
     *
     * @return true if there is at least one track in this movie that has at least one more frame to read.
     */
    public boolean hasMoreFrames() {
        for (Track track : tracks) {
            if (track.hasMoreFrames()) return true;
        }
        return false;
    }

    /**
     * Reads the next frame from this movie (from one of the contained tracks).
     * The frame is the next in time-order, thus the next for playback. If none
     * of the tracks contains any more frames, null is returned.
     *
     * @return the next frame or null if there are no more frames to read from this movie.
     * @throws IOException if reading fails
     */
    public Frame readNextFrame() throws IOException {
        Track track = null;
        for (Track t : tracks) {
            if (t.hasMoreFrames()&&(track==null||t.getNextTimeStamp()<track.getNextTimeStamp())) track = t;
        }

        return (track==null) ? null : track.readNextFrame();
    }
}
