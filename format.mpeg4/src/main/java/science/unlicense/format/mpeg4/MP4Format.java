
package science.unlicense.format.mpeg4;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;

/**
 * Mpeg-4 format.
 * This format is defined by ISO 14496-1 to 14496-22.
 * I'm not rich enough to pay more than 3000€ for just this spec !
 * So this reader/writer will likely remain a draft/in-progress state.
 *
 * Other descriptive docs and tutorials :
 * http://en.wikipedia.org/wiki/ISO_base_media_file_format
 * http://www.ftyps.com
 * http://atomicparsley.sourceforge.net/mpeg-4files.html
 * http://perso.telecom-paristech.fr/~concolat/MPEGFileFormats.pdf
 *
 * @author Johann Sorel
 */
public class MP4Format extends AbstractMediaFormat {

    public static final MP4Format INSTANCE = new MP4Format();

    public MP4Format() {
        super(new Chars("mp4"));
        shortName = new Chars("MP4");
        longName = new Chars("MPEG-4");
        mimeTypes.add(new Chars("video/mp4"));
        extensions.add(new Chars("mp4"));
        extensions.add(new Chars("m4a"));
        extensions.add(new Chars("m4p"));
        extensions.add(new Chars("m4b"));
        extensions.add(new Chars("m4r"));
        extensions.add(new Chars("m4v"));
        resourceTypes.add(Media.class);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public Store open(Object input) throws IOException {
        return new MP4Store((Path) input);
    }

}
