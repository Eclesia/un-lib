package science.unlicense.format.mpeg4;

import science.unlicense.encoding.api.io.IOException;


/**
 *
 * @author Alexander Simm
 */
public class MP4Exception extends IOException {

    public MP4Exception(String message) {
        super(null, message);
    }
}
