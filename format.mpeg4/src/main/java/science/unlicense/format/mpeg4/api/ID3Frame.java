package science.unlicense.format.mpeg4.api;

import java.nio.charset.Charset;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Alexander Simm
 * @author Johann Sorel
 */
class ID3Frame {

    public static final int ALBUM_TITLE                                 = 1413565506; //TALB
    public static final int ALBUM_SORT_ORDER                            = 1414745921; //TSOA
    public static final int ARTIST                                      = 1414546737; //TPE1
    public static final int ATTACHED_PICTURE                            = 1095780675; //APIC
    public static final int AUDIO_ENCRYPTION                            = 1095061059; //AENC
    public static final int AUDIO_SEEK_POINT_INDEX                      = 1095979081; //ASPI
    public static final int BAND                                        = 1414546738; //TPE2
    public static final int BEATS_PER_MINUTE                            = 1413632077; //TBPM
    public static final int COMMENTS                                    = 1129270605; //COMM
    public static final int COMMERCIAL_FRAME                            = 1129270610; //COMR
    public static final int COMMERCIAL_INFORMATION                      = 1464029005; //WCOM
    public static final int COMPOSER                                    = 1413697357; //TCOM
    public static final int CONDUCTOR                                   = 1414546739; //TPE3
    public static final int CONTENT_GROUP_DESCRIPTION                   = 1414091825; //TIT1
    public static final int CONTENT_TYPE                                = 1413697358; //TCON
    public static final int COPYRIGHT                                   = 1464029008; //WCOP
    public static final int COPYRIGHT_MESSAGE                           = 1413697360; //TCOP
    public static final int ENCODED_BY                                  = 1413828163; //TENC
    public static final int ENCODING_TIME                               = 1413760334; //TDEN
    public static final int ENCRYPTION_METHOD_REGISTRATION              = 1162756946; //ENCR
    public static final int EQUALISATION                                = 1162958130; //EQU2
    public static final int EVENT_TIMING_CODES                          = 1163150159; //ETCO
    public static final int FILE_OWNER                                  = 1414485838; //TOWN
    public static final int FILE_TYPE                                   = 1413893204; //TFLT
    public static final int GENERAL_ENCAPSULATED_OBJECT                 = 1195724610; //GEOB
    public static final int GROUP_IDENTIFICATION_REGISTRATION           = 1196575044; //GRID
    public static final int INITIAL_KEY                                 = 1414219097; //TKEY
    public static final int INTERNET_RADIO_STATION_NAME                 = 1414681422; //TRSN
    public static final int INTERNET_RADIO_STATION_OWNER                = 1414681423; //TRSO
    public static final int MODIFIED_BY                                 = 1414546740; //TPE4
    public static final int INVOLVED_PEOPLE_LIST                        = 1414090828; //TIPL
    public static final int INTERNATIONAL_STANDARD_RECORDING_CODE       = 1414746691; //TSRC
    public static final int LANGUAGES                                   = 1414283598; //TLAN
    public static final int LENGTH                                      = 1414284622; //TLEN
    public static final int LINKED_INFORMATION                          = 1279872587; //LINK
    public static final int LYRICIST                                    = 1413830740; //TEXT
    public static final int MEDIA_TYPE                                  = 1414350148; //TMED
    public static final int MOOD                                        = 1414352719; //TMOO
    public static final int MPEG_LOCATION_LOOKUP_TABLE                  = 1296845908; //MLLT
    public static final int MUSICIAN_CREDITS_LIST                       = 1414349644; //TMCL
    public static final int MUSIC_CD_IDENTIFIER                         = 1296254025; //MCDI
    public static final int OFFICIAL_ARTIST_WEBPAGE                     = 1464811858; //WOAR
    public static final int OFFICIAL_AUDIO_FILE_WEBPAGE                 = 1464811846; //WOAF
    public static final int OFFICIAL_AUDIO_SOURCE_WEBPAGE               = 1464811859; //WOAS
    public static final int OFFICIAL_INTERNET_RADIO_STATION_HOMEPAGE    = 1464816211; //WORS
    public static final int ORIGINAL_ALBUM_TITLE                        = 1414480204; //TOAL
    public static final int ORIGINAL_ARTIST                             = 1414484037; //TOPE
    public static final int ORIGINAL_FILENAME                           = 1414481486; //TOFN
    public static final int ORIGINAL_LYRICIST                           = 1414483033; //TOLY
    public static final int ORIGINAL_RELEASE_TIME                       = 1413762898; //TDOR
    public static final int OWNERSHIP_FRAME                             = 1331121733; //OWNE
    public static final int PART_OF_A_SET                               = 1414549331; //TPOS
    public static final int PAYMENT                                     = 1464877401; //WPAY
    public static final int PERFORMER_SORT_ORDER                        = 1414745936; //TSOP
    public static final int PLAYLIST_DELAY                              = 1413762137; //TDLY
    public static final int PLAY_COUNTER                                = 1346588244; //PCNT
    public static final int POPULARIMETER                               = 1347375181; //POPM
    public static final int POSITION_SYNCHRONISATION_FRAME              = 1347375955; //POSS
    public static final int PRIVATE_FRAME                               = 1347570006; //PRIV
    public static final int PRODUCED_NOTICE                             = 1414550095; //TPRO
    public static final int PUBLISHER                                   = 1414550850; //TPUB
    public static final int PUBLISHERS_OFFICIAL_WEBPAGE                 = 1464882498; //WPUB
    public static final int RECOMMENDED_BUFFER_SIZE                     = 1380078918; //RBUF
    public static final int RECORDING_TIME                              = 1413763651; //TDRC
    public static final int RELATIVE_VOLUME_ADJUSTMENT                  = 1381384498; //RVA2
    public static final int RELEASE_TIME                                = 1413763660; //TDRL
    public static final int REVERB                                      = 1381388866; //RVRB
    public static final int SEEK_FRAME                                  = 1397048651; //SEEK
    public static final int SET_SUBTITLE                                = 1414746964; //TSST
    public static final int SIGNATURE_FRAME                             = 1397311310; //SIGN
    public static final int ENCODING_TOOLS_AND_SETTINGS                 = 1414746949; //TSSE
    public static final int SUBTITLE                                    = 1414091827; //TIT3
    public static final int SYNCHRONISED_LYRIC                          = 1398361172; //SYLT
    public static final int SYNCHRONISED_TEMPO_CODES                    = 1398363203; //SYTC
    public static final int TAGGING_TIME                                = 1413764167; //TDTG
    public static final int TERMS_OF_USE                                = 1431520594; //USER
    public static final int TITLE                                       = 1414091826; //TIT2
    public static final int TITLE_SORT_ORDER                            = 1414745940; //TSOT
    public static final int TRACK_NUMBER                                = 1414677323; //TRCK
    public static final int UNIQUE_FILE_IDENTIFIER                      = 1430669636; //UFID
    public static final int UNSYNCHRONISED_LYRIC                        = 1431522388; //USLT
    public static final int USER_DEFINED_TEXT_INFORMATION_FRAME         = 1415075928; //TXXX
    public static final int USER_DEFINED_URL_LINK_FRAME                 = 1465407576; //WXXX
    private static final String[] TEXT_ENCODINGS = {"ISO-8859-1", "UTF-16"/*BOM*/, "UTF-16", "UTF-8"};
    private static final String[] VALID_TIMESTAMPS = {"yyyy, yyyy-MM", "yyyy-MM-dd", "yyyy-MM-ddTHH", "yyyy-MM-ddTHH:mm", "yyyy-MM-ddTHH:mm:ss"};
    private static final String UNKNOWN_LANGUAGE = "xxx";

    private final long size;
    private final int id;
    private final int flags;
    private int groupID;
    private int encryptionMethod;
    private final byte[] data;

    ID3Frame(DataInputStream in) throws IOException {
        id = in.readInt();
        size = ID3Tag.readSynch(in);
        flags = in.readShort();

        if (isInGroup()) groupID = in.read();
        if (isEncrypted()) encryptionMethod = in.read();
        //TODO: data length indicator, unsync

        data = new byte[(int) size];
        in.readFully(data);
    }

    //header data
    public int getID() {
        return id;
    }

    public long getSize() {
        return size;
    }

    public final boolean isInGroup() {
        return (flags&0x40)==0x40;
    }

    public int getGroupID() {
        return groupID;
    }

    public final boolean isCompressed() {
        return (flags&8)==8;
    }

    public final boolean isEncrypted() {
        return (flags&4)==4;
    }

    public int getEncryptionMethod() {
        return encryptionMethod;
    }

    //content data
    public byte[] getData() {
        return data;
    }

    public String getText() {
        return new String(data, Charset.forName(TEXT_ENCODINGS[0]));
    }

    public String getEncodedText() {
        //first byte indicates encoding
        final int enc = data[0];

        //charsets 0,3 end with '0'; 1,2 end with '00'
        int t = -1;
        for (int i = 1; i<data.length&&t<0; i++) {
            if (data[i]==0&&(enc==0||enc==3||data[i+1]==0)) t = i;
        }
        return new String(data, 1, t-1, Charset.forName(TEXT_ENCODINGS[enc]));
    }

    public int getNumber() {
        return Integer.parseInt(new String(data));
    }

    public int[] getNumbers() {
        //multiple numbers separated by '/'
        final String x = new String(data, Charset.forName(TEXT_ENCODINGS[0]));
        final int i = x.indexOf('/');
        final int[] y;
        if (i>0) y = new int[]{Integer.parseInt(x.substring(0, i)), Integer.parseInt(x.substring(i+1))};
        else y = new int[]{Integer.parseInt(x)};
        return y;
    }

    public Date getDate() {
        //timestamp lengths: 4,7,10,13,16,19
        final int i = (int) Math.floor(data.length/3)-1;
        final Date date;
        if (i>=0&&i<VALID_TIMESTAMPS.length) {
            final SimpleDateFormat sdf = new SimpleDateFormat(VALID_TIMESTAMPS[i]);
            date = sdf.parse(new String(data), new ParsePosition(0));
        }
        else date = null;
        return date;
    }

    public Locale getLocale() {
        final String s = new String(data).toLowerCase();
        final Locale l;
        if (s.equals(UNKNOWN_LANGUAGE)) l = null;
        else l = new Locale(s);
        return l;
    }
}
