package science.unlicense.format.mpeg4.api;

import java.util.Date;

/**
 *
 * @author Alexander Simm
 */
final class Utils {

    private static final long DATE_OFFSET = 2082850791998l;

    static Date getDate(long time) {
        return new Date(time*1000-DATE_OFFSET);
    }

    private Utils(){}
}
