
package science.unlicense.format.mpeg4.boxes.impl.sampleentries;

import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4Constants;

/**
 *
 * @author Alexander Simm
 */
public class XMLMetadataSampleEntry extends MetadataSampleEntry {

    private String namespace, schemaLocation;

    public XMLMetadataSampleEntry() {
        super("XML Metadata Sample Entry");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        namespace = in.readUTFString((int) getLeft(in), MP4Constants.UTF8);
        schemaLocation = in.readUTFString((int) getLeft(in), MP4Constants.UTF8);
    }

    /**
     * Gives the namespace of the schema for the timed XML metadata. This is
     * needed for identifying the type of metadata, e.g. gBSD or AQoS
     * (MPEG-21-7) and for decoding using XML aware encoding mechanisms such as
     * BiM.
     * @return the namespace
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Optionally provides an URL to find the schema corresponding to the
     * namespace. This is needed for decoding of the timed metadata by XML aware
     * encoding mechanisms such as BiM.
     * @return the schema's URL
     */
    public String getSchemaLocation() {
        return schemaLocation;
    }
}
