package science.unlicense.format.mpeg4.boxes.impl.drm;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.Box;

/**
 *
 * @author Alexander Simm
 */
public class FairPlayDataBox extends Box {

    private byte[] data;

    public FairPlayDataBox() {
        super("iTunes FairPlay Data Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        data = new byte[(int) getLeft(in)];
        in.readBytes(data);
    }

    public byte[] getData() {
        return data;
    }
}
