package science.unlicense.format.mpeg4.boxes.impl;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4Constants;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.FullBox;

/**
 * The Copyright box contains a copyright declaration which applies to the
 * entire presentation, when contained within the Movie Box, or, when contained
 * in a track, to that entire track. There may be multiple copyright boxes using
 * different language codes.
 *
 * @author Alexander Simm
 */
public class CopyrightBox extends FullBox {

    private String languageCode, notice;

    public CopyrightBox() {
        super("Copyright Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        if (parent.getType()==MP4Constants.BOX_USER_DATA) {
            super.decode(in);
            //1 bit padding, 5*3 bits language code (ISO-639-2/T)
            languageCode = MP4Constants.getLanguageCode(in.readBytes(2));

            notice = in.readUTFString((int) getLeft(in));
        }
        else if (parent.getType()==MP4Constants.BOX_ITUNES_META_LIST) readChildren(in);
    }

    /**
     * The language code for the following text. See ISO 639-2/T for the set of
     * three character codes.
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * The copyright notice.
     */
    public String getNotice() {
        return notice;
    }
}
