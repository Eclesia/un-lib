package science.unlicense.format.mpeg4.boxes.impl.meta;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4Constants;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.FullBox;

//TODO: use nio ByteBuffer instead of array
/**
 *
 * @author Alexander Simm
 */
public class ID3TagBox extends FullBox {

    private String language;
    private byte[] id3Data;

    public ID3TagBox() {
        super("ID3 Tag Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        language = MP4Constants.getLanguageCode(in.readBytes(2));

        id3Data = new byte[(int) getLeft(in)];
        in.readBytes(id3Data);
    }

    public byte[] getID3Data() {
        return id3Data;
    }

    /**
     * The language code for the following text. See ISO 639-2/T for the set of
     * three character codes.
     */
    public String getLanguage() {
        return language;
    }
}
