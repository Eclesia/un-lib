
package science.unlicense.format.mpeg4;

import java.nio.charset.Charset;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.boxes.Box;
import science.unlicense.format.mpeg4.boxes.FullBox;
import science.unlicense.format.mpeg4.boxes.UnknownBox;
import science.unlicense.format.mpeg4.boxes.impl.AppleLosslessBox;
import science.unlicense.format.mpeg4.boxes.impl.BinaryXMLBox;
import science.unlicense.format.mpeg4.boxes.impl.BitRateBox;
import science.unlicense.format.mpeg4.boxes.impl.ChapterBox;
import science.unlicense.format.mpeg4.boxes.impl.ChunkOffsetBox;
import science.unlicense.format.mpeg4.boxes.impl.CleanApertureBox;
import science.unlicense.format.mpeg4.boxes.impl.CompositionTimeToSampleBox;
import science.unlicense.format.mpeg4.boxes.impl.CopyrightBox;
import science.unlicense.format.mpeg4.boxes.impl.DataEntryUrlBox;
import science.unlicense.format.mpeg4.boxes.impl.DataEntryUrnBox;
import science.unlicense.format.mpeg4.boxes.impl.DataReferenceBox;
import science.unlicense.format.mpeg4.boxes.impl.DecodingTimeToSampleBox;
import science.unlicense.format.mpeg4.boxes.impl.DegradationPriorityBox;
import science.unlicense.format.mpeg4.boxes.impl.ESDBox;
import science.unlicense.format.mpeg4.boxes.impl.EditListBox;
import science.unlicense.format.mpeg4.boxes.impl.FileTypeBox;
import science.unlicense.format.mpeg4.boxes.impl.FreeSpaceBox;
import science.unlicense.format.mpeg4.boxes.impl.HandlerBox;
import science.unlicense.format.mpeg4.boxes.impl.HintMediaHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.IPMPControlBox;
import science.unlicense.format.mpeg4.boxes.impl.IPMPInfoBox;
import science.unlicense.format.mpeg4.boxes.impl.ItemInformationBox;
import science.unlicense.format.mpeg4.boxes.impl.ItemInformationEntry;
import science.unlicense.format.mpeg4.boxes.impl.ItemLocationBox;
import science.unlicense.format.mpeg4.boxes.impl.ItemProtectionBox;
import science.unlicense.format.mpeg4.boxes.impl.MediaDataBox;
import science.unlicense.format.mpeg4.boxes.impl.MediaHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.MetaBox;
import science.unlicense.format.mpeg4.boxes.impl.MetaBoxRelationBox;
import science.unlicense.format.mpeg4.boxes.impl.MovieExtendsHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.MovieFragmentHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.MovieFragmentRandomAccessOffsetBox;
import science.unlicense.format.mpeg4.boxes.impl.MovieHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.ObjectDescriptorBox;
import science.unlicense.format.mpeg4.boxes.impl.OriginalFormatBox;
import science.unlicense.format.mpeg4.boxes.impl.PaddingBitBox;
import science.unlicense.format.mpeg4.boxes.impl.PixelAspectRatioBox;
import science.unlicense.format.mpeg4.boxes.impl.PrimaryItemBox;
import science.unlicense.format.mpeg4.boxes.impl.ProgressiveDownloadInformationBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleDependencyBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleDependencyTypeBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleDescriptionBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleGroupDescriptionBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleScaleBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleSizeBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleToChunkBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleToGroupBox;
import science.unlicense.format.mpeg4.boxes.impl.SchemeTypeBox;
import science.unlicense.format.mpeg4.boxes.impl.ShadowSyncSampleBox;
import science.unlicense.format.mpeg4.boxes.impl.SoundMediaHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.SubSampleInformationBox;
import science.unlicense.format.mpeg4.boxes.impl.SyncSampleBox;
import science.unlicense.format.mpeg4.boxes.impl.TrackExtendsBox;
import science.unlicense.format.mpeg4.boxes.impl.TrackFragmentHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.TrackFragmentRandomAccessBox;
import science.unlicense.format.mpeg4.boxes.impl.TrackFragmentRunBox;
import science.unlicense.format.mpeg4.boxes.impl.TrackHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.TrackReferenceBox;
import science.unlicense.format.mpeg4.boxes.impl.TrackSelectionBox;
import science.unlicense.format.mpeg4.boxes.impl.VideoMediaHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.XMLBox;
import science.unlicense.format.mpeg4.boxes.impl.drm.FairPlayDataBox;
import science.unlicense.format.mpeg4.boxes.impl.fd.FDItemInformationBox;
import science.unlicense.format.mpeg4.boxes.impl.fd.FDSessionGroupBox;
import science.unlicense.format.mpeg4.boxes.impl.fd.FECReservoirBox;
import science.unlicense.format.mpeg4.boxes.impl.fd.FilePartitionBox;
import science.unlicense.format.mpeg4.boxes.impl.fd.GroupIDToNameBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.EncoderBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.GenreBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.ID3TagBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.ITunesMetadataBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.ITunesMetadataMeanBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.ITunesMetadataNameBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.NeroMetadataTagsBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.RatingBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.RequirementBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.ThreeGPPAlbumBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.ThreeGPPKeywordsBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.ThreeGPPLocationBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.ThreeGPPMetadataBox;
import science.unlicense.format.mpeg4.boxes.impl.meta.ThreeGPPRecordingYearBox;
import science.unlicense.format.mpeg4.boxes.impl.oma.OMAAccessUnitFormatBox;
import science.unlicense.format.mpeg4.boxes.impl.oma.OMACommonHeadersBox;
import science.unlicense.format.mpeg4.boxes.impl.oma.OMAContentIDBox;
import science.unlicense.format.mpeg4.boxes.impl.oma.OMAContentObjectBox;
import science.unlicense.format.mpeg4.boxes.impl.oma.OMADiscreteMediaHeadersBox;
import science.unlicense.format.mpeg4.boxes.impl.oma.OMARightsObjectBox;
import science.unlicense.format.mpeg4.boxes.impl.oma.OMATransactionTrackingBox;
import science.unlicense.format.mpeg4.boxes.impl.oma.OMAURLBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.AudioSampleEntry;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.FDHintSampleEntry;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.MPEGSampleEntry;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.RTPHintSampleEntry;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.TextMetadataSampleEntry;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.VideoSampleEntry;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.XMLMetadataSampleEntry;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.AC3SpecificBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.AMRSpecificBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.AVCSpecificBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.EAC3SpecificBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.EVRCSpecificBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.H263SpecificBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.QCELPSpecificBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.SMVSpecificBox;

/**
 *
 * @author Johann Sorel
 * @author Alexander Simm
 */
public final class MP4Constants {

    public static final Charset UTF8 = Charset.forName("UTF-8");
    public static final Charset UTF16 = Charset.forName("UTF-16");
    public static final Logger LOGGER = Loggers.getLogger(new Chars("MP4 API"));

    private static final long UNDETERMINED = 4294967295l;

    //list of MP4 box types
    public static final long BOX_EXTENDED_TYPE = 1970628964; //uuid
    //standard boxes (ISO BMFF)
    public static final long BOX_ADDITIONAL_METADATA_CONTAINER = 1835361135l; //meco
    public static final long BOX_APPLE_LOSSLESS = 1634492771l; //alac
    public static final long BOX_BINARY_XML = 1652059500l; //bxml
    public static final long BOX_BIT_RATE = 1651798644l; //btrt
    public static final long BOX_CHAPTER = 1667788908l; //chpl
    public static final long BOX_CHUNK_OFFSET = 1937007471l; //stco
    public static final long BOX_CHUNK_LARGE_OFFSET = 1668232756l; //co64
    public static final long BOX_CLEAN_APERTURE = 1668047216l; //clap
    public static final long BOX_COMPACT_SAMPLE_SIZE = 1937013298l; //stz2
    public static final long BOX_COMPOSITION_TIME_TO_SAMPLE = 1668576371l; //ctts
    public static final long BOX_COPYRIGHT = 1668313716l; //cprt
    public static final long BOX_DATA_ENTRY_URN = 1970433568l; //urn
    public static final long BOX_DATA_ENTRY_URL = 1970433056l; //url
    public static final long BOX_DATA_INFORMATION = 1684631142l; //dinf
    public static final long BOX_DATA_REFERENCE = 1685218662l; //dref
    public static final long BOX_DECODING_TIME_TO_SAMPLE = 1937011827l; //stts
    public static final long BOX_DEGRADATION_PRIORITY = 1937007728l; //stdp
    public static final long BOX_EDIT = 1701082227l; //edts
    public static final long BOX_EDIT_LIST = 1701606260l; //elst
    public static final long BOX_FD_ITEM_INFORMATION = 1718184302l; //fiin
    public static final long BOX_FD_SESSION_GROUP = 1936025458l; //segr
    public static final long BOX_FEC_RESERVOIR = 1717920626l; //fecr
    public static final long BOX_FILE_PARTITION = 1718641010l; //fpar
    public static final long BOX_FILE_TYPE = 1718909296l; //ftyp
    public static final long BOX_FREE_SPACE = 1718773093l; //free
    public static final long BOX_GROUP_ID_TO_NAME = 1734964334l; //gitn
    public static final long BOX_HANDLER = 1751411826l; //hdlr
    public static final long BOX_HINT_MEDIA_HEADER = 1752000612l; //hmhd
    public static final long BOX_IPMP_CONTROL = 1768975715l; //ipmc
    public static final long BOX_IPMP_INFO = 1768778086l; //imif
    public static final long BOX_ITEM_INFORMATION = 1768517222l; //iinf
    public static final long BOX_ITEM_INFORMATION_ENTRY = 1768842853l; //infe
    public static final long BOX_ITEM_LOCATION = 1768714083l; //iloc
    public static final long BOX_ITEM_PROTECTION = 1768977007l; //ipro
    public static final long BOX_MEDIA = 1835297121l; //mdia
    public static final long BOX_MEDIA_DATA = 1835295092l; //mdat
    public static final long BOX_MEDIA_HEADER = 1835296868l; //mdhd
    public static final long BOX_MEDIA_INFORMATION = 1835626086l; //minf
    public static final long BOX_META = 1835365473l; //meta
    public static final long BOX_META_RELATION = 1835364965l; //mere
    public static final long BOX_MOVIE = 1836019574l; //moov
    public static final long BOX_MOVIE_EXTENDS = 1836475768l; //mvex
    public static final long BOX_MOVIE_EXTENDS_HEADER = 1835362404l; //mehd
    public static final long BOX_MOVIE_FRAGMENT = 1836019558l; //moof
    public static final long BOX_MOVIE_FRAGMENT_HEADER = 1835427940l; //mfhd
    public static final long BOX_MOVIE_FRAGMENT_RANDOM_ACCESS = 1835430497l; //mfra
    public static final long BOX_MOVIE_FRAGMENT_RANDOM_ACCESS_OFFSET = 1835430511l; //mfro
    public static final long BOX_MOVIE_HEADER = 1836476516l; //mvhd
    public static final long BOX_NERO_METADATA_TAGS = 1952540531l; //tags
    public static final long BOX_NULL_MEDIA_HEADER = 1852663908l; //nmhd
    public static final long BOX_ORIGINAL_FORMAT = 1718775137l; //frma
    public static final long BOX_PADDING_BIT = 1885430882l; //padb
    public static final long BOX_PARTITION_ENTRY = 1885431150l; //paen
    public static final long BOX_PIXEL_ASPECT_RATIO = 1885434736l; //pasp
    public static final long BOX_PRIMARY_ITEM = 1885959277l; //pitm
    public static final long BOX_PROGRESSIVE_DOWNLOAD_INFORMATION = 1885628782l; //pdin
    public static final long BOX_PROTECTION_SCHEME_INFORMATION = 1936289382l; //sinf
    public static final long BOX_SAMPLE_DEPENDENCY_TYPE = 1935963248l; //sdtp
    public static final long BOX_SAMPLE_DESCRIPTION = 1937011556l; //stsd
    public static final long BOX_SAMPLE_GROUP_DESCRIPTION = 1936158820l; //sgpd
    public static final long BOX_SAMPLE_SCALE = 1937011564l; //stsl
    public static final long BOX_SAMPLE_SIZE = 1937011578l; //stsz
    public static final long BOX_SAMPLE_TABLE = 1937007212l; //stbl
    public static final long BOX_SAMPLE_TO_CHUNK = 1937011555l; //stsc
    public static final long BOX_SAMPLE_TO_GROUP = 1935828848l; //sbgp
    public static final long BOX_SCHEME_TYPE = 1935894637l; //schm
    public static final long BOX_SCHEME_INFORMATION = 1935894633l; //schi
    public static final long BOX_SHADOW_SYNC_SAMPLE = 1937011560l; //stsh
    public static final long BOX_SKIP = 1936419184l; //skip
    public static final long BOX_SOUND_MEDIA_HEADER = 1936549988l; //smhd
    public static final long BOX_SUB_SAMPLE_INFORMATION = 1937072755l; //subs
    public static final long BOX_SYNC_SAMPLE = 1937011571l; //stss
    public static final long BOX_TRACK = 1953653099l; //trak
    public static final long BOX_TRACK_EXTENDS = 1953654136l; //trex
    public static final long BOX_TRACK_FRAGMENT = 1953653094l; //traf
    public static final long BOX_TRACK_FRAGMENT_HEADER = 1952868452l; //tfhd
    public static final long BOX_TRACK_FRAGMENT_RANDOM_ACCESS = 1952871009l; //tfra
    public static final long BOX_TRACK_FRAGMENT_RUN = 1953658222l; //trun
    public static final long BOX_TRACK_HEADER = 1953196132l; //tkhd
    public static final long BOX_TRACK_REFERENCE = 1953654118l; //tref
    public static final long BOX_TRACK_SELECTION = 1953719660l; //tsel
    public static final long BOX_USER_DATA = 1969517665l; //udta
    public static final long BOX_VIDEO_MEDIA_HEADER = 1986881636l; //vmhd
    public static final long BOX_WIDE = 2003395685l; //wide
    public static final long BOX_XML = 2020437024l; //xml
    //mp4 extension
    public static final long BOX_OBJECT_DESCRIPTOR = 1768907891l; //iods
    public static final long BOX_SAMPLE_DEPENDENCY = 1935959408l; //sdep
    //metadata: id3
    public static final long BOX_ID3_TAG = 1768174386l; //id32
    //metadata: itunes
    public static final long BOX_ITUNES_META_LIST = 1768715124l; //ilst
    public static final long BOX_CUSTOM_ITUNES_METADATA = 757935405l; //----
    public static final long BOX_ITUNES_METADATA = 1684108385l; //data
    public static final long BOX_ITUNES_METADATA_NAME = 1851878757l; //name
    public static final long BOX_ITUNES_METADATA_MEAN = 1835360622l; //mean
    public static final long BOX_ALBUM_ARTIST_NAME = 1631670868l; //aART
    public static final long BOX_ALBUM_ARTIST_SORT = 1936679265l; //soaa
    public static final long BOX_ALBUM_NAME = 2841734242l; //©alb
    public static final long BOX_ALBUM_SORT = 1936679276l; //soal
    public static final long BOX_ARTIST_NAME = 2839630420l; //©ART
    public static final long BOX_ARTIST_SORT = 1936679282l; //soar
    public static final long BOX_CATEGORY = 1667331175l; //catg
    public static final long BOX_COMMENTS = 2841865588l; //©cmt
    public static final long BOX_COMPILATION_PART = 1668311404l; //cpil
    public static final long BOX_COMPOSER_NAME = 2843177588l; //©wrt
    public static final long BOX_COMPOSER_SORT = 1936679791l; //soco
    public static final long BOX_COVER = 1668249202l; //covr
    public static final long BOX_CUSTOM_GENRE = 2842125678l; //©gen
    public static final long BOX_DESCRIPTION = 1684370275l; //desc
    public static final long BOX_DISK_NUMBER = 1684632427l; //disk
    public static final long BOX_ENCODER_NAME = 2841996899l; //©enc
    public static final long BOX_ENCODER_TOOL = 2842980207l; //©too
    public static final long BOX_EPISODE_GLOBAL_UNIQUE_ID = 1701276004l; //egid
    public static final long BOX_GAPLESS_PLAYBACK = 1885823344l; //pgap
    public static final long BOX_GENRE = 1735291493l; //gnre
    public static final long BOX_GROUPING = 2842129008l; //©grp
    public static final long BOX_HD_VIDEO = 1751414372l; //hdvd
    public static final long BOX_ITUNES_PURCHASE_ACCOUNT = 1634748740l; //apID
    public static final long BOX_ITUNES_ACCOUNT_TYPE = 1634421060l; //akID
    public static final long BOX_ITUNES_CATALOGUE_ID = 1668172100l; //cnID
    public static final long BOX_ITUNES_COUNTRY_CODE = 1936083268l; //sfID
    public static final long BOX_KEYWORD = 1801812343l; //keyw
    public static final long BOX_LONG_DESCRIPTION = 1818518899l; //ldes
    public static final long BOX_LYRICS = 2842458482l; //©lyr
    public static final long BOX_META_TYPE = 1937009003l; //stik
    public static final long BOX_PODCAST = 1885565812l; //pcst
    public static final long BOX_PODCAST_URL = 1886745196l; //purl
    public static final long BOX_PURCHASE_DATE = 1886745188l; //purd
    public static final long BOX_RATING = 1920233063l; //rtng
    public static final long BOX_RELEASE_DATE = 2841928057l; //©day
    public static final long BOX_REQUIREMENT = 2842846577l; //©req
    public static final long BOX_TEMPO = 1953329263l; //tmpo
    public static final long BOX_TRACK_NAME = 2842583405l; //©nam
    public static final long BOX_TRACK_NUMBER = 1953655662l; //trkn
    public static final long BOX_TRACK_SORT = 1936682605l; //sonm
    public static final long BOX_TV_EPISODE = 1953916275l; //tves
    public static final long BOX_TV_EPISODE_NUMBER = 1953916270l; //tven
    public static final long BOX_TV_NETWORK_NAME = 1953918574l; //tvnn
    public static final long BOX_TV_SEASON = 1953919854l; //tvsn
    public static final long BOX_TV_SHOW = 1953919848l; //tvsh
    public static final long BOX_TV_SHOW_SORT = 1936683886l; //sosn
    //metadata: 3gpp
    public static final long BOX_THREE_GPP_ALBUM = 1634493037l; //albm
    public static final long BOX_THREE_GPP_AUTHOR = 1635087464l; //auth
    public static final long BOX_THREE_GPP_CLASSIFICATION = 1668051814l; //clsf
    public static final long BOX_THREE_GPP_DESCRIPTION = 1685283696l; //dscp
    public static final long BOX_THREE_GPP_KEYWORDS = 1803122532l; //kywd
    public static final long BOX_THREE_GPP_LOCATION_INFORMATION = 1819239273l; //loci
    public static final long BOX_THREE_GPP_PERFORMER = 1885696614l; //perf
    public static final long BOX_THREE_GPP_RECORDING_YEAR = 2037543523l; //yrrc
    public static final long BOX_THREE_GPP_TITLE = 1953068140l; //titl
    //metadata: google/youtube
    public static final long BOX_GOOGLE_HOST_HEADER = 1735616616l; //gshh
    public static final long BOX_GOOGLE_PING_MESSAGE = 1735618669l; //gspm
    public static final long BOX_GOOGLE_PING_URL = 1735618677l; //gspu
    public static final long BOX_GOOGLE_SOURCE_DATA = 1735619428l; //gssd
    public static final long BOX_GOOGLE_START_TIME = 1735619444l; //gsst
    public static final long BOX_GOOGLE_TRACK_DURATION = 1735619684l; //gstd
    //sample entries
    public static final long BOX_MP4V_SAMPLE_ENTRY = 1836070006l; //mp4v
    public static final long BOX_H263_SAMPLE_ENTRY = 1932670515l; //s263
    public static final long BOX_ENCRYPTED_VIDEO_SAMPLE_ENTRY = 1701733238l; //encv
    public static final long BOX_AVC_SAMPLE_ENTRY = 1635148593l; //avc1
    public static final long BOX_MP4A_SAMPLE_ENTRY = 1836069985l; //mp4a
    public static final long BOX_AC3_SAMPLE_ENTRY = 1633889587l; //ac-3
    public static final long BOX_EAC3_SAMPLE_ENTRY = 1700998451l; //ec-3
    public static final long BOX_DRMS_SAMPLE_ENTRY = 1685220723l; //drms
    public static final long BOX_AMR_SAMPLE_ENTRY = 1935764850l; //samr
    public static final long BOX_AMR_WB_SAMPLE_ENTRY = 1935767394l; //sawb
    public static final long BOX_EVRC_SAMPLE_ENTRY = 1936029283l; //sevc
    public static final long BOX_QCELP_SAMPLE_ENTRY = 1936810864l; //sqcp
    public static final long BOX_SMV_SAMPLE_ENTRY = 1936944502l; //ssmv
    public static final long BOX_ENCRYPTED_AUDIO_SAMPLE_ENTRY = 1701733217l; //enca
    public static final long BOX_MPEG_SAMPLE_ENTRY = 1836070003l; //mp4s
    public static final long BOX_TEXT_METADATA_SAMPLE_ENTRY = 1835365492l; //mett
    public static final long BOX_XML_METADATA_SAMPLE_ENTRY = 1835365496l; //metx
    public static final long BOX_RTP_HINT_SAMPLE_ENTRY = 1920233504l; //rtp
    public static final long BOX_FD_HINT_SAMPLE_ENTRY = 1717858336l; //fdp
    //codec infos
    public static final long BOX_ESD = 1702061171l; //esds
    //video codecs
    public static final long BOX_H263_SPECIFIC = 1681012275l; //d263
    public static final long BOX_AVC_SPECIFIC = 1635148611l; //avcC
    //audio codecs
    public static final long BOX_AC3_SPECIFIC = 1684103987l; //dac3
    public static final long BOX_EAC3_SPECIFIC = 1684366131l; //dec3
    public static final long BOX_AMR_SPECIFIC = 1684106610l; //damr
    public static final long BOX_EVRC_SPECIFIC = 1684371043l; //devc
    public static final long BOX_QCELP_SPECIFIC = 1685152624l; //dqcp
    public static final long BOX_SMV_SPECIFIC = 1685286262l; //dsmv
    //OMA DRM
    public static final long BOX_OMA_ACCESS_UNIT_FORMAT = 1868849510l; //odaf
    public static final long BOX_OMA_COMMON_HEADERS = 1869112434l; //ohdr
    public static final long BOX_OMA_CONTENT_ID = 1667459428l; //ccid
    public static final long BOX_OMA_CONTENT_OBJECT = 1868850273l; //odda
    public static final long BOX_OMA_COVER_URI = 1668706933l; //cvru
    public static final long BOX_OMA_DISCRETE_MEDIA_HEADERS = 1868851301l; //odhe
    public static final long BOX_OMA_DRM_CONTAINER = 1868853869l; //odrm
    public static final long BOX_OMA_ICON_URI = 1768124021l; //icnu
    public static final long BOX_OMA_INFO_URL = 1768842869l; //infu
    public static final long BOX_OMA_LYRICS_URI = 1819435893l; //lrcu
    public static final long BOX_OMA_MUTABLE_DRM_INFORMATION = 1835299433l; //mdri
    public static final long BOX_OMA_KEY_MANAGEMENT = 1868852077l; //odkm
    public static final long BOX_OMA_RIGHTS_OBJECT = 1868853858l; //odrb
    public static final long BOX_OMA_TRANSACTION_TRACKING = 1868854388l; //odtt
    //iTunes DRM (FairPlay)
    public static final long BOX_FAIRPLAY_USER_ID = 1970496882l; //user
    public static final long BOX_FAIRPLAY_USER_NAME = 1851878757l; //name
    public static final long BOX_FAIRPLAY_USER_KEY = 1801812256l; //key
    public static final long BOX_FAIRPLAY_IV = 1769367926l; //iviv
    public static final long BOX_FAIRPLAY_PRIVATE_KEY = 1886546294l; //priv


    public static final Chars BOX_FTYP = Chars.constant(new byte[]{'f','t','y','p'});
    public static final Chars BOX_MOOV = Chars.constant(new byte[]{'m','o','o','v'});
        public static final Chars BOX_MVHD = Chars.constant(new byte[]{'m','v','h','d'});
        public static final Chars BOX_TRAK = Chars.constant(new byte[]{'t','r','a','k'});
            public static final Chars BOX_TKHD = Chars.constant(new byte[]{'t','k','h','d'});
            public static final Chars BOX_MDIA = Chars.constant(new byte[]{'m','d','i','a'});
                public static final Chars BOX_MDHD = Chars.constant(new byte[]{'m','d','h','d'});
                public static final Chars BOX_MINF = Chars.constant(new byte[]{'m','i','n','f'});
                    public static final Chars BOX_VMHD = Chars.constant(new byte[]{'v','m','h','d'});
                    public static final Chars BOX_DINF = Chars.constant(new byte[]{'d','i','n','f'});
                        public static final Chars BOX_DREF = Chars.constant(new byte[]{'d','r','e','f'});
                    public static final Chars BOX_STBL = Chars.constant(new byte[]{'s','t','b','l'});
                        public static final Chars BOX_STSD = Chars.constant(new byte[]{'s','t','s','d'});
                        public static final Chars BOX_STTS = Chars.constant(new byte[]{'s','t','t','s'});
                        public static final Chars BOX_CTTS = Chars.constant(new byte[]{'c','t','t','s'});
                        public static final Chars BOX_STSS = Chars.constant(new byte[]{'s','t','s','s'});
                        public static final Chars BOX_STSC = Chars.constant(new byte[]{'s','t','s','c'});
                        public static final Chars BOX_STSZ = Chars.constant(new byte[]{'s','t','s','z'});
                        public static final Chars BOX_STCO = Chars.constant(new byte[]{'s','t','c','o'});
                public static final Chars BOX_HDLR = Chars.constant(new byte[]{'h','d','l','r'});
            public static final Chars BOX_TREF = Chars.constant(new byte[]{'t','r','e','f'});
        public static final Chars BOX_IODS = Chars.constant(new byte[]{'i','o','d','s'});
    public static final Chars BOX_MDAT = Chars.constant(new byte[]{'m','d','a','t'});
    public static final Chars BOX_FREE = Chars.constant(new byte[]{'f','r','e','e'});



    //special boxes
    private static final Dictionary BOXES = new HashDictionary();
    private static final Dictionary FULLNAME = new HashDictionary();
    private static final Set GROUPBOXES = new HashSet();
    static {
        //classes
        BOXES.add(BOX_ADDITIONAL_METADATA_CONTAINER, Box.class);
        BOXES.add(BOX_APPLE_LOSSLESS, AppleLosslessBox.class);
        BOXES.add(BOX_BINARY_XML, BinaryXMLBox.class);
        BOXES.add(BOX_BIT_RATE, BitRateBox.class);
        BOXES.add(BOX_CHAPTER, ChapterBox.class);
        BOXES.add(BOX_CHUNK_OFFSET, ChunkOffsetBox.class);
        BOXES.add(BOX_CHUNK_LARGE_OFFSET, ChunkOffsetBox.class);
        BOXES.add(BOX_CLEAN_APERTURE, CleanApertureBox.class);
        BOXES.add(BOX_COMPACT_SAMPLE_SIZE, SampleSizeBox.class);
        BOXES.add(BOX_COMPOSITION_TIME_TO_SAMPLE, CompositionTimeToSampleBox.class);
        BOXES.add(BOX_COPYRIGHT, CopyrightBox.class);
        BOXES.add(BOX_DATA_ENTRY_URN, DataEntryUrnBox.class);
        BOXES.add(BOX_DATA_ENTRY_URL, DataEntryUrlBox.class);
        BOXES.add(BOX_DATA_INFORMATION, Box.class);
        BOXES.add(BOX_DATA_REFERENCE, DataReferenceBox.class);
        BOXES.add(BOX_DECODING_TIME_TO_SAMPLE, DecodingTimeToSampleBox.class);
        BOXES.add(BOX_DEGRADATION_PRIORITY, DegradationPriorityBox.class);
        BOXES.add(BOX_EDIT, Box.class);
        BOXES.add(BOX_EDIT_LIST, EditListBox.class);
        BOXES.add(BOX_FD_ITEM_INFORMATION, FDItemInformationBox.class);
        BOXES.add(BOX_FD_SESSION_GROUP, FDSessionGroupBox.class);
        BOXES.add(BOX_FEC_RESERVOIR, FECReservoirBox.class);
        BOXES.add(BOX_FILE_PARTITION, FilePartitionBox.class);
        BOXES.add(BOX_FILE_TYPE, FileTypeBox.class);
        BOXES.add(BOX_FREE_SPACE, FreeSpaceBox.class);
        BOXES.add(BOX_GROUP_ID_TO_NAME, GroupIDToNameBox.class);
        BOXES.add(BOX_HANDLER, HandlerBox.class);
        BOXES.add(BOX_HINT_MEDIA_HEADER, HintMediaHeaderBox.class);
        BOXES.add(BOX_IPMP_CONTROL, IPMPControlBox.class);
        BOXES.add(BOX_IPMP_INFO, IPMPInfoBox.class);
        BOXES.add(BOX_ITEM_INFORMATION, ItemInformationBox.class);
        BOXES.add(BOX_ITEM_INFORMATION_ENTRY, ItemInformationEntry.class);
        BOXES.add(BOX_ITEM_LOCATION, ItemLocationBox.class);
        BOXES.add(BOX_ITEM_PROTECTION, ItemProtectionBox.class);
        BOXES.add(BOX_MEDIA, Box.class);
        BOXES.add(BOX_MEDIA_DATA, MediaDataBox.class);
        BOXES.add(BOX_MEDIA_HEADER, MediaHeaderBox.class);
        BOXES.add(BOX_MEDIA_INFORMATION, Box.class);
        BOXES.add(BOX_META, MetaBox.class);
        BOXES.add(BOX_META_RELATION, MetaBoxRelationBox.class);
        BOXES.add(BOX_MOVIE, Box.class);
        BOXES.add(BOX_MOVIE_EXTENDS, Box.class);
        BOXES.add(BOX_MOVIE_EXTENDS_HEADER, MovieExtendsHeaderBox.class);
        BOXES.add(BOX_MOVIE_FRAGMENT, Box.class);
        BOXES.add(BOX_MOVIE_FRAGMENT_HEADER, MovieFragmentHeaderBox.class);
        BOXES.add(BOX_MOVIE_FRAGMENT_RANDOM_ACCESS, Box.class);
        BOXES.add(BOX_MOVIE_FRAGMENT_RANDOM_ACCESS_OFFSET, MovieFragmentRandomAccessOffsetBox.class);
        BOXES.add(BOX_MOVIE_HEADER, MovieHeaderBox.class);
        BOXES.add(BOX_NERO_METADATA_TAGS, NeroMetadataTagsBox.class);
        BOXES.add(BOX_NULL_MEDIA_HEADER, FullBox.class);
        BOXES.add(BOX_ORIGINAL_FORMAT, OriginalFormatBox.class);
        BOXES.add(BOX_PADDING_BIT, PaddingBitBox.class);
        BOXES.add(BOX_PARTITION_ENTRY, Box.class);
        BOXES.add(BOX_PIXEL_ASPECT_RATIO, PixelAspectRatioBox.class);
        BOXES.add(BOX_PRIMARY_ITEM, PrimaryItemBox.class);
        BOXES.add(BOX_PROGRESSIVE_DOWNLOAD_INFORMATION, ProgressiveDownloadInformationBox.class);
        BOXES.add(BOX_PROTECTION_SCHEME_INFORMATION, Box.class);
        BOXES.add(BOX_SAMPLE_DEPENDENCY_TYPE, SampleDependencyTypeBox.class);
        BOXES.add(BOX_SAMPLE_DESCRIPTION, SampleDescriptionBox.class);
        BOXES.add(BOX_SAMPLE_GROUP_DESCRIPTION, SampleGroupDescriptionBox.class);
        BOXES.add(BOX_SAMPLE_SCALE, SampleScaleBox.class);
        BOXES.add(BOX_SAMPLE_SIZE, SampleSizeBox.class);
        BOXES.add(BOX_SAMPLE_TABLE, Box.class);
        BOXES.add(BOX_SAMPLE_TO_CHUNK, SampleToChunkBox.class);
        BOXES.add(BOX_SAMPLE_TO_GROUP, SampleToGroupBox.class);
        BOXES.add(BOX_SCHEME_TYPE, SchemeTypeBox.class);
        BOXES.add(BOX_SCHEME_INFORMATION, Box.class);
        BOXES.add(BOX_SHADOW_SYNC_SAMPLE, ShadowSyncSampleBox.class);
        BOXES.add(BOX_SKIP, FreeSpaceBox.class);
        BOXES.add(BOX_SOUND_MEDIA_HEADER, SoundMediaHeaderBox.class);
        BOXES.add(BOX_SUB_SAMPLE_INFORMATION, SubSampleInformationBox.class);
        BOXES.add(BOX_SYNC_SAMPLE, SyncSampleBox.class);
        BOXES.add(BOX_TRACK, Box.class);
        BOXES.add(BOX_TRACK_EXTENDS, TrackExtendsBox.class);
        BOXES.add(BOX_TRACK_FRAGMENT, Box.class);
        BOXES.add(BOX_TRACK_FRAGMENT_HEADER, TrackFragmentHeaderBox.class);
        BOXES.add(BOX_TRACK_FRAGMENT_RANDOM_ACCESS, TrackFragmentRandomAccessBox.class);
        BOXES.add(BOX_TRACK_FRAGMENT_RUN, TrackFragmentRunBox.class);
        BOXES.add(BOX_TRACK_HEADER, TrackHeaderBox.class);
        BOXES.add(BOX_TRACK_REFERENCE, TrackReferenceBox.class);
        BOXES.add(BOX_TRACK_SELECTION, TrackSelectionBox.class);
        BOXES.add(BOX_USER_DATA, Box.class);
        BOXES.add(BOX_VIDEO_MEDIA_HEADER, VideoMediaHeaderBox.class);
        BOXES.add(BOX_WIDE, FreeSpaceBox.class);
        BOXES.add(BOX_XML, XMLBox.class);
        BOXES.add(BOX_OBJECT_DESCRIPTOR, ObjectDescriptorBox.class);
        BOXES.add(BOX_SAMPLE_DEPENDENCY, SampleDependencyBox.class);
        BOXES.add(BOX_ID3_TAG, ID3TagBox.class);
        BOXES.add(BOX_ITUNES_META_LIST, Box.class);
        BOXES.add(BOX_CUSTOM_ITUNES_METADATA, Box.class);
        BOXES.add(BOX_ITUNES_METADATA, ITunesMetadataBox.class);
        BOXES.add(BOX_ITUNES_METADATA_NAME, ITunesMetadataNameBox.class);
        BOXES.add(BOX_ITUNES_METADATA_MEAN, ITunesMetadataMeanBox.class);
        BOXES.add(BOX_ALBUM_ARTIST_NAME, Box.class);
        BOXES.add(BOX_ALBUM_ARTIST_SORT, Box.class);
        BOXES.add(BOX_ALBUM_NAME, Box.class);
        BOXES.add(BOX_ALBUM_SORT, Box.class);
        BOXES.add(BOX_ARTIST_NAME, Box.class);
        BOXES.add(BOX_ARTIST_SORT, Box.class);
        BOXES.add(BOX_CATEGORY, Box.class);
        BOXES.add(BOX_COMMENTS, Box.class);
        BOXES.add(BOX_COMPILATION_PART, Box.class);
        BOXES.add(BOX_COMPOSER_NAME, Box.class);
        BOXES.add(BOX_COMPOSER_SORT, Box.class);
        BOXES.add(BOX_COVER, Box.class);
        BOXES.add(BOX_CUSTOM_GENRE, Box.class);
        BOXES.add(BOX_DESCRIPTION, Box.class);
        BOXES.add(BOX_DISK_NUMBER, Box.class);
        BOXES.add(BOX_ENCODER_NAME, EncoderBox.class);
        BOXES.add(BOX_ENCODER_TOOL, EncoderBox.class);
        BOXES.add(BOX_EPISODE_GLOBAL_UNIQUE_ID, Box.class);
        BOXES.add(BOX_GAPLESS_PLAYBACK, Box.class);
        BOXES.add(BOX_GENRE, GenreBox.class);
        BOXES.add(BOX_GROUPING, Box.class);
        BOXES.add(BOX_HD_VIDEO, Box.class);
        BOXES.add(BOX_ITUNES_PURCHASE_ACCOUNT, Box.class);
        BOXES.add(BOX_ITUNES_ACCOUNT_TYPE, Box.class);
        BOXES.add(BOX_ITUNES_CATALOGUE_ID, Box.class);
        BOXES.add(BOX_ITUNES_COUNTRY_CODE, Box.class);
        BOXES.add(BOX_KEYWORD, Box.class);
        BOXES.add(BOX_LONG_DESCRIPTION, Box.class);
        BOXES.add(BOX_LYRICS, Box.class);
        BOXES.add(BOX_META_TYPE, Box.class);
        BOXES.add(BOX_PODCAST, Box.class);
        BOXES.add(BOX_PODCAST_URL, Box.class);
        BOXES.add(BOX_PURCHASE_DATE, Box.class);
        BOXES.add(BOX_RATING, RatingBox.class);
        BOXES.add(BOX_RELEASE_DATE, Box.class);
        BOXES.add(BOX_REQUIREMENT, RequirementBox.class);
        BOXES.add(BOX_TEMPO, Box.class);
        BOXES.add(BOX_TRACK_NAME, Box.class);
        BOXES.add(BOX_TRACK_NUMBER, Box.class);
        BOXES.add(BOX_TRACK_SORT, Box.class);
        BOXES.add(BOX_TV_EPISODE, Box.class);
        BOXES.add(BOX_TV_EPISODE_NUMBER, Box.class);
        BOXES.add(BOX_TV_NETWORK_NAME, Box.class);
        BOXES.add(BOX_TV_SEASON, Box.class);
        BOXES.add(BOX_TV_SHOW, Box.class);
        BOXES.add(BOX_TV_SHOW_SORT, Box.class);
        BOXES.add(BOX_THREE_GPP_ALBUM, ThreeGPPAlbumBox.class);
        BOXES.add(BOX_THREE_GPP_AUTHOR, ThreeGPPMetadataBox.class);
        BOXES.add(BOX_THREE_GPP_CLASSIFICATION, ThreeGPPMetadataBox.class);
        BOXES.add(BOX_THREE_GPP_DESCRIPTION, ThreeGPPMetadataBox.class);
        BOXES.add(BOX_THREE_GPP_KEYWORDS, ThreeGPPKeywordsBox.class);
        BOXES.add(BOX_THREE_GPP_LOCATION_INFORMATION, ThreeGPPLocationBox.class);
        BOXES.add(BOX_THREE_GPP_PERFORMER, ThreeGPPMetadataBox.class);
        BOXES.add(BOX_THREE_GPP_RECORDING_YEAR, ThreeGPPRecordingYearBox.class);
        BOXES.add(BOX_THREE_GPP_TITLE, ThreeGPPMetadataBox.class);
        BOXES.add(BOX_GOOGLE_HOST_HEADER, Box.class);
        BOXES.add(BOX_GOOGLE_PING_MESSAGE, Box.class);
        BOXES.add(BOX_GOOGLE_PING_URL, Box.class);
        BOXES.add(BOX_GOOGLE_SOURCE_DATA, Box.class);
        BOXES.add(BOX_GOOGLE_START_TIME, Box.class);
        BOXES.add(BOX_GOOGLE_TRACK_DURATION, Box.class);
        BOXES.add(BOX_MP4V_SAMPLE_ENTRY, VideoSampleEntry.class);
        BOXES.add(BOX_H263_SAMPLE_ENTRY, VideoSampleEntry.class);
        BOXES.add(BOX_ENCRYPTED_VIDEO_SAMPLE_ENTRY, VideoSampleEntry.class);
        BOXES.add(BOX_AVC_SAMPLE_ENTRY, VideoSampleEntry.class);
        BOXES.add(BOX_MP4A_SAMPLE_ENTRY, AudioSampleEntry.class);
        BOXES.add(BOX_AC3_SAMPLE_ENTRY, AudioSampleEntry.class);
        BOXES.add(BOX_EAC3_SAMPLE_ENTRY, AudioSampleEntry.class);
        BOXES.add(BOX_DRMS_SAMPLE_ENTRY, AudioSampleEntry.class);
        BOXES.add(BOX_AMR_SAMPLE_ENTRY, AudioSampleEntry.class);
        BOXES.add(BOX_AMR_WB_SAMPLE_ENTRY, AudioSampleEntry.class);
        BOXES.add(BOX_EVRC_SAMPLE_ENTRY, AudioSampleEntry.class);
        BOXES.add(BOX_QCELP_SAMPLE_ENTRY, AudioSampleEntry.class);
        BOXES.add(BOX_SMV_SAMPLE_ENTRY, AudioSampleEntry.class);
        BOXES.add(BOX_ENCRYPTED_AUDIO_SAMPLE_ENTRY, AudioSampleEntry.class);
        BOXES.add(BOX_MPEG_SAMPLE_ENTRY, MPEGSampleEntry.class);
        BOXES.add(BOX_TEXT_METADATA_SAMPLE_ENTRY, TextMetadataSampleEntry.class);
        BOXES.add(BOX_XML_METADATA_SAMPLE_ENTRY, XMLMetadataSampleEntry.class);
        BOXES.add(BOX_RTP_HINT_SAMPLE_ENTRY, RTPHintSampleEntry.class);
        BOXES.add(BOX_FD_HINT_SAMPLE_ENTRY, FDHintSampleEntry.class);
        BOXES.add(BOX_ESD, ESDBox.class);
        BOXES.add(BOX_H263_SPECIFIC, H263SpecificBox.class);
        BOXES.add(BOX_AVC_SPECIFIC, AVCSpecificBox.class);
        BOXES.add(BOX_AC3_SPECIFIC, AC3SpecificBox.class);
        BOXES.add(BOX_EAC3_SPECIFIC, EAC3SpecificBox.class);
        BOXES.add(BOX_AMR_SPECIFIC, AMRSpecificBox.class);
        BOXES.add(BOX_EVRC_SPECIFIC, EVRCSpecificBox.class);
        BOXES.add(BOX_QCELP_SPECIFIC, QCELPSpecificBox.class);
        BOXES.add(BOX_SMV_SPECIFIC, SMVSpecificBox.class);
        BOXES.add(BOX_OMA_ACCESS_UNIT_FORMAT, OMAAccessUnitFormatBox.class);
        BOXES.add(BOX_OMA_COMMON_HEADERS, OMACommonHeadersBox.class);
        BOXES.add(BOX_OMA_CONTENT_ID, OMAContentIDBox.class);
        BOXES.add(BOX_OMA_CONTENT_OBJECT, OMAContentObjectBox.class);
        BOXES.add(BOX_OMA_COVER_URI, OMAURLBox.class);
        BOXES.add(BOX_OMA_DISCRETE_MEDIA_HEADERS, OMADiscreteMediaHeadersBox.class);
        BOXES.add(BOX_OMA_DRM_CONTAINER, FullBox.class);
        BOXES.add(BOX_OMA_ICON_URI, OMAURLBox.class);
        BOXES.add(BOX_OMA_INFO_URL, OMAURLBox.class);
        BOXES.add(BOX_OMA_LYRICS_URI, OMAURLBox.class);
        BOXES.add(BOX_OMA_MUTABLE_DRM_INFORMATION, Box.class);
        BOXES.add(BOX_OMA_KEY_MANAGEMENT, FullBox.class);
        BOXES.add(BOX_OMA_RIGHTS_OBJECT, OMARightsObjectBox.class);
        BOXES.add(BOX_OMA_TRANSACTION_TRACKING, OMATransactionTrackingBox.class);
        BOXES.add(BOX_FAIRPLAY_USER_ID, FairPlayDataBox.class);
        BOXES.add(BOX_FAIRPLAY_USER_NAME, FairPlayDataBox.class);
        BOXES.add(BOX_FAIRPLAY_USER_KEY, FairPlayDataBox.class);
        BOXES.add(BOX_FAIRPLAY_IV, FairPlayDataBox.class);
        BOXES.add(BOX_FAIRPLAY_PRIVATE_KEY, FairPlayDataBox.class);
        //parameter
        FULLNAME.add(BOX_ADDITIONAL_METADATA_CONTAINER, "Additional Metadata Container Box");
        FULLNAME.add(BOX_DATA_INFORMATION,              "Data Information Box");
        FULLNAME.add(BOX_EDIT,                          "Edit Box");
        FULLNAME.add(BOX_MEDIA,                         "Media Box");
        FULLNAME.add(BOX_MEDIA_INFORMATION,             "Media Information Box");
        FULLNAME.add(BOX_MOVIE,                         "Movie Box");
        FULLNAME.add(BOX_MOVIE_EXTENDS,                 "Movie Extends Box");
        FULLNAME.add(BOX_MOVIE_FRAGMENT,                "Movie Fragment Box");
        FULLNAME.add(BOX_MOVIE_FRAGMENT_RANDOM_ACCESS,  "Movie Fragment Random Access Box");
        FULLNAME.add(BOX_NULL_MEDIA_HEADER,             "Null Media Header Box");
        FULLNAME.add(BOX_PARTITION_ENTRY,               "Partition Entry");
        FULLNAME.add(BOX_PROTECTION_SCHEME_INFORMATION, "Protection Scheme Information Box");
        FULLNAME.add(BOX_SAMPLE_TABLE,                  "Sample Table Box");
        FULLNAME.add(BOX_SCHEME_INFORMATION,            "Scheme Information Box");
        FULLNAME.add(BOX_TRACK,                         "Track Box");
        FULLNAME.add(BOX_TRACK_FRAGMENT,                "Track Fragment Box");
        FULLNAME.add(BOX_USER_DATA,                     "User Data Box");
        FULLNAME.add(BOX_ITUNES_META_LIST,              "iTunes Meta List Box");
        FULLNAME.add(BOX_CUSTOM_ITUNES_METADATA,        "Custom iTunes Metadata Box");
        FULLNAME.add(BOX_ALBUM_ARTIST_NAME,             "Album Artist Name Box");
        FULLNAME.add(BOX_ALBUM_ARTIST_SORT,             "Album Artist Sort Box");
        FULLNAME.add(BOX_ALBUM_NAME,                    "Album Name Box");
        FULLNAME.add(BOX_ALBUM_SORT,                    "Album Sort Box");
        FULLNAME.add(BOX_ARTIST_NAME,                   "Artist Name Box");
        FULLNAME.add(BOX_ARTIST_SORT,                   "Artist Sort Box");
        FULLNAME.add(BOX_CATEGORY,                      "Category Box");
        FULLNAME.add(BOX_COMMENTS,                      "Comments Box");
        FULLNAME.add(BOX_COMPILATION_PART,              "Compilation Part Box");
        FULLNAME.add(BOX_COMPOSER_NAME,                 "Composer Name Box");
        FULLNAME.add(BOX_COMPOSER_SORT,                 "Composer Sort Box");
        FULLNAME.add(BOX_COVER,                         "Cover Box");
        FULLNAME.add(BOX_CUSTOM_GENRE,                  "Custom Genre Box");
        FULLNAME.add(BOX_DESCRIPTION,                   "Description Cover Box");
        FULLNAME.add(BOX_DISK_NUMBER,                   "Disk Number Box");
        FULLNAME.add(BOX_EPISODE_GLOBAL_UNIQUE_ID,      "Episode Global Unique ID Box");
        FULLNAME.add(BOX_GAPLESS_PLAYBACK,              "Gapless Playback Box");
        FULLNAME.add(BOX_GROUPING,                      "Grouping Box");
        FULLNAME.add(BOX_HD_VIDEO,                      "HD Video Box");
        FULLNAME.add(BOX_ITUNES_PURCHASE_ACCOUNT,       "iTunes Purchase Account Box");
        FULLNAME.add(BOX_ITUNES_ACCOUNT_TYPE,           "iTunes Account Type Box");
        FULLNAME.add(BOX_ITUNES_CATALOGUE_ID,           "iTunes Catalogue ID Box");
        FULLNAME.add(BOX_ITUNES_COUNTRY_CODE,           "iTunes Country Code Box");
        FULLNAME.add(BOX_KEYWORD,                       "Keyword Box");
        FULLNAME.add(BOX_LONG_DESCRIPTION,              "Long Description Box");
        FULLNAME.add(BOX_LYRICS,                        "Lyrics Box");
        FULLNAME.add(BOX_META_TYPE,                     "Meta Type Box");
        FULLNAME.add(BOX_PODCAST,                       "Podcast Box");
        FULLNAME.add(BOX_PODCAST_URL,                   "Podcast URL Box");
        FULLNAME.add(BOX_PURCHASE_DATE,                 "Purchase Date Box");
        FULLNAME.add(BOX_RELEASE_DATE,                  "Release Date Box");
        FULLNAME.add(BOX_TEMPO,                         "Tempo Box");
        FULLNAME.add(BOX_TRACK_NAME,                    "Track Name Box");
        FULLNAME.add(BOX_TRACK_NUMBER,                  "Track Number Box");
        FULLNAME.add(BOX_TRACK_SORT,                    "Track Sort Box");
        FULLNAME.add(BOX_TV_EPISODE,                    "TV Episode Box");
        FULLNAME.add(BOX_TV_EPISODE_NUMBER,             "TV Episode Number Box");
        FULLNAME.add(BOX_TV_NETWORK_NAME,               "TV Network Name Box");
        FULLNAME.add(BOX_TV_SEASON,                     "TV Season Box");
        FULLNAME.add(BOX_TV_SHOW,                       "TV Show Box");
        FULLNAME.add(BOX_TV_SHOW_SORT,                  "TV Show Sort Box");
        FULLNAME.add(BOX_THREE_GPP_AUTHOR,              "3GPP Author Box");
        FULLNAME.add(BOX_THREE_GPP_CLASSIFICATION,      "3GPP Classification Box");
        FULLNAME.add(BOX_THREE_GPP_DESCRIPTION,         "3GPP Description Box");
        FULLNAME.add(BOX_THREE_GPP_PERFORMER,           "3GPP Performer Box");
        FULLNAME.add(BOX_THREE_GPP_TITLE,               "3GPP Title Box");
        FULLNAME.add(BOX_GOOGLE_HOST_HEADER,            "Google Host Header Box");
        FULLNAME.add(BOX_GOOGLE_PING_MESSAGE,           "Google Ping Message Box");
        FULLNAME.add(BOX_GOOGLE_PING_URL,               "Google Ping URL Box");
        FULLNAME.add(BOX_GOOGLE_SOURCE_DATA,            "Google Source Data Box");
        FULLNAME.add(BOX_GOOGLE_START_TIME,             "Google Start Time Box");
        FULLNAME.add(BOX_GOOGLE_TRACK_DURATION,         "Google Track Duration Box");
        FULLNAME.add(BOX_MP4V_SAMPLE_ENTRY,             "MPEG-4 Video Sample Entry");
        FULLNAME.add(BOX_H263_SAMPLE_ENTRY,             "H263 Video Sample Entry");
        FULLNAME.add(BOX_ENCRYPTED_VIDEO_SAMPLE_ENTRY,  "Encrypted Video Sample Entry");
        FULLNAME.add(BOX_AVC_SAMPLE_ENTRY,              "AVC Video Sample Entry");
        FULLNAME.add(BOX_MP4A_SAMPLE_ENTRY,             "MPEG- 4Audio Sample Entry");
        FULLNAME.add(BOX_AC3_SAMPLE_ENTRY,              "AC-3 Audio Sample Entry");
        FULLNAME.add(BOX_EAC3_SAMPLE_ENTRY,             "Extended AC-3 Audio Sample Entry");
        FULLNAME.add(BOX_DRMS_SAMPLE_ENTRY,             "DRMS Audio Sample Entry");
        FULLNAME.add(BOX_AMR_SAMPLE_ENTRY,              "AMR Audio Sample Entry");
        FULLNAME.add(BOX_AMR_WB_SAMPLE_ENTRY,           "AMR-Wideband Audio Sample Entry");
        FULLNAME.add(BOX_EVRC_SAMPLE_ENTRY,             "EVC Audio Sample Entry");
        FULLNAME.add(BOX_QCELP_SAMPLE_ENTRY,            "QCELP Audio Sample Entry");
        FULLNAME.add(BOX_SMV_SAMPLE_ENTRY,              "SMV Audio Sample Entry");
        FULLNAME.add(BOX_ENCRYPTED_AUDIO_SAMPLE_ENTRY,  "Encrypted Audio Sample Entry");
        FULLNAME.add(BOX_OMA_COVER_URI,                 "OMA DRM Cover URI Box");
        FULLNAME.add(BOX_OMA_DRM_CONTAINER,             "OMA DRM Container Box");
        FULLNAME.add(BOX_OMA_ICON_URI,                  "OMA DRM Icon URI Box");
        FULLNAME.add(BOX_OMA_INFO_URL,                  "OMA DRM Info URL Box");
        FULLNAME.add(BOX_OMA_LYRICS_URI,                "OMA DRM Lyrics URI Box");
        FULLNAME.add(BOX_OMA_MUTABLE_DRM_INFORMATION,   "OMA DRM Mutable DRM Information Box");

        //sub boxes
        GROUPBOXES.add(BOX_MOOV);
        GROUPBOXES.add(BOX_TRAK);
        GROUPBOXES.add(BOX_MDIA);
        GROUPBOXES.add(BOX_MINF);
        GROUPBOXES.add(BOX_DINF);
        GROUPBOXES.add(BOX_STBL);
    }

    private MP4Constants(){}

    public static Box createBox(long type){
        Class clazz = (Class) BOXES.getValue(type);
        if (clazz==null) clazz = Box.class;
        try {
            return (Box) clazz.newInstance();
        } catch (InstantiationException ex) {
            throw new RuntimeException(ex);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static boolean containsSubBoxes(Chars name){
        return GROUPBOXES.contains(name);
    }

    public static Chars getFourCC(long type){
        final byte[] array = new byte[4];
        Endianness.BigEndian.BIG_ENDIAN.writeUInt(type, array, 0);
        return new Chars(array, CharEncodings.US_ASCII);
    }

    public static Chars getFullName(long type){
        String str = (String) FULLNAME.getValue(type);
        if (str!=null) return new Chars(str);
        return getFourCC(type);
    }

    public static String getLanguageCode(long l) {
        //1 bit padding, 5*3 bits language code (ISO-639-2/T)
        char[] c = new char[3];
        c[0] = (char) (((l>>10)&31)+0x60);
        c[1] = (char) (((l>>5)&31)+0x60);
        c[2] = (char) ((l&31)+0x60);
        return new String(c);
    }

    public static long detectUndetermined(long l) {
        final long x;
        if (l==UNDETERMINED) x = -1;
        else x = l;
        return x;
    }

    public static Box parseBox(Box parent, MP4InputStream in) throws IOException {
        final long offset = in.getOffset();

        long size = in.readBytes(4);
        long type = in.readBytes(4);
        if (size==1) size = in.readBytes(8);
        if (type==BOX_EXTENDED_TYPE) in.skipBytes(16);

        //error protection
        if (parent!=null) {
            final long parentLeft = (parent.getOffset()+parent.getSize())-offset;
            if (size>parentLeft) throw new IOException(in, "error while decoding box '"+MP4Constants.getFourCC(type)+"' at offset "+offset+": box too large for parent");
        }

        MP4Constants.LOGGER.debug(MP4Constants.getFourCC(type));
        final Box box = createBox(type);
        box.setParams(parent, size, type, offset);
        box.decode(in);

        //if box doesn't contain data it only contains children
        final Class<?> cl = box.getClass();
        if (cl==Box.class||cl==FullBox.class){
            box.readChildren(in);
        }

        //check bytes left
        final long left = (box.getOffset()+box.getSize())-in.getOffset();
        if (left>0
                &&!(box instanceof MediaDataBox)
                &&!(box instanceof UnknownBox)
                &&!(box instanceof FreeSpaceBox)) MP4Constants.LOGGER.info(new Chars("bytes left after reading box "+MP4Constants.getFourCC(type)+": left: "+left+", offset: "+in.getOffset()));
        else if (left<0) MP4Constants.LOGGER.critical(new Chars("box "+MP4Constants.getFourCC(type)+" overread:"+(-left)+" bytes, offset: "+in.getOffset()));

        //if mdat found and no random access, don't skip
        if (box.getType()!=BOX_MEDIA_DATA||in.hasRandomAccess()) in.skipBytes(left);
        return box;
    }

}
