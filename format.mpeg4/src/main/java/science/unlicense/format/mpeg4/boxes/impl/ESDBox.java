package science.unlicense.format.mpeg4.boxes.impl;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.FullBox;
import science.unlicense.format.mpeg4.desc.ESDescriptor;
import science.unlicense.format.mpeg4.desc.ObjectDescriptor;

/**
 * The entry sample descriptor (ESD) box is a container for entry descriptors.
 * If used, it is located in a sample entry. Instead of an <code>ESDBox</code> a
 * <code>CodecSpecificBox</code> may be present.
 *
 * @author Alexander Simm
 */
public class ESDBox extends FullBox {

    private ESDescriptor esd;

    public ESDBox() {
        super("ESD Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        esd = (ESDescriptor) ObjectDescriptor.createDescriptor(in);
    }

    public ESDescriptor getEntryDescriptor() {
        return esd;
    }
}
