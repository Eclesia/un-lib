package science.unlicense.format.mpeg4.boxes.impl;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.Box;

/**
 * This class is used for all boxes, that are known but don't contain necessary
 * data and can be skipped. This is mainly used for 'skip', 'free' and 'wide'.
 *
 * @author Alexander Simm
 */
public class FreeSpaceBox extends Box {

    public FreeSpaceBox() {
        super("Free Space Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        //no need to read, box will be skipped
    }
}
