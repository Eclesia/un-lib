package science.unlicense.format.mpeg4.boxes.impl;

import science.unlicense.format.mpeg4.boxes.FullBox;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4Constants;

/**
 *
 * @author Alexander Simm
 */
public class ChunkOffsetBox extends FullBox {

    private long[] chunks;

    public ChunkOffsetBox() {
        super("Chunk Offset Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        final int len = (type==MP4Constants.BOX_CHUNK_LARGE_OFFSET) ? 8 : 4;
        final int entryCount = (int) in.readBytes(4);
        chunks = new long[entryCount];

        for (int i = 0; i<entryCount; i++) {
            chunks[i] = in.readBytes(len);
        }
    }

    public long[] getChunks() {
        return chunks;
    }
}
