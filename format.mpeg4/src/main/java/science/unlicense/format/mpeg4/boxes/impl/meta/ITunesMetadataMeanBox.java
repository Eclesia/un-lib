package science.unlicense.format.mpeg4.boxes.impl.meta;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.FullBox;

/**
 *
 * @author Alexander Simm
 */
public class ITunesMetadataMeanBox extends FullBox {

    private String domain;

    public ITunesMetadataMeanBox() {
        super("iTunes Metadata Mean Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        domain = in.readString((int) getLeft(in));
    }

    public String getDomain() {
        return domain;
    }
}
