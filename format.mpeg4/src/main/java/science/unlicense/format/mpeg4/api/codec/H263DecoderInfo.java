package science.unlicense.format.mpeg4.api.codec;

import science.unlicense.format.mpeg4.api.DecoderInfo;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.CodecSpecificBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.H263SpecificBox;

/**
 *
 * @author Alexander Simm
 */
public class H263DecoderInfo extends DecoderInfo {

    private final H263SpecificBox box;

    public H263DecoderInfo(CodecSpecificBox box) {
        this.box = (H263SpecificBox) box;
    }

    public int getDecoderVersion() {
        return box.getDecoderVersion();
    }

    public long getVendor() {
        return box.getVendor();
    }

    public int getLevel() {
        return box.getLevel();
    }

    public int getProfile() {
        return box.getProfile();
    }
}
