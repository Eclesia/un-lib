
package science.unlicense.format.mpeg4.boxes.impl.sampleentries;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;

/**
 *
 * @author Alexander Simm
 */
public class RTPHintSampleEntry extends SampleEntry {

    private int hintTrackVersion, highestCompatibleVersion;
    private long maxPacketSize;

    public RTPHintSampleEntry() {
        super("RTP Hint Sample Entry");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        hintTrackVersion = (int) in.readBytes(2);
        highestCompatibleVersion = (int) in.readBytes(2);
        maxPacketSize = in.readBytes(4);
    }

    /**
     * The maximum packet size indicates the size of the largest packet that
     * this track will generate.
     *
     * @return the maximum packet size
     */
    public long getMaxPacketSize() {
        return maxPacketSize;
    }
}
