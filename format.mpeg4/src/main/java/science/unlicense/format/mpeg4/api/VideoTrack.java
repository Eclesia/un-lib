package science.unlicense.format.mpeg4.api;

import science.unlicense.format.mpeg4.MP4Constants;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.impl.ESDBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleDescriptionBox;
import science.unlicense.format.mpeg4.boxes.impl.VideoMediaHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.VideoSampleEntry;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.CodecSpecificBox;
import science.unlicense.format.mpeg4.boxes.Box;

/**
 *
 * @author Alexander Simm
 */
public class VideoTrack extends Track {

    public enum VideoCodec implements Codec {

        AVC,
        H263,
        MP4_ASP,
        UNKNOWN_VIDEO_CODEC;

        static Codec forType(long type) {
            final Codec ac;
            if (type==MP4Constants.BOX_AVC_SAMPLE_ENTRY) ac = AVC;
            else if (type==MP4Constants.BOX_H263_SAMPLE_ENTRY) ac = H263;
            else if (type==MP4Constants.BOX_MP4V_SAMPLE_ENTRY) ac = MP4_ASP;
            else ac = UNKNOWN_VIDEO_CODEC;
            return ac;
        }
    }
    private final VideoMediaHeaderBox vmhd;
    private final VideoSampleEntry sampleEntry;
    private final Codec codec;

    public VideoTrack(Box trak, MP4InputStream in) {
        super(trak, in);

        final Box minf = trak.getChild(MP4Constants.BOX_MEDIA).getChild(MP4Constants.BOX_MEDIA_INFORMATION);
        vmhd = (VideoMediaHeaderBox) minf.getChild(MP4Constants.BOX_VIDEO_MEDIA_HEADER);

        final Box stbl = minf.getChild(MP4Constants.BOX_SAMPLE_TABLE);

        //sample descriptions: 'mp4v' has an ESDBox, all others have a CodecSpecificBox
        final SampleDescriptionBox stsd = (SampleDescriptionBox) stbl.getChild(MP4Constants.BOX_SAMPLE_DESCRIPTION);
        if (stsd.getChildren().get(0) instanceof VideoSampleEntry) {
            sampleEntry = (VideoSampleEntry) stsd.getChildren().get(0);
            final long type = sampleEntry.getType();
            if (type==MP4Constants.BOX_MP4V_SAMPLE_ENTRY) findDecoderSpecificInfo((ESDBox) sampleEntry.getChild(MP4Constants.BOX_ESD));
            else if (type==MP4Constants.BOX_ENCRYPTED_VIDEO_SAMPLE_ENTRY||type==MP4Constants.BOX_DRMS_SAMPLE_ENTRY) {
                findDecoderSpecificInfo((ESDBox) sampleEntry.getChild(MP4Constants.BOX_ESD));
                protection = Protection.parse(sampleEntry.getChild(MP4Constants.BOX_PROTECTION_SCHEME_INFORMATION));
            }
            else decoderInfo = DecoderInfo.parse((CodecSpecificBox) sampleEntry.getChildren().get(0));

            codec = VideoCodec.forType(sampleEntry.getType());
        }
        else {
            sampleEntry = null;
            codec = VideoCodec.UNKNOWN_VIDEO_CODEC;
        }
    }

    @Override
    public Type getType() {
        return Type.VIDEO;
    }

    @Override
    public Codec getCodec() {
        return codec;
    }

    public int getWidth() {
        return (sampleEntry!=null) ? sampleEntry.getWidth() : 0;
    }

    public int getHeight() {
        return (sampleEntry!=null) ? sampleEntry.getHeight() : 0;
    }

    public double getHorizontalResolution() {
        return (sampleEntry!=null) ? sampleEntry.getHorizontalResolution() : 0;
    }

    public double getVerticalResolution() {
        return (sampleEntry!=null) ? sampleEntry.getVerticalResolution() : 0;
    }

    public int getFrameCount() {
        return (sampleEntry!=null) ? sampleEntry.getFrameCount() : 0;
    }

    public String getCompressorName() {
        return (sampleEntry!=null) ? sampleEntry.getCompressorName() : "";
    }

    public int getDepth() {
        return (sampleEntry!=null) ? sampleEntry.getDepth() : 0;
    }

    public int getLayer() {
        return tkhd.getLayer();
    }
}
