package science.unlicense.format.mpeg4.boxes.impl;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4Constants;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.FullBox;

/**
 *
 * @author Alexander Simm
 */
public class DataEntryUrnBox extends FullBox {

    private boolean inFile;
    private String referenceName, location;

    public DataEntryUrnBox() {
        super("Data Entry Urn Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        inFile = (flags&1)==1;
        if (!inFile) {
            referenceName = in.readUTFString((int) getLeft(in), MP4Constants.UTF8);
            if (getLeft(in)>0) location = in.readUTFString((int) getLeft(in), MP4Constants.UTF8);
        }
    }

    public boolean isInFile() {
        return inFile;
    }

    public String getReferenceName() {
        return referenceName;
    }

    public String getLocation() {
        return location;
    }
}
