package science.unlicense.format.mpeg4.api;

import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.boxes.impl.meta.ITunesMetadataBox.DataType;

/**
 *
 * @author Alexander Simm
 */
public class Artwork {

    //TODO: need this enum? it just copies the DataType
    public enum Type {

        GIF, JPEG, PNG, BMP;

        static Type forDataType(DataType dataType) {
            Type type;
            switch(dataType) {
                case GIF:
                    type = GIF;
                    break;
                case JPEG:
                    type = JPEG;
                    break;
                case PNG:
                    type = PNG;
                    break;
                case BMP:
                    type = BMP;
                    break;
                default:
                    type = null;
            }
            return type;
        }
    }
    private Type type;
    private byte[] data;
    private Image image;

    Artwork(Type type, byte[] data) {
        this.type = type;
        this.data = data;
    }

    /**
     * Returns the type of data in this artwork.
     *
     * @see Type
     * @return the data's type
     */
    public Type getType() {
        return type;
    }

    /**
     * Returns the encoded data of this artwork.
     *
     * @return the encoded data
     */
    public byte[] getData() {
        return data;
    }

    /**
     * Returns the decoded image, that can be painted.
     *
     * @return the decoded image
     * @throws IOException if decoding fails
     */
    public Image getImage() throws IOException {
        if (image==null){
            image = Images.read(new ArrayInputStream(data));
        }
        return image;
    }
}
