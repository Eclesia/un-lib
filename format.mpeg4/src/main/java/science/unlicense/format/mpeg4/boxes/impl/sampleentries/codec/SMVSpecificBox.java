package science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;

/**
 *
 * @author Alexander Simm
 */
public class SMVSpecificBox extends CodecSpecificBox {

    private int framesPerSample;

    public SMVSpecificBox() {
        super("SMV Specific Structure");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        decodeCommon(in);

        framesPerSample = in.read();
    }

    public int getFramesPerSample() {
        return framesPerSample;
    }
}
