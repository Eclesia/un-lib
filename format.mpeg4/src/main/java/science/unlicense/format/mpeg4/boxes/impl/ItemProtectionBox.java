package science.unlicense.format.mpeg4.boxes.impl;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.FullBox;

/**
 * The item protection box provides an array of item protection information, for
 * use by the Item Information Box.
 *
 * @author Alexander Simm
 */
public class ItemProtectionBox extends FullBox {

    public ItemProtectionBox() {
        super("Item Protection Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        final int protectionCount = (int) in.readBytes(2);

        readChildren(in, protectionCount);
    }
}
