package science.unlicense.format.mpeg4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.format.mpeg4.api.AudioTrack;
import science.unlicense.format.mpeg4.api.Brand;
import science.unlicense.format.mpeg4.api.Movie;
import science.unlicense.format.mpeg4.api.Track;
import science.unlicense.format.mpeg4.api.VideoTrack;
import science.unlicense.format.mpeg4.boxes.Box;
import science.unlicense.format.mpeg4.boxes.impl.FileTypeBox;
import science.unlicense.format.mpeg4.boxes.impl.ProgressiveDownloadInformationBox;
import static science.unlicense.media.api.AudioTrack.*;
import science.unlicense.media.api.DefaultAudioTrack;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;

/**
 * The MP4Store is the central class for the MP4 demultiplexer. It reads the
 * container and gives access to the containing data.
 *
 * The data source can be either an <code>InputStream</code> or a
 * <code>RandomAccessFile</code>. Since the specification does not decree a
 * specific order of the content, the data needed for parsing (the sample
 * tables) may be at the end of the stream. In this case, random access is
 * needed and reading from an <code>InputSteam</code> will cause an exception.
 * Thus, whenever possible, a <code>RandomAccessFile</code> should be used for
 * local files. Parsing from an <code>InputStream</code> is useful when reading
 * from a network stream.
 *
 * Each <code>MP4Container</code> can return the used file brand (file format
 * version). Optionally, the following data may be present:
 * <ul>
 * <li>progressive download informations: pairs of download rate and playback
 * delay, see {@link #getDownloadInformationPairs() getDownloadInformationPairs()}</li>
 * <li>a <code>Movie</code></li>
 * </ul>
 *
 * Additionally it gives access to the underlying MP4 boxes, that can be
 * retrieved by <code>getBoxes()</code>. However, it is not recommended to
 * access the boxes directly.
 *
 * @author Alexander Simm
 * @author Johann Sorel
 */
public class MP4Store extends AbstractStore implements Media {

    private final MP4InputStream in;
    private final List<Box> boxes = new ArrayList<Box>();
    private Brand major, minor;
    private Brand[] compatible;
    private FileTypeBox ftyp;
    private ProgressiveDownloadInformationBox pdin;
    private Box moov;
    private Movie movie;

    public MP4Store(Path input) throws IOException {
        super(MP4Format.INSTANCE,input);
        this.in = new MP4InputStream(input.createSeekableBuffer(true, false, false));
        readContent();
    }

    @Override
    public science.unlicense.media.api.Track[] getTracks() throws IOException {
        final List<Track> tracks = getMovie().getTracks(AudioTrack.AudioCodec.AAC);
        final science.unlicense.media.api.Track[] metas = new science.unlicense.media.api.Track[tracks.size()];
        for (int i=0;i<metas.length;i++){
            final Track track = tracks.get(i);
            if (track instanceof AudioTrack){
                final AudioTrack at = (AudioTrack) track;
                final Chars name = new Chars("AAC");
                final int nbChan = at.getChannelCount();
                final int[] channels;
                final int[] bitsPerSample;
                if (nbChan==1){
                    channels = new int[]{CHANNEL_MONO};
                    bitsPerSample = new int[]{at.getSampleSize()};
                } else {
                    //TODO improve this
                    channels = new int[]{CHANNEL_LEFT,CHANNEL_RIGHT};
                    bitsPerSample = new int[]{at.getSampleSize(),at.getSampleSize()};
                }
                final double sampleRate = at.getSampleRate();
                metas[i] = new DefaultAudioTrack(name, channels, bitsPerSample, sampleRate);

            } else if (track instanceof VideoTrack){
                metas[i] = null;
            } else {
                throw new IOException(this, "Unexpected track type : "+track);
            }
        }

        return metas;
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        final Track[] tracks = getMovie().getTracks(AudioTrack.AudioCodec.AAC).toArray(new Track[0]);
        final science.unlicense.media.api.Track[] metas = getTracks();
        final MP4MediaReader reader = new MP4MediaReader(tracks, metas);
        return reader;
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    private void readContent() throws IOException {
        //read all boxes
        Box box = null;
        long type;
        boolean moovFound = false;
        while (in.hasLeft()) {
            box = MP4Constants.parseBox(null, in);
            if (boxes.isEmpty()&&box.getType()!=MP4Constants.BOX_FILE_TYPE) throw new MP4Exception("no MP4 signature found");
            boxes.add(box);

            type = box.getType();
            if (type==MP4Constants.BOX_FILE_TYPE) {
                if (ftyp==null) ftyp = (FileTypeBox) box;
            }
            else if (type==MP4Constants.BOX_MOVIE) {
                if (movie==null) moov = box;
                moovFound = true;
            }
            else if (type==MP4Constants.BOX_PROGRESSIVE_DOWNLOAD_INFORMATION) {
                if (pdin==null) pdin = (ProgressiveDownloadInformationBox) box;
            }
            else if (type==MP4Constants.BOX_MEDIA_DATA) {
                if (moovFound) break;
                else if (!in.hasRandomAccess()) throw new MP4Exception("movie box at end of file, need random access");
            }
        }
    }

    public Brand getMajorBrand() {
        if (major==null) major = Brand.forID(ftyp.getMajorBrand());
        return major;
    }

    public Brand getMinorBrand() {
        if (minor==null) minor = Brand.forID(ftyp.getMajorBrand());
        return minor;
    }

    public Brand[] getCompatibleBrands() {
        if (compatible==null) {
            final String[] s = ftyp.getCompatibleBrands();
            compatible = new Brand[s.length];
            for (int i = 0; i<s.length; i++) {
                compatible[i] = Brand.forID(s[i]);
            }
        }
        return compatible;
    }

    //TODO: pdin, movie fragments??
    public Movie getMovie() {
        if (moov==null) return null;
        else if (movie==null) movie = new Movie(moov, in);
        return movie;
    }

    public List<Box> getBoxes() {
        return Collections.unmodifiableList(boxes);
    }
}
