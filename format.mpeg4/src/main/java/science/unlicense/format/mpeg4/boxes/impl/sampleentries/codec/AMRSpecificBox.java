package science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;

/**
 *
 * @author Alexander Simm
 */
public class AMRSpecificBox extends CodecSpecificBox {

    private int modeSet, modeChangePeriod, framesPerSample;

    public AMRSpecificBox() {
        super("AMR Specific Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        decodeCommon(in);

        modeSet = (int) in.readBytes(2);
        modeChangePeriod = in.read();
        framesPerSample = in.read();
    }

    public int getModeSet() {
        return modeSet;
    }

    public int getModeChangePeriod() {
        return modeChangePeriod;
    }

    public int getFramesPerSample() {
        return framesPerSample;
    }
}
