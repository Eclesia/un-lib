
package science.unlicense.format.mpeg4.boxes.impl.sampleentries;

import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Alexander Simm
 */
public class VideoSampleEntry extends SampleEntry {

    private int width, height;
    private double horizontalResolution, verticalResolution;
    private int frameCount, depth;
    private String compressorName;

    public VideoSampleEntry() {
        super("");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        in.skipBytes(2); //pre-defined: 0
        in.skipBytes(2); //reserved
        //3x32 pre_defined
        in.skipBytes(4); //pre-defined: 0
        in.skipBytes(4); //pre-defined: 0
        in.skipBytes(4); //pre-defined: 0

        width = (int) in.readBytes(2);
        height = (int) in.readBytes(2);
        horizontalResolution = in.readFixedPoint(16, 16);
        verticalResolution = in.readFixedPoint(16, 16);
        in.skipBytes(4); //reserved
        frameCount = (int) in.readBytes(2);

        final int len = in.read();
        compressorName = in.readString(len);
        in.skipBytes(31-len);

        depth = (int) in.readBytes(2);
        in.skipBytes(2); //pre-defined: -1

        readChildren(in);
    }

    /**
     * The width is the maximum visual width of the stream described by this
     * sample description, in pixels.
     */
    public int getWidth() {
        return width;
    }

    /**
     * The height is the maximum visual height of the stream described by this
     * sample description, in pixels.
     */
    public int getHeight() {
        return height;
    }

    /**
     * The horizontal resolution of the image in pixels-per-inch, as a floating
     * point value.
     */
    public double getHorizontalResolution() {
        return horizontalResolution;
    }

    /**
     * The vertical resolution of the image in pixels-per-inch, as a floating
     * point value.
     */
    public double getVerticalResolution() {
        return verticalResolution;
    }

    /**
     * The frame count indicates how many frames of compressed video are stored
     * in each sample.
     */
    public int getFrameCount() {
        return frameCount;
    }

    /**
     * The compressor name, for informative purposes.
     */
    public String getCompressorName() {
        return compressorName;
    }

    /**
     * The depth takes one of the following values
     * DEFAULT_DEPTH (0x18) – images are in colour with no alpha
     */
    public int getDepth() {
        return depth;
    }
}
