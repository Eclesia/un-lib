package science.unlicense.format.mpeg4.adts;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;


/**
 * Audio Data Transport Stream.
 *
 * http://en.wikipedia.org/wiki/Advanced_Audio_Coding
 *
 * @author Alexander Simm
 * @author Johann Sorel
 */
public class ADTSDemultiplexer {

    private static final int MAXIMUM_FRAME_SIZE = 6144;

    private final BacktrackInputStream in;
    private final DataInputStream din;
    private boolean first;
    private ADTSFrame frame;

    public ADTSDemultiplexer(ByteInputStream in) throws IOException {
        this.in = new BacktrackInputStream(in);
        this.din = new DataInputStream(this.in);
        this.first = true;
        if (!findNextFrame()){
            throw new IOException(in, "No ADTS header found");
        }
    }

    public int getSampleFrequency() {
        return frame.getSampleFrequency();
    }

    public int getChannelCount() {
        return frame.getChannelCount();
    }

    public byte[] getDecoderSpecificInfo() {
        return frame.createDecoderSpecificInfo();
    }

    public byte[] readNextFrame() throws IOException {
        if (first){
            first = false;
        } else {
            findNextFrame();
        }

        byte[] b = new byte[frame.getFrameLength()];
        din.readFully(b);
        return b;
    }

    private boolean findNextFrame() throws IOException {
        //find next ADTS ID
        boolean found = false;
        int left = MAXIMUM_FRAME_SIZE;
        int i;
        while (!found&&left>0) {
            i = in.read();
            left--;
            if (i==0xFF) {
                in.mark();
                i = in.read();
                if (((i>>4)&0xF)==0xF){
                    found = true;
                }
                in.rewind();
            }
        }

        if (found){
            frame = new ADTSFrame(din);
        }
        return found;
    }

}
