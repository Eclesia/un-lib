package science.unlicense.format.mpeg4.boxes.impl.samplegroupentries;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.Box;

/**
 *
 * @author Alexander Simm
 */
public abstract class SampleGroupDescriptionEntry extends Box {

    protected SampleGroupDescriptionEntry(String name) {
        super(name);
    }

    @Override
    public abstract void decode(MP4InputStream in) throws IOException;
}
