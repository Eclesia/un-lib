package science.unlicense.format.mpeg4.boxes;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;

/**
 *
 * @author Alexander Simm
 */
public class FullBox extends Box {

    protected int version;
    protected int flags;

    public FullBox(String name) {
        super(name);
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        version = in.read();
        flags = (int) in.readBytes(3);
    }
}
