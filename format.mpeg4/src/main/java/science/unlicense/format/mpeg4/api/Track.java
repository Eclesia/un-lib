package science.unlicense.format.mpeg4.api;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.EOSException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4Constants;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.Box;
import science.unlicense.format.mpeg4.boxes.impl.ChunkOffsetBox;
import science.unlicense.format.mpeg4.boxes.impl.DataEntryUrlBox;
import science.unlicense.format.mpeg4.boxes.impl.DataReferenceBox;
import science.unlicense.format.mpeg4.boxes.impl.DecodingTimeToSampleBox;
import science.unlicense.format.mpeg4.boxes.impl.ESDBox;
import science.unlicense.format.mpeg4.boxes.impl.MediaHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleSizeBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleToChunkBox;
import science.unlicense.format.mpeg4.boxes.impl.TrackHeaderBox;
import science.unlicense.format.mpeg4.desc.DecoderSpecificInfo;
import science.unlicense.format.mpeg4.desc.Descriptor;

/**
 * This class represents a track in a movie.
 *
 * Each track contains either a decoder specific info as a byte array or a
 * <code>DecoderInfo</code> object that contains necessary information for the
 * decoder.
 *
 * @author Alexander Simm
 */
//TODO: expand javadoc; use generics for subclasses?
public abstract class Track {

    public interface Codec {
        //TODO: currently only marker interface
    }
    private final MP4InputStream in;
    protected final TrackHeaderBox tkhd;
    private final MediaHeaderBox mdhd;
    private final boolean inFile;
    private final List<Frame> frames;
    private URL location;
    private int currentFrame;
    //info structures
    protected DecoderSpecificInfo decoderSpecificInfo;
    protected DecoderInfo decoderInfo;
    protected Protection protection;

    Track(Box trak, MP4InputStream in) {
        this.in = in;

        tkhd = (TrackHeaderBox) trak.getChild(MP4Constants.BOX_TRACK_HEADER);

        final Box mdia = trak.getChild(MP4Constants.BOX_MEDIA);
        mdhd = (MediaHeaderBox) mdia.getChild(MP4Constants.BOX_MEDIA_HEADER);
        final Box minf = mdia.getChild(MP4Constants.BOX_MEDIA_INFORMATION);

        final Box dinf = minf.getChild(MP4Constants.BOX_DATA_INFORMATION);
        final DataReferenceBox dref = (DataReferenceBox) dinf.getChild(MP4Constants.BOX_DATA_REFERENCE);
        //TODO: support URNs
        if (dref.hasChild(MP4Constants.BOX_DATA_ENTRY_URL)) {
            DataEntryUrlBox url = (DataEntryUrlBox) dref.getChild(MP4Constants.BOX_DATA_ENTRY_URL);
            inFile = url.isInFile();
            if (!inFile) {
                try {
                    location = new URL(url.getLocation());
                }
                catch(MalformedURLException e) {
                    MP4Constants.LOGGER.warning(new Chars("Parsing URL-Box failed:"+e.toString()+" url:"+url.getLocation()));
                    location = null;
                }
            }
        }
        /*else if (dref.containsChild(MP4Constants.BOX_DATA_ENTRY_URN)) {
        DataEntryUrnBox urn = (DataEntryUrnBox) dref.getChild(MP4Constants.BOX_DATA_ENTRY_URN);
        inFile = urn.isInFile();
        location = urn.getLocation();
        }*/
        else {
            inFile = true;
            location = null;
        }

        //sample table
        final Box stbl = minf.getChild(MP4Constants.BOX_SAMPLE_TABLE);
        if (stbl.hasChildren()) {
            frames = new ArrayList<Frame>();
            parseSampleTable(stbl);
        }
        else frames = Collections.emptyList();
        currentFrame = 0;
    }

    private void parseSampleTable(Box stbl) {
        final double timeScale = mdhd.getTimeScale();
        final Type type = getType();

        //sample sizes
        final long[] sampleSizes = ((SampleSizeBox) stbl.getChild(MP4Constants.BOX_SAMPLE_SIZE)).getSampleSizes();

        //chunk offsets
        final ChunkOffsetBox stco;
        if (stbl.hasChild(MP4Constants.BOX_CHUNK_OFFSET)) stco = (ChunkOffsetBox) stbl.getChild(MP4Constants.BOX_CHUNK_OFFSET);
        else stco = (ChunkOffsetBox) stbl.getChild(MP4Constants.BOX_CHUNK_LARGE_OFFSET);
        final long[] chunkOffsets = stco.getChunks();

        //samples to chunks
        final SampleToChunkBox stsc = ((SampleToChunkBox) stbl.getChild(MP4Constants.BOX_SAMPLE_TO_CHUNK));
        final long[] firstChunks = stsc.getFirstChunks();
        final long[] samplesPerChunk = stsc.getSamplesPerChunk();

        //sample durations/timestamps
        final DecodingTimeToSampleBox stts = (DecodingTimeToSampleBox) stbl.getChild(MP4Constants.BOX_DECODING_TIME_TO_SAMPLE);
        final long[] sampleCounts = stts.getSampleCounts();
        final long[] sampleDeltas = stts.getSampleDeltas();
        final long[] timeOffsets = new long[sampleSizes.length];
        long tmp = 0;
        int off = 0;
        for (int i = 0; i<sampleCounts.length; i++) {
            for (int j = 0; j<sampleCounts[i]; j++) {
                timeOffsets[off+j] = tmp;
                tmp += sampleDeltas[i];
            }
            off += sampleCounts[i];
        }

        //create samples
        int current = 0;
        int lastChunk;
        double timeStamp;
        long offset = 0;
        //iterate over all chunk groups
        for (int i = 0; i<firstChunks.length; i++) {
            if (i<firstChunks.length-1) lastChunk = (int) firstChunks[i+1]-1;
            else lastChunk = chunkOffsets.length;

            //iterate over all chunks in current group
            for (int j = (int) firstChunks[i]-1; j<lastChunk; j++) {
                offset = chunkOffsets[j];

                //iterate over all samples in current chunk
                for (int k = 0; k<samplesPerChunk[i]; k++) {
                    //create samples
                    timeStamp = ((double) timeOffsets[current])/timeScale;
                    frames.add(new Frame(type, offset, sampleSizes[current], timeStamp));
                    offset += sampleSizes[current];
                    current++;
                }
            }
        }

        //frames need not to be time-ordered: sort by timestamp
        //TODO: is it possible to add them to the specific position?
        Collections.sort(frames);
    }

    //TODO: implement other entry descriptors
    protected void findDecoderSpecificInfo(ESDBox esds) {
        final Descriptor ed = esds.getEntryDescriptor();
        final List<Descriptor> children = ed.getChildren();
        List<Descriptor> children2;

        for (Descriptor e : children) {
            children2 = e.getChildren();
            for (Descriptor e2 : children2) {
                switch(e2.getType()) {
                    case Descriptor.TYPE_DECODER_SPECIFIC_INFO:
                        decoderSpecificInfo = (DecoderSpecificInfo) e2;
                        break;
                }
            }
        }
    }

    protected <T> void parseSampleEntry(Box sampleEntry, Class<T> clazz) {
        T type;
        try {
            type = clazz.newInstance();
            if (sampleEntry.getClass().isInstance(type)) {
                System.out.println("true");
            }
        }
        catch(InstantiationException ex) {
            ex.printStackTrace();
        }
        catch(IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    public abstract Type getType();

    public abstract Codec getCodec();

    //tkhd
    /**
     * Returns true if the track is enabled. A disabled track is treated as if
     * it were not present.
     * @return true if the track is enabled
     */
    public boolean isEnabled() {
        return tkhd.isTrackEnabled();
    }

    /**
     * Returns true if the track is used in the presentation.
     * @return true if the track is used
     */
    public boolean isUsed() {
        return tkhd.isTrackInMovie();
    }

    /**
     * Returns true if the track is used in previews.
     * @return true if the track is used in previews
     */
    public boolean isUsedForPreview() {
        return tkhd.isTrackInPreview();
    }

    /**
     * Returns the time this track was created.
     * @return the creation time
     */
    public Date getCreationTime() {
        return Utils.getDate(tkhd.getCreationTime());
    }

    /**
     * Returns the last time this track was modified.
     * @return the modification time
     */
    public Date getModificationTime() {
        return Utils.getDate(tkhd.getModificationTime());
    }

    //mdhd
    /**
     * Returns the language for this media.
     * @return the language
     */
    public Locale getLanguage() {
        return new Locale(mdhd.getLanguage());
    }

    /**
     * Returns true if the data for this track is present in this file (stream).
     * If not, <code>getLocation()</code> returns the URL where the data can be
     * found.
     * @return true if the data is in this file (stream), false otherwise
     */
    public boolean isInFile() {
        return inFile;
    }

    /**
     * If the data for this track is not present in this file (if
     * <code>isInFile</code> returns false), this method returns the data's
     * location. Else null is returned.
     * @return the data's location or null if the data is in this file
     */
    public URL getLocation() {
        return location;
    }

    //info structures
    /**
     * Returns the decoder specific info, if present. It contains configuration
     * data for the decoder. If the decoder specific info is not present, the
     * track contains a <code>DecoderInfo</code>.
     *
     * @see #getDecoderInfo()
     * @return the decoder specific info
     */
    public byte[] getDecoderSpecificInfo() {
        return decoderSpecificInfo.getData();
    }

    /**
     * Returns the <code>DecoderInfo</code>, if present. It contains
     * configuration information for the decoder. If the structure is not
     * present, the track contains a decoder specific info.
     *
     * @see #getDecoderSpecificInfo()
     * @return the codec specific structure
     */
    public DecoderInfo getDecoderInfo() {
        return decoderInfo;
    }

    /**
     * Returns the <code>ProtectionInformation</code> object that contains
     * details about the DRM system used. If no protection is present this
     * method returns null.
     *
     * @return a <code>ProtectionInformation</code> object or null if no
     * protection is used
     */
    public Protection getProtection() {
        return protection;
    }

    //reading
    /**
     * Indicates if there are more frames to be read in this track.
     *
     * @return true if there is at least one more frame to read.
     */
    public boolean hasMoreFrames() {
        return currentFrame<frames.size();
    }

    /**
     * Reads the next frame from this track. If it contains no more frames to
     * read, null is returned.
     *
     * @return the next frame or null if there are no more frames to read
     * @throws IOException if reading fails
     */
    public Frame readNextFrame() throws IOException {
        Frame frame = null;
        if (hasMoreFrames()) {
            frame = frames.get(currentFrame);

            final long diff = frame.getOffset()-in.getOffset();
            if (diff>0) in.skipBytes(diff);
            else if (diff<0) {
                if (in.hasRandomAccess()) in.seek(frame.getOffset());
                else {
                    MP4Constants.LOGGER.warning(new Chars("readNextFrame failed: frame "+currentFrame+" already skipped, offset:"+frame.getOffset()+", stream:"+in.getOffset()));
                    throw new IOException(in, "frame already skipped and no random access");
                }
            }

            final byte[] b = new byte[(int) frame.getSize()];
            try {
                in.readBytes(b);
            }
            catch(EOSException e) {
                MP4Constants.LOGGER.warning(new Chars("readNextFrame failed: tried to read "+frame.getSize()+" bytes at "+in.getOffset()));
                throw e;
            }
            frame.setData(b);
            currentFrame++;
        }
        return frame;
    }

    /**
     * This method tries to seek to the frame that is nearest to the given
     * timestamp. It returns the timestamp of the frame it seeked to or -1 if
     * none was found.
     *
     * @param timestamp a timestamp to seek to
     * @return the frame's timestamp that the method seeked to
     */
    public double seek(double timestamp) {
        //find first frame > timestamp
        Frame frame = null;
        for (int i = 0; i<frames.size(); i++) {
            frame = frames.get(i++);
            if (frame.getTime()>timestamp) {
                currentFrame = i;
                break;
            }
        }
        return (frame==null) ? -1 : frame.getTime();
    }

    /**
     * Returns the timestamp of the next frame to be read. This is needed to
     * read frames from a movie that contains multiple tracks.
     *
     * @return the next frame's timestamp
     */
    double getNextTimeStamp() {
        return frames.get(currentFrame).getTime();
    }
}
