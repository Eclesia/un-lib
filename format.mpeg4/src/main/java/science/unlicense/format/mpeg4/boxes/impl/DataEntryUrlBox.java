package science.unlicense.format.mpeg4.boxes.impl;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4Constants;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.FullBox;

/**
 *
 * @author Alexander Simm
 */
public class DataEntryUrlBox extends FullBox {

    private boolean inFile;
    private String location;

    public DataEntryUrlBox() {
        super("Data Entry Url Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        inFile = (flags&1)==1;
        if (!inFile) location = in.readUTFString((int) getLeft(in), MP4Constants.UTF8);
    }

    public boolean isInFile() {
        return inFile;
    }

    public String getLocation() {
        return location;
    }
}
