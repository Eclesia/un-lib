package science.unlicense.format.mpeg4.boxes.impl;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.FullBox;

/**
 * The chapter box allows to specify individual chapters along the main timeline
 * of a movie. The chapter box occurs within a movie box.
 * Defined in "Adobe Video File Format Specification v10".
 *
 * @author Alexander Simm
 */
public class ChapterBox extends FullBox {

    private final Dictionary chapters;

    public ChapterBox() {
        super("Chapter Box");
        chapters = new HashDictionary();
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        in.skipBytes(4); //??

        final int count = in.read();

        long timestamp;
        int len;
        String name;
        for (int i = 0; i<count; i++) {
            timestamp = in.readBytes(8);
            len = in.read();
            name = in.readString(len);
            chapters.add(timestamp, name);
        }
    }

    /**
     * Returns a map that maps the timestamp of each chapter to its name.
     *
     * @return the chapters
     */
    public Dictionary getChapters() {
        return chapters;
    }
}
