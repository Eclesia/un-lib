package science.unlicense.format.mpeg4.api;

import science.unlicense.format.mpeg4.MP4Constants;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.impl.ESDBox;
import science.unlicense.format.mpeg4.boxes.impl.SampleDescriptionBox;
import science.unlicense.format.mpeg4.boxes.impl.SoundMediaHeaderBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.AudioSampleEntry;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.CodecSpecificBox;
import science.unlicense.format.mpeg4.boxes.Box;

/**
 *
 * @author Alexander Simm
 */
public class AudioTrack extends Track {

    public enum AudioCodec implements Codec {

        AAC,
        AC3,
        AMR,
        AMR_WIDE_BAND,
        EVRC,
        EXTENDED_AC3,
        QCELP,
        SMV,
        UNKNOWN_AUDIO_CODEC;

        static Codec forType(long type) {
            final Codec ac;
            if (type==MP4Constants.BOX_MP4A_SAMPLE_ENTRY) ac = AAC;
            else if (type==MP4Constants.BOX_AC3_SAMPLE_ENTRY) ac = AC3;
            else if (type==MP4Constants.BOX_AMR_SAMPLE_ENTRY) ac = AMR;
            else if (type==MP4Constants.BOX_AMR_WB_SAMPLE_ENTRY) ac = AMR_WIDE_BAND;
            else if (type==MP4Constants.BOX_EVRC_SAMPLE_ENTRY) ac = EVRC;
            else if (type==MP4Constants.BOX_EAC3_SAMPLE_ENTRY) ac = EXTENDED_AC3;
            else if (type==MP4Constants.BOX_QCELP_SAMPLE_ENTRY) ac = QCELP;
            else if (type==MP4Constants.BOX_SMV_SAMPLE_ENTRY) ac = SMV;
            else ac = UNKNOWN_AUDIO_CODEC;
            return ac;
        }
    }
    private final SoundMediaHeaderBox smhd;
    private final AudioSampleEntry sampleEntry;
    private Codec codec;

    public AudioTrack(Box trak, MP4InputStream in) {
        super(trak, in);

        final Box mdia = trak.getChild(MP4Constants.BOX_MEDIA);
        final Box minf = mdia.getChild(MP4Constants.BOX_MEDIA_INFORMATION);
        smhd = (SoundMediaHeaderBox) minf.getChild(MP4Constants.BOX_SOUND_MEDIA_HEADER);

        final Box stbl = minf.getChild(MP4Constants.BOX_SAMPLE_TABLE);

        //sample descriptions: 'mp4a' and 'enca' have an ESDBox, all others have a CodecSpecificBox
        final SampleDescriptionBox stsd = (SampleDescriptionBox) stbl.getChild(MP4Constants.BOX_SAMPLE_DESCRIPTION);
        if (stsd.getChildren().get(0) instanceof AudioSampleEntry) {
            sampleEntry = (AudioSampleEntry) stsd.getChildren().get(0);
            final long type = sampleEntry.getType();
            if (sampleEntry.hasChild(MP4Constants.BOX_ESD)) findDecoderSpecificInfo((ESDBox) sampleEntry.getChild(MP4Constants.BOX_ESD));
            else decoderInfo = DecoderInfo.parse((CodecSpecificBox) sampleEntry.getChildren().get(0));

            if (type==MP4Constants.BOX_ENCRYPTED_AUDIO_SAMPLE_ENTRY||type==MP4Constants.BOX_DRMS_SAMPLE_ENTRY) {
                findDecoderSpecificInfo((ESDBox) sampleEntry.getChild(MP4Constants.BOX_ESD));
                protection = Protection.parse(sampleEntry.getChild(MP4Constants.BOX_PROTECTION_SCHEME_INFORMATION));
                codec = protection.getOriginalFormat();
            }
            else codec = AudioCodec.forType(sampleEntry.getType());
        }
        else {
            sampleEntry = null;
            codec = AudioCodec.UNKNOWN_AUDIO_CODEC;
        }
    }

    @Override
    public Type getType() {
        return Type.AUDIO;
    }

    @Override
    public Codec getCodec() {
        return codec;
    }

    /**
     * The balance is a floating-point number that places mono audio tracks in a
     * stereo space: 0 is centre (the normal value), full left is -1.0 and full
     * right is 1.0.
     *
     * @return the stereo balance for a this track
     */
    public double getBalance() {
        return smhd.getBalance();
    }

    /**
     * Returns the number of channels in this audio track.
     * @return the number of channels
     */
    public int getChannelCount() {
        return sampleEntry.getChannelCount();
    }

    /**
     * Returns the sample rate of this audio track.
     * @return the sample rate
     */
    public int getSampleRate() {
        return sampleEntry.getSampleRate();
    }

    /**
     * Returns the sample size in bits for this track.
     * @return the sample size
     */
    public int getSampleSize() {
        return sampleEntry.getSampleSize();
    }

    public double getVolume() {
        return tkhd.getVolume();
    }
}
