
package science.unlicense.format.mpeg4.boxes.impl.sampleentries;

import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Alexander Simm
 */
public class AudioSampleEntry extends SampleEntry {

    private int channelCount;
    private int sampleSize;
    private int sampleRate;

    public AudioSampleEntry() {
        super("");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        in.skipBytes(8); //reserved
        channelCount = (int) in.readBytes(2);
        sampleSize = (int) in.readBytes(2);
        in.skipBytes(2); //pre-defined: 0
        in.skipBytes(2); //reserved
        sampleRate = (int) in.readBytes(2);
        in.skipBytes(2); //not used by samplerate

        readChildren(in);
    }

    public int getChannelCount() {
        return channelCount;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public int getSampleSize() {
        return sampleSize;
    }
}
