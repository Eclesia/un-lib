package science.unlicense.format.mpeg4.api.codec;

import science.unlicense.format.mpeg4.api.DecoderInfo;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.AVCSpecificBox;
import science.unlicense.format.mpeg4.boxes.impl.sampleentries.codec.CodecSpecificBox;

/**
 *
 * @author Alexander Simm
 */
public class AVCDecoderInfo extends DecoderInfo {

    private final AVCSpecificBox box;

    public AVCDecoderInfo(CodecSpecificBox box) {
        this.box = (AVCSpecificBox) box;
    }

    public int getConfigurationVersion() {
        return box.getConfigurationVersion();
    }

    public int getProfile() {
        return box.getProfile();
    }

    public byte getProfileCompatibility() {
        return box.getProfileCompatibility();
    }

    public int getLevel() {
        return box.getLevel();
    }

    public int getLengthSize() {
        return box.getLengthSize();
    }

    public byte[][] getSequenceParameterSetNALUnits() {
        return box.getSequenceParameterSetNALUnits();
    }

    public byte[][] getPictureParameterSetNALUnits() {
        return box.getPictureParameterSetNALUnits();
    }
}
