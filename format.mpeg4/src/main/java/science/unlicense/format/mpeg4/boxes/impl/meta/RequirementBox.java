package science.unlicense.format.mpeg4.boxes.impl.meta;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg4.MP4InputStream;
import science.unlicense.format.mpeg4.boxes.FullBox;

/**
 *
 * @author Alexander Simm
 */
public class RequirementBox extends FullBox {

    private String requirement;

    public RequirementBox() {
        super("Requirement Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        requirement = in.readString((int) getLeft(in));
    }

    public String getRequirement() {
        return requirement;
    }
}
