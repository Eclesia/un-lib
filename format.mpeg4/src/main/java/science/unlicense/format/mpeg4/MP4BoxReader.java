

package science.unlicense.format.mpeg4;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.format.mpeg4.boxes.Box;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class MP4BoxReader implements MediaReadStream{

    private final Path path;

    public MP4BoxReader(Path path) {
        this.path = path;
    }

    public void read() throws IOException{

        final ByteInputStream is = path.createInputStream();
        final DataInputStream ds = new DataInputStream(is, Endianness.BIG_ENDIAN);

        final byte[] blength = new byte[4];

        while (true){
            Box box = read(ds);
            if (box==null) break;
            System.out.println(box);
        }
    }

    private Box read(DataInputStream ds) throws IOException{
        final int b = ds.read();
        if (b==-1) return null;

        final byte[] blength = new byte[4];
        blength[0] = (byte) b;
        ds.readFully(blength, 1, 3);
        final long length = Endianness.BIG_ENDIAN.readUInt(blength, 0);
        final long type = ds.readUInt();
        final Chars name = new Chars(ds.readFully(new byte[4]));

        final Box box = MP4Constants.createBox(type);
        box.setParams(null, length, type, ds.getByteOffset()-8);

        if (MP4Constants.containsSubBoxes(name)){
            long end = length - 8 + ds.getByteOffset();
            while (end != ds.getByteOffset()){
                final Box child = read(ds);
                box.getChildren().add(child);
            }
        } else {
            box.read(ds);
        }

        return box;
    }

    @Override
    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaPacket next() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
