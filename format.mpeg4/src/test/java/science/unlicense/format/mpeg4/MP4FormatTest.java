

package science.unlicense.format.mpeg4;

import science.unlicense.format.mpeg4.MP4Format;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.media.api.MediaFormat;
import science.unlicense.media.api.Medias;

/**
 * Test MP4 Format declaration.
 *
 * @author Johann Sorel
 */
public class MP4FormatTest {

    @Test
    public void testFormat(){
        final MediaFormat[] formats = Medias.getFormats();
        for (MediaFormat format : formats){
            if (format instanceof MP4Format){
                return;
            }
        }

        Assert.fail("MP4 Format not found.");
    }

}
