package tablegen;

import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;

public class PSHuffmanTables implements PSHuffmanTableData {

    public static void main(String[] args) {
        printTable(HUFF_IID_DEFAULT_DF, "HUFF_IID_DEFAULT_DF");
        printTable(HUFF_IID_DEFAULT_DT, "HUFF_IID_DEFAULT_DT");
        printTable(HUFF_IID_FINE_DF, "HUFF_IID_FINE_DF");
        printTable(HUFF_IID_FINE_DT, "HUFF_IID_FINE_DT");
        printTable(HUFF_ICC_DF, "HUFF_ICC_DF");
        printTable(HUFF_ICC_DT, "HUFF_ICC_DT");
        printTable(HUFF_IPD_DF, "HUFF_IPD_DF");
        printTable(HUFF_IPD_DT, "HUFF_IPD_DT");
        printTable(HUFF_OPD_DF, "HUFF_OPD_DF");
        printTable(HUFF_OPD_DT, "HUFF_OPD_DT");
    }

    private static void printTable(int[][] table, String name) {
        Arrays.sort(table, new TableComparator());

        System.out.println("int[][] " + name + " = {");
        CharBuffer sb;
        for (int i = 0; i < table.length; i++) {
            sb = new CharBuffer();

            //length, codeword, index
            sb.append("{" + table[i][1] + ", " + table[i][2] + ", " + table[i][0] + "}");
            if (i < table.length - 1) {
                sb.append(",");
            }

            System.out.println(sb.toString());
        }
        System.out.println("};");
    }

    private static class TableComparator implements Sorter {

        @Override
        public int sort(Object first, Object second) {
            final int[] i1 = (int[]) first;
            final int[] i2 = (int[]) second;
            if (i1[1] != i2[1]) {
                return i1[1] - i2[1];
            } else {
                return i1[2] - i2[2];
            }
        }
    }
}
