package tablegen;

import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;

public class SBRHuffmanTables implements SBRHuffmanTableData {

    public static void main(String[] args) {
        printTable(T_HUFFMAN_ENV_1_5, "T_HUFFMAN_ENV_1_5");
        printTable(F_HUFFMAN_ENV_1_5, "F_HUFFMAN_ENV_1_5");
        printTable(T_HUFFMAN_ENV_BAL_1_5, "T_HUFFMAN_ENV_BAL_1_5");
        printTable(F_HUFFMAN_ENV_BAL_1_5, "F_HUFFMAN_ENV_BAL_1_5");
        printTable(T_HUFFMAN_ENV_3_0, "T_HUFFMAN_ENV_3_0");
        printTable(F_HUFFMAN_ENV_3_0, "F_HUFFMAN_ENV_3_0");
        printTable(T_HUFFMAN_ENV_BAL_3_0, "T_HUFFMAN_ENV_BAL_3_0");
        printTable(F_HUFFMAN_ENV_BAL_3_0, "F_HUFFMAN_ENV_BAL_3_0");
        printTable(T_HUFFMAN_NOISE_3_0, "T_HUFFMAN_NOISE_3_0");
        printTable(T_HUFFMAN_NOISE_BAL_3_0, "T_HUFFMAN_NOISE_BAL_3_0");
    }

    private static void printTable(int[][] table, String name) {
        Arrays.sort(table, new TableComparator());

        System.out.println("int[][] " + name + " = {");
        CharBuffer cb;
        for (int i = 0; i < table.length; i++) {
            cb = new CharBuffer();

            //length, codeword, index
            cb.append("{" + table[i][1] + ", " + table[i][2] + ", " + table[i][0] + "}");
            if (i < table.length - 1) {
                cb.append(",");
            }

            System.out.println(cb.toString());
        }
        System.out.println("};");
    }

    private static class TableComparator implements Sorter {

        @Override
        public int sort(Object first, Object second) {
            final int[] i1 = (int[]) first;
            final int[] i2 = (int[]) second;
            if (i1[1] != i2[1]) {
                return i1[1] - i2[1];
            } else {
                return i1[2] - i2[2];
            }
        }
    }
}
