
package science.unlicense.format.psd;

import science.unlicense.format.psd.PSDFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class PSDFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final ImageFormat[] formats = Images.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof PSDFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("Image format not found.");
    }

}
