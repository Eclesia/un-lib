
package science.unlicense.format.psd;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * @author Johann Sorel
 */
public class PSDFormat extends AbstractImageFormat {

    public static final PSDFormat INSTANCE = new PSDFormat();

    private PSDFormat() {
        super(new Chars("psd"));
        shortName = new Chars("DICOM");
        longName = new Chars("Digital Imaging and Communications in Medicine");
        mimeTypes.add(new Chars("image/photoshop"));
        mimeTypes.add(new Chars("image/x-photoshop"));
        mimeTypes.add(new Chars("image/psd"));
        mimeTypes.add(new Chars("application/photoshop"));
        mimeTypes.add(new Chars("application/psd"));
        extensions.add(new Chars("psd"));
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new PSDStore(this, source);
    }

}
