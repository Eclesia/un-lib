
package science.unlicense.format.bvh;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Biovision hierarchical data format.
 *
 * Useful links :
 * http://research.cs.wisc.edu/graphics/Courses/cs-838-1999/Jeff/BVH.html
 * http://www.character-studio.net/bvh_file_specification.htm
 * http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.103.2097&rep=rep1&type=pdf
 * http://www.cs.man.ac.uk/~toby/bvh/
 *
 * And public resources :
 * https://sites.google.com/a/cgspeed.com/cgspeed/motion-capture/cmu-bvh-conversion
 *
 * @author Johann Sorel
 */
public class BVHFormat extends AbstractModel3DFormat{

    public static final BVHFormat INSTANCE = new BVHFormat();

    private BVHFormat() {
        super(new Chars("bvh"));
        shortName = new Chars("bvh");
        longName = new Chars("Biovision hierarchical data");
        extensions.add(new Chars("bvh"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new BVHStore(input);
    }

}
