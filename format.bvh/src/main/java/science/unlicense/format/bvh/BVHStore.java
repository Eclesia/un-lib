
package science.unlicense.format.bvh;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import static science.unlicense.format.bvh.BVHConstants.*;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.physics.api.skeleton.Skeleton;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenType;

/**
 *
 * @author Johann Sorel
 */
public class BVHStore extends AbstractModel3DStore{

    private Skeleton skeleton;

    public BVHStore(Object input) {
        super(BVHFormat.INSTANCE,input);
    }

    public Collection getElements() throws StoreException {
        final Collection col = new ArraySequence();

        try {
            read();
            col.add(skeleton);
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        return col;
    }

    private void read() throws IOException{

        final Lexer lexer = new Lexer();
        lexer.setInput(source);
        BVHConstants.prepareLexer(lexer);
        lexer.init();

        Token token = lexer.next();
        if (token.type == TT_HIERARCHY){
            readSkeleton(lexer);
        }

        //TODO parse motion

    }

    private void readSkeleton(Lexer lexer) throws IOException{
        expectNext(lexer, TT_ROOT);

        skeleton = new Skeleton();
        final BVHJoint root = new BVHJoint();
        skeleton.getChildren().add(root);

        final Token next = next(lexer);
        if (next.type == TT_WORD){
            //bone name
            root.setTitle(next.value);
            expectNext(lexer, TT_BRACKET_START);
            readBoneDesc(lexer, root);
        } else if (next.type == TT_BRACKET_START){
            readBoneDesc(lexer, root);
        } else {
            throw new IOException(lexer, "Unexpected token "+next.type.getName());
        }
    }

    private void readBoneDesc(final Lexer lexer, final BVHJoint bone) throws IOException{
        while (true){
            Token next = next(lexer);

            if (next.type == TT_OFFSET){
                final Token offsetX = expectNext(lexer, TT_NUMBER);
                final Token offsetY = expectNext(lexer, TT_NUMBER);
                final Token offsetZ = expectNext(lexer, TT_NUMBER);
                bone.getNodeTransform().getTranslation().setXYZ(
                        Float64.decode(offsetX.value),
                        Float64.decode(offsetY.value),
                        Float64.decode(offsetZ.value));
                bone.getNodeTransform().notifyChanged();
            } else if (next.type == TT_CHANNELS){
                final Token number = expectNext(lexer, TT_NUMBER);
                bone.channels = new Chars[Int32.decode(number.value)];
                for (int i=0;i<bone.channels.length;i++){
                    final Token channel = next(lexer);
                    if (  channel.type != TT_POSITION_X
                      && channel.type != TT_POSITION_Y
                      && channel.type != TT_POSITION_Z
                      && channel.type != TT_ROTATION_X
                      && channel.type != TT_ROTATION_Y
                      && channel.type != TT_ROTATION_Z){
                        throw new IOException(lexer, "Unexpected token "+next.type.getName());
                    }
                    bone.channels[i] = channel.value;
                }

            } else if (next.type == TT_JOINT){
                final BVHJoint child = new BVHJoint();
                bone.getChildren().add(child);

                next = next(lexer);
                if (next.type == TT_WORD){
                    //bone name
                    child.setTitle(next.value);
                    expectNext(lexer, TT_BRACKET_START);
                    readBoneDesc(lexer, child);
                } else if (next.type == TT_BRACKET_START){
                    readBoneDesc(lexer, child);
                } else {
                    throw new IOException(lexer, "Unexpected token "+next.type.getName());
                }
            } else if (next.type == TT_END){
                expectNext(lexer, TT_SITE);
                expectNext(lexer, TT_BRACKET_START);
                BVHJoint end = new BVHJoint();
                end.setTitle(new Chars("End Site"));
                bone.getChildren().add(end);
                readBoneDesc(lexer, end);

            } else if (next.type == TT_BRACKET_END){
                //end of bone desc
                return;
            } else {
                throw new IOException(lexer, "Unexpected token "+next.type.getName());
            }
        }

    }


    private Token expectNext(Lexer lexer, TokenType type) throws IOException{
        Token next = next(lexer);
        if (next.type != type){
            throw new IOException(lexer, "Was expecting a token of type "+type.getName()+" but was "+next.type.getName());
        }
        return next;
    }

    private Token next(Lexer lexer) throws IOException{
        for (;;){
            Token token = lexer.next();
            if (token == null){
                throw new IOException(lexer, "Was expecting more tokens");
            }
            if (token.type != TT_SPACE){
                return token;
            }
        }
    }

}
