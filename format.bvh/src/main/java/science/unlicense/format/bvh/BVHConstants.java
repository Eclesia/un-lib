
package science.unlicense.format.bvh;

import science.unlicense.common.api.character.Chars;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.RegexTokenType;
import science.unlicense.syntax.api.lexer.TokenGroup;
import science.unlicense.syntax.api.lexer.TokenType;

/**
 *
 * @author Johann Sorel
 */
public final class BVHConstants {

    public static final Chars KW_HIERARCHY = Chars.constant("HIERARCHY");
    public static final Chars KW_ROOT = Chars.constant("ROOT");
    public static final Chars KW_JOINT = Chars.constant("JOINT");
    public static final Chars KW_CHANNELS = Chars.constant("CHANNELS");
    public static final Chars KW_OFFSET = Chars.constant("OFFSET");
    public static final Chars KW_END_SITE = Chars.constant("End Site");
    public static final Chars KW_MOTION = Chars.constant("MOTION");
    public static final Chars KW_FRAMES = Chars.constant("Frames");
    public static final Chars KW_FRAME_TIME = Chars.constant("Frame Time");

    public static final Chars KW_POSITION_X = Chars.constant("Xposition");
    public static final Chars KW_POSITION_Y = Chars.constant("Yposition");
    public static final Chars KW_POSITION_Z = Chars.constant("Zposition");
    public static final Chars KW_ROTATION_X = Chars.constant("Xrotation");
    public static final Chars KW_ROTATION_Y = Chars.constant("Yrotation");
    public static final Chars KW_ROTATION_Z = Chars.constant("Zrotation");

    //Lexer token types
    static final TokenType TT_HIERARCHY     = RegexTokenType.keyword(KW_HIERARCHY,KW_HIERARCHY);
    static final TokenType TT_ROOT          = RegexTokenType.keyword(KW_ROOT,KW_ROOT);
    static final TokenType TT_JOINT         = RegexTokenType.keyword(KW_JOINT,KW_JOINT);
    static final TokenType TT_CHANNELS      = RegexTokenType.keyword(KW_CHANNELS,KW_CHANNELS);
    static final TokenType TT_OFFSET        = RegexTokenType.keyword(KW_OFFSET,KW_OFFSET);
    static final TokenType TT_END           = RegexTokenType.keyword(new Chars("End"),new Chars("End"));
    static final TokenType TT_SITE          = RegexTokenType.keyword(new Chars("Site"),new Chars("Site"));
    static final TokenType TT_MOTION        = RegexTokenType.keyword(KW_MOTION,KW_MOTION);
    static final TokenType TT_FRAMES        = RegexTokenType.keyword(KW_FRAMES,KW_FRAMES);
    static final TokenType TT_FRAME         = TT_SITE;
    static final TokenType TT_TIME          = RegexTokenType.keyword(new Chars("Time"),new Chars("Time"));
    static final TokenType TT_POSITION_X    = RegexTokenType.keyword(KW_POSITION_X,KW_POSITION_X);
    static final TokenType TT_POSITION_Y    = RegexTokenType.keyword(KW_POSITION_Y,KW_POSITION_Y);
    static final TokenType TT_POSITION_Z    = RegexTokenType.keyword(KW_POSITION_Z,KW_POSITION_Z);
    static final TokenType TT_ROTATION_X    = RegexTokenType.keyword(KW_ROTATION_X,KW_ROTATION_X);
    static final TokenType TT_ROTATION_Y    = RegexTokenType.keyword(KW_ROTATION_Y,KW_ROTATION_Y);
    static final TokenType TT_ROTATION_Z    = RegexTokenType.keyword(KW_ROTATION_Z,KW_ROTATION_Z);

    static final TokenType TT_DOUBLEDOT     = RegexTokenType.keyword(new Chars(":"),new Chars(":"));
    static final TokenType TT_NUMBER        = RegexTokenType.decimal();
    static final TokenType TT_SPACE         = RegexTokenType.space();
    static final TokenType TT_BRACKET_START = RegexTokenType.keyword(new Chars("{"),new Chars("{"));
    static final TokenType TT_BRACKET_END   = RegexTokenType.keyword(new Chars("}"),new Chars("}"));
    static final TokenType TT_WORD          = RegexTokenType.word();

    static void prepareLexer(Lexer lexer){
        final TokenGroup tts = lexer.getTokenGroup();
        tts.addTokenType(TT_HIERARCHY);
        tts.addTokenType(TT_ROOT);
        tts.addTokenType(TT_JOINT);
        tts.addTokenType(TT_CHANNELS);
        tts.addTokenType(TT_OFFSET);
        tts.addTokenType(TT_END);
        tts.addTokenType(TT_SITE);
        tts.addTokenType(TT_MOTION);
        tts.addTokenType(TT_FRAMES);
        //tts.add(TT_FRAME);
        tts.addTokenType(TT_TIME);
        tts.addTokenType(TT_POSITION_X);
        tts.addTokenType(TT_POSITION_Y);
        tts.addTokenType(TT_POSITION_Z);
        tts.addTokenType(TT_ROTATION_X);
        tts.addTokenType(TT_ROTATION_Y);
        tts.addTokenType(TT_ROTATION_Z);
        tts.addTokenType(TT_DOUBLEDOT);
        tts.addTokenType(TT_NUMBER);
        tts.addTokenType(TT_SPACE);
        tts.addTokenType(TT_BRACKET_START);
        tts.addTokenType(TT_BRACKET_END);
        tts.addTokenType(TT_WORD);
    }

    private BVHConstants() {}

}
