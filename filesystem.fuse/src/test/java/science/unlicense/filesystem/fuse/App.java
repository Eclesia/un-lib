
package science.unlicense.filesystem.fuse;

import java.net.URISyntaxException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.concurrent.api.VirtualPath;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class App {

    public static void main(String[] args) throws URISyntaxException, InterruptedException, IOException {

        final VirtualPath vp = VirtualPath.onRam(new Chars("folder"));
        vp.createContainer();
        final Path file = vp.resolve(new Chars("hello.txt"));
        ByteOutputStream out = file.createOutputStream();
        out.write(new Chars("Hello Fuse!").toBytes());
        out.flush();
        out.close();

        final Path mountPoint = Paths.resolve(new Chars("file:/.../fusepoint2"));
        mountPoint.createContainer();

        final FuseMountPoint mp = new FuseMountPoint(mountPoint,vp);
        mp.enable();

        try{
            Thread.sleep(50000);
        }finally{
            mp.disable();
        }


    }

}
