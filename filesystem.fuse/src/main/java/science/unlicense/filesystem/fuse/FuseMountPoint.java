
package science.unlicense.filesystem.fuse;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class FuseMountPoint {

    private final Path localPath;
    private final FusePathFS mapper;

    /**
     *
     * @param localPath path on disk where the mount point will be made
     * @param mountedPath path containing the datas to be mounted
     */
    public FuseMountPoint(Path localPath, Path mountedPath) {
        this.localPath = localPath;
        mapper = new FusePathFS(mountedPath);
    }

    public void enable() throws URISyntaxException {
        mapper.mount(Paths.get(new URI(localPath.toURI().toString())), true, true, new String[0]);
    }

    public void disable(){
        mapper.umount();
    }

}
