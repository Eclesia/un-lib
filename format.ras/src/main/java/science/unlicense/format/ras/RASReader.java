
package science.unlicense.format.ras;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;

/**
 *
 * @author Johann Sorel
 */
public class RASReader extends AbstractImageReader{

    @Override
    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {

        final DataInputStream ds = new DataInputStream(stream,Endianness.BIG_ENDIAN);

        //read header
        if (!Arrays.equals(RASConstants.SIGNATURE,ds.readFully(new byte[4]))){
            throw new IOException(ds, "Wrong signature, stream is not a valid sun raster image.");
        }
        final RASHeader header = new RASHeader();
        header.read(ds);

        //read colormap
        if (header.colormaptype==RASConstants.COLORMAP_RGB){

        } else if (header.colormaptype==RASConstants.COLORMAP_RAW){

        }

        //read image datas
        throw new UnimplementedException("Not supported yet.");
    }

}
