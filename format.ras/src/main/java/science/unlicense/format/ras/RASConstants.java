
package science.unlicense.format.ras;


/**
 * Sun raster constants.
 *
 * @author Johann Sorel
 */
public final class RASConstants {

    /** file signature */
    public static final byte[] SIGNATURE = new byte[]{(byte) 0x59,(byte) 0xA6,(byte) 0x6A,(byte) 0x95};

    public static final int TYPE_OLD            = 0x0000;
    public static final int TYPE_STANDARD       = 0x0001;
    public static final int TYPE_BYTE_ENCODED   = 0x0002;
    public static final int TYPE_RGB            = 0x0003;
    public static final int TYPE_TIFF           = 0x0004;
    public static final int TYPE_IFF            = 0x0005;
    public static final int TYPE_EXPERIMENTAL   = 0xFFFF;

    public static final int COLORMAP_NONE   = 0x0000;
    public static final int COLORMAP_RGB    = 0x0001;
    public static final int COLORMAP_RAW    = 0x0002;


    private RASConstants(){}

}
