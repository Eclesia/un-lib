
package science.unlicense.format.ras;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * Resources :
 * https://en.wikipedia.org/wiki/Sun_Raster
 * http://www.fileformat.info/format/sunraster/egff.htm
 *
 * @author Johann Sorel
 */
public class RASFormat extends AbstractImageFormat{

    public static final RASFormat INSTANCE = new RASFormat();

    private RASFormat() {
        super(new Chars("ras"));
        shortName = new Chars("RAS");
        longName = new Chars("Sun Raster");
        extensions.add(new Chars("ras"));
        extensions.add(new Chars("sun"));
        signatures.add(RASConstants.SIGNATURE);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new RASStore(this, source);
    }

}
