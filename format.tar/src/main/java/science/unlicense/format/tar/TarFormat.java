

package science.unlicense.format.tar;

import science.unlicense.archive.api.AbstractArchiveFormat;
import science.unlicense.archive.api.Archive;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocumentType;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class TarFormat extends AbstractArchiveFormat {

    private static final DocumentType PARAMETERS_TYPE = new DefaultDocumentType();
    private static final Chars SUFFIX = Chars.constant(".tar");

    public TarFormat() {
        super(new Chars("tar"));
        shortName = new Chars("TAR");
        longName = new Chars("TAR");
        mimeTypes.add(new Chars("application/x-tar"));
        extensions.add(new Chars("tar"));
    }

    @Override
    public DocumentType getParameters() {
        return PARAMETERS_TYPE;
    }

    @Override
    public boolean canDecode(Object candidate) {
        if (candidate instanceof Path) {
            return ((Path) candidate).getName().toLowerCase().endsWith(SUFFIX);
        }
        return false;
    }

    @Override
    public Archive open(Object candidate) throws IOException {
        throw new IOException(candidate, "TODO");
    }

    @Override
    public boolean isAbsolute() {
        return false;
    }

    @Override
    public boolean canCreate(Path base) throws IOException {
        return base.getName().toLowerCase().endsWith(SUFFIX);
    }

    @Override
    public PathResolver createResolver(Path base) throws IOException {
        return open(base);
    }

}
