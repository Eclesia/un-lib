package science.unlicense.format.tar;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.concurrent.api.Formats;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Format;


/**
 * The TarArchive class implements the concept of a tar archive. A tar archive
 * is a series of entries, each of which represents a file system object. Each
 * entry in the archive consists of a header record. Directory entries consist
 * only of the header record, and are followed by entries for the directory's
 * contents. File entries consist of a header record followed by the number of
 * records needed to contain the file's contents. All entries are written on
 * record boundaries. Records are 512 bytes long.
 *
 * TarArchives are instantiated in either read or write mode, based upon whether
 * they are instantiated with an InputStream or an OutputStream. Once
 * instantiated TarArchives read/write mode can not be changed.
 *
 * There is currently no support for random access to tar archives. However, it
 * seems that subclassing TarArchive, and using the
 * TarBuffer.getCurrentRecordNum() and TarBuffer.getCurrentBlockNum() methods,
 * this would be rather trvial.
 *
 * @author Timothy Gerard Endres, <time@gjt.org>
 */
public class TarArchive extends Object {

    protected boolean keepOldFiles;
    protected boolean asciiTranslate;
    protected int userId;
    protected String userName;
    protected int groupId;
    protected String groupName;
    protected String rootPath;
    protected String tempPath;
    protected String pathPrefix;
    protected int recordSize;
    protected byte[] recordBuf;
    protected TarInputStream tarIn;
    protected TarOutputStream tarOut;
    protected TarTransFileTyper transTyper;

    /**
     * The InputStream based constructors create a TarArchive for the purposes
     * of e'x'tracting or lis't'ing a tar archive. Thus, use these constructors
     * when you wish to extract files from or list the contents of an existing
     * tar archive.
     */
    public TarArchive(ByteInputStream inStream) {
        this(inStream, TarBuffer.DEFAULT_BLKSIZE);
    }

    public TarArchive(ByteInputStream inStream, int blockSize) {
        this(inStream, blockSize, TarBuffer.DEFAULT_RCDSIZE);
    }

    public TarArchive(ByteInputStream inStream, int blockSize, int recordSize) {
        this.tarIn = new TarInputStream(inStream, blockSize, recordSize);
        this.initialize(recordSize);
    }

    /**
     * The OutputStream based constructors create a TarArchive for the purposes
     * of 'c'reating a tar archive. Thus, use these constructors when you wish
     * to create a new tar archive and write files into it.
     */
    public TarArchive(ByteOutputStream outStream) {
        this(outStream, TarBuffer.DEFAULT_BLKSIZE);
    }

    public TarArchive(ByteOutputStream outStream, int blockSize) {
        this(outStream, blockSize, TarBuffer.DEFAULT_RCDSIZE);
    }

    public TarArchive(ByteOutputStream outStream, int blockSize, int recordSize) {
        this.tarOut = new TarOutputStream(outStream, blockSize, recordSize);
        this.initialize(recordSize);
    }

    /**
     * Common constructor initialization code.
     */
    private void initialize(int recordSize) {
        this.rootPath = null;
        this.pathPrefix = null;
        this.tempPath = System.getProperty("user.dir");
        this.userId = 0;
        this.userName = "";
        this.groupId = 0;
        this.groupName = "";
        this.keepOldFiles = false;
        this.recordBuf = new byte[this.getRecordSize()];
    }

    /**
     * Set the flag that determines whether existing files are kept, or
     * overwritten during extraction.
     *
     * @param keepOldFiles If true, do not overwrite existing files.
     */
    public void setKeepOldFiles(boolean keepOldFiles) {
        this.keepOldFiles = keepOldFiles;
    }

    /**
     * Set the ascii file translation flag. If ascii file translatio is true,
     * then the MIME file type will be consulted to determine if the file is of
     * type 'text/*'. If the MIME type is not found, then the TransFileTyper is
     * consulted if it is not null. If either of these two checks indicates the
     * file is an ascii text file, it will be translated. The translation
     * converts the local operating system's concept of line ends into the UNIX
     * line end, '\n', which is the defacto standard for a TAR archive. This
     * makes text files compatible with UNIX, and since most tar implementations
     * for other platforms, compatible with most other platforms.
     *
     * @param asciiTranslate If true, translate ascii text files.
     */
    public void setAsciiTranslation(boolean asciiTranslate) {
        this.asciiTranslate = asciiTranslate;
    }

    /**
     * Set the object that will determine if a file is of type ascii text for
     * translation purposes.
     *
     * @param transTyper The new TransFileTyper object.
     */
    public void setTransFileTyper(TarTransFileTyper transTyper) {
        this.transTyper = transTyper;
    }

    /**
     * Set user and group information that will be used to fill in the tar
     * archive's entry headers. Since Java currently provides no means of
     * determining a user name, user id, group name, or group id for a given
     * File, TarArchive allows the programmer to specify values to be used in
     * their place.
     *
     * @param userId The user Id to use in the headers.
     * @param userName The user name to use in the headers.
     * @param groupId The group id to use in the headers.
     * @param groupName The group name to use in the headers.
     */
    public void setUserInfo(
            int userId, String userName,
            int groupId, String groupName) {
        this.userId = userId;
        this.userName = userName;
        this.groupId = groupId;
        this.groupName = groupName;
    }

    /**
     * Get the user id being used for archive entry headers.
     *
     * @return The current user id.
     */
    public int getUserId() {
        return this.userId;
    }

    /**
     * Get the user name being used for archive entry headers.
     *
     * @return The current user name.
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * Get the group id being used for archive entry headers.
     *
     * @return The current group id.
     */
    public int getGroupId() {
        return this.groupId;
    }

    /**
     * Get the group name being used for archive entry headers.
     *
     * @return The current group name.
     */
    public String getGroupName() {
        return this.groupName;
    }

    /**
     * Get the current temporary directory path. Because Java's File did not
     * support temporary files until version 1.2, TarArchive manages its own
     * concept of the temporary directory. The temporary directory defaults to
     * the 'user.dir' System property.
     *
     * @return The current temporary directory path.
     */
    public String getTempDirectory() {
        return this.tempPath;
    }

    /**
     * Set the current temporary directory path.
     *
     * @param path The new temporary directory path.
     */
    public void setTempDirectory(String path) {
        this.tempPath = path;
    }

    /**
     * Get the archive's record size. Because of its history, tar supports the
     * concept of buffered IO consisting of BLOCKS of RECORDS. This allowed tar
     * to match the IO characteristics of the physical device being used. Of
     * course, in the Java world, this makes no sense, WITH ONE EXCEPTION -
     * archives are expected to be propertly "blocked". Thus, all of the
     * horrible TarBuffer support boils down to simply getting the "boundaries"
     * correct.
     *
     * @return The record size this archive is using.
     */
    public int getRecordSize() {
        if (this.tarIn != null) {
            return this.tarIn.getRecordSize();
        } else if (this.tarOut != null) {
            return this.tarOut.getRecordSize();
        }

        return TarBuffer.DEFAULT_RCDSIZE;
    }

    /**
     * Get a path for a temporary file for a given File. The temporary file is
     * NOT created. The algorithm attempts to handle filename collisions so that
     * the name is unique.
     *
     * @return The temporary file's path.
     */
    private String getTempFilePath(File eFile) {
        String pathStr = this.tempPath + File.separator + eFile.getName() + ".tmp";

        for (int i = 1; i < 5; ++i) {
            File f = new File(pathStr);
            if (!f.exists()) {
                break;
            }
            pathStr = this.tempPath + File.separator + eFile.getName() + "-" + i + ".tmp";
        }

        return pathStr;
    }

    /**
     * Close the archive. This simply calls the underlying tar stream's close()
     * method.
     */
    public void closeArchive() throws IOException {
        if (this.tarIn != null) {
            this.tarIn.dispose();
        } else if (this.tarOut != null) {
            this.tarOut.close();
        }
    }

    /**
     * Perform the "list" command and list the contents of the archive. NOTE
     * That this method uses the progress display to actually list the conents.
     * If the progress display is not set, nothing will be listed!
     */
    public void listContents() throws IOException, InvalidHeaderException {
        for (;;) {
            TarEntry entry = this.tarIn.getNextEntry();

            if (entry == null) {
                break;
            }

        }
    }

    /**
     * Perform the "extract" command and extract the contents of the archive.
     *
     * @param destDir The destination directory into which to extract.
     */
    public void extractContents(File destDir) throws IOException, InvalidHeaderException, java.io.IOException {
        for (;;) {
            TarEntry entry = this.tarIn.getNextEntry();
            if (entry == null) {
                break;
            }
            this.extractEntry(destDir, entry);
        }
    }

    /**
     * Extract an entry from the archive. This method assumes that the tarIn
     * stream has been properly set with a call to getNextEntry().
     *
     * @param destDir The destination directory into which to extract.
     * @param entry The TarEntry returned by tarIn.getNextEntry().
     */
    private void extractEntry(File destDir, TarEntry entry) throws IOException, java.io.IOException {

        Chars name = entry.getName();
        name = name.replaceAll('/', File.separatorChar);

        File destFile = new File(destDir, name.toJVMString());

        if (entry.isDirectory()) {
            if (!destFile.exists()) {
                if (!destFile.mkdirs()) {
                    throw new IOException(destFile, "error making directory path '"
                            + destFile.getPath() + "'");
                }
            }
        } else {
            File subDir = new File(destFile.getParent());

            if (!subDir.exists()) {
                if (!subDir.mkdirs()) {
                    throw new IOException(subDir, "error making directory path '"
                            + subDir.getPath() + "'");
                }
            }

            if (this.keepOldFiles && destFile.exists()) {

            } else {
                final boolean asciiTrans = isAscii(destFile);

                try (FileOutputStream out = new FileOutputStream(destFile)) {

                    byte[] rdbuf = new byte[32 * 1024];

                    if (asciiTrans) {
                        try (PrintWriter outw = new PrintWriter(out)) {
                            for (;;) {
                                int numRead = this.tarIn.read(rdbuf);
                                if (numRead == -1) {
                                    break;
                                }
                                for (int off = 0, b = 0; b < numRead; ++b) {
                                    if (rdbuf[ b] == 10) {
                                        String s = new String(rdbuf, off, (b - off));
                                        outw.println(s);
                                        off = b + 1;
                                    }
                                }
                            }
                        }
                    } else {
                        for (;;) {
                            int numRead = this.tarIn.read(rdbuf);
                            if (numRead == -1) {
                                break;
                            }
                            out.write(rdbuf, 0, numRead);
                        }
                    }
                }
            }
        }
    }

    private boolean isAscii(File eFile) {
        if (!asciiTranslate) return false;

        boolean asciiTrans = false;

        final Format format = Formats.findFormat(Paths.resolve(new Chars(eFile.getPath())));
        if (format!=null) {
            final Iterator ite = format.getMimeTypes().createIterator();
            while (ite.hasNext()) {
                Chars mime = (Chars) ite.next();
                if (mime.getFirstOccurence(new Chars("text"))>=0) {
                    asciiTrans = true;
                    break;
                }
            }
        }
        if (!asciiTrans && transTyper!=null) {
            asciiTrans = transTyper.isAsciiFile(eFile);
        }

        return asciiTrans;
    }

    /**
     * Write an entry to the archive. This method will call the putNextEntry()
     * and then write the contents of the entry, and finally call closeEntry()
     * for entries that are files. For directories, it will call putNextEntry(),
     * and then, if the recurse flag is true, process each entry that is a child
     * of the directory.
     *
     * @param oldEntry The TarEntry representing the entry to write to the archive.
     * @param recurse If true, process the children of directory entries.
     */
    public void writeEntry(TarEntry oldEntry, boolean recurse) throws IOException, java.io.IOException {
        boolean unixArchiveFormat = oldEntry.isUnixTarFormat();

        File tFile = null;
        File eFile = oldEntry.getFile();

        // Work on a copy of the entry so we can manipulate it.
        // Note that we must distinguish how the entry was constructed.
        //
        TarEntry entry = new TarEntry(oldEntry);

        if (this.asciiTranslate && !entry.isDirectory()) {

            if (isAscii(eFile)) {
                String tempFileName = this.getTempFilePath(eFile);
                tFile = new File(tempFileName);

                final BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(eFile)));
                try {
                    final BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(tFile));
                    try {
                        for (;;) {
                            String line = in.readLine();
                            if (line == null) {
                                break;
                            }

                            out.write(line.getBytes());
                            out.write((byte) '\n');
                        }
                        out.flush();
                    } finally {
                        out.close();
                    }
                } finally {
                    in.close();
                }

                entry.setSize(tFile.length());

                eFile = tFile;
            }
        }

        String newName = null;

        if (this.rootPath != null) {
            if (entry.getName().startsWith(new Chars(this.rootPath))) {
                newName = ""+entry.getName().truncate(this.rootPath.length() + 1,-1);
            }
        }

        if (this.pathPrefix != null) {
            newName = (newName == null)
                    ? this.pathPrefix + "/" + entry.getName()
                    : this.pathPrefix + "/" + newName;
        }

        if (newName != null) {
            entry.setName(new Chars(newName));
        }

        this.tarOut.putNextEntry(entry);

        if (entry.isDirectory()) {
            if (recurse) {
                TarEntry[] list = entry.getDirectoryEntries();

                for (int i = 0; i < list.length; ++i) {
                    TarEntry dirEntry = list[i];

                    if (unixArchiveFormat) {
                        dirEntry.setUnixTarFormat();
                    }

                    this.writeEntry(dirEntry, recurse);
                }
            }
        } else {
            final FileInputStream in = new FileInputStream(eFile);
            try {
                byte[] eBuf = new byte[32 * 1024];
                for (;;) {
                    int numRead = in.read(eBuf, 0, eBuf.length);

                    if (numRead == -1) {
                        break;
                    }

                    this.tarOut.write(eBuf, 0, numRead);
                }
            } finally {
                in.close();
            }

            if (tFile != null) {
                tFile.delete();
            }

            this.tarOut.closeEntry();
        }
    }
}
