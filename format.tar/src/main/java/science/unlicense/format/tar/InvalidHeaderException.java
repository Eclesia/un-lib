package science.unlicense.format.tar;

import science.unlicense.encoding.api.io.IOException;

/**
 * This exception is used to indicate that there is a problem with a TAR archive
 * header.
 */
public class InvalidHeaderException extends IOException {

    public InvalidHeaderException() {
        super(null,(String) null);
    }

    public InvalidHeaderException(String msg) {
        super(null, msg);
    }
}
