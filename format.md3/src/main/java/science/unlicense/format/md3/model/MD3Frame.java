

package science.unlicense.format.md3.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Animation frame properties.
 *
 * @author Johann Sorel
 */
public class MD3Frame {

    public VectorRW bboxMin;
    public VectorRW bboxMax;
    public VectorRW origin;
    public float radius;
    public Chars name;

    public void read(DataInputStream ds) throws IOException{
        bboxMin = VectorNf64.create(ds.readFloat(3));
        bboxMax = VectorNf64.create(ds.readFloat(3));
        origin  = VectorNf64.create(ds.readFloat(3));
        radius  = ds.readFloat();
        name    = ds.readBlockZeroTerminatedChars(16, CharEncodings.US_ASCII);
    }


}
