

package science.unlicense.format.md3.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.Maths;

/**
 *
 * @author Johann Sorel
 */
public class MD3Vertex {

    /** 3xShort : multiply by 1/64 for real coordinate */
    public short[] coords;
    /** 2xByte : spherical coordinate 255 = 2PI*/
    public int[] normals;

    public void read(DataInputStream ds) throws IOException{
        coords = ds.readShort(3);
        normals = ds.readUByte(2);
    }

    public float[] normalVector(){
        final double lat = normals[0] * Maths.TWO_PI / 255.0;
        final double lng = normals[1] * Maths.TWO_PI / 255.0;
        return new float[]{
            (float) (Math.cos(lng) * Math.sin(lat)),
            (float) (Math.sin(lng) * Math.sin(lat)),
            (float) (Math.cos(lat))
        };
    }

}
