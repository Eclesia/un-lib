

package science.unlicense.format.md3.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Someking of anchor point.
 *
 * @author Johann Sorel
 */
public class MD3Tag {

    public Chars name;
    public VectorRW origin;
    public Matrix3x3 rotation;

    public void read(DataInputStream ds) throws IOException{
        name    = ds.readBlockZeroTerminatedChars(64, CharEncodings.US_ASCII);
        origin  = VectorNf64.create(ds.readFloat(3));
        final float[] r = ds.readFloat(9);
        rotation = new Matrix3x3();
        rotation.set(0, 0, r[0]);
        rotation.set(0, 1, r[1]);
        rotation.set(0, 2, r[2]);
        rotation.set(1, 0, r[3]);
        rotation.set(1, 1, r[4]);
        rotation.set(1, 2, r[5]);
        rotation.set(2, 0, r[6]);
        rotation.set(2, 1, r[7]);
        rotation.set(2, 2, r[8]);
    }
}
