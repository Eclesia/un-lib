
package science.unlicense.format.md3;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Read QC shader file.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class QCReader extends AbstractReader {


    public Chars readTexture() throws IOException{

        final CharInputStream cs = getInputAsCharStream(CharEncodings.US_ASCII);

        Chars textureFile = null;
        for (Chars line=cs.readLine();line!=null;line=cs.readLine()){
            if (line.startsWith(MD3Constants.QC_SKIN)){
                textureFile = line.split(' ')[1].trim();
                break;
            }
        }

        return textureFile;
    }


}
