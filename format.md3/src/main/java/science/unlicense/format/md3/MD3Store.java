

package science.unlicense.format.md3;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.RenderState;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.Technique;
import science.unlicense.format.md3.model.MD3Frame;
import science.unlicense.format.md3.model.MD3Header;
import science.unlicense.format.md3.model.MD3Surface;
import science.unlicense.format.md3.model.MD3Tag;
import science.unlicense.format.md3.model.MD3Vertex;

/**
 *
 * Useful resource :
 * http://projets.developpez.com/projects/castor3d
 *
 * @author Johann Sorel
 */
public class MD3Store extends AbstractModel3DStore{

    private MD3Header header;
    private MD3Frame[] frames;
    private MD3Tag[] tags;
    private MD3Surface[] surfaces;

    public MD3Store(Object input) {
        super(MD3Format.INSTANCE,input);
    }

    public Collection getElements() throws StoreException {
        final Collection col = new ArraySequence();

        try {
            read();
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        MotionModel mpm = rebuildModel();
        if (mpm!=null){
            col.add(mpm);
        }

        return col;
    }

    private void read() throws IOException{
        if (header!=null) return;

        final ByteInputStream bi = getSourceAsInputStream();
        final BacktrackInputStream bs = new BacktrackInputStream(bi);
        bs.mark();
        final DataInputStream ds = new DataInputStream(bs,Endianness.LITTLE_ENDIAN);

        //read header
        header = new MD3Header();
        header.read(ds);

        //read frames
        bs.rewind();
        ds.skipFully(header.offsetFrames);
        frames = new MD3Frame[header.nbFrames];
        for (int i=0;i<header.nbFrames;i++){
            frames[i] = new MD3Frame();
            frames[i].read(ds);
        }

        //read tags
        bs.rewind();
        ds.skipFully(header.offsetTags);
        tags = new MD3Tag[header.nbTags];
        for (int i=0;i<header.nbTags;i++){
            tags[i] = new MD3Tag();
            tags[i].read(ds);
        }

        //read surfaces
        bs.rewind();
        ds.skipFully(header.offsetSurfaces);
        surfaces = new MD3Surface[header.nbSurfaces];
        for (int i=0;i<header.nbSurfaces;i++){
            surfaces[i] = new MD3Surface();
            surfaces[i].read(bs,ds);
        }

    }

    private MotionModel rebuildModel(){
        if (header==null) return null;

        final MotionModel mpm = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);

        for (int i=0;i<surfaces.length;i++){
            final MD3Surface surface = surfaces[i];

            final DefaultModel mesh = new DefaultModel();
            mesh.getTechniques().add(new SimpleBlinnPhong());
            mpm.getChildren().add(mesh);
            final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
            mesh.setShape(shell);
            ((Technique) mesh.getTechniques().get(0)).getState().setCulling(RenderState.CULLING_FRONT);

            //TODO thre are multiple frames, we should rebuild then as animation
            //limit to one for now
            final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(surface.nbVertices*3).cursor();
            final Float32Cursor normals = DefaultBufferFactory.INSTANCE.createFloat32(surface.nbVertices*3).cursor();
            for (int k=0;k<surface.nbVertices;k++){
                final MD3Vertex vertex = surface.vertices[k];
                vertices.write(vertex.coords[0]/64f);
                vertices.write(vertex.coords[1]/64f);
                vertices.write(vertex.coords[2]/64f);
                normals.write(vertex.normalVector());
            }
            shell.setPositions(new VBO(vertices.getBuffer(), 3));
            shell.setNormals(new VBO(normals.getBuffer(), 3));

            //rebuild UVS
            final Float32Cursor uvs = DefaultBufferFactory.INSTANCE.createFloat32(surface.nbVertices*2).cursor();
            for (int k=0;k<surface.nbVertices;k++){
                uvs.write( surface.sts[k*2+0]);
                uvs.write( surface.sts[k*2+1]);
            }
            shell.setUVs(new VBO(uvs.getBuffer(), 2));

            //rebuild indices
            final IBO ibo = new IBO(surface.triangles);
            shell.setIndex(ibo);
            shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) ibo.getPrimitiveBuffer().getNumbersSize())});

            //rebuild texture
            if (surface.shaders.length>0){
                Chars shaderFile = surface.shaders[0].name;
                final int index = shaderFile.getLastOccurence(new Char('/'));
                if (index>=0){
                    shaderFile = shaderFile.truncate(index, shaderFile.getCharLength());
                }

                Chars textureName = shaderFile;
                if (shaderFile.endsWith(new Chars("qc"))){
                    //shader path
                    final Path shaderPath = ((Path) source).getParent().resolve(shaderFile);
                    try{
                        if (shaderPath!=null){
                            final QCReader reader = new QCReader();
                            reader.setInput(shaderPath);
                            textureName = reader.readTexture();
                        }
                    }catch(IOException ex){
                        getLogger().warning(ex);
                    }
                }

                //read image
                final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
                mesh.getMaterials().add(material);

                if (textureName!=null){
                    try{
                        final Path texturePath = ((Path) source).getParent().resolve(textureName);
                        final Image image = Images.read(texturePath);
                        if (image!=null){
                            material.setDiffuseTexture(new TextureMapping(new Texture2D(image)));
                        }
                    }catch(IOException ex){
                        getLogger().warning(ex);
                    }
                }
            }
            mesh.getShape().getBoundingBox();

        }

        mpm.getNodeTransform().getRotation().set(Matrix3x3.createRotation3(Angles.degreeToRadian(-90), new Vector3f64(1,0,0)));
        mpm.getNodeTransform().notifyChanged();

        return mpm;
    }

}
