

package science.unlicense.format.md3;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * Useful docs :
 * http://en.wikipedia.org/wiki/MD3_(file_format)
 * http://www.icculus.org/~phaethon/q3/collection.html
 *
 * @author Johann Sorel
 */
public class MD3Format extends AbstractModel3DFormat{

    public static final MD3Format INSTANCE = new MD3Format();

    private MD3Format() {
        super(new Chars("MD3"));
        shortName = new Chars("MD3");
        longName = new Chars("MD3");
        extensions.add(new Chars("md3"));
        signatures.add(MD3Constants.SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new MD3Store(input);
    }

}
