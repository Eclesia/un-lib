

package science.unlicense.format.md3.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Shader external file information.
 *
 * @author Johann Sorel
 */
public class MD3Shader {

    public Chars name;
    public int index;

    public void read(DataInputStream ds) throws IOException{
        name   = ds.readBlockZeroTerminatedChars(64, CharEncodings.US_ASCII);
        index  = ds.readInt();
    }
}
