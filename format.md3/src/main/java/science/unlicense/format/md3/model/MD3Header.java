

package science.unlicense.format.md3.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.md3.MD3Constants;

/**
 *
 * @author Johann Sorel
 */
public class MD3Header {

    /** Int32 */
    public int version;
    /** 0 terminated string, at most 64chars */
    public Chars name;
    /** Int32 */
    public int flags;
    /** Int32 maximum of 1024 */
    public int nbFrames;
    /** Int32 maximum of 16 */
    public int nbTags;
    /** Int32 maximum of 32 */
    public int nbSurfaces;
    /** Int32 */
    public int nbSkins;
    /** Int32 offset to first frame */
    public int offsetFrames;
    /** Int32 offset to first tag */
    public int offsetTags;
    /** Int32 offset to first surface */
    public int offsetSurfaces;
    /** Int32 offset to eof */
    public int offsetEndOfFile;

    public void read(DataInputStream ds) throws IOException {

        final byte[] sign = ds.readFully(new byte[4]);
        if (!Arrays.equals(sign, MD3Constants.SIGNATURE)){
            throw new IOException(ds, "Not an MD3 file");
        }

        version         = ds.readInt();
        name            = ds.readBlockZeroTerminatedChars(64, CharEncodings.US_ASCII);
        flags           = ds.readInt();
        nbFrames        = ds.readInt();
        nbTags          = ds.readInt();
        nbSurfaces      = ds.readInt();
        nbSkins         = ds.readInt();
        offsetFrames    = ds.readInt();
        offsetTags      = ds.readInt();
        offsetSurfaces  = ds.readInt();
        offsetEndOfFile = ds.readInt();
    }

}
