

package science.unlicense.format.md3;

import science.unlicense.common.api.character.Chars;

/**
 * MD3 constants
 *
 * @author Johann Sorel
 */
public class MD3Constants {

    public static final byte[] SIGNATURE = new byte[]{'I','D','P','3'};

    public static final Chars QC_MODEL = Chars.constant("$model");
    public static final Chars QC_FRAMES = Chars.constant("$frames");
    public static final Chars QC_FLAGS = Chars.constant("$flags");
    public static final Chars QC_NUMSKINS = Chars.constant("$numskins");
    public static final Chars QC_MESH = Chars.constant("$mesh");
    public static final Chars QC_SKIN = Chars.constant("$skin");

    private MD3Constants(){}

}
