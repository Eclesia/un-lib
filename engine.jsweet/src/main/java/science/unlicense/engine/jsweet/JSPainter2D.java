
package science.unlicense.engine.jsweet;

import jsweet.dom.CanvasGradient;
import jsweet.dom.CanvasPattern;
import jsweet.dom.CanvasRenderingContext2D;
import jsweet.dom.HTMLCanvasElement;
import static jsweet.util.Globals.union;
import jsweet.util.StringTypes;
import jsweet.util.union.Union3;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.painter2d.AbstractPainter2D;
import science.unlicense.display.api.painter2d.Brush;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.LinearGradientPaint;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.display.api.painter2d.RadialGradientPaint;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector4f64;

/**
 *
 * @author Johann Sorel
 */
public class JSPainter2D extends AbstractPainter2D {

    private final HTMLCanvasElement canvas;
    private final CanvasRenderingContext2D g;

    public JSPainter2D(HTMLCanvasElement canvas) {
        this.canvas = canvas;
        g = canvas.getContext(StringTypes._2d);
    }

    @Override
    public void setClip(PlanarGeometry geom) {
        super.setClip(geom);
        if (geom!=null) {
            g.setTransform(1, 0, 0, 1, 0, 0);
            pushGeometry(TransformedGeometry.create(geom, transform));
            g.clip();
        } else {
            Rectangle rect = new Rectangle(0, 0, canvas.width, canvas.height);
            pushGeometry(rect);
            g.clip();
        }
    }

    @Override
    public void fill(CharArray text, float x, float y) {
        final Union3<String, CanvasGradient, CanvasPattern> fill = toPaint(fillPaint);
        pushFont(font);
        pushBlending(alphaBlending);
        g.fillStyle = fill;
        pushTransform(transform);
        g.fillText(text.toString(), x, y);
    }

    @Override
    public void fill(PlanarGeometry geom) {
        final Union3<String, CanvasGradient, CanvasPattern> fill = toPaint(fillPaint);

        if (geom!=null && fill!=null) {
            pushTransform(transform);
            pushBlending(alphaBlending);
            g.fillStyle = fill;
            pushGeometry(geom);
            g.fill();
        }

    }

    @Override
    public void stroke(PlanarGeometry geom) {
        final Union3<String, CanvasGradient, CanvasPattern> fill = toPaint(fillPaint);

        if (geom!=null && fill!=null) {
            pushTransform(transform);
            pushBlending(alphaBlending);
            pushGeometry(geom);
            g.strokeStyle = fill;
            pushStroke(brush);
            g.stroke();
        }
    }

    @Override
    public void paint(Image image, Affine trs) {
        final HTMLCanvasElement img = toImage(image);
        if (trs==null) trs = new Affine2(); //TODO not used

        if (img!=null) {
            pushTransform(transform);
            pushBlending(alphaBlending);
            g.drawImage(img, 0, 0);
        }
    }

    @Override
    public FontStore getFontStore() {
        return null; //TODO
    }

    @Override
    public void dispose() {
    }

    private Union3<String, CanvasGradient, CanvasPattern> toPaint(science.unlicense.display.api.painter2d.Paint paint) {
        if (paint==null) return null;

        Union3<String, CanvasGradient, CanvasPattern> p;
        if (paint instanceof ColorPaint) {
            String str = toCSSColor(((ColorPaint) paint).getColor());
            p = union(str);
        } else if (paint instanceof LinearGradientPaint) {

            final LinearGradientPaint lgp = (LinearGradientPaint) paint;
            final CanvasGradient cg = g.createLinearGradient(lgp.getStartX(), lgp.getStartY(), lgp.getEndX(),lgp.getEndY());
            final Color[] colors = lgp.getStopColors();
            final double[] positions = lgp.getStopOffsets();
            for (int i=0;i<colors.length;i++) {
                cg.addColorStop(positions[i], toCSSColor(colors[i]));
            }
            p = union(cg);
        } else if (paint instanceof RadialGradientPaint) {
            final RadialGradientPaint lgp = (RadialGradientPaint) paint;
            final CanvasGradient cg = g.createRadialGradient(lgp.getCenterX(), lgp.getCenterY(), lgp.getRadius(), lgp.getFocusX(), lgp.getFocusY(), lgp.getRadius());
            final Color[] colors = lgp.getStopColors();
            final double[] positions = lgp.getStopOffsets();
            for (int i=0;i<colors.length;i++) {
                cg.addColorStop(positions[i], toCSSColor(colors[i]));
            }
            p = union(cg);
        } else {
            throw new InvalidArgumentException("Unexpected type : "+paint.getClass());
        }
        return p;
    }

    private HTMLCanvasElement toImage(Image image) {
        if (image==null) return null;

        //format can not be supported directly in opengl.
        //convert it to a RGBA buffer.
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final TupleGrid pb = image.getColorModel().asTupleBuffer(image);
        final int[] coord = new int[2];

        throw new UnimplementedException("todo");
//        final HTMLCanvasElement ele = new HTMLCanvasElement();
//        ele.width = width;
//        ele.height = height;
//        final CanvasRenderingContext2D ctx = ele.getContext(StringTypes._2d);
//        final ImageData imgData = ctx.createImageData(width,height);
//
//        for (int y = 0; y <height; y++) {
//            coord[1] = y;
//            for (int x = 0; x < width; x++) {
//                coord[0] = x;
//                Color color = pb.getColor(coord);
//                int off = (y*width + x)*4;
//                imgData.data.set(off+0, color.getAlpha()*255);
//                imgData.data.set(off+1, color.getRed()*255);
//                imgData.data.set(off+2, color.getGreen()*255);
//                imgData.data.set(off+3, color.getBlue()*255);
//            }
//        }
//        ctx.putImageData(imgData, 0, 0);
//
//        return ele;
    }

    private String toCSSColor(science.unlicense.image.api.color.Color color) {
        final int argb = color.toARGB();
        return "rgb(" + (argb >> 16 & 0xFF) + "," + (argb >> 8 & 0xFF) + "," + (argb & 0xFF) + ")";
    }

    private void pushTransform(Affine2 affine) {
        g.setTransform(
                affine.getM00(), affine.getM10(),
                affine.getM01(), affine.getM11(),
                affine.getM02(), affine.getM12());
    }

    private void pushGeometry(PlanarGeometry geom) {
        g.beginPath();
        if (geom==null) return;

        if (geom instanceof Point) {
            final Point p = (Point) geom;
            final TupleRW coord = p.getCoordinate();
            g.moveTo(coord.get(0), coord.get(1));
        } else {
            final PathIterator ite = geom.createPathIterator();

            final Vector4f64 p = new Vector4f64();
            final Vector4f64 c1 = new Vector4f64();
            final Vector4f64 c2 = new Vector4f64();
            while (ite.next()) {
                final int type = ite.getType();
                if (type==PathIterator.TYPE_MOVE_TO) {
                    ite.getPosition(p);
                    g.moveTo(p.get(0), p.get(1));
                } else if (type==PathIterator.TYPE_LINE_TO) {
                    ite.getPosition(p);
                    g.lineTo(p.get(0), p.get(1));
                } else if (type==PathIterator.TYPE_ARC) {
                    ite.getPosition(p);
                    g.lineTo(p.get(0), p.get(1));
                } else if (type==PathIterator.TYPE_CUBIC) {
                    ite.getPosition(p);
                    ite.getFirstControl(c1);
                    ite.getSecondControl(c2);
                    g.bezierCurveTo(c1.get(0), c1.get(1), c2.get(0), c2.get(1), p.get(0), p.get(1));
                } else if (type==PathIterator.TYPE_QUADRATIC) {
                    ite.getPosition(p);
                    ite.getFirstControl(c1);
                    g.quadraticCurveTo(c1.get(0), c1.get(1), p.get(0), p.get(1));
                } else if (type==PathIterator.TYPE_CLOSE) {
                    g.closePath();
                }
            }
        }
    }

    private void pushStroke(Brush brush) {

        if (brush instanceof PlainBrush) {
            final PlainBrush b = (PlainBrush) this.brush;
            final int cap = b.getLineCap();
            final int join = b.getLineJoin();
            final float[] dashes = b.getDashes();
            final float offset = b.getOffset();
            final float width = b.getWidth();
            final float miterLimit = b.getMiterLimit();

            g.lineCap = (cap==PlainBrush.LINECAP_BUTT) ? "butt" : (cap==PlainBrush.LINECAP_ROUND) ? "round" : "square";
            g.lineJoin = (join==PlainBrush.LINEJOIN_BEVEL) ? "bevel" : (join==PlainBrush.LINEJOIN_ROUND) ? "round" : "miter";
            g.setLineDash(Arrays.reformatDouble(dashes));
            g.lineDashOffset = offset;
            g.lineWidth = width;
            g.miterLimit = miterLimit;

        } else {
            throw new InvalidArgumentException("Unexpected type : "+brush.getClass());
        }
    }

    private void pushFont(FontChoice font) {
        final String family = font.getFamilies()[0].toString();
        final int weight = font.getWeight();
        final float size = font.getSize();
        g.font = ""+(int) size+"px "+family;
    }

    private void pushBlending(AlphaBlending blending) {
        g.globalAlpha = blending.getAlpha();
        final int type = blending.getType();
        switch (type) {

            case AlphaBlending.CLEAR :      throw new InvalidArgumentException("Unsupported type : "+type);
            case AlphaBlending.XOR :        g.globalCompositeOperation = "xor"; break;
            case AlphaBlending.DST :        throw new InvalidArgumentException("Unsupported type : "+type);
            case AlphaBlending.DST_ATOP :   g.globalCompositeOperation = "destination-atop"; break;
            case AlphaBlending.DST_IN :     g.globalCompositeOperation = "destination-in"; break;
            case AlphaBlending.DST_OUT :    g.globalCompositeOperation = "destination-out"; break;
            case AlphaBlending.DST_OVER :   g.globalCompositeOperation = "destination-over"; break;
            case AlphaBlending.SRC :        g.globalCompositeOperation = "copy"; break;
            case AlphaBlending.SRC_ATOP :   g.globalCompositeOperation = "source-atop"; break;
            case AlphaBlending.SRC_IN :     g.globalCompositeOperation = "source-in"; break;
            case AlphaBlending.SRC_OUT :    g.globalCompositeOperation = "source-out"; break;
            case AlphaBlending.SRC_OVER :   g.globalCompositeOperation = "source-over"; break;
            default : throw new InvalidArgumentException("Unexpected type : "+type);
        }

    }

}
