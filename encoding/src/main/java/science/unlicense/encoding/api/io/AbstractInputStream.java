package science.unlicense.encoding.api.io;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.math.api.Maths;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractInputStream extends CObject implements ByteInputStream{

    private byte[] bb = null;

    @Override
    public int read(byte[] buffer) throws IOException {
        return read(buffer,0,buffer.length);
    }

    @Override
    public int read(byte[] buffer, int offset, int length) throws IOException {
        int position = offset;
        int nb = -1;
        for (int n = offset+length; position<n; position++) {
            final int b = read();
            if (b == -1) {
                break;
            }
            nb += (nb==-1) ? 2:1;
            buffer[position] = (byte) b;
        }
        return nb;
    }

    @Override
    public int read(Buffer buffer, int offset, int length) throws IOException {
        if (bb==null) bb = new byte[4096];

        int nbRead = read(bb, 0, Maths.min(bb.length,length));
        if (nbRead == -1) return nbRead;
        buffer.writeInt8(bb, 0, nbRead, offset);

        return nbRead;
    }

    @Override
    public long skip(long n) throws IOException {
        if (n<=0) return 0;
        long toRead = n;
        final byte[] buffer = new byte[256];
        while (toRead > 0) {
            final int nbRead = read(buffer, 0, (int) Math.min(256, toRead));
            if (nbRead<0) {
                if (toRead==n) {
                    //end of stream
                    return nbRead;
                }
                break;
            }
            toRead -= nbRead;
        }
        return n-toRead;
    }

}
