
package science.unlicense.encoding.api.path;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.model.tree.AbstractNode;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.SeekableByteBuffer;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractPath extends AbstractNode implements Path {

    public AbstractPath() {
    }

    @Override
    public boolean canHaveChildren() {
        try {
            return isContainer();
        }catch(IOException ex) {
            return false;
        }
    }

    @Override
    public Object getPathInfo(Chars key) {
        return null;
    }

    @Override
    public long getSize() {
        final Number num = (Number) getPathInfo(INFO_OCTETSIZE);
        if (num == null) return -1;
        return num.longValue();
    }

    @Override
    public Collection getChildren(Predicate filter) {
        return Collections.filter(getChildren(), filter);
    }

    @Override
    public void delete() throws IOException {
        throw new IOException(this, "Delete not supported.");
    }

    @Override
    public final int getHash() {
        return toURI().hashCode();
    }

    @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof Path) {
            return CObjects.equals( ((Path) obj).toURI(), this.toURI());
        }

        return false;
    }

    @Override
    public SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException {
        throw new IOException(this, "Not supported.");
    }

    @Override
    public Chars toChars() {
        return getName();
    }

}
