package science.unlicense.encoding.api.io;

/**
 * Byte input stream backed by a byte array.
 *
 * @author Johann Sorel
 */
public class ArrayInputStream extends AbstractInputStream{

    private final byte[] array;
    private final int end;
    private int index = 0;

    public ArrayInputStream(byte[] array) {
        this(array,0,array.length);
    }

    public ArrayInputStream(byte[] array, int start, int length) {
        this.array = array;
        this.index = start;
        this.end = start + length;
    }

    /**
     * Current index in the array.
     * @return
     */
    public int getIndex() {
        return index;
    }

    @Override
    public int read() throws IOException {
        if (index>=end) {
            return -1;
        }
        final int b = array[index] & 0xff; //unsign it
        index++;
        return b;
    }

    public int remaining() {
        return end-index;
    }

    @Override
    public void dispose() throws IOException {
    }

}
