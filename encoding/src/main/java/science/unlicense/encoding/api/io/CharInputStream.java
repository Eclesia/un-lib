
package science.unlicense.encoding.api.io;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.ByteSequence;

/**
 * Character input stream.
 * Wraps a ByteInpustStream and provide methods for Characters reading.
 *
 * @author Johann Sorel
 */
public class CharInputStream extends AbstractInputStream{

    private static final int BUFFER_SIZE = 4096*4;

    private final ByteInputStream in;
    private final CharEncoding encoding;
    private final byte[] elb;

    //store several bytes ahead
    private final byte[] array = new byte[BUFFER_SIZE];
    private boolean finished = false;
    private int offset = 0;
    private int remain = 0;

    //next char info
//    private byte[] charArr = new byte[12];
//    private int charOffset = charArr.length;
    private int charSize = 0;
//    private boolean hasNext = false;

    private final ByteSequence lineBuffer = new ByteSequence();

    /**
     * New character input stream with '\n' end of line character.
     *
     * @param in wrapped byte input stream, not null
     * @param encoding Character encoding to read in, not null
     */
    public CharInputStream(final ByteInputStream in, final CharEncoding encoding) {
        this(in,encoding,new Char('\n'));
    }

    /**
     * New character input stream.
     *
     * @param in wrapped byte input stream, not null
     * @param encoding Character encoding to read in, not null
     * @param endline End of line character, not null
     */
    public CharInputStream(ByteInputStream in, CharEncoding encoding, Char endline) {
        this.in = in;
        this.encoding = encoding;
        this.elb = endline.toBytes();
        if (elb.length != 1) throw new RuntimeException("End line size != 1, not supported yet.");
    }

    private void fill() throws IOException {
        if (remain > 3 || finished) return;

        //copy the last bytes at the beginning
        switch (remain) {
            case 3 : array[0] = array[offset+remain-3];
                     array[1] = array[offset+remain-2];
                     array[2] = array[offset+remain-1]; break;
            case 2 : array[0] = array[offset+remain-2];
                     array[1] = array[offset+remain-1]; break;
            case 1 : array[0] = array[offset+remain-1]; break;
        }
        offset = 0;

        int nb = in.read(array, remain, array.length-remain);
        if (nb == -1) {
            finished = true;
            return;
        }
        remain += nb;

        if (remain>3) return;

        while (!finished && remain < 1024) {
            nb = in.read(array, remain, array.length-remain);
            if (nb == -1) {
                finished = true;
            } else {
                remain += nb;
            }
        }
    }

    /**
     *
     * @return true if finished
     * @throws IOException
     */
    private boolean prepare() throws IOException {
        fill();
        if (remain == 0) return true;
        charSize = encoding.charlength(array, offset);
        return remain < charSize ;
    }

    /**
     * Use with caution, this may break the char encoding offsets.
     *
     * @return causes an IOException
     * @throws IOException
     */
    @Override
    public int read() throws IOException {
        fill();
        if (remain > 0) {
            offset++;
            remain--;
            return array[offset-1] & 0xFF;
        } else {
            return -1;
        }
    }

    /**
     * Read a single char.
     *
     * @return Char or null if nothing left.
     * @throws IOException
     */
    public Char readChar() throws IOException {
        if (prepare()) return null;

        if (charSize == 1) {
            Char c = new Char(new byte[]{array[offset]}, encoding);
            offset++;
            remain--;
            return c;
        } else {
            Char c = new Char(Arrays.copy(array, offset, charSize), encoding);
            offset += charSize;
            remain -= charSize;
            return c;
        }

    }

    /**
     * Read a single char, write it in given buffer
     *
     * @param buffer byte buffer to copy character bytes, not null
     * @param offset offset in the byte buffer where to start writing character
     * @return number of bytes read, -1 if no more chars.
     * @throws IOException
     */
    public int readChar(final byte[] buffer, final int offset) throws IOException{
        if (prepare()) return -1;

        if (charSize == 1) {
            buffer[offset] = array[this.offset];
            this.offset++;
            remain--;
            return 1;
        } else {
            Arrays.copy(array, this.offset, charSize, buffer, offset);
            this.offset += charSize;
            remain -= charSize;
            return charSize;
        }
    }

    /**
     * Read a single char, write it in given buffer
     *
     * @return number of bytes read, -1 if no more chars.
     * @throws IOException
     */
    public int readChar(final ByteSequence buffer) throws IOException{
        if (prepare()) return -1;

        if (charSize == 1) {
            buffer.put(array[offset]);
            offset++;
            remain--;
            return 1;
        } else {
            buffer.put(array,offset,charSize);
            offset+=charSize;
            remain-=charSize;
            return charSize;
        }
    }


//direct version, without local cache, nearly as fast
//    /**
//     * Use with caution, this may break the char encoding offsets.
//     *
//     * @return causes an IOException
//     * @throws IOException
//     */
//    public int read() throws IOException {
//        return in.read();
//    }
//
//    /**
//     * Read a single char.
//     *
//     * @return Char or null if nothing left.
//     * @throws IOException
//     */
//    public Char readChar() throws IOException {
//        int b = in.read();
//        if (b==-1) return null;
//        charArr[0] = (byte) b;
//        if (encoding.isComplete(charArr, 0, 1)) {
//            return new Char(new byte[]{(byte)b}, encoding);
//        }
//
//        //more then one byte per char, loop
//        int length = 1;
//        do {
//            b = in.read();
//            if (b==-1) throw new IOException("Truncated character");
//            charArr[length] = (byte) b;
//            length++;
//        } while (encoding.isComplete(charArr, 0, length));
//
//        return new Char(Arrays.copy(charArr,0, length), encoding);
//    }
//
//    /**
//     * Read a single char, write it in given buffer
//     *
//     * @param buffer byte buffer to copy character bytes, not null
//     * @param offset offset in the byte buffer where to start writing character
//     * @return number of bytes read, -1 if no more chars.
//     * @throws IOException
//     */
//    public int readChar(final byte[] buffer, final int offset) throws IOException{
//        int b = in.read();
//        if (b==-1) return -1;
//        buffer[offset] = (byte) b;
//        if (encoding.isComplete(buffer, offset, 1)) return 1;
//
//        //more then one byte per char, loop
//        int length = 1;
//        do {
//            b = in.read();
//            if (b==-1) throw new IOException("Truncated character");
//            buffer[offset+length] = (byte) b;
//            length++;
//        } while (encoding.isComplete(buffer, offset, length));
//
//        return length;
//    }
//
//    /**
//     * Read a single char, write it in given buffer
//     *
//     * @return number of bytes read, -1 if no more chars.
//     * @throws IOException
//     */
//    public int readChar(final ByteSequence buffer) throws IOException{
//        int b = in.read();
//        if (b==-1) return -1;
//        charArr[0] = (byte) b;
//        if (encoding.isComplete(charArr, 0, 1)) {
//            buffer.put((byte) b);
//            return 1;
//        }
//
//        //more then one byte per char, loop
//        int length = 1;
//        do {
//            b = in.read();
//            if (b==-1) throw new IOException("Truncated character");
//            charArr[length] = (byte) b;
//            length++;
//        } while (encoding.isComplete(charArr, 0, length));
//
//        buffer.put(charArr, 0, length);
//        return length;
//    }




    /**
     * Read a character sequence until it reached a new line character.
     *
     * @return next line or null if nothing left.
     * @throws IOException
     */
    public Chars readLine() throws IOException {
        final byte[] data = readLineAsBytes();
        if (data != null) {
            return new Chars(data, encoding);
        }
        return null;
    }

    /**
     * Read a character sequence as byte array
     * until it reached a new line character.
     *
     * @return next line or null when finished.
     * @throws IOException
     */
    public byte[] readLineAsBytes() throws IOException {
        lineBuffer.removeAll();
        while (readChar(lineBuffer) > 0) {
            if (lineBuffer.endWidth(elb)) {
                break;
            }
        }

        if (lineBuffer.getSize() == 0) {
            return null;
        } else {
            return lineBuffer.toArrayByte();
        }
    }

    /**
     * Close underlying stream.
     *
     * @throws IOException
     */
    @Override
    public void dispose() throws IOException {
        in.dispose();
    }

}
