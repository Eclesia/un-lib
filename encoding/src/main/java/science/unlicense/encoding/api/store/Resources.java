
package science.unlicense.encoding.api.store;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;

/**
 *
 * @author Johann Sorel
 */
public final class Resources {

    private Resources(){}

    /**
     * Find resource with given id.
     *
     * @param root
     * @param id, not null
     * @return resource or null if no resource for given identifier found
     * @throws science.unlicense.encoding.api.store.StoreException
     */
    public static Resource find(Resource root, Chars id) throws StoreException {

        if (root instanceof Identifiable) {
            if (((Identifiable) root).getId().equals(id)) {
                return root;
            }
        }

        if (root instanceof ResourceSet) {
            final Collection elements = ((ResourceSet) root).getElements();
            final Iterator ite = elements.createIterator();

            while (ite.hasNext()) {
                final Resource r = (Resource) ite.next();
                final Resource f = find(r, id);
                if (f != null) return f;
            }
        }

        return null;
    }

    /**
     * Find and return the first resource of given type.
     *
     * @param root
     * @param resourceClass
     * @return resource or null if no resource of given type found
     * @throws science.unlicense.encoding.api.store.StoreException
     */
    public static Resource findFirst(Resource root, Class resourceClass) throws StoreException {
        CObjects.ensureNotNull(resourceClass);

        if (resourceClass.isInstance(root)) {
            return root;
        }

        if (root instanceof ResourceSet) {
            final Collection elements = ((ResourceSet) root).getElements();
            final Iterator ite = elements.createIterator();

            while (ite.hasNext()) {
                final Resource r = (Resource) ite.next();
                final Resource f = findFirst(r, resourceClass);
                if (f != null) return f;
            }
        }

        return null;
    }

}
