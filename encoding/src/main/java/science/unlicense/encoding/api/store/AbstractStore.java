

package science.unlicense.encoding.api.store;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;

/**
 *
 * @author Johann Sorel
 */
public class AbstractStore extends CObject implements Store {

    protected final Format format;
    protected final Object source;
    private Logger logger = Loggers.get();

    public AbstractStore(Format format, Object source) {
        this.format = format;
        this.source = source;
    }

    public Format getFormat() {
        return format;
    }

    public Object getInput() {
        return source;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        CObjects.ensureNotNull(logger);
        this.logger = logger;
    }

    public void dispose() {
    }

    @Override
    public Resource find(Chars id) throws StoreException {
        return Resources.find(this, id);
    }

}
