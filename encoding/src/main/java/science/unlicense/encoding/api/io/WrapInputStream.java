
package science.unlicense.encoding.api.io;

import science.unlicense.common.api.character.Chars;

/**
 * A ByteOutputStream which wrap another.
 *
 * @author Johann Sorel
 */
public abstract class WrapInputStream extends AbstractInputStream{

    protected final ByteInputStream in;

    public WrapInputStream(ByteInputStream in) {
        this.in = in;
    }

    @Override
    public void dispose() throws IOException {
        in.dispose();
    }

    @Override
    public Chars toChars() {
        return new Chars(this.getClass().getName() +", "+in);
    }

}
