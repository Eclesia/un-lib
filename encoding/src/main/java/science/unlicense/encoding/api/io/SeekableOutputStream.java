
package science.unlicense.encoding.api.io;

/**
 *
 * @author Johann Sorel
 */
public class SeekableOutputStream extends AbstractOutputStream {

    private final SeekableByteBuffer buffer;

    public SeekableOutputStream(SeekableByteBuffer buffer) {
        this.buffer = buffer;
    }

    public void write(byte b) throws IOException {
        buffer.write(b);
    }

    public void flush() throws IOException {
        buffer.flush();
    }

    /**
     * Do nothing, the backend SeekableByteBuffer must be closed separately.
     * @throws IOException
     */
    public void close() throws IOException {
    }

}
