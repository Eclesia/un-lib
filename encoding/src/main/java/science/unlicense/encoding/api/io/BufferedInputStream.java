
package science.unlicense.encoding.api.io;

import science.unlicense.common.api.Arrays;
import science.unlicense.math.api.Maths;

/**
 *
 * @author Johann Sorel
 */
public class BufferedInputStream extends WrapInputStream {

    /**
     * Default buffer size.
     * Note : when searching for benchmarks of the most efficient cache size the
     * result range from 4K to 32K. But such result have in mind usual file
     * systems and older disks in rpm.
     * TODO : find a proper benchmark using SSD disk tests.
     */
    private static final int BUFFER_SIZE = 4096*4;

    private final byte[] buffer;
    private int offset = 0;
    private int used = 0;

    public BufferedInputStream(ByteInputStream in) {
        this(in,BUFFER_SIZE);
    }

    public BufferedInputStream(ByteInputStream in, int bufferSize) {
        super(in);
        this.buffer = new byte[bufferSize];
    }

    /**
     * Get number bytes remaining in the buffer.
     * @return
     */
    public int bufferRemaining() {
        return Maths.max(0,used-offset);
    }

    private void checkBuffer() throws IOException{
        if (offset>=used) {
           used = in.read(buffer, 0, buffer.length);
           offset = 0;
        }
    }

    @Override
    public int read() throws IOException {
        checkBuffer();
        if (used==-1) return -1;
        final int b = buffer[offset] & 0xFF;
        offset++;
        return b;
    }

    @Override
    public int read(final byte buffer[], final int offset, int length) throws IOException {
        if (offset>=used && length>=buffer.length) {
            //buffer is empty and request is as large as the buffer
            //copy directly from input
            return in.read(buffer, offset, length);
        } else {
            //copy from buffer
            checkBuffer();
            if (used==-1) return -1;
            length = Maths.min(length, used-this.offset);
            Arrays.copy(this.buffer, this.offset, length, buffer, offset);
            this.offset += length;
            return length;
        }
    }

    public long skip(long n) throws IOException {
        if (offset>=used) {
           //buffer is empty
           return in.skip(n);
        } else {
            //move in the buffer
            n = Maths.min(n, used-offset);
            offset += n;
            return n;
        }
    }

}
