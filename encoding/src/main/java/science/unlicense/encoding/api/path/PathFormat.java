
package science.unlicense.encoding.api.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface PathFormat {

    /**
     * Indicate if this resolver need a base path to create a resolver.
     * Example :
     * - The operating system file resolver is absolute.
     * - A zip archive resolver is not absolute.
     *
     * @return true if absolute
     */
    boolean isAbsolute();

    /**
     * Returns the URI protocol prefix.
     * Formats which are absolute always have a prefix, unlike others.
     * example : file,http,ftp
     *
     * @return prefix, or null if this format do not have a prefix
     */
    Chars getPrefix();

    /**
     * Indicate if this path can be used.
     * For non-absolute resolvers.
     * Absolute resolvers should always return false.
     *
     * @param base
     * @return true if it can be used
     * @throws IOException
     */
    boolean canCreate(Path base) throws IOException;

    /**
     * Create path resolver.
     *
     * @param base, need only if resolver is not absolute.
     * @return PathResolver, never null
     * @throws IOException if something goes wrong
     */
    PathResolver createResolver(Path base) throws IOException;

}
