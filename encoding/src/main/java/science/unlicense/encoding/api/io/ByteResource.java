
package science.unlicense.encoding.api.io;

/**
 *
 * @author Johann Sorel
 */
public interface ByteResource {

    /**
     * Get resource size if knowned, else return -1;
     *
     * @return resource size or -1 for unknowned.
     */
    long getSize();

    /**
     * Create an inputstream for this resource.
     *
     * @return ByteInputStream
     * @throws IOException if stream can not be created
     */
    ByteInputStream createInputStream() throws IOException;

    /**
     * Create an outputstream for this resource.
     *
     * @return ByteOutputStream
     * @throws IOException if stream can not be created
     */
    ByteOutputStream createOutputStream() throws IOException;

    /**
     * Create a seekable buffer on this resource datas.
     *
     * @param read set to true to request reading support
     * @param write set to true to request writing support
     * @param resize set to true to request resizing support
     * @return SeekableByteBuffer, never null
     * @throws IOException if buffer creation failed
     */
    SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException;

}
