
package science.unlicense.encoding.api.store;

import science.unlicense.encoding.api.io.IOException;

/**
 * Used by format decoder and encoder to indicate an encoding error.
 *
 * @author Johann Sorel
 */
public class FormatEncodingException extends IOException {

    public FormatEncodingException(Object origin, String message) {
        super(origin, message);
    }

    public FormatEncodingException(Object origin, Throwable cause) {
        super(origin, cause);
    }

    public FormatEncodingException(Object origin, String message, Throwable cause) {
        super(origin, message, cause);
    }

}
