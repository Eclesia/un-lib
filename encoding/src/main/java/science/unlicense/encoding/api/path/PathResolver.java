
package science.unlicense.encoding.api.path;

import science.unlicense.common.api.character.Chars;

/**
 * A PathResolver is in charge of resolving adresses and show them as Path objects.
 *
 * @author Johann Sorel
 */
public interface PathResolver {

    /**
     * Get original path resolver format.
     * @return PathResolverFormat
     */
    PathFormat getPathFormat();

    /**
     * Passing null should return the root path of the resolver.
     *
     * @param path
     * @return
     */
    Path resolve(Chars path);

}
