
package science.unlicense.encoding.api.store;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.doc.DefaultDocumentType;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.DocumentTypeBuilder;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.math.api.Maths;

/**
 * Default format.
 *
 * @author Johann Sorel
 */
public abstract class DefaultFormat implements Format {

    protected static final DocumentType EMPTY = new DefaultDocumentType();
    private static final Chars SUF = Chars.constant(".");

    protected final Chars identifier;
    protected Chars shortName;
    protected Chars longName;
    protected final Sequence mimeTypes = new ArraySequence();
    protected final Sequence extensions = new ArraySequence();
    protected final Sequence signatures = new ArraySequence();
    protected final Sequence resourceTypes = new ArraySequence();
    protected DocumentType parameters = EMPTY;
    protected boolean isSubset = false;

    private final Sequence mimeTypesView = Collections.readOnlySequence(mimeTypes);
    private final Sequence extensionsView = Collections.readOnlySequence(extensions);
    private final Sequence signaturesView = Collections.readOnlySequence(signatures);
    private final Sequence resourcesView = Collections.readOnlySequence(resourceTypes);

    public DefaultFormat(Chars identifier) {
        CObjects.ensureNotNull(identifier);
        this.identifier = identifier;
        this.shortName = identifier;
        this.longName = identifier;
        this.parameters = new DocumentTypeBuilder(identifier).build();
    }

    @Override
    public Chars getIdentifier() {
        return identifier;
    }

    @Override
    public Chars getShortName() {
        return shortName == null ? getIdentifier() : shortName;
    }

    @Override
    public Chars getLongName() {
        return longName == null ? getShortName() : longName;
    }

    @Override
    public Sequence getMimeTypes() {
        return mimeTypesView;
    }

    @Override
    public Sequence getExtensions() {
        return extensionsView;
    }

    @Override
    public Sequence getSignature() {
        return signaturesView;
    }

    @Override
    public Sequence getResourceTypes() {
        return resourcesView;
    }

    @Override
    public DocumentType getParameters() {
        return parameters;
    }

    @Override
    public boolean isSubsetFormat() {
        return isSubset;
    }

    /**
     * Default implementation checks for the path extension or signatures.
     *
     * @param input
     * @return
     * @throws IOException
     */
    @Override
    public boolean canDecode(Object input) throws IOException {

        //check the signatures if we can obtain a stream
        final Sequence signatures = getSignature();
        int maxLength = 0;
        for (int i=0,n=signatures.getSize();i<n;i++) {
            maxLength = Maths.max(maxLength, ((byte[]) signatures.get(i)).length);
        }

        if (maxLength!=0) {
            final boolean[] mustClose = new boolean[]{false};
            ByteInputStream stream = null;
            try {
                stream = IOUtilities.toInputStream(input, mustClose);
                final DataInputStream ds = new DataInputStream(stream);
                final byte[] sign = ds.readFully(new byte[maxLength]);
                for (int i=0,n=signatures.getSize();i<n;i++) {
                    final byte[] signature = (byte[]) signatures.get(i);
                    if (Arrays.equals(signature,0,signature.length,sign,0)) {
                        return true;
                    }
                }
            } finally {
                if (mustClose[0]) {
                    stream.dispose();
                }
            }
        }

        //check by extension if we have a path
        final Collection exts = getExtensions();
        if (!exts.isEmpty() && input instanceof Path) {
            final Path path = (Path) input;
            final Chars name = new Chars(path.getName()).toLowerCase();
            final Iterator ite = exts.createIterator();
            while (ite.hasNext()) {
                CharArray ext = (CharArray) ite.next();
                if (name.endsWith(SUF.concat(ext))) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public long searchEnd(ByteInputStream in, boolean fullscan) throws IOException {
        return -1;
    }

}
