
package science.unlicense.encoding.api.store;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public interface Identifiable {

    /**
     * Image unique id in the store.
     * @return Chars, never null
     */
    Chars getId();

}
