package science.unlicense.encoding.api.io;

/**
 * End of Stream exception.
 *
 * @author Johann Sorel
 */
public class EOSException extends IOException{

    public EOSException(Object origin, String message) {
        super(origin, message);
    }

}
