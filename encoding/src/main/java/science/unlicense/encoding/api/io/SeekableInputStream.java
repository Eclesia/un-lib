
package science.unlicense.encoding.api.io;

/**
 *
 * @author Johann Sorel
 */
public class SeekableInputStream extends AbstractInputStream {

    private final SeekableByteBuffer buffer;

    public SeekableInputStream(SeekableByteBuffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public int read() throws IOException {
        if (buffer.getPosition() >= buffer.getSize()) {
            return -1;
        }
        return buffer.read();
    }

    @Override
    public long skip(long n) throws IOException {
        long remaining = buffer.getSize() - buffer.getPosition();
        if (remaining <= 0) return 0;
        return buffer.skip(remaining);
    }

    /**
     * Do nothing, the backend SeekableByteBuffer must be closed separately.
     * @throws IOException
     */
    @Override
    public void dispose() throws IOException {
        //do nothing
    }

}
