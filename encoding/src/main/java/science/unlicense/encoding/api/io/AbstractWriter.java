
package science.unlicense.encoding.api.io;

import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.path.Path;

/**
 * Base class for writer implementations.
 * Provides the common used functionnalities to properly open and close
 * the output.
 *
 * @author Johann Sorel
 */
public class AbstractWriter implements Writer{

    private boolean closeStream = false;

    protected Object output;
    private ByteOutputStream byteStream;
    private DataOutputStream dataStream;
    private CharOutputStream charStream;

    @Override
    public void setOutput(Object output) throws IOException {
        this.output = output;
    }

    @Override
    public Object getOutput() {
        return output;
    }

    @Override
    public Document getConfiguration() {
        return null;
    }

    @Override
    public void setConfiguration(Document configuration) {
        //ignore it
    }

    protected final ByteOutputStream getOutputAsByteStream() throws IOException {
        if (byteStream!=null) return byteStream;

        Object output = this.output;
        if (output instanceof Chars) {
            //try to resolve it
            //TODO cyclic reference
        }

        if (output instanceof ByteOutputStream) {
            byteStream = (ByteOutputStream) output;
            //we did not create the stream, we do not close it.
            closeStream = false;
            return byteStream;
        } else if (output instanceof Path) {
            byteStream = ((Path) output).createOutputStream();
            closeStream = true;
            return byteStream;
        } else {
            throw new IOException(output, "Unsupported output : " + output);
        }
    }

    protected final DataOutputStream getOutputAsDataStream(Endianness encoding) throws IOException {
        if (dataStream == null) {
            dataStream = new DataOutputStream(getOutputAsByteStream(), encoding);
        } else {
            dataStream.setEndianness(encoding);
        }
        return dataStream;
    }

    protected final CharOutputStream getOutputAsCharStream(CharEncoding encoding) throws IOException {
        if (charStream!=null) return charStream;
        charStream = new CharOutputStream(getOutputAsByteStream(),encoding);
        return charStream;
    }

    public void dispose() throws IOException {
        if (closeStream && byteStream!=null) {
            byteStream.close();
            byteStream = null;
            dataStream = null;
            charStream = null;
        }
    }

    private void checkOpen() throws IOException {
        if (byteStream != null) {
            throw new IOException(byteStream, "Input stream is already open.");
        }
    }

}
