
package science.unlicense.encoding.api.io;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;

/**
 * Outputstream with methods to write standard primitive types.
 *
 * @author Johann Sorel
 */
public class DataOutputStream extends WrapOutputStream{

    private Endianness encoding;

    // working buffer
    private final byte temp[] = new byte[10];
    // when writing bit per bit, keep track of remaining bits
    private int bitOffset = 0;
    private int currentByte = 0;
    //current offset from start.
    private long byteOffset = 0;

    public DataOutputStream(ByteOutputStream out) {
        this(out,null);
    }

    public DataOutputStream(ByteOutputStream out, Endianness encoding) {
        super(out);
        if (encoding == null) encoding = Endianness.BIG_ENDIAN;
        this.encoding = encoding;
    }

    public long getByteOffset() {
        return byteOffset;
    }

    /**
     * Get default stream encoding.
     * @return
     */
    public Endianness getEndianness() {
        return encoding;
    }

    /**
     * Set default stream encoding
     * @param encoding
     */
    public void setEndianness(Endianness encoding) {
        this.encoding = encoding;
    }

    public void skipFully(long nbbyte) throws IOException {
        for (long i=0;i<nbbyte;i++) {
            write((byte) 0);
        }
    }

    public void write(byte b) throws IOException {
        if (bitOffset == 0) {
            byteOffset++;
            out.write(b);
        } else {
            writeBits(b, 8);
        }
    }

    public void write(byte[] buffer, int offset, int length) throws IOException {
        if (bitOffset == 0) {
            byteOffset+=length;
            //write directly in the main stream
            out.write(buffer, offset, length);
        } else {
            //bit offset prevent direct write.
            for (int i=offset,n=length;i<n;i++) {
                write(buffer[i]);
            }
        }
    }

    public void writeBit(int value) throws IOException {
        writeBits(value,1);
    }

    public void writeBits(int value, int nbBit) throws IOException {

        for (int i=nbBit; i>0; i--) {

            int offset = (i-1);
            int mask = 1 << offset;
            int t = value & mask;
            t = t >> offset;
            currentByte |= (t << (7-bitOffset));

            bitOffset++;
            if (bitOffset == 8) {
                byteOffset++;
                bitOffset = 0;
                out.write((byte) currentByte);
                currentByte = 0;
            }
        }

    }

    public void writeSignedBits(int value, int nbBit) throws IOException{
        //write the sign
        writeBits((value<0)?1:1,1);
        //write absolute value
        value = Math.abs(value);
        writeBits(value, nbBit-1);
    }

    public void writeByte(byte value) throws IOException{
        writeByte(value, encoding);
    }

    public void writeByte(byte value, Endianness encoding) throws IOException{
        encoding.writeByte(value, temp, 0);
        write(temp, 0, 1);
    }

    public void writeUByte(int value) throws IOException{
        DataOutputStream.this.writeUByte(value, encoding);
    }

    public void writeUByte(int value, Endianness encoding) throws IOException{
        encoding.writeUByte(value, temp, 0);
        write(temp, 0, 1);
    }

    public void writeUByte(int[] values) throws IOException{
        DataOutputStream.this.writeUByte(values, 0, values.length);
    }

    public void writeUByte(int[] values, Endianness encoding) throws IOException{
        writeUByte(values, 0, values.length, encoding);
    }

    public void writeUByte(int[] values, int offset, int length) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) DataOutputStream.this.writeUByte(values[i]);
    }

    public void writeUByte(int[] values, int offset, int length, Endianness encoding) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) DataOutputStream.this.writeUByte(values[i],encoding);
    }

    public void writeShort(short value) throws IOException{
        writeShort(value, encoding);
    }

    public void writeShort(short value, Endianness encoding) throws IOException{
        encoding.writeShort(value, temp, 0);
        write(temp, 0, 2);
    }

    public void writeShort(short[] values) throws IOException{
        writeShort(values, 0, values.length);
    }

    public void writeShort(short[] values, Endianness encoding) throws IOException{
        writeShort(values, 0, values.length, encoding);
    }

    public void writeShort(short[] values, int offset, int length) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeShort(values[i]);
    }

    public void writeShort(short[] values, int offset, int length, Endianness encoding) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeShort(values[i],encoding);
    }

    public void writeUShort(int value) throws IOException{
        DataOutputStream.this.writeUShort(value, encoding);
    }

    public void writeUShort(int value, Endianness encoding) throws IOException{
        encoding.writeUShort(value, temp, 0);
        write(temp, 0, 2);
    }

    public void writeUShort(int[] values) throws IOException{
        DataOutputStream.this.writeUShort(values, 0, values.length);
    }

    public void writeUShort(int[] values, Endianness encoding) throws IOException{
        writeUShort(values, 0, values.length, encoding);
    }

    public void writeUShort(int[] values, int offset, int length) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) DataOutputStream.this.writeUShort(values[i]);
    }

    public void writeUShort(int[] values, int offset, int length, Endianness encoding) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) DataOutputStream.this.writeUShort(values[i],encoding);
    }

    public void writeInt24(int value) throws IOException{
        writeInt24(value, encoding);
    }

    public void writeInt24(int value, Endianness encoding) throws IOException{
        encoding.writeInt24(value, temp, 0);
        write(temp, 0, 3);
    }

    public void writeInt24(int[] values) throws IOException{
        writeInt24(values, 0, values.length);
    }

    public void writeInt24(int[] values, Endianness encoding) throws IOException{
        writeInt24(values, 0, values.length, encoding);
    }

    public void writeInt24(int[] values, int offset, int length) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeInt24(values[i]);
    }

    public void writeInt24(int[] values, int offset, int length, Endianness encoding) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeInt24(values[i],encoding);
    }

    public void writeUInt24(int value) throws IOException{
        DataOutputStream.this.writeUInt24(value, encoding);
    }

    public void writeUInt24(int value, Endianness encoding) throws IOException{
        encoding.writeUInt24(value, temp, 0);
        write(temp, 0, 3);
    }

    public void writeUInt24(int[] values) throws IOException{
        DataOutputStream.this.writeUInt24(values, 0, values.length);
    }

    public void writeUInt24(int[] values, Endianness encoding) throws IOException{
        writeUInt24(values, 0, values.length, encoding);
    }

    public void writeUInt24(int[] values, int offset, int length) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) DataOutputStream.this.writeUInt24(values[i]);
    }

    public void writeUInt24(int[] values, int offset, int length, Endianness encoding) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) DataOutputStream.this.writeUInt24(values[i],encoding);
    }

    public void writeInt(int value) throws IOException{
        writeInt(value, encoding);
    }

    public void writeInt(int value, Endianness encoding) throws IOException{
        encoding.writeInt(value, temp, 0);
        write(temp, 0, 4);
    }

    public void writeInt(int[] values) throws IOException{
        writeInt(values, 0, values.length);
    }

    public void writeInt(int[] values, Endianness encoding) throws IOException{
        writeInt(values, 0, values.length, encoding);
    }

    public void writeInt(int[] values, int offset, int length) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeInt(values[i]);
    }

    public void writeInt(int[] values, int offset, int length, Endianness encoding) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeInt(values[i],encoding);
    }

    public void writeUInt(long value) throws IOException{
        DataOutputStream.this.writeUInt(value, encoding);
    }

    public void writeUInt(long value, Endianness encoding) throws IOException{
        encoding.writeUInt(value, temp, 0);
        write(temp, 0, 4);
    }

    public void writeUInt(long[] values) throws IOException{
        DataOutputStream.this.writeUInt(values, 0, values.length);
    }

    public void writeUInt(long[] values, Endianness encoding) throws IOException{
        writeUInt(values, 0, values.length, encoding);
    }

    public void writeUInt(long[] values, int offset, int length) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) DataOutputStream.this.writeUInt(values[i]);
    }

    public void writeUInt(long[] values, int offset, int length, Endianness encoding) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) DataOutputStream.this.writeUInt(values[i],encoding);
    }

    /**
     * Write a varying length unsigned int.
     * This is a common method used to compress integer values.
     * The first bit of each byte indicate if the value continue on the next byte.
     * The 7 new bits contain the value.
     * There can be up to 10 bytes.
     *
     * resource :
     * https://en.wikipedia.org/wiki/LEB128 (Note : provided pseudo-code is crap)
     *
     * @param value
     */
    public void writeVarLengthUInt(long value) throws IOException{
        temp[9] = (byte) ((value >>> 63) & 0x7F);
        temp[8] = (byte) ((value >>> 56) & 0x7F);
        temp[7] = (byte) ((value >>> 49) & 0x7F);
        temp[6] = (byte) ((value >>> 42) & 0x7F);
        temp[5] = (byte) ((value >>> 35) & 0x7F);
        temp[4] = (byte) ((value >>> 28) & 0x7F);
        temp[3] = (byte) ((value >>> 21) & 0x7F);
        temp[2] = (byte) ((value >>> 14) & 0x7F);
        temp[1] = (byte) ((value >>>  7) & 0x7F);
        temp[0] = (byte) ((value >>>  0) & 0x7F);

        int size = 10;
        for (int i=9;i>0;i--,size--) {
            if (temp[i]!=0) break;
        }
        for (int i=0;i<size-1;i++) {
            temp[i] = (byte) (temp[i] | 0x80);
        }

        write(temp, 0, size);
    }


    /**
     * Write a varying length int.
     * This is a common method used to compress integer values.
     * The first bit of each byte indicate if the value continue on the next byte.
     * The 7 new bits contain the value.
     * There can be up to 10 bytes.
     *
     * resource :
     * https://en.wikipedia.org/wiki/LEB128 (Note : provided pseudo-code is crap)
     *
     * @param value
     */
    public void writeVarLengthInt(long value) throws IOException{
        boolean negative = value<0;

        temp[9] = (byte) ((value >>> 63) & 0x7F);
        temp[8] = (byte) ((value >>> 56) & 0x7F);
        temp[7] = (byte) ((value >>> 49) & 0x7F);
        temp[6] = (byte) ((value >>> 42) & 0x7F);
        temp[5] = (byte) ((value >>> 35) & 0x7F);
        temp[4] = (byte) ((value >>> 28) & 0x7F);
        temp[3] = (byte) ((value >>> 21) & 0x7F);
        temp[2] = (byte) ((value >>> 14) & 0x7F);
        temp[1] = (byte) ((value >>>  7) & 0x7F);
        temp[0] = (byte) ((value >>>  0) & 0x7F);

        if (negative) {
            temp[9] |= 0x7E; //with 64bits long, only the first bit is set with the shift operator

            int size = 10;
            for (int i=9;i>0;i--,size--) {
                if (temp[i]!=0x7F) break;
            }
            //if the 0x40 bit of last byte is not set we use an extra byte
            //because 0x40 is used to indicate negative values
            if ((temp[size-1] & 0x40) ==0) {
                size++;
            }
            for (int i=0;i<size-1;i++) {
                temp[i] = (byte) (temp[i] | 0x80);
            }

            write(temp, 0, size);
        } else {
            int size = 10;
            for (int i=9;i>0;i--,size--) {
                if (temp[i]!=0) break;
            }
            //if the 0x40 bit of last byte is set we use an extra byte
            //because 0x40 is used to indicate negative values
            if ((temp[size-1] & 0x40) !=0) {
                size++;
            }
            for (int i=0;i<size-1;i++) {
                temp[i] = (byte) (temp[i] | 0x80);
            }

            write(temp, 0, size);
        }
    }

    public void writeLong(long value) throws IOException{
        writeLong(value, encoding);
    }

    public void writeLong(long value, Endianness encoding) throws IOException{
        encoding.writeLong(value, temp, 0);
        write(temp, 0, 8);
    }

    public void writeLong(long[] values) throws IOException{
        writeLong(values, 0, values.length);
    }

    public void writeLong(long[] values, Endianness encoding) throws IOException{
        writeLong(values, 0, values.length, encoding);
    }

    public void writeLong(long[] values, int offset, int length) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeLong(values[i]);
    }

    public void writeLong(long[] values, int offset, int length, Endianness encoding) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeLong(values[i],encoding);
    }

    public void writeFloat(float value) throws IOException{
        writeFloat(value, encoding);
    }

    public void writeFloat(float value, Endianness encoding) throws IOException{
        encoding.writeFloat(value, temp, 0);
        write(temp, 0, 4);
    }

    public void writeFloat(float[] values) throws IOException{
        writeFloat(values, 0, values.length);
    }

    public void writeFloat(float[] values, Endianness encoding) throws IOException{
        writeFloat(values, 0, values.length, encoding);
    }

    public void writeFloat(float[] values, int offset, int length) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeFloat(values[i]);
    }

    public void writeFloat(float[] values, int offset, int length, Endianness encoding) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeFloat(values[i],encoding);
    }

    public void writeDouble(double value) throws IOException{
        writeDouble(value, encoding);
    }

    public void writeDouble(double value, Endianness encoding) throws IOException{
        encoding.writeDouble(value, temp, 0);
        write(temp, 0, 8);
    }

    public void writeDouble(double[] values) throws IOException{
        writeDouble(values, 0, values.length);
    }

    public void writeDouble(double[] values, Endianness encoding) throws IOException{
        writeDouble(values, 0, values.length, encoding);
    }

    public void writeDouble(double[] values, int offset, int length) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeDouble(values[i]);
    }

    public void writeDouble(double[] values, int offset, int length, Endianness encoding) throws IOException{
        for (int i=offset,n=offset+length;i<n;i++) writeDouble(values[i],encoding);
    }

    /**
     * Zero terminated string are common in most format.
     *
     * The last '0' byte will not be written of the string length is exactly the maxSize.
     *
     * @param maxSize : maximum size , 0 for no size limit
     * @param encoding : expected characters encoding
     * @throws IOException
     */
    public void writeZeroTerminatedChars(Chars chars, int maxSize, CharEncoding encoding) throws IOException{
        final byte[] data = chars.toBytes(encoding);

        if (maxSize>0 && data.length>maxSize) {
            throw new IOException(this, "Char sequence is superior to maxsize : "+data.length+" > "+maxSize);
        }

        write(data);
        if (maxSize>0) {
            if (data.length<maxSize) {
                write((byte) 0);
            }
        } else {
            write((byte) 0);
        }
    }

    /**
     * Zero terminated string are common in most format.
     * To obtain a constant size in some parts like header, string may be defined
     * in a given maximum number of bytes, if the string to not fill the max size
     * then 0 bytes are used to fill space.
     *
     * @param blockSize : string block size
     * @param encoding : expected characters encoding
     * @throws IOException
     */
    public void writeBlockZeroTerminatedChars(Chars chars, int blockSize, CharEncoding encoding) throws IOException{
        final byte[] data;
        if (chars!=null) {
            data = chars.toBytes(encoding);
        } else {
            data = Arrays.ARRAY_BYTE_EMPTY;
        }

        final int remain = blockSize-data.length;
        if (remain<0) {
            throw new IOException(this, "Char sequence is superior to maxsize : "+data.length+" > "+blockSize);
        }
        write(data);
        if (remain>0) {
            write(new byte[remain]);
        }

    }

    /**
     * Write current byte if it is unfinished.
     *
     */
    public void skipToByteEnd() throws IOException{
        if (bitOffset!=0) {
            writeBits(0, 8-bitOffset);
        }
    }

    /**
     * Align the stream, skip any bytes needed to match alignment value.
     * This method moves the cursor forward if needed.
     */
    public void realign(int alignByteSize) throws IOException{
        skipToByteEnd();
        long res = byteOffset%alignByteSize;
        if (res==0) return;
        skipFully(alignByteSize-res);
    }

    @Override
    public void flush() throws IOException {
        if (bitOffset!=0) out.write((byte) currentByte);
        out.flush();
    }

    @Override
    public void close() throws IOException {
        out.close();
    }

}
