
package science.unlicense.encoding.api.io;

import science.unlicense.common.api.Arrays;

/**
 *
 * @author Johann Sorel
 */
public class BufferedOutputStream extends WrapOutputStream {

    /**
     * Default buffer size.
     * Note : when searching for benchmarks of the most efficient cache size the
     * result range from 4K to 32K. But such result have in mind usual file
     * systems and older disks in rpm.
     * TODO : find a proper benchmark using SSD disk tests.
     */
    private static final int BUFFER_SIZE = 4096*4;

    private final byte[] buffer;
    private int offset = 0;
    private int used = 0;

    public BufferedOutputStream(ByteOutputStream out) {
        this(out,BUFFER_SIZE);
    }

    public BufferedOutputStream(ByteOutputStream out, int bufferSize) {
        super(out);
        this.buffer = new byte[bufferSize];
    }

    public void write(byte b) throws IOException {
        if (offset==buffer.length) {
            out.write(buffer);
            offset=0;
        }
        buffer[offset] = b;
        offset++;
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        if (length > this.buffer.length/2 || length > this.buffer.length-this.offset) {
            //avoid a copy we will write it directly in the output
            out.write(this.buffer, 0, this.offset);
            this.offset=0;
            out.write(buffer, offset, length);
        } else {
            //writer in buffer
            Arrays.copy(buffer, offset, length, this.buffer, this.offset);
            this.offset += length;
        }
    }

    @Override
    public void flush() throws IOException {
        if (offset>0) {
           out.write(buffer, 0, offset);
           offset=0;
        }
        super.flush();
    }

    @Override
    public void close() throws IOException {
        flush();
        super.close();
    }

}
