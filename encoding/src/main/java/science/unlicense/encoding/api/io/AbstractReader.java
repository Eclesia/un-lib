
package science.unlicense.encoding.api.io;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.impl.io.BacktrackInputStream;

/**
 * Base class for reader implementations.
 * Provides the common used functionnalities to properly open and close
 * the input.
 *
 * @author Johann Sorel
 */
public class AbstractReader implements Reader{

    private boolean closeStream = false;

    protected Object input;
    private ByteInputStream byteStream;
    private DataInputStream dataStream;
    private BacktrackInputStream backStream;
    private CharInputStream charStream;
    private Document config = new DefaultDocument();

    @Override
    public void setInput(Object input) throws IOException {
        this.input = input;
        //remove all cached streams
        byteStream = null;
        dataStream = null;
        backStream = null;
        charStream = null;
    }

    @Override
    public Object getInput() {
        return input;
    }

    @Override
    public Document getConfiguration() {
        return null;
    }

    @Override
    public void setConfiguration(Document configuration) {
        //ignore it
    }

    protected final ByteInputStream getInputAsByteStream() throws IOException {
        if (byteStream!=null) return byteStream;

        Object input = this.input;
        if (input instanceof Chars) {
            //try to resolve it
            //TODO cyclic reference
        }

        final boolean[] res = new boolean[1];
        byteStream = IOUtilities.toInputStream(input, res);
        closeStream = res[0];

        return byteStream;
    }

    protected final DataInputStream getInputAsDataStream(Endianness encoding) throws IOException {
        if (dataStream == null) {
            dataStream = new DataInputStream(getInputAsByteStream(), encoding);
        } else {
            dataStream.setEndianness(encoding);
        }
        return dataStream;
    }

    protected final BacktrackInputStream getInputAsBacktrackStream() throws IOException {
        if (backStream!=null) return backStream;
        backStream = new BacktrackInputStream(getInputAsByteStream());
        backStream.mark();
        return backStream;
    }

    protected final CharInputStream getInputAsCharStream(CharEncoding encoding) throws IOException {
        if (charStream!=null) return charStream;
        charStream = new CharInputStream(getInputAsByteStream(),encoding);
        return charStream;
    }

    protected final CharInputStream getInputAsCharStream(CharEncoding encoding, Char endline) throws IOException {
        if (charStream!=null) return charStream;
        charStream = new CharInputStream(getInputAsByteStream(),encoding,endline);
        return charStream;
    }

    @Override
    public void dispose() throws IOException {
        if (closeStream && byteStream!=null) {
            byteStream.dispose();
            byteStream = null;
            dataStream = null;
            backStream = null;
            charStream = null;
        }
    }

    private void checkOpen() throws IOException {
        if (byteStream != null) {
            throw new IOException(byteStream, "Input stream is already open.");
        }
    }

}
