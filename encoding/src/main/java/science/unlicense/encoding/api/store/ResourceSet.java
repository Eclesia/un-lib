
package science.unlicense.encoding.api.store;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;

/**
 *
 * @author Johann Sorel
 */
public interface ResourceSet {

    /**
     * Find resource with given id.
     *
     * @param id, not null
     * @return resource or null if no resource for given identifier found
     */
    Resource find(Chars id) throws StoreException;

    Collection getElements() throws StoreException;

}
