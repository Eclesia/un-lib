
package science.unlicense.encoding.api.store;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * A format define a group of bytes organize in a defined structure.
 * Formats are often associated to mime types and file extensions.
 *
 * Formats produce stores which in turn give access to resources of various types :
 * - ImageResource : PNG,BMP,TGA,...
 * - Media : WAV,MKV,MP4,...
 * - SceneResource : Collada,OBJ,3DS,...
 * - Archive : ZIP,TAR,...
 *
 * @author Johann Sorel
 */
public interface Format {

    /**
     * Identifier inside the library.
     * Should be unique.
     *
     * @return Chars, never null
     */
    Chars getIdentifier();

    /**
     * Short name, most commun abbreviation used for this format.
     *
     * @return Chars, never null
     */
    CharArray getShortName();

    /**
     * Long name, full name of this format.
     *
     * @return Chars, never null
     */
    CharArray getLongName();

    /**
     * Set of knowned mime-types.
     *
     * @return Set, never null, can be empty.
     */
    Sequence getMimeTypes();

    /**
     * List of used file extensions.
     *
     * @return Chars array, never null, can be empty.
     */
    Sequence getExtensions();

    /**
     * Get a list of possible file signatures used by this format.
     * A signature is a small number of bytes located at the begining of the file
     * which allows to quickly identify the file format.
     * Some format might have more then one signature.
     *
     * Size 0 if there are no specific signature for this format.
     *
     * @return collection of possible signatures, never null
     */
    Sequence getSignature();

    /**
     * Some formats extend existing formats, this is the case for formats based
     * on all purpose encodings like geojson with json, or all xml schemas.
     * PNG image format also has two subtypes : APNG and TDCG
     * @return true if format is a subtype
     */
    boolean isSubsetFormat();

    /**
     * Get format possible stored types.
     * Formats which acts as containers like archives and Asset bundles can
     * return an empty array to indicate anything may be encountered.
     *
     * @return collection of Resource classes expected to be found in this format.
     */
    Sequence getResourceTypes();

    /**
     * Test to check if given input can be decoded.
     * This method is expected to be fast, only a minimal decoding should be performed.
     * Most format have a signature at the beginning of a file, it is
     * enough to test only this signature.
     *
     * Note : subtype formats should implement a deeper test.
     *
     * @param input object or Document describing parameters.
     * @return true if input can be decoded by this format.
     * @throws science.unlicense.encoding.api.io.IOException
     */
    boolean canDecode(Object input) throws IOException;

    /**
     * Finding the expected end of the current format can have multiple use.
     *
     * If the file size is available near the begining, often in the header then
     * it should be returned whatever value has the fullscan flag.
     *
     * If the size can not be determinated then method should return -1 unless
     * the fullscan flag is set to true it which case it should read the stream until it
     * finds the end.
     *
     * Formats are allowed to return -1 in any case.
     *
     * @param in
     * @param fullscan
     * @return
     * @throws IOException
     */
    long searchEnd(ByteInputStream in, boolean fullscan) throws IOException;

    /**
     * Get a description of parameters used to open a new store.
     *
     * @return DocumentType, never null
     */
    DocumentType getParameters();

    /**
     *
     * @param source input object or Document describing parameters.
     * @return
     */
    Store open(Object source) throws IOException;

}
