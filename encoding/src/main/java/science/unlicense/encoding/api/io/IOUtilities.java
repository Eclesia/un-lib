
package science.unlicense.encoding.api.io;

import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.encoding.api.path.Path;

/**
 * Utility methods for stream manipulation.
 *
 * @author Johann Sorel
 */
public final class IOUtilities {

    private IOUtilities() {}

    public static ByteInputStream toInputStream(Object input, boolean[] closeStream) throws IOException{
        ByteInputStream byteStream = null;
        if (input instanceof ByteInputStream) {
            byteStream = (ByteInputStream) input;
            //we did not create the stream, we do not close it.
            closeStream[0] = false;
            return byteStream;
        } else if (input instanceof Path) {
            byteStream = ((Path) input).createInputStream();
            closeStream[0] = true;
            return byteStream;
        } else if (input instanceof byte[]) {
            byteStream = new ArrayInputStream((byte[]) input);
            closeStream[0] = true;
            return byteStream;
        } else {
            throw new IOException(input, "Unsupported input : " + input);
        }
    }

    public static ByteOutputStream toOutputStream(Object output, boolean[] closeStream) throws IOException{
        ByteOutputStream byteStream = null;
         if (output instanceof ByteOutputStream) {
            byteStream = (ByteOutputStream) output;
            //we did not create the stream, we do not close it.
            closeStream[0] = false;
            return byteStream;
        } else if (output instanceof Path) {
            byteStream = ((Path) output).createOutputStream();
            closeStream[0] = true;
            return byteStream;
        } else {
            throw new IOException(output, "Unsupported input : " + output);
        }
    }

    /**
     * Read all bytes in given stream.
     * Does not close the stream.
     *
     * @param stream
     * @return byte[]
     */
    public static byte[] readAll(final ByteInputStream stream) throws IOException{
        final ByteSequence buffer = new ByteSequence();

        final byte[] temp = new byte[1024];
        int nb = 0;
        while ((nb=stream.read(temp)) >= 0) {
            buffer.put(temp,0,nb);
        }

        return buffer.toArrayByte();
    }

    /**
     * Copy content from input stream to output stream.
     * Streams are not closed.
     *
     * @param in stream to read from, not null
     * @param out stream to write to, not null
     * @return number of copied bytes
     */
    public static long copy(final ByteInputStream in, final ByteOutputStream out) throws IOException{
        final byte[] buffer = new byte[32000];
        long count = 0;
        while (true) {
            final int nb = in.read(buffer);
            if (nb==-1) break;
            out.write(buffer,0,nb);
            count += nb;
        }
        return count;
    }

    public static void skip(ByteInputStream in, long length) throws IOException {
        while (length>0) {
            final long nb = in.skip(length);
            if (nb < 0) {
                throw new EOSException(in, null);
            }
            length -= nb;
        }
    }
}
