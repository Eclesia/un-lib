package science.unlicense.encoding.api.io;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.number.Endianness;

/**
 * ByteInputStream handling number parsing in big or little endian.
 *
 * @author Johann Sorel
 */
public class DataInputStream extends WrapInputStream {

    /** Indicate to read bit starting from the left most bit */
    public static final int MSB = 0;
    /** Indicate to read bit starting from the right most bit */
    public static final int LSB = 1;

    protected Endianness endianness;
    protected int bitDirection = MSB;

    // working buffer
    private final byte temp[] = new byte[8];
    //current offset from start.
    private long byteOffset = 0;

    // when reading bit per bit, keep track of remaining bits
    private int bitOffset = 0;
    private int currentByte = -1;

    //chars byte buffer;
    private final ByteSequence charBuffer = new ByteSequence();

    /**
     * Create a DatainputStream with big endian number encoding.
     * @param in
     */
    public DataInputStream(final ByteInputStream in) {
        this(in,Endianness.BIG_ENDIAN);
    }

    public DataInputStream(final ByteInputStream in, final Endianness encoding) {
        super(in);
        this.endianness = encoding;
    }

    public long getByteOffset() {
        return byteOffset;
    }

    /**
     * Get default stream encoding.
     * @return
     */
    public Endianness getEndianness() {
        return endianness;
    }

    /**
     * Set default stream encoding
     * @param encoding
     */
    public void setEndianness(Endianness encoding) {
        this.endianness = encoding;
    }


    /**
     * Get default bits direction.
     * @return
     */
    public int getBitsDirection() {
        return bitDirection;
    }

    /**
     * Set default bits direction.
     * @param bitDirection
     */
    public void setBitsDirection(int bitDirection) {
        this.bitDirection = bitDirection;
    }

    public int read() throws IOException {
        if (bitOffset == 0) {
            byteOffset++;
            return in.read();
        } else {
            int i = (int) readBits(8);
            return i;
        }
    }

    /**
     * @return current bit offset, between 0 and 8.
     */
    public int getBitOffset() {
        return bitOffset;
    }

    /**
     * Read given number of bits, interpreted as int.
     *
     * @param nbBits
     * @return int
     * @throws IOException
     */
    public int readBits(int nbBits) throws IOException{
        return readBits(nbBits, bitDirection);
    }

    /**
     * Read given number of bits, interpreted as int.
     *
     * @param nbBits : number of bits to read
     * @param startOrder : LSB or MSB
     * @return int
     * @throws IOException
     */
    public int readBits(int nbBits, int startOrder) throws IOException{
//        if (nbBits==8 && bitOffset==0) {
//            byteOffset++;
//            return in.read();
//        }
        if (nbBits < 0) {
            throw new IOException(this, "Number of bits must be positive");
        }

        int value = 0;
        if (MSB == startOrder) {
            for (int i=0;i<nbBits;i++) {
                value <<= 1;
                if (currentByte == -1) {
                    byteOffset++;
                    int next = in.read();
                    if (next == -1) throw new EOSException(this, null);
                    currentByte = next;
                }

                final int displacement = (7-bitOffset);
                final int mask = 1 << displacement;
                int t = currentByte & mask;
                value |= t >> displacement;
                bitOffset++;
                if (bitOffset == 8) {
                    currentByte = -1;
                    bitOffset = 0;
                }
            }
        } else if (LSB == startOrder) {
            for (int i=0;i<nbBits;i++) {
                if (currentByte == -1) {
                    byteOffset++;
                    int next = in.read();
                    if (next == -1) throw new EOSException(this, null);
                    currentByte = next;
                }

                final int mask = 1 << bitOffset;
                int t = currentByte & mask;
                value |= (t >> bitOffset) << i;
                bitOffset++;
                if (bitOffset == 8) {
                    currentByte = -1;
                    bitOffset = 0;
                }
            }
        } else {
            throw new IOException(this, "start order not supported, must be LEFT_TO_RIGHT or RIGHT_TO_LEFT : " + startOrder);
        }

        return value;
    }

    public int[] readBits(int[] buffer, int nbBits) throws IOException {
        return readBits(buffer,0,buffer.length,nbBits);
    }

    public int[] readBits(int[] buffer, final int offset, final int length, int nbBits) throws IOException {
        for (int i=0;i<length;i++) {
            buffer[offset+i] = readBits(nbBits);
        }
        return buffer;
    }

    /**
     * Read given number of bits, interpreted as long.
     *
     * @param nbBits
     * @return int
     * @throws IOException
     */
    public long readLongBits(int nbBits) throws IOException{
        return readBits(nbBits, bitDirection);
    }

    /**
     * Read given number of bits, interpreted as long.
     *
     * @param nbBits : number of bits to read
     * @param startOrder : LEFT_TO_RIGHT or RIGHT_TO_LEFT
     * @return int
     * @throws IOException
     */
    public long readLongBits(int nbBits, int startOrder) throws IOException{
        if (nbBits==8 && bitOffset==0) {
            byteOffset++;
            return in.read();
        }
        if (nbBits < 0) {
            throw new IOException(this, "Number of bits must be positive");
        }

        long value = 0;
        if (MSB == startOrder) {
            for (int i=0;i<nbBits;i++) {
                value <<= 1;
                if (currentByte == -1) {
                    byteOffset++;
                    int next = in.read();
                    if (next == -1) throw new EOSException(this, null);
                    currentByte = next;
                }

                final int displacement = (7-bitOffset);
                final long mask = 1l << displacement;
                long t = currentByte & mask;
                value |= t >> displacement;
                bitOffset++;
                if (bitOffset == 8) {
                    currentByte = -1;
                    bitOffset = 0;
                }
            }
        } else if (LSB == startOrder) {
            for (int i=0;i<nbBits;i++) {
                if (currentByte == -1) {
                    byteOffset++;
                    int next = in.read();
                    if (next == -1) throw new EOSException(this, null);
                    currentByte = next;
                }

                final long mask = 1l << bitOffset;
                long t = currentByte & mask;
                value |= (t >> bitOffset) << i;
                bitOffset++;
                if (bitOffset == 8) {
                    currentByte = -1;
                    bitOffset = 0;
                }
            }
        } else {
            throw new IOException(this, "start order not supported, must be LEFT_TO_RIGHT or RIGHT_TO_LEFT : " + startOrder);
        }

        return value;
    }

    /**
     * Read given number of bits, the first bit is interpreted as the sign.
     *
     * @param nbBits : number of bits to read
     * @return int
     * @throws IOException
     */
    public int readSignedBits(int nbBits) throws IOException{
        return readSignedBits(nbBits, bitDirection);
    }

    /**
     * Read given number of bits, the first bit is interpreted as the sign.
     *
     * @param nbBits : number of bits to read
     * @param startOrder : LEFT_TO_RIGHT or RIGHT_TO_LEFT
     * @return int
     * @throws IOException
     */
    public int readSignedBits(int nbBits, int startOrder) throws IOException{
        int val = readBits(nbBits);
        final int offset = 32-nbBits;
        //push bits to the left to put the first bit on the sign bit
        val <<= offset;
        //push back with >> and not >>> to leave the bit sign in place
        val >>= offset;
        return val;
    }

    public int read(final byte buffer[], final int offset, final int length) throws IOException {
        if (bitOffset == 0) {
            //read directly in the main stream
            final int nb = in.read(buffer, offset, length);
            byteOffset +=nb;
            return nb;
        } else {
            //bit offset prevent direct read.
            int position = offset;
            for (int n = offset+length; position<n; position++) {
                final int b = read();
                if (b == -1) {
                    position--;
                    break;
                }
                buffer[position] = (byte) b;
            }
            return position-offset;
        }
    }

    public byte[] readFully(final byte[] buffer) throws IOException {
        return readFully(buffer, 0, buffer.length);
    }

    public byte[] readFully(final byte[] buffer, final int offset, final int length) throws IOException {
        if (offset<0 || length<0 || offset+length>buffer.length) {
            throw new IndexOutOfBoundsException();
        }

        for (int x=0; x<length; ) {
            final int nb = read(buffer, offset+x, length-x);
            if (nb<0) {
                throw new EOSException(this, null);
            }
            x += nb;
        }
        return buffer;
    }

    public void readFully(Buffer buffer) throws IOException {
        readFully(buffer,0, (int) buffer.getByteSize());
    }

    public void readFully(Buffer buffer, int offset, int length) throws IOException {
        int nbRead;
        while (length > 0) {
            nbRead = read(buffer,offset,length);
            if (nbRead<0) {
                throw new EOSException(this, null);
            }
            length -= nbRead;
            offset += nbRead;
        }

    }

    public long skip(long n) throws IOException {
        if (bitOffset == 0) {
            //skip directly in the main stream
            final long nb = in.skip(n);
            byteOffset += nb;
            return nb;
        } else {
            //bit offset prevent directly skipping bytes.
            long toRead = n;
            final byte[] buffer = new byte[256];
            while (toRead > 0) {
                final int nbRead = read(buffer, 0, (int) Math.min(256, toRead));
                if (nbRead<0) {
                    break;
                }
                toRead -= nbRead;
            }
            return n-toRead;
        }
    }

    /**
     * Skip n bytes, do not return until the requested number of
     * bytes has been read.
     *
     * @param length number of bytes to skip.
     * @throws IOException
     */
    public void skipFully(long length) throws IOException{
        while (length>0) {
            final long nb = skip(length);
            if (nb<0) {
                throw new EOSException(this, null);
            }
            length -= nb;
        }
    }

    /**
     * Skip remaining bits in current byte.
     * If the current bit offset is 0 this method does not skip the byte.
     *
     * @return true if some bits have been skipped
     */
    public boolean skipToByteEnd() throws IOException{
        if (bitOffset!=0) {
            readBits(8-bitOffset);
            return true;
        }
        return false;
    }

    public byte readByte() throws IOException {
        final int b = read();
        if (b<0) {
            throw new EOSException(this, null);
        }
        return endianness.readByte(b);
    }

    public byte[] readBytes(int nb) throws IOException {
        return readFully(new byte[nb]);
    }

    public int readUByte() throws IOException {
        final int b = read();
        if (b<0) {
            throw new EOSException(this, null);
        }
        return endianness.readUByte(b);
    }

    public int[] readUByte(int nb) throws IOException{
        return readUByte(new int[nb],0,nb);
    }

    public int[] readUByte(int[] buffer) throws IOException {
        return readUByte(buffer,0,buffer.length);
    }

    public int[] readUByte(int[] buffer, final int offset,
            final int length) throws IOException {
        for (int i=0;i<length;i++) {
            buffer[offset+i] = DataInputStream.this.readUByte();
        }
        return buffer;
    }

    public short readShort() throws IOException {
        return readShort(endianness);
    }

    public short readShort(Endianness encoding) throws IOException {
        readFully(temp, 0, 2);
        return encoding.readShort(temp, 0);
    }

    public short[] readShort(int nb) throws IOException{
        return readShort(new short[nb],0,nb,endianness);
    }

    public short[] readShort(int nb, Endianness encoding) throws IOException{
        return readShort(new short[nb],0,nb,encoding);
    }

    public short[] readShort(short[] buffer) throws IOException {
        return readShort(buffer,0,buffer.length,endianness);
    }

    public short[] readShort(short[] buffer, final int offset, final int length) throws IOException {
        return readShort(buffer,offset,length,endianness);
    }

    public short[] readShort(short[] buffer, Endianness encoding) throws IOException {
        return readShort(buffer,0,buffer.length,encoding);
    }

    public short[] readShort(short[] buffer, final int offset,
            final int length, Endianness encoding) throws IOException {
        for (int i=0;i<length;i++) {
            buffer[offset+i] = readShort(encoding);
        }
        return buffer;
    }

    public int readUShort() throws IOException {
        return DataInputStream.this.readUShort(endianness);
    }

    public int readUShort(Endianness encoding) throws IOException {
        readFully(temp, 0, 2);
        return encoding.readUShort(temp, 0);
    }

    public int[] readUShort(int nb) throws IOException{
        return readUShort(new int[nb],0,nb,endianness);
    }

    public int[] readUShort(int nb, Endianness encoding) throws IOException{
        return readUShort(new int[nb],0,nb,encoding);
    }

    public int[] readUShort(int[] buffer) throws IOException {
        return readUShort(buffer,0,buffer.length,endianness);
    }

    public int[] readUShort(int[] buffer, final int offset, final int length) throws IOException {
        return readUShort(buffer,offset,length,endianness);
    }

    public int[] readUShort(int[] buffer, Endianness encoding) throws IOException {
        return readUShort(buffer,0,buffer.length,encoding);
    }

    public int[] readUShort(int[] buffer, final int offset,
            final int length, Endianness encoding) throws IOException {
        for (int i=0;i<length;i++) {
            buffer[offset+i] = DataInputStream.this.readUShort(encoding);
        }
        return buffer;
    }


    public int readInt24() throws IOException {
        return readInt24(endianness);
    }

    public int readInt24(Endianness encoding) throws IOException {
        readFully(temp, 0, 3);
        return encoding.readInt24(temp, 0);
    }

    public int[] readInt24(int nb) throws IOException{
        return readInt24(new int[nb],0,nb,endianness);
    }

    public int[] readInt24(int nb, Endianness encoding) throws IOException{
        return readInt24(new int[nb],0,nb,encoding);
    }

    public int[] readInt24(int[] buffer) throws IOException {
        return readInt24(buffer,0,buffer.length,endianness);
    }

    public int[] readInt24(int[] buffer, final int offset, final int length) throws IOException {
        return readInt24(buffer,offset,length,endianness);
    }

    public int[] readInt24(int[] buffer, Endianness encoding) throws IOException {
        return readInt24(buffer,0,buffer.length,encoding);
    }

    public int[] readInt24(int[] buffer, final int offset,
            final int length, Endianness encoding) throws IOException {
        for (int i=0;i<length;i++) {
            buffer[offset+i] = readInt24(encoding);
        }
        return buffer;
    }

    public int readUInt24() throws IOException {
        return DataInputStream.this.readUInt24(endianness);
    }

    public int readUInt24(Endianness encoding) throws IOException {
        readFully(temp, 0, 3);
        return encoding.readUInt24(temp, 0);
    }

    public int[] readUInt24(int nb) throws IOException{
        return readUInt24(new int[nb],0,nb,endianness);
    }

    public int[] readUInt24(int nb, Endianness encoding) throws IOException{
        return readUInt24(new int[nb],0,nb,encoding);
    }

    public int[] readUInt24(int[] buffer) throws IOException {
        return readUInt24(buffer,0,buffer.length,endianness);
    }

    public int[] readUInt24(int[] buffer, final int offset, final int length) throws IOException {
        return readUInt24(buffer,offset,length,endianness);
    }

    public int[] readUInt24(int[] buffer, Endianness encoding) throws IOException {
        return readUInt24(buffer,0,buffer.length,encoding);
    }

    public int[] readUInt24(int[] buffer, final int offset,
            final int length, Endianness encoding) throws IOException {
        for (int i=0;i<length;i++) {
            buffer[offset+i] = DataInputStream.this.readUInt24(encoding);
        }
        return buffer;
    }

    public int readInt() throws IOException {
        return readInt(endianness);
    }

    public int readInt(Endianness encoding) throws IOException {
        readFully(temp, 0, 4);
        return encoding.readInt(temp, 0);
    }

    public int[] readInt(int nb) throws IOException{
        return readInt(new int[nb],0,nb,endianness);
    }

    public int[] readInt(int nb, Endianness encoding) throws IOException{
        return readInt(new int[nb],0,nb,encoding);
    }

    public int[] readInt(int[] buffer) throws IOException {
        return readInt(buffer,0,buffer.length,endianness);
    }

    public int[] readInt(int[] buffer, final int offset, final int length) throws IOException {
        return readInt(buffer,offset,length,endianness);
    }

    public int[] readInt(int[] buffer, Endianness encoding) throws IOException {
        return readInt(buffer,0,buffer.length,encoding);
    }

    public int[] readInt(int[] buffer, final int offset,
            final int length, Endianness encoding) throws IOException {
        for (int i=0;i<length;i++) {
            buffer[offset+i] = readInt(encoding);
        }
        return buffer;
    }

    public long readUInt() throws IOException {
        return DataInputStream.this.readUInt(endianness);
    }

    public long readUInt(Endianness encoding) throws IOException {
        readFully(temp, 0, 4);
        return encoding.readUInt(temp, 0);
    }

    public long[] readUInt(int nb) throws IOException{
        return readUInt(new long[nb],0,nb,endianness);
    }

    public long[] readUInt(int nb, Endianness encoding) throws IOException{
        return readUInt(new long[nb],0,nb,encoding);
    }

    public long[] readUInt(long[] buffer) throws IOException {
        return readUInt(buffer,0,buffer.length,endianness);
    }

    public long[] readUInt(long[] buffer, final int offset, final int length) throws IOException {
        return readUInt(buffer,offset,length,endianness);
    }

    public long[] readUInt(long[] buffer, Endianness encoding) throws IOException {
        return readUInt(buffer,0,buffer.length,encoding);
    }

    public long[] readUInt(long[] buffer, final int offset,
            final int length, Endianness encoding) throws IOException {
        for (int i=0;i<length;i++) {
            buffer[offset+i] = DataInputStream.this.readUInt(encoding);
        }
        return buffer;
    }

    /**
     * Read a variable length integer.
     *
     * resource :
     * https://en.wikipedia.org/wiki/LEB128 (Note : provided pseudo-code is crap)
     *
     * @return int
     * @throws IOException
     */
    public long readVarLengthUInt() throws IOException{
        int b;
        long v;
        long result = 0;
        int offset = 0;

        for (;;) {
            b = readUByte();
            v = b & 0x7F;
            result = result | (v << offset);
            if ((b & 0x80) == 0) break;
            offset += 7;
        }

        return result;
    }

    /**
     * Read a variable length signed integer.
     *
     * resource :
     * https://en.wikipedia.org/wiki/LEB128 (Note : provided pseudo-code is crap)
     *
     * @return int
     * @throws IOException
     */
    public long readVarLengthInt() throws IOException{
        int b;
        long v;
        long result = 0;
        int offset = 0;

        for (;;) {
            b = readUByte();
            v = b & 0x7F;
            result = result | (v << offset);
            offset += 7;
            if ((b & 0x80) == 0) break;
        }
        boolean signed = (b & 0x40) != 0;
        //check we don't go over 64bits
        if (offset>=64) offset = 63;

        if (signed) {
            return result | (-1l << offset);
        } else {
            return result;
        }
    }

    public long readLong() throws IOException {
        return readLong(endianness);
    }

    public long readLong(Endianness encoding) throws IOException {
        readFully(temp, 0, 8);
        return encoding.readLong(temp, 0);
    }

    public long[] readLong(int nb) throws IOException{
        return readLong(new long[nb],0,nb,endianness);
    }

    public long[] readLong(int nb, Endianness encoding) throws IOException{
        return readLong(new long[nb],0,nb,encoding);
    }

    public long[] readLong(long[] buffer) throws IOException {
        return readLong(buffer,0,buffer.length,endianness);
    }

    public long[] readLong(long[] buffer, final int offset, final int length) throws IOException {
        return readLong(buffer,offset,length,endianness);
    }

    public long[] readLong(long[] buffer, Endianness encoding) throws IOException {
        return readLong(buffer,0,buffer.length,encoding);
    }

    public long[] readLong(long[] buffer, final int offset,
            final int length, Endianness encoding) throws IOException {
        for (int i=0;i<length;i++) {
            buffer[offset+i] = readLong(encoding);
        }
        return buffer;
    }

    public float readFloat() throws IOException {
        return readFloat(endianness);
    }

    public float readFloat(Endianness encoding) throws IOException {
        readFully(temp, 0, 4);
        return encoding.readFloat(temp, 0);
    }

    public float[] readFloat(int nb) throws IOException{
        return readFloat(new float[nb],0,nb,endianness);
    }

    public float[] readFloat(int nb, Endianness encoding) throws IOException{
        return readFloat(new float[nb],0,nb,encoding);
    }

    public float[] readFloat(float[] buffer) throws IOException {
        return readFloat(buffer,0,buffer.length,endianness);
    }

    public float[] readFloat(float[] buffer, final int offset, final int length) throws IOException {
        return readFloat(buffer,offset,length,endianness);
    }

    public float[] readFloat(float[] buffer, Endianness encoding) throws IOException {
        return readFloat(buffer,0,buffer.length,encoding);
    }

    public float[] readFloat(float[] buffer, final int offset,
            final int length, Endianness encoding) throws IOException {
        for (int i=0;i<length;i++) {
            buffer[offset+i] = readFloat(encoding);
        }
        return buffer;
    }

    public double readDouble() throws IOException {
        return readDouble(endianness);
    }

    public double readDouble(Endianness encoding) throws IOException {
        readFully(temp, 0, 8);
        return encoding.readDouble(temp, 0);
    }

    public double[] readDouble(int nb) throws IOException{
        return readDouble(new double[nb],0,nb,endianness);
    }

    public double[] readDouble(int nb, Endianness encoding) throws IOException{
        return readDouble(new double[nb],0,nb,encoding);
    }

    public double[] readDouble(double[] buffer) throws IOException {
        return readDouble(buffer,0,buffer.length,endianness);
    }

    public double[] readDouble(double[] buffer, final int offset, final int length) throws IOException {
        return readDouble(buffer,offset,length,endianness);
    }

    public double[] readDouble(double[] buffer, Endianness encoding) throws IOException {
        return readDouble(buffer,0,buffer.length,encoding);
    }

    public double[] readDouble(double[] buffer, final int offset,
            final int length, Endianness encoding) throws IOException {
        for (int i=0;i<length;i++) {
            buffer[offset+i] = readDouble(encoding);
        }
        return buffer;
    }

    /**
     * Zero terminated string are common in most format.
     *
     * @param maxSize : maximum size , 0 for no size limit
     * @param encoding : expected characters encoding
     * @return Chars
     * @throws IOException
     */
    public Chars readZeroTerminatedChars(int maxSize, CharEncoding encoding) throws IOException{
        charBuffer.removeAll();
        for (int i=1;;i++) {
            int b = readByte();
            if (b==0) {
                //reached end
                break;
            }
            charBuffer.put((byte) b);
            if (i==maxSize) {
                //reached max size
                break;
            }
        }
        return new Chars(charBuffer.toArrayByte(),encoding);
    }

    /**
     * Zero terminated string are common in most format.
     * To obtain a constant size in some parts like header, string may be defined
     * in a given maximum number of bytes, if the string to not fill the max size
     * then 0 bytes are used.
     *
     * @param blockSize : string block size
     * @param encoding : expected characters encoding
     * @return Chars
     * @throws IOException
     */
    public Chars readBlockZeroTerminatedChars(int blockSize, CharEncoding encoding) throws IOException{
        return readBlockZeroTerminatedChars(blockSize, encoding, true);
    }

    /**
     * Zero terminated string are common in most format.
     * To obtain a constant size in some parts like header, string may be defined
     * in a given maximum number of bytes, if the string to not fill the max size
     * then 0 bytes are used.
     *
     * @param blockSize : string block size
     * @param encoding : expected characters encoding
     * @param validate : trim out any unfinished character.
     * @return Chars
     * @throws IOException
     */
    public Chars readBlockZeroTerminatedChars(int blockSize, CharEncoding encoding, boolean validate) throws IOException{
        final byte[] buffer = new byte[blockSize];
        readFully(buffer);
        int end = buffer.length;
        for (int i=0;i<end;i++) {
            if (buffer[i] == 0x00) {
                end = i;
                break;
            }
        }

        if (validate) {
            //verify characters
            int l=0;
            int off;
            for (off=0;off<end;off+=l) {
                l = encoding.charlength(buffer, off);
            }
            end = off;
        }

        return new Chars(Arrays.copy(buffer, 0, end),encoding);
    }

    /**
     * Align the stream, skip any bytes needed to match alignement value.
     * This method moves the cursor forward if needed.
     * @param alignByteSize
     * @return
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public int realign(int alignByteSize) throws IOException{
        long res = byteOffset%4l;
        if (res==0) return 0;
        try{
            skipFully(4-res);
        }catch(EOSException ex) {
            return -1;
        }
        return (int) res;
    }

    @Override
    public void dispose() throws IOException {
        in.dispose();
    }

}
