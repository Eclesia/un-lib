package science.unlicense.encoding.api.io;

import science.unlicense.common.api.CObjects;

/**
 * Exception caused by inputs or outputs.
 *
 * @author Johann Sorel
 */
public class IOException extends Exception {

    private final Object origin;

    /**
     *
     */
    public IOException() {
        this(null, null, null);
    }

    /**
     *
     */
    public IOException(String message) {
        this(null, message, null);
    }

    /**
     *
     */
    public IOException(Throwable cause) {
        this(null, null, cause);
    }

    /**
     *
     * @param origin input or output source causing this exception, can be null.
     * @param message exception message
     */
    public IOException(Object origin, String message) {
        this(origin, message, null);
    }

    /**
     *
     * @param origin input or output source causing this exception, can be null.
     * @param cause exception cause
     */
    public IOException(Object origin, Throwable cause) {
        this(origin, null, cause);
    }

    /**
     *
     * @param origin input or output source causing this exception, can be null.
     * @param message exception message
     * @param cause exception cause
     */
    public IOException(Object origin, String message, Throwable cause) {
        super(message, cause);
        this.origin = origin;
    }

    @Override
    public String getMessage() {
        String message = super.getMessage();
        if (origin != null) {
            if (message == null) {
                message = "Origin : " + CObjects.toChars(origin).toString();
            } else {
                message += "\nOrigin : " + CObjects.toChars(origin).toString();
            }
        }
        return message;
    }

}
