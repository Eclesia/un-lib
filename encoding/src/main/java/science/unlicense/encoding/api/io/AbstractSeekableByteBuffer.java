

package science.unlicense.encoding.api.io;

import science.unlicense.math.api.Maths;


/**
 * Abstract seekable byte buffer.
 * All read,write,resize operations will raise an IOException.
 *
 * Read operations are redirected on read(byte[] buffer, int offset, int length)
 * Write operations are redirected on write(byte[] buffer, int offset, int length)
 * Insert operations are redirected on insert(byte[] buffer, int offset, int length)
 *
 *
 * @author Johann Sorel
 */
public abstract class AbstractSeekableByteBuffer implements SeekableByteBuffer {

    private static final byte[] BUFFER = new byte[4096];

    @Override
    public boolean isReadable() {
        return false;
    }

    @Override
    public boolean isWritable() {
        return false;
    }

    @Override
    public boolean isResizable() {
        return false;
    }

    @Override
    public int read() throws IOException {
        return read(new byte[1], 0, 1)[0] & 0xFF;
    }

    @Override
    public byte[] read(int nb) throws IOException {
        return read(new byte[nb], 0, nb);
    }

    @Override
    public byte[] read(byte[] buffer) throws IOException {
        return read(buffer,0,buffer.length);
    }

    @Override
    public byte[] read(byte[] buffer, int offset, int length) throws IOException {
        throw new IOException(null, "Not supported.");
    }

    @Override
    public long skip(final long nb) throws IOException {
        final int n = Maths.min((int) nb,4096);
        read(BUFFER, 0, n);
        return n;
    }

    @Override
    public void skipAll(long nb) throws IOException {
        while (nb>0) {
            nb -= skip(nb);
        }
    }

    @Override
    public void write(byte b) throws IOException {
        write(new byte[]{b}, 0, 1);
    }

    @Override
    public void write(byte[] buffer) throws IOException {
        write(buffer, 0, buffer.length);
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        throw new IOException(null, "Not supported.");
    }

    @Override
    public void insert(byte b) throws IOException {
        insert(new byte[]{b}, 0, 1);
    }

    @Override
    public void insert(byte[] buffer) throws IOException {
        insert(buffer, 0, buffer.length);
    }

    @Override
    public void insert(byte[] buffer, int offset, int length) throws IOException {
        throw new IOException(null, "Not supported.");
    }

    @Override
    public void remove(int nb) throws IOException {
        throw new IOException(null, "Not supported.");
    }

    @Override
    public void flush() throws IOException {
    }

    @Override
    public void dispose() throws IOException {
    }

}
