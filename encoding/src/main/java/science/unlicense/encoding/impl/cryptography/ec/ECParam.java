package science.unlicense.encoding.impl.cryptography.ec;

/**
 * The ECField class implements one of the interfaces to select compiled in
 * parameters. I am curious as to how much slower it would be if these
 * parameters were variables which could be changed at runtime. It would be
 * handy to select the field size at runtime by specifying GF_M. This class
 * simulates using runtime parameters. This will also require changing ECField
 * to move static parameters into another class, so I haven't done it yet.
 */
interface ECParam {

    /**
     * Dimension of the large finite field (GF_M = GF_L*GF_K).
     */
    int GF_M = 1264;
    /**
     * Dimension of the small finite field.
     */
    int GF_L = 16;
    /**
     * Degree of the large field reduction trinomial.
     */
    char GF_K = 79;
    /**
     * Intermediate power of the reduction trinomial.
     */
    int GF_T = 9;
    /**
     * Reduction polynomial for the small field (truncated).
     */
    int GF_RP = 0x002d;
    /**
     * Element of the large field with nonzero trace.
     */
    char GF_NZT = 0x0800;
    /**
     * Size of trace mask.
     */
    int GF_TM0 = 1;
    /**
     * 1st nonzero element of trace mask.
     */
    int GF_TM1 = 0x2800;
    /**
     * 2nd nonzero element of trace mask.
     */
    int GF_TM2 = 0;
    /**
     * Scalar term of elliptic curve equation (y^2 + xy = x^3 + EC_B).
     */
    char EC_B = 0x0805;
    /**
     * Cofactor of the selected elliptic curve point G.
     */
    int EC_COF = 65428;
    /**
     * MQV modulus (ceil (lg(r)/2), where r is the prime order of G).
     */
    int EC_H = 625;
}
