package science.unlicense.encoding.impl.cryptography.ec;

import java.math.BigInteger;
import java.util.Random;

/**
 * Elliptic curves over GF(2^m).
 *
 * This public domain software was written by Stuart D. Gathman,
 * based on C software written by Paulo S.L.M. Barreto
 * <pbarreto@uninet.com.br> based on original C++ software written by
 * George Barwood <george.barwood@dial.pipex.com>
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

public class ECPoint extends ECField {
  /* ECField has static functions implementing the field operations.
     We extend it to gain easy access to the names and minimize
     source changes from the C version until everything works.
     We could represent the field points with a class instead
     of hardwiring char[].  The overhead would not be much compared
     to the total work, but that would require more source changes -
     so we'll leave that for later.
     This class might also be more efficient if immutable.
   */

  private final char[] x = gfNew();
  private final char[] y = gfNew();
  private static final boolean trace = false;

  /** Create a new Elliptic Curve point at infinity (the zero value). */
  public ECPoint() { }

  /** Create a copy of an existing Elliptic Curve point. */
  public ECPoint(ECPoint q) { copy(q); }

  /** Unpack a BigInteger into a new Elliptic Curve point. */
  public ECPoint(BigInteger k) { unpack(k); }

  /* Sets p := q */
  public void copy(ECPoint q) {
    gfCopy(x, q.x);
    gfCopy(y, q.y);
  } /* ecCopy */

  /** Copy just the x coordinate. */
  void copyX(ECPoint q) {
    gfCopy(x, q.x);
  }

  private static final char[] b = { 1, EC_B };

  /** Given the x coordinate of p and a suitable bit ybit of the corresponding
      y coordinate, evaluate the full y value such that y^2 + x*y = x^3 + EC_B
      returning true if successful and false if there is no solution.
  */
  public boolean calcY (boolean ybit) {

    if (x[0] == 0) {
      /* elliptic equation reduces to y^2 = EC_B: */
      gfSquareRoot(y, EC_B);
      return true;
    }
    /* evaluate alpha = x^3 + b = (x^2)*x + EC_B: */
    final char[] a = gfNew();
    final char[] t = gfNew();
    try {
      gfSquare (t, x); /* keep t = x^2 for beta evaluation */
      gfMultiply (a, t, x);
      gfAdd (a, a, b); /* now a == alpha */
      if (a[0] == 0) {
        y[0] = 0;
        return true;
      }
      /* evaluate beta = alpha/x^2 = x + EC_B/x^2 */
      gfSmallDiv (t, EC_B);
      gfInvert (a, t);
      gfAdd (a, x, a); /* now a == beta */
      /* check if a solution exists: */
      if (gfTrace (a))
        return false; /* no solution */
      /* solve equation t^2 + t + beta = 0 so that gfYbit(t) == ybit: */
      gfQuadSolve (t, a);
      if (gfYbit(t) != ybit) {
        t[1] ^= 1;
      }
      /* compute y = x*t: */
      gfMultiply (y, x, t);
    }
    finally {
      /* destroy potentially sensitive data: */
      gfClear (a); gfClear (t);
    }
    return true;
  } /* ecCalcY */

  public final boolean isZero() {
    return x[0] == 0 && y[0] == 0;
  }


  /** Sets p := p + q */
  public void add(ECPoint q) {

    /* first check if there is indeed work to do (q != 0): */
    if (q.isZero()) return;
    if (!isZero()) {
      /* p != 0 and q != 0 */
      if (gfEqual (x, q.x)) {
        /* either p == q or p == -q: */
        if (gfEqual (y, q.y)) {
          /* points are equal; double p: */
          dbl();
        } else {
          /* must be inverse: result is zero */
          /* (should assert that q->y = p->x + p->y) */
          x[0] = y[0] = 0;
    }
      } else {
    /* p != 0, q != 0, p != q, p != -q */
    /* evaluate lambda = (y1 + y2)/(x1 + x2): */
    final char[] ty = gfNew();
    final char[] tx = gfNew();
    final char[] t = gfNew();
    final char[] lambda = gfNew();
    gfAdd (ty, y, q.y);
    gfAdd (tx, x, q.x);
    gfInvert (t, tx);
    gfMultiply (lambda, ty, t);
    /* evaluate x3 = lambda^2 + lambda + x1 + x2: */
    final char[] x3 = gfNew();
    gfSquare (x3, lambda);
    gfAdd (x3, x3, lambda);
    gfAdd (x3, x3, tx);
    /* evaluate y3 = lambda*(x1 + x3) + x3 + y1: */
    gfAdd (tx, x, x3);
    gfMultiply (t, lambda, tx);
    gfAdd (t, t, x3);
    gfAdd (y, t, y);
    /* deposit the value of x3: */
    gfCopy (x, x3);
      }
    }
    else // just copy q into p:
      copy(q);
  } /* ecAdd */


  /** sets p := p - r */
  public void sub (ECPoint r) {
    ECPoint t = new ECPoint();

    gfCopy (t.x, r.x);
    gfAdd  (t.y, r.x, r.y);
    add(t);
  } /* ecSub */


  /** Sets p := 2*p */
  public void dbl() {
    final char[] lambda = gfNew();
    final char[] t1 = gfNew();
    final char[] t2 = gfNew();

    /* evaluate lambda = x + y/x: */
    gfInvert (t1, x);
    if (trace)
      System.out.println("1/x = " + toString(t1));
    gfMultiply (lambda, y, t1);
    gfAdd (lambda, lambda, x);
    /* evaluate x3 = lambda^2 + lambda: */
    gfSquare (t1, lambda);
    gfAdd (t1, t1, lambda); /* now t1 = x3 */
    /* evaluate y3 = x^2 + lambda*x3 + x3: */
    gfSquare (y, x);
    gfMultiply (t2, lambda, t1);
    gfAdd (y, y, t2);
    gfAdd (y, y, t1);
    /* deposit the value of x3: */
    gfCopy (x, t1);
  } /* ecDouble */

  /** Sets p := k*p. */
  public ECPoint mult(BigInteger k) {
    ECPoint r = new ECPoint(this);
    /* set p to the point at infinity: */
    x[0] = 0; y[0] = 0;
    /* multiply p by k if necessary: */
    if (k.signum() != 0) {
      /* E.g., to multiply by x * 15, the bits are:
001111    (15)
101101    (3 * 15 = 45)
+   -
    I.e., we add x * 16 and sub x * 1.  Note that bit 0 is
    always the same and can be skipped.
       */
      BigInteger h = k.multiply(BigInteger.valueOf(3));
      int z = h.bitLength() - 1; // h.testBit(z) == 1
      for (int i = 1;;r.dbl()) {
    boolean hi = h.testBit(i);
    boolean ki = k.testBit(i);
    if (hi != ki) {
      if (hi)
        add(r);
      else
        sub(r);
      if (trace) {
        System.out.println("r = " + r);
        if (hi)
          System.out.println("add = " + this);
        else
          System.out.println("sub = " + this);
      }
    }
    if (i == z) break;
    ++i;
      }
      if (trace)
    System.out.println("-----------");
    }
    return this;
  } /* ecMultiply */

  /** evaluates to 0 if p->x == 0, otherwise to gfYbit (p->y / p->x) */
  public boolean Ybit() {

    if (x[0] == 0)
      return false;
    final char[] t1 = gfNew();
    final char[] t2 = gfNew();
    gfInvert (t1, x);
    gfMultiply (t2, y, t1);
    return gfYbit(t2);
  } /* ecYbit */

  /** Return just the X coord packed into a BigInteger. */
  public BigInteger packX() {
    return gfPack(x);
  }

  /** Pack this curve point into a BigInteger. */
  public BigInteger pack() {
    BigInteger k = gfPack(x);
    if (x[0] != 0) {
      k = k.shiftLeft(1);
      if (Ybit())
        k = k.add(BigInteger.valueOf(1));
    }
    else if (y[0] != 0)
      k = BigInteger.valueOf(1);
    return k;
  } /* ecPack */

  /** unpacks a vlPoint into a curve point */
  public void unpack(BigInteger a) {
    boolean yb = a.testBit(0);
    a = a.shiftRight(1);
    gfUnpack (x, a);
    if (x[0] != 0 || yb) {
      if (!calcY(yb))
        throw new RuntimeException("Invalid Elliptic Curve Point");
    }
    else
      y[0] = 0;
  } /* ecUnpack */


  /** Format the Elliptic Curve point as a String for debugging. */
  public String toString() {
    StringBuffer buf = new StringBuffer();
    buf.append('(');
    buf.append(toString(x));
    buf.append(", ");
    buf.append(toString(y));
    buf.append(')');
    return buf.toString();
  }

  /* Confirm that y^2 + x*y = x^3 + EC_B for point p. */
  public boolean check() {
    final char[] t1 = gfNew();
    final char[] t2 = gfNew();
    final char[] t3 = gfNew();
    gfSquare (t1, y);
    gfMultiply (t2, x, y);
    gfAdd (t1, t1, t2);    /* t1 := y^2 + x*y */
    gfSquare (t2, x);
    gfMultiply (t3, t2, x);
    gfAdd (t2, t3, b);    /*/ t2 := x^3 + EC_B */
    return gfEqual (t1, t2);
  } /* ecCheck */

  /** Evaluates to true if p == q, otherwise false (or an error code) */
  public boolean equals(Object obj) {
    if (!(obj instanceof ECPoint)) return false;
    ECPoint q = (ECPoint) obj;
    return gfEqual(x, q.x) && gfEqual(y, q.y);
  }

  /** Sets p to a random point of the elliptic curve defined by
    y^2 + x*y = x^3 + EC_B */
  public void random(Random rand) {
    do {
      /* generate a pseudo-random x component: */
      gfRandom(x,rand);
      /* evaluate the corresponding y component: */
    } while (!calcY(false));
    if (!check())
      throw new RuntimeException("Invalid elliptic curve point");
  } /* ecRandom */

  /** Sets p to the point at infinity O, clearing entirely the content of p.
      Clearing entirely sets all bits to zero to avoid any possibility
      of leaking secret data.
   */
  public void clear() {
    gfClear(x);
    gfClear(y);
  }

  /** sets p := -p */
  public void negate() {
    gfAdd(y, x, y);
  } /* ecNegate */

}
