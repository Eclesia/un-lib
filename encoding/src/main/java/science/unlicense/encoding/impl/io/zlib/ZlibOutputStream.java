
package science.unlicense.encoding.impl.io.zlib;

import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.WrapOutputStream;
import science.unlicense.encoding.impl.cryptography.hash.Adler32;
import science.unlicense.encoding.impl.cryptography.hash.HashOutputStream;
import science.unlicense.encoding.impl.io.deflate.DeflateOutputStream;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class ZlibOutputStream extends WrapOutputStream{

    private boolean header = false;
    private boolean finish = false;
    private DeflateOutputStream deflateOut;
    private HashOutputStream adlerOut;

    public ZlibOutputStream(ByteOutputStream out) {
        super(out);
    }

    public void write(byte b) throws IOException {
        writeHeader();
        adlerOut.write(b);
    }

    public void flush() throws IOException {
        deflateOut.flush();
        super.flush();
    }

    /**
     * Call to finish the zlib stream.
     * Calculates and write the CRC32 checksum.
     * @throws IOException
     */
    public void finish() throws IOException{
        if (finish) return;
        finish = true;

        adlerOut.flush();
        final byte[] bs = new byte[4];
        Endianness.BIG_ENDIAN.writeInt((int) adlerOut.hashValue(), bs, 0);
        out.write(bs);
    }

    public void close() throws IOException {
        finish();
        super.close();
    }

    private void writeHeader() throws IOException{
        if (header) return;
        header = true;

        // deflate method + window
        final int cm = 8;
        final int cinfo = 7 << 4;
        final int cmf = cm | cinfo;
        out.write((byte) cmf);

        final int fdict  = 0 << 5; //no dictionary
        final int flevel = 1 << 6; //uncompressed ~ speed
        int flg = fdict | flevel;

        //The FCHECK value must be such that CMF and FLG, when viewed as
        //a 16-bit unsigned integer stored in MSB order (CMF*256 + FLG),
        //is a multiple of 31.
        int fcheck = ((cmf << 8) | flg) % 31;
        if (fcheck > 0) {
            flg += (31 - fcheck);
        }
        out.write((byte) flg);
        deflateOut = new DeflateOutputStream(out);
        adlerOut = new HashOutputStream(new Adler32(), deflateOut);
    }


}
