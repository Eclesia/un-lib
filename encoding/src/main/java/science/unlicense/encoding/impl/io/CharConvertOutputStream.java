
package science.unlicense.encoding.impl.io;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.WrapOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * On the fly character encoding change stream.
 *
 * @author Johann Sorel
 */
public class CharConvertOutputStream extends WrapOutputStream{

    public CharConvertOutputStream(ByteOutputStream out) {
        super(out);
    }

    @Override
    public void write(byte b) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
