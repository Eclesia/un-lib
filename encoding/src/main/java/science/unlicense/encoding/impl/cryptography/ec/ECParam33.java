package science.unlicense.encoding.impl.cryptography.ec;

/**
 *
 */
interface ECParam33 {

    int GF_M = 33;
    int GF_L = 11;
    char GF_K = 3;
    int GF_T = 1;
    int GF_RP = 0x0005;
    char GF_NZT = 0x0800;
    int GF_TM0 = 1;
    int GF_TM1 = 0x0201;
    int GF_TM2 = 0;
    char EC_B = 0x0065;
    int EC_COF = 1964;
    int EC_H = 12;
}
