
package science.unlicense.encoding.impl.store.keyvalue;

import science.unlicense.common.api.collection.Collection;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface DocumentSet {

    Collection getDocuments() throws IOException;

    void insert(Collection docs) throws IOException;

}
