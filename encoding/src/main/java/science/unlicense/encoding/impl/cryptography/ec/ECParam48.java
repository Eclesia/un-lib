package science.unlicense.encoding.impl.cryptography.ec;

/**
 *
 */
interface ECParam48 {

    int GF_M = 48;
    int GF_L = 16;
    char GF_K = 3;
    int GF_T = 1;
    int GF_RP = 0x002d;
    char GF_NZT = 0x0800;
    int GF_TM0 = 1;
    int GF_TM1 = 0x2800;
    int GF_TM2 = 0;
    char EC_B = 0x0b56;
    int EC_COF = 65036;
    int EC_H = 17;
}
