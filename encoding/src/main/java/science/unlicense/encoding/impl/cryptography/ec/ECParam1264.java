package science.unlicense.encoding.impl.cryptography.ec;

/**
 *
 */
interface ECParam1264 {

    static final int GF_M = 1264;
    static final int GF_L = 16;
    static final char GF_K = 79;
    static final int GF_T = 9;
    static final int GF_RP = 0x002d;
    static final char GF_NZT = 0x0800;
    static final int GF_TM0 = 1;
    static final int GF_TM1 = 0x2800;
    static final int GF_TM2 = 0;
    static final char EC_B = 0x0805;
    static final int EC_COF = 65428;
    static final int EC_H = 625;

}
