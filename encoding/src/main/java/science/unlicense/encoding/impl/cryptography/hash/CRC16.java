
package science.unlicense.encoding.impl.cryptography.hash;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;

/**
 * CRC16 is composed of the 2 less significative bytes of a CRC32.
 * http://tools.ietf.org/html/rfc1952 (page6)
 *
 * @author Johann Sorel
 */
public class CRC16 extends CRC32{

    public static final Chars NAME = Chars.constant("CRC-16");

    public Chars getName() {
        return NAME;
    }

    public int getResultLength() {
        return 16;
    }


    public byte[] getResultBytes() {
        final byte[] buffer = new byte[2];
        Endianness.BIG_ENDIAN.writeUShort((int) getResultLong(), buffer, 0);
        return buffer;
    }

    public long getResultLong() {
        return crc & 0xFFFF;
    }

}
