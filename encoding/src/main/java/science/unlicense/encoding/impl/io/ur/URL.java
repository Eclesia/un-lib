
package science.unlicense.encoding.impl.io.ur;

import science.unlicense.common.api.character.Chars;

/**
 * Uniform Resource Locator
 *
 * @author Johann Sorel
 */
public class URL extends URI{

    public URL(Chars schema, Chars path) {
        super(schema, path);
    }

}
