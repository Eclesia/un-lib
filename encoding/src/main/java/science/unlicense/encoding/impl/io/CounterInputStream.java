
package science.unlicense.encoding.impl.io;

import science.unlicense.encoding.api.io.WrapInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * An input stream with an attached counter.
 * Counter of often used to keep track of position in the files.
 *
 * @author Johann Sorel
 */
public class CounterInputStream extends WrapInputStream{

    private long counter;

    public CounterInputStream(ByteInputStream in) {
        super(in);
    }

    public long getCounterValue() {
        return counter;
    }

    public void setCounterValue(long value) {
        this.counter = value;
    }

    public int read() throws IOException {
        int b = in.read();
        if (b!=-1) counter++;
        return b;
    }

    public int read(byte[] buffer, int offset, int length) throws IOException {
        final int nb = in.read(buffer, offset, length);
        if (nb>0) counter += nb;
        return nb;
    }

    public long skip(long n) throws IOException {
        final long nb = in.skip(n);
        counter += nb;
        return nb;
    }

}
