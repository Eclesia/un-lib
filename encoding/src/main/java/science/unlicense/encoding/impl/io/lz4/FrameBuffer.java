
package science.unlicense.encoding.impl.io.lz4;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.DefaultInt8Buffer;
import science.unlicense.common.api.collection.primitive.ByteSequence;

/**
 * NOTE,FIXME : memoty leak
 *
 * @author Johann Sorel
 */
public class FrameBuffer {

    private final int window;
    private final ByteSequence buffer = new ByteSequence();
    private final byte[] memory;
    private int memoryOffset = 0;
    private int bufferOffset = 0;

    public FrameBuffer(int windowSize) {
        this.window = windowSize;
        this.memory = new byte[windowSize*2];
    }

    public void put(byte b) {
        buffer.put(b);

        if (memoryOffset == memory.length) {
            Arrays.copy(memory, window, window, memory, 0);
            memoryOffset = window;
        }
        memory[memoryOffset] = b;
        memoryOffset++;
    }

    public byte remember(int distance) {
        return memory[memoryOffset-distance];
    }

    public boolean isEmpty() {
        return buffer.getSize()-bufferOffset <= 0;
    }

    public byte peek() {
        byte b = buffer.read(bufferOffset);
        bufferOffset++;
        return b;
    }

}
