
package science.unlicense.encoding.impl.io.huffman;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public final class HuffmanNode extends CObject{

    public int length;
    public int code;
    public Object value;

    //for internal use only
    boolean leaf;
    HuffmanNode next0;
    HuffmanNode next1;

    /**
     *
     * @param length
     * @param code bit values are increasing, the last bit is at (code >> (length-1)) & 0x01
     */
    public HuffmanNode(int length, int code) {
        this(length,code,null);
    }

    public HuffmanNode(int length, int code, Object value) {
        this.length = length;
        this.code = code;
        this.value = value;
    }

    final int getBit(int index) {
        return (code >> index) & 0x01;
    }

    public Chars toChars() {
        Chars str = Chars.EMPTY; //Int32.encode(length).concat(' ').concat(Int32.encode(code)).concat(' ').concat(' ');

        for (int i=0;i<length;i++) {
            str = str.concat( (getBit(i) == 0) ? '0' : '1' );
        }

        if (!leaf) {
            str = Nodes.toChars(str, new Object[]{next0,next1});
        } else {
            str = str.concat(new Chars(" = ")).concat(CObjects.toChars(value));
        }

        return str;
    }

}
