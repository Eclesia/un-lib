package science.unlicense.encoding.impl.cryptography.hash;

import science.unlicense.common.api.character.Chars;

/**
 * Implementation of SHA-256.
 *
 * @author Francois Berder
 *
 */
public final class SHA256 extends SHA2_32 {

    public static final Chars NAME = Chars.constant("SHA-256");

    private static final int resetState[] = {
        0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
        0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19
    };

    public SHA256() {
        super(resetState);
    }

    @Override
    public Chars getName() {
        return NAME;
    }

    @Override
    public int getResultLength() {
        return 256;
    }
}
