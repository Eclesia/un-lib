
package science.unlicense.encoding.impl.io;

import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.WrapOutputStream;

/**
 * An output stream with an attached counter.
 * Counter of often used to keep track of position in the files.
 *
 * @author Johann Sorel
 */
public class CounterOutputStream extends WrapOutputStream{

    private long counter;

    public CounterOutputStream(ByteOutputStream in) {
        super(in);
    }

    public long getCounterValue() {
        return counter;
    }

    public void setCounterValue(long value) {
        this.counter = value;
    }

    @Override
    public void write(byte b) throws IOException {
        counter++;
        out.write(b);
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        out.write(buffer, offset, length);
        counter += length;
    }


}
