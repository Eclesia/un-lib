

package science.unlicense.encoding.impl.io.rolz;

/**
 * Reduced Offset Lempel-Ziv.
 * Derivate from LZ77 compression.
 *
 * TODO
 *
 * References :
 * http://fr.wikipedia.org/wiki/ROLZ
 *
 * Source available in public domain :
 * http://sourceforge.net/projects/balz/files/balz/1.15/
 *
 * @author Johann Sorel
 */
public class ROLZInputStream {

}
