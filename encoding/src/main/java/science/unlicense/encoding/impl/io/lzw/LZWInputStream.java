
package science.unlicense.encoding.impl.io.lzw;

import science.unlicense.encoding.api.io.AbstractInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * TODO
 *
 * @author Johann Sorel
 */
public class LZWInputStream extends AbstractInputStream {

    private final DataInputStream in;
    private final int codeSize;

    public LZWInputStream(final ByteInputStream in, int codeSize) {
        if (in instanceof DataInputStream) {
            this.in = (DataInputStream) in;
        } else {
            this.in = new DataInputStream(in);
        }
        this.codeSize = codeSize;
    }

    @Override
    public int read() throws IOException {
        throw new IOException(this, "Not supported yet.");
    }

    @Override
    public void dispose() throws IOException {
        in.dispose();
    }

}
