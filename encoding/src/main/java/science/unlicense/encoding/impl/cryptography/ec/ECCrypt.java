package science.unlicense.encoding.impl.cryptography.ec;
import java.math.BigInteger;

/**
  Elliptic curve cryptographic primitives.  The cryptography is based on
  the fact that it is reasonably fast to multiply an elliptic curve point by an
  integer (k*P), but apparently very hard to recover the integer by
  computing the inverse of an elliptic curve point.  Thus, if (x) is
  a secret key, making the product (x*P) public hopefully won't
  expose the secret key.  Multiplication is commutative, associative, and
  distributive so that x*k*P == k*x*P == (k*x)*P and (x + k)*P == x*P + k*P.
  Addition of curve points is commutative and associative, so that
  x*P + k*P == k*P + x*P.

  This public domain software was written by Stuart D. Gathman
  based on a C program written by Paulo S.L.M. Barreto
  <pbarreto@uninet.com.br> based on original C++ software written by
  George Barwood <george.barwood@dial.pipex.com>

  THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  @author Stuart D. Gathman
 */

public class ECCrypt {

  private ECPoint curve_point;

  public ECCrypt(ECPoint curve_point) {
    this.curve_point = curve_point;
  }

  public static class Pair {
    BigInteger r,s;
  }

  /** Compute the public key for a private key.
    @param privateKey The private key (x).
    @return the public key (x*P).
   */
  public BigInteger makePublicKey(BigInteger privateKey) {
    ECPoint ecPublicKey = new ECPoint(curve_point);
    ecPublicKey.mult(privateKey);
    return ecPublicKey.pack();
  } /* cpMakePublicKey */

  /** Encode a secret into a public form and a shared value.
    @param secret holds a one-time secret multiplier (k).
    @param publicKey holds a partner's public key (x*P).
    @return the secret multiplier's public form (k*P) in r
    *       and the shared value (k*x*P) in s.
  */
  public Pair encodeSecret(BigInteger publicKey, BigInteger secret) {
    Pair p = new Pair();
    ECPoint q = new ECPoint(curve_point);
    q.mult(secret);
    p.r = q.pack();
    q.unpack(publicKey);
    q.mult(secret);
    p.s = q.packX();
    return p;
  } /* cpMakeSecret */

  /** Decode a secret shared value.
   @param privateKey holds one's secret key (x).
   @param message holds the public form of a partner's secret multiplier (k*P).
   @return the shared value (x*k*P).
  */
  BigInteger decodeSecret(BigInteger privateKey, BigInteger message) {
    ECPoint q = new ECPoint(message);
    q.mult(privateKey);
    return q.packX();
  } /* cpDecodeSecret */

  /** Compute a signature <code>[r = k*P + c, s = k - r*x] mod m<code>.
      The  elliptic curve point (P) was  supplied
      in the constructor.  The secret multiplier must be random.  For instance,
      if you use the same multiplier for two signatures, the secret key (x) can
      be recovered by subtracting:
      <code>s2 - s1 == (k - r2*x) - (k - r1*x) == (r1 - r2)*x</code>
      and <code>x == (s2 - s1)/(r1 - r2)</code>.

     @param privateKey The secret key (x).
     @param secret A one time secret multiplier (k).
     @param mac  The message authentication code or hash (c).
     @return the Pair (r,s).
   */
  public Pair sign(BigInteger privateKey, BigInteger secret, BigInteger mac) {
    ECPoint q = new ECPoint(curve_point);
    Pair sig = new Pair();
    q.mult(secret);
    sig.r = q.packX().add(mac);
    sig.s = secret.subtract(privateKey.multiply(sig.r));
    return sig;
  } /* cpSign */

  /** Verify a signature [r,s].  Computes <code>r - s*P + r*x*P</code>
   *  == <code>k*P + c - (k-r*x)*P + r*x*P</code> == <code>c</code> mod m,
   * and compares with mac.  The curve point (P)
   * was supplied in the contructor.
   *@param publicKey The signers public key (x*P).
   *@param mac The message authentication code or hash.
   *@param sig The signature (r,s).
   *@return true if the signature matches.
  */
  public boolean verify(BigInteger publicKey, BigInteger mac, Pair sig) {
    ECPoint t1 = new ECPoint(curve_point);
    t1.mult(sig.s);
    ECPoint t2 = new ECPoint(publicKey);
    t2.mult(sig.r);
    t1.add(t2);
    BigInteger t3 = sig.r.subtract(t1.packX());
    return t3.compareTo(mac) == 0;
  } /* cpVerify */

  /** Test key exchange and signature algorithms.
   */
  public static void main(String[] argv) throws Exception {
    java.util.Random rnd = new java.util.Random();
    ECPoint cp = new ECPoint();
    cp.random(rnd);
    ECCrypt ec = new ECCrypt(cp);
    BigInteger priv = new BigInteger(255,rnd);
    BigInteger pub = ec.makePublicKey(priv);
    // test key exchange
    BigInteger secret = new BigInteger(255,rnd); // random secret
    System.err.println("Secret = " + secret.toString(36));
    System.err.println("Public key = " + pub.toString(36));
    Pair p = ec.encodeSecret(pub,secret);
    System.err.println("Public msg = " + p.r.toString(36));
    System.err.println("Shared secret = " + p.s.toString(36));
    BigInteger s = ec.decodeSecret(priv,p.r);
    System.err.println("Recovered secret = " + s.toString(36));
    System.err.print("Key exchange test: ");
    if (s.compareTo(p.s) == 0)
      System.err.println("PASS");
    else
      System.err.println("FAIL");
    // test signature
    BigInteger mac;
    if (argv.length > 0) { // use param
      java.security.MessageDigest md = null;
      md = md.getInstance("MD5");
      byte[] b = argv[0].getBytes();
      md.update(b,0,b.length);
      mac = new BigInteger(md.digest());
    }
    else
      mac = new BigInteger(128,rnd);
    System.err.println("MAC = " + mac.toString(36));
    Pair sig = ec.sign(priv,secret,mac);
    System.err.println("sig.r = " + sig.r.toString(36));
    System.err.println("sig.s = " + sig.s.toString(36));
    System.err.print("DSA pos test: ");
    if (ec.verify(pub,mac,sig))
      System.err.println("PASS");
    else
      System.err.println("FAIL");
    System.err.print("DSA neg test: ");
    sig.r = sig.r.add(BigInteger.valueOf(1));
    if (!ec.verify(pub,mac,sig))
      System.err.println("PASS");
    else
      System.err.println("FAIL");
  }
}
