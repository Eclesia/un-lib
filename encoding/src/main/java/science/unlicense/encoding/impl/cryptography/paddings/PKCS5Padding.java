package science.unlicense.encoding.impl.cryptography.paddings;

/**
 *
 * @author Bertrand COTE
 */
public class PKCS5Padding implements Padding{

    private final int BLOCK_LENGTH;

    public PKCS5Padding( final int BLOCK_LENGTH ) {
        this.BLOCK_LENGTH = BLOCK_LENGTH;
    }

    @Override
    public byte[] pad(byte[] text) {

        int numberOfBytesToPadd = BLOCK_LENGTH - text.length%BLOCK_LENGTH;

        byte[] paddedText = new byte[text.length + numberOfBytesToPadd];
        System.arraycopy(text, 0, paddedText, 0, text.length);
        for (int i = text.length; i < paddedText.length; i++) {
            paddedText[i] = (byte) numberOfBytesToPadd;
        }
        return paddedText;
}

    @Override
    public byte[] unPad(byte[] text) throws InvalidPadException {

        final int padValue = text[text.length-1];

        // ========== Checking for InvalidPadException ==========

        // pad value in the limits: >0 and <= this.BLOCK_LENGTH
        if ((padValue < 1) || (padValue > this.BLOCK_LENGTH)) {
            throw new InvalidPadException();
        }
        // all padded bytes must be == padValue
        for ( int i=text.length-padValue; i<text.length; i++ ) {
            if ( text[i] != padValue ) {
                throw new InvalidPadException();
            }
        }

        // ========== Unpadding text ==========

        int newLength = text.length - padValue;
        byte[] unpaddedText = new byte[newLength];

        System.arraycopy(text, 0, unpaddedText, 0, newLength);
        return unpaddedText;
    }

}
