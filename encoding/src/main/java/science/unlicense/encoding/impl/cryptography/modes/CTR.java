package science.unlicense.encoding.impl.cryptography.modes;

import science.unlicense.encoding.impl.cryptography.algorithms.CipherAlgorithm;
import science.unlicense.encoding.impl.cryptography.paddings.InvalidPadException;
import science.unlicense.encoding.impl.cryptography.paddings.Padding;

import science.unlicense.common.api.Arrays;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Block cipher counter mode (CTR).
 *
 * https://en.wikipedia.org/wiki/Block_cipher_modes_of_operation#Counter_.28CTR.29
 *
 * @author Bertrand COTE
 */
public class CTR implements CipherMode {

    private final Padding padding;
    private final CipherAlgorithm algorithm;

    private static final int BLOCK_LENGTH = 16;

    // ========== Constructor ==================================================

    public CTR(CipherAlgorithm algorithm, Padding padding ) {
        this.algorithm = algorithm;
        this.padding = padding;
    }

    // ========== CipherMode methods ===========================================

    /**
     * In CTR mode, the IV is prepended to the plaintext.
     *
     * @param plainText the given plaintext with the IV prepended.
     * @return the ciphertext (First block is the IV)
     */
    @Override
    public byte[] cipher( byte[] plainText ) {

        return CTRProcess( this.padding.pad(plainText) );
    }

    @Override
    public void cipher( ByteInputStream input, ByteOutputStream output ) throws IOException {
        try {
            CTRStreamProcess( input, output, true );
        } catch ( InvalidPadException ipe ) {
            // Never occurs in cipher (only in invCipher)
        }
    }

    /**
     *
     * @param cipherText
     * @return the plaintext without the IV.
     * @throws InvalidPadException <B>WARNING:</B> to avoid padding <U>oracle
     * attacks</U>, do not let the user know the kind of exception thrown by
     * this method.
     */
    @Override
    public byte[] invCipher( byte[] cipherText ) throws InvalidPadException {

        byte[] plainText = Arrays.copy( CTRProcess(cipherText), BLOCK_LENGTH, cipherText.length-BLOCK_LENGTH );
        try {
            return this.padding.unPad(plainText);
        } catch ( InvalidPadException ipe ) {
            throw ipe;
        }
    }

    @Override
    public void invCipher( ByteInputStream input, ByteOutputStream output ) throws IOException, InvalidPadException {
        CTRStreamProcess( input, output, false );
    }


    // =========================================================================

    /**
     * Process the CTR cipher/invCipher.
     * @param input
     * @return
     */
    private byte[] CTRProcess( byte[] input ) {

        final int lastBlockLength = input.length % BLOCK_LENGTH;
        final int lastBlockIndex = input.length - lastBlockLength;

        byte[] IV = Arrays.copy(input, 0, BLOCK_LENGTH);
        byte[] IVCtrEnc = new byte[BLOCK_LENGTH];

        byte[] output = Arrays.copy(input);
        int length = BLOCK_LENGTH;

        for ( int i=BLOCK_LENGTH; i<input.length; i+=BLOCK_LENGTH ) {

            // Encode (IV + ctr)
            Arrays.copy(IV, 0, length, IVCtrEnc, 0);
            this.algorithm.cipher(IVCtrEnc);

            // output[block i ] = E( k, (IV + ctr)) XOR input[block i]
            if ( i == lastBlockIndex ) {
                length = lastBlockLength;
            }
            Arrays.arrayXOR(input, i, IVCtrEnc, 0, length);
            Arrays.copy(IVCtrEnc, 0, length, output, i);

            // increment ctr
            incIV(IV);
        }

        return output;
    }

    private void CTRStreamProcess( ByteInputStream input, ByteOutputStream output, boolean cipher ) throws IOException, InvalidPadException {

        byte[] IV = new byte[CTR.BLOCK_LENGTH];
        byte[] IVCtrEnc = new byte[CTR.BLOCK_LENGTH];
        byte[] previousBlock = new byte[CTR.BLOCK_LENGTH];
        byte[] currentBlock = new byte[CTR.BLOCK_LENGTH];

        input.read(IV); // IV block // throws IOException
        if ( cipher ) {
            output.write(IV); // throws IOException
        }

        int currentNumberOfBytesRead = input.read(currentBlock); // throws IOException
        int previousNumberOfBytesRead;

        while ( currentNumberOfBytesRead > 0 ) {

            // *****  *****
            previousNumberOfBytesRead = currentNumberOfBytesRead;
            previousBlock = Arrays.copy(currentBlock);

            currentNumberOfBytesRead = input.read(currentBlock); // throws IOException

            // Encode (IV + ctr)
            Arrays.copy(IV, 0, CTR.BLOCK_LENGTH, IVCtrEnc, 0);
            this.algorithm.cipher(IVCtrEnc);


            Arrays.arrayXOR(previousBlock, 0, IVCtrEnc, 0, previousNumberOfBytesRead);

            if ( currentNumberOfBytesRead < 0 ) {
                // previousBlock contains the input stream's last block
                if ( cipher ) {
                    // cipher
                    IVCtrEnc = this.padding.pad(Arrays.copy(IVCtrEnc, 0, previousNumberOfBytesRead));
                } else {
                    // invCipher
                    IVCtrEnc = this.padding.unPad(Arrays.copy(IVCtrEnc, 0, previousNumberOfBytesRead)); // throws InvalidPadException
                }
            }

            output.write(IVCtrEnc); // throws IOException

            // increment ctr
            incIV(IV);
        }
    }

    /**
     * Increment value coded in byte array (MSB=IV[0] and LSB=IV[IV.length-1]).
     *
     * @param IV
     */
    private static void incIV( byte[] IV ) {
        for ( int i=IV.length-1; i>-1; i-- ) {
            int val = IV[i];
            val++;
            val = val & 0xFF;
            IV[i] = (byte) val;

            if ( val != 0x00 ) {
                break;
            }
        }
    }
}
