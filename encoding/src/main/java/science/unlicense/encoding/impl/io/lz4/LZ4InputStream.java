
package science.unlicense.encoding.impl.io.lz4;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.EOSException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.WrapInputStream;

/**
 *
 * @author Johann Sorel
 */
public class LZ4InputStream extends WrapInputStream {

    private LZ4Frame frame = null;

    public LZ4InputStream(ByteInputStream in) {
        super(in);
    }

    @Override
    public int read() throws IOException {

        for (;;) {
            if (frame == null) {
                frame = new LZ4Frame();
                try {
                    frame.init(in);
                } catch( EOSException ex) {
                    frame = null;
                    return -1;
                }
            }

            final int b = frame.read();
            if (b != -1) {
                return b;
            } else {
                //frame is finished
                frame = null;
            }
        }
    }

}
