
package science.unlicense.encoding.impl.io.deflate;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.Numbers;
import science.unlicense.encoding.impl.io.huffman.HuffmanNode;
import science.unlicense.encoding.impl.io.huffman.HuffmanTree;

/**
 * All constants from RFC1951 deflate algorithm.
 *
 * @author Johann Sorel
 */
public final class HuffmanUtilities {

    /** Indicate an end of block */
    public static final int END_OF_BLOCK = 256;

    static final int[] DYNAMIC_KEY_LENGTH = new int[]{
        16, 17, 18, 0, 8, 7, 9, 6, 10, 5,
        11, 4, 12, 3, 13, 2, 14, 1, 15};

    static final int[] LENGTH_EXTRABITS = {
        0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1,
        2, 2, 2, 2,
        3, 3, 3, 3,
        4, 4, 4, 4,
        5, 5, 5, 5,
        0
    };
    static final int[] LENGTH_BASEVALUE = {
        3, 4, 5, 6, 7, 8, 9, 10,
        11, 13, 15, 17,
        19, 23, 27, 31,
        35, 43, 51, 59,
        67, 83, 99, 115,
        131, 163, 195, 227,
        258
    };

    static final int[] DISTANCE_EXTRABITS = {
        0, 0, 0, 0,
        1, 1,
        2, 2,
        3, 3,
        4, 4,
        5, 5,
        6, 6,
        7, 7,
        8, 8,
        9, 9,
        10, 10,
        11, 11,
        12, 12,
        13, 13
    };

    static final int[] DISTANCE_BASEVALUE = {
        1, 2, 3, 4,
        5, 7,
        9, 13,
        17, 25,
        33, 49,
        65, 97,
        129, 193,
        257, 385,
        513, 769,
        1025, 1537,
        2049, 3073,
        4097, 6145,
        8193, 12289,
        16385, 24577
    };

    private HuffmanUtilities(){}

    /**
     *
     * @param lengths : code lengths
     * @param maxsize : max number of bits per code
     * @return huffman tree
     */
    public static HuffmanTree buildHuffmanTree(int[] lengths, int maxsize) {

        //used in step 3
        int max_code = 0;

        // 1 - Count the number of codes for each code length.  Let
        //     bl_count[N] be the number of codes of length N, N >= 1.
        final int[] bl_count = new int[maxsize + 1];
        for (int n=0; n<lengths.length; n++) {
            final int length = lengths[n];
            if (length > 0) {
                max_code = n + 1;
                bl_count[length]++;
            } else if (length < 0 || length > maxsize) {
                throw new InvalidArgumentException("Length exceed maximum authorized length.");
            }
        }

        // 2 - Find the numerical value of the smallest code
        //     for each code length:
        //        code = 0;
        //        bl_count[0] = 0;
        //        for (bits = 1; bits <= MAX_BITS; bits++) {
        //            code = (code + bl_count[bits-1]) << 1;
        //            next_code[bits] = code;
        //        }
        final int[] next_code = new int[maxsize+1];
        int code = 0;
        bl_count[0] = 0;
        for (int bits=1; bits <= maxsize; bits++) {
            code = (code + bl_count[bits-1]) << 1;
            next_code[bits] = code;
        }

        // 3 - Assign numerical values to all codes, using consecutive
        //     values for all codes of the same length with the base
        //     values determined at step 2. Codes that are never used
        //     (which have a bit length of zero) must not be assigned a
        //     value.
        //
        //    for (n = 0;  n <= max_code; n++) {
        //        len = tree[n].Len;
        //        if (len != 0) {
        //            tree[n].Code = next_code[len];
        //            next_code[len]++;
        //        }
        //    }

        final HuffmanNode[] nodes = new HuffmanNode[max_code];
        for (int i=0;i<max_code;i++) {
            nodes[i] = new HuffmanNode(lengths[i],0,i);
        }

        for (int n=0; n<max_code; n++) {
            final int len = nodes[n].length;
            if (len != 0) {
                nodes[n].code = next_code[len];
                next_code[len]++;
            }
        }

        //reverse bits, our huffman tree expect bits in index order.
        for (int i=0;i<nodes.length;i++) {
            nodes[i].code = Numbers.reverseBits(nodes[i].code, nodes[i].length);
        }
        return new HuffmanTree(nodes);
    }

    public static HuffmanTree buildFixedHuffmanLiteralTree() {
        final int[] lengths = new int[288];
        Arrays.fill(lengths, 0, 144, 8);
        Arrays.fill(lengths, 144, 112, 9);
        Arrays.fill(lengths, 256, 24, 7);
        Arrays.fill(lengths, 280, 8, 8);
        return buildHuffmanTree(lengths, 9);
    }

    public static HuffmanTree buildFixedHuffmanDistanceTree() {
        final int[] lengths = new int[32];
        Arrays.fill(lengths, 0, 32, 5);
        return buildHuffmanTree(lengths, 5);
    }

}
