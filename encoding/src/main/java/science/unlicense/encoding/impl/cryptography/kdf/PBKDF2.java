

package science.unlicense.encoding.impl.cryptography.kdf;

import java.security.GeneralSecurityException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * TODO : replace JVM api used here by public domain ones.
 *
 * @author Thibaut Cuvelier
 */
public final class PBKDF2 {

    private PBKDF2() {}

    /**
     * PBKDF2 algorithm to generate keys from a password, based on SHA512. (RFC 2898.)
     *
     * @param password      The password from which the algorithm will derive one key; RFC 2898's P
     * @param salt          The salt for the generation; RFC 2898's S
     * @param iterations    The number of iterations to apply (the greater, the better security); RFC 2898's c
     * @param length        The wanted length for the key (number of 8-bit bytes); RFC 2898's dkLen
     * @param hmacAlgorithm The HMAC algorithm to use
     * @return The generated key
     * @throws java.security.GeneralSecurityException  The asked key length is too long.
     */
    public static byte[] pbkdf2(byte[] password, byte[] salt, int iterations, int length, String hmacAlgorithm)
            throws GeneralSecurityException {
        Mac md = Mac.getInstance(hmacAlgorithm);
        md.init(new SecretKeySpec(password, hmacAlgorithm));
        int hLen = md.getMacLength();
        byte[] out = new byte[length];

        // Step 1: check key length.
        if (length > (Math.pow(2, 32) - 1) * hLen) {
            throw new GeneralSecurityException("Requested key too long (maximum " + ((Math.pow(2, 32) - 1) * hLen * 8)
                    + " bits).");
        }

        // Step 2: number of blocks and rest.
        int nbBlocks = (int) Math.ceil((double) length / hLen); // RFC's l
        int rest = length - (nbBlocks - 1) * hLen; // RFC's r

        byte[] toSum  = new byte[hLen];
        byte[] summed = new byte[hLen];
        byte[] block  = new byte[salt.length + 4];

        System.arraycopy(salt, 0, block, 0, salt.length);

        // Step 3: for each block...
        for (int i = 1; i <= nbBlocks; i++) {
            block[salt.length]     = (byte) (i >> 24 & 0xff);
            block[salt.length + 1] = (byte) (i >> 16 & 0xff);
            block[salt.length + 2] = (byte) (i >> 8  & 0xff);
            block[salt.length + 3] = (byte) (i       & 0xff);

            md.update(block);
            md.doFinal(toSum, 0);
            System.arraycopy(toSum, 0, summed, 0, hLen);

            // Xor the consecutive applications of the hash function.
            for (int j = 1; j < iterations; j++) {
                md.update(toSum);
                md.doFinal(toSum, 0);

                for (int k = 0; k < hLen; k++) {
                    summed[k] ^= toSum[k];
                }
            }

            // Step 4: extract the first dkLen octets (done at each block instead of at once).
            System.arraycopy(summed, 0, out, (i - 1) * hLen, (i == nbBlocks ? rest : hLen));
        }

        // Step 5: output the derived key.
        return out;
    }

}
