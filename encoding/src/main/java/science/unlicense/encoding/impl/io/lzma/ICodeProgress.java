package science.unlicense.encoding.impl.io.lzma;

/**
 * From 7-ZIP SDK, placed under public domain.
 * http://www.7-zip.org/sdk.html
 *
 * @author Igor Pavlov
 */
public interface ICodeProgress {

    public void setProgress(long inSize, long outSize);

}
