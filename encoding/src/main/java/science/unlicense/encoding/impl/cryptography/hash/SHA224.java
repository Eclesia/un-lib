package science.unlicense.encoding.impl.cryptography.hash;

import science.unlicense.common.api.character.Chars;

/**
 * Implementation of SHA-224.
 *
 * @author Francois Berder
 *
 */
public class SHA224 extends SHA2_32 {

    public static final Chars NAME = Chars.constant("SHA-224");

    private static final int resetState[] = {
        0xc1059ed8, 0x367cd507, 0x3070dd17, 0xf70e5939,
        0xffc00b31, 0x68581511, 0x64f98fa7, 0xbefa4fa4
    };

    public SHA224() {
        super(resetState);
    }

    @Override
    public Chars getName() {
        return NAME;
    }

    @Override
    public int getResultLength() {
        return 224;
    }

}
