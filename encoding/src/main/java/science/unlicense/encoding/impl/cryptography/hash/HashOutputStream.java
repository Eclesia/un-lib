
package science.unlicense.encoding.impl.cryptography.hash;

import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.WrapOutputStream;

/**
 * Inputstream which calculate hash function while writing.
 *
 * @author Johann Sorel
 */
public class HashOutputStream extends WrapOutputStream{

    private final HashFunction hashFunction;

    public HashOutputStream(final HashFunction hashFunction, final ByteOutputStream out) {
        super(out);
        this.hashFunction = hashFunction;
    }

    public void hashReset() {
        hashFunction.reset();
    }

    public long hashValue() {
        return hashFunction.getResultLong();
    }

    @Override
    public void write(byte b) throws IOException {
        hashFunction.update(b);
        out.write(b);
    }

    @Override
    public void write(final byte[] buffer, final int offset, final int length) throws IOException {
        hashFunction.update(buffer,offset,length);
        out.write(buffer, offset, length);
    }

}
