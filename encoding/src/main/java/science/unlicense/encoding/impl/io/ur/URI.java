
package science.unlicense.encoding.impl.io.ur;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;

/**
 * Uniform Resource Identifier
 *
 * @author Johann Sorel
 */
public class URI extends CObject {

    protected final Chars schema;
    protected final Chars user;
    protected final Chars password;
    protected final Chars host;
    protected final Integer port;
    protected final Chars path;
    protected final Chars query;
    protected final Chars fragment;

    public URI(Chars schema, Chars path) {
        this(schema,null,null,null,null,path,null,null);
    }

    public URI(Chars schema, Chars user, Chars password, Chars host, Integer port, Chars path, Chars query, Chars fragment) {
        CObjects.ensureNotNull(schema);
        CObjects.ensureNotNull(path);
        this.schema = schema;
        if (user!=null || password!=null) {
            CObjects.ensureNotNull(user);
            CObjects.ensureNotNull(password);
        }
        this.user = user;
        this.password = password;
        this.host = host;
        this.port = port;
        this.path = path;
        this.query = query;
        this.fragment = fragment;
    }

    /**
     * Format URI.
     * scheme:[//[user:password@]host[:port]][/]path[?query][#fragment]
     *
     * @return
     */
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(schema);
        cb.append(':');
        if (user!=null) {
            cb.append(user);
            cb.append(':');
            cb.append(password);
            cb.append('@');
        }
        if (host!=null) {
            cb.append(host);
        }
        if (port!=null) {
            cb.append(':');
            cb.append(Int32.encode(port));
        }
        cb.append(path);
        if (query!=null) {
            cb.append('?');
            cb.append(query);
        }
        if (fragment!=null) {
            cb.append('#');
            cb.append(fragment);
        }
        return cb.toChars();
    }


}
