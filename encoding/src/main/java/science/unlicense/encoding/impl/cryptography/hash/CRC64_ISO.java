
package science.unlicense.encoding.impl.cryptography.hash;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;

/**
 * CRC64 checksum.
 *
 * CRC-64 : ISO 3309 version
 * https://en.wikipedia.org/wiki/Cyclic_redundancy_check
 *
 * @author Xavier Philippeau (http://xphilipp.developpez.com/)
 * @author Johann Sorel (UN project adaptation)
 */
public class CRC64_ISO implements HashFunction{

    public static final Chars NAME = Chars.constant("CRC-64");

    // the precomputed values for all possible byte values
    private static long[] CRC64Table = new long[256];

    // precompute the CRC-64 table for one byte
    static {
        // x64 + x4 + x3 + x1 + 1
        long POLY64 = 0x000000000000001BL;
        long POLY64Reverse = 0xd800000000000000L;

        long reminder;
        for (int i = 0; i < 256; i++) {
            reminder = i;
            for (int j = 0; j < 8; j++) {
                if ((reminder & 1) != 0) {
                    reminder = (reminder >>> 1) ^ POLY64Reverse;
                } else {
                    reminder = reminder >>> 1;
                }
            }
            CRC64Table[i] = reminder;
        }
    }

    private long crc = 0;

    public Chars getName() {
        return NAME;
    }

    public int getResultLength() {
        return 64;
    }

    public boolean isCryptographic() {
        return false;
    }

    public void reset() {
        crc = 0;
    }

    public void update(int b) {
        update(new byte[]{(byte) b},0,1);
    }

    public void update(byte[] buffer) {
        update(buffer, 0, buffer.length);
    }

    public void update(byte[] buffer, int offset, int length) {
        for (int i=offset,n=offset+length; i<n; i++) {
            int index = (buffer[i] ^ (int) crc) & 0xff;
            crc = CRC64Table[index] ^ (crc >>> 8);
        }
    }

    public byte[] getResultBytes() {
        final byte[] buffer = new byte[8];
        Endianness.BIG_ENDIAN.writeLong(getResultLong(), buffer, 0);
        return buffer;
    }

    public long getResultLong() {
        return crc;
    }

}
