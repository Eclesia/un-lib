
package science.unlicense.encoding.impl.io;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.ByteResource;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.io.SeekableByteBuffer;

/**
 *
 * @author Johann Sorel
 */
public class ClipByteResource implements ByteResource {

    private final ByteResource parent;
    private final long offset;
    private final long size;

    public ClipByteResource(ByteResource parent, long offset, long size) {
        this.parent = parent;
        this.offset = offset;
        this.size = size;
    }

    @Override
    public long getSize() {
        return size;
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        ByteInputStream is = parent.createInputStream();
        if (offset > 0) IOUtilities.skip(is, offset);
        if (size >= 0) is = new ClipInputStream(is, size);
        return is;
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
