package science.unlicense.encoding.impl.cryptography.ec;

/**
 *
 */
interface ECParam255 {

    int GF_M = 255;
    int GF_L = 15;
    char GF_K = 17;
    char GF_NZT = 0x0800;
    int GF_T = 3;
    int GF_RP = 0x0003;
    int GF_TM0 = 1;
    int GF_TM1 = 0x0001;
    int GF_TM2 = 0;
    char EC_B = 0x00a1;
    int EC_COF = 32460;
    int EC_H = 121;
}
