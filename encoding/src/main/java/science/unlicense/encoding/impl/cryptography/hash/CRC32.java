package science.unlicense.encoding.impl.cryptography.hash;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;

/**
 * CRC32 checksum.
 * Java adaptation of PNG annexe D : http://www.w3.org/TR/PNG/#D-CRCAppendix
 *
 * And
 *
 * From 7-ZIP SDK, placed under public domain.
 * http://www.7-zip.org/sdk.html
 *
 * @author Johann Sorel
 * @author Igor Pavlov
 */
public class CRC32 implements HashFunction {

    public static final Chars NAME = Chars.constant("CRC-32");

    /**
     * Fast CRC table. store checksum for all 8 bits combinaison.
     */
    private static final int[] FAST_TABLE;

    static {
        FAST_TABLE = new int[256];
        for (int n = 0; n < 256; n++) {
            int c = n;
            for (int k = 0; k < 8; k++) {
                if ((c & 1) != 0) {
                    c = 0xedb88320 ^ (c >>> 1);
                } else {
                    c = c >>> 1;
                }
            }
            FAST_TABLE[n] = c;
        }
    }

    //current checksum value
    protected int crc = -1;


    public Chars getName() {
        return NAME;
    }

    public int getResultLength() {
        return 32;
    }

    public boolean isCryptographic() {
        return false;
    }

    public void reset() {
        crc = -1;
    }

    public void update(int b) {
        crc = FAST_TABLE[(crc ^ b) & 0xFF] ^ (crc >>> 8);
    }

    public void update(final byte[] buffer) {
        int size = buffer.length;
        for (int i = 0; i < size; i++) {
            crc = FAST_TABLE[(crc ^ buffer[i]) & 0xFF] ^ (crc >>> 8);
        }
    }

    public void update(final byte[] buffer, int offset, int length) {
        for (int i = 0; i < length; i++) {
            crc = FAST_TABLE[(crc ^ buffer[offset + i]) & 0xFF] ^ (crc >>> 8);
        }
    }

    public byte[] getResultBytes() {
        final byte[] buffer = new byte[4];
        Endianness.BIG_ENDIAN.writeUInt(getResultLong(), buffer, 0);
        return buffer;
    }

    public long getResultLong() {
        return (crc ^ (-1)) & 0xFFFFFFFFL;
    }

    public int getDigest() {
        return crc ^ (-1);
    }
}
