package science.unlicense.encoding.impl.cryptography.algorithms;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.exception.InvalidArgumentException;

/**
 * ADVANCED ENCRYPTION STANDARD (AES)
 * AES-128, AES-192 and AES-256.
 *
 * http://csrc.nist.gov/publications/fips/fips197/fips-197.pdf
 *
 * @author Bertrand COTE
 */
public class AES implements CipherAlgorithm {

    /**
     * Non-linear substitution table used in several byte substitution
     * transformations and in the Key Expansion routine to perform a one-for-one
     * substitution of a byte value.
     */
    private static final byte[] S_BOX;
    /**
     * The S_BOX inverse substitution.
     */
    private static final byte[] INV_SBOX;

    /**
     * The round constant word array.
     */
    private static final byte[][] Rcon;
    /**
     * Length of words in bytes.
     */
    private static final int WORDS_LENGHT;
    /**
     * Number of columns (32-bit words) comprising the State. For this standard,
     * Nb = 4. (Also see Sec. 6.3.)
     */
    private static final int Nb;
    /**
     * Blocks length in bytes.
     */
    private static final int BLOCK_LENGTH;

    static {

        S_BOX = new byte[] {
            //      0           1           2           3           4           5           6           7           8           9           A           B           C           D           E           F
            (byte) 0x63, (byte) 0x7C, (byte) 0x77, (byte) 0x7B, (byte) 0xF2, (byte) 0x6B, (byte) 0x6F, (byte) 0xC5, (byte) 0x30, (byte) 0x01, (byte) 0x67, (byte) 0x2B, (byte) 0xFE, (byte) 0xD7, (byte) 0xAB, (byte) 0x76, // 0
            (byte) 0xCA, (byte) 0x82, (byte) 0xC9, (byte) 0x7D, (byte) 0xFA, (byte) 0x59, (byte) 0x47, (byte) 0xF0, (byte) 0xAD, (byte) 0xD4, (byte) 0xA2, (byte) 0xAF, (byte) 0x9C, (byte) 0xA4, (byte) 0x72, (byte) 0xC0, // 1
            (byte) 0xB7, (byte) 0xFD, (byte) 0x93, (byte) 0x26, (byte) 0x36, (byte) 0x3F, (byte) 0xF7, (byte) 0xCC, (byte) 0x34, (byte) 0xA5, (byte) 0xE5, (byte) 0xF1, (byte) 0x71, (byte) 0xD8, (byte) 0x31, (byte) 0x15, // 2
            (byte) 0x04, (byte) 0xC7, (byte) 0x23, (byte) 0xC3, (byte) 0x18, (byte) 0x96, (byte) 0x05, (byte) 0x9A, (byte) 0x07, (byte) 0x12, (byte) 0x80, (byte) 0xE2, (byte) 0xEB, (byte) 0x27, (byte) 0xB2, (byte) 0x75, // 3
            (byte) 0x09, (byte) 0x83, (byte) 0x2C, (byte) 0x1A, (byte) 0x1B, (byte) 0x6E, (byte) 0x5A, (byte) 0xA0, (byte) 0x52, (byte) 0x3B, (byte) 0xD6, (byte) 0xB3, (byte) 0x29, (byte) 0xE3, (byte) 0x2F, (byte) 0x84, // 4
            (byte) 0x53, (byte) 0xD1, (byte) 0x00, (byte) 0xED, (byte) 0x20, (byte) 0xFC, (byte) 0xB1, (byte) 0x5B, (byte) 0x6A, (byte) 0xCB, (byte) 0xBE, (byte) 0x39, (byte) 0x4A, (byte) 0x4C, (byte) 0x58, (byte) 0xCF, // 5
            (byte) 0xD0, (byte) 0xEF, (byte) 0xAA, (byte) 0xFB, (byte) 0x43, (byte) 0x4D, (byte) 0x33, (byte) 0x85, (byte) 0x45, (byte) 0xF9, (byte) 0x02, (byte) 0x7F, (byte) 0x50, (byte) 0x3C, (byte) 0x9F, (byte) 0xA8, // 6
            (byte) 0x51, (byte) 0xA3, (byte) 0x40, (byte) 0x8F, (byte) 0x92, (byte) 0x9D, (byte) 0x38, (byte) 0xF5, (byte) 0xBC, (byte) 0xB6, (byte) 0xDA, (byte) 0x21, (byte) 0x10, (byte) 0xFF, (byte) 0xF3, (byte) 0xD2, // 7
            (byte) 0xCD, (byte) 0x0C, (byte) 0x13, (byte) 0xEC, (byte) 0x5F, (byte) 0x97, (byte) 0x44, (byte) 0x17, (byte) 0xC4, (byte) 0xA7, (byte) 0x7E, (byte) 0x3D, (byte) 0x64, (byte) 0x5D, (byte) 0x19, (byte) 0x73, // 8
            (byte) 0x60, (byte) 0x81, (byte) 0x4F, (byte) 0xDC, (byte) 0x22, (byte) 0x2A, (byte) 0x90, (byte) 0x88, (byte) 0x46, (byte) 0xEE, (byte) 0xB8, (byte) 0x14, (byte) 0xDE, (byte) 0x5E, (byte) 0x0B, (byte) 0xDB, // 9
            (byte) 0xE0, (byte) 0x32, (byte) 0x3A, (byte) 0x0A, (byte) 0x49, (byte) 0x06, (byte) 0x24, (byte) 0x5C, (byte) 0xC2, (byte) 0xD3, (byte) 0xAC, (byte) 0x62, (byte) 0x91, (byte) 0x95, (byte) 0xE4, (byte) 0x79, // A
            (byte) 0xE7, (byte) 0xC8, (byte) 0x37, (byte) 0x6D, (byte) 0x8D, (byte) 0xD5, (byte) 0x4E, (byte) 0xA9, (byte) 0x6C, (byte) 0x56, (byte) 0xF4, (byte) 0xEA, (byte) 0x65, (byte) 0x7A, (byte) 0xAE, (byte) 0x08, // B
            (byte) 0xBA, (byte) 0x78, (byte) 0x25, (byte) 0x2E, (byte) 0x1C, (byte) 0xA6, (byte) 0xB4, (byte) 0xC6, (byte) 0xE8, (byte) 0xDD, (byte) 0x74, (byte) 0x1F, (byte) 0x4B, (byte) 0xBD, (byte) 0x8B, (byte) 0x8A, // C
            (byte) 0x70, (byte) 0x3E, (byte) 0xB5, (byte) 0x66, (byte) 0x48, (byte) 0x03, (byte) 0xF6, (byte) 0x0E, (byte) 0x61, (byte) 0x35, (byte) 0x57, (byte) 0xB9, (byte) 0x86, (byte) 0xC1, (byte) 0x1D, (byte) 0x9E, // D
            (byte) 0xE1, (byte) 0xF8, (byte) 0x98, (byte) 0x11, (byte) 0x69, (byte) 0xD9, (byte) 0x8E, (byte) 0x94, (byte) 0x9B, (byte) 0x1E, (byte) 0x87, (byte) 0xE9, (byte) 0xCE, (byte) 0x55, (byte) 0x28, (byte) 0xDF, // E
            (byte) 0x8C, (byte) 0xA1, (byte) 0x89, (byte) 0x0D, (byte) 0xBF, (byte) 0xE6, (byte) 0x42, (byte) 0x68, (byte) 0x41, (byte) 0x99, (byte) 0x2D, (byte) 0x0F, (byte) 0xB0, (byte) 0x54, (byte) 0xBB, (byte) 0x16, // F
        };

        INV_SBOX = new byte[] {
            //      0           1           2           3           4           5           6           7           8           9           A           B           C           D           E           F
            (byte) 0x52, (byte) 0x09, (byte) 0x6A, (byte) 0xD5, (byte) 0x30, (byte) 0x36, (byte) 0xA5, (byte) 0x38, (byte) 0xBF, (byte) 0x40, (byte) 0xA3, (byte) 0x9E, (byte) 0x81, (byte) 0xF3, (byte) 0xD7, (byte) 0xFB, // 0
            (byte) 0x7C, (byte) 0xE3, (byte) 0x39, (byte) 0x82, (byte) 0x9B, (byte) 0x2F, (byte) 0xFF, (byte) 0x87, (byte) 0x34, (byte) 0x8E, (byte) 0x43, (byte) 0x44, (byte) 0xC4, (byte) 0xDE, (byte) 0xE9, (byte) 0xCB, // 1
            (byte) 0x54, (byte) 0x7B, (byte) 0x94, (byte) 0x32, (byte) 0xA6, (byte) 0xC2, (byte) 0x23, (byte) 0x3D, (byte) 0xEE, (byte) 0x4C, (byte) 0x95, (byte) 0x0B, (byte) 0x42, (byte) 0xFA, (byte) 0xC3, (byte) 0x4E, // 2
            (byte) 0x08, (byte) 0x2E, (byte) 0xA1, (byte) 0x66, (byte) 0x28, (byte) 0xD9, (byte) 0x24, (byte) 0xB2, (byte) 0x76, (byte) 0x5B, (byte) 0xA2, (byte) 0x49, (byte) 0x6D, (byte) 0x8B, (byte) 0xD1, (byte) 0x25, // 3
            (byte) 0x72, (byte) 0xF8, (byte) 0xF6, (byte) 0x64, (byte) 0x86, (byte) 0x68, (byte) 0x98, (byte) 0x16, (byte) 0xD4, (byte) 0xA4, (byte) 0x5C, (byte) 0xCC, (byte) 0x5D, (byte) 0x65, (byte) 0xB6, (byte) 0x92, // 4
            (byte) 0x6C, (byte) 0x70, (byte) 0x48, (byte) 0x50, (byte) 0xFD, (byte) 0xED, (byte) 0xB9, (byte) 0xDA, (byte) 0x5E, (byte) 0x15, (byte) 0x46, (byte) 0x57, (byte) 0xA7, (byte) 0x8D, (byte) 0x9D, (byte) 0x84, // 5
            (byte) 0x90, (byte) 0xD8, (byte) 0xAB, (byte) 0x00, (byte) 0x8C, (byte) 0xBC, (byte) 0xD3, (byte) 0x0A, (byte) 0xF7, (byte) 0xE4, (byte) 0x58, (byte) 0x05, (byte) 0xB8, (byte) 0xB3, (byte) 0x45, (byte) 0x06, // 6
            (byte) 0xD0, (byte) 0x2C, (byte) 0x1E, (byte) 0x8F, (byte) 0xCA, (byte) 0x3F, (byte) 0x0F, (byte) 0x02, (byte) 0xC1, (byte) 0xAF, (byte) 0xBD, (byte) 0x03, (byte) 0x01, (byte) 0x13, (byte) 0x8A, (byte) 0x6B, // 7
            (byte) 0x3A, (byte) 0x91, (byte) 0x11, (byte) 0x41, (byte) 0x4F, (byte) 0x67, (byte) 0xDC, (byte) 0xEA, (byte) 0x97, (byte) 0xF2, (byte) 0xCF, (byte) 0xCE, (byte) 0xF0, (byte) 0xB4, (byte) 0xE6, (byte) 0x73, // 8
            (byte) 0x96, (byte) 0xAC, (byte) 0x74, (byte) 0x22, (byte) 0xE7, (byte) 0xAD, (byte) 0x35, (byte) 0x85, (byte) 0xE2, (byte) 0xF9, (byte) 0x37, (byte) 0xE8, (byte) 0x1C, (byte) 0x75, (byte) 0xDF, (byte) 0x6E, // 9
            (byte) 0x47, (byte) 0xF1, (byte) 0x1A, (byte) 0x71, (byte) 0x1D, (byte) 0x29, (byte) 0xC5, (byte) 0x89, (byte) 0x6F, (byte) 0xB7, (byte) 0x62, (byte) 0x0E, (byte) 0xAA, (byte) 0x18, (byte) 0xBE, (byte) 0x1B, // A
            (byte) 0xFC, (byte) 0x56, (byte) 0x3E, (byte) 0x4B, (byte) 0xC6, (byte) 0xD2, (byte) 0x79, (byte) 0x20, (byte) 0x9A, (byte) 0xDB, (byte) 0xC0, (byte) 0xFE, (byte) 0x78, (byte) 0xCD, (byte) 0x5A, (byte) 0xF4, // B
            (byte) 0x1F, (byte) 0xDD, (byte) 0xA8, (byte) 0x33, (byte) 0x88, (byte) 0x07, (byte) 0xC7, (byte) 0x31, (byte) 0xB1, (byte) 0x12, (byte) 0x10, (byte) 0x59, (byte) 0x27, (byte) 0x80, (byte) 0xEC, (byte) 0x5F, // C
            (byte) 0x60, (byte) 0x51, (byte) 0x7F, (byte) 0xA9, (byte) 0x19, (byte) 0xB5, (byte) 0x4A, (byte) 0x0D, (byte) 0x2D, (byte) 0xE5, (byte) 0x7A, (byte) 0x9F, (byte) 0x93, (byte) 0xC9, (byte) 0x9C, (byte) 0xEF, // D
            (byte) 0xA0, (byte) 0xE0, (byte) 0x3B, (byte) 0x4D, (byte) 0xAE, (byte) 0x2A, (byte) 0xF5, (byte) 0xB0, (byte) 0xC8, (byte) 0xEB, (byte) 0xBB, (byte) 0x3C, (byte) 0x83, (byte) 0x53, (byte) 0x99, (byte) 0x61, // E
            (byte) 0x17, (byte) 0x2B, (byte) 0x04, (byte) 0x7E, (byte) 0xBA, (byte) 0x77, (byte) 0xD6, (byte) 0x26, (byte) 0xE1, (byte) 0x69, (byte) 0x14, (byte) 0x63, (byte) 0x55, (byte) 0x21, (byte) 0x0C, (byte) 0x7D, // F
        };

        Rcon = new byte[][]{
            {(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,}, // Rcon[0] is unused
            {(byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00,},
            {(byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x00,},
            {(byte) 0x04, (byte) 0x00, (byte) 0x00, (byte) 0x00,},
            {(byte) 0x08, (byte) 0x00, (byte) 0x00, (byte) 0x00,},
            {(byte) 0x10, (byte) 0x00, (byte) 0x00, (byte) 0x00,},
            {(byte) 0x20, (byte) 0x00, (byte) 0x00, (byte) 0x00,},
            {(byte) 0x40, (byte) 0x00, (byte) 0x00, (byte) 0x00,},
            {(byte) 0x80, (byte) 0x00, (byte) 0x00, (byte) 0x00,},
            {(byte) 0x1B, (byte) 0x00, (byte) 0x00, (byte) 0x00,},   // 0x1B = 0x100 mod 0x11B
            {(byte) 0x36, (byte) 0x00, (byte) 0x00, (byte) 0x00,},}; // 0x36 = 0x200 mod 0x11B

        Nb = 4;

        WORDS_LENGHT = 4;
        BLOCK_LENGTH = 16;
    }

    /**
     * The ciphern key.
     * Must be 128, 192 or 256 bits long (16, 24 or 32 bytes long).
     */
    private byte[] key;
    /**
     * Intermediate Cipher result that can be pictured as a rectangular array of
     * bytes, having four rows and Nb columns.
     */
    private byte[] state;
    /**
     * key expansion array
     */
    private byte[] w;
    /**
     * Number of 32-bit words comprising the Cipher Key. For this standard,
     * Nk = 4, 6, or 8. (Also see Sec. 6.3.)
     */
    private int Nk;
    /**
     * Number of rounds, which is a function of Nk and Nb (which is fixed). For
     * this standard, Nr = 10, 12, or 14. (Also see Sec. 6.3.)
     */
    private int Nr;

    public AES(byte[] key) {

        int tempoNk = key.length / 4;
        if (tempoNk != 4 && tempoNk != 6 && tempoNk != 8) {
            throw new InvalidArgumentException("Illegal key length (Must be 16, 24, or 32 bytes long).");
        }

        this.key = key;

        this.Nk = tempoNk;

        if (this.Nk == 4) {
            this.Nr = 10;
        } else if (this.Nk == 6) {
            this.Nr = 12;
        } else { // tempoNk == 8 here
            this.Nr = 14;
        }

        this.state = new byte[16];

        this.w = new byte[AES.Nb * (this.Nr + 1) * AES.WORDS_LENGHT];
        this.keyExpansion();
    }

    // ========== CipherAlgorithm interface ====================================

    @Override
    public byte[] cipher(byte[] input) {
        if (input.length != AES.BLOCK_LENGTH) {
            throw new InvalidArgumentException("Bad input length (must be " + AES.BLOCK_LENGTH + " bytes long).");
        }
        this.state = input;

        // ***** cipher processing: *****
        // round 0
        this.addRoundKey(0);
        // rounds 1 to Nr-1
        for (int round = 1; round < this.Nr; round++) {
            this.subBytes(AES.S_BOX);
            this.shiftRows();
            this.mixColumns();
            this.addRoundKey(round);
        }
        // round Nr
        this.subBytes(AES.S_BOX);
        this.shiftRows();
        this.addRoundKey(this.Nr);

        return this.state;
    }

    @Override
    public byte[] invCipher(byte[] input) {
        if (input.length != AES.BLOCK_LENGTH) {
            throw new InvalidArgumentException("Bad input length (must be " + AES.BLOCK_LENGTH + " bytes long).");
        }
        this.state = input;

        // ***** inverse cipher processing: *****
        // round Nr
        this.addRoundKey(this.Nr);
        this.invShiftRows();
        this.subBytes(AES.INV_SBOX);
        // round Nr-1 to 1
        for (int round = this.Nr - 1; round > 0; round--) {
            this.addRoundKey(round);
            this.invMixColumns();
            this.invShiftRows();
            this.subBytes(AES.INV_SBOX);
        }
        // round 0
        this.addRoundKey(0);

        return this.state;
    }

    // =========================================================================

    /**
     * Transformation in the Cipher that processes the State using a non­linear
     * byte substitution table (S-box) that operates on each of the State bytes
     * independently.
     *
     * @param subBox use S_BOX for normal substitution and INV_SBOX for inverse
     * substitution.
     */
    private void subBytes(byte[] subBox) {
        subArray(this.state, subBox);
    }

    /**
     * see 5.2 Key expansion: takes a word [a0,a1,a2,a3] as input, performs a
     * cyclic permutation, and returns the word [a1,a2,a3,a0].
     *
     * @param byteArray
     */
    private void rotArray(byte[] byteArray) {
        byte byteArray_0 = byteArray[0];
        Arrays.copy(byteArray, 1, byteArray.length - 1, byteArray, 0);
        byteArray[byteArray.length - 1] = byteArray_0;
    }

    /**
     * Transformation in the Cipher that processes the State by cyclically
     * shifting the last three rows of the State by different offsets.
     */
    private void shiftRows() {
        byte tempo;
        // row 1
        tempo = this.state[1];
        this.state[1] = this.state[5];
        this.state[5] = this.state[9];
        this.state[9] = this.state[13];
        this.state[13] = tempo;
        // row 2
        tempo = this.state[10];
        this.state[10] = this.state[2];
        this.state[2] = tempo;
        tempo = this.state[14];
        this.state[14] = this.state[6];
        this.state[6] = tempo;
        // row 3
        tempo = this.state[11];
        this.state[11] = this.state[7];
        this.state[7] = this.state[3];
        this.state[3] = this.state[15];
        this.state[15] = tempo;
    }

    private void invShiftRows() {
        byte tempo;
        // row 1
        tempo = this.state[13];
        this.state[13] = this.state[9];
        this.state[9] = this.state[5];
        this.state[5] = this.state[1];
        this.state[1] = tempo;
        // row 2
        tempo = this.state[10];
        this.state[10] = this.state[2];
        this.state[2] = tempo;
        tempo = this.state[14];
        this.state[14] = this.state[6];
        this.state[6] = tempo;
        // row 3
        tempo = this.state[15];
        this.state[15] = this.state[3];
        this.state[3] = this.state[7];
        this.state[7] = this.state[11];
        this.state[11] = tempo;
    }

    /**
     * Transformation in the Cipher that takes all of the columns of the State
     * and mixes their data (independently of one another) to produce new
     * columns.
     */
    private void mixColumns() {
        for (int i = 0; i < 16; i += 4) {
            int s0c = this.state[i] & 0xFF;
            int s1c = this.state[i + 1] & 0xFF;
            int s2c = this.state[i + 2] & 0xFF;
            int s3c = this.state[i + 3] & 0xFF;
            // s 0,c = ({02} • s0,c ) XOR ({03} • s1,c ) XOR s2,c XOR s3,c
            this.state[i] = (byte) (
                      modulo_0x11B((s0c << 1)) // {02} • s0,c
                    ^ modulo_0x11B(((s1c << 1) ^ s1c)) // {03} • s1,c
                    ^ s2c
                    ^ s3c);
            // s1,c = s0,c XOR ({02} • s1,c ) XOR ({03} • s 2,c ) XOR s3,c
            this.state[i + 1] = (byte) (
                      s0c
                    ^ modulo_0x11B((s1c << 1))
                    ^ modulo_0x11B(((s2c << 1) ^ s2c))
                    ^ s3c);
            // s 2,c = s0,c XOR s1,c XOR ({02} • s 2,c ) XOR ({03} • s3,c )
            this.state[i + 2] = (byte) (
                      s0c
                    ^ s1c
                    ^ modulo_0x11B((s2c << 1))
                    ^ modulo_0x11B(((s3c << 1) ^ s3c)));
            // s3,c = ({03} • s0,c ) XOR s1,c XOR s 2,c XOR ({02} • s3,c )
            this.state[i + 3] = (byte) (
                      modulo_0x11B(((s0c << 1) ^ s0c))
                    ^ s1c
                    ^ s2c
                    ^ modulo_0x11B((s3c << 1)));
        }
    }

    private void invMixColumns() {
        for (int i = 0; i < 16; i += 4) {
            int s0c = this.state[i] & 0xFF;
            int s1c = this.state[i + 1] & 0xFF;
            int s2c = this.state[i + 2] & 0xFF;
            int s3c = this.state[i + 3] & 0xFF;
            // s0,c = ({0e} • s0,c ) XOR ({0b} • s1,c ) XOR ({0d} • s 2,c ) XOR ({09} • s3,c )
            this.state[i] = (byte) (
                      modulo_0x11B((s0c << 3) ^ (s0c << 2) ^ (s0c << 1)) // 0x0e = 0000 1110
                    ^ modulo_0x11B((s1c << 3) ^ (s1c << 1) ^ s1c) // 0x0b = 0000 1011
                    ^ modulo_0x11B((s2c << 3) ^ (s2c << 2) ^ s2c) // 0x0d = 0000 1101
                    ^ modulo_0x11B((s3c << 3) ^ s3c) // 0x09 = 0000 1001
                    );
            // s1,c = ({09} • s0,c ) XOR ({0e} • s1,c ) XOR ({0b} • s 2,c ) XOR ({0d} • s3,c )
            this.state[i + 1] = (byte) (
                      modulo_0x11B((s0c << 3) ^ s0c) // 0x09 = 0000 1001
                    ^ modulo_0x11B((s1c << 3) ^ (s1c << 2) ^ (s1c << 1)) // 0x0e = 0000 1110
                    ^ modulo_0x11B((s2c << 3) ^ (s2c << 1) ^ s2c) // 0x0b = 0000 1011
                    ^ modulo_0x11B((s3c << 3) ^ (s3c << 2) ^ s3c) // 0x0d = 0000 1101
                    );
            // s2,c = ({0d} • s0,c ) XOR ({09} • s1,c ) XOR ({0e} • s 2,c ) XOR ({0b} • s3,c )
            this.state[i + 2] = (byte) (
                      modulo_0x11B((s0c << 3) ^ (s0c << 2) ^ s0c) // 0x0d = 0000 1101
                    ^ modulo_0x11B((s1c << 3) ^ s1c) // 0x09 = 0000 1001
                    ^ modulo_0x11B((s2c << 3) ^ (s2c << 2) ^ (s2c << 1)) // 0x0e = 0000 1110
                    ^ modulo_0x11B((s3c << 3) ^ (s3c << 1) ^ s3c) // 0x0b = 0000 1011
                    );
            // s3,c = ({0b} • s0,c ) XOR ({0d} • s1,c ) XOR ({09} • s 2,c ) XOR ({0e} • s3,c )
            this.state[i + 3] = (byte) (
                      modulo_0x11B((s0c << 3) ^ (s0c << 1) ^ s0c) // 0x0b = 0000 1011
                    ^ modulo_0x11B((s1c << 3) ^ (s1c << 2) ^ s1c) // 0x0d = 0000 1101
                    ^ modulo_0x11B((s2c << 3) ^ s2c) // 0x09 = 0000 1001
                    ^ modulo_0x11B((s3c << 3) ^ (s3c << 2) ^ (s3c << 1)) // 0x0e = 0000 1110
                    );
        }
    }

    /**
     * Transformation in the Cipher and Inverse Cipher in which a Round Key is
     * added to the State using an XOR operation. The length of a Round Key
     * equals the size of the State (i.e., for Nb = 4, the Round Key length
     * equals 128 bits/16 bytes).
     *
     * @param round
     */
    private void addRoundKey(int round) {
        Arrays.arrayXOR(
                this.w,
                AES.Nb * AES.WORDS_LENGHT * round,
                this.state,
                0,
                this.state.length);
    }

    private void keyExpansion() {

        // Copy the key values at the biginning of w
        Arrays.copy(this.key, 0, this.key.length, this.w, 0);

        // Computes expansion values
        byte[] temp = new byte[AES.WORDS_LENGHT];
        for (int i = this.Nk; i < AES.Nb * (this.Nr + 1); i++) {

            // copy previous word in w in temp
            Arrays.copy(this.w, AES.Nb * (i - 1), AES.WORDS_LENGHT, temp, 0);

            if (i % this.Nk == 0) {
                rotArray(temp);
                subArray(temp, AES.S_BOX);
                Arrays.arrayXOR(AES.Rcon[i / this.Nk], 0, temp, 0, AES.WORDS_LENGHT);
            } else if ((this.Nk > 6) && (i % this.Nk == 4)) {
                subArray(temp, AES.S_BOX);
            }

            Arrays.arrayXOR(this.w, (i - this.Nk) * AES.WORDS_LENGHT, temp, 0, AES.WORDS_LENGHT);
            Arrays.copy(temp, 0, AES.WORDS_LENGHT, this.w, i * AES.Nb);
        }
    }

    // =========================================================================

    /**
     * Computes p modulo 0x11B using XOR instead of MINUS.
     *
     * @param p
     * @return
     */
    private static int modulo_0x11B(int p) {
        final int msbInM = 0x100; // most significant bit of m=0x11B

        if (!(p < msbInM)) {
            int deltaDegree = 0;
            // most significant bit of p, and degree of p
            int msbInP = 0x100;
            int temp = p;
            while (temp >= 0x100) {
                deltaDegree++;
                temp = temp >> 1;
                msbInP = msbInP << 1;
            }

            // computing modulo
            int denominator = 0x11B << deltaDegree;
            while (p >= msbInM) {
                denominator = denominator >> 1;
                msbInP = msbInP >> 1;
                if ((p ^ msbInP) < p) {
                    p = p ^ denominator;
                }
            }
        }
        return p & 0xFF;
    }

    private void subArray(byte[] byteArray, byte[] subBox) {
        for (int i = 0; i < byteArray.length; i++) {
            byteArray[i] = subBox[byteArray[i] & 0xFF];
        }
    }

}
