
package science.unlicense.encoding.impl.io.x364;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;

/**
 * Terminal/Console colors knowned as X3.64 - ECMA-48 - ISO/IEC 6429.
 * Write the following byte sequence in the output stream.
 *
 * @author Johann Sorel
 */
public final class X364 {

    public static final Chars MODE_RESET                 = mode( 0);
    public static final Chars MODE_BOLD                  = mode( 1);
    public static final Chars MODE_FAINT                 = mode( 2);
    public static final Chars MODE_ITALIC                = mode( 3);
    public static final Chars MODE_UNDERSCORE            = mode( 4);
    public static final Chars MODE_BLINK                 = mode( 5);
    public static final Chars MODE_BLINK_FAST            = mode( 6);
    public static final Chars MODE_REVERSE               = mode( 7);
    public static final Chars MODE_HIDDEN                = mode( 8);
    public static final Chars MODE_CROSSOUT              = mode( 9);
    public static final Chars MODE_PRIME_FONT            = mode(10);
    public static final Chars MODE_FRAKTUR               = mode(20);
    public static final Chars MODE_DOUBLEUNDERLINE       = mode(21);
    public static final Chars MODE_FAINT2                = mode(22);
    public static final Chars MODE_NO_ITALIC             = mode(23);
    public static final Chars MODE_NO_UNDERLINE          = mode(24);
    public static final Chars MODE_NO_BLINK              = mode(25);
    public static final Chars MODE_POSITIVE              = mode(27);
    public static final Chars MODE_REVEAL                = mode(28);
    public static final Chars MODE_NO_CROSSOUT           = mode(29);
    public static final Chars MODE_FRAMED                = mode(51);
    public static final Chars MODE_ENCIRCLED             = mode(52);
    public static final Chars MODE_OVERLINED             = mode(53);
    public static final Chars MODE_NOt_FRAMED_ORENCIRCLED= mode(54);
    public static final Chars MODE_NOT_OVERLINED         = mode(55);
    public static final Chars MODE_IDEO_UNDERLINE        = mode(60);
    public static final Chars MODE_IDEO_DOUBLEUNDERLINE  = mode(61);
    public static final Chars MODE_IDEO_OVERLINE         = mode(62);
    public static final Chars MODE_IDEO_DOUBLEOVERLINE   = mode(63);
    public static final Chars MODE_IDEO_STRESS           = mode(64);
    public static final Chars MODE_IDEO_ATT_OFF          = mode(65);


    public static final Chars FOREGROUND_BLACK    = foreground((byte) 0);
    public static final Chars FOREGROUND_RED      = foreground((byte) 1);
    public static final Chars FOREGROUND_GREEN    = foreground((byte) 2);
    public static final Chars FOREGROUND_YELLOW   = foreground((byte) 3);
    public static final Chars FOREGROUND_BLUE     = foreground((byte) 4);
    public static final Chars FOREGROUND_MAGENTA  = foreground((byte) 5);
    public static final Chars FOREGROUND_CYAN     = foreground((byte) 6);
    public static final Chars FOREGROUND_WHITE    = foreground((byte) 7);
    public static final Chars FOREGROUND_DEFAULT  = foreground((byte) 8);

    public static final Chars BACKGROUND_BLACK    = background((byte) 0);
    public static final Chars BACKGROUND_RED      = background((byte) 1);
    public static final Chars BACKGROUND_GREEN    = background((byte) 2);
    public static final Chars BACKGROUND_YELLOW   = background((byte) 3);
    public static final Chars BACKGROUND_BLUE     = background((byte) 4);
    public static final Chars BACKGROUND_MAGENTA  = background((byte) 5);
    public static final Chars BACKGROUND_CYAN     = background((byte) 6);
    public static final Chars BACKGROUND_WHITE    = background((byte) 7);
    public static final Chars BACKGROUND_DEFAULT  = background((byte) 8);

    public static final byte COLOR_BLACK    = 0;
    public static final byte COLOR_RED      = 1;
    public static final byte COLOR_GREEN    = 2;
    public static final byte COLOR_YELLOW   = 3;
    public static final byte COLOR_BLUE     = 4;
    public static final byte COLOR_MAGENTA  = 5;
    public static final byte COLOR_CYAN     = 6;
    public static final byte COLOR_WHITE    = 7;
    public static final byte COLOR_DEFAULT  = 8;

    /**
     * Erase line
     */
    public static final Chars ERASE_LINE = toCode((byte) '2', (byte) 'K');

    /**
     * Clear screen
     */
    public static final Chars CLEAR_SCREEN = toCode((byte) '2', (byte) 'J');


    private X364() {}

    /**
     * font mode.
     */
    public static Chars mode(int mode) {
        return toCode(Int32.encode(mode).toBytes(), (byte) 'm');
    }

    /**
     * Background color.
     */
    public static Chars foreground(int color) {
        return toCode(Int32.encode(30 + color).toBytes(), (byte) 'm');
    }

    /**
     * Background color.
     */
    public static Chars background(int color) {
        return toCode(Int32.encode(40 + color).toBytes(), (byte) 'm');
    }

    /**
     * Cursor up
     */
    public static Chars cursorUp(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'A');
    }

    /**
     * Cursor down
     */
    public static Chars cursorDown(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'B');
    }

    /**
     * Cursor forward
     */
    public static Chars cursorForward(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'C');
    }

    /**
     * Cursor backward
     */
    public static Chars cursorBackward(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'D');
    }

    /**
     * Cursor next line
     */
    public static Chars cursorNextLine(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'E');
    }

    /**
     * Cursor previous line
     */
    public static Chars cursorPreviousLine(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'F');
    }

    /**
     * Cursor horizontal absolute.
     * Move cursor to column
     */
    public static Chars cursorHAbsolute(int col) {
        return toCode(Int32.encode(col).toBytes(), (byte) 'G');
    }

    /**
     * Cursor position
     */
    public static Chars cursorPosition(int row, int col) {
        return toCode(Int32.encode(row).toBytes(), Int32.encode(col).toBytes(), (byte) 'H');
    }

    private static Chars toCode(byte b) {
        return new Chars(new byte[]{27,91, b},CharEncodings.US_ASCII);
    }

    private static Chars toCode(byte b1, byte b2) {
        return new Chars(new byte[]{27,91, b1, b2},CharEncodings.US_ASCII);
    }

    private static Chars toCode(byte b1, byte b2, byte b3) {
        return new Chars(new byte[]{27,91, b1, b2, b3},CharEncodings.US_ASCII);
    }

    private static Chars toCode(byte[] bs, byte end) {
        final byte[] table = new byte[3+bs.length];
        table[0] = 27;
        table[1] = 91;
        Arrays.copy(bs, 0, bs.length, table, 2);
        table[table.length-1] = end;
        return new Chars(table,CharEncodings.US_ASCII);
    }

    private static Chars toCode(byte[] bs1, byte[] bs2, byte end) {
        final byte[] table = new byte[4+bs1.length+bs2.length];
        table[0] = 27;
        table[1] = 91;
        Arrays.copy(bs1, 0, bs1.length, table, 2);
        table[1+bs1.length] = ';';
        Arrays.copy(bs2, 0, bs2.length, table, 2+bs1.length);
        table[table.length-1] = end;
        return new Chars(table,CharEncodings.US_ASCII);
    }


}
