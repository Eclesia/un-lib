
package science.unlicense.encoding.impl.cryptography.hash;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;

/**
 * Hash function doing nothing, can be used as a place holder when hashing is not
 * really needed.
 *
 * @author Johann Sorel
 */
public class None implements HashFunction {

    private static final Chars NAME = Chars.constant("None");

    @Override
    public Chars getName() {
        return NAME;
    }

    @Override
    public boolean isCryptographic() {
        return false;
    }

    @Override
    public int getResultLength() {
        return 0;
    }

    @Override
    public void reset() {
    }

    @Override
    public void update(int b) {
    }

    @Override
    public void update(byte[] buffer) {
    }

    @Override
    public void update(byte[] buffer, int offset, int length) {
    }

    @Override
    public byte[] getResultBytes() {
        return Arrays.ARRAY_BYTE_EMPTY;
    }

    @Override
    public long getResultLong() {
        return 0;
    }

}
