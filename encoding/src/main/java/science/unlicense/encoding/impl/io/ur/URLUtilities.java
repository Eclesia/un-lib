

package science.unlicense.encoding.impl.io.ur;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodingException;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.CharIterator;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;

/**
 *
 * @author Johann Sorel
 */
public final class URLUtilities {

    private URLUtilities() {}

    /**
     * Encode given path following url encoding.
     *
     * See :
     * http://en.wikipedia.org/wiki/Percent-encoding
     * http://www.w3schools.com/tags/ref_urlencode.asp
     *
     * TODO recheck this code, just a draft
     *
     * @param path
     * @return
     */
    public static Chars encode(Chars path) {
        final ByteSequence buffer = new ByteSequence();
        final CharIterator ite = path.createIterator();
        while (ite.hasNext()) {
            final int unicode = ite.nextToUnicode();

            if (unicode>=33 && unicode <=126) {
                //valid ascii char
                buffer.put((byte) unicode);
            } else {
                //encode it
                final byte[] utf8Char = CharEncodings.UTF_8.toBytes(unicode);
                for (int i=0;i<utf8Char.length;i++) {
                    final byte[] c = Int32.encodeHexa(utf8Char[i]&0xFF).toBytes();
                    buffer.put((byte) '%');
                    if (c.length==1) buffer.put((byte) '0');
                    buffer.put(c);
                }
            }
        }
        return new Chars(buffer.toArrayByte());
    }

    /**
     * Decode URL encoded string.
     *
     * TODO recheck this code, just a draft
     *
     * @param path
     * @return
     */
    public static Chars decode(Chars path) {
        final byte[] utf8Char = new byte[4];
        int idx = 0;

        final CharBuffer buffer = new CharBuffer(path.getEncoding());
        final CharIterator ite = path.createIterator();
        while (ite.hasNext()) {
            int unicode = ite.nextToUnicode();

            if (unicode=='%') {
                //encoded char
                idx=0;
                utf8Char[idx] = (byte) Int32.decodeHexa(ite,2);
                for (idx++;!CharEncodings.UTF_8.isComplete(utf8Char, 0, idx);idx++) {
                    unicode = ite.nextToUnicode();
                    if (unicode!='%') {
                        throw new CharEncodingException("Unvalid Percent encoding, expected a %");
                    }
                    utf8Char[idx] = (byte) Int32.decodeHexa(ite,2);
                }
                buffer.append(CharEncodings.UTF_8.toUnicode(utf8Char));
            } else {
                buffer.append(unicode);
            }
        }
        return buffer.toChars();
    }

}
