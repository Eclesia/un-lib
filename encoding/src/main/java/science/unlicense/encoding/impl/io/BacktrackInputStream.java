
package science.unlicense.encoding.impl.io;

import science.unlicense.encoding.api.io.WrapInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.math.api.Maths;

/**
 * ByteInputStream which allow to move forward and backward in the stream
 * by placing tags.
 *
 * @author Johann Sorel
 */
public class BacktrackInputStream extends WrapInputStream {

    private final byte[] temp = new byte[8196];
    private final ByteSequence buffer = new ByteSequence();
    private boolean buffering = false;
    private int position;


    public BacktrackInputStream(ByteInputStream in) {
        super(in);
    }

    /**
     * Placement a marker at current bytestream position.
     */
    public void mark(){
        if (position < buffer.getSize()) {
            // there are bytes remaining in the buffer
            buffer.trimStart(position);
            position = 0;
        } else {
            buffer.removeAll();
            position = 0;
        }
        buffering = true;
    }

    /**
     * Current offset position from the mark.
     * @return distance from mark
     * @throws InvalidArgumentException if stream has no mark set
     */
    public int position() {
        if (!buffering) {
            throw new InvalidArgumentException("BacktrackStream has no mark set.");
        }
        return position;
    }

    /**
     * Rewind back to last mark position.
     * @throws InvalidArgumentException if stream has no mark set
     */
    public void rewind() {
        if (!buffering) {
            throw new InvalidArgumentException("BacktrackStream has no mark set.");
        }
        position = 0;
    }

    /**
     * Remove the marker and stop buffering datas.
     * If some bytes are still in the buffer they will be returned first
     * before using the underlying stream.
     */
    public void clearMark() {
        mark();
        buffering = false;
    }

    @Override
    public int read() throws IOException {
        if (position < buffer.getSize()) {
            // there are bytes remaining in the buffer
            final int b = buffer.read(position) & 0xFF;
            position++;
            return b;
        }

        if (buffering) {
            int b = in.read();
            if (b == -1) return b;
            buffer.put((byte) b);
            position++;
            return b;
        } else {
            return in.read();
        }
    }

    @Override
    public int read(byte[] buf, int offset, int length) throws IOException {
        if (position < buffer.getSize()) {
            // there are bytes remaining in the buffer
            int nb = this.buffer.read(position, buf, offset, length);
            position += nb;
            return nb;
        }

        if (buffering) {
            int nb = in.read(buf, offset, length);
            if (nb == -1) return nb;
            buffer.put(buf,offset,nb);
            position += nb;
            return nb;
        } else {
            return in.read(buf, offset, length);
        }
    }

    @Override
    public long skip(long count) throws IOException {
        if (position < buffer.getSize()) {
            // there are bytes remaining in the buffer
            int nb = (int) Maths.min(buffer.getSize()-position, count);
            position += nb;
            return nb;
        }

        if (buffering) {
            int nb = in.read(temp, 0, Maths.min(temp.length, (int) count));
            if (nb == -1) return nb;
            buffer.put(temp,0,nb);
            position += nb;
            return nb;
        } else {
            return in.skip(count);
        }
    }
}
