package science.unlicense.encoding.impl.cryptography.algorithms;

/**
 *
 * @author Bertrand COTE
 */
public interface CipherAlgorithm {

    /**
     * Encrypts input array.
     *
     * @param input
     * @return
     */
    public byte[] cipher(byte[] input);

    /**
     * Decrypts input array.
     * @param input
     * @return
     */
    public byte[] invCipher(byte[] input);

}
