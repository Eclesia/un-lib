/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package science.unlicense.encoding.impl.io;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.encoding.api.io.AbstractSeekableByteBuffer;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SeekableBuffer extends AbstractSeekableByteBuffer {

    final Buffer buffer;
    private long position = 0;

    public SeekableBuffer(Buffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public long getSize() throws IOException {
        return buffer.getByteSize();
    }

    @Override
    public void setPosition(long position) throws IOException {
        this.position = position;
    }

    @Override
    public long getPosition() throws IOException {
        return position;
    }

    @Override
    public byte[] read(byte[] buffer, int offset, int length) throws IOException {
        this.buffer.readInt8(buffer, offset, length, position);
        return buffer;
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        this.buffer.writeInt8(buffer, offset, length, position);
    }

}
