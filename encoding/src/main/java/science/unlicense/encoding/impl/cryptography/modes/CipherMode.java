package science.unlicense.encoding.impl.cryptography.modes;

import science.unlicense.encoding.impl.cryptography.paddings.InvalidPadException;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Bertrand COTE
 */
public interface CipherMode {

    /**
     * Encrypts input array(The IV must be prepended to the input text).
     *
     * @param input the text to cipher (The IV is prepended to the input text).
     * @return the cipherText.
     */
    public byte[] cipher(byte[] input);

    /**
     * Encrypts inputStream (The IV must be prepended to the input text),
     * and put the ciphered text to the outputStream.
     *
     * @param input
     * @param output
     * @throws IOException
     */
    public void cipher( ByteInputStream input, ByteOutputStream output ) throws IOException;

    /**
     * Decrypts input array.
     *
     * @param input the cipheText.
     * @return the plainText without the IV.
     * @throws science.unlicense.encoding.impl.cryptography.paddings.InvalidPadException
     */
    public byte[] invCipher(byte[] input) throws InvalidPadException;

    public void invCipher( ByteInputStream input, ByteOutputStream output ) throws IOException, InvalidPadException;

}
