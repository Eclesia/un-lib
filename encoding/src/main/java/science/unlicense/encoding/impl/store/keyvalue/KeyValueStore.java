
package science.unlicense.encoding.impl.store.keyvalue;

import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 * Store key value pairs in plain text with pattern :
 * key1=value1
 * key2=value2
 *
 * @author Johann Sorel
 */
public class KeyValueStore {

    private final Dictionary properties = new HashDictionary();
    private final CharEncoding encoding;
    private Path path;

    public KeyValueStore() {
        this(null);
    }

    /**
     * Store from path with default encoding (UTF-8).
     *
     * @param path
     */
    public KeyValueStore(Path path) {
        this(path, CharEncodings.UTF_8);
    }

    public KeyValueStore(Path path, CharEncoding encoding) {
        this.path = path;
        this.encoding = encoding;
    }

    public Chars getProperty(Chars name) {
        return (Chars) properties.getValue(name);
    }

    /**
     *
     * @param name property name
     * @param value property value
     */
    public void setProperty(Chars name,Chars value) {
        properties.add(name, value);
    }

    /**
     * Write properties on the original path.
     *
     * @return this store
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public KeyValueStore save() throws IOException{
        return save(path);
    }

    /**
     * Write properties on the given path.
     *
     * @param path
     * @return this store
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public KeyValueStore save(Path path) throws IOException {
        ByteOutputStream bs = null;
        try{
            bs = path.createOutputStream();
            return save(bs);
        }finally{
            if (bs != null) {
                bs.close();
            }
        }
    }

    /**
     * Write properties in given stream.
     *
     * @param stream output stream
     * @return this store
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public KeyValueStore save(final ByteOutputStream stream) throws IOException {
        final CharOutputStream cs = new CharOutputStream(stream, encoding);
        final Iterator ite = properties.getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            final Chars key = (Chars) pair.getValue1();
            final Chars value = (Chars) pair.getValue2();
            if (value!=null) {
                cs.write(key);
                cs.write('=');
                cs.write(value);
                cs.write('\n');
            }
        }
        return this;
    }

    /**
     * Load properties from original path.
     *
     * @return this store
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public KeyValueStore load() throws IOException {
        return load(path);
    }

    /**
     * Load properties from given path.
     *
     * @param path
     * @return this store
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public KeyValueStore load(final Path path) throws IOException {
        ByteInputStream bs = null;
        try{
            if (path.exists()) {
                bs = path.createInputStream();
                load(bs);
            }
            return this;
        }finally{
            if (bs != null) {
                bs.dispose();
            }
        }
    }

    /**
     * Load properties from given stream.
     *
     * @param stream output stream
     * @return this store
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public KeyValueStore load(final ByteInputStream stream) throws IOException {
        final CharInputStream cs = new CharInputStream(stream, encoding);
        for (Chars line = cs.readLine();line!=null;line=cs.readLine()) {
            final int index = line.getFirstOccurence('=');
            final Chars key = line.truncate(0, index);
            final Chars value = line.truncate(index+1,line.getCharLength());
            properties.add(key, value.trim());
        }
        return this;
    }

}
