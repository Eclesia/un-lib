
package science.unlicense.encoding.impl.io.huffman;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.exception.MishandleException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public final class HuffmanTree extends CObject{

    public final HuffmanNode[] nodes;

    //sorted nodes in a binary tree
    private final HuffmanNode root = new HuffmanNode(0, 0);

    public HuffmanTree(HuffmanNode[] nodes) {
        this.nodes = nodes;

        for (int i=0;i<nodes.length;i++) {
            append(nodes[i]);
        }

        //fill the leaf property of each node
        fillLeaf(root);
    }

    public HuffmanNode getRoot() {
        return root;
    }

    private void fillLeaf(HuffmanNode node) {
        if (node==null) return;
        final boolean n0 = node.next0==null;
        final boolean n1 = node.next1==null;
        final boolean v = node.value==null;

        if (n0 && n1) {
            if (v) throw new MishandleException("Unvalid huffman tree, leaf node has no value");
            node.leaf = true;
        } else if (!v) {
            throw new MishandleException("Unvalid huffman tree, a node with childrens has a value");
        }
        fillLeaf(node.next0);
        fillLeaf(node.next1);
    }

    private void append(HuffmanNode node) {
        if (node.length==0) return;
        final HuffmanNode parent = getParent(node);
        final int val = node.getBit(node.length-1);
        if (val==0) {
            parent.next0 = node;
        } else {
            parent.next1 = node;
        }
    }

    private HuffmanNode getParent(HuffmanNode node) {
        HuffmanNode parent = root;
        int val;
        for (int i=0;i<node.length-1;i++) {
            val = node.getBit(i);
            if (val==0) {
                if (parent.next0 == null) {
                    parent.next0 = new HuffmanNode(parent.length+1, parent.code);
                }
                parent = parent.next0;
            } else {
                if (parent.next1 == null) {
                    parent.next1 = new HuffmanNode(parent.length+1, parent.code | (1 << parent.length));
                }
                parent = parent.next1;
            }
        }
        return parent;
    }



    /**
     * Decode value from input stream and given huffman tree.
     *
     * @param in
     * @return
     * @throws science.unlicense.encoding.api.io.IOException
     */
    public Object decode(DataInputStream in) throws IOException{
        int code;
        HuffmanNode node = root;
        for (;;) {
            code = in.readBits(1);
            node = (code==0) ? node.next0 : node.next1;
            if (node.leaf) return node.value;
        }
    }

}
