
package science.unlicense.encoding.impl.io;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.WrapInputStream;

/**
 *
 * @author Johann Sorel
 */
public class HexInputStream extends WrapInputStream {

    public HexInputStream(ByteInputStream in) {
        super(in);
    }

    @Override
    public int read() throws IOException {
        int v = in.read();
        if (v<0) return v;

        int value = 0;

        int b1 = v - 48;
        if (b1>9) b1-=7;

        v = in.read();
        int b2 = v - 48;
        if (b2>9) b2-=7;
        value = 16*b1 + b2;

        return value;
    }

}
