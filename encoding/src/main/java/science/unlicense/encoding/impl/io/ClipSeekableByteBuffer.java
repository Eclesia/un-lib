
package science.unlicense.encoding.impl.io;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.SeekableByteBuffer;

/**
 * Restricted seekable byte buffer.
 *
 * TODO : just a draft, must do position check and size change on insert
 *
 * @author Johann Sorel
 */
public class ClipSeekableByteBuffer implements SeekableByteBuffer {

    private final SeekableByteBuffer parent;
    private final long offset;
    private final long size;

    public ClipSeekableByteBuffer(SeekableByteBuffer parent, long offset, long size) throws IOException {
        this.parent = parent;
        this.offset = offset;
        this.size = size;
        parent.skipAll(offset);
    }

    @Override
    public long getSize() throws IOException {
        return size;
    }

    @Override
    public boolean isReadable() {
        return parent.isReadable();
    }

    @Override
    public boolean isWritable() {
        return parent.isWritable();
    }

    @Override
    public boolean isResizable() {
        return parent.isResizable();
    }

    @Override
    public void setPosition(long position) throws IOException {
        parent.setPosition(offset+position);
    }

    @Override
    public long getPosition() throws IOException {
        return parent.getPosition()-offset;
    }

    @Override
    public long skip(long nb) throws IOException {
        return parent.skip(nb);
    }

    @Override
    public void skipAll(long nb) throws IOException {
        parent.skipAll(nb);
    }

    @Override
    public int read() throws IOException {
        return parent.read();
    }

    @Override
    public byte[] read(int nb) throws IOException {
        return parent.read(nb);
    }

    @Override
    public byte[] read(byte[] buffer) throws IOException {
        return parent.read(buffer);
    }

    @Override
    public byte[] read(byte[] buffer, int offset, int length) throws IOException {
        return parent.read(buffer, offset, length);
    }

    @Override
    public void write(byte b) throws IOException {
        parent.write(b);
    }

    @Override
    public void write(byte[] buffer) throws IOException {
        parent.write(buffer);
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        parent.write(buffer, offset, length);
    }

    @Override
    public void insert(byte b) throws IOException {
        parent.insert(b);
    }

    @Override
    public void insert(byte[] buffer) throws IOException {
        parent.insert(buffer);
    }

    @Override
    public void insert(byte[] buffer, int offset, int length) throws IOException {
        parent.insert(buffer, offset, length);
    }

    @Override
    public void remove(int nb) throws IOException {
        parent.remove(nb);
    }

    @Override
    public void flush() throws IOException {
        parent.flush();
    }

    @Override
    public void dispose() throws IOException {
        parent.dispose();
    }

}
