
package science.unlicense.encoding.impl.cryptography.hash;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;

/**
 * Adler32 checksum
 * Java adaptation of RFC1950 : http://tools.ietf.org/html/rfc1950#page-10
 *
 * @author Johann Sorel
 */
public final class Adler32 implements HashFunction{

    public static final Chars NAME = Chars.constant("ADLER-32");

    /* largest prime smaller than 65536 */
    private static final int BASE = 65521;

    //current checksum value
    private int s1 = 1;
    private int s2 = 0;

    public Chars getName() {
        return NAME;
    }

    public int getResultLength() {
        return 32;
    }

    public boolean isCryptographic() {
        return false;
    }

    public void reset() {
        s1 = 1;
        s2 = 0;
    }

    public void update(int b) {
        s1 = (s1 + b) % BASE;
        s2 = (s2 + s1) % BASE;
    }

    public void update(final byte[] buffer) {
        update(buffer, 0, buffer.length);
    }

    public void update(final byte[] buffer, int offset, int length) {
        for (int i=0;i<length;i++) {
            update(buffer[i+offset] & 0xff);
        }
    }

    public byte[] getResultBytes() {
        final byte[] buffer = new byte[4];
        Endianness.BIG_ENDIAN.writeUInt(getResultLong(), buffer, 0);
        return buffer;
    }

    public long getResultLong() {
        return (s2 << 16) + s1 ;
    }

}
