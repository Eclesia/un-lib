
package science.unlicense.encoding.impl.io.deflate;

import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.WrapOutputStream;
import science.unlicense.common.api.number.Endianness;

/**
 * Deflate output stream, write only uncompressed for now.
 *
 * @author Johann Sorel
 */
public class DeflateOutputStream extends WrapOutputStream{

    private final ByteSequence buffer = new ByteSequence();

    public DeflateOutputStream(ByteOutputStream out) {
        super(out);
    }

    @Override
    public void write(byte b) throws IOException {
        buffer.put(b);
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        this.buffer.put(buffer, offset, length);
    }

    @Override
    public void flush() throws IOException {
        final DataOutputStream ds = new DataOutputStream(out);
        int offset = 0;
        final byte[] data = buffer.toArrayByte();
        while (offset<data.length) {
            int dist = data.length-offset;
            if (dist>65535) {
                dist = 65535;
                ds.write((byte) 0x00);
            } else {
                ds.write((byte) 0x01);
            }
            ds.writeUShort(dist, Endianness.LITTLE_ENDIAN);
            ds.writeUShort(~dist, Endianness.LITTLE_ENDIAN);
            ds.write(data,offset,dist);
            offset += dist;
        }
        buffer.removeAll();

        super.flush();
    }

}
