
package science.unlicense.encoding.impl.io;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.ByteResource;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.SeekableByteBuffer;
import science.unlicense.encoding.api.io.SeekableInputStream;
import science.unlicense.encoding.api.io.SeekableOutputStream;

/**
 *
 * @author Johann Sorel
 */
public class ByteBufferResource implements ByteResource {

    private final Buffer buffer;

    public ByteBufferResource(Buffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public long getSize() {
        return buffer.getByteSize();
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        return new SeekableInputStream(new SeekableBuffer(buffer));
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        return new SeekableOutputStream(new SeekableBuffer(buffer));
    }

    @Override
    public SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException {
        return new SeekableBuffer(buffer);
    }

}
