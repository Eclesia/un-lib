package science.unlicense.encoding.impl.cryptography.algorithms;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.math.impl.number.LargeInteger;

/**
 * Implementation of RSA
 *
 * @author Francois Berder
 *
 */
public class RSA {

    private LargeInteger modulus;
    private LargeInteger publicExponent;
    private LargeInteger privateExponent;

    public RSA(LargeInteger modulus, LargeInteger publicExponent, LargeInteger privateExponent) {
        this.modulus = modulus;
        this.publicExponent = publicExponent;
        this.privateExponent = privateExponent;
    }

    public byte[] cipher(byte[] input) {
        return encrypt(input, modulus, publicExponent);
    }

    public byte[] invCipher(byte[] input) {
        return encrypt(input, modulus, privateExponent);
    }

    private static byte[] encrypt(byte[] input, LargeInteger modulus, LargeInteger exponent) {
        // 1. Split input into blocks
        byte[][] blocks = splitIntoBlocks(input, modulus.getByteLength(), modulus);

        // 2. Encrypt each block and store result into byte[][]
        byte[][] cipherText = new byte[blocks.length][];
        for (int i = 0; i < blocks.length; ++i) {
             LargeInteger blockValue = new LargeInteger(1, blocks[i]);
             LargeInteger cipherBlock = blockValue.modPowFast(exponent, modulus);

            int byteLength = cipherBlock.getByteLength();
            cipherText[i] = new byte[byteLength];
            long[] mag = cipherBlock.getMagnitude();
            for (int j = 0; j < byteLength; ++j) {
                cipherText[i][j] = (byte) ((mag[j/8] >>> ((j % 8) * 8)) & 0xff);
            }
        }

        // 3. Convert byte[][] into byte[]
        int nbTotalBytes = 0;
        for (int i = 0; i < cipherText.length; ++i) {
            nbTotalBytes += cipherText[i].length;
        }

        byte[] result = new byte[nbTotalBytes];
        int index = 0;
        for (int i = 0; i < cipherText.length; ++i) {
            for (int j = 0; j < cipherText[i].length; ++j) {
                result[index] = cipherText[i][j];
                ++index;
            }
        }
        return result;
    }

    /**
     * Split input into an array of blocks.
     *
     * @param input
     * @param maxBlockSize max number of bytes in each block
     * @param maxValue Maximum value of a block (needed to ensure that each block is lower than modulus)
     * @return
     */
    private static byte[][] splitIntoBlocks(byte[] input, int maxBlockSize, LargeInteger maxValue) {
        if (input.length < maxBlockSize) {
            return new byte[][] { Arrays.copy(input) };
        }

        int currentIndex = 0;
        ArraySequence seq = new ArraySequence();
        while (currentIndex < input.length) {
            int blockLength = maxBlockSize;
            if (input.length - currentIndex < maxBlockSize) {
                blockLength = input.length - currentIndex;
            }
            if (blockLength == maxBlockSize) {
                byte[] tmp = Arrays.copy(input, currentIndex, blockLength);
                LargeInteger blockValue = new LargeInteger(1, tmp);
                if (blockValue.subtract(maxValue).getSign() == 1) {
                    --blockLength;
                }
            }
            byte[] block = Arrays.copy(input, currentIndex, blockLength);
            seq.add(block);

            currentIndex += blockLength;
        }

        byte[][] blocks = new byte[seq.getSize()][];
        for (int i = 0; i < blocks.length; ++i) {
            blocks[i] = Arrays.copy((byte[]) seq.get(i));
        }
        return blocks;
    }
}
