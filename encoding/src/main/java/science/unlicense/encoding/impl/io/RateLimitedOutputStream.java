
package science.unlicense.encoding.impl.io;

import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.WrapOutputStream;

/**
 * OutputStream which limit the byte write rate per second.
 *
 * TODO : implement array and buffer methods for better performances
 *
 * @author Johann Sorel
 */
public class RateLimitedOutputStream extends WrapOutputStream {

    private int bytePerSecond;
    private int nbRead = 0;
    private long step = System.currentTimeMillis();

    public RateLimitedOutputStream(ByteOutputStream out, int bytePerSecond) {
        super(out);
        this.bytePerSecond = bytePerSecond;
    }

    public int getBytePerSecond() {
        return bytePerSecond;
    }

    public void setBytePerSecond(int bytePerSecond) {
        this.bytePerSecond = bytePerSecond;
    }

    @Override
    public void write(byte b) throws IOException {
        if (nbRead>=bytePerSecond) {
            final long remaining = System.currentTimeMillis() - step;
            if (remaining<1000) {
                try {
                    // sleep until second end
                    Thread.sleep(remaining);
                } catch (InterruptedException ex) {
                    throw new IOException(this, ex);
                }
            }

            //reset
            nbRead = 0;
            step = System.currentTimeMillis();
        }
        out.write(b);
    }

}
