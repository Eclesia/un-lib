
package science.unlicense.encoding.impl.io;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.WrapInputStream;

/**
 * InputStream which limit the byte read rate per second.
 *
 * TODO : implement array and buffer methods for better performances
 *
 * @author Johann Sorel
 */
public class RateLimitedInputStream extends WrapInputStream {

    private int bytePerSecond;
    private int nbRead = 0;
    private long step = System.currentTimeMillis();

    public RateLimitedInputStream(ByteInputStream in, int bytePerSecond) {
        super(in);
        this.bytePerSecond = bytePerSecond;
    }

    public int getBytePerSecond() {
        return bytePerSecond;
    }

    public void setBytePerSecond(int bytePerSecond) {
        this.bytePerSecond = bytePerSecond;
    }

    @Override
    public int read() throws IOException {
        if (nbRead>=bytePerSecond) {
            final long remaining = System.currentTimeMillis() - step;
            if (remaining<1000) {
                try {
                    // sleep until second end
                    Thread.sleep(remaining);
                } catch (InterruptedException ex) {
                    throw new IOException(this, ex);
                }
            }

            //reset
            nbRead = 0;
            step = System.currentTimeMillis();
        }
        return in.read();
    }

}
