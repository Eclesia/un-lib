
package science.unlicense.encoding.impl.io.lz4;

import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Decode a LZ4 Frame.
 *
 * Specification : https://github.com/lz4/lz4/blob/dev/doc/lz4_Frame_format.md
 *
 * @author Johann Sorel
 */
public class LZ4Frame {

    public static final int MAGIC = 0x184D2204;

    private DataInputStream ds;
    private int version;
    private boolean flagBlockIndep;
    private boolean flagBlockChecksum;
    private boolean flagContentSize;
    private boolean flagContentChecksum;
    private boolean flagDictId;
    private int blockMaxSize;
    private long contentSize;
    private int dicoId;

    //decoded blocks
    private final FrameBuffer buffer = new FrameBuffer(65536);
    private boolean finished = false;

    /**
     *
     * @param in
     * @throws IOException
     */
    public void init(ByteInputStream in) throws IOException {
        ds = new DataInputStream(in,Endianness.LITTLE_ENDIAN);

        if (ds.readInt() != MAGIC) {
            throw new IOException(in, "Frame signature do not match");
        }

        //first flag byte
        version   = ds.readBits(2, DataInputStream.MSB);
        if (version != 1) {
            throw new IOException(in, "Unsupported version "+version);
        }
        flagBlockIndep      = ds.readBits(1, DataInputStream.MSB) == 1;
        flagBlockChecksum   = ds.readBits(1, DataInputStream.MSB) == 1;
        flagContentSize     = ds.readBits(1, DataInputStream.MSB) == 1;
        flagContentChecksum = ds.readBits(1, DataInputStream.MSB) == 1;
                              ds.readBits(1, DataInputStream.MSB);
        flagDictId          = ds.readBits(1, DataInputStream.MSB) == 1;
        //second flag
                              ds.readBits(1, DataInputStream.MSB);
        blockMaxSize        = ds.readBits(3, DataInputStream.MSB);
                              ds.readBits(4, DataInputStream.MSB);

        if (flagContentSize) {
            contentSize = ds.readUInt();
        }
        if (flagDictId) {
            dicoId = ds.readInt();
        }
        final int headerChecksum = ds.readUByte();

        //reset caches
        finished = false;
    }

    /**
     * Read next byte in frame.
     *
     * @return -1 if stream is finished
     * @throws IOException
     */
    public int read() throws IOException {

        boolean compressed = false;
        int blockSize = 0;
        while (buffer.isEmpty()) {
            if (finished) return -1;

            blockSize = ds.readInt();
            compressed = (blockSize >>> 31) == 0;
            blockSize = blockSize & 0x7FFFFFFF;
            if (blockSize == 0 && compressed) {
                //end marker
                finished = true;
                if (flagContentChecksum) {
                    int contentChecksum = ds.readInt();
                }
                continue;
            }

            final byte[] block = ds.readBytes(blockSize);
            if (flagBlockChecksum) {
                int blockChecksum = ds.readInt();
            }
            if (compressed) {
                final LZ4Block lz4block = new LZ4Block(block);
                lz4block.uncompress(buffer);
            } else {
                for (int i=0; i<block.length; i++) buffer.put(block[i]);
            }
        }

        return buffer.peek();
    }

}
