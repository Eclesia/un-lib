
package science.unlicense.encoding.impl.io.zlib;

import science.unlicense.encoding.api.io.AbstractInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.deflate.DeflateInputStream;

/**
 * ZLib decompressor.
 * As defined in : http://tools.ietf.org/html/rfc1950
 *
 * @author Johann Sorel
 */
public class ZlibInputStream extends AbstractInputStream {

    private final DataInputStream in;
    private DeflateInputStream deflate;

    public ZlibInputStream(final ByteInputStream in) {
        this.in = new DataInputStream(in);
    }

    private int initDeflate() throws IOException{
        if (deflate == null) {
            // Compression Method and flags
            // bits 0 to 3  CM     Compression method
            // bits 4 to 7  CINFO  Compression info
            final int cmf = in.read();
            final int flg = in.read();
            if (cmf == -1 || flg == -1) return -1;
            final int cm = (cmf & 0x0F);
            final int cinfo = (cmf & 0xF0) >> 4;
            if (cm != 8) throw new IOException(this, "unsupported compression method : " + cm);
            if (cinfo > 7) throw new IOException(this, "unvalid window : " + cinfo);
            final int fcheck = flg & 0x1F;
            final int fdict =  flg & 0x20;
            final int flevel = flg >>> 6;

            if (fdict != 0) {
                //has a dictionnary
                int dictId = in.readInt();
                throw new IOException(this, "Dictionnary not supported yet.");
            }


            deflate = new DeflateInputStream(in);
        }
        return 0;
    }

    @Override
    public int read() throws IOException {
        if (initDeflate()==-1) return -1;

        int b = deflate.read();
        if (b==-1) {
            //deflate finished
            //skip end adler32 value
            in.readInt();
        }

        return b;
    }

    @Override
    public int read(byte[] buffer, int offset, int length) throws IOException {
        if (initDeflate()==-1) return -1;
        return deflate.read(buffer, offset, length);
    }

    @Override
    public void dispose() throws IOException {
        in.dispose();
    }

}
