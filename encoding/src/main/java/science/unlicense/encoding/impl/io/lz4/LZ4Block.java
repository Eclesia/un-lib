
package science.unlicense.encoding.impl.io.lz4;

import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * Decode a LZ4 block.
 *
 * Specification : https://github.com/lz4/lz4/blob/dev/doc/lz4_Block_format.md
 *
 * @author Johann Sorel
 */
public class LZ4Block {

    private final byte[] data;
    private final DataInputStream ds;

    public LZ4Block(byte[] data) {
        this.data = data;
        this.ds = new DataInputStream(new ArrayInputStream(data), Endianness.LITTLE_ENDIAN);
    }

    public void uncompress(FrameBuffer buffer) throws IOException {
        for (;;) {
            int literalLength = ds.readBits(4, DataInputStream.MSB);
            int matchLength = ds.readBits(4, DataInputStream.MSB);

            //read extra length
            if (literalLength==15) {
                int v;
                do {
                    v = ds.readUByte();
                    literalLength += v;
                } while (v==255);
            }

            //read literals
            byte[] literals = ds.readBytes(literalLength);
            for (int i=0; i<literalLength; i++) buffer.put(literals[i]);

            //Note that the last sequence is also incomplete, and stops right after literals.
            if (ds.getByteOffset() == data.length) break;

            //read offset
            final int offset = ds.readUShort();
            if (offset == 0) throw new IOException(ds, "Unvalid offset value");

            //read extra match length
            if (matchLength==15) {
                int v;
                do {
                    v = ds.readUByte();
                    matchLength += v;
                } while (v==255);
            }
            matchLength += 4; //minimum length is 4

            for (int i=0;i<matchLength;i++) {
                //we dont change the offset since it will be displaced when
                //putting the byte at the end.
                final byte b = buffer.remember(offset);
                buffer.put(b);
            }
        }
    }

}
