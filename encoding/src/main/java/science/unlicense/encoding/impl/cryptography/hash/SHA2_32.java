package science.unlicense.encoding.impl.cryptography.hash;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;

/**
 * Code used by SHA224 and SHA256.
 *
 * Description: https://tools.ietf.org/html/rfc4634
 *
 * @author Francois Berder
 *
 */
public abstract class SHA2_32 implements HashFunction {

    private final int resetState[];
    private int state[] = new int[8];
    private long count;
    private byte[] digestBits;

    private int block[] = new int[16];
    private int blockIndex;

    //    static const uint8_t MASK = 0x0F;
    //    #define W(t) (w[(t)] = SSIG1(w[((t)+14)&MASK]) + w[((t)+9)&MASK] + SSIG0(w[((t)+1)&MASK]) + w[t])
    //
    //    #define ROTL(W,N) (((W) << (N)) | ((W) >> (32-(N))))
    //    #define ROTR(W,N) (ROTL(W,32-N))
    //    #define CH(X,Y,Z) (((X) & (Y)) ^ ((~(X)) & (Z)))
    //    #define MAJ(X,Y,Z) (((X) & (Y)) ^ ((X) & (Z)) ^ ((Y) & (Z)))
    //    #define BSIG0(X) (ROTR(X,2) ^ ROTR(X,13) ^ ROTR(X,22))
    //    #define BSIG1(X) (ROTR(X,6) ^ ROTR(X,11) ^ ROTR(X,25))
    //    #define SSIG0(X) (ROTR((X),7) ^ ROTR((X),18) ^ ((X) >> 3))
    //    #define SSIG1(X) (ROTR((X),17) ^ ROTR((X),19) ^ ((X) >> 10))
    //    #define R(A,B,C,D,E,F,G,H,T,K)  T1 = H + BSIG1(E) + CH(E,F,G) + K + (w[T] = rev(buffer2[T])); \
    //                                  T2 = BSIG0(A) + MAJ(A,B,C); \
    //                                  D += T1; \
    //                                  H = T1 + T2;
    //    #define R2(A,B,C,D,E,F,G,H,T,K)  T1 = H + BSIG1(E) + CH(E,F,G) + K + W(T&MASK); \
    //                                  T2 = BSIG0(A) + MAJ(A,B,C); \
    //                                  D += T1; \
    //                                  H = T1 + T2;
    private final int dd[] = new int[8];

    SHA2_32(int[] resetState) {
        this.resetState = resetState;
        reset();
    }

    @Override
    public boolean isCryptographic() {
        return true;
    }

    @Override
    public void reset() {

        state[0] = resetState[0];
        state[1] = resetState[1];
        state[2] = resetState[2];
        state[3] = resetState[3];
        state[4] = resetState[4];
        state[5] = resetState[5];
        state[6] = resetState[6];
        state[7] = resetState[7];

        count = 0;
        digestBits = new byte[getResultLength()/8];
        Arrays.fill(digestBits, (byte) 0);
        blockIndex = 0;
    }

    public void update(int b) {
        update((byte) b);
    }

    public void update(byte b) {
        final int mask = (8 * (blockIndex & 3));

        count += 8;
        block[blockIndex >> 2] &= ~(0xff << mask);
        block[blockIndex >> 2] |= (b & 0xff) << mask;
        blockIndex++;
        if (blockIndex == 64) {
            transform();
            blockIndex = 0;
        }
    }

    public byte[] getResultBytes() {
        finish();
        return Arrays.copy(digestBits, new byte[digestBits.length]);
    }

    public long getResultLong() {
        return Endianness.BIG_ENDIAN.readLong(getResultBytes(), 0);
    }

    public void update(byte input[]) {
        update(input, 0, input.length);
    }

    public void update(byte input[], int offset, int len) {
        for (int i = 0; i < len; i++) {
            update(input[i + offset]);
        }
    }

    private void transform() {
        int w[] = new int[16];
        int T[] = new int[2];

        /* Copy context->state[] to working vars */
        dd[0] = state[0];
        dd[1] = state[1];
        dd[2] = state[2];
        dd[3] = state[3];
        dd[4] = state[4];
        dd[5] = state[5];
        dd[6] = state[6];
        dd[7] = state[7];

        R(T,w,dd,0,1,2,3,4,5,6,7,0,0x428a2f98);
        R(T,w,dd,7,0,1,2,3,4,5,6,1,0x71374491);
        R(T,w,dd,6,7,0,1,2,3,4,5,2,0xb5c0fbcf);
        R(T,w,dd,5,6,7,0,1,2,3,4,3,0xe9b5dba5);
        R(T,w,dd,4,5,6,7,0,1,2,3,4,0x3956c25b);
        R(T,w,dd,3,4,5,6,7,0,1,2,5,0x59f111f1);
        R(T,w,dd,2,3,4,5,6,7,0,1,6,0x923f82a4);
        R(T,w,dd,1,2,3,4,5,6,7,0,7,0xab1c5ed5);

        R(T,w,dd,0,1,2,3,4,5,6,7,8,0xd807aa98);
        R(T,w,dd,7,0,1,2,3,4,5,6,9,0x12835b01);
        R(T,w,dd,6,7,0,1,2,3,4,5,10,0x243185be);
        R(T,w,dd,5,6,7,0,1,2,3,4,11,0x550c7dc3);
        R(T,w,dd,4,5,6,7,0,1,2,3,12,0x72be5d74);
        R(T,w,dd,3,4,5,6,7,0,1,2,13,0x80deb1fe);
        R(T,w,dd,2,3,4,5,6,7,0,1,14,0x9bdc06a7);
        R(T,w,dd,1,2,3,4,5,6,7,0,15,0xc19bf174);

        R2(T,w,dd,0,1,2,3,4,5,6,7,16,0xe49b69c1);
        R2(T,w,dd,7,0,1,2,3,4,5,6,17,0xefbe4786);
        R2(T,w,dd,6,7,0,1,2,3,4,5,18,0x0fc19dc6);
        R2(T,w,dd,5,6,7,0,1,2,3,4,19,0x240ca1cc);
        R2(T,w,dd,4,5,6,7,0,1,2,3,20,0x2de92c6f);
        R2(T,w,dd,3,4,5,6,7,0,1,2,21,0x4a7484aa);
        R2(T,w,dd,2,3,4,5,6,7,0,1,22,0x5cb0a9dc);
        R2(T,w,dd,1,2,3,4,5,6,7,0,23,0x76f988da);

        R2(T,w,dd,0,1,2,3,4,5,6,7,24,0x983e5152);
        R2(T,w,dd,7,0,1,2,3,4,5,6,25,0xa831c66d);
        R2(T,w,dd,6,7,0,1,2,3,4,5,26,0xb00327c8);
        R2(T,w,dd,5,6,7,0,1,2,3,4,27,0xbf597fc7);
        R2(T,w,dd,4,5,6,7,0,1,2,3,28,0xc6e00bf3);
        R2(T,w,dd,3,4,5,6,7,0,1,2,29,0xd5a79147);
        R2(T,w,dd,2,3,4,5,6,7,0,1,30,0x06ca6351);
        R2(T,w,dd,1,2,3,4,5,6,7,0,31,0x14292967);

        R2(T,w,dd,0,1,2,3,4,5,6,7,32,0x27b70a85);
        R2(T,w,dd,7,0,1,2,3,4,5,6,33,0x2e1b2138);
        R2(T,w,dd,6,7,0,1,2,3,4,5,34,0x4d2c6dfc);
        R2(T,w,dd,5,6,7,0,1,2,3,4,35,0x53380d13);
        R2(T,w,dd,4,5,6,7,0,1,2,3,36,0x650a7354);
        R2(T,w,dd,3,4,5,6,7,0,1,2,37,0x766a0abb);
        R2(T,w,dd,2,3,4,5,6,7,0,1,38,0x81c2c92e);
        R2(T,w,dd,1,2,3,4,5,6,7,0,39,0x92722c85);

        R2(T,w,dd,0,1,2,3,4,5,6,7,40,0xa2bfe8a1);
        R2(T,w,dd,7,0,1,2,3,4,5,6,41,0xa81a664b);
        R2(T,w,dd,6,7,0,1,2,3,4,5,42,0xc24b8b70);
        R2(T,w,dd,5,6,7,0,1,2,3,4,43,0xc76c51a3);
        R2(T,w,dd,4,5,6,7,0,1,2,3,44,0xd192e819);
        R2(T,w,dd,3,4,5,6,7,0,1,2,45,0xd6990624);
        R2(T,w,dd,2,3,4,5,6,7,0,1,46,0xf40e3585);
        R2(T,w,dd,1,2,3,4,5,6,7,0,47,0x106aa070);

        R2(T,w,dd,0,1,2,3,4,5,6,7,48,0x19a4c116);
        R2(T,w,dd,7,0,1,2,3,4,5,6,49,0x1e376c08);
        R2(T,w,dd,6,7,0,1,2,3,4,5,50,0x2748774c);
        R2(T,w,dd,5,6,7,0,1,2,3,4,51,0x34b0bcb5);
        R2(T,w,dd,4,5,6,7,0,1,2,3,52,0x391c0cb3);
        R2(T,w,dd,3,4,5,6,7,0,1,2,53,0x4ed8aa4a);
        R2(T,w,dd,2,3,4,5,6,7,0,1,54,0x5b9cca4f);
        R2(T,w,dd,1,2,3,4,5,6,7,0,55,0x682e6ff3);

        R2(T,w,dd,0,1,2,3,4,5,6,7,56,0x748f82ee);
        R2(T,w,dd,7,0,1,2,3,4,5,6,57,0x78a5636f);
        R2(T,w,dd,6,7,0,1,2,3,4,5,58,0x84c87814);
        R2(T,w,dd,5,6,7,0,1,2,3,4,59,0x8cc70208);
        R2(T,w,dd,4,5,6,7,0,1,2,3,60,0x90befffa);
        R2(T,w,dd,3,4,5,6,7,0,1,2,61,0xa4506ceb);
        R2(T,w,dd,2,3,4,5,6,7,0,1,62,0xbef9a3f7);
        R2(T,w,dd,1,2,3,4,5,6,7,0,63,0xc67178f2);


        /* Add the working vars back into context.state[] */
        state[0] += dd[0];
        state[1] += dd[1];
        state[2] += dd[2];
        state[3] += dd[3];
        state[4] += dd[4];
        state[5] += dd[5];
        state[6] += dd[6];
        state[7] += dd[7];
    }

    private static int rev(int w) {
        return ((w & 0x000000FF) << 24)
            |  ((w & 0x0000FF00) << 8)
            |  ((w & 0x00FF0000) >>> 8)
            |  ((w & 0xFF000000) >>> 24);
    }


    private int W(int w[], int t) {
        w[t] = SSIG1(w[(t+14) & 0x0F]) + w[(t+9)&0x0F] + SSIG0(w[(t+1)&0x0F]) + w[t];
        return w[t];
    }

    private int ROTL(int W, int N) {
        return (W << N) | (W >>> (32-N));
    }

    private int ROTR(int W, int N) {
        return ROTL(W, 32-N);
    }

    private int CH(int X, int Y, int Z) {
        return ((X & Y) ^ ((~X) & Z));
    }

    private int MAJ(int X, int Y, int Z) {
        return (X & Y) ^ (X & Z) ^ (Y & Z);
    }

    private int BSIG0(int X) {
        return ROTR(X,2) ^ ROTR(X,13) ^ ROTR(X,22);
    }

    private int BSIG1(int X) {
        return ROTR(X,6) ^ ROTR(X,11) ^ ROTR(X,25);
    }

    private int SSIG0(int X) {
        return ROTR(X,7) ^ ROTR(X,18) ^ (X >>> 3);
    }

    private int SSIG1(int X) {
        return ROTR(X,17) ^ ROTR(X,19) ^ (X >>> 10);
    }

    private void R(int[] T, int[] w, int[] dd, int indexA, int indexB, int indexC, int indexD, int indexE, int indexF, int indexG, int indexH, int L, int K) {
        w[L] = rev(block[L]);
        T[0] = dd[indexH] + BSIG1(dd[indexE]) + CH(dd[indexE],dd[indexF],dd[indexG]) + K + w[L];
        T[1] = BSIG0(dd[indexA]) + MAJ(dd[indexA],dd[indexB],dd[indexC]);
        dd[indexD] += T[0];
        dd[indexH] = T[0] + T[1];
    }

    private void R2(int[] T, int[] w, int[] dd, int indexA, int indexB, int indexC, int indexD, int indexE, int indexF, int indexG, int indexH, int L, int K) {

        T[0] = dd[indexH] + BSIG1(dd[indexE]) + CH(dd[indexE],dd[indexF],dd[indexG]) + K + W(w, L&0x0F);
        T[1] = BSIG0(dd[indexA]) + MAJ(dd[indexA],dd[indexB],dd[indexC]);
        dd[indexD] += T[0];
        dd[indexH] = T[0] + T[1];
    }

    private void finish() {
        final byte bits[] = new byte[8];

        for (int i = 0; i < 8; i++) {
            bits[i] = (byte) ((count >>> (((7 - i) * 8))) & 0xff);
        }

        update((byte) 128);
        while (blockIndex != 56) {
            update((byte) 0);
        }
        // This should cause a transform to happen.
        update(bits);
        for (int i = 0; i < this.getResultLength()/8; i++) {
            digestBits[i] = (byte) ((state[i >> 2] >> ((3 - (i & 3)) * 8)) & 0xff);
        }
    }
}
