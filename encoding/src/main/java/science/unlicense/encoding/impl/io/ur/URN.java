
package science.unlicense.encoding.impl.io.ur;

import science.unlicense.common.api.character.Chars;

/**
 * Uniform Resource Name
 *
 * @author Johann Sorel
 */
public class URN extends URI{

    public URN(Chars schema, Chars path) {
        super(schema, path);
    }

}
