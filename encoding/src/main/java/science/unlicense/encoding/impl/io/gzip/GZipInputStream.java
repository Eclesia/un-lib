
package science.unlicense.encoding.impl.io.gzip;

import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.WrapInputStream;
import science.unlicense.encoding.impl.io.deflate.DeflateInputStream;

/**
 * GZip input stream :http://www.ietf.org/rfc/rfc1952.txt
 *
 * @author Johann Sorel
 */
public class GZipInputStream extends WrapInputStream{

    public static final int SIGNATURE = 35615;

    private final DataInputStream ds;
    private GZipMember currentMember = null;
    private DeflateInputStream deflateStream;

    //member end crc + len
    private int crc32;
    private int len;


    public GZipInputStream(ByteInputStream in) {
        super(in);
        ds = new DataInputStream(in);
        ds.setBitsDirection(DataInputStream.LSB);
    }

    public GZipMember getCurrentMember() {
        return currentMember;
    }

    @Override
    public int read() throws IOException {

        for (;;) {
            if (deflateStream != null) {
                final int b = deflateStream.read();
                if (b < 0) {
                    //we have finish this member
                    crc32 = ds.readInt(Endianness.LITTLE_ENDIAN);
                    len = ds.readInt(Endianness.LITTLE_ENDIAN);
                    currentMember = null;
                    deflateStream = null;
                } else {
                    return b;
                }
            }

            if (currentMember == null) {
                final int s1 = ds.read();
                final int s2 = ds.read();
                if (s1 == -1 || s2 == -1) {
                    return -1;
                }
                final int signature = (s2 << 8) + s1;
                if (signature != SIGNATURE) {
                    throw new IOException(this, "Stream is not a valid GZip stream.");
                }

                final int cm = ds.read();
                if (cm != 8) {
                    throw new IOException(this, "Unsupported compression method : "+cm+" only deflate(8) supported.");
                }

                currentMember = new GZipMember();

                //This flag byte is divided into individual bits as follows:
                //bit 0   FTEXT
                //bit 1   FHCRC
                //bit 2   FEXTRA
                //bit 3   FNAME
                //bit 4   FCOMMENT
                //bit 5   reserved
                //bit 6   reserved
                //bit 7   reserved
                currentMember.ftext = ds.readBits(1) != 0;
                currentMember.fhcrc = ds.readBits(1) != 0;
                currentMember.fextra = ds.readBits(1) != 0;
                final boolean fname = ds.readBits(1) != 0;
                final boolean fcomment = ds.readBits(1) != 0;
                ds.readBits(3);

                currentMember.mtime = ds.readUInt(Endianness.LITTLE_ENDIAN);
                currentMember.xfl = ds.read();
                currentMember.os = ds.read();

                if (currentMember.fextra) {
                    final int xlen = ds.readUShort(Endianness.LITTLE_ENDIAN);
                    final byte[] extra = new byte[xlen];
                    ds.readFully(extra);
                }

                if (fname) {
                    final ByteSequence namebuffer = new ByteSequence();
                    for (int b=ds.read();b>0;b=ds.read()) {
                        namebuffer.put((byte) b);
                    }
                    final String name = new String(namebuffer.toArrayByte());
                    currentMember.fname = name;
                }

                if (fcomment) {
                    final ByteSequence commentbuffer = new ByteSequence();
                    for (int b=ds.read();b>0;b=ds.read()) {
                        commentbuffer.put((byte) b);
                    }
                    final String comment = new String(commentbuffer.toArrayByte());
                    currentMember.fcomment = comment;
                }

                if (currentMember.fhcrc) {
                    final int crc16 = ds.readUShort(Endianness.LITTLE_ENDIAN);
                }

                deflateStream = new DeflateInputStream(in);
            }

            if (deflateStream == null) {
                //we have finish
                return -1;
            }
        }

    }

}
