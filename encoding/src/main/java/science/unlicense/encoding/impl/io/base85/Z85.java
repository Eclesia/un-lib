
package science.unlicense.encoding.impl.io.base85;

/**
 *
 * Document:
 * https://en.wikipedia.org/wiki/Ascii85
 * http://rfc.zeromq.org/spec:32
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class Z85 {

    private static final int[] TABLE_Z85 = {
    '0','1','2','3','4','5','6','7','8','9',
    'a','b','c','d','e','f','g','h','i','j',
    'k','l','m','n','o','p','q','r','s','t',
    'u','v','w','x','y','z','A','B','C','D',
    'E','F','G','H','I','J','K','L','M','N',
    'O','P','Q','R','S','T','U','V','W','X',
    'Y','Z','.','-',':','+','=','^','!','/',
    '*','?','&','<','>','(',')','[',']','{',
    '}','@','%','$','#'
    };

    private static final int[] SCALES = new int[]{
        52200625,
        614125,
        7225,
        85,
        1
    };

}
