package science.unlicense.encoding.impl.cryptography.modes;

import science.unlicense.common.api.Arrays;
import science.unlicense.encoding.impl.cryptography.algorithms.CipherAlgorithm;
import science.unlicense.encoding.impl.cryptography.paddings.InvalidPadException;
import science.unlicense.encoding.impl.cryptography.paddings.Padding;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Block cipher chaining mode (CBC).
 *
 * https://en.wikipedia.org/wiki/Block_cipher_modes_of_operation#Cipher-block_chaining_.28CBC.29
 *
 * @author Bertrand COTE
 */
public class CBC implements CipherMode {

    private final Padding padding;
    CipherAlgorithm algorithm;

    private static final int BLOCK_LENGTH = 16;

    // ========== Constructor ==================================================

    /**
     *
     * @param algorithm the cipher algorithm used ( e.g.: AES, DES... )
     * @param padding the padding used (e.g: NoPadding, PKCS5, ....
     */
    public CBC(CipherAlgorithm algorithm, Padding padding ) {
        this.algorithm = algorithm;
        this.padding = padding;
    }

    // ========== CipherMode methods ===========================================

    /**
     * In CBC mode, the IV is prepended to the plaintext.
     *
     * @param plaintext the given plaintext with the IV prepended.
     * @return the ciphertext (First block is the IV)
     */
    @Override
    public byte[] cipher( byte[] plaintext ) {

        byte[] paddedPlainText = this.padding.pad(plaintext);

        byte[] ciphertext = new byte[paddedPlainText.length];
        byte[] temp = new byte[CBC.BLOCK_LENGTH];

        // get IV in temp
        Arrays.copy(paddedPlainText, 0, CBC.BLOCK_LENGTH, temp, 0);
        // then IV is prepended to the cipher text
        Arrays.copy(paddedPlainText, 0, CBC.BLOCK_LENGTH, ciphertext, 0);

        for ( int i=temp.length; i<paddedPlainText.length; i+=CBC.BLOCK_LENGTH ) {
            // here j=i/blockLength-1 (the block index)
            // temp = m[j-1] XOR m[j] ( if j==0: temp = IV XOR m[j] )
            Arrays.arrayXOR(paddedPlainText, i, temp, 0, CBC.BLOCK_LENGTH);
            // temp = E(key, m[j-1] XOR m[j]) = ciphertext[j]
            this.algorithm.cipher(temp);
            // copy encrypted block in ciphertext
            Arrays.copy(temp, 0, CBC.BLOCK_LENGTH, ciphertext, i);
        }

        return ciphertext;
    }

    @Override
    public void cipher( ByteInputStream input, ByteOutputStream output ) throws IOException {

        byte[] previousCipherBlock = new byte[16];
        byte[] currentPlainBlock = new byte[16];

        int currentNumberOfBytesRead = input.read(previousCipherBlock); // IV block // throws IOException
        // Send IV to output stream
        output.write(previousCipherBlock); // throws IOException

        while (currentNumberOfBytesRead == CBC.BLOCK_LENGTH) {

            // ***** Reading from inputStream *****
            currentNumberOfBytesRead = input.read(currentPlainBlock); // throws IOException
            // pad if this is the last block
            if (currentNumberOfBytesRead != 16) {
                if (currentNumberOfBytesRead < 0) {
                    currentNumberOfBytesRead = 0;
                }
                currentPlainBlock = this.padding.pad(Arrays.copy(currentPlainBlock, 0, currentNumberOfBytesRead));
            }

            // ***** ciphering currentCipherBlock: *****
            // previousCipherBlock = m[j-1] XOR m[j] ( if j==0: previousCipherBlock = IV XOR m[j] )
            Arrays.arrayXOR(currentPlainBlock, 0, previousCipherBlock, 0, previousCipherBlock.length);
            // previousCipherBlock = E(key, m[j-1] XOR m[j]) = ct[j]
            this.algorithm.cipher(previousCipherBlock);

            // ***** Writing to outputStream *****
            output.write(previousCipherBlock); // throws IOException
        }
    }

    /**
     *
     * @param ciphertext
     * @return the plaintext without the IV.
     * @throws InvalidPadException <B>WARNING:</B> to avoid padding <U>oracle
     * attacks</U>, do not let the user know the kind of exception thrown by
     * this method.
     */
    @Override
    public byte[] invCipher( byte[] ciphertext ) throws InvalidPadException {

        byte[] plainText = new byte[ciphertext.length-CBC.BLOCK_LENGTH];
        byte[] temp = new byte[CBC.BLOCK_LENGTH];

        for ( int i=16; i<ciphertext.length; i+=CBC.BLOCK_LENGTH ) {
            // temp = D(key, c[i])
            Arrays.copy(ciphertext, i, CBC.BLOCK_LENGTH, temp, 0);
            this.algorithm.invCipher(temp);
            // temp = c[i-1] XOR D(key, c[i])
            Arrays.arrayXOR(ciphertext, i-CBC.BLOCK_LENGTH, temp, 0, CBC.BLOCK_LENGTH);
            // plainText[i-1] = c[i-1] XOR D(key, c[i])
            Arrays.copy(temp, 0, CBC.BLOCK_LENGTH, plainText, i-CBC.BLOCK_LENGTH);
        }
        try {
            return this.padding.unPad(plainText);
        } catch ( InvalidPadException ipe ) {
            throw ipe;
        }
    }

    @Override
    public void invCipher( ByteInputStream input, ByteOutputStream output ) throws IOException, InvalidPadException {

        byte[] previousCipherBlock;
        byte[] currentCipherBlock = new byte[CBC.BLOCK_LENGTH];
        byte[] nextCipherBlock = new byte[CBC.BLOCK_LENGTH];
        byte[] temp;

        // read IV
        int currentNumberOfBytesRead = input.read(currentCipherBlock); // IV block // throws IOException

        if (currentNumberOfBytesRead == CBC.BLOCK_LENGTH) {
            currentNumberOfBytesRead = input.read(nextCipherBlock); // throws IOException

            while (currentNumberOfBytesRead == CBC.BLOCK_LENGTH) {

                // ***** preparing loop *****
                previousCipherBlock = currentCipherBlock;
                currentCipherBlock = Arrays.copy(nextCipherBlock);
                currentNumberOfBytesRead = input.read(nextCipherBlock); // throws IOException

                // ***** unciphering currentCipherBlock: *****
                temp = Arrays.copy(currentCipherBlock);
                this.algorithm.invCipher(temp);
                Arrays.arrayXOR(previousCipherBlock, 0, temp, 0, CBC.BLOCK_LENGTH);

                // unpad if last block
                if (currentNumberOfBytesRead != 16) {
                    temp = this.padding.unPad(temp); // throws InvalidPadException
                }

                // ***** Writing to outputStream *****
                output.write(temp); // throws IOException
            }
        }
    }

}
