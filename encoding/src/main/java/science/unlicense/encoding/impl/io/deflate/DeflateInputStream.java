
package science.unlicense.encoding.impl.io.deflate;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.collection.primitive.IntSequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.huffman.HuffmanTree;

/**
 * Deflate decompressor.
 * As defined in : http://www.ietf.org/rfc/rfc1951.txt
 *
 * @author Johann Sorel
 */
public class DeflateInputStream extends AbstractInputStream {


    private final DataInputStream in;
    private final RemainderBuffer buffer = new RemainderBuffer();
    private boolean finished = false;

    public DeflateInputStream(final ByteInputStream in) {
        this.in = new DataInputStream(in);
        this.in.setBitsDirection(DataInputStream.LSB);
    }

    @Override
    public int read() throws IOException {
        while (buffer.isEmpty() && !finished) {
            //block header
            // first bit       BFINAL
            // next 2 bits     BTYPE
            //indicate it's the last block
            finished = in.readBits(1,DataInputStream.LSB) != 0;
            final int btype = in.readBits(2,DataInputStream.LSB);

            if (btype == DeflateConstants.NO_COMPRESSION) {
                //skip remaining bits in byte
                in.skipToByteEnd();
                readUncompressed();
            } else if (btype == DeflateConstants.FIXED_HUFFMAN) {
                readFixedHuffman();
            } else if (btype == DeflateConstants.DYNAMIC_HUFFMAN) {
                readDynamicHuffman();
            } else {
                throw new IOException(this, "Compression type not supported : "+btype);
            }
        }

        if (buffer.isEmpty()) {
            //nothing left to decompress
            return -1;
        }

        int b = buffer.peek() & 0xff; //unsign it

        return b;
    }

    private void readUncompressed() throws IOException{
        final int len = in.readUShort(Endianness.LITTLE_ENDIAN);
        final int nlen = in.readUShort(Endianness.LITTLE_ENDIAN);
        for (int i=0;i<len;i++) {
            buffer.put(in.readByte());
        }
    }

    private void readFixedHuffman() throws IOException{
        readHuffman(DeflateConstants.FIX_HUFFMAN_LITERAL,DeflateConstants.FIX_HUFFMAN_DISTANCE);
    }

    private void readDynamicHuffman() throws IOException{

        //(HCLEN + 4) x 3 bits: code lengths for the code length
        //  alphabet given just above, in the order: 16, 17, 18,
        //  0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15
        //
        //  These code lengths are interpreted as 3-bit integers
        //  (0-7); as above, a code length of 0 means the
        //  corresponding symbol (literal/length or distance code
        //  length) is not used.
        //
        //HLIT + 257 code lengths for the literal/length alphabet,
        //  encoded using the code length Huffman code
        //
        //HDIST + 1 code lengths for the distance alphabet,
        //  encoded using the code length Huffman code
        final int hlit  = in.readBits(5,DataInputStream.LSB) + 257;
        final int hdist = in.readBits(5,DataInputStream.LSB) + 1;
        final int hclen = in.readBits(4,DataInputStream.LSB) + 4;

        final int[] keyLengths = new int[19];
        for (int i=0; i<hclen; i++) {
            final int keyIndex = HuffmanUtilities.DYNAMIC_KEY_LENGTH[i];
            keyLengths[keyIndex] = in.readBits(3,DataInputStream.LSB);
        }
        final HuffmanTree keyTree = HuffmanUtilities.buildHuffmanTree(keyLengths, 7);

        //The alphabet for code lengths is as follows:
        //0 - 15: Represent code lengths of 0 - 15
        //    16: Copy the previous code length 3 - 6 times.
        //       The next 2 bits indicate repeat length
        //             (0 = 3, ... , 3 = 6)
        //          Example:  Codes 8, 16 (+2 bits 11),
        //                    16 (+2 bits 10) will expand to
        //                    12 code lengths of 8 (1 + 6 + 5)
        //    17: Repeat a code length of 0 for 3 - 10 times.
        //       (3 bits of length)
        //    18: Repeat a code length of 0 for 11 - 138 times
        //       (7 bits of length)
        final int size = hlit + hdist;
        final IntSequence buffer = new IntSequence();
        int codeLength = -1;
        while (buffer.getSize() < size) {
            final int code = (Integer) keyTree.decode(in);
            if (code <= 15) {
                codeLength = code;
                buffer.put(code);
                continue;
            } else if (code == 16) {
                final int n = 3 + in.readBits(2,DataInputStream.LSB);
                for (int i=0;i<n;i++) buffer.put(codeLength);
            } else if (code == 17) {
                codeLength = 0;
                final int n = 3 + in.readBits(3,DataInputStream.LSB);
                for (int i=0;i<n;i++) buffer.put(codeLength);
            } else if (code == 18) {
                codeLength = 0;
                final int n = 11 + in.readBits(7,DataInputStream.LSB);
                for (int i=0;i<n;i++) buffer.put(codeLength);
            } else {
                throw new IOException(this, "Unexpected key code : "+code);
            }
        }

        final int[] litdistLengths = buffer.toArrayInt();
        final int[] literalLengths = new int[hlit];
        final int[] distanceLengths = new int[hdist];
        Arrays.copy(litdistLengths, 0, hlit, literalLengths, 0);
        Arrays.copy(litdistLengths, hlit, hdist, distanceLengths, 0);
        final HuffmanTree literalTree = HuffmanUtilities.buildHuffmanTree(literalLengths, 15);
        final HuffmanTree distanceTree = HuffmanUtilities.buildHuffmanTree(distanceLengths, 15);
        readHuffman(literalTree, distanceTree);
    }

    private void readHuffman(final HuffmanTree literalAphabet,
                             final HuffmanTree distanceAlphabet) throws IOException{
        //loop (until end of block code recognized)
        //decode literal/length value from input stream
        //   if value < 256
        //      copy value (literal byte) to output stream
        //   otherwise
        //      if value = end of block (256)
        //         break from loop
        //      otherwise (value = 257..285)
        //         decode distance from input stream
        //
        //         move backwards distance bytes in the output
        //         stream, and copy length bytes from this
        //         position to the output stream.
        //end loop

        //loop (until end of block code recognized)
        for (;;) {
            //decode literal/length value from input stream
            final int value = (Integer) literalAphabet.decode(in);
            if (value < 256) {
                // copy value (literal byte) to output stream
                buffer.put((byte) value);
            } else if (value == 256) {
                break;
            } else if (value >= 257 && value <=285) {
                // decode length
                final int lengthIndex = value-257;
                final int lengthExtrabit = HuffmanUtilities.LENGTH_EXTRABITS[lengthIndex];
                final int length = HuffmanUtilities.LENGTH_BASEVALUE[lengthIndex]
                        + in.readBits(lengthExtrabit,DataInputStream.LSB);

                // decode distance
                final int distanceIndex = (Integer) distanceAlphabet.decode(in);
                final int distanceExtrabit = HuffmanUtilities.DISTANCE_EXTRABITS[distanceIndex];
                final int distance = HuffmanUtilities.DISTANCE_BASEVALUE[distanceIndex]
                        + in.readBits(distanceExtrabit,DataInputStream.LSB);

                for (int i=0;i<length;i++) {
                    //we dont change the distance since it will be displaced when
                    //putting the byte at the end.
                    final byte b = buffer.remember(distance);
                    buffer.put(b);
                }
            }
        }

    }

    @Override
    public void dispose() throws IOException {
        in.dispose();
    }

    /**
     *
     * TODO,FIXME : memoty leak
     * TODO : merge with FrameBuffer
     */
    private class RemainderBuffer{

        private static final int WINDOW = 32768;

        private final ByteSequence buffer = new ByteSequence();
        private final byte[] memory = new byte[WINDOW*2];
        private int memoryOffset = 0;
        private int bufferOffset = 0;

        public void put(byte b) {
            buffer.put(b);

            if (memoryOffset == 65536) {
                Arrays.copy(memory, WINDOW, WINDOW, memory, 0);
                memoryOffset=WINDOW;
            }
            memory[memoryOffset] = b;
            memoryOffset++;
        }

        public byte remember(int distance) {
            return memory[memoryOffset-distance];
        }

        public boolean isEmpty() {
            return buffer.getSize()-bufferOffset <= 0;
        }

        public byte peek() {
            byte b = buffer.read(bufferOffset);
            bufferOffset++;
            return b;
        }

    }

}
