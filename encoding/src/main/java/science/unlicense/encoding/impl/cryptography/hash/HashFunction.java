

package science.unlicense.encoding.impl.cryptography.hash;

import science.unlicense.common.api.character.Chars;

/**
 * HashFunction are different from checksum in the way they offer low collisions.
 * TODO : merge with checksum ?
 *
 * Definition :
 * http://en.wikipedia.org/wiki/Cryptographic_hash_function
 *
 * @author Johann Sorel
 */
public interface HashFunction {

    /**
     * Hash function name.
     * @return Chars, never null
     */
    Chars getName();

    /**
     * Indicate if this hash function was created to match cryptographic requests.
     * example :
     * CRC-32, Adler-32 return false
     * SHA-1, MD5 return true;
     *
     * @return true is used for cryptographic purpose, false otherwise
     */
    boolean isCryptographic();

    /**
     * Expected result length in bits.
     * @return length in bits
     */
    int getResultLength();

    /**
     * Reinitialize hash function.
     */
    void reset();

    /**
     * Update hash with given byte.
     * @param b
     */
    void update(int b);

    /**
     * Update hash with given bytes.
     * @param buffer
     */
    void update(final byte[] buffer);

    /**
     * Update hash with given bytes.
     * @param buffer byte array
     * @param offset start offset in array
     * @param length number of element to use
     */
    void update(final byte[] buffer, int offset, int length);

    /**
     * Result as byte array.
     * Length is given by getResultLength()/8.
     *
     * @return byte array never null;
     */
    byte[] getResultBytes();

    /**
     * Result as number value.
     * This value can be truncated, use getResultBytes is length exceed 64bits.
     *
     * @return long value
     */
    long getResultLong();

}
