
package science.unlicense.encoding.impl.cryptography.hash;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class CRC64_ECMATest {

    @Test
    public void testCRC64() {

        final Chars str = new Chars("hello world this is the UN project");
        final byte[] bytes = str.toBytes(CharEncodings.US_ASCII);

        CRC64_ECMA checksum = new CRC64_ECMA();
        checksum.update(bytes);
        long value = checksum.getResultLong();
        Assert.assertEquals(-2716046290547542869l, value);
    }

}
