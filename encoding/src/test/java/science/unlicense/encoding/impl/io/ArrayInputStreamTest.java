package science.unlicense.encoding.impl.io;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * @author Johann Sorel
 */
public class ArrayInputStreamTest {

    @Test
    public void testRead() throws IOException{

        final byte[] data = new byte[]{1,2,3,4,5};

        final ArrayInputStream stream = new ArrayInputStream(data);

        Assert.assertEquals(1,stream.read());
        Assert.assertEquals(2,stream.read());
        Assert.assertEquals(3,stream.read());
        Assert.assertEquals(4,stream.read());
        Assert.assertEquals(5,stream.read());
        Assert.assertEquals(-1,stream.read());

        //should not raise any error
        stream.dispose();

    }

}
