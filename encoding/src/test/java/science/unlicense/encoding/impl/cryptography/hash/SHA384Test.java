package science.unlicense.encoding.impl.cryptography.hash;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;

/**
 *
 * @author Francois Berder
 *
 */
public class SHA384Test {

    @Test
    public void testSHA384() {
        final SHA384 s = new SHA384();

        // "abc"
        String z = "abc";
        s.reset();
        s.update((byte) 'a');
        s.update((byte) 'b');
        s.update((byte) 'c');
        Assert.assertEquals(new Chars("CB00753F45A35E8BB5A03D699AC65007272C32AB0EDED1631A8B605A43FF5BED8086072BA1E7CC2358BAECA134C825A7"), Int32.encodeHexa(s.getResultBytes()));

        // "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
        z = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
        s.reset();
        s.update(z.getBytes());
        Assert.assertEquals(new Chars("3391FDDDFC8DC7393707A65B1B4709397CF8B1D162AF05ABFE8F450DE5F36BC6B0455A8520BC4E6F5FE95B1FE3C8452B"), Int32.encodeHexa(s.getResultBytes()));

        // A million repetitions of "a"
        s.reset();
        for (int i = 0; i < 1000000; i++) {
            s.update((byte) 'a');
        }
        Assert.assertEquals(new Chars("9D0E1809716474CB086E834E310A4A1CED149E9C00F248527972CEC5704C2A5B07B8B3DC38ECC4EBAE97DDD87F3D8985"), Int32.encodeHexa(s.getResultBytes()));
    }

}
