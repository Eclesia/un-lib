
package science.unlicense.encoding.impl.io.ur;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class URLUtilitiesTest {

    @Test
    public void testUrlDecode() {
        //U+0F3B TIBETAN MARK GUG RTAGS GYAS
        //U+0FBF TIBETAN KU RU KHA BZHI MIG CAN
        final Chars encoded = new Chars("%E0%BC%BB%E0%BE%BF");
        final Chars decoded = URLUtilities.decode(encoded);
        final Chars expected = new Chars(new int[]{0x0F3B,0x0FBF});
        Assert.assertEquals(expected,decoded);
    }

    @Test
    public void testUrlEncode() {
        //U+0F3B TIBETAN MARK GUG RTAGS GYAS
        //U+0FBF TIBETAN KU RU KHA BZHI MIG CAN
        final Chars base = new Chars(new int[]{0x0F3B,0x0FBF});
        final Chars encoded = URLUtilities.encode(base);
        final Chars expected = new Chars("%E0%BC%BB%E0%BE%BF");
        Assert.assertEquals(expected,encoded);
    }

}
