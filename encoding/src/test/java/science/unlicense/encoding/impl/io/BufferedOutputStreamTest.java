
package science.unlicense.encoding.impl.io;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.BufferedOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class BufferedOutputStreamTest {

    @Test
    public void testWrite() throws IOException {

        final ArrayOutputStream out = new ArrayOutputStream();
        final BufferedOutputStream stream = new BufferedOutputStream(out, 4);

        stream.write((byte) 1);
        stream.write(new byte[]{2,3,4,5,6,7,8});
        stream.write((byte) 9);
        stream.write((byte) 10);
        stream.close();

        Assert.assertArrayEquals(new byte[]{1,2,3,4,5,6,7,8,9,10}, out.getBuffer().toArrayByte());


    }

}
