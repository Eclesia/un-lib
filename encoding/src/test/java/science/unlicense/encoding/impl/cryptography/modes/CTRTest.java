package science.unlicense.encoding.impl.cryptography.modes;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.Arrays;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.cryptography.algorithms.AES;
import science.unlicense.encoding.impl.cryptography.paddings.InvalidPadException;
import science.unlicense.encoding.impl.cryptography.paddings.NoPadding;

/**
 *
 * @author Bertrand COTE
 */
public class CTRTest {

    // ========== Helper functions ==========

    public static byte[] hexStringToByteArray( String hexString ) {
        String hexa = hexString.replaceAll("\\s", "");
        byte[] byteArray = new byte[hexa.length()/2];
        for ( int i=0; i<byteArray.length; i++ ) {
            String hex = hexa.substring(2*i, 2*i+2);
            int valInt = Integer.parseInt(hex, 16);
            byteArray[i] = (byte) valInt;
        }
        return byteArray;
    }

    // ======================================

    private static final String[][] datasCTR = new String[][]{
        //{ key, cipher },
        { // 3: CTR
            "36f18357be4dbd77f050515c73fcf9f2",  // key
            // plain text
            "69dda8455c7dd4254bf353b773304eec" + // IV
            "435452206d6f6465206c65747320796f" +
            "75206275696c6420612073747265616d" +
            "206369706865722066726f6d20612062" +
            "6c6f636b206369706865722e",
            // cipher text
            "69dda8455c7dd4254bf353b773304eec" + // IV
            "0ec7702330098ce7f7520d1cbbb20fc3" + // block 1
            "88d1b0adb5054dbd7370849dbf0b88d3" + // block 2
            "93f252e764f1f5f7ad97ef79d59ce29f" + // block 3
            "5f51eeca32eabedd9afa9329"           // block 3: 4 byte shorter
        },
        { // 4: CTR
            "36f18357be4dbd77f050515c73fcf9f2",  // key
            // plain text
            "770b80259ec33beb2561358a9f2dc617" + // IV
            "416c776179732061766f696420746865" +
            "2074776f2074696d652070616421",
            // cipher text
            "770b80259ec33beb2561358a9f2dc617" + // IV
            "e46218c0a53cbeca695ae45faa8952aa" + // block 1
            "0e311bde9d4e01726d3184c34451"       // block 2: 2 byte shorter
        }};

    /**
     * Test of cipher method, of class CTR.
     */
    @Test
    public void testCipher_byteArr() {
        System.out.println("cipher");
        for ( String[] data : datasCTR ) {

            byte[] key = hexStringToByteArray(data[0]);         // key
            byte[] pt = hexStringToByteArray(data[1]);          // IV + plainText
            byte[] expResult = hexStringToByteArray(data[2]);   // cipherText

            CTR instance = new CTR(new AES(key), new NoPadding() );
            byte[] result = instance.cipher(pt);

            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of invCipher method, of class CTR.
     */
    @Test
    public void testInvCipher_byteArr() {
        System.out.println("invCipher");
        for ( String[] data : datasCTR ) {

            byte[] key = hexStringToByteArray(data[0]);                 // key
            byte[] ct = hexStringToByteArray(data[2]);                  // cipherText
            byte[] expPlainText = hexStringToByteArray(data[1]);        // IV + plainText
            expPlainText = Arrays.copy(expPlainText, 16, expPlainText.length-16);   // plainText

            CTR instance = new CTR(new AES(key), new NoPadding() );
            byte[] result = null;
            try {
                result = instance.invCipher(ct);
            } catch (InvalidPadException ipe) {
                // TODO
                // No Exception thrown with the actual datas...
            }
            Assert.assertArrayEquals(expPlainText, result);
        }
    }

    /**
     * Test of cipher method, of class CTR.
     */
    @Test
    public void testCipher_ByteInputStream_ByteOutputStream() throws Exception {
        System.out.println("cipher");

        for ( String[] data : datasCTR ) {

            byte[] key = hexStringToByteArray(data[0]);         // key
            byte[] pt = hexStringToByteArray(data[1]);          // IV + plainText
            byte[] expResult = hexStringToByteArray(data[2]);   // cipherText
            CTR instance = new CTR(new AES(key), new NoPadding() );

            ByteInputStream input = new ArrayInputStream(pt);
            ArrayOutputStream output = new ArrayOutputStream();

            try {
                instance.cipher(input, output);
            } catch (IOException ioe) {
                // TODO
            } finally {
                try {
                    input.dispose();
                    output.close();
                } catch (IOException ioe) {
                    // TODO
                }
            }

            byte[] result = output.getBuffer().toArrayByte();
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of invCipher method, of class CTR.
     */
    @Test
    public void testInvCipher_ByteInputStream_ByteOutputStream() throws Exception {
        System.out.println("invCipher");

        for ( String[] data : datasCTR ) {

            byte[] key = hexStringToByteArray(data[0]);                 // key
            byte[] ct = hexStringToByteArray(data[2]);                  // cipherText
            byte[] expResult = hexStringToByteArray(data[1]);        // IV + plainText
            expResult = Arrays.copy(expResult, 16, expResult.length-16);   // plainText
            CTR instance = new CTR(new AES(key), new NoPadding() );

            ByteInputStream input = new ArrayInputStream(ct);
            ArrayOutputStream output = new ArrayOutputStream();

            byte[] result = null;
            try {
                instance.invCipher(input, output);
                result = output.getBuffer().toArrayByte();

            } catch (IOException ioe) {
                // TODO
            }catch (InvalidPadException ipe) {
                // TODO
            } finally {
                try {
                    input.dispose();
                    output.close();
                } catch (IOException ioe) {
                    // TODO
                }
            }
            Assert.assertArrayEquals(expResult, result);
        }
    }
}
