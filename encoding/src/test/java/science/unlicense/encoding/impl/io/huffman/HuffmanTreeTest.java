
package science.unlicense.encoding.impl.io.huffman;

import org.junit.Test;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.common.api.Assert.*;

/**
 *
 * @author Johann Sorel
 */
public class HuffmanTreeTest {

    @Test
    public void treeCreationTest() throws IOException {

        //test creation
        final HuffmanTree tree = new HuffmanTree(new HuffmanNode[]{
            new HuffmanNode(2, 0b001, "first"),
            new HuffmanNode(2, 0b011, "second"),
            new HuffmanNode(3, 0b010, "third"),
            new HuffmanNode(3, 0b110, "fourth")
        });

        HuffmanNode root = tree.getRoot();
        assertEquals(0, root.length);
        assertEquals(0b0, root.code);
            HuffmanNode n0 = root.next0;
            assertEquals(1, n0.length);
            assertEquals(0b0, n0.code);
                HuffmanNode n01 = n0.next1;
                assertEquals(2, n01.length);
                assertEquals(0b10, n01.code);
                    HuffmanNode n010 = n01.next0;
                    assertEquals(3, n010.length);
                    assertEquals(0b010, n010.code);
                    HuffmanNode n011 = n01.next1;
                    assertEquals(3, n011.length);
                    assertEquals(0b110, n011.code);

            HuffmanNode n1 = root.next1;
            assertEquals(1, n1.length);
            assertEquals(0b1, n1.code);
                HuffmanNode n10 = n1.next0;
                assertEquals(2, n10.length);
                assertEquals(0b01, n10.code);
                HuffmanNode n11 = n1.next1;
                assertEquals(2, n11.length);
                assertEquals(0b11, n11.code);


        //test decoding
        byte[] data = new byte[]{(byte) 0b11011010};
        final DataInputStream ds = new DataInputStream(new ArrayInputStream(data));
        ds.setBitsDirection(DataInputStream.LSB);

        assertEquals("third", tree.decode(ds));
        assertEquals("second", tree.decode(ds));
        assertEquals("fourth", tree.decode(ds));




    }

}
