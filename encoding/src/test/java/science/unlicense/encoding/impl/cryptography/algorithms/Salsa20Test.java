package science.unlicense.encoding.impl.cryptography.algorithms;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 *
 * @author bertrand
 */
public class Salsa20Test {

    private static final int [][] circularLeftShiftDataTest = new int[][] {
        // register, leftShift, expResult
        {   0xc0a8787e, 5, 0x150f0fd8 },
        {   0x81, 9, 0x10200 },
        {   0x10280, 13, 0x20500000 },
        {   0x20510200, 18, 0x08008144 }
    };

    private static final int[][][] quarterroundDataTest = new int[][][] {
        {
            {   // y
                0x00000000, 0x00000000, 0x00000000, 0x00000000
            },
            {   // z
                0x00000000, 0x00000000, 0x00000000, 0x00000000
            }
        },
        {
            {   // y
                0x00000001, 0x00000000, 0x00000000, 0x00000000
            },
            {   // z
                0x08008145, 0x00000080, 0x00010200, 0x20500000
            }
        },
        {
            {   // y
                0x00000000, 0x00000001, 0x00000000, 0x00000000
            },
            {   // z
                0x88000100, 0x00000001, 0x00000200, 0x00402000
            }
        },
        {
            {   // y
                0x00000000, 0x00000000, 0x00000001, 0x00000000
            },
            {   // z
                0x80040000, 0x00000000, 0x00000001, 0x00002000
            }
        },
        {
            {   // y
                0x00000000, 0x00000000, 0x00000000, 0x00000001
            },
            {   // z
                0x00048044, 0x00000080, 0x00010000, 0x20100001
            }
        },
        {
            {   // y
                0xe7e8c006, 0xc4f9417d, 0x6479b4b2, 0x68c67137
            },
            {   // z
                0xe876d72b, 0x9361dfd5, 0xf1460244, 0x948541a3
            }
        },
        {
            {   // y
                0xd3917c5b, 0x55f1c407, 0x52a58a7a, 0x8f887a3b
            },
            {   // z
                0x3e2f308c, 0xd90a8f36, 0x6ab2a923, 0x2883524c
            }
        }
    };

    private static final int[][][] rowRoundDataTest = new int[][][]{
        {
            {   // y
                0x00000001, 0x00000000, 0x00000000, 0x00000000,
                0x00000001, 0x00000000, 0x00000000, 0x00000000,
                0x00000001, 0x00000000, 0x00000000, 0x00000000,
                0x00000001, 0x00000000, 0x00000000, 0x00000000
            },
            {   // z
                0x08008145, 0x00000080, 0x00010200, 0x20500000,
                0x20100001, 0x00048044, 0x00000080, 0x00010000,
                0x00000001, 0x00002000, 0x80040000, 0x00000000,
                0x00000001, 0x00000200, 0x00402000, 0x88000100
            }
        },
        {
            {   // y
                0x08521bd6, 0x1fe88837, 0xbb2aa576, 0x3aa26365,
                0xc54c6a5b, 0x2fc74c2f, 0x6dd39cc3, 0xda0a64f6,
                0x90a2f23d, 0x067f95a6, 0x06b35f61, 0x41e4732e,
                0xe859c100, 0xea4d84b7, 0x0f619bff, 0xbc6e965a

            },
            {   // z
                0xa890d39d, 0x65d71596, 0xe9487daa, 0xc8ca6a86,
                0x949d2192, 0x764b7754, 0xe408d9b9, 0x7a41b4d1,
                0x3402e183, 0x3c3af432, 0x50669f96, 0xd89ef0a8,
                0x0040ede5, 0xb545fbce, 0xd257ed4f, 0x1818882d
            }
        }
    };

    private static final int[][][] columnRoundDataTest = new int[][][]{
        {
            {   // x
                0x00000001, 0x00000000, 0x00000000, 0x00000000,
                0x00000001, 0x00000000, 0x00000000, 0x00000000,
                0x00000001, 0x00000000, 0x00000000, 0x00000000,
                0x00000001, 0x00000000, 0x00000000, 0x00000000
            },
            {   // y
                0x10090288, 0x00000000, 0x00000000, 0x00000000,
                0x00000101, 0x00000000, 0x00000000, 0x00000000,
                0x00020401, 0x00000000, 0x00000000, 0x00000000,
                0x40a04001, 0x00000000, 0x00000000, 0x00000000
            }
        },
        {
            {   // x
                0x08521bd6, 0x1fe88837, 0xbb2aa576, 0x3aa26365,
                0xc54c6a5b, 0x2fc74c2f, 0x6dd39cc3, 0xda0a64f6,
                0x90a2f23d, 0x067f95a6, 0x06b35f61, 0x41e4732e,
                0xe859c100, 0xea4d84b7, 0x0f619bff, 0xbc6e965a
            },
            {   // y
                0x8c9d190a, 0xce8e4c90, 0x1ef8e9d3, 0x1326a71a,
                0x90a20123, 0xead3c4f3, 0x63a091a0, 0xf0708d69,
                0x789b010c, 0xd195a681, 0xeb7d5504, 0xa774135c,
                0x481c2027, 0x53a8e4b5, 0x4c1f89c5, 0x3f78c9c8
            }
        }
    };

    private static final int[][][] doubleRoundDataTest = new int[][][]{
        {
            {
                0x00000001, 0x00000000, 0x00000000, 0x00000000,
                0x00000000, 0x00000000, 0x00000000, 0x00000000,
                0x00000000, 0x00000000, 0x00000000, 0x00000000,
                0x00000000, 0x00000000, 0x00000000, 0x00000000
            },
            {
                0x8186a22d, 0x0040a284, 0x82479210, 0x06929051,
                0x08000090, 0x02402200, 0x00004000, 0x00800000,
                0x00010200, 0x20400000, 0x08008104, 0x00000000,
                0x20500000, 0xa0000040, 0x0008180a, 0x612a8020
            }
        },
        {
            {
                0xde501066, 0x6f9eb8f7, 0xe4fbbd9b, 0x454e3f57,
                0xb75540d3, 0x43e93a4c, 0x3a6f2aa0, 0x726d6b36,
                0x9243f484, 0x9145d1e8, 0x4fa9d247, 0xdc8dee11,
                0x054bf545, 0x254dd653, 0xd9421b6d, 0x67b276c1
            },
            {
                0xccaaf672, 0x23d960f7, 0x9153e63a, 0xcd9a60d0,
                0x50440492, 0xf07cad19, 0xae344aa0, 0xdf4cfdfc,
                0xca531c29, 0x8e7943db, 0xac1680cd, 0xd503ca00,
                0xa74b2ad6, 0xbc331c5c, 0x1dda24c7, 0xee928277
            }
        }
    };

    private static final byte[][] littleEndianInputDataTest = new byte[][] {
        {   (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00 },
        {   (byte) 0x56, (byte) 0x4b, (byte) 0x1e, (byte) 0x09 },
        {   (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xfa },
    };
    private static final int[] littleEndianOutputDataTest = new int[] {
        0x0,
        0x091e4b56,
        0xfaffffff,
    };

    private static final byte[][][] hashDataTest = new byte[][][]{
        {
            {   // x
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00
            },
            {   // z
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00
            }
        },
        {
            {   // x
                (byte) 211, (byte) 159, (byte) 13, (byte) 115, (byte) 76, (byte) 55, (byte) 82, (byte) 183, (byte) 3, (byte) 117, (byte) 222, (byte) 37, (byte) 191, (byte) 187, (byte) 234, (byte) 136,
                (byte) 49, (byte) 237, (byte) 179, (byte) 48, (byte) 1, (byte) 106, (byte) 178, (byte) 219, (byte) 175, (byte) 199, (byte) 166, (byte) 48, (byte) 86, (byte) 16, (byte) 179, (byte) 207,
                (byte) 31, (byte) 240, (byte) 32, (byte) 63, (byte) 15, (byte) 83, (byte) 93, (byte) 161, (byte) 116, (byte) 147, (byte) 48, (byte) 113, (byte) 238, (byte) 55, (byte) 204, (byte) 36,
                (byte) 79, (byte) 201, (byte) 235, (byte) 79, (byte) 3, (byte) 81, (byte) 156, (byte) 47, (byte) 203, (byte) 26, (byte) 244, (byte) 243, (byte) 88, (byte) 118, (byte) 104, (byte) 54

            },
            {   // z
                (byte) 109, (byte) 42, (byte) 178, (byte) 168, (byte) 156, (byte) 240, (byte) 248, (byte) 238, (byte) 168, (byte) 196, (byte) 190, (byte) 203, (byte) 26, (byte) 110, (byte) 170, (byte) 154,
                (byte) 29,(byte) 29, (byte) 150, (byte) 26, (byte) 150, (byte) 30, (byte) 235, (byte) 249, (byte) 190, (byte) 163, (byte) 251, (byte) 48, (byte) 69, (byte) 144, (byte) 51, (byte) 57,
                (byte) 118, (byte) 40, (byte) 152, (byte) 157, (byte) 180, (byte) 57, (byte) 27, (byte) 94, (byte) 107, (byte) 42, (byte) 236, (byte) 35, (byte) 27, (byte) 111, (byte) 114, (byte) 114,
                (byte) 219, (byte) 236, (byte) 232, (byte) 135, (byte) 111, (byte) 155, (byte) 110, (byte) 18, (byte) 24, (byte) 232, (byte) 95, (byte) 158, (byte) 179, (byte) 19, (byte) 48, (byte) 202

            }
        },
        {
            {   // x
                (byte) 88, (byte) 118, (byte) 104, (byte) 54, (byte) 79, (byte) 201, (byte) 235, (byte) 79, (byte) 3, (byte) 81, (byte) 156, (byte) 47, (byte) 203, (byte) 26, (byte) 244, (byte) 243,
                (byte) 191, (byte) 187, (byte) 234, (byte) 136, (byte) 211, (byte) 159, (byte) 13, (byte) 115, (byte) 76, (byte) 55, (byte) 82, (byte) 183, (byte) 3, (byte) 117, (byte) 222, (byte) 37,
                (byte) 86, (byte) 16, (byte) 179, (byte) 207, (byte) 49, (byte) 237, (byte) 179, (byte) 48, (byte) 1, (byte) 106, (byte) 178, (byte) 219, (byte) 175, (byte) 199, (byte) 166, (byte) 48,
                (byte) 238, (byte) 55, (byte) 204, (byte) 36, (byte) 31, (byte) 240, (byte) 32, (byte) 63,(byte) 15, (byte) 83, (byte) 93, (byte) 161, (byte) 116, (byte) 147, (byte) 48, (byte) 113

            },
            {   // z
                (byte) 179, (byte) 19, (byte) 48, (byte) 202, (byte) 219, (byte) 236, (byte) 232, (byte) 135, (byte) 111, (byte) 155, (byte) 110, (byte) 18, (byte) 24, (byte) 232, (byte) 95, (byte) 158,
                (byte) 26, (byte) 110, (byte) 170, (byte) 154, (byte) 109, (byte) 42, (byte) 178, (byte) 168, (byte) 156, (byte) 240, (byte) 248, (byte) 238, (byte) 168, (byte) 196, (byte) 190, (byte) 203,
                (byte) 69, (byte) 144, (byte) 51, (byte) 57, (byte) 29, (byte) 29, (byte) 150, (byte) 26, (byte) 150, (byte) 30, (byte) 235, (byte) 249, (byte) 190, (byte) 163, (byte) 251, (byte) 48,
                (byte) 27, (byte) 111, (byte) 114, (byte) 114, (byte) 118, (byte) 40, (byte) 152, (byte) 157, (byte) 180, (byte) 57, (byte) 27, (byte) 94, (byte) 107, (byte) 42, (byte) 236, (byte) 35

            }
        },
    };

    private static final byte[][][] expansionDataTest = new byte[][][]{
        {
            {   // key ( if 16 bytes: k, if 32 bytes: k0, k1 )
                (byte)  1, (byte)  2, (byte)  3, (byte)  4, (byte)  5, (byte)  6, (byte)  7, (byte)  8,
                (byte)  9, (byte) 10, (byte) 11, (byte) 12, (byte) 13, (byte) 14, (byte) 15, (byte) 16,
                (byte) 201, (byte) 202, (byte) 203, (byte) 204, (byte) 205, (byte) 206, (byte) 207, (byte) 208,
                (byte) 209, (byte) 210, (byte) 211, (byte) 212, (byte) 213, (byte) 214, (byte) 215, (byte) 216,
            },
            {   // n
                (byte) 101, (byte) 102, (byte) 103, (byte) 104, (byte) 105, (byte) 106, (byte) 107, (byte) 108,
                (byte) 109, (byte) 110, (byte) 111, (byte) 112, (byte) 113, (byte) 114, (byte) 115, (byte) 116,
            },
            {   // expResult
                (byte) 101, (byte) 120, (byte) 112, (byte) 97, (byte)  1, (byte)  2, (byte)  3, (byte)  4,
                (byte)  5, (byte)  6, (byte)  7, (byte)  8, (byte)  9, (byte) 10, (byte) 11, (byte) 12,
                (byte) 13, (byte) 14, (byte) 15, (byte) 16, (byte) 110, (byte) 100, (byte) 32, (byte) 51,
                (byte) 101, (byte) 102, (byte) 103, (byte) 104, (byte) 105, (byte) 106, (byte) 107, (byte) 108,
                (byte) 109, (byte) 110, (byte) 111, (byte) 112, (byte) 113, (byte) 114, (byte) 115, (byte) 116,
                (byte) 50, (byte) 45, (byte) 98, (byte) 121, (byte) 201, (byte) 202, (byte) 203, (byte) 204,
                (byte) 205, (byte) 206, (byte) 207, (byte) 208, (byte) 209, (byte) 210, (byte) 211, (byte) 212,
                (byte) 213, (byte) 214, (byte) 215, (byte) 216, (byte) 116, (byte) 101, (byte) 32, (byte) 107
            }
        },
        {
            {   // key ( if 16 bytes: k, if 32 bytes: k0, k1 )
                (byte)  1, (byte)  2, (byte)  3, (byte)  4, (byte)  5, (byte)  6, (byte)  7, (byte)  8,
                (byte)  9, (byte) 10, (byte) 11, (byte) 12, (byte) 13, (byte) 14, (byte) 15, (byte) 16,
            },
            {   // n
                (byte) 101, (byte) 102, (byte) 103, (byte) 104, (byte) 105, (byte) 106, (byte) 107, (byte) 108,
                (byte) 109, (byte) 110, (byte) 111, (byte) 112, (byte) 113, (byte) 114, (byte) 115, (byte) 116,
            },
            {   // expResult
                (byte) 101, (byte) 120, (byte) 112, (byte) 97, (byte)  1, (byte)  2, (byte)  3, (byte)  4,
                (byte)  5, (byte)  6, (byte)  7, (byte)  8, (byte)  9, (byte) 10, (byte) 11, (byte) 12,
                (byte) 13, (byte) 14, (byte) 15, (byte) 16, (byte) 110, (byte) 100, (byte) 32, (byte) 49,
                (byte) 101, (byte) 102, (byte) 103, (byte) 104, (byte) 105, (byte) 106, (byte) 107, (byte) 108,
                (byte) 109, (byte) 110, (byte) 111, (byte) 112, (byte) 113, (byte) 114, (byte) 115, (byte) 116,
                (byte) 54, (byte) 45, (byte) 98, (byte) 121, (byte)  1, (byte)  2, (byte)  3, (byte)  4,
                (byte)  5, (byte)  6, (byte)  7, (byte)  8, (byte)  9, (byte) 10, (byte) 11, (byte) 12,
                (byte) 13, (byte) 14, (byte) 15, (byte) 16, (byte) 116, (byte) 101, (byte) 32, (byte) 107
            }
        }
    };

    /**
     * Test of quarterRound method, of class Salsa20.
     */
    @Test
    public void testQuarterRound() {
        System.out.println("quarterround");

        int cpt = 0;

        for ( int[][] data : quarterroundDataTest ) {
            System.out.println("" + cpt++);
            int[] y = data[0];
            int[] expResult = data[1];
            int[] result = Salsa20.quarterRound(y);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of cipher method, of class Salsa20.
     */
    @Test
    public void testCipher() {
        System.out.println("cipher");
//        byte[] input = null;
//        Salsa20 instance = new Salsa20();
//        byte[] expResult = null;
//        byte[] result = instance.cipher(input);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of invCipher method, of class Salsa20.
     */
    @Test
    public void testInvCipher() {
        System.out.println("invCipher");
//        byte[] input = null;
//        Salsa20 instance = new Salsa20();
//        byte[] expResult = null;
//        byte[] result = instance.invCipher(input);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of getNewCipher method, of class Salsa20.
     */
    @Test
    public void testGetNewCipher() {
        System.out.println("getNewCipher");
    }

    /**
     * Test of circularLeftShift method, of class Salsa20.
     */
    @Test
    public void testCircularLeftShift() {
        System.out.println("circularLeftShift");
        for ( int[] data : circularLeftShiftDataTest ) {
            int register = data[0];
            int rightShift = data[1];
            int expResult = data[2];
            int result = Salsa20.circularLeftShift(register, rightShift);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of rowRound method, of class Salsa20.
     */
    @Test
    public void testRowRound() {
        System.out.println("rowRound");
        for ( int[][] data : rowRoundDataTest ) {
            int[] y = data[0];
            int[] expResult = data[1];
            int[] result = Salsa20.rowRound(y);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of columnRound method, of class Salsa20.
     */
    @Test
    public void testColumnRound() {
        System.out.println("columnRound");
        for ( int[][] data : columnRoundDataTest ) {
            int[] x = data[0];
            int[] expResult = data[1];
            int[] result = Salsa20.columnRound(x);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of doubleRound method, of class Salsa20.
     */
    @Test
    public void testDoubleRound() {
        System.out.println("doubleRound");
        for ( int[][] data : doubleRoundDataTest ) {
            int[] x = data[0];
            int[] expResult = data[1];
            int[] result = Salsa20.doubleRound(x);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of littleEndian method, of class Salsa20.
     */
    @Test
    public void testLittleEndian() {
        System.out.println("littleEndian");
        for ( int i=0; i<littleEndianInputDataTest.length; i++ ) {
            byte[] b = littleEndianInputDataTest[i];
            int expResult = littleEndianOutputDataTest[i];
            int result = Salsa20.littleEndian(b);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of invLittleEndian method, of class Salsa20.
     */
    @Test
    public void testInvLittleEndian() {
        System.out.println("invLittleEndian");
        for ( int i=0; i<littleEndianInputDataTest.length; i++ ) {
            int w = littleEndianOutputDataTest[i];
            byte[] expResult = littleEndianInputDataTest[i];
            byte[] result = Salsa20.invLittleEndian(w);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of hash method, of class Salsa20.
     */
    @Test
    public void testHash() {
        System.out.println("hash");
        for ( byte[][] data : hashDataTest ) {
            byte[] x = data[0];
            byte[] expResult = data[1];
            byte[] result = Salsa20.hash(x);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of expansion method, of class Salsa20.
     */
    @Test
    public void testExpansion() {
        System.out.println("expansion");
        for ( byte[][] data : expansionDataTest ) {
            byte[] k = data[0];
            byte[] n = data[1];
            byte[] expResult = data[2];
            byte[] result = Salsa20.expansion(k, n);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    private static final byte[][][] encryptionDataTest = new byte[][][]{
        {
            {   // key
                (byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x04, (byte) 0x05, (byte) 0x06, (byte) 0x07, (byte) 0x08,
                (byte) 0x09, (byte) 0x0a, (byte) 0x0b, (byte) 0x0c, (byte) 0x0d, (byte) 0x0e, (byte) 0x0f, (byte) 0x10,
                (byte) 0x11, (byte) 0x12, (byte) 0x13, (byte) 0x14, (byte) 0x15, (byte) 0x16, (byte) 0x17, (byte) 0x18,
                (byte) 0x19, (byte) 0x1a, (byte) 0x1b, (byte) 0x1c, (byte) 0x1d, (byte) 0x1e, (byte) 0x1f, (byte) 0x20
            },
            {   // v
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00
            },
            {   // m
                (byte) 0x61, (byte) 0x70, (byte) 0x78, (byte) 0x65, (byte) 0x04, (byte) 0x03, (byte) 0x02, (byte) 0x01,
                (byte) 0x08, (byte) 0x07, (byte) 0x06, (byte) 0x05, (byte) 0x0c, (byte) 0x0b, (byte) 0x0a, (byte) 0x09,
                (byte) 0x10, (byte) 0x0f, (byte) 0x0e, (byte) 0x0d, (byte) 0x33, (byte) 0x20, (byte) 0x64, (byte) 0x6e,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x79, (byte) 0x62, (byte) 0x2d, (byte) 0x32, (byte) 0x14, (byte) 0x13, (byte) 0x12, (byte) 0x11,
                (byte) 0x18, (byte) 0x17, (byte) 0x16, (byte) 0x15, (byte) 0x1c, (byte) 0x1b, (byte) 0x1a, (byte) 0x19,
                (byte) 0x20, (byte) 0x1f, (byte) 0x1e, (byte) 0x1d, (byte) 0x6b, (byte) 0x20, (byte) 0x65, (byte) 0x74
            },
            {   // ct
                (byte) 0x0b, (byte) 0x9e, (byte) 0x28, (byte) 0x77, (byte) 0xda, (byte) 0xf0, (byte) 0x6c, (byte) 0xa2,
                (byte) 0x5b, (byte) 0x70, (byte) 0x0d, (byte) 0x25, (byte) 0xdb, (byte) 0xc3, (byte) 0x95, (byte) 0x05,
                (byte) 0x79, (byte) 0xb7, (byte) 0xaf, (byte) 0xe1, (byte) 0x21, (byte) 0x4f, (byte) 0xab, (byte) 0x40,
                (byte) 0x77, (byte) 0xa4, (byte) 0x7a, (byte) 0x7d, (byte) 0x36, (byte) 0x9c, (byte) 0xd5, (byte) 0x6b,
                (byte) 0xe8, (byte) 0x3a, (byte) 0x3e, (byte) 0x0e, (byte) 0x63, (byte) 0x20, (byte) 0xd7, (byte) 0x4c,
                (byte) 0x3e, (byte) 0xe9, (byte) 0x8f, (byte) 0x99, (byte) 0xc7, (byte) 0xec, (byte) 0x07, (byte) 0x6c,
                (byte) 0x2f, (byte) 0x12, (byte) 0x76, (byte) 0x6d, (byte) 0x71, (byte) 0x79, (byte) 0xc0, (byte) 0xcb,
                (byte) 0xd3, (byte) 0x5a, (byte) 0x05, (byte) 0x18, (byte) 0x7b, (byte) 0xc8, (byte) 0x16, (byte) 0x6d
            }
        }
    };

    /**
     * Test of encryption method, of class Salsa20.
     */
    @Test
    @Ignore
    public void testSkipEncryption() {
        System.out.println("encryption");
        for ( byte[][] data : encryptionDataTest ) {
            byte[] k = data[0];
            byte[] v = data[1];
            byte[] m = data[2];
            byte[] expResult = data[3];
            byte[] result = Salsa20.encryption(k, v, m);
            Assert.assertArrayEquals(expResult, result);
        }
    }

}
