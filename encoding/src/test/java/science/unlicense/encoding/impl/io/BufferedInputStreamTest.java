
package science.unlicense.encoding.impl.io;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.BufferedInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class BufferedInputStreamTest {

    @Test
    public void testRead() throws IOException{

        final byte[] data = new byte[]{0,1,2,3,4,5,6,7,8,9};

        final BufferedInputStream in = new BufferedInputStream(new ArrayInputStream(data),4);
        Assert.assertEquals(0, in.bufferRemaining());
        Assert.assertEquals(0, in.read());
        Assert.assertEquals(3, in.bufferRemaining());
        Assert.assertEquals(1, in.read());
        Assert.assertEquals(2, in.bufferRemaining());
        Assert.assertEquals(2, in.read());
        Assert.assertEquals(1, in.bufferRemaining());
        Assert.assertEquals(3, in.read());
        Assert.assertEquals(0, in.bufferRemaining());
        Assert.assertEquals(4, in.read());
        Assert.assertEquals(3, in.bufferRemaining());

        Assert.assertEquals(2, in.skip(2));
        Assert.assertEquals(7, in.read());
        Assert.assertEquals(0, in.bufferRemaining());

        byte[] buffer = new byte[30];
        int nb = in.read(buffer, 0, 30);
        Assert.assertEquals(2, nb);
        Assert.assertEquals(8, buffer[0]);
        Assert.assertEquals(9, buffer[1]);
        Assert.assertEquals(0, in.bufferRemaining());

        Assert.assertEquals(-1, in.read());


    }

}
