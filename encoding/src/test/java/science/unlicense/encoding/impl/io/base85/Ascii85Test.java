
package science.unlicense.encoding.impl.io.base85;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class Ascii85Test {

    @Test
    public void testEncode() {
        final Chars base85 = new Chars("<~87cURD]iJ0@qfX\"GAhM<A0C~>");
        final byte[] text = "Hello ascii world!".getBytes();

        //assertArrayEquals(text, Base85.decode(base85));
        Assert.assertEquals(base85, Ascii85.encodeBytes(text));


    }

    @Test
    public void testDecode() {
        final Chars base85 = new Chars("<~87cURD]iJ0@qfX\"GAhM<A0C~>");
        final byte[] text = "Hello ascii world!".getBytes();

        final byte[] res = Ascii85.decode(base85);
        Assert.assertArrayEquals(text, res);

    }


}
