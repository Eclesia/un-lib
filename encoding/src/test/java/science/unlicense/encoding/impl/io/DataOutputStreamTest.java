
package science.unlicense.encoding.impl.io;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class DataOutputStreamTest {

    private static final double DELTA = 0.00000001;

    @Test
    public void testWriting() throws IOException{

        final ByteSequence buffer = new ByteSequence();
        final ArrayOutputStream outstream = new ArrayOutputStream(buffer);
        final DataOutputStream ods = new DataOutputStream(outstream);

        ods.writeByte((byte) 86);
        ods.writeUByte(216);
        ods.writeShort((short)'k');
        ods.writeShort((short) 721);
        ods.writeUShort((short) 1056);
        ods.writeInt(-894);
        ods.writeUInt(3815);
        ods.writeLong(7756);
        ods.writeFloat(-3.14f);
        ods.writeDouble(6.28);
        ods.writeVarLengthUInt(456798123);

        final ArrayInputStream intstream = new ArrayInputStream(buffer.getBackArray());
        final DataInputStream ids = new DataInputStream(intstream);
        Assert.assertEquals(86, ids.readByte());
        Assert.assertEquals(216, ids.readUByte());
        Assert.assertEquals('k', ids.readShort());
        Assert.assertEquals(721, ids.readShort());
        Assert.assertEquals(1056, ids.readUShort());
        Assert.assertEquals(-894, ids.readInt());
        Assert.assertEquals(3815, ids.readUInt());
        Assert.assertEquals(7756, ids.readLong());
        Assert.assertEquals(-3.14f, ids.readFloat(),DELTA);
        Assert.assertEquals(6.28, ids.readDouble(),DELTA);
        Assert.assertEquals(456798123l, ids.readVarLengthUInt());

    }

    @Test
    public void testBitWriting() throws IOException{

        ArrayOutputStream stream;
        DataOutputStream ds;
        byte[] expected;

        //simple case, bit writing on a single byte
        stream = new ArrayOutputStream();
        ds = new DataOutputStream(stream);
        ds.writeByte((byte) 17);
        ds.writeByte((byte)-13);
        ds.writeBit(1);
        ds.writeBits(3,2);
        ds.writeBits(7,3);
        ds.writeBits(3,2);
        ds.writeByte((byte) 21);

        expected = new byte[]{(byte) 17,(byte)-13,(byte) 0xFF,(byte) 21};
        Assert.assertArrayEquals(expected,stream.getBuffer().toArrayByte());

        //writing bits overlapping bytes
        // 1  2   3        9    13         42          57          197           183           173   7
        // 1 10 011 10    01 01101 1    01010 011    1001 1100    0101 0101    10111 101     01101 111
        expected = new byte[]{
            (byte) 0xCE,   (byte) 0x5B,  (byte) 0x53,  (byte) 0x9C,  (byte) 0x55,  (byte) 0xBD,   (byte) 0x6F
        };
        stream = new ArrayOutputStream();
        ds = new DataOutputStream(stream);

        ds.writeBits(1,     1);
        ds.writeBits(2,     2);
        ds.writeBits(3,     3);
        ds.writeBits(9,     4);
        ds.writeBits(13,    5);
        ds.writeBits(42,    6);
        ds.writeBits(57,    7);
        ds.writeBits(197,   8);
        ds.writeBits(183,   9);
        ds.writeByte((byte) 173);
        ds.writeBits(7,     3);

        final byte[] res = stream.getBuffer().toArrayByte();
        Assert.assertEquals(expected.length, res.length);
        Assert.assertEquals(expected[0], res[0]);
        Assert.assertEquals(expected[1], res[1]);
        Assert.assertEquals(expected[2], res[2]);
        Assert.assertEquals(expected[3], res[3]);
        Assert.assertEquals(expected[4], res[4]);
        Assert.assertEquals(expected[5], res[5]);
        Assert.assertEquals(expected[6], res[6]);

    }

    @Test
    public void testVarLengthWrite() throws IOException {
        final ArrayOutputStream stream = new ArrayOutputStream();
        final DataOutputStream ds = new DataOutputStream(stream);
        ds.writeVarLengthInt(-624485l);

        final byte[] expected = new byte[]{(byte) 0x9B,(byte) 0xF1,(byte) 0x59};
        final byte[] res = stream.getBuffer().toArrayByte();
        Assert.assertArrayEquals(expected, res);

    }

    @Test
    public void testUvarLengthWrite() throws IOException {
        ArrayOutputStream stream = new ArrayOutputStream();
        DataOutputStream ds = new DataOutputStream(stream);
        ds.writeVarLengthUInt(624485l);
        byte[] expected = new byte[]{(byte) 0xE5,(byte) 0x8E,(byte) 0x26};
        byte[] res = stream.getBuffer().toArrayByte();
        Assert.assertArrayEquals(expected, res);

        stream = new ArrayOutputStream();
        ds = new DataOutputStream(stream);
        ds.writeVarLengthUInt(0x3ffffffffffl);
        expected = new byte[]{(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,(byte) 0x7F};
        res = stream.getBuffer().toArrayByte();
        Assert.assertArrayEquals(expected, res);

    }

}
