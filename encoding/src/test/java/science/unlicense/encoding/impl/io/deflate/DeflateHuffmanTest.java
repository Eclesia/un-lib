
package science.unlicense.encoding.impl.io.deflate;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.impl.io.JVMInputStream;
import science.unlicense.encoding.impl.io.huffman.HuffmanTree;
import science.unlicense.encoding.impl.io.zlib.ZlibInputStream;

/**
 *
 * @author Johann Sorel
 */
public class DeflateHuffmanTest {

    private static final String TEXT =
            "This is free and unencumbered software released into the public domain."+
            ""+
            "Anyone is free to copy, modify, publish, use, compile, sell, or"+
            "distribute this software, either in source code form or as a compiled"+
            "binary, for any purpose, commercial or non-commercial, and by any"+
            "means."+
            ""+
            "In jurisdictions that recognize copyright laws, the author or authors"+
            "of this software dedicate any and all copyright interest in the"+
            "software to the public domain. We make this dedication for the benefit"+
            "of the public at large and to the detriment of our heirs and"+
            "successors. We intend this dedication to be an overt act of"+
            "relinquishment in perpetuity of all present and future rights to this"+
            "software under copyright law."+
            ""+
            "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,"+
            "EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF"+
            "MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT."+
            "IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR"+
            "OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,"+
            "ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR"+
            "OTHER DEALINGS IN THE SOFTWARE."+
            ""+
            "For more information, please refer to <http://unlicense.org/>";

    /**
     * this test is extract from RFC1951 example
     */
    @Test
    public void testBuildTree()  {

        final int[] lengths = new int[]{3, 3, 3, 3, 3, 2, 4, 4};
        final int maxSize = 4;

        final HuffmanTree tree = HuffmanUtilities.buildHuffmanTree(lengths, maxSize);

        //Symbol Length   Code
        //------ ------   ----
        //A       3        010
        //B       3        011
        //C       3        100
        //D       3        101
        //E       3        110
        //F       2        00
        //G       4        1110
        //H       4        1111
        Assert.assertEquals(8, tree.nodes.length);
        Assert.assertEquals(3, tree.nodes[0].length); Assert.assertEquals(0x2, tree.nodes[0].code);
        Assert.assertEquals(3, tree.nodes[1].length); Assert.assertEquals(0x6, tree.nodes[1].code);
        Assert.assertEquals(3, tree.nodes[2].length); Assert.assertEquals(0x1, tree.nodes[2].code);
        Assert.assertEquals(3, tree.nodes[3].length); Assert.assertEquals(0x5, tree.nodes[3].code);
        Assert.assertEquals(3, tree.nodes[4].length); Assert.assertEquals(0x3, tree.nodes[4].code);
        Assert.assertEquals(2, tree.nodes[5].length); Assert.assertEquals(0x0, tree.nodes[5].code);
        Assert.assertEquals(4, tree.nodes[6].length); Assert.assertEquals(0x7, tree.nodes[6].code);
        Assert.assertEquals(4, tree.nodes[7].length); Assert.assertEquals(0xF, tree.nodes[7].code);

    }

    @Test
    public void testStaticHuffmanTree()  {

        final HuffmanTree tree = HuffmanUtilities.buildFixedHuffmanLiteralTree();

        //Lit Value    Bits        Codes
        //---------    ----        -----
        // 0 - 143      8           00110000 through  10111111
        //144 - 255     9          110010000 through 111111111
        //256 - 279     7            0000000 through   0010111
        //280 - 287     8           11000000 through  11000111
        Assert.assertEquals(8, tree.nodes[0].length);   Assert.assertEquals(0x0C,  tree.nodes[0].code);
        Assert.assertEquals(8, tree.nodes[143].length); Assert.assertEquals(0xFD, tree.nodes[143].code);
        Assert.assertEquals(9, tree.nodes[144].length); Assert.assertEquals(0x13, tree.nodes[144].code);
        Assert.assertEquals(9, tree.nodes[255].length); Assert.assertEquals(0x1FF, tree.nodes[255].code);
        Assert.assertEquals(7, tree.nodes[256].length); Assert.assertEquals(0x00,   tree.nodes[256].code);
        Assert.assertEquals(7, tree.nodes[279].length); Assert.assertEquals(0x74,  tree.nodes[279].code);
        Assert.assertEquals(8, tree.nodes[280].length); Assert.assertEquals(0x03, tree.nodes[280].code);
        Assert.assertEquals(8, tree.nodes[287].length); Assert.assertEquals(0xE3, tree.nodes[287].code);

    }

    @Test
    public void testDecodeUncompressed() throws Exception  {

        final ZlibInputStream in = new ZlibInputStream(
                new JVMInputStream(
                DeflateHuffmanTest.class.getResourceAsStream("/science/unlicense/impl/cryptography/io/deflate/uncompressedText.txt")));

        final ArrayOutputStream out = new ArrayOutputStream();
        for (int b=in.read();b>=0;b=in.read()) {
            out.write((byte) b);
        }

        final byte[] uncompressed = out.getBuffer().toArrayByte();
        final String str = new String(uncompressed);
        Assert.assertEquals(TEXT, str);
    }

    @Test
    public void testDecodeFixed() throws Exception  {

        final ZlibInputStream in = new ZlibInputStream(
                new JVMInputStream(
                DeflateHuffmanTest.class.getResourceAsStream("/science/unlicense/impl/cryptography/io/deflate/compressedFixedText.txt")));

        final ArrayOutputStream out = new ArrayOutputStream();
        for (int b=in.read();b>=0;b=in.read()) {
            out.write((byte) b);
        }

        final byte[] uncompressed = out.getBuffer().toArrayByte();
        final String str = new String(uncompressed);
        Assert.assertEquals(TEXT, str);
    }

    @Test
    public void testDecodeDynamic() throws Exception  {

        final ZlibInputStream in = new ZlibInputStream(
                new JVMInputStream(
                DeflateHuffmanTest.class.getResourceAsStream("/science/unlicense/impl/cryptography/io/deflate/compressedDynamicText.txt")));

        final ArrayOutputStream out = new ArrayOutputStream();
        for (int b=in.read();b>=0;b=in.read()) {
            out.write((byte) b);
        }

        final byte[] uncompressed = out.getBuffer().toArrayByte();
        final String str = new String(uncompressed);
        Assert.assertEquals(TEXT, str);
    }

}
