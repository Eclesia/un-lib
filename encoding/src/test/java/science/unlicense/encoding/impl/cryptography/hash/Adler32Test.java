
package science.unlicense.encoding.impl.cryptography.hash;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class Adler32Test {

    @Test
    public void testAdler32() {

        final Chars str = new Chars("hello world this is the UN project");
        final byte[] bytes = str.toBytes(CharEncodings.US_ASCII);

        Adler32 checksum = new Adler32();
        checksum.update(bytes);
        long value = checksum.getResultLong() & 0xffffffffL;
        Assert.assertEquals(3683650668l, value);
    }

}
