package science.unlicense.encoding.impl.io;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * @author Johann Sorel
 */
public class CharInputStreamTest {

    @Test
    public void testReadChar() throws IOException{

        final byte[] data = new byte[]{1,2,3,4,5};
        final ArrayInputStream sub = new ArrayInputStream(data);
        final CharInputStream stream = new CharInputStream(sub, CharEncodings.US_ASCII);

        Char c = stream.readChar();
        Assert.assertArrayEquals(new byte[]{1},c.toBytes());
        Assert.assertEquals(CharEncodings.US_ASCII, c.getEncoding());
        c = stream.readChar();
        Assert.assertArrayEquals(new byte[]{2},c.toBytes());
        Assert.assertEquals(CharEncodings.US_ASCII,c.getEncoding());
        c = stream.readChar();
        Assert.assertArrayEquals(new byte[]{3},c.toBytes());
        Assert.assertEquals(CharEncodings.US_ASCII,c.getEncoding());
        c = stream.readChar();
        Assert.assertArrayEquals(new byte[]{4},c.toBytes());
        Assert.assertEquals(CharEncodings.US_ASCII,c.getEncoding());
        c = stream.readChar();
        Assert.assertArrayEquals(new byte[]{5},c.toBytes());
        Assert.assertEquals(CharEncodings.US_ASCII,c.getEncoding());

        c = stream.readChar();
        Assert.assertNull(c);

        //should not raise any error
        stream.dispose();

    }

}
