
package science.unlicense.encoding.impl.io.base64;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class Base64Test {

    @Test
    public void testBase64ReadWrite() throws IOException {
        final Chars base64 = new Chars("aGVsbG8gd29ybGQsIHRvZGF5IGlzIDIwIHNlcHRlbWJlciAxOTY4Lg==");
        final byte[] text = "hello world, today is 20 september 1968.".getBytes();

        Assert.assertArrayEquals(text, Base64.decode(base64));
        Assert.assertEquals(base64, Base64.encodeBytes(text));

    }

}
