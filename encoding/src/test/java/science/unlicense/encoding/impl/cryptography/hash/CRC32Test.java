
package science.unlicense.encoding.impl.cryptography.hash;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class CRC32Test {

    @Test
    public void testCRC32() {

        final Chars str = new Chars("hello world this is the UN project");
        final byte[] bytes = str.toBytes(CharEncodings.US_ASCII);

        CRC32 checksum = new CRC32();
        checksum.update(bytes);
        long value = checksum.getResultLong() & 0xffffffffL;
        Assert.assertEquals(2185167914l, value);
    }

}
