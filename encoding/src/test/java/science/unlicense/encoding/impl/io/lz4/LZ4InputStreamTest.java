
package science.unlicense.encoding.impl.io.lz4;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.impl.io.JVMInputStream;

/**
 *
 * @author Johann Sorel
 */
public class LZ4InputStreamTest {

    @Test
    public void testUncompressedStream() throws IOException {

        JVMInputStream stream = new JVMInputStream(LZ4InputStream.class.getResourceAsStream("/science/unlicense/encoding/impl/io/lz4/notCompressed.txt.lz4"));

        LZ4InputStream in = new LZ4InputStream(stream);
        byte[] all = IOUtilities.readAll(in);

        Assert.assertEquals(new Chars("ABCD 1234"), new Chars(all));

    }

    @Test
    public void testCompressedStream() throws IOException {

        JVMInputStream stream = new JVMInputStream(LZ4InputStream.class.getResourceAsStream("/science/unlicense/encoding/impl/io/lz4/compressed.txt.lz4"));

        LZ4InputStream in = new LZ4InputStream(stream);
        byte[] all = IOUtilities.readAll(in);

        Assert.assertEquals(new Chars("AAAAAAAAAABBBBBBCCCCCCCCCCDDDDDD101010101010"), new Chars(all));

    }
}
