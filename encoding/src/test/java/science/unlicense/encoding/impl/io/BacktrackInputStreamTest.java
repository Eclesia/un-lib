
package science.unlicense.encoding.impl.io;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Tests for BacktrackInputStream.
 * 
 * @author Johann Sorel
 */
public class BacktrackInputStreamTest {

    @Test
    public void testNoMark() throws IOException {

        final ArrayInputStream in = new ArrayInputStream(new byte[]{0,1,2,3,4,5,6,7,8,9,10});
        final BacktrackInputStream bs = new BacktrackInputStream(in);

        Assert.assertEquals(0,bs.read());
        Assert.assertEquals(1,bs.read());
        Assert.assertEquals(2,bs.read());
        Assert.assertEquals(3,bs.read());
        Assert.assertEquals(4,bs.read());
        Assert.assertEquals(5,bs.read());
        Assert.assertEquals(6,bs.read());
        Assert.assertEquals(7,bs.read());
        Assert.assertEquals(8,bs.read());
        Assert.assertEquals(9,bs.read());
        Assert.assertEquals(10,bs.read());
        Assert.assertEquals(-1,bs.read());

    }

    @Test
    public void testRewindAt0() throws IOException {

        final ArrayInputStream in = new ArrayInputStream(new byte[]{0,1,2,3,4,5,6,7,8,9,10});
        final BacktrackInputStream bs = new BacktrackInputStream(in);

        bs.mark();
        Assert.assertEquals(0,bs.read());
        Assert.assertEquals(1,bs.read());
        Assert.assertEquals(2,bs.read());
        Assert.assertEquals(3,bs.read());
        Assert.assertEquals(4,bs.read());
        Assert.assertEquals(5,bs.read());
        Assert.assertEquals(6,bs.read());
        Assert.assertEquals(7,bs.read());
        Assert.assertEquals(8,bs.read());
        Assert.assertEquals(9,bs.read());
        Assert.assertEquals(10,bs.read());
        Assert.assertEquals(-1,bs.read());

        bs.rewind();
        Assert.assertEquals(0,bs.read());
        Assert.assertEquals(1,bs.read());
        Assert.assertEquals(2,bs.read());
        Assert.assertEquals(3,bs.read());
        Assert.assertEquals(4,bs.read());
        Assert.assertEquals(5,bs.read());
        Assert.assertEquals(6,bs.read());
        Assert.assertEquals(7,bs.read());
        Assert.assertEquals(8,bs.read());
        Assert.assertEquals(9,bs.read());
        Assert.assertEquals(10,bs.read());
        Assert.assertEquals(-1,bs.read());

    }

    @Test
    public void testRewindAt5() throws IOException {

        final ArrayInputStream in = new ArrayInputStream(new byte[]{0,1,2,3,4,5,6,7,8,9,10});
        final BacktrackInputStream bs = new BacktrackInputStream(in);

        Assert.assertEquals(0,bs.read());
        Assert.assertEquals(1,bs.read());
        Assert.assertEquals(2,bs.read());
        Assert.assertEquals(3,bs.read());
        Assert.assertEquals(4,bs.read());
        bs.mark();
        Assert.assertEquals(5,bs.read());
        Assert.assertEquals(6,bs.read());
        Assert.assertEquals(7,bs.read());
        Assert.assertEquals(8,bs.read());
        Assert.assertEquals(9,bs.read());
        Assert.assertEquals(10,bs.read());
        Assert.assertEquals(-1,bs.read());

        bs.rewind();
        Assert.assertEquals(5,bs.read());
        Assert.assertEquals(6,bs.read());
        Assert.assertEquals(7,bs.read());
        Assert.assertEquals(8,bs.read());
        Assert.assertEquals(9,bs.read());
        Assert.assertEquals(10,bs.read());
        Assert.assertEquals(-1,bs.read());

    }
}
