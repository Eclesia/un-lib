

package science.unlicense.encoding.impl.cryptography.hash;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;

/**
 *
 * @author Johann Sorel
 */
public class SHA1Test {

    @Test
    public void testSHA1() {
        int i, j;
        final SHA1 s = new SHA1();

        // "abc"
        // A9993E36 4706816A BA3E2571 7850C26C 9CD0D89D
        String z = "abc";
        s.reset();
        s.update((byte) 'a');
        s.update((byte) 'b');
        s.update((byte) 'c');
        //s.finish();
        Assert.assertEquals(new Chars("A9993E364706816ABA3E25717850C26C9CD0D89D"), Int32.encodeHexa(s.getResultBytes()));

        // "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
        // 84983E44 1C3BD26E BAAE4AA1 F95129E5 E54670F1
        z = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
        s.reset();
        s.update(z.getBytes());
        //s.finish();
        Assert.assertEquals(new Chars("84983E441C3BD26EBAAE4AA1F95129E5E54670F1"), Int32.encodeHexa(s.getResultBytes()));

        // A million repetitions of "a"
        // 34AA973C D4C4DAA4 F61EEB2B DBAD2731 6534016F
        s.reset();
        for (i = 0; i < 1000000; i++) {
            s.update((byte) 'a');
        }
        //s.finish();
        Assert.assertEquals(new Chars("34AA973CD4C4DAA4F61EEB2BDBAD27316534016F"), Int32.encodeHexa(s.getResultBytes()));
    }

}
