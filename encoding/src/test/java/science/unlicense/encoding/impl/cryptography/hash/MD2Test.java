package science.unlicense.encoding.impl.cryptography.hash;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;

/**
 *
 * @author Francois Berder
 *
 */
public class MD2Test {

    @Test
    public void testMD2() {
        MD2 md2 = new MD2();

        // "abc"
        String z = "abc";
        md2.reset();
        md2.update((byte) 'a');
        md2.update((byte) 'b');
        md2.update((byte) 'c');
        Assert.assertEquals(new Chars("DA853B0D3F88D99B30283A69E6DED6BB"), Int32.encodeHexa(md2.getResultBytes()));

        md2.reset();
        md2.update("".getBytes());
        Assert.assertEquals(new Chars("8350E5A3E24C153DF2275C9F80692773"), Int32.encodeHexa(md2.getResultBytes()));

        //  "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
        z = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
        md2.reset();
        md2.update(z.getBytes());
        Assert.assertEquals(new Chars("0DFF6B398AD5A62AC8D97566B80C3A7F"), Int32.encodeHexa(md2.getResultBytes()));

        // A million repetitions of "a"
        md2.reset();
        for (int i = 0; i < 1000000; i++) {
            md2.update((byte) 'a');
        }
        Assert.assertEquals(new Chars("8C0A09FF1216ECAF95C8130953C62EFD"), Int32.encodeHexa(md2.getResultBytes()));
    }

}
