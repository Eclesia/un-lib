
package science.unlicense.encoding.impl.io;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.EOSException;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class DataInputStreamTest {

    @Test
    public void testBitReading() throws IOException{

        byte[] data;
        ArrayInputStream stream;
        DataInputStream ds;

        //simple case, bit reading on a single byte
        data = new byte[]{(byte) 17,(byte)-13,(byte) 0xFF,(byte) 21};
        stream = new ArrayInputStream(data);
        ds = new DataInputStream(stream);

        Assert.assertEquals(17,ds.readByte());
        Assert.assertEquals(-13,ds.readByte());

        Assert.assertEquals(1, ds.readBits(1));
        Assert.assertEquals(3, ds.readBits(2));
        Assert.assertEquals(7, ds.readBits(3));
        Assert.assertEquals(3, ds.readBits(2));

        Assert.assertEquals(21,ds.readByte());

        //reading bits overlapping bytes
        // 1  2   3        9    13         42          57          197           183           173   7
        // 1 10 011 10    01 01101 1    01010 011    1001 1100    0101 0101    10111 101     01101 111
        data = new byte[]{
            (byte) 0xCE,   (byte) 0x5B,  (byte) 0x53,  (byte) 0x9C,  (byte) 0x55,  (byte) 0xBD,   (byte) 0x6F
        };
        stream = new ArrayInputStream(data);
        ds = new DataInputStream(stream);

        Assert.assertEquals(1,     ds.readBits(1));
        Assert.assertEquals(2,     ds.readBits(2));
        Assert.assertEquals(3,     ds.readBits(3));
        Assert.assertEquals(9,     ds.readBits(4));
        Assert.assertEquals(13,    ds.readBits(5));
        Assert.assertEquals(42,    ds.readBits(6));
        Assert.assertEquals(57,    ds.readBits(7));
        Assert.assertEquals(197,   ds.readBits(8));
        Assert.assertEquals(183,   ds.readBits(9));
        Assert.assertEquals(173,   ds.readByte() & 0xff);
        Assert.assertEquals(7,   ds.readBits(3));

        try{
            ds.readBits(2);
            Assert.fail("should have throw a EOSException.");
        }catch(EOSException ex) {
            //OK
        }

    }

    @Test
    public void testVarLengthRead() throws IOException {
        byte[] data = new byte[]{(byte) 0x9B,(byte) 0xF1,(byte) 0x59};
        ArrayInputStream stream = new ArrayInputStream(data);
        DataInputStream ds = new DataInputStream(stream);
        Assert.assertEquals(-624485l, ds.readVarLengthInt());

    }

    @Test
    public void testUvarLengthRead() throws IOException {
        byte[] data = new byte[]{(byte) 0xE5,(byte) 0x8E,(byte) 0x26};
        ArrayInputStream stream = new ArrayInputStream(data);
        DataInputStream ds = new DataInputStream(stream);
        Assert.assertEquals(624485l, ds.readVarLengthUInt());

        data = new byte[]{(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,(byte) 0x7F};
        stream = new ArrayInputStream(data);
        ds = new DataInputStream(stream);
        Assert.assertEquals(0x3ffffffffffl, ds.readVarLengthUInt());
    }

}
