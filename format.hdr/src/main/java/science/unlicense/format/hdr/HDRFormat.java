
package science.unlicense.format.hdr;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * Source :
 * http://en.wikipedia.org/wiki/RGBE_image_format
 * http://en.wikipedia.org/wiki/Radiance_(software)#HDR_image_format
 *
 *
 * @author Johann Sorel
 */
public class HDRFormat extends AbstractImageFormat {

    public static final HDRFormat INSTANCE = new HDRFormat();

    private HDRFormat() {
        super(new Chars("hdr"));
        shortName = new Chars("HDR");
        longName = new Chars("RGBE - Radiance");
        mimeTypes.add(new Chars("image/vnd.radiance"));
        extensions.add(new Chars("hdr"));
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new HDRStore(this, source);
    }

}
