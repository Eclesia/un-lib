
package science.unlicense.format.hdr;

import science.unlicense.format.hdr.HDRFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class HDRFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final ImageFormat[] formats = Images.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof HDRFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("Image format not found.");
    }

}
