
package science.unlicense.code.actionscript.model;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCMethod extends ABCBlock {

    //u30 param_count
    /** u30 return_type */
    public int returnType;
    /** u30 param_type[param_count] */
    public int[] paramTypes;
    /** u30 name */
    public int name;
    /** u8 flags */
    public int flags;
//    option_info options
//    param_info param_names

    public void read(DataInputStream ds) throws IOException {
        final int paramNb = readU30(ds);
        returnType = readU30(ds);
        paramTypes = new int[paramNb];
        for (int i=0;i<paramNb;i++) paramTypes[i] = readU30(ds);
        name = readU30(ds);
        flags = ds.readUByte();

        throw new UnimplementedException("Not supported yet.");
    }

    public void write(DataOutputStream ds) throws IOException {
        writeU30(ds, paramTypes.length);
        writeU30(ds, returnType);
        for (int i=0;i<paramTypes.length;i++) writeU30(ds, paramTypes[i]);
        writeU30(ds, name);
        ds.writeUByte(flags);

        throw new UnimplementedException("Not supported yet.");
    }

}
