
package science.unlicense.code.actionscript.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCScript extends ABCBlock {

    /** u30 init */
    public int init;
    /**
     * u30 trait_count
     * traits_info trait[trait_count]
     */
    public ABCTrait[] traits;

    public void read(DataInputStream ds) throws IOException {
        init = readU30(ds);
        traits = (ABCTrait[]) readBlockArray(ds, ABCTrait.class);
    }

    public void write(DataOutputStream ds) throws IOException {
        writeU30(ds, init);
        writeBlocksArray(ds, traits);
    }

}
