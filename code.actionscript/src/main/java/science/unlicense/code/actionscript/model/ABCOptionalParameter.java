
package science.unlicense.code.actionscript.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCOptionalParameter extends ABCBlock {

    /** u30 val
     * u8 kind
     */
    public int val;
    public int kind;

    public void read(DataInputStream ds) throws IOException {
        val = readU30(ds);
        kind = ds.readUByte();
    }

    public void write(DataOutputStream ds) throws IOException {
        writeU30(ds, val);
        ds.writeUByte(kind);
    }

}
