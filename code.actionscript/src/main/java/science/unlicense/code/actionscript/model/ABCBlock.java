
package science.unlicense.code.actionscript.model;

import java.lang.reflect.Array;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class ABCBlock {

    public abstract void read(DataInputStream ds) throws IOException;

    public abstract void write(DataOutputStream ds) throws IOException;

    protected Chars readChars(DataInputStream ds) {
        return null;
    }

    protected int readU30(DataInputStream ds) throws IOException{
        return ds.readInt();
    }

    protected int readU32(DataInputStream ds) throws IOException{
        return ds.readInt();
    }

    protected int readS32(DataInputStream ds) throws IOException{
        return ds.readInt();
    }

    protected int[] readU30Array(DataInputStream ds) throws IOException{
        final int[] array = new int[readU30(ds)];
        for (int i=0;i<array.length;i++) array[i] = readU30(ds);
        return array;
    }

    protected Object readBlockArray(DataInputStream ds, Class clazz) throws IOException{
        final int length = readU30(ds);
        final Object array = Array.newInstance(clazz, length);
        try{
            for (int i=0;i<length;i++) {
                final ABCBlock block = (ABCBlock) clazz.newInstance();
                block.read(ds);
                Array.set(array, i, block);
            }
        }catch(InstantiationException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }catch(IllegalAccessException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
        return array;
    }

    protected void writeChars(DataOutputStream ds, Chars str) {

    }

    protected void writeU30(DataOutputStream ds, int value) throws IOException{
        ds.writeInt(value);
    }

    protected void writeU32(DataOutputStream ds, int value) throws IOException{
        ds.writeInt(value);
    }

    protected void writeS32(DataOutputStream ds, int value) throws IOException{
        ds.writeInt(value);
    }

    /**
     * Write size as u30
     * write values after
     *
     * @param ds
     * @param value
     * @throws IOException
     */
    protected void writeU30Array(DataOutputStream ds, int[] value) throws IOException{
        writeU30(ds, value.length);
        for (int i=0;i<value.length;i++) writeU30(ds, value[i]);
    }

    /**
     * Write size as u30
     * write blocks after
     *
     * @param ds
     * @param value
     * @throws IOException
     */
    protected void writeBlocksArray(DataOutputStream ds, ABCBlock[] value) throws IOException{
        writeU30(ds, value.length);
        for (int i=0;i<value.length;i++) value[i].write(ds);
    }

}
