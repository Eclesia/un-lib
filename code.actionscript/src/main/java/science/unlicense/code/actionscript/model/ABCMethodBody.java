
package science.unlicense.code.actionscript.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCMethodBody extends ABCBlock {

    /** u30 method */
    public int method;
    /** u30 max_stack */
    public int maxStack;
    /** u30 local_count */
    public int localCount;
    /** u30 init_scope_depth */
    public int initScopeDepth;
    /** u30 max_scope_depth */
    public int maxScopeDepth;
    /**
     * u30 code_length
     * u8 code[code_length]
     */
    public int[] codes;
    /**
     * u30 exception_count
     * exception_info exception[exception_count]
     */
    public ABCException[] exceptions;
    /** u30 trait_count
     * traits_info trait[trait_count]
     */
    public ABCTrait[] traits;

    public void read(DataInputStream ds) throws IOException {
        method = readU30(ds);
        maxStack = readU30(ds);
        localCount = readU30(ds);
        initScopeDepth = readU30(ds);
        maxScopeDepth = readU30(ds);
        codes = new int[readU30(ds)];
        for (int i=0;i<codes.length;i++) codes[i] = readU30(ds);

        exceptions = new ABCException[readU30(ds)];
        for (int i=0;i<exceptions.length;i++) {
            exceptions[i] = new ABCException();
            exceptions[i].read(ds);
        }

        traits = new ABCTrait[readU30(ds)];
        for (int i=0;i<traits.length;i++) {
            traits[i] = new ABCTrait();
            traits[i].read(ds);
        }
    }

    public void write(DataOutputStream ds) throws IOException {
        writeU30(ds, method);
        writeU30(ds, maxStack);
        writeU30(ds, localCount);
        writeU30(ds, initScopeDepth);
        writeU30(ds, maxScopeDepth);
        writeU30Array(ds, codes);
        writeBlocksArray(ds, exceptions);
        writeBlocksArray(ds, traits);
    }

}
