
package science.unlicense.code.actionscript.model;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCMetadataInfo extends ABCBlock {

    /** u30 */
    public int name;
    /** u30 count
     * item_info items[item_count]
     */

    public void read(DataInputStream ds) throws IOException {
        name = readU30(ds);
        throw new UnimplementedException("Not supported yet.");
    }

    public void write(DataOutputStream ds) throws IOException {
        writeU30(ds, name);
        throw new UnimplementedException("Not supported yet.");
    }

}
