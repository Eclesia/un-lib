
package science.unlicense.format.wbmp;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * Wireless Application Protocol Bitmap Format.
 *
 * resources :
 * http://en.wikipedia.org/wiki/Wireless_Application_Protocol_Bitmap_Format
 * http://www.wapforum.org/what/technical/SPEC-WAESpec-19990524.pdf
 *
 * @author Johann Sorel
 */
public class WBMPFormat extends AbstractImageFormat {

    public static final WBMPFormat INSTANCE = new WBMPFormat();

    private WBMPFormat() {
        super(new Chars("wbmp"));
        shortName = new Chars("WBMP");
        longName = new Chars("Wireless Application Protocol Bitmap Format");
        mimeTypes.add(new Chars("image/x-wap.wbmp"));
        mimeTypes.add(new Chars("image/vnd-wap.wbmp"));
        extensions.add(new Chars("wbmp"));
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new WBMPStore(this, source);
    }

}
