package science.unlicense.format.wbmp;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.common.api.number.Bits;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import static science.unlicense.image.api.ImageSetMetadata.*;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.PlanarModel;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 *
 * @author Johann Sorel
 */
public class WBMPReader extends AbstractImageReader{

    private byte type;
    private int width;
    private int height;
    private DefaultTypedNode mdImage;

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws science.unlicense.encoding.api.io.IOException {
        final DataInputStream ds = new DataInputStream(stream, Endianness.LITTLE_ENDIAN);

        type = ds.readByte();
        if (type != 0){
            throw new IOException("Unvalid wbmp type : "+type);
        }

        final byte fixedHeader = ds.readByte();
        if (fixedHeader != 0){
            throw new IOException("Unvalid fix header value : "+fixedHeader);
        }

        width = readInt(ds);
        height = readInt(ds);

        mdImage =
        new DefaultTypedNode(MD_IMAGE.getType(),new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION.getType(),new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID.getType(),"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND.getType(),width)}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION.getType(),new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID.getType(),"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND.getType(),height)})
        });

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    /**
     * WBMP define variable size integer.
     * the first bit of each byte indicate the value continues on the next byte.
     *
     * @param ds
     * @return int
     * @throws IOException
     */
    private static int readInt(final DataInputStream ds) throws IOException{
        int b = ds.readByte();
        int val = 0;
        for (;;){
            val = (val << 7) | (b & 0x7F);
            if ((b&0x80) != 0){
                //there is a next byte
                b = ds.readByte();
            } else {
                break;
            }
        }
        return val;
    }

    protected Image read(ImageReadParameters params, BacktrackInputStream stream) throws science.unlicense.encoding.api.io.IOException {
        readMetadatas(stream);
        final DataInputStream ds = new DataInputStream(stream);

        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ods = new DataOutputStream(out);
        for (int y=0;y<height;y++){
            for (int x=0;x<width;x++){
                final int b = ds.readBits(1);
                ods.writeBit(b);
            }
            ds.skipToByteEnd();
        }
        ods.flush();

        final Buffer bank = DefaultBufferFactory.wrap(out.getBuffer().getBackArray());
        final ImageModel sm = new PlanarModel(new UndefinedSystem(1), Bits.TYPE_1_BIT);
        final ImageModel cm = DerivateModel.create(sm, new int[]{0,0,0}, null, null, ColorSystem.RGB_8BITS);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

}
