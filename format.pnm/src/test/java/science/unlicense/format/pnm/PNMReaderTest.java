package science.unlicense.format.pnm;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class PNMReaderTest {

    private static final Vector2i32 b00 = new Vector2i32(0,0);
    private static final Vector2i32 b10 = new Vector2i32(1,0);
    private static final Vector2i32 b20 = new Vector2i32(2,0);
    private static final Vector2i32 b30 = new Vector2i32(3,0);
    private static final Vector2i32 b01 = new Vector2i32(0,1);
    private static final Vector2i32 b11 = new Vector2i32(1,1);
    private static final Vector2i32 b21 = new Vector2i32(2,1);
    private static final Vector2i32 b31 = new Vector2i32(3,1);

    @Test
    public void testBWASCII() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/pnm/ascii.pbm")).createInputStream();

        final ImageReader reader = new PNMReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleGrid tb = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();

        final TupleRW pixel = tb.createTuple();
        tb.getTuple(new Vector2i32(0,0),pixel);
        Assert.assertEquals(true, pixel.get(0)!=0); //white
        tb.getTuple(new Vector2i32(1,0),pixel);
        Assert.assertEquals(false, pixel.get(0)!=0); //black
        tb.getTuple(new Vector2i32(0,1),pixel);
        Assert.assertEquals(false, pixel.get(0)!=0); //black
        tb.getTuple(new Vector2i32(1,1),pixel);
        Assert.assertEquals(true, pixel.get(0)!=0); //white

    }

    @Test
    public void testBWBinary() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/pnm/binary.pbm")).createInputStream();

        final ImageReader reader = new PNMReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleGrid tb = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();

        final TupleRW pixel = tb.createTuple();
        tb.getTuple(new Vector2i32(0,0),pixel);
        Assert.assertEquals(true, pixel.get(0)!=0); //white
        tb.getTuple(new Vector2i32(1,0),pixel);
        Assert.assertEquals(false, pixel.get(0)!=0); //black
        tb.getTuple(new Vector2i32(0,1),pixel);
        Assert.assertEquals(false, pixel.get(0)!=0); //black
        tb.getTuple(new Vector2i32(1,1),pixel);
        Assert.assertEquals(true, pixel.get(0)!=0); //white

    }

    @Test
    public void testGrayscaleASCII() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/pnm/ascii.pgm")).createInputStream();

        final ImageReader reader = new PNMReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final ImageModel sm = image.getRawModel();
        final ImageModel cm = image.getColorModel();

        Assert.assertArrayEquals(new int[]{ 54}, image.getTuple(b00,sm).toInt());
        Assert.assertArrayEquals(new int[]{237}, image.getTuple(b10,sm).toInt());
        Assert.assertArrayEquals(new int[]{201}, image.getTuple(b01,sm).toInt());
        Assert.assertArrayEquals(new int[]{ 18}, image.getTuple(b11,sm).toInt());

        Assert.assertEquals(new ColorRGB( 54,  54,  54, 255), image.getColor(b00));
        Assert.assertEquals(new ColorRGB(237, 237, 237, 255), image.getColor(b10));
        Assert.assertEquals(new ColorRGB(201, 201, 201, 255), image.getColor(b01));
        Assert.assertEquals(new ColorRGB( 18,  18,  18, 255), image.getColor(b11));

    }

    @Test
    public void testGrayscaleBinary() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/pnm/binary.pgm")).createInputStream();

        final ImageReader reader = new PNMReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final ImageModel sm = image.getRawModel();
        final ImageModel cm = image.getColorModel();

        Assert.assertArrayEquals(new int[]{ 54}, image.getTuple(b00,sm).toInt());
        Assert.assertArrayEquals(new int[]{237}, image.getTuple(b10,sm).toInt());
        Assert.assertArrayEquals(new int[]{201}, image.getTuple(b01,sm).toInt());
        Assert.assertArrayEquals(new int[]{ 18}, image.getTuple(b11,sm).toInt());

        Assert.assertEquals(new ColorRGB( 54,  54,  54, 255), image.getColor(b00));
        Assert.assertEquals(new ColorRGB(237, 237, 237, 255), image.getColor(b10));
        Assert.assertEquals(new ColorRGB(201, 201, 201, 255), image.getColor(b01));
        Assert.assertEquals(new ColorRGB( 18,  18,  18, 255), image.getColor(b11));

    }

    @Test
    public void testRGBASCII() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/pnm/ascii.ppm")).createInputStream();

        final ImageReader reader = new PNMReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final ImageModel sm = image.getRawModel();
        final ImageModel cm = image.getColorModel();

        Assert.assertArrayEquals(new int[]{255,  0,  0}, image.getTuple(b00,sm).toInt()); //RED
        Assert.assertArrayEquals(new int[]{255,255,  0}, image.getTuple(b10,sm).toInt()); //YELLOW
        Assert.assertArrayEquals(new int[]{  0,255,255}, image.getTuple(b01,sm).toInt()); //CYAN
        Assert.assertArrayEquals(new int[]{  0,  0,255}, image.getTuple(b11,sm).toInt()); //BLUE

        Assert.assertEquals(new ColorRGB(255,   0,   0, 255), image.getColor(b00));
        Assert.assertEquals(new ColorRGB(255, 255,   0, 255), image.getColor(b10));
        Assert.assertEquals(new ColorRGB(0,   255, 255, 255), image.getColor(b01));
        Assert.assertEquals(new ColorRGB(0,     0, 255, 255), image.getColor(b11));

    }

    @Test
    public void testRGBBinary() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/pnm/binary.ppm")).createInputStream();

        final ImageReader reader = new PNMReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final ImageModel sm = image.getRawModel();
        final ImageModel cm = image.getColorModel();

        Assert.assertArrayEquals(new int[]{255,  0,  0}, image.getTuple(b00,sm).toInt()); //RED
        Assert.assertArrayEquals(new int[]{255,255,  0}, image.getTuple(b10,sm).toInt()); //YELLOW
        Assert.assertArrayEquals(new int[]{  0,255,255}, image.getTuple(b01,sm).toInt()); //CYAN
        Assert.assertArrayEquals(new int[]{  0,  0,255}, image.getTuple(b11,sm).toInt()); //BLUE

        Assert.assertEquals(new ColorRGB(255,   0,   0, 255), image.getColor(b00));
        Assert.assertEquals(new ColorRGB(255, 255,   0, 255), image.getColor(b10));
        Assert.assertEquals(new ColorRGB(0,   255, 255, 255), image.getColor(b01));
        Assert.assertEquals(new ColorRGB(0,     0, 255, 255), image.getColor(b11));

    }

}
