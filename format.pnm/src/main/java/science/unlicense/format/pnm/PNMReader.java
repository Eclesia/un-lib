package science.unlicense.format.pnm;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.number.Bits;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt16;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import science.unlicense.image.api.ImageSetMetadata;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.image.api.model.PlanarModel;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 *
 * @author Johann Sorel
 */
public class PNMReader extends AbstractImageReader{

    private Chars type;
    private int width;
    private int height;
    //used in grascale and rgb images only
    private int maxValue;
    private ImageSetMetadata.Image mdImage;

    //for various internal use
    private final ByteSequence buffer = new ByteSequence();

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        stream.mark();
        final DataInputStream ds = new DataInputStream(stream);
        type = new Chars(ds.readFully(new byte[2]));
        expectSpace(stream,ds,true);
        width = Int32.decode(new Chars(readWord(stream,ds)));
        expectSpace(stream,ds,true);
        height = Int32.decode(new Chars(readWord(stream,ds)));
        expectSpace(stream,ds,true);
        if (   PNMMetaModel.SIGNATURE_GRAYSCALE_ASCII.equals(type)
           || PNMMetaModel.SIGNATURE_GRAYSCALE_BINARY.equals(type)
           || PNMMetaModel.SIGNATURE_RGB_ASCII.equals(type)
           || PNMMetaModel.SIGNATURE_RGB_BINARY.equals(type)){
            maxValue = Int32.decode(new Chars(readWord(stream,ds)));
            expectSpace(stream,ds,true);
        }

        mdImage = new ImageSetMetadata.Image(width, height);
        final ImageSetMetadata mdImageSet = new ImageSetMetadata(mdImage);
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        stream.mark();
        readMetadatas(stream);

        if (PNMMetaModel.SIGNATURE_BW_ASCII.equals(type)){
            return readBWASCII(stream);
        } else if (PNMMetaModel.SIGNATURE_BW_BINARY.equals(type)){
            return readBWBinary(stream);
        } else if (PNMMetaModel.SIGNATURE_GRAYSCALE_ASCII.equals(type)){
            return readGrayscaleASCII(stream);
        } else if (PNMMetaModel.SIGNATURE_GRAYSCALE_BINARY.equals(type)){
            return readGrayscaleBinary(stream);
        } else if (PNMMetaModel.SIGNATURE_RGB_ASCII.equals(type)){
            return readRGBASCII(stream);
        } else if (PNMMetaModel.SIGNATURE_RGB_BINARY.equals(type)){
            return readRGBBinary(stream);
        } else {
            throw new IOException(stream, "Unknowed PNM image type : "+type);
        }

    }

    private Image readBWASCII(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,Endianness.BIG_ENDIAN);

        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ods = new DataOutputStream(out);
        for (int y=0;y<height;y++){
            expectSpace(stream, ds, false);
            for (int x=0;x<width;x++){
                expectSpace(stream, ds, false);
                final byte b = ds.readByte();
                if (b=='0'){
                    ods.writeBit(0);
                } else if (b=='1'){
                    ods.writeBit(1);
                } else {
                    throw new IOException(stream, "Unpected value "+b);
                }
            }
        }
        ods.flush();

        final Buffer bank = DefaultBufferFactory.wrap(out.getBuffer().getBackArray());
        final ImageModel sm = new PlanarModel(new UndefinedSystem(1), Bits.TYPE_1_BIT);
        final ImageModel cm = DerivateModel.create(sm, new int[]{0,0,0}, null, null, ColorSystem.RGB_8BITS);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    private Image readBWBinary(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,Endianness.BIG_ENDIAN);

        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ods = new DataOutputStream(out);
        for (int y=0;y<height;y++){
            expectSpace(stream, ds, false);
            for (int x=0;x<width;x++){
                final int b = ds.readBits(1);
                ods.writeBit(b);
            }
            ds.skipToByteEnd();
        }
        ods.flush();

        final Buffer bank = DefaultBufferFactory.wrap(out.getBuffer().getBackArray());
        final ImageModel sm = new PlanarModel(new UndefinedSystem(1), Bits.TYPE_1_BIT);
        final ImageModel cm = DerivateModel.create(sm, new int[]{0,0,0}, null, null, ColorSystem.RGB_8BITS);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    private Image readGrayscaleASCII(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,Endianness.BIG_ENDIAN);

        final NumberType sampleType = (maxValue<256) ? UInt8.TYPE : UInt16.TYPE;

        final int bytePerSample = (sampleType.getSizeInBits()/8);
        final int scanline = bytePerSample*width;
        final byte[] datas = new byte[scanline*height];
        int offset = 0;

        for (int y=0;y<height;y++){
            expectSpace(stream, ds, false);
            for (int x=0;x<width;x++){
                expectSpace(stream, ds, false);
                final int val = Int32.decode(new Chars(readWord(stream,ds)));
                if (sampleType==UInt8.TYPE){
                    Endianness.BIG_ENDIAN.writeUByte(val, datas, offset);
                    offset+=1;
                } else {
                    Endianness.BIG_ENDIAN.writeUShort(val, datas, offset);
                    offset+=2;
                }
            }
        }

        final Buffer bank = DefaultBufferFactory.wrap(datas);
        final ImageModel sm = new PlanarModel(new UndefinedSystem(1), sampleType);
        final ImageModel cm = DerivateModel.create(sm, new int[]{0,0,0}, null, null, ColorSystem.RGB_8BITS);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    private Image readGrayscaleBinary(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,Endianness.BIG_ENDIAN);

        final NumberType sampleType = (maxValue<256) ? UInt8.TYPE : UInt16.TYPE;

        final int scanline = (sampleType.getSizeInBits()/8)*width;
        final byte[] datas = new byte[scanline*height];

        for (int y=0,offset=0;y<height;y++,offset+=scanline){
            expectSpace(stream, ds, false);
            ds.readFully(datas,offset,scanline);
        }

        final Buffer bank = DefaultBufferFactory.wrap(datas);
        final ImageModel sm = new PlanarModel(new UndefinedSystem(1), sampleType);
        final ImageModel cm = DerivateModel.create(sm, new int[]{0,0,0}, null, null, ColorSystem.RGB_8BITS);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    private Image readRGBASCII(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,Endianness.BIG_ENDIAN);
        final NumberType sampleType = (maxValue<256) ? UInt8.TYPE : UInt16.TYPE;

        final int scanline = (sampleType.getSizeInBits()/8)*width*3;
        final byte[] datas = new byte[scanline*height];
        int offset = 0;

        for (int y=0;y<height;y++){
            expectSpace(stream, ds, false);
            for (int x=0,n=width*3;x<n;x++){
                expectSpace(stream, ds, false);
                final int val = Int32.decode(new Chars(readWord(stream,ds)));
                if (sampleType==UInt8.TYPE){
                    Endianness.BIG_ENDIAN.writeUByte(val, datas, offset);
                    offset+=1;
                } else {
                    Endianness.BIG_ENDIAN.writeUShort(val, datas, offset);
                    offset+=2;
                }
            }
        }

        final Buffer bank = DefaultBufferFactory.wrap(datas);
        final ImageModel sm = new InterleavedModel(new UndefinedSystem(3), sampleType);
        final ImageModel cm = DerivateModel.create(sm, new int[]{0,1,2}, null, null, ColorSystem.RGB_8BITS);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    private Image readRGBBinary(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,Endianness.BIG_ENDIAN);

        final NumberType sampleType = (maxValue<256) ? UInt8.TYPE : UInt16.TYPE;

        final int scanline = (sampleType.getSizeInBits()/8)*width*3;
        final byte[] datas = new byte[scanline*height];

        for (int y=0,offset=0;y<height;y++,offset+=scanline){
            expectSpace(stream, ds, false);
            ds.readFully(datas,offset,scanline);
        }

        final Buffer bank = DefaultBufferFactory.wrap(datas);
        final ImageModel sm = new InterleavedModel(new UndefinedSystem(3), sampleType);
        final ImageModel cm = DerivateModel.create(sm, new int[]{0,1,2}, null, null, ColorSystem.RGB_8BITS);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    /**
     * Check escape character and skip comments.
     * @param ds
     * @throws IOException
     */
    private void expectSpace(BacktrackInputStream bs, DataInputStream ds, boolean expected) throws IOException{
        final byte c = ds.readByte();

        if (c==' ' || c=='\t'){
            //do nothing
        } else if (c=='\n'){
            //skip comment lines
            for (byte b=ds.readByte(); b=='#'; b=ds.readByte()){
                //find end of line
                for (b=ds.readByte(); b!='\n'; b=ds.readByte());
            }
            int position = bs.position()-1;
            bs.rewind();
            bs.skip(position);
        } else {
            if (expected){
                //escape char is imposed
                throw new IOException(ds, "Was expecting a spacing character.");
            } else {
                //escape char is optional, move back
                int position = bs.position()-1;
                bs.rewind();
                bs.skip(position);
            }
        }

    }

    private byte[] readWord(BacktrackInputStream bs, final DataInputStream ds) throws IOException{
        buffer.removeAll();
        for (byte b=ds.readByte(); !isEscape(b); b=ds.readByte()){
            buffer.put(b);
        }
        int position = bs.position()-1;
        bs.rewind();
        bs.skip(position);
        return buffer.toArrayByte();
    }

    private static boolean isEscape(final byte b){
        return b==' '||b=='\t'||b=='\n';
    }

}
