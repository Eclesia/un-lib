package science.unlicense.format.pnm;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class PNMMetaModel {

    public static final Chars SIGNATURE_BW_ASCII        = Chars.constant("P1");
    public static final Chars SIGNATURE_BW_BINARY       = Chars.constant("P4");
    public static final Chars SIGNATURE_GRAYSCALE_ASCII = Chars.constant("P2");
    public static final Chars SIGNATURE_GRAYSCALE_BINARY= Chars.constant("P5");
    public static final Chars SIGNATURE_RGB_ASCII       = Chars.constant("P3");
    public static final Chars SIGNATURE_RGB_BINARY      = Chars.constant("P6");

    private PNMMetaModel(){}

}
