
package science.unlicense.format.pnm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * Format explication :
 * http://fr.wikipedia.org/wiki/Portable_pixmap
 *
 * @author Johann Sorel
 */
public class PNMFormat extends AbstractImageFormat {

    public static final PNMFormat INSTANCE = new PNMFormat();

    private PNMFormat() {
        super(new Chars("pnm"));
        shortName = new Chars("Netpbm");
        longName = new Chars("Netpbm");
        mimeTypes.add(new Chars("image/x-portable-pixmap"));
        mimeTypes.add(new Chars("image/x-portable-graymap"));
        mimeTypes.add(new Chars("image/x-portable-bitmap"));
        extensions.add(new Chars("ppm"));
        extensions.add(new Chars("pgm"));
        extensions.add(new Chars("pbm"));
        extensions.add(new Chars("pnm"));
        signatures.add(PNMMetaModel.SIGNATURE_BW_ASCII.toBytes());
        signatures.add(PNMMetaModel.SIGNATURE_BW_BINARY.toBytes());
        signatures.add(PNMMetaModel.SIGNATURE_GRAYSCALE_ASCII.toBytes());
        signatures.add(PNMMetaModel.SIGNATURE_GRAYSCALE_BINARY.toBytes());
        signatures.add(PNMMetaModel.SIGNATURE_RGB_ASCII.toBytes());
        signatures.add(PNMMetaModel.SIGNATURE_RGB_BINARY.toBytes());
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return true;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new PNMStore(this, source);
    }

}
