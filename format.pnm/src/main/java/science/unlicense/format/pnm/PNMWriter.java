package science.unlicense.format.pnm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.AbstractImageWriter;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageWriteParameters;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Remi Bonnaud
 */
public class PNMWriter  extends AbstractImageWriter {

    /**
     * Write an image.
     * @param image
     * @param params
     * @param stream
     * @throws IOException
     */
    protected void write(Image image, ImageWriteParameters params, ByteOutputStream stream) throws IOException {
        PNMWriteParameters pnmParams = (PNMWriteParameters) params;
        Chars signature = pnmParams.getSignature();

        if ( PNMMetaModel.SIGNATURE_BW_ASCII.equals(signature) ) {
            writeBWASCII(stream, image );
        } else if (PNMMetaModel.SIGNATURE_BW_BINARY.equals(signature)) {
            writeBWBinary(stream, image);
        } else if (PNMMetaModel.SIGNATURE_GRAYSCALE_ASCII.equals(signature)) {
            writeGrayscaleASCII(stream, image);
        } else if (PNMMetaModel.SIGNATURE_GRAYSCALE_BINARY.equals(signature)) {
            writeGrayscaleBinary(stream, image);
        } else if (PNMMetaModel.SIGNATURE_RGB_ASCII.equals(signature)) {
            writeRGBASCII( stream, image);
        } else if (PNMMetaModel.SIGNATURE_RGB_BINARY.equals(signature)) {
            writeRGBBinary(stream, image);
        } else {
            throw new IOException(stream, "Unknowed PNM image type : "+signature);
        }
        stream.close();
    }

    /**
     * Store an image in BW_ASCII
     * @param stream
     * @param image
     * @throws IOException
     * CHECKED OK
     */
    private void writeBWASCII(ByteOutputStream stream, Image image) throws IOException {
        final CharOutputStream ds = new CharOutputStream(stream);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        // Signature
        ds.write(PNMMetaModel.SIGNATURE_BW_ASCII.toBytes());
        ds.write('\n');
        // Size
        ds.write( Int32.encode(width) );
        ds.write(' ');
        ds.write(Int32.encode(height) );
        ds.write('\n');
        //write each pixel
        final TupleGridCursor cursor = image.getRawModel().asTupleBuffer(image).cursor();
        final Vector2i32 coord = new Vector2i32();
        for (int y=0; y<height; y++) {
            coord.y = y;
            for (int x=0; x<width; x++) {
                coord.x = x;
                cursor.moveTo(coord);
                if ( cursor.samples().get(0) != 0 ) {
                    ds.write( Int32.encode(1) );
                } else {
                    ds.write( Int32.encode(0) );
                }
                ds.write(' ');
            }
            ds.write('\n');
        }
    }

    /**
     * Store an image in BW_BINARY
     * @param stream
     * @param image
     * @throws IOException
     * CHECKED OK
     */
    private void writeBWBinary(ByteOutputStream stream, Image image) throws IOException{
        final CharOutputStream ds = new CharOutputStream(stream);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final TupleGridCursor cursor = image.getRawModel().asTupleBuffer(image).cursor();
        // Signature
        ds.write(PNMMetaModel.SIGNATURE_BW_BINARY.toBytes());
        ds.write('\n');
        // Size
        ds.write( Int32.encode(width) );
        ds.write(' ');
        ds.write(Int32.encode(height));
        ds.write('\n');

        final ArrayOutputStream aos = new ArrayOutputStream();
        final DataOutputStream dos = new DataOutputStream( aos );
        //write each pixel
        final Vector2i32 coord = new Vector2i32();
        final Boolean needPadding = width % 8 != 0;
        final int padding = 8 - (width % 8) ;
        for (int y = 0; y<height; y++) {
            coord.y = y;
            for (int x = 0; x<width; x++) {
                coord.x = x;
                cursor.moveTo(coord);
                if ( cursor.samples().get(0) != 0 ) {
                    dos.writeBit( 1 );
                } else {
                    dos.writeBit( 0 );
                }
            }
            if ( needPadding ) {
                dos.writeBits(0, padding);
            }
        }
        dos.flush();
        ds.write( aos.getBuffer().toArrayByte() );
        dos.close();
        aos.close();
    }

    /**
     * Store an image in GRAYSCALE_ASCII
     * @param stream
     * @param image
     * @throws IOException
     * CHECKED OK
     */
    private void writeGrayscaleASCII(ByteOutputStream stream, Image image) throws IOException{
        final CharOutputStream ds = new CharOutputStream(stream);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final ImageModel rm = image.getRawModel();
        final TupleGridCursor cursor = rm.asTupleBuffer(image).cursor();

        // Signature
        ds.write(PNMMetaModel.SIGNATURE_GRAYSCALE_ASCII.toBytes());
        ds.write('\n');
        // Size
        ds.write( Int32.encode(width) );
        ds.write(' ');
        ds.write(Int32.encode(height));
        ds.write('\n');
        //
        ds.write( Int32.encode( (int) Math.pow(2, rm.getNumericType().getSizeInBits())-1) );
        ds.write('\n');
        //write each grayscale
        final Vector2i32 coord = new Vector2i32();
        for (int y = 0; y <height; y++) {
            coord.y = y;
            for (int x = 0; x < width; x++) {
                coord.x = x;
                cursor.moveTo(coord);
                ds.write( Int32.encode( (int) cursor.samples().get(0) ) );
                ds.write(' ');
            }
            ds.write('\n');
        }
    }

    /**
     *
     * @param stream
     * @param image
     * @throws IOException
     */
    private void writeGrayscaleBinary(ByteOutputStream stream, Image image) throws IOException {
        final CharOutputStream ds = new CharOutputStream(stream);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final ImageModel rm = image.getRawModel();
        final TupleGrid sm = rm.asTupleBuffer(image);
        // Signature
        ds.write(PNMMetaModel.SIGNATURE_GRAYSCALE_BINARY.toBytes());
        ds.write('\n');
        // Size
        ds.write( Int32.encode(width) );
        ds.write(' ');
        ds.write(Int32.encode(height));
        ds.write('\n');
        final int maxValue = (int) Math.pow(2,  rm.getNumericType().getSizeInBits())-1;
        ds.write( Int32.encode(maxValue) );
        ds.write('\n');
        //write each pixel
        final ArrayOutputStream aos = new ArrayOutputStream();
        final DataOutputStream dos = new DataOutputStream( aos );
        final Vector2i32 coord = new Vector2i32();
        if ( maxValue<256 ) {
            TupleRW pixel = sm.createTuple();
            for (coord.y = 0; coord.y<height; coord.y++) {
                for (coord.x = 0; coord.x<width; coord.x++) {
                    sm.getTuple(coord, pixel);
                    dos.write((byte) pixel.get(0));
                }
            }
        } else {
            TupleRW pixel = sm.createTuple();
            for (coord.y = 0; coord.y<height; coord.y++) {
                for (coord.x = 0; coord.x<width; coord.x++) {
                    sm.getTuple(coord, pixel);
                    dos.writeUShort((int) pixel.get(0));
                }
            }
        }
        dos.flush();
        ds.write( aos.getBuffer().toArrayByte() );
        dos.close();
        aos.close();
    }

    /**
     * Store an image in RGB_ASCII
     * @param stream ByteOutputStream for save.
     * @param image image object to save.
     * @throws IOException
     * CHECKED OK
     */
    private void writeRGBASCII(ByteOutputStream stream, Image image) throws IOException {
        final CharOutputStream ds = new CharOutputStream(stream);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final ImageModel rm = image.getRawModel();
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor cursor = cm.asTupleBuffer(image).cursor();
        final Color pixel = Colors.castOrWrap(cursor.samples(), cs);

        // Signature
        ds.write(PNMMetaModel.SIGNATURE_RGB_ASCII.toBytes());
        ds.write('\n');
        // width and height
        ds.write( Int32.encode(width) );
        ds.write(' ');
        ds.write(Int32.encode(height));
        ds.write('\n');
        //
        ds.write( Int32.encode( (int) Math.pow(2,  rm.getNumericType().getSizeInBits())-1) );
        ds.write('\n');
        //write each color
        final Vector2i32 coord = new Vector2i32();
        for (int y = 0; y <height; y++) {
            coord.y = y;
            for (int x = 0; x < width; x++) {
                coord.x = x;
                cursor.moveTo(coord);
                ds.write( Int32.encode( (int) (pixel.getRed() * 255)) );
                ds.write(' ');
                ds.write( Int32.encode( (int) (pixel.getGreen() * 255)) );
                ds.write(' ');
                ds.write( Int32.encode( (int) (pixel.getBlue() * 255)) );
                ds.write(' ');
            }
            ds.write('\n');
        }
    }

    /**
     * Store an image in RGB_BINARY
     * @param stream
     * @param image
     * @throws IOException
     * CHECKER OK
     */
    private void writeRGBBinary(ByteOutputStream stream, Image image) throws IOException {
        final CharOutputStream ds = new CharOutputStream(stream);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final ImageModel rm = image.getRawModel();
        final TupleGrid sm = rm.asTupleBuffer(image);
        // Signature
        ds.write(PNMMetaModel.SIGNATURE_RGB_BINARY.toBytes());
        ds.write('\n');
        // Size
        ds.write( Int32.encode(width) );
        ds.write(' ');
        ds.write(Int32.encode(height));
        ds.write('\n');
        final int maxValue = (int) Math.pow(2,  rm.getNumericType().getSizeInBits())-1;
        ds.write( Int32.encode(maxValue) );
        ds.write('\n');
        //write each pixel
        final Vector2i32 coord = new Vector2i32();
        final ArrayOutputStream aos = new ArrayOutputStream();
        final DataOutputStream dos = new DataOutputStream( aos );
        if ( maxValue<256 ) {

            TupleRW pixel = sm.createTuple();
            for (coord.y = 0; coord.y<height; coord.y++) {
                for (coord.x = 0; coord.x<width; coord.x++) {
                    sm.getTuple(coord, pixel);
                    dos.write((byte) pixel.get(0) );
                    dos.write((byte) pixel.get(1) );
                    dos.write((byte) pixel.get(2) );
                }
            }
        }
        else {
            TupleRW pixel = sm.createTuple();
            for (coord.y = 0; coord.y<height; coord.y++) {
                for (coord.x = 0; coord.x<width; coord.x++) {
                    sm.getTuple(coord, pixel);
                    dos.writeUShort((int) pixel.get(0));
                    dos.writeUShort((int) pixel.get(1));
                    dos.writeUShort((int) pixel.get(2));
                }
            }
        }
        dos.flush();
        ds.write( aos.getBuffer().toArrayByte() );
        dos.close();
        aos.close();
    }

    /**
     * return a default ImageWriteParameters for PNM image
     * @return PNMWriteParameters.getPNMMetaModelGrayScaleAscii()
     */
    public ImageWriteParameters createParameters() {
        return PNMWriteParameters.getPNMMetaModelGrayScaleAscii() ;
    }

}
