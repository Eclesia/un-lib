
package science.unlicense.format.pnm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.ImageResource;
import science.unlicense.image.api.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class PNMStore extends AbstractStore implements ImageResource {

    public PNMStore(ImageFormat format, Object input) {
        super(format, input);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return true;
    }

    @Override
    public ImageReader createReader() throws IOException {
        final PNMReader reader = new PNMReader();
        reader.setInput(source);
        return reader;
    }

    @Override
    public ImageWriter createWriter() throws IOException {
        final PNMWriter writer = new PNMWriter();
        writer.setOutput(source);
        return writer;
    }

    @Override
    public Chars getId() {
        return null;
    }

}
