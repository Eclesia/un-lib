

package science.unlicense.format.mpeg123.mp3.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class AudioDecoder {

    public abstract byte[] decode(DataInputStream ds, AudioFrameHeader header) throws IOException;

}
