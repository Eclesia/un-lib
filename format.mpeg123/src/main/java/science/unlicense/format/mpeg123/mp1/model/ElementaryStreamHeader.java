
package science.unlicense.format.mpeg123.mp1.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ElementaryStreamHeader extends Block {

    public int packetLength;
    public boolean stbBufferScale;
    public int stdBufferSize;
    public boolean pts;
    public boolean dts;
    //presentation time stamp
    public int pts0;
    public int pts1;
    public int pts2;
    //decoding time stamp
    public int dts0;
    public int dts1;
    public int dts2;

    public void read(DataInputStream ds) throws IOException {
        packetLength = ds.readUShort();

        //TODO stuffing bytes FF, max 16

        int mark = ds.readBits(2);
        if (mark==1){
            stbBufferScale = ds.readBits(1)==1;
            stdBufferSize = ds.readBits(13);
            mark = ds.readBits(2);
        }

        if (mark!=0){
            throw new IOException(ds, "Invalid pack");
        }

        pts = ds.readBits(1)==1;
        dts = ds.readBits(1)==1;

        if (!pts && dts){
            throw new IOException(ds, "Invalid pack");
        }

        if (pts){
            pts0 = ds.readBits(3);
            if (ds.readBits(1)!=1) throw new IOException(ds, "Invalid pack");
            pts1 = ds.readBits(15);
            if (ds.readBits(1)!=1) throw new IOException(ds, "Invalid pack");
            pts2 = ds.readBits(15);
            if (ds.readBits(1)!=1) throw new IOException(ds, "Invalid pack");
        }
        if (dts){
            if (ds.readBits(4)!=1) throw new IOException(ds, "Invalid pack");
            dts0 = ds.readBits(3);
            if (ds.readBits(1)!=1) throw new IOException(ds, "Invalid pack");
            dts1 = ds.readBits(15);
            if (ds.readBits(1)!=1) throw new IOException(ds, "Invalid pack");
            dts2 = ds.readBits(15);
            if (ds.readBits(1)!=1) throw new IOException(ds, "Invalid pack");
        }

        if (!pts && !dts){
            if (ds.readBits(4)!=0xF) throw new IOException(ds, "Invalid pack");
        }
    }

}
