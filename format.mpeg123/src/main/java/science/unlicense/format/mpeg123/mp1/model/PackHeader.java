
package science.unlicense.format.mpeg123.mp1.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class PackHeader extends Block {

    /**
     * System clock reference.
     * bits 32 to 30
     */
    public int scr0;
    /** bits 29 to 15 */
    public int scr1;
    /** bits 14 to 0 */
    public int scr2;
    /** in units of 50 bytes per second */
    public int bitrate;

    public void read(DataInputStream ds) throws IOException {
        if (ds.readBits(4)!=2) throw new IOException(ds, "Invalid pack header");
        scr0 = ds.readBits(3);
        if (ds.readBits(1)!=1) throw new IOException(ds, "Invalid pack header");
        scr1 = ds.readBits(15);
        if (ds.readBits(1)!=1) throw new IOException(ds, "Invalid pack header");
        scr2 = ds.readBits(15);
        if (ds.readBits(2)!=3) throw new IOException(ds, "Invalid pack header");
        bitrate = ds.readBits(22);
        if (ds.readBits(1)!=1) throw new IOException(ds, "Invalid pack header");
    }



}
