

package science.unlicense.format.mpeg123.mp1.model;

import science.unlicense.common.api.CObject;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Mpeg-1/2 stream block.
 *
 * @author Johann Sorel
 */
public class Block extends CObject {

    public void read(DataInputStream ds) throws IOException {
        throw new IOException(ds, "Not supported yet.");
    }

    public void write(DataOutputStream ds) throws IOException{
        throw new IOException(ds, "Not supported yet.");
    }

}
