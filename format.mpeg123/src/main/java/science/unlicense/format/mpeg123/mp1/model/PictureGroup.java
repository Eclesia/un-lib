
package science.unlicense.format.mpeg123.mp1.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class PictureGroup extends Block {

    public boolean dropFrame;
    public int hours;
    public int minutes;
    public int seconds;
    public int picture;
    public boolean closedGop;
    public boolean brokenLink;

    public void read(DataInputStream ds) throws IOException {
        dropFrame = ds.readBits(1)==1;
        hours = ds.readBits(5);
        minutes = ds.readBits(6);
        if (ds.readBits(1)!=1) throw new IOException(ds, "Invalid picture group");
        seconds = ds.readBits(6);
        picture = ds.readBits(6);
        closedGop = ds.readBits(1)==1;
        brokenLink = ds.readBits(1)==1;
        //unused bits
        ds.readBits(5);
    }

}
