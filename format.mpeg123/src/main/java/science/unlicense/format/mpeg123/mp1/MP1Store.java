

package science.unlicense.format.mpeg123.mp1;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;

/**
 *
 * @author Johann Sorel
 */
public class MP1Store extends AbstractStore implements Media {

    public MP1Store(Path input) {
        super(MP1Format.INSTANCE,input);
    }

    @Override
    public Track[] getTracks() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
