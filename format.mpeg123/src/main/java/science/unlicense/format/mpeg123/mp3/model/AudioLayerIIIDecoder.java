

package science.unlicense.format.mpeg123.mp3.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Maths;
import science.unlicense.format.mpeg123.mp3.MP3Constants;

/**
 *
 * Original informations are from :
 * https://github.com/wilkie/djehuty
 *
 * @author Dave Wilkinson (Original author, written in D)
 * @author Johann Sorel
 */
public class AudioLayerIIIDecoder extends AudioDecoder {


    private static final int SBLIMIT = 32;
    private static final int SSLIMIT = 18;

    //decoding variables -------------------------------------------------------
    //current decoding state
    private AudioFrameHeader header;
    private int remainingBytes;
    private int curByte;
    private int curPos;


    // Decoded Huffman Data -- is(i) in the spec
    private final int[][] codedData = new int[SSLIMIT][SBLIMIT];

    // Requantizated Data -- xr(i) in the spec
    private final double[][][] quantizedData = new double[2][SSLIMIT][SBLIMIT];

    // Normalized Data -- lr(i)
    private final double[][][] normalizedData = new double[2][SSLIMIT][SBLIMIT];

    // reordered data -- re(i)
    private final double[][] reorderedData = new double[SSLIMIT][SBLIMIT];

    // anti-aliased hybrid synthesis data -- hybridIn
    private final double[][] hybridData = new double[SSLIMIT][SBLIMIT];

    // data for the polysynth phase -- hybridOut
    private final double[][][] polysynthData = new double[2][SSLIMIT][SBLIMIT];

    // cb_limit -- Number of scalefactor bands for long blocks
    // (block_type != 2). This is a constant, 21, for Layer III
    // in all modes and at all sampling frequencies.
    private static final int cb_limit = 21;

    // cb_limit_short -- Number of scalefactor bands for long
    // blocks (block_type == 2). This is a constant, 12, for
    // Layer III in all modes and at all sampling frequencies.

    private static final int cb_limit_short = 12;

    // scfsi[scfsi_band] -- In Layer III, the scalefactor
    // selection information works similarly to Layers I and
    // II. The main difference is the use of variable
    // scfsi_band to apply scfsi to groups of scalefactors
    // instead of single scalefactors. scfsi controls the
    // use of scalefactors to the granules.

    private final int[][] scfsi = new int[2][cb_limit];

    // part2_3_length[gr] -- This value contains the number of
    // main_data bits used for scalefactors and Huffman code
    // data. Because the length of the side information is
    // always the same, this value can be used to calculate
    // the beginning of the main information for each granule
    // and the position of ancillary information (if used).
    private final int[][] part2_3_length = new int[2][2];

    // part2_length -- This value contains the number of
    // main_data bits used for scalefactors and Huffman code
    // data. Because the length of the side information is
    // always the same, this value can be used to calculate
    // the beginning of the main information for each granule
    // and the position of ancillary information (if used).
    private int part2_length;

    // big_values[gr] -- The spectral values of each granule
    // are coded with different Huffman code tables. The full
    // frequency range from zero to the Nyquist frequency is
    // divided into several regions, which then are coded using
    // different table. Partitioning is done according to the
    // maximum quantized values. This is done with the
    // assumption the values at higher frequencies are expected
    // to have lower amplitudes or don't need to be coded at all.
    // Starting at high frequencies, the pairs of quantized values
    // with absolute value not exceeding 1 (i.e. only 3 possible
    // quantization levels) are counted. This number is named
    // "count1". Again, an even number of values remains. Finally,
    // the number of pairs of values in the region of the spectrum
    // which extends down to zero is named "big_values". The
    // maximum absolute value in this range is constrained to 8191.

    // The figure shows the partitioning:
    // xxxxxxxxxxxxx------------------00000000000000000000000000000
    // |           |                 |                            |
    // 1      bigvalues*2    bigvalues*2+count1*4             iblen
    //
    // The values 000 are all zero
    // The values --- are -1, 0, or +1. Their number is a multiple of 4
    // The values xxx are not bound
    // iblen = 576
    private final int[][] big_values = new int[2][2];

    // global_gain[gr] -- The quantizer step size information is
    // transmitted in the side information variable global_gain.
    // It is logarithmically quantized. For the application of
    // global_gain, refer to the formula in 2.4.3.4 (ISO 11172-3)
    private final int[][] global_gain = new int[2][2];

    // scalefac_compress[gr] -- selects the number of bits used
    // for the transmission of the scalefactors according to the
    // following table:

    // if block_type is 0, 1, or 3:
    // slen1: length of scalefactors for scalefactor bands 0 to 10
    // slen2: length of scalefactors for scalefactor bands 11 to 20

    // if block_type is 2 and switch_point is 0:
    // slen1: length of scalefactors for scalefactor bands 0 to 5
    // slen2: length of scalefactors for scalefactor bands 6 to 11

    // if block_type is 2 and switch_point is 1:
    // slen1: length of scalefactors for scalefactor bands 0 to 7
    //        (long window scalefactor band) and 4 to 5 (short window
    //        scalefactor band)
    // slen2: length of scalefactors for scalefactor bands 6 to 11

    // scalefactor_compress slen1 slen2
    //                    0     0     0
    //                    1     0     1
    //                    2     0     2
    //                    3     0     3
    //                    4     3     0
    //                    5     1     1
    //                    6     1     2
    //                    7     1     3
    //                    8     2     1
    //                    9     2     2
    //                   10     2     3
    //                   11     3     1
    //                   12     3     2
    //                   13     3     3
    //                   14     4     2
    //                   15     4     3
    private final int[][] scalefac_compress = new int[2][2];
    private final int[][] slen1 = new int[2][2];
    private final int[][] slen2 = new int[2][2];
    private static final int[] slen1_interpret = {0, 0, 0, 0, 3, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4};
    private static final int[] slen2_interpret = {0, 1, 2, 3, 0, 1, 2, 3, 1, 2, 3, 1, 2, 3, 2, 3};

    // blocksplit_flag[gr] -- signals that the block uses an other
    // than normal (type 0) window. If blocksplit_flag is set,
    // several other variables are set by default:

    // region_address1 = 8 (in case of block_type == 1 or 3)
    // region_address1 = 9 (in case of block_type == 2)
    // region_address2 = 0 and the length of region 2 is zero

    // If it is not set, the value of block_type is zero
    private final int[][] blocksplit_flag = new int[2][2];

    // block_type[gr] -- indicates the window type for the actual
    // granule (see the description of filterbank)

    // type 0 - reserved
    // type 1 - start block
    // type 2 - 3 short windows
    // type 3 - end block
    private final int[][] block_type = new int[2][2];

    // switch_point[gr] -- signals the split point of short/long
    // transforms. The following table shows the number of the
    // scalefactor band above which window switching (i.e.
    // block_type different from 0) is used.

    // switch_point switch_point_l switch_point_s
    //                (num of sb)    (num of sb)
    // --------------------------------------------
    //       0             0              0
    //       1             8              3
    private final int[][] switch_point = new int[2][2];

    // switch_point_l -- Number of the scalefactor band (long
    // block scalefactor band) from which point on window
    // switching is used.
    private final int[][] switch_point_l = new int[2][2];

    // switch_point_s -- Number of the scalefactor band (short
    // block scalefactor band) from which point on window
    // switching is used.
    private final int[][] switch_point_s = new int[2][2];

    // table_select[region][gr] -- different Huffman code tables
    // are used depending on the maximum quantized value and the
    // local statistics of the signal. There are a total of 32
    // possible tables given in 3-Annex-B Table 3-B.7 (ISO 11172-3)
    private final int[][][] table_select = new int[2][2][3];

    // subblock_gain[window][gr] -- indicates the gain offset
    // (quantization: factor 4) from the global gain for one
    // subblock. Used only with block type 2 (short windows).
    // The values of the subblock have to be divided by 4.
    private final int[][][] subblock_gain = new int[2][2][3];

    // region_address1[gr] -- A further partitioning of the
    // spectrum is used to enhance the performance of the Huffman
    // coder. It is a subdivision of the region which is described
    // by big_values. The purpose of this subdivision is to get
    // better error robustness and better coding efficiency. Three
    // regions are used. Each region is coded using a different
    // Huffman code table depending on the maximum quantized value
    // and the local statistics.

    // The values region_address[1,2] are used to point to the
    // boundaries of the regions. The region boundaries are
    // aligned with the partitioning of the spectrum into critical
    // bands.

    // In the case of block_type == 2 (short blocks) the scalefactor
    // bands representing the different time slots are counted
    // separately. If switch_point == 0, the total amount of scalefactor
    // bands for the granule in this case is 12*3=36. If block_type == 2
    // and switch_point == 1, the amount of scalefactor bands is
    // 8+9*3=35. region_address1 counts the number of scalefactor bands
    // until the upper edge of the first region:

    // region_address1   upper_edge of region is upper edge
    //                      of scalefactor band number:
    //               0   0 (no first region)
    //               1   1
    //               2   2
    //             ...   ...
    //              15   15
    private final int[][] region_address1 = new int[2][2];

    // region_address2[gr] -- counts the number of scalefactor bands
    // which are partially or totally in region 3. Again, if
    // block_type == 2, then the scalefactor bands representing
    // different time slots are counted separately.
    private final int[][] region_address2 = new int[2][2];

    // preflag[gr] -- This is a shortcut for additional high
    // frequency amplification of the quantized values. If preflag
    // is set, the values of a table are added to the scalefactors
    // (see 3-Annex B, Table 3-B.6 ISO 11172-3). This is equivalent
    // to multiplication of the requantized scalefactors with table
    // values. preflag is never used if block_type == 2 (short blocks)
    private final int[][] preflag = new int[2][2];

    // scalefac_scale[gr] -- The scalefactors are logarithmically
    // quantized with a step size of 2 or sqrt(2) depending on
    // the value of scalefac_scale:

    // scalefac_scale = 0 ... stepsize = sqrt(2)
    // scalefac_scale = 1 ... stepsize = 2
    private final int[][] scalefac_scale = new int[2][2];

    // count1table_select[gr] -- This flag selects one of two
    // possible Huffman code tables for the region of
    // quadruples of quantized values with magnitude not
    // exceeding 1. (ref to ISO 11172-3)

    // count1table_select = 0 .. Table A of 3-Annex B.7
    // count1table_select = 1 .. Table B of 3-Annex B.7
    private final int[][] count1table_select = new int[2][2];

    // scalefac[cb][gr] -- The scalefactors are used to color
    // the quantization noise. If the quantization noise is
    // colored with the right shape, it is masked completely.
    // Unlike Layers I and II, the Layer III scalefactors say
    // nothing about the local maximum of the quantized signal.
    // In Layer III, the blocks stretch over several frequency
    // lines. These blocks are called scalefactor bands and are
    // selected to resemble critical bands as closely as possible.

    // The scalefac_compress table shows that scalefactors 0-10
    // have a range of 0 to 15 (maximum length 4 bits) and the
    // scalefactors 11-21 have a range of 0 to 7 (3 bits).

    // If intensity_stereo is enabled (modebit_extension) the
    // scalefactors of the "zero_part" of the difference (right)
    // channel are used as intensity_stereo positions

    // The subdivision of the spectrum into scalefactor bands is
    // fixed for every block length and sampling frequency and
    // stored in tables in the coder and decoder. (3-Annex 3-B.8)

    // The scalefactors are logarithmically quantized. The
    // quantization step is set with scalefac_scale.

    // scalefac[cb][window][gr] -- same as scalefac[cb][gr] but for
    // different windows when block_type == 2.
    private static class ScaleFactor {
        private final int[][] short_window = new int[3][13];
        private final int[] long_window = new int[23];
    }

    private final ScaleFactor[][] scalefac = new ScaleFactor[2][2];

    public byte[] decode(DataInputStream ds, AudioFrameHeader header) throws IOException {
        scalefac[0][0] = new ScaleFactor();
        scalefac[0][1] = new ScaleFactor();
        scalefac[1][0] = new ScaleFactor();
        scalefac[1][1] = new ScaleFactor();


        this.header = header;
        remainingBytes = header.frameLength;
        final int audioHeaderLength = header.numChannels==1 ? 17 : 32;

        final int main_data_end = ds.readBits(9);
        //skip private bits
        ds.readBits((header.numChannels == 1) ? 5 : 3);

        for (int ch=0; ch<header.numChannels; ch++){
            ds.readBits(scfsi[ch], 0, 4, 1);
        }

        for (int gr=0; gr<2; gr++){
            for (int ch=0; ch<header.numChannels; ch++){

                part2_3_length[ch][gr]    = ds.readBits(12);
                big_values[ch][gr]        = ds.readBits(9);
                global_gain[ch][gr]       = ds.readBits(8);
                scalefac_compress[ch][gr] = ds.readBits(4);
                blocksplit_flag[ch][gr]   = ds.readBits(1);

                slen1[ch][gr] = slen1_interpret[scalefac_compress[ch][gr]];
                slen2[ch][gr] = slen2_interpret[scalefac_compress[ch][gr]];

                if (blocksplit_flag[ch][gr]==1) {
                    block_type[ch][gr]   = ds.readBits(2);
                    switch_point[ch][gr] = ds.readBits(1);
                    ds.readBits(table_select[ch][gr], 0, 2, 5);

                    // window -- Number of actual time slot in case of
                    // block_type == 2, 0 = window = 2.
                    ds.readBits(subblock_gain[ch][gr], 0, 3, 3);

                    if (block_type[ch][gr] == 2 && switch_point[ch][gr] == 0) {
                        region_address1[ch][gr] = 8;
                    } else {
                        region_address1[ch][gr] = 7;
                    }

                    region_address2[ch][gr] = 20 - region_address1[ch][gr];

                    if (switch_point[ch][gr] == 1) {
                        switch_point_l[ch][gr] = 8;
                        switch_point_s[ch][gr] = 3;
                    } else {
                        switch_point_l[ch][gr] = 0;
                        switch_point_s[ch][gr] = 0;
                    }
                } else {
                    block_type[ch][gr] = 0;
                    ds.readBits(table_select[ch][gr], 0, 3, 5);
                    region_address1[ch][gr] = ds.readBits(4);
                    region_address2[ch][gr] = ds.readBits(3);
                    switch_point[ch][gr]    = 0;
                    switch_point_l[ch][gr]  = 0;
                    switch_point_s[ch][gr]  = 0;
                }

                preflag[ch][gr]            = ds.readBits(1);
                scalefac_scale[ch][gr]     = ds.readBits(1);
                count1table_select[ch][gr] = ds.readBits(1);
            }
        }

        remainingBytes -= audioHeaderLength;

        // read in the data
        //byte[] audioData = ds.readFully(new byte[remainingBytes]);


        for (int gr=0; gr<2; gr++){
            for (int ch=0; ch<header.numChannels; ch++){

                //part2_length = (curByte * 8) + curPos;

                // Read the scalefactors for this granule
                readScalefactors(ds, ch, gr);

                //part2_length = ((curByte * 8) + curPos) - part2_length;

                // Decode the current huffman data
                decodeHuffman(ds, ch, gr);

                // Requantize
                requantizeSample(gr, ch);
                // ro[ch] == quantizedData[ch]
                for (int sb = 0; sb < SBLIMIT; sb++) {
                    for (int ss = 0; ss < SSLIMIT; ss++) {
                        //printf("%f\n", quantizedData[ch][sb][ss]);
                    }
                }
            }

            // Account for a mode switch from intensity stereo to MS_stereo
            normalizeStereo(gr);
            for (int ch=0; ch<header.numChannels; ch++){
                for (int sb=0; sb<SBLIMIT; sb++){
                    for (int ss = 0; ss < SSLIMIT; ss++){
                        //if (normalizedData[ch][sb][ss] > 0.0) {
                        //    printf("%f\n", normalizedData[ch][sb][ss]);
                        //}
                    }
                }
            }

            for (int ch = 0; ch < header.numChannels; ch++) {
                // Reorder the short blocks
                reorder(gr, ch);
                for (int sb = 0; sb < SBLIMIT; sb++) {
                    for (int ss = 0; ss < SSLIMIT; ss++) {
                        //if (reorderedData[sb][ss] > 0.0) {
                                //printf("%f\n", reorderedData[sb][ss]);
                        //}
                    }
                }

                // Perform anti-alias pass on subband butterflies
                antialias(gr, ch);
                for (int sb=0; sb<SBLIMIT; sb++){
                    for (int ss = 0; ss < SSLIMIT; ss++) {
                    //    if (hybridData[sb][ss] > 0.0) {
                                    //printf("%f\n", hybridData[sb][ss]);
                    //    }
                    }
                }

                // Perform hybrid synthesis pass
                for (int sb=0; sb<SBLIMIT; sb++){
                    hybridSynthesis(gr, ch, sb);
                }

                // Multiply every second subband's every second input by -1
                // To correct for frequency inversion of the polyphase filterbank
                for (int sb=0; sb<SBLIMIT; sb++){
                    for (int ss=0; ss < SSLIMIT; ss++) {
                        if (((ss % 2) == 1) && ((sb % 2) == 1)) {
                            polysynthData[ch][sb][ss] = -polysynthData[ch][sb][ss];
                        }
                    }
                }

                for (int sb=0; sb<SBLIMIT; sb++){
                    for (int ss=0; ss<SSLIMIT; ss++){
                        //if (polysynthData[ch][sb][ss] > 0.0) {
                        //    printf("%f\n", polysynthData[ch][sb][ss]);
                        //}
                    }
                }
            }

//            for (int ss=0; ss<18; ss++){
//                polyphaseSynthesis(gr, ss, toBuffer);
//            }
        }

        return null;
    }


    private void readScalefactors(final DataInputStream ds, final int ch, final int gr) throws IOException {
        if (blocksplit_flag[ch][gr] == 1 && block_type[ch][gr] == 2) {
            if (switch_point[ch][gr] == 0) {

                // Decoding scalefactors for a short window.
                for (int cb=0; cb<6; cb++){
                    for (int window=0; window<3; window++){
                        scalefac[ch][gr].short_window[cb][window] = ds.readBits(slen1[ch][gr]);
                    }
                }

                for (int cb=6; cb<cb_limit_short; cb++){
                    for (int window = 0; window < 3; window++) {
                        scalefac[ch][gr].short_window[cb][window] = ds.readBits(slen2[ch][gr]);
                    }
                }

                for (int window=0; window<3; window++){
                    scalefac[ch][gr].short_window[12][window] = 0;
                }
            } else {

                // Decoding scalefactors for a long window with a switch point to short.
                for (int cb=0; cb< 8; cb++){
                    if ((scfsi[cb][ch] == 0) || (gr == 0)) {
                        scalefac[ch][gr].long_window[cb] = ds.readBits(slen1[ch][gr]);
                    }
                }

                for (int cb = 3; cb < 6; cb++) {
                    for (int window = 0; window < 3; window++) {
                        if ((scfsi[cb][ch] == 0) || (gr == 0)) {
                            scalefac[ch][gr].short_window[cb][window] = ds.readBits(slen1[ch][gr]);
                        }
                    }
                }

                for (int cb = 6; cb < cb_limit_short; cb++) {
                    for (int window = 0; window < 3; window++) {
                        if ((scfsi[cb][ch] == 0) || (gr == 0)) {
                            scalefac[ch][gr].short_window[cb][window] = ds.readBits(slen2[ch][gr]);
                        }
                    }
                }

                for (int window = 0; window < 3; window++) {
                    if ((scfsi[cb_limit_short][ch] == 0) || (gr == 0)) {
                        scalefac[ch][gr].short_window[cb_limit_short][window] = 0;
                    }
                }
            }
        } else {
            // The block_type cannot be 2 in this block (so, it must be block 0, 1, or 3).

            // Decoding the scalefactors for a long window

            if ((scfsi[0][ch] == 0) || (gr == 0)) {
                for (int cb = 0; cb < 6; cb++) {
                    scalefac[ch][gr].long_window[cb] = ds.readBits(slen1[ch][gr]);
                }
            }

            if ((scfsi[1][ch] == 0) || (gr == 0)) {
                for (int cb = 6; cb < 11; cb++) {
                    scalefac[ch][gr].long_window[cb] = ds.readBits(slen1[ch][gr]);
                }
            }

            if ((scfsi[2][ch] == 0) || (gr == 0)) {
                for (int cb = 11; cb < 16; cb++) {
                    scalefac[ch][gr].long_window[cb] = ds.readBits(slen2[ch][gr]);
                }
            }

            if ((scfsi[3][ch] == 0) || (gr == 0)) {
                for (int cb = 16; cb < 21; cb++) {
                    scalefac[ch][gr].long_window[cb] = ds.readBits(slen2[ch][gr]);
                }
            }

            // (The reference implementation does nothing with subband 21)

            // We fill it with a high negative integer:
            scalefac[ch][gr].long_window[21] = -858993460;

            scalefac[ch][gr].long_window[22] = 0;
        }
    }


    private void decodeHuffman(final DataInputStream ds, int ch, int gr) throws IOException{
        // part2_3_length is the length of all of the data
        // (huffman + scalefactors). part2_length is just
        // the scalefactors by themselves.

        // Note: SSLIMIT * SBLIMIT = 32 * 18 = 576

        final int max_table_entry = 15;

        int region1Start;
        int region2Start;

        if (blocksplit_flag[gr][ch] == 1 && block_type[gr][ch] == 2) {
                // Short Blocks
                region1Start = 36;
                region2Start = 576; // There isn't a region 2 for short blocks
        }
        else {
                // Long Blocks
                region1Start = MP3Constants.sfindex_long[header.frequency][region_address1[gr][ch] + 1];
                region2Start = MP3Constants.sfindex_long[header.frequency][region_address1[gr][ch] + region_address2[gr][ch] + 2];
        }

        int maxBand = big_values[gr][ch] * 2;

        if (region1Start > maxBand) { region1Start = maxBand; }
        if (region2Start > maxBand) { region2Start = maxBand; }

        int freqIndex = 0;

        int pos = curByte;
        int posbit = curPos;

        // The number of bits used for the huffman data
        int huffmanLength = (part2_3_length[gr][ch] - part2_length);

        // The bit position in the stream to stop.
        int maxBit = huffmanLength + curPos + (curByte * 8);

        // Region 0
        if (freqIndex < region1Start) {
            initializeHuffman(0,gr,ch);
        }

        for (; freqIndex < region1Start; freqIndex+=2) {
            final int[] code = readCode(ds);
            codedData[freqIndex/SSLIMIT][freqIndex%SSLIMIT] = code[0];
            codedData[(freqIndex+1)/SSLIMIT][(freqIndex+1)%SSLIMIT] = code[1];
        }

        // Region 1
        if (freqIndex < region2Start) {
            initializeHuffman(1,gr,ch);
        }

        for (; freqIndex < region2Start; freqIndex+=2) {
            final int[] code = readCode(ds);
            codedData[freqIndex/SSLIMIT][freqIndex%SSLIMIT] = code[0];
            codedData[(freqIndex+1)/SSLIMIT][(freqIndex+1)%SSLIMIT] = code[1];
        }

        // Region 2
        if (freqIndex < maxBand) {
            initializeHuffman(2,gr,ch);
        }

        for (; freqIndex < maxBand; freqIndex+=2) {
            final int[] code = readCode(ds);
            codedData[freqIndex/SSLIMIT][freqIndex%SSLIMIT] = code[0];
            codedData[(freqIndex+1)/SSLIMIT][(freqIndex+1)%SSLIMIT] = code[1];
        }

        // Read in Count1 Area
        initializeQuantizationHuffman(gr,ch);

        for (; (curPos + (curByte * 8)) < maxBit && freqIndex < 574; freqIndex += 4) {
            final int[] code = readQuantizationCode(ds);
            codedData[freqIndex/SSLIMIT][freqIndex%SSLIMIT] = code[0];
            codedData[(freqIndex+1)/SSLIMIT][(freqIndex+1)%SSLIMIT] = code[1];
            codedData[(freqIndex+2)/SSLIMIT][(freqIndex+2)%SSLIMIT] = code[2];
            codedData[(freqIndex+3)/SSLIMIT][(freqIndex+3)%SSLIMIT] = code[3];
        }

        // Zero rest
        for (; freqIndex < 576; freqIndex++) {
            codedData[freqIndex/SSLIMIT][freqIndex%SSLIMIT] = 0;
        }

        // Resync to the correct position
        // (where we started + the number of bits that would have been used)
        curByte = maxBit / 8;
        curPos = maxBit % 8;
    }


    private int[][] curTable;
    private int[] curValues;
    private int linbits;

    // Select the table to use for decoding and reset state.
    void initializeHuffman(int region, int gr, int ch) {
        int tableIndex = table_select[region][gr][ch];

        switch (tableIndex) {
            case 16: linbits = 1;  break;
            case 17: linbits = 2;  break;
            case 18: linbits = 3;  break;
            case 24:
            case 19: linbits = 4;  break;
            case 25: linbits = 5;  break;
            case 26:
            case 20: linbits = 6;  break;
            case 27: linbits = 7;  break;
            case 28:
            case 21: linbits = 8;  break;
            case 29: linbits = 9;  break;
            case 22: linbits = 10; break;
            case 30: linbits = 11; break;
            case 31:
            case 23: linbits = 13; break;
            default: linbits = 0;  break;
        }

        if (tableIndex >= 24) {
            tableIndex = 17;
        } else if (tableIndex >= 16) {
            tableIndex = 16;
        }

        // XXX: This silliness is due to a compiler bug in DMD 1.046
        if (tableIndex == 17) {
            curTable = MP3Constants.huffmanTable24;
        } else {
            curTable = MP3Constants.huffmanTables[tableIndex];
        }
        curValues = MP3Constants.huffmanValues[tableIndex];
    }

    private int[] readCode(DataInputStream ds) throws IOException {
        int code=0;
        int bitlength=0;
        int valoffset=0;

        for (;;) {
            code <<= 1;
            code |= ds.readBits(1);

            if (bitlength > curTable.length) {
                break;
            }

            for (int i=0;i<curTable[bitlength].length;i++) {
                int foo = curTable[bitlength][i];
                if (foo == code) {
                    // found code

                    // get value offset
                    valoffset += i;
                    valoffset *= 2;

                    int[] values = new int[]{curValues[valoffset], curValues[valoffset+1]};

                    // read linbits (x)
                    if (linbits > 0 && values[0] == 15) {
                        values[0] += ds.readBits(linbits);
                    }

                    if (values[0] > 0) {
                        if (ds.readBits(1) == 1) {
                            values[0] = -values[0];
                        }
                    }

                    if (linbits > 0 && values[1] == 15) {
                        values[1] += ds.readBits(linbits);
                    }

                    if (values[1] > 0) {
                        if (ds.readBits(1) == 1) {
                            values[1] = -values[1];
                        }
                    }

                    return values;
                }
            }

            valoffset += curTable[bitlength].length;
            bitlength++;
            if (bitlength >= curTable.length) {
                return new int[]{128, 128};
            }
        }

        return new int[]{128, 128};
    }

    private int curCountTable;

    private void initializeQuantizationHuffman(int ch, int gr) {
        curCountTable = count1table_select[ch][gr];
    }

    private int[] readQuantizationCode(DataInputStream ds) throws IOException {
        final int code = decodeQuantization(ds);

        int v = ((code >> 3) & 1);
        int w = ((code >> 2) & 1);
        int x = ((code >> 1) & 1);
        int y = (code & 1);

        // Get Sign Bits (for non-zero values)
        if (v > 0 && ds.readBits(1) == 1) {
            v = -v;
        }
        if (w > 0 && ds.readBits(1) == 1) {
            w = -w;
        }
        if (x > 0 && ds.readBits(1) == 1) {
            x = -x;
        }
        if (y > 0 && ds.readBits(1) == 1) {
            y = -y;
        }

        return new int[]{v,w,x,y};
    }

    private int decodeQuantization(DataInputStream ds) throws IOException {
        int code=0;

        if (curCountTable == 1) {
            // Quantization Huffman Table B is trivial...
            // It is simply the bitwise negation of 4 bits from the stream.
            // code = ~readBits(4);
            code = ds.readBits(4);
            code = ~code;
        } else {
            // Quantization Huffman Table A is the only case,
            // so it is written here by hand:

            // state 1
            code = ds.readBits(1);

            if (code == 1) { //0b1
                return 0; //0b0000
            }

            // state 2
            code = ds.readBits(3);

            if (code >= 4 && code <= 7) { // 0b0100  0b0111
                int idx = code - 4; // 0b0100
                // 0b0010  0b0101  0b0110  0b0111
                final int[] stage2_values = {2, 5, 6, 7};
                return stage2_values[idx];
            }

            // state 3
            code <<= 1;
            code |= ds.readBits(1);

            if (code >= 3 && code <= 7) { // 0b00011  0b00111
                int idx = code - 3; // 0b00011
                // 0b1001   0b0110  0b0011  0b1010  0b1100
                final int[] stage3_values = {9, 6, 3, 10, 12};
                return stage3_values[idx];
            }

            // state 4
            code <<= 1;
            code |= ds.readBits(1);

            if (code <= 5) { // 0b000101
                // 0b1011  0b1111  0b1101  0b1110  0b0111  0b0101
                final int[] stage4_values = {11, 15, 13, 14, 7, 5};
                return stage4_values[code];
            }

            // invalid code;
            code = 0;
        }

        return code;
    }

    private void requantizeSample(int ch, int gr) {
        int criticalBandBegin=0;
        int criticalBandWidth=0;
        int criticalBandBoundary=0;
        int criticalBandIndex=0;

        final int[] pretab = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 3, 3, 3, 2, 0};

        // Initialize the critical boundary information
        if ((blocksplit_flag[gr][ch] == 1) && (block_type[gr][ch] == 2)) {
            if (switch_point[gr][ch] == 0) {
                // Short blocks
                criticalBandBoundary = MP3Constants.sfindex_short[header.frequency][1] * 3;
                criticalBandWidth = MP3Constants.sfindex_short[header.frequency][1];
                criticalBandBegin = 0;
            } else {
                // Long blocks come first for switched windows
                criticalBandBoundary = MP3Constants.sfindex_long[header.frequency][1];
            }
        } else {
            // Long windows
            criticalBandBoundary = MP3Constants.sfindex_long[header.frequency][1];
        }

        for (int sb=0; sb < SBLIMIT; sb++) {
            for (int ss=0; ss < SSLIMIT; ss++) {

                // Get the critical band boundary
                if ((sb * 18) + ss == criticalBandBoundary) {
                    if (blocksplit_flag[gr][ch] == 1 && block_type[gr][ch] == 2) {
                        if (switch_point[gr][ch] == 0) {
                            // Requantizing the samples for a short window.
                            criticalBandIndex++;
                            criticalBandBoundary = MP3Constants.sfindex_short[header.frequency][criticalBandIndex+1]*3;
                            criticalBandWidth = MP3Constants.sfindex_short[header.frequency][criticalBandIndex + 1] - MP3Constants.sfindex_short[header.frequency][criticalBandIndex];
                            criticalBandBegin = MP3Constants.sfindex_short[header.frequency][criticalBandIndex] * 3;
                        } else {
                            // Requantizing the samples for a long window that switches to short.

                            // The first two are long windows and the last two are short windows
                            if (((sb * 18) + ss) == MP3Constants.sfindex_long[header.frequency][8]) {
                                criticalBandBoundary = MP3Constants.sfindex_short[header.frequency][4] * 3;
                                criticalBandIndex = 3;
                                criticalBandWidth = MP3Constants.sfindex_short[header.frequency][criticalBandIndex + 1] - MP3Constants.sfindex_short[header.frequency][criticalBandIndex];
                                criticalBandBegin = MP3Constants.sfindex_short[header.frequency][criticalBandIndex] * 3;
                            } else if (((sb * 18) + ss) < MP3Constants.sfindex_long[header.frequency][8]) {
                                criticalBandIndex++;
                                criticalBandBoundary = MP3Constants.sfindex_long[header.frequency][criticalBandIndex+1];
                            } else {
                                criticalBandIndex++;
                                criticalBandBoundary = MP3Constants.sfindex_short[header.frequency][criticalBandIndex + 1] * 3;
                                criticalBandWidth = MP3Constants.sfindex_short[header.frequency][criticalBandIndex + 1] - MP3Constants.sfindex_short[header.frequency][criticalBandIndex];
                                criticalBandBegin = MP3Constants.sfindex_short[header.frequency][criticalBandIndex] * 3;
                            }
                        }
                    } else {
                        // The block_type cannot be 2 in this block (so, it must be block 0, 1, or 3).

                        // Requantizing the samples for a long window
                        criticalBandIndex++;
                        criticalBandBoundary = MP3Constants.sfindex_long[header.frequency][criticalBandIndex+1];
                    }
                }

                // Global gain
                quantizedData[ch][sb][ss] = Math.pow(2.0, (0.25 * ((double) global_gain[gr][ch] - 210.0)));
                //printf("g : %d %d: %f\n", sb,ss,quantizedData[ch][sb][ss]);
                boolean output = false;
                // Perform the scaling that depends on the type of window
                if (blocksplit_flag[gr][ch] == 1
                    && (((block_type[gr][ch] == 2) && (switch_point[gr][ch] == 0))
                    || ((block_type[gr][ch] == 2) && (switch_point[gr][ch] == 1) && (sb >= 2)))) {

                    // Short blocks (either via block_type 2 or the last 2 bands for switched windows)

                    int sbgainIndex = (((sb * 18) + ss) - criticalBandBegin) / criticalBandWidth;

                    // if (output) printf("%d %d\n", sbgainIndex, criticalBandIndex);
                    quantizedData[ch][sb][ss] *= Math.pow(2.0, 0.25 * -8.0
                            * subblock_gain[sbgainIndex][gr][ch]);
                    quantizedData[ch][sb][ss] *= Math.pow(2.0, 0.25 * -2.0 * (1.0 + scalefac_scale[gr][ch])
                            * scalefac[gr][ch].short_window[criticalBandIndex][sbgainIndex]);
                } else {
                    // Long blocks (either via block_type 0, 1, or 3, or the 1st 2 bands
                    double powExp = -0.5 * (1.0 + (double) scalefac_scale[gr][ch])
                            * ((double) scalefac[gr][ch].long_window[criticalBandIndex]
                            + ((double) preflag[gr][ch] * (double) pretab[criticalBandIndex]));
                    double powResult = Math.pow(2.0, powExp);
                            //if (powResult > 0.0) {
                                    //printf("r : %f\nfrom : %f\n", powResult, powExp);
                                    //printf("with : %f %d [%d, %d, %d] %f %f\n", cast(double) scalefac_scale[gr][ch], scalefac[gr][ch].long_window[criticalBandIndex], gr, ch, criticalBandIndex,
                            //    cast(double) preflag[gr][ch], cast(double) pretab[criticalBandIndex]);
                            //}
                    quantizedData[ch][sb][ss] *= powResult;
                }

                // Scale values

                double powResult = Math.pow((double) Math.abs(codedData[sb][ss]), 4.0/3.0);
        //    printf("%f\n", powResult);

                quantizedData[ch][sb][ss] *= powResult;
                if (codedData[sb][ss] < 0) {
                    quantizedData[ch][sb][ss] = -quantizedData[ch][sb][ss];
                }
//printf("%d %d: [%d] %.21f\n", sb,ss, codedData[sb][ss], quantizedData[ch][sb][ss]);
            }
        }
    }

    private void normalizeStereo(int gr) {

        double io;
        if ((scalefac_compress[gr][0] % 2) == 1) {
            io = 0.707106781188;
        } else {
            io = 0.840896415256;
        }

        final short[] decodedPos = new short[576];
        final double[] decodedRatio = new double[576];
        final double[][] k = new double[576][2];

        int i=0;
        int sb=0;
        int ss=0;
        int ch=0;

        int scalefactorBand;

        // Initialize
        Arrays.fill(decodedPos, (short) 7);

        boolean intensityStereo = (header.mode == MP3Constants.MODE_JOINT_STEREO) && ((header.modeExt & 0x1) !=0);
        boolean msStereo = (header.mode == MP3Constants.MODE_JOINT_STEREO) && ((header.modeExt & 0x2) !=0);

        if ((header.numChannels == 2) && intensityStereo) {
            if ((blocksplit_flag[gr][ch] == 1) && (block_type[gr][ch] == 2)) {
                if (switch_point[gr][ch] == 0) {
                    for (int j = 0; j < 3; j++) {
                        int scalefactorCount = -1;

                        for (scalefactorBand = 12; scalefactorBand >= 0; scalefactorBand--) {
                            int lines = MP3Constants.sfindex_short[header.frequency][scalefactorBand + 1]
                                    - MP3Constants.sfindex_short[header.frequency][scalefactorBand];

                            i = 3 * MP3Constants.sfindex_short[header.frequency][scalefactorBand]
                                    + ((j + 1) * lines) - 1;

                            for (; lines > 0; lines--) {
                                if (quantizedData[1][i/SSLIMIT][i%SSLIMIT] != 0.0) {
                                    scalefactorCount = scalefactorBand;
                                    scalefactorBand = -10;
                                    lines = -10;
                                }
                                i--;
                            }
                        }

                        scalefactorBand = scalefactorCount + 1;

                        for (; scalefactorBand < 12; scalefactorBand++) {
                            sb = MP3Constants.sfindex_short[header.frequency][scalefactorBand+1]
                                    - MP3Constants.sfindex_short[header.frequency][scalefactorBand];

                            i = 3 * MP3Constants.sfindex_short[header.frequency][scalefactorBand] + (j * sb);

                            for ( ; sb > 0; sb--) {
                                decodedPos[i] = (short) scalefac[gr][1].short_window[scalefactorBand][j];
                                if (decodedPos[i] != 7) {
                                    // IF (MPEG2) { ... }
                                    // ELSE {
                                        decodedRatio[i] = Math.tan((double) decodedPos[i] * (Maths.PI / 12));
                                    // }
                                }
                                i++;
                            }
                        }

                        sb = MP3Constants.sfindex_short[header.frequency][12] - MP3Constants.sfindex_short[header.frequency][11];
                        scalefactorBand = (3 * MP3Constants.sfindex_short[header.frequency][11]) + (j * sb);
                        sb = MP3Constants.sfindex_short[header.frequency][13] - MP3Constants.sfindex_short[header.frequency][12];

                        i = (3 * MP3Constants.sfindex_short[header.frequency][11]) + (j * sb);

                        for (; sb > 0; sb--) {
                            decodedPos[i] = decodedPos[scalefactorBand];
                            decodedRatio[i] = decodedRatio[scalefactorBand];
                            k[0][i] = k[0][scalefactorBand];
                            k[1][i] = k[1][scalefactorBand];
                            i++;
                        }
                    }
                } else {
                    int maxScalefactorBand=0;

                    for (int j=0; j<3; j++) {
                        int scalefactorCount = 2;
                        for (scalefactorBand = 12; scalefactorBand >= 3; scalefactorBand--) {
                            int lines = MP3Constants.sfindex_short[header.frequency][scalefactorBand+1]
                                    - MP3Constants.sfindex_short[header.frequency][scalefactorBand];

                            i = 3 * MP3Constants.sfindex_short[header.frequency][scalefactorBand]
                                    + ((j + 1) * lines) - 1;

                            for (; lines > 0; lines--) {
                                if (quantizedData[1][i/SSLIMIT][i%SSLIMIT] != 0.0) {
                                    scalefactorCount = scalefactorBand;
                                    scalefactorBand = -10;
                                    lines = -10;
                                }
                                i--;
                            }
                        }

                        scalefactorBand = scalefactorCount + 1;

                        if (scalefactorBand > maxScalefactorBand) {
                                maxScalefactorBand = scalefactorBand;
                        }

                        for (; scalefactorBand < 12; scalefactorBand++) {
                            sb = MP3Constants.sfindex_short[header.frequency][scalefactorBand+1]
                                    - MP3Constants.sfindex_short[header.frequency][scalefactorBand];

                            i = 3 * MP3Constants.sfindex_short[header.frequency][scalefactorBand]
                                    + (j * sb);

                            for (; sb > 0; sb--) {
                                decodedPos[i] = (short) scalefac[gr][1].short_window[scalefactorBand][j];
                                if (decodedPos[i] != 7) {
                                    // IF (MPEG2) { ... }
                                    // ELSE {
                                    decodedRatio[i] = Math.tan((double) decodedPos[i] * (Maths.PI / 12.0));
                                    // }
                                }
                                i++;
                            }
                        }

                        sb = MP3Constants.sfindex_short[header.frequency][12]
                                        - MP3Constants.sfindex_short[header.frequency][11];
                        scalefactorBand = 3 * MP3Constants.sfindex_short[header.frequency][11]
                                        + j * sb;
                        sb = MP3Constants.sfindex_short[header.frequency][13]
                                        - MP3Constants.sfindex_short[header.frequency][12];

                        i = 3 * MP3Constants.sfindex_short[header.frequency][11] + j * sb;
                        for (; sb > 0; sb--) {
                            decodedPos[i] = decodedPos[scalefactorBand];
                            decodedRatio[i] = decodedRatio[scalefactorBand];
                            k[0][i] = k[0][scalefactorBand];
                            k[1][i] = k[1][scalefactorBand];
                            i++;
                        }
                    }

                    if (maxScalefactorBand <= 3) {
                        i = 2;
                        ss = 17;
                        sb = -1;
                        while (i >= 0) {
                            if (quantizedData[1][i][ss] != 0.0) {
                                sb = (i * 18) + ss;
                                i = -1;
                            } else {
                                ss--;
                                if (ss < 0) {
                                    i--;
                                    ss = 17;
                                }
                            }
                        }

                        i = 0;
                        while (MP3Constants.sfindex_long[header.frequency][i] <= sb) {
                            i++;
                        }

                        scalefactorBand = i;
                        i = MP3Constants.sfindex_long[header.frequency][i];

                        for (; scalefactorBand < 8; scalefactorBand++) {
                            sb = MP3Constants.sfindex_long[header.frequency][scalefactorBand+1]
                                            - MP3Constants.sfindex_long[header.frequency][scalefactorBand];
                            for (; sb > 0; sb--) {
                                decodedPos[i] = (short) scalefac[gr][1].long_window[scalefactorBand];
                                if (decodedPos[i] != 7) {
                                    // IF (MPEG2) { ... }
                                    // ELSE {
                                        decodedRatio[i] = Math.tan((double) decodedPos[i] * (Maths.PI / 12.0));
                                    // }
                                }
                                i++;
                            }
                        }
                    }
                }
            } else {
                i = 31;
                ss = 17;
                sb = 0;

                while (i >= 0) {
                    if (quantizedData[1][i][ss] != 0.0) {
                        sb = (i * 18) + ss;
                        i = -1;
                    } else {
                        ss--;
                        if (ss < 0) {
                            i--;
                            ss = 17;
                        }
                    }
                }
                i = 0;

                while (MP3Constants.sfindex_long[header.frequency][i] <= sb) {
                    i++;
                }

                scalefactorBand = i;
                i = MP3Constants.sfindex_long[header.frequency][i];

                for (; scalefactorBand < 21; scalefactorBand++) {
                    sb = MP3Constants.sfindex_long[header.frequency][scalefactorBand+1]
                        - MP3Constants.sfindex_long[header.frequency][scalefactorBand];

                    for (; sb > 0; sb--) {
                        decodedPos[i] = (short) scalefac[gr][1].long_window[scalefactorBand];
                        if (decodedPos[i] != 7) {
                            // IF (MPEG2) { ... }
                            // ELSE {
                            decodedRatio[i] = Math.tan((double) decodedPos[i] * (Maths.PI / 12.0));
                            // }
                        }
                        i++;
                    }
                }

                scalefactorBand = MP3Constants.sfindex_long[header.frequency][20];

                for (sb = 576 - MP3Constants.sfindex_long[header.frequency][21]; sb > 0; sb--) {
                    decodedPos[i] = decodedPos[scalefactorBand];
                    decodedRatio[i] = decodedRatio[scalefactorBand];
                    k[0][i] = k[0][scalefactorBand];
                    k[1][i] = k[1][scalefactorBand];
                    i++;
                }
            }
        }

        for (ch = 0; ch < 2; ch++) {
            for (sb = 0; sb < SBLIMIT; sb++) {
                for (ss = 0; ss < SSLIMIT; ss++) {
                    normalizedData[ch][sb][ss] = 0;
                }
            }
        }

        if (header.numChannels == 2) {
            for (sb = 0; sb < SBLIMIT; sb++) {
                for (ss = 0; ss < SSLIMIT; ss++) {
                    i = (sb * 18) + ss;
                    if (decodedPos[i] == 7) {
                        if (msStereo) {
                            normalizedData[0][sb][ss] = (quantizedData[0][sb][ss] + quantizedData[1][sb][ss])
                                    / 1.41421356;
                            normalizedData[1][sb][ss] = (quantizedData[0][sb][ss] - quantizedData[1][sb][ss])
                                    / 1.41421356;
                        } else {
                            normalizedData[0][sb][ss] = quantizedData[0][sb][ss];
                            normalizedData[1][sb][ss] = quantizedData[1][sb][ss];
                        }
                    } else if (intensityStereo) {
                        // IF (MPEG2) {
                        // normalizedData[0][sb][ss] = quantizedData[0][sb][ss] * k[0][i];
                        // normalizedData[1][sb][ss] = quantizedData[0][sb][ss] * k[1][i];
                        // }
                        // ELSE {
                        normalizedData[0][sb][ss] = quantizedData[0][sb][ss] * (decodedRatio[i] / (1 + decodedRatio[i]));
                        normalizedData[1][sb][ss] = quantizedData[0][sb][ss] * (1 / (1 + decodedRatio[i]));
                        // }
                    } else {
                        // Error ...
                    }
                }
            }
        } else { // Mono
            for (sb = 0; sb < SBLIMIT; sb++) {
                for (ss = 0; ss < SSLIMIT; ss++) {
                    normalizedData[0][sb][ss] = quantizedData[0][sb][ss];
                }
            }
        }
    }

    private void reorder(int ch, int gr) {
        int sfreq = header.frequency;

        for (int sb=0; sb < SBLIMIT; sb++) {
            for (int ss=0; ss < SSLIMIT; ss++) {
                reorderedData[sb][ss] = 0;
            }
        }

        if ((blocksplit_flag[gr][ch] == 1) && (block_type[gr][ch] == 2)) {
            if (switch_point[gr][ch] == 0) {
                // Recoder the short blocks
                int scalefactorStart;
                int scalefactorLines;

                for (int scalefactorBand=0; scalefactorBand < 13; scalefactorBand++) {
                    scalefactorStart = MP3Constants.sfindex_short[sfreq][scalefactorBand];
                    scalefactorLines = MP3Constants.sfindex_short[sfreq][scalefactorBand + 1] - scalefactorStart;

                    for (int window=0; window < 3; window++) {
                        for (int freq=0; freq < scalefactorLines; freq++) {
                            int srcLine = (scalefactorStart * 3) + (window * scalefactorLines) + freq;
                            int destLine = (scalefactorStart * 3) + window + (freq * 3);
                            reorderedData[destLine / SSLIMIT][destLine % SSLIMIT] =
                                    normalizedData[ch][srcLine / SSLIMIT][srcLine % SSLIMIT];
                        }
                    }
                }
            } else {
                // We do not reorder the long blocks
                for (int sb=0; sb < 2; sb++) {
                    for (int ss=0; ss < SSLIMIT; ss++) {
                        reorderedData[sb][ss] = normalizedData[ch][sb][ss];
                    }
                }

                // We reorder the short blocks
                int scalefactorStart;
                int scalefactorLines;

                for (int scalefactorBand = 3; scalefactorBand < 13; scalefactorBand++) {
                    scalefactorStart = MP3Constants.sfindex_short[sfreq][scalefactorBand];
                    scalefactorLines = MP3Constants.sfindex_short[sfreq][scalefactorBand + 1] - scalefactorStart;

                    for (int window=0; window < 3; window++) {
                        for (int freq=0; freq < scalefactorLines; freq++) {
                            int srcLine = (scalefactorStart * 3) + (window * scalefactorLines) + freq;
                            int destLine = (scalefactorStart * 3) + window + (freq * 3);
                            reorderedData[destLine / SSLIMIT][destLine % SSLIMIT] =
                                    normalizedData[ch][srcLine / SSLIMIT][srcLine % SSLIMIT];
                        }
                    }
                }
            }
        } else {
            // We do not reorder long blocks
            for (int sb=0; sb < SBLIMIT; sb++) {
                for (int ss=0; ss < SSLIMIT; ss++) {
                    reorderedData[sb][ss] = normalizedData[ch][sb][ss];
                }
            }
        }
    }

    // Butterfly anti-alias
    private void antialias(int ch, int gr) {
        int subbandLimit = SBLIMIT - 1;

        // Ci[i] = [-0.6,-0.535,-0.33,-0.185,-0.095,-0.041,-0.0142,-0.0037];
        // cs[i] = 1.0 / (sqrt(1.0 + (Ci[i] * Ci[i])));
        // ca[i] = ca[i] * Ci[i];

        final double[] cs = {
                0.85749292571254418689325777610964,
                0.88174199731770518177557399759066,
                0.94962864910273289204833276115398,
                0.98331459249179014599030200066392,
                0.99551781606758576429088040422867,
                0.99916055817814750452934664352117,
                0.99989919524444704626703489425565,
                0.99999315507028023572010150517204
        };

        final double[] ca = {
                -0.5144957554275265121359546656654,
                -0.47173196856497227224993208871065,
                -0.31337745420390185437594981118049,
                -0.18191319961098117700820587012266,
                -0.09457419252642064760763363840166,
                -0.040965582885304047685703212384361,
                -0.014198568572471148056991895498421,
                -0.0036999746737600368721643755691364
        };

        // Init our working array with quantized data
        for (int sb=0; sb < SBLIMIT; sb++) {
            for (int ss=0; ss < SSLIMIT; ss++) {
                hybridData[sb][ss] = reorderedData[sb][ss];
            }
        }

        if ((blocksplit_flag[gr][ch] == 1) && (block_type[gr][ch] == 2) && (switch_point[gr][ch] == 0)) {
            return;
        }

        if ((blocksplit_flag[gr][ch] == 1) && (block_type[gr][ch] == 2) && (switch_point[gr][ch] == 1)) {
            subbandLimit = 1;
        }

        // 8 butterflies for each pair of subbands
        for (int sb=0; sb < subbandLimit; sb++) {
            for (int ss=0; ss < 8; ss++) {
                double bu = reorderedData[sb][17 - ss];
                double bd = reorderedData[sb + 1][ss];
                hybridData[sb][17 - ss] = (bu * cs[ss]) - (bd * ca[ss]);
                hybridData[sb + 1][ss] = (bd * cs[ss]) + (bu * ca[ss]);
            }
        }
    }

    // zerodouble initializes to 0.0 instead of nil
    private final double[][][] previousBlock = new double[2][SSLIMIT][SBLIMIT];

    private void hybridSynthesis(int gr, int ch, int sb) {

        int blockType = block_type[gr][ch];

        if ((blocksplit_flag[gr][ch] == 1) && (switch_point[gr][ch] == 1) && (sb < 2)) {
            blockType = 0;
        }

        double[] output = inverseMDCT(hybridData[sb], blockType);

        // Overlapping and Adding with Previous Block:
        // The last half gets reserved for the next block, and used in this block
        for (int ss=0; ss < SSLIMIT; ss++) {
            polysynthData[ch][sb][ss] = output[ss] + previousBlock[ch][sb][ss];
            previousBlock[ch][sb][ss] = output[ss+18];
        }
    }

    private double[] inverseMDCT(double[] working, int blockType) {
        double[] ret = new double[36];

        // The Constant Parts of the Windowing equations

        final double[][] win = { //[36][4]
                // Block Type 0
                // win[i] = sin( (PI / 36) * ( i + 0.5 ) ) ; i = 0 to 35
                {
                        0.043619387365336000084, 0.130526192220051573400, 0.216439613938102876070,
                        0.300705799504273119100, 0.382683432365089781770, 0.461748613235033911190,
                        0.537299608346823887030, 0.608761429008720655880, 0.675590207615660132130,
                        0.737277336810123973260, 0.793353340291235165080, 0.843391445812885720550,
                        0.887010833178221602670, 0.923879532511286738480, 0.953716950748226821580,
                        0.976296007119933362260, 0.991444861373810382150, 0.999048221581857798230,
                        0.999048221581857798230, 0.991444861373810382150, 0.976296007119933362260,
                        0.953716950748226932610, 0.923879532511286738480, 0.887010833178221824720,
                        0.843391445812885831570, 0.793353340291235165080, 0.737277336810124084280,
                        0.675590207615660354170, 0.608761429008720877930, 0.537299608346824109080,
                        0.461748613235033911190, 0.382683432365089892800, 0.300705799504273341140,
                        0.216439613938103181380, 0.130526192220051573400, 0.043619387365336069473
                },
                // Block Type 1
                // win[i] = sin( (PI / 36) * ( i + 0.5 ) ) ; i = 0 to 17
                // win[i] = 1 ; i = 18 to 23
                // win[i] = sin( (PI / 12) * ( i - 18 + 0.5 ) ) ; i = 24 to 29
                // win[i] = 0 ; i = 30 to 35
                {
                        0.043619387365336000084, 0.130526192220051573400, 0.216439613938102876070,
                        0.300705799504273119100, 0.382683432365089781770, 0.461748613235033911190,
                        0.537299608346823887030, 0.608761429008720655880, 0.675590207615660132130,
                        0.737277336810123973260, 0.793353340291235165080, 0.843391445812885720550,
                        0.887010833178221602670, 0.923879532511286738480, 0.953716950748226821580,
                        0.976296007119933362260, 0.991444861373810382150, 0.999048221581857798230,
                        1.000000000000000000000, 1.000000000000000000000, 1.000000000000000000000,
                        1.000000000000000000000, 1.000000000000000000000, 1.000000000000000000000,
                        0.991444861373810382150, 0.923879532511286738480, 0.793353340291235165080,
                        0.608761429008720433840, 0.382683432365089448710, 0.130526192220051129310,
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000,
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000
                },
                // Block Type 2
                // win[i] = sin( (PI / 12.0) * (i + 0.5) ) ; i = 0 to 11
                // win[i] = 0.0 ; i = 12 to 35
                {
                        0.130526192220051601150, 0.382683432365089781770, 0.608761429008720766900,
                        0.793353340291235165080, 0.923879532511286849500, 0.991444861373810382150,
                        0.991444861373810382150, 0.923879532511286738480, 0.793353340291235165080,
                        0.608761429008720433840, 0.382683432365089448710, 0.130526192220051129310,
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000,
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000,
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000,
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000,
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000,
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000,
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000,
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000
                },
                // Block Type 3
                // win[i] = 0 ; i = 0 to 5
                // win[i] = sin( (PI / 12) * ( i - 6 + 0.5 ) ) ; i = 6 to 11
                // win[i] = 1 ; i = 12 to 17
                // win[i] = sin( (PI / 36) * ( i + 0.5 ) ) ; i = 18 to 35
                {
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000,
                        0.000000000000000000000, 0.000000000000000000000, 0.000000000000000000000,
                        0.130526192220051601150, 0.382683432365089781770, 0.608761429008720766900,
                        0.793353340291235165080, 0.923879532511286849500, 0.991444861373810382150,
                        1.000000000000000000000, 1.000000000000000000000, 1.000000000000000000000,
                        1.000000000000000000000, 1.000000000000000000000, 1.000000000000000000000,
                        0.999048221581857798230, 0.991444861373810382150, 0.976296007119933362260,
                        0.953716950748226932610, 0.923879532511286738480, 0.887010833178221824720,
                        0.843391445812885831570, 0.793353340291235165080, 0.737277336810124084280,
                        0.675590207615660354170, 0.608761429008720877930, 0.537299608346824109080,
                        0.461748613235033911190, 0.382683432365089892800, 0.300705799504273341140,
                        0.216439613938103181380, 0.130526192220051573400, 0.043619387365336069473
            }
        };

        double[] cosLookup = { //4*36
                1.000000000000000000000, 0.999048221581857798230, 0.996194698091745545190, 0.991444861373810382150,
                0.984807753012208020310, 0.976296007119933362260, 0.965925826289068312210, 0.953716950748226932610,
                0.939692620785908427900, 0.923879532511286738480, 0.906307787036649936670, 0.887010833178221713700,
                0.866025403784438707610, 0.843391445812885720550, 0.819152044288991798550, 0.793353340291235165080,
                0.766044443118978013450, 0.737277336810124084280, 0.707106781186547572730, 0.675590207615660354170,
                0.642787609686539362910, 0.608761429008720655880, 0.573576436351046159420, 0.537299608346823887030,
                0.500000000000000111020, 0.461748613235034077720, 0.422618261740699441280, 0.382683432365089837290,
                0.342020143325668823930, 0.300705799504273285630, 0.258819045102520739470, 0.216439613938102903830,
                0.173648177666930414450, 0.130526192220051712170, 0.087155742747658360158, 0.043619387365336007023,
                0.000000000000000061230, -0.043619387365335889061, -0.087155742747658013214, -0.130526192220051601150,
                -0.173648177666930303430, -0.216439613938102792810, -0.258819045102520628450, -0.300705799504272952570,
                -0.342020143325668712900, -0.382683432365089726260, -0.422618261740699330260, -0.461748613235033744660,
                -0.499999999999999777950, -0.537299608346823553970, -0.573576436351045826350, -0.608761429008720655880,
                -0.642787609686539362910, -0.675590207615660243150, -0.707106781186547461710, -0.737277336810123973260,
                -0.766044443118977902420, -0.793353340291235054060, -0.819152044288991576510, -0.843391445812885498510,
                -0.866025403784438707610, -0.887010833178221713700, -0.906307787036649936670, -0.923879532511286738480,
                -0.939692620785908316880, -0.953716950748226821580, -0.965925826289068201190, -0.976296007119933251230,
                -0.984807753012208020310, -0.991444861373810382150, -0.996194698091745545190, -0.999048221581857798230,
                -1.000000000000000000000, -0.999048221581857798230, -0.996194698091745545190, -0.991444861373810493170,
                -0.984807753012208131330, -0.976296007119933473280, -0.965925826289068312210, -0.953716950748226932610,
                -0.939692620785908427900, -0.923879532511286849500, -0.906307787036650047690, -0.887010833178221824720,
                -0.866025403784438818630, -0.843391445812885831570, -0.819152044288992020600, -0.793353340291235165080,
                -0.766044443118978013450, -0.737277336810124084280, -0.707106781186547683750, -0.675590207615660354170,
                -0.642787609686539473940, -0.608761429008720877930, -0.573576436351046381460, -0.537299608346824220100,
                -0.500000000000000444080, -0.461748613235034410790, -0.422618261740699940880, -0.382683432365090336890,
                -0.342020143325669379040, -0.300705799504272952570, -0.258819045102520628450, -0.216439613938102820560,
                -0.173648177666930331180, -0.130526192220051628910, -0.087155742747658249136, -0.043619387365336131923,
                -0.000000000000000183690, 0.043619387365335764161, 0.087155742747657888314, 0.130526192220051268080,
                0.173648177666929970360, 0.216439613938102459740, 0.258819045102520295380, 0.300705799504272619500,
                0.342020143325668157790, 0.382683432365089171150, 0.422618261740698830660, 0.461748613235034077720,
                0.500000000000000111020, 0.537299608346823887030, 0.573576436351046048400, 0.608761429008720544860,
                0.642787609686539251890, 0.675590207615660132130, 0.707106781186547350690, 0.737277336810123862240,
                0.766044443118977791400, 0.793353340291234943030, 0.819152044288991576510, 0.843391445812885498510,
                0.866025403784438374540, 0.887010833178221491650, 0.906307787036649714620, 0.923879532511286516430,
                0.939692620785908094830, 0.953716950748226710560, 0.965925826289068312210, 0.976296007119933362260,
                0.984807753012208020310, 0.991444861373810382150, 0.996194698091745545190, 0.999048221581857798230,
                };

        if (blockType == 2) {
            int N = 12;
            for (int i=0; i < 3; i++) {
                    double[] tmp = new double[12];

                    for (int p=0; p < N; p++) {
                        double sum = 0.0;
                        for (int m=0; m < (N / 2); m++) {
                            sum += working[i + (3 * m)] * Math.cos((Maths.PI / (double) (2 * N)) * (double) ((2 * p) + 1 + (N / 2)) * ((2 * m) + 1));

                        }
                        tmp[p] = sum * win[2][p];
                    }
                    for (int p=0; p < N; p++) {
                        ret[(6 * i) + p + 6] += tmp[p];
                    }
            }
        } else {
            int N = 36;
            for (int p=0; p < 36; p++) {
                double sum = 0.0;
                for (int m=0; m < 18; m++) {
                    sum += working[m] * cosLookup[(((2 * p) + 1 + 18) * ((2 * m) + 1)) % (4 * 36)];
                }
                ret[p] = sum * win[blockType][p];
            }
        }

        return ret;
    }


//    private void polyphaseSynthesis(int gr, int ss, ref Wavelet toBuffer) {
//        double sum=0;
//        int i=0;
//        int k=0;
//        int j=0;
//
//        long foo;
//
//        double* bufOffsetPtr;
//        double* bufOffsetPtr2;
//
//        if (header.numChannels == 1) {
//            int channel = 0;
//
//            bufOffset[channel] = (bufOffset[channel] - 64) & 0x3ff;
//            bufOffsetPtr = cast(double*)&BB[channel][bufOffset[channel]];
//
//            for (i=0; i<64; i++) {
//                sum = 0;
//                for (k=0; k<32; k++) {
//                    sum += polysynthData[channel][k][ss] * nCoefficients[i][k];
//                }
//                bufOffsetPtr[i] = sum;
//            }
//
//            for (j=0; j<32; j++) {
//                sum = 0;
//                for (i=0; i<16; i++) {
//                    k = j + (i << 5);
//
//                    sum += windowCoefficients[k] * BB[channel][((k + (((i + 1) >> 1) << 6)) + bufOffset[channel]) & 0x3ff];
//                }
//
//                if (sum > 0) {
//                    foo = cast(long) (sum * cast(double) 32768 + cast(double) 0.5);
//                } else {
//                    foo = cast(long) (sum * cast(double) 32768 - cast(double) 0.5);
//                }
//
//                if (foo >= cast(long) 32768) {
//                    toBuffer.write(cast(short) (32768-1));
//                    //++clip;
//                } else if (foo < cast(long)-32768) {
//                    toBuffer.write(cast(short) (-32768));
//                    //++clip;
//                } else {
//                    toBuffer.write(cast(short) foo);
//                }
//
//            }
//        } else {
//            // INTERLEAVE CHANNELS!
//            bufOffset[0] = (bufOffset[0] - 64) & 0x3ff;
//            bufOffsetPtr = cast(double*)&BB[0][bufOffset[0]];
//
//            bufOffset[1] = (bufOffset[1] - 64) & 0x3ff;
//            bufOffsetPtr2 = cast(double*)&BB[1][bufOffset[1]];
//
//            double sum2;
//
//            for (i=0; i<64; i++) {
//                sum = 0;
//                sum2 = 0;
//                for (k=0; k<32; k++) {
//                        sum += polysynthData[0][k][ss] * nCoefficients[i][k];
//                        sum2 += polysynthData[1][k][ss] * nCoefficients[i][k];
//                }
//                bufOffsetPtr[i] = sum;
//                bufOffsetPtr2[i] = sum2;
//            }
//
//            long[] ch1 = new long[32];
//            long[] ch2 = new long[32];
//
//            for (j=0; j<32; j++) {
//                sum = 0;
//                sum2 = 0;
//                for (i=0; i<16; i++) {
//                    k = j + (i << 5);
//                    sum += windowCoefficients[k] * BB[0][( (k + ( ((i+1)>>1) << 6) ) + bufOffset[0]) & 0x3ff];
//                    sum2 += windowCoefficients[k] * BB[1][( (k + ( ((i+1)>>1) << 6) ) + bufOffset[1]) & 0x3ff];
//                }
//
//                if (sum > 0) {
//                    foo = (long) (sum * (double) 32768 + (double) 0.5);
//                } else {
//                    foo = (long) (sum * (double) 32768 - (double) 0.5);
//                }
//
//                if (foo >= (long) 32768) {
//                    toBuffer.write((short) (32768-1));
//                    //++clip;
//                    ch1[j] = 32768;
//                } else if (foo < (long)-32768) {
//                    toBuffer.write((short) (-32768));
//                    //++clip;
//                    ch1[j] = -32768;
//                } else {
//                    toBuffer.write((short) foo);
//                    ch1[j] = foo;
//                }
//
//                if (sum2 > 0) {
//                    foo = (long) (sum2 * (double) 32768 + (double) 0.5);
//                } else {
//                    foo = (long) (sum2 * (double) 32768 - (double) 0.5);
//                }
//
//                if (foo >= (long) 32768) {
//                    toBuffer.write((short) (32768-1));
//                    //++clip;
//                    ch2[j] = 32768;
//                } else if (foo < (long)-32768) {
//                    toBuffer.write((short) (-32768));
//                    //++clip;
//                    ch2[j] = -32768;
//                } else {
//                    toBuffer.write((short) foo);
//                    ch2[j] = foo;
//                }
//
//                //printf("%d\n", ch1[j]);
//            }
//
//            for (j=0; j < 32; j++) {
//                //printf("%d\n", ch2[j]);
//            }
//        }
//    }


}
