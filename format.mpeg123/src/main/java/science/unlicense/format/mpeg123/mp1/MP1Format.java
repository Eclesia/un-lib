
package science.unlicense.format.mpeg123.mp1;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;

/**
 * Mpeg-1/ Mepg-2 format.
 *
 * Specification :
 * https://www.itu.int/rec/dologin_pub.asp?lang=e&id=T-REC-H.261-199303-I!!PDF-E&type=items
 *
 * Useful docs :
 * http://en.wikipedia.org/wiki/MPEG-1
 * http://en.wikipedia.org/wiki/MPEG_program_stream
 * http://msdn.microsoft.com/en-us/library/windows/desktop/dd390712(v=vs.85).aspx
 * http://dvd.sourceforge.net/dvdinfo/mpeghdrs.html
 * http://stnsoft.com/DVD/mpeg-1_pes-hdr.html
 * http://www.andrewduncan.ws/MPEG/MPEG-1_Picts.html
 *
 * @author Johann Sorel
 */
public class MP1Format extends AbstractMediaFormat {

    public static final MP1Format INSTANCE = new MP1Format();

    public MP1Format() {
        super(new Chars("mp1"));
        shortName = new Chars("MPEG");
        longName = new Chars("MPEG-1");
        mimeTypes.add(new Chars("audio/mpeg"));
        mimeTypes.add(new Chars("video/mpeg"));
        extensions.add(new Chars("mpg"));
        extensions.add(new Chars("mpeg"));
        extensions.add(new Chars("mp1"));
        extensions.add(new Chars("mp2"));
        extensions.add(new Chars("mp3"));
        extensions.add(new Chars("m1v"));
        extensions.add(new Chars("m1a"));
        extensions.add(new Chars("m2a"));
        extensions.add(new Chars("mpa"));
        extensions.add(new Chars("mpv"));
        resourceTypes.add(Media.class);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    public Store open(Object input) throws IOException {
        return new MP1Store((Path) input);
    }

}
