
package science.unlicense.format.mpeg123.mp1.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SequenceHeader extends Block {

    public int horizontalSize;
    public int verticalSize;
    public int pixelAspectRatio;
    public int pictureRate;
    public int bitRate;
    public int ybybuffersize;
    public boolean constrained;
    public boolean loadIntraQMatrix;
    public int[] intraQuantizerMatrix;
    public boolean loadNonIntraQMatrix;
    public int[] nonIntraQuantizerMatrix;


    public void read(DataInputStream ds) throws IOException {
        horizontalSize      = ds.readBits(12);
        verticalSize        = ds.readBits(12);
        pixelAspectRatio    = ds.readBits(4);
        pictureRate         = ds.readBits(4);
        bitRate             = ds.readBits(18);
        if (ds.readBits(1)!=1) throw new IOException(ds, "Invalid sequence header");
        ybybuffersize       = ds.readBits(10);
        constrained         = ds.readBits(1)==1;
        loadIntraQMatrix    = ds.readBits(1)==1;
        if (loadIntraQMatrix){
            intraQuantizerMatrix = new int[64];
            ds.readUByte(intraQuantizerMatrix);
        }
        loadNonIntraQMatrix = ds.readBits(1)==1;
        if (loadNonIntraQMatrix){
            nonIntraQuantizerMatrix = new int[64];
            ds.readUByte(nonIntraQuantizerMatrix);
        }
        //skip remaining bits
        ds.skipToByteEnd();
    }

}
