

package science.unlicense.format.mpeg123.mp1;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import static science.unlicense.format.mpeg123.mp1.MP1Constants.*;
import science.unlicense.format.mpeg123.mp1.model.Block;
import science.unlicense.format.mpeg123.mp1.model.SystemHeader;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.media.api.MediaReadStream;

/**
 *
 * @author Johann Sorel
 */
public class MP1Reader implements MediaReadStream{

    private final Path path;

    public MP1Reader(Path path) {
        this.path = path;
    }

    public void read() throws IOException{

        final ByteInputStream is = path.createInputStream();
        final DataInputStream ds = new DataInputStream(is, Endianness.BIG_ENDIAN);


        final byte[] blockId = new byte[4];
        int offset=0;
        for (;;){
            ds.readFully(blockId, offset, 4-offset);
            offset=0;

            if (!Arrays.equals(blockId,0,3,SIGNATURE,0)){
                throw new IOException(ds, "Invalid block id");
            }

            if (blockId[3]==STREAM_PROG_END){
                //end marker
                break;
            } else {
                final Block block = createBlock(blockId[3]);
                block.read(ds);
                if (block instanceof SystemHeader){
                    //check for stream specs elements
                    while (ds.readBits(1)==1){
                        final SystemHeader.StreamSpec spec = new SystemHeader.StreamSpec();
                        spec.streamId = ds.readBits(7) | 0x80;
                        if (ds.readBits(2)!=3) throw new IOException(ds, "Invalid stream spec");
                        spec.bufferBoundScale = ds.readBits(1)==1;
                        spec.stdBufferSize = ds.readBits(13);
                        ((SystemHeader) block).streamSpecs.add(spec);
                    }
                    offset=1;
                    ds.readBits(7);
                }

                System.out.println(block);
            }
        }


    }

    @Override
    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaPacket next() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
