

package science.unlicense.format.mpeg123.mp3;

import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg123.mp3.model.ID3Tag;
import science.unlicense.format.mpeg123.mp3.model.MP3Frame;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class MP3Reader extends AbstractReader{

    public static void main(String[] args) throws IOException {

        final MP3Reader reader = new MP3Reader();
        reader.read();
    }

    public void read() throws IOException{

        final DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);

        int b = ds.read();
        if (b==(byte) 0x49){
            //skip the ID3 tag code
            ds.skipFully(2);
            final ID3Tag id3Header = new ID3Tag();
            id3Header.read(ds, true);

            b = ds.read();
        }

        for (;b!=-1;b=ds.read()){
            if (b!=0xFF){
                //search for the sink byte
                continue;
            }
            //total of 12 bits are used for sync, skip the remaining 4 bits
            ds.readBits(4);

            final MP3Frame frame = new MP3Frame();
            frame.read(ds, true);

        }


    }

}
