
package science.unlicense.format.mpeg123.mp3;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;

/**
 * MP3 format.
 *
 * Useful docs :
 * http://en.wikipedia.org/wiki/MP3
 * http://www.datavoyage.com/mpgscript/mpeghdr.htm
 *
 * @author Johann Sorel
 */
public class MP3Format extends AbstractMediaFormat {

    public static final MP3Format INSTANCE = new MP3Format();

    public MP3Format() {
        super(new Chars("mp3"));
        shortName = new Chars("MP3");
        longName = new Chars("MPEG-2 Audio Layer III");
        mimeTypes.add(new Chars("audio/mpeg"));
        mimeTypes.add(new Chars("audio/MPA"));
        mimeTypes.add(new Chars("audio/mpa-robust"));
        extensions.add(new Chars("mp3"));
        resourceTypes.add(Media.class);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public Store open(Object input) throws IOException {
        return new MP3Store((Path) input);
    }

}
