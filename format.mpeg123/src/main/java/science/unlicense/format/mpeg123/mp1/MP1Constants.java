

package science.unlicense.format.mpeg123.mp1;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mpeg123.mp1.model.Block;
import science.unlicense.format.mpeg123.mp1.model.ElementaryStreamHeader;
import science.unlicense.format.mpeg123.mp1.model.ExtensionHeader;
import science.unlicense.format.mpeg123.mp1.model.PackHeader;
import science.unlicense.format.mpeg123.mp1.model.Picture;
import science.unlicense.format.mpeg123.mp1.model.Picture.Slice;
import science.unlicense.format.mpeg123.mp1.model.PictureGroup;
import science.unlicense.format.mpeg123.mp1.model.SequenceHeader;
import science.unlicense.format.mpeg123.mp1.model.SystemHeader;

/**
 * MPEG-1 Constants.
 *
 * @author Johann Sorel
 */
public final class MP1Constants {

    public static final byte[] SIGNATURE = new byte[]{0,0,1};

    //Video stream start codes
    public static final byte STREAM_PICTURE = 0x00;
    public static final byte STREAM_SLICE_MIN = 0x01;
    public static final byte STREAM_SLICE_MAX = (byte) 0xAF;
    public static final byte STREAM_USER_DATA = (byte) 0xB2;
    public static final byte STREAM_SEQ_HEADER = (byte) 0xB3;
    public static final byte STREAM_SEQ_ERROR = (byte) 0xB4;
    public static final byte STREAM_EXTENSION = (byte) 0xB5;
    public static final byte STREAM_SEQ_END = (byte) 0xB7;
    public static final byte STREAM_PICTURE_GROUP = (byte) 0xB8;

    //stream ids
    public static final byte STREAM_PROG_END = (byte) 0xB9;
    public static final byte STREAM_PACK_HEADER = (byte) 0xBA;
    public static final byte STREAM_SYS_HEADER = (byte) 0xBB;
    public static final byte STREAM_PROG_STREAM_MAP = (byte) 0xBC;
    public static final byte STREAM_PRIVATE_STREAM_1 = (byte) 0xBD;
    public static final byte STREAM_PADDING_STREAM = (byte) 0xBE;
    public static final byte STREAM_PRIVATE_STREAM_2 = (byte) 0xBF;
    public static final byte STREAM_AUDIO_START = (byte) 0xC0;
    public static final byte STREAM_AUDIO_END = (byte) 0xDF;
    public static final byte STREAM_VIDEO_START = (byte) 0xE0;
    public static final byte STREAM_VIDEO_END = (byte) 0xEF;
    public static final byte STREAM_ECM = (byte) 0xF0;
    public static final byte STREAM_EMM = (byte) 0xF1;
    public static final byte STREAM_H2220 = (byte) 0xF2;
    public static final byte STREAM_ISO13522 = (byte) 0xF3;
    public static final byte STREAM_H2221_A = (byte) 0xF4;
    public static final byte STREAM_H2221_B = (byte) 0xF5;
    public static final byte STREAM_H2221_C = (byte) 0xF6;
    public static final byte STREAM_H2221_D = (byte) 0xF7;
    public static final byte STREAM_H2221_E= (byte) 0xF8;
    public static final byte STREAM_ANCILLARY = (byte) 0xF9;
    public static final byte STREAM_PROG_DICO = (byte) 0xFF;

    public static final double[] PIXEL_ASPECT_RATIO = new double[]{
        0, //forbidden
        1.0,
        0.6735,
        0.7031,
        0.7615,
        0.8055,
        0.8437,
        0.8935,
        0.9157,
        0.9815,
        1.0255,
        1.0695,
        1.095,
        1.1575,
        1.2051
    };

    public static final double[] PICTURE_RATE = new double[]{
        0, //forbidden
        23.976,
        24,
        25,
        29.97,
        30,
        50,
        59.94,
        60
    };

    public static final int PICTURE_TYPE_I = 1;
    public static final int PICTURE_TYPE_P = 2;
    public static final int PICTURE_TYPE_B = 3;
    public static final int PICTURE_TYPE_D = 4;

    /** ZigZag indice matrix */
    public static final int[] ZIGZAG = new int[]{
         0,  1,  8, 16,  9,  2,  3, 10,
        17, 24, 32, 25, 18, 11,  4,  5,
        12, 19, 26, 33, 40, 48, 41, 34,
        27, 20, 13,  6,  7, 14, 21, 28,
        35, 42, 49, 56, 57, 50, 43, 36,
        29, 22, 15, 23, 30, 37, 44, 51,
        58, 59, 52, 45, 38, 31, 39, 46,
        53, 60, 61, 54, 47, 55, 62, 63 };

    /** Default intra frame quantification matrix */
    public static final int[] QUANT_MATRIX_INTRA = new int[]{
         8, 16, 19, 22, 26, 27, 29, 34,
        16, 16, 22, 24, 27, 29, 34, 37,
        19, 22, 26, 27, 29, 34, 34, 38,
        22, 22, 26, 27, 29, 34, 37, 40,
        22, 26, 27, 29, 32, 35, 40, 48,
        26, 27, 29, 32, 35, 40, 48, 58,
        26, 27, 29, 34, 38, 46, 56, 69,
        27, 29, 35, 38, 46, 56, 69, 83 };

    /** Default non-intra frame quantification matrix */
    public static final int[] QUANT_MATRIX_NONINTRA = new int[64];
    static {Arrays.fill(QUANT_MATRIX_NONINTRA, 16);}

    /** Table 1 : macroblock addressing */
    public static int[] MACROBLOCK_ADRESSING = new int[]{
            //1
            //011
            //010
            //0011
            //0010
            //0001 1
            //0001 0
            //0000 111
            //0000 110
            //0000 1011
            //0000 1010
            //0000 1001
            //0000 1000
            //0000 0111
            //0000 0110
            //0000 0101 11
            //0000 0101 10
            //0000 0101 01
            //0000 0101 00
            //0000 0100 11
            //0000 0100 10
            //0000 0100 011
            //0000 0100 010
            //0000 0100 001
            //0000 0100 000
            //0000 0011 111
            //0000 0011 110
            //0000 0011 101
            //0000 0011 100
            //0000 0011 011
            //0000 0011 010
            //0000 0011 001
            //0000 0011 000
            //0000 0001 111 - fill mba
            //0000 0000 0000 0001 - start code
    };

    /** Table 2 : VLC table for MTYPE */

    /** Table 3 : VLC table for MVD */

    /** Table 4 : VLC table for CBP */

    /** Table 5 : VLC table for TCOEFF */


    //block types
    private static final Dictionary Blocks = new HashDictionary();
    static {
        Blocks.add(STREAM_PICTURE, Picture.class);
        Blocks.add(STREAM_SEQ_HEADER, SequenceHeader.class);
        Blocks.add(STREAM_SYS_HEADER, SystemHeader.class);
        Blocks.add(STREAM_EXTENSION, ExtensionHeader.class);
        Blocks.add(STREAM_PICTURE_GROUP, PictureGroup.class);
        Blocks.add(STREAM_PACK_HEADER, PackHeader.class);

        //slice
        for (int i=1;i<=0xAF;i++){
            Blocks.add((byte) i,Slice.class);
        }

        //audio et video
        for (int i=0;i<16;i++){
            Blocks.add((byte) (0xC0 | i), ElementaryStreamHeader.class);
            Blocks.add((byte) (0xD0 | i), ElementaryStreamHeader.class);
            Blocks.add((byte) (0xE0 | i), ElementaryStreamHeader.class);
        }
    }

    public static Block createBlock(byte code) throws IOException{
        Class clazz = (Class) Blocks.getValue(code);
        if (clazz==null) throw new IOException(null, "Unknowned block for id : "+code);
        try {
            return (Block) clazz.newInstance();
        } catch (InstantiationException ex) {
            throw new RuntimeException(ex);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }


    private  MP1Constants() {}

}
