
package science.unlicense.format.sevenz;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * 7z uses little endian encoding.
 *
 * @author Johann Sorel
 */
public class SevenZInputStream extends DataInputStream {

    public SevenZInputStream(ByteInputStream in) {
        super(in, Endianness.LITTLE_ENDIAN);
    }

    public long[] readRealUnit64(int number) throws IOException {
        final long[] values = new long[number];
        for (int i=0;i<values.length;i++) {
            values[i] = readRealUnit64();
        }
        return values;
    }
    /**
     *
     * REAL_UINT64 means real UINT64.
     *
     * UINT64 means real UINT64 encoded with the following scheme:
     *
     *   Size of encoding sequence depends from first byte:
     *   First_Byte  Extra_Bytes        Value
     *   (binary)
     *   0xxxxxxx               : ( xxxxxxx           )
     *   10xxxxxx    BYTE y[1]  : (  xxxxxx << (8 * 1)) + y
     *   110xxxxx    BYTE y[2]  : (   xxxxx << (8 * 2)) + y
     *   ...
     *   1111110x    BYTE y[6]  : (       x << (8 * 6)) + y
     *   11111110    BYTE y[7]  :                         y
     *   11111111    BYTE y[8]  :                         y
     *
     * @return
     * @throws IOException
     */
    public long readRealUnit64() throws IOException {

        int firstLength = 7;
        int nexts = 0;

        for (int i=0;i<8;i++) {
            final int b = readBits(1);
            if (b == 0) {
                break;
            }
            firstLength--;
            nexts++;
        }
        long value = readBits(firstLength);
        if (nexts > 0) {
            value = (value << nexts*8) | readBits(nexts*8, DataInputStream.LSB);
        }

        return value;
    }

    /**
     * Digests (NumStreams)
     * ------------------------------------------
     *   BYTE AllAreDefined
     *   if (AllAreDefined == 0)
     *   {
     *     for(NumStreams)
     *       BIT Defined
     *   }
     *   UINT32 CRCs[NumDefined]
     * ------------------------------------------
     */
    public boolean[] readDigestDefined(int count) throws IOException {
        final boolean[] crcsDefined = new boolean[count];
        final int allAreDefined = readUByte();
        if (allAreDefined == 0) {
            for (int i=0;i<crcsDefined.length;i++) {
                crcsDefined[i] = (readBits(1) == 1);
            }
            skipToByteEnd();
        } else {
            Arrays.fill(crcsDefined, true);
        }
        return crcsDefined;
    }

    public int[] readDigestCRC(boolean[] defined) throws IOException {
        final int[] crcs = new int[defined.length];
        for (int i=0;i<crcs.length;i++) {
            if (defined[i]) {
                crcs[i] = readInt();
            }
        }
        return crcs;
    }
}
