
package science.unlicense.format.sevenz;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.SeekableByteBuffer;
import science.unlicense.encoding.api.path.AbstractPath;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.encoding.impl.io.ClipInputStream;

/**
 *
 * @author Johann Sorel
 */
public class SevenZPath extends AbstractPath {

    private final Path archiveFile;
    private final Decoder.Streams streams;
    private final Folder.Coder coder;
    private final long uncompressedSize;
    private final long offset;
    private final long length;

    private final science.unlicense.format.sevenz.FilesInfo.File entry;
    private final Path parent;
    final Sequence children = new ArraySequence();

    public SevenZPath(Path parent, Path archiveFile, Decoder.Streams streams, Folder.Coder coder, long uncompressedSize, long offset, long length, science.unlicense.format.sevenz.FilesInfo.File entry) {
        this.archiveFile = archiveFile;
        this.streams = streams;
        this.coder = coder;
        this.uncompressedSize = uncompressedSize;
        this.offset = offset;
        this.length = length;

        this.parent = parent;
        this.entry = entry;
    }

    @Override
    public Chars getName() {
        final Chars[] path = entry.name.split('/');
        return path[path.length-1];
    }

    @Override
    public Path getParent() {
        return parent;
    }

    @Override
    public boolean isContainer() throws IOException {
        return entry.isEmptyStream;
    }

    @Override
    public boolean exists() throws IOException {
        return true;
    }

    @Override
    public Path resolve(Chars address) {
        return getResolver().resolve(entry.name.concat('/').concat(address));
    }

    @Override
    public PathResolver getResolver() {
        return parent.getResolver();
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        if (isContainer()) {
            throw new IOException(this, "Path is as container");
        }

        final DataInputStream inStream = new DataInputStream(Decoder.open(archiveFile, streams, coder, uncompressedSize));
        inStream.skipFully(offset);
        return new ClipInputStream(inStream, (int) length);
    }

    @Override
    public Chars toURI() {
        if (parent instanceof SevenZArchive) {
            //root element inside the archive
            return parent.toURI().concat(new Chars("!/")).concat(getName());
        } else {
            return parent.toURI().concat('/').concat(getName());
        }
    }

    @Override
    public Chars toChars() {
        return new Chars("7Z entry : "+getName());
    }

    @Override
    public boolean createContainer() throws IOException {
        throw new IOException(this, "Not supported yet.");
    }

    @Override
    public boolean createLeaf() throws IOException {
        throw new IOException(this, "Not supported yet.");
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        throw new IOException(this, "Not supported yet.");
    }

    @Override
    public void delete() throws IOException {
        throw new IOException(this, "Not supported yet.");
    }

    @Override
    public Object getPathInfo(Chars key) {
        return null;
    }

    @Override
    public SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException {
        throw new IOException(this, "Not supported yet.");
    }

    @Override
    public Collection getChildren() {
        return Collections.readOnlyCollection(children);
    }

    void addChild(Path child) {
        children.add(child);
    }

}
