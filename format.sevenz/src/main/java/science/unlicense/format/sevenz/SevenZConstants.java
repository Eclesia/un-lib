
package science.unlicense.format.sevenz;

/**
 *
 * Resources :
 * https://en.wikipedia.org/wiki/7z
 * http://cpansearch.perl.org/src/BJOERN/Compress-Deflate7-1.0/7zip/DOC/7zFormat.txt
 *
 * @author Johann Sorel
 */
public class SevenZConstants {

    public static final byte[] SIGNATURE = new byte[]{'7', 'z', (byte) 0xBC, (byte) 0xAF, (byte) 0x27, (byte) 0x1C};

    // Property IDs
    public static final int KEND = 0x00;
    public static final int KHEADER = 0x01;
    public static final int KARCHIVEPROPERTIES = 0x02;
    public static final int KADDITIONALSTREAMSINFO = 0x03;
    public static final int KMAINSTREAMSINFO = 0x04;
    public static final int KFILESINFO = 0x05;
    public static final int KPACKINFO = 0x06;
    public static final int KUNPACKINFO = 0x07;
    public static final int KSUBSTREAMSINFO = 0x08;
    public static final int KSIZE = 0x09;
    public static final int KCRC = 0x0A;
    public static final int KFOLDER = 0x0B;
    public static final int KCODERSUNPACKSIZE = 0x0C;
    public static final int KNUMUNPACKSTREAM = 0x0D;
    public static final int KEMPTYSTREAM = 0x0E;
    public static final int KEMPTYFILE = 0x0F;
    public static final int KANTI = 0x10;
    public static final int KNAME = 0x11;
    public static final int KCTIME = 0x12;
    public static final int KATIME = 0x13;
    public static final int KMTIME = 0x14;
    public static final int KWINATTRIBUTES = 0x15;
    public static final int KCOMMENT = 0x16;
    public static final int KENCODEDHEADER = 0x17;
    public static final int KSTARTPOS = 0x18;
    public static final int KDUMMY = 0x19;

    // CODEC IDS ///////////////////////////////////////////////////////////////

    public static final long CODEC_NONE         = 0x00;
    public static final long CODEC_LZMA         = (0x01 << 16) | (0x01 << 8) | 0x03;
    public static final long CODEC_LZMA2        = 0x21;
    public static final long CODEC_DEFLATE      = (0x04 << 16) | (0x01 << 8) | 0x08;
    public static final long CODEC_DEFLATE64    = (0x04 << 16) | (0x01 << 8) | 0x09;
    public static final long CODEC_BZIP2        = (0x04 << 16) | (0x02 << 8) | 0x02;
    public static final long CODEC_AESSHA256    = (0x06 << 24) | (0xF1 << 16) | (0x07 << 8) + 0x01;

    private SevenZConstants(){}

}
