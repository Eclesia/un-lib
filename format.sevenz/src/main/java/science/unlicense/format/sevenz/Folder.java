
package science.unlicense.format.sevenz;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * ------------------------------------------
 * UINT64 NumCoders;
 *  for (NumCoders)
 *  {
 *    BYTE
 *    {
 *      0:3 CodecIdSize
 *      4:  Is Complex Coder
 *      5:  There Are Attributes
 *      6:  Reserved
 *      7:  There are more alternative methods. (Not used anymore, must be 0).
 *    }
 *    BYTE CodecId[CodecIdSize]
 *    if (Is Complex Coder)
 *    {
 *      UINT64 NumInStreams;
 *      UINT64 NumOutStreams;
 *    }
 *    if (There Are Attributes)
 *    {
 *      UINT64 PropertiesSize
 *      BYTE Properties[PropertiesSize]
 *    }
 *  }
 *
 *  NumBindPairs = NumOutStreamsTotal - 1;
 *
 *  for (NumBindPairs)
 *  {
 *    UINT64 InIndex;
 *    UINT64 OutIndex;
 *  }
 *
 *  NumPackedStreams = NumInStreamsTotal - NumBindPairs;
 *  if (NumPackedStreams > 1)
 *    for(NumPackedStreams)
 *    {
 *      UINT64 Index;
 *    };
 * ------------------------------------------
 *
 * @author Johann Sorel
 */
public class Folder {

    public static class Coder {
        public int codecIdSize;
        public boolean isComplex;
        public boolean hasAttributes;
        public long codecId;
        //complex
        public long numInStream = 1; //one if not complex
        public long numOutStream = 1;
        //attributes
        public byte[] properties;
    }

    public static class BindPair {
        public long inIndex;
        public long outIndex;
    }

    public Coder[] coders;
    public long numInStreamsTotal = 0;
    public long numOutStreamsTotal = 0;
    public BindPair[] bindPairs;
    public long numPackedStreams;
    public long[] packedStreams;

    //for each outputstream (total)
    public long[] unPackSize;

    public long numUnPackStreams;

    //digest
    public boolean withCrc;
    public int crc;

    public void read(SevenZInputStream ds) throws IOException {

        long numCoders = ds.readRealUnit64();
        coders = new Coder[(int) numCoders];

        for (int i=0;i<coders.length;i++) {
            coders[i] = new Coder();
            int b = ds.readUByte();
            coders[i].codecIdSize = b & 0x0F;
            coders[i].isComplex = (b & 0x10) != 0;
            coders[i].hasAttributes = (b & 0x11) != 0;
            coders[i].codecId = ds.readBits(8*coders[i].codecIdSize, DataInputStream.LSB);
            if (coders[i].isComplex) {
                coders[i].numInStream = ds.readRealUnit64();
                coders[i].numOutStream = ds.readRealUnit64();
            }
            if (coders[i].hasAttributes) {
                int propertiesSize = (int) ds.readRealUnit64();
                coders[i].properties = ds.readBytes(propertiesSize);
            }
            numInStreamsTotal += coders[i].numInStream;
            numOutStreamsTotal += coders[i].numOutStream;
        }

        long numBindPairs = numOutStreamsTotal - 1;
        bindPairs = new BindPair[(int) numBindPairs];
        for (int i=0;i<numBindPairs; i++) {
            bindPairs[i] = new BindPair();
            bindPairs[i].inIndex = ds.readRealUnit64();
            bindPairs[i].outIndex = ds.readRealUnit64();
        }

        numPackedStreams = numInStreamsTotal - numBindPairs;
        if (numPackedStreams > 1) {
            packedStreams = ds.readRealUnit64((int) numPackedStreams);
        }

    }

}
