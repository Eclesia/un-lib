

package science.unlicense.format.sevenz;

import science.unlicense.archive.api.AbstractArchiveFormat;
import science.unlicense.archive.api.Archive;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class SevenZFormat extends AbstractArchiveFormat {

    public static final SevenZFormat INSTANCE = new SevenZFormat();
    private static final Chars SUFFIX = Chars.constant(".7z");

    private SevenZFormat() {
        super(new Chars("7z"));
        shortName = new Chars("7ZIP");
        longName = new Chars("7ZIP");
        mimeTypes.add(new Chars("application/x-7z-compressed"));
        extensions.add(new Chars("7z"));
    }

    @Override
    public boolean canDecode(Object candidate) {
        if (candidate instanceof Path) {
            return ((Path) candidate).getName().toLowerCase().endsWith(SUFFIX);
        }
        return false;
    }

    @Override
    public Archive open(Object candidate) throws IOException {
        if (candidate instanceof Path) {
            return new SevenZArchive((Path) candidate);
        }
        throw new IOException(candidate, "Unsupported input");
    }

    @Override
    public boolean isAbsolute() {
        return false;
    }

    @Override
    public boolean canCreate(Path base) throws IOException {
        return base.getName().toLowerCase().endsWith(SUFFIX);
    }

    @Override
    public PathResolver createResolver(Path base) throws IOException {
        return open(base);
    }

}
