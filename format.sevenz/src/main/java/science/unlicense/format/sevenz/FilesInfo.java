
package science.unlicense.format.sevenz;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * ------------------------------------------
 *  BYTE NID::kFilesInfo;  (0x05)
 *  UINT64 NumFiles
 *
 *  for (;;)
 *  {
 *    BYTE PropertyType;
 *    if (aType == 0)
 *      break;
 *
 *    UINT64 Size;
 *
 *    switch(PropertyType)
 *    {
 *      kEmptyStream:   (0x0E)
 *        for(NumFiles)
 *          BIT IsEmptyStream
 *
 *      kEmptyFile:     (0x0F)
 *        for(EmptyStreams)
 *          BIT IsEmptyFile
 *
 *      kAnti:          (0x10)
 *        for(EmptyStreams)
 *          BIT IsAntiFile
 *
 *      case kCTime: (0x12)
 *      case kATime: (0x13)
 *      case kMTime: (0x14)
 *        BYTE AllAreDefined
 *        if (AllAreDefined == 0)
 *        {
 *          for(NumFiles)
 *            BIT TimeDefined
 *        }
 *        BYTE External;
 *        if(External != 0)
 *          UINT64 DataIndex
 *        []
 *        for(Definded Items)
 *          UINT64 Time
 *        []
 *
 *      kNames:     (0x11)
 *        BYTE External;
 *        if(External != 0)
 *          UINT64 DataIndex
 *        []
 *        for(Files)
 *        {
 *          wchar_t Names[NameSize];
 *          wchar_t 0;
 *        }
 *        []
 *
 *      kAttributes:  (0x15)
 *        BYTE AllAreDefined
 *        if (AllAreDefined == 0)
 *        {
 *          for(NumFiles)
 *            BIT AttributesAreDefined
 *        }
 *        BYTE External;
 *        if(External != 0)
 *          UINT64 DataIndex
 *        []
 *        for(Definded Attributes)
 *          UINT32 Attributes
 *        []
 *    }
 *  }
 * ------------------------------------------
 *
 * @author Johann Sorel
 */
public class FilesInfo {

    public static class File extends CObject {
        public boolean isEmptyStream;
        public boolean isEmptyFile;
        public boolean isAntiFile;
        public long creationDate;
        public long accessDate;
        public long modifiedDate;
        public Chars name;

        @Override
        public Chars toChars() {
            return name;
        }
    }

    public File[] files;

    public void read(SevenZInputStream ds) throws IOException {

        final long numFiles = ds.readRealUnit64();
        files = new File[(int) numFiles];
        for (int i=0;i<numFiles;i++) {
            files[i] = new File();
        }

        for (;;) {
            int propertyType = ds.readUByte();
            if (propertyType == 0) break;
            long size = ds.readRealUnit64();

            switch (propertyType) {
                case SevenZConstants.KEMPTYSTREAM : {
                    for (int i=0;i<files.length;i++) {
                        files[i].isEmptyStream = ds.readBits(1) != 0;
                    }
                    ds.skipToByteEnd();
                    break;
                    }
                case SevenZConstants.KEMPTYFILE : {
                    for (int i=0;i<files.length;i++) {
                        if (files[i].isEmptyStream) {
                            files[i].isEmptyFile = ds.readBits(1) != 0;
                        }
                    }
                    ds.skipToByteEnd();
                    break;
                    }
                case SevenZConstants.KANTI : {
                    for (int i=0;i<files.length;i++) {
                        if (files[i].isEmptyStream) {
                            files[i].isAntiFile = ds.readBits(1) != 0;
                        }
                    }
                    ds.skipToByteEnd();
                    break;
                    }
                case SevenZConstants.KCTIME :
                case SevenZConstants.KATIME :
                case SevenZConstants.KMTIME : {
                    final boolean[] defined = ds.readDigestDefined(files.length);
                    final int external = ds.readUByte();
                    if (external != 0) {
                        throw new IOException(ds, "TODO");
                    }
                    for (int i=0;i<defined.length;i++) {
                        if (defined[i]) {
                            switch (propertyType) {
                                case SevenZConstants.KCTIME : files[i].creationDate = ds.readLong(); break;
                                case SevenZConstants.KATIME : files[i].accessDate = ds.readLong(); break;
                                case SevenZConstants.KMTIME : files[i].modifiedDate = ds.readLong(); break;
                            }
                        }
                    }
                    break;
                    }
                case SevenZConstants.KNAME : {
                    final int external = ds.readUByte();
                    if (external != 0) {
                        throw new IOException(ds, "TODO");
                    }
                    final byte[] data = ds.readBytes((int) size-1);
                    final Chars[] names = new Chars(data,CharEncodings.UTF_16LE).split(0);
                    for (int i=0;i<files.length;i++) {
                        files[i].name = names[i];
                    }
                    break;
                    }
                case SevenZConstants.KWINATTRIBUTES : {
                    //windows file flags, TODO
                    ds.skipFully(size);
                    break;
                    }
                case SevenZConstants.KDUMMY : {
                    //padding, likely used by compressors
                    ds.skipFully(size);
                    break;
                    }
                default :
                    throw new IOException(ds, "Unexpected property type " + propertyType);
            }

        }

    }

}
