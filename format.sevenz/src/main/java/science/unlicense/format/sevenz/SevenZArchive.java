

package science.unlicense.format.sevenz;

import science.unlicense.archive.api.Archive;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.tree.NodeMessage;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.AbstractPath;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.encoding.api.store.Resource;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.sevenz.Decoder.Streams;
import science.unlicense.format.sevenz.FilesInfo.File;
import science.unlicense.math.api.Maths;

/**
 *
 * @author Johann Sorel
 */
public class SevenZArchive extends AbstractPath implements Archive {

    private final Path base;
    private final Sequence children = new ArraySequence();

    public SevenZArchive(Path base) throws IOException {
        this.base = base;

        final Decoder decoder = new Decoder();
        decoder.read(base);

        //extract archive
        final Streams streams = decoder.mainStreams;
        final Folder.Coder coder = streams.folders[0].coders[0];

        //total uncompressed size of lzma stream
        final long uncompressedSize = Maths.sum(streams.folders[0].unPackSize);

        int index = 0;
        long toSkip = 0;
        for (File f : decoder.filesInfo.files) {

            Chars[] split = f.name.split('/');
            Path parent = this;
            Collection parentCol = children;
            if (split.length > 1) {
                //find parent
                for (int i=0;i<split.length-1;i++) {
                    parent = parent.resolve(split[i]);
                    if (parent == null) {
                        throw new IOException(base, "parent not found "+split[i]);
                    }
                    parentCol = ((SevenZPath) parent).children;
                }
            }

            final SevenZPath path;
            if (f.isEmptyStream) {
                //folder
                path = new SevenZPath(parent, base, streams, coder, uncompressedSize, toSkip, 0, f);
            } else {
                long size = streams.folders[0].unPackSize[index];
                path = new SevenZPath(parent, base, streams, coder, uncompressedSize, toSkip, size, f);
                index++;
                toSkip += size;
            }
            parentCol.add(path);
        }
    }

    @Override
    public Format getFormat() {
        return SevenZFormat.INSTANCE;
    }

    @Override
    public PathFormat getPathFormat() {
        return SevenZFormat.INSTANCE;
    }

    @Override
    public boolean canHaveChildren() {
        return true;
    }

    @Override
    public Chars getName() {
        return base.getName();
    }

    @Override
    public Path getParent() {
        return base.getParent();
    }

    @Override
    public boolean isContainer() throws IOException {
        return true;
    }

    @Override
    public boolean exists() throws IOException {
        return base.exists();
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[]{NodeMessage.class};
    }

    @Override
    public Collection getChildren() {
        return Collections.readOnlySequence(children);
    }

    @Override
    public Path resolve(Chars address) {
        if (address==null) return this;

        if (address.startsWith('/')) {
            address = address.truncate(1,-1);
        }

        Chars[] split = address.split('/');

        Path root = this;
        Collection children = this.children;
        for (int i=0;i<split.length;i++) {
            root = null;
            final Iterator ite = children.createIterator();
            while (ite.hasNext()) {
                Path child = (Path) ite.next();
                if (child.getName().equals(split[i])) {
                    root = child;
                    children = ((SevenZPath) root).children;
                    break;
                }
            }
            if (root == null) {
                return null;
            }
        }

        return root;
    }

    @Override
    public PathResolver getResolver() {
        return this;
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        return base.createInputStream();
    }

    @Override
    public Chars toChars() {
        return new Chars("7Z archive : "+getName());
    }

    @Override
    public Chars toURI() {
        return base.toURI();
    }

    // WRITING OPERATIONS NOT SUPPORTED YET ////////////////////////////////////


    @Override
    public boolean createContainer() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean createLeaf() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Logger getLogger() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLogger(Logger logger) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getInput() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void dispose() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Resource find(Chars id) throws StoreException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
