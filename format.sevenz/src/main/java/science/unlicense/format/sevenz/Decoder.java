
package science.unlicense.format.sevenz;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.BufferedInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.SeekableByteBuffer;
import science.unlicense.encoding.api.io.SeekableInputStream;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.xz.LZMA2InputStream;
import science.unlicense.format.xz.LZMAInputStream;
import science.unlicense.math.api.Maths;

/**
 *
 * @author Johann Sorel
 */
public class Decoder {

    private static final int HEADER_SIZE = 32;

    static final class Streams {
        public long packPos;
        public long numPackStreams;
        public long[] packSizes;
        public boolean[] crcsDefined;
        public int[] crcs;

        public Folder[] folders;
    }

    //signature header
    int majorVersion;
    int minorVersion;
    int startHeaderCRC;
    long nextHeaderOffset;
    long nextHeaderSize;
    int nextHeaderCRC;

    //header
    FilesInfo filesInfo;
    private long datablocksSize;

    //archive properties
    final Dictionary properties = new HashDictionary();

    //main stream infos
    Streams mainStreams;

    //additional stream infos
    Streams addStreams;

    public void read(Path path) throws IOException {

        final SeekableByteBuffer seek = path.createSeekableBuffer(true, false, false);

        SeekableInputStream is = new SeekableInputStream(seek);
        SevenZInputStream ds = new SevenZInputStream(is);

        readSignatureHeader(ds);

        if (majorVersion != 0 && minorVersion != 2) {
            throw new IOException(ds, "Unsupported version");
        }

        //move to next header
        if (seek.getPosition() != HEADER_SIZE) { //start header size
            throw new IOException(ds, "Decoding error, uncorrect position");
        }
        seek.setPosition(HEADER_SIZE + nextHeaderOffset);

        int id = ds.readUByte();
        if (id == SevenZConstants.KENCODEDHEADER) {
            readEncodedHeader(ds);

            if (mainStreams.folders.length != 1) {
                throw new IOException(ds, "Only single root folder supported");
            }

            //move to real header
            seek.setPosition(HEADER_SIZE + mainStreams.packPos);
            final Folder.Coder coder = mainStreams.folders[0].coders[0];
            final long codecId = coder.codecId;
            ByteInputStream decodedStream;
            if (codecId == SevenZConstants.CODEC_LZMA) {
                final byte propsByte = coder.properties[0];
                final int dictSize = Endianness.LITTLE_ENDIAN.readInt(coder.properties, 1);
                datablocksSize = mainStreams.folders[0].unPackSize[0];
                decodedStream = new LZMAInputStream(is, mainStreams.folders[0].unPackSize[0], propsByte, dictSize);
            } else {
                throw new IOException(ds, "Only LZMA supported");
            }

            ds = new SevenZInputStream(decodedStream);
            id = ds.readUByte();
        }
        if (id == SevenZConstants.KHEADER) {
            readHeader(ds);
        } else {
            throw new IOException(ds, "Unexpected code : "+id);
        }

    }

    static ByteInputStream open(Path path, Streams streams, Folder.Coder coder, long uncompressedSize) throws IOException {

        final ByteInputStream is = path.createInputStream();
        final BufferedInputStream bi = new BufferedInputStream(is);
        final DataInputStream ds = new DataInputStream(bi);

        ds.skipFully(HEADER_SIZE + streams.packPos);
        final long codecId = coder.codecId;
        if (codecId == SevenZConstants.CODEC_LZMA) {
            final byte propsByte = coder.properties[0];
            final int dictSize = Endianness.LITTLE_ENDIAN.readInt(coder.properties, 1);
            return new LZMAInputStream(is, uncompressedSize, propsByte, dictSize);
        } else if (codecId == SevenZConstants.CODEC_LZMA2) {
            final byte propsByte = coder.properties[0];
            final int nbBits = propsByte & 0xFF;
            final int dictSize;
            if (nbBits == 0x28) {
                dictSize = 0xFFFFFFFF;
            } else {
                dictSize = (2 | (nbBits & 0x1)) << (nbBits / 2 + 11);
            }
            return new LZMA2InputStream(is, dictSize);
        } else {
            throw new IOException(ds, "Only LZMA supported");
        }
    }

    /**
     *
     * -------------------------------------------------------------
     *  BYTE kSignature[6] = {'7', 'z', 0xBC, 0xAF, 0x27, 0x1C};
     *
     *  ArchiveVersion
     *  {
     *    BYTE Major;   // now = 0
     *    BYTE Minor;   // now = 2
     *  };
     *
     *  UINT32 StartHeaderCRC;
     *
     *  StartHeader
     *  {
     *    REAL_UINT64 NextHeaderOffset
     *    REAL_UINT64 NextHeaderSize
     *    UINT32 NextHeaderCRC
     *  }
     * -------------------------------------------------------------
     */
    private void readSignatureHeader(SevenZInputStream ds) throws IOException{

        final byte[] signature = ds.readBytes(6);

        if (!Arrays.equals(signature, SevenZConstants.SIGNATURE)) {
            throw new IOException(ds, "Invalid signature");
        }

        majorVersion = ds.readUByte();
        minorVersion = ds.readUByte();
        startHeaderCRC = ds.readInt();
        nextHeaderOffset = ds.readLong();
        nextHeaderSize = ds.readLong();
        nextHeaderCRC = ds.readInt();
    }

    /**
     *
     * ------------------------------------------
     *  BYTE NID::kHeader (0x01)
     *
     *  []
     *  ArchiveProperties
     *  []
     *
     *  []
     *  BYTE NID::kAdditionalStreamsInfo; (0x03)
     *  StreamsInfo
     *  []
     *
     *  []
     *  BYTE NID::kMainStreamsInfo;    (0x04)
     *  StreamsInfo
     *  []
     *
     *  []
     *  FilesInfo
     *  []
     *
     *  BYTE NID::kEnd
     * ------------------------------------------
     */
    private void readHeader(SevenZInputStream ds) throws IOException {
        int id = ds.readUByte();

        if (id == SevenZConstants.KARCHIVEPROPERTIES) {
            readArchiveProperties(ds);
            id = ds.readUByte();
        }

        if (id == SevenZConstants.KADDITIONALSTREAMSINFO) {
            addStreams = new Streams();
            readStreamInfos(ds, addStreams);
            id = ds.readUByte();
        }

        if (id == SevenZConstants.KMAINSTREAMSINFO) {
            mainStreams = new Streams();
            readStreamInfos(ds, mainStreams);
            id = ds.readUByte();
        }

        if (id == SevenZConstants.KFILESINFO) {
            filesInfo = new FilesInfo();
            filesInfo.read(ds);
            id = ds.readUByte();
        }

        if (id != SevenZConstants.KEND) {
            throw new IOException(ds, "Missing end marker");
        }
    }

    /**
     * ------------------------------------------
     * []
     * BYTE NID::kEncodedHeader; (0x17)
     * StreamsInfo for Encoded Header
     * []
     * ------------------------------------------
     */
    private void readEncodedHeader(SevenZInputStream ds) throws IOException {
        mainStreams = new Streams();
        readStreamInfos(ds, mainStreams);
    }

    /**
     *
     * ------------------------------------------
     * BYTE NID::kArchiveProperties (0x02)
     * for (;;)
     * {
     *   BYTE PropertyType;
     *   if (aType == 0)
     *     break;
     *   UINT64 PropertySize;
     *   BYTE PropertyData[PropertySize];
     * }
     * ------------------------------------------
     */
    private void readArchiveProperties(SevenZInputStream ds) throws IOException {

        for (int type=ds.readUByte();type==SevenZConstants.KEND;type=ds.readUByte()) {
            long size = ds.readRealUnit64();
            final byte[] data = ds.readBytes((int) size);
            properties.add(type, data);
        }
    }

    /**
     * ------------------------------------------
     *  []
     *  PackInfo
     *  []
     *
     *  []
     *  CodersInfo
     *  []
     *
     *  []
     *  SubStreamsInfo
     *  []
     *
     *  BYTE NID::kEnd
     * ------------------------------------------
     */
    private void readStreamInfos(SevenZInputStream ds, Streams streams) throws IOException {
        int id = ds.readUByte();

        if (id == SevenZConstants.KPACKINFO) {
            readPackInfo(ds, streams);
            id = ds.readUByte();
        }

        if (id == SevenZConstants.KUNPACKINFO) {
            readCodersInfo(ds, streams);
            id = ds.readUByte();
        }

        if (id == SevenZConstants.KSUBSTREAMSINFO) {
            readSubStreamsInfo(ds, streams);
            id = ds.readUByte();
        }

        if (id != SevenZConstants.KEND) {
            throw new IOException(ds, "Missing end marker");
        }
    }

    /**
     * ------------------------------------------
     *  BYTE NID::kPackInfo  (0x06)
     *  UINT64 PackPos
     *  UINT64 NumPackStreams
     *
     *  []
     *  BYTE NID::kSize    (0x09)
     *  UINT64 PackSizes[NumPackStreams]
     *  []
     *
     *  []
     *  BYTE NID::kCRC      (0x0A)
     *  PackStreamDigests[NumPackStreams]
     *  []
     *
     *  BYTE NID::kEnd
     * ------------------------------------------
     */
    private void readPackInfo(SevenZInputStream ds, Streams streams) throws IOException {
        streams.packPos = ds.readRealUnit64();
        streams.numPackStreams = ds.readRealUnit64();

        int id = ds.readUByte();

        if (id == SevenZConstants.KSIZE) {
            streams.packSizes = ds.readRealUnit64((int) streams.numPackStreams);
            id = ds.readUByte();
        }

        if (id == SevenZConstants.KCRC) {
            //digests
            streams.crcsDefined = ds.readDigestDefined((int) streams.numPackStreams);
            streams.crcs = ds.readDigestCRC(streams.crcsDefined);
            id = ds.readUByte();
        }

        if (id != SevenZConstants.KEND) {
            throw new IOException(ds, "Missing end marker");
        }
    }

    /**
     * ------------------------------------------
     *  BYTE NID::kUnPackInfo  (0x07)
     *
     *  BYTE NID::kFolder  (0x0B)
     *  UINT64 NumFolders
     *  BYTE External
     *  switch(External)
     *  {
     *    case 0:
     *      Folders[NumFolders]
     *    case 1:
     *      UINT64 DataStreamIndex
     *  }
     *
     *  BYTE ID::kCodersUnPackSize  (0x0C)
     *  for(Folders)
     *    for(Folder.NumOutStreams)
     *     UINT64 UnPackSize;
     *
     *  []
     *  BYTE NID::kCRC   (0x0A)
     *  UnPackDigests[NumFolders]
     *  []
     *
     *  BYTE NID::kEnd
     * ------------------------------------------
     */
    private void readCodersInfo(SevenZInputStream ds, Streams streams) throws IOException {
        if (ds.readUByte() != SevenZConstants.KFOLDER) {
            throw new IOException(ds, "Unexpected marker");
        }

        long numFolders = ds.readRealUnit64();
        streams.folders = new Folder[(int) numFolders];
        for (int i=0;i<numFolders;i++) {
            streams.folders[i] = new Folder();
        }
        int external = ds.readUByte();

        if (external == 0) {
            for (int i=0;i<numFolders;i++) {
                streams.folders[i].read(ds);
            }
        } else if (external == 1) {
            throw new IOException(ds, "TODO");
        } else {
            throw new IOException(ds, "Unexpected external value");
        }

        int marker = ds.readUByte();
        if (marker != SevenZConstants.KCODERSUNPACKSIZE) {
            throw new IOException(ds, "Unexpected marker " + marker);
        }
        for (int i=0;i<streams.folders.length;i++) {
            streams.folders[i].unPackSize = ds.readRealUnit64((int) streams.folders[i].numOutStreamsTotal);
        }

        int id = ds.readUByte();
        if (id == SevenZConstants.KCRC) {
            //digests
            boolean[] defines = ds.readDigestDefined((int) streams.folders.length);
            for (int i=0;i<defines.length;i++) {
                streams.folders[i].withCrc = defines[i];
                if (defines[i]) {
                    streams.folders[i].crc = ds.readInt();
                }
            }
            id = ds.readUByte();
        }

        if (id != SevenZConstants.KEND) {
            throw new IOException(ds, "Unexpected marker" + id);
        }
    }

    /**
     * ------------------------------------------
     *  BYTE NID::kSubStreamsInfo; (0x08)
     *
     *  []
     *  BYTE NID::kNumUnPackStream; (0x0D)
     *  UINT64 NumUnPackStreamsInFolders[NumFolders];
     *  []
     *
     *  []
     *  BYTE NID::kSize  (0x09)
     *  UINT64 UnPackSizes[]
     *  []
     *
     *  []
     *  BYTE NID::kCRC  (0x0A)
     *  Digests[Number of streams with unknown CRC]
     *  []
     *
     *  BYTE NID::kEnd
     * ------------------------------------------
     */
    private void readSubStreamsInfo(SevenZInputStream ds, Streams streams) throws IOException {
        int id = ds.readUByte();

        if (id == SevenZConstants.KNUMUNPACKSTREAM) {
            for (int i=0;i<streams.folders.length;i++) {
                streams.folders[i].numUnPackStreams = ds.readRealUnit64();
                streams.folders[i].unPackSize = new long[(int) streams.folders[i].numUnPackStreams];
            }
            id = ds.readUByte();
        }

        for (Folder f : streams.folders) {
            if (id == SevenZConstants.KSIZE) {
                f.unPackSize = ds.readRealUnit64(f.unPackSize.length-1); // last is folder ?
                long sum = Maths.sum(f.unPackSize);
                f.unPackSize = Arrays.resize(f.unPackSize,f.unPackSize.length+1);
                f.unPackSize[f.unPackSize.length-1] = datablocksSize - sum;
            }
        }

        if (id == SevenZConstants.KSIZE) {
            id = ds.readUByte();
        }

        if (id == SevenZConstants.KCRC) {
            //digests
            int nbDigets = 0;
            for (final Folder folder : streams.folders) {
                if (folder.numUnPackStreams != 1 || !folder.withCrc) {
                    nbDigets += folder.numUnPackStreams;
                }
            }

            boolean[] defines = ds.readDigestDefined(nbDigets);
            ds.readDigestCRC(defines);
            id = ds.readUByte();
        }

        if (id != SevenZConstants.KEND) {
            throw new IOException(ds, "Missing end marker");
        }
    }

}
