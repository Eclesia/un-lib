
package science.unlicense.code.impl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.code.api.CodeFile;
import science.unlicense.code.api.CodeFileReader;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.syntax.api.parser.Parser;
import science.unlicense.syntax.api.parser.Rule;
import science.unlicense.syntax.api.parser.SyntaxNode;

/**
 * Code File reader using default grammar modele.
 *
 * @author Johann Sorel
 */
public class GrammarCodeFileReader implements CodeFileReader{

    private final Parser parser;

    public GrammarCodeFileReader(Rule rootRule) {
        parser = new Parser(rootRule);
    }

    @Override
    public void setInput(Object input) throws IOException {
        parser.setInput(input);
    }

    @Override
    public Object getInput() {
        return parser.getInput();
    }

    public Document getConfiguration() {
        return parser.getConfiguration();
    }

    public void setConfiguration(Document configuration) {
        parser.setConfiguration(configuration);
    }

    @Override
    public CodeFile read() throws IOException {
        final SyntaxNode sn = parser.parse();
        final CodeFile file = new CodeFile();
        final Object input = getInput();
        if (input instanceof Path) {
            file.path = new Chars(((Path) input).toURI());
        }
        file.syntax = sn;
        return file;
    }

    @Override
    public void dispose() throws IOException {
        parser.dispose();
    }

}
