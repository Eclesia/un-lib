
package science.unlicense.code.api.inst;


/**
 * Most primitive branching instruction.
 *
 *
 * @author Johann Sorel
 */
public class Jump implements CodeNode {

    /**
     * Condition may be null.
     * In this case, the jump is actually a goto.
     */
    private Reference condition;

    /**
     * Target instruction of the jump.
     */
    private Label label;

    public Jump() {
    }

    public Jump(Reference condition, Label label) {
        this.condition = condition;
        this.label = label;
    }

    public Reference getCondition() {
        return condition;
    }

    public void setCondition(Reference condition) {
        this.condition = condition;
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

}
