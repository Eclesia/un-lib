
package science.unlicense.code.api;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.Reader;

/**
 * Read code from input and split in tokens.
 * This step is not included in the CodeProducer to offer quick processing
 * such as syntax coloration or first stage validation.
 *
 * @author Johann Sorel
 */
public interface CodeFileReader extends Reader {

    /**
     * Convert the content of input to CodeFile.
     *
     * @return CodeFile
     * @throws IOException
     */
    CodeFile read() throws IOException;

}
