
package science.unlicense.code.api.inst;

/**
 * Exit of function.
 * All ends of a function instruction graph must be of this type.
 *
 * @author Johann Sorel
 */
public class Return implements CodeNode {

    /**
     * May be null.
     * In such case it is equivalent to a 'return void'.
     */
    private Reference value;

    public Return() {
    }

    public Return(Reference value) {
        this.value = value;
    }

    public Reference getValue() {
        return value;
    }

    public void setValue(Reference value) {
        this.value = value;
    }

}
