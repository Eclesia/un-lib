
package science.unlicense.code.api;

import science.unlicense.encoding.api.store.Format;

/**
 *
 * @author Johann Sorel
 */
public interface CodeFormat extends Format {

    /**
     * Create a code reader.
     *
     * @return CodeFileReader, never null
     */
    CodeFileReader createReader();

    /**
     * Create a code producer.
     *
     * @return CodeProducer, never null
     */
    CodeProducer createProducer();

}
