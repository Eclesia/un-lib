
package science.unlicense.code.api.inst;

/**
 *
 * @author Johann Sorel
 */
public class DoWhile implements CodeNode {

    private CodeNode condition;
    private CodeNode content;

    public DoWhile() {
    }

    public DoWhile(CodeNode condition, CodeNode content) {
        this.condition = condition;
        this.content = content;
    }

    public CodeNode getCondition() {
        return condition;
    }

    public void setCondition(CodeNode condition) {
        this.condition = condition;
    }

    public CodeNode getContent() {
        return content;
    }

    public void setContent(CodeNode content) {
        this.content = content;
    }

}
