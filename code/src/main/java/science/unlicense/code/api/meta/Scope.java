
package science.unlicense.code.api.meta;

/**
 * Define the scope of a Property,Class or Function.
 *
 * @author Johann Sorel
 */
public interface Scope extends Constraint {

}
