
package science.unlicense.code.api;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.Writer;

/**
 *
 * @author Johann Sorel
 */
public interface CodeFileWriter extends Writer {

    void write(CodeFile file) throws IOException;

}
