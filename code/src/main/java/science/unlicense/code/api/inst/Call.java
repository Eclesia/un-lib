
package science.unlicense.code.api.inst;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class Call implements CodeNode {

    public Reference target;

    public Chars function;

    /** References or expressions */
    public Sequence arguments;

    public Call() {
    }

}
