
package science.unlicense.code.api.inst;

/**
 *
 * @author Johann Sorel
 */
public class Affectation implements CodeNode {

    private Reference ref;

    private CodeNode value;

    public Affectation() {
    }

    public Affectation(Reference ref, CodeNode value) {
        this.ref = ref;
        this.value = value;
    }

    public Reference getRef() {
        return ref;
    }

    public void setRef(Reference ref) {
        this.ref = ref;
    }

    public CodeNode getValue() {
        return value;
    }

    public void setValue(CodeNode value) {
        this.value = value;
    }

}
