
package science.unlicense.code.api.inst;

import science.unlicense.common.api.character.Chars;

/**
 * Target of a jump instruction.
 *
 * labels have no effect, they as place holder is the function instruction graph
 * for jump instructions.
 *
 * @author Johann Sorel
 */
public class Label implements CodeNode {

    /**
     * Convenient name of the label, can be null.
     */
    private Chars name;

    public Label() {
    }

    public Label(Chars name) {
        this.name = name;
    }

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        this.name = name;
    }

}
