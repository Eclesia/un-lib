
package science.unlicense.code.api.meta;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DefaultMeta extends CObject implements Meta {

    protected final Chars id;

    public DefaultMeta(Chars id) {
        this.id = id;
    }

    public Chars getId() {
        return id;
    }

}
