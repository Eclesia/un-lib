
package science.unlicense.code.api.inst;

import science.unlicense.common.api.collection.Pair;

/**
 *
 * @author Johann Sorel
 */
public class Switch implements CodeNode {

    private CodeNode base;
    private Pair[] cases;

    public CodeNode getBase() {
        return base;
    }

    public void setBase(CodeNode base) {
        this.base = base;
    }

    public Pair[] getCases() {
        return cases;
    }

    public void setCases(Pair[] cases) {
        this.cases = cases;
    }

}
