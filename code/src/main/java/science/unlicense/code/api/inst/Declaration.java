
package science.unlicense.code.api.inst;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class Declaration implements CodeNode {

    private Chars id;

    public Declaration(Chars id) {
        this.id = id;
    }

    public Chars getId() {
        return id;
    }

    public void setId(Chars id) {
        this.id = id;
    }

}
