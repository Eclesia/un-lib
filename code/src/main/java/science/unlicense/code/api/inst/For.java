
package science.unlicense.code.api.inst;

/**
 *
 * @author Johann Sorel
 */
public class For implements CodeNode {

    private CodeNode init;
    private CodeNode condition;
    private CodeNode increment;
    private CodeNode content;

    public For() {
    }

    public For(CodeNode condition, CodeNode content) {
        this.condition = condition;
        this.content = content;
    }

    public CodeNode getInit() {
        return init;
    }

    public void setInit(CodeNode init) {
        this.init = init;
    }

    public CodeNode getCondition() {
        return condition;
    }

    public void setCondition(CodeNode condition) {
        this.condition = condition;
    }

    public CodeNode getIncrement() {
        return increment;
    }

    public void setIncrement(CodeNode increment) {
        this.increment = increment;
    }

    public CodeNode getContent() {
        return content;
    }

    public void setContent(CodeNode content) {
        this.content = content;
    }

}
