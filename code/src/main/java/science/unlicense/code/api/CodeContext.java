
package science.unlicense.code.api;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;

/**
 * Contains the context objects :
 * classes, functions and properties.
 *
 * @author Johann Sorel
 */
public class CodeContext {

    public CodeFormat format;

    /**
     * key : unique identifier Chars
     */
    public final Dictionary elements = new HashDictionary();

}
