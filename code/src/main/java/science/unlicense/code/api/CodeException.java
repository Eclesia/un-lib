
package science.unlicense.code.api;

/**
 *
 * @author Johann Sorel
 */
public class CodeException extends Exception {

    public CodeException() {
    }

    public CodeException(String message) {
        super(message);
    }

    public CodeException(Throwable cause) {
        super(cause);
    }

    public CodeException(String message, Throwable cause) {
        super(message, cause);
    }

}
