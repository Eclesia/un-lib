
package science.unlicense.code.api;

import science.unlicense.common.api.character.Chars;

/**
 * A transcoder reorganize CodeContexts object to be supported for another language.
 * This step is necessary even when there is common api between contexts because
 * languages all have different limitations.
 *
 * TODO : need a factory, must not be a singleton.
 *
 * @author Johann Sorel
 */
public interface Transcoder {

    Chars getSourceFormat();

    Chars getTargetFormat();

    CodeContext convert(CodeContext context);

}
