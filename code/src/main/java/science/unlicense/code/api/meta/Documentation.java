
package science.unlicense.code.api.meta;

/**
 * A documentation is a kind of meta which have strictly no influence on the code itself.
 * It is provided for human as description of the related object.
 *
 * @author Johann Sorel
 */
public interface Documentation extends Meta {


}
