
package science.unlicense.code.api.meta;

import science.unlicense.common.api.character.Chars;

/**
 * General concept of abstraction.
 *
 *
 * @author Johann Sorel
 */
public class DefaultAbstract extends DefaultMeta implements Abstract {

    public static final Chars ID = Chars.constant("abstract");

    public DefaultAbstract() {
        super(ID);
    }

}
