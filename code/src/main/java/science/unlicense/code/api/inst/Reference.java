
package science.unlicense.code.api.inst;

/**
 * A reference is a 'pointer' toward an object.
 * This is a virtual placeholder which should be replaced by a real instance
 * at execution.
 *
 * @author Johann Sorel
 */
public class Reference {

    /**
     * A special reference which is used to indicate the current object.
     * Used by object methods, often as first argument.
     */
    public static final Reference SELF = new Reference(new Object());

    public final Object id;

    public Reference(Object id) {
        this.id = id;
    }

}
