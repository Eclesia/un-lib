
package science.unlicense.code.api.inst;

/**
 * TODO
 *
 * @author Johann Sorel
 */
public class If {

    private CodeNode condition;
    private CodeNode content;

    public If() {
    }

    public If(CodeNode condition, CodeNode content) {
        this.condition = condition;
        this.content = content;
    }

    public CodeNode getCondition() {
        return condition;
    }

    public void setCondition(CodeNode condition) {
        this.condition = condition;
    }

    public CodeNode getContent() {
        return content;
    }

    public void setContent(CodeNode content) {
        this.content = content;
    }

}
