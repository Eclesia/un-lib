
package science.unlicense.code.api.inst;

/**
 * Plain value.
 * it's interpretation in based on the context.
 *
 * @author Johann Sorel
 */
public class Literal {

    /**
     * Raw bytes of the literal.
     */
    private Object value;

    public Literal() {
    }

    public Literal(byte[] value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

}
