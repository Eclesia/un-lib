
package science.unlicense.code.api;

import science.unlicense.common.api.character.Chars;

/**
 * A function in or out parameter.
 *
 * @author Johann Sorel
 */
public class Parameter {

    public Chars id;
    public Class type;
    public final Metas metas = new Metas();

}
