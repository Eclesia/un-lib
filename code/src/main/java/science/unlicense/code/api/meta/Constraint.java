
package science.unlicense.code.api.meta;

/**
 * Constraints are a larger general concept used to group scopes, preconditions,...
 *
 * @author Johann Sorel
 */
public interface Constraint extends Meta{

}
