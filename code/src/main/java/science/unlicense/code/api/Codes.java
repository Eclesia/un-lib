
package science.unlicense.code.api;

import science.unlicense.code.api.inst.Affectation;
import science.unlicense.code.api.inst.Call;
import science.unlicense.code.api.inst.CodeNode;
import science.unlicense.code.api.inst.CodeSequence;
import science.unlicense.code.api.inst.Declaration;
import science.unlicense.code.api.inst.Jump;
import science.unlicense.code.api.inst.Label;
import science.unlicense.code.api.inst.Return;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.predicate.Predicates;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.system.ModuleSeeker;

/**
 *
 * @author Johann Sorel
 */
public final class Codes {

    private Codes() {}

    /**
     * Get a list of available transcoders.
     *
     * @return pair of source to target formats.
     */
    public static Pair[] getTranscoders() {
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/format/science.unlicense.code.api.Transcoder"),
                Predicates.instanceOf(CodeFormat.class));
        final Pair[] formats = new Pair[results.getSize()];
        for (int i=0,n=results.getSize();i<n;i++) {
            final Transcoder trs = (Transcoder) results.get(i);
            formats[i] = new Pair(trs.getSourceFormat(), trs.getTargetFormat());
        }
        return formats;
    }

    public static Transcoder createTranscoder(Chars source, Chars target) {
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/format/science.unlicense.code.api.Transcoder"),
                Predicates.instanceOf(CodeFormat.class));
        for (int i=0,n=results.getSize();i<n;i++) {
            final Transcoder trs = (Transcoder) results.get(i);
            if (trs.getSourceFormat().equals(source) && trs.getTargetFormat().equals(target)) {
                return trs;
            }
        }
        throw new InvalidArgumentException("No transcoder from "+source+" to "+target);
    }

    /**
     * Lists available programming formats.
     *
     * @return array of CodeFormat, never null but can be empty.
     */
    public static CodeFormat[] getFormats() {
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/format/science.unlicense.code.api.CodeFormat"),
                Predicates.instanceOf(CodeFormat.class));
        final CodeFormat[] formats = new CodeFormat[results.getSize()];
        Collections.copy(results, formats, 0);
        return formats;
    }

    public static CodeFormat getFormat(Chars name) throws InvalidArgumentException{
        final CodeFormat[] formats = getFormats();
        for (int i=0;i<formats.length;i++) {
            if (formats[i].getIdentifier().equals(name)) {
                return formats[i];
            }
        }
        throw new InvalidArgumentException("Format "+name+" not found.");
    }

    public static CodeFormat getFormatForExtension(Chars ext) throws InvalidArgumentException{
        final CodeFormat[] formats = getFormats();
        for (int i=0;i<formats.length;i++) {
            final Iterator ite = formats[i].getExtensions().createIterator();
            while (ite.hasNext()) {
                final Chars e = (Chars) ite.next();
                if (e.equals(ext, true, true)) {
                    return formats[i];
                }
            }
        }
        throw new InvalidArgumentException("Format "+ext+" not found.");
    }

    /**
     * Convineent method to read a code source of unknowned format.
     * The method will loop on available formats until one can decode the source.
     *
     * @param input
     * @return CodeContext, never null
     * @throws IOException if not format could support the source.
     */
    public static CodeContext read(Object input) throws IOException, CodeException {
        if (!(input instanceof Path)) {
            throw new IOException(input, "Only Path input are supported for now.");
        }

        final Path path = (Path) input;
        final Chars ext = Paths.getExtension(new Chars(path.getName()));
        final CodeFormat format = getFormatForExtension(ext);

        final CodeFileReader reader = format.createReader();
        reader.setInput(input);
        final CodeFile codeFile = reader.read();

        final CodeProducer producer = format.createProducer();
        return producer.createCodeContext(codeFile);
    }

    /**
     * Convenient method to write a code context.
     * This method will search for the format with given name and try to write
     * the code.
     *
     * @param codeContext
     * @param format
     * @param output
     * @throws IOException
     */
    public static void write(CodeContext codeContext, Chars format, Object output) throws IOException, CodeException {
        if (!(output instanceof Path)) {
            throw new IOException(output, "Only Path output are supported for now.");
        }

        final Path path = (Path) output;
        final CodeFormat cf = getFormat(format);

        if (!codeContext.format.getIdentifier().equals(format)) {
            final Transcoder trs = createTranscoder(codeContext.format.getIdentifier(), format);
            codeContext = trs.convert(codeContext);
        }

        //convert to tokens
        final CodeProducer producer = cf.createProducer();
        final Sequence sequence = producer.createCodeFiles(codeContext);

        //write files
        final DefaultCodeFileWriter writer = new DefaultCodeFileWriter();
        for (int i=0,n=sequence.getSize();i<n;i++) {
            final CodeFile codeFile = (CodeFile) sequence.get(i);
            final Path p = path.resolve(codeFile.path);
            writer.setOutput(p);
            writer.write(codeFile);
        }
        writer.dispose();

    }


    /**
     * Atomic instructions are :
     * - Declaration
     * - Affectation
     * - Call
     * - Label
     * - Jump
     * - Return
     * Those instructions can not be decomposed.
     *
     * @param node
     * @return true if node is atomic
     */
    public static boolean isAtomic(CodeNode node) {
        return node instanceof Declaration
            || node instanceof Affectation
            || node instanceof Call
            || node instanceof Label
            || node instanceof Jump
            || node instanceof Return;
    }

    /**
     * Decompose this instruction in atomic instructions.
     * Has no effect if instruction is already atomic.
     *
     * @return
     */
    public static CodeSequence decompose(CodeNode node) {
        throw new RuntimeException("TODO");
    }

}
