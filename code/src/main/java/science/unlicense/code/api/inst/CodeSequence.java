
package science.unlicense.code.api.inst;

import science.unlicense.common.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class CodeSequence implements CodeNode {

    private Sequence codes;

    public Sequence getCodes() {
        return codes;
    }

    public void setCodes(Sequence codes) {
        this.codes = codes;
    }

}
