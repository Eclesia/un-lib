
package science.unlicense.code.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.code.api.inst.CodeNode;

/**
 * A static function or class method.
 *
 * @author Johann Sorel
 */
public class Function {

    public Chars id;

    public final Metas metas = new Metas();

    /**
     * Function input parameters.
     */
    public final Sequence inParameters = new ArraySequence();
    /**
     * Function output parameters.
     */
    public final Sequence outParameters = new ArraySequence();

    /**
     * Root instruction of the function.
     */
    public CodeNode startInstruction;

}
