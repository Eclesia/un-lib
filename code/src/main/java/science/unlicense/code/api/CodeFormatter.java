
package science.unlicense.code.api;

import science.unlicense.common.api.collection.Set;

/**
 *
 * @author Johann Sorel
 */
public interface CodeFormatter {

    /**
     * Get set of supported languages.
     * The values match the id of CodeFormats.
     *
     * @return Set of Chars never null
     */
    Set getSupportedLanguage();

    /**
     * Reorganize and possibly create tokens to match formatting.
     *
     * @param codeFile
     * @return
     */
    CodeFile format(CodeFile codeFile);

}
