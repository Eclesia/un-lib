
package science.unlicense.code.api.meta;

/**
 * Indicate an abstract class or method.
 *
 * @author Johann Sorel
 */
public interface Abstract extends Meta {

}
