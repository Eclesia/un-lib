
package science.unlicense.code.api;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;

/**
 * A static or class property.
 *
 * @author Johann Sorel
 */
public class Property extends CObject {

    public Chars id;
    public Class objclass;
    //find a way to remove use of jre class
    public java.lang.Class primclass;
    public final Metas metas = new Metas();

    public Property() {
    }

    public Property(Chars name, Class objclass, java.lang.Class primclass) {
        this.id = name;
        this.objclass = objclass;
        this.primclass = primclass;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Property other = (Property) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.objclass != other.objclass && (this.objclass == null || !this.objclass.equals(other.objclass))) {
            return false;
        }
        if (this.primclass != other.primclass && (this.primclass == null || !this.primclass.equals(other.primclass))) {
            return false;
        }
        if (this.metas != other.metas && (this.metas == null || !this.metas.equals(other.metas))) {
            return false;
        }
        return true;
    }

    @Override
    public int getHash() {
        int hash = 7;
        hash = 31 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 31 * hash + (this.objclass != null ? this.objclass.hashCode() : 0);
        hash = 31 * hash + (this.primclass != null ? this.primclass.hashCode() : 0);
        hash = 31 * hash + (this.metas != null ? this.metas.hashCode() : 0);
        return hash;
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(id).append(':').append(CObjects.toChars(primclass));
        return Nodes.toChars(cb.toChars(), metas);
    }

}
