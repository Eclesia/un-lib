
package science.unlicense.code.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.syntax.api.parser.SyntaxNode;

/**
 * A CodeFile is the representation of one or several objects
 * as tokens for a single file.
 *
 * @author Johann Sorel
 */
public class CodeFile {

    /**
     * Relative path of the code file.
     */
    public Chars path;

    public SyntaxNode syntax;

}
