
package science.unlicense.code.vm;

import science.unlicense.code.api.inst.Reference;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class VMContext {

    private final HashDictionary heap = new HashDictionary();
    private final Sequence stack = new ArraySequence();
    private long id = 0;

    public Reference stackPick() {
        return (Reference) stack.get(stack.getSize()-1);
    }

    public void stackPush(Reference ref) {
        stack.add(ref);
    }

    /**
     *
     * @param ref
     * @return Function or Instance
     */
    public Object heapGet(Reference ref) {
        return heap.getValue(ref);
    }

    public synchronized Instance newInstance(science.unlicense.code.api.Class clazz) {
        final Reference ref = new Reference(id++);
        final Instance ist = new Instance(ref,clazz);
        heap.add(ist, ist);
        return ist;
    }

}
