
package science.unlicense.code.vm;

/**
 * Virtual machine exception.
 *
 * @author Johann Sorel
 */
public class VMException extends Exception{

    public VMException() {
        super();
    }

    public VMException(String message) {
        super(message);
    }

    public VMException(Throwable ex) {
        super(ex);
    }

    public VMException(String message, Throwable ex) {
        super(message,ex);
    }

}
