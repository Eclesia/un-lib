
package science.unlicense.code.vm;

import science.unlicense.code.api.Function;
import science.unlicense.code.api.inst.Reference;

/**
 * DRAFT experimental work on a virtual machine.
 * Totaly inefficient, made only to confirm the code API is complete.
 *
 * @author Johann Sorel
 */
public class VirtualMachine {

    private final VMContext context = new VMContext();

    public VirtualMachine() {

    }

    public VMContext getContext() {
        return context;
    }

    public void execute(Reference ref) {
        final Object main = context.heapGet(ref);
        if (!(main instanceof Function)) {
            throw new RuntimeException("Main entry point is not a function :"+ ref);
        }
        final Function fct = (Function) main;
        //fct.execute(context);
    }

}
