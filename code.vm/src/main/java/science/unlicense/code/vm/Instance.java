
package science.unlicense.code.vm;

import science.unlicense.code.api.Class;
import science.unlicense.code.api.inst.Reference;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;

/**
 *
 * @author Johann Sorel
 */
public class Instance {

    private final science.unlicense.code.api.Class clazz;
    private final Reference ref;
    private final Dictionary properties = new HashDictionary();

    public Instance(Reference ref, Class clazz) {
        this.ref = ref;
        this.clazz = clazz;
    }

    public Reference getReference() {
        return ref;
    }

    public Class getClazz() {
        return clazz;
    }

    public Instance get(Reference ref) {
        if (ref==Reference.SELF) {
            return this;
        } else {
            return (Instance) properties.getValue(ref);
        }
    }

    public void set(Reference ref, Instance value) {
        properties.add(ref, value);
    }

}
