

package science.unlicense.physics.api.skeleton;

import org.junit.Test;
import science.unlicense.math.api.Angles;

/**
 *
 * @author Johann Sorel
 */
public class IKCCDTest {

    /**
     * Sanity test to ensure the most basic test works.
     */
    @Test
    public void testSanity(){

        final Joint target = new Joint(3);
        final Joint base = new Joint(3);
        final Joint effector = new Joint(3);
        target.getNodeTransform().getTranslation().setXYZ(10, 0, 0);
        target.getNodeTransform().notifyChanged();
        base.getNodeTransform().getTranslation().setXYZ(0, 0, 0);
        base.getNodeTransform().notifyChanged();
        effector.getNodeTransform().getTranslation().setXYZ(0, 10, 0);
        effector.getNodeTransform().notifyChanged();

        base.getChildren().add(effector);

        final Skeleton skeleton = new Skeleton();
        skeleton.getChildren().add(base);
        skeleton.getChildren().add(target);
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        final IKSolverCCD solver = new IKSolverCCD(10, Angles.degreeToRadian(45),3);

        final InverseKinematic kinematic = new InverseKinematic(target, effector, new Joint[]{base}, solver);
        kinematic.solve();

        System.out.println(effector.getNodeTransform().getTranslation());

    }

}
