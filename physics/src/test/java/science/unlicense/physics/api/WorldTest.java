
package science.unlicense.physics.api;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.physics.api.body.BodyGroup;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.integration.EulerIntegrator;

/**
 *
 * @author Johann Sorel
 */
public class WorldTest {

    /**
     * Test same collision group
     */
    @Test
    public void testSameCollisionGroup(){

        //collision test
        final RigidBody rb1 = new RigidBody(new Sphere(1), 1);
        final RigidBody rb2 = new RigidBody(new Sphere(1), 1);
        rb2.getNodeTransform().getTranslation().setXYZ(1, 0, 0);
        rb2.getNodeTransform().notifyChanged();

        final BodyGroup group = new BodyGroup(new Chars("group"));
        rb1.setGroup(group);
        rb2.setGroup(group);

        final World world = new DefaultWorld(3);
        world.setIntegrator(new EulerIntegrator(3));
        world.getGroups().add(group);
        world.addBody(rb1);
        world.addBody(rb2);

        world.update(1);

        Assert.assertEquals(new Vector3f64(0, 0, 0), rb1.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector3f64(1, 0, 0), rb2.getNodeTransform().getTranslation());
    }

    /**
     * Test collision groups are correctly used.
     */
    @Test
    public void testDifferentCollisionGroup(){

        //collision test
        final RigidBody rb1 = new RigidBody(new Sphere(1), 1);
        final RigidBody rb2 = new RigidBody(new Sphere(1), 1);
        rb2.getNodeTransform().getTranslation().setXYZ(1, 0, 0);
        rb2.getNodeTransform().notifyChanged();

        final BodyGroup group1 = new BodyGroup(new Chars("group1"));
        final BodyGroup group2 = new BodyGroup(new Chars("group2"));
        rb1.setGroup(group1);
        rb2.setGroup(group2);

        final World world = new DefaultWorld(3);
        world.setIntegrator(new EulerIntegrator(3));
        world.getGroups().add(group1);
        world.getGroups().add(group2);
        world.addBody(rb1);
        world.addBody(rb2);

        world.update(1);

        Assert.assertEquals(new Vector3f64(-0.5, 0, 0), rb1.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector3f64( 1.5, 0, 0), rb2.getNodeTransform().getTranslation());
    }

    @Test
    public void testCircle_circle() throws OperationException{
        final RigidBody geom1 = new RigidBody(new Circle(2),1);
        geom1.setWorldPosition(new Vector2f64(100, 200));
        geom1.getMotion().getVelocity().setY(0);
        //vertical penetration of 2 unit
        final RigidBody geom2 = new RigidBody(new Circle(3),0);
        geom2.setWorldPosition(new Vector2f64(100, 203));

        final DefaultWorld world = new DefaultWorld(2);
        world.setIntegrationTimeStep(2);
        world.setDrag(1.0);
        world.addBody(geom1);
        world.addBody(geom2);

        world.update(2);

        //geom 1 should be pushed back
        Assert.assertEquals(new Matrix3x3(
                1, 0, 100,
                0, 1, 198,
                0, 0,  1),
                geom1.getNodeTransform().viewMatrix());
    }


}
