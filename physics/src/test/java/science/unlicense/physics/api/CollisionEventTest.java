
package science.unlicense.physics.api;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.physics.api.body.RigidBody;

/**
 *
 * @author Johann Sorel
 */
public class CollisionEventTest {

    @Test
    public void testCollisionEvent(){

        //collision test
        final RigidBody rb1 = new RigidBody(new Sphere(1), 1);
        final RigidBody rb2 = new RigidBody(new Sphere(1), 1);
        rb1.setPhantom(true);

        final World world = new DefaultWorld(3);
        world.addBody(rb1);
        world.addBody(rb2);

        final Catcher catcher = new Catcher();
        world.getCollisionsState().addEventListener(CollisionMessage.PREDICATE, catcher);


        catcher.reset();
        rb2.getNodeTransform().getTranslation().setXYZ(5, 0, 0);
        rb2.getNodeTransform().notifyChanged();
        world.update(1);
        Assert.assertEquals(0,world.getCollisionsState().getCollisions().getSize());
        //expect no event
        Assert.assertEquals(0, catcher.nbEvent);
        Assert.assertNull(catcher.event);


        catcher.reset();
        rb2.getNodeTransform().getTranslation().setXYZ(1, 0, 0);
        rb2.getNodeTransform().notifyChanged();
        world.update(1);
        Assert.assertEquals(1,world.getCollisionsState().getCollisions().getSize());
        //expect a collision event
        Assert.assertEquals(1, catcher.nbEvent);
        Assert.assertEquals(CollisionMessage.TYPE_COLLIDE, catcher.event.getType());


        catcher.reset();
        rb2.getNodeTransform().getTranslation().setXYZ(0, 0, 0);
        rb2.getNodeTransform().notifyChanged();
        world.update(1);
        Assert.assertEquals(1,world.getCollisionsState().getCollisions().getSize());
        //collision is still here, expect no event
        Assert.assertEquals(0, catcher.nbEvent);
        Assert.assertNull(catcher.event);


        catcher.reset();
        rb2.getNodeTransform().getTranslation().setXYZ(5, 0, 0);
        rb2.getNodeTransform().notifyChanged();
        world.update(1);
        Assert.assertEquals(0,world.getCollisionsState().getCollisions().getSize());
        //collision ended, expect a separation event
        Assert.assertEquals(1, catcher.nbEvent);
        Assert.assertEquals(CollisionMessage.TYPE_SEPARATE, catcher.event.getType());

    }


    private static class Catcher implements EventListener {

        private int nbEvent = 0;
        private CollisionMessage event;

        public void reset(){
            nbEvent = 0;
            event = null;
        }

        public void receiveEvent(Event event) {
            this.event = (CollisionMessage) event.getMessage();
            nbEvent++;
        }

    }

}
