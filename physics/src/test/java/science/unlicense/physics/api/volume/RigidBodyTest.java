
package science.unlicense.physics.api.volume;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.math.impl.Affine3;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.physics.api.body.RigidBody;

/**
 *
 * @author Johann Sorel
 */
public class RigidBodyTest {

    /**
     * Basic test, ensure the base maths are ok.
     */
    @Test
    public void testSanity(){

        final Sphere sphere = new Sphere(1);
        final RigidBody body = new RigidBody(sphere, 1);
        body.setUpdateMode(RigidBody.UPDATE_LOCAL);
        body.getNodeTransform().getTranslation().localAdd(new Vector3f64(1, 2, 3));
        body.getNodeTransform().notifyChanged();

        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 1,
                0, 1, 0, 2,
                0, 0, 1, 3,
                0, 0, 0, 1),
                body.getNodeTransform().viewMatrix());

        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -1,
                0, 1, 0, -2,
                0, 0, 1, -3,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

    }

    @Test
    public void testLocalModeNoParent(){

        final Sphere sphere = new Sphere(1);
        final RigidBody body = new RigidBody(sphere, 1);
        body.setUpdateMode(RigidBody.UPDATE_LOCAL);
        body.getNodeTransform().getTranslation().localAdd(new Vector3f64(1, 2, 3));
        body.getNodeTransform().notifyChanged();

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5));

        //world transform should be as expected
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

        //the local transform should have changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 3,
                0, 1, 0, 4,
                0, 0, 1, 5,
                0, 0, 0, 1),
                body.getNodeTransform().viewMatrix());

    }

    @Test
    public void testLocalModeParent(){

        final Sphere sphere = new Sphere(1);
        final RigidBody body = new RigidBody(sphere, 1);
        body.setUpdateMode(RigidBody.UPDATE_LOCAL);
        body.getNodeTransform().getTranslation().localAdd(new Vector3f64(1, 2, 3));
        body.getNodeTransform().notifyChanged();

        //create a parent
        final SceneNode parent = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        parent.getChildren().add(body);

        Assert.assertEquals(parent, body.getParent());

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5));

        //world transform should be as expected
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

        //the local transform should have changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 3,
                0, 1, 0, 4,
                0, 0, 1, 5,
                0, 0, 0, 1),
                body.getNodeTransform().viewMatrix());

        //check that the parent was not updated
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1),
                parent.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1),
                parent.getRootToNodeSpace().toMatrix());

    }

    @Test
    public void testParentModeNoParent(){

        final Sphere sphere = new Sphere(1);
        final RigidBody body = new RigidBody(sphere, 1);
        body.setUpdateMode(RigidBody.UPDATE_PARENT);
        body.getNodeTransform().getTranslation().localAdd(new Vector3f64(1, 2, 3));
        body.getNodeTransform().notifyChanged();

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5));

        //world transform should be as expected
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

        //the local transform should have changed
        //there is no parent so this body transform should have changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 3,
                0, 1, 0, 4,
                0, 0, 1, 5,
                0, 0, 0, 1),
                body.getNodeTransform().viewMatrix());

    }

    @Test
    public void testParentModeParent(){

        final Sphere sphere = new Sphere(1);
        final RigidBody body = new RigidBody(sphere, 1);
        body.setUpdateMode(RigidBody.UPDATE_PARENT);
        body.getNodeTransform().getTranslation().localAdd(new Vector3f64(1, 2, 3));
        body.getNodeTransform().notifyChanged();

        //create a parent
        final SceneNode parent = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        parent.getChildren().add(body);

        Assert.assertEquals(parent, body.getParent());

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5));

        //the local transform should not have changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 1,
                0, 1, 0, 2,
                0, 0, 1, 3,
                0, 0, 0, 1),
                body.getNodeTransform().viewMatrix());

        //world transform should be as expected
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

        //check that the parent was updated
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 2,
                0, 1, 0, 2,
                0, 0, 1, 2,
                0, 0, 0, 1),
                parent.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -2,
                0, 1, 0, -2,
                0, 0, 1, -2,
                0, 0, 0, 1),
                parent.getRootToNodeSpace().toMatrix());

    }


}
