

package science.unlicense.physics.api.constraint;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.api.MatrixRW;

/**
 *
 * @author Johann Sorel
 */
public class FocusConstraintTest {

    private static final double EPSILON = 1e-12;

    @Test
    public void testFocusBackward(){

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode node = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode target = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);

        scene.getChildren().add(node);
        scene.getChildren().add(target);

        target.getNodeTransform().getTranslation().setXYZ(10, 0, 0);
        target.getNodeTransform().notifyChanged();

        final FocusConstraint constraint = new FocusConstraint(node, target);
        constraint.apply();

        final MatrixRW m = node.getNodeTransform().getRotation();
        Assert.assertArrayEquals(new double[]{
            0,0,1,
            0,1,0,
            1,0,0
        }, m.toArrayDouble(), EPSILON);

    }

    @Test
    public void testHierarchy(){

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode node = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode target = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);

        scene.getChildren().add(node);
        scene.getChildren().add(target);

        target.getNodeTransform().getTranslation().setXYZ(-2, 5, 7);
        target.getNodeTransform().notifyChanged();

        node.getNodeTransform().getTranslation().setXYZ(-2, 5, 3);
        node.getNodeTransform().notifyChanged();

        final FocusConstraint constraint = new FocusConstraint(node, target);
        constraint.apply();

        final MatrixRW m = node.getNodeTransform().getRotation();
        Assert.assertArrayEquals(new double[]{
           -1,0,0,
            0,1,0,
            0,0,1
        }, m.toArrayDouble(), EPSILON);

    }

}
