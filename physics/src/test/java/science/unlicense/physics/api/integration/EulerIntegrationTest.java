

package science.unlicense.physics.api.integration;

import org.junit.Test;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.physics.api.DefaultWorld;
import science.unlicense.physics.api.body.RigidBody;

/**
 *
 * @author Johann Sorel
 */
public class EulerIntegrationTest extends AbstractIntegratorTest{

    @Override
    protected Integrator createIntegrator() {
        return new EulerIntegrator(3);
    }

    @Test
    public void testTranslationIntegration(){

        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getVelocity().setXYZ(1, 2, 3);
        rb.setMass(1);
        final Similarity trs = rb.getNodeTransform();

        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);

        world.update(1);
        assertVEquals(new Vector3f64(1, 2, 3), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());

        world.update(1);
        assertVEquals(new Vector3f64(2, 4, 6), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());

        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector3f64(2.5, 5, 7.5), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());

    }

    @Test
    public void testTranslationIntermetiateIntegration(){

        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getVelocity().setXYZ(1, 2, 3);
        rb.setMass(1);
        final Similarity trs = rb.getNodeTransform();

        final SceneNode parent = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        parent.getNodeTransform().getTranslation().setXYZ(4, 4, 4);
        parent.getNodeTransform().notifyChanged();
        parent.getChildren().add(rb);

        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);

        world.update(1);
        assertVEquals(new Vector3f64(1, 2, 3), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());

        world.update(1);
        assertVEquals(new Vector3f64(2, 4, 6), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());

        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector3f64(2.5, 5, 7.5), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());

    }

    @Test
    public void testTranslationParentUpdateIntegration(){

        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.setUpdateMode(RigidBody.UPDATE_PARENT);
        rb.getMotion().getVelocity().setXYZ(1, 2, 3);
        rb.getNodeTransform().getTranslation().setXYZ(0.1, 0.2, 0.3);
        rb.getNodeTransform().notifyChanged();
        rb.setMass(1);
        final Similarity rbtrs = rb.getNodeTransform();

        final SceneNode parent = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        parent.getNodeTransform().getTranslation().setXYZ(4, 4, 4);
        parent.getNodeTransform().notifyChanged();
        parent.getChildren().add(rb);
        final Similarity parenttrs = parent.getNodeTransform();

        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);

        world.update(1);
        assertVEquals(new Vector3f64(0.1, 0.2, 0.3), rbtrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), rbtrs.getRotation());
        assertVEquals(new Vector3f64(5, 6, 7), parenttrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), parenttrs.getRotation());

        world.update(1);
        assertVEquals(new Vector3f64(0.1, 0.2, 0.3), rbtrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), rbtrs.getRotation());
        assertVEquals(new Vector3f64(6, 8, 10), parenttrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), parenttrs.getRotation());

        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector3f64(0.1, 0.2, 0.3), rbtrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), rbtrs.getRotation());
        assertVEquals(new Vector3f64(6.5, 9, 11.5), parenttrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), parenttrs.getRotation());

    }

    @Test
    public void testRotationIntegration(){

        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getAngularVelocity().setXYZ(0, Maths.QUATER_PI, 0);
        rb.setMass(1);
        final Similarity trs = rb.getNodeTransform();


        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);

        world.update(1);
        assertVEquals(new Vector3f64(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI, new Vector3f64(0, 1, 0)), trs.getRotation());

        world.update(1);
        assertVEquals(new Vector3f64(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2, new Vector3f64(0, 1, 0)), trs.getRotation());

        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector3f64(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2.5, new Vector3f64(0, 1, 0)), trs.getRotation());

    }

    @Test
    public void testRotationIntermediateIntegration(){

        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getAngularVelocity().setXYZ(0, Maths.QUATER_PI, 0);
        rb.setMass(1);
        final Similarity trs = rb.getNodeTransform();

        final SceneNode parent = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        parent.getNodeTransform().getTranslation().setXYZ(4, 4, 4);
        parent.getNodeTransform().notifyChanged();
        parent.getChildren().add(rb);


        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);

        world.update(1);
        assertVEquals(new Vector3f64(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI, new Vector3f64(0, 1, 0)), trs.getRotation());

        world.update(1);
        assertVEquals(new Vector3f64(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2, new Vector3f64(0, 1, 0)), trs.getRotation());

        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector3f64(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2.5, new Vector3f64(0, 1, 0)), trs.getRotation());

    }

    @Test
    public void testRotationParentUpdateIntegration(){

        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.setUpdateMode(RigidBody.UPDATE_PARENT);
        rb.getMotion().getAngularVelocity().setXYZ(0, Maths.QUATER_PI, 0);
        rb.setMass(1);
        final Similarity trs = rb.getNodeTransform();

        final SceneNode parent = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        parent.getNodeTransform().getTranslation().setXYZ(4, 4, 4);
        parent.getNodeTransform().notifyChanged();
        parent.getChildren().add(rb);
        final Similarity parenttrs = parent.getNodeTransform();


        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);

        world.update(1);
        assertVEquals(new Vector3f64(0, 0, 0), trs.getTranslation());
        assertMEquals(new Matrix3x3().setToIdentity(), trs.getRotation());
        assertVEquals(new Vector3f64(4, 4, 4), parenttrs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI, new Vector3f64(0, 1, 0)), parenttrs.getRotation());

        world.update(1);
        assertVEquals(new Vector3f64(0, 0, 0), trs.getTranslation());
        assertMEquals(new Matrix3x3().setToIdentity(), trs.getRotation());
        assertVEquals(new Vector3f64(4, 4, 4), parenttrs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2, new Vector3f64(0, 1, 0)), parenttrs.getRotation());

        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector3f64(0, 0, 0), trs.getTranslation());
        assertMEquals(new Matrix3x3().setToIdentity(), trs.getRotation());
        assertVEquals(new Vector3f64(4, 4, 4), parenttrs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2.5, new Vector3f64(0, 1, 0)), parenttrs.getRotation());

    }

    @Test
    public void testTranslationAndRotationIntegration(){

        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getVelocity().setXYZ(1, 2, 3);
        rb.getMotion().getAngularVelocity().setXYZ(0, Maths.QUATER_PI, 0);
        rb.setMass(1);
        final Similarity trs = rb.getNodeTransform();


        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);

        world.update(1);
        assertVEquals(new Vector3f64(1, 2, 3), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI, new Vector3f64(0, 1, 0)), trs.getRotation());

        world.update(1);
        assertVEquals(new Vector3f64(2, 4, 6), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2, new Vector3f64(0, 1, 0)), trs.getRotation());

        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector3f64(2.5, 5, 7.5), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2.5, new Vector3f64(0, 1, 0)), trs.getRotation());

    }

    /**
     * Intermediate node in the scene
     */
    @Test
    public void testTranslationAndRotationIntermetiateIntegration(){

        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getVelocity().setXYZ(1, 2, 3);
        rb.getMotion().getAngularVelocity().setXYZ(0, Maths.QUATER_PI, 0);
        rb.setMass(1);
        final Similarity trs = rb.getNodeTransform();

        final SceneNode parent = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        parent.getNodeTransform().getTranslation().setXYZ(4, 4, 4);
        parent.getNodeTransform().notifyChanged();
        parent.getChildren().add(rb);


        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);

        world.update(1);
        assertVEquals(new Vector3f64(1, 2, 3), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI, new Vector3f64(0, 1, 0)), trs.getRotation());

        world.update(1);
        assertVEquals(new Vector3f64(2, 4, 6), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2, new Vector3f64(0, 1, 0)), trs.getRotation());

        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector3f64(2.5,5,7.5),trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2.5, new Vector3f64(0, 1, 0)), trs.getRotation());

    }

}
