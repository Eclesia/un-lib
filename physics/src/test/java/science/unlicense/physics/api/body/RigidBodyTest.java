
package science.unlicense.physics.api.body;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.math.impl.Affine3;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class RigidBodyTest {

    /**
     * Single body set world position test.
     */
    @Test
    public void testSetWorldPosition(){
        final RigidBody body = new RigidBody(new Circle(2),1);
        body.setWorldPosition(new Vector2f64(100, 200));

        Assert.assertEquals(new Matrix3x3(
                1, 0, -100,
                0, 1, -200,
                0, 0,  1),
                body.getRootToNodeSpace().toMatrix());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 100,
                0, 1, 200,
                0, 0,  1),
                body.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 100,
                0, 1, 200,
                0, 0,  1),
                body.getNodeToRootSpace().toMatrix());

        Assert.assertEquals(new Vector2f64(100,200), body.getWorldPosition(null));
    }

    @Test
    public void testSetRootToNodeSpace_Single(){
        final RigidBody body = new RigidBody(new Sphere(2),1);
        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2));

        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

        Assert.assertEquals(new Vector3f64(-9,3,-2), body.getWorldPosition(null));
    }

    @Test
    public void testSetRootToNodeSpace_ParentNoUpdate(){
        final DefaultSceneNode parent = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final RigidBody body = new RigidBody(new Sphere(2),1);
        parent.getChildren().add(body);

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2));

        //body should have moved
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0,-9,
                0, 1, 0, 3,
                0, 0, 1,-2,
                0, 0, 0, 1),
                body.getNodeTransform().viewMatrix());
        //parent should not have moved
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1),
                parent.getNodeTransform().viewMatrix());

        Assert.assertEquals(new Vector3f64(-9,3,-2), body.getWorldPosition(null));
    }

    /**
     * Test the update mode modifies the parent transform and not the body.
     */
    @Test
    public void testSetRootToNodeSpace_ParentUpdate(){
        final DefaultSceneNode parent = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final RigidBody body = new RigidBody(new Sphere(2),1);
        body.setUpdateMode(RigidBody.UPDATE_PARENT);
        body.getNodeTransform().set(new Matrix4x4(
                1, 0, 0, 1,
                0, 1, 0,-2,
                0, 0, 1, 3,
                0, 0, 0, 1));
        parent.getChildren().add(body);

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2));

        //body should have moved
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());
        //but not it's trandform
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 1,
                0, 1, 0,-2,
                0, 0, 1, 3,
                0, 0, 0, 1),
                body.getNodeTransform().viewMatrix());
        //parent should have moved
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0,-10,
                0, 1, 0, 5,
                0, 0, 1,-5,
                0, 0, 0, 1),
                parent.getNodeTransform().viewMatrix());

        Assert.assertEquals(new Vector3f64(-9,3,-2), body.getWorldPosition(null));
    }

}
