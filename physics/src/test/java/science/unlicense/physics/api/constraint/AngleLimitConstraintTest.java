

package science.unlicense.physics.api.constraint;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.impl.Matrices;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;

/**
 * Test angle limit constraint.
 *
 * @author Johann Sorel
 */
public class AngleLimitConstraintTest {

    private static final double DELTA = 0.0000000001;

    @Test
    public void testZero(){
        final SceneNode node = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final AngleLimitConstraint cst = new AngleLimitConstraint(
                node, new BBox(
                        new Vector3f64(Angles.degreeToRadian(-40), Angles.degreeToRadian(-30), Angles.degreeToRadian(-20)),
                        new Vector3f64(Angles.degreeToRadian(+40), Angles.degreeToRadian(+30), Angles.degreeToRadian(+20))
                ));

        cst.apply();
        testAngles(node, 0, 0, 0);
    }

    //TODO fix it
    @Ignore
    @Test
    public void testOutside(){
        final SceneNode node = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        setToAngles(node, -50, -60, -30);

        final AngleLimitConstraint cst = new AngleLimitConstraint(
                node, new BBox(
                        new Vector3f64(Angles.degreeToRadian(-40), Angles.degreeToRadian(-30), Angles.degreeToRadian(-20)),
                        new Vector3f64(Angles.degreeToRadian(+40), Angles.degreeToRadian(+30), Angles.degreeToRadian(+20))
                ));
        cst.apply();
        testAngles(node, -40, -30, -20);
    }

    /**
     *
     * @param node
     * @param ax in degrees
     * @param ay in degrees
     * @param az in degrees
     */
    private static void testAngles(SceneNode node, double ax, double ay, double az){
        // check the transform
        MatrixRW m = node.getNodeTransform().getRotation();
        double[] euler = Matrices.toEuler(m.getValuesCopy(), null);
        Assert.assertEquals(Angles.degreeToRadian(ax), euler[2], DELTA);
        Assert.assertEquals(Angles.degreeToRadian(ay), euler[1], DELTA);
        Assert.assertEquals(Angles.degreeToRadian(az), euler[0], DELTA);

    }

    private static void setToAngles(SceneNode node, double x, double y, double z){
        final Matrix3x3 m = Matrix3x3.createRotationEuler(new Vector3f64(z, y, x));
        node.getNodeTransform().getRotation().set(m);
        node.getNodeTransform().notifyChanged();
    }

}
