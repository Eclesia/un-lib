

package science.unlicense.physics.api.constraint;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class TranslationConstraintTest {

    private static final double DELTA = 0.0000000001;

    /**
     * Test a node contraint relative to parent position.
     */
    @Test
    public void testCentered(){

        final SceneNode node = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final BBox limits = new BBox(
                new double[]{-10,-20,-30},
                new double[]{+10,+20,+30});

        final TranslationConstraint tc = new TranslationConstraint(node, limits, true);

        //within tests
        node.getNodeTransform().getTranslation().setXYZ(-9, -18, -27);
        tc.apply();
        Assert.assertEquals(new Vector3f64(-9, -18, -27), node.getNodeTransform().getTranslation());

        node.getNodeTransform().getTranslation().setXYZ(+9, +18, +27);
        tc.apply();
        Assert.assertEquals(new Vector3f64(+9, +18, +27), node.getNodeTransform().getTranslation());

        //outside tests
        node.getNodeTransform().getTranslation().setXYZ(-15, -21, -89);
        tc.apply();
        Assert.assertEquals(new Vector3f64(-10, -20, -30), node.getNodeTransform().getTranslation());

        node.getNodeTransform().getTranslation().setXYZ(+15, +21, +89);
        tc.apply();
        Assert.assertEquals(new Vector3f64(+10, +20, +30), node.getNodeTransform().getTranslation());
    }

}
