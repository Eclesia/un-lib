
package science.unlicense.physics.api.collision;

import junit.framework.TestCase;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.Operations;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.operation.Collision;

/**
 *
 * @author Johann Sorel
 */
public class CollisionTest extends TestCase {

    private static final double DELTA = 0.000000001;

    ////////////////////////////////////////////////////////////////////////////
    // 2D //////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Test
    public void testRectanglePoint() throws OperationException {
        RigidBody geom1 = new RigidBody(new Rectangle(100, 200, 10, 10),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new DefaultPoint(105, 208), 1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector2f64(0, 1), collision.getNormal());
        Assert.assertEquals(0d, collision.getDistance(),DELTA);
    }

    @Test
    public void testRectangleRectangle() throws OperationException{
        RigidBody geom1 = new RigidBody(new Rectangle(100, 200, 10, 10),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Rectangle(100, 208, 10, 10), 1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector2f64(-0, 1), collision.getNormal());
        Assert.assertEquals(-2d, collision.getDistance(),DELTA);
    }

    @Test
    public void testRectangleCircle() throws OperationException{
        RigidBody geom1 = new RigidBody(new Rectangle(100, 200, 10, 10), 1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Circle(new Vector2f64(105, 202),3),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
    }

    @Test
    public void testPolygonPoint() throws OperationException {
        RigidBody geom1 = new RigidBody(new Rectangle(100, 200, 10, 10),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new DefaultPoint(105, 208), 1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector2f64(0, 1), collision.getNormal());
        Assert.assertEquals(0d, collision.getDistance(),DELTA);
    }

    @Test
    public void testCirclePoint() throws OperationException{
        RigidBody geom1 = new RigidBody(new Circle(new Vector2f64(100, 200),10),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new DefaultPoint(100, 208), 1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector2f64(-0, 1), collision.getNormal());
        Assert.assertEquals(-2d, collision.getDistance(),DELTA);
    }

    @Test
    public void testCircleCircle() throws OperationException{
        RigidBody geom1 = new RigidBody(new Circle(new Vector2f64(100, 200),2),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Circle(new Vector2f64(100, 203),3),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector2f64(-0, 1), collision.getNormal());
        Assert.assertEquals(-2d, collision.getDistance(),DELTA);
    }

    ////////////////////////////////////////////////////////////////////////////
    // 3D //////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Test
    public void testSpherePlan() throws OperationException{

        RigidBody geom1 = new RigidBody(new Plane(new Vector3f64(0,1,0),new Vector3f64(0, 6, 0)),1);
        RigidBody geom2 = new RigidBody(new Sphere(new Vector3f64(0,5,0),2),1);
        geom2.getMotion().getVelocity().setXYZ(0, -0.2, 0);
        //vertical penetration of 1 unit in Y

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector3f64(0,1,0), collision.getNormal());
        Assert.assertEquals(-3d, collision.getDistance(),DELTA);
        //with a velocity of -0.2 on Y, the sphere was 3y above at the first contact : 15seconds
        Assert.assertEquals(-3*(1/0.2), collision.getRollbackTime(), DELTA);

    }

    @Test
    public void testSpherePlan2() throws OperationException{

        RigidBody geom1 = new RigidBody(new Sphere(2),1);
        geom1.getNodeTransform().getTranslation().setXYZ(0, 5, 0);
        geom1.getNodeTransform().notifyChanged();
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Plane(new Vector3f64(0,-1,0),new Vector3f64(0, 0, 0)),1);
        geom2.getNodeTransform().getTranslation().setXYZ(0, 6, 0);
        geom2.getNodeTransform().notifyChanged();

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector3f64(0,1,0), collision.getNormal());
        Assert.assertEquals(-1d, collision.getDistance(),DELTA);
    }

    @Test
    public void testSphereSphere() throws OperationException{

        RigidBody geom1 = new RigidBody(new Sphere(new Vector3f64(0,20,0),10),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Sphere(new Vector3f64(0,9,0),3),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector3f64(0,-1,0), collision.getNormal());
        Assert.assertEquals(-2d, collision.getDistance(),DELTA);
    }

    @Test
    public void testSphereSphere2() throws OperationException{

        RigidBody geom1 = new RigidBody(new Sphere(1),1);
        geom1.getNodeTransform().getTranslation().setXYZ(0,12.1,0);
        geom1.getNodeTransform().notifyChanged();

        //vertical penetration of 0.1 unit
        RigidBody geom2 = new RigidBody(new Sphere(3),1);
        geom2.getNodeTransform().getTranslation().setXYZ(0,16,0);
        geom2.getNodeTransform().notifyChanged();

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector3f64(0,1,0), collision.getNormal());
        Assert.assertEquals(-0.1d, collision.getDistance(),DELTA);
    }

    @Test
    public void testSphereCapsule() throws OperationException{
        RigidBody geom1 = new RigidBody(new Sphere(new Vector3f64(0,9,0),3),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Capsule(10,3),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector3f64(0,-1,0), collision.getNormal());
        Assert.assertEquals(-2d, collision.getDistance(),DELTA);
    }

    @Test
    public void testSphereBBox() throws OperationException{

        RigidBody geom1 = new RigidBody(new Sphere(new Vector3f64(0,5,0),2),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new BBox(new Vector3f64(-2,1,-2),new Vector3f64(2, 4, 2)),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector3f64(0,-1,0), collision.getNormal());
        Assert.assertEquals(-1d, collision.getDistance(),DELTA);
    }


    @Ignore
    @Test
    public void testSphereObbox() throws OperationException{
    }

    @Ignore
    @Test
    public void testCapsuleObbox() throws OperationException{
    }

    @Test
    public void testCapsuleCapsule() throws OperationException{

        RigidBody geom1 = new RigidBody(new Capsule(new Vector3f64(0, 4, 0), new Vector3f64(0, 5, 0),2),1);
        //vertical penetration of 1 uni
        RigidBody geom2 = new RigidBody(new Capsule(new Vector3f64(0, 8, 0), new Vector3f64(0, 9, 0),2),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector3f64(0,1,0), collision.getNormal());
        Assert.assertEquals(-1d, collision.getDistance(),DELTA);
    }

    @Ignore
    @Test
    public void testObboxObbox() throws OperationException{
    }

    @Test
    public void testPlaneSphere() throws OperationException{
        RigidBody geom1 = new RigidBody(new Sphere(new Vector3f64(0,9,0),3),1);
        //vertical penetration of 1 unit
        RigidBody geom2 = new RigidBody(new Plane(new Vector3f64(0, 1, 0),new Vector3f64(0, 7, 0)),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector3f64(0,-1,0), collision.getNormal());
        Assert.assertEquals(-1d, collision.getDistance(),DELTA);
    }

    @Test
    public void testPlaneCapsule() throws OperationException{

        RigidBody geom1 = new RigidBody(new Capsule(new Vector3f64(0, 4, 0), new Vector3f64(0,5,0),2),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Plane(new Vector3f64(0,-1,0),new Vector3f64(0, 6, 0)),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector3f64(0,1,0), collision.getNormal());
        Assert.assertEquals(-1d, collision.getDistance(),DELTA);
    }

    @Ignore
    @Test
    public void testPlaneObbox() throws OperationException{
    }
}
