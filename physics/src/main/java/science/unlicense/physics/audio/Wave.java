
package science.unlicense.physics.audio;

import science.unlicense.common.api.number.NumberType;
import science.unlicense.geometry.api.tuple.AbstractTupleSpace;
import science.unlicense.geometry.api.tuple.TupleSpace1Dx1S;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;

/**
 * Represent a pure wave signal.
 *
 * formula : A*sin(2*PI*f*t + p)
 *
 * @author Johann Sorel
 */
public class Wave extends AbstractTupleSpace implements TupleSpace1Dx1S {

    /**
     * Determinate the wave frequency.
     * Default is 1.0
     */
    public double frequency = 1.0;
    /**
     * Determinate the height of the wave.
     * Default is 1.0
     */
    public double amplitude = 1.0;
    /**
     * Determinate the wave offset, called phase.
     * Default is 0.0
     */
    public double phase = 0.0;

    public Wave(SampleSystem coordinateSystem, SampleSystem sampleSystem, NumberType numericType) {
        super(coordinateSystem, sampleSystem, numericType);
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

    public double getPeriod() {
        return 1.0 / frequency;
    }

    public void setPeriod(double period) {
        frequency = 1.0 / period;
    }

    public double getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(double amplitude) {
        this.amplitude = amplitude;
    }

    public double getPhase() {
        return phase;
    }

    public void setPhase(double phase) {
        this.phase = phase;
    }

    @Override
    public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
        buffer.set(0, getSample(coordinate.get(0)));
        return buffer;
    }

    @Override
    public double getSample(double coordinate) {
        return amplitude * Math.sin( (Maths.TWO_PI * frequency * coordinate) + phase );
    }

}
