
package science.unlicense.physics.api.law;

import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public final class Euler {

    private Euler(){}

    /**
     * Calculate acceleration.
     * acceleration = force / mass.
     * @param force
     * @param mass
     * @return
     */
    public static VectorRW acceleration(VectorRW force, double mass){
        return force.scale(1/mass);
    }

    /**
     * Calculate velocity.
     * acceleration * time_step
     *
     * @param acceleration
     * @param timeellapsed
     * @return
     */
    public static VectorRW velocity(VectorRW acceleration, double timeellapsed){
        return acceleration.scale(timeellapsed);
    }

}
