
package science.unlicense.physics.api.constraint;

import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.api.VectorRW;

/**
 * Contains the joint rotation limits in 3 dimensions.
 * dimensions X,Y,Z , min and max rotation angle.
 *
 * Simplification of Collada(v1.5 page 462) axis_info limit/min/max values.
 *
 * @author Johann Sorel
 */
public class AngleLimitConstraint implements Constraint{

    private final SceneNode target;
    private final BBox limits;

    public AngleLimitConstraint(SceneNode target, BBox limits) {
        this.target = target;
        this.limits = limits;
    }

    public SceneNode getTarget() {
        return target;
    }

    public BBox getLimits() {
        return limits;
    }

    public void apply() {

        final Matrix3x3 rotation = new Matrix3x3(target.getNodeTransform().getRotation());

        final VectorRW euler = rotation.toEuler();
        euler.set(2, Maths.clamp(euler.get(2),limits.getMin(0),limits.getMax(0)) );
        euler.set(1, Maths.clamp(euler.get(1),limits.getMin(1),limits.getMax(1)) );
        euler.set(0, Maths.clamp(euler.get(0),limits.getMin(2),limits.getMax(2)) );
        rotation.fromEuler(euler);
        target.getNodeTransform().getRotation().set(rotation);
        target.getNodeTransform().notifyChanged();
    }

}
