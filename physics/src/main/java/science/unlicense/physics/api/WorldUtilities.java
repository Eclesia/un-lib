

package science.unlicense.physics.api;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.physics.api.body.Body;
import science.unlicense.physics.api.force.Force;
import science.unlicense.physics.api.force.Singularity;

/**
 * Utility methods for physic world.
 *
 * @author Johann Sorel
 */
public final class WorldUtilities {

    private WorldUtilities(){}

    /**
     * Apply singularities,velocity and forces on world bodies.
     *
     * @param world
     */
    public static void applyForces(final World world){
        final Sequence bodies = world.getBodies();
        final Sequence singularities = world.getSingularities();
        final Sequence forces = world.getForces();
        final double drag = world.getDrag();

        //apply singularities
        for (int i=0,n=bodies.getSize(); i<n; i++){
            final Body body = (Body) bodies.get(i);
            for (int k=0,p=singularities.getSize();k<p;k++){
                final Singularity singularity = (Singularity) singularities.get(k);
                singularity.apply(body);
            }
        }

        //apply forces
        for (int i=0,n=forces.getSize(); i<n; i++){
            final Force f = (Force) forces.get(i);
            f.apply();
        }
    }

    /**
     * Loop on all world bodies and set forces/torques to zero.
     *
     * @param world
     */
    public static void clearBodyForces(World world) {
        final Iterator ite = world.getBodies().createIterator();
        while (ite.hasNext()) {
            final Body body = (Body) ite.next();
            body.getMotion().getForce().setAll(0.0);
            body.getMotion().getTorque().setAll(0.0);
        }
    }
}
