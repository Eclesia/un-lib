package science.unlicense.physics.api.integration;

import science.unlicense.physics.api.World;


/**
 * The integrator is in charge of updating the world state with the current
 * bodies and forces in action.
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public interface Integrator {

    /**
     * Get the collision solver used to update the physique world.
     * @return WorldSolver
     */
    CollisionSolver getCollisionSolver();

    /**
     * Set the collision solver used to update the physique world.
     * @param solver
     */
    void setCollisionSolver(CollisionSolver solver);

    /**
     * Update the given world.
     * @param world
     * @param timeellapsed : in seconds if using conventional units
     */
    void update(World world, double timeellapsed);

}
