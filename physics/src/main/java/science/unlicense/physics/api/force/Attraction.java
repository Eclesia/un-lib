
package science.unlicense.physics.api.force;

import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.physics.api.body.Particle;
import science.unlicense.physics.api.body.RigidBody;

/**
 * Attract positive.
 * Repel negative.
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class Attraction extends AbstractForce {

    private RigidBody a;
    private RigidBody b;
    private double k;
    private double distanceMin;
    private double distanceMinSquared;

    //caches for calculation
    private final VectorRW a2b;

    public Attraction(RigidBody a, RigidBody b, double k, double distanceMin) {
        this.a = a;
        this.b = b;
        this.k = k;
        this.distanceMin = distanceMin;
        this.distanceMinSquared = distanceMin * distanceMin;
        this.a2b = VectorNf64.createDouble(a.getDimension());
    }

    public double getMinimumDistance() {
        return distanceMin;
    }

    public void setMinimumDistance(double d) {
        distanceMin = d;
        distanceMinSquared = d * d;
    }

    public double getStrength() {
        return k;
    }

    public void setStrength(double k) {
        this.k = k;
    }

    public RigidBody getFirstEnd() {
        return a;
    }

    public RigidBody getSecondEnd() {
        return b;
    }

    public void apply() {
        if (!enable) return;

        if ( (a instanceof Particle && !((Particle) a).isFree()) &&
            (b instanceof Particle && !((Particle) b).isFree())){
            return;
        }


        final VectorRW posA = a.getWorldPosition(null);
        final VectorRW posB = b.getWorldPosition(null);
        posA.subtract(posB,a2b);

        double a2bDistanceSquared = a2b.lengthSquare();

        if (a2bDistanceSquared < distanceMinSquared){
            a2bDistanceSquared = distanceMinSquared;
        }

        final double force = k * a.getMass() * b.getMass() / a2bDistanceSquared;
        final double length = Math.sqrt(a2bDistanceSquared);

        // make unit vector
        a2b.localScale(1.0/length);

        // multiply by force
        a2b.localScale(force);

        // apply
        if (a instanceof Particle){
            if (((Particle) a).isFree()) a.getMotion().getForce().localSubtract(a2b);
        } else {
            a.getMotion().getForce().localSubtract(a2b);
        }
        if (b instanceof Particle){
            if (((Particle) b).isFree()) b.getMotion().getForce().localAdd(a2b);
        } else {
            b.getMotion().getForce().localAdd(a2b);
        }
    }

}
