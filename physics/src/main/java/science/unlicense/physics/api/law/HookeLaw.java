
package science.unlicense.physics.api.law;

import science.unlicense.math.api.Maths;
import science.unlicense.math.api.VectorRW;

/**
 * Law used for linear type spring objects.
 * Reference : http://en.wikipedia.org/wiki/Hooke's_law
 *
 * F = kx
 * where :
 * k is the material stiffness
 * x is a distance
 *
 * @author Johann Sorel
 */
public final class HookeLaw {

    private HookeLaw(){}

    /**
     * Calculate force.
     * F = kx
     *
     * @param stiffness
     * @param distance
     * @return Vector force
     */
    public static VectorRW force(double stiffness, VectorRW distance){
        return distance.scale(stiffness);
    }

    /**
     * Calculate stiffness.
     * k = F/x
     *
     * @param force
     * @param distance
     * @return double stiffness
     */
    public static double stiffness(VectorRW force, VectorRW distance){
        final VectorRW t = force.divide(distance);
        return Maths.mean(t.toDouble());
    }

    /**
     * Calculate distance.
     * x = F/k
     *
     * @param force
     * @param stiffness
     * @return Vector distance
     */
    public static VectorRW distance(VectorRW force, double stiffness){
        return force.scale(1/stiffness);
    }

}
