
package science.unlicense.physics.api.constraint;

import science.unlicense.math.api.MatrixRW;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;

/**
 * Joint constraints.
 * Allow joint rotation only around given axis.
 *
 * @author Johann Sorel
 */
public class FixedAxisConstraint2 implements Constraint{

    private final SceneNode target;
    private final VectorRW axis;
    private final VectorRW up;
    private final VectorRW right;

    public FixedAxisConstraint2(SceneNode target, VectorRW axis, VectorRW up, VectorRW right) {
        this.target = target;
        this.axis = axis;
        this.up = up;
        this.right = right;
    }

    public void apply() {

        final MatrixRW nodeRotation = target.getNodeTransform().getRotation();

        final VectorRW forward = right.cross(up);

        //calculate rotation from axis to forward
        // we will make the calculation in this space
        final MatrixRW axisToForward = Matrix3x3.createRotation(axis, forward);

        //calculate reference direction
        final VectorRW ref = (VectorRW) nodeRotation.transform(forward);
        axisToForward.transform(ref, ref);

        //calculate angle on the 2 other planes
        double horizontalAngle = forward.shortestAngle(new Vector3f64(ref.getX(),0,ref.getZ()).localNormalize());
        if (ref.getX()<0) horizontalAngle = -horizontalAngle;

        double verticalAngle = ref.shortestAngle(new Vector3f64(ref.getX(),0,ref.getZ()).localNormalize());
        if (ref.getY()<0) verticalAngle = -verticalAngle;

        //rebuild matrix
        final MatrixRW temp = new Matrix3x3().setToIdentity();
        if (!Double.isNaN(horizontalAngle)){
            final Matrix3x3 mh = Matrix3x3.createRotation3(horizontalAngle, up);
            temp.localMultiply(mh);
        }
        if (!Double.isNaN(verticalAngle)){
            final Matrix3x3 mv = Matrix3x3.createRotation3(verticalAngle, right);
            temp.localMultiply(mv);
        }

        //invert axis transform
        temp.localMultiply(axisToForward.localInvert());

        //set transform
        nodeRotation.set(temp);
        target.getNodeTransform().notifyChanged();

    }
}
