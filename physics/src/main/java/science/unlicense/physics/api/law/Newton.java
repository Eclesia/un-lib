
package science.unlicense.physics.api.law;

import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public final class Newton {

    private Newton(){}

    /**
     * Calculate velocity.
     * acceleration = distance / time.
     * @param force
     * @param mass
     * @return
     */
    public static VectorRW velocity(VectorRW distance, double timeellapsed){
        return distance.scale(1/timeellapsed);
    }

}
