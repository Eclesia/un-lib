
package science.unlicense.physics.api.skeleton;

import science.unlicense.common.api.exception.UnimplementedException;

/**
 * Jacobian method to solve IK.
 * TODO
 *
 * @author Johann Sorel
 */
public class IKSolverJacobian implements IKSolver{

    public void solve(InverseKinematic ik) {
        throw new UnimplementedException("Not supported yet.");
    }

}
