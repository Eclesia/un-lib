
package science.unlicense.physics.api.constraint;

import science.unlicense.math.api.Affine;
import science.unlicense.display.api.scene.SceneNode;

/**
 * Copy the world to node transform.
 *
 * @author Johann Sorel
 */
public class CopyWorldTransformConstraint implements Constraint{

    private final SceneNode target;
    private final SceneNode toCopy;
    private final float factor;

    public CopyWorldTransformConstraint(SceneNode target, SceneNode toCopy, float factor) {
        this.target = target;
        this.toCopy = toCopy;
        this.factor = factor;
    }

    public void apply() {
        final Affine m = toCopy.getRootToNodeSpace();
        target.getNodeTransform().set(m);
    }

}
