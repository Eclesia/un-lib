
package science.unlicense.physics.api.operation;

import science.unlicense.geometry.api.operation.TransformOp;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.api.operation.OperationExecutor;
import science.unlicense.image.impl.s3d.Grid;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.impl.AffineN;

/**
 * Transform executors.
 *
 * @author Johann Sorel
 */
public class TransformExecutors {

    private TransformExecutors(){}

    private abstract static class TransformExecutor implements OperationExecutor{

        private final Class clazz;

        public TransformExecutor(Class geometryClass) {
            this.clazz = geometryClass;
        }

        @Override
        public Class getOperationClass() {
            return TransformOp.class;
        }

        @Override
        public Geometry execute(Operation operation) {
            final TransformOp transform = (TransformOp) operation;
            final science.unlicense.math.api.transform.Transform matrix = transform.getTransform();
            final Geometry geom = transform.getGeometry();
            return execute(geom, matrix);
        }

        protected abstract Geometry execute(Geometry geom, science.unlicense.math.api.transform.Transform transform);

        @Override
        public boolean canHandle(Operation operation) {
            return operation instanceof TransformOp &&
                    clazz.isInstance( ((TransformOp) operation).getGeometry());
        }

    }

    public static final OperationExecutor GRID =
            new TransformExecutor(Grid.class){

        @Override
        protected Grid execute(Geometry geom, science.unlicense.math.api.transform.Transform transform) {
            final Grid grid = (Grid) geom;
            final AffineRW trs = AffineN.create(grid.getGeomToGrid());
            trs.localMultiply((Affine) transform);
            return new Grid(grid.getTuples(), trs);
        }
    };

}
