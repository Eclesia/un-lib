
package science.unlicense.physics.api.constraint;

import science.unlicense.math.api.MatrixRW;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.math.impl.Matrices;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;

/**
 * Joint constraints.
 * Allow joint rotation only around given axis.
 *
 * @author Johann Sorel
 */
public class FixedAxisConstraint implements Constraint{

    private final SceneNode target;
    private final VectorRW axis;

    public FixedAxisConstraint(SceneNode target, VectorRW axis) {
        this.target = target;
        this.axis = axis;
    }

    public void apply() {

        final MatrixRW rotation = target.getNodeTransform().getRotation();

        //transform the contraints axis on X
        final Matrix3x3 temp = Matrix3x3.createRotation(axis,new Vector3f64(1, 0, 0));
        temp.multiply(rotation, rotation);

        //convert the rotation in Euler angles
        //this way can simply revome the 2 axis rotations we don't want
        final double[] euler = Matrices.toEuler(rotation.getValuesCopy(), null);
        //rmore the rotation which are not allowed
        euler[0] = 0;
        euler[1] = 0;

        //change our euler angle back in the rotation
        rotation.set(Matrices.fromEuler(euler, rotation.getValuesCopy()));

        //transform the contraints back on the fixed axis
        temp.localInvert();
        temp.multiply(rotation, rotation);
        target.getNodeTransform().notifyChanged();

    }

}
