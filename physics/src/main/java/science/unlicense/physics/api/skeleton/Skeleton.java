
package science.unlicense.physics.api.skeleton;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeSequence;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.common.api.predicate.ClassPredicate;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.SceneUtils;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.SimilarityRW;

/**
 * Skeleton composed of joints.
 *
 * @author Johann Sorel
 */
public class Skeleton extends DefaultSceneNode{

    private static final NodeVisitor BINDPOSE_SOLVER = new NodeVisitor(){
            public Object visit(Node node, Object context) {
                if (node instanceof Joint){
                    final Joint jt = (Joint) node;
                    jt.updateBindBose();
                    return super.visit(node, context);
                }
                //something else attached to the joint, skip it
                return null;
            }
        };

    private static final NodeVisitor INVBINDPOSE_SOLVER = new NodeVisitor(){
            public Object visit(Node node, Object context) {
                if (node instanceof Joint){
                    final Joint jt = (Joint) node;
                    jt.getInvertBindPose().set(jt.getBindPose());
                    jt.getInvertBindPose().localInvert();
                    return super.visit(node, context);
                }
                //something else attached to the joint, skip it
                return null;
            }
        };

    private static final NodeVisitor RESTORE_BASE = new NodeVisitor(){
            public Object visit(Node node, Object context) {
                if (node instanceof Joint){
                    final Joint jt = (Joint) node;
                    final SimilarityRW bindPose = jt.getBindPose();
                    bindPose.set(jt.getInvertBindPose());
                    bindPose.localInvert();
                    //at this stage, the bind pose contains the root to joint matrix

                    final SceneNode parent = jt.getParent();
                    if (parent instanceof Joint){
                        final Joint jpa = (Joint) parent;
                        //we remove the root to parent transform
                        final AffineRW parentToRoot = jpa.getBindPose().invert();
                        final AffineRW parentToJoint = parentToRoot.multiply(bindPose);
                        //convert the matrix back to a NodeTransform
                        jt.getNodeTransform().set(parentToJoint);
                    } else {
                        //no parent, the bind pose is the same as parent to joint matrix
                        jt.getNodeTransform().set(bindPose);
                    }

                    return super.visit(node, context);
                }
                //something else attached to the joint, skip it
                return null;
            }
        };

    private final Sequence iks = new ArraySequence();

    //view of all joints in the skeleton
    private final Sequence joints;
    private final Dictionary jointById = new HashDictionary();
    private final Predicate ID_PREDICATE = new PropertyPredicate(SceneNode.PROPERTY_ID);
    private final EventListener idChangeTracker = new EventListener() {
        public void receiveEvent(Event event) {
            jointById.removeAll();
        }
    };

    public Skeleton() {
        this(3);
    }

    public Skeleton(int dimension) {
        this(CoordinateSystems.undefined(dimension));
    }

    public Skeleton(CoordinateSystem cs) {
        super(cs);
        joints = new NodeSequence(this, false, new ClassPredicate(Joint.class)) {
            @Override
            protected void nodeAdded(Node n) {
                n.addEventListener(ID_PREDICATE, idChangeTracker);
                super.nodeAdded(n);
            }

            @Override
            protected void nodeRemoved(Node n) {
                n.removeEventListener(ID_PREDICATE, idChangeTracker);
                super.nodeRemoved(n);
            }
        };
    }

    /**
     * The returned list is not ordered in any particular order.
     * The order is therefor NOT the mesh joint index, use getJoint(id) instead.
     *
     * @return skeleton joints.
     */
    public Sequence getAllJoints() {
        return joints;
    }


    /**
     * Get the joint for given joint identifier.
     *
     * @param identifier searched joint identifier
     * @return Joint may be null
     */
    public Joint getJointById(final Object identifier){
        if (jointById.getSize()==0) {
            Iterator ite = getAllJoints().createIterator();
            while (ite.hasNext()) {
                Joint joint = (Joint) ite.next();
                jointById.add(joint.getId(), joint);
            }
        }
        return (Joint) jointById.getValue(identifier);
    }

    /**
     * Get the joint for given joint title.
     *
     * @param title searched joint title
     * @return Joint may be null
     */
    public Joint getJointByTitle(final CharArray title){
        return (Joint) Nodes.SEARCHER.visit(this, new Predicate() {
            public Boolean evaluate(Object candidate) {
                return candidate instanceof Joint && title.equals(((Joint) candidate).getTitle());
            }
        });
    }


    /**
     * @return Sequence of inverse kinematics.
     */
    public Sequence getIks() {
        return iks;
    }

    /**
     * Calculate root to joint matrix and invert matrix for each joint.
     */
    public void updateBindPose(){
        for (int i=0,n=children.getSize();i<n;i++){
            BINDPOSE_SOLVER.visit((Joint) children.get(i), null);
        }
    }

    /**
     * Calculate root to joint matrix and invert matrix for each joint.
     */
    public void updateInvBindPose(){
        for (int i=0,n=children.getSize();i<n;i++){
            INVBINDPOSE_SOLVER.visit((Joint) children.get(i), null);
        }
    }

    /**
     * When all joints transforms are world to joint transforms.
     * This method will rebuild the parent to joint transforms.
     */
    public void reverseWorldPose(){
        for (int i=0,n=children.getSize();i<n;i++){
            SceneUtils.reverseWorldToNodeTrsRecursive((Joint) children.get(i));
        }
    }

    /**
     * When all joints transforms are parent to joint transforms.
     * This method will rebuild the world to joint transforms.
     */
    public void resetToWorldPose(){
        for (int i=0,n=children.getSize();i<n;i++){
            SceneUtils.reverseParentToNodeTrsRecursive((Joint) children.get(i));
        }
    }

    /**
     * Use the inverse bind pose matrix to restore the original joint poses.
     * Bind pose matrix and NodeTransform are restored.
     */
    public void resetToBase(){
        for (int i=0,n=children.getSize();i<n;i++){
            RESTORE_BASE.visit((Joint) children.get(i), null);
        }
    }

    /**
     * Loop on all inverse kinematics and call solve method.
     */
    public void solveKinematics(){
        final Iterator ikite = iks.createIterator();
        while (ikite.hasNext()){
            final InverseKinematic ik = (InverseKinematic) ikite.next();
            ik.solve();
        }
    }

    /**
     * Deep cop, copies joint nodes.
     *
     * @return
     */
    public Skeleton copy(){
        final Skeleton copy = new Skeleton();
        for (int i=0,n=children.getSize();i<n;i++){
            copy.getChildren().add( ((Joint) children.get(i)).deepCopy());
        }
        return copy;
    }

    public Chars toChars() {
        return new Chars("Skeleton");
    }

}
