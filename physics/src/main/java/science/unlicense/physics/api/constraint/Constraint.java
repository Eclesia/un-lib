
package science.unlicense.physics.api.constraint;

/**
 *
 * @author Johann Sorel
 */
public interface Constraint {

    void apply();

}
