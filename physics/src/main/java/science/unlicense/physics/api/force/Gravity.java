
package science.unlicense.physics.api.force;

import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.physics.api.body.Body;

/**
 *
 * @author Johann Sorel
 */
public class Gravity implements Singularity{

    private final VectorRW gravity;

    public Gravity(VectorRW gravity) {
        this.gravity = gravity;
    }

    public Gravity(double g) {
        gravity = new Vector3f64(0, g, 0);
    }

    public void apply(Body body) {
        body.getMotion().getForce().localAdd(gravity);
    }

}
