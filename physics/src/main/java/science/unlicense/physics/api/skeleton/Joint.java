
package science.unlicense.physics.api.skeleton;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.character.LChars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.impl.SimilarityNd;

/**
 *
 * @author Johann Sorel
 */
public class Joint extends DefaultSceneNode {

    private final SimilarityRW bindpose;
    private final SimilarityRW invbindpose;

    private final Sequence constraints = new ArraySequence();

    public Joint(int dimension) {
        this(dimension,null);
    }

    public Joint(int dimension,CharArray name) {
        super(CoordinateSystems.undefined(dimension), true);
        bindpose = SimilarityNd.create(dimension);
        invbindpose = SimilarityNd.create(dimension);
        setTitle(name);
    }

    /**
     * Constraints applied to this joint.
     */
    public Sequence getConstraints() {
        return constraints;
    }

    public SimilarityRW getBindPose() {
        return bindpose;
    }

    public SimilarityRW getInvertBindPose() {
        return invbindpose;
    }

    /**
     * Update bind pose, relative to the parent bind pose.
     */
    public void updateBindBose(){
        final SceneNode parent = getParent();
        if (parent instanceof Joint){
            final Joint jpa = (Joint) parent;
            bindpose.set(jpa.getBindPose().multiply(getNodeTransform()));
        } else {
            bindpose.set(getNodeTransform());
        }
    }

    /**
     * @return copy of this joint without children
     */
    public Joint copy(){
        final Joint t = new Joint(bindpose.getInputDimensions());
        t.setTitle(getTitle());
        t.getNodeTransform().set(getNodeTransform());
        t.bindpose.set(bindpose);
        t.invbindpose.set(invbindpose);
        return t;
    }

    public Joint deepCopy(){
        final Joint copy = copy();
        for (int i=0,n=children.getSize();i<n;i++){
            copy.children.add(((Joint) children.get(i)).deepCopy() );
        }
        return copy;
    }

    /**
     * @return true if node is an element in a kinematic chain.
     */
    public boolean isKinematic(Skeleton skeleton){
        final Iterator ite = skeleton.getIks().createIterator();
        while (ite.hasNext()){
            final InverseKinematic ik = (InverseKinematic) ite.next();
            if (Arrays.contains(ik.getChain(), this)){
                return true;
            }
        }
        return false;
    }

    /**
     * @return true if node is a kinematic effector.
     */
    public boolean isKinematicEffector(Skeleton skeleton){
        final Iterator ite = skeleton.getIks().createIterator();
        while (ite.hasNext()){
            final InverseKinematic ik = (InverseKinematic) ite.next();
            if (ik.getEffector() == this){
                return true;
            }
        }
        return false;
    }

    /**
     * @return true if node is a kinematic target.
     */
    public boolean isKinematicTarget(Skeleton skeleton){
        final Iterator ite = skeleton.getIks().createIterator();
        while (ite.hasNext()){
            final InverseKinematic ik = (InverseKinematic) ite.next();
            if (ik.getTarget() == this){
                return true;
            }
        }
        return false;
    }

    @Override
    public Chars toChars() {
        if (getTitle() instanceof LChars){
            return ((LChars) getTitle()).toCharsAll();
        }
        return CObjects.toChars(getTitle());
    }

}
