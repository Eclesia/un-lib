
package science.unlicense.physics.api.constraint;

import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class CopyScaleConstraint implements Constraint{

    private final SceneNode target;
    private final SceneNode toCopy;
    private final float factor;

    public CopyScaleConstraint(SceneNode target, SceneNode toCopy, float factor) {
        this.target = target;
        this.toCopy = toCopy;
        this.factor = factor;
    }

    public void apply() {
        applyConstraint(target, toCopy, factor);
    }

    static void applyConstraint(SceneNode target, SceneNode toCopy, float factor){
        final VectorRW scl = toCopy.getNodeTransform().getScale();
        final VectorRW baseScl = target.getNodeTransform().getScale();
        scl.scale(factor, baseScl);
        target.getNodeTransform().notifyChanged();
    }

}
