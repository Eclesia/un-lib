
package science.unlicense.physics.api.skeleton;

/**
 * Inverse kinematic object.
 * Defines the chain of joints making the 'arm'.
 *
 * @author Johann Sorel
 */
public class InverseKinematic {

    private final Joint target;
    private final Joint effector;
    private final Joint[] chainJoints;
    private final IKSolver solver;

    /**
     *
     * @param target target joint
     * @param effector effector joint
     * @param joints joints in the chain, must be in the effector -> base order, does not contain the effector
     * @param solver inverse kinematic solver
     */
    public InverseKinematic(Joint target, Joint effector, Joint[] joints, IKSolver solver) {
        this.target = target;
        this.effector = effector;
        this.chainJoints = joints;
        this.solver = solver;
    }

    /**
     * Get the target joint.
     * The position that the effector is trying to reach.
     * @return Joint.
     */
    public Joint getTarget(){
        return target;
    }

    /**
     * Get the effector joint.
     * The last joint in the kinematic chain. trying to reach the target joint.
     * @return Joint.
     */
    public Joint getEffector(){
        return effector;
    }

    /**
     * Get the joints in the kinematic chain.
     * @return Joint[]
     */
    public Joint[] getChain(){
        return chainJoints;
    }

    /**
     * Solve the inverse kinematic.
     * Call it to solve the kinematic when the target position has change.
     */
    public void solve() {
        solver.solve(this);
    }

}
