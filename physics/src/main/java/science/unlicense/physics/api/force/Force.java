
package science.unlicense.physics.api.force;

/**
 * Force/constraint interface.
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public interface Force{

    /**
     * The difference between forces (spring,...) and constraints (distance,rotation,...)
     * is order. Constraint forces are applied after normal forces.
     * - forces could be applies in a random order, they are more or less independant
     * - Constraints are absolute, they are used to compensate the forces applied.
     *
     * @return true if is constraint.
     */
    boolean isConstraint();

    /**
     * Is this force active.
     * @return true if active, false otherwise
     */
    boolean isEnable();

    /**
     * Set active state.
     * @param enable
     */
    void setEnable(boolean enable);

    /**
     * Apply force.
     */
    void apply();

}
