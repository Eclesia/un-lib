package science.unlicense.physics.api.integration;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.WorldUtilities;
import science.unlicense.physics.api.body.Particle;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class RungeKuttaIntegrator extends AbstractIntegrator{

    private final Sequence originalPositions   = new ArraySequence();
    private final Sequence originalVelocities  = new ArraySequence();
    private final Sequence k1Forces            = new ArraySequence();
    private final Sequence k1Velocities        = new ArraySequence();
    private final Sequence k2Forces            = new ArraySequence();
    private final Sequence k2Velocities        = new ArraySequence();
    private final Sequence k3Forces            = new ArraySequence();
    private final Sequence k3Velocities        = new ArraySequence();
    private final Sequence k4Forces            = new ArraySequence();
    private final Sequence k4Velocities        = new ArraySequence();

    private int worldDimension;

    public RungeKuttaIntegrator(int worldDimension) {
        this.worldDimension = worldDimension;
    }

    private void allocateParticles(final World world) {
        final Sequence bodies = world.getBodies();
        while (bodies.getSize()> originalPositions.getSize()){
            originalPositions.add(VectorNf64.createDouble(worldDimension));
            originalVelocities.add(VectorNf64.createDouble(worldDimension));
            k1Forces.add(VectorNf64.createDouble(worldDimension));
            k1Velocities.add(VectorNf64.createDouble(worldDimension));
            k2Forces.add(VectorNf64.createDouble(worldDimension));
            k2Velocities.add(VectorNf64.createDouble(worldDimension));
            k3Forces.add(VectorNf64.createDouble(worldDimension));
            k3Velocities.add(VectorNf64.createDouble(worldDimension));
            k4Forces.add(VectorNf64.createDouble(worldDimension));
            k4Velocities.add(VectorNf64.createDouble(worldDimension));
        }
    }

    public void update(final World world, final double timeellapsed){
        allocateParticles(world);
        final Sequence bodies = world.getBodies();

        /////////////////////////////////////////////////////////
        // save original position and velocities

        for (int i=0; i<bodies.getSize(); ++i){
            final RigidBody rb = (RigidBody) bodies.get(i);
            if (rb.isFree()){
                ((VectorRW) originalPositions.get(i)).set(rb.getWorldPosition(null));
                ((VectorRW) originalVelocities.get(i)).set(rb.getMotion().getVelocity());
            }
            rb.getMotion().getForce().setAll(0.0);// and clear the forces
        }

        ////////////////////////////////////////////////////////
        // get all the k1 values

        WorldUtilities.applyForces(world);

        // save the intermediate forces
        for (int i=0; i<bodies.getSize(); ++i){
            final RigidBody p = (RigidBody) bodies.get(i);
            if (p.isFree()){
                ((VectorRW) k1Forces.get(i)).set(p.getMotion().getForce());
                ((VectorRW) k1Velocities.get(i)).set(p.getMotion().getVelocity());
            }
            p.getMotion().getForce().setAll(0.0);
        }

        ////////////////////////////////////////////////////////////////
        // get k2 values

        for (int i=0; i<bodies.getSize(); ++i){
            final RigidBody rb = (RigidBody) bodies.get(i);

            if (rb.isFree()){
                final VectorRW originalPosition = (VectorRW) originalPositions.get(i);
                final VectorRW k1Velocity = (VectorRW) k1Velocities.get(i);

                final VectorRW translation = VectorNf64.create(k1Velocity);
                translation.localScale(0.5 * timeellapsed);
                translation.localAdd(originalPosition);

                final VectorRW originalVelocity = (VectorRW) originalVelocities.get(i);
                final VectorRW k1Force = (VectorRW) k1Forces.get(i);

                rb.getMotion().getVelocity().set(k1Force);
                rb.getMotion().getVelocity().localScale(0.5 * timeellapsed / rb.getMass());
                rb.getMotion().getVelocity().localAdd(originalVelocity);

                rb.setWorldPosition(translation);
            }
        }

        WorldUtilities.applyForces(world);

        // save the intermediate forces
        for (int i=0; i<bodies.getSize(); ++i){
            final RigidBody p = (RigidBody) bodies.get(i);
            if (p.isFree()){
                ((VectorRW) k2Forces.get(i)).set(p.getMotion().getForce());
                ((VectorRW) k2Velocities.get(i)).set(p.getMotion().getVelocity());
            }

            p.getMotion().getForce().setAll(0.0); // and clear the forces now that we are done with them
        }


        /////////////////////////////////////////////////////
        // get k3 values

        for (int i=0; i<bodies.getSize(); ++i){
            final RigidBody rb = (RigidBody) bodies.get(i);
            if (rb.isFree()){
                final VectorRW originalPosition = (VectorRW) originalPositions.get(i);
                final VectorRW k2Velocity = (VectorRW) k2Velocities.get(i);

                final VectorRW translation = VectorNf64.create(k2Velocity);
                translation.localScale(0.5 * timeellapsed);
                translation.localAdd(originalPosition);

                final VectorRW originalVelocity = (VectorRW) originalVelocities.get(i);
                final VectorRW k2Force = (VectorRW) k2Forces.get(i);

                rb.getMotion().getVelocity().set(k2Force);
                rb.getMotion().getVelocity().localScale(0.5 * timeellapsed / rb.getMass());
                rb.getMotion().getVelocity().localAdd(originalVelocity);

                rb.setWorldPosition(translation);
            }
        }

        WorldUtilities.applyForces(world);

        // save the intermediate forces
        for (int i=0; i<bodies.getSize(); ++i){
            final RigidBody p = (RigidBody) bodies.get(i);
            if (p.isFree()){
                ((VectorRW) k3Forces.get(i)).set(p.getMotion().getForce());
                ((VectorRW) k3Velocities.get(i)).set(p.getMotion().getVelocity());
            }

            p.getMotion().getForce().setAll(0.0); // and clear the forces now that we are done with them
        }


        //////////////////////////////////////////////////
        // get k4 values

        for (int i=0; i<bodies.getSize(); ++i){
            final RigidBody rb = (RigidBody) bodies.get(i);
            if (rb.isFree()){
                final VectorRW originalPosition = (VectorRW) originalPositions.get(i);
                final VectorRW k3Velocity = (VectorRW) k3Velocities.get(i);

                final VectorRW translation = VectorNf64.create(k3Velocity);
                translation.localScale(timeellapsed);
                translation.localAdd(originalPosition);

                final VectorRW originalVelocity = (VectorRW) originalVelocities.get(i);
                final VectorRW k3Force = (VectorRW) k3Forces.get(i);

                rb.getMotion().getVelocity().set(k3Force);
                rb.getMotion().getVelocity().localScale(timeellapsed / rb.getMass());
                rb.getMotion().getVelocity().localAdd(originalVelocity);

                rb.setWorldPosition(translation);
            }
        }

        WorldUtilities.applyForces(world);

        // save the intermediate forces
        for (int i=0; i<bodies.getSize(); ++i){
            final RigidBody p = (RigidBody) bodies.get(i);
            if (p.isFree()){
                ((VectorRW) k4Forces.get(i)).set(p.getMotion().getForce());
                ((VectorRW) k4Velocities.get(i)).set(p.getMotion().getVelocity());
            }
        }

        /////////////////////////////////////////////////////////////
        // put them all together and what do you get?

        for (int i=0; i<bodies.getSize(); ++i){
            final RigidBody rb = (RigidBody) bodies.get(i);
            if (rb instanceof Particle){
                ((Particle) rb).setAge(((Particle) rb).getAge() + timeellapsed);
            }
            if (rb.isFree()){

                // update position
                final VectorRW originalPosition = (VectorRW) originalPositions.get(i);
                final VectorRW k1Velocity = (VectorRW) k1Velocities.get(i);
                final VectorRW k2Velocity = (VectorRW) k2Velocities.get(i);
                final VectorRW k3Velocity = (VectorRW) k3Velocities.get(i);
                final VectorRW k4Velocity = (VectorRW) k4Velocities.get(i);

                final VectorRW translation = VectorNf64.createDouble(worldDimension);
                k2Velocity.localScale(2.0);
                k3Velocity.localScale(2.0);
                translation.localAdd(k1Velocity);
                translation.localAdd(k2Velocity);
                translation.localAdd(k3Velocity);
                translation.localAdd(k4Velocity);
                translation.localScale(timeellapsed / 6.0);
                translation.localAdd(originalPosition);

                // update velocity
                final VectorRW originalVelocity = (VectorRW) originalVelocities.get(i);
                final VectorRW k1Force = (VectorRW) k1Forces.get(i);
                final VectorRW k2Force = (VectorRW) k2Forces.get(i);
                final VectorRW k3Force = (VectorRW) k3Forces.get(i);
                final VectorRW k4Force = (VectorRW) k4Forces.get(i);

                rb.getMotion().getVelocity().setAll(0.0);
                k2Force.localScale(2.0);
                k3Force.localScale(2.0);
                rb.getMotion().getVelocity().localAdd(k1Force);
                rb.getMotion().getVelocity().localAdd(k2Force);
                rb.getMotion().getVelocity().localAdd(k3Force);
                rb.getMotion().getVelocity().localAdd(k4Force);
                rb.getMotion().getVelocity().localScale(timeellapsed / (6.0f*rb.getMass()));
                rb.getMotion().getVelocity().localAdd(originalVelocity);

                rb.setWorldPosition(translation);
            }
        }
    }
}
