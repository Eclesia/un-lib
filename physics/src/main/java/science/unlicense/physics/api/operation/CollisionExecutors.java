
package science.unlicense.physics.api.operation;

import java.util.Random;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.operation.CentroidOp;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.operation.NearestOp;
import science.unlicense.geometry.api.operation.Operation;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.OperationExecutor;
import science.unlicense.geometry.api.operation.Operations;
import science.unlicense.geometry.api.operation.TransformOp;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.geometry.impl.tuple.BoxCursor;
import science.unlicense.image.impl.operation.GridOperations;
import science.unlicense.image.impl.s3d.Grid;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vectors;
import science.unlicense.physics.api.body.RigidBody;

/**
 *
 * @author Johann Sorel
 */
public class CollisionExecutors {

    private CollisionExecutors(){}

    /**
     * Collision for cercle and sphere.
     */
    private static Collision radiusCollision(Collision collision,
                            final Tuple c1, final double r1,
                            final Tuple c2, final double r2){

        //we could use distance and intersect operations but this would duplicate
        //multiple calculation, so we do it here.
        final double[] diff = Vectors.subtract(c2.toDouble(),c1.toDouble());
        final double r = r1+r2;
        final double rSquare = r*r;
        double length = Vectors.lengthSquare(diff);

        if (length > rSquare){
            //no collision
            collision.setCollide(false);
            return collision;
        }

        length = Math.sqrt(length);
        collision.setCollide(true);

        if (length==0){
            //bodies overlap, push it aside.
            collision.setDistance(-r1);
            final VectorRW normal = VectorNf64.createDouble(diff.length);
            normal.set(1, 1);
            collision.setNormal(normal);
        } else {
            collision.setDistance(-(r-length));
            collision.setNormal(VectorNf64.create(Vectors.scale(diff, 1/length)));
        }
        return collision;
    }

    /**
     * Collision for rectangle and bbox.
     */
    private static Collision boxCollision(Collision collision,
                                    final double[] min1, double[] max1,
                                    final double[] min2, double[] max2){

        //find if it intersects
        final double[] overlap = new double[min1.length];
        for (int i=0;i<min1.length;i++){
            if (max1[i] < min2[i] || max2[i] < min1[i]){
                //no overlap
                collision.setCollide(false);
                return collision;
            }
            overlap[i] = Maths.min(max1[i], max2[i]) - Maths.max(min1[i], min2[i]);
        }

        //find normal direction
        final double[] c1 = Vectors.add(min1, max1);
        Vectors.scale(c1, 0.5, c1);
        final double[] c2 = Vectors.add(min2, max2);
        Vectors.scale(c2, 0.5, c2);
        Vectors.subtract(c2, c1, c2);
        Vectors.normalize(c2, c2);

        collision.setCollide(true);
        collision.setNormal(VectorNf64.create(c2));
        collision.setDistance(-Maths.min(overlap));

        return collision;
    }

    private static int intersectSegment(double[] a, double[] b, double[] i, double[] p) {
        final double[] d = {b[0] - a[0], b[1] - a[1]};
        final double[] e = {p[0] - i[0], p[1] - i[1]};
        double denom = d[0]*e[1] - d[1]*e[0];
        if (denom == 0) {
            return -1;
        }
        double t = - (a[0]*e[1] - i[0]*e[1] - e[0]*a[1] + e[0]*i[1]) / denom;
        if (t < 0 || t > 1) {
            return 0;
        }
        t = - (- d[0]*a[1] + d[0]*i[1] + a[0]*d[1] - i[0]*d[1]) / denom;
        if (t < 0 || t > 1) {
            return 0;
        }
        return 1;
    }

    private static boolean collision(TupleGrid1D points, Tuple point) {
        // TODO use random API when it exists
        // we choose a point which is far from the polygon
        Random r = new Random();
        double x = 10000 + r.nextDouble();
        double y = 10000 + r.nextDouble();
        final double[] i = {x, y};

        // we count nb of intersections, if it's even the point is outside
        // if it's odd the point is inside
        int nbIntersections = 0;
        int last = points.getDimension()-1;
        final TupleRW a = points.createTuple();
        final VectorRW b = VectorNf64.createDouble(a.getSampleCount());
        for (int cpt = 0; cpt < last; cpt++) {
            points.getTuple(cpt,a);
            points.getTuple(cpt+1,b);
            int iseg = intersectSegment(a.toDouble(),
                b.toDouble(), i, new double[]{point.get(0), point.get(1)});
            if ((iseg == -1)) {
                return collision(points, point);
            }
            nbIntersections += iseg;
        }
        return nbIntersections % 2 == 1;
    }

    private static double polygonArea(TupleGrid1D points) {
        double area = 0;
        int last = points.getDimension()-1;
        final TupleRW a = points.createTuple();
        final VectorRW b = VectorNf64.createDouble(a.getSampleCount());
        for (int cpt = 0; cpt < last; cpt++) {
            points.getTuple(cpt,a);
            points.getTuple(cpt+1,b);
            area += a.get(0)*b.get(1) - b.get(0)*a.get(1);
        }
        return area / 2;
    }

    private static double[] polygonCenter(TupleGrid1D points, double area) {
        double[] center = new double[2];
        int last = points.getDimension()-1;
        final TupleRW a = points.createTuple();
        final VectorRW b = VectorNf64.createDouble(a.getSampleCount());
        for (int cpt = 0; cpt < last; cpt++) {
            points.getTuple(cpt,a);
            points.getTuple(cpt+1,b);
            center[0] += (a.get(0) + b.get(0)) * (a.get(0)*b.get(1) - b.get(0)*a.get(1));
            center[1] += (a.get(1) + b.get(1)) * (a.get(0)*b.get(1) - b.get(0)*a.get(1));
        }
        center[0] /= 6*area;
        center[1] /= 6*area;
        return center;
    }

    private static Collision pointPolygonCollision(Collision collision, TupleGrid1D outer, Sequence inners, Tuple point) {
        boolean pointInside = collision(outer, point);
        if (pointInside && inners != null) {
            for (int i=0,n=inners.getSize();i<n;i++){
                final Polyline hole = (Polyline) inners.get(i);
                pointInside = !collision(hole.getCoordinates(), point);
            }

        }
        if (!pointInside) {
            collision.setCollide(false);
            return collision;
        }

        //find normal direction
        final double[] c1 = polygonCenter(outer, polygonArea(outer));
        final double[] c2 = {point.get(0), point.get(1)};
        Vectors.subtract(c2, c1, c2);
        Vectors.normalize(c2, c2);

        collision.setCollide(true);
        collision.setNormal(VectorNf64.create(c2));
        collision.setDistance(0d);

        return collision;
    }

    private static Collision polygonPolygonCollision(Collision collision, TupleGrid1D coord1, Sequence holes1, TupleGrid1D coord2, Sequence holes2) {
        // TODO : un polygone est une suite de points, on peut donc réutiliser l'algo de la méthode pointPolygonCollision de façon itérative
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private abstract static class CollisionExecutor implements OperationExecutor{

        protected final Class firstGeomClass;
        protected final Class secondGeomClass;
        protected final boolean canInvert;

        public CollisionExecutor(Class firstGeomClass, Class secondGeomClass, boolean canInvert) {
            this.firstGeomClass = firstGeomClass;
            this.secondGeomClass = secondGeomClass;
            this.canInvert = canInvert;
        }

        public Class getOperationClass() {
            return Collision.class;
        }

        @Override
        public Object execute(Operation operation) throws OperationException {
            final Collision op = (Collision) operation;
            RigidBody firstRB = ((RigidBody) op.getFirst());
            RigidBody secondRB = ((RigidBody) op.getSecond());
            Geometry first = firstRB.getGeometry();
            Geometry second = secondRB.getGeometry();

            //convert geometries to world space for collision test
            first = (Geometry) Operations.execute(new TransformOp(first, firstRB.getNodeToRootSpace()));
            second = (Geometry) Operations.execute(new TransformOp(second, secondRB.getNodeToRootSpace()));


            boolean inverted = false;
            if (canInvert && !(firstGeomClass.isInstance(first) && secondGeomClass.isInstance(second))){
                //set geometries in expected order
                inverted = true;
                //invert geometries
                Geometry temp = first;
                first = second;
                second = temp;
                //invert rb
                RigidBody tempRB = firstRB;
                firstRB = secondRB;
                secondRB = tempRB;
            }
            try{
                executeInternal(op, firstRB, first, secondRB, second);
            }catch(Exception ex){
                return op;
            }
            if (op.isCollide()){
                //convert impact position back to geometry space TODO
//                final Geometry[] impacts = op.getImpactGeometries();
//                impacts[0] = (Geometry) Operations.execute(new Transform(impacts[0], firstRB.getRootToNodeSpace()));
//                impacts[1] = (Geometry) Operations.execute(new Transform(impacts[1], secondRB.getRootToNodeSpace()));

                if (inverted){
                    //flip the normal if first<>second have been inverted
                    op.getNormal().localNegate();
//                    Geometry imp = impacts[0];
//                    impacts[0] = impacts[1];
//                    impacts[1] = imp;
                }


            }

            //validate collision values
            if (op.isCollide() && !op.getNormal().isFinite()){
                throw new RuntimeException("Collision normal is not finite");
            }

            return op;
        }

        protected abstract Object executeInternal(Collision op,
                RigidBody firstRB, Object first,
                RigidBody secondRB, Object second) throws OperationException;

        @Override
        public boolean canHandle(Operation operation) {
            if (!(operation instanceof Collision)) return false;
            final Collision op = (Collision) operation;
            if (!(op.getFirst() instanceof RigidBody)) return false;
            if (!(op.getSecond() instanceof RigidBody)) return false;
            final Object first = ((RigidBody) op.getFirst()).getGeometry();
            final Object second = ((RigidBody) op.getSecond()).getGeometry();
            if (canInvert){
                return (firstGeomClass.isInstance(first)  && secondGeomClass.isInstance(second))
                    || (firstGeomClass.isInstance(second) && secondGeomClass.isInstance(first));
            } else {
                return (firstGeomClass.isInstance(first)  && secondGeomClass.isInstance(second));
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // 2D //////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public static final CollisionExecutor RECTANGLE_POINT =
            new CollisionExecutor(Rectangle.class, Point.class, true) {
        @Override
        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Rectangle rect = (Rectangle) first;
            final Point point = (Point) second;
            final TupleRW coord = point.getCoordinate();
            return boxCollision(collision, new double[]{rect.getX(), rect.getY()},
                                           new double[]{rect.getX()+rect.getWidth(), rect.getY()+rect.getHeight()},
                                           new double[]{coord.get(0), coord.get(1)},
                                           new double[]{coord.get(0), coord.get(1)});
        }
    };

    public static final CollisionExecutor RECTANGLE_RECTANGLE =
            new CollisionExecutor(Rectangle.class, Rectangle.class, false){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Rectangle rect1 = (Rectangle) first;
            final Rectangle rect2 = (Rectangle) second;
            return boxCollision(collision, new double[]{rect1.getX(),rect1.getY()},
                                           new double[]{rect1.getX()+rect1.getWidth(),rect1.getY()+rect1.getHeight()},
                                           new double[]{rect2.getX(),rect2.getY()},
                                           new double[]{rect2.getX()+rect2.getWidth(),rect2.getY()+rect2.getHeight()});
        }
    };

    public static final CollisionExecutor RECTANGLE_CIRCLE =
            new CollisionExecutor(Rectangle.class, Circle.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Rectangle rect = (Rectangle) first;
            final Circle circle  = (Circle) second;

            return collision;
        }
    };

    public static final CollisionExecutor POLYGON_POINT =
            new CollisionExecutor(Polygon.class, Point.class, true) {
        @Override
        protected Object executeInternal(Collision op, RigidBody firstRB, Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Polygon pol = (Polygon) first;
            final Point point = (Point) second;
            return pointPolygonCollision(collision, pol.getExterior().getCoordinates(), pol.getInteriors(), point.getCoordinate());
        }
    };

    public static final CollisionExecutor POLYGON_POLYGON =
            new CollisionExecutor(Polygon.class, Point.class, true) {
        @Override
        protected Object executeInternal(Collision op, RigidBody firstRB, Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Polygon pol1 = (Polygon) first;
            final Polygon pol2 = (Polygon) second;
            return polygonPolygonCollision(collision, pol1.getExterior().getCoordinates(), pol1.getInteriors(), pol2.getExterior().getCoordinates(), pol2.getInteriors());
        }
    };

    public static final CollisionExecutor CIRCLE_POINT =
            new CollisionExecutor(Circle.class, Point.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Circle circle = (Circle) first;
            final Point point = (Point) second;

            return radiusCollision(collision,
                    circle.getCenter(), circle.getRadius(),
                    point.getCoordinate(), 0);
        }
    };

    public static final CollisionExecutor CIRCLE_CIRCLE =
            new CollisionExecutor(Circle.class, Circle.class, false){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Circle circle1 = (Circle) first;
            final Circle circle2 = (Circle) second;

            return radiusCollision(collision,
                    circle1.getCenter(), circle1.getRadius(),
                    circle2.getCenter(), circle2.getRadius());
        }
    };

    ////////////////////////////////////////////////////////////////////////////
    // 3D //////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public static final CollisionExecutor PLANE_SPHERE =
            new CollisionExecutor(Plane.class, Sphere.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Plane plane = (Plane) first;
            final Sphere sphere = (Sphere) second;
            final double radius = sphere.getRadius();
            final double realDistance = DistanceOp.distance(sphere.getCenter(), plane);
            final double penetration;
            final boolean collide;
            if (realDistance<=0){
                //sphere center is under the plan
                collide = true;
                penetration = realDistance - radius;
            } else {
                collide = realDistance < radius;
                penetration = (collide) ? -(radius-realDistance) : (radius-realDistance);
            }
            collision.setCollide(collide);
            collision.setNormal(plane.getNormal().copy());
            collision.setDistance(penetration);

            if (collide){
                //calculate collision points
                final double[] impactPoint = Geometries.projectPointOnPlan(sphere.getCenter().toDouble(), plane.getNormal().toDouble(), plane.getD());
                collision.setImpactGeometries(new Geometry[]{
                    new DefaultPoint(impactPoint),
                    new DefaultPoint(impactPoint)
                });

                //calculate rollback to find first contact
                final VectorRW velocityA = firstRB.getMotion().getVelocity();
                final VectorRW velocityB = secondRB.getMotion().getVelocity();
                final VectorRW relativeVelocity = velocityB.subtract(velocityA);

                final double angle = plane.calculateAngle(relativeVelocity);
                final double rollbackDist = penetration / Math.sin(angle);
                final double rollbackTime = rollbackDist / relativeVelocity.length();
                collision.setRollbackTime(rollbackTime);

            }
            return collision;
        }
    };


    public static final CollisionExecutor SEGMENT_SPHERE =
            new CollisionExecutor(Line.class, Sphere.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Line segment = (Line) first;
            final Sphere sphere = (Sphere) second;
            final double radius = sphere.getRadius();

            final double[] nearest = new double[segment.getStart().getSampleCount()];
            final double distanceSquare = DistanceOp.distanceSquare(segment.getStart().toDouble(), segment.getEnd().toDouble(), nearest, sphere.getCenter().toDouble(), new double[1], op.getEpsilon());
            final double realDistance = Math.sqrt(distanceSquare);


            final boolean collide = realDistance < radius;
            final double penetration = (collide) ? -(radius-realDistance) : (radius-realDistance);
            final VectorRW normal = VectorNf64.create(Arrays.copy(nearest, new double[nearest.length]));
            normal.localSubtract(sphere.getCenter());
            normal.localNormalize();

            collision.setCollide(collide);
            collision.setNormal(normal);
            collision.setDistance(penetration);

            if (collide){
                //calculate collision points
                collision.setImpactGeometries(new Geometry[]{
                    new DefaultPoint(Arrays.copy(nearest,new double[nearest.length])),
                    new DefaultPoint(Arrays.copy(nearest,new double[nearest.length]))
                });

                //calculate rollback to find first contact
                final VectorRW velocityA = firstRB.getMotion().getVelocity();
                final VectorRW velocityB = secondRB.getMotion().getVelocity();
                final VectorRW relativeVelocity = velocityB.subtract(velocityA);

                final Plane p = new Plane(normal, VectorNf64.create(Arrays.copy(nearest,new double[nearest.length])));
                final double angle = p.calculateAngle(relativeVelocity);
                final double rollbackDist = penetration / Math.sin(angle);
                final double rollbackTime = rollbackDist / relativeVelocity.length();
                collision.setRollbackTime(rollbackTime);

            }
            return collision;
        }
    };

    public static final CollisionExecutor BBOX_BBOX =
            new CollisionExecutor(BBox.class, BBox.class, false){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final BBox bbox1 = (BBox) first;
            final BBox bbox2 = (BBox) second;
            return boxCollision(collision, bbox1.getLower().toDouble(),
                                           bbox1.getUpper().toDouble(),
                                           bbox2.getLower().toDouble(),
                                           bbox2.getUpper().toDouble());
        }
    };

    public static final CollisionExecutor BBOX_SPHERE =
            new CollisionExecutor(BBox.class, Sphere.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) throws OperationException {
            final Collision collision = (Collision) op;
            final BBox bbox = (BBox) first;
            final Sphere sphere = (Sphere) second;
            final Tuple center = sphere.getCenter();
            final double radius = sphere.getRadius();

            //find nearest plan
            int index = -1;
            double d = Double.POSITIVE_INFINITY;
            for (int i=0;i<3;i++){
                double c = center.get(i);
                double d1 = Math.abs(c-bbox.getMax(i));
                double d2 = Math.abs(c-bbox.getMin(i));
                if (d1<d){
                    d = d1;
                    index = i*2;
                }
                if (d2<d){
                    d = d2;
                    index = i*2 + 1;
                }
            }

            //check if point projects on the plan
            final Plane plane;
            switch(index){
                case 0 : plane = new Plane(new Vector3f64(+1, 0, 0), new Vector3f64(bbox.getMax(0),0,0)); break;
                case 1 : plane = new Plane(new Vector3f64(-1, 0, 0), new Vector3f64(bbox.getMin(0),0,0)); break;
                case 2 : plane = new Plane(new Vector3f64( 0,+1, 0), new Vector3f64(0,bbox.getMax(1),0)); break;
                case 3 : plane = new Plane(new Vector3f64( 0,-1, 0), new Vector3f64(0,bbox.getMin(1),0)); break;
                case 4 : plane = new Plane(new Vector3f64( 0, 0,+1), new Vector3f64(0,0,bbox.getMax(2))); break;
                case 5 : plane = new Plane(new Vector3f64( 0, 0,-1), new Vector3f64(0,0,bbox.getMin(2))); break;
                default : throw new RuntimeException("Invalid index");
            }
            final double[] proj = Geometries.projectPointOnPlan(center.toDouble(), plane.getNormal().toDouble(), plane.getD());
            if (bbox.intersects(VectorNf64.create(proj))){
                //point is on or inside the face
                return PLANE_SPHERE.executeInternal(op, firstRB, plane, secondRB, second);
            } else {
                //point is outside the face
                //check if it's not too far
                double dist = DistanceOp.distance(center, plane);
                if (dist>radius) return collision;

                //check this face edges
                final double min0 = bbox.getMin(0);
                final double max0 = bbox.getMax(0);
                final double min1 = bbox.getMin(1);
                final double max1 = bbox.getMax(1);
                final double min2 = bbox.getMin(2);
                final double max2 = bbox.getMax(2);
                final Line[] edges;
                switch(index){
                    case 0 :
                    case 1 : {
                             final double k = index==0 ? max0 : min0;
                             edges = new Line[]{
                                new DefaultLine(new Vector3f64(k,min1,min2),new Vector3f64(k,min1,max2)),
                                new DefaultLine(new Vector3f64(k,min1,max2),new Vector3f64(k,max1,max2)),
                                new DefaultLine(new Vector3f64(k,max1,max2),new Vector3f64(k,max1,min2)),
                                new DefaultLine(new Vector3f64(k,max1,min2),new Vector3f64(k,min1,min2))
                             };
                             }break;
                    case 2 :
                    case 3 : {
                             final double k = index==2 ? max1 : min1;
                             edges = new Line[]{
                                new DefaultLine(new Vector3f64(min0,k,min2),new Vector3f64(min0,k,max2)),
                                new DefaultLine(new Vector3f64(min0,k,max2),new Vector3f64(max0,k,max2)),
                                new DefaultLine(new Vector3f64(max0,k,max2),new Vector3f64(max0,k,min2)),
                                new DefaultLine(new Vector3f64(max0,k,min2),new Vector3f64(min0,k,min2))
                             };
                             }break;
                    case 4 :
                    case 5 : {
                             final double k = index==4 ? max2 : min2;
                             edges = new Line[]{
                                new DefaultLine(new Vector3f64(min0,min1,k),new Vector3f64(min0,max1,k)),
                                new DefaultLine(new Vector3f64(min0,max1,k),new Vector3f64(max0,max1,k)),
                                new DefaultLine(new Vector3f64(max0,max1,k),new Vector3f64(max0,min1,k)),
                                new DefaultLine(new Vector3f64(max0,min1,k),new Vector3f64(min0,min1,k))
                             };
                             }break;

                    default : throw new RuntimeException("Invalid index");
                }

                //find nearest edge
                d = Double.POSITIVE_INFINITY;
                Line edge = null;
                for (int i=0;i<4;i++){
                    dist = DistanceOp.distance(edges[i].getStart(), edges[i].getEnd(), center);
                    if (dist < radius && dist<d){
                        edge = edges[i];
                        d = dist;
                    }
                }

                if (edge!=null){
                    return SEGMENT_SPHERE.executeInternal(op, firstRB, edge, secondRB, second);
                }
            }

            return collision;
        }
    };

    public static final CollisionExecutor BBOX_CAPSULE =
            new CollisionExecutor(BBox.class, Capsule.class, true){

        protected Object executeInternal(Collision collision, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) throws OperationException {
            final BBox bbox = (BBox) first;
            final Capsule capsule = (Capsule) second;

            //TODO
            collision.setCollide(false);
            if (true) return collision;

            //find the nearest point on the bbox
            final Geometry[] nearest = new NearestOp(bbox, new DefaultLine(capsule.getFirst(),capsule.getSecond())).execute();
            final VectorRW direction = VectorNf64.create(new CentroidOp(nearest[0]).execute().getCoordinate());
            direction.localSubtract(new CentroidOp(nearest[1]).execute().getCoordinate());
            final double distance = direction.length();
            boolean collide = distance <= capsule.getRadius();

            collision.setCollide(collide);

            //calculate the push back direction
            if (distance>0){
                //center of the sphere is outside the bbox
                collision.setNormal(direction.negate().localNormalize());
                collision.setDistance(-(capsule.getRadius()-distance));
            } else {
                //center of the sphere is inside the bbox
                //find the closest border
                //TODO
//                final Vector bboxCenter = new Vector(bbox.getCentroid().getCoordinate());
//                bboxCenter.localSubtract(sphere.getCenter());
//                final double[] diff = bboxCenter.getValues();
//                final int axis;
//                if (diff[0]<diff[1] && diff[0]<diff[2]) axis=0;
//                else if (diff[1]<diff[0] && diff[1]<diff[2]) axis=1;
//                else axis = 2;
//
//                collision.setDistance(sphere.getRadius());

                //push it back up for now
                collision.setNormal(new Vector3f64(0, 1, 0));
                collision.setDistance(capsule.getRadius());

            }

            return collision;
        }
    };

    public static final CollisionExecutor SPHERE_SPHERE =
            new CollisionExecutor(Sphere.class, Sphere.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Sphere sphere1 = (Sphere) first;
            final Sphere sphere2 = (Sphere) second;

            return radiusCollision(collision,
                    sphere1.getCenter(), sphere1.getRadius(),
                    sphere2.getCenter(), sphere2.getRadius());
        }
    };

    public static final CollisionExecutor SPHERE_CAPSULE =
            new CollisionExecutor(Sphere.class, Capsule.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;

            final Sphere sphere = (Sphere) first;
            final double[] sphereFirst = sphere.getCenter().toDouble();
            final double[] sphereSecond = sphereFirst;
            final double[] sphereClosest = new double[3];

            final Capsule capsule = (Capsule) second;
            final double[] capsuleFirst = capsule.getFirst().toDouble();
            final double[] capsuleSecond = capsule.getSecond().toDouble();
            final double[] capsuleClosest = new double[3];

            final double[] ratio = new double[2];

            double distance = DistanceOp.distance(
                    capsuleFirst, capsuleSecond, capsuleClosest,
                    sphereFirst, sphereSecond, sphereClosest,
                    ratio, op.getEpsilon());

            //remove radius
            distance -= sphere.getRadius();
            distance -= capsule.getRadius();

            collision.setImpactGeometries(new Geometry[]{new DefaultPoint(sphereClosest),new DefaultPoint(capsuleClosest)});

            VectorRW normal;
            if (distance<=0){
                //capsule segment and sphere center intersect
                normal = VectorNf64.create(Vectors.subtract(capsule.getCentroid().getCoordinate().toDouble(),sphereClosest)).localNormalize();
            } else {
                normal = VectorNf64.create(Vectors.subtract(capsuleClosest,sphereClosest)).localNormalize();
            }

            if (!normal.isFinite()){
                normal.setXYZ(0, 1, 0);
            }

            collision.setNormal(normal);
            collision.setCollide(distance<0);
            collision.setDistance(distance);

            return collision;
        }
    };

    public static final CollisionExecutor SPHERE_GRID =
            new CollisionExecutor(Sphere.class, Grid.class, true){

        @Override
        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) throws OperationException {
            final Collision collision = (Collision) op;
            final Sphere a = (Sphere) first;
            final Grid b = (Grid) second;

            final double radius = a.getRadius();
            final TupleGrid sm = b.getTuples();
            final double[] coord = GridOperations.toGridCoord(b, a.getCenter());
            //check if point is outside grid
            for (int i=0;i<coord.length;i++){
                if (coord[i]+radius<0 || coord[i]-radius>=sm.getExtent().get(i)) return false;
            }

            //check all intersecting blocks
            final double[] lower = new double[coord.length];
            final double[] upper = new double[coord.length];
            for (int i=0;i<coord.length;i++){
                lower[i] = (int) Math.floor(coord[i]-radius);
                upper[i] = (int) Math.ceil(coord[i]+radius);
                //check we don't go out of image
                lower[i] = Maths.max(lower[i], 0);
                upper[i] = Maths.min(upper[i], sm.getExtent().get(i));
            }

            final TupleGridCursor cursor = new BoxCursor(sm.cursor(),new BBox(lower, upper));
            final BBox voxel = new BBox(lower.length);
            boolean[] sample = new boolean[1];
            while (cursor.next()) {
                cursor.samples().toBoolean(sample, 0);
                if (sample[0]){
                    //check collision with sphere
                    final Tuple pixelCoord = cursor.coordinate();
                    for (int i=0,n=pixelCoord.getSampleCount();i<n;i++){
                        voxel.getLower().set(i, pixelCoord.get(i));
                        voxel.getUpper().set(i, pixelCoord.get(i)+1);
                    }
                    //convert coordinate in world space
                    b.getGridToGeom().transform(voxel.getLower(), voxel.getLower());
                    b.getGridToGeom().transform(voxel.getUpper(), voxel.getUpper());

                    final RigidBody rb1 = new RigidBody(a, firstRB.getMass());
                    final RigidBody rb2 = new RigidBody(voxel, secondRB.getMass());
                    final Collision col = new Collision(rb1, rb2);
                    Operations.execute(col);
                    if (col.isCollide()){
                        collision.setCollide(true);
                        collision.setDistance(col.getDistance());
                        collision.setNormal(col.getNormal());
                        return collision;
                    }
                }
            }

            collision.setCollide(false);
            return collision;

        }
    };

    public static final CollisionExecutor CAPSULE_CAPSULE =
            new CollisionExecutor(Capsule.class, Capsule.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) throws OperationException {
            final Collision collision = (Collision) op;
            final Capsule capsule1 = (Capsule) first;
            final Capsule capsule2 = (Capsule) second;

            final Geometry[] nearest = (Geometry[]) Operations.execute(new NearestOp(
                    new DefaultLine(capsule1.getFirst(),capsule1.getSecond()),
                    new DefaultLine(capsule2.getFirst(),capsule2.getSecond())
                    ));

            final VectorRW direction = VectorNf64.create(new CentroidOp(nearest[0]).execute().getCoordinate());
            direction.localSubtract(new CentroidOp(nearest[1]).execute().getCoordinate());
            final double distance = direction.length();
            final double sumRadius = capsule1.getRadius() + capsule2.getRadius();
            final boolean collide = distance <= sumRadius;
            collision.setCollide(collide);
            collision.setDistance(distance-sumRadius);
            if (distance==0){
                //geometries overlap exactly
                collision.setNormal(new Vector3f64(0, 1, 0));
            } else {
                collision.setNormal(direction.negate().localNormalize());
            }


            if (!collision.getNormal().isFinite()){
                System.out.println("la");
            }

            return collision;
        }
    };

    public static final CollisionExecutor PLANE_PLANE =
            new CollisionExecutor(Plane.class, Plane.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            //planes are infinite, unless they are stictly parallel they always collide.
            //planes are part of a scene, but we ignore collision between them
            collision.setCollide(false);
            return collision;
        }
    };

    public static final CollisionExecutor PLANE_CAPSULE =
            new CollisionExecutor(Plane.class, Capsule.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) throws OperationException {
            final Collision collision = (Collision) op;
            final Plane plane = (Plane) first;
            final Capsule capsule = (Capsule) second;
            final double radius = capsule.getRadius();

            final Geometry[] nearest = new NearestOp(plane, new DefaultLine(capsule.getFirst(),capsule.getSecond())).execute();
            final VectorRW nearestCapsule = VectorNf64.create(new CentroidOp(nearest[0]).execute().getCoordinate());
            final VectorRW nearestPlane = VectorNf64.create(new CentroidOp(nearest[1]).execute().getCoordinate());
            final VectorRW direction = nearestCapsule.subtract(nearestPlane);
            final double distance = direction.length();
            final boolean collide = distance <= radius;

            collision.setCollide(collide);
            if (!collide) return collision;

            //calculate collision points
            final VectorRW impactCapsule = nearestCapsule.add(direction.scale(distance));
            final VectorRW impactPlane = nearestPlane;
            collision.setImpactGeometries(new Geometry[]{
                new DefaultPoint(impactPlane),
                new DefaultPoint(impactCapsule)
            });

            //calculate the push back direction
            if (distance>0){
                //center of the sphere is outside the bbox
                collision.setNormal(direction.negate().localNormalize());
                collision.setDistance(-(radius-distance));
            } else {
                //center of the sphere is inside the bbox
                //find the closest border
                //TODO
//                final Vector bboxCenter = new Vector(bbox.getCentroid().getCoordinate());
//                bboxCenter.localSubtract(sphere.getCenter());
//                final double[] diff = bboxCenter.getValues();
//                final int axis;
//                if (diff[0]<diff[1] && diff[0]<diff[2]) axis=0;
//                else if (diff[1]<diff[0] && diff[1]<diff[2]) axis=1;
//                else axis = 2;
//
//                collision.setDistance(sphere.getRadius());

                //push it back up for now
                collision.setNormal(new Vector3f64(0, 1, 0));
                collision.setDistance(radius);

            }

            return collision;
        }
    };



}
