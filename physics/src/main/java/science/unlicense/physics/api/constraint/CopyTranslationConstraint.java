
package science.unlicense.physics.api.constraint;

import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class CopyTranslationConstraint implements Constraint{

    private final SceneNode target;
    private final SceneNode toCopy;
    private final float factor;

    public CopyTranslationConstraint(SceneNode target, SceneNode toCopy, float factor) {
        this.target = target;
        this.toCopy = toCopy;
        this.factor = factor;
    }

    public void apply() {
        applyConstraint(target, toCopy, factor);
    }

    static void applyConstraint(SceneNode target, SceneNode toCopy, float factor) {
        final VectorRW trs = toCopy.getNodeTransform().getTranslation();
        final VectorRW baseTrs = target.getNodeTransform().getTranslation();
        trs.scale(factor, baseTrs);
        target.getNodeTransform().notifyChanged();
    }

}
