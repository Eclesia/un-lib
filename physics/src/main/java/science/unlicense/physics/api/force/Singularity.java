
package science.unlicense.physics.api.force;

import science.unlicense.physics.api.body.Body;

/**
 *
 * @author Johann Sorel
 */
public interface Singularity {

    /**
     * Apply singularity on given body.
     * @param body
     */
    void apply(Body body);

}
