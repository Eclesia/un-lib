
package science.unlicense.physics.api.body;

import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Current motion state of a rigid body.
 *
 * @author Johann Sorel
 */
public class Motion {

    protected final VectorRW velocity;
    protected final VectorRW force;
    protected final VectorRW angularVelocity;
    protected final VectorRW torque;

    public Motion(int dim) {
        this.velocity = VectorNf64.createDouble(dim);
        this.force = VectorNf64.createDouble(dim);
        this.angularVelocity = VectorNf64.createDouble(dim);
        this.torque = VectorNf64.createDouble(dim);
    }

    /**
     * Get body velocity.
     * @return Vector velocity in unit/second
     */
    public VectorRW getVelocity() {
        return velocity;
    }

    /**
     * Get body currently applied forces.
     * @return Vector
     */
    public VectorRW getForce() {
        return force;
    }

    /**
     * Rotation angle x rotation speed.
     * @return Vector
     */
    public VectorRW getAngularVelocity() {
        return angularVelocity;
    }

    /**
     * Similar to Force but for body rotation.
     * @return Vector
     */
    public VectorRW getTorque() {
        return torque;
    }

}
