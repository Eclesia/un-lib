
package science.unlicense.physics.api.constraint;

import science.unlicense.display.api.scene.SceneNode;

/**
 *
 * @author Johann Sorel
 */
public class CopyTransformConstraint implements Constraint{

    private final SceneNode target;
    private final SceneNode toCopy;
    private final float factor;

    public CopyTransformConstraint(SceneNode target, SceneNode toCopy, float factor) {
        this.target = target;
        this.toCopy = toCopy;
        this.factor = factor;
    }

    public void apply() {
        CopyRotationConstraint.applyConstraint(target, toCopy, factor);
        CopyTranslationConstraint.applyConstraint(target, toCopy, factor);
        CopyScaleConstraint.applyConstraint(target, toCopy, factor);
    }

}
