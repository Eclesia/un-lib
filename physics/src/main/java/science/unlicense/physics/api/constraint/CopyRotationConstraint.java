
package science.unlicense.physics.api.constraint;

import science.unlicense.math.api.MatrixRW;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.math.impl.Matrix3x3;

/**
 *
 * @author Johann Sorel
 */
public class CopyRotationConstraint implements Constraint{

    private final SceneNode target;
    private final SceneNode toCopy;
    private final float factor;

    /**
     *
     * @param target
     * @param toCopy
     * @param factor between 0 and 1
     */
    public CopyRotationConstraint(SceneNode target, SceneNode toCopy, float factor) {
        this.target = target;
        this.toCopy = toCopy;
        this.factor = factor;
    }

    public void apply() {
        applyConstraint(target, toCopy, factor);
    }

    static void applyConstraint(SceneNode target, SceneNode toCopy, float factor) {
        Matrix3x3 rotation = new Matrix3x3(toCopy.getNodeTransform().getRotation());

        final MatrixRW baserot = target.getNodeTransform().getRotation();
        baserot.set(rotation);
        target.getNodeTransform().notifyChanged();
    }

}
