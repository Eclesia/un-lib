
package science.unlicense.physics.api.law;

import science.unlicense.math.api.Maths;
import science.unlicense.math.api.VectorRW;

/**
 * Source : http://en.wikipedia.org/wiki/Damping
 *
 * F = -cv
 * where :
 * c is the viscous damping coefficient
 * v is the velocity
 *
 * @author Johann Sorel
 */
public final class Damping {

    private Damping(){}


    /**
     * Calculate force.
     * F = -cv
     *
     * @param viscousCoeff
     * @param velocity
     * @return Vector force
     */
    public static VectorRW force(double viscousCoeff, VectorRW velocity){
        return velocity.scale(viscousCoeff).localNegate();
    }

    /**
     * Calculate viscous damping coefficient.
     * c = -(F/v)
     *
     * @param force
     * @param velocity
     * @return double viscous damping coefficient
     */
    public static double viscousCoeff(VectorRW force, VectorRW velocity){
        final VectorRW t = force.divide(velocity).localNegate();
        return Maths.mean(t.toDouble());
    }

    /**
     * Calculate velocity.
     * v = F/-c
     *
     * @param force
     * @param viscousCoeff
     * @return double stiffness
     */
    public static VectorRW velocity(VectorRW force, double viscousCoeff){
        return force.scale(1/-(viscousCoeff));
    }

}
