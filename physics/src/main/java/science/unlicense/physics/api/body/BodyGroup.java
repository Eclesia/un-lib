
package science.unlicense.physics.api.body;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Set;

/**
 * A Body group is a set of bodies which doesn't collide between them.
 *
 * @author Johann Sorel
 */
public class BodyGroup {

    private Chars name;

    private final Set bodies = new HashSet();

    public BodyGroup(Chars name) {
        this.name = name;
    }

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        this.name = name;
    }

    public Set getBodies() {
        return Collections.readOnlySet(bodies);
    }

    /**
     * Add given body to this group.
     *
     * @param body
     */
    public void add(Body body){
        bodies.add(body);
    }

    /**
     * Remove given body from this group.
     *
     * @param body
     */
    public void remove(Body body){
        bodies.remove(body);
    }

    /**
     * Test if given body is in this group.
     *
     * @param first
     * @return true if body is in this group
     */
    public boolean contains(Body first){
        return bodies.contains(first);
    }

    /**
     * Test if given bodies are in this group.
     *
     * @param first
     * @param second
     * @return true if both bodies are in this group
     */
    public boolean contains(Body first, Body second){
        return bodies.contains(first) && bodies.contains(second);
    }

}
