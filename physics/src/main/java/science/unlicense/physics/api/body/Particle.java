
package science.unlicense.physics.api.body;

import science.unlicense.geometry.impl.DefaultPoint;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class Particle extends RigidBody {

    private double age = 0;

    public Particle(int dimension, double m) {
        super(new DefaultPoint(dimension),m);
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

}
