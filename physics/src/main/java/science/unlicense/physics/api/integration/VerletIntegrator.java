
package science.unlicense.physics.api.integration;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Hasher;
import science.unlicense.common.api.collection.HasherDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.WorldUtilities;
import science.unlicense.physics.api.body.Body;
import science.unlicense.physics.api.body.RigidBody;

/**
 *
 * Source :
 * http://en.wikipedia.org/wiki/Verlet_integration#Velocity_Verlet
 *
 * tutorials :
 * http://gamedevelopment.tutsplus.com/tutorials/simulate-tearable-cloth-and-ragdolls-with-simple-verlet-integration--gamedev-519
 * http://www.gamedev.net/page/resources/_/technical/math-and-physics/a-verlet-based-approach-for-2d-game-physics-r2714
 * http://graphics.cs.cmu.edu/nsp/course/15-869/2006/papers/jakobsen.htm
 *
 * @author Johann Sorel
 */
public final class VerletIntegrator extends AbstractIntegrator{

    private static final class VerletState{
        public final VectorRW lastPosition;
        public final VectorRW acceleration;

        public VerletState(int wd) {
            this.lastPosition = VectorNf64.createDouble(wd);
            this.acceleration = VectorNf64.createDouble(wd);
        }
    }

    private final Dictionary states = new HasherDictionary(Hasher.IDENTITY);
    private final int worldDimension;

    public VerletIntegrator(int worldDimension) {
        this.worldDimension = worldDimension;
    }

    @Override
    public void update(World world, double timeellapsed) {

        final Sequence bodies = world.getBodies();

        //apply singularity and forces
        WorldUtilities.applyForces(world);

        //calculate acceleration
        for (int i=0,n=bodies.getSize(); i<n; i++){
            final Body body = (Body) bodies.get(i);
            if (!(body instanceof RigidBody)) continue;
            final RigidBody rb = (RigidBody) body;
            if (!rb.isFree()) continue;

            VerletState state = (VerletState) states.getValue(rb);
            if (state==null){
                state = new VerletState(worldDimension);
                state.lastPosition.set(rb.getWorldPosition(null));
                states.add(rb, state);
            }

            rb.getAcceleration(state.acceleration);
        }

        //solve constraints
        for (int i=0,n=bodies.getSize(); i<n; i++){
            final Body body = (Body) bodies.get(i);
            if (!(body instanceof RigidBody)) continue;
            final RigidBody rb = (RigidBody) body;
            if (!rb.isFree()) continue;

            //TODO

        }

        //calculate positions base on acceleration
        final double time2 = timeellapsed * timeellapsed;
        for (int i=0,n=bodies.getSize(); i<n; i++){
            final Body body = (Body) bodies.get(i);
            if (!(body instanceof RigidBody)) continue;
            final RigidBody rb = (RigidBody) body;
            if (!rb.isFree()) continue;

            final VerletState state = (VerletState) states.getValue(rb);
            final VectorRW pos = rb.getWorldPosition(null);
            final VectorRW vel = pos.subtract(state.lastPosition);
            final VectorRW newPos = vel.add(state.acceleration.scale(time2));
            state.lastPosition.set(pos);
            rb.setWorldPosition(newPos);
        }

        //clear forces
        WorldUtilities.clearBodyForces(world);

    }

}
