
package science.unlicense.physics.api.operation;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.AbstractOperation;
import science.unlicense.math.api.VectorRW;
import science.unlicense.physics.api.body.Body;

/**
 * Collision operation.
 *
 * @author Johann Sorel
 */
public class Collision extends AbstractOperation {

    public static final Chars NAME = Chars.constant(new byte[]{'C','O','L','L','I','S','I','O','N'});

    private boolean collide = false;
    private VectorRW normal;
    private double distance;
    private Geometry[] impactGeometries;
    private Body first;
    private Body second;

    //rollback in time to find first contact
    private double timeRollback;

    public Collision(Body first, Body second) {
        this.first = first;
        this.second = second;
    }

    public Chars getName() {
        return NAME;
    }

    public void reset(Body first, Body second){
        this.first = first;
        this.second = second;
        this.collide = false;
        this.normal = null;
        this.distance = 0;
        impactGeometries = null;
        timeRollback = 0.0;
    }

    public Body getFirst() {
        return first;
    }

    public Body getSecond() {
        return second;
    }

    public void setFirst(Body first) {
        this.first = first;
    }

    public void setSecond(Body second) {
        this.second = second;
    }

    public boolean isCollide() {
        return collide;
    }

    public void setCollide(boolean collide) {
        this.collide = collide;
    }

    /**
     * Direction to push back second body.
     * @return Vector in world space
     */
    public VectorRW getNormal() {
        return normal;
    }

    /**
     * Set direction to push back second body.
     *
     * @param normal in world space
     */
    public void setNormal(VectorRW normal) {
        this.normal = normal;
    }

    /**
     * Get distance between object.
     * Position value when objects do not collide
     * Negative value is a the penetration depth.
     * @return distance or penetration if negative
     */
    public double getDistance() {
        return distance;
    }

    /**
     *
     * @param distance
     */
    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Geometry[] getImpactGeometries() {
        return impactGeometries;
    }

    public void setImpactGeometries(Geometry[] impactGeometries) {
        this.impactGeometries = impactGeometries;
    }

    public void setRollbackTime(double time){
        this.timeRollback = time;
    }

    /**
     * Returns the time backward to the first contact.
     * @return time in seconds
     */
    public double getRollbackTime() {
        return timeRollback;
    }


}
