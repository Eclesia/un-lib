
package science.unlicense.physics.api.constraint;

import science.unlicense.math.api.Affine;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.math.impl.Matrices;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;

/**
 * Force target node to look at given node.
 *
 * @author Johann Sorel
 */
public class FocusConstraint implements Constraint{

    private final SceneNode node;
    private SceneNode target;

    public FocusConstraint(SceneNode node, SceneNode target) {
        this.node = node;
        this.target = target;
    }

    public SceneNode getTarget() {
        return target;
    }

    public void setTarget(SceneNode target) {
        this.target = target;
    }

    public void apply() {
        if (target==null) return;

        final Affine rootToNode = node.getRootToNodeSpace();
        final Affine targetToRoot = target.getNodeToRootSpace();
        final Affine targetToNode = targetToRoot.multiply(rootToNode);

        final VectorRW t = new Vector3f64(0,0,0);
        targetToNode.transform(t, t);
        final VectorRW targetPosition = t.getXYZ();

        if (targetPosition.length()<=0){
            //node overlaps
            return;
        }

        final VectorRW up = new Vector3f64(0, 1, 0);
        targetToNode.transform(up, up);
        up.localNormalize();

        final Matrix4x4 newTransform = new Matrix4x4(Matrices.lookAt(new Vector3f64(0, 0, 0), targetPosition, up.getXYZ(), null));

//        node.setRootToNodeSpace(newTransform);
        node.getNodeTransform().getRotation().set(newTransform.getMatrix(0, 2, 0, 2));
        node.getNodeTransform().notifyChanged();
    }

}
