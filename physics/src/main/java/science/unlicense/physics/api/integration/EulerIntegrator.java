
package science.unlicense.physics.api.integration;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.WorldUtilities;
import science.unlicense.physics.api.body.Body;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * Rotation/torque from : http://www.enchantedage.com/node/68
 * Thanks to Jon Watte for the public domain resources.
 *
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class EulerIntegrator extends AbstractIntegrator {

    //linear buffers
    private final VectorRW translation;
    private final VectorRW acceleration;
    //angular buffers
    private final VectorRW rotation;
    private final VectorRW scaledTorque;

    public EulerIntegrator(int worldDimension) {
        this.translation = VectorNf64.createDouble(worldDimension);
        this.acceleration = VectorNf64.createDouble(worldDimension);
        this.rotation = VectorNf64.createDouble(worldDimension);
        this.scaledTorque = VectorNf64.createDouble(worldDimension);
    }

    public void update(final World world, final double timeellapsed){
        WorldUtilities.clearBodyForces(world);
        WorldUtilities.applyForces(world);

        if (collisionSolver!=null){
            collisionSolver.update(world, timeellapsed);
        }

        final Sequence bodies = world.getBodies();
        for (int i=0,n=bodies.getSize(); i<n; i++){
            final Body body = (Body) bodies.get(i);

            if (body instanceof RigidBody){
                final RigidBody rb = (RigidBody) body;
                if (rb.isFixed()) continue;

                final double mass = rb.getMass();

                //linear integration
                final VectorRW linearVelocity = rb.getMotion().getVelocity();
                final VectorRW force = rb.getMotion().getForce();

                if (mass<=0){
                    //negative check just in case
                    acceleration.setAll(0.0);
                } else {
                    force.scale(1.0/mass, acceleration);
                }

                acceleration.localScale(timeellapsed);
                linearVelocity.localAdd(acceleration);
                linearVelocity.scale(timeellapsed,translation);
                rb.worldTranslate(translation);

                //angular integration
                final VectorRW angularVelocity = rb.getMotion().getAngularVelocity();
                final VectorRW torque = rb.getMotion().getTorque();

                if (mass<=0){
                    //negative check just in case
                    scaledTorque.setAll(0.0);
                } else {
                    torque.scale(1.0/mass, scaledTorque);
                }
                scaledTorque.localScale(timeellapsed);
                angularVelocity.localAdd(scaledTorque);
                angularVelocity.scale(timeellapsed,rotation);
                rb.worldRotate(rotation);

            }
        }
    }

}
