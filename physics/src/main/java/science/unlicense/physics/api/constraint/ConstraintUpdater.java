
package science.unlicense.physics.api.constraint;

import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.updater.Updater;

/**
 *
 * @author Johann Sorel
 */
public class ConstraintUpdater implements Updater {

    private final Constraint constraint;

    public ConstraintUpdater(Constraint constraint) {
        this.constraint = constraint;
    }

    public Constraint getConstraint() {
        return constraint;
    }

    public void update(DisplayTimerState context) {
        constraint.apply();
    }

}
