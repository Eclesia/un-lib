package science.unlicense.physics.api.integration;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.WorldUtilities;
import science.unlicense.physics.api.body.Body;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class ModifiedEulerIntegrator extends AbstractIntegrator{

    private final VectorRW translation;
    private final VectorRW scaledForce;
    private final VectorRW scaledForce2;

    public ModifiedEulerIntegrator(int worldDimension) {
        this.translation = VectorNf64.createDouble(worldDimension);
        this.scaledForce = VectorNf64.createDouble(worldDimension);
        this.scaledForce2 = VectorNf64.createDouble(worldDimension);
    }

    public void update(final World world, final double timeellapsed){
        WorldUtilities.clearBodyForces(world);
        WorldUtilities.applyForces(world);

        if (collisionSolver!=null){
            collisionSolver.update(world, timeellapsed);
        }

        final double halftt = 0.5f*timeellapsed*timeellapsed;

        final Sequence bodies = world.getBodies();
        for (int i=0,n=bodies.getSize(); i<n; i++){
            final Body body = (Body) bodies.get(i);

            if (body instanceof RigidBody){
                if (!((RigidBody) body).isFree()) continue;
            }

            if (body instanceof RigidBody){
                final RigidBody rb = (RigidBody) body;

                final VectorRW force = rb.getMotion().getForce();
                final VectorRW velocity = rb.getMotion().getVelocity();
                final double mass = rb.getMass();

                if (mass<=0){
                    //negative check just in case
                    scaledForce.setAll(0.0);
                } else {
                    force.scale(1.0/mass, scaledForce);
                }

                velocity.scale(timeellapsed, translation);
                scaledForce.scale(halftt,scaledForce2);
                translation.localAdd(scaledForce2);
                scaledForce.scale(timeellapsed,scaledForce2);
                velocity.localAdd(scaledForce2);

                rb.worldTranslate(translation);
            }
        }
    }

}
