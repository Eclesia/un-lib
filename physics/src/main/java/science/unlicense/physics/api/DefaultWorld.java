package science.unlicense.physics.api;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.physics.api.body.Body;
import science.unlicense.physics.api.force.Force;
import science.unlicense.physics.api.force.Singularity;

/**
 * Physic world, adapted from collada.
 * Reference : Collada 1.5.0 (Chapter 6: Physics Reference,p.228)
 *
 * @author Johann Sorel
 */
public class DefaultWorld extends AbstractWorld{

    protected final Sequence singularities = new ArraySequence();
    protected final Sequence bodies = new ArraySequence();
    protected final Sequence forces = new ArraySequence();

    public DefaultWorld(int dimension) {
        this(dimension,DEFAULT_DRAG);
    }

    public DefaultWorld(int dimension,double drag){
        super(dimension);
        setDrag(drag);
    }

    public Sequence getSingularities() {
        return singularities;
    }

    public void addSingularity(Singularity singularity) {
        singularities.add(singularity);
    }

    public void removeSingularity(Singularity singularity) {
        singularities.remove(singularity);
    }

    public Sequence getBodies() {
        return bodies;
    }

    public void addBody(Body body) {
        bodies.add(body);
    }

    public void removeBody(Body body) {
        bodies.remove(body);
    }

    public Sequence getForces() {
        return forces;
    }

    public void addForce(Force force) {
        forces.add(force);
    }

    public void removeForce(Force force) {
        forces.remove(force);
    }

    public void removeAll() {
        singularities.removeAll();
        bodies.removeAll();
        forces.removeAll();
    }

}
