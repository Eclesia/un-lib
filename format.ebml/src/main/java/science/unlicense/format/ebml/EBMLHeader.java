
package science.unlicense.format.ebml;

import science.unlicense.common.api.character.Chars;

/**
 * EBML header;
 *
 *  EBML := 1a45dfa3 container [ card:+; ] {
 *      EBMLVersion := 4286 uint [ def:1; ]
 *      EBMLReadVersion := 42f7 uint [ def:1; ]
 *      EBMLMaxIDLength := 42f2 uint [ def:4; ]
 *      EBMLMaxSizeLength := 42f3 uint [ def:8; ]
 *      DocType := 4282 string [ range:32..126; ]
 *      DocTypeVersion := 4287 uint [ def:1; ]
 *      DocTypeReadVersion := 4285 uint [ def:1; ]
 *  }
 *
 * @author Johann Sorel
 */
public class EBMLHeader extends EBMLChunk {

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x4286, TYPE_UINT, new Chars("EBMLVersion")),
        new PropertyType(0x42f7, TYPE_UINT, new Chars("EBMLReadVersion")),
        new PropertyType(0x42f2, TYPE_UINT, new Chars("EBMLMaxIDLength")),
        new PropertyType(0x42f3, TYPE_UINT, new Chars("EBMLMaxSizeLength")),
        new PropertyType(0x4282, TYPE_STRING, new Chars("DocType")),
        new PropertyType(0x4287, TYPE_UINT, new Chars("DocTypeVersion")),
        new PropertyType(0x4285, TYPE_UINT, new Chars("DocTypeReadVersion"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

}
