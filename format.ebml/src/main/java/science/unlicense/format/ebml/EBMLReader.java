
package science.unlicense.format.ebml;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.EOSException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;

/**
 * EBML reader.
 *
 * Specification :
 * http://www.matroska.org/technical/specs/rfc/index.html
 *
 * @author Johann Sorel
 */
public class EBMLReader extends AbstractReader {

    private final Dictionary types = new HashDictionary();

    private EBMLDecoder decoder = null;
    private EBMLChunk next = null;

    /**
     *
     * @param types Dictionary of knowned chunks : int -> class
     *          EBMLHeader is already present
     */
    public EBMLReader(Dictionary types) {
        if (types!=null) this.types.addAll(types);
        this.types.add(0x1a45dfa3, EBMLHeader.class);
    }

    public boolean hasNext() throws IOException {
        findNext();
        return next != null;
    }

    public EBMLChunk next() throws IOException{
        findNext();
        final EBMLChunk temp = next;
        next = null;
        return temp;
    }

    private void findNext() throws IOException{
        if (next!=null) return;

        if (decoder==null) {
            decoder = new EBMLDecoder();
            decoder.ds = getInputAsDataStream(Endianness.BIG_ENDIAN);
            decoder.types = types;
        }

        final int chunkid;
        try{
            chunkid = (int) decoder.peekId();
        }catch(EOSException ex) {
            //file finished
            return;
        }

        //consume id
        decoder.consumeId();
        //read size of the container.
        final long size = decoder.readVarInt();

        final EBMLChunk chunk = decoder.createChunk(chunkid);
        chunk.setId(chunkid);
        chunk.setSize(size);
        chunk.read(decoder);
        next = chunk;
    }

}
