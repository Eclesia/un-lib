package science.unlicense.format.ps.ps.vm.graphicstateindep;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Maths;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Set color to a rgb.
 *
 * Stack in|out :
 * red green blue | -
 *
 * @author Johann Sorel
 */
public class SetRGBColor extends PSFunction {

    public static final Chars NAME = Chars.constant("setrgbcolor");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        float b = ((Number) pullValue(vm)).floatValue();
        float g = ((Number) pullValue(vm)).floatValue();
        float r = ((Number) pullValue(vm)).floatValue();

        r = Maths.clamp(r, 0f, 1f);
        g = Maths.clamp(g, 0f, 1f);
        b = Maths.clamp(b, 0f, 1f);

        final GraphicState state = vm.getCurrentGraphicState();
        state.paint = new ColorPaint(new ColorRGB(r, g, b));
        vm.getPainter().setPaint(state.paint);
    }

}
