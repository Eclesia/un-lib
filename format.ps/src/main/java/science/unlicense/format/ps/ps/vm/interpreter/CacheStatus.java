package science.unlicense.format.ps.ps.vm.interpreter;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 *
 * @author Johann Sorel
 */
public class CacheStatus extends PSFunction {

    public static final Chars NAME = Chars.constant("cachestatus");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        throw new UnimplementedException("not supported yet");
    }

}
