package science.unlicense.format.ps.ps.vm.graphicstateindep;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Set line dashes.
 *
 * Stack in|out :
 * array offset | -
 *
 * @author Johann Sorel
 */
public class SetDash extends PSFunction {

    public static final Chars NAME = Chars.constant("setdash");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Number offset = (Number) pullValue(vm);
        final Object[] array = (Object[]) pullValue(vm);
        final float[] arrayf = new float[array.length];
        for (int i=0;i<array.length;i++){
            arrayf[i] = ((Number) array[i]).floatValue();
        }

        final GraphicState state = vm.getCurrentGraphicState();
        state.dashes = arrayf;
        state.lineOffset = offset.floatValue();
        state.restoreBrush(vm.getPainter());
    }

}
