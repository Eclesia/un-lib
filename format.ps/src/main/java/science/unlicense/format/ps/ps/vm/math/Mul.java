package science.unlicense.format.ps.ps.vm.math;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 *
 * @author Johann Sorel
 */
public class Mul extends PSFunction {

    public static final Chars NAME = Chars.constant("mul");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Double v1 = (Double) vm.pull();
        Double v2 = (Double) vm.pull();
        vm.push(v1 * v2);
    }

}
