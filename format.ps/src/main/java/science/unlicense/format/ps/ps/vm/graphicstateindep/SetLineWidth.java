package science.unlicense.format.ps.ps.vm.graphicstateindep;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Set stroke line width.
 *
 * Stack in|out :
 * width | -
 *
 * @author Johann Sorel
 */
public class SetLineWidth extends PSFunction {

    public static final Chars NAME = Chars.constant("setlinewidth");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        float width = ((Number) pullValue(vm)).floatValue();

        final GraphicState state = vm.getCurrentGraphicState();
        state.lineWidth = width;
        state.restoreBrush(vm.getPainter());

    }

}
