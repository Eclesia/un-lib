package science.unlicense.format.ps.ps.vm.dict;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Put current dictionary on the stack.
 *
 * Stack in|out :
 * - | dict
 *
 * @author Johann Sorel
 */
public class CurrentDict extends PSFunction {

    public static final Chars NAME = Chars.constant("currentdict");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        vm.push(vm.getCurrentDictionary());
    }

}
