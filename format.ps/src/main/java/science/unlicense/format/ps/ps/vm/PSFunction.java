package science.unlicense.format.ps.ps.vm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.code.vm.VMException;

/**
 *
 * @author Johann Sorel
 */
public abstract class PSFunction {

    public abstract Chars getName();

    public abstract void execute(PSVirtualMachine state) throws VMException;

    protected static Object pullValue(PSVirtualMachine vm) {
        Object candidate = vm.pull();
        if (candidate instanceof PSPointer) {
            return vm.resolve((PSPointer) candidate);
        }
        return candidate;
    }

}
