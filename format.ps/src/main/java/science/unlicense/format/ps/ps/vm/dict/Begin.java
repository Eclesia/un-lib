package science.unlicense.format.ps.ps.vm.dict;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Push dictionary on operand stack on dictionay stack.
 *
 * Stack in|out :
 * dict | -
 *
 * @author Johann Sorel
 */
public class Begin extends PSFunction {

    public static final Chars NAME = Chars.constant("begin");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Dictionary dict = (Dictionary) pullValue(vm);
        vm.getDictStack().add(dict);
    }

}
