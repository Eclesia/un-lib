package science.unlicense.format.ps.ps.vm.array;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Create a new array of given size.
 *
 * Stack in|out :
 * size | -
 *
 * @author Johann Sorel
 */
public class Array extends PSFunction {

    public static final Chars NAME = Chars.constant("array");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final int size = ((Number) pullValue(vm)).intValue();
        final Object[] array = new Object[size];
        vm.push(array);
    }

}
