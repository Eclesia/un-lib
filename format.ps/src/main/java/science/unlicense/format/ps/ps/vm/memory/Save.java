package science.unlicense.format.ps.ps.vm.memory;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Save a copy of the current vm state on the stack.
 *
 * Stack in|out :
 * - | save
 *
 * @author Johann Sorel
 */
public class Save extends PSFunction {

    public static final Chars NAME = Chars.constant("save");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO
    }

}
