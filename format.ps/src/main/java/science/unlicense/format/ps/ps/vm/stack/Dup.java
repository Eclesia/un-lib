package science.unlicense.format.ps.ps.vm.stack;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Duplicate the last element in the stack.
 *
 * Stack in|out :
 * o1 | o1 o1
 *
 * @author Johann Sorel
 */
public class Dup extends PSFunction {

    public static final Chars NAME = Chars.constant("dup");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Object o1 = vm.pull();
        vm.push(o1);
        vm.push(o1);
    }

}
