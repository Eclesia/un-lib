package science.unlicense.format.ps.ps.vm.memory;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Restore last saved state.
 *
 * @author Johann Sorel
 */
public class Restore extends PSFunction {

    public static final Chars NAME = Chars.constant("restore");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO
    }

}
