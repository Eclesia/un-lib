
package science.unlicense.format.ps.ps.vm;

import science.unlicense.common.api.Arrays;
import science.unlicense.image.api.color.Color;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.geometry.impl.Path;
import science.unlicense.math.impl.Affine2;

/**
 * State of the painter2d and active geometries.
 *
 * @author Johann Sorel
 */
public class GraphicState {

    /**
     * Different behavior of gsave/grestore if the state was saved with gsave or save.
     */
    public final boolean isSave;
    public Path currentGeometry;
    public Path clip;
    public float lineWidth;
    public float miterLimit;
    public float lineOffset;
    public int lineCap = PlainBrush.LINECAP_BUTT;
    public int lineJoin = PlainBrush.LINEJOIN_BEVEL;
    public float[] dashes;
    public Affine2 ctm = new Affine2();
    public Paint paint = new ColorPaint(Color.BLACK);
    public FontChoice font;

    public GraphicState(boolean isSave) {
        this.isSave = isSave;
    }

    public void reset(){
        currentGeometry = null;
        clip = null;
        lineWidth = 1;
        miterLimit = 0;
        lineOffset = 0;
        lineCap = PlainBrush.LINECAP_BUTT;
        lineJoin = PlainBrush.LINEJOIN_BEVEL;
        dashes = null;
        ctm = new Affine2();
        paint = new ColorPaint(Color.BLACK);
        font = null;
    }

    public void restore(Painter2D painter){
        restoreBrush(painter);
        painter.setTransform(ctm);
        painter.setPaint(paint);
        painter.setFont(font);
    }

    public void restoreBrush(Painter2D painter){
        painter.setBrush(new PlainBrush(lineWidth, lineCap, lineJoin, miterLimit, lineOffset, dashes));
    }

    public GraphicState copy(boolean isSave){
        final GraphicState state = new GraphicState(isSave);
        state.currentGeometry = currentGeometry!=null ? currentGeometry.copy() : null;
        state.clip = clip!=null ? clip.copy() : null;
        state.lineWidth = lineWidth;
        state.miterLimit = miterLimit;
        state.lineCap = lineCap;
        state.lineJoin = lineJoin;
        state.dashes = dashes!=null ? Arrays.copy(dashes) : null;
        state.ctm = new Affine2(ctm);
        state.paint = paint;
        state.font = font;
        return state;
    }

}
