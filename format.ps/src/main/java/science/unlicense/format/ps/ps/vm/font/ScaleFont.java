package science.unlicense.format.ps.ps.vm.font;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Scale font.
 *
 * Stack in|out :
 * font scale | font
 *
 * @author Johann Sorel
 */
public class ScaleFont extends PSFunction {

    public static final Chars NAME = Chars.constant("scalefont");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final float scale = ((Number) pullValue(vm)).floatValue();
        final FontChoice font = (FontChoice) pullValue(vm);
        vm.push(new FontChoice(font.getFamilies(), font.getSize()*scale, font.getWeight()));
    }

}
