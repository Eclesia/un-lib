package science.unlicense.format.ps.eps;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.format.ps.ps.PSConstants;

/**
 * Encapsulated PostScript format.
 *
 * Resource :
 * https://en.wikipedia.org/wiki/Encapsulated_PostScript
 *
 * @author Johann Sorel
 */
public class EPSFormat extends DefaultFormat {

    public EPSFormat() {
        super(new Chars("eps"));
        shortName = new Chars("Encapsulated PostScript");
        longName = new Chars("Encapsulated PostScript");
        mimeTypes.add(new Chars("application/postscript"));
        mimeTypes.add(new Chars("application/eps"));
        mimeTypes.add(new Chars("application/x-eps"));
        mimeTypes.add(new Chars("image/eps"));
        mimeTypes.add(new Chars("image/x-eps"));
        extensions.add(new Chars("ps"));
        extensions.add(new Chars("eps"));
        signatures.add(EPSConstants.WIN_SIGNATURE);
        signatures.add(PSConstants.SIGNATURE);
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
