package science.unlicense.format.ps.ps;

/**
 * Postscript constants.
 *
 * @author Johann Sorel
 */
public final class PSConstants {

    /**
     * Postscript file signature.
     */
    public static byte[] SIGNATURE = new byte[]{'%', '!'};

    private PSConstants() {
    }

}
