package science.unlicense.format.ps.ps.vm.font;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Set font.
 *
 * Stack in|out :
 * font | -
 *
 * @author Johann Sorel
 */
public class SetFont extends PSFunction {

    public static final Chars NAME = Chars.constant("setfont");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final FontChoice font = (FontChoice) pullValue(vm);
        final GraphicState state = vm.getCurrentGraphicState();
        state.font = font;
        vm.getPainter().setFont(font);
    }

}
