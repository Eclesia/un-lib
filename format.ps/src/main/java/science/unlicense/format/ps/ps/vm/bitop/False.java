package science.unlicense.format.ps.ps.vm.bitop;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Push a boolean false value on the vm stack.
 *
 * Stack in|out :
 * - | false
 *
 * @author Johann Sorel
 */
public class False extends PSFunction {

    public static final Chars NAME = Chars.constant("false");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        vm.push(Boolean.FALSE);
    }

}
