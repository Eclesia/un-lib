package science.unlicense.format.ps.ps.vm.stack;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Rol N elements in the stack.
 * spec page 665
 *
 * Stack in|out :
 * o1 ... oN n j | ...
 *
 * @author Johann Sorel
 */
public class Roll extends PSFunction {

    public static final Chars NAME = Chars.constant("roll");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final int j = ((Number) pullValue(vm)).intValue();
        final int n = ((Number) pullValue(vm)).intValue();

        final Sequence seq = new ArraySequence();
        for (int i=0;i<n;i++) seq.add(0,vm.pull());

        if (j>0){
            int last = seq.getSize()-1;
            for (int i=0;i<j;i++){
                final Object s = seq.get(last);
                seq.remove(last);
                seq.add(0,s);
            }
        } else {
            for (int i=0;i<j;i++){
                final Object s = seq.get(0);
                seq.remove(0);
                seq.add(s);
            }
        }

        for (int i=0;i<n;i++) vm.push(seq.get(i));

    }

}
