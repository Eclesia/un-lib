package science.unlicense.format.ps.ps.vm.string;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Create a new string of given length.
 *
 * Stack in|out :
 * size | string
 *
 * @author Johann Sorel
 */
public class String extends PSFunction {

    public static final Chars NAME = Chars.constant("string");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final int size = ((Number) pullValue(vm)).intValue();

        vm.push(new Chars(new byte[size],CharEncodings.UTF_8));

    }

}
