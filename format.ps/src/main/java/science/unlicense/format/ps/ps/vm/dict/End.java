package science.unlicense.format.ps.ps.vm.dict;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Remove dictionary from dictionary stack.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class End extends PSFunction {

    public static final Chars NAME = Chars.constant("end");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Object dict = vm.getDictStack().pickEnd();
        if (dict==vm.getUserDictionary()){
            throw new VMException("dictstackunderflow : Unallowed to pull user dict.");
        }
    }

}
