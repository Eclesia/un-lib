package science.unlicense.format.ps.ps.vm.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Close current path.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class ClosePath extends PSFunction {

    public static final Chars NAME = Chars.constant("closepath");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        vm.getCurrentGraphicState().currentGeometry.appendClose();
    }

}
