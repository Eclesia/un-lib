
package science.unlicense.format.ps.ps.vm;

import science.unlicense.common.api.collection.ArrayStack;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;

/**
 * Stores the VM state.
 *
 * @author Johann Sorel
 */
public class VMState {

    final science.unlicense.common.api.collection.Stack operandStack = new ArrayStack();
    final ArrayStack dictStack = new ArrayStack();
    final science.unlicense.common.api.collection.Stack execStack = new ArrayStack();
    final science.unlicense.common.api.collection.Stack graphicStack = new ArrayStack();
    final Dictionary heap = new HashDictionary();

    final Dictionary userDict = new HashDictionary();
    final Dictionary globalDict = new HashDictionary();
    final Dictionary statusDict = new HashDictionary();

    public void restore(VMState state){

    }

    public VMState copy(){
        final VMState state = new VMState();
        return state;
    }

}
