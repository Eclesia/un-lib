package science.unlicense.format.ps.ps.vm.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.impl.Path;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Start a new path.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class NewPath extends PSFunction {

    public static final Chars NAME = Chars.constant("newpath");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        vm.getCurrentGraphicState().currentGeometry = new Path();
    }

}
