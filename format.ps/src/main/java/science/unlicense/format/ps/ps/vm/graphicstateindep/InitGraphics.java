package science.unlicense.format.ps.ps.vm.graphicstateindep;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Reset graphics state.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class InitGraphics extends PSFunction {

    public static final Chars NAME = Chars.constant("initgraphics");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final GraphicState state = vm.getCurrentGraphicState();
        state.reset();
    }

}
