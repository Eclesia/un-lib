package science.unlicense.format.ps.ps.vm.array;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Restore in an array to the stack.
 *
 * Stack in|out :
 * array | o1 ... o2 array
 *
 * @author Johann Sorel
 */
public class ALoad extends PSFunction {

    public static final Chars NAME = Chars.constant("aload");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final Object[] array = (Object[]) pullValue(vm);
        for (int i=0;i<array.length;i++){
            vm.push(array[i]);
        }
    }

}
