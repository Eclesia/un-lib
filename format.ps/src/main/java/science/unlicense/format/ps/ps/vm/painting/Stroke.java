package science.unlicense.format.ps.ps.vm.painting;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSCall;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;
import science.unlicense.format.ps.ps.vm.path.NewPath;

/**
 * Stroke current path.
 * spec page 714
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class Stroke extends PSFunction {

    public static final Chars NAME = Chars.constant("stroke");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final GraphicState gs = vm.getCurrentGraphicState();
        vm.getPainter().stroke(gs.currentGeometry);

        //clear the path, strange it's the bahavior defined in the spec
        vm.push(new PSCall(NewPath.NAME));
    }

}
