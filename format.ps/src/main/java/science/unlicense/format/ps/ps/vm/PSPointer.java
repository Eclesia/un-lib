package science.unlicense.format.ps.ps.vm;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class PSPointer {

    public final Chars name;

    public PSPointer(Chars name) {
        this.name = name;
    }

}
