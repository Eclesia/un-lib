package science.unlicense.format.ps.ps.vm.font;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Set;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSPointer;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Find font for given name.
 *
 * Stack in|out :
 * key | font
 *
 * @author Johann Sorel
 */
public class FindFont extends PSFunction {

    public static final Chars NAME = Chars.constant("findfont");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final PSPointer pointer = (PSPointer) vm.pull();
        Chars family = pointer.name;
        final Set fonts = vm.getPainter().getFontStore().getFamilies();
        if (!fonts.contains(family)){
            //fallback on first font
            family = (Chars) fonts.createIterator().next();
        }
        final FontChoice font = new FontChoice(new Chars[]{family}, 1, FontChoice.WEIGHT_NONE);
        vm.push(font);
    }

}
