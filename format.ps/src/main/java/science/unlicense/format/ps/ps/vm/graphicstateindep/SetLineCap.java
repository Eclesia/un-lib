package science.unlicense.format.ps.ps.vm.graphicstateindep;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Set line cap.
 *
 * Stack in|out :
 * int | -
 *
 * @author Johann Sorel
 */
public class SetLineCap extends PSFunction {

    public static final Chars NAME = Chars.constant("setlinecap");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final int val = ((Number) pullValue(vm)).intValue();

        final GraphicState state = vm.getCurrentGraphicState();
        state.lineCap = (val==0) ? PlainBrush.LINECAP_BUTT : (val==1) ? PlainBrush.LINECAP_ROUND : PlainBrush.LINECAP_SQUARE;
        state.restoreBrush(vm.getPainter());
    }

}
