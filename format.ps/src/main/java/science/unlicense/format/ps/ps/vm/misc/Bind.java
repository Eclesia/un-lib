package science.unlicense.format.ps.ps.vm.misc;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Replace operators in a procedure.
 *
 * Stack in|out :
 * proc | proc
 *
 * @author Johann Sorel
 */
public class Bind extends PSFunction {

    public static final Chars NAME = Chars.constant("bind");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO this is some optimisation function
    }

}
