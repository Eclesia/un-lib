package science.unlicense.format.ps.ps.vm.matrix;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.Affine2;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Translate ctm or given matrix.
 *
 * Stack in|out :
 * tx ty | -
 * tx ty matrix | matrix
 *
 * @author Johann Sorel
 */
public class Translate extends PSFunction {

    public static final Chars NAME = Chars.constant("translate");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Object obj3 = pullValue(vm);
        Object obj2 = pullValue(vm);

        if (obj3 instanceof Matrix){
            Object obj1 = pullValue(vm);
            final double ty = ((Number) obj2).doubleValue();
            final double tx = ((Number) obj1).doubleValue();
            final Affine2 t = new Affine2();
            t.set(0, 3, tx);
            t.set(1, 3, ty);
            Affine2 m = (Affine2) obj3;
            t.localMultiply(m);
            vm.push(t);

        } else {
            final double ty = ((Number) obj3).doubleValue();
            final double tx = ((Number) obj2).doubleValue();
            final Affine2 t = new Affine2();
            t.set(0, 3, tx);
            t.set(1, 3, ty);

            final GraphicState state = vm.getCurrentGraphicState();
            t.localMultiply(state.ctm);
            state.ctm = t;
            vm.getPainter().setTransform(state.ctm);
        }

    }

}
