package science.unlicense.format.ps.ps.vm.graphicstateindep;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Maths;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Set color to a gray variation.
 * 0 = black
 * 1 = white
 *
 * Stack in|out :
 * num | -
 *
 * @author Johann Sorel
 */
public class SetGray extends PSFunction {

    public static final Chars NAME = Chars.constant("setgray");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        float g = ((Number) pullValue(vm)).floatValue();
        g = Maths.clamp(g, 0f, 1f);

        final GraphicState state = vm.getCurrentGraphicState();
        state.paint = new ColorPaint(new ColorRGB(g, g, g));
        vm.getPainter().setPaint(state.paint);
    }

}
