package science.unlicense.format.ps.ps.vm.matrix;

import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Concatenate given matrix with ctm.
 *
 * Stack in|out :
 * matrix | -
 *
 * @author Johann Sorel
 */
public class Concat extends PSFunction {

    public static final Chars NAME = Chars.constant("concat");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO
//        final Matrix4x4 mat = (Matrix4x4) pullValue(vm);
//        final GraphicState state = vm.getCurrentGraphicState();
//        final Matrix4x4 res = (Matrix4x4) mat.multiply(state.ctm);
//        state.ctm = res;
//        vm.getPainter().setTransform(res.copy());
    }

}
