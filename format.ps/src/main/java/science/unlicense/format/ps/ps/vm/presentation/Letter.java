package science.unlicense.format.ps.ps.vm.presentation;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.color.Color;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Configures the page to letter format.
 * spec page 824
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class Letter extends PSFunction {

    public static final Chars NAME = Chars.constant("letter");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO : event ? draw a back border of format size ?
        Painter2D painter = vm.getPainter();
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
        final Rectangle rect = new Rectangle(0, 0, 8.5*72, 11*72);
        painter.setPaint(new ColorPaint(Color.WHITE));
        painter.fill(rect);
        painter.setPaint(new ColorPaint(Color.BLACK));
        painter.stroke(rect);
        vm.getCurrentGraphicState().restore(painter);
    }

}
