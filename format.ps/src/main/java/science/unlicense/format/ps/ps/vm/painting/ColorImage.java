package science.unlicense.format.ps.ps.vm.painting;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.MatrixNxN;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Paint an image.
 *
 * Stack in|out :
 * width height bits/comp matrix datasrc0 ... datasrc ncomp−1 multi ncomp | -
 *
 * @author Johann Sorel
 */
public class ColorImage extends PSFunction {

    public static final Chars NAME = Chars.constant("colorimage");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final int ncomp = ((Number) pullValue(vm)).intValue();
        final boolean multi = (Boolean) pullValue(vm);
        final Object[] sources;
        if (multi){
            sources = new Object[ncomp];
            for (int i=0;i<sources.length;i++){
                sources[i] = vm.pull();
            }
        } else {
            sources = new Object[1];
            sources[0] = vm.pull();
        }
        final MatrixNxN trs = (MatrixNxN) pullValue(vm);
        final int nbBits = ((Number) pullValue(vm)).intValue();
        final int height = ((Number) pullValue(vm)).intValue();
        final int width = ((Number) pullValue(vm)).intValue();

        //TODO
    }

}
