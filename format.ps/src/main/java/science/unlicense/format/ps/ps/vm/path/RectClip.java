package science.unlicense.format.ps.ps.vm.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.impl.Path;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Define a rectangle clip area.
 *
 * Stack in|out :
 * x y width height | -
 * numarray | -
 * numstring | -
 *
 * @author Johann Sorel
 */
public class RectClip extends PSFunction {

    public static final Chars NAME = Chars.constant("rectclip");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO support other input types
        final double height = ((Number) pullValue(vm)).doubleValue();
        final double width = ((Number) pullValue(vm)).doubleValue();
        final double y = ((Number) pullValue(vm)).doubleValue();
        final double x = ((Number) pullValue(vm)).doubleValue();
        final GraphicState state = vm.getCurrentGraphicState();
        state.clip = new Path();
        state.clip.appendMoveTo(x, y);
        state.clip.appendLineTo(x+width, y);
        state.clip.appendLineTo(x+width, y+height);
        state.clip.appendLineTo(x, y+height);
        state.clip.appendClose();
        vm.getPainter().setClip(state.clip);
    }

}
