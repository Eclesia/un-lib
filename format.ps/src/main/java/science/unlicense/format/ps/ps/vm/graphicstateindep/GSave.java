package science.unlicense.format.ps.ps.vm.graphicstateindep;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Save a copy of the current graphic state in the graphic stack.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class GSave extends PSFunction {

    public static final Chars NAME = Chars.constant("gsave");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Painter2D painter = vm.getPainter();

        final GraphicState state = vm.getCurrentGraphicState();
        //save a copy of the state
        final GraphicState copy = state.copy(false);
        vm.getGraphicStack().add(copy);
        //reset the clipping path to empty
        state.clip = null;
        painter.setClip(null);

    }

}
