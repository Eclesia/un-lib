package science.unlicense.format.ps.ps;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.format.ps.ps.vm.PSCall;
import science.unlicense.format.ps.ps.vm.PSPointer;
import science.unlicense.format.ps.ps.vm.PSProcedure;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenType;
import science.unlicense.syntax.api.parser.NFATokenState;
import science.unlicense.syntax.api.parser.Parser;
import science.unlicense.syntax.api.parser.ParserStream;
import science.unlicense.syntax.api.parser.Rule;
import science.unlicense.syntax.api.parser.SyntaxNode;
import science.unlicense.syntax.impl.grammar.io.UNGrammarReader;

/**
 *
 * @author Johann Sorel
 */
public class PSReader extends AbstractReader {

    private static OrderedHashDictionary TOKENS = new OrderedHashDictionary();
    private static final TokenType TOKEN_NUMBER;
    private static final TokenType TOKEN_WORD;
    private static final TokenType TOKEN_STRING;
    private static final TokenType TOKEN_VARIABLE;
    private static final TokenType TOKEN_PROC_START;
    private static final TokenType TOKEN_PROC_END;
    private static final TokenType TOKEN_ARRAY_START;
    private static final TokenType TOKEN_ARRAY_END;
    private static final TokenType TOKEN_COMMENT;
    private static final TokenType TOKEN_WS;

    private static OrderedHashDictionary RULES= new OrderedHashDictionary();
    private static final Rule FILE_RULE;
    private static final TokenType[] TO_EXCLUDE;
    private static final Predicate EXCLUDE_PREDICATE;

    static {
        try{
            final UNGrammarReader reader = new UNGrammarReader();
            reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/format/ps/postscript.gr")));
            reader.read(TOKENS, RULES);
            final Dictionary defaults = (Dictionary) TOKENS.getValue(new Chars("DEFAULT"));
            FILE_RULE = (Rule) RULES.getValue(new Chars("file"));
            TOKEN_NUMBER = (TokenType) defaults.getValue(new Chars("NUMBER"));
            TOKEN_WORD = (TokenType) defaults.getValue(new Chars("WORD"));
            TOKEN_STRING = (TokenType) defaults.getValue(new Chars("STRING"));
            TOKEN_VARIABLE = (TokenType) defaults.getValue(new Chars("VARIABLE"));
            TOKEN_PROC_START = (TokenType) defaults.getValue(new Chars("PROC_START"));
            TOKEN_PROC_END = (TokenType) defaults.getValue(new Chars("PROC_END"));
            TOKEN_ARRAY_START = (TokenType) defaults.getValue(new Chars("ARRAY_START"));
            TOKEN_ARRAY_END = (TokenType) defaults.getValue(new Chars("ARRAY_END"));
            TOKEN_COMMENT = (TokenType) defaults.getValue(new Chars("COMMENT"));
            TOKEN_WS = (TokenType) defaults.getValue(new Chars("WS"));

            TO_EXCLUDE = new TokenType[]{
                (TokenType) defaults.getValue(new Chars("WS")),
                (TokenType) defaults.getValue(new Chars("COMMENT"))
            };
            EXCLUDE_PREDICATE = new Predicate() {
                public Boolean evaluate(Object candidate) {
                    return candidate instanceof NFATokenState &&
                            Arrays.containsIdentity(TO_EXCLUDE, 0, TO_EXCLUDE.length, ((NFATokenState) candidate).getTokenType());
                }
            };
        }catch(IOException ex){
            throw new RuntimeException("Fail to initialize Postscript reader : "+ex.getMessage(),ex);
        }
    }

    private Lexer lexer;
    private ParserStream parser;

    private void init() throws IOException{
        if (parser==null){
            final BacktrackInputStream in = getInputAsBacktrackStream();

            //check for the windows signature used in EPS file
            in.mark();
            int c = in.read();
            if (c!= '%'){
                //windows header skip it
                final DataInputStream ds = new DataInputStream(in);
                ds.skipFully(3); //end of header
                ds.skipFully(6*4); //6 intergers
                ds.skipFully(2); //16bit checksum
                in.clearMark();
            } else {
                in.rewind();
            }

            //prepare lexer
            lexer = new Lexer(CharEncodings.US_ASCII);
            lexer.setInput(in);
            parser = new ParserStream(lexer, FILE_RULE, EXCLUDE_PREDICATE);
        }
    }

    public SyntaxNode readAsAST() throws IOException{
        final Parser parser = new Parser(FILE_RULE);
        parser.setInput(getInput());
        return parser.parse();
    }

    /**
     * Possible types are :
     * - Integer
     * - Double
     * - Chars
     * - PSProcedure
     * - PSPointer
     * - PSCall
     * - Object[]
     * - Dictionary
     * - byte[]
     *
     * @return
     * @throws IOException
     */
    public Object next() throws IOException{
        init();

        while (true){
            Token token = lexer.next();
            if (token==null) return null;

            Object obj = readSimple(token);
            if (obj!=null) return obj;
        }
    }

    private PSProcedure readProcedure() throws IOException{
        final PSProcedure proc = new PSProcedure();

        while (true){
            final Token token = lexer.next();
            final TokenType tokenType = token.type;

            if (tokenType==TOKEN_PROC_END){
                return proc;
            } else {
                Object obj = readSimple(token);
                if (obj!=null) proc.add(obj);
            }
        }
    }

    private Object[] readArray() throws IOException{
        final Sequence array = new ArraySequence();

        while (true){
            final Token token = lexer.next();
            final TokenType tokenType = token.type;

            if (tokenType==TOKEN_ARRAY_END){
                return array.toArray();
            } else {
                Object obj = readSimple(token);
                if (obj!=null) array.add(obj);
            }
        }
    }

    private Object readSimple(Token token) throws IOException{
        final TokenType tokenType = token.type;
        if (tokenType==TOKEN_NUMBER){
            return Float64.decode(token.value);
        } else if (tokenType==TOKEN_VARIABLE){
            return new PSPointer(token.value.truncate(1, token.value.getCharLength()));
        } else if (tokenType==TOKEN_WORD){
            return new PSCall(token.value);
        } else if (tokenType==TOKEN_STRING){
            return token.value.truncate(1, token.value.getCharLength()-1);
        } else if (tokenType==TOKEN_PROC_START){
            return readProcedure();
        } else if (tokenType==TOKEN_ARRAY_START){
            return readArray();
        } else if (tokenType==TOKEN_COMMENT){
            return null;
        } else if (tokenType==TOKEN_WS){
            return null;
        } else {
            throw new IOException(null, "Unexpected token "+tokenType);
        }
    }

}
