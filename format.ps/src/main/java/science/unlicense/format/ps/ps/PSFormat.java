package science.unlicense.format.ps.ps;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * PostScript format.
 *
 * Resource :
 * http://en.wikipedia.org/wiki/PostScript
 * http://www.adobe.com/products/postscript/pdfs/PLRM.pdf
 * http://partners.adobe.com/public/developer/en/ps/PLRM.pdf
 * http://www-cdf.fnal.gov/offline/PostScript/BLUEBOOK.PDF
 *
 * @author Johann Sorel
 */
public class PSFormat extends DefaultFormat {

    public PSFormat() {
        super(new Chars("ps"));
        shortName = new Chars("PostScript");
        longName = new Chars("PostScript");
        mimeTypes.add(new Chars("application/postscript"));
        extensions.add(new Chars("ps"));
        extensions.add(new Chars("eps"));
        signatures.add(PSConstants.SIGNATURE);
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
