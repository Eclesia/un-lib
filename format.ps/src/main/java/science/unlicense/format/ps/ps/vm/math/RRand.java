package science.unlicense.format.ps.ps.vm.math;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 *
 * @author Johann Sorel
 */
public class RRand extends PSFunction {

    public static final Chars NAME = Chars.constant("rrand");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        throw new UnimplementedException("Not supported yet");
    }

}
