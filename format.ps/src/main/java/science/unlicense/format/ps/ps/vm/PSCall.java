package science.unlicense.format.ps.ps.vm;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class PSCall extends CObject{

    public final Chars functionName;

    public PSCall(Chars functionName) {
        this.functionName = functionName;
    }

    public Chars toChars() {
        return new Chars("PSCall:").concat(functionName);
    }

}
