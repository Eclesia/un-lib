package science.unlicense.format.ps.ps.vm.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.impl.Path;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Add a new point in current path.
 *
 * Stack in|out :
 * x y | -
 *
 * @author Johann Sorel
 */
public class MoveTo extends PSFunction {

    public static final Chars NAME = Chars.constant("moveto");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Number y = (Number) pullValue(vm);
        final Number x = (Number) pullValue(vm);
        final GraphicState state = vm.getCurrentGraphicState();
        if (state.currentGeometry==null){
            state.currentGeometry = new Path();
        }
        state.currentGeometry.appendMoveTo(x.doubleValue(), y.doubleValue());
    }

}
