package science.unlicense.format.ps.ps.vm.dict;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Create a dictionary and push it on the operand stack.
 *
 * Stack in|out :
 * int | dict
 *
 * @author Johann Sorel
 */
public class Dict extends PSFunction {

    public static final Chars NAME = Chars.constant("dict");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final Number num = (Number) pullValue(vm);
        final Dictionary dict = new HashDictionary(num.intValue());
        vm.push(dict);
    }

}
