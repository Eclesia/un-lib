
package science.unlicense.format.ps.eps;

/**
 *
 * @author Johann Sorel
 */
public final class EPSConstants {

    public static final byte[] WIN_SIGNATURE = new byte[]{(byte) 0xC5,(byte) 0xD0,(byte) 0xD3,(byte) 0xC6};

    private EPSConstants(){}

}
