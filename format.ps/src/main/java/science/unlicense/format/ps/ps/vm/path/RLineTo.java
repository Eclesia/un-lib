package science.unlicense.format.ps.ps.vm.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.impl.path.PathStep2D;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Add a new point in current path.
 * Coordinates are relative to the last point.
 *
 * Stack in|out :
 * x y | -
 *
 * @author Johann Sorel
 */
public class RLineTo extends PSFunction {

    public static final Chars NAME = Chars.constant("rlineto");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Number y = (Number) pullValue(vm);
        final Number x = (Number) pullValue(vm);
        final GraphicState state = vm.getCurrentGraphicState();
        final Sequence steps = state.currentGeometry.getSteps();
        final PathStep2D lastStep = (PathStep2D) steps.get(steps.getSize()-1);
        state.currentGeometry.appendLineTo(lastStep.values[0] + x.doubleValue(), lastStep.values[1] + y.doubleValue());
    }

}
