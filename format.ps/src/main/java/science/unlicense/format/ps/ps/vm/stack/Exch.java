package science.unlicense.format.ps.ps.vm.stack;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Exchange the last two element in the stack.
 *
 * Stack in|out :
 * o1 o2 | o2 o1
 *
 * @author Johann Sorel
 */
public class Exch extends PSFunction {

    public static final Chars NAME = Chars.constant("exch");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Object o2 = vm.pull();
        Object o1 = vm.pull();
        vm.push(o2);
        vm.push(o1);
    }

}
