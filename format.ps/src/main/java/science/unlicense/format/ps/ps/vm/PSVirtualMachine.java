package science.unlicense.format.ps.ps.vm;

import science.unlicense.common.api.collection.ArrayStack;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.image.api.color.Color;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.code.vm.VMException;
import science.unlicense.format.ps.ps.vm.array.ALoad;
import science.unlicense.format.ps.ps.vm.array.AStore;
import science.unlicense.format.ps.ps.vm.array.Array;
import science.unlicense.format.ps.ps.vm.array.Copy;
import science.unlicense.format.ps.ps.vm.array.EndArray;
import science.unlicense.format.ps.ps.vm.array.ForAll;
import science.unlicense.format.ps.ps.vm.array.Get;
import science.unlicense.format.ps.ps.vm.array.GetInterval;
import science.unlicense.format.ps.ps.vm.array.Length;
import science.unlicense.format.ps.ps.vm.array.Put;
import science.unlicense.format.ps.ps.vm.array.PutInterval;
import science.unlicense.format.ps.ps.vm.array.StartArray;
import science.unlicense.format.ps.ps.vm.bitop.And;
import science.unlicense.format.ps.ps.vm.bitop.BitShift;
import science.unlicense.format.ps.ps.vm.bitop.Eq;
import science.unlicense.format.ps.ps.vm.bitop.False;
import science.unlicense.format.ps.ps.vm.bitop.Ge;
import science.unlicense.format.ps.ps.vm.bitop.Gt;
import science.unlicense.format.ps.ps.vm.bitop.Le;
import science.unlicense.format.ps.ps.vm.bitop.Lt;
import science.unlicense.format.ps.ps.vm.bitop.Ne;
import science.unlicense.format.ps.ps.vm.bitop.Not;
import science.unlicense.format.ps.ps.vm.bitop.Or;
import science.unlicense.format.ps.ps.vm.bitop.True;
import science.unlicense.format.ps.ps.vm.bitop.Xor;
import science.unlicense.format.ps.ps.vm.control.CountExecStack;
import science.unlicense.format.ps.ps.vm.control.Exec;
import science.unlicense.format.ps.ps.vm.control.ExecStack;
import science.unlicense.format.ps.ps.vm.control.Exit;
import science.unlicense.format.ps.ps.vm.control.For;
import science.unlicense.format.ps.ps.vm.control.If;
import science.unlicense.format.ps.ps.vm.control.IfElse;
import science.unlicense.format.ps.ps.vm.control.Loop;
import science.unlicense.format.ps.ps.vm.control.Quit;
import science.unlicense.format.ps.ps.vm.control.Repeat;
import science.unlicense.format.ps.ps.vm.control.Start;
import science.unlicense.format.ps.ps.vm.control.Stop;
import science.unlicense.format.ps.ps.vm.control.Stopped;
import science.unlicense.format.ps.ps.vm.dict.Begin;
import science.unlicense.format.ps.ps.vm.dict.ClearDictStack;
import science.unlicense.format.ps.ps.vm.dict.CountDictStack;
import science.unlicense.format.ps.ps.vm.dict.CurrentDict;
import science.unlicense.format.ps.ps.vm.dict.Def;
import science.unlicense.format.ps.ps.vm.dict.Dict;
import science.unlicense.format.ps.ps.vm.dict.DictStack;
import science.unlicense.format.ps.ps.vm.dict.End;
import science.unlicense.format.ps.ps.vm.dict.ErrorDict;
import science.unlicense.format.ps.ps.vm.dict.GlobalDict;
import science.unlicense.format.ps.ps.vm.dict.Known;
import science.unlicense.format.ps.ps.vm.dict.Load;
import science.unlicense.format.ps.ps.vm.dict.MaxLength;
import science.unlicense.format.ps.ps.vm.dict.MaxMax;
import science.unlicense.format.ps.ps.vm.dict.MinMin;
import science.unlicense.format.ps.ps.vm.dict.StatusDict;
import science.unlicense.format.ps.ps.vm.dict.Store;
import science.unlicense.format.ps.ps.vm.dict.SystemDict;
import science.unlicense.format.ps.ps.vm.dict.Undef;
import science.unlicense.format.ps.ps.vm.dict.UserDict;
import science.unlicense.format.ps.ps.vm.dict.Where;
import science.unlicense.format.ps.ps.vm.file.BytesAvailable;
import science.unlicense.format.ps.ps.vm.file.CloseFile;
import science.unlicense.format.ps.ps.vm.file.CurrentFile;
import science.unlicense.format.ps.ps.vm.file.CurrentObjectFormat;
import science.unlicense.format.ps.ps.vm.file.DeleteFile;
import science.unlicense.format.ps.ps.vm.file.Equal;
import science.unlicense.format.ps.ps.vm.file.EqualEqual;
import science.unlicense.format.ps.ps.vm.file.File;
import science.unlicense.format.ps.ps.vm.file.FileNameForAll;
import science.unlicense.format.ps.ps.vm.file.FilePosition;
import science.unlicense.format.ps.ps.vm.file.Filter;
import science.unlicense.format.ps.ps.vm.file.Flush;
import science.unlicense.format.ps.ps.vm.file.FlushFile;
import science.unlicense.format.ps.ps.vm.file.PStack;
import science.unlicense.format.ps.ps.vm.file.Print;
import science.unlicense.format.ps.ps.vm.file.PrintObject;
import science.unlicense.format.ps.ps.vm.file.Read;
import science.unlicense.format.ps.ps.vm.file.ReadHexString;
import science.unlicense.format.ps.ps.vm.file.ReadLine;
import science.unlicense.format.ps.ps.vm.file.ReadString;
import science.unlicense.format.ps.ps.vm.file.RenameFile;
import science.unlicense.format.ps.ps.vm.file.ResetFile;
import science.unlicense.format.ps.ps.vm.file.Run;
import science.unlicense.format.ps.ps.vm.file.SetFilePosition;
import science.unlicense.format.ps.ps.vm.file.SetObjectFormat;
import science.unlicense.format.ps.ps.vm.file.Stack;
import science.unlicense.format.ps.ps.vm.file.Status;
import science.unlicense.format.ps.ps.vm.file.Write;
import science.unlicense.format.ps.ps.vm.file.WriteHexString;
import science.unlicense.format.ps.ps.vm.file.WriteObject;
import science.unlicense.format.ps.ps.vm.file.WriteString;
import science.unlicense.format.ps.ps.vm.graphicstatedep.CurrentBlackGeneration;
import science.unlicense.format.ps.ps.vm.graphicstatedep.CurrentColorRendering;
import science.unlicense.format.ps.ps.vm.graphicstatedep.CurrentColorScreen;
import science.unlicense.format.ps.ps.vm.graphicstatedep.CurrentColorTransfer;
import science.unlicense.format.ps.ps.vm.graphicstatedep.CurrentFlat;
import science.unlicense.format.ps.ps.vm.graphicstatedep.CurrentHalfTone;
import science.unlicense.format.ps.ps.vm.graphicstatedep.CurrentOverPrint;
import science.unlicense.format.ps.ps.vm.graphicstatedep.CurrentScreen;
import science.unlicense.format.ps.ps.vm.graphicstatedep.CurrentSmoothness;
import science.unlicense.format.ps.ps.vm.graphicstatedep.CurrentTransfer;
import science.unlicense.format.ps.ps.vm.graphicstatedep.CurrentUnderColorRemoval;
import science.unlicense.format.ps.ps.vm.graphicstatedep.SetBlackGeneration;
import science.unlicense.format.ps.ps.vm.graphicstatedep.SetColorRendering;
import science.unlicense.format.ps.ps.vm.graphicstatedep.SetColorScreen;
import science.unlicense.format.ps.ps.vm.graphicstatedep.SetColorTransfer;
import science.unlicense.format.ps.ps.vm.graphicstatedep.SetFlat;
import science.unlicense.format.ps.ps.vm.graphicstatedep.SetHalfTone;
import science.unlicense.format.ps.ps.vm.graphicstatedep.SetOverPrint;
import science.unlicense.format.ps.ps.vm.graphicstatedep.SetScreen;
import science.unlicense.format.ps.ps.vm.graphicstatedep.SetSmoothness;
import science.unlicense.format.ps.ps.vm.graphicstatedep.SetTransfer;
import science.unlicense.format.ps.ps.vm.graphicstatedep.SetUnderColorRemoval;
import science.unlicense.format.ps.ps.vm.graphicstateindep.ClipRestore;
import science.unlicense.format.ps.ps.vm.graphicstateindep.ClipSave;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentCMYKColor;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentColor;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentColorspace;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentDash;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentGState;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentGray;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentHSBColor;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentLineCap;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentLineJoin;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentLineWidth;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentMiterLimit;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentRGBColor;
import science.unlicense.format.ps.ps.vm.graphicstateindep.CurrentStrokeAdjust;
import science.unlicense.format.ps.ps.vm.graphicstateindep.GRestore;
import science.unlicense.format.ps.ps.vm.graphicstateindep.GRestoreAll;
import science.unlicense.format.ps.ps.vm.graphicstateindep.GSave;
import science.unlicense.format.ps.ps.vm.graphicstateindep.GState;
import science.unlicense.format.ps.ps.vm.graphicstateindep.InitGraphics;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetCMYKColor;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetColor;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetColorspace;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetDash;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetGState;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetGray;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetHSBColor;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetLineCap;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetLineJoin;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetLineWidth;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetMiterLimit;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetRGBColor;
import science.unlicense.format.ps.ps.vm.graphicstateindep.SetStrokeAdjust;
import science.unlicense.format.ps.ps.vm.insideness.InEOFill;
import science.unlicense.format.ps.ps.vm.insideness.InFill;
import science.unlicense.format.ps.ps.vm.insideness.InStroke;
import science.unlicense.format.ps.ps.vm.insideness.InUEOFill;
import science.unlicense.format.ps.ps.vm.insideness.InUFill;
import science.unlicense.format.ps.ps.vm.insideness.InUStroke;
import science.unlicense.format.ps.ps.vm.math.Abs;
import science.unlicense.format.ps.ps.vm.math.Add;
import science.unlicense.format.ps.ps.vm.math.Atan;
import science.unlicense.format.ps.ps.vm.math.Ceiling;
import science.unlicense.format.ps.ps.vm.math.Cos;
import science.unlicense.format.ps.ps.vm.math.Div;
import science.unlicense.format.ps.ps.vm.math.Exp;
import science.unlicense.format.ps.ps.vm.math.Floor;
import science.unlicense.format.ps.ps.vm.math.IDiv;
import science.unlicense.format.ps.ps.vm.math.Ln;
import science.unlicense.format.ps.ps.vm.math.Log;
import science.unlicense.format.ps.ps.vm.math.Mod;
import science.unlicense.format.ps.ps.vm.math.Mul;
import science.unlicense.format.ps.ps.vm.math.Neg;
import science.unlicense.format.ps.ps.vm.math.RRand;
import science.unlicense.format.ps.ps.vm.math.Rand;
import science.unlicense.format.ps.ps.vm.math.Round;
import science.unlicense.format.ps.ps.vm.math.SRand;
import science.unlicense.format.ps.ps.vm.math.Sin;
import science.unlicense.format.ps.ps.vm.math.Sqrt;
import science.unlicense.format.ps.ps.vm.math.Sub;
import science.unlicense.format.ps.ps.vm.math.Truncate;
import science.unlicense.format.ps.ps.vm.matrix.Concat;
import science.unlicense.format.ps.ps.vm.matrix.ConcatMatrix;
import science.unlicense.format.ps.ps.vm.matrix.CurrentMatrix;
import science.unlicense.format.ps.ps.vm.matrix.DTransform;
import science.unlicense.format.ps.ps.vm.matrix.DefaultMatrix;
import science.unlicense.format.ps.ps.vm.matrix.IDTransform;
import science.unlicense.format.ps.ps.vm.matrix.ITransform;
import science.unlicense.format.ps.ps.vm.matrix.IdentMatrix;
import science.unlicense.format.ps.ps.vm.matrix.InitMatrix;
import science.unlicense.format.ps.ps.vm.matrix.InvertMatrix;
import science.unlicense.format.ps.ps.vm.matrix.Matrix;
import science.unlicense.format.ps.ps.vm.matrix.Rotate;
import science.unlicense.format.ps.ps.vm.matrix.Scale;
import science.unlicense.format.ps.ps.vm.matrix.SetMatrix;
import science.unlicense.format.ps.ps.vm.matrix.Transform;
import science.unlicense.format.ps.ps.vm.matrix.Translate;
import science.unlicense.format.ps.ps.vm.memory.CurrentGlobal;
import science.unlicense.format.ps.ps.vm.memory.DefineUserObject;
import science.unlicense.format.ps.ps.vm.memory.ExecUserObject;
import science.unlicense.format.ps.ps.vm.memory.GCheck;
import science.unlicense.format.ps.ps.vm.memory.Restore;
import science.unlicense.format.ps.ps.vm.memory.Save;
import science.unlicense.format.ps.ps.vm.memory.SetGlobal;
import science.unlicense.format.ps.ps.vm.memory.StartJob;
import science.unlicense.format.ps.ps.vm.memory.UndefineUserObject;
import science.unlicense.format.ps.ps.vm.memory.UserObjects;
import science.unlicense.format.ps.ps.vm.misc.Bind;
import science.unlicense.format.ps.ps.vm.misc.Echo;
import science.unlicense.format.ps.ps.vm.misc.Executive;
import science.unlicense.format.ps.ps.vm.misc.LanguageLevel;
import science.unlicense.format.ps.ps.vm.misc.Null;
import science.unlicense.format.ps.ps.vm.misc.Product;
import science.unlicense.format.ps.ps.vm.misc.Prompt;
import science.unlicense.format.ps.ps.vm.misc.RealTime;
import science.unlicense.format.ps.ps.vm.misc.Revision;
import science.unlicense.format.ps.ps.vm.misc.SerialNumber;
import science.unlicense.format.ps.ps.vm.misc.UserTime;
import science.unlicense.format.ps.ps.vm.misc.Version;
import science.unlicense.format.ps.ps.vm.packedarray.CurrentPacking;
import science.unlicense.format.ps.ps.vm.packedarray.PackedArray;
import science.unlicense.format.ps.ps.vm.packedarray.SetPacking;
import science.unlicense.format.ps.ps.vm.painting.ColorImage;
import science.unlicense.format.ps.ps.vm.painting.EOFill;
import science.unlicense.format.ps.ps.vm.painting.ErasePage;
import science.unlicense.format.ps.ps.vm.painting.Fill;
import science.unlicense.format.ps.ps.vm.painting.Image;
import science.unlicense.format.ps.ps.vm.painting.ImageMask;
import science.unlicense.format.ps.ps.vm.painting.RectFill;
import science.unlicense.format.ps.ps.vm.painting.RectStroke;
import science.unlicense.format.ps.ps.vm.painting.SHFill;
import science.unlicense.format.ps.ps.vm.painting.Stroke;
import science.unlicense.format.ps.ps.vm.painting.UEOFill;
import science.unlicense.format.ps.ps.vm.painting.UFill;
import science.unlicense.format.ps.ps.vm.painting.UStroke;
import science.unlicense.format.ps.ps.vm.path.Arc;
import science.unlicense.format.ps.ps.vm.path.ArcTo;
import science.unlicense.format.ps.ps.vm.path.Arcn;
import science.unlicense.format.ps.ps.vm.path.Arct;
import science.unlicense.format.ps.ps.vm.path.CharPath;
import science.unlicense.format.ps.ps.vm.path.Clip;
import science.unlicense.format.ps.ps.vm.path.ClipPath;
import science.unlicense.format.ps.ps.vm.path.ClosePath;
import science.unlicense.format.ps.ps.vm.path.CurrentPoint;
import science.unlicense.format.ps.ps.vm.path.CurveTo;
import science.unlicense.format.ps.ps.vm.path.EOClip;
import science.unlicense.format.ps.ps.vm.path.FlattenPath;
import science.unlicense.format.ps.ps.vm.path.InitClip;
import science.unlicense.format.ps.ps.vm.path.LineTo;
import science.unlicense.format.ps.ps.vm.path.MoveTo;
import science.unlicense.format.ps.ps.vm.path.NewPath;
import science.unlicense.format.ps.ps.vm.path.PathBBox;
import science.unlicense.format.ps.ps.vm.path.PathForAll;
import science.unlicense.format.ps.ps.vm.path.RCurveTo;
import science.unlicense.format.ps.ps.vm.path.RLineTo;
import science.unlicense.format.ps.ps.vm.path.RMoveTo;
import science.unlicense.format.ps.ps.vm.path.RectClip;
import science.unlicense.format.ps.ps.vm.path.ReversePath;
import science.unlicense.format.ps.ps.vm.path.SetBBox;
import science.unlicense.format.ps.ps.vm.path.StrokePath;
import science.unlicense.format.ps.ps.vm.path.UAppend;
import science.unlicense.format.ps.ps.vm.path.UCache;
import science.unlicense.format.ps.ps.vm.path.UPath;
import science.unlicense.format.ps.ps.vm.path.UStrokePath;
import science.unlicense.format.ps.ps.vm.device.CopyPage;
import science.unlicense.format.ps.ps.vm.device.CurrentPageDevice;
import science.unlicense.format.ps.ps.vm.pattern.ExecForm;
import science.unlicense.format.ps.ps.vm.pattern.MakePattern;
import science.unlicense.format.ps.ps.vm.device.NullDevice;
import science.unlicense.format.ps.ps.vm.device.SetPageDevice;
import science.unlicense.format.ps.ps.vm.pattern.SetPattern;
import science.unlicense.format.ps.ps.vm.device.ShowPage;
import science.unlicense.format.ps.ps.vm.font.AShow;
import science.unlicense.format.ps.ps.vm.font.AWidthShow;
import science.unlicense.format.ps.ps.vm.font.CShow;
import science.unlicense.format.ps.ps.vm.font.ComposeFont;
import science.unlicense.format.ps.ps.vm.font.CurrentFont;
import science.unlicense.format.ps.ps.vm.font.DefineFont;
import science.unlicense.format.ps.ps.vm.font.FindEncoding;
import science.unlicense.format.ps.ps.vm.font.FindFont;
import science.unlicense.format.ps.ps.vm.font.FontDirectory;
import science.unlicense.format.ps.ps.vm.font.GlobalFontDirectory;
import science.unlicense.format.ps.ps.vm.font.GlyphShow;
import science.unlicense.format.ps.ps.vm.font.ISOLatin1Encoding;
import science.unlicense.format.ps.ps.vm.font.KShow;
import science.unlicense.format.ps.ps.vm.font.MakeFont;
import science.unlicense.format.ps.ps.vm.font.RootFont;
import science.unlicense.format.ps.ps.vm.font.ScaleFont;
import science.unlicense.format.ps.ps.vm.font.SelectFont;
import science.unlicense.format.ps.ps.vm.font.SetCacheDevice;
import science.unlicense.format.ps.ps.vm.font.SetCacheDevice2;
import science.unlicense.format.ps.ps.vm.font.SetCharWidth;
import science.unlicense.format.ps.ps.vm.font.SetFont;
import science.unlicense.format.ps.ps.vm.font.Show;
import science.unlicense.format.ps.ps.vm.font.StandardEncoding;
import science.unlicense.format.ps.ps.vm.font.StringWidth;
import science.unlicense.format.ps.ps.vm.font.UndefineFont;
import science.unlicense.format.ps.ps.vm.font.WidthShow;
import science.unlicense.format.ps.ps.vm.font.XShow;
import science.unlicense.format.ps.ps.vm.font.XYShow;
import science.unlicense.format.ps.ps.vm.font.YShow;
import science.unlicense.format.ps.ps.vm.interpreter.CacheStatus;
import science.unlicense.format.ps.ps.vm.interpreter.CurrentCacheParams;
import science.unlicense.format.ps.ps.vm.interpreter.CurrentDevParams;
import science.unlicense.format.ps.ps.vm.interpreter.CurrentSystemParams;
import science.unlicense.format.ps.ps.vm.interpreter.CurrentUserParams;
import science.unlicense.format.ps.ps.vm.interpreter.SetCacheLimit;
import science.unlicense.format.ps.ps.vm.interpreter.SetCacheParams;
import science.unlicense.format.ps.ps.vm.interpreter.SetDevParams;
import science.unlicense.format.ps.ps.vm.interpreter.SetSystemParams;
import science.unlicense.format.ps.ps.vm.interpreter.SetUCacheParams;
import science.unlicense.format.ps.ps.vm.interpreter.SetUserParams;
import science.unlicense.format.ps.ps.vm.interpreter.SetVMThreshold;
import science.unlicense.format.ps.ps.vm.interpreter.UCacheStatus;
import science.unlicense.format.ps.ps.vm.interpreter.VMReclaim;
import science.unlicense.format.ps.ps.vm.interpreter.VMStatus;
import science.unlicense.format.ps.ps.vm.presentation.A4;
import science.unlicense.format.ps.ps.vm.presentation.A4Small;
import science.unlicense.format.ps.ps.vm.presentation.B5;
import science.unlicense.format.ps.ps.vm.presentation.Ledger;
import science.unlicense.format.ps.ps.vm.presentation.Legal;
import science.unlicense.format.ps.ps.vm.presentation.Letter;
import science.unlicense.format.ps.ps.vm.presentation.LetterSmall;
import science.unlicense.format.ps.ps.vm.resource.DefineResource;
import science.unlicense.format.ps.ps.vm.resource.FindColorRendering;
import science.unlicense.format.ps.ps.vm.resource.FindResource;
import science.unlicense.format.ps.ps.vm.resource.ResourceForAll;
import science.unlicense.format.ps.ps.vm.resource.ResourceStatus;
import science.unlicense.format.ps.ps.vm.resource.UndefineResource;
import science.unlicense.format.ps.ps.vm.stack.Clear;
import science.unlicense.format.ps.ps.vm.stack.ClearToMark;
import science.unlicense.format.ps.ps.vm.stack.Count;
import science.unlicense.format.ps.ps.vm.stack.CountToMark;
import science.unlicense.format.ps.ps.vm.stack.Dup;
import science.unlicense.format.ps.ps.vm.stack.Exch;
import science.unlicense.format.ps.ps.vm.stack.Index;
import science.unlicense.format.ps.ps.vm.stack.Mark;
import science.unlicense.format.ps.ps.vm.stack.Pop;
import science.unlicense.format.ps.ps.vm.stack.Roll;
import science.unlicense.format.ps.ps.vm.string.AnchorSearch;
import science.unlicense.format.ps.ps.vm.string.Search;
import science.unlicense.format.ps.ps.vm.string.Token;
import science.unlicense.format.ps.ps.vm.type.Cvi;
import science.unlicense.format.ps.ps.vm.type.Cvlit;
import science.unlicense.format.ps.ps.vm.type.Cvn;
import science.unlicense.format.ps.ps.vm.type.Cvr;
import science.unlicense.format.ps.ps.vm.type.Cvrs;
import science.unlicense.format.ps.ps.vm.type.Cvs;
import science.unlicense.format.ps.ps.vm.type.Cvx;
import science.unlicense.format.ps.ps.vm.type.ExecuteOnly;
import science.unlicense.format.ps.ps.vm.type.NoAccess;
import science.unlicense.format.ps.ps.vm.type.RCheck;
import science.unlicense.format.ps.ps.vm.type.ReadOnly;
import science.unlicense.format.ps.ps.vm.type.Type;
import science.unlicense.format.ps.ps.vm.type.WCheck;
import science.unlicense.format.ps.ps.vm.type.XCheck;

/**
 * A Postscript virtual machine interpret the postscript code.
 * While this VM undetstand postscript it does not do the related rendering
 * or queries to the user, For this you must provide the VM a Painter2D and a Prompter.
 *
 * @author Johann Sorel
 */
public class PSVirtualMachine {

    //objects reuquiered to implement all postcript fonctionnalities.
    static final Dictionary systemDict = new HashDictionary();

    private final VMState vmstate = new VMState();


    private Painter2D painter;
    private GraphicState graphicState;

    static {
        //reference : PLRM.pdf page 521
        final PSFunction[] functions = new PSFunction[]{
            //stack
            new Pop(), new Exch(), new Dup(), new science.unlicense.format.ps.ps.vm.stack.Copy(),
            new Index(), new Roll(), new Clear(), new Count(), new Mark(), new ClearToMark(),
            new CountToMark(),
            //arithmetic
            new Add(), new Div(), new IDiv(), new Mod(), new Mul(), new Sub(),
            new Abs(), new Neg(), new Ceiling(), new Floor(), new Round(), new Truncate(),
            new Sqrt(), new Atan(), new Cos(), new Sin(), new Exp(), new Ln(),
            new Log(), new Rand(), new SRand(), new RRand(),
            //array
            new Array(), new StartArray(), new EndArray(), new Length(), new Get(),
            new Put(), new GetInterval(), new PutInterval(), new AStore(), new ALoad(),
            new Copy(), new ForAll(),
            //packed array
            new PackedArray(), new SetPacking(), new CurrentPacking(), new science.unlicense.format.ps.ps.vm.packedarray.Length(),
            new science.unlicense.format.ps.ps.vm.packedarray.Get(), new science.unlicense.format.ps.ps.vm.packedarray.GetInterval(),
            new science.unlicense.format.ps.ps.vm.packedarray.ALoad(), new science.unlicense.format.ps.ps.vm.packedarray.Copy(),
            new science.unlicense.format.ps.ps.vm.packedarray.ForAll(),
            //dictionnary
            new Dict(), new MinMin(), new MaxMax(), new science.unlicense.format.ps.ps.vm.dict.Length(),
            new MaxLength(), new Begin(), new End(), new Def(), new Load(), new Store(),
            new science.unlicense.format.ps.ps.vm.dict.Get(), new science.unlicense.format.ps.ps.vm.dict.Put(),
            new Undef(), new Known(), new Where(), new science.unlicense.format.ps.ps.vm.dict.Copy(),
            new science.unlicense.format.ps.ps.vm.dict.ForAll(), new CurrentDict(), new ErrorDict(),
            new science.unlicense.format.ps.ps.vm.dict.Error(), new SystemDict(), new UserDict(),
            new GlobalDict(), new StatusDict(), new CountDictStack(), new DictStack(),
            new ClearDictStack(),
            //string
            new science.unlicense.format.ps.ps.vm.string.String(), new science.unlicense.format.ps.ps.vm.string.Length(),
            new science.unlicense.format.ps.ps.vm.string.Get(), new science.unlicense.format.ps.ps.vm.string.Put(),
            new science.unlicense.format.ps.ps.vm.string.GetInterval(), new science.unlicense.format.ps.ps.vm.string.PutInterval(),
            new science.unlicense.format.ps.ps.vm.string.Copy(), new science.unlicense.format.ps.ps.vm.string.ForAll(),
            new AnchorSearch(), new Search(), new Token(),
            //relation,boolean,bitwise
            new Eq(), new Ne(), new Ge(), new Gt(), new Le(), new Lt(), new And(),
            new Not(), new Or(), new Xor(), new True(), new False(), new BitShift(),
            //control
            new Exec(), new If(), new IfElse(), new For(), new Repeat(), new Loop(),
            new Exit(), new Stop(), new Stopped(), new CountExecStack(), new ExecStack(),
            new Quit(), new Start(),
            //type,attribute,conversion
            new Type(), new Cvlit(), new Cvx(), new XCheck(), new ExecuteOnly(),
            new NoAccess(), new ReadOnly(), new RCheck(), new WCheck(), new Cvi(),
            new Cvn(), new Cvr(), new Cvrs(), new Cvs(),
            //file
            new File(), new Filter(), new CloseFile(), new Read(), new Write(),
            new ReadHexString(), new WriteHexString(), new ReadString(), new WriteString(),
            new ReadLine(), new science.unlicense.format.ps.ps.vm.file.Token(), new BytesAvailable(),
            new Flush(), new FlushFile(), new ResetFile(), new Status(), new Run(),
            new CurrentFile(), new DeleteFile(), new RenameFile(), new FileNameForAll(),
            new SetFilePosition(), new FilePosition(), new Print(), new Equal(),
            new EqualEqual(), new Stack(), new PStack(), new PrintObject(), new WriteObject(),
            new SetObjectFormat(), new CurrentObjectFormat(),
            //resource
            new DefineResource(), new UndefineResource(), new FindResource(),
            new FindColorRendering(), new ResourceStatus(), new ResourceForAll(),
            //virtual memory
            new Save(), new Restore(), new SetGlobal(), new CurrentGlobal(),
            new GCheck(), new StartJob(), new DefineUserObject(), new ExecUserObject(),
            new UndefineUserObject(), new UserObjects(),
            //miscellaneous
            new Bind(), new Null(), new Version(), new RealTime(), new UserTime(),
            new LanguageLevel(), new Product(), new Revision(), new SerialNumber(),
            new Executive(), new Echo(), new Prompt(),
            //graphic state independante
            new GSave(), new GRestore(), new ClipSave(), new ClipRestore(),
            new GRestoreAll(), new InitGraphics(), new GState(), new SetGState(),
            new CurrentGState(), new SetLineWidth(), new CurrentLineWidth(),
            new SetLineCap(), new CurrentLineCap(), new SetLineJoin(), new CurrentLineJoin(),
            new SetMiterLimit(), new CurrentMiterLimit(), new SetStrokeAdjust(),
            new CurrentStrokeAdjust(), new SetDash(), new CurrentDash(),
            new SetColorspace(), new CurrentColorspace(), new SetColor(),
            new CurrentColor(), new SetGray(), new CurrentGray(), new SetHSBColor(),
            new CurrentHSBColor(), new SetRGBColor(), new CurrentRGBColor(),
            new SetCMYKColor(), new CurrentCMYKColor(),
            //graphic state dependante
            new SetHalfTone(), new CurrentHalfTone(), new SetScreen(), new CurrentScreen(),
            new SetColorScreen(), new CurrentColorScreen(), new SetTransfer(),
            new CurrentTransfer(), new SetColorTransfer(), new CurrentColorTransfer(),
            new SetBlackGeneration(), new CurrentBlackGeneration(), new SetUnderColorRemoval(),
            new CurrentUnderColorRemoval(), new SetColorRendering(), new CurrentColorRendering(),
            new SetFlat(), new CurrentFlat(), new SetOverPrint(), new CurrentOverPrint(),
            new SetSmoothness(), new CurrentSmoothness(),
            //coordinate system, matrix
            new Matrix(), new InitMatrix(), new IdentMatrix(), new DefaultMatrix(),
            new CurrentMatrix(), new SetMatrix(), new Translate(), new Scale(),
            new Rotate(), new Concat(), new ConcatMatrix(), new Transform(),
            new DTransform(), new ITransform(), new IDTransform(), new InvertMatrix(),
            //path construction
            new NewPath(), new CurrentPoint(), new MoveTo(), new RMoveTo(), new LineTo(),
            new RLineTo(), new Arc(), new Arcn(), new Arct(), new ArcTo(),
            new CurveTo(), new RCurveTo(), new ClosePath(), new FlattenPath(),
            new ReversePath(), new StrokePath(), new UStrokePath(), new CharPath(),
            new UAppend(), new ClipPath(), new SetBBox(), new PathBBox(), new PathForAll(),
            new UPath(), new InitClip(), new Clip(), new EOClip(), new RectClip(),
            new UCache(),
            //painting
            new ErasePage(), new Stroke(), new Fill(), new EOFill(), new RectStroke(),
            new RectFill(), new UStroke(), new UFill(), new UEOFill(), new SHFill(),
            new Image(), new ColorImage(), new ImageMask(),
            //insideness testing
            new InFill(), new InEOFill(), new InUFill(), new InUEOFill(),
            new InStroke(), new InUStroke(),
            //form and pattern
            new MakePattern(), new SetPattern(), new ExecForm(),
            //device setup and output
            new ShowPage(), new CopyPage(), new SetPageDevice(),
            new CurrentPageDevice(), new NullDevice(),
            //glyph and font
            new DefineFont(), new ComposeFont(), new UndefineFont(), new FindFont(),
            new ScaleFont(), new MakeFont(), new SetFont(), new RootFont(), new CurrentFont(),
            new SelectFont(), new Show(), new AShow(), new WidthShow(), new AWidthShow(),
            new XShow(), new XYShow(), new YShow(), new GlyphShow(), new StringWidth(),
            new CShow(), new KShow(), new FontDirectory(), new GlobalFontDirectory(),
            new StandardEncoding(), new ISOLatin1Encoding(), new FindEncoding(),
            new SetCacheDevice(), new SetCacheDevice2(), new SetCharWidth(),
            //interpreter
            new SetSystemParams(), new CurrentSystemParams(), new SetUserParams(),
            new CurrentUserParams(), new SetDevParams(), new CurrentDevParams(),
            new VMReclaim(), new SetVMThreshold(), new VMStatus(), new CacheStatus(),
            new SetCacheLimit(), new SetCacheParams(), new CurrentCacheParams(),
            new SetUCacheParams(), new UCacheStatus(),
            //presentation, page 824
            new A4(), new A4Small(), new B5(), new Ledger(), new Legal(),
            new Letter(), new LetterSmall()

        };
        for (PSFunction f : functions){
            systemDict.add(f.getName(), f);
        }
    }


    public PSVirtualMachine() {
        //dictionnary stack starts with : system,global and user dictionaries
        vmstate.dictStack.add(systemDict);
        vmstate.dictStack.add(vmstate.globalDict);
        vmstate.dictStack.add(vmstate.userDict);
        //default graphic stack
        graphicState = new GraphicState(false);
    }

    public Painter2D getPainter() {
        return painter;
    }

    public void setPainter(Painter2D painter) {
        this.painter = painter;
        graphicState.restore(painter);
    }

    public science.unlicense.common.api.collection.Stack getOperandStack() {
        return vmstate.operandStack;
    }

    public ArrayStack getDictStack() {
        return vmstate.dictStack;
    }

    public science.unlicense.common.api.collection.Stack getExecStack() {
        return vmstate.execStack;
    }

    public science.unlicense.common.api.collection.Stack getGraphicStack() {
        return vmstate.graphicStack;
    }

    public Dictionary getSystemDictionary() {
        return systemDict;
    }

    public Dictionary getGlobalDictionary() {
        return vmstate.globalDict;
    }

    public Dictionary getUserDictionary() {
        return vmstate.userDict;
    }

    public Dictionary getStatusDictionary() {
        return vmstate.statusDict;
    }

    public Dictionary getCurrentDictionary() {
        return (Dictionary) vmstate.dictStack.lookEnd();
    }

    public GraphicState getCurrentGraphicState(){
        return graphicState;
    }

    public Object resolve(PSPointer pointer){
        return vmstate.heap.getValue(pointer.name);
    }

    public void push(Object candidate) throws VMException{
        if (candidate instanceof PSCall){
            final PSCall call = (PSCall) candidate;

            Object fct = null;
            for (int i=vmstate.dictStack.getSize()-1;fct==null && i>=0;i--){
                fct = ((Dictionary) vmstate.dictStack.get(i)).getValue(call.functionName);
            }

            if (fct instanceof PSFunction){
                ((PSFunction) fct).execute(this);
            } else if (fct instanceof PSProcedure){
                ((PSProcedure) fct).execute(this);
            } else {
                throw new VMException("Unknown function "+call.functionName);
            }
        } else {
            vmstate.operandStack.add(candidate);
        }
    }

    public Object pull(){
        return vmstate.operandStack.pickEnd();
    }

}
