package science.unlicense.format.ps.ps.vm.presentation;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Configures the page to A4 format.
 * spec page 824
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class A4 extends PSFunction {

    public static final Chars NAME = Chars.constant("a4");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO : event ? draw a back border of format size ?

    }

}
