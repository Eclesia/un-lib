package science.unlicense.format.ps.ps.vm.device;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Show current page.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class ShowPage extends PSFunction {

    public static final Chars NAME = Chars.constant("showpage");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO send somekind of event to notify the page is ready and we have to reset the painter.
    }

}
