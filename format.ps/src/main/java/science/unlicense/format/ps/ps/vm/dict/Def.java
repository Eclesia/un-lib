package science.unlicense.format.ps.ps.vm.dict;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSPointer;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Put in current dictionary the given key and associated value.
 * Page 583
 *
 * Stack in|out :
 * key value | -
 *
 * @author Johann Sorel
 */
public class Def extends PSFunction {

    public static final Chars NAME = Chars.constant("def");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Object value = vm.pull();
        final Object key = vm.pull();
        vm.getCurrentDictionary().add(((PSPointer) key).name, value);
    }

}
