package science.unlicense.format.ps.ps.vm;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.code.vm.VMException;

/**
 *
 * @author Johann Sorel
 */
public class PSProcedure extends ArraySequence {

    public void execute(PSVirtualMachine vm) throws VMException{
        for (int i=0,n=getSize();i<n;i++){
            vm.push(get(i));
        }
    }

}
