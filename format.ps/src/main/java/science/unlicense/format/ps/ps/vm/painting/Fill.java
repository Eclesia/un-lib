package science.unlicense.format.ps.ps.vm.painting;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Fill current path.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class Fill extends PSFunction {

    public static final Chars NAME = Chars.constant("fill");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final GraphicState gs = vm.getCurrentGraphicState();
        vm.getPainter().fill(gs.currentGeometry);
    }

}
