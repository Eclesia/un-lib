package science.unlicense.format.ps.ps.vm.matrix;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.Affine2;
import science.unlicense.format.ps.ps.vm.GraphicState;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Scale ctm or given matrix.
 *
 * Stack in|out :
 * sx sy | -
 * sx sy matrix | matrix
 *
 * @author Johann Sorel
 */
public class Scale extends PSFunction {

    public static final Chars NAME = Chars.constant("scale");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Object obj3 = pullValue(vm);
        Object obj2 = pullValue(vm);

        if (obj3 instanceof Matrix){
            Object obj1 = pullValue(vm);
            final double sy = ((Number) obj2).doubleValue();
            final double sx = ((Number) obj1).doubleValue();
            final Affine2 t = new Affine2();
            t.set(0, 0, sx);
            t.set(1, 1, sy);
            science.unlicense.math.impl.Affine2 m = new Affine2((science.unlicense.math.impl.MatrixNxN) obj3);
            t.localMultiply(m);
            vm.push(t);

        } else {
            final double sy = ((Number) obj3).doubleValue();
            final double sx = ((Number) obj2).doubleValue();
            final Affine2 t = new Affine2();
            t.set(0, 0, sx);
            t.set(1, 1, sy);

            final GraphicState state = vm.getCurrentGraphicState();
            t.localMultiply(state.ctm);
            state.ctm = t;
            vm.getPainter().setTransform(state.ctm);
        }
    }

}
