package science.unlicense.format.ps.ps.vm.math;

import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 *
 * @author Johann Sorel
 */
public class IDiv extends PSFunction {

    public static final Chars NAME = Chars.constant("idiv");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final Number v2 = (Number) pullValue(vm);
        final Number v1 = (Number) pullValue(vm);
        vm.push((int) (v1.doubleValue() / v2.doubleValue()));
    }

}
