package science.unlicense.format.ps.ps.vm.painting;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.ps.ps.vm.PSFunction;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 * Erase current page.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class ErasePage extends PSFunction {

    public static final Chars NAME = Chars.constant("erasepage");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO : fire an event ? clear the page using a white transparent blackground ?

    }

}
