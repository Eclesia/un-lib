
package science.unlicense.format.ps;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.ps.ps.PSReader;

/**
 *
 * @author Johann Sorel
 */
public class EPSReadTest {

    @Test
    public void testRead() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/eps/square.eps"));
        final PSReader reader = new PSReader();
        reader.setInput(path);

        for (Object o = reader.next();o!=null;o = reader.next()){
        }
    }

}
