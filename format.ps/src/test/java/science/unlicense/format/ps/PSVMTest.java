package science.unlicense.format.ps;

import org.junit.Test;
import science.unlicense.code.vm.VMException;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.ps.ps.PSReader;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;

/**
 *
 * @author Johann Sorel
 */
public class PSVMTest {

    private static final double DELTA = 0.00000001;

    @Test
    public void testMath() throws IOException, VMException{
        Object result = execute("mod:/science/unlicense/format/ps/math.ps");
        Assert.assertEquals(9.0, (Double) result, DELTA);

    }

    @Test
    public void testFunction() throws IOException, VMException{
        Object result = execute("mod:/science/unlicense/format/ps/function.ps");
        Assert.assertEquals(16, (Double) result, DELTA);

    }

    private Object execute(String filePath) throws IOException, VMException{
        final Path path = Paths.resolve(new Chars(filePath));
        final PSReader reader = new PSReader();
        reader.setInput(path);

        final PSVirtualMachine vm = new PSVirtualMachine();

        for (Object o = reader.next();o!=null;o = reader.next()){
            vm.push(o);
        }

        return vm.pull();
    }

}
