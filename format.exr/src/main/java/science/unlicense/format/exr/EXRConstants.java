
package science.unlicense.format.exr;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class EXRConstants {


    public static final byte[] SIGNATURE = new byte[]{0x76, 0x2f, 0x31, 0x01};

    /**
     * for file types : all
     * type : chlist
     */
    public static final Chars ATT_CHANNELS = Chars.constant("channels");
    /**
     * for file types : all
     * type : compression
     */
    public static final Chars ATT_COMPRESSION = Chars.constant("compression");
    /**
     * for file types : all
     * type : box2i
     */
    public static final Chars ATT_DATA_WINDOW = Chars.constant("dataWindow");
    /**
     * for file types : all
     * type : box2i
     */
    public static final Chars ATT_DISPLAY_WINDOW = Chars.constant("displayWindow");
    /**
     * for file types : all
     * type : lineOrder
     */
    public static final Chars ATT_LINE_ORDER = Chars.constant("lineOrder");
    /**
     * for file types : all
     * type : float
     */
    public static final Chars ATT_PIXEL_ASPECT_RATIO = Chars.constant("pixelAspectRatio");
    /**
     * for file types : all
     * type : v2f
     */
    public static final Chars ATT_SCREEN_WINDOW_CENTER = Chars.constant("screenWindowCenter");
    /**
     * for file types : all
     * type : float
     */
    public static final Chars ATT_SCREEN_WINDOW_WIDTH = Chars.constant("screenWindowWidth");
    /**
     * for file types : tile, multi-part,deep
     * type : tiledesc
     */
    public static final Chars ATT_TILES = Chars.constant("tiles");
    /**
     * for file types : multi-view
     * type : text
     */
    public static final Chars ATT_VIEW = Chars.constant("view");
    /**
     * for file types :  multi-part,deep
     * type : string
     */
    public static final Chars ATT_NAME = Chars.constant("name");
    /**
     * for file types :  multi-part,deep
     * type : string
     */
    public static final Chars ATT_TYPE = Chars.constant("type");
    /**
     * for file types :  multi-part,deep
     * type : chlist
     */
    public static final Chars ATT_VERSION = Chars.constant("version");
    /**
     * for file types :  multi-part,deep
     * type : int
     */
    public static final Chars ATT_CHUNK_COUNT = Chars.constant("chunkCount");
    /**
     * for file types :  deep
     * type : int
     */
    public static final Chars ATT_MAX_SAMPLES_PER_PIXEL = Chars.constant("maxSamplesPerPixel");


    public static final Chars TYPE_SCANLINE_IMAGE = Chars.constant("scanlineimage");
    public static final Chars TYPE_TILED_IMAGE = Chars.constant("tiledimage");
    public static final Chars TYPE_DEEP_SCANLINE = Chars.constant("deepscanline");
    public static final Chars TYPE_DEEP_TILE = Chars.constant("deeptile");

//    public static final Chars NO_COMPRESSION;
//    public static final Chars RLE_COMPRESSION;
//    public static final Chars ZIPS_COMPRESSION;
//    public static final Chars ZIP_COMPRESSION;
//    public static final Chars PIZ_COMPRESSION;
//    public static final Chars PXR24_COMPRESSION;
//    public static final Chars B44_COMPRESSION;
//    public static final Chars B44A_COMPRESSION;

    private EXRConstants(){}

}
