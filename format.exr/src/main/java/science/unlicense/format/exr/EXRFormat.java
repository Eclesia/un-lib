
package science.unlicense.format.exr;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * OpenEXR, high quality multiband images :
 * http://www.openexr.com/documentation.html
 *
 * @author Johann Sorel
 */
public class EXRFormat extends AbstractImageFormat {

    public static final EXRFormat INSTANCE = new EXRFormat();

    private EXRFormat() {
        super(new Chars("exr"));
        shortName = new Chars("EXR");
        longName = new Chars("OpenEXR");
        extensions.add(new Chars("exr"));
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new EXRStore(this, source);
    }

}
