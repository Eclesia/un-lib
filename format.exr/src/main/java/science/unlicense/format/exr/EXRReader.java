
package science.unlicense.format.exr;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import static science.unlicense.format.exr.EXRConstants.*;
import science.unlicense.format.exr.model.Attribute;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;

/**
 *
 * @author Johann Sorel
 */
public class EXRReader extends AbstractImageReader {

    public EXRReader() {
    }

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {

        DataInputStream ds = new DataInputStream(stream, Endianness.LITTLE_ENDIAN);

        if (!Arrays.equals(ds.readFully(new byte[4]),SIGNATURE)){
            throw new IOException(ds, "Stream is not an EXR file.");
        }

        final int version = ds.readBits(8);
        final int flags = ds.readBits(24);
        final boolean singleTile = (flags & 0x200) ==1;
        final boolean longNames = (flags & 0x400) ==1;
        final boolean nonImage = (flags & 0x800) ==1;
        final boolean multipart = (flags & 0x1000) ==1;

        //reade header
        final Dictionary attributes = new HashDictionary();
        for (;;){
            final Attribute att = new Attribute();
            att.read(ds, longNames);
            if (att.name.isEmpty()) break;
            attributes.add(att.name, att);
        }

        //read table
        final int nbEntry;
        if (!multipart && attributes.getValue(ATT_CHUNK_COUNT)==null){
            //calculated number of entries
            throw new IOException(ds, "TODO");
        } else {
            nbEntry = (Integer) ((Attribute) attributes.getValue(ATT_CHUNK_COUNT)).value;
        }
        final long[] offsets = ds.readLong(nbEntry);

        throw new UnimplementedException("Not supported yet.");
    }

}
