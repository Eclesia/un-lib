
package science.unlicense.format.exr.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class Attribute {

    public Chars name;
    public int type;
    public int size;
    public Object value;

    public void read(DataInputStream ds, boolean longName) throws IOException {
        if (longName) {
            name = ds.readZeroTerminatedChars(32, CharEncodings.US_ASCII);
        } else {
            name = ds.readZeroTerminatedChars(256, CharEncodings.US_ASCII);
        }
    }

}
