
package science.unlicense.format.draco.model;

/**
 *
 * @author Johann Sorel
 */
public class AttributeEncoding {

    public int num_attributes_decoders;
    /**
     * Array of attribute decoder ids
     */
    public int att_dec_data_id;
    /**
     * Array of attribute decoder types
     * 0: MESH_VERTEX_ATTRIBUTE
     * 1: MESH_CORNER_ATTRIBUTE
     */
    public int att_dec_decoder_type;
    /**
     * Array of attribute traversal methods
     * 0: MESH_TRAVERSAL_DEPTH_FIRST
     *  1: MESH_TRAVERSAL_PREDICTION_DEGREE
     */
    public int att_dec_traversal_method;
    /**
     * Number of values to decode per attribute
     */
    public int att_dec_num_values_to_decode;
    /**
     * Array of number of attributes encoded per attribute type
     */
    public int att_dec_num_attributes;

    public int att_dec_att_type;
    /**
     * Attribute’s data type
     */
    public int att_dec_data_type;
    /**
     * Attribute’s component count
     */
    public int att_dec_num_components;

    public int att_dec_normalized;
    /**
     * Attribute’s unique encoded id
     */
    public int att_dec_unique_id;
    /**
     * Array of attribute encoding type
     * 0: SEQUENTIAL_ATTRIBUTE_ENCODER_GENERIC
     * 1: SEQUENTIAL_ATTRIBUTE_ENCODER_INTEGER
     * 2: SEQUENTIAL_ATTRIBUTE_ENCODER_QUANTIZATION
     * 3: SEQUENTIAL_ATTRIBUTE_ENCODER_NORMALS
     */
    public int seq_att_dec_decoder_type;
    /**
     * Array of attribute prediction scheme method
     * -2: PREDICTION_NONE
     *  0: PREDICTION_DIFFERENCE
     *  1: MESH_PREDICTION_PARALLELOGRAM
     *  4: MESH_PREDICTION_CONSTRAINED_MULTI_PARALLELOGRAM
     *  5: MESH_PREDICTION_TEX_COORDS_PORTABLE
     *  6: MESH_PREDICTION_GEOMETRIC_NORMAL
     */
    public int seq_att_dec_prediction_scheme;
    /**
     * Array of attribute prediction transform method
     * 1: PREDICTION_TRANSFORM_WRAP
     * 3: PREDICTION_TRANSFORM_NORMAL_OCTAHEDRON_CANONICALIZED
     */
    public int seq_att_dec_prediction_transform_type;

    public int seq_int_att_dec_compressed;
    /**
     * Array for multi parallelogram prediction signifying if the edge is the last edge
     */
    public int pred_cons_multi_is_cease_edge;
    /**
     * Array signifying orientation for the texture coordinate prediction
     */
    public int pred_tex_coords_orientations;
    /**
     * Array of minimum clamp values used by the wrap transform
     */
    public int pred_trasnform_wrap_min;
    /**
     * Array of maximum clamp values used by the wrap transform
     */
    public int pred_trasnform_wrap_max;
    /**
     * Maximum quantization array used by the normal transform
     */
    public int pred_trasnform_normal_max_q_val;
    /**
     * Array of flags used by the normal transform
     */
    public int pred_transform_normal_flip_normal_bits;
    /**
     * Array of attribute decoded symbols
     */
    public int seq_int_att_dec_decoded_values;
    /**
     * Array of decoded symbols converted to signed ints
     */
    public int seq_int_att_dec_symbols_to_signed_ints;
    /**
     * Array containing the attribute’s original quantized values
     */
    public int seq_int_att_dec_original_values;
    /**
     * Array containing the attribute’s original values
     */
    public int seq_int_att_dec_dequantized_values;
    /**
     * Array of minimum quantization values
     */
    public int quantized_data_min_values;
    /**
     * Array of quantization range
     */
    public int quantized_data_max_value_df;
    /**
     * Array of number of quantization bits
     */
    public int quantized_data_quantization_bits;

}
