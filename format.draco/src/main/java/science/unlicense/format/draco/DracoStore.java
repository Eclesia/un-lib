
package science.unlicense.format.draco;

import science.unlicense.common.api.collection.Collection;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.model3d.api.AbstractModel3DStore;

/**
 *
 * @author Johann Sorel
 */
public class DracoStore extends AbstractModel3DStore {

    public DracoStore(Object input) {
        super(DracoFormat.INSTANCE, input);
    }

    @Override
    public Collection getElements() throws StoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
