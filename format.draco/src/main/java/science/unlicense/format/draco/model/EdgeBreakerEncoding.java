
package science.unlicense.format.draco.model;

/**
 *
 * @author Johann Sorel
 */
public class EdgeBreakerEncoding {

    /**
     * 0: MeshEdgeBreakerTraversalDecoder
     * 1: MeshEdgeBreakerTraversalPredictiveDecoder
     * 2: MeshEdgeBreakerTraversalValenceDecoder
     */
    public int edgebreaker_traversal_type;
    /**
     * Number of new vertices
     */
    public int num_new_vertices;
    /**
     * Number of encoded vertices
     */
    public int num_encoded_vertices;
    /**
     * Number of encoded faces
     */
    public int num_faces;
    /**
     * Number of encoded attributes
     */
    public int num_attribute_data;
    /**
     * Number of encoded EdgeBreaker symbols
     */
    public int num_encoded_symbols;
    /**
     * Number of encoded EdgeBreaker split symbols
     */
    public int num_encoded_split_symbols;
    /**
     * Size of encoded connectivity data in bytes
     */
    public int encoded_connectivity_size;

    public int num_topology_splits;
    /**
     * Array of delta encoded source symbol ids
     */
    public int[] source_id_delta;
    /**
     * Array of delta encoded split symbol ids
     */
    public int[] split_id_delta;
    /**
     * Array of source edge types
     * 0: LEFT_FACE_EDGE
     * 1: RIGHT_FACE_EDGE
     */
    public int[] source_edge_bit;
    /**
     * Array of source symbol ids
     */
    public int source_symbol_id;
    /**
     * Array of split symbol ids
     */
    public int split_symbol_id;
    /**
     * Last EdgeBreaker symbol decoded
     */
    public int last_symbol_;
    /**
     * Id of the last vertex decoded
     */
    public int last_vert_added;
    /**
     * Array of current working corners used during EdgeBreaker decoding
     */
    public int active_corner_stack;
    /**
     * Array of EdgeBreaker symbols
     *  0: TOPOLOGY_C
     *  1: TOPOLOGY_S
     *  2: TOPOLOGY_L
     *  3: TOPOLOGY_R
     *  4: TOPOLOGY_E
     */
    public int edge_breaker_symbol_to_topology_id;
    /**
     * List of decoder split ids encountered during a topology split.
     */
    public int topology_split_id;
    /**
     * List of corners encountered during a topology split.
     */
    public int split_active_corners;

}
