
package science.unlicense.format.draco.model;

/**
 *
 * @author Johann Sorel
 */
public class EdgeBreakerTraversal {

    public int eb_symbol_buffer_size;
    /**
     * Standard EdgeBreaker encoded symbol data
     */
    public int eb_symbol_buffer;
    /**
     * Face configuration encoded probability
     */
    public int eb_start_face_buffer_prob_zero;

    public int eb_start_face_buffer_size;
    /**
     * EdgeBreaker encoded face configuration data
     */
    public int eb_start_face_buffer;
    /**
     * Array of encoded attribute probabilities
     */
    public int attribute_connectivity_decoders_prob_zero;
    /**
     * Array of attribute connectivity size
     */
    public int attribute_connectivity_decoders_size;
    /**
     * Array of attribute connectivity data
     */
    public int attribute_connectivity_decoders_buffer;


}
