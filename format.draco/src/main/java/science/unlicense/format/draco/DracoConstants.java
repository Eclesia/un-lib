
package science.unlicense.format.draco;

/**
 *
 * @author Johann Sorel
 */
public final class DracoConstants {

    private DracoConstants(){}

    // Mesh encoding methods
    public static final int MESH_SEQUENTIAL_ENCODING = 0;
    public static final int MESH_EDGEBREAKER_ENCODING = 1;
    // Metadata constants
    public static final int METADATA_FLAG_MASK = 32768;
    // Sequential attribute encoding methods
    public static final int SEQUENTIAL_ATTRIBUTE_ENCODER_GENERIC = 0;
    public static final int SEQUENTIAL_ATTRIBUTE_ENCODER_INTEGER = 1;
    public static final int SEQUENTIAL_ATTRIBUTE_ENCODER_QUANTIZATION = 2;
    public static final int SEQUENTIAL_ATTRIBUTE_ENCODER_NORMALS = 3;
    // Sequential indices encoding methods
    public static final int SEQUENTIAL_COMPRESSED_INDICES = 0;
    public static final int SEQUENTIAL_UNCOMPRESSED_INDICES = 1;
    // Prediction encoding methods
    public static final int PREDICTION_NONE = -2;
    public static final int PREDICTION_DIFFERENCE = 0;
    public static final int MESH_PREDICTION_PARALLELOGRAM = 1;
    public static final int MESH_PREDICTION_CONSTRAINED_MULTI_PARALLELOGRAM = 4;
    public static final int MESH_PREDICTION_TEX_COORDS_PORTABLE = 5;
    public static final int MESH_PREDICTION_GEOMETRIC_NORMAL = 6;
    // Prediction scheme transform methods
    public static final int PREDICTION_TRANSFORM_WRAP = 1;
    public static final int PREDICTION_TRANSFORM_NORMAL_OCTAHEDRON_CANONICALIZED = 3;
    // Mesh traversal methods
    public static final int MESH_TRAVERSAL_DEPTH_FIRST = 0;
    public static final int MESH_TRAVERSAL_PREDICTION_DEGREE = 1;
    // Mesh attribute encoding methods
    public static final int MESH_VERTEX_ATTRIBUTE = 0;
    public static final int MESH_CORNER_ATTRIBUTE = 1;
    // EdgeBreaker encoding methods
    public static final int STANDARD_EDGEBREAKER = 0;
    public static final int VALENCE_EDGEBREAKER = 2;
    // EdgeBreaker constants
    public static final int kInvalidCornerIndex = -1;
    public static final int LEFT_FACE_EDGE = 0;
    public static final int RIGHT_FACE_EDGE = 1;
    public static final int kTexCoordsNumComponents = 2;
    public static final int kMaxNumParallelograms = 4;
    public static final int kMaxPriority = 3;
    // EdgeBreaker bit pattern constants
    public static final int TOPOLOGY_C = 0;
    public static final int TOPOLOGY_S = 1;
    public static final int TOPOLOGY_L = 3;
    public static final int TOPOLOGY_R = 5;
    public static final int TOPOLOGY_E = 7;
    // Valence EdgeBreaker constants
    public static final int MIN_VALENCE = 2;
    public static final int MAX_VALENCE = 7;
    public static final int NUM_UNIQUE_VALENCES = 6;
    // ANS constants
    public static final int rabs_ans_p8_precision = 256;
    public static final int rabs_ans_p10_precision = 1024;
    public static final int abs_l_base = 4096;
    public static final int IO_BASE = 256;
    public static final int L_RANS_BASE = 4096;
    public static final int TAGGED_RANS_BASE = 16384;
    public static final int TAGGED_RANS_PRECISION = 4096;
    // Symbol encoding methods
    public static final int TAGGED_SYMBOLS = 0;
    public static final int RAW_SYMBOLS = 1;

}
