
package science.unlicense.format.draco.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class Metadata {

    public static class SubMeta {
        public int num_entries;
        public byte[][] sub_metadata_key;
        public byte[][] keys;
        public byte[][] values;
        public int num_sub_metadata;
        public SubMeta[] sub_metadata;
    }

    /**
     * Attribute metadata count
     */
    public int num_att_metadata;
    /**
     * Array of attribute metadata ids
     */
    public int[] att_metadata_id;
    /**
     * Array of attribute metadata
     */
    public SubMeta[] att_metadata;
    /**
     * Global metadata
     */
    public SubMeta file_metadata;


    /**
     * 4.1 DecodeMetadata()
     * 4.2 ParseMetadataCount()
     * 4.3 ParseAttributeMetadataId()
     */
    public void read(DataInputStream ds) throws IOException {
        //ParseMetadataCount()
        num_att_metadata = (int) ds.readVarLengthUInt();
        att_metadata_id = new int[num_att_metadata];
        att_metadata = new SubMeta[num_att_metadata];

        for (int i = 0; i < num_att_metadata; ++i) {
            //ParseAttributeMetadataId(i);
            att_metadata_id[i] = (int) ds.readVarLengthUInt();
            att_metadata[i] = new SubMeta();
            DecodeMetadataElement(ds, att_metadata[i]);
        }

        file_metadata = new SubMeta();
        DecodeMetadataElement(ds, file_metadata);
    }

    /**
     * 4.4 ParseMetadataElement()
     */
    private void ParseMetadataElement(DataInputStream ds, SubMeta metadata) throws IOException {
        metadata.num_entries = (int) ds.readVarLengthUInt();
        metadata.keys = new byte[metadata.num_entries][0];
        for (int i = 0; i < metadata.num_entries; ++i) {
            int sz = ds.readUByte();
            metadata.keys[i] = ds.readBytes(sz);
            sz = ds.readUByte();
            metadata.values[i] = ds.readBytes(sz);
        }
        metadata.num_sub_metadata = (int) ds.readVarLengthUInt();
        metadata.sub_metadata = new SubMeta[metadata.num_sub_metadata];
    }

    /**
     * 4.5 ParseSubMetadataKey()
     */
    private void ParseSubMetadataKey(DataInputStream ds, SubMeta metadata, int index) throws IOException {
        int sz = ds.readUByte();
        metadata.sub_metadata_key[index] = ds.readBytes(sz);
    }

    /**
     * 4.6 DecodeMetadataElement()
     */
    private void DecodeMetadataElement(DataInputStream ds, SubMeta metadata) throws IOException {
        ParseMetadataElement(ds, metadata);
        for (int i = 0; i < metadata.num_sub_metadata; ++i) {
            ParseSubMetadataKey(ds, metadata, i);
            DecodeMetadataElement(ds, metadata.sub_metadata[i]);
        }
    }

}
