
package science.unlicense.format.draco.model;

/**
 *
 * @author Johann Sorel
 */
public class SequentialEncoding {

    /**
     * Number of encoded points
     */
    public int num_points;

    public int connectivity_method;

}
