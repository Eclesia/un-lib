
package science.unlicense.format.draco.model;

/**
 *
 * @author Johann Sorel
 */
public class AttributeTraversal {

    /**
     * Current attribute decoder type
     */
    public int curr_att_dec;
    /**
     * Current attribute within a decoder type
     */
    public int curr_att;
    /**
     * Array of the last vertex visited per attribute
     */
    public int vertex_visited_point_ids;

    public int att_connectivity_seam_opp;
    public int att_connectivity_seam_src;
    public int att_connectivity_seam_dest;
    public int corner_to_point_map;
    /**
     * Array of bools signifying if the corner’s opposite edge is on a seam
     */
    public int is_edge_on_seam_;
    /**
     * Array for storing the corner ids in the order their associated attribute entries were encoded
     */
    public int encoded_attribute_value_index_to_corner_map;
    /**
     * Array for storing encoding order of attribute entries for each vertex
     */
    public int vertex_to_encoded_attribute_value_index_map;

    public int indices_map_;
    /**
     * Current rans zero probability
     */
    public int prediction_rans_prob_zero;
    /**
     * Current size of rans encoded data
     */
    public int prediction_rans_data_size;
    /**
     * Ans encoded prediction data for an attribute
     */
    public int prediction_rans_data_buffer;
    /**
     * Current number of orientations for encoded Texture data
     */
    public int tex_coords_num_orientations;
    /**
     * Array of available corners
     */
    public int traversal_stacks_;
    /**
     * Current best available priority
     */
    public int best_priority_;
    /**
     * Array of current degree prediction for each vertex
     */
    public int prediction_degree_;

    public int constrained_multi_num_flags;

}
