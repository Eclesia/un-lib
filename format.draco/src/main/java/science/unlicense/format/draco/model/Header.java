
package science.unlicense.format.draco.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class Header {

    /**
     * Must equal “DRACO”
     */
    public Chars draco_string;
    /**
     * bitstream major version number
     */
    public int major_version;
    /**
     * bitstream minor version number
     */
    public int minor_version;
    /**
     * 0: POINT_CLOUD
     * 1: TRIANGULAR_MESH
     */
    public int encoder_type;
    /**
     * 0: MESH_SEQUENTIAL_ENCODING
     * 1: MESH_EDGEBREAKER_ENCODING
     */
    public int encoder_method;
    public int flags;

    public void read(DataInputStream ds) throws IOException {
        draco_string = new Chars(ds.readBytes(5));
        if (!new Chars("DRACO").equals(draco_string)) {
            throw new IOException(ds, "Invalid signature");
        }
        major_version = ds.readUByte();
        minor_version = ds.readUByte();
        encoder_type = ds.readUByte();
        encoder_method = ds.readUByte();
        flags = ds.readUShort();
    }

}
