
package science.unlicense.format.draco;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Specification :
 * https://google.github.io/draco/spec/
 *
 * @author Johann Sorel
 */
public class DracoFormat extends AbstractModel3DFormat{

    public static final DracoFormat INSTANCE = new DracoFormat();

    private DracoFormat() {
        super(new Chars("Draco"));
        shortName = new Chars("Draco");
        longName = new Chars("Draco");
        signatures.add(new byte[]{'D','R','A','C','O'});
    }

    @Override
    public Store open(Object input) throws IOException {
        return new DracoStore(input);
    }

}
