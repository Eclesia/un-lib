
package science.unlicense.format.draco.model;

/**
 *
 * @author Johann Sorel
 */
public class EdgeBreakerValenceTraversal {

    /**
     * Array of number of context symbols
     */
    public int ebv_context_counters;
    /**
     * Array of encoded context symbol data
     */
    public int ebv_context_symbols;
    /**
     * Index to the current valence
     */
    public int active_context_;
    /**
     * Array of current vertices valences
     */
    public int vertex_valences_;

}
