
package science.unlicense.format.zip;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.zip.model.ZipBlock;
import science.unlicense.format.zip.model.ZipCentralDirectory;
import science.unlicense.format.zip.model.ZipCentralDirectoryEnd;
import science.unlicense.format.zip.model.ZipFileEntry;

/**
 *
 * @author Johann Sorel
 */
public class ZipIteratorTest {

    @Test
    public void testRead() throws IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/zip/sample.zip"));
        final ByteInputStream bi = path.createInputStream();
        final ZipIterator ite = new ZipIterator(path, bi, CharEncodings.DEFAULT);

        Object candidate;

        //third entry--------------------------------
        Assert.assertTrue(ite.hasNext());
        candidate = ite.next();
        Assert.assertTrue(candidate instanceof ZipFileEntry);
        final ZipFileEntry entry3 = (ZipFileEntry) candidate;
        Assert.assertEquals(new Chars("file1.txt"),    entry3.name);
        Assert.assertArrayEquals(new Chars[]{new Chars("file1.txt")}, entry3.getPathChars());
        Assert.assertEquals(new Chars("file1.txt"),    entry3.getSimpleName());
        Assert.assertEquals(Chars.EMPTY,             entry3.getParentPath());

        //fourth entry--------------------------------
        Assert.assertTrue(ite.hasNext());
        candidate = ite.next();
        Assert.assertTrue(candidate instanceof ZipFileEntry);
        final ZipFileEntry entry4 = (ZipFileEntry) candidate;
        Assert.assertEquals(new Chars("file2.txt"),    entry4.name);
        Assert.assertArrayEquals(new Chars[]{new Chars("file2.txt")}, entry4.getPathChars());
        Assert.assertEquals(new Chars("file2.txt"),    entry4.getSimpleName());
        Assert.assertEquals(Chars.EMPTY,             entry4.getParentPath());

        //first entry--------------------------------
        Assert.assertTrue(ite.hasNext());
        candidate = ite.next();
        Assert.assertTrue(candidate instanceof ZipFileEntry);
        final ZipFileEntry entry1 = (ZipFileEntry) candidate;
        Assert.assertEquals(new Chars("folder/"),  entry1.name);
        Assert.assertArrayEquals(new Chars[]{new Chars("folder")}, entry1.getPathChars());
        Assert.assertEquals(new Chars("folder"),   entry1.getSimpleName());
        Assert.assertEquals(Chars.EMPTY,         entry1.getParentPath());

        //second entry--------------------------------
        Assert.assertTrue(ite.hasNext());
        candidate = ite.next();
        Assert.assertTrue(candidate instanceof ZipFileEntry);
        final ZipFileEntry entry2 = (ZipFileEntry) candidate;
        Assert.assertEquals(new Chars("folder/file3.txt"), entry2.name);
        Assert.assertArrayEquals(new Chars[]{new Chars("folder"), new Chars("file3.txt")}, entry2.getPathChars());
        Assert.assertEquals(new Chars("file3.txt"),        entry2.getSimpleName());
        Assert.assertEquals(new Chars("folder/"),          entry2.getParentPath());

        //second entry--------------------------------
        Assert.assertTrue(ite.hasNext());
        candidate = ite.next();
        Assert.assertTrue(candidate instanceof ZipFileEntry);
        final ZipFileEntry entry5 = (ZipFileEntry) candidate;
        Assert.assertEquals(new Chars("folder/file4.txt"), entry5.name);
        Assert.assertArrayEquals(new Chars[]{new Chars("folder"), new Chars("file4.txt")}, entry5.getPathChars());
        Assert.assertEquals(new Chars("file4.txt"),        entry5.getSimpleName());
        Assert.assertEquals(new Chars("folder/"),          entry5.getParentPath());

        //first central directory--------------------------------
        Assert.assertTrue(ite.hasNext());
        candidate = ite.next();
        Assert.assertTrue(candidate instanceof ZipCentralDirectory);
        final ZipCentralDirectory direc1 = (ZipCentralDirectory) candidate;

        //second central directory--------------------------------
        Assert.assertTrue(ite.hasNext());
        candidate = ite.next();
        Assert.assertTrue(candidate instanceof ZipCentralDirectory);
        final ZipCentralDirectory direc2 = (ZipCentralDirectory) candidate;

        //third central directory--------------------------------
        Assert.assertTrue(ite.hasNext());
        candidate = ite.next();
        Assert.assertTrue(candidate instanceof ZipCentralDirectory);
        final ZipCentralDirectory direc3 = (ZipCentralDirectory) candidate;

        //fourth central directory--------------------------------
        Assert.assertTrue(ite.hasNext());
        candidate = ite.next();
        Assert.assertTrue(candidate instanceof ZipCentralDirectory);
        final ZipCentralDirectory direc4 = (ZipCentralDirectory) candidate;

        //fourth central directory--------------------------------
        Assert.assertTrue(ite.hasNext());
        candidate = ite.next();
        Assert.assertTrue(candidate instanceof ZipCentralDirectory);
        final ZipCentralDirectory direc5 = (ZipCentralDirectory) candidate;

        //central directory end----------------------------------
        Assert.assertTrue(ite.hasNext());
        candidate = ite.next();
        Assert.assertTrue(candidate instanceof ZipCentralDirectoryEnd);
        final ZipCentralDirectoryEnd end = (ZipCentralDirectoryEnd) candidate;


        Assert.assertFalse(ite.hasNext());
    }

    /**
     * Read and rewrite zip blocks.
     * Byte stream should be exactly the same.
     *
     * @throws IOException
     */
    @Test
    public void testReadWrite() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/zip/sample.zip"));
        final ByteInputStream bi = path.createInputStream();
        final ZipIterator ite = new ZipIterator(path, bi, CharEncodings.DEFAULT);

        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ds = new DataOutputStream(out, Endianness.LITTLE_ENDIAN);

        while (ite.hasNext()) {
            ZipBlock block = (ZipBlock) ite.next();
            block.write(ds, CharEncodings.UTF_8);
        }

        ds.flush();
        ds.close();
        byte[] writeData = out.getBuffer().toArrayByte();
        byte[] readData = IOUtilities.readAll(path.createInputStream());

        Assert.assertArrayEquals(readData, writeData);

    }

}
