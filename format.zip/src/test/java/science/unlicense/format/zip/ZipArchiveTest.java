
package science.unlicense.format.zip;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class ZipArchiveTest {

    @Test
    public void testArchive() throws IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/zip/sample.zip"));
        final ZipArchive archive = new ZipArchive(null,path);

        Assert.assertTrue(archive.isContainer());
        Assert.assertEquals(new Chars("sample.zip"),archive.getName());
        Assert.assertEquals(3, archive.getChildren().getSize());

        final Path file1 = (Path) archive.getChildren().toArray()[0];
        Assert.assertEquals(new Chars("file1.txt"),file1.getName());
        Assert.assertFalse(file1.isContainer());
        assertContent(new Chars("hello world\n"),file1);

        final Path file2 = (Path) archive.getChildren().toArray()[1];
        Assert.assertEquals(new Chars("file2.txt"),file2.getName());
        Assert.assertFalse(file2.isContainer());
        assertContent(new Chars("I like \ncheese.\n"),file2);

        final Path folder = (Path) archive.getChildren().toArray()[2];
        Assert.assertEquals(new Chars("folder"),folder.getName());
        Assert.assertTrue(folder.isContainer());
        Assert.assertEquals(2, folder.getChildren().getSize());

        final Path file3 = (Path) folder.getChildren().toArray()[0];
        Assert.assertEquals(new Chars("file3.txt"),file3.getName());
        Assert.assertFalse(file3.isContainer());
        assertContent(new Chars("Johann is awesome.\n"),file3);

        final Path file4 = (Path) folder.getChildren().toArray()[1];
        Assert.assertEquals(new Chars("file4.txt"),file4.getName());
        Assert.assertFalse(file4.isContainer());
        assertContent(new Chars("just to be sure."),file4);

    }

    @Test
    public void testZipPath() throws IOException{

        final Path file2 = Paths.resolve(new Chars("mod:/science/unlicense/format/zip/sample.zip!/file2.txt"));
        Assert.assertEquals(new Chars("mod:/science/unlicense/format/zip/sample.zip!/file2.txt"), file2.toURI());
        Assert.assertFalse(file2.isContainer());
        assertContent(new Chars("I like \ncheese.\n"),file2);

        final Path file1 = Paths.resolve(new Chars("mod:/science/unlicense/format/zip/sample.zip!/file1.txt"));
        Assert.assertFalse(file1.isContainer());
        assertContent(new Chars("hello world\n"),file1);

        final Path file3 = Paths.resolve(new Chars("mod:/science/unlicense/format/zip/sample.zip!/folder/file3.txt"));
        Assert.assertFalse(file3.isContainer());
        assertContent(new Chars("Johann is awesome.\n"),file3);

    }

    private void assertContent(Chars result, Path path) throws IOException{
        final ByteInputStream bi = path.createInputStream();
        final byte[] buffer = IOUtilities.readAll(bi);
        bi.dispose();
        Assert.assertEquals(result, new Chars(buffer));
    }

}
