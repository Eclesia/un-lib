
package science.unlicense.format.zip;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.VirtualPath;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.zip.model.ZipCentralDirectory;
import science.unlicense.format.zip.model.ZipCentralDirectoryEnd;
import science.unlicense.format.zip.model.ZipFileEntry;

/**
 *
 * @author Johann Sorel
 */
public class ZipWriterTest {

    @Test
    public void testWriteFile() throws IOException{

        VirtualPath.Ram archive = new VirtualPath.Ram(Chars.EMPTY);

        VirtualPath.Ram root = new VirtualPath.Ram(Chars.EMPTY);
        Path path = root.resolve(new Chars("message.txt"));
        ByteOutputStream out = path.createOutputStream();
        out.write(new Chars("hello").toBytes(CharEncodings.UTF_8));
        out.close();

        ZipWriter writer = new ZipWriter();
        writer.setOutput(archive);
        writer.write(root);


        final ByteInputStream bi = archive.createInputStream();
        final ZipIterator ite = new ZipIterator(path, bi, CharEncodings.DEFAULT);

        final ZipFileEntry entry = (ZipFileEntry) ite.next();
        Assert.assertEquals(new Chars("message.txt"), entry.name);
        Assert.assertEquals(5, entry.sizeCompressed);
        Assert.assertEquals(5, entry.sizeUncompressed);

        final ZipCentralDirectory dir = (ZipCentralDirectory) ite.next();
        Assert.assertEquals(new Chars("message.txt"), dir.name);
        Assert.assertEquals(5, dir.sizeCompressed);
        Assert.assertEquals(5, dir.sizeUncompressed);

        final ZipCentralDirectoryEnd end = (ZipCentralDirectoryEnd) ite.next();
        Assert.assertFalse(ite.hasNext());

    }

    @Test
    public void testWriteFolder() throws IOException{

        VirtualPath.Ram archive = new VirtualPath.Ram(Chars.EMPTY);

        VirtualPath.Ram root = new VirtualPath.Ram(Chars.EMPTY);
        root.createContainer();
        Path folder = root.resolve(new Chars("folder"));
        folder.createContainer();
        Path path = folder.resolve(new Chars("message.txt"));
        path.createLeaf();
        ByteOutputStream out = path.createOutputStream();
        out.write(new Chars("hello").toBytes(CharEncodings.UTF_8));
        out.close();

        ZipWriter writer = new ZipWriter();
        writer.setOutput(archive);
        writer.write(root);


        final ByteInputStream bi = archive.createInputStream();
        final ZipIterator ite = new ZipIterator(path, bi, CharEncodings.DEFAULT);

        final ZipFileEntry folderEntry = (ZipFileEntry) ite.next();
        Assert.assertEquals(new Chars("folder/"), folderEntry.name);
        Assert.assertEquals(0, folderEntry.sizeCompressed);
        Assert.assertEquals(0, folderEntry.sizeUncompressed);

        final ZipFileEntry fileEntry = (ZipFileEntry) ite.next();
        Assert.assertEquals(new Chars("folder/message.txt"), fileEntry.name);
        Assert.assertEquals(5, fileEntry.sizeCompressed);
        Assert.assertEquals(5, fileEntry.sizeUncompressed);

        final ZipCentralDirectory folderDir = (ZipCentralDirectory) ite.next();
        Assert.assertEquals(new Chars("folder/"), folderDir.name);
        Assert.assertEquals(0, folderDir.sizeCompressed);
        Assert.assertEquals(0, folderDir.sizeUncompressed);

        final ZipCentralDirectory fileDir = (ZipCentralDirectory) ite.next();
        Assert.assertEquals(new Chars("folder/message.txt"), fileDir.name);
        Assert.assertEquals(5, fileDir.sizeCompressed);
        Assert.assertEquals(5, fileDir.sizeUncompressed);

        final ZipCentralDirectoryEnd end = (ZipCentralDirectoryEnd) ite.next();
        Assert.assertFalse(ite.hasNext());

    }

}
