
package science.unlicense.format.zip.model;

import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.zip.ZipMetaModel;

/**
 *
 * end of central dir signature    4 bytes  (0x06054b50)
 * number of this disk             2 bytes
 * number of the disk with the
 * start of the central directory  2 bytes
 * total number of entries in the
 * central directory on this disk  2 bytes
 * total number of entries in
 * the central directory           2 bytes
 * size of the central directory   4 bytes
 * offset of start of central
 * directory with respect to
 * the starting disk number        4 bytes
 * .ZIP file comment length        2 bytes
 * .ZIP file comment       (variable size)
 *
 * @author Johann Sorel
 */
public class ZipCentralDirectoryEnd extends ZipBlock {

    public int numberDisk;
    public int numberDiskWithCentralDir;
    public int numberEntryOnDisk;
    public int numberEntryInCentralDir;
    public int directorySize;
    public int offsetCentralDir;
    public Chars comment = Chars.EMPTY;

    @Override
    public int getSignature() {
        return ZipMetaModel.SIGNATURE_END_CENTRAL_DIRECTORY;
    }

    @Override
    public void read(DataInputStream ds, CharEncoding encoding) throws IOException {
        numberDisk                  = ds.readUShort();
        numberDiskWithCentralDir    = ds.readUShort();
        numberEntryOnDisk           = ds.readUShort();
        numberEntryInCentralDir     = ds.readUShort();
        directorySize               = ds.readInt();
        offsetCentralDir            = ds.readInt();
        final int commentLength     = ds.readUShort();
        comment = new Chars(ds.readFully(new byte[commentLength]),encoding);
    }

    @Override
    public void write(DataOutputStream ds, CharEncoding encoding) throws IOException {
        ds.writeInt(getSignature());
        ds.writeUShort(numberDisk);
        ds.writeUShort(numberDiskWithCentralDir);
        ds.writeUShort(numberEntryOnDisk);
        ds.writeUShort(numberEntryInCentralDir);
        ds.writeInt(directorySize);
        ds.writeInt(offsetCentralDir);
        final byte[] comment = this.comment.toBytes(encoding);
        ds.writeUShort(comment.length);
        ds.write(comment);
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("ZipCentralDirectoryEnd"), new Object[]{
            "numberDisk : " + numberDisk,
            "numberDiskWithCentralDir : " + numberDiskWithCentralDir,
            "numberEntryOnDisk : " + numberEntryOnDisk,
            "numberEntryInCentralDir : " + numberEntryInCentralDir,
            "directorySize : " + directorySize,
            "offsetCentralDir : " + offsetCentralDir,
            "comment : " + comment
        });
    }

}
