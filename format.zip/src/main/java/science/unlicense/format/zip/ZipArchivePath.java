
package science.unlicense.format.zip;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.SeekableByteBuffer;
import science.unlicense.encoding.api.path.AbstractPath;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.encoding.impl.io.deflate.DeflateInputStream;
import science.unlicense.format.zip.model.ZipCentralDirectory;
import science.unlicense.format.zip.model.ZipFileEntry;

/**
 * A Path inside a zip.
 *
 * @author Johann Sorel
 */
public class ZipArchivePath extends AbstractPath {

    private final ZipCentralDirectory directory;
    private final ZipFileEntry entry;
    private final Path parent;
    private final Sequence children = new ArraySequence();

    public ZipArchivePath(Path parent, ZipCentralDirectory directory, ZipFileEntry entry) {
        this.parent = parent;
        this.directory = directory;
        this.entry = entry;
    }

    @Override
    public Chars getName() {
        final Chars[] path = entry.getPathChars();
        return path[path.length-1];
    }

    @Override
    public Path getParent() {
        return parent;
    }

    @Override
    public boolean isContainer() throws IOException {
        return entry.isContainer();
    }

    @Override
    public boolean exists() throws IOException {
        return true;
    }

    @Override
    public Path resolve(Chars address) {
        return getResolver().resolve(entry.name.concat(address));
    }

    @Override
    public PathResolver getResolver() {
        return parent.getResolver();
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        final ByteInputStream clip = createRawInputStream();

        if (entry.compression == ZipMetaModel.COMPRESSION_NONE) {
            //no compression, we can return the stream
            return clip;
        }if (entry.compression == ZipMetaModel.COMPRESSION_DEFLATE) {
            //Deflate compression
            final DeflateInputStream decompressed = new DeflateInputStream(clip);
            return decompressed;
        } else {
            throw new IOException("Compression  "+entry.compression+" not supported.");
        }
    }

    /**
     * Raw stream, not decompressed.
     * @return
     * @throws IOException
     */
    public ByteInputStream createRawInputStream() throws IOException {
        return entry.byteResource.createInputStream();
    }

    @Override
    public Chars toURI() {
        if (parent instanceof ZipArchive) {
            //root element inside the zip
            return parent.toURI().concat(new Chars("!/")).concat(getName());
        } else {
            return parent.toURI().concat('/').concat(getName());
        }
    }

    @Override
    public Chars toChars() {
        return new Chars("Zip entry : "+getName());
    }

    @Override
    public boolean createContainer() throws IOException {
        throw new IOException("Not supported yet.");
    }

    @Override
    public boolean createLeaf() throws IOException {
        throw new IOException("Not supported yet.");
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        throw new IOException("Not supported yet.");
    }

    @Override
    public void delete() throws IOException {
        throw new IOException("Not supported yet.");
    }

    @Override
    public Object getPathInfo(Chars key) {
        return null;
    }

    @Override
    public SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException {
        throw new IOException("Not supported yet.");
    }

    @Override
    public Collection getChildren() {
        return Collections.readOnlyCollection(children);
    }

    void addChild(Path child) {
        children.add(child);
    }

}
