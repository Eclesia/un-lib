
package science.unlicense.format.zip.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.zip.ZipMetaModel;


/**
 *
 * header signature                4 bytes  (0x05054b50)
 * size of data                    2 bytes
 * signature data (variable size)
 *
 * @author Johann Sorel
 */
public class ZipDigitalSignature extends ZipBlock {

    public byte[] signature = Arrays.ARRAY_BYTE_EMPTY;

    @Override
    public int getSignature() {
        return ZipMetaModel.SIGNATURE_DIGITAL_SIGNATURE;
    }

    @Override
    public void read(DataInputStream ds, CharEncoding encoding) throws IOException {
        final int signatureSize = ds.readUShort();
        signature = ds.readFully(new byte[signatureSize]);
    }

    @Override
    public void write(DataOutputStream ds, CharEncoding encoding) throws IOException {
        ds.writeInt(getSignature());
        ds.writeUShort(signature.length);
        ds.write(signature);
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("ZipDigitalSignature"), new Object[]{
            "signature : " + CObjects.toChars(signature, 64)
        });
    }

}
