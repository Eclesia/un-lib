
package science.unlicense.format.zip.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * A Zip Block element.
 *
 * @author Johann Sorel
 */
public abstract class ZipBlock extends CObject {

    public long fileOffset;

    public abstract int getSignature();

    /**
     * Read zip block, does not read starting signature, it is expected to be read previously.
     *
     * @param ds
     * @param encoding
     * @throws IOException
     */
    public abstract void read(DataInputStream ds, CharEncoding encoding) throws IOException;

    /**
     * Write zip block, writes block signature.
     *
     * @param ds
     * @param encoding
     * @throws IOException
     */
    public abstract void write(DataOutputStream ds, CharEncoding encoding) throws IOException;

}
