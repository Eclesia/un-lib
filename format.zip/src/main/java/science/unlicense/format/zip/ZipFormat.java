

package science.unlicense.format.zip;

import science.unlicense.archive.api.AbstractArchiveFormat;
import science.unlicense.archive.api.Archive;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocumentType;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;

/**
 * http://www.pkware.com/documents/casestudies/APPNOTE.TXT
 *
 * @author Johann Sorel
 */
public class ZipFormat extends AbstractArchiveFormat {

    public static final ZipFormat INSTANCE = new ZipFormat();
    private static final Chars SUFFIX = Chars.constant(".zip");

    private static final DocumentType PARAMETERS_TYPE = new DefaultDocumentType(new Chars("zip"), null,null,true,new FieldType[]{PARAM_ENCODING},null);

    private ZipFormat() {
        super(new Chars("zip"));
        shortName = new Chars("ZIP");
        longName = new Chars("ZIP");
        mimeTypes.add(new Chars("application/zip"));
        extensions.add(new Chars("zip"));
        extensions.add(new Chars("zipx"));
    }

    @Override
    public DocumentType getParameters() {
        return PARAMETERS_TYPE;
    }

    @Override
    public boolean canDecode(Object candidate) {
        if (candidate instanceof Path) {
            return ((Path) candidate).getName().toLowerCase().endsWith(SUFFIX);
        }
        return false;
    }

    @Override
    public Archive open(Object candidate) throws IOException {
        if (candidate instanceof Path) {
            final CharEncoding enc = CharEncodings.DEFAULT;
            final ZipArchive arc = new ZipArchive(this, (Path) candidate);
            arc.setEncoding(enc);
            return arc;
        } else if (candidate instanceof Document) {
            final Document params = (Document) candidate;
            CharEncoding enc = CharEncodings.DEFAULT;
            if (params!=null) {
                Object value = params.getPropertyValue(PARAM_ENCODING.getId());
                if (value!=null) enc = (CharEncoding) value;
            }
            final ZipArchive arc = new ZipArchive(this, (Path) candidate);
            arc.setEncoding(enc);
            return arc;
        }

        throw new IOException("Unsupported input");
    }

    @Override
    public boolean isAbsolute() {
        return false;
    }

    @Override
    public boolean canCreate(Path base) throws IOException {
        return base.getName().toLowerCase().endsWith(SUFFIX);
    }

    @Override
    public PathResolver createResolver(Path base) throws IOException {
        return open(base);
    }

}
