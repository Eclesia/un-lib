
package science.unlicense.format.zip.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.zip.ZipMetaModel;

/**
 * archive extra data signature    4 bytes  (0x08064b50)
 * extra field length              4 bytes
 * extra field data                (variable size)
 *
 * @author Johann Sorel
 */
public class ZipExtraRecord extends ZipBlock {

    public byte[] fieldData = Arrays.ARRAY_BYTE_EMPTY;

    @Override
    public int getSignature() {
        return ZipMetaModel.SIGNATURE_EXTRA_RECORD;
    }

    @Override
    public void read(DataInputStream ds, CharEncoding encoding) throws IOException {
        final int fieldLength = ds.readUShort();
        fieldData = ds.readFully(new byte[fieldLength]);
    }

    @Override
    public void write(DataOutputStream ds, CharEncoding encoding) throws IOException {
        ds.writeInt(getSignature());
        ds.writeUShort(fieldData.length);
        ds.write(fieldData);
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("ZipExtraRecord"), new Object[]{
            "fieldData : " + CObjects.toChars(fieldData, 64)
        });
    }

}
