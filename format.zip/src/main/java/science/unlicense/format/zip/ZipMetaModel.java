
package science.unlicense.format.zip;

/**
 *
 * Documentation from spécification :
 * http://www.pkware.com/documents/casestudies/APPNOTE.TXT
 *
 * @author Johann Sorel
 */
public final class ZipMetaModel {

    public static final int SIGNATURE_LOCAL_FILE = 67324752;
    public static final int SIGNATURE_EXTRA_RECORD = 134630224;
    public static final int SIGNATURE_CENTRAL_DIRECTORY = 33639248;
    public static final int SIGNATURE_DIGITAL_SIGNATURE = 84233040;
    public static final int SIGNATURE_ZIP64_END_CENTRAL_DIRECTORY_RECORD = 101075792;
    public static final int SIGNATURE_ZIP64_END_CENTRAL_DIRECTORY_LOCATOR = 117853008;
    public static final int SIGNATURE_END_CENTRAL_DIRECTORY = 101010256;

    public static final int MSDOS       = 0; //MS-DOS and OS/2 (FAT / VFAT / FAT32 file systems)
    public static final int AMIGA       = 1; //Amiga
    public static final int OPENVMS     = 2; //OpenVMS
    public static final int UNIX        = 3; //UNIX
    public static final int VM_CMS      = 4; //VM/CMS
    public static final int ATARI_ST    = 5; //Atari ST
    public static final int OS2         = 6; //OS/2 H.P.F.S.
    public static final int MACINTOSH   = 7; //Macintosh
    public static final int ZSYSTEM     = 8; //Z-System
    public static final int CP_M        = 9; //CP/M
    public static final int WINDOWS_NTFS= 10; //Windows NTFS
    public static final int MVS         = 11; //MVS (OS/390 - Z/OS)
    public static final int VSE         = 12; //VSE
    public static final int ACORN_RISC  = 13; //Acorn Risc
    public static final int VFAT        = 14; //VFAT
    public static final int ALTERNATE_MVS= 15; //alternate MVS
    public static final int BEOS        = 16; //BeOS
    public static final int TANDEM      = 17; //Tandem
    public static final int OS_400      = 18; //OS/400
    public static final int OS_X        = 19;  //OS X (Darwin)
    public static final int UNUSED      = 20; //thru 255 - unused


    // 4.4.4 general purpose bit flag: (2 bytes)

    /**
     * Bit 0: If set, indicates that the file is encrypted.
     */
    public static final int FLAG_ENCRYPTED = 1 << 0;

    /**
     * (For Method 6 - Imploding)
     *  Bit 1: If the compression method used was type 6,
     *      Imploding, then this bit, if set, indicates
     *      an 8K sliding dictionary was used.  If clear,
     *      then a 4K sliding dictionary was used.
     *  Bit 2: If the compression method used was type 6,
     *      Imploding, then this bit, if set, indicates
     *      3 Shannon-Fano trees were used to encode the
     *      sliding dictionary output.  If clear, then 2
     *      Shannon-Fano trees were used.
     *
     *   (For Methods 8 and 9 - Deflating)
     *   Bit 2  Bit 1
     *     0      0    Normal (-en) compression option was used.
     *     0      1    Maximum (-exx/-ex) compression option was used.
     *     1      0    Fast (-ef) compression option was used.
     *     1      1    Super Fast (-es) compression option was used.
     *
     *  (For Method 14 - LZMA)
     *  Bit 1: If the compression method used was type 14,
     *          LZMA, then this bit, if set, indicates
     *          an end-of-stream (EOS) marker is used to
     *          mark the end of the compressed data stream.
     *          If clear, then an EOS marker is not present
     *          and the compressed data size must be known
     *          to extract.
     *
     *   Note:  Bits 1 and 2 are undefined if the compression
     *          method is any other.
     */
    public static final int FLAG_COMPRESSION_PARAM0 = 1 << 1;
    public static final int FLAG_COMPRESSION_PARAM1 = 1 << 2;

    /**
     *   Bit 3: If this bit is set, the fields crc-32, compressed
     *          size and uncompressed size are set to zero in the
     *          local header.  The correct values are put in the
     *          data descriptor immediately following the compressed
     *          data.  (Note: PKZIP version 2.04g for DOS only
     *          recognizes this bit for method 8 compression, newer
     *          versions of PKZIP recognize this bit for any
     *          compression method.)
     */
    public static final int FLAG_CRC32 = 1 << 3;

    /**
     *   Bit 5: If this bit is set, this indicates that the file is
     *          compressed patched data.  (Note: Requires PKZIP
     *          version 2.70 or greater)
     */
    public static final int FLAG_COMPRESSED_PATCHED = 1 << 5;

    /**
     *   Bit 6: Strong encryption.  If this bit is set, you MUST
     *          set the version needed to extract value to at least
     *          50 and you MUST also set bit 0.  If AES encryption
     *          is used, the version needed to extract value MUST
     *          be at least 51. See the section describing the Strong
     *          Encryption Specification for details.  Refer to the
     *          section in this document entitled "Incorporating PKWARE
     *          Proprietary Technology into Your Product" for more
     *          information.
     */
    public static final int FLAG_ENCRYPTION = 1 << 6;

    /**
     *   Bit 11: Language encoding flag (EFS).  If this bit is set,
     *           the filename and comment fields for this file
     *           MUST be encoded using UTF-8. (see APPENDIX D)
     */
    public static final int FLAG_EFS = 1 << 11;

    /**
     *  Bit 13: Set when encrypting the Central Directory to indicate
     *          selected data values in the Local Header are masked to
     *          hide their actual values.  See the section describing
     *          the Strong Encryption Specification for details.  Refer
     *          to the section in this document entitled "Incorporating
     *          PKWARE Proprietary Technology into Your Product" for
     *          more information.
     */
    public static final int FLAG_HIDE = 1 << 13;


    /** 0 - The file is stored (no compression) */
    public static final int COMPRESSION_NONE = 0;
    /** 1 - The file is Shrunk */
    public static final int COMPRESSION_SHRUNK = 1;
    /** 2 - The file is Reduced with compression factor 1 */
    public static final int COMPRESSION_REDUCED_1 = 2;
    /** 3 - The file is Reduced with compression factor 2 */
    public static final int COMPRESSION_REDUCED_2 = 3;
    /** 4 - The file is Reduced with compression factor 3 */
    public static final int COMPRESSION_REDUCED_3 = 4;
    /** 5 - The file is Reduced with compression factor 4 */
    public static final int COMPRESSION_REDUCED_4 = 5;
    /** 6 - The file is Imploded */
    public static final int COMPRESSION_IMPLODED = 6;
    /** 7 - Reserved for Tokenizing compression algorithm */
    /** 8 - The file is Deflated */
    public static final int COMPRESSION_DEFLATE = 8;
    /** 9 - Enhanced Deflating using Deflate64(tm) */
    public static final int COMPRESSION_DEFLATE64 = 9;
    /** 10 - PKWARE Data Compression Library Imploding (old IBM TERSE) */
    public static final int COMPRESSION_OLDIMPLODING = 10;
    /** 11 - Reserved by PKWARE */
    /** 12 - File is compressed using BZIP2 algorithm */
    public static final int COMPRESSION_BZIP2 = 12;
    /** 13 - Reserved by PKWARE */
    /** 14 - LZMA (EFS) */
    public static final int COMPRESSION_LZMA = 14;
    /** 15 - Reserved by PKWARE */
    /** 16 - Reserved by PKWARE */
    /** 17 - Reserved by PKWARE */
    /** 18 - File is compressed using IBM TERSE (new) */
    public static final int COMPRESSION_IBM_TERSE = 18;
    /** 19 - IBM LZ77 z Architecture (PFS) */
    public static final int COMPRESSION_LZ777 = 19;
    /** 97 - WavPack compressed data */
    public static final int COMPRESSION_WAVPACK = 97;
    /** 98 - PPMd version I, Rev 1 */
    public static final int COMPRESSION_PPMd = 98;

    private ZipMetaModel() {}

}
