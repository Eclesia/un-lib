
package science.unlicense.format.zip.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.zip.ZipMetaModel;

/**
 *
 * central file header signature   4 bytes  (0x02014b50)
 * version made by                 2 bytes
 * version needed to extract       2 bytes
 * general purpose bit flag        2 bytes
 * compression method              2 bytes
 * last mod file time              2 bytes
 * last mod file date              2 bytes
 * crc-32                          4 bytes
 * compressed size                 4 bytes
 * uncompressed size               4 bytes
 * file name length                2 bytes
 * extra field length              2 bytes
 * file comment length             2 bytes
 * disk number start               2 bytes
 * internal file attributes        2 bytes
 * external file attributes        4 bytes
 * relative offset of local header 4 bytes
 *
 * file name (variable size)
 * extra field (variable size)
 * file comment (variable size)
 *
 * @author Johann Sorel
 */
public class ZipCentralDirectory extends ZipBlock {

    public int version_made;
    public int version_extract;
    public int flags;
    public int compression;
    public int last_modified_time;
    public int last_modified_date;
    public int crc32;
    public int sizeCompressed;
    public int sizeUncompressed;
    public int diskNumberStart;
    public int internalFileAttributes;
    public int externalFileAttributes;
    public int relativeOffsetLocalHeader;

    public Chars name;
    public byte[] extra = Arrays.ARRAY_BYTE_EMPTY;
    public Chars comment = Chars.EMPTY;

    @Override
    public int getSignature() {
        return ZipMetaModel.SIGNATURE_CENTRAL_DIRECTORY;
    }

    @Override
    public void read(DataInputStream ds, CharEncoding encoding) throws IOException {
        version_made                = ds.readUShort();
        version_extract             = ds.readUShort();
        flags                       = ds.readUShort();
        compression                 = ds.readUShort();
        last_modified_time          = ds.readUShort();
        last_modified_date          = ds.readUShort();
        crc32                       = ds.readInt();
        sizeCompressed              = ds.readInt();
        sizeUncompressed            = ds.readInt();
        final int filenameLength    = ds.readUShort();
        final int extraLength       = ds.readUShort();
        final int commentLength     = ds.readUShort();
        diskNumberStart             = ds.readUShort();
        internalFileAttributes      = ds.readUShort();
        externalFileAttributes      = ds.readInt();
        relativeOffsetLocalHeader   = ds.readInt();

        name = new Chars(ds.readFully(new byte[filenameLength]),encoding);
        extra = ds.readFully(new byte[extraLength]);
        comment = new Chars(ds.readFully(new byte[commentLength]),encoding);
    }

    @Override
    public void write(DataOutputStream ds, CharEncoding encoding) throws IOException {
        ds.writeInt(getSignature());
        ds.writeUShort(version_made);
        ds.writeUShort(version_extract);
        ds.writeUShort(flags);
        ds.writeUShort(compression);
        ds.writeUShort(last_modified_time);
        ds.writeUShort(last_modified_date);
        ds.writeInt(crc32);
        ds.writeInt(sizeCompressed);
        ds.writeInt(sizeUncompressed);
        final byte[] filename = name.toBytes(encoding);
        final byte[] comment = this.comment.toBytes(encoding);
        ds.writeUShort(filename.length);
        ds.writeUShort(extra.length);
        ds.writeUShort(comment.length);
        ds.writeUShort(diskNumberStart);
        ds.writeUShort(internalFileAttributes);
        ds.writeInt(externalFileAttributes);
        ds.writeInt(relativeOffsetLocalHeader);
        ds.write(filename);
        ds.write(extra);
        ds.write(comment);
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("ZipCentralDirectory"), new Object[]{
            "version_made : " + version_made,
            "version_extract : " + version_extract,
            "flags : " + flags,
            "compression : " + compression,
            "last_modified_time : " + last_modified_time,
            "last_modified_date : " + last_modified_date,
            "crc32 : " + crc32,
            "sizeCompressed : " + sizeCompressed,
            "sizeUncompressed : " + sizeUncompressed,
            "filename : " + name,
            "comment : " + comment,
            "extra : " + CObjects.toChars(extra, 64),
            "diskNumberStart : " + diskNumberStart,
            "internalFileAttributes : " + internalFileAttributes,
            "externalFileAttributes : " + externalFileAttributes,
            "relativeOffsetLocalHeader " + relativeOffsetLocalHeader
        });
    }
}
