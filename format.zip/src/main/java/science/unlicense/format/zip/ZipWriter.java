
package science.unlicense.format.zip;

import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.concurrent.api.VirtualPath;
import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.zip.model.ZipCentralDirectory;
import science.unlicense.format.zip.model.ZipCentralDirectoryEnd;
import science.unlicense.format.zip.model.ZipFileEntry;

/**
 *
 * @author Johann Sorel
 */
public class ZipWriter extends AbstractWriter {

    public static void main(String[] args) throws IOException {

        ZipWriter writer = new ZipWriter();
        writer.setOutput(Paths.resolveSystem(new Chars("/home/club/myzip.zip")));

        VirtualPath p = VirtualPath.onRam(Chars.EMPTY);
        Path c = p.resolve(new Chars("toto2.txt"));
        c.createOutputStream().write(new Chars("hello world!").toBytes());

        writer.write(p);

    }

    private DataOutputStream ds;
    private CharEncoding enc;
    private Sequence entries = new ArraySequence();

    public void write(Path datas) throws IOException {

        ds = getOutputAsDataStream(Endianness.LITTLE_ENDIAN);
        enc = CharEncodings.UTF_8;

        //write entries
        final Iterator ite = datas.getChildren().createIterator();
        while (ite.hasNext()) {
            final Path p = (Path) ite.next();
            write(datas, p);
        }

        //write central directory
        final long offsetCentralDir = ds.getByteOffset();
        final Iterator itedir = entries.createIterator();
        while (itedir.hasNext()) {
            final ZipCentralDirectory dir = (ZipCentralDirectory) itedir.next();
            dir.write(ds, enc);
        }
        final long directorySize = ds.getByteOffset() - offsetCentralDir;

        ZipCentralDirectoryEnd end = new ZipCentralDirectoryEnd();
        end.numberDisk = 0;
        end.numberEntryOnDisk = entries.getSize();
        end.numberEntryInCentralDir = entries.getSize();
        end.offsetCentralDir = (int) offsetCentralDir;
        end.directorySize = (int) directorySize;
        end.comment = Chars.EMPTY;

        end.write(ds, enc);
    }

    private void write(Path root, Path path) throws IOException {

        Chars relativePath = Paths.relativePath(root, path);
        relativePath = relativePath.truncate(2, -1);

        final long offset = ds.getByteOffset();

        final ZipFileEntry entry = new ZipFileEntry();
        entry.compression = 0;
        entry.name = relativePath;
        if (path.isContainer()) {
            entry.name = entry.name.concat(ZipFileEntry.DIRECTORY_SUFFIX);
        } else {
            entry.updateCRC();
            entry.byteResource = path;
        }

        final ZipCentralDirectory dir = new ZipCentralDirectory();
        dir.name = entry.name;
        dir.relativeOffsetLocalHeader = (int) offset;
        if (path.isContainer()) {
            dir.sizeCompressed = 0;
            dir.sizeUncompressed = 0;
        } else {
            dir.sizeCompressed = (int) path.getSize();
            dir.sizeUncompressed = (int) path.getSize();
        }
        dir.crc32 = entry.crc32;
        dir.flags = 0;

        entry.write(ds, enc);

        entries.add(dir);

        if (path.isContainer()) {
            final Iterator ite = path.getChildren().createIterator();
            while (ite.hasNext()) {
                final Path p = (Path) ite.next();
                write(root, p);
            }
        }

    }

}
