
package science.unlicense.format.zip.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteResource;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.impl.cryptography.hash.CRC32;
import science.unlicense.encoding.impl.cryptography.hash.HashInputStream;
import science.unlicense.format.zip.ZipMetaModel;

/**
 * A File entry in a Zip archive.
 *
 * local file header signature     4 bytes  (0x04034b50)
 * version needed to extract       2 bytes
 * general purpose bit flag        2 bytes
 * compression method              2 bytes
 * last mod file time              2 bytes
 * last mod file date              2 bytes
 * crc-32                          4 bytes
 * compressed size                 4 bytes
 * uncompressed size               4 bytes
 * file name length                2 bytes
 * extra field length              2 bytes
 *
 * file name (variable size)
 * extra field (variable size)
 *
 * @author Johann Sorel
 */
public class ZipFileEntry extends ZipBlock {

    public static final Char DIRECTORY_SUFFIX = new Char('/');

    public int version;
    public int flags;
    public int compression;
    public int last_modified_time;
    public int last_modified_date;
    public int crc32;
    public int sizeCompressed;
    public int sizeUncompressed;

    public Chars name;
    public byte[] extra = Arrays.ARRAY_BYTE_EMPTY;

    /**
     * Accessor to resource bytes.
     * This is the compressed resource bytes.
     */
    public ByteResource byteResource;
    /**
     * Additional information for the compressed data file offset
     */
    public long dataFileOffset;

    @Override
    public int getSignature() {
        return ZipMetaModel.SIGNATURE_LOCAL_FILE;
    }

    /**
     * File entry path chars from archive root
     * @return Chars array
     */
    public Chars[] getPathChars() {

        Chars name = this.name;
        if (isContainer()) {
            name = name.truncate(0, name.getCharLength()-1);
        }

        return name.split('/');
    }

    /**
     * Name without full path.
     * @return
     */
    public Chars getSimpleName() {
        final Chars[] path = getPathChars();
        return path[path.length-1];
    }

    /**
     * Path to entry, without file entry simple name.
     * @return
     */
    public Chars getParentPath() {
        final int size = getSimpleName().getCharLength();
        return name.truncate(0, name.getCharLength() - size - ((isContainer())?1:0) );
    }

    /**
     * Indicate if this file entry is a container/folder.
     * @return true if entry is a container
     */
    public boolean isContainer() {
        return name.endsWith(DIRECTORY_SUFFIX);
    }

    /**
     * Compute and update crc32 field.
     * @throws IOException
     */
    public void updateCRC() throws IOException {
        if (byteResource != null) {
            ByteInputStream in = byteResource.createInputStream();
            HashInputStream hasher = new HashInputStream(new CRC32(), in);
            while (hasher.skip(4096) != -1) {
                //skip to file end
            }
            in.dispose();
            crc32 = (int) hasher.hashValue();
        } else {
            crc32 = 0;
        }
    }

    @Override
    public void read(DataInputStream ds, CharEncoding encoding) throws IOException {
        version                  = ds.readUShort();
        flags                    = ds.readUShort();
        compression              = ds.readUShort();
        last_modified_time       = ds.readUShort();
        last_modified_date       = ds.readUShort();
        crc32                    = ds.readInt();
        sizeCompressed           = ds.readInt();
        sizeUncompressed         = ds.readInt();
        final int filenameLength = ds.readUShort();
        final int extraLength    = ds.readUShort();

        name = new Chars(ds.readFully(new byte[filenameLength]),encoding);
        extra = ds.readFully(new byte[extraLength]);
    }

    @Override
    public void write(DataOutputStream ds, CharEncoding encoding) throws IOException {

        //TODO
        if (byteResource != null) {
            sizeCompressed = (int) byteResource.getSize();
            sizeUncompressed = (int) byteResource.getSize();
        }

        ds.writeInt(getSignature());
        ds.writeUShort(version);
        ds.writeUShort(flags);
        ds.writeUShort(compression);
        ds.writeUShort(last_modified_time);
        ds.writeUShort(last_modified_date);
        ds.writeInt(crc32);
        ds.writeInt(sizeCompressed);
        ds.writeInt(sizeUncompressed);

        final byte[] name = this.name.toBytes(encoding);
        ds.writeUShort(name.length);
        ds.writeUShort(extra.length);
        ds.write(name);
        ds.write(extra);
        if (byteResource != null) {
            IOUtilities.copy(byteResource.createInputStream(), ds);
        }
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("ZipFileEntry"), new Object[]{
            "version : " + version,
            "flags : " + flags,
            "compression : " + compression,
            "last_modified_time : " + last_modified_time,
            "last_modified_date : " + last_modified_date,
            "crc32 : " + crc32,
            "sizeCompressed : " + sizeCompressed,
            "sizeUncompressed : " + sizeUncompressed,
            "name : " + name,
            "extra : " + CObjects.toChars(extra, 64)
        });
    }

}
