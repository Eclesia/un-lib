
package science.unlicense.format.zip.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.zip.ZipMetaModel;

/**
 *
 * zip64 end of central dir
 * signature                       4 bytes  (0x06064b50)
 * size of zip64 end of central
 * directory record                8 bytes
 * version made by                 2 bytes
 * version needed to extract       2 bytes
 * number of this disk             4 bytes
 * number of the disk with the
 * start of the central directory  4 bytes
 * total number of entries in the
 * central directory on this disk  8 bytes
 * total number of entries in the
 * central directory               8 bytes
 * size of the central directory   8 bytes
 * offset of start of central
 * directory with respect to
 * the starting disk number        8 bytes
 * zip64 extensible data sector    (variable size)
 *
 * @author Johann Sorel
 */
public class Zip64CentralDirectoryRecord extends ZipBlock {

    public long directoryRecord;
    public int version_made;
    public int version_extract;
    public int numberDisk;
    public int numberDiskInCentralDir;
    public long numberEntriesOnDisk;
    public long numberEntriesInCentralDir;
    public long centralDirSize;
    public long centralDirOffset;
    public byte[] extensionDataSector = Arrays.ARRAY_BYTE_EMPTY;

    @Override
    public int getSignature() {
        return ZipMetaModel.SIGNATURE_ZIP64_END_CENTRAL_DIRECTORY_RECORD;
    }

    @Override
    public void read(DataInputStream ds, CharEncoding encoding) throws IOException {
        directoryRecord             = ds.readLong();
        version_made                = ds.readUShort();
        version_extract             = ds.readUShort();
        numberDisk                  = ds.readInt();
        numberDiskInCentralDir      = ds.readInt();
        numberEntriesOnDisk         = ds.readLong();
        numberEntriesInCentralDir   = ds.readLong();
        centralDirSize              = ds.readLong();
        centralDirOffset            = ds.readLong();
    }

    @Override
    public void write(DataOutputStream ds, CharEncoding encoding) throws IOException {
        ds.writeInt(getSignature());
        ds.writeLong(directoryRecord);
        ds.writeUShort(version_made);
        ds.writeUShort(version_extract);
        ds.writeInt(numberDisk);
        ds.writeInt(numberDiskInCentralDir);
        ds.writeLong(numberEntriesOnDisk);
        ds.writeLong(numberEntriesInCentralDir);
        ds.writeLong(centralDirSize);
        ds.writeLong(centralDirOffset);
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("Zip64CentralDirectoryRecord"), new Object[]{
            "directoryRecord : " + directoryRecord,
            "version_made : " + version_made,
            "version_extract : " + version_extract,
            "numberDisk : " + numberDisk,
            "numberDiskInCentralDir : " + numberDiskInCentralDir,
            "numberEntriesOnDisk : " + numberEntriesOnDisk,
            "numberEntriesInCentralDir : " + numberEntriesInCentralDir,
            "centralDirSize : " + centralDirSize,
            "centralDirOffset : " + centralDirOffset,
            "extensionDataSector : " + CObjects.toChars(extensionDataSector, 64)
        });
    }
}
