

package science.unlicense.format.zip;

import science.unlicense.archive.api.Archive;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.logging.DefaultLogger;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.model.tree.NodeMessage;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.AbstractPath;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.encoding.api.store.Resource;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.zip.model.ZipCentralDirectory;
import science.unlicense.format.zip.model.ZipFileEntry;

/**
 *
 * @author Johann Sorel
 */
public class ZipArchive extends AbstractPath implements Archive {

    /**
     * Sort zip entries by path length.
     */
    private static final Sorter ENTRY_SORTER = new Sorter() {
        public int sort(Object first, Object second) {
            final Chars[] p1 = ((ZipFileEntry) first).getPathChars();
            final Chars[] p2 = ((ZipFileEntry) second).getPathChars();
            final int diff = Math.min(p1.length,p2.length);
            for (int i=0;i<diff;i++) {
                //sort by path names
                int c = p1[i].order(p2[i]);
                if (c!=0) return c;
            }
            if (p2.length>p1.length) {
                return -1;
            } else if (p2.length<p1.length) {
                return +1;
            } else {
                throw new RuntimeException("2 identical paths, not possible"+((ZipFileEntry) first).name);
            }
        }
    };

    private Logger logger = new DefaultLogger();
    private final PathFormat format;
    private final Path base;
    private Sequence children;
    private final Dictionary entries = new HashDictionary();
    private CharEncoding encoding = CharEncodings.DEFAULT;

    public ZipArchive(PathFormat format, Path base) {
        this.format = format;
        this.base = base;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public Object getInput() {
        return base;
    }

    @Override
    public void dispose() {
    }

    @Override
    public Resource find(Chars id) throws StoreException {
        return null;
    }

    /**
     * Must be set before any reading.
     *
     * @param enc not null
     */
    public void setEncoding(CharEncoding enc) {
        CObjects.ensureNotNull(enc);
        this.encoding = enc;
    }

    public CharEncoding getEncoding() {
        return encoding;
    }

    @Override
    public Format getFormat() {
        return (Format) format;
    }

    @Override
    public PathFormat getPathFormat() {
        return format;
    }

    @Override
    public boolean canHaveChildren() {
        return true;
    }

    @Override
    public Chars getName() {
        return base.getName();
    }

    @Override
    public Path getParent() {
        return base.getParent();
    }

    @Override
    public boolean isContainer() throws IOException {
        return true;
    }

    @Override
    public boolean exists() throws IOException {
        return base.exists();
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[]{NodeMessage.class};
    }

    @Override
    public Sequence getChildren() {
        analyze();
        return Collections.readOnlySequence(children);
    }

    @Override
    public Path resolve(Chars address) {
        if (address==null) return this;

        if (address.startsWith('/')) {
            address = address.truncate(1,-1);
        }

        analyze();
        Path candidate = (Path) entries.getValue(address);
        if (candidate == null) {
            candidate = (Path) entries.getValue(address.concat('/'));
        }
        return candidate;
    }

    @Override
    public PathResolver getResolver() {
        return this;
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        return base.createInputStream();
    }

    @Override
    public Chars toChars() {
        return new Chars("Zip archive : "+getName());
    }

    @Override
    public Chars toURI() {
        return base.toURI();
    }

    private void analyze() {
        if (children!=null) return;

        children = new ArraySequence();

        final ByteInputStream bi;
        try {
            bi = base.createInputStream();

            final ZipIterator ite = new ZipIterator(base, bi, encoding);
            final Sequence entries = new ArraySequence();
            while (ite.hasNext()) {
                final Object obj = ite.next();

                if (obj instanceof ZipCentralDirectory) {
                    final ZipCentralDirectory dir = (ZipCentralDirectory) obj;
                    //TODO we should use this, but for now we use the found file entries directly

                } else if (obj instanceof ZipFileEntry) {
                    final ZipFileEntry entry = (ZipFileEntry) obj;
                    entries.add(entry);
                }
            }
            bi.dispose();

            //sort entries by path length
            Collections.sort(entries, ENTRY_SORTER);

            //rebuild archive paths
            for (int i=0,n=entries.getSize();i<n;i++) {
                create((ZipFileEntry) entries.get(i));
            }

        } catch (IOException ex) {
            Loggers.get().log(ex, science.unlicense.common.api.logging.Logger.LEVEL_WARNING);
        }

    }

    private void create(ZipFileEntry entry) {
        final Chars[] path = entry.getPathChars();

        Path parent;
        if (path.length==1) {
            parent = this;
        } else {
            parent = resolve(entry.getParentPath());
        }

        if (parent == null) {
            //path structure is not or only partialy declared in file entries
            parent = this;
            Chars fullPath = Chars.EMPTY;
            for (int i=0;i<path.length-1;i++) {
                fullPath = fullPath.concat(path[i]).concat('/');
                parent = getOrCreateDirectory(parent, path[i], fullPath);
            }
        }

        final ZipArchivePath zp = new ZipArchivePath(parent, null, entry);
        entries.add(entry.name, zp);

        if (parent == this) {
            children.add(zp);
        } else {
            ((ZipArchivePath) parent).addChild(zp);
        }

    }

    private Path getOrCreateDirectory(Path parent, Chars name, Chars basePath) {
        Path path = resolve(name);
        if (path == null) {
            //create it
            final ZipFileEntry entry = new ZipFileEntry();
            entry.name = basePath;
            entry.compression = ZipMetaModel.COMPRESSION_NONE;
            final ZipArchivePath zp = new ZipArchivePath(parent, null, entry);
            path = zp;

            if (parent == this) {
                children.add(zp);
                entries.add(basePath, zp);
            } else {
                ((ZipArchivePath) parent).addChild(zp);
            }
        }

        return path;
    }

    // WRITING OPERATIONS NOT SUPPORTED YET ////////////////////////////////////

    @Override
    public boolean createContainer() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean createLeaf() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        throw new IOException("Not supported, this is a ZIP Archive, stream writing is not supported.");
    }

}
