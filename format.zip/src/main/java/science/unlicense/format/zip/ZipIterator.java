
package science.unlicense.format.zip;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteResource;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.impl.io.ClipByteResource;
import science.unlicense.encoding.impl.io.CounterInputStream;
import science.unlicense.format.zip.model.Zip64CentralDirectoryLocator;
import science.unlicense.format.zip.model.Zip64CentralDirectoryRecord;
import science.unlicense.format.zip.model.ZipBlock;
import science.unlicense.format.zip.model.ZipCentralDirectory;
import science.unlicense.format.zip.model.ZipCentralDirectoryEnd;
import science.unlicense.format.zip.model.ZipDigitalSignature;
import science.unlicense.format.zip.model.ZipExtraRecord;
import science.unlicense.format.zip.model.ZipFileEntry;

/**
 * Iterator on each block in the zip.
 *
 * @author Johann Sorel
 */
public class ZipIterator extends CObject {

    private final ByteResource archive;
    private final CounterInputStream cs;
    private final DataInputStream ds;
    private final CharEncoding encoding;
    private ZipBlock next = null;

    public ZipIterator(ByteResource archive, ByteInputStream ds, CharEncoding encoding) {
        this.archive = archive;
        this.cs = new CounterInputStream(ds);
        this.ds = new DataInputStream(cs, Endianness.LITTLE_ENDIAN);
        this.encoding = encoding;
    }

    public boolean hasNext() throws IOException {
        findNext();
        return next != null;
    }

    public Object next() throws IOException {
        findNext();
        if (next == null) throw new InvalidArgumentException("No more elements.");
        ZipBlock b = next;
        next = null;
        return b;
    }

    private void findNext() throws IOException{
        if (next != null) return;

        OUTER:
        while (true) {
            final long fileOffset = cs.getCounterValue();
            final int signature;
            try {
                signature = ds.readInt();
            } catch(IOException ex) {
                break;
            }
            switch (signature) {
                case ZipMetaModel.SIGNATURE_LOCAL_FILE:
                    final ZipFileEntry entry = new ZipFileEntry();
                    entry.read(ds, encoding);
                    //store the data file offset
                    entry.dataFileOffset = cs.getCounterValue();
                    entry.byteResource = new ClipByteResource(archive, entry.dataFileOffset, entry.sizeCompressed);

                    //TODO skip compressed data
                    ds.skipFully(entry.sizeCompressed);
                    next = entry;
                    next.fileOffset = fileOffset;
                    break OUTER;
                case ZipMetaModel.SIGNATURE_EXTRA_RECORD:
                    next = new ZipExtraRecord();
                    next.read(ds, encoding);
                    next.fileOffset = fileOffset;
                    break OUTER;
                case ZipMetaModel.SIGNATURE_CENTRAL_DIRECTORY:
                    next = new ZipCentralDirectory();
                    next.read(ds, encoding);
                    next.fileOffset = fileOffset;
                    break OUTER;
                case ZipMetaModel.SIGNATURE_DIGITAL_SIGNATURE:
                    next = new ZipDigitalSignature();
                    next.read(ds, encoding);
                    next.fileOffset = fileOffset;
                    break OUTER;
                case ZipMetaModel.SIGNATURE_ZIP64_END_CENTRAL_DIRECTORY_RECORD:
                    next = new Zip64CentralDirectoryRecord();
                    next.read(ds, encoding);
                    next.fileOffset = fileOffset;
                    break OUTER;
                case ZipMetaModel.SIGNATURE_ZIP64_END_CENTRAL_DIRECTORY_LOCATOR:
                    next = new Zip64CentralDirectoryLocator();
                    next.read(ds, encoding);
                    next.fileOffset = fileOffset;
                    break OUTER;
                case ZipMetaModel.SIGNATURE_END_CENTRAL_DIRECTORY:
                    next = new ZipCentralDirectoryEnd();
                    next.read(ds, encoding);
                    next.fileOffset = fileOffset;
                    break OUTER;
                default:
                    throw new IOException("Unknowned tag : " + signature);
            }
        }
    }


    public static void print(Path path) throws IOException {
        final ByteInputStream bi = path.createInputStream();
        final ZipIterator ite = new ZipIterator(path, bi, CharEncodings.DEFAULT);
        while(ite.hasNext()) {
            ite.next();
            //System.out.println(ite.next());
        }
    }
}
