
package science.unlicense.format.zip.model;

import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.zip.ZipMetaModel;

/**
 * zip64 end of central dir locator
 * signature                       4 bytes  (0x07064b50)
 * number of the disk with the
 * start of the zip64 end of
 * central directory               4 bytes
 * relative offset of the zip64
 * end of central directory record 8 bytes
 * total number of disks           4 bytes
 *
 * @author Johann Sorel
 */
public class Zip64CentralDirectoryLocator extends ZipBlock {

    public int diskNumber;
    public long relativeOffset;
    public int numberOfDisks;

    @Override
    public int getSignature() {
        return ZipMetaModel.SIGNATURE_ZIP64_END_CENTRAL_DIRECTORY_LOCATOR;
    }

    @Override
    public void read(DataInputStream ds, CharEncoding encoding) throws IOException {
        diskNumber      = ds.readInt();
        relativeOffset  = ds.readLong();
        numberOfDisks   = ds.readInt();
    }

    @Override
    public void write(DataOutputStream ds, CharEncoding encoding) throws IOException {
        ds.writeInt(getSignature());
        ds.writeInt(diskNumber);
        ds.writeLong(relativeOffset);
        ds.writeInt(numberOfDisks);
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("Zip64CentralDirectoryLocator"), new Object[]{
            "diskNumber : " + diskNumber,
            "relativeOffset : " + relativeOffset,
            "numberOfDisks : " + numberOfDisks
        });
    }
}
