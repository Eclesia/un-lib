
package science.unlicense.format.blend;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.opengl.mesh.GLMesh;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.tessellation.DisplacementTessellator;
import science.unlicense.engine.opengl.tessellation.Tessellator;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.MorphTarget;
import science.unlicense.gpu.api.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.api.Model3Ds;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.model3d.impl.physic.JointTimeSerie;
import science.unlicense.model3d.impl.physic.SkeletonAnimation;
import science.unlicense.model3d.impl.physic.Skeletons;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.model3d.impl.technique.AbstractTechnique;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.skeleton.InverseKinematic;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public class BlendStoreTest {

    private static final float DELTA = 0.000001f;

    @Test
    public void testReadCube() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/SimpleScene.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;
        final GraphicNode scene = (GraphicNode) bs.searchElements(GraphicNode.class).createIterator().next();
        assertBlendCS(scene);
        //TODO check vertices

    }

    @Test
    public void testReadScene() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/MultipleMeshScene.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final GraphicNode scene = (GraphicNode) bs.searchElements(GraphicNode.class).createIterator().next();
        assertBlendCS(scene);
        Assert.assertEquals(5, scene.getChildren().getSize());

        final GLModel mc = (GLModel) scene.getChildren().get(0);
        final GLModel mx = (GLModel) scene.getChildren().get(1);
        final GLModel my = (GLModel) scene.getChildren().get(2);
        final GLModel mz = (GLModel) scene.getChildren().get(3);
        final GLModel mxyz = (GLModel) scene.getChildren().get(4);
        Assert.assertEquals(new Chars("Centered"), mc.getTitle());
        Assert.assertEquals(new Chars("OffsetX"), mx.getTitle());
        Assert.assertEquals(new Chars("OffsetY"), my.getTitle());
        Assert.assertEquals(new Chars("OffsetZ"), mz.getTitle());
        Assert.assertEquals(new Chars("OffsetXYZ"), mxyz.getTitle());

        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            mc.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0,10,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            mx.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0,10,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            my.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1,10,
                0, 0, 0, 1).toArrayDouble(),
            mz.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                0.1, 0, 0,10,
                0, 0.2, 0,10,
                0, 0, 0.3,10,
                0, 0, 0, 1).toArrayDouble(),
            mxyz.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);

    }

    /**
     * Test reading diffuse and specular colors.
     */
    @Test
    public void testReadColor() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/SimpleColor.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final GraphicNode scene = (GraphicNode) bs.searchElements(GraphicNode.class).createIterator().next();
        assertBlendCS(scene);
        final GLModel mesh = (GLModel) scene.getChildren().get(0);

        final SimpleBlinnPhong.Material material = (SimpleBlinnPhong.Material) mesh.getMaterials().get(0);

        //diffuse/specular base
        Assert.assertEquals(new ColorRGB(1.0f, 0.0f, 0.0f, 1.0f), material.getDiffuse());
        Assert.assertEquals(new ColorRGB(0.0f, 1.0f, 0.0f, 1.0f), material.getSpecular());
        Assert.assertEquals(0.6, material.getAlpha(), DELTA);

    }

    /**
     * Test reading mirror and specular colors.
     *
     * TODO : update 3d model, find a way to express mirror textures.
     */
    @Test
    @Ignore
    public void testReadMirror() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/uscience/unlicense/format/blend/SimpleMirror.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final GraphicNode scene = (GraphicNode) bs.searchElements(GraphicNode.class).createIterator().next();
        assertBlendCS(scene);
        final GLModel mesh = (GLModel) scene.getChildren().get(0);


        final SimpleBlinnPhong.Material material = (SimpleBlinnPhong.Material) mesh.getMaterials().get(0);

        //diffuse/specular base
        Assert.assertEquals(new ColorRGB(1.0f, 1.0f, 1.0f, 1.0f), material.getDiffuse());
        Assert.assertEquals(new ColorRGB(1.0f, 1.0f, 1.0f, 1.0f), material.getSpecular());

//        final Layer mirrorLayer = (Layer) layers.get(0);
//        Assert.assertEquals(Layer.TYPE_DIFFUSE, mirrorLayer.getType());
//        final ReflectionMapping refMapping = (ReflectionMapping) mirrorLayer.getMap();

    }

    /**
     * Test reading texture.
     */
    @Test
    public void testReadTexture() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/SimpleTexture.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final GraphicNode scene = (GraphicNode) bs.searchElements(GraphicNode.class).createIterator().next();
        assertBlendCS(scene);
        final GLModel mesh = (GLModel) scene.getChildren().get(0);

        final SimpleBlinnPhong.Material material = (SimpleBlinnPhong.Material) mesh.getMaterials().get(0);

        //diffuse/specular base
        Assert.assertEquals(new ColorRGB(0.8f, 0.8f, 0.8f, 1.0f), material.getDiffuse());
        Assert.assertEquals(new ColorRGB(1.0f, 1.0f, 1.0f, 1.0f), material.getSpecular());

        //diffuse texture
        final TextureMapping diffuseLayer = material.getDiffuseTexture();
        Assert.assertEquals(Mesh.ATT_TEXCOORD_0, diffuseLayer.getMapping());
        final Texture2D diffusetex = diffuseLayer.getTexture();
        Assert.assertEquals(256, diffusetex.getWidth());
        Assert.assertEquals(256, diffusetex.getHeight());

        //normal
        final TextureMapping normalLayer = (TextureMapping) material.properties().getPropertyValue(AbstractTechnique.NORMAL);
        final Texture2D normaltex = normalLayer.getTexture();
        Assert.assertEquals(256, normaltex.getWidth());
        Assert.assertEquals(256, normaltex.getHeight());

    }

    /**
     * Test reading texture with clip, offset and scale values.
     */
    @Test
    public void testReadTextureClipScaleOffset() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/TextureClipOffsetScale.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final GraphicNode scene = (GraphicNode) bs.searchElements(GraphicNode.class).createIterator().next();
        assertBlendCS(scene);
        final GLModel mesh = (GLModel) scene.getChildren().get(0);

        final SimpleBlinnPhong.Material material = (SimpleBlinnPhong.Material) mesh.getMaterials().get(0);

        //diffuse/specular base
        Assert.assertEquals(new ColorRGB(0.8f, 0.8f, 0.8f, 1.0f), material.getDiffuse());
        Assert.assertEquals(new ColorRGB(1.0f, 1.0f, 1.0f, 1.0f), material.getSpecular());

        //diffuse
        final TextureMapping diffuseLayer = material.getDiffuseTexture();
        final Texture2D diffusetex = diffuseLayer.getTexture();
        Assert.assertEquals(256, diffusetex.getWidth());
        Assert.assertEquals(256, diffusetex.getHeight());
        Assert.assertEquals(new Vector2f64(4.0,2.0), diffuseLayer.getTransform().getScale());
        //y should be flipped
        Assert.assertArrayEquals(new float[]{0.54321f,-0.12345f}, diffuseLayer.getTransform().getTranslation().toFloat(),DELTA);
        Assert.assertTrue(diffusetex.getInfo().isClipped());

    }

    /**
     * Test displacement mapping modifier.
     */
    @Test
    public void testReadDisplacementMapping() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/SimpleDisplacement.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final GraphicNode scene = (GraphicNode) bs.searchElements(GraphicNode.class).createIterator().next();
        assertBlendCS(scene);
        final GLModel mesh = (GLModel) scene.getChildren().get(0);

        final SimpleBlinnPhong.Material material = (SimpleBlinnPhong.Material) mesh.getMaterials().get(0);

        //diffuse/specular base
        Assert.assertEquals(new ColorRGB(0.8f, 0.8f, 0.8f, 1.0f), material.getDiffuse());
        Assert.assertEquals(new ColorRGB(1.0f, 1.0f, 1.0f, 1.0f), material.getSpecular());


        //diffuse
        final TextureMapping diffuseLayer = material.getDiffuseTexture();
        final Texture2D diffusetex = diffuseLayer.getTexture();
        Assert.assertEquals(256, diffusetex.getWidth());
        Assert.assertEquals(256, diffusetex.getHeight());

        //normal
        final TextureMapping normalLayer = (TextureMapping) material.properties().getPropertyValue(AbstractTechnique.NORMAL);
        final Texture2D normaltex = normalLayer.getTexture();
        Assert.assertEquals(256, normaltex.getWidth());
        Assert.assertEquals(256, normaltex.getHeight());

        //displacement
        final Tessellator tess = mesh.getTessellator();
        Assert.assertTrue(tess instanceof DisplacementTessellator);
        final DisplacementTessellator dt = (DisplacementTessellator) tess;
        Assert.assertEquals(1.0f, dt.getFactor(), DELTA);

    }

    /**
     * Test reading a skeleton.
     */
    @Test
    public void testReadSkeleton() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/SimpleSkeleton.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final GraphicNode scene = (GraphicNode) bs.searchElements(GraphicNode.class).createIterator().next();
        assertBlendCS(scene);
        final MotionModel mpm = (MotionModel) scene.getChildren().get(0);


        //check skeleton structure
        final Skeleton skeleton = mpm.getSkeleton();
        final Sequence joints = skeleton.getChildren();
        Assert.assertEquals(1, joints.getSize());

        final Joint firstBone = (Joint) joints.get(0);
        Assert.assertEquals(1, firstBone.getChildren().getSize());
        final Joint secondBone = (Joint) firstBone.getChildren().get(0);
        Assert.assertEquals(1, secondBone.getChildren().getSize());
        final Joint thirdBone = (Joint) secondBone.getChildren().get(0);
        Assert.assertEquals(0, thirdBone.getChildren().getSize());

        Assert.assertEquals(new Chars("first bone"), firstBone.getTitle());
        Assert.assertEquals(new Chars("second bone"), secondBone.getTitle());
        Assert.assertEquals(new Chars("third bone"), thirdBone.getTitle());

        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(),
            firstBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            secondBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            thirdBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);


        //check the mapping with the mesh
        Assert.assertEquals(2, mpm.getChildren().getSize());
        final GLModel mesh = (GLModel) mpm.getChildren().get(1);
        final GLMesh shell = (GLMesh) mesh.getShape();
        final Skin skin = mesh.getSkin();

        Assert.assertArrayEquals(new float[]{
            -1,-1, 0,
             1,-1, 0,
            -1, 0, 0,
             1, 0, 0,
            -1, 1, 0,
             1, 1, 0
        }, Geometries.toFloat32(shell.getPositions()), DELTA);

        Assert.assertEquals(1, skin.getMaxWeightPerVertex());
        Assert.assertArrayEquals(new int[]{
            0,0,
            1,1,
            2,2,
        }, ((VBO) shell.getAttributes().getValue(Skin.ATT_JOINTS_0)).getPrimitiveBuffer().toIntArray());

        Assert.assertArrayEquals(new float[]{
            1,1,
            1,1,
            1,1,
        }, ((VBO) shell.getAttributes().getValue(Skin.ATT_WEIGHTS_0)).getPrimitiveBuffer().toFloatArray(), DELTA);

    }

    /**
     * Test reading shape keys (morph targets)
     */
    @Test
    public void testReadShapeKeys() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/SimpleMorph.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final GraphicNode scene = (GraphicNode) bs.searchElements(GraphicNode.class).createIterator().next();
        assertBlendCS(scene);
        final GLModel mesh = (GLModel) scene.getChildren().get(0);

        final GLMesh skin = (GLMesh) mesh.getShape();
        final Sequence morphs = skin.getMorphs();
        Assert.assertEquals(3, morphs.getSize());

        final MorphTarget morph1 = (MorphTarget) morphs.get(0);
        final MorphTarget morph2 = (MorphTarget) morphs.get(1);
        final MorphTarget morph3 = (MorphTarget) morphs.get(2);
        Assert.assertEquals(new Chars("Basis"), morph1.getName());
        Assert.assertEquals(new Chars("move1"), morph2.getName());
        Assert.assertEquals(new Chars("move2"), morph3.getName());

        Assert.assertArrayEquals(
                new float[]{
                0, 0, 0,
                0, 0, 0,
                0, 0, 0,
                0, 0, 0,
                0, 0, 0,
                0, 0, 0},
                Geometries.toFloat32(morph1.getVertices()), DELTA);
        Assert.assertArrayEquals(
                new float[]{
                0, 0, 1,
                0, 0, 1,
                0, 0, 0,
                0, 0, 0,
                0, 0, 1,
                0, 0, 1},
                Geometries.toFloat32(morph2.getVertices()), DELTA);
        Assert.assertArrayEquals(
                new float[]{
                0, 0, 0,
                0, 0, 0,
                2, 0, 0,
                2, 0, 0,
                0, 0, 0,
                0, 0, 0},
                Geometries.toFloat32(morph3.getVertices()), DELTA);

    }

    /**
     * Test reading an animation.
     */
    @Test
    public void testReadAnimation() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/SimpleAnimation.blend"));

        final Chars bone1Name = new Chars("first bone");
        final Chars bone2Name = new Chars("second bone");
        final Chars bone3Name = new Chars("third bone");

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final Sequence elements = new ArraySequence(bs.getElements());
        final GraphicNode scene = (GraphicNode) elements.get(0);
        assertBlendCS(scene);
        final MotionModel mpm = (MotionModel) scene.getChildren().get(0);
        final SkeletonAnimation animation = (SkeletonAnimation) mpm.getAnimations().getValues().createIterator().next();


        //check skeleton structure
        final Skeleton skeleton = mpm.getSkeleton();
        final Sequence joints = skeleton.getChildren();
        Assert.assertEquals(1, joints.getSize());

        final Joint firstBone = (Joint) joints.get(0);
        Assert.assertEquals(1, firstBone.getChildren().getSize());
        final Joint secondBone = (Joint) firstBone.getChildren().get(0);
        Assert.assertEquals(1, secondBone.getChildren().getSize());
        final Joint thirdBone = (Joint) secondBone.getChildren().get(0);
        Assert.assertEquals(0, thirdBone.getChildren().getSize());

        Assert.assertEquals(bone1Name, firstBone.getTitle());
        Assert.assertEquals(bone2Name, secondBone.getTitle());
        Assert.assertEquals(bone3Name, thirdBone.getTitle());

        //check parent to node transform
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(),
            firstBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            secondBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            thirdBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);

        //check bind pose
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(),
            firstBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(firstBone.getBindPose().invert().toMatrix().toArrayDouble(),
            firstBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 1,
                0, 0, 0, 1).toArrayDouble(),
            secondBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(secondBone.getBindPose().invert().toMatrix().toArrayDouble(),
            secondBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 2,
                0, 0, 0, 1).toArrayDouble(),
            thirdBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(thirdBone.getBindPose().invert().toMatrix().toArrayDouble(),
            thirdBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);



        //check reset to base
        skeleton.resetToBase();
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(),
            firstBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            secondBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            thirdBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);


        //check the mapping with the mesh
        Assert.assertEquals(2, mpm.getChildren().getSize());
        final GLModel mesh = (GLModel) mpm.getChildren().get(1);
        final GLMesh shell = (GLMesh) mesh.getShape();
        final Skin skin = mesh.getSkin();

        Assert.assertEquals(1, skin.getMaxWeightPerVertex());

        Assert.assertArrayEquals(new int[]{
            0,0,
            1,1,
            2,2,
        }, ((VBO) shell.getAttributes().getValue(Skin.ATT_JOINTS_0)).getPrimitiveBuffer().toIntArray());

        Assert.assertArrayEquals(new float[]{
            1,1,
            1,1,
            1,1,
        }, ((VBO) shell.getAttributes().getValue(Skin.ATT_WEIGHTS_0)).getPrimitiveBuffer().toFloatArray(), DELTA);

        //check animation
        final JointTimeSerie serie1 = (JointTimeSerie) animation.getSeries().get(0);
        final JointTimeSerie serie2 = (JointTimeSerie) animation.getSeries().get(1);
        final JointTimeSerie serie3 = (JointTimeSerie) animation.getSeries().get(2);
        Assert.assertEquals(bone1Name, serie1.getJointIdentifier());
        Assert.assertEquals(bone2Name, serie2.getJointIdentifier());
        Assert.assertEquals(bone3Name, serie3.getJointIdentifier());
        Assert.assertEquals(3, serie1.getFrames().getSize());
        Assert.assertEquals(3, serie2.getFrames().getSize());
        Assert.assertEquals(3, serie3.getFrames().getSize());

        //first bone serie
        final Iterator serie1Ite = serie1.getFrames().createIterator();
        final JointKeyFrame s1f1 = (JointKeyFrame) serie1Ite.next();
        final JointKeyFrame s1f2 = (JointKeyFrame) serie1Ite.next();
        final JointKeyFrame s1f3 = (JointKeyFrame) serie1Ite.next();
        Assert.assertFalse(serie1Ite.hasNext());
        Assert.assertEquals(   0.0, s1f1.getTime(), DELTA);
        Assert.assertEquals(2000.0, s1f2.getTime(), DELTA);
        Assert.assertEquals(4000.0, s1f3.getTime(), DELTA);
        Assert.assertEquals(bone1Name, s1f1.getJoint());
        Assert.assertEquals(bone1Name, s1f2.getJoint());
        Assert.assertEquals(bone1Name, s1f3.getJoint());
        Assert.assertEquals(new Vector3f64(0, 0, 0), s1f1.getValue().getTranslation());
        Assert.assertEquals(new Vector3f64(0, 0, 0), s1f2.getValue().getTranslation());
        Assert.assertEquals(new Vector3f64(0, 0, 0), s1f3.getValue().getTranslation());
        Assert.assertEquals(new Vector3f64(1, 1, 1), s1f1.getValue().getScale());
        Assert.assertEquals(new Vector3f64(1, 1, 1), s1f2.getValue().getScale());
        Assert.assertEquals(new Vector3f64(1, 1, 1), s1f3.getValue().getScale());
//        Assert.assertEquals(new Quaternion(0,0,0,1), s1f1p.getRotation());
//        Assert.assertEquals(new Quaternion(0,0,0,1), s1f2p.getRotation());
//        Assert.assertEquals(new Quaternion(0,0,0,1), s1f3p.getRotation());

        //second bone serie
        final Iterator serie2Ite = serie2.getFrames().createIterator();
        final JointKeyFrame s2f1 = (JointKeyFrame) serie2Ite.next();
        final JointKeyFrame s2f2 = (JointKeyFrame) serie2Ite.next();
        final JointKeyFrame s2f3 = (JointKeyFrame) serie2Ite.next();
        Assert.assertFalse(serie2Ite.hasNext());
        Assert.assertEquals(   0.0, s2f1.getTime(), DELTA);
        Assert.assertEquals(2000.0, s2f2.getTime(), DELTA);
        Assert.assertEquals(4000.0, s2f3.getTime(), DELTA);
        Assert.assertEquals(bone2Name, s2f1.getJoint());
        Assert.assertEquals(bone2Name, s2f2.getJoint());
        Assert.assertEquals(bone2Name, s2f3.getJoint());
        Assert.assertEquals(new Vector3f64(0, 0, 0), s2f1.getValue().getTranslation());
        Assert.assertEquals(new Vector3f64(0, 0, 0), s2f2.getValue().getTranslation());
        Assert.assertEquals(new Vector3f64(0, 0, 0), s2f3.getValue().getTranslation());
        Assert.assertEquals(new Vector3f64(1, 1, 1), s2f1.getValue().getScale());
        Assert.assertEquals(new Vector3f64(1, 1, 1), s2f2.getValue().getScale());
        Assert.assertEquals(new Vector3f64(1, 1, 1), s2f3.getValue().getScale());
//        Assert.assertEquals(new Quaternion(0,0,0,1), s2f1p.getRotation());
//        Assert.assertArrayEquals(
//                Quaternion.createFromAxis(new Vector(0,0,1),Angles.degreeToRadian(40)).toArrayDouble(),
//                s2f2p.getRotation().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(
//                Quaternion.createFromAxis(new Vector(0,0,1),Angles.degreeToRadian(-40)).toArrayDouble(),
//                s2f3p.getRotation().toArrayDouble(), DELTA);

        //third bone serie
        final Iterator serie3Ite = serie3.getFrames().createIterator();
        final JointKeyFrame s3f1 = (JointKeyFrame) serie3Ite.next();
        final JointKeyFrame s3f2 = (JointKeyFrame) serie3Ite.next();
        final JointKeyFrame s3f3 = (JointKeyFrame) serie3Ite.next();
        Assert.assertFalse(serie3Ite.hasNext());
        Assert.assertEquals(   0.0, s3f1.getTime(), DELTA);
        Assert.assertEquals(2000.0, s3f2.getTime(), DELTA);
        Assert.assertEquals(4000.0, s3f3.getTime(), DELTA);
        Assert.assertEquals(bone3Name, s3f1.getJoint());
        Assert.assertEquals(bone3Name, s3f2.getJoint());
        Assert.assertEquals(bone3Name, s3f3.getJoint());
        Assert.assertEquals(new Vector3f64(0, 0, 0), s3f1.getValue().getTranslation());
        Assert.assertEquals(new Vector3f64(0, 0, 0), s3f2.getValue().getTranslation());
        Assert.assertEquals(new Vector3f64(0, 0, 0), s3f3.getValue().getTranslation());
        Assert.assertEquals(new Vector3f64(1, 1, 1), s3f1.getValue().getScale());
        Assert.assertEquals(new Vector3f64(1, 1, 1), s3f2.getValue().getScale());
        Assert.assertEquals(new Vector3f64(1, 1, 1), s3f3.getValue().getScale());
//        Assert.assertArrayEquals(
//                new Quaternion(0,0,0,1).getValues(),
//                s3f1p.getRotation().getValues(), DELTA);
//        Assert.assertArrayEquals(
//                Quaternion.createFromAxis(new Vector(0,0,1),Angles.degreeToRadian(-70)).getValues(),
//                s3f2p.getRotation().getValues(), DELTA);
//        Assert.assertArrayEquals(
//                Quaternion.createFromAxis(new Vector(0,0,1),Angles.degreeToRadian(70)).getValues(),
//                s3f3p.getRotation().getValues(), DELTA);


        //test the animation
        Skeletons.mapAnimation(animation, skeleton, mpm, null);
        animation.start();
        animation.setTime(0);
        animation.update();

        //check parent to node transform
        //this node transform must have been converted to relative -> identity
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            firstBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            secondBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            thirdBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);

        //check bind pose
        //Skeletons.mapAnimation(animation, skeleton, scene);
        //this node transform must have been converted to relative -> identity
//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 1, 0, 0,
//                0, 0, 1, 0,
//                0, 0, 0, 1).toArrayDouble(),
//            firstBone.getBindPose().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(firstBone.getBindPose().invert().toArrayDouble(),
//            firstBone.getInvertBindPose().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 0,-1, 0,
//                0, 1, 0, 1,
//                0, 0, 0, 1).toArrayDouble(),
//            secondBone.getBindPose().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(secondBone.getBindPose().invert().toArrayDouble(),
//            secondBone.getInvertBindPose().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 0,-1, 0,
//                0, 1, 0, 2,
//                0, 0, 0, 1).toArrayDouble(),
//            thirdBone.getBindPose().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(thirdBone.getBindPose().invert().toArrayDouble(),
//            thirdBone.getInvertBindPose().toArrayDouble(), DELTA);
//
//        animation.update(2000*1000000, mpm);

//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 1, 0, 0,
//                0, 0, 1, 0,
//                0, 0, 0, 1).toArrayDouble(),
//            firstBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 1, 0, 1,
//                0, 0, 1, 0,
//                0, 0, 0, 1).toArrayDouble(),
//            secondBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 1, 0, 1,
//                0, 0, 1, 0,
//                0, 0, 0, 1).toArrayDouble(),
//            thirdBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);

    }

    /**
     * Test reading an animation.
     */
    @Test
    public void testReadInstancing() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/SimpleInstancing.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final Sequence elements = new ArraySequence(bs.getElements());
        for (int i=0;i<elements.getSize();i++){
            final Object ele = elements.get(i);
            if (ele instanceof SceneNode){
                assertBlendCS((SceneNode) ele);
            }
        }
    }

    @Test
    public void testReadNgon() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/Ngon.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final GraphicNode scene = (GraphicNode) bs.searchElements(GraphicNode.class).createIterator().next();
        assertBlendCS(scene);
        final GLModel ngon = (GLModel) scene.getChildren().get(0);

        final GLMesh shell = (GLMesh) ngon.getShape();
        final VBO vertices = (VBO) shell.getPositions();
        final VBO normals = (VBO) shell.getNormals();
        final IBO indexes = (IBO) shell.getIndex();
        final IndexedRange[] modes = shell.getRanges();

        Assert.assertEquals(6, vertices.getTupleCount());
        Assert.assertEquals(6, normals.getTupleCount());

        final TupleRW tuple = vertices.createTuple();
        vertices.getTuple(0, tuple);
        Assert.assertEquals(new Vector3f64(-2f,0f,2.7f), tuple);
        vertices.getTuple(1, tuple);
        Assert.assertEquals(new Vector3f64(0.1f,1f,1.4f), tuple);
        vertices.getTuple(2, tuple);
        Assert.assertEquals(new Vector3f64(-1.3f,-2.5f,0.75f), tuple);
        vertices.getTuple(3, tuple);
        Assert.assertEquals(new Vector3f64(-0.45f,1.4f,3.9f), tuple);
        vertices.getTuple(4, tuple);
        Assert.assertEquals(new Vector3f64(-0.45f,1.2f,2.8f), tuple);
        vertices.getTuple(5, tuple);
        Assert.assertEquals(new Vector3f64(-0.3f,1.2f,1.6f), tuple);

        Assert.assertArrayEquals(new int[]{3,0,2, 4,3,2, 5,4,2, 1,5,2}, indexes.getPrimitiveBuffer().toIntArray());
        Assert.assertEquals(1, modes.length);
        Assert.assertEquals(GLC.GL_TRIANGLES, modes[0].getMode());

    }

    /**
     * Test reading a skeleton and morph with vertex groups.
     */
    @Test
    public void testReadSkeletonAndMorph() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/SkeletonAndMorph.blend"));

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final GraphicNode scene = (GraphicNode) bs.searchElements(GraphicNode.class).createIterator().next();
        assertBlendCS(scene);
        final MotionModel mpm = (MotionModel) scene.getChildren().get(0);


        //check skeleton structure
        final Skeleton skeleton = mpm.getSkeleton();
        final Sequence joints = skeleton.getChildren();
        Assert.assertEquals(1, joints.getSize());

        final Joint firstBone = (Joint) joints.get(0);
        Assert.assertEquals(1, firstBone.getChildren().getSize());
        final Joint secondBone = (Joint) firstBone.getChildren().get(0);
        Assert.assertEquals(0, secondBone.getChildren().getSize());

        Assert.assertEquals(new Chars("b0"), firstBone.getTitle());
        Assert.assertEquals(new Chars("b1"), secondBone.getTitle());

        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(),
            firstBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            secondBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);


        //check the mapping with the mesh
        Assert.assertEquals(2, mpm.getChildren().getSize());
        final GLModel mesh = (GLModel) mpm.getChildren().get(1);
        final GLMesh shell = (GLMesh) mesh.getShape();
        final Skin skin = mesh.getSkin();

        /*
        +  4
        | \
        +--+ 0/1 , 3/5
        | /
        +   2
        */
        Assert.assertArrayEquals(new float[]{
             1,-1, 1,
             1, 1, 1,
             1,-1, 0,
             1,-1, 1,
             1,-1, 2,
             1, 1, 1
        }, Geometries.toFloat32(shell.getPositions()), DELTA);

        Assert.assertArrayEquals(new int[]{
            0,1,2,
            3,4,5
        }, Geometries.toInt32(shell.getIndex()));

        Assert.assertEquals(2, skin.getMaxWeightPerVertex());
        Assert.assertArrayEquals(new int[]{
            0,1,
            0,1,
            0,0,

            0,1,
            1,0,
            0,1,
        }, ((VBO) shell.getAttributes().getValue(Skin.ATT_JOINTS_0)).getPrimitiveBuffer().toIntArray());
        Assert.assertArrayEquals(new float[]{
            0.5f,0.5f,
            0.5f,0.5f,
            1.0f,0.0f,

            0.5f,0.5f,
            1.0f,0.0f,
            0.5f,0.5f,
        }, ((VBO) shell.getAttributes().getValue(Skin.ATT_WEIGHTS_0)).getPrimitiveBuffer().toFloatArray(), DELTA);

        final Sequence morphs = shell.getMorphs();
        final MorphTarget mbas = (MorphTarget) morphs.get(0);
        final MorphTarget mk1 = (MorphTarget) morphs.get(1);
        final MorphTarget mk2 = (MorphTarget) morphs.get(2);

        Assert.assertEquals(new Chars("Basis"), mbas.getName());
        Assert.assertEquals(new Chars("key1"), mk1.getName());
        Assert.assertEquals(new Chars("key2"), mk2.getName());

        //the basis morph is the same as original vertices
        Assert.assertArrayEquals(new float[]{
             0, 0, 0,
             0, 0, 0,
             0, 0, 0,
             0, 0, 0,
             0, 0, 0,
             0, 0, 0
        }, Geometries.toFloat32(mbas.getVertices()), DELTA);

        Assert.assertArrayEquals(new float[]{
             1, 0, 0,
             1, 0, 0,
             1, 0, 0,
             1, 0, 0,
             0, 0, 0,
             1, 0, 0
        }, Geometries.toFloat32(mk1.getVertices()), DELTA);

        Assert.assertArrayEquals(new float[]{
             1, 0, 0,
             1, 0, 0,
             0, 0, 0,
             1, 0, 0,
             1, 0, 0,
             1, 0, 0
        }, Geometries.toFloat32(mk2.getVertices()), DELTA);

    }

    /**
     * Test reading an inverse kinematic chain.
     */
    @Test
    public void testReadInverseKinematic() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/blend/InverseKinematic.blend"));

        final Chars bone1Name = new Chars("first bone");
        final Chars bone2Name = new Chars("second bone");
        final Chars bone3Name = new Chars("third bone");
        final Chars targetName = new Chars("target");

        final Store store = Model3Ds.open(path);
        Assert.assertTrue(store instanceof BlendStore);
        final BlendStore bs = (BlendStore) store;

        final Sequence elements = new ArraySequence(bs.getElements());
        final GraphicNode scene = (GraphicNode) elements.get(0);
        assertBlendCS(scene);
        final MotionModel mpm = (MotionModel) scene.getChildren().get(0);


        //check skeleton structure
        final Skeleton skeleton = mpm.getSkeleton();
        final Sequence joints = skeleton.getChildren();
        Assert.assertEquals(2, joints.getSize());

        final Joint firstBone = (Joint) joints.get(0);
        final Joint secondBone = (Joint) firstBone.getChildren().get(0);
        final Joint thirdBone = (Joint)  secondBone.getChildren().get(0);
        final Joint targetBone = (Joint)  joints.get(1);

        Assert.assertEquals(bone1Name, firstBone.getTitle());
        Assert.assertEquals(bone2Name, secondBone.getTitle());
        Assert.assertEquals(bone3Name, thirdBone.getTitle());
        Assert.assertEquals(targetName, targetBone.getTitle());

        //check parent to node transform
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(),
            firstBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            secondBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            thirdBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);

        //check bind pose
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(),
            firstBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(firstBone.getBindPose().invert().toMatrix().toArrayDouble(),
            firstBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 1,
                0, 0, 0, 1).toArrayDouble(),
            secondBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(secondBone.getBindPose().invert().toMatrix().toArrayDouble(),
            secondBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 2,
                0, 0, 0, 1).toArrayDouble(),
            thirdBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(thirdBone.getBindPose().invert().toMatrix().toArrayDouble(),
            thirdBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);



        //check reset to base
        skeleton.resetToBase();
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(),
            firstBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            secondBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(),
            thirdBone.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);


        //check the mapping with the mesh
        Assert.assertEquals(2, mpm.getChildren().getSize());
        final GLModel mesh = (GLModel) mpm.getChildren().get(1);
        final GLMesh shell = (GLMesh) mesh.getShape();
        final Skin skin = mesh.getSkin();

        Assert.assertEquals(1, skin.getMaxWeightPerVertex());

        Assert.assertArrayEquals(new int[]{
            0,0,
            1,1,
            2,2,
        }, ((VBO) shell.getAttributes().getValue(Skin.ATT_JOINTS_0)).getPrimitiveBuffer().toIntArray());

        Assert.assertArrayEquals(new float[]{
            1,1,
            1,1,
            1,1,
        }, ((VBO) shell.getAttributes().getValue(Skin.ATT_WEIGHTS_0)).getPrimitiveBuffer().toFloatArray(), DELTA);

        //check inverse kinematic
        Assert.assertEquals(1, skeleton.getIks().getSize());

        final InverseKinematic ik = (InverseKinematic) skeleton.getIks().get(0);
        final Joint[] chain = ik.getChain();
        Assert.assertEquals(2, chain.length);
        Assert.assertEquals(chain[0], secondBone);
        Assert.assertEquals(chain[1], firstBone);
        Assert.assertEquals(ik.getEffector(), thirdBone);
        Assert.assertEquals(ik.getTarget(), targetBone);

    }

    private static void assertBlendCS(SceneNode node){
        final NodeVisitor visitor = new NodeVisitor(){
            public Object visit(Node node, Object context) {
                if (node instanceof SceneNode){
                    final CoordinateSystem cs = ((SceneNode) node).getCoordinateSystem();
                    Assert.assertEquals(BlendConstants.COORDSYS, cs);
                }
                if (node instanceof MotionModel){
                    final Skeleton skeleton = ((MotionModel) node).getSkeleton();
                    if (skeleton!=null){
                        final Sequence roots = skeleton.getChildren();
                        for (int i=0;i<roots.getSize();i++){
                            this.visit((Node) roots.get(i), null);
                        }
                    }
                }
                return super.visit(node, context);
            }
        };
        visitor.visit(node, null);
    }

}
