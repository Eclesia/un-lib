
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendBonePose extends BlendFileBlock{

    public BlendBonePose(BlendFileBlock block) {
        super(block);
    }

    public Sequence getChannels(){
        return getRefList(Blend_2_72.BONE_POSE.CHANBASE);
    }

}
