
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendGroup extends BlendIdentified{

    public BlendGroup(BlendFileBlock block) {
        super(block);
    }

    public BlendGroupObject[] getBases(){
        final Sequence seq = getRefList(Blend_2_72.GROUP.GOBJECT);
        final BlendGroupObject[] bases = new BlendGroupObject[seq.getSize()];
        Collections.copy(seq, bases, 0);
        return bases;
    }

}
