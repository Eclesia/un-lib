
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendArmature extends BlendIdentified{

    public BlendArmature(BlendFileBlock block) {
        super(block);
    }

}
