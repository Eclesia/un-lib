
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.TypedNode;

/**
 *
 * @author Johann Sorel
 */
public class BlendId {

    private final TypedNode node;

    public BlendId(TypedNode node) {
        this.node = node;
    }

    public Chars getName(){
        final Chars name = (Chars) node.getChild(Blend_2_72.IDNODE.NAME).getValue();
        return name.truncate(2, name.getCharLength());
    }

}
