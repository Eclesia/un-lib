
package science.unlicense.format.blend.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.format.blend.BlendConstants;
import science.unlicense.format.blend.BlendDataInputStream;

/**
 *
 * @author Johann Sorel
 */
public class BlendFile {

    private static final Dictionary BLOCK_TYPES = new HashDictionary();
    private static final Chars SDNA = Chars.constant("DNA1");

    public Path filePath;
    public Endianness encoding;
    public final Sequence blocks = new ArraySequence();
    public BlendFileHeader header;
    public BlendSDNA sdnaBlock = null;

    public BlendFile(Path filePath) {
        this.filePath = filePath;
    }

    public  void read(ByteInputStream stream) throws IOException, StoreException {

        final BacktrackInputStream bs = new BacktrackInputStream(stream);
        bs.mark();
        BlendDataInputStream ds = new BlendDataInputStream(bs,Endianness.LITTLE_ENDIAN);

        //read header
        header = new BlendFileHeader();
        ds.skip(7); //signature
        header.pointerSize = ds.readByte();
        header.endianess = ds.readByte();
        header.version = new Chars(ds.readFully(new byte[3]));
        encoding = BlendFileHeader.getEncoding(header.endianess);

        //change the data input stream to match encoding
        if (encoding != ds.getEndianness()){
            ds = new BlendDataInputStream(bs,encoding);
        }
        ds.setPointerSize(header.pointerSize);


        //split blocks
        for (int b = ds.read();b!=-1;b=ds.read()){
            final byte[] codeArray = new byte[4];
            ds.readFully(codeArray, 1, 3);
            codeArray[0] = (byte) b;
            final Chars code = new Chars(BlendDataInputStream.trimAtZero(codeArray));
            final long fileOffset = bs.position()-4;

            final BlendFileBlock block;
            if (SDNA.equals(code)){
                block = new BlendSDNA();
                sdnaBlock = (BlendSDNA) block;
            } else {
                block = new BlendFileBlock();
            }

            block.read(ds, code);
            block.fileOffset = fileOffset;
            blocks.add(block);

            if (BlendConstants.BLOCK_ENDB.equals(block.header.code)){
                //end file block indicator
                break;
            }
            //avoid keeping a to big rollback, TODO remove backtrack stream
            //when reader properly works
        }

        //parse the SDNA block first, rebuild node types which are used for other blocks
        sdnaBlock.parse(header, null);

        //parse all blocks raw data
        for (int i=0,n=blocks.getSize();i<n;i++){
            final BlendFileBlock block = (BlendFileBlock) blocks.get(i);
            block.parse(header,sdnaBlock);
        }

    }

    /**
     * This method replace all address pointer type fields by the real block object
     */
    public void resolvePointers(){
        for (int i=0,n=blocks.getSize();i<n;i++){
            final BlendFileBlock block = (BlendFileBlock) blocks.get(i);
            if (block.parsedData!=null){
                resolvePointers(block.parsedData);
            }
        }
    }

    private void resolvePointers(TypedNode node) {

        final BlendNodeType type = (BlendNodeType) node.getType();
        if (type.isPointer()){
            final long adr = ((Number) node.getValue()).longValue();
            if (adr!=0){
                node.setValue(getBlock(adr));
            } else {
                //set to null
                node.setValue(null);
            }
        }

        final Iterator params = node.getChildren().createIterator();
        while (params.hasNext()) {
            resolvePointers((TypedNode) params.next());
        }

    }

    /**
     * Find the first block with given header code.
     *
     * @param code
     * @return
     */
    public BlendFileBlock getBlockByCode(Chars code){
        for (int i=0,n=blocks.getSize();i<n;i++){
            final BlendFileBlock block = (BlendFileBlock) blocks.get(i);
            if (code.equals(block.header.code)){
                return block;
            }
        }
        return null;
    }

    /**
     * Find the first block with given header code.
     *
     * @param sdnaType
     * @return
     */
    public BlendFileBlock getBlockByType(Chars sdnaType){
        for (int i=0,n=blocks.getSize();i<n;i++){
            final BlendFileBlock block = (BlendFileBlock) blocks.get(i);
            if (block.parsedData==null) continue;
            if (sdnaType.equals(block.parsedData.getType().getId())){
                return block;
            }
        }
        return null;
    }

    /**
     * Find the first block with given address.
     *
     * @param address
     * @return
     */
    public BlendFileBlock getBlock(long address){
        for (int i=0,n=blocks.getSize();i<n;i++){
            final BlendFileBlock block = (BlendFileBlock) blocks.get(i);
            if (address == block.header.oldMemoryAddress){
                return block;
            }
        }
        return null;
    }

    /**
     * Find the first block witch is a BlendIdentified and has the given name.
     *
     * @param name
     * @return
     */
    public BlendFileBlock getBlockByName(Chars name){
        for (int i=0,n=blocks.getSize();i<n;i++){
            final BlendFileBlock block = (BlendFileBlock) blocks.get(i);
            final TypedNode idChild = block.parsedData.getChild(new Chars("id"));
            if (idChild !=null){
                Chars childName = (Chars) idChild.getChild(BlendConstants.NAME).getValue();
                childName = childName.truncate(2, childName.getCharLength());
                if (name.equals(childName)){
                    return block;
                }
            }
        }
        return null;
    }

    /**
     * Blender datas are align on 4 bytes.
     * This method moves the cursor forward if needed.
     */
    public static void realign(ArrayInputStream as) throws IOException{
        int res = as.getIndex()%4;
        if (res==0) return;
        as.skip(4-res);
    }

}
