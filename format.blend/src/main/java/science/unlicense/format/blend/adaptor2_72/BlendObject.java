
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.common.api.Arrays;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendObject extends BlendIdentified{

    public BlendObject(BlendFileBlock block) {
        super(block);
    }

    public BlendFileBlock getDataBlock(){
        return (BlendFileBlock) parsedData.getChild(Blend_2_72.OBJECT.DATA).getValue();
    }

    public BlendGroup getDuplicateGroup(){
        return (BlendGroup) parsedData.getChild(Blend_2_72.OBJECT.DUPLICATE_GROUP).getValue();
    }

    public Matrix4x4 getMatrix(){
        final float[][] m = (float[][]) parsedData.getChild(Blend_2_72.OBJECT.MATRIX).getValue();
        final Matrix4x4 parentToNode = new Matrix4x4();
        parentToNode.setCol(0, Arrays.reformatDouble(m[0]));
        parentToNode.setCol(1, Arrays.reformatDouble(m[1]));
        parentToNode.setCol(2, Arrays.reformatDouble(m[2]));
        parentToNode.setCol(3, Arrays.reformatDouble(m[3]));
        return parentToNode;
    }

}
