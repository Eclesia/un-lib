
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendBase extends BlendFileBlock{

    public BlendBase(BlendFileBlock block) {
        super(block);
    }

    public BlendObject getBlendObject(){
        return (BlendObject) parsedData.getChild(Blend_2_72.BASE.OBJECT).getValue();
    }

}
