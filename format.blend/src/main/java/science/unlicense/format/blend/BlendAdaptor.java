
package science.unlicense.format.blend;

import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public interface BlendAdaptor {

    BlendFileBlock adapt(BlendFileBlock block);

}
