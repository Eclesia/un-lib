
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendMTexture extends BlendFileBlock{

    public BlendMTexture(BlendFileBlock block) {
        super(block);
    }

    public int getMapto(){
        return ((Number) parsedData.getChild(Blend_2_72.MTEXTURE.MAPTO).getValue()).intValue();
    }

    public BlendTexture getTexture(){
        return (BlendTexture) parsedData.getChild(Blend_2_72.MTEXTURE.TEX).getValue();
    }

    public float[] getScale(){
        return (float[]) parsedData.getChild(Blend_2_72.MTEXTURE.SIZE).getValue();
    }

    public float[] getOffset(){
        return (float[]) parsedData.getChild(Blend_2_72.MTEXTURE.OFS).getValue();
    }

}
