
package science.unlicense.format.blend;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Extend data input stream with blender pointer reader.
 *
 * @author Johann Sorel
 */
public class BlendDataInputStream extends DataInputStream{

    private byte pointerSize = BlendConstants.POINTER_4;

    private final ByteSequence buffer = new ByteSequence();

    public BlendDataInputStream(ByteInputStream in) {
        super(in);
    }

    public BlendDataInputStream(ByteInputStream in, Endianness encoding) {
        super(in, encoding);
    }

    public void setPointerSize(byte pointerSize) {
        this.pointerSize = pointerSize;
    }

    public byte getPointerSize() {
        return pointerSize;
    }

    public long readPointer() throws IOException{
        if (pointerSize == BlendConstants.POINTER_4){
            return readUInt();
        } else if (pointerSize == BlendConstants.POINTER_8){
            return readLong();
        } else {
            throw new IOException(this, "Unknowed pointer size : "+pointerSize);
        }
    }

    public long[] readPointers(long[] buffer) throws IOException{
        if (pointerSize == BlendConstants.POINTER_4){
            return readUInt(buffer);
        } else if (pointerSize == BlendConstants.POINTER_8){
            return readLong(buffer);
        } else {
            throw new IOException(this, "Unknowed pointer size : "+pointerSize);
        }
    }

    public Chars readChars(int size) throws IOException {
        return new Chars(readFully(new byte[size]));
    }

    public Chars readCharsEndZero() throws IOException {
        buffer.removeAll();
        for (byte b=readByte();b!=0;b=readByte()){
            buffer.put(b);
        }
        return new Chars(buffer.toArrayByte());
    }

    public static byte[] trimAtZero(byte[] ba){
        final int index = Arrays.getFirstOccurence(ba, 0, ba.length, (byte) 0);
        if (index>=0){
            ba = Arrays.copy(ba, 0, index);
        }
        return ba;
    }

}
