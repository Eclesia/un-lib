
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.blend.BlendConstants;

/**
 * Model of a Blender file version 2.72b.
 *
 * @author Johann Sorel
 */
public class Blend_2_72 extends Blend {

    //blocks
    public static final Chars BLOCK_DATA = Chars.constant(new byte[]{'D','A','T','A'});

    public interface IDNODE{
        public static final Chars NEXT     = BlendConstants.NEXT;
        public static final Chars PREVIOUS = BlendConstants.PREVIOUS;
        /** name is prefixed by 2chars which indicate the type */
        public static final Chars NAME     = Chars.constant("name");
    }

    public interface SCENE{
        public static final Chars CODE = Chars.constant(new byte[]{'S','C'});
        public static final Chars SCENE     = Chars.constant("Scene");
        public static final Chars BASA      = Chars.constant("*basa");
        public static final Chars BASE      = Chars.constant("base");
        public static final Chars BASE_FIRST= BlendConstants.FIRST;
        public static final Chars BASE_LAST = BlendConstants.LAST;
    }

    public interface BASE{
        public static final Chars BASE          = Chars.constant("Base");
        /** This property points to an object */
        public static final Chars OBJECT   = Chars.constant("*object");
        public static final Chars NEXT     = BlendConstants.NEXT;
        public static final Chars PREVIOUS = BlendConstants.PREVIOUS;
    }

    public interface GROUP{
        public static final Chars GROUP         = Chars.constant("Group");
        /** Name is prefixed by GR */
        public static final Chars ID            = BlendConstants.ID;
        public static final Chars GOBJECT       = Chars.constant("gobject");
        public static final Chars GOBJECT_FIRST = BlendConstants.FIRST;
        public static final Chars GOBJECT_LAST  = BlendConstants.LAST;
    }

    public interface GROUP_OBJECT{
        public static final Chars GROUP_OBJECT = Chars.constant("GroupObject");
        public static final Chars NEXT     = BlendConstants.NEXT;
        public static final Chars PREVIOUS = BlendConstants.PREVIOUS;
        /** This property points to an object */
        public static final Chars OBJECT   = Chars.constant("*ob");
    }

    public interface OBJECT{
        public static final Chars OBJECT = Chars.constant("Object");
        /** This property points to the real type, can be a mesh, armature, ... */
        public static final Chars DATA   = Chars.constant("*data");
        /** object parent to node matrix */
        public static final Chars MATRIX = Chars.constant("obmat");
        public static final Chars PARENT = BlendConstants.PARENT;
        public static final Chars ID     = Chars.constant("id");
        /**
         * bone deform model :
         * Object.defbase > N:bDeformGroup  : groups have a name which is the same as the bone
         * Mesh.dve > MDeformVert > (for each vertex):MDeformWeight
         * MDeformWeight.def_nr : point to the index of a bDeformGroup
         */
        public static final Chars DEFORMBASE = Chars.constant("defbase");
        public static final Chars DEFORMBASE_FIRST = BlendConstants.FIRST;
        public static final Chars DEFORMBASE_LAST = BlendConstants.LAST;
        /** Links to an AnimData */
        public static final Chars ADT = Chars.constant("*adt");
        /** This is used to indicate an instance rendering */
        public static final Chars DUPLICATE_GROUP = Chars.constant("*dup_group");
        /** used by armature object type, link to a bPose */
        public static final Chars POSE = Chars.constant("*pose");
        /** modifiers list */
        public static final Chars MODIFIERS = Chars.constant("modifiers");
        public static final Chars MODIFIERS_FIRST = BlendConstants.FIRST;
        public static final Chars MODIFIERS_LAST = BlendConstants.LAST;
    }

    public interface MESH{
        public static final Chars MESH     = Chars.constant("Mesh");
        public static final Chars MATERIAL = Chars.constant("**mat");
        public static final Chars MVERT    = Chars.constant("*mvert");
        public static final Chars MFACE    = Chars.constant("*mface");
        public static final Chars MPOLY    = Chars.constant("*mpoly");
        public static final Chars MLOOP    = Chars.constant("*mloop");
        //used for uv
        public static final Chars MLOOPUV  = Chars.constant("*mloopuv");
        /** links to a MDeformVert for bone weights */
        public static final Chars DVERT    = Chars.constant("*dvert");
        /** links to Key for shapekeys */
        public static final Chars KEY      = Chars.constant("*key");
    }

    public interface ARMATURE_MODIFIER_DATA{
        public static final Chars ARMATUREMODIFIERDATA = Chars.constant("ArmatureModifierData");
        public static final Chars MODIFIER = Chars.constant("modifier");
        /** name of the armature object */
        public static final Chars MODIFIER_NAME = Chars.constant("name");
        /** link to the object with this modifier */
        public static final Chars OBJECT = Chars.constant("*object");
    }

    public interface MVERT{
        public static final Chars MVERT   = Chars.constant("MVert");
        public static final Chars CO      = Chars.constant("co");
        public static final Chars NO      = Chars.constant("no");
        public static final Chars FLAG    = BlendConstants.FLAG;
        public static final Chars WEIGHT  = Chars.constant("bweight");
    }

    public interface MFACE{
        public static final Chars MFACE   = Chars.constant("MFace");
        public static final Chars V1      = Chars.constant("v1");
        public static final Chars V2      = Chars.constant("v2");
        public static final Chars V3      = Chars.constant("v3");
        public static final Chars V4      = Chars.constant("v4");
        public static final Chars MAT_NR  = Chars.constant("mat_nr");
        public static final Chars EDCODE  = Chars.constant("edcode");
        public static final Chars FLAG    = Chars.constant("flag");
    }

    public interface LOOP{
        public static final Chars VERTEX  = Chars.constant("v");
        public static final Chars EDGE    = Chars.constant("e");
    }

    public interface MLOOPUV{
        public static final Chars UV    = Chars.constant("uv");
        public static final Chars FLAGS = BlendConstants.FLAG;
    }

    public interface MPOLY{
        public static final Chars LOOPSTART    = Chars.constant("loopstart");
        public static final Chars TOTALLOOP    = Chars.constant("totloop");
        public static final Chars MAT_NR       = Chars.constant("mat_nr");
        public static final Chars FLAG         = Chars.constant("flag");
        public static final Chars PAD          = Chars.constant("pad");
    }

    public interface MATERIAL{
        public static final Chars MATERIAL = Chars.constant("Material");
        public static final Chars R = Chars.constant("r");
        public static final Chars G = Chars.constant("g");
        public static final Chars B = Chars.constant("b");
        public static final Chars SPECULAR_R = Chars.constant("specr");
        public static final Chars SPECULAR_G = Chars.constant("specg");
        public static final Chars SPECULAR_B = Chars.constant("specb");
        /** specular intensity : ~shininess */
        public static final Chars SPECULAR = Chars.constant("spec");
        /** transparency alpha */
        public static final Chars ALPHA = Chars.constant("alpha");
        /** ray_mirror : called reflectivity in the mirror panel */
        public static final Chars RAY_MIRROR = Chars.constant("ray_mirror");
        //something odd here, textures are not a list
        public static final Chars MTEX = Chars.constant("*mtex");
        public static final Chars NODETREE = Chars.constant("*nodetree");
        public static final Chars IPO = Chars.constant("*ipo");
    }

    public interface MTEXTURE{
        public static final Chars MTEXTURE = Chars.constant("MTex");
        public static final Chars TEX = Chars.constant("*tex");
        /**
         * This contains the texture use :
         * - 1 : diffuse
         * - 2 : normal
         * - X : TODO find others
         */
        public static final Chars MAPTO = Chars.constant("mapto");
        /** texture offset */
        public static final Chars OFS = Chars.constant("ofs");
        /** texture scale */
        public static final Chars SIZE = Chars.constant("size");
    }

    public interface TEXTURE{
        public static final Chars TEXTURE = Chars.constant("Tex");
        public static final Chars IMA = Chars.constant("*ima");
        /** type of extension :
         * 2 : clip
         * 3 : repeat
         * x : TODO find others
         */
        public static final Chars EXTEND = Chars.constant("extend");
    }

    public interface IMAGE{
        public static final Chars IMAGE = Chars.constant("Image");
        public static final Chars NAME = BlendConstants.NAME;
    }

    public interface BONE_ARMATURE{
        public static final Chars ARMATURE = Chars.constant("bArmature");
        public static final Chars BONE = Chars.constant("bonebase");
        public static final Chars BONE_FIRST = BlendConstants.FIRST;
        public static final Chars BONE_LAST = BlendConstants.LAST;
    }

    public interface BONE{
        public static final Chars BONE = Chars.constant("Bone");
        public static final Chars NAME = BlendConstants.NAME;
        public static final Chars PARENT = BlendConstants.PARENT;
        public static final Chars NEXT = BlendConstants.NEXT;
        public static final Chars CHILD = Chars.constant("childbase");
        public static final Chars CHILD_FIRST = BlendConstants.FIRST;
        public static final Chars CHILD_LAST = BlendConstants.LAST;
        public static final Chars _ROLL = Chars.constant("roll");
        /** column order 2D array for a Matrix 3x3 */
        public static final Chars MATRIX_BONE = Chars.constant("bone_mat");
        public static final Chars LENGTH = Chars.constant("length");
        /** column order 2D array for a Matrix 4x4 : root to node matrix */
        public static final Chars MATRIX_ARM = Chars.constant("arm_mat");
    }

    public interface MDEFORMVERT{
        public static final Chars MDEFORMVERT = Chars.constant("MDeformVert");
        /** link to a MDeformWeight */
        public static final Chars DW = Chars.constant("*dw");
        public static final Chars TOTWEIGHT = Chars.constant("totweight");
        public static final Chars FLAG = BlendConstants.FLAG;
    }

    public interface MDEFORMWEIGHT{
        public static final Chars MDEFORMWEIGHT = Chars.constant("MDeformWeight");
        public static final Chars DEF = Chars.constant("def_nr");
        public static final Chars WEIGHT = Chars.constant("weight");
    }

    public interface BDEFORMGROUP{
        /** Those are VertexGRoup in Blender : Bone deform name must match bone name */
        public static final Chars BDEFORMGROUP = Chars.constant("bDeformGroup");
        public static final Chars NAME = BlendConstants.NAME;
    }

    public interface BONE_POSE{
        public static final Chars BPOSE = Chars.constant("bPose");
        /** liste of bPoseChannel */
        public static final Chars CHANBASE = Chars.constant("chanbase");
        public static final Chars CHANBASE_FIRST = BlendConstants.FIRST;
        public static final Chars CHANBASE_LAST = BlendConstants.LAST;
    }

    public interface BONE_POSE_CHANNEL{
        public static final Chars BPOSECHANNEL = Chars.constant("bPoseChannel");
        public static final Chars NEXT = BlendConstants.NEXT;
        public static final Chars PREVIOUS = BlendConstants.PREVIOUS;
        public static final Chars NAME = BlendConstants.NAME;
        public static final Chars PARENT = BlendConstants.PARENT;
        public static final Chars CHILD = BlendConstants.CHILD;
        /** liste of bConstraint */
        public static final Chars CONSTRAINTS = Chars.constant("constraints");
        public static final Chars CONSTRAINTS_FIRST = BlendConstants.FIRST;
        public static final Chars CONSTRAINTS_LAST = BlendConstants.LAST;
    }

    public interface BONE_CONSTRAINT{
        public static final Chars BCONSTRAINT = Chars.constant("bConstraint");
        public static final Chars NEXT = BlendConstants.NEXT;
        public static final Chars PREVIOUS = BlendConstants.PREVIOUS;
        public static final Chars NAME = BlendConstants.NAME;
        /** point to various constraints type : bKinematicConstraint... */
        public static final Chars DATA = Chars.constant("*data");
        public static final Chars TYPE = Chars.constant("type");
        public static final Chars FLAG = BlendConstants.FLAG;
    }

    public interface BONE_KINEMATIC_CONSTRAINT{
        public static final Chars BKINEMATICCONSTRAINT = Chars.constant("bKinematicConstraint");
        /** target : armature object */
        public static final Chars TARGET = Chars.constant("*tar");
        /** kinematic chain length */
        public static final Chars ROOTBONE = Chars.constant("rootbone");
        /** target bone name in the armature */
        public static final Chars SUBTARGET = Chars.constant("subtarget");
    }

    public interface KEY{
        public static final Chars KEY = Chars.constant("Key");
        /** links to KeyBlocks */
        public static final Chars BLOCK = Chars.constant("block");
        public static final Chars BLOCK_FIRST = BlendConstants.FIRST;
        public static final Chars BLOCK_LAST = BlendConstants.LAST;
    }

    public interface KEYBLOCK{
        public static final Chars KEYBLOCK  = Chars.constant("KeyBlock");
        public static final Chars NEXT      = BlendConstants.NEXT;
        public static final Chars PREVIOUS  = BlendConstants.PREVIOUS;
        public static final Chars NAME      = BlendConstants.NAME;
        public static final Chars TYPE      = Chars.constant("type");
        public static final Chars DATA      = Chars.constant("*data");
    }

    public interface ANIMDATA{
        /** contains animation/motions informations */
        public static final Chars ANIMDATA = Chars.constant("AnimData");
        /** links to bAction */
        public static final Chars ACTION = Chars.constant("*action");
    }

    public interface BACTION{
        /** contains animation/motions informations */
        public static final Chars BDEFORMGROUP = Chars.constant("bAction");
        public static final Chars ID = BlendConstants.ID;
        public static final Chars ID_NAME = BlendConstants.NAME;
        /** links to FCurve */
        public static final Chars CURVES = Chars.constant("curves");
        public static final Chars CURVES_FIRST = BlendConstants.FIRST;
        public static final Chars CURVES_LAST = BlendConstants.LAST;
        /** links to bActionGroup */
        public static final Chars GROUPS = Chars.constant("groups");
        public static final Chars GROUPS_FIRST = BlendConstants.FIRST;
        public static final Chars GROUPS_LAST = BlendConstants.LAST;

    }

    public interface FCURVE{
        /** keyframe */
        public static final Chars FCURVE = Chars.constant("FCurve");
        /** link to a BezTriple */
        public static final Chars BEZT = Chars.constant("*bezt");
        /** Contains the name of the property modified by this curve */
        public static final Chars RNA_PATH = Chars.constant("*rna_path");
        /** array index in property vector/quaternion */
        public static final Chars ARRAY_INDEX = Chars.constant("array_index");
    }

    public interface BACTIONGROUP{
        /** keyframe */
        public static final Chars BACTIONGROUP = Chars.constant("bActionGroup");
        /** links to FCurve */
        public static final Chars CHANNELS = Chars.constant("channels");
        public static final Chars CHANNELS_FIRST = BlendConstants.FIRST;
        public static final Chars CHANNELS_LAST = BlendConstants.LAST;
        /** name of a bone */
        public static final Chars NAME = BlendConstants.NAME;

    }

    public interface BEZTRIPLE{
        /** keyframe */
        public static final Chars BEZTRIPLE = Chars.constant("BezTriple");
        /** 2D array, seems to contains time in second array*/
        public static final Chars VEC = Chars.constant("vec");

    }

    /**
     * Mesh subdivision modifier.
     * Unused
     */
    public interface SUBSURFMODIFIERDATA{

        public static final Chars DISPLACEMEMODIFIERDATA = Chars.constant("SubsurfModifierData");

    }

    /**
     * Mesh displacement mapping.
     */
    public interface DISPLACEMEMODIFIERDATA{

        public static final Chars DISPLACEMEMODIFIERDATA = Chars.constant("DisplaceModifierData");
        /**
         * texture of the displacement.
         */
        public static final Chars TEXTURE = Chars.constant("*texture");
        public static final Chars STRENGTH = Chars.constant("strength");

    }

}
