
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendGroupObject extends BlendFileBlock{

    public BlendGroupObject(BlendFileBlock block) {
        super(block);
    }

    public BlendObject getBlendObject(){
        return (BlendObject) parsedData.getChild(Blend_2_72.GROUP_OBJECT.OBJECT).getValue();
    }

}
