
package science.unlicense.format.blend.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.blend.BlendConstants;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class BlendFileHeader {

    /** 1 byte */
    public byte pointerSize;
    /** 1 byte */
    public byte endianess;
    /** 3 byte */
    public Chars version;

    public static Endianness getEncoding(byte endianess) throws StoreException{
        if (endianess == BlendConstants.LITTLE_ENDIAN){
            return Endianness.LITTLE_ENDIAN;
        } else if (endianess == BlendConstants.BIG_ENDIAN){
            return Endianness.BIG_ENDIAN;
        } else {
            throw new StoreException("Unknowned encoding : "+endianess);
        }
    }


}
