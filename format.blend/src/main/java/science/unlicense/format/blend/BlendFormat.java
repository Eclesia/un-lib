
package science.unlicense.format.blend;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Blender Format.
 *
 * Only resources available :
 * http://www.blender.org/development/architecture/blender-file-format/
 * http://www.atmind.nl/blender/mystery_ot_blend.html
 *
 * @author Johann Sorel
 */
public class BlendFormat extends AbstractModel3DFormat {

    public static final BlendFormat INSTANCE = new BlendFormat();

    private BlendFormat() {
        super(new Chars("BLEND"));
        shortName = new Chars("Blend");
        longName = new Chars("Blender3D");
        extensions.add(new Chars("blend"));
        signatures.add(BlendConstants.SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new BlendStore(input);
    }

}
