
package science.unlicense.format.blend;

import science.unlicense.format.blend.adaptor2_72.Adaptor_2_72;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.blend.model.BlendFile;

/**
 *
 * @author Johann Sorel
 */
public class BlendStore extends AbstractModel3DStore{

    private BlendFile blendFile;

    public BlendStore(Object input) {
        super(BlendFormat.INSTANCE,input);
    }

    public BlendFile getBlendFile() throws IOException, StoreException {
        if (blendFile==null){
            blendFile = new BlendFile((Path) getInput());
            blendFile.read(getSourceAsInputStream());
        }
        return blendFile;
    }

    public Collection getElements() throws StoreException {

        final Collection col = new ArraySequence();
        try {
            final BlendFile blendFile = getBlendFile();
            final Adaptor_2_72 adaptor = new Adaptor_2_72(blendFile);
            col.addAll(adaptor.convert());
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        return col;
    }

}
