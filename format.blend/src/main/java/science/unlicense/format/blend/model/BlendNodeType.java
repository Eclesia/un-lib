
package science.unlicense.format.blend.model;

import java.lang.reflect.Array;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.DefaultNodeType;
import science.unlicense.common.api.model.tree.NodeType;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.blend.BlendConstants;
import science.unlicense.format.blend.BlendDataInputStream;

/**
 *
 * @author Johann Sorel
 */
public class BlendNodeType extends DefaultNodeType{

    private final int length;

    //information on the data type
    private final boolean isPointer;
    private boolean isComplex = false;
    private int[] arrayLength = null;
    private ValueParser valueParser;

    public BlendNodeType(Chars type) {
        setId(type);
        length = 0;
        isPointer = false;
        isComplex = true;
        valueParser = new ComplexValueParser(this);
    }

    public BlendNodeType(Chars type, int length, boolean isPointer, int[] arrayLength) throws IOException {
        super();
        setId(type);
        this.length = length;
        this.isPointer = isPointer;
        this.arrayLength = arrayLength;

        if (isPointer){
            valueParser = new PointerValueParser();
            setBinding(Long.class);
            return;
        }

        if (BlendConstants.TYPE_CHAR.equals(type)){
            valueParser = new CharValueParser();
            setBinding(Byte.class);
        } else if (BlendConstants.TYPE_SHORT.equals(type)){
            valueParser = new ShortValueParser();
            setBinding(Short.class);
        } else if (BlendConstants.TYPE_INT.equals(type)){
            valueParser = new IntValueParser();
            setBinding(Integer.class);
        } else if (BlendConstants.TYPE_UINT64.equals(type)){
            valueParser = new LongValueParser();
            setBinding(Long.class);
        } else if (BlendConstants.TYPE_FLOAT.equals(type)){
            valueParser = new FloatValueParser();
            setBinding(Float.class);
        } else if (BlendConstants.TYPE_DOUBLE.equals(type)){
            valueParser = new DoubleValueParser();
            setBinding(Double.class);
        } else if (BlendConstants.TYPE_DOUBLE.equals(type)){
            valueParser = new DoubleValueParser();
            setBinding(Double.class);
        } else if (BlendConstants.TYPE_VOID.equals(type)){
            valueParser = new VoidValueParser();
            setBinding(Object.class);
        } else {
            //complex type
            isComplex = true;
        }

        if (arrayLength!=null){
            valueParser = new ArrayValueParser(arrayLength, valueParser);
            if (BlendConstants.TYPE_CHAR.equals(type)){
                setBinding(byte[].class);
            } else if (BlendConstants.TYPE_SHORT.equals(type)){
                setBinding(short[].class);
            } else if (BlendConstants.TYPE_INT.equals(type)){
                setBinding(int[].class);
            } else if (BlendConstants.TYPE_UINT64.equals(type)){
                setBinding(long[].class);
            } else if (BlendConstants.TYPE_FLOAT.equals(type)){
                setBinding(float[].class);
            } else if (BlendConstants.TYPE_DOUBLE.equals(type)){
                setBinding(double[].class);
            }
        }

    }

    public boolean isComplex() {
        return isComplex;
    }

    public boolean isPointer() {
        return isPointer;
    }

    public Object read(BlendDataInputStream ds) throws IOException {
        return valueParser.read(ds);
    }

    private static NodeType toType(Chars type){
        final DefaultNodeType fieldNodeType = new DefaultNodeType();
        fieldNodeType.setId(type);
        return fieldNodeType;
    }


    private static interface ValueParser{
        Object read(BlendDataInputStream ds) throws IOException;
        Object read1D(BlendDataInputStream ds, int size) throws IOException;
        Class dataType();
    }

    private static class PointerValueParser implements ValueParser{
        public Object read(BlendDataInputStream ds) throws IOException{
            return ds.readPointer();
        }
        public long[] read1D(BlendDataInputStream ds, int size) throws IOException {
            return ds.readPointers(new long[size]);
        }
        public Class dataType() {
            return long.class;
        }
    }

    private static class CharValueParser implements ValueParser{
        public Object read(BlendDataInputStream ds) throws IOException{
            return ds.readByte();
        }
        public byte[] read1D(BlendDataInputStream ds, int size) throws IOException {
            return ds.readFully(new byte[size]);
        }
        public Class dataType() {
            return byte.class;
        }
    }

    private static class ShortValueParser implements ValueParser{
        public Object read(BlendDataInputStream ds) throws IOException{
            return ds.readShort();
        }
        public short[] read1D(BlendDataInputStream ds, int size) throws IOException {
            return ds.readShort(size);
        }
        public Class dataType() {
            return short.class;
        }
    }

    private static class IntValueParser implements ValueParser{
        public Object read(BlendDataInputStream ds) throws IOException{
            return ds.readInt();
        }
        public int[] read1D(BlendDataInputStream ds, int size) throws IOException {
            return ds.readInt(size);
        }
        public Class dataType() {
            return int.class;
        }
    }

    private static class LongValueParser implements ValueParser{
        public Object read(BlendDataInputStream ds) throws IOException{
            return ds.readLong();
        }
        public long[] read1D(BlendDataInputStream ds, int size) throws IOException {
            return ds.readLong(size);
        }
        public Class dataType() {
            return long.class;
        }
    }

    private static class FloatValueParser implements ValueParser{
        public Object read(BlendDataInputStream ds) throws IOException{
            return ds.readFloat();
        }
        public float[] read1D(BlendDataInputStream ds, int size) throws IOException {
            return ds.readFloat(size);
        }
        public Class dataType() {
            return float.class;
        }
    }

    private static class DoubleValueParser implements ValueParser{
        public Object read(BlendDataInputStream ds) throws IOException{
            return ds.readDouble();
        }
        public double[] read1D(BlendDataInputStream ds, int size) throws IOException {
            return ds.readDouble(size);
        }
        public Class dataType() {
            return double.class;
        }
    }

    private static class VoidValueParser implements ValueParser{
        public Object read(BlendDataInputStream ds) throws IOException{
            return null;
        }
        public Object[] read1D(BlendDataInputStream ds, int size) throws IOException {
            return new Object[size];
        }
        public Class dataType() {
            return Object.class;
        }
    }

    private static class ArrayValueParser implements ValueParser{
        private final int[] lengths;
        private final ValueParser elementParser;

        public ArrayValueParser(int[] lengths, ValueParser elementParser) {
            this.lengths = lengths;
            this.elementParser = elementParser;
        }

        public Object read1D(BlendDataInputStream ds, int size) throws IOException {
            throw new UnimplementedException("Not supported.");
        }

        public Object read(BlendDataInputStream ds) throws IOException{
            if (lengths.length==1){
                final Object array = elementParser.read1D(ds, lengths[0]);
                if (elementParser.dataType() == byte.class){
                    //remove trailing zeros
                    return new Chars(BlendDataInputStream.trimAtZero((byte[]) array));
                } else {
                    return array;
                }
            } else if (lengths.length==2){
                final Object array = Array.newInstance(elementParser.dataType(), lengths[0], lengths[1]);
                for (int i=0;i<lengths[0];i++){
                    final Object sarray = Array.get(array, i);
                    for (int j=0;j<lengths[1];j++){
                        Array.set(sarray, j, elementParser.read(ds));
                    }
                }
                return array;
            }
            return null;
        }
        public Class dataType() {
            throw new UnimplementedException("Not supported yet.");
        }
    }

    private static class ComplexValueParser implements ValueParser{
        private BlendNodeType subtype;

        public ComplexValueParser(BlendNodeType subtype) {
            this.subtype = subtype;
        }

        public Object read(BlendDataInputStream ds) throws IOException{
            Sequence values = new ArraySequence();
            return values;
        }
        public Class dataType() {
            throw new UnimplementedException("Not supported yet.");
        }

        public Object read1D(BlendDataInputStream ds, int size) throws IOException {
            throw new UnimplementedException("Not supported yet.");
        }
    }

}
