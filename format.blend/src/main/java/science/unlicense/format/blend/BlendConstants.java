
package science.unlicense.format.blend;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.geometry.api.system.Axis;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.DefaultCoordinateSystem;
import science.unlicense.geometry.api.system.Direction;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.unitold.Units;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Quaternion;

/**
 * Blender format constants.
 *
 * @author Johann Sorel
 */
public final class BlendConstants {

    /** Blender file signature */
    public static final byte[] SIGNATURE = new byte[]{'B','L','E','N','D','E','R'};

    public static final CoordinateSystem COORDSYS = new DefaultCoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.METER),
                new Axis(Direction.FORWARD, Units.METER),
                new Axis(Direction.UP, Units.METER)
            }
    );

    /** Possible blocks */
    public static final Chars BLOCK_SDNA = Chars.constant(new byte[]{'D','N','A','1'});
    public static final Chars BLOCK_ENDB = Chars.constant(new byte[]{'E','N','D','B'});

    public static final byte LITTLE_ENDIAN = 'v';
    public static final byte BIG_ENDIAN = 'V';

    public static final byte POINTER_4 = '_';
    public static final byte POINTER_8 = '-';

    public static final Chars DNA_SDNA = Chars.constant(new byte[]{'S','D','N','A'});
    public static final Chars DNA_NAME = Chars.constant(new byte[]{'N','A','M','E'});
    public static final Chars DNA_TYPE = Chars.constant(new byte[]{'T','Y','P','E'});
    public static final Chars DNA_TLEN = Chars.constant(new byte[]{'T','L','E','N'});
    public static final Chars DNA_STRC = Chars.constant(new byte[]{'S','T','R','C'});

    //data types
    public static final Chars TYPE_INT = Chars.constant(new byte[]{'i','n','t'});
    public static final Chars TYPE_UINT64 = Chars.constant(new byte[]{'u','i','n','t','6','4','_','t'});
    public static final Chars TYPE_CHAR = Chars.constant(new byte[]{'c','h','a','r'});
    public static final Chars TYPE_SHORT = Chars.constant(new byte[]{'s','h','o','r','t'});
    public static final Chars TYPE_FLOAT = Chars.constant(new byte[]{'f','l','o','a','t'});
    public static final Chars TYPE_DOUBLE = Chars.constant(new byte[]{'d','o','u','b','l','e'});
    public static final Chars TYPE_VOID = Chars.constant(new byte[]{'v','o','i','d'});
    public static final int TYPE_POINTER = '*';
    public static final int TYPE_ARRAY_START = '[';
    public static final int TYPE_ARRAY_END = ']';

    public static final Chars BLOCK_TYPE_LINK = Chars.constant("Link");

    public static final Chars RNA_PATH_LOCATION = Chars.constant("location",CharEncodings.US_ASCII);
    public static final Chars RNA_PATH_ROTATION_QUATERNION = Chars.constant("rotation_quaternion",CharEncodings.US_ASCII);
    public static final Chars RNA_PATH_ROTATION_AXIS_ANGLE = Chars.constant("rotation_axis_angle",CharEncodings.US_ASCII);
    public static final Chars RNA_PATH_SCALE = Chars.constant("scale",CharEncodings.US_ASCII);

    private static final double COS45_2 = Math.cos(Angles.degreeToRadian(45.0))*2.0;

    //common tag names
    public static final Chars ID = Chars.constant("id");
    public static final Chars FLAG = Chars.constant("flag");
    public static final Chars NEXT = Chars.constant("*next");
    public static final Chars PREVIOUS = Chars.constant("*prev");
    public static final Chars PARENT = Chars.constant("*parent");
    public static final Chars CHILD = Chars.constant("*child");
    public static final Chars FIRST = Chars.constant("*first");
    public static final Chars LAST = Chars.constant("*last");
    public static final Chars NAME = Chars.constant("name");
    public static final Chars MODIFIER = Chars.constant("modifier");


    private BlendConstants(){}

    public static int getBtePointerSize(int pointersize) throws StoreException{
        if (pointersize==POINTER_4){
            return 4;
        } else if (pointersize==POINTER_8){
            return 8;
        } else {
            throw new StoreException("Unvalid pointer size : "+pointersize);
        }
    }

    /**
     * There are multiple variants to convert a quaternion to a matrix.
     * This is the formula used in blender.
     * We convert it the same way to reduce mathematical differences.
     *
     * @param q
     * @return
     */
    public static Matrix3x3 toMatrix3(Quaternion q){

        final double x = COS45_2 * q.get(0);
        final double y = COS45_2 * q.get(1);
        final double z = COS45_2 * q.get(2);
        final double w = COS45_2 * q.get(3);
        final double xx = x * x;
        final double xy = x * y;
        final double xz = x * z;
        final double xw = x * w;
        final double yy = y * y;
        final double yz = y * z;
        final double yw = y * w;
        final double zz = z * z;
        final double zw = z * w;

        final double[][] m = new double[3][3];
        m[0][0] = 1.0 - yy - zz;
        m[0][1] =       zw + xy;
        m[0][2] =      -yw + xz;

        m[1][0] =      -zw + xy;
        m[1][1] = 1.0 - xx - zz;
        m[1][2] =       xw + yz;

        m[2][0] =       yw + xz;
        m[2][1] =      -xw + yz;
        m[2][2] = 1.0 - xx - yy;

        return new Matrix3x3(m);
    }

}
