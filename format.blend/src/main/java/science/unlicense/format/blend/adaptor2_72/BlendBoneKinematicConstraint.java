
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendBoneKinematicConstraint extends BlendFileBlock{

    public BlendBoneKinematicConstraint(BlendFileBlock block) {
        super(block);
    }

    public int getChainLength(){
        return ((Number) parsedData.getChild(Blend_2_72.BONE_KINEMATIC_CONSTRAINT.ROOTBONE).getValue()).intValue();
    }

    public Chars getSubTarget(){
        return (Chars) parsedData.getChild(Blend_2_72.BONE_KINEMATIC_CONSTRAINT.SUBTARGET).getValue();
    }

}
