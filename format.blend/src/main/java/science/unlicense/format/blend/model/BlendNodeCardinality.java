
package science.unlicense.format.blend.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.model.tree.DefaultNodeCardinality;
import science.unlicense.common.api.model.tree.DefaultNodeType;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.NodeCardinality;
import science.unlicense.common.api.model.tree.NodeType;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.format.blend.BlendConstants;
import science.unlicense.format.blend.BlendDataInputStream;

/**
 *
 * @author Johann Sorel
 */
public class BlendNodeCardinality extends DefaultNodeCardinality{

    private final Chars fullName;

    public BlendNodeCardinality(Chars name, Chars type, int length) throws IOException {
        super(trimName(name), Chars.EMPTY, Chars.EMPTY,toType(type),1,1);
        this.fullName = name;

        final boolean isPointer = name.getCharacter(0).toUnicode() == BlendConstants.TYPE_POINTER;

        int[] arrayLength = null;
        int arrayStartIndex = name.getFirstOccurence(BlendConstants.TYPE_ARRAY_START);
        while (arrayStartIndex>=0){
            final int arrayEndIndex = name.getFirstOccurence(BlendConstants.TYPE_ARRAY_END);
            if (arrayLength==null){
                arrayLength = new int[1];
            } else {
                arrayLength = Arrays.resize(arrayLength, arrayLength.length+1);
            }
            arrayLength[arrayLength.length-1] = Int32.decode(name, arrayStartIndex+1, arrayEndIndex);

            name = name.truncate(arrayEndIndex+1, name.getCharLength());
            arrayStartIndex = name.getFirstOccurence(BlendConstants.TYPE_ARRAY_START);
        }

        setType(new BlendNodeType(type, length, isPointer, arrayLength));
    }

    public TypedNode read(BlendDataInputStream ds) throws IOException {
        final TypedNode node = new DefaultTypedNode(this);
        final BlendNodeType type = (BlendNodeType) getType();
        if (type.isComplexe()){
            final NodeCardinality[] properties = type.getChildrenTypes();
            final Node[] children = new Node[properties.length];
            for (int i=0;i<properties.length;i++){
                final BlendNodeCardinality card = (BlendNodeCardinality) properties[i];
                children[i] = card.read(ds);
            }
            node.getChildren().addAll(children);
        } else {
            node.setValue(type.read(ds));
        }
        return node;
    }

    /**
     * Remove array brackets.
     * @param name
     * @return clean name
     */
    private static Chars trimName(Chars name){
        final int arrayIndex = name.getFirstOccurence(BlendConstants.TYPE_ARRAY_START);
        if (arrayIndex !=-1){
            return name.truncate(0, arrayIndex);
        } else {
            return name;
        }
    }

    private static NodeType toType(Chars type){
        final DefaultNodeType fieldNodeType = new DefaultNodeType();
        fieldNodeType.setId(type);
        return fieldNodeType;
    }

}
