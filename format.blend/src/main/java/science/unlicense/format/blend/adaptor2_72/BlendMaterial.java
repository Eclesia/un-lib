
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendMaterial extends BlendIdentified{

    public BlendMaterial(BlendFileBlock block) {
        super(block);
    }

    public Color getDiffuse(){
        final float r = (Float) parsedData.getChild(Blend_2_72.MATERIAL.R).getValue();
        final float g = (Float) parsedData.getChild(Blend_2_72.MATERIAL.G).getValue();
        final float b = (Float) parsedData.getChild(Blend_2_72.MATERIAL.B).getValue();
        return new ColorRGB(r, g, b);
    }

    public Color getSpecular(){
        final float specr = (Float) parsedData.getChild(Blend_2_72.MATERIAL.SPECULAR_R).getValue();
        final float specg = (Float) parsedData.getChild(Blend_2_72.MATERIAL.SPECULAR_G).getValue();
        final float specb = (Float) parsedData.getChild(Blend_2_72.MATERIAL.SPECULAR_B).getValue();
        return new ColorRGB(specr, specg, specb);
    }

    public float getSpecularIntensity(){
        return (Float) parsedData.getChild(Blend_2_72.MATERIAL.SPECULAR).getValue();
    }

    public float getAlpha(){
        final float r = (Float) parsedData.getChild(Blend_2_72.MATERIAL.ALPHA).getValue();
        return r;
    }

    public BlendMTexture[] getMaterialTextures(){
        final BlendMTexture mtex0 = (BlendMTexture) parsedData.getChild(Blend_2_72.MATERIAL.MTEX).getValue();
        final BlendMTexture mtex1 = (BlendMTexture) parsedData.getChild(Blend_2_72.MATERIAL.NODETREE).getValue();
        final BlendMTexture mtex2 = (BlendMTexture) parsedData.getChild(Blend_2_72.MATERIAL.IPO).getValue();
        return new BlendMTexture[]{mtex0,mtex1,mtex2};
    }

    public float getReflection(){
        final float r = (Float) parsedData.getChild(Blend_2_72.MATERIAL.RAY_MIRROR).getValue();
        return r;
    }

}
