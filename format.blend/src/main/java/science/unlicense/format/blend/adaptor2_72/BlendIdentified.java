
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendIdentified extends BlendFileBlock{

    public BlendIdentified(BlendFileBlock block) {
        super(block);
    }

    public BlendId getId(){
        return new BlendId(parsedData.getChild(Blend_2_72.OBJECT.ID));
    }

}
