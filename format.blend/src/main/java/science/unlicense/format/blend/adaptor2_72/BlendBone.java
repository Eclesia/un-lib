
package science.unlicense.format.blend.adaptor2_72;

import science.unlicense.format.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendBone extends BlendFileBlock{

    public BlendBone(BlendFileBlock block) {
        super(block);
    }

}
