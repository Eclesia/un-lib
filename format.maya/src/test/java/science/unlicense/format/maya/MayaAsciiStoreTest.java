
package science.unlicense.format.maya;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.maya.ascii.MayaAsciiStore;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class MayaAsciiStoreTest {

    @Ignore
    @Test
    public void testReadCube() throws StoreException{

        final Path path = Paths.resolve(new Chars("todo find a file"));
        final MayaAsciiStore store = new MayaAsciiStore(path);

        store.getElements();

    }

}
