
package science.unlicense.format.maya.ascii;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.maya.MayaStore;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenType;
import science.unlicense.syntax.api.parser.Parser;
import science.unlicense.syntax.api.parser.Rule;
import science.unlicense.syntax.api.parser.SyntaxNode;
import science.unlicense.syntax.impl.grammar.io.UNGrammarReader;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class MayaAsciiStore extends MayaStore {

    public MayaAsciiStore(Path path) {
        super(MayaAsciiFormat.INSTANCE, path);
    }

    @Override
    public Collection getElements() throws StoreException{
        try {
            read();
        } catch (IOException ex) {
            throw new StoreException(ex);
        }
        throw new UnimplementedException("Not supported yet.");
    }

    protected Node read() throws IOException {
        final ByteInputStream in = getSourceAsInputStream();

        //parse text content
        final UNGrammarReader reader = new UNGrammarReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/model3d/maya/ma.gr")));
        final OrderedHashDictionary tokens = new OrderedHashDictionary();
        final OrderedHashDictionary rules = new OrderedHashDictionary();
        reader.read(tokens, rules);

        final Rule rule = (Rule) rules.getValue(new Chars("file"));

        //prepare lexer
        final Lexer lexer = new Lexer();
        lexer.setInput(in);

        //prepare parser
        final Parser parser = new Parser(rule);
        parser.setInput(lexer);
        final SyntaxNode node = parser.parse();

        final TokenType whitespace = (TokenType) tokens.getValue(new Chars("WS"));

        node.trim(new Predicate() {
            public Boolean evaluate(Object candidate) {
                final SyntaxNode sn = (SyntaxNode) candidate;
                final Token token = sn.getToken();
                return token!=null && token.type == whitespace;
            }
        });
        System.out.println(Nodes.toCharsTree(node, Chars.EMPTY, 20));

        return null;
    }

}
