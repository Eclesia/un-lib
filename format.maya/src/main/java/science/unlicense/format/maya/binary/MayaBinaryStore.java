
package science.unlicense.format.maya.binary;

import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.format.maya.MayaStore;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.maya.ascii.MayaAsciiFormat;

/**
 *
 * @author Johann Sorel
 */
public class MayaBinaryStore extends MayaStore {

    public MayaBinaryStore(Path path) {
        super(MayaAsciiFormat.INSTANCE, path);
    }

    @Override
    public Collection getElements() {
        throw new UnimplementedException("Not supported yet.");
    }

}
