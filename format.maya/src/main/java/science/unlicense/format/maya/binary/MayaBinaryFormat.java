
package science.unlicense.format.maya.binary;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class MayaBinaryFormat extends AbstractModel3DFormat {

    public static final MayaBinaryFormat INSTANCE = new MayaBinaryFormat();

    private MayaBinaryFormat() {
        super(new Chars("Maya-Binary"));
        shortName = new Chars("Maya-Binary");
        longName = new Chars("Maya Binary Format");
        extensions.add(new Chars("mb"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new MayaBinaryStore((Path) input);
    }

}
