
package science.unlicense.format.maya;

import science.unlicense.encoding.api.store.Format;
import science.unlicense.model3d.api.AbstractModel3DStore;


/**
 *
 * @author Johann Sorel
 */
public abstract class MayaStore extends AbstractModel3DStore {

    public MayaStore(Format format, Object input) {
        super(format, input);
    }



}
