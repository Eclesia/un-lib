
package science.unlicense.format.maya.ascii;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * Reference :
 * http://download.autodesk.com/us/maya/2010help/index.html?url=Maya_ASCII_file_format.htm,topicNumber=d0e677706
 *
 * @author Johann Sorel
 */
public class MayaAsciiFormat extends AbstractModel3DFormat {

    public static final MayaAsciiFormat INSTANCE = new MayaAsciiFormat();

    private MayaAsciiFormat() {
        super(new Chars("Maya-Ascii"));
        shortName = new Chars("Maya-Ascii");
        longName = new Chars("Maya Ascii Format");
        extensions.add(new Chars("ma"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new MayaAsciiStore((Path) input);
    }

}
