
package science.unlicense.format.html;

import science.unlicense.format.xml.XMLOutputStream;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class HTMLOutputStream extends XMLOutputStream{

    private static final Chars H1 = Chars.constant("h1");
    private static final Chars H2 = Chars.constant("h2");
    private static final Chars H3 = Chars.constant("h3");
    private static final Chars H4 = Chars.constant("h4");
    private static final Chars H5 = Chars.constant("h5");

    private static final Chars BR = Chars.constant("br");
    private static final Chars HR = Chars.constant("hr");
    private static final Chars SPAN = Chars.constant("span");
    private static final Chars CLASS = Chars.constant("class");
    private static final Chars A = Chars.constant("a");
    private static final Chars NAME = Chars.constant("name");
    private static final Chars HREF = Chars.constant("href");
    private static final Chars TARGET = Chars.constant("target");

    public HTMLOutputStream() {
    }

    public void writeH(int value, CharArray text) throws IOException{
        switch(value) {
            case 1 : writeElementStart(null, H1);writeText(text);writeElementEnd(null, H1); break;
            case 2 : writeElementStart(null, H1);writeText(text);writeElementEnd(null, H1); break;
            case 3 : writeElementStart(null, H1);writeText(text);writeElementEnd(null, H1); break;
            case 4 : writeElementStart(null, H1);writeText(text);writeElementEnd(null, H1); break;
            default : writeElementStart(null, H1);writeText(text);writeElementEnd(null, H1); break;
        }
    }

    public void writeBr() throws IOException{
        writeElementStart(null, BR);
        writeElementEnd(null, BR);
    }

    public void writeHr() throws IOException{
        writeElementStart(null, HR);
        writeElementEnd(null, HR);
    }

    public void writeSpanText(CharArray text, Chars styleClass) throws IOException{
        writeElementStart(null, SPAN);
        if (styleClass!=null) {
            writeProperty(CLASS, styleClass);
        }
        writeText(text);
        writeElementEnd(null, SPAN);
    }

    public void writeAnchor(Chars text, Chars name) throws IOException{
        writeElementStart(null, A);
        writeProperty(NAME, name);
        writeText(text);
        writeElementEnd(null, A);
    }

    public void writeAText(Chars text, Chars href, Chars target) throws IOException{
        writeElementStart(null, A);
        writeProperty(HREF, href);
        if (target!=null) {
            writeProperty(TARGET,target);
        }
        writeText(text);
        writeElementEnd(null, A);
    }

}
