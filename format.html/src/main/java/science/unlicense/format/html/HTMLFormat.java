
package science.unlicense.format.html;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public class HTMLFormat extends DefaultFormat {

    public HTMLFormat() {
        super(new Chars("html"));
        shortName = new Chars("HTML");
        longName = new Chars("Hypertext Markup Language");
        mimeTypes.add(new Chars("text/html"));
        extensions.add(new Chars("html"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
