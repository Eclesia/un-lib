
package science.unlicense.format.ini;

import science.unlicense.format.ini.INIReader;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.encoding.api.io.IOException;

/**
 * INI reader test.
 *
 * @author Johann Sorel
 */
public class INIReaderTest {

    /**
     * Read ini file test
     *
     * @throws IOException
     */
    @Test
    public void testReadDefault() throws IOException {

        Chars csv = new Chars(
                "; config file\n"
              + "id=1234\n"
              + " \n "
              + "[data]\n"
              + "isbn = ABCD \n"
              + "name = The Big Book \n"
              + "; a nice book\n"
              + "[user]\n"
              + "\t name = tom\n"
              + "job=none");

        INIReader reader = new INIReader();
        reader.setInput(csv.toBytes());

        Document doc = reader.read();
        Assert.assertEquals(new Chars("1234"), doc.getPropertyValue(new Chars("id")));
        Document data = (Document) doc.getPropertyValue(new Chars("data"));
        Assert.assertEquals(new Chars("ABCD"), data.getPropertyValue(new Chars("isbn")));
        Assert.assertEquals(new Chars("The Big Book"), data.getPropertyValue(new Chars("name")));
        Document user = (Document) doc.getPropertyValue(new Chars("user"));
        Assert.assertEquals(new Chars("tom"), user.getPropertyValue(new Chars("name")));
        Assert.assertEquals(new Chars("none"), user.getPropertyValue(new Chars("job")));
    }

}
