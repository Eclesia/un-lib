
package science.unlicense.format.ini;

import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.encoding.impl.store.keyvalue.DocumentSet;

/**
 *
 * @author Johann Sorel
 */
public class INIStore extends AbstractStore implements DocumentSet {

    public INIStore(Format format, Object source) {
        super(format, source);
    }

    @Override
    public Collection getDocuments() throws IOException {
        final INIReader reader = new INIReader();
        reader.setInput(source);
        final Document doc = reader.read();
        return Collections.singletonCollection(doc);
    }

    @Override
    public void insert(Collection docs) throws IOException {
        final INIWriter writer = new INIWriter();
        writer.setOutput(source);
        try {
            final Iterator ite = docs.createIterator();
            while (ite.hasNext()) {
                final Document doc = (Document) ite.next();
                writer.write(doc);
            }
        } finally {
            writer.dispose();
        }
    }

}
