
package science.unlicense.format.ini;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class INIWriter extends AbstractWriter {

    public void write(Document doc) throws IOException {
        CharOutputStream cs = getOutputAsCharStream(CharEncodings.UTF_8);
        writeDocument(cs, doc);
        cs.close();
    }

    private void writeDocument(CharOutputStream cs, Document doc) throws IOException {

        final Iterator ite = doc.getPropertyNames().createIterator();

        while (ite.hasNext()) {
            final Chars name = (Chars) ite.next();
            final Object value = doc.getPropertyValue(name);

            if (value instanceof Document) {
                cs.write('[');
                cs.write(name);
                cs.write(']');
                cs.endLine();
                writeDocument(cs, (Document) value);
            } else if (value != null) {
                cs.write(name);
                cs.write('=');
                cs.write(CObjects.toChars(value));
                cs.endLine();
            }

        }
    }

}
