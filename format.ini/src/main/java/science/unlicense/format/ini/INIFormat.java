
package science.unlicense.format.ini;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.impl.store.keyvalue.DocumentSet;

/**
 *
 * Resources :
 * https://en.wikipedia.org/wiki/INI_file
 *
 * @author Johann Sorel
 */
public class INIFormat extends DefaultFormat {

    public static final INIFormat INSTANCE = new INIFormat();

    private INIFormat() {
        super(new Chars("ini"));
        shortName = new Chars("INI");
        longName = new Chars("Initialization file");
        extensions.add(new Chars("ini"));
        resourceTypes.add(DocumentSet.class);
    }

    @Override
    public INIStore open(Object source) throws IOException {
        return new INIStore(this, source);
    }

}
