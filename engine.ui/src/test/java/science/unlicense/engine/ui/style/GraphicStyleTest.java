
package science.unlicense.engine.ui.style;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Contour;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.s2d.GeometryNode2D;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.software.SoftwarePainter2D;
import science.unlicense.engine.ui.io.UWKTReader;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Vector2f64;

/**
 * Test GraphicStyle.
 *
 * @author Johann Sorel
 */
public class GraphicStyleTest {

    private static final Rectangle EXTENT = new Rectangle(-10, -4, 20, 8);
    private static final Painter2D PAINTER = new SoftwarePainter2D(Images.create(new Extent.Long(1, 1), Images.IMAGE_TYPE_RGBA));

    /**
     * Test no scene node is created when no painting information is defined.
     */
    @Test
    public void testEmpty() {
        final GraphicStyle style = new GraphicStyle();
        final SceneNode scene = style.toSceneNode(PAINTER, EXTENT);
        Assert.assertNull(scene);
    }

    /**
     * Test simple rectangle graphic.
     */
    @Test
    public void testRectangle() {
        final GraphicStyle style = new GraphicStyle();
        style.setFillPaint(new ColorPaint(Color.BLUE));

        final SceneNode scene = style.toSceneNode(PAINTER, EXTENT);
        Assert.assertInstance(scene, GeometryNode2D.class);
        final GeometryNode2D node = (GeometryNode2D) scene;

        final PlanarGeometry geometry = node.getGeometry();
        Assert.assertInstance(geometry, Rectangle.class);
        final Rectangle rectangle = (Rectangle) geometry;
        Assert.assertEquals(new Rectangle(-10, -4, 20, 8), rectangle);

        final Paint[] fills = node.getFills();
        final Contour[] contours = node.getContours();
        Assert.assertEquals(1, fills.length);
        Assert.assertEquals(0, contours.length);
    }

    /**
     * Test margins graphic.
     */
    @Test
    public void testRectangleMargin() {
        final GraphicStyle style = new GraphicStyle();
        style.setFillPaint(new ColorPaint(Color.BLUE));
        style.setMargin(new Margin(2, 3, 4, 5));

        final SceneNode scene = style.toSceneNode(PAINTER, EXTENT);
        Assert.assertInstance(scene, GeometryNode2D.class);
        final GeometryNode2D node = (GeometryNode2D) scene;

        final PlanarGeometry geometry = node.getGeometry();
        Assert.assertInstance(geometry, Rectangle.class);
        final Rectangle rectangle = (Rectangle) geometry;
        Assert.assertEquals(new Rectangle(-10 - 5, -4 - 2, 20 + 3 + 5, 8 + 2 + 4), rectangle);

        final Paint[] fills = node.getFills();
        final Contour[] contours = node.getContours();
        Assert.assertEquals(1, fills.length);
        Assert.assertEquals(0, contours.length);
    }

    /**
     * Test graphic with a UWKT geometry.
     */
    @Test
    public void testShapeUWKT() throws IOException {
        final GraphicStyle style = new GraphicStyle();
        style.setFillPaint(new ColorPaint(Color.BLUE));
        style.setGeometry((PlanarGeometry) new UWKTReader().read(new Chars("POLYGON((0 0, 2 2, 0 0))")));

        final SceneNode scene = style.toSceneNode(PAINTER, EXTENT);
        Assert.assertInstance(scene, GeometryNode2D.class);
        final GeometryNode2D node = (GeometryNode2D) scene;

        final PlanarGeometry geometry = node.getGeometry();
        Assert.assertInstance(geometry, Polygon.class);
        final Polygon polygon = (Polygon) geometry;
        Assert.assertTrue(polygon.getInteriors().isEmpty());
        final TupleGridCursor cursor = polygon.getExterior().getCoordinates().cursor();
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(new Vector2f64(0, 0), cursor.samples());
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(new Vector2f64(2, 2), cursor.samples());
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(new Vector2f64(0, 0), cursor.samples());
        Assert.assertFalse(cursor.next());


        final Paint[] fills = node.getFills();
        final Contour[] contours = node.getContours();
        Assert.assertEquals(1, fills.length);
        Assert.assertEquals(0, contours.length);
    }
    /**
     * Test graphic with a UWKT geometry.
     */
    @Test
    public void testShapeUWKTPercent() throws IOException {
        final GraphicStyle style = new GraphicStyle();
        style.setFillPaint(new ColorPaint(Color.BLUE));
        style.setGeometry((PlanarGeometry) new UWKTReader().read(new Chars("POLYGON((0% 0%, 100% 100%, 50% 50%, 0% 0%))")));

        final SceneNode scene = style.toSceneNode(PAINTER, EXTENT);
        Assert.assertInstance(scene, GeometryNode2D.class);
        final GeometryNode2D node = (GeometryNode2D) scene;

        final PlanarGeometry geometry = node.getGeometry();
        Assert.assertInstance(geometry, Polygon.class);
        final Polygon polygon = (Polygon) geometry;
        Assert.assertTrue(polygon.getInteriors().isEmpty());
        final TupleGridCursor cursor = polygon.getExterior().getCoordinates().cursor();
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(new Vector2f64(-10, -4), cursor.samples());
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(new Vector2f64(10, 4), cursor.samples());
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(new Vector2f64(0, 0), cursor.samples());
        Assert.assertTrue(cursor.next());
        Assert.assertEquals(new Vector2f64(-10, -4), cursor.samples());
        Assert.assertFalse(cursor.next());


        final Paint[] fills = node.getFills();
        final Contour[] contours = node.getContours();
        Assert.assertEquals(1, fills.length);
        Assert.assertEquals(0, contours.length);
    }
}
