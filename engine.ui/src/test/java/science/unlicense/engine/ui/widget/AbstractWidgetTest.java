
package science.unlicense.engine.ui.widget;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.predicate.Predicate;

/**
 *
 * @author Johann Sorel
 */
public class AbstractWidgetTest {

    @Test
    public void testStackEnabled() {

        final CollectorListener collector = new CollectorListener();

        { // test a single widget
            final WSpace space = new WSpace();
            space.addEventListener(Predicate.TRUE, collector);

            space.setEnable(true);
            //should cause no event, value is true by default
            Assert.assertTrue(collector.messages.isEmpty());
            Assert.assertEquals(Boolean.TRUE, space.getPropertyValue(Widget.PROPERTY_STACKENABLE));

            space.setEnable(false);
            Assert.assertEquals(Boolean.FALSE, space.getPropertyValue(Widget.PROPERTY_STACKENABLE));
            Assert.assertEquals(2, collector.messages.getSize());
            Assert.assertInstance(collector.messages.get(0), PropertyMessage.class);
            Assert.assertInstance(collector.messages.get(1), PropertyMessage.class);
            PropertyMessage msg1 = (PropertyMessage) collector.messages.get(0);
            PropertyMessage msg2 = (PropertyMessage) collector.messages.get(1);
            Assert.assertEquals(Widget.PROPERTY_STACKENABLE, msg1.getPropertyName());
            Assert.assertEquals(Widget.PROPERTY_ENABLE, msg2.getPropertyName());
            Assert.assertEquals(Boolean.FALSE, msg2.getNewValue());
            collector.messages.removeAll();
        }

        { //test inherited
            final WContainer parent = new WContainer();
            final WSpace space = new WSpace();
            parent.getChildren().add(space);

            space.addEventListener(Predicate.TRUE, collector);

            parent.setEnable(false);
            Assert.assertEquals(Boolean.FALSE, space.getPropertyValue(Widget.PROPERTY_STACKENABLE));
            Assert.assertEquals(1, collector.messages.getSize());
            Assert.assertInstance(collector.messages.get(0), PropertyMessage.class);
            PropertyMessage msg1 = (PropertyMessage) collector.messages.get(0);
            Assert.assertEquals(Widget.PROPERTY_STACKENABLE, msg1.getPropertyName());
            Assert.assertEquals(Boolean.FALSE, msg1.getNewValue());
            collector.messages.removeAll();
        }

    }
}
