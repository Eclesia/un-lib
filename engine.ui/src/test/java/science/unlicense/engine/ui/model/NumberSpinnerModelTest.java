
package science.unlicense.engine.ui.model;

import org.junit.Test;
import static science.unlicense.common.api.Assert.*;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;

/**
 *
 * @author Johann Sorel
 */
public class NumberSpinnerModelTest {

    @Test
    public void testFloat64() {

        final NumberSpinnerModel m = new NumberSpinnerModel(Float64.class, 0.0, -2, +3, 0.5);
        assertEquals( 0.0, m.getDefaultValue());

        assertEquals( 0.5, m.nextValue(0.0));
        assertEquals( 2.0, m.nextValue(1.5));
        assertEquals( 2.5, m.nextValue(2.0));
        assertEquals(-0.5, m.previousValue(0.0));
        assertEquals(-2.0, m.previousValue(-1.5));

        //check model range
        assertTrue(m.hasNextValue(0.0));
        assertTrue(m.hasNextValue(2.0));
        assertTrue(m.hasNextValue(2.5));
        assertFalse(m.hasNextValue(3.0));
        assertTrue(m.hasPreviousValue(0.0));
        assertTrue(m.hasPreviousValue(-1.0));
        assertTrue(m.hasPreviousValue(-1.5));
        assertFalse(m.hasPreviousValue(-2.0));
        try {
            m.previousValue(-2.0);
            fail("InvalidArgumentException expected");
        } catch (InvalidArgumentException ex) {/* ok */}
        try {
            m.nextValue(3.0);
            fail("InvalidArgumentException expected");
        } catch (InvalidArgumentException ex) {/* ok */}

    }

    @Test
    public void testInt32() {

        final NumberSpinnerModel m = new NumberSpinnerModel(Int32.class, 0.0, -2, +3, 1);
        assertEquals( 0, m.getDefaultValue());

        assertEquals( 1, m.nextValue(0));
        assertEquals( 2, m.nextValue(1));
        assertEquals( 3, m.nextValue(2));
        assertEquals(-1, m.previousValue(0));
        assertEquals(-2, m.previousValue(-1));

        //check model range
        assertTrue(m.hasNextValue(0));
        assertTrue(m.hasNextValue(2));
        assertFalse(m.hasNextValue(3));
        assertTrue(m.hasPreviousValue(0));
        assertTrue(m.hasPreviousValue(-1));
        assertFalse(m.hasPreviousValue(-2));
        try {
            m.previousValue(-2);
            fail("InvalidArgumentException expected");
        } catch (InvalidArgumentException ex) {/* ok */}
        try {
            m.nextValue(3);
            fail("InvalidArgumentException expected");
        } catch (InvalidArgumentException ex) {/* ok */}

    }
}
