
package science.unlicense.engine.ui.style;

import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 * System style test.
 *
 * @author Johann Sorel
 */
public class SystemStyleTest {

    @Test
    public void testDefault() {

        final StyleDocument style = SystemStyle.INSTANCE;
        Assert.assertEquals(51, style.getRules().getSize());

    }

}
