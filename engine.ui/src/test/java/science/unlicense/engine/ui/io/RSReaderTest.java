
package science.unlicense.engine.ui.io;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.predicate.Constant;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.format.json.HJSONReader;
import science.unlicense.format.json.JSONComparator;

/**
 *
 * @author Johann Sorel
 */
public class RSReaderTest {

    @Test
    public void testReadDocArray(){
        final Chars text = new Chars(
                "{"
              + "filter : true,"
              + "multi.1 : {name:'one'},"
              + "multi.2 : {name:'two'},"
              + "multi.4 : {name:'four'}"
              + "}");

        final StyleDocument rule = RSReader.readStyleDoc(text);
        final Object[] array = WidgetStyles.getFieldValues(rule.getProperty(new Chars("multi")));
        Assert.assertEquals(5, array.length);
        final Document doc0 = (Document) array[0];
        final Document doc1 = (Document) array[1];
        final Document doc2 = (Document) array[2];
        final Document doc3 = (Document) array[3];
        final Document doc4 = (Document) array[4];
        Assert.assertNull(doc0);
        Assert.assertEquals(new Constant(new Chars("one")), doc1.getProperty(new Chars("name")).getValue());
        Assert.assertEquals(new Constant(new Chars("two")), doc2.getProperty(new Chars("name")).getValue());
        Assert.assertNull(doc3);
        Assert.assertEquals(new Constant(new Chars("four")), doc4.getProperty(new Chars("name")).getValue());

    }

    @Test
    public void testWriteNoneProperty(){
        final Chars text = new Chars(
                "{\n"
              + "filter : true\n"
              + "graphic : none\n"
              + "}");

        final StyleDocument rule = RSReader.readStyleDoc(text);
        final Object[] array = WidgetStyles.getFieldValues(rule.getProperty(new Chars("graphic")));
        Assert.assertEquals(1, array.length);
        Assert.assertTrue(array[0] instanceof Constant);
        Assert.assertTrue(WidgetStyles.isNone(array[0]));

        Assert.assertEquals(text, RSWriter.toChars(rule));

    }

    @Test
    public void testWriteRuleProperty(){
        final Chars text = new Chars(
                "{\n"
              + "base : {\n"
              + "  filter : true\n"
              + "  graphic : {\n"
              + "    color : rgba(255 255 255 255)\n"
              + "    }\n"
              + "  }\n"
              + "}");

        final StyleDocument style = RSReader.readStyleDoc(text);
        Assert.assertEquals(text, RSWriter.toChars(style));

    }

    @Test
    public void testWriteFunctionProperty(){
        final Chars text = new Chars(
                "{\n"
              + "base : {\n"
              + "  filter : true\n"
              + "  graphic : {\n"
              + "    color : lineargradientfill('%' 0 0 0 1 0 rotateHSL(@color-main 0 1 1.8) 1 rotateHSL(@color-main 0 1 1.2))\n"
              + "    }\n"
              + "  }\n"
              + "}");

        final StyleDocument style = RSReader.readStyleDoc(text);
        Assert.assertEquals(text, RSWriter.toChars(style));
    }

    @Test
    public void testReadArray(){
        final Chars text = new Chars(
                "{\n"
              + "filter : true\n"
              + "vals : [1,2,3,4]\n"
              + "}");

        final StyleDocument style = RSReader.readStyleDoc(text);
        final Object[] array = WidgetStyles.getFieldValues(style.getProperty(new Chars("vals")));
        Assert.assertEquals(1, array.length);
        Assert.assertTrue(array[0] instanceof Constant);
        final Object value = ((Constant) array[0]).getValue();
        Assert.assertTrue(value instanceof Sequence);
        final Sequence values = (Sequence) value;
        Assert.assertEquals(4, values.getSize());
        Assert.assertEquals(1, ((Constant) values.get(0)).getValue());
        Assert.assertEquals(2, ((Constant) values.get(1)).getValue());
        Assert.assertEquals(3, ((Constant) values.get(2)).getValue());
        Assert.assertEquals(4, ((Constant) values.get(3)).getValue());

        Assert.assertEquals(text, RSWriter.toChars(style));
    }

    @Test
    public void testReadStyle() throws IOException{
        final Chars text = new Chars(
                "{\n"
              + "base : {\n"
              + "  filter : true\n"
              + "  }\n"
              + "}");

        final StyleDocument style = RSReader.readStyleDoc(text);

        Assert.assertEquals(text, RSWriter.toChars(style));
    }

    @Test
    public void testReadEscape(){
        final Chars text = new Chars(
                "{\n"
              + "filter : true\n"
              + "val : '\\u0048'\n"
              + "}");

        final StyleDocument style = RSReader.readStyleDoc(text);
        final Object[] array = WidgetStyles.getFieldValues(style.getProperty(new Chars("val")));
        Assert.assertEquals(1, array.length);
        Assert.assertTrue(array[0] instanceof Constant);
        Assert.assertEquals(new Chars("H"), ((Constant) array[0]).getValue());

        //escaped char has been converted to 'H'
        final Chars text2 = new Chars(
                "{\n"
              + "filter : true\n"
              + "val : 'H'\n"
              + "}");
        Assert.assertEquals(text2, RSWriter.toChars(style));
    }

    @Test
    public void testReadFilter(){
        final Chars text = new Chars(
                "{\n"
              + "filter : $abc = true\n"
              + "}");

        final StyleDocument style = RSReader.readStyleDoc(text);
        final Object[] array = WidgetStyles.getFieldValues(style.getProperty(new Chars("filter")));
        Assert.assertEquals(1, array.length);
        Assert.assertTrue(array[0] instanceof EqualsPredicate);
        EqualsPredicate predicate = (EqualsPredicate) array[0];
        Assert.assertEquals(new PropertyName( Chars.constant("abc")), predicate.getExp1());
        Assert.assertEquals(new Constant(true), predicate.getExp2());

        Assert.assertEquals(text, RSWriter.toChars(style));
    }

    /**
     * Test reading properties
     */
    @Test
    public void testReadProperties() throws IOException{
//        final Chars text = new Chars(
//                "border-brush-width : 1\n" +
//                "border-brush-linecap : 'round'\n" +
//                "border-radius : [3,1,2]\n" +
//                "|some comments\n"+
//                "border-brush-paint : #FF0000\n" +
//                "byreference1 : @system:font\n" +
//                "|some comments\n"+
//                "|some comments\n"+
//                "byreference2 : @bgcolor\n" +
//                "border-fill-paint : lineargradientfill('%',0,0,0,100,0,#FFFFFF,1,#AAAAAA)");
//
//        final Dictionary properties = WCSParser.readProperties(text);
//
//        Assert.assertEquals(7, properties.getSize());
//
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-width")), new Constant(1d));
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-linecap")), new Constant(new Chars("round")));
//        Assert.assertEquals(properties.getValue(new Chars("border-radius")), new Constant(
//                new ArraySequence( new Object[]{
//                    new Constant(3d),
//                    new Constant(1d),
//                    new Constant(2d)})));
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-paint")), new Constant(new ColorRGB(255, 0, 0, 255)));
//        Assert.assertEquals(properties.getValue(new Chars("byreference1")),
//                new ExpressionReference(new Chars("system"), new Chars("font")));
//        Assert.assertEquals(properties.getValue(new Chars("byreference2")),
//                new ExpressionReference(null,new Chars("bgcolor")));
//        //TODO test gradient, not sure of definition yet
    }

    /**
     * Test reading classes
     */
    @Test
    public void testReadClasses() throws IOException{
//        final Chars text = new Chars(
//                "| some comments\n" +
//                "myclass-enable {\n" +
//                "    border-brush-width : 1\n" +
//                "    | some comments\n"+
//                "    border-brush-linecap : 'round'\n" +
//                "}\n" +
//                "| some comments\n"+
//                "\n" +
//                "myclass-event {\n" +
//                "    | some comments\n"+
//                "    border-brush-paint : #FF0000\n" +
//                "    byreference1 : @system:font\n" +
//                "    byreference2 : @bgcolor\n" +
//                "    | some comments\n"+
//                "}");
//
//        final WStyle style = WCSParser.readStyle(text);
//
//        Dictionary properties = style.getClass(new Chars("myclass-enable")).getLocalProperties();
//        Assert.assertEquals(2, properties.getSize());
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-width")), new Constant(1d));
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-linecap")), new Constant(new Chars("round")));
//
//
//        properties = style.getClass(new Chars("myclass-event")).getLocalProperties();
//        Assert.assertEquals(3, properties.getSize());
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-paint")), new Constant(new ColorRGB(255, 0, 0, 255)));
//        Assert.assertEquals(properties.getValue(new Chars("byreference1")),
//                new ExpressionReference(new Chars("system"), new Chars("font")));
//        Assert.assertEquals(properties.getValue(new Chars("byreference2")),
//                new ExpressionReference(null,new Chars("bgcolor")));
    }

    private static void compare(Object expected, Object result) throws IOException {

        if (expected instanceof StyleDocument) {
            expected = RSWriter.toChars((StyleDocument) expected);
        }
        if (result instanceof StyleDocument) {
            result = RSWriter.toChars((StyleDocument) result);
        }

        final HJSONReader reader1 = new HJSONReader();
        reader1.setInput(expected);
        final HJSONReader reader2 = new HJSONReader();
        reader2.setInput(result);
        new JSONComparator().compare(reader1, reader2);
    }

}
