
package science.unlicense.engine.ui.widget;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Images;

/**
 * Test WLabel widget.
 *
 * @author Johann Sorel
 */
public class WLabelTest {

    @Test
    public void testBestExtent(){
        WLabel lbl;
        Extent extent;

        //check empty extent
        lbl = new WLabel();
        lbl.setText(Chars.EMPTY);
        extent = lbl.getExtents(null).getBest(null);
        Assert.assertEquals(new Extent.Double(0, 0), extent);

        //check forcing extent
        lbl.setOverrideExtents(new Extents(120, 30));
        lbl.setText(new Chars("fdsfds"));
        extent = lbl.getExtents(null).getBest(null);
        Assert.assertEquals(new Extent.Double(120, 30), extent);

        //check image extend
        lbl = new WLabel();
        lbl.setGraphic(new WGraphicImage(Images.create(new Extent.Long(20, 24),Images.IMAGE_TYPE_RGB)));
        extent = lbl.getExtents(null).getBest(null);
        Assert.assertEquals(new Extent.Double(20, 24), extent);

    }

}
