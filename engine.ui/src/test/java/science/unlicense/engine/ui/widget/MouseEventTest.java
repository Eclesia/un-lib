
package science.unlicense.engine.ui.widget;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class MouseEventTest {

    @Test
    public void testReceiveEvent(){
        final Listener listener = new Listener();

        final WContainer c = new WContainer();
        c.addEventListener(MouseMessage.PREDICATE, listener);

        Tuple position = new Vector2f64(0, 0);
        Tuple screenposition = new Vector2f64(0, 0);
        c.receiveEvent(new Event(null, new MouseMessage(MouseMessage.TYPE_TYPED, MouseMessage.BUTTON_1, 1, position, screenposition, 0, false)));

        Assert.assertEquals(1,listener.events.getSize());

    }

    @Test
    public void testChildReceiveEvent(){
        final Listener listener = new Listener();

        final WContainer c = new WContainer(new GridLayout(2, 2));
        c.setEffectiveExtent(new Extent.Double(20,20));
        c.getNodeTransform().setToTranslation(new double[]{10,10});

        final WSpace space1 = new WSpace(new Extent.Double(10, 10));
        final WSpace space2 = new WSpace(new Extent.Double(10, 10));
        final WSpace space3 = new WSpace(new Extent.Double(10, 10));
        final WSpace space4 = new WSpace(new Extent.Double(10, 10));
        c.getChildren().add(space1);
        c.getChildren().add(space2);
        c.getChildren().add(space3);
        c.getChildren().add(space4);
        space1.addEventListener(MouseMessage.PREDICATE, listener);

        Tuple position = new Vector2f64(-5, -5);
        Tuple screenposition = new Vector2f64(0, 0);
        c.receiveEvent(new Event(null,new MouseMessage(MouseMessage.TYPE_TYPED, MouseMessage.BUTTON_1, 1, position, screenposition, 0, false)));

        //mouse enter + click
        Assert.assertEquals(2,listener.events.getSize());

    }

    @Test
    public void testMouseEnterExit(){
        final Listener l1 = new Listener();
        final Listener l2 = new Listener();
        final Listener l3 = new Listener();

        final WContainer c1 = new WContainer(new AbsoluteLayout());
        c1.setEffectiveExtent(new Extent.Double(30,30));
        c1.addEventListener(MouseMessage.PREDICATE, l1);

        final WContainer c2 = new WContainer(new AbsoluteLayout());
        c2.setEffectiveExtent(new Extent.Double(10,10));
        c2.addEventListener(MouseMessage.PREDICATE, l2);

        final WSpace c3 = new WSpace(new Extent.Double(10, 10));
        c3.setEffectiveExtent(new Extent.Double(6, 6));
        c3.addEventListener(MouseMessage.PREDICATE, l3);

        c1.getChildren().add(c2);
        c2.getChildren().add(c3);

        VectorRW position = new Vector2f64(0, 0);
        VectorRW screenposition = new Vector2f64(0, 0);
        c1.receiveEvent(new Event(null,new MouseMessage(MouseMessage.TYPE_MOVE, 0, -1, position, screenposition, 0, false)));

        Assert.assertEquals(1, l1.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_MOVE, ((MouseMessage) l1.events.get(0)).getType());
        Assert.assertEquals(2, l2.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_ENTER, ((MouseMessage) l2.events.get(0)).getType());
        Assert.assertEquals(MouseMessage.TYPE_MOVE, ((MouseMessage) l2.events.get(1)).getType());
        Assert.assertEquals(2, l3.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_ENTER, ((MouseMessage) l3.events.get(0)).getType());
        Assert.assertEquals(MouseMessage.TYPE_MOVE, ((MouseMessage) l3.events.get(1)).getType());

        l1.events.removeAll();
        l2.events.removeAll();
        l3.events.removeAll();

        position.setXY(-100, -100);
        screenposition.setXY(-100, -100);
        c1.receiveEvent(new Event(null,new MouseMessage(MouseMessage.TYPE_MOVE, 0, -1, position, screenposition, 0, false)));

        Assert.assertEquals(1, l1.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_MOVE, ((MouseMessage) l1.events.get(0)).getType());
        Assert.assertEquals(1, l2.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_EXIT, ((MouseMessage) l2.events.get(0)).getType());
        Assert.assertEquals(1, l3.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_EXIT, ((MouseMessage) l3.events.get(0)).getType());


    }

    /**
     * Check the child node process the event before the parent and that the parent receive the consumed state.
     */
    @Test
    public void testMouseChildEventBeforeParent() {

        final Sequence order = new ArraySequence();
        final Sequence types = new ArraySequence();
        final Sequence states = new ArraySequence();

        EventListener lst = new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                order.add(event.getSource());
                types.add( ((MouseMessage) event.getMessage()).getType() );
                states.add(event.getMessage().isConsumed());
                event.getMessage().consume();
            }
        };

        final WContainer container = new WContainer(new AbsoluteLayout());
        container.setEffectiveExtent(new Extent.Double(10,10));
        final WSpace child = new WSpace(new Extent.Double(10, 10));
        child.setEffectiveExtent(new Extent.Double(10, 10));
        container.getChildren().add(child);

        container.addEventListener(MouseMessage.PREDICATE, lst);
        child.addEventListener(MouseMessage.PREDICATE, lst);


        //send event
        TupleRW position = new Vector2f64(0, 0);
        TupleRW screenposition = new Vector2f64(0, 0);
        container.receiveEvent(new Event(null,new MouseMessage(MouseMessage.TYPE_PRESS, 0, -1, position, screenposition, 0, false)));

        Assert.assertEquals(3, order.getSize());

        Assert.assertEquals(child, order.get(0));
        Assert.assertEquals(false, states.get(0));
        Assert.assertEquals(MouseMessage.TYPE_ENTER, types.get(0));

        Assert.assertEquals(child, order.get(1));
        Assert.assertEquals(false, states.get(1));
        Assert.assertEquals(MouseMessage.TYPE_PRESS, types.get(1));

        Assert.assertEquals(container, order.get(2));
        Assert.assertEquals(true, states.get(2));
        Assert.assertEquals(MouseMessage.TYPE_PRESS, types.get(2));


    }


    private static class Listener implements EventListener{

        private final Sequence events = new ArraySequence();

        @Override
        public void receiveEvent(Event event) {
            if (event.getMessage() instanceof MouseMessage){
                events.add(event.getMessage());
            }
        }

    }

}
