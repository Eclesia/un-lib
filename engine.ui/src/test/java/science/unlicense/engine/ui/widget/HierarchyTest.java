

package science.unlicense.engine.ui.widget;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.common.api.predicate.Or;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.visual.VirtualPainter2D;
import science.unlicense.geometry.api.BBox;

/**
 *
 * @author Johann Sorel
 */
public class HierarchyTest {

    private static final Painter2D PAINTER = new VirtualPainter2D();
    private static final BBox AREA = new BBox(new double[]{0,0}, new double[]{500,500});

    @Test
    public void testHierarchyChange(){
//
//        final WContainer container1 = new WContainer(new GridLayout(1, 1));
//        final WContainer container2 = new WContainer(new GridLayout(1, 1));
//        container1.setEffectiveExtent(new Extent.Double(10, 10));
//        container2.setEffectiveExtent(new Extent.Double(10, 10));
//
//        final WLabel lbl = new WLabel();
//        lbl.setBestExtent(new Extent.Double(10, 10));
//
//        Assert.assertTrue(container1.isDirty());
//        Assert.assertTrue(container2.isDirty());
//        container1.render(PAINTER, AREA);
//        container2.render(PAINTER, AREA);
//        Assert.assertFalse(container1.isDirty());
//        Assert.assertFalse(container2.isDirty());
//
//        final int before = lbl.getEventManager().getNbListeners();
//
//        //check adding a children makes the container dirty
//        container1.addChild(lbl);
//        Assert.assertEquals(container1,lbl.getParent());
//        Assert.assertTrue(container1.isDirty());
//        container1.render(PAINTER, AREA);
//        Assert.assertFalse(container1.isDirty());
//
//        final int after = lbl.getEventManager().getNbListeners();
//
//        //remove it an ensure it does not have effects on the previous parent
//        container1.removeChild(lbl);
//        Assert.assertTrue(container1.isDirty());
//        Assert.assertNull(lbl.getParent());
//        container1.render(PAINTER, AREA);
//        Assert.assertFalse(container1.isDirty());
//        lbl.setBestExtent(new Extent.Double(20, 20));
//        Assert.assertFalse(container1.isDirty());
//        lbl.setDirty();
//        Assert.assertFalse(container1.isDirty());
//
//        final int after2 = lbl.getEventManager().getNbListeners();
//        Assert.assertTrue(before<after);
//        Assert.assertEquals(before, after2);
//
//        //test transfering child
//
//

    }

    /**
     * Change enable state must fire a stackenable event.
     *
     */
    @Test
    public void testStackEnable(){

        final Listener listener = new Listener();

        //test single widget event
        final WSpace space = new WSpace();
        space.addEventListener(
                new Or(new Predicate[]{
                    new PropertyPredicate(Widget.PROPERTY_ENABLE),
                    new PropertyPredicate(Widget.PROPERTY_STACKENABLE)}), listener
                );

        space.setEnable(false);
        Assert.assertEquals(2, listener.events.getSize());
        Assert.assertEquals(Widget.PROPERTY_ENABLE, ((PropertyMessage) listener.events.get(0)).getPropertyName());
        Assert.assertEquals(Boolean.FALSE, ((PropertyMessage) listener.events.get(0)).getNewValue());
        Assert.assertEquals(Widget.PROPERTY_STACKENABLE, ((PropertyMessage) listener.events.get(1)).getPropertyName());
        Assert.assertEquals(Boolean.FALSE, ((PropertyMessage) listener.events.get(1)).getNewValue());

        space.setEnable(true);
        listener.events.removeAll();

        //test changing the paren enable state
        final WContainer container = new WContainer(new GridLayout(1,1));
        container.getChildren().add(space);
        container.setEnable(false);

        Assert.assertEquals(1, listener.events.getSize());
        Assert.assertEquals(Widget.PROPERTY_STACKENABLE, ((PropertyMessage) listener.events.get(0)).getPropertyName());
        Assert.assertEquals(Boolean.FALSE, ((PropertyMessage) listener.events.get(0)).getNewValue());

    }

    private static class Listener implements EventListener{

        private final Sequence events = new ArraySequence();

        @Override
        public void receiveEvent(Event event) {
            events.add(event.getMessage());
        }


    }

}
