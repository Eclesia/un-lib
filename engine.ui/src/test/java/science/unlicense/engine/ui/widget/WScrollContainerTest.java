

package science.unlicense.engine.ui.widget;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.api.Similarity;

/**
 *
 * @author Johann Sorel
 */
public class WScrollContainerTest {

    private static final double DELTA = 0.000001;

    /**
     * Test a normal scrolled widget.
     */
    @Test
    @Ignore
    public void testSmallScrolledWidth(){

        final WLeaf leaf = new WSpace(new Extent.Double(20, 20));
        final WScrollContainer container = new WScrollContainer(leaf);
        container.setEffectiveExtent(new Extent.Double(100, 80));

        //scrolled widget should have a width 100 - scrollbar width
        final Similarity transform = leaf.getNodeTransform();
        final Extent size = leaf.getEffectiveExtent();
        Assert.assertEquals(new Extent.Double(82d, 20d),size);
        Assert.assertEquals(new Matrix3x3(
                1, 0, 0,
                0, 1, 0,
                0, 0, 1),
                transform.viewMatrix());

    }

    /**
     * Test a large widget.
     */
    @Test
    @Ignore
    public void testLargeScrolledWidth(){

//        final WTable leaf = new WTable();
//        final WScrollContainer container = new WScrollContainer(leaf);
//        container.setEffectiveExtent(new Extent.Double(100, 80));
//
//        //scrolled widget should have a width 100 - scrollbar width
//        final NodeTransform transform = new NodeTransform(2);
//        final Extent size = new Extent.Double(2);
//        container.getLayout().getTransform(container.getScrollingContainer(), transform, size);
//        container.getScrollingContainer().getLayout().getTransform(leaf, transform, size);
//        Assert.assertEquals(new Extent.Double(82d, 0d),size);
//        Assert.assertEquals(new Matrix3(
//                1, 0, 0,
//                0, 1, 0,
//                0, 0, 1),
//                transform.asMatrix());

    }

}
