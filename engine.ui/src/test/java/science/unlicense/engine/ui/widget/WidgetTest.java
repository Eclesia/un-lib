
package science.unlicense.engine.ui.widget;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.predicate.Constant;
import science.unlicense.display.api.desktop.Frame;
import science.unlicense.display.api.desktop.FrameDecoration;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.io.RSReader;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.visual.VirtualFrame;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class WidgetTest {

    /**
     * Test widget screen position computation.
     */
    @Test
    public void testGetOnScreenPosition() {

        final UIFrame frame = new VirtualFrame();
        frame.setSize(200, 100);
        final WContainer container = new WContainer(new AbsoluteLayout());
        frame.getContainer().setLayout(new BorderLayout());
        frame.getContainer().addChild(container, BorderConstraint.CENTER);

        //test at 0,0
        Assert.assertEquals(new Vector2f64(0, 0), container.getOnSreenPosition());

        //test with frame position
        frame.setOnScreenLocation(new Vector2f64(50, 20));
        Assert.assertEquals(new Vector2f64(50, 20), container.getOnSreenPosition());

        //test with frame decoration
        frame.setDecoration(new FrameDecoration() {
            public Margin getMargin() {
                return new Margin(10, 5, 10, 5);
            }
            public Frame getFrame() {return frame; }
            public void setFrame(Frame frame) {}
        });
        Assert.assertEquals(new Vector2f64(50+5, 20+10), container.getOnSreenPosition());

    }

    /**
     * Test widget style properties are the same as system style by default.
     */
    @Test
    public void testDefaultStyle() {
        final WGraphicText widget = new WGraphicText();
        Assert.assertNull(widget.getStyle());
        Assert.assertNull(widget.getInlineStyle());
        final StyleDocument stylesheet = widget.getEffectiveStyleSheet();
        Assert.assertTrue(SystemStyle.INSTANCE.equals(stylesheet));
    }

    /**
     * Test widget inline style.
     */
    @Test
    public void testInlineStyle() {
        final WGraphicText widget = new WGraphicText();

        widget.setInlineStyle(new Chars("testkey: 'testvalue'"));
        Assert.assertNotNull(widget.getInlineStyle());
        final StyleDocument stylesheet = widget.getEffectiveStyleSheet();
        Assert.assertFalse(SystemStyle.INSTANCE.equals(stylesheet));

        final Document style = widget.getEffectiveStyleDoc();
        Assert.assertEquals(new Constant(new Chars("testvalue")), style.getPropertyValue(new Chars("testkey")));

    }

    @Test
    public void testListStyleAffectingProperties() {

        final Chars text = new Chars(
            "{\n" +
            "x : $Var1\n" +
            "RuleFalse : {\n" +
            "  filter : $FilterProp1 = 'value1'\n" +
            "  y : $Var2\n" +
            "}\n" +
            "RuleTrue : {\n" +
            "  filter : $StyleClass = 'test'\n" +
            "  z : $Var3\n" +
            "}\n" +
            "}"
        );

        final StyleDocument style = RSReader.readStyleDoc(text);

        final WSpace widget = new WSpace();
        widget.setPropertyValue(new Chars("StyleClass"), new Chars("test"));

        final Set styleChangingProperties = new HashSet();
        final Set filterChangingProperties = new HashSet();
        WidgetStyles.listProperties(style, widget, styleChangingProperties, filterChangingProperties);

        Assert.assertEquals(2, styleChangingProperties.getSize());
        Assert.assertTrue(styleChangingProperties.contains(new Chars("Var1")));
        Assert.assertTrue(styleChangingProperties.contains(new Chars("Var3")));

        Assert.assertEquals(2, filterChangingProperties.getSize());
        Assert.assertTrue(filterChangingProperties.contains(new Chars("FilterProp1")));
        Assert.assertTrue(filterChangingProperties.contains(new Chars("StyleClass")));

    }
}
