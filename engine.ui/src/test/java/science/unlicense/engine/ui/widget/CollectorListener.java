
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;

/**
 *
 * @author Johann Sorel
 */
public final class CollectorListener implements EventListener {

    public final Sequence messages = new ArraySequence();

    @Override
    public void receiveEvent(Event event) {
        messages.add(event.getMessage());
    }

}
