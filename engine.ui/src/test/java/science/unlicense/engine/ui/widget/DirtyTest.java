
package science.unlicense.engine.ui.widget;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.engine.ui.visual.ViewRenderLoop;
import science.unlicense.engine.ui.visual.VirtualFrame;
import science.unlicense.engine.ui.visual.VirtualPainter2D;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.Vector2f64;

/**
 * TODO : there is an extra event when dealing with containers.
 *        Find a way to avoid this additional event.
 *
 * @author Johann Sorel
 */
public class DirtyTest {

    /**
     * Resize a widget must call a dirty event
     */
    @Test
    public void testResize(){

        final Listener listener = new Listener();

        final WLabel label = new WLabel();
        label.setFrame(new VirtualFrame());
        label.addEventListener(PropertyMessage.PREDICATE, listener);

        label.setEffectiveExtent(new Extent.Double(999, 888));
        Assert.assertEquals(2, listener.nb);
        Assert.assertEquals(new BBox(new double[]{-499.5,-444},new double[]{499.5,444}),listener.dirty);

        //dirty area should be the size of the biggest between new and old extent
        listener.reset();
        label.setEffectiveExtent(new Extent.Double(100, 100));
        Assert.assertEquals(2, listener.nb);
        Assert.assertEquals(new BBox(new double[]{-499.5,-444},new double[]{499.5,444}),listener.dirty);

    }

    /**
     * Check dirty event are correctly send down the hierarchy.
     */
    @Test
    public void testDirtyArea(){

        final VirtualPainter2D painter = new VirtualPainter2D();

        final WSpace space1 = new WSpace(new Extent.Double(1, 1));
        final WSpace space2 = new WSpace(new Extent.Double(1, 1));
        final WSpace space3 = new WSpace(new Extent.Double(1, 1));
        final WSpace space4 = new WSpace(new Extent.Double(1, 1));
        final WSpace space5 = new WSpace(new Extent.Double(1, 1));
        final WSpace space6 = new WSpace(new Extent.Double(1, 1));

        final Listener listener = new Listener();

        final WContainer container = new WContainer(new GridLayout(3, 3));
        container.setFrame(new VirtualFrame());
        container.addEventListener(PropertyMessage.PREDICATE, listener);
        container.setEffectiveExtent(new Extent.Double(60, 60));
        Assert.assertEquals(2, listener.nb);
        container.getChildren().add(space1);
        Assert.assertEquals(3, listener.nb);
        container.getChildren().add(space2);
        container.getChildren().add(space3);
        container.getChildren().add(space4);
        container.getChildren().add(space5);
        container.getChildren().add(space6);
        Assert.assertEquals(8, listener.nb);

        Assert.assertEquals(new BBox(new double[]{-30,-30},new double[]{30,30}),listener.dirty);
        listener.reset();
        ViewRenderLoop.render(container, painter, new BBox(new double[]{0,0}, new double[]{60,60}));
        Assert.assertTrue(!listener.dirty.isValid());

        space5.setDirty();
        Assert.assertEquals(new BBox(new double[]{-10,-10},new double[]{10,10}),listener.dirty);

    }

    /**
     * Check dirty event are correctly send down the hierarchy.
     * Same as previous but including a container margin.
     */
    @Test
    public void testDirtyAreaWithMargin(){

        final VirtualPainter2D painter = new VirtualPainter2D();

        final WSpace space1 = new WSpace(new Extent.Double(1, 1));
        final WSpace space2 = new WSpace(new Extent.Double(1, 1));
        final WSpace space3 = new WSpace(new Extent.Double(1, 1));
        final WSpace space4 = new WSpace(new Extent.Double(1, 1));
        final WSpace space5 = new WSpace(new Extent.Double(1, 1));
        final WSpace space6 = new WSpace(new Extent.Double(1, 1));

        final Listener listener = new Listener();

        final WContainer root = new WContainer(new BorderLayout());
        root.setFrame(new VirtualFrame());
        root.setInlineStyle(new Chars("margin:[10,10,10,10]"));
        root.addEventListener(PropertyMessage.PREDICATE, listener);
        root.setEffectiveExtent(new Extent.Double(100, 100));

        final WContainer container = new WContainer(new GridLayout(3, 3));
        root.addChild(container, BorderConstraint.CENTER);
        container.setInlineStyle(new Chars("margin:[10,10,10,10]"));
        container.getChildren().add(space1);
        container.getChildren().add(space2);
        container.getChildren().add(space3);
        container.getChildren().add(space4);
        container.getChildren().add(space5);
        container.getChildren().add(space6);

        Assert.assertEquals(new Extent.Double(80,80),container.getEffectiveExtent());
        Assert.assertEquals(new Extent.Double(20,20),space1.getEffectiveExtent());

        Assert.assertEquals(new BBox(new double[]{-60,-60},new double[]{60,60}),listener.dirty);
        listener.reset();
        ViewRenderLoop.render(container, painter, new BBox(new double[]{0,0}, new double[]{80,80}));
        Assert.assertTrue(!listener.dirty.isValid());

        space5.setDirty();
        Assert.assertEquals(new BBox(new double[]{-10,-10},new double[]{10,10}),listener.dirty);

    }

    /**
     * Test style changes makes the widget dirty.
     */
    @Test
    public void testStyleChangeEvent(){

        final VirtualPainter2D painter = new VirtualPainter2D();
        final Listener listener = new Listener();

        final WSpace space = new WSpace(new Extent.Double(20, 20));
        space.setFrame(new VirtualFrame());
        space.setEffectiveExtent(new Extent.Double(20, 20));
        space.addEventListener(PropertyMessage.PREDICATE, listener);
        ViewRenderLoop.render(space, painter, new BBox(new double[]{0,0}, new double[]{20,20}));
        listener.reset();

        //change self style
        space.setInlineStyle(new Chars("background:none"));
        Assert.assertEquals(new BBox(new double[]{-10,-10},new double[]{10,10}),listener.dirty);
        listener.reset();

        //make the same change, there must be no event
        space.setInlineStyle(new Chars("background:none"));
        Assert.assertFalse(listener.dirty.isValid());
        listener.reset();

        //check mouse events, they should not produce any effect
        final MouseMessage event = new MouseMessage(MouseMessage.TYPE_ENTER, 0, -1, new Vector2f64(0,0), new Vector2f64(0,0), 0, false);
        space.receiveEvent(new Event(null, event));
        Assert.assertFalse(listener.dirty.isValid());
        listener.reset();

    }


    private static final class Listener implements EventListener{

        private final BBox dirty = new BBox(2);
        private int nb = 0;

        private void reset(){
            dirty.setToNaN();
            nb=0;
        }

        @Override
        public void receiveEvent(Event event) {
            final PropertyMessage pe = (PropertyMessage) event.getMessage();
            if (pe.getPropertyName().equals(Widget.PROPERTY_DIRTY)){
                nb++;
                if (dirty.isValid()){
                    dirty.expand((BBox) pe.getNewValue());
                } else {
                    dirty.set((BBox) pe.getNewValue());
                }

            }
        }

    }

}
