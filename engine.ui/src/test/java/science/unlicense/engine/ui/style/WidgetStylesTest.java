
package science.unlicense.engine.ui.style;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.predicate.Constant;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.color.Color;

/**
 *
 * @author Johann Sorel
 */
public class WidgetStylesTest {

    private static final double DELTA = 0.0000001;

    private static final Chars PROP_TEST = new Chars("test");

    @Test
    public void testReadShapeStyle(){
        final Document doc = new DefaultDocument();
        doc.getProperty(StyleDocument.STYLE_PROP_RADIUS).setValue(new Constant(3));
        doc.getProperty(StyleDocument.STYLE_PROP_BRUSH).setValue(new Constant(new PlainBrush(3, PlainBrush.LINECAP_ROUND)));
        doc.getProperty(StyleDocument.STYLE_PROP_BRUSH_PAINT).setValue(new Constant(new ColorPaint(Color.GREEN)));
        doc.getProperty(StyleDocument.STYLE_PROP_FILL_PAINT).setValue(new Constant(new ColorPaint(Color.BLUE)));
        doc.getProperty(StyleDocument.STYLE_PROP_MARGIN).setValue(new Constant(6));

        final WSpace lbl = new WSpace(new Extent.Double(2));
        lbl.setInlineStyle(new StyleDocument());
        lbl.getInlineStyle().getProperty(PROP_TEST).setValue(doc);

        final Document sdoc = lbl.getEffectiveStyleDoc();
        final GraphicStyle[] shapeStyles = WidgetStyles.readShapeStyle(sdoc, PROP_TEST, lbl);
        Assert.assertEquals(1,shapeStyles.length);
        final GraphicStyle ss = shapeStyles[0];
        Assert.assertEquals(new PlainBrush(3, PlainBrush.LINECAP_ROUND),ss.getBrush());
        Assert.assertEquals(new ColorPaint(Color.GREEN),ss.getBrushPaint());
        Assert.assertEquals(new ColorPaint(Color.BLUE),ss.getFillPaint());
        Assert.assertEquals(new Margin(6, 6, 6, 6),ss.getMargin());
    }

    @Test
    public void testStyleFilter(){
        final StyleDocument doc = new StyleDocument();
        doc.setProperties(new Chars("filter:$Enable"));

        final WLabel lbl = new WLabel();
        Assert.assertEquals(Boolean.TRUE,doc.getFilter().evaluate(lbl));

        lbl.setEnable(false);
        Assert.assertEquals(Boolean.FALSE,doc.getFilter().evaluate(lbl));
    }

    @Test
    public void testMergeSimpleNoCreate(){
        final Chars prop1 = new Chars("prop1");
        final Chars prop2 = new Chars("prop2");
        final Chars prop3 = new Chars("prop3");

        final Document base = new DefaultDocument();
        final Document diff = new DefaultDocument();
        base.getProperty(prop1).setValue(1);
        base.getProperty(prop3).setValue(5);
        diff.getProperty(prop2).setValue(3);
        diff.getProperty(prop3).setValue(10);

        final Document res = WidgetStyles.mergeDoc(base, diff, false);
        Assert.assertTrue(res == base);
        Assert.assertEquals((Integer) 1,res.getProperty(prop1).getValue());
        Assert.assertEquals((Integer) 3,res.getProperty(prop2).getValue());
        Assert.assertEquals((Integer) 10,res.getProperty(prop3).getValue());
    }

    @Test
    public void testMergeSimpleCreate(){
        final Chars prop1 = new Chars("prop1");
        final Chars prop2 = new Chars("prop2");
        final Chars prop3 = new Chars("prop3");

        final Document base = new DefaultDocument();
        final Document diff = new DefaultDocument();
        base.getProperty(prop1).setValue(1);
        base.getProperty(prop3).setValue(5);
        diff.getProperty(prop2).setValue(3);
        diff.getProperty(prop3).setValue(10);

        final Document res = WidgetStyles.mergeDoc(base, diff, true);
        Assert.assertTrue(res != base);
        Assert.assertEquals((Integer) 1,res.getProperty(prop1).getValue());
        Assert.assertEquals((Integer) 3,res.getProperty(prop2).getValue());
        Assert.assertEquals((Integer) 10,res.getProperty(prop3).getValue());
    }

    @Test
    public void testMergeCreate(){
        final Chars prop = new Chars("prop");
        final Chars prop1 = new Chars("prop1");
        final Chars prop2 = new Chars("prop2");
        final Chars prop3 = new Chars("prop3");

        final Document base = new DefaultDocument();
        final Document baseSub = new DefaultDocument();
        final Document diff = new DefaultDocument();
        final Document diffSub = new DefaultDocument();
        baseSub.getProperty(prop1).setValue(1);
        baseSub.getProperty(prop3).setValue(5);
        diffSub.getProperty(prop2).setValue(3);
        diffSub.getProperty(prop3).setValue(10);

        base.getProperty(prop).setValue(baseSub);
        diff.getProperty(prop).setValue(diffSub);

        final Document res = WidgetStyles.mergeDoc(base, diff, true);
        Assert.assertTrue(res != base);
        final Document resSub = (Document) res.getProperty(prop).getValue();

        Assert.assertEquals((Integer) 1,resSub.getProperty(prop1).getValue());
        Assert.assertEquals((Integer) 3,resSub.getProperty(prop2).getValue());
        Assert.assertEquals((Integer) 10,resSub.getProperty(prop3).getValue());
    }

    @Test
    public void testMargin(){
        final WSpace lbl = new WSpace();
        lbl.setInlineStyle(new Chars("margin:6"));

        Extents extents = lbl.getExtents(null, null);
        Assert.assertEquals(12,extents.minX,DELTA);
        Assert.assertEquals(12,extents.minY,DELTA);
        Assert.assertEquals(12,extents.bestX,DELTA);
        Assert.assertEquals(12,extents.bestY,DELTA);
        Assert.assertEquals(Double.POSITIVE_INFINITY,extents.maxX,DELTA);
        Assert.assertEquals(Double.POSITIVE_INFINITY,extents.maxY,DELTA);
    }

}
