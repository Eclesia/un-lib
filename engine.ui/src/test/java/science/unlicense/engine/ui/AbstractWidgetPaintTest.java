

package science.unlicense.engine.ui;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.painter2d.ImagePainter2D;
import science.unlicense.engine.ui.visual.ViewRenderLoop;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.geometry.api.tuple.TupleGrid;

/**
 * Test widget painting.
 *
 * @author Johann Sorel
 */
public abstract class AbstractWidgetPaintTest {

    protected abstract ImagePainter2D createPainter(int width,int height);

    @Ignore
    @Test
    public void testSanity(){

        final ImagePainter2D painter = createPainter(20, 20);

        //build a scene devided in 4 squares
        final WContainer container = new WContainer();
        container.setLayout(new GridLayout(2, 2));
        final WContainer upperLeft = new WContainer();
        final WContainer upperRight = new WContainer();
        final WContainer bottomLeft = new WContainer();
        container.getChildren().add(upperLeft);
        container.getChildren().add(upperRight);
        container.getChildren().add(bottomLeft);

//        upperLeft.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.RED)));
//        upperRight.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.GREEN)));
//        bottomLeft.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.BLUE)));
//        container.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.BLACK)));

        //render the widgets scene
//        container.setEffectiveExtent(new Extent.Double(20, 20));
//        ViewRenderLoop.render(container, painter, new BBox(new double[]{0,0}, new double[]{20,20}));
//        painter.flush();
//
//        //check the image content
//        final Image image = painter.getImage();
//        final TupleBuffer sm = image.getColorModel().asTupleBuffer(image);
//
//        final Color c1 = sm.getColor(new int[]{ 0, 0});
//        final Color c2 = sm.getColor(new int[]{10, 0});
//        final Color c3 = sm.getColor(new int[]{ 0,10});
//        final Color c4 = sm.getColor(new int[]{10,10});
//
//        Assert.assertEquals(c1, Color.RED);
//        Assert.assertEquals(c2, Color.GREEN);
//        Assert.assertEquals(c3, Color.BLUE);
//        Assert.assertEquals(c4, Color.BLACK);

    }

    @Ignore
    @Test
    public void testImageGraphicStyle(){

        final Image img = Images.create(new Extent.Long(1, 1), Images.IMAGE_TYPE_RGB);


        final ImagePainter2D painter = createPainter(10, 10);

        //build a scene devided in 4 squares
        final WSpace container = new WSpace(10, 10);
        container.setEffectiveExtent(new Extent.Long(10, 10));

//        upperLeft.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.RED)));
//        upperRight.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.GREEN)));
//        bottomLeft.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.BLUE)));
//        container.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.BLACK)));

        //render the widgets scene
//        ViewRenderLoop.render(container, painter, new BBox(new double[]{0,0}, new double[]{20,20}));
//        painter.flush();
//
//        //check the image content
//        final Image image = painter.getImage();
//        final TupleBuffer sm = image.getColorModel().asTupleBuffer(image);
//
//        final Color c1 = sm.getColor(new int[]{ 0, 0});
//        final Color c2 = sm.getColor(new int[]{10, 0});
//        final Color c3 = sm.getColor(new int[]{ 0,10});
//        final Color c4 = sm.getColor(new int[]{10,10});
//
//        Assert.assertEquals(c1, Color.RED);
//        Assert.assertEquals(c2, Color.RED);
//        Assert.assertEquals(c3, Color.RED);
//        Assert.assertEquals(c4, Color.RED);

    }

}
