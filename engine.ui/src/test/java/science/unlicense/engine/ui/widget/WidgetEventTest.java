
package science.unlicense.engine.ui.widget;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.visual.VirtualFrame;

/**
 *
 * @author Johann Sorel
 */
public class WidgetEventTest {

    @Test
    public void testFrameSet() {

        final VirtualFrame frame1 = new VirtualFrame();
        frame1.setSize(100, 100);
        final VirtualFrame frame2 = new VirtualFrame();
        frame2.setSize(100, 100);

        final int[] node1Evt = new int[1];
        final int[] node1VG = new int[1];
        final int[] node2Evt = new int[1];
        final int[] node2VG = new int[1];
        final int[] node3Evt = new int[1];
        final int[] node3VG = new int[1];
        final UIFrame[] expectedFrame = new UIFrame[]{frame1};


        //test setting frame on root node //////////////////////////////////////
        final WContainer node1 = new WContainer(new BorderLayout()) {
            @Override
            protected void valueChanged(Chars name, Object oldValue, Object value) {
                super.valueChanged(name, oldValue, value);
                if (Widget.PROPERTY_FRAME.equals(name)) {
                    node1VG[0]++;
                    Assert.assertEquals(expectedFrame[0], value);
                    Assert.assertEquals(expectedFrame[0], getFrame());
                    Assert.assertEquals(expectedFrame[0], getPropertyValue(Widget.PROPERTY_FRAME));
                }
            }
        };
        node1.varFrame().addListener(new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                node1Evt[0]++;
                Assert.assertEquals(expectedFrame[0], node1.getFrame());
                Assert.assertEquals(expectedFrame[0], node1.getPropertyValue(Widget.PROPERTY_FRAME));
            }
        });
        node1.setFrame(frame1);
        Assert.assertEquals(1, node1Evt[0]);
        Assert.assertEquals(1, node1VG[0]);

        //test added child inherid frame ///////////////////////////////////////
        final WContainer node2 = new WContainer(new BorderLayout()) {
            @Override
            protected void valueChanged(Chars name, Object oldValue, Object value) {
                super.valueChanged(name, oldValue, value);
                if (Widget.PROPERTY_FRAME.equals(name)) {
                    node2VG[0]++;
                    Assert.assertEquals(expectedFrame[0], value);
                    Assert.assertEquals(expectedFrame[0], getFrame());
                    Assert.assertEquals(expectedFrame[0], getPropertyValue(Widget.PROPERTY_FRAME));
                }
            }
        };
        node2.varFrame().addListener(new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                node2Evt[0]++;
                Assert.assertEquals(expectedFrame[0], node2.getFrame());
                Assert.assertEquals(expectedFrame[0], node2.getPropertyValue(Widget.PROPERTY_FRAME));
            }
        });
        node1.addChild(node2, BorderConstraint.CENTER);
        Assert.assertEquals(1, node1Evt[0]);
        Assert.assertEquals(1, node1VG[0]);
        Assert.assertEquals(1, node2Evt[0]);
        Assert.assertEquals(1, node2VG[0]);

        //test added sub-child inherid frame ///////////////////////////////////
        final WContainer node3 = new WContainer(new BorderLayout()) {
            @Override
            protected void valueChanged(Chars name, Object oldValue, Object value) {
                super.valueChanged(name, oldValue, value);
                if (Widget.PROPERTY_FRAME.equals(name)) {
                    node3VG[0]++;
                    Assert.assertEquals(expectedFrame[0], value);
                    Assert.assertEquals(expectedFrame[0], getFrame());
                    Assert.assertEquals(expectedFrame[0], getPropertyValue(Widget.PROPERTY_FRAME));
                }
            }
        };
        node3.varFrame().addListener(new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                node3Evt[0]++;
                Assert.assertEquals(expectedFrame[0], node3.getFrame());
                Assert.assertEquals(expectedFrame[0], node3.getPropertyValue(Widget.PROPERTY_FRAME));
            }
        });
        node2.addChild(node3, BorderConstraint.CENTER);
        Assert.assertEquals(1, node1Evt[0]);
        Assert.assertEquals(1, node1VG[0]);
        Assert.assertEquals(1, node2Evt[0]);
        Assert.assertEquals(1, node2VG[0]);
        Assert.assertEquals(1, node3Evt[0]);
        Assert.assertEquals(1, node3VG[0]);

        //change root frame, ensure all nodes are updated //////////////////////
        expectedFrame[0] = frame2;
        node1.setFrame(frame2);
        Assert.assertEquals(2, node1Evt[0]);
        Assert.assertEquals(2, node1VG[0]);
        Assert.assertEquals(2, node2Evt[0]);
        Assert.assertEquals(2, node2VG[0]);
        Assert.assertEquals(2, node3Evt[0]);
        Assert.assertEquals(2, node3VG[0]);


    }

}
