
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.engine.ui.style.StyleDocument;

/**
 * Container, displaying tabs for each child component.
 *
 * @author Johann Sorel
 */
public class WTabContainer extends WContainer {

    public static final int TAB_POSITION_TOP = 0;
    public static final int TAB_POSITION_BOTTOM = 1;
    public static final int TAB_POSITION_LEFT = 2;
    public static final int TAB_POSITION_RIGHT = 3;

    /**
     * Property for the active ab index.
     */
    public static final Chars PROPERTY_ACTIVE_TAB = Chars.constant("tabActive");
    /**
     * Property for the tabs position.
     */
    public static final Chars PROPERTY_TAB_POSITION = Chars.constant("TabPosition");
    /**
     * Property for the active tab state.
     */
    public static final Chars PROPERTY_ACTIVE = Chars.constant("Active");


    /** tab border : BorderStyle class */
    public static final Chars STYLE_PROP_TAB_PREFIX = Chars.constant("tab-");
    /** tab inner margin */
    public static final Chars STYLE_PROP_TAB_MARGIN = Chars.constant("tab-inner-margin");

    protected static final Dictionary CLASS_DEFAULTS;
    protected static final Dictionary CLASS_TAB_DEFAULTS;
    protected static final Dictionary CLASS_BAR_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WButton.CLASS_DEFAULTS);
        defs.add(PROPERTY_TAB_POSITION, 0);
        defs.add(PROPERTY_ACTIVE, Boolean.FALSE);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("tabcontainer")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);

        final Dictionary def2s = new HashDictionary();
        def2s.addAll(WContainer.CLASS_DEFAULTS);
        def2s.add(PROPERTY_ACTIVE, Boolean.FALSE);
        def2s.add(PROPERTY_TAB_POSITION, 0);
        def2s.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("tab")));
        CLASS_TAB_DEFAULTS = Collections.readOnlyDictionary(def2s);

        final Dictionary def3s = new HashDictionary();
        def3s.addAll(WContainer.CLASS_DEFAULTS);
        def3s.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("tabbar")));
        CLASS_BAR_DEFAULTS = Collections.readOnlyDictionary(def3s);
    }

    private int tabActive = -1;
    private int tabPosition = TAB_POSITION_TOP;
    final Sequence tabs = new ArraySequence();
    private final WTabBar tabBar = new WTabBar();
    private final WContainer center = new WContainer(new BorderLayout());

    public WTabContainer() {
        super(new BorderLayout());
        addChild(tabBar,BorderConstraint.TOP);
        addChild(center,BorderConstraint.CENTER);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public WTab addTab(Widget child, Widget tabHeader){
        return addTab(child, tabHeader, getChildren().getSize());
    }

    public WTab addTab(Widget child, Widget tabHeader, int index){
        final WTab tab = new WTab(tabHeader, child);
        tabs.add(tab);
        tabBar.getChildren().removeAll();

        for (int i=0,n=tabs.getSize();i<n;i++){
            final WTab tabi = (WTab) tabs.get(i);
            tabi.setTabPosition(tabPosition);
            tabBar.getChildren().add(tabi);
        }
        tabBar.setTabPosition(tabPosition);

        //first tab added, activate it
        if (tabs.getSize()==1){
            setActiveTab(0);
        }
        return tab;
    }

    public void removeTab(Widget content){
        //TODO
    }

    public void removeTabs(){
        tabBar.getChildren().removeAll();
        tabs.removeAll();
        setActiveTab(-1);

    }

    /**
     * Set active tab.
     * @param visibletab set visible tab
     */
    public void setActiveTab(int visibletab) {
        if (this.tabActive == visibletab) return;
        if (visibletab>=tabs.getSize()){
            throw new InvalidArgumentException("Invalid tab index "+visibletab+" only have "+tabs.getSize()+" tabs");
        }

        for (int i=0,n=tabs.getSize();i<n;i++){
            final WTab tab = (WTab) tabs.get(i);
            tab.setActive(visibletab==i);
        }

        this.tabActive = visibletab;

        if (visibletab>=0){
            ((WTab) tabs.get(visibletab)).tabContent.setLayoutConstraint(BorderConstraint.CENTER);
            center.getChildren().replaceAll(new Node[]{((WTab) tabs.get(visibletab)).tabContent});
        } else {
            center.getChildren().removeAll();
        }
    }

    /**
     * Get active tab index.
     * @return int, can be negative if no tab active
     */
    public int getActiveTab(){
        return tabActive;
    }

    /**
     * Set tab position.
     * @param tabposition set tab position, one of TAB_POSITION_X
     */
    public void setTabPosition(int tabposition) {
        if (this.tabPosition != tabposition){
            final int oldValue = this.tabPosition;
            this.tabPosition = tabposition;
            sendPropertyEvent(PROPERTY_TAB_POSITION, oldValue, tabposition);
        }

        children.remove(tabBar);
        final LineLayout tabLayout = (LineLayout) tabBar.getLayout();


        if (tabposition==TAB_POSITION_BOTTOM){
            tabLayout.setDirection(LineLayout.DIRECTION_HORIZONTAL);
            tabLayout.setVerticalAlignement(LineLayout.VALIGN_TOP);
            addChild(tabBar, BorderConstraint.BOTTOM);
        } else if (tabposition==TAB_POSITION_TOP){
            tabLayout.setDirection(LineLayout.DIRECTION_HORIZONTAL);
            tabLayout.setVerticalAlignement(LineLayout.VALIGN_BOTTOM);
            addChild(tabBar, BorderConstraint.TOP);
        } else if (tabposition==TAB_POSITION_LEFT){
            tabLayout.setDirection(LineLayout.DIRECTION_VERTICAL);
            tabLayout.setHorizontalAlignement(LineLayout.HALIGN_RIGHT);
            addChild(tabBar, BorderConstraint.LEFT);
        } else if (tabposition==TAB_POSITION_RIGHT){
            tabLayout.setDirection(LineLayout.DIRECTION_VERTICAL);
            tabLayout.setHorizontalAlignement(LineLayout.HALIGN_LEFT);
            addChild(tabBar, BorderConstraint.RIGHT);
        }
        tabBar.setTabPosition(tabPosition);

        tabBar.fireTabEvent();
    }

    /**
     * Get tab position.
     * @return int, one of TAB_POSITION_X
     */
    public int getTabPosition(){
        return tabPosition;
    }

    public final class WTabBar extends WContainer {
        private WTabBar(){
            super(new LineLayout());
            final LineLayout fl = (LineLayout) getLayout();
            fl.setDistance(0);
            fl.setVerticalAlignement(LineLayout.VALIGN_BOTTOM);
            fl.setFitToCell(true);
        }

        @Override
        protected Dictionary getClassDefaultProperties() {
            return CLASS_BAR_DEFAULTS;
        }

        private void fireTabEvent(){
            WTabBar.this.sendPropertyEvent(PROPERTY_TAB_POSITION, 0, tabPosition);
        }

        public int getTabPosition() {
            return (Integer) getPropertyValue(PROPERTY_TAB_POSITION);
        }

        void setTabPosition(int position) {
            setPropertyValue(PROPERTY_TAB_POSITION, position);
        }
    }

    public final class WTab extends WContainer {

        private final Widget tabHeader;
        final Widget tabContent;

        public WTab(Widget tabHeader, Widget tabContent) {
            super.setLayout(new BorderLayout());
            this.tabHeader = tabHeader;
            this.tabContent = tabContent;
            addChild(tabHeader, BorderConstraint.CENTER);
        }

        @Override
        protected Dictionary getClassDefaultProperties() {
            return CLASS_TAB_DEFAULTS;
        }

        public Widget getTabHeader() {
            return tabHeader;
        }

        public Widget getTabContent() {
            return tabContent;
        }

        public int getTabPosition() {
            return (Integer) getPropertyValue(PROPERTY_TAB_POSITION);
        }

        void setTabPosition(int position) {
            setPropertyValue(PROPERTY_TAB_POSITION, position);
        }

        public boolean isActive() {
            return (Boolean) getPropertyValue(PROPERTY_ACTIVE);
        }

        public void setActive(boolean active) {
            setPropertyValue(PROPERTY_ACTIVE, active);
        }

        protected void receiveEventParent(Event event) {
            //catch event, pass to children
            super.receiveEventParent(event);
            //if it is not consumed and is a mouse left click we activate this tab
            if (event.getMessage() instanceof MouseMessage && !event.getMessage().isConsumed()) {
                final MouseMessage me = (MouseMessage) event.getMessage();
                if (me.getButton() == MouseMessage.BUTTON_1) {
                    me.consume();
                    setActiveTab(tabs.search(WTab.this));
                }
            }
        }

    }

}
