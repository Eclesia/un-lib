
package science.unlicense.engine.ui.widget.frame;

import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.display.api.layout.Layout;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Widget container which catch mouse event to drag the current frame.
 *
 * @author Johann Sorel
 */
public class WFrameDrag extends WContainer {

    private final VectorRW realPosition = VectorNf64.createDouble(2);
    private final VectorRW borderDist = VectorNf64.createDouble(2);
    private boolean dragging = false;

    public WFrameDrag() {}

    public WFrameDrag(Layout layout) {
        super(layout);
    }

    protected void receiveEventParent(Event event) {
        super.receiveEventParent(event);

        final EventMessage message = event.getMessage();
        if (message.isConsumed() || !(message instanceof MouseMessage) ) return;

        final MouseMessage e = (MouseMessage) message;
        final int type = e.getType();

        final int mousebutton = e.getButton();
        if (MouseMessage.TYPE_PRESS==type){
            message.consume();
            dragging = true;
            borderDist.set(e.getMouseScreenPosition());
            borderDist.localSubtract(getFrame().getOnScreenLocation());
            requestFocus();
        } else if (MouseMessage.TYPE_RELEASE==type || MouseMessage.TYPE_EXIT==type){
            dragging = false;

        } else if (MouseMessage.TYPE_MOVE==type && dragging && mousebutton==1){
            message.consume();
            realPosition.set(e.getMouseScreenPosition());
            getFrame().setOnScreenLocation(realPosition.subtract(borderDist));
        }
    }

}
