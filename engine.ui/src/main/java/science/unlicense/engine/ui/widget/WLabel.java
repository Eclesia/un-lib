package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.engine.ui.style.StyleDocument;

/**
 * Widget displaying a text message and optional image.
 *
 * @author Johann Sorel
 */
public class WLabel extends AbstractLabeled{

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(AbstractLabeled.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("label")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WLabel() {
        this(null,null);
    }

    public WLabel(CharArray text) {
        this(text,null);
    }

    public WLabel(CharArray text, Widget graphic) {
        super(text,graphic);
        setHorizontalAlignment(HALIGN_LEFT);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

}
