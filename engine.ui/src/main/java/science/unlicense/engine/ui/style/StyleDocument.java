
package science.unlicense.engine.ui.style;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.io.RSReader;

/**
 * A style rule define a filter, a set of properties and sub-rules.
 *
 * @author Johann Sorel
 */
public class StyleDocument extends DefaultDocument {

    // style properties shared by most widgets.
    /** Text font : Font class */
    public static final Chars STYLE_PROP_FONT = Chars.constant("font");
    public static final Chars STYLE_PROP_GRAPHIC = Chars.constant("graphic");
    public static final Chars STYLE_PROP_BACKGROUND = Chars.constant("background");
    public static final Chars PROPERTY_TEXT_SAMPLE = Chars.constant("text-sample");
    /**
     * Property used bye style filters.
     */
    public static final Chars PROPERTY_STYLE_CLASS = Chars.constant("StyleClass");

    /** radius : double class */
    public static final Chars STYLE_PROP_FAMILY = Chars.constant("family");
    /** radius : double class */
    public static final Chars STYLE_PROP_RADIUS = Chars.constant("radius");
    /** brush : Brush class */
    public static final Chars STYLE_PROP_BRUSH = Chars.constant("brush");
    /** brush paint : Paint class */
    public static final Chars STYLE_PROP_BRUSH_PAINT = Chars.constant("brush-paint");
    /** fill paint : Paint class */
    public static final Chars STYLE_PROP_FILL_PAINT = Chars.constant("fill-paint");
    /** visual margin : double class, this margin is used at rendering time for clipping */
    public static final Chars STYLE_PROP_VISUAL_MARGIN = Chars.constant("visual-margin");
    /** margin : double class, this margin is used by layouts */
    public static final Chars STYLE_PROP_MARGIN = Chars.constant("margin");
    /** shape : String class */
    public static final Chars STYLE_PROP_SHAPE = Chars.constant("shape");
    /** zorder : double class */
    public static final Chars STYLE_PROP_ZORDER = Chars.constant("zorder");
    /** transform : transform to apply on shape */
    public static final Chars STYLE_PROP_TRANSFORM = Chars.constant("transform");
    /** effect : special effects on widget, blur, morphologic,... could be anything visual */
    public static final Chars STYLE_PROP_EFFECT = Chars.constant("effect");

    /** Flag object used to set to null a property value */
    public static final Object NONE = new Object();
    public static final Chars PROP_FILTER = Chars.constant("filter");
    public static final Chars PROP_ISTRIGGER = Chars.constant("_isTrigger");
    public static final Chars INLINE_RULE = Chars.constant("INLINE");

    private Chars name = Chars.EMPTY;

    public StyleDocument() {
        super(true);
    }

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        CObjects.ensureNotNull(name);
        this.name = name;
    }

    public Expression getFilter() {
        Object value = getPropertyValue(PROP_FILTER);
        return (Expression) ((value instanceof Expression) ? value : Predicate.TRUE);
    }

    public void setFilter(Expression filter) {
        setProperty(PROP_FILTER, filter);
    }

    public boolean isTrigger() {
        return getPropertyValue(PROP_ISTRIGGER) != null;
    }

    public boolean isRule() {
        return getPropertyValue(PROP_FILTER) != null;
    }

    public Sequence getRules() {
        Sequence seq = null;

        final Iterator ite = getPropertyNames().createIterator();
        while (ite.hasNext()) {
            Object val = getPropertyValue((Chars) ite.next());
            if (val instanceof StyleDocument && ((StyleDocument) val).isRule() ){
                if (seq == null) seq = new ArraySequence();
                seq.add(val);
            }
        }
        return seq == null ? Collections.emptySequence() : Collections.readOnlySequence(seq);
    }

    public Sequence getTriggers() {
        Sequence seq = null;
        final Iterator ite = properties.getValues().createIterator();
        while (ite.hasNext()) {
            Object val = ite.next();
            if (val instanceof StyleDocument && ((StyleDocument) val).isTrigger()) {
                if (seq == null) seq = new ArraySequence();
                seq.add(val);
            }
        }
        return (seq == null) ? Collections.emptySequence() : seq;
    }

    public void setProperty(Chars key, Chars value) {
        try {
            setProperty(key, RSReader.readExpression(value,true));
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    public void setProperty(Chars key, Expression value) {
        setPropertyValue(key,value);
    }

    public void setProperties(CharArray chars) throws InvalidArgumentException {
        final StyleDocument style = RSReader.readStyleDoc(new Chars("{\n").concat(chars).concat(new Chars("\n}")));
        WidgetStyles.mergeDoc(this, style, false);
    }

}
