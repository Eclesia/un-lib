
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.model.SpinnerEditor;
import science.unlicense.engine.ui.model.SpinnerModel;

/**
 *
 * @author Johann Sorel
 */
public class WSpinner extends AbstractControlWidget {

    public static final Chars PROPERTY_EDITOR = Chars.constant("Editor");
    public static final Chars PROPERTY_MODEL = WSlider.PROPERTY_MODEL;

    private final WAction up = new WAction(null, null, new EventListener() {
        public void receiveEvent(Event event) {
            Object value = getEditor().getValue();
            if (getModel().hasNextValue(value)){
                value = getModel().nextValue(value);
                setValue(value);
            }
        }
    });
    private final WAction down = new WAction(null, null, new EventListener() {
        public void receiveEvent(Event event) {
            Object value = getEditor().getValue();
            if (getModel().hasPreviousValue(value)){
                value = getModel().previousValue(value);
                setValue(value);
            }
        }
    });
    private final EventListener editorListener = new EventListener() {
        public void receiveEvent(Event event) {
            final PropertyMessage msg = (PropertyMessage) event.getMessage();
            final Chars propName = msg.getPropertyName();
            if (PROPERTY_VALUE.equals(propName)) {
                setValue(msg.getNewValue());
            } else if (PROPERTY_EDITION_VALID.equals(propName)) {
                setEditionValid((Boolean) msg.getNewValue());
            }
        }
    };

    public WSpinner(SpinnerModel model) {
        this(model,model.createEditor(),model.getDefaultValue());
    }

    public WSpinner(SpinnerModel model, Object value) {
        this(model,model.createEditor(),value);
    }

    public WSpinner(SpinnerModel model, SpinnerEditor editor, Object value) {
        setPropertyValue(PROPERTY_MODEL, model);
        setPropertyValue(PROPERTY_VALUE, value);
        final FormLayout layout = new FormLayout();
        layout.setColumnSize(0, FormLayout.SIZE_EXPAND);
        setLayout(layout);

        up.setPropertyValue(XPROP_STYLEMARKER, new Chars("WSpinner-UpButton"));
        down.setPropertyValue(XPROP_STYLEMARKER, new Chars("WSpinner-DownButton"));

        setEditor(editor);
    }

    public SpinnerModel getModel() {
        return (SpinnerModel) getPropertyValue(PROPERTY_MODEL);
    }

    public void setModel(SpinnerModel model) {
        setPropertyValue(PROPERTY_MODEL, model);
    }

    public SpinnerEditor getEditor() {
        return (SpinnerEditor) getPropertyValue(PROPERTY_EDITOR);
    }

    public void setEditor(SpinnerEditor editor) {
        final SpinnerEditor oldEditor = getEditor();
        if (CObjects.equals(oldEditor, editor)) return;

        if (oldEditor!=null){
            oldEditor.removeEventListener(PropertyMessage.PREDICATE, editorListener);
        }

        final Object value = getValue();
        getChildren().removeAll();
        editor.setValue(value);
        addChild(editor.getWidget(), FillConstraint.builder().coord(0, 0).fill(true, true).span(1, 2).build());
        addChild(up,                 FillConstraint.builder().coord(1, 0).fill(true, true).build());
        addChild(down,               FillConstraint.builder().coord(1, 1).fill(true, true).build());
        editor.setValue(value);
        editor.addEventListener(PropertyMessage.PREDICATE, editorListener);

        setPropertyValue(PROPERTY_EDITOR,editor);
    }

    @Override
    protected boolean prevalidateChange(Chars name, Object oldValue, Object value) {
        if (PROPERTY_VALUE.equals(name)) {
            if (!getModel().isValid(value)) {
                return false;
                //throw new InvalidArgumentException("Value "+value+" is not valid for model "+getModel());
            }
        }
        return super.prevalidateChange(name, oldValue, value);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_VALUE.equals(name)) {
            SpinnerEditor editor = getEditor();
            if (editor != null) editor.setValue(value);
        }

    }

}
