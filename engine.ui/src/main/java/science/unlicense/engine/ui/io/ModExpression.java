

package science.unlicense.engine.ui.io;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.common.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public class ModExpression extends CObject implements Expression {

    private final Expression exp1;
    private final Expression exp2;

    public ModExpression(Expression exp1, Expression exp2) {
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    public Expression getExp1() {
        return exp1;
    }

    public Expression getExp2() {
        return exp2;
    }

    public Chars getPropertyName() {
        return ((PropertyName) exp1).getName();
    }

    public Number evaluate(Object candidate) {
        if (candidate==null) return 0;

        final Object obj1 = exp1.evaluate(candidate);
        final Object obj2 = exp2.evaluate(candidate);

        if (obj1 instanceof Number && obj2 instanceof Number){
            return ((Number) obj1).doubleValue() % ((Number) obj2).doubleValue();
        }

        return 0;
    }

    public Chars toChars() {
        return Nodes.toChars(new Chars("Mod"), new Object[]{exp1,exp2});
    }

    public int getHash() {
        int hash = 7;
        hash = 29 * hash + (this.exp1 != null ? this.exp1.hashCode() : 0);
        hash = 29 * hash + (this.exp2 != null ? this.exp2.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ModExpression other = (ModExpression) obj;
        if (this.exp1 != other.exp1 && (this.exp1 == null || !this.exp1.equals(other.exp1))) {
            return false;
        }
        if (this.exp2 != other.exp2 && (this.exp2 == null || !this.exp2.equals(other.exp2))) {
            return false;
        }
        return true;
    }



}
