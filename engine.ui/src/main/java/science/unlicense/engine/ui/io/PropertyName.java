
package science.unlicense.engine.ui.io;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class PropertyName extends CObject implements Expression{

    private final Chars propertyName;

    public PropertyName(Chars name) {
        this.propertyName = name;
    }

    public Chars getName() {
        return propertyName;
    }

    @Override
    public Object evaluate(Object candidate) {

        if (candidate instanceof Widget) {
            return ((Widget) candidate).getPropertyValue(propertyName);
        } else {
            throw new RuntimeException("Object "+candidate+" is not a widget");
        }
    }

    @Override
    public Chars toChars() {
        return new Chars("$").concat(propertyName);
    }
    @Override
    public int getHash() {
        int hash = 7;
        hash = 23 * hash + (this.propertyName != null ? this.propertyName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PropertyName other = (PropertyName) obj;
        if (this.propertyName != other.propertyName && (this.propertyName == null || !this.propertyName.equals(other.propertyName))) {
            return false;
        }
        return true;
    }

}
