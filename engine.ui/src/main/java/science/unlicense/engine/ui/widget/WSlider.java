package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.engine.ui.model.SliderModel;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.visual.SliderView;
import science.unlicense.math.api.Tuple;

/**
 * @author Johann Sorel
 */
public class WSlider extends AbstractWidget implements WValueWidget {

    public static final Chars PROPERTY_MODEL = WRangeSlider.PROPERTY_MODEL;
    public static final Chars PROPERTY_VALUE = Chars.constant("Value");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLeaf.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("slider")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WSlider(SliderModel model) {
        this(model,model.getDefaultValue());
    }

    public WSlider(SliderModel model, Object value) {
        super(false);
        setPropertyValue(PROPERTY_MODEL, model);
        setPropertyValue(PROPERTY_VALUE, value);
        setView(new SliderView(this));
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public final SliderModel getModel() {
        return (SliderModel) getPropertyValue(PROPERTY_MODEL);
    }

    public final void setModel(SliderModel model) {
        setPropertyValue(PROPERTY_MODEL, model);
    }

    public final Property varModel() {
        return getProperty(PROPERTY_MODEL);
    }

    @Override
    public Tuple getOnSreenPosition(Widget child) {
        throw new UnimplementedException("Not supported.");
    }

    @Override
    public final Chars getValuePropertyName() {
        return PROPERTY_VALUE;
    }

    /**
     * Get widget value.
     *
     * @return value, can be null.
     */
    public final Object getValue(){
        return getPropertyValue(PROPERTY_VALUE);
    }

    /**
     * Set widget value.
     *
     * @param value, can be null.
     */
    public final void setValue(Object value){
        setPropertyValue(PROPERTY_VALUE,value);
    }

    public final Property varValue(){
        return getProperty(PROPERTY_VALUE);
    }

}
