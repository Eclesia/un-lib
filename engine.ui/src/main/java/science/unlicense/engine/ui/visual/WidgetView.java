
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.character.LChars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.model.doc.DocMessage;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.display.api.painter2d.Brush;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.ui.style.GraphicStyle;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.task.api.Task;

/**
 *
 * @author Johann Sorel
 */
public class WidgetView implements View {

    protected Extent INFINITE = new Extent.Double(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);

    protected final Widget widget;

    public WidgetView(final Widget widget) {
        this.widget = widget;
    }

    public Widget getWidget() {
        return widget;
    }

    /**
     * Get the most appropriate text translation if text is an LChars
     * @param text
     * @return CharArray
     */
    protected CharArray getTranslatedText(CharArray text) {
        if (text instanceof LChars) {
            //search for a translation
            LChars trs = ((LChars) text).translate(widget.getLanguage());
            if (trs != null) {
                return trs;
            } else {
                return text;
            }

        } else if (text instanceof Chars) {
            return (Chars) text;
        }
        return text;
    }

    @Override
    public void getExtents(Extents buffer, Extent constraint) {
        buffer.minX = 0;
        buffer.minY = 0;
        buffer.bestX = 0;
        buffer.bestY = 0;
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }

    @Override
    public BBox calculateVisualExtent(BBox buffer) {
        if (buffer == null) buffer = new BBox(2);
        final Margin bmargin = WidgetStyles.readMargin(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_MARGIN, widget);
        final Margin vmargin = WidgetStyles.readMargin(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_VISUAL_MARGIN, widget);
        final Margin margin;
        if (bmargin != null && vmargin != null) {
            margin = bmargin.expand(vmargin);
        } else if (bmargin!=null) {
            margin = bmargin;
        } else if (vmargin != null) {
            margin = vmargin;
        } else {
            margin = null;
        }

        final Extent current = widget.getEffectiveExtent();
        final double hx = current.get(0) / 2.0;
        final double hy = current.get(1) / 2.0;

        if (margin != null) {
            //use defined visual margin
            buffer.setRange(0,-hx-margin.left,hx+margin.right);
            buffer.setRange(1,-hy-margin.bottom,hy+margin.top);
        } else {
            //use default widget size
            buffer.setRange(0,-hx,hx);
            buffer.setRange(1,-hy,hy);
        }
        return buffer;
    }

    @Override
    public final void render(Painter2D painter) {
        renderBack(painter);
        renderFront(painter);
    }

    @Override
    public final void renderBack(Painter2D painter) {

        if (!widget.isVisible()) {
            //no visible, don't render it
            return;
        }

        final BBox innerBBox = widget.getInnerExtent();

        final Document style = widget.getEffectiveStyleDoc();
        final GraphicStyle[] background = WidgetStyles.readShapeStyle(style, StyleDocument.STYLE_PROP_BACKGROUND, widget);
        final GraphicStyle[] graphics = WidgetStyles.readShapeStyle(style, StyleDocument.STYLE_PROP_GRAPHIC, widget);

        //TODO : remove this, should be tested earlier
        final BBox bbox = widget.getBoundingBox(null);
        if (bbox.getSpan(0) <= 0 || bbox.getSpan(1) <= 0) {
            //nothing visible
            return;
        }

        //render the backgrounds
        final Rectangle rect = new Rectangle(bbox);
        renderShape(painter, background, rect);

        //render the graphics under
        rect.set(widget.getInnerExtent());
        int i = 0;
        for (;i<graphics.length;i++) {
            if (graphics[i] != null && graphics[i].getZorder() > 0f) break;
            renderShapeForExt(painter, graphics[i], rect);
        }

        renderSelf(painter,innerBBox);

    }

    @Override
    public final void renderFront(Painter2D painter) {

        if (!widget.isVisible()) {
            //no visible, don't render it
            return;
        }

        //TODO : remove this, should be tested earlier
        final BBox bbox = widget.getBoundingBox(null);
        if (bbox.getSpan(0) <= 0 || bbox.getSpan(1) <= 0) {
            //nothing visible
            return;
        }

        final Document style = widget.getEffectiveStyleDoc();
        final GraphicStyle[] graphics = WidgetStyles.readShapeStyle(style, StyleDocument.STYLE_PROP_GRAPHIC, widget);
        final Rectangle rect = new Rectangle(widget.getBoundingBox(null));
        rect.set(widget.getInnerExtent());

        //render the graphics above
        for (int i = 0; i < graphics.length; i++) {
            if (graphics[i] != null && graphics[i].getZorder() > 0f) {
                renderShapeForExt(painter, graphics[i], rect);
            }
        }

    }

    @Override
    public final void renderEffects(Painter2D painter) {

        final Task[] effects = WidgetStyles.readEffects(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_EFFECT, widget);
        if (effects.length > 0) {
            //create a separate painter fbo
            //TODO
        }

        if (effects.length > 0) {
            //apply effects on this image
            for (Task t : effects) {
                painter.execute(t, new Rectangle(widget.getEffectiveExtent()));
            }
        }

    }

    @Override
    public void receiveEvent(Event event) {

    }

    public void receiveWidgetEvent(PropertyMessage event) {

    }

    public void receiveStyleEvent(DocMessage event) {
        widget.updateExtents();
        widget.setDirty();
    }

    protected void renderSelf(Painter2D painter, BBox innerBBox) {

    }

    protected void renderShape(Painter2D painter, GraphicStyle[] shapes, Rectangle rect) {
        for (GraphicStyle shape : shapes){
            renderShape(painter, shape, rect);
        }
    }

    private void renderShapeForExt(Painter2D painter, GraphicStyle shape, Rectangle rect) {
        if (shape == null || rect == null) return;

        //render the graphic and fill
        final SceneNode node = shape.toSceneNode(painter, rect);
        if (node != null) painter.render(node);
    }

    protected void renderShape(Painter2D painter, GraphicStyle shape, PlanarGeometry geom) {
        if (shape == null || geom == null) return;

        //render the graphic and fill
        final Paint fillPaint = shape.getFillPaint();
        if (fillPaint != null) {
            painter.setPaint(fillPaint);
            painter.fill(geom);
        }

        final Brush brush = shape.getBrush();
        final Paint brushPaint = shape.getBrushPaint();
        if (brush != null && brushPaint != null) {
            painter.setBrush(brush);
            painter.setPaint(brushPaint);
            painter.stroke(geom);
        }
    }

}
