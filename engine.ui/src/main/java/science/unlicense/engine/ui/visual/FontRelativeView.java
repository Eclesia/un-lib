
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.engine.ui.style.FontStyle;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;

/**
 * A Widget view why extent size is based on font height.
 *
 * @author Johann Sorel
 */
public class FontRelativeView extends WidgetView{

    private static final CharArray text = new Chars(" ");

    public FontRelativeView(final Widget widget) {
        super(widget);
    }

    public void getExtents(Extents buffer, Extent constraint) {
        final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
        final FontStyle font = WidgetStyles.readFontStyle(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_FONT, widget);

        final Extent extent = new Extent.Double(0,0);

        //get text size
        final FontMetadata meta = font!=null ? fontStore.getFont(font.getFont()).getMetaData() : null;

        if (meta!=null){
            final BBox bbox = meta.getCharsBox(text);
            extent.set(0, bbox.getSpan(1));
            extent.set(1, bbox.getSpan(1));
        }

        buffer.setAll(extent);
    }

}
