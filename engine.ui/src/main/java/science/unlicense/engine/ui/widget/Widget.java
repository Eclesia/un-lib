package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.character.Language;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventManager;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.proto.Prototype;
import science.unlicense.display.api.desktop.cursor.Cursor;
import science.unlicense.display.api.layout.Positionable;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.visual.View;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.Tuple;

/**
 * Widget interface.
 *
 * Widgets all share common properties which are :
 * - min/best/max extent
 * - enable state
 * - visible state
 * - tooltip
 * - popup
 * - border
 * - background
 * - style
 *
 * @author Johann Sorel
 */
public interface Widget extends Positionable, SceneNode, Prototype {

    /**
     * Property indicating when the cursor changes.
     */
    public static final Chars PROPERTY_CURSOR = Chars.constant("Cursor");
    /**
     * Property indicating the widget needs to be repainted or not.
     */
    public static final Chars PROPERTY_DIRTY = Chars.constant("Dirty");
    /**
     * Property indicating if the widget is enable.
     */
    public static final Chars PROPERTY_ENABLE = Chars.constant("Enable");
    /**
     * Property indicating if the widget is enable.
     */
    public static final Chars PROPERTY_STACKENABLE = Chars.constant("StackEnable");
    /**
     * Property indicating if the widget is focused.
     */
    public static final Chars PROPERTY_FOCUSED = Chars.constant("Focused");
    /**
     * Property indicating if the widget has a focused child.
     */
    public static final Chars PROPERTY_CHILDFOCUSED = Chars.constant("ChildFocused");
    /**
     * Property indicating when the popup widget changed.
     */
    public static final Chars PROPERTY_POPUP = Chars.constant("Popup");
    /**
     * Property indicating when the tooltip widget changed.
     */
    public static final Chars PROPERTY_TOOLTIP = Chars.constant("Tooltip");
    /**
     * Property indicating when the frame changed.
     */
    public static final Chars PROPERTY_FRAME = Chars.constant("Frame");
    /**
     * Property indicating when the view changed.
     */
    public static final Chars PROPERTY_VIEW = Chars.constant("View");
    /**
     * Property indicating the widget style.
     */
    public static final Chars PROPERTY_STYLE = Chars.constant("Style");
    /**
     * Property indicating the widget inline style.
     */
    public static final Chars PROPERTY_INLINESTYLE = Chars.constant("InlineStyle");
    /**
     * Property indicating when the widget language changed.
     */
    public static final Chars PROPERTY_LANGUAGE = Chars.constant("Language");
    /**
     * Property indicating the widget parent.
     */
    public static final Chars PROPERTY_MOUSEOVER = Chars.constant("MouseOver");
    /**
     * Property indicating the widget parent.
     */
    public static final Chars PROPERTY_MOUSEPRESS = Chars.constant("MousePress");

    /**
     * @return id variable
     */
    Property varId();

    /**
     * @return visible variable
     */
    Property varVisible();

    /**
     * @return extents variable
     */
    Property varExtents();

    /**
     * @return effective extent variable
     */
    Property varEffectiveExtent();

    Property varFrame();

    /**
     * Set widget state, enable or disable.
     * @param enable
     */
    void setEnable(boolean enable);

    /**
     * @return true if widget is active
     */
    boolean isEnable();

    /**
     * @return enable variable
     */
    Property varEnable();

    /**
     * search throw parents to find if any of them is disable.
     * @return true if all parents in the stack are enable.
     */
    boolean isStackEnable();

    /**
     * Indicate if the widget has a focused child.
     *
     * @return true if widget has a focused child
     */
    boolean isChildFocused();

    /**
     * Indicate if the widget is focused.
     *
     * @return true if widget is focused
     */
    boolean isFocused();

    /**
     * WARNING : INTERNAL USE ONLY, DO NOT CALL IT YOURSELF.
     * USE method requestFocus.
     *
     * @param focused
     */
    void setFocused(boolean focused);

    /**
     * Indicate if the widget is focused.
     *
     * @return true if widget is focused
     */
    Property varFocused();

    /**
     * Request focus on this widget.
     */
    void requestFocus();

    /**
     * Set widget tooltip.
     *
     * @param tooltip, can be null
     */
    void setToolTip(Widget tooltip);

    /**
     * @return popup tooltip, can be null.
     */
    Widget getToolTip();

    /**
     * Set widget popup.
     * @param popup, can be null
     */
    void setPopup(Widget popup);

    /**
     * @return popup widget, can be null.
     */
    Widget getPopup();

    /**
     * Set widget language.
     * @param language, can not be null
     * @param recursive, loop on childrens to set language
     */
    void setLanguage(Language language, boolean recursive);

    /**
     * @return language, can not be null.
     */
    Language getLanguage();

    /**
     * @param language, can not be null
     */
    void setLanguage(Language language);

    /**
     * Set widget cursor.
     * @param cursor, can be null
     */
    void setCursor(Cursor cursor);

    /**
     * @return cursor  can be null.
     */
    Cursor getCursor();

    /**
     * search throw parents to find the first cursor
     * @return Cursor, can be null
     */
    Cursor getStackCursor();

    /**
     * WARNING : INTERNAL USE ONLY, DO NOT CALL IT YOURSELF.
     * Reaffect extents which are not overridden.
     */
    boolean updateExtents();

    /**
     * The widget effective inner bounding box, excluding margins and overlaps.
     * @return BoundingBox
     */
    BBox getInnerExtent();

    /**
     * INTERNAL USE ONLY, DO NOT CALL THIS METHOD YOURSELF !
     * @param frame
     */
    void setFrame(UIFrame frame);

    /**
     * Get the frame this widget is currently rendered on.
     * The frame has acces to the screen which in turns knows the DotPerMilimmeter
     * information used to correctly size widgets.
     * If the widget is not placed in a frame yet, this method returns a virtual frame.
     *
     * @return frame, never nul, can be a virtual frame
     */
    UIFrame getFrame();

    /**
     * Style definition.
     * @return
     */
    StyleDocument getStyle();

    void setStyle(StyleDocument style);

    /**
     * Style definition, used only on this widget.
     * @return can be null if no specific style applied.
     */
    StyleDocument getInlineStyle();

    void setInlineStyle(StyleDocument doc);

    void setInlineStyle(CharArray doc);

    /**
     * To be called when style document is changed directly.
     */
    void notifyStyleChanged();

    /**
     * To be called when inline style documents is changed directly.
     */
    void notifyInlineStyleChanged();

    /**
     * Merge system style, widget style and inline style sheets.
     */
    StyleDocument getEffectiveStyleSheet();

    /**
     * Evaluate style sheet result.
     */
    Document getEffectiveStyleDoc();

    ////////////////////////////////////////////////////////////////////////////
    // rendering ///////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * A widget is considered displayed if a frame is defined and
     * is root to node transform is valid (has no NaN values in it).
     *
     * @return true if widget is displayed.
     */
    boolean isDisplayed();

    void setView(View view);

    View getView();

    void setDirty();

    /**
     * Set dirty.
     *
     * This will trigger and event if the widget is displayed.
     *
     * @param dirtyArea
     */
    void setDirty(BBox dirtyArea);

    ////////////////////////////////////////////////////////////////////////////
    // popup and tooltip ///////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Get on screen position of a children of this widget.
     * The returned position if the top left corner of the widget.
     *
     * @return position of null if widget is not displayed.
     */
    Tuple getOnSreenPosition(Widget child);

    /**
     * Get on screen position of this widget.
     * The returned position if the top left corner of the widget.
     *
     * @return position of null if widget is not displayed.
     */
    Tuple getOnSreenPosition();

    /**
     * Force showing tooltip.
     */
    void showTooltip();

    /**
     * Force showing popup.
     */
    void showPopup();


    ////////////////////////////////////////////////////////////////////////////
    // events //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Catch events.
     * Such as mouse and keyboard events.
     *
     * @param event
     */
    void receiveEvent(Event event);

    ////////////////////////////////////////////////////////////////////////////
    // mouse event state ///////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * @return mouse over variable
     */
    Property varMouseOver();

    /**
     * Utility method used by styles to get current mouse state relative to this widget.
     *
     * @return true if mouse is over this widget
     */
    boolean isMouseOver();

    /**
     * @return mouse over variable
     */
    Property varMousePress();

    /**
     * Utility method used by styles to get current mouse state relative to this widget.
     *
     * @return true if mouse is pressed this widget
     */
    boolean isMousePress();


    ////////////////////////////////////////////////////////////////////////////
    // Utils for all widgets ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * FOR INTERNAL USE ONLY.
     *
     * @return
     */
    EventManager getEventManager();

}
