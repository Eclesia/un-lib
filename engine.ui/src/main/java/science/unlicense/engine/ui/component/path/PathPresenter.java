
package science.unlicense.engine.ui.component.path;

import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.encoding.api.path.Path;

/**
 * Used by WPatchChooser to generate previews for files.
 *
 * @author Johann Sorel
 */
public interface PathPresenter {

    /**
     * Presenters may have a priority.
     * For example media types should be tested before images because it is
     * more interesting to display an animated gif/png/webp rather then a static
     * image.
     *
     * @return priority, higher values for more importance
     */
    float getPriority();

    /**
     * Indicate id this presenter can support the given input.
     * @param path
     * @return true if path object is supported.
     */
    boolean canHandle(Path path);

    /**
     * Create a mime type like image.
     * This is normaly a small size image, 12 to 48 pixels.
     * The image should represent general data type.
     * This method is expected to retun very quickly.
     *
     * @param size
     * @return Image
     */
    Image createMimeImage(Extent.Long size);

    /**
     * Create a preview image.
     * This is usaly a downscaled representation of the data.
     * This method may take some time to return.
     *
     * @param path
     * @param size
     * @return Image
     */
    Image createImage(Path path, Extent.Long size);

    /**
     * Create an interactive widget for the given object.
     * The widget is only for visualization purpose.
     * no function or only a minimum set should be available.
     *
     * @param path
     * @return widget
     */
    Widget createInteractive(Path path);

}
