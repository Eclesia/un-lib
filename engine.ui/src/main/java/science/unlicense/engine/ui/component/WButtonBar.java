

package science.unlicense.engine.ui.component;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.widget.WContainer;

/**
 *
 * @author Johann Sorel
 */
public class WButtonBar extends WContainer{

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant("buttonbar"));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WButtonBar() {
        setLayout(new FormLayout());
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }
}
