

package science.unlicense.engine.ui.component;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.DefaultColorRW;

/**
 * Color chooser.
 *
 * @author Johann Sorel
 */
public class WColorPane extends WLeaf implements WValueWidget{

    /**
     * Property for the widget color.
     */
    public static final Chars PROPERTY_COLOR = Chars.constant("Color");


    public WColorPane() {
        setView(new ColorPaneView(this));
    }

    @Override
    public final Chars getValuePropertyName() {
        return PROPERTY_COLOR;
    }

    public final Color getColor() {
        return (Color) getPropertyValue(PROPERTY_COLOR);
    }

    public final void setColor(Color color) {
        setPropertyValue(PROPERTY_COLOR, color);
    }

    public final Property varColor() {
        return getProperty(PROPERTY_COLOR);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_COLOR.equals(name)) {
            setDirty();
        }

    }

    protected void receiveEventParent(Event event) {

        if (event.getMessage() instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) event.getMessage();
            final int type = me.getType();
            if (MouseMessage.TYPE_PRESS == type && me.getButton() == MouseMessage.BUTTON_1){
                me.consume();
                setColor(toColor(getEffectiveExtent(),
                        (float) me.getMousePosition().get(0),
                        (float) me.getMousePosition().get(1)));
            }
        }

        //forward mouse and key event to listeners if any.
        super.receiveEventParent(event);
    }

    /**
     * Convert mouse coordinate on image in a color.
     * @param ext
     * @param x
     * @param y
     * @return
     */
    private Color toColor(Extent ext, float x, float y){
        final Color base = getColor();
        final float[] buffer;
        if (base != null) {
            buffer = base.toColorSystem(ColorSystem.HSL_FLOAT).getSamples(null);
        } else {
            buffer = new float[]{0,0,0,1};
        }
        x+=ext.get(0)/2.0;
        y+=ext.get(1)/2.0;
        buffer[0] = (x*360f) / (float) ext.get(0);
        buffer[1] = 1f;
        buffer[2] = 1f - ((float) y / (float) ext.get(1));
        return new DefaultColorRW(buffer, ColorSystem.HSL_FLOAT);
    }

}
