package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.visual.FontRelativeView;

/**
 * Two state widget, checked or unchecked.
 *
 * @author Johann Sorel
 */
public class WCheckBox extends AbstractLabeled implements WValueWidget {

    /**
     * Property for the widget check value.
     */
    public static final Chars PROPERTY_CHECK = Chars.constant("Check");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(AbstractLabeled.CLASS_DEFAULTS);
        defs.add(PROPERTY_CHECK, Boolean.FALSE);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    final Graphic graphic = new Graphic();

    public WCheckBox() {
        this(null,null);
    }

    public WCheckBox(CharArray text) {
        this(text,null);
    }

    /**
     *
     * @param text
     * @param listener listener to attach on the Check property
     */
    public WCheckBox(CharArray text, EventListener listener) {
        setHorizontalAlignment(HALIGN_LEFT);
        setGraphicPlacement(GRAPHIC_LEFT);
        setGraphic(graphic);
        setText(text);
        if (listener != null) {
            addEventListener(new PropertyPredicate(PROPERTY_CHECK), listener);
        }
        graphic.properties.getProperty(PROPERTY_CHECK).sync(varCheck(), true);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    @Override
    public final Chars getValuePropertyName() {
        return PROPERTY_CHECK;
    }

    /**
     * Set check value.
     * @param check
     */
    public final void setCheck(boolean check) {
        setPropertyValue(PROPERTY_CHECK, check);
    }

    /**
     * Get check value.
     * @return boolean
     */
    public final boolean isCheck() {
        //test equality, value could be null at first
        return (Boolean) getPropertyValue(PROPERTY_CHECK);
    }

    /**
     * Get check variable.
     * @return Variable.
     */
    public final Property varCheck(){
        return getProperty(PROPERTY_CHECK);
    }

    @Override
    protected void receiveEventParent(Event event) {
        super.receiveEventParent(event);

        if (event.getMessage() instanceof MouseMessage && isStackEnable()){
            final MouseMessage me = (MouseMessage) event.getMessage();
            final int type = me.getType();

            if (MouseMessage.TYPE_PRESS == type && me.getButton() == MouseMessage.BUTTON_1){
                requestFocus();
                me.consume();
            } else if (MouseMessage.TYPE_RELEASE == type && me.getButton() == MouseMessage.BUTTON_1){
                me.consume();
                setCheck(!isCheck());
            }
        }
    }

    private static final Dictionary GRAPHICCLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLeaf.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("checkbox")));
        GRAPHICCLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public class Graphic extends WLeaf{

        public Graphic() {
            setView(new FontRelativeView(Graphic.this));
        }

        @Override
        protected Dictionary getClassDefaultProperties() {
            return GRAPHICCLASS_DEFAULTS;
        }

    }

}
