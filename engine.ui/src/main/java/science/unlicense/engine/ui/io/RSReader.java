
package science.unlicense.engine.ui.io;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Characters;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.Field;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.predicate.And;
import science.unlicense.common.api.predicate.Constant;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.common.api.predicate.Not;
import science.unlicense.common.api.predicate.Or;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.style.ExpressionReference;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.format.json.HJSONReader;
import science.unlicense.format.json.JSONUtilities;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenGroup;
import science.unlicense.syntax.api.lexer.TokenType;
import science.unlicense.syntax.api.parser.Parser;
import science.unlicense.syntax.api.parser.Rule;
import science.unlicense.syntax.api.parser.SyntaxNode;
import science.unlicense.syntax.impl.grammar.io.UNGrammarReader;

/**
 *
 * @author Johann Sorel
 */
public class RSReader {

    private static final Chars GRAMMAR = new Chars(
    "# Ruled Style grammar\n" +
    "# author : Johann Sorel\n" +
    "\n" +
    "\n" +
    "# PREPROCESS token part\n" +
    "$D : [0-9] ;\n" +
    "$L : ([a-z]|[A-Z]|[_\\-]) ;\n" +
    "$S : (-|\\+)? ;\n" +
    "$H : ([0-9]|[a-f]|[A-F]) ;\n" +
    "\n" +
    "\n" +
    "# TOKENS used in lexer\n" +
    "DEFAULT : {\n" +
    "    AND         : AND ;\n" +
    "    OR          : OR ;\n" +
    "    TRUE        : \"true\" ;\n" +
    "    FALSE       : \"false\" ;\n" +
    "    NONE        : \"none\" ;\n" +
    "    IN          : \"in\" ;\n" +
    "    UNIT        : em|ch|%|cm|mm|px|pt|pc ;\n" +
    "    NUMBER      : $S$D+(\\.$D+)?((e|E)$S$D+)? ;\n" +
    "    STRING      : \\'[^\\']*\\' ;\n" +
    "    ESCWORD     : \\$$L($D|$L)* ;\n" +
    "    WORD        : $L($D|$L)* ;\n" +
    "    REFERENCE   : \\@$L($D|$L)* ;\n" +
    "\n" +
    "    WS          : ( |\\t|\\n|\\r)+ ;\n" +
    "    COM         : , ;\n" +
    "    EQUAL       : = ;\n" +
    "    BL          : \\[ ;\n" +
    "    BR          : \\] ;\n" +
    "    PL          : \\( ;\n" +
    "    PR          : \\) ;\n" +
    "}\n" +
    "\n" +
    "\n" +
    "\n" +
    "# RULES used in parser\n" +
    "id          : ESCWORD;\n" +
    "idx         : BL NUMBER BR ;\n" +
    "arr         : BL WS? exp WS? (COM WS? exp WS?)* BR ;\n" +
    "lit         : (NUMBER UNIT?) | STRING | TRUE | FALSE | NONE | arr ;\n" +
    "fct         : WORD WS? PL WS? (exp (WS exp)* WS?)? PR ;\n" +
    "basic       : id | REFERENCE | lit | fct | (PL WS? exp WS? PR) ;\n" +
    "logic       : basic (WS? (EQUAL|IN|AND|OR) WS? exp)? ;\n" +
    "exp         : logic ;\n" +
    "");

    private static final Chars RULE_ID          = Chars.constant("id");
    private static final Chars RULE_ARRAY       = Chars.constant("arr");
    private static final Chars RULE_LITERAL     = Chars.constant("lit");
    private static final Chars RULE_FUNCTION    = Chars.constant("fct");
    private static final Chars RULE_EXPRESSION  = Chars.constant("exp");
    private static final Chars RULE_BASIC       = Chars.constant("basic");
    private static final Chars RULE_INDEX       = Chars.constant("idx");
    private static final Chars RULE_DOC         = Chars.constant("doc");
    private static final Chars RULE_LOGIC       = Chars.constant("logic");

    private static final Chars TOKEN_EQUAL      = Chars.constant("EQUAL");
    private static final Chars TOKEN_IN         = Chars.constant("IN");
    private static final Chars TOKEN_UNIT       = Chars.constant("UNIT");
    private static final Chars TOKEN_TRUE       = Chars.constant("TRUE");
    private static final Chars TOKEN_FALSE      = Chars.constant("FALSE");
    private static final Chars TOKEN_NONE       = Chars.constant("NONE");
    private static final Chars TOKEN_NUMBER     = Chars.constant("NUMBER");
    private static final Chars TOKEN_WORD       = Chars.constant("WORD");
    private static final Chars TOKEN_PROPERTY   = Chars.constant("PROPERTY");
    private static final Chars TOKEN_STRING     = Chars.constant("STRING");
    private static final Chars TOKEN_REF        = Chars.constant("REFERENCE");
    private static final Chars TOKEN_AND        = Chars.constant("AND");
    private static final Chars TOKEN_OR         = Chars.constant("OR");

    private static final Predicate TRIM;
    private static final Lexer LEXER;
    private static final Parser PARSER_EXP;

    //OPTIMISATION : very commun case
    private static final PropertyName CLASS_PROPERTYNAME = new PropertyName(EqualsPredicate.CLASS);

    static {

        try{
            final UNGrammarReader reader = new UNGrammarReader();
            //reader.setInput(Paths.resolve(new Chars("mod:/module-res/rs.gr")));
            reader.setInput(GRAMMAR.toBytes());
            final HashDictionary groups = new HashDictionary();
            final OrderedHashDictionary rules = new OrderedHashDictionary();
            reader.read(groups, rules);

            final TokenGroup tokens = (TokenGroup) groups.getValues().createIterator().next();

            // grammar parser is already tested
            final Rule ruleExp = (Rule) rules.getValue(new Chars("exp"));

            //prepare lexer
            LEXER = new Lexer();
            LEXER.setTokenGroup(tokens);

            //prepare parser
            PARSER_EXP = new Parser(ruleExp);

            final TokenType[] toExclude = new TokenType[]{
                (TokenType) tokens.getValue(new Chars("WS")),
                (TokenType) tokens.getValue(new Chars("COM")),
                (TokenType) tokens.getValue(new Chars("BL")),
                (TokenType) tokens.getValue(new Chars("BR")),
                (TokenType) tokens.getValue(new Chars("PL")),
                (TokenType) tokens.getValue(new Chars("PR"))
            };

            TRIM = new Predicate() {
                public Boolean evaluate(Object candidate) {
                    final SyntaxNode sn = (SyntaxNode) candidate;
                    final Token token = sn.getToken();
                    return token!=null && Arrays.containsIdentity(toExclude, 0, toExclude.length, token.type);
                }
            };
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private RSReader(){}

    public static synchronized StyleDocument readStyleDoc(Object input) throws InvalidArgumentException{

        try {
            if (input instanceof CharArray) input = ((CharArray) input).toBytes();
            final HJSONReader reader = new HJSONReader();
            reader.setInput(input);
            final Document doc = JSONUtilities.readAsDocument(reader, null);
            return toStyleDoc(null, doc);

//            final StyleDocument style = new StyleDocument();
//            style.set(doc);
//
//            final Iterator ite = doc.getPropertyNames().createIterator();
//            while (ite.hasNext()) {
//                final Chars ruleName = (Chars) ite.next();
//                Document rule = (Document) doc.getPropertyValue(ruleName);
//                style.getRules().add(toStyleDoc(ruleName, rule));
//            }
//            return style;
        } catch (IOException ex) {
            throw new InvalidArgumentException(ex.getMessage(), ex);
        }
    }

    private static StyleDocument toStyleDoc(Chars name, Document doc) throws IOException{
        final StyleDocument sd = new StyleDocument();
        if (name!=null) sd.setName(name);

        final Iterator ite = doc.getPropertyNames().createIterator();
        while (ite.hasNext()) {
            Chars fieldName = (Chars) ite.next();
            Object value = doc.getPropertyValue(fieldName);
            final int sep = fieldName.getFirstOccurence('.');
            int idx = -1;
            if (sep > 0) {
               idx = Int32.decode(fieldName, sep+1, -1);
               fieldName = fieldName.truncate(0, sep);
            }
            value = jsonValueToStyleValue(null, value);

            if (idx >= 0) {
                final Field prop = sd.getProperty(fieldName);
                Object[] values = WidgetStyles.getFieldValues(prop);
                if (values.length <= idx) {
                    values = Arrays.resize(values, idx+1);
                }
                values[idx] = value;
                WidgetStyles.setFieldValues(prop, values);
            } else {
                sd.setPropertyValue(Chars.constant(fieldName), value);
            }
        }

        return sd;
    }

     private static Object jsonValueToStyleValue(Chars fieldName, Object value) throws IOException{

        if (value instanceof Chars) {
            value = readExpression((Chars) value,true);
        } else if (value instanceof Document) {
            value = toStyleDoc(fieldName, (Document) value);
        } else if (value instanceof Sequence) {
            final Sequence s = (Sequence) value;
            for (int i=0,n=s.getSize();i<n;i++) {
                Object e = jsonValueToStyleValue(fieldName, s.get(i));
                s.replace(i, e);
            }
        } else if (value.getClass().isArray()) {
            final int size = Arrays.getSize(value);
            final Sequence table = new ArraySequence(size);
            for (int i=0;i<size;i++) {
                table.add( jsonValueToStyleValue(fieldName, Arrays.getValue(value, i)) );
            }
            value = new Constant(table);
        } else if (value != null) {
            //already type mapped by json
            value = new Constant(value);
        } else {
            throw new IOException(null, "Null value ");
        }

        return value;
    }

     /**
      *
      * @param text
      * @param optimize will try to replace static expressions by literals
      * @return
      * @throws IOException
      */
    public static synchronized Expression readExpression(Chars text, boolean optimize) throws IOException{
        final ArrayInputStream is = new ArrayInputStream(text.toBytes());

        //parse
        LEXER.setInput(is);
        PARSER_EXP.setInput(LEXER);
        final SyntaxNode node = PARSER_EXP.parse();
        node.trim(TRIM);

        Expression exp =  readExpression(node);
        if (optimize) {
            exp = optimize(exp);
        }
        return exp;
    }

    private static Expression readExpression(SyntaxNode node) throws IOException{

        final SyntaxNode logicNode = node.getChildByRule(RULE_LOGIC);
        if (logicNode != null){
            return readLogicFilter(logicNode);
        } else {
            return Predicate.TRUE;
        }
    }

    private static Expression readLogicFilter(SyntaxNode sn) throws IOException{
        final Sequence exps = sn.getChildren();
        if (exps.getSize()==1){
            return readBasic((SyntaxNode) exps.get(0));
        } else if (exps.getSize()!=3){
            return readBasic((SyntaxNode) exps.get(0));
        }
        final Expression exp1 = readBasic((SyntaxNode) exps.get(0));
        final Expression exp2 = readExpression((SyntaxNode) exps.get(2));

        if (sn.getChildByToken(TOKEN_AND)!=null){
            return new And(new Predicate[]{(Predicate) exp1, (Predicate) exp2});
        } else if (sn.getChildByToken(TOKEN_OR)!=null){
            return new Or(new Predicate[]{(Predicate) exp1, (Predicate) exp2});
        } else if (sn.getChildByToken(TOKEN_IN)!=null){
            return new InPredicate(exp1, exp2);
        } else if (sn.getChildByToken(TOKEN_EQUAL)!=null){
            return new EqualsPredicate(exp1, exp2);
        } else {
            throw new IOException(null, "Unexpected");
        }
    }

    private static Expression readBasic(SyntaxNode sn) throws IOException{
        final SyntaxNode filterNode = sn.getChildByRule(RULE_EXPRESSION);
        if (filterNode != null){
            return readExpression(filterNode);
        }

        final SyntaxNode idNode = sn.getChildByRule(RULE_ID);
        if (idNode!=null){
            return readId(idNode);
        }

        final SyntaxNode refNode = sn.getChildByToken(TOKEN_REF);
        if (refNode!=null){
            //expression reference
            final Chars text = refNode.getToken().value;
            final Chars value = text.truncate(1, text.getCharLength());
            return new ExpressionReference(Chars.constant(value));
        }

        final SyntaxNode litNode = sn.getChildByRule(RULE_LITERAL);
        if (litNode!=null){
            return readLiteral(litNode);
        }

        final SyntaxNode fctNode = sn.getChildByRule(RULE_FUNCTION);
        if (fctNode!=null){
            return readFunction(fctNode);
        }

        throw new IOException(null, "Invalid expression "+sn);
    }

    private static Expression readId(SyntaxNode sn) throws IOException{
        final SyntaxNode child = (SyntaxNode) sn.getChildren().get(0);
        Chars id = child.getToken().value;
        if (id.startsWith('$')){
            id = id.truncate(1, id.getCharLength());
        }
        id = replaceConstantNames(id);
        if (id.equals(EqualsPredicate.CLASS)){
            return CLASS_PROPERTYNAME;
        } else {
            //TODO : for performance reasons, making id constant
            //this may cause the constant pool to grow large
            return new PropertyName(Chars.constant(id));
        }
    }

    private static Expression readLiteral(SyntaxNode sn) throws IOException{

        final SyntaxNode numNode = sn.getChildByToken(TOKEN_NUMBER);
        if (numNode!=null){
            science.unlicense.common.api.number.Number n;
            try{
                n = new Int32(Int32.decode(numNode.getToken().value));
            }catch(Exception ex){
                n = new Float64(Float64.decode(numNode.getToken().value));
            }
            final SyntaxNode unitNode = sn.getChildByToken(TOKEN_UNIT);
            if (unitNode!=null) {
                final Chars un = unitNode.getToken().value;
                return new UnitNumber(n,RSUnits.forName(un));
            }
            return new Constant(n);
        }

        final SyntaxNode strNode = sn.getChildByToken(TOKEN_STRING);
        if (strNode!=null){
            final Chars text = strNode.getToken().value;
            final Chars value = text.truncate(1, text.getCharLength()-1);
            //TODO : for performance reasons, making literal constant
            //this may cause the constant pool to grow large
            return new Constant(Chars.constant(Characters.resolveEscapes(value)));
        }

        final SyntaxNode trueNode = sn.getChildByToken(TOKEN_TRUE);
        if (trueNode!=null){
            return new Constant(Boolean.TRUE);
        }

        final SyntaxNode falseNode = sn.getChildByToken(TOKEN_FALSE);
        if (falseNode!=null){
            return new Constant(Boolean.FALSE);
        }

        final SyntaxNode noneNode = sn.getChildByToken(TOKEN_NONE);
        if (noneNode!=null){
            return WidgetStyles.NULL_CST;
        }

        final SyntaxNode arrNode = sn.getChildByRule(RULE_ARRAY);
        if (arrNode!=null){
            final Sequence array = new ArraySequence();
            for (int i=0,n=arrNode.getChildren().getSize();i<n;i++){
                array.add(readExpression((SyntaxNode) arrNode.getChildren().get(i)));
            }
            return new Constant(Collections.readOnlySequence(array));
        }

        throw new IOException(null, "Invalid literal "+sn);
    }

    private static Expression readFunction(SyntaxNode sn) throws IOException{
        final Chars name = sn.getChildByToken(TOKEN_WORD).getToken().value;
        final Sequence exps = sn.getChildrenByRule(RULE_EXPRESSION);
        final Sequence array = new ArraySequence();
        for (int i=0,n=exps.getSize();i<n;i++){
            array.add(readExpression((SyntaxNode) exps.get(i)));
        }
        final Expression[] arr = new Expression[exps.getSize()];
        Collections.copy(array, arr, 0);

        return RSFunctions.INSTANCE.resolve(name, arr);
    }

    private static Chars replaceConstantNames(Chars candidate){
        //TODO find a more efficient way to replace constants
        if (StyleDocument.PROP_FILTER.equals(candidate)){
            return StyleDocument.PROP_FILTER;
        } else if (StyleDocument.PROP_ISTRIGGER.equals(candidate)){
            return StyleDocument.PROP_ISTRIGGER;
        } else if (EqualsPredicate.CLASS.equals(candidate)){
            return EqualsPredicate.CLASS;
        }
        return candidate;
    }

    private static Expression optimize(Expression exp) {
        if (exp instanceof Constant || exp instanceof PropertyName || exp instanceof ExpressionReference) {
            //can no optimize
            return exp;
        } else if (exp instanceof And) {
            And fct = (And) exp;
            Predicate[] parameters = fct.getPrecidates();

            boolean changed = false;
            boolean allConstant = true;
            for (int i=0;i<parameters.length;i++) {
                Expression opt = optimize(parameters[i]);
                if (opt!=parameters[i]) {
                    changed = true;
                    parameters[i] = (Predicate) opt;
                }
                allConstant &= (opt instanceof Constant);
            }

            if (changed) {
                fct = new And(parameters);
            }
            if (allConstant) {
                //evaluate now
                return new Constant(fct.evaluate(null));
            } else {
                return fct;
            }
        } else if (exp instanceof Or) {
            Or fct = (Or) exp;
            Predicate[] parameters = fct.getPrecidates();

            boolean changed = false;
            boolean allConstant = true;
            for (int i=0;i<parameters.length;i++) {
                Expression opt = optimize(parameters[i]);
                if (opt!=parameters[i]) {
                    changed = true;
                    parameters[i] = (Predicate) opt;
                }
                allConstant &= (opt instanceof Constant);
            }

            if (changed) {
                fct = new Or(parameters);
            }
            if (allConstant) {
                //evaluate now
                return new Constant(fct.evaluate(null));
            } else {
                return fct;
            }
        } else if (exp instanceof Not) {
            Not fct = (Not) exp;
            Expression exp1 = optimize(fct.getPredicate());
            if (exp1!=fct.getPredicate()) {
                fct = new Not((Predicate) exp1);
            }
            if (exp1 instanceof Constant) {
                //evaluate now
                return new Constant(fct.evaluate(null));
            } else {
                return fct;
            }
        } else if (exp instanceof EqualsPredicate) {
            EqualsPredicate fct = (EqualsPredicate) exp;
            Expression exp1 = optimize(fct.getExp1());
            Expression exp2 = optimize(fct.getExp2());
            if (exp1!=fct.getExp1() || exp2!=fct.getExp2()) {
                fct = new EqualsPredicate(exp1, exp2);
            }
            if (exp1 instanceof Constant && exp2 instanceof Constant) {
                //evaluate now
                return new Constant(fct.evaluate(null));
            } else {
                return fct;
            }

        } else {
            //unknown, can't optimize
            return exp;
        }
    }

}
