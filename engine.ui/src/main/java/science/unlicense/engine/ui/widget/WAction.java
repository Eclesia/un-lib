package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.engine.ui.ievent.AbstractActionExecutable;
import science.unlicense.engine.ui.ievent.ActionExecutable;
import science.unlicense.engine.ui.visual.ActionView;

/**
 * Action type widget.
 *
 * @author Johann Sorel
 */
public class WAction extends AbstractLabeled{

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(AbstractLabeled.CLASS_DEFAULTS);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WAction() {
        this(null,(Widget) null);
    }

    public WAction(CharArray text) {
        this(text,(Widget) null);
    }

    public WAction(CharArray text, Widget graphic) {
        this(text,graphic,null);
    }

    public WAction(CharArray text, Widget graphic, EventListener listener) {
        super(text,graphic);
        setView(new ActionView(this));
        setHorizontalAlignment(HALIGN_CENTER);
        setVerticalAlignment(VALIGN_CENTER);
        setGraphicPlacement(GRAPHIC_LEFT);
        if (listener!=null){
            addEventListener(ActionMessage.PREDICATE, listener);
        }
    }

    public WAction(final ActionExecutable exec){
        setView(new ActionView(this));
        new ActionMapper(this, exec);
        setHorizontalAlignment(HALIGN_CENTER);
        setVerticalAlignment(VALIGN_CENTER);
        setGraphicPlacement(GRAPHIC_LEFT);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    /**
     * Override to do extra work when button is clicked.
     */
    public void doClick(){
        if (hasListeners()) getEventManager().sendEvent(new Event(this, new ActionMessage()));
    }

    public Class[] getEventClasses() {
        return new Class[]{
            MouseMessage.class,
            KeyMessage.class,
            ActionMessage.class
        };
    }

    protected static class ActionMapper implements EventListener {

        private final ActionExecutable exec;
        private final WAction button;

        public ActionMapper(WAction button, ActionExecutable exec) {
            this.button = button;
            this.exec = exec;
            button.setText(exec.getText());
            button.setEnable(exec.isActive());
            exec.addEventListener(PropertyMessage.PREDICATE, ActionMapper.this);
            button.addEventListener(ActionMessage.PREDICATE, ActionMapper.this);
        }

        public void receiveEvent(Event event) {
            final EventMessage message = event.getMessage();
            if (message instanceof PropertyMessage){
                final PropertyMessage pe = (PropertyMessage) message;
                if (AbstractActionExecutable.PROPERTY_ACTIVE.equals(pe.getPropertyName())){
                    button.setEnable(exec.isActive());
                } else if (AbstractActionExecutable.PROPERTY_TEXT.equals(pe.getPropertyName())){
                    button.setText(exec.getText());
                } else if (AbstractActionExecutable.PROPERTY_IMAGE.equals(pe.getPropertyName())){
                    //TODO image on button
                }
            } else if (message instanceof ActionMessage){
                exec.perform();
            }
        }

    }

}
