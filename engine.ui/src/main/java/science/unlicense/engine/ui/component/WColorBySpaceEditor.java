
package science.unlicense.engine.ui.component;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.number.Float64;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.model.NumberSliderModel;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSlider;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.DefaultColorRW;
import science.unlicense.image.api.colorspace.ColorSpace;
import science.unlicense.image.api.colorspace.ColorSpaceComponent;
import science.unlicense.math.impl.Affine1;

/**
 * Color editor using color system definition.
 *
 * @author Johann Sorel
 */
public class WColorBySpaceEditor extends WContainer implements WValueWidget{

    public static final Chars PROPERTY_COLOR = Chars.constant("Color");

    private boolean updating = false;
    private final ColorSystem cs;
    private final WSlider[] sliders;
    private final WSpinner[] spinners;

    public WColorBySpaceEditor(ColorSystem cs) {
        super(new FormLayout());
        this.cs = cs;

        final EventListener listener = new EventListener() {
            public void receiveEvent(Event event) {
                rebuild();
            }
        };

        final ColorSpace space = cs.getColorSpace();
        final Affine1[] trs = cs.getSampleToCs();
        spinners = new WSpinner[cs.getNumComponents()];
        sliders = new WSlider[cs.getNumComponents()];
        for (int i=0;i<spinners.length;i++) {
            final ColorSpaceComponent csc;
            if (cs.hasAlpha() && i == spinners.length-1) {
                csc = ColorSpaceComponent.ALPHA;
            } else {
                csc = space.getComponents()[i];
            }
            double min = trs[i].invert().transform(csc.getMinValue());
            double max = trs[i].invert().transform(csc.getMaxValue());
            spinners[i] = new WSpinner(new NumberSpinnerModel(Float64.class, 0, min, max, 1.0));
            spinners[i].varValue().addListener(listener);
            sliders[i] = new WSlider(new NumberSliderModel(Float64.class, min, max, 0));
            sliders[i].varValue().addListener(listener);
            sliders[i].varValue().sync(spinners[i].varValue());

            final WLabel lbl = new WLabel(csc.getName().toUpperCase());
            addChild(lbl, FillConstraint.builder().coord(0, i).fill(true, true).build());
            addChild(sliders[i],FillConstraint.builder().coord(1, i).fill(true, true).build());
            addChild(spinners[i],FillConstraint.builder().coord(2, i).fill(true, true).build());
        }

    }

    private void rebuild() {
        if (updating) return;

        final float[] values = new float[spinners.length];
        for (int i=0;i<values.length;i++) {
            values[i] = ((Number) spinners[i].getValue()).floatValue();
        }

        final Color color = new DefaultColorRW(values, cs);
        setColor(color);
    }

    @Override
    public final Chars getValuePropertyName() {
        return PROPERTY_COLOR;
    }

    public final Color getColor() {
        return (Color) getPropertyValue(PROPERTY_COLOR);
    }

    public final void setColor(Color color) {
        setPropertyValue(PROPERTY_COLOR, color);
    }

    public final Property varColor() {
        return getProperty(PROPERTY_COLOR);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_COLOR.equals(name)) {
            updating = true;
            Color color = (Color) value;
            color = color.toColorSystem(cs);
            for (int i=0;i<spinners.length;i++) {
                spinners[i].setValue(color.get(i));
            }
            updating = false;
        }
    }

}
