

package science.unlicense.engine.ui.component;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.desktop.FrameManager;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WTabContainer;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorSystem;

/**
 *
 * @author Johann Sorel
 */
public class WColorChooser extends WContainer implements WValueWidget{

    /**
     * Property for the widget color.
     */
    public static final Chars PROPERTY_COLOR = Chars.constant("Color");

    private final WColorPane view = new WColorPane();
    private final WColorSquare square = new WColorSquare(new Extent.Double(30, 30));
    private final WColorBySpaceEditor rgbEditor = new WColorBySpaceEditor(ColorSystem.RGBA_8BITS);
    private final WColorBySpaceEditor hslEditor = new WColorBySpaceEditor(ColorSystem.HSL_FLOAT);

    public WColorChooser() {
        this(Color.RED);
    }
    public WColorChooser(Color baseColor) {
        final FormLayout layout = new FormLayout();
        layout.setColumnSize(0, 150);
        layout.setColumnSize(1, FormLayout.SIZE_EXPAND);
        layout.setRowSize(0, FormLayout.SIZE_EXPAND);
        layout.setRowSize(1, 30);
        setLayout(layout);
        setColor(Color.WHITE);

        addChild(view, FillConstraint.builder().coord(0, 0).fill(true, true).build());
        addChild(square, FillConstraint.builder().coord(0, 1).fill(true, true).span(2, 1).build());

        final WTabContainer tabs = new WTabContainer();
        addChild(tabs, FillConstraint.builder().coord(1, 0).fill(true, true).build());
        tabs.addTab(rgbEditor, new WLabel(new Chars("RGB")));
        tabs.addTab(hslEditor, new WLabel(new Chars("HSL")));

        view.varColor().sync(varColor());
        square.varColor().sync(varColor());
        rgbEditor.varColor().sync(varColor());
        hslEditor.varColor().sync(varColor());
        setColor(baseColor);
    }

    @Override
    public final Chars getValuePropertyName() {
        return PROPERTY_COLOR;
    }

    public final Color getColor() {
        return (Color) getPropertyValue(PROPERTY_COLOR);
    }

    public final void setColor(Color color) {
        setPropertyValue(PROPERTY_COLOR, color);
    }

    public final Property varColor() {
        return getProperty(PROPERTY_COLOR);
    }

    public Color showOpenDialog(UIFrame parent){
        if (parent!=null){
            return showOpenDialog(parent.getManager(), parent);
        } else {
            return showOpenDialog(FrameManagers.getFrameManager());
        }
    }

    public Color showOpenDialog(FrameManager framemanager){
        return showOpenDialog(framemanager!=null?framemanager:FrameManagers.getFrameManager(), (UIFrame) null);
    }

    private Color showOpenDialog(FrameManager frameManager, UIFrame parent){
        //addChild(this,BorderConstraint.BOTTOM);

        final UIFrame dialog;
        if (parent!=null){
            dialog = (UIFrame) parent.getManager().createFrame(parent, true, false);
        } else if (frameManager!=null){
            dialog = (UIFrame) frameManager.createFrame(parent, true, false);
        } else {
            dialog = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        }
        final WContainer contentPane = dialog.getContainer();
        contentPane.setLayout(new BorderLayout());
        contentPane.addChild(this, BorderConstraint.CENTER);

        dialog.setSize(500, 280);
        dialog.setTitle(new Chars("Color chooser"));
        dialog.setVisible(true);

        return getColor();
    }

}
