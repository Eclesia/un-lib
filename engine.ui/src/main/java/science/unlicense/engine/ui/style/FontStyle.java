
package science.unlicense.engine.ui.style;

import science.unlicense.common.api.CObjects;
import science.unlicense.display.api.painter2d.Brush;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.painter2d.Paint;

/**
 *
 * @author Johann Sorel
 */
public class FontStyle extends GeomStyle {

    private FontChoice font;

    public FontStyle() {
    }

    public FontStyle(FontChoice font, Brush brush, Paint brushPaint, Paint fillPaint) {
        super(brush,brushPaint,fillPaint);
        CObjects.ensureNotNull(font);
        this.font = font;
    }

    public FontChoice getFont() {
        return font;
    }

    public void setFont(FontChoice font) {
        CObjects.ensureNotNull(font);
        this.font = font;
    }

}
