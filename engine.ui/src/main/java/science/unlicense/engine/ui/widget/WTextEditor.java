
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.engine.ui.model.TextModel;
import science.unlicense.engine.ui.visual.TextEditorView;

/**
 *
 * @author Johann Sorel
 */
public class WTextEditor extends WLeaf{

    /**
     * Property for the widget text.
     */
    public static final Chars PROPERTY_TEXT = WTextField.PROPERTY_TEXT;
    public static final Chars PROPERTY_EDITED_TEXT = WTextField.PROPERTY_EDITED_TEXT;
    public static final Chars PROPERTY_PREVIEW_TEXT = WTextField.PROPERTY_PREVIEW_TEXT;
    public static final Chars PROPERTY_VALIDATOR = WTextField.PROPERTY_VALIDATOR;
    public static final Chars PROPERTY_EDITION_VALID = WTextField.PROPERTY_EDITION_VALID;

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLeaf.CLASS_DEFAULTS);
        defs.add(PROPERTY_EDITION_VALID, Boolean.TRUE);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private final TextModel textModel = new TextModel();

    public WTextEditor() {
        this(Chars.EMPTY);
    }

    public WTextEditor(CharArray text) {
        textModel.varText().sync(varEditedText());
        setText(text);
        setView(new TextEditorView(this));
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    /**
     * Get text model,
     * The model provide more control ovre text caret and selection.
     *
     * @return
     */
    public final TextModel getTextModel() {
        return textModel;
    }

    /**
     * Set text area text.
     * @param text can be null
     */
    public final void setText(CharArray text) {
        setPropertyValue(PROPERTY_TEXT,text);
    }

    /**
     * Get text area text.
     * @return Chars, can be null
     */
    public final CharArray getText() {
        return (CharArray) getPropertyValue(PROPERTY_TEXT);
    }

    /**
     * Get text variable.
     * @return Variable.
     */
    public final Property varText(){
        return getProperty(PROPERTY_TEXT);
    }

    public final void setEditedText(CharArray text) {
        setPropertyValue(PROPERTY_EDITED_TEXT,text);
    }

    public final CharArray getEditedText() {
        return (CharArray) getPropertyValue(PROPERTY_EDITED_TEXT);
    }

    /**
     * Get edited text variable.
     * @return Variable.
     */
    public final Property varEditedText(){
        return getProperty(PROPERTY_EDITED_TEXT);
    }

    public final void setValidator(Predicate text) {
        setPropertyValue(PROPERTY_VALIDATOR,text);
    }

    public final Predicate getValidator() {
        return (Predicate) getPropertyValue(PROPERTY_VALIDATOR);
    }

    /**
     * Get edited text variable.
     * @return Variable.
     */
    public final Property varValidator() {
        return getProperty(PROPERTY_VALIDATOR);
    }

    public final Property varEditionValid(){
        return getProperty(PROPERTY_EDITION_VALID);
    }

    private void setEditionValid(boolean valid) {
        setPropertyValue(PROPERTY_EDITION_VALID,valid);
    }

    public final boolean isEditionValid() {
        return (Boolean) getPropertyValue(PROPERTY_EDITION_VALID);
    }

    private boolean isEditedTextValid(){
        final Predicate validator = getValidator();
        return validator==null || validator.evaluate(getEditedText());
    }

    public final void setPreviewText(CharArray text) {
        setPropertyValue(PROPERTY_PREVIEW_TEXT,text);
    }

    public final CharArray getPreviewText() {
        return (CharArray) getPropertyValue(PROPERTY_PREVIEW_TEXT);
    }

    /**
     * Get preview text variable.
     * @return Variable.
     */
    public final Property varPreviewText(){
        return getProperty(PROPERTY_PREVIEW_TEXT);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_TEXT.equals(name)) {
            setEditedText((CharArray) value);
        } else if (PROPERTY_EDITED_TEXT.equals(name)) {
            //check if text is valid
            final boolean state = isEditedTextValid();
            setEditionValid(state);
            //if valid set the edited value as base text
            if (state) setText((CharArray) value);
        } else if (PROPERTY_VALIDATOR.equals(name)) {
            setEditionValid(isEditedTextValid());
        }

    }


}
