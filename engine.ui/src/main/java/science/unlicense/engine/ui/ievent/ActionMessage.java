
package science.unlicense.engine.ui.ievent;

import science.unlicense.common.api.event.DefaultEventMessage;
import science.unlicense.common.api.event.MessagePredicate;
import science.unlicense.common.api.predicate.Predicate;

/**
 * Action event message, often use on clickable widgets.
 *
 * @author Johann Sorel
 */
public class ActionMessage extends DefaultEventMessage {

    public static final Predicate PREDICATE = new MessagePredicate(ActionMessage.class);

    public ActionMessage() {
        super(true);
    }

}
