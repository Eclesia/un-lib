
package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.concurrent.api.Formats;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.StackConstraint;
import science.unlicense.display.api.layout.StackLayout;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.visual.ContainerView;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageResource;
import science.unlicense.image.api.Images;
import science.unlicense.media.api.Media;
import science.unlicense.model3d.api.SceneResource;

/**
 *
 * @author Johann Sorel
 */
public class WThumbnail extends WLabel {

    public static final Chars PROPERTY_SELECTED = Chars.constant("Selected");
    public static final Chars STYLECLASS = Chars.constant("thumbnail");

    private static final Extent THUMBNAIL_SIZE = new Extent.Double(128, 128);
    private static final Chars CACHE_FORMAT = Chars.constant("png");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLabel.CLASS_DEFAULTS);
        defs.add(PROPERTY_SELECTED, Boolean.FALSE);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, STYLECLASS);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private final Path path;
    private volatile boolean updatingImage = false;
    private final boolean useCache;
    private final boolean usePreview;
    private final Chars cacheFormat;
    private final Predicate filter;

    public WThumbnail(Path path, boolean useCache, Chars cacheFormat) {
        this(path, useCache, cacheFormat, true, true, null);
    }

    public WThumbnail(Path path, boolean useCache, Chars cacheFormat, boolean showText, boolean preview, Predicate filter) {
        setView(new PreviewView(this));
        if (cacheFormat==null) cacheFormat = CACHE_FORMAT;
        if (filter==null) filter = Predicate.TRUE;

        this.path = path;
        this.useCache = useCache;
        this.cacheFormat = cacheFormat;
        this.usePreview = preview;
        this.filter = filter;
        setVerticalAlignment(WLabel.VALIGN_CENTER);
        setHorizontalAlignment(WLabel.HALIGN_CENTER);
        setGraphicPlacement(WLabel.GRAPHIC_TOP);
        if (showText) {
            setText(new Chars(path.getName()));
        }
    }

    @Override
    protected void receiveEventParent(Event event) {
        if (!event.getMessage().isConsumed() && event.getMessage() instanceof MouseMessage) {
            final MouseMessage me = (MouseMessage) event.getMessage();
            if (me.getType() == MouseMessage.TYPE_TYPED) {
                me.consume();
                setSelected(!isSelected());
            }
        }
        super.receiveEventParent(event);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public boolean isSelected() {
        return (Boolean) getPropertyValue(PROPERTY_SELECTED);
    }

    public void setSelected(boolean selected) {
        setPropertyValue(PROPERTY_SELECTED, selected);
    }

    public Path getPath() {
        return path;
    }

    synchronized void updateImage() {
        if (updatingImage) {
            return;
        }
        updatingImage = true;
        new Thread() {
            public void run() {

                try {
                    if (path.isContainer()){

                        if (usePreview){
                            final WLeaf leaf = new WSpace(1,1);
                            leaf.setInlineStyle(WPathChooser.STYLE_FOLDER);
                            leaf.getInlineStyle().setProperties(new Chars("margin:3"));
                            final WContainer cnt = new WContainer(new StackLayout());
                            cnt.addChild(leaf, new StackConstraint(0));
                            setGraphic(cnt);

                            final Path p = searchMostRepresentative(path.getChildren());
                            if (p != null){
                                final Widget gra = createGraphic(p, useCache);
                                gra.setInlineStyle(new Chars("margin:[30,3,3,22]"));
                                cnt.addChild(gra, new StackConstraint(1));
                            }
                        } else {
                            final WLeaf leaf = new WSpace(16,16);
                            leaf.setInlineStyle(WPathChooser.STYLE_FOLDER);
                            leaf.getInlineStyle().setProperties(new Chars("margin:0"));
                            setGraphic(leaf);
                        }
                    } else {
                        if (usePreview){
                            setGraphic(createGraphic(path, useCache));
                        } else {
                            final WLeaf leaf = new WSpace(16,16);
                            leaf.setInlineStyle(WPathChooser.STYLE_FILE);
                            leaf.getInlineStyle().setProperties(new Chars("margin:0"));
                            setGraphic(leaf);
                        }
                    }
                } catch (IOException ex) {
                    Loggers.get().info(ex);
                }
            }
        }.start();
    }

    private Widget createGraphic(Path path, boolean useCache){

        //try to load from cache first
        Path thumbPath = null;
        Image img = null;
        if (useCache) {
            try {
                thumbPath = PathPresenters.createThumbnailPath(path);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try {
                img = Images.read(thumbPath);
            } catch (Exception ex) {
            }
        }

        generateImage:
        if (img == null) {
            //generate and save the thumbnail
            img = PathPresenters.generateImage(path, THUMBNAIL_SIZE);
            if (useCache && img != null) {
                try {
                    Images.write(img, cacheFormat, thumbPath);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (img==null){
            final WLeaf leaf = new WSpace(1,1);
            leaf.setInlineStyle(WPathChooser.STYLE_FILE);
            leaf.getInlineStyle().setProperties(new Chars("margin:3"));
            return leaf;
        } else {
            final WGraphicImage graphic = new WGraphicImage();
            Extents overrideExtents = graphic.getOverrideExtents();
            overrideExtents.minX = 1; overrideExtents.minY = 1;
            overrideExtents.maxX = Double.MAX_VALUE; overrideExtents.maxY = Double.MAX_VALUE;
            graphic.setOverrideExtents(overrideExtents);
            graphic.setFitting(WGraphicImage.FITTING_SCALED);
            graphic.setImage(img);
            return graphic;
        }

    }

    /**
     * Search the most representative path in the array.
     * This method checks if the paths are images, medias or 3d models.
     * Priority is given in the order :
     * - 3d model
     * - media
     * - image
     *
     * if multiple instances exist then the first one is used.
     *
     * @param paths
     * @return
     */
    private Path searchMostRepresentative(Collection paths) throws IOException{

        Path candidate = null;
        Chars extension;
        Format format;
        boolean isMediaFormat = false;
        boolean isImageFormat = false;

        final Iterator ite = paths.createIterator();
        while (ite.hasNext()) {
            final Path p = (Path) ite.next();
            if (p.isContainer() || !filter.evaluate(p) ) continue;
            extension = Paths.getExtension(new Chars(p.getName()));
            try{
                format = Formats.getFormatForExtension(extension);
            }catch(InvalidArgumentException ex){continue;}
            if (isModel3DFormat(format)) return p;
            else if (isMediaFormat(format) && !isMediaFormat){ candidate = p; isMediaFormat=true;}
            else if (isImageFormat(format) && !isImageFormat){ candidate = p; isImageFormat=true;}
        }
        return candidate;
    }

    private static boolean isImageFormat(Format format){
        return format.getResourceTypes().contains(ImageResource.class);
    }

    private static boolean isMediaFormat(Format format){
        return format.getResourceTypes().contains(Media.class);
    }

    private static boolean isModel3DFormat(Format format){
        return format.getResourceTypes().contains(SceneResource.class);
    }

    private final class PreviewView extends ContainerView{

        public PreviewView(WThumbnail labeled) {
            super(labeled);
        }

        protected void renderSelf(Painter2D painter, BBox innerBBox) {
            super.renderSelf(painter, innerBBox);
            if (!updatingImage){
                ((WThumbnail) getWidget()).updateImage();
            }
        }

    }
}
