
package science.unlicense.engine.ui.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.widget.WButton;

/**
 *
 * @author Johann Sorel
 */
public class WFoldButton extends WButton {

    public static final Chars PROPERTY_UNFOLD = Chars.constant("UnFold");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WButton.CLASS_DEFAULTS);
        defs.add(PROPERTY_UNFOLD, Boolean.FALSE);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("foldbutton")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private final TreeRowModel treeModel;
    private final Node node;

    public WFoldButton(TreeRowModel treeModel, Node node) {
        this.treeModel = treeModel;
        this.node = node;
        setOverrideExtents(new Extents(14, 13));
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public void doClick() {
        super.doClick();
        boolean foldState = !treeModel.isUnfold(node);
        treeModel.setFold(node, !treeModel.isUnfold(node));
        sendPropertyEvent(PROPERTY_UNFOLD, foldState, !foldState);
    }

    public boolean isUnFold() {
        return (Boolean) getPropertyValue(PROPERTY_UNFOLD);
    }

}
