
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.visual.ScrollBarView;

/**
 *
 * @author Johann Sorel
 */
public class WScrollBar extends WContainer{

    public static final Chars DIRECTION     = Chars.constant("direction");
    public static final Chars FLAG_TOP      = Chars.constant("scrollbar-top");
    public static final Chars FLAG_BOTTOM   = Chars.constant("scrollbar-bottom");
    public static final Chars FLAG_LEFT     = Chars.constant("scrollbar-left");
    public static final Chars FLAG_RIGHT    = Chars.constant("scrollbar-right");

    public static final Chars STYPE_PROP_CURSOR_PAINT = Chars.constant("cursor-paint");

    public static final Chars PROPERTY_HORIZONTAL = Chars.constant("Horizontal");
    public static final Chars PROPERTY_RATIO = Chars.constant("Ratio");
    public static final Chars PROPERTY_CURSORSIZE = Chars.constant("CursorSize");
    public static final Chars PROPERTY_STEP = Chars.constant("Step");
    public static final Chars PROPERTY_HANDLEVISIBLE = Chars.constant("HandleVisible");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(PROPERTY_HANDLEVISIBLE, Boolean.FALSE);
        defs.add(PROPERTY_HORIZONTAL, Boolean.TRUE);
        defs.add(PROPERTY_RATIO, 0.0);
        defs.add(PROPERTY_CURSORSIZE, 0.0);
        defs.add(PROPERTY_STEP, 0.1);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("scrollbar")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }
    protected static final Dictionary SCROLLCLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLeaf.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("scroller")));
        SCROLLCLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private final WAction corner1 = new WAction(null,null,new EventListener() {
        public void receiveEvent(Event event) {
            double d = getRatio()-getStep();
            if (d<0) d=0;
            setRatio(d);
        }
    });
    private final WAction corner2 = new WAction(null,null,new EventListener() {
        public void receiveEvent(Event event) {
            double d = getRatio()+getStep();
            if (d>1) d=1;
            setRatio(d);
        }
    });
    private final Scroll scroll = new Scroll();

    public WScrollBar() {
        this(false);
    }

    public WScrollBar(boolean horizontal) {
        setPropertyValue(PROPERTY_HORIZONTAL, horizontal);
        final BorderLayout layout = new BorderLayout();
        setLayout(layout);
        children.add(scroll);

        updateLayout();
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    private void updateLayout(){
        if (isHorizontal()){
            corner1.setPropertyValue(XPROP_STYLEMARKER, FLAG_LEFT);
            corner2.setPropertyValue(XPROP_STYLEMARKER, FLAG_RIGHT);
            corner1.setLayoutConstraint(BorderConstraint.LEFT);
            scroll.setLayoutConstraint(BorderConstraint.CENTER);
            corner2.setLayoutConstraint(BorderConstraint.RIGHT);
        } else {
            corner1.setPropertyValue(XPROP_STYLEMARKER, FLAG_TOP);
            corner2.setPropertyValue(XPROP_STYLEMARKER, FLAG_BOTTOM);
            corner1.setLayoutConstraint(BorderConstraint.TOP);
            scroll.setLayoutConstraint(BorderConstraint.CENTER);
            corner2.setLayoutConstraint(BorderConstraint.BOTTOM);
        }
    }

    public void setRatio(double ratio) {
        final double oldRatio = getRatio();
        if (setPropertyValue(PROPERTY_RATIO, ratio)){
            scroll.sendRatioEvent(oldRatio, ratio);
        }
    }

    public double getRatio() {
        return (Double) getPropertyValue(PROPERTY_RATIO);
    }

    public Property varRatio() {
        return getProperty(PROPERTY_RATIO);
    }

    public void setStep(double step) {
        setPropertyValue(PROPERTY_STEP, step);
    }

    public double getStep() {
        return (Double) getPropertyValue(PROPERTY_STEP);
    }

    public Property varStep() {
        return getProperty(PROPERTY_STEP);
    }

    public void setHorizontal(boolean horizontal) {
        if (setPropertyValue(PROPERTY_HORIZONTAL, horizontal)){
            updateLayout();
        }
    }

    public boolean isHorizontal() {
        return (Boolean) getPropertyValue(PROPERTY_HORIZONTAL);
    }

    public void setCursorSize(double cursorSize) {
        setPropertyValue(PROPERTY_CURSORSIZE, cursorSize);
    }

    public double getCursorSize() {
        return (Double) getPropertyValue(PROPERTY_CURSORSIZE);
    }

    public void setHandleVisible(boolean visible) {
        if (setPropertyValue(PROPERTY_HANDLEVISIBLE, visible)) {
            if (visible) {
                children.add(corner1);
                children.add(corner2);
            } else {
                children.remove(corner1);
                children.remove(corner2);
            }
        }
    }

    public boolean isHandleVisible() {
        return Boolean.TRUE.equals(getPropertyValue(PROPERTY_HANDLEVISIBLE));
    }

    public class Scroll extends WLeaf{

        public Scroll() {
            setView(new ScrollBarView(this));
        }

        @Override
        protected Dictionary getClassDefaultProperties() {
            return SCROLLCLASS_DEFAULTS;
        }

        private void sendRatioEvent(Object oldValue, Object newValue) {
            Scroll.this.sendPropertyEvent(PROPERTY_RATIO, oldValue, newValue);
        }

        public double getRatio() {
            return WScrollBar.this.getRatio();
        }

        public WScrollBar getScrollBar(){
            return WScrollBar.this;
        }

    }

}
