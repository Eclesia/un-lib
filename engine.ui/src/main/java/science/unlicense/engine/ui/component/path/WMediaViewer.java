

package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.anim.AbstractAnimation;
import science.unlicense.display.api.anim.Animation;
import science.unlicense.display.api.anim.DefaultTimer;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Resources;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.transform.Projections;
import science.unlicense.geometry.impl.transform.ViewTransform;
import science.unlicense.image.api.Image;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.Angles;
import science.unlicense.media.api.ImagePacket;
import science.unlicense.media.api.ImageTrack;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.Medias;
import science.unlicense.media.api.Track;

/**
 *
 * @author Johann Sorel
 */
public class WMediaViewer extends WContainer {

    private final Path path;

    //cache all video images
    //TODO must improve this
    private final Sequence times = new ArraySequence();
    private final Sequence textures = new ArraySequence();
    private Extent.Long extent;

    private final WButtonBar bar = new WButtonBar();
    private final WGraphicImage imgGraphic = new WGraphicImage();
    private final WContainer meta = new WContainer();
    private final ViewTransform view = new ViewTransform();
    private boolean userAction = false;

    //user actions
    private final double[] mvCoord = new double[]{Double.NaN, Double.NaN};
    private boolean pressed = false;
    private boolean rotate = false;

    public WMediaViewer(Path path) throws IOException, StoreException {
        this.path = path;

        setLayout(new BorderLayout());

        bar.addChild(new WButton(new Chars("+")), null);
        bar.addChild(new WButton(new Chars("-")), null);
        bar.addChild(new WButton(new Chars("o")), null);

        WContainer c = new WContainer();
        c.setLayout(new AbsoluteLayout());
        c.addChild(imgGraphic, null);
        c.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final MouseMessage message = (MouseMessage) event.getMessage();
                final int type = message.getType();
                final double[] m = message.getMousePosition().toDouble();
                final int button = message.getButton();

                if (type == MouseMessage.TYPE_WHEEL) {
                    if (rotate) {
                        final double angle = Angles.degreeToRadian(message.getWheelOffset());
                        view.rotateAt(angle, message.getMousePosition().toDouble());
                    } else {
                        final double scale = 1 + message.getWheelOffset()*0.1;
                        view.scaleAt(scale, message.getMousePosition().toDouble());
                    }
                    imgGraphic.getNodeTransform().set(view.getSourceToTarget());
                } else if (type == MouseMessage.TYPE_MOVE) {
                    if (pressed){
                        final double[] diff = new double[]{
                            m[0]-mvCoord[0],
                            m[1]-mvCoord[1]
                        };
                        view.translate(diff);
                        imgGraphic.getNodeTransform().set(view.getSourceToTarget());
                    }
                    mvCoord[0] = m[0];
                    mvCoord[1] = m[1];
                } else if (type == MouseMessage.TYPE_PRESS && button == MouseMessage.BUTTON_1) {
                    pressed = true;
                } else if (type == MouseMessage.TYPE_RELEASE && button == MouseMessage.BUTTON_1) {
                    pressed = false;
                } else if (type == MouseMessage.TYPE_EXIT) {
                    pressed = false;
                } else if (type == MouseMessage.TYPE_ENTER) {
                    c.requestFocus();
                }
            }
        });
        c.addEventListener(KeyMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final KeyMessage message = (KeyMessage) event.getMessage();
                final int type = message.getType();
                final int button = message.getCode();
                if (type == KeyMessage.TYPE_PRESS && button == KeyMessage.KC_CONTROL) {
                    rotate = true;
                } else if (type == KeyMessage.TYPE_RELEASE && button == KeyMessage.KC_CONTROL) {
                    rotate = false;
                }

            }
        });

        addChild(c, BorderConstraint.CENTER);
        addChild(meta, BorderConstraint.RIGHT);
        addChild(bar, BorderConstraint.BOTTOM);


        //open a video media
        final Store store = Medias.open(path);
        final Media media = (Media) Resources.findFirst(store, Media.class);
        if (media == null) {
            throw new StoreException("No media found");
        }

        final Track[] metas = media.getTracks();
        //find video stream
        int videoStreamIndex = -1;
        double fr = 50;
        for (int i=0;i<metas.length;i++) {
            if (metas[i] instanceof ImageTrack) {
                ImageTrack m = (ImageTrack) metas[i];
                videoStreamIndex = i;
                fr = m.getFrameRate();
            }
        }
        final double frameRate = fr;

        final MediaReadParameters mediaParams = new MediaReadParameters();
        mediaParams.setStreamIndexes(new int[]{videoStreamIndex});
        final MediaReadStream reader = media.createReader(mediaParams);

        try{
            for (MediaPacket pack=reader.next();pack!=null;pack=reader.next()){
                final Image mi = ((ImagePacket) pack).getImage();
                extent = mi.getExtent();
                times.add(pack.getStartTime());
                textures.add(mi);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }

        final double totalLength = frameRate*times.getSize();

        Animation anim = new AbstractAnimation() {
            @Override
            public double getLength() {
                return totalLength;
            }

            @Override
            public void update() {
                final int index = (int) (getTime()/frameRate);
                final Object img = textures.get(index);
                imgGraphic.setImage((Image) img);
            }
        };
        anim.setRepeatCount(0);
        anim.setTimer(new DefaultTimer((int) fr));
        anim.start();

    }

    @Override
    public void setEffectiveExtent(Extent extent) {
        super.setEffectiveExtent(extent);

        if (!userAction && this.extent != null) {
            final double scalex = extent.get(0) / this.extent.get(0);
            final double scaley = extent.get(1) / this.extent.get(1);
            final Extent ext;
            if (scalex<scaley){
                ext = new Extent.Double(extent.get(0), this.extent.get(1)*scalex);
            } else {
                ext = new Extent.Double(this.extent.get(0)*scaley, extent.get(1));
            }
            AffineRW t = Projections.scaled(new BBox(this.extent), new BBox(ext));
            view.getSourceToTarget().set(t);
        }

    }

}
