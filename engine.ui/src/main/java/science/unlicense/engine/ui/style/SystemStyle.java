
package science.unlicense.engine.ui.style;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.engine.ui.io.RSReader;

/**
 * Global style informations defining system main colors and font.
 *
 * @author Johann Sorel
 */
public final class SystemStyle {

    private static final Chars BASIC = new Chars(
"{\n" +
"    // default widget theme styles\n" +
"    // @author Johann Sorel\n" +
"\n" +
"\n" +
"    // base system, commun to all elements\n" +
"    // main theme color\n" +
"    // those colors are the base colors from which others are derivated\n" +
"    // background : is use for container, large flat areas\n" +
"    // transparent : is use for areas which should be mostly transparent\n" +
"    // main : is use as primary color\n" +
"    // focus : is use as a secondary color\n" +
"    // text : is use for text on global elements like buttons or labels\n" +
"    // textarea-* : is use for text areas such as textfields, textareas or spinners\n" +
"    // delimiter : is use for tree nodes and separators\n" +
"    color-background : rgba(240 240 240 255)\n" +
"    color-transparent: rgba(  0   0   0   0)\n" +
"    color-main       : rgba(145 145 145 255)\n" +
"    color-focus      : rgba(135 165 200 255)\n" +
"    color-error      : rgba(255 150 150 255)\n" +
"    color-text       : rgba( 50  50  50 255)\n" +
"    color-disable    : rgba(170 170 170 255)\n" +
"    color-textarea-background : rgba(255 255 255 255)\n" +
"    color-textarea-text       : rgba(50 50 50 255)\n" +
"    color-delimiter  : rgba(200 200 200 200)\n" +
"\n" +
"    arrow-left  : \"'POLYGON((20% 50%, 80% 0%, 80% 100%, 20% 50%))'\"\n" +
"    arrow-right : \"'POLYGON((80% 50%, 20% 100%, 20% 0%, 80% 50%))'\"\n" +
"    arrow-up    : \"'POLYGON((50% 20%, 100% 80%, 0% 80%, 50% 20%))'\"\n" +
"    arrow-down  : \"'POLYGON((0% 20%, 100% 20%, 50% 80%, 0% 20%))'\"\n" +
"\n" +
"    font-size : 14.5\n" +
"    font : {\n" +
"        family : font('Mona' 1em 'none')\n" +
"        fill-paint   : @color-text\n" +
"    }\n" +
"    text-sample : 'AqApgBAqApgBAqA'\n" +
"\n" +
"\n" +
"    WGraphicText : {\n" +
"        filter : $StyleClass = 'graphictext'\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            font   : {\n" +
"                fill-paint : @color-disable\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WButton : {\n" +
"        filter : $StyleClass = 'button'\n" +
"        margin : [5,10,5,10]\n" +
"        graphic : {\n" +
"            radius       : 3\n" +
"            margin       : [4,9,4,9]\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : lineargradientfill('%' 0 0 0 1 0 rotateHSL(@color-background 0 1 1.05) 1 rotateHSL(@color-background 0 1 0.95))\n" +
"        }\n" +
"\n" +
"        MouseOver : {\n" +
"            filter : $MouseOver\n" +
"            graphic : {\n" +
"                brush-paint  : @color-focus\n" +
"                fill-paint   : rotateHSL(@color-background 0 1 1.1)\n" +
"            }\n" +
"        }\n" +
"\n" +
"        MousePress : {\n" +
"            filter : $MousePress\n" +
"            graphic : {\n" +
"                brush-paint  : @color-focus\n" +
"                fill-paint   : rotateHSL(@color-background 0 1 0.9)\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Focused : {\n" +
"            filter : $Focused\n" +
"            graphic : {\n" +
"                brush        : plainbrush(2 'round')\n" +
"                brush-paint  : @color-focus\n" +
"            }\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            graphic : {\n" +
"                fill-paint : @color-background\n" +
"                brush-paint  : @color-disable\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WCheckBox : {\n" +
"        filter : $StyleClass = 'checkbox'\n" +
"        margin : 5\n" +
"\n" +
"        graphic.0 : {\n" +
"            margin       : 3\n" +
"            radius       : 3\n" +
"            fill-paint   : @color-textarea-background\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            zorder       : -2\n" +
"        }\n" +
"        graphic.1 : {\n" +
"            margin       : 0\n" +
"            shape        : \"'POLYGON((40 60, 95 5, 95 20, 40 95, 5 55, 5 45, 40 60))'\"\n" +
"            transform    : scaleTransform()\n" +
"            zorder       : -1\n" +
"        }\n" +
"\n" +
"        Check : {\n" +
"            filter    : $Check\n" +
"            graphic.1 : {\n" +
"                fill-paint : @color-focus\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Focused : {\n" +
"            filter : $Focused\n" +
"            graphic.0 : {\n" +
"                brush        : plainbrush(2 'round')\n" +
"                brush-paint  : @color-focus\n" +
"            }\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            graphic.0 : {\n" +
"                brush-paint : @color-disable\n" +
"            }\n" +
"            graphic.1 : {\n" +
"                fill-paint : @color-disable\n" +
"            }\n" +
"        }\n" +
"\n" +
"    }\n" +
"\n" +
"    WTextField : {\n" +
"        filter : $StyleClass = 'textfield'\n" +
"        margin : [5,6,5,6]\n" +
"        graphic : {\n" +
"            margin       : [3,5,3,5]\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-textarea-background\n" +
"        }\n" +
"        font : {\n" +
"            fill-paint   : @color-textarea-text\n" +
"        }\n" +
"\n" +
"        Focused : {\n" +
"            filter : $Focused\n" +
"            graphic : {\n" +
"                brush        : plainbrush(1 'round')\n" +
"                brush-paint  : @color-focus\n" +
"            }\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            graphic : {\n" +
"                fill-paint : @color-background\n" +
"                brush-paint  : @color-disable\n" +
"            }\n" +
"            font : {\n" +
"                fill-paint   : @color-disable\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Valid : {\n" +
"            filter : $EditionValid=false\n" +
"            graphic : {\n" +
"                fill-paint   : @color-error\n" +
"                brush-paint  : rotateHSL(@color-error 0 1 0.9)\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WTextArea : {\n" +
"        filter : $StyleClass = 'textarea'\n" +
"        margin : [5,6,5,6]\n" +
"        graphic : {\n" +
"            margin       : [3,5,3,5]\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-textarea-background\n" +
"        }\n" +
"        font : {\n" +
"            fill-paint   : @color-textarea-text\n" +
"        }\n" +
"\n" +
"        Focused : {\n" +
"            filter : $Focused\n" +
"            graphic : {\n" +
"                brush        : plainbrush(1 'round')\n" +
"                brush-paint  : @color-focus\n" +
"            }\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            graphic : {\n" +
"                fill-paint : @color-background\n" +
"                brush-paint  : @color-disable\n" +
"            }\n" +
"            font : {\n" +
"                fill-paint   : @color-disable\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Valid : {\n" +
"            filter : $TextValid=false\n" +
"            graphic : {\n" +
"                fill-paint   : @color-error\n" +
"                brush-paint  : rotateHSL(@color-error 0 1 0.9)\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WSlider : {\n" +
"        filter : $StyleClass = 'slider'\n" +
"        margin : 4\n" +
"        graphic : {\n" +
"            radius       : 3\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-textarea-background\n" +
"            margin       : 3\n" +
"        }\n" +
"        font : {\n" +
"            fill-paint   : @color-textarea-text\n" +
"        }\n" +
"        marker : {\n" +
"            radius       : 5\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-main\n" +
"            margin       : -3\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            graphic : {\n" +
"                fill-paint   : rotateHSL(@color-textarea-background 0 1 0.5)\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Focused : {\n" +
"            filter : $Focused\n" +
"            graphic : {\n" +
"                brush        : plainbrush(1 'round')\n" +
"                brush-paint  : @color-focus\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WRangeSlider : {\n" +
"        filter : $StyleClass = 'rangeslider'\n" +
"        margin : 4\n" +
"        graphic : {\n" +
"            radius       : 3\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-textarea-background\n" +
"            margin       : 3\n" +
"        }\n" +
"        font : {\n" +
"            fill-paint   : @color-textarea-text\n" +
"        }\n" +
"        marker : {\n" +
"            radius       : 5\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-main\n" +
"            margin       : -3\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            graphic : {\n" +
"                fill-paint   : rotateHSL(@color-textarea-background 0 1 0.5)\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Focused : {\n" +
"            filter : $Focused\n" +
"            graphic : {\n" +
"                brush        : plainbrush(1 'round')\n" +
"                brush-paint  : @color-focus\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WTupleField : {\n" +
"        filter : $StyleClass = 'tuplefield'\n" +
"        margin : 4\n" +
"        graphic : {\n" +
"            radius       : 3\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-textarea-background\n" +
"            margin       : 3\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            graphic : {\n" +
"                fill-paint   : rotateHSL(@color-textarea-background 0 1 0.5)\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Focused : {\n" +
"            filter : $Focused\n" +
"            graphic : {\n" +
"                brush        : plainbrush(2 'round')\n" +
"                brush-paint  : @color-focus\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WMatrixField : {\n" +
"        filter : $StyleClass = 'matrixfield'\n" +
"        margin : 4\n" +
"        graphic : {\n" +
"            radius       : 3\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-textarea-background\n" +
"            margin       : 3\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            graphic : {\n" +
"                fill-paint   : rotateHSL(@color-textarea-background 0 1 0.5)\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Focused : {\n" +
"            filter : $Focused\n" +
"            graphic : {\n" +
"                brush        : plainbrush(2 'round')\n" +
"                brush-paint  : @color-focus\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WContainer : {\n" +
"        filter : $StyleClass = 'container'\n" +
"    }\n" +
"\n" +
"    WButtonBar : {\n" +
"        filter : $StyleClass = 'buttonbar'\n" +
"        margin  : [3,4,3,4]\n" +
"        background : {\n" +
"            fill-paint   : lineargradientfill('%' 0 0 0 1 0 rotateHSL(@color-background 0 1 1.05) 1 rotateHSL(@color-background 0 1 0.95))\n" +
"        }\n" +
"    }\n" +
"\n" +
"\n" +
"    WGraphicGeometry : {\n" +
"        filter : $StyleClass = 'graphicgeometry'\n" +
"        geom:{\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : rotateHSL(@color-focus 0 1 0.5)\n" +
"            fill-paint   : @color-focus\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            geom:{\n" +
"                brush-paint  : rotateHSL(@color-main 0 1 0.5)\n" +
"                fill-paint   : @color-main\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WLabel : {\n" +
"        filter : $StyleClass = 'label'\n" +
"        margin : 5\n" +
"        font   : {\n" +
"            fill-paint : @color-text\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            font : {\n" +
"                fill-paint : @color-disable\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WMenuButton : {\n" +
"        filter : $StyleClass = 'menubutton'\n" +
"        margin : [2,6,2,6]\n" +
"        graphic : {\n" +
"            radius       : 0\n" +
"            margin       : [2,6,2,6]\n" +
"            brush        : plainbrush(0 'round')\n" +
"            fill-paint   : none\n" +
"            brush-paint  : none\n" +
"        }\n" +
"\n" +
"        MouseOver : {\n" +
"            filter : $MouseOver\n" +
"            graphic : {\n" +
"                fill-paint   : @color-focus\n" +
"            }\n" +
"        }\n" +
"\n" +
"        MousePress : {\n" +
"            filter : $MousePress\n" +
"            graphic : {\n" +
"                fill-paint   : @color-main\n" +
"            }\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            font : {\n" +
"                fill-paint : @color-main\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WSwitch : {\n" +
"        filter : $StyleClass = 'switch'\n" +
"        margin : [5,10,5,10]\n" +
"        graphic : {\n" +
"            radius       : 3\n" +
"            margin       : [4,9,4,9]\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : rotateHSL(@color-background 0 1 1.05)\n" +
"        }\n" +
"\n" +
"        MouseOver : {\n" +
"            filter : $MouseOver\n" +
"            graphic : {\n" +
"                brush-paint  : @color-focus\n" +
"                fill-paint   : rotateHSL(@color-background 0 1 1.1)\n" +
"            }\n" +
"        }\n" +
"\n" +
"        MousePress : {\n" +
"            filter : $MousePress\n" +
"            graphic : {\n" +
"                brush-paint  : @color-focus\n" +
"                fill-paint   : rotateHSL(@color-background 0 1 0.9)\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Check : {\n" +
"            filter : $Check\n" +
"            graphic : {\n" +
"                fill-paint : @color-focus\n" +
"            }\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            graphic : {\n" +
"                fill-paint : @color-background\n" +
"                brush-paint  : @color-disable\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WMenuSwitch : {\n" +
"        filter : $StyleClass = 'menuswitch'\n" +
"        margin : [2,6,2,6]\n" +
"        graphic : {\n" +
"            radius       : 0\n" +
"            margin       : [2,6,2,6]\n" +
"            brush        : plainbrush(0 'round')\n" +
"            fill-paint   : none\n" +
"            brush-paint  : none\n" +
"        }\n" +
"\n" +
"        MouseOver : {\n" +
"            filter : $MouseOver\n" +
"            graphic : {\n" +
"                fill-paint : rotateHSL(@color-main 0 1 1.9)\n" +
"            }\n" +
"        }\n" +
"\n" +
"        MousePress : {\n" +
"            filter : $MousePress\n" +
"            graphic : {\n" +
"                fill-paint : @color-main\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Check : {\n" +
"            filter : $Check\n" +
"            graphic : {\n" +
"                fill-paint   : @color-focus\n" +
"            }\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            font : {\n" +
"                fill-paint : @color-main\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WProgressBar : {\n" +
"        filter : $StyleClass = 'progressbar'\n" +
"        graphic : {\n" +
"            radius       : 4\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-background\n" +
"        }\n" +
"\n" +
"        progress : {\n" +
"            radius       : 4\n" +
"            fill-paint   : @color-focus\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            graphic : {\n" +
"                fill-paint   : @color-main\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WSeparator : {\n" +
"        filter : $StyleClass = 'separator'\n" +
"        graphic.0 : {\n" +
"            brush       : plainbrush(1 'round')\n" +
"            brush-paint : @color-main\n" +
"        }\n" +
"\n" +
"        Horizontal : {\n" +
"            filter : $Horizontal\n" +
"            graphic.0 : {\n" +
"                shape : \"'LINESTRING(5% 50%, 95% 50%)'\"\n" +
"            }\n" +
"        }\n" +
"        Vertical : {\n" +
"            filter : $Horizontal=false\n" +
"            graphic.0 : {\n" +
"                shape : \"'LINESTRING(50% 5%, 50% 95%)'\"\n" +
"            }\n" +
"        }\n" +
"\n" +
"    }\n" +
"\n" +
"    WScrollContainer : {\n" +
"        filter : $StyleClass = 'scrollcontainer'\n" +
"        graphic.0 : {\n" +
"            brush         : plainbrush(0.5 'butt')\n" +
"            brush-paint   : @color-main\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WScrollBar : {\n" +
"        filter : $StyleClass = 'scrollbar'\n" +
"        margin : 1\n" +
"        graphic : {\n" +
"            radius       : 0\n" +
"            brush        : plainbrush(0 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-textarea-background\n" +
"            margin       : 0\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WScrollBarScroll : {\n" +
"        filter : $StyleClass = 'scroller'\n" +
"        margin : 2\n" +
"    }\n" +
"\n" +
"\n" +
"    WScrollCornerTop : {\n" +
"        filter : $StyleMarker = 'scrollbar-top'\n" +
"        margin : [6,5,12,5]\n" +
"        graphic.0 : {\n" +
"            margin : [6,4,6,4]\n" +
"            fill-paint : @color-main\n" +
"            shape: @arrow-up\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WScrollCornerBottom : {\n" +
"        filter : $StyleMarker = 'scrollbar-bottom'\n" +
"        margin : [12,5,6,5]\n" +
"        graphic.0 : {\n" +
"            margin : [6,4,6,4]\n" +
"            fill-paint : @color-main\n" +
"            shape: @arrow-down\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WScrollCornerLeft : {\n" +
"        filter : $StyleMarker = 'scrollbar-left'\n" +
"        margin : [5,12,5,6]\n" +
"        graphic.0 : {\n" +
"            margin : [4,6,4,6]\n" +
"            fill-paint : @color-main\n" +
"            shape: @arrow-left\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WScrollCornerRight : {\n" +
"        filter : $StyleMarker = 'scrollbar-right'\n" +
"        margin : [5,6,5,12]\n" +
"        graphic.0 : {\n" +
"            margin : [4,6,4,6]\n" +
"            fill-paint : @color-main\n" +
"            shape: @arrow-right\n" +
"        }\n" +
"    }\n" +
"\n" +
"\n" +
"    AbstractControlWidget : {\n" +
"        filter : $StyleClass = 'controlwidget'\n" +
"        margin : 4\n" +
"        graphic : {\n" +
"            radius       : 3\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-textarea-background\n" +
"            margin       : 3\n" +
"        }\n" +
"        font : {\n" +
"            fill-paint   : @color-textarea-text\n" +
"        }\n" +
"\n" +
"        StackEnable : {\n" +
"            filter : $StackEnable=false\n" +
"            graphic : {\n" +
"                fill-paint   : rotateHSL(@color-textarea-background 0 1 0.5)\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Focused : {\n" +
"            filter : $ChildFocused\n" +
"            graphic : {\n" +
"                brush        : plainbrush(2 'round')\n" +
"                brush-paint  : @color-focus\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Valid : {\n" +
"            filter : $EditionValid=false\n" +
"            graphic : {\n" +
"                fill-paint   : @color-error\n" +
"                brush-paint  : rotateHSL(@color-error 0 1 0.9)\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WSpinner-UpButton : {\n" +
"        filter : $StyleMarker = 'WSpinner-UpButton'\n" +
"        margin : [4,6,4,6]\n" +
"        graphic.0 : {\n" +
"            margin : [4.5,5,4.5,5]\n" +
"            fill-paint : @color-main\n" +
"            shape: @arrow-up\n" +
"            transform : scaleTransform()\n" +
"        }\n" +
"\n" +
"        MouseOver : {\n" +
"            filter : $MouseOver\n" +
"            graphic : {\n" +
"                fill-paint : @color-focus\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WSpinner-DownButton : {\n" +
"        filter : $StyleMarker = 'WSpinner-DownButton'\n" +
"        margin : [4,6,4,6]\n" +
"        graphic.0 : {\n" +
"            margin : [4.5,5,4.5,5]\n" +
"            fill-paint : @color-main\n" +
"            shape: @arrow-down\n" +
"            transform : scaleTransform()\n" +
"        }\n" +
"\n" +
"        MouseOver : {\n" +
"            filter : $MouseOver\n" +
"            graphic : {\n" +
"                fill-paint : @color-focus\n" +
"            }\n" +
"        }\n" +
"\n" +
"    }\n" +
"\n" +
"    WSpinner-NumberEditor : {\n" +
"        filter : $StyleMarker = 'WSpinner-NumberEditor'\n" +
"        graphic : none\n" +
"        margin : none\n" +
"    }\n" +
"\n" +
"    WSpinner-LabelEditor : {\n" +
"        filter : $StyleMarker = 'WSpinner-LabelEditor'\n" +
"    }\n" +
"\n" +
"    WDateChooser-Button : {\n" +
"        filter : $StyleMarker = 'WDateChooser-Button'\n" +
"        margin : 8\n" +
"        graphic.0 : {\n" +
"            margin : 8\n" +
"            fill-paint : @color-main\n" +
"            shape: glyph('Unicon' '\\uE033' false)\n" +
"            transform : scaleTransform()\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WSelect-Button : {\n" +
"        filter : $StyleMarker = 'WSelect-DownButton'\n" +
"        margin : 8\n" +
"        graphic.0 : {\n" +
"            margin : 6\n" +
"            fill-paint : @color-main\n" +
"            shape: @arrow-down\n" +
"            transform : scaleTransform()\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WPathField-Editor : {\n" +
"        filter : $StyleMarker = 'WPathField-Editor'\n" +
"        graphic : none\n" +
"        margin : none\n" +
"    }\n" +
"    WPathField-Button : {\n" +
"        filter : $StyleMarker = 'WPathField-Button'\n" +
"        margin : 8\n" +
"        graphic.0 : {\n" +
"            margin : 8\n" +
"            fill-paint : @color-main\n" +
"            shape: glyph('Unicon' '\\uE017' false)\n" +
"            transform : scaleTransform()\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WSplitter : {\n" +
"        filter : $StyleMarker = 'SplitSeparator'\n" +
"        background : {\n" +
"            fill-paint : rotateHSL(@color-main 0 1 1.3)\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WTabContainer : {\n" +
"        filter : $StyleClass = 'tabcontainer'\n" +
"    }\n" +
"\n" +
"    WTabBar : {\n" +
"        filter : $StyleClass = 'tabbar'\n" +
"        background : {\n" +
"            fill-paint : @color-background\n" +
"        }\n" +
"        TabPos0 : {\n" +
"            filter : $TabPosition=0\n" +
"            graphic : {\n" +
"                shape       : \"'POLYGON((0% 100%, 100% 100%, 0% 100%))'\"\n" +
"                zorder      : -1\n" +
"                brush       : plainbrush(2 'butt')\n" +
"                brush-paint : @color-main\n" +
"            }\n" +
"        }\n" +
"        TabPos1 : {\n" +
"            filter : $TabPosition=1\n" +
"            graphic : {\n" +
"                shape       : \"'POLYGON((0% 0%, 100% 0%, 0% 0%))'\"\n" +
"                zorder      : -1\n" +
"                brush       : plainbrush(2 'butt')\n" +
"                brush-paint : @color-main\n" +
"            }\n" +
"        }\n" +
"        TabPos2 : {\n" +
"            filter : $TabPosition=2\n" +
"            graphic : {\n" +
"                shape       : \"'POLYGON((100% 0%, 100% 100%, 100% 0%))'\"\n" +
"                zorder      : -1\n" +
"                brush       : plainbrush(2 'butt')\n" +
"                brush-paint : @color-main\n" +
"            }\n" +
"        }\n" +
"        TabPos3 : {\n" +
"            filter : $TabPosition=3\n" +
"            graphic : {\n" +
"                shape       : \"'POLYGON((0% 0%, 0% 100%, 0% 0%))'\"\n" +
"                zorder      : -1\n" +
"                brush       : plainbrush(2 'butt')\n" +
"                brush-paint : @color-main\n" +
"            }\n" +
"        }\n" +
"\n" +
"    }\n" +
"\n" +
"    WTab : {\n" +
"        filter : $StyleClass = 'tab'\n" +
"\n" +
"        TabPos0 : {\n" +
"            filter : $TabPosition=0\n" +
"            margin : [8,10,8,10]\n" +
"            graphic.0 : {\n" +
"                margin      : [5,8,8,8]\n" +
"                zorder      : -3\n" +
"            }\n" +
"            graphic.1 : {\n" +
"                shape        : \"'POLYGON((0% 100%, 100% 100%, 0% 100%))'\"\n" +
"                margin       : [5,8,5,8]\n" +
"                brush        : plainbrush(4 'butt')\n" +
"                zorder       : -2\n" +
"            }\n" +
"            graphic.2 : {\n" +
"                shape       : \"'POLYGON((100% 0%, 100% 100%, 100% 0%))'\"\n" +
"                margin      : [0,9,0,9]\n" +
"                zorder      : -1\n" +
"                brush       : plainbrush(1 'butt')\n" +
"                brush-paint : @color-main\n" +
"            }\n" +
"        }\n" +
"        TabPos1 : {\n" +
"            filter : $TabPosition=1\n" +
"            margin : [8,10,8,10]\n" +
"            graphic.0 : {\n" +
"                margin      : [8,8,5,8]\n" +
"                zorder      : -3\n" +
"            }\n" +
"            graphic.1 : {\n" +
"                shape        : \"'POLYGON((0% 0%, 100% 0%, 0% 0%))'\"\n" +
"                margin       : [5,8,5,8]\n" +
"                brush        : plainbrush(4 'butt')\n" +
"                zorder       : -2\n" +
"            }\n" +
"            graphic.2 : {\n" +
"                shape       : \"'POLYGON((100% 0%, 100% 100%, 100% 0%))'\"\n" +
"                margin      : [0,9,0,9]\n" +
"                zorder      : -1\n" +
"                brush       : plainbrush(1 'butt')\n" +
"                brush-paint : @color-main\n" +
"            }\n" +
"        }\n" +
"        TabPos2 : {\n" +
"            filter : $TabPosition=2\n" +
"            margin : [10,8,10,8]\n" +
"            graphic.0 : {\n" +
"                margin      : [8,8,8,5]\n" +
"                zorder      : -3\n" +
"            }\n" +
"            graphic.1 : {\n" +
"                shape        : \"'POLYGON((100% 0%, 100% 100%, 100% 0%))'\"\n" +
"                margin       : [8,5,8,5]\n" +
"                brush        : plainbrush(4 'butt')\n" +
"                zorder       : -2\n" +
"            }\n" +
"            graphic.2 : {\n" +
"                shape       : \"'POLYGON((0% 100%, 100% 100%, 0% 100%))'\"\n" +
"                margin      : [9,0,9,0]\n" +
"                zorder      : -1\n" +
"                brush       : plainbrush(1 'butt')\n" +
"                brush-paint : @color-main\n" +
"            }\n" +
"        }\n" +
"        TabPos3 : {\n" +
"            filter : $TabPosition=3\n" +
"            margin : [10,8,10,8]\n" +
"            graphic.0 : {\n" +
"                margin      : [8,8,8,5]\n" +
"                zorder      : -3\n" +
"            }\n" +
"            graphic.1 : {\n" +
"                shape        : \"'POLYGON((0% 0%, 0% 100%, 0% 0%))'\"\n" +
"                margin       : [8,5,8,5]\n" +
"                brush        : plainbrush(4 'butt')\n" +
"                zorder       : -2\n" +
"            }\n" +
"            graphic.2 : {\n" +
"                shape       : \"'POLYGON((0% 100%, 100% 100%, 0% 100%))'\"\n" +
"                margin      : [9,0,9,0]\n" +
"                zorder      : -1\n" +
"                brush       : plainbrush(1 'butt')\n" +
"                brush-paint : @color-main\n" +
"            }\n" +
"        }\n" +
"\n" +
"        ActiveTrue : {\n" +
"            filter : $Active\n" +
"            graphic.0 : {\n" +
"                fill-paint  : @color-background\n" +
"            }\n" +
"            graphic.1 : {\n" +
"                brush-paint  : @color-focus\n" +
"            }\n" +
"        }\n" +
"        MouseOver : {\n" +
"            filter : $MouseOver\n" +
"            graphic.0 : {\n" +
"                fill-paint  : rotateHSL(@color-background 0 1 1.20)\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WPlatformBar : {\n" +
"        filter : $StyleClass = 'platformbar'\n" +
"        background : {\n" +
"            fill-paint : lineargradientfill('%' 0 0 0 1 0 rotateHSL(@color-background 0 1 1.05) 1 rotateHSL(@color-background 0 1 0.95))\n" +
"        }\n" +
"        graphic : {radius : [1,1,1,1]\n" +
"                  margin : [0,0,0,0]\n" +
"                  brush        : plainbrush(1 'round')\n" +
"                  brush-paint  : @color-main\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WPlatformTab : {\n" +
"        filter : $StyleClass = 'platformtab'\n" +
"\n" +
"        TabPos0 : {\n" +
"            filter : $TabPosition=0\n" +
"            margin : [6,6,6,6]\n" +
"            graphic : {margin : [6,6,6,6]\n" +
"                  brush        : plainbrush(1 'round')\n" +
"                  brush-paint  : @color-main\n" +
"            }\n" +
"        }\n" +
"        TabPos1 : {\n" +
"            filter : $TabPosition=1\n" +
"            margin : [5,9,8,11]\n" +
"            graphic : {margin : [6,9,4,9]\n" +
"            }\n" +
"        }\n" +
"        TabPos2 : {\n" +
"            filter : $TabPosition=2\n" +
"            margin : [6,5,6,8]\n" +
"            graphic : {margin : [5,8,5,5]\n" +
"            }\n" +
"        }\n" +
"        TabPos3 : {\n" +
"            filter : $TabPosition=3\n" +
"            margin : [6,8,6,5]\n" +
"            graphic : {margin : [5,5,5,8]\n" +
"            }\n" +
"        }\n" +
"\n" +
"        ActiveTrue : {\n" +
"            filter : $Active\n" +
"            graphic : {\n" +
"                fill-paint : lineargradientfill('%' 0 0 0 1 0 rotateHSL(@color-focus 0 1 1.00) 1 rotateHSL(@color-background 0 1 0.95))\n" +
"            }\n" +
"        }\n" +
"        ActiveFalse : {\n" +
"            filter : $Active=false\n" +
"            graphic : {\n" +
"                fill-paint : lineargradientfill('%' 0 0 0 1 0 rotateHSL(@color-background 0 1 1.05) 1 rotateHSL(@color-background 0 1 0.95))\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WDesktop : {\n" +
"        filter : $StyleClass = 'desktop'\n" +
"    }\n" +
"\n" +
"    WDesktopFrame : {\n" +
"        filter : $StyleMarker = 'systemframe'\n" +
"        margin : 0\n" +
"        graphic : {\n" +
"            radius       : 6\n" +
"            brush        : plainbrush(1 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : @color-background\n" +
"            margin       : 0\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WDesktopFrame-Header : {\n" +
"        filter : $StyleMarker = 'systemframe-header'\n" +
"        margin : [3,4,4,4]\n" +
"        graphic : {\n" +
"            margin       : [3,4,12,4]\n" +
"            radius       : 6\n" +
"            fill-paint   : @color-main\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WDesktopFrame-Footer : {\n" +
"        filter : $StyleMarker = 'systemframe-footer'\n" +
"        margin : 0\n" +
"        graphic : {\n" +
"            margin       : 0\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WPopup : {\n" +
"        filter : $StyleClass = 'popup'\n" +
"        margin : [1,1,1,1]\n" +
"        background : {\n" +
"            fill-paint : @color-background\n" +
"        }\n" +
"        graphic : {\n" +
"            radius       : 0\n" +
"            brush        : plainbrush(2 'square')\n" +
"            brush-paint  : @color-main\n" +
"            margin       : [1,1,1,1]\n" +
"        }\n" +
"    }\n" +
"\n" +
"    WThumbnail : {\n" +
"        filter : $StyleClass = 'thumbnail'\n" +
"\n" +
"        Selected : {\n" +
"            filter : $Selected\n" +
"            graphic : {\n" +
"                fill-paint   : @color-focus\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"    RowWidget-Headers : {\n" +
"        filter : $StyleMarker = 'RowWidget-headers'\n" +
"        background : {\n" +
"            fill-paint   : lineargradientfill('%' 0 0 0 1 0 rotateHSL(@color-background 0 1 1.1) 1 rotateHSL(@color-background 0 1 0.9))\n" +
"        }\n" +
"    }\n" +
"\n" +
"    RowWidget-Header : {\n" +
"        filter : $StyleMarker = 'RowWidget-header'\n" +
"        margin : [6,4,6,4]\n" +
"        graphic.0 : {\n" +
"            margin : [8,4,8,4]\n" +
"            brush         : plainbrush(1 'butt')\n" +
"            brush-paint   : @color-main\n" +
"        }\n" +
"    }\n" +
"\n" +
"    RowWidget-Row : {\n" +
"        filter : $StyleClass = 'row'\n" +
"        background : {\n" +
"            fill-paint : @color-background\n" +
"        }\n" +
"\n" +
"        RowP : {\n" +
"            filter : modulo($Index 2) = 0\n" +
"            background : {\n" +
"                fill-paint : @color-background\n" +
"            }\n" +
"        }\n" +
"\n" +
"        RowI : {\n" +
"            filter : modulo($Index 2) = 1\n" +
"            background : {\n" +
"                fill-paint : rotateHSL(@color-background 0 1 1.05)\n" +
"            }\n" +
"        }\n" +
"\n" +
"        Selected : {\n" +
"            filter : $Selected\n" +
"            background : {\n" +
"                fill-paint : @color-focus\n" +
"            }\n" +
"        }\n" +
"\n" +
"    }\n" +
"\n" +
"    RowWidget-Cell : {\n" +
"        filter : $StyleClass = 'cell'\n" +
"        margin : [0,0,0,0]\n" +
"        graphic : {\n" +
"            margin : [3,0,3,0]\n" +
"            brush         : plainbrush(0.5 'butt')\n" +
"            brush-paint   : @color-main\n" +
"        }\n" +
"\n" +
"    }\n" +
"\n" +
"    FoldButton : {\n" +
"        filter : $StyleClass = 'foldbutton'\n" +
"        margin : [0,2,0,2]\n" +
"        graphic:none\n" +
"\n" +
"        Fold : {\n" +
"            filter : $UnFold = false\n" +
"            graphic : {\n" +
"                shape : @arrow-right\n" +
"                transform : scaleTransform()\n" +
"                fill-paint:@color-main\n" +
"            }\n" +
"        }\n" +
"\n" +
"        UnFold : {\n" +
"            filter : $UnFold\n" +
"            graphic : {\n" +
"                shape : @arrow-down\n" +
"                transform : scaleTransform()\n" +
"                fill-paint:@color-main\n" +
"            }\n" +
"        }\n" +
"\n" +
"    }\n" +
"\n" +
"\n" +
"    WCrumbBar-Child : {\n" +
"        filter : $StyleClass = 'crumb'\n" +
"        margin:[10,10,10,10]\n" +
"        visual-margin:20\n" +
"        graphic.0 :{\n" +
"            margin       : [9,10,9,20]\n" +
"            brush        : plainbrush(0.8 'round')\n" +
"            brush-paint  : @color-main\n" +
"            fill-paint   : lineargradientfill('%' 0 0 0 1 0 rotateHSL(@color-background 0 1 1.1) 1 rotateHSL(@color-background 0 1 0.9))\n" +
"            shape: \"'POLYGON((0% 0%, 100% 0%, 100%+10 50%, 100% 100%, 0% 100%, 0%+10 50%, 0% 0%))'\"\n" +
"        }\n" +
"\n" +
"        First : {\n" +
"            filter : $Index = 0\n" +
"            graphic.0 :{\n" +
"                margin : [9,10,9,10]\n" +
"                shape:\"'POLYGON((0% 0%, 100% 0%, 100%+10 50%, 100% 100%, 0% 100%, 0% 0%))'\"\n" +
"            }\n" +
"        }\n" +
"\n" +
"        MouseOver : {\n" +
"            filter : $MouseOver\n" +
"            graphic.0 : {\n" +
"                fill-paint   : lineargradientfill('%' 0 0 0 1 0 @color-focus 1 @color-focus)\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"\n" +
"}"

    );

    public static final Chars COLOR_BACKGROUND = Chars.constant("color-background");
    public static final Chars COLOR_TRANSPARENT = Chars.constant("color-transparent");
    public static final Chars COLOR_MAIN = Chars.constant("color-main");
    public static final Chars COLOR_FOCUS = Chars.constant("color-focus");
    public static final Chars COLOR_DELIMITER = Chars.constant("color-delimiter");
    public static final Chars COLOR_TEXT = Chars.constant("color-text");
    public static final Chars COLOR_TEXT_BACKGROUND = Chars.constant("color-textarea-background");

    public static final Chars RULE_SYSTEM = Chars.constant("System");

    public static final Chars THEME_LIGHT = Chars.constant("mod:/module-res/default.style");
    public static final Chars THEME_DARK = Chars.constant("mod:/module-res/dark.style");
    public static final Chars THEME_FLAT = Chars.constant("mod:/module-res/flat.style");

    public static final StyleDocument INSTANCE;
    static {
        try {
            INSTANCE = RSReader.readStyleDoc(Paths.resolve(THEME_LIGHT));
            //INSTANCE = RSReader.readStyleDoc(BASIC);
        } catch (InvalidArgumentException ex){
            throw new RuntimeException("Failed to load System default style "+ex.getMessage(),ex);
        }
    }

    private SystemStyle() {
    }

}
