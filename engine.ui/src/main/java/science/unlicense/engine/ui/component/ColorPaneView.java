
package science.unlicense.engine.ui.component;

import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.impl.colorspace.HSL;
import science.unlicense.image.impl.colorspace.SRGB;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class ColorPaneView extends WidgetView{

    private static final HSL hsl = HSL.INSTANCE;

    private Image hslImage = null;

    public ColorPaneView(WColorPane widget) {
        super(widget);
    }

    public WColorPane getWidget() {
        return (WColorPane) super.getWidget();
    }

    public void calculateExtents(Extent min, Extent best, Extent max) {
        min.set(0, 120);
        min.set(1, 60);
        best.set(min);
        max.set(min);
    }

    protected void renderSelf(Painter2D painter, BBox innerBBox) {
        super.renderSelf(painter, innerBBox);

        final Extent ext = widget.getEffectiveExtent();
        if (hslImage == null || hslImage.getExtent().get(0) != ext.get(0)
                            || hslImage.getExtent().get(1) != ext.get(1)){
            //component size differ from image size
            hslImage = createHSLLinearImage(ext);
        }
        painter.paint(hslImage, new Affine2(
                1, 0, -ext.get(0)/2.0,
                0, 1, -ext.get(1)/2.0));

        final TupleRW coord = toCoord(innerBBox, getWidget().getColor());
        final PlanarGeometry circle = new Circle(coord, 3);
        painter.setPaint(new ColorPaint(Color.WHITE));
        painter.fill(circle);
        painter.setPaint(new ColorPaint(Color.BLACK));
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
        painter.stroke(circle);
    }

    /**
     * Convert color to HSL image coordinate.
     * @param ext
     * @param x
     * @param y
     * @return
     */
    private static TupleRW toCoord(BBox ext, Color color){
        if (color==null) color = Color.WHITE;
        float[] buffer = color.toColorSystem(ColorSystem.HSL_FLOAT).getSamples(null);
        float x = buffer[0] / 360f;
        float y =  buffer[2];
        return new Vector2f64(
            Maths.lerp(ext.getMin(0), ext.getMax(0), x),
            Maths.lerp(ext.getMax(1), ext.getMin(1), y));
    }

    /**
     * Create an HSL linear image.
     *
     * @param width
     * @param height
     * @return
     */
    private static Image createHSLLinearImage(Extent ext){
        final int width = (int) ext.get(0);
        final int height = (int) ext.get(1);


        final Image image = Images.create(new Extent.Long(width,height),Images.IMAGE_TYPE_RGBA);
        final ColorSystem cs = (ColorSystem) image.getColorModel().getSampleSystem();
        final TupleGridCursor cursor = image.getColorModel().asTupleBuffer(image).cursor();
        final float[] buffer = new float[3];
        final float[] rgba = new float[3];

        //fixed saturation
        buffer[1] = 1f;
        for (int x=0;x<width;x++){
            // Hue
            buffer[0] = (x*360f) / width;
            for (int y=0;y<height;y++){
                //lightness
                buffer[2] = 1f - ((float) y / height);
                hsl.getTranform(SRGB.INSTANCE).transform(buffer, 0, rgba, 0, 1);
                Color color = new ColorRGB(rgba).toColorSystem(cs);
                cursor.moveTo(new Vector2i32(x,y));
                cursor.samples().set(color);
            }
        }

        return image;
    }

}
