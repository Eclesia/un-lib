
package science.unlicense.engine.ui.widget.frame;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.math.impl.Vector2f64;

/**
 * Container displaying frames.
 *
 * @author Johann Sorel
 */
public class WDesktop extends WContainer{

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("desktop")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private final WDesktopScreen screen = new WDesktopScreen(this);

    public WDesktop() {
        super(new AbsoluteLayout());
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public WDesktopScreen getScreen() {
        return screen;
    }

    public UIFrame addFrame(){
        final WDesktopFrame frame = new WDesktopFrame(this);
        children.add(frame);
        frame.setOnScreenLocation(new Vector2f64(50, 50));
        frame.setSize(100,100);
        return frame;
    }

}
