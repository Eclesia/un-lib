
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.engine.ui.visual.PasswordFieldView;

/**
 *
 * @author Johann Sorel
 */
public class WPasswordField extends WTextField {

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WTextField.CLASS_DEFAULTS);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WPasswordField() {
        this(Chars.EMPTY);
    }

    public WPasswordField(CharArray text) {
        super(text);
        setView(new PasswordFieldView(this));
    }
}
