
package science.unlicense.engine.ui.widget.frame;

import science.unlicense.display.api.desktop.Screen;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public class WDesktopScreen implements Screen{

    private final WDesktop desktop;

    public WDesktopScreen(WDesktop desktop) {
        this.desktop = desktop;
    }

    @Override
    public Extent getExtent() {
        return desktop.getEffectiveExtent().copy();
    }

    @Override
    public double getDotPerMillimeter() {
        return desktop.getFrame().getScreen().getDotPerMillimeter();
    }

}
