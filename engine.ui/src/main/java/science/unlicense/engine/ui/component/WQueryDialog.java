
package science.unlicense.engine.ui.component;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.desktop.FrameManager;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class WQueryDialog {

    private Chars title;
    private Chars[] actions = new Chars[]{new Chars("Ok")};
    private Widget content;
    private FrameManager frameManager;

    private Chars resultAction = null;

    public WQueryDialog() {

    }

    public void setTitle(Chars title) {
        this.title = title;
    }

    public Chars getTitle() {
        return title;
    }

    public Chars[] getActions() {
        return actions;
    }

    public void setActions(Chars[] actions) {
        this.actions = actions;
    }

    public Widget getContent() {
        return content;
    }

    public void setContent(Widget content) {
        this.content = content;
    }

    public FrameManager getFrameManager() {
        return frameManager;
    }

    public void setFrameManager(FrameManager frameManager) {
        this.frameManager = frameManager;
    }

    public Chars show() {

        FrameManager fm = frameManager;
        if (fm == null) fm = FrameManagers.getFrameManager();
        final UIFrame frame = (UIFrame) fm.createFrame(null, true, false);
        frame.setTitle(title);
        final WContainer container = frame.getContainer();
        container.setLayout(new BorderLayout());

        final LineLayout lineLayout = new LineLayout();
        lineLayout.setHorizontalAlignement(LineLayout.HALIGN_CENTER);
        lineLayout.setDistance(6);

        final WContainer actionsPane = new WContainer(lineLayout);
        for (int i=0;i<actions.length;i++) {
            final Chars name = actions[i];
            final WButton button = new WButton(name, null, new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    resultAction = name;
                    frame.setVisible(false);
                    frame.dispose();
                }
            });
            actionsPane.getChildren().add(button);
        }

        if (content != null) {
            container.addChild(content, BorderConstraint.CENTER);
        }

        final WContainer bottomLow = new WContainer(new BorderLayout());
        bottomLow.addChild(actionsPane, BorderConstraint.CENTER);
        container.addChild(bottomLow, BorderConstraint.BOTTOM);

        final Extents extents = container.getExtents(null);

        frame.setSize((int) extents.bestX+32, (int) extents.bestY+32);
        frame.setVisible(true);
        frame.waitForDisposal();
        return resultAction;
    }

    public static Chars showInput(FrameManager fm, CharArray message, Chars[] actions) {
        final WQueryDialog dialog = new WQueryDialog();
        if (actions != null) dialog.setActions(actions);
        dialog.setFrameManager(fm);

        if (message != null) {
            final WContainer container = new WContainer();
            final LineLayout layout = new LineLayout();
            layout.setHorizontalAlignement(LineLayout.HALIGN_CENTER);
            layout.setVerticalAlignement(LineLayout.VALIGN_CENTER);
            container.getChildren().add(new WLabel(message));
            dialog.setContent(container);
        }

        return dialog.show();
    }

}
