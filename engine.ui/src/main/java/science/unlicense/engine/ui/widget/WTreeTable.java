
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.collection.CollectionMessage;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.DefaultNode;
import science.unlicense.engine.ui.model.Column;
import science.unlicense.engine.ui.model.DefaultTreeColumn;
import science.unlicense.engine.ui.model.TreeRowModel;

/**
 * Display a multi column tree.
 *
 * @author Johann Sorel
 */
public class WTreeTable extends AbstractRowWidget{

    public WTreeTable() {
        super(new TreeRowModel(new DefaultNode(true)));
    }

    public TreeRowModel getRowModel() {
        return (TreeRowModel) super.getRowModel();
    }

    public void setRowModel(TreeRowModel rowModel) {
        for (int i=0,n=getColumns().getSize();i<n;i++){
            Object col = getColumns().get(i);
            if (col instanceof DefaultTreeColumn){
                ((DefaultTreeColumn) col).setRowModel(rowModel);
            }
        }
        super.setRowModel(rowModel);
    }

    public Sequence getColumns() {
        return super.getColumns();
    }

    protected void columnChanged(CollectionMessage event) {
        final Sequence columns = getColumns();
        for (int i=0,n=columns.getSize();i<n;i++){
            final Column col = (Column) columns.get(i);
            if (col instanceof DefaultTreeColumn){
                ((DefaultTreeColumn) col).setRowModel(getRowModel());
            }
        }
        super.columnChanged(event);
    }

}
