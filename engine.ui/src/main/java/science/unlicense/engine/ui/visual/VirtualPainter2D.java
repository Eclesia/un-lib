

package science.unlicense.engine.ui.visual;

import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.painter2d.AbstractPainter2D;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.image.api.Image;
import science.unlicense.math.api.Affine;

/**
 * Fake painter2D used for tests.
 *
 * @author Johann Sorel
 */
public class VirtualPainter2D extends AbstractPainter2D{

    public VirtualPainter2D() {
    }

    @Override
    public void fill(PlanarGeometry geom) {

    }

    @Override
    public void stroke(PlanarGeometry geom) {

    }

    @Override
    public void paint(Image image, Affine transform) {

    }

    @Override
    public void dispose() {

    }

    @Override
    public FontStore getFontStore() {
        return new VirtualFontStore();
    }

}
