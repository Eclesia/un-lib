
package science.unlicense.engine.ui.widget.menu;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.layout.PairLayout;
import science.unlicense.engine.ui.ievent.ActionExecutable;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.widget.WSwitch;
import science.unlicense.engine.ui.widget.Widget;

/**
 * Toolbar or Menu switch button.
 *
 * @author Johann Sorel
 */
public class WMenuSwitch extends WSwitch {

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WSwitch.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("menuswitch")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WMenuSwitch() {
        this((CharArray) null);
    }

    public WMenuSwitch(CharArray text){
        this(text,null);
    }

    public WMenuSwitch(CharArray text, Widget graphic){
        this(text, graphic,null);
    }

    public WMenuSwitch(CharArray text, Widget graphic, EventListener lst){
        super(text, graphic, lst);
        setHorizontalAlignment(PairLayout.HALIGN_LEFT);
    }

    public WMenuSwitch(ActionExecutable exec){
        super(exec);
        setHorizontalAlignment(PairLayout.HALIGN_LEFT);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }
}
