
package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.DisplacementLayout;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WScrollContainer;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class WListView extends AbstractPathView {

    private final WContainer list = new WContainer(new LineLayout());
    private final WScrollContainer scroll = new WScrollContainer(list);

    private final EventListener selectionListener = new EventListener() {
        public void receiveEvent(Event event) {
            if (event.getMessage() instanceof PropertyMessage){
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                if (WThumbnail.PROPERTY_SELECTED.equals(pe.getPropertyName())){
                    if (Boolean.TRUE.equals(pe.getNewValue())){
                        //path selected
                        final Path p = ((WCell) event.getSource()).thumbnail.getPath();
                        setSelectedPath(new Path[]{p});
                    } else {
                        //path unselected
                        final Path p = ((WCell) event.getSource()).thumbnail.getPath();
                        Path[] selectedPaths = getSelectedPath();
                        int idx = Arrays.getFirstOccurence(selectedPaths, 0, selectedPaths.length, p);
                        if (idx>=0){
                            final Path[] sel = (Path[]) Arrays.remove(selectedPaths, idx);
                            setSelectedPath(sel);
                        }
                    }
                }
            } else if (event.getMessage() instanceof MouseMessage){
                final MouseMessage me = (MouseMessage) event.getMessage();
                final WCell source = (WCell) event.getSource();
                try{
                    if (me.getType()==MouseMessage.TYPE_PRESS && me.getNbClick()>1 && source.thumbnail.getPath().isContainer()){
                        //open selected container
                        setViewRoot(source.thumbnail.getPath());
                    }
                }catch(IOException ex){}
            }
        }
    };
    public WListView() {
        this(18);
    }

    public WListView(int rowSize) {
        super(createGlyph("\uE025"),new Chars("List"));
        setLayout(new BorderLayout());
        addChild(scroll, BorderConstraint.CENTER);
        final DisplacementLayout l = (DisplacementLayout) scroll.getScrollingContainer().getLayout();
        l.setFillHeight(true);

        final LineLayout layout = (LineLayout) list.getLayout();
        layout.setDirection(LineLayout.DIRECTION_VERTICAL);
        layout.setHorizontalAlignement(LineLayout.HALIGN_LEFT);
        layout.setVerticalAlignement(LineLayout.VALIGN_TOP);
        layout.setDistance(0);
        layout.setFitToCell(true);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_VIEW_ROOT.equals(name)){
            update(true);
        } else if (PROPERTY_SELECTED_PATHS.equals(name)){
            update(false);
        } else if (PROPERTY_FILTER.equals(name)){
            update(true);
        }

    }

    private void update(boolean all){
        final Path viewRoot = getViewRoot();

        if (all){
            //reset scroll position
            scroll.getVerticalScrollBar().setRatio(0);
            scroll.getHorizontalScrollBar().setRatio(0);

            Sequence datas;
            if (viewRoot==null){
                datas = new ArraySequence();
            } else {
                datas = new ArraySequence(viewRoot.getChildren());
            }

            final Predicate filter = getFilterInternal(true);
            datas = Collections.filter(datas, filter);

            //sort by name and type(folder/file)
            Collections.sort(datas, WPathChooser.FILE_SORTER);

            final Sequence elements = new ArraySequence();
            for (int i=0;i<datas.getSize();i++){
                elements.add(createWidget(datas.get(i)));
            }
            list.getChildren().replaceAll(elements);
        }

        //set selected paths
        final Sequence elements = list.getChildren();
        final Path[] selectedPaths = getSelectedPath();
        for (int i=0,n=elements.getSize();i<n;i++) {
            final WCell prev = (WCell) elements.get(i);
            prev.removeEventListener(Predicate.TRUE, selectionListener);
            prev.thumbnail.setSelected(Arrays.contains(selectedPaths, prev.thumbnail.getPath()));
            prev.addEventListener(Predicate.TRUE, selectionListener);
        }

    }

    private class WCell extends WContainer {

        private final WThumbnail thumbnail;

        public WCell(Path path) {
            super(new BorderLayout());
            //TODO find less intricate solution
            thumbnail = new WThumbnail(path, false, null, true, false, getFilter());
            thumbnail.setGraphicPlacement(WLabel.GRAPHIC_LEFT);
            thumbnail.setHorizontalAlignment(WLabel.HALIGN_LEFT);
            setPropertyValue(StyleDocument.PROPERTY_STYLE_CLASS, WThumbnail.STYLECLASS);
            getProperty(WThumbnail.PROPERTY_SELECTED).sync(thumbnail.getProperty(WThumbnail.PROPERTY_SELECTED));
            setInlineStyle(new Chars("margin:3\ngraphic:{\nmargin:3\n}"));
            addChild(thumbnail, BorderConstraint.LEFT);
        }

        @Override
        protected void receiveEventParent(Event event) {
            if (!event.getMessage().isConsumed() && event.getMessage() instanceof MouseMessage) {
                final MouseMessage me = (MouseMessage) event.getMessage();
                if (me.getType() == MouseMessage.TYPE_TYPED) {
                    thumbnail.setSelected(!thumbnail.isSelected());
                    me.consume();
                }
            }
            super.receiveEventParent(event);
        }

    }

    private Widget createWidget(Object candidate) {

        if (candidate instanceof Path){
            final Path path = (Path) candidate;
            return new WCell(path);
        } else {
            final WLabel label = new WLabel();
            return label;
        }
    }

}
