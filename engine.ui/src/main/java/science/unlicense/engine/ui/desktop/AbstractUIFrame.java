
package science.unlicense.engine.ui.desktop;

import science.unlicense.display.api.desktop.AbstractFrame;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractUIFrame extends AbstractFrame implements UIFrame{

    public AbstractUIFrame(UIFrame parent) {
        super(parent);
    }

}
