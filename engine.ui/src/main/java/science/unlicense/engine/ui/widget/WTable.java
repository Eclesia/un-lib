
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.engine.ui.model.Column;
import science.unlicense.engine.ui.model.RowModel;

/**
 * Table widget.
 *
 * @author Johann Sorel
 */
public class WTable extends AbstractRowWidget{

    public WTable() {
    }

    public WTable(RowModel rowModel, Column colModel) {
        this();
        setRowModel(rowModel);
        getColumns().add(colModel);
    }

    public RowModel getRowModel() {
        return super.getRowModel();
    }

    public void setRowModel(RowModel rowModel) {
        super.setRowModel(rowModel);
    }

    public Sequence getColumns() {
        return super.getColumns();
    }

}
