
package science.unlicense.engine.ui.widget.chart;

import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.transform.Projections;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class WFunctionSpaceChart extends WLeaf {

    private TupleSpace functionSpace;
    private ColorSystem colorSystem;
    private BBox viewBox = new BBox(new Vector2f64(0, 0), new Vector2f64(1, 1));

    public WFunctionSpaceChart() {
        setView(new View(this));
    }

    public TupleSpace getFunctionSpace() {
        return functionSpace;
    }

    public void setFunctionSpace(TupleSpace functionSpace) {
        this.functionSpace = functionSpace;
    }

    public ColorSystem getColorSystem() {
        return colorSystem;
    }

    public void setColorSystem(ColorSystem colorSystem) {
        this.colorSystem = colorSystem;
    }

    public BBox getViewBox() {
        return viewBox;
    }

    public void setViewBox(BBox viewBox) {
        this.viewBox = viewBox;
    }

    private class View extends WidgetView {

        public View(Widget widget) {
            super(widget);
        }

        @Override
        protected void renderSelf(Painter2D painter, BBox innerBBox) {
            super.renderSelf(painter, innerBBox);

            //painter.setClip(new Rectangle(innerBBox));
            painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
            painter.setPaint(new ColorPaint(Color.BLACK));

            //we want Y upward
            final AffineRW widgetToView = Projections.zoomed(innerBBox, viewBox);
            final AffineRW viewToWidget = widgetToView.invert(null);

            final Image image = Images.copyToImage(functionSpace, new Extent.Long((int) innerBBox.getSpan(0), (int) innerBBox.getSpan(1)));

            painter.paint(image, new Affine2(viewToWidget));

            drawGrid(painter, innerBBox);
        }

        private void drawGrid(Painter2D painter, BBox innerBBox) {

            painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
            painter.setPaint(new ColorPaint(Color.GRAY_LIGHT));

            final AffineRW widgetToView = Projections.zoomed(innerBBox, viewBox);
            final AffineRW viewToWidget = widgetToView.invert(null);

            final BBox bbox = Geometries.transform(innerBBox, widgetToView, null);


            double minx = bbox.getMin(0);
            double maxx = bbox.getMax(0);
            double miny = bbox.getMin(1);
            double maxy = bbox.getMax(1);
            double step = 1.0;

            minx = ((int) (minx * step)) / step - step;
            maxx = ((int) (maxx * step)) / step + step;
            miny = ((int) (miny * step)) / step - step;
            maxy = ((int) (maxy * step)) / step + step;


            final Vector2f64 v = new Vector2f64();
            final Line segment = new DefaultLine(0, 0, 0, 0);
            for (double y=miny; y<=maxy; y+=step) {
                v.x = minx;
                v.y = y;
                viewToWidget.transform(v, v);
                segment.getStart().set(v);
                v.x = maxx;
                v.y = y;
                viewToWidget.transform(v, v);
                segment.getEnd().set(v);

                painter.stroke(segment);
            }

            for (double x=minx; x<=maxx; x+=step) {
                v.x = x;
                v.y = miny;
                viewToWidget.transform(v, v);
                segment.getStart().set(v);
                v.x = x;
                v.y = maxy;
                viewToWidget.transform(v, v);
                segment.getEnd().set(v);

                painter.stroke(segment);
            }

        }

    }

}
