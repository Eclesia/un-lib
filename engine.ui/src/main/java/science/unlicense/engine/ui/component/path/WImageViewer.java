
package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.transform.PanaromicTo2DTransform;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.transform.Projections;
import science.unlicense.geometry.impl.transform.ViewTransform;
import science.unlicense.geometry.impl.tuple.TupleSpaceTransform;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.process.ExtrapolatorWrap;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Matrix3x3;

/**
 *
 * @author Johann Sorel
 */
public class WImageViewer extends WContainer implements WValueWidget {

    public static final Chars PROPERTY_IMAGE = Chars.constant("Image");

    private final WButtonBar bar = new WButtonBar();
    private final WGraphicImage imgGraphic = new WGraphicImage();
    private final WContainer meta = new WContainer();
    private final ViewTransform view = new ViewTransform();
    private boolean userAction = false;

    //user actions
    private final double[] mvCoord = new double[]{Double.NaN, Double.NaN};
    private boolean pressed = false;
    private boolean rotate = false;

    public WImageViewer() {
        setLayout(new BorderLayout());

        bar.addChild(new WButton(new Chars("+")), null);
        bar.addChild(new WButton(new Chars("-")), null);
        bar.addChild(new WButton(new Chars("o")), null);

        final WContainer c = new WContainer();
        c.setLayout(new AbsoluteLayout());
        c.addChild(imgGraphic, null);
        c.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final MouseMessage message = (MouseMessage) event.getMessage();
                final int type = message.getType();
                final double[] m = message.getMousePosition().toDouble();
                final int button = message.getButton();

                if (type == MouseMessage.TYPE_WHEEL) {
                    if (rotate) {
                        final double angle = Angles.degreeToRadian(message.getWheelOffset());
                        view.rotateAt(angle, message.getMousePosition().toDouble());
                    } else {
                        final double scale = 1 + message.getWheelOffset()*0.1;
                        view.scaleAt(scale, message.getMousePosition().toDouble());
                    }
                    imgGraphic.getNodeTransform().set(view.getSourceToTarget());
                } else if (type == MouseMessage.TYPE_MOVE) {
                    if (pressed){
                        final double[] diff = new double[]{
                            m[0]-mvCoord[0],
                            m[1]-mvCoord[1]
                        };
                        view.translate(diff);
                        imgGraphic.getNodeTransform().set(view.getSourceToTarget());
                    }
                    mvCoord[0] = m[0];
                    mvCoord[1] = m[1];
                } else if (type == MouseMessage.TYPE_PRESS && button == MouseMessage.BUTTON_1) {
                    pressed = true;
                } else if (type == MouseMessage.TYPE_RELEASE && button == MouseMessage.BUTTON_1) {
                    pressed = false;
                } else if (type == MouseMessage.TYPE_EXIT) {
                    pressed = false;
                } else if (type == MouseMessage.TYPE_ENTER) {
                    c.requestFocus();
                }

            }
        });
        c.addEventListener(KeyMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final KeyMessage message = (KeyMessage) event.getMessage();
                final int type = message.getType();
                final int button = message.getCode();
                if (type == KeyMessage.TYPE_PRESS && button == KeyMessage.KC_CONTROL) {
                    rotate = true;
                } else if (type == KeyMessage.TYPE_RELEASE && button == KeyMessage.KC_CONTROL) {
                    rotate = false;
                }

            }
        });

        addChild(c, BorderConstraint.CENTER);
        addChild(meta, BorderConstraint.RIGHT);
        addChild(bar, BorderConstraint.BOTTOM);


        meta.setLayout(new GridLayout(0, 1));
        final WButton view2d = new WButton(new Chars("2D"), null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
            }
        });
        final WButton viewPan = new WButton(new Chars("360°"), null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                Image baseImage = getImage();
                getChildren().remove(c);
                addChild(new PanoramicView(baseImage), BorderConstraint.CENTER);
            }
        });
        meta.getChildren().add(view2d);
        meta.getChildren().add(viewPan);

    }

    @Override
    public void setEffectiveExtent(Extent extent) {
        super.setEffectiveExtent(extent);

        final Image image = getImage();
        if (!userAction && image != null) {
            final double scalex = extent.get(0) / image.getExtent().get(0);
            final double scaley = extent.get(1) / image.getExtent().get(1);
            final Extent ext;
            if (scalex<scaley){
                ext = new Extent.Double(extent.get(0), image.getExtent().get(1)*scalex);
            } else {
                ext = new Extent.Double(image.getExtent().get(0)*scaley, extent.get(1));
            }
            AffineRW t = Projections.scaled(new BBox(image.getExtent()), new BBox(ext));
            view.getSourceToTarget().set(t);
        }

    }

    @Override
    public final Chars getValuePropertyName() {
        return PROPERTY_IMAGE;
    }

    public final Image getImage() {
        return (Image) getPropertyValue(PROPERTY_IMAGE);
    }

    public final void setImage(Object value) {
        setPropertyValue(PROPERTY_IMAGE, (Image) value);
    }

    public final Property varImage() {
        return getProperty(PROPERTY_IMAGE);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_IMAGE.equals(name)) {
            imgGraphic.setImage((Image) value);
            imgGraphic.getNodeTransform().set(view.getSourceToTarget());
        }
    }

    private static class PanoramicView extends WGraphicImage {

        private final MonoCamera camera = new MonoCamera();
        private double angleX = 0.0;
        private double angleY = 0.0;
        private final Image baseImage;

        public PanoramicView(Image baseImage) {
            this.baseImage = baseImage;
            camera.setFieldOfView(60);
            camera.setRenderArea(new Rectangle(0, 0, baseImage.getExtent().getL(0), baseImage.getExtent().getL(1)));
            camera.setCameraType(MonoCamera.TYPE_ORTHO);

            setFitting(FITTING_SCALED);

            addEventListener(MouseMessage.PREDICATE, new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    final MouseMessage message = (MouseMessage) event.getMessage();
                    final int type = message.getType();
                    if (type == MouseMessage.TYPE_ENTER) {
                        PanoramicView.this.requestFocus();
                    }
                }
            });
            final double speed = 10;
            addEventListener(KeyMessage.PREDICATE, new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    final KeyMessage message = (KeyMessage) event.getMessage();
                    final int type = message.getType();
                    final int button = message.getCode();
                    if (type == KeyMessage.TYPE_PRESS) {
                        if (button == KeyMessage.KC_LEFT) {
                            angleX-=speed;
                        } else if (button == KeyMessage.KC_RIGHT) {
                            angleX+=speed;
                        } else if (button == KeyMessage.KC_UP) {
                            angleY+=speed;
                        } else if (button == KeyMessage.KC_DOWN) {
                            angleY-=speed;
                        }
                        update();
                    }
                }
            });
            update();
        }

        private void update() {
            final Extent.Long extent = baseImage.getExtent();

            double forwardX = angleX-180;
            final Matrix3x3 mx = Matrix3x3.createRotation3(Math.toRadians(forwardX), camera.getUpAxis());
            final Matrix3x3 my = Matrix3x3.createRotation3(Math.toRadians(angleY), camera.getRightAxis());
            MatrixRW m = mx.multiply(my);
            camera.getNodeTransform().getRotation().set(m);
            camera.getNodeTransform().notifyChanged();
            final Transform trs = PanaromicTo2DTransform.create(extent, camera, camera.getNodeToRootSpace());

            final long resSizeX = 1024;
            final long resSizeY = 768;
            final Image res = Images.create(baseImage, new Extent.Long(resSizeX, resSizeY));
            final TupleGrid outGrid = res.getTupleBuffer(res.getColorModel());

            TupleGrid srcGrid = baseImage.getTupleBuffer(baseImage.getColorModel());
            TupleSpace srcSpace = new ExtrapolatorWrap(srcGrid);
            srcSpace = new TupleSpaceTransform(srcSpace, null, trs);

            Images.sample(srcSpace, outGrid);

            setImage(res);
        }

    }

}
