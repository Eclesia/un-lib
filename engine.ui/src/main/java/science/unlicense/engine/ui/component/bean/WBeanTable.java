
package science.unlicense.engine.ui.component.bean;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.DefaultProperty;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.lang.Reflection;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.engine.ui.widget.Widget;

/**
 * TODO : memory leak by property editors not unsync.
 *
 * @author Johann Sorel
 */
public class WBeanTable extends WContainer implements WValueWidget {

    public static final Chars PROPERTY_BEAN = Chars.constant("Bean");

    private final Sequence editors = new ArraySequence();

    private final WTable attributeTable = new WTable();

    public WBeanTable() {
        super(new BorderLayout());
        addChild(attributeTable, BorderConstraint.CENTER);

        final DefaultColumn nameColumn = new DefaultColumn(new Chars("Property"), new PropertyNamePresenter());
        nameColumn.setBestWidth(140);
        final DefaultColumn valueColumn = new DefaultColumn(new Chars("Value"), new PropertyValuePresenter());
        valueColumn.setBestWidth(FormLayout.SIZE_EXPAND);
        attributeTable.getColumns().add(nameColumn);
        attributeTable.getColumns().add(valueColumn);

        //default editors
        editors.add(CharArrayEditor.INSTANCE);
        editors.add(BooleanEditor.INSTANCE);
        editors.add(DoubleEditor.INSTANCE);
        editors.add(IntegerEditor.INSTANCE);
    }

    @Override
    public final Chars getValuePropertyName() {
        return PROPERTY_BEAN;
    }

    public final EventSource getBean() {
        return (EventSource) getPropertyValue(PROPERTY_BEAN);
    }

    public final void setBean(Object bean) {
        setPropertyValue(PROPERTY_BEAN, bean);
    }

    public final Property varBean() {
        return getProperty(PROPERTY_BEAN);
    }

    public Sequence getEditors() {
        return editors;
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_BEAN.equals(name)) {
            final Object bean = value;
            final Sequence properties = new ArraySequence();
            if (bean instanceof EventSource) {
                final Sequence propNames = Reflection.listProperties(bean.getClass());
                final Iterator ite = propNames.createIterator();
                while (ite.hasNext()) {
                    final Chars propName = (Chars) ite.next();
                    final Property prop = new DefaultProperty((EventSource) bean, propName);

                    for (int k=0,kn=editors.getSize();k<kn;k++) {
                        final WValueWidget editor = ((PropertyEditor) editors.get(k)).create(prop);
                        if (editor != null) {
                            properties.add(prop);
                            break;
                        }
                    }
                }
            }
            attributeTable.setRowModel(new DefaultRowModel(properties));
        }
    }

    private static final class PropertyNamePresenter implements ObjectPresenter{
        public Widget createWidget(Object candidate) {
            final Property property = (Property) candidate;
            final WLabel lbl = new WLabel(property.getName());
            lbl.setInlineStyle(new Chars("margin:2"));
            return lbl;
        }
    }

    private final class PropertyValuePresenter implements ObjectPresenter{
        public Widget createWidget(Object candidate) {
            final Property property = (Property) candidate;

            for (int k=0,kn=editors.getSize();k<kn;k++){
                final WValueWidget editor = ((PropertyEditor) editors.get(k)).create(property);
                if (editor != null) {
                    editor.getProperty(editor.getValuePropertyName()).sync(property);
                    return editor;
                }
            }

            return new WSpace();
        }
    }

}
