
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WScrollBar;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.RoundedRectangle;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;

/**
 *
 * @author Johann Sorel
 */
public class ScrollBarView extends WidgetView{

    private boolean dragging = false;

    public ScrollBarView(WScrollBar.Scroll widget) {
        super(widget);
    }

    @Override
    public WScrollBar.Scroll getWidget() {
        return (WScrollBar.Scroll) super.getWidget();
    }

    @Override
    public void getExtents(Extents buffer, Extent constraint) {
        buffer.minX = 6;
        buffer.minY = 6;
        buffer.bestX = buffer.minX;
        buffer.bestY = buffer.minY;
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }

    @Override
    protected void renderSelf(Painter2D painter, BBox innerBBox) {

        final boolean horizontal = getWidget().getScrollBar().isHorizontal();
        final BBox bbox = widget.getInnerExtent();
        final double width = bbox.getSpan(0);
        final double height = bbox.getSpan(1);
        final double ssize = horizontal?height:width;
        final double hss = ssize/2.0;

        final int cursorSize = (horizontal?width:height) > 60 ? 40 : 10;
        final Document style = widget.getEffectiveStyleDoc();

        final Color borderColor = (Color) WidgetStyles.getOrEvaluate(style, SystemStyle.COLOR_MAIN, widget);

        //draw the scroll cursor
        painter.setPaint(new ColorPaint(borderColor));
        double cursorPos = (bbox.getSpan(horizontal?0:1)-cursorSize) * getWidget().getScrollBar().getRatio();
        cursorPos += bbox.getMin(horizontal?0:1);
        cursorPos += cursorSize/2;

        final PlanarGeometry cercle;
        if (horizontal){
            cercle = new RoundedRectangle(cursorPos-cursorSize/2, -hss, cursorSize, ssize,3,3);
        } else {
            cercle = new RoundedRectangle(-hss, cursorPos-cursorSize/2, ssize,cursorSize,3,3);
        }
        painter.fill(cercle);

    }

    @Override
    public void receiveEvent(Event event) {

        final EventMessage message = event.getMessage();
        if (message instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) message;
            final int type = me.getType();

            if (type == MouseMessage.TYPE_PRESS){
                dragging = true;
            } else if (type == MouseMessage.TYPE_RELEASE){
                dragging = false;
            } else if (type == MouseMessage.TYPE_ENTER){
                dragging = false;
            } else if (type == MouseMessage.TYPE_EXIT){
                dragging = false;
            }

            final WScrollBar scroll = getWidget().getScrollBar();

            if (type == MouseMessage.TYPE_WHEEL){
                handleWheel(me);
            } else if (dragging || type == MouseMessage.TYPE_PRESS){
                final Tuple position = me.getMousePosition();
                final boolean horizontal = scroll.isHorizontal();
                final BBox bbox = widget.getBoundingBox(null);
                final int ordinate = horizontal?0:1;
                final double size = bbox.getSpan(ordinate);
                final double x = Maths.clamp(position.get(ordinate)-bbox.getMin(ordinate), 0, size);
                final double r = x/size;
                scroll.setRatio(Maths.clamp(r, 0, 1));
                me.consume();
            }
        }

    }

    @Override
    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        if (WScrollBar.PROPERTY_RATIO.equals(event.getPropertyName())){
            widget.setDirty();
        }
    }

    private void handleWheel(final MouseMessage me){
        final WScrollBar scroll = getWidget().getScrollBar();
        final double ratio = scroll.getRatio();
        final double step = scroll.getStep();
        double r = ratio - me.getWheelOffset()*step;
        r = Maths.clamp(r, 0, 1);
        scroll.setRatio(r);
        me.consume();
    }

}
