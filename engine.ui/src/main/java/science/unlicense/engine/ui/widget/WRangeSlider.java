package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.model.SliderModel;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.visual.RangeSliderView;

/**
 *
 * @author Johann Sorel
 */
public class WRangeSlider extends WLeaf{

    public static final Chars PROPERTY_VALUE_MIN = Chars.constant("ValueMin");
    public static final Chars PROPERTY_VALUE_MAX = Chars.constant("ValueMax");
    public static final Chars PROPERTY_INNER_RANGE = Chars.constant("InnerRange");
    public static final Chars PROPERTY_MODEL = Chars.constant("Model");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLeaf.CLASS_DEFAULTS);
        defs.add(PROPERTY_INNER_RANGE, Boolean.TRUE);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("rangeslider")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WRangeSlider(SliderModel model) {
        this(model,model.getDefaultValue());
    }

    public WRangeSlider(SliderModel model, Object value) {
        setPropertyValue(PROPERTY_MODEL, model);
        setPropertyValue(PROPERTY_VALUE_MIN, value);
        setPropertyValue(PROPERTY_VALUE_MAX, value);
        setView(new RangeSliderView(this));
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public final SliderModel getModel() {
        return (SliderModel) getPropertyValue(PROPERTY_MODEL);
    }

    public final void setModel(SliderModel model) {
        setPropertyValue(PROPERTY_MODEL, model);
    }

    public final Property varModel() {
        return getProperty(PROPERTY_MODEL);
    }

    public final Object getValueMin() {
        return getPropertyValue(PROPERTY_VALUE_MIN);
    }

    public final void setValueMin(Object value) {
        setPropertyValue(PROPERTY_VALUE_MIN, value);
    }
    /**
     * Get value min variable.
     * @return Variable.
     */
    public final Property varValueMin(){
        return getProperty(PROPERTY_VALUE_MIN);
    }

    public final Object getValueMax() {
        return getPropertyValue(PROPERTY_VALUE_MAX);
    }

    public final void setValueMax(Object value) {
        setPropertyValue(PROPERTY_VALUE_MAX, value);
    }

    /**
     * Get value max variable.
     * @return Variable.
     */
    public final Property varValueMax(){
        return getProperty(PROPERTY_VALUE_MAX);
    }

    public final boolean isInnerRange() {
        return (Boolean) getPropertyValue(PROPERTY_INNER_RANGE);
    }

    public final void setInnerRange(boolean inner) {
        setPropertyValue(PROPERTY_INNER_RANGE, inner);
    }

    /**
     * Get inner range variable.
     * @return Variable.
     */
    public final Property varInnerRange(){
        return getProperty(PROPERTY_INNER_RANGE);
    }

}
