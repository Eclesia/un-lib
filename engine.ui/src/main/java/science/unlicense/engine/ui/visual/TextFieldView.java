
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharIterator;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.font.Font;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.model.TextModel;
import science.unlicense.engine.ui.style.FontStyle;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WidgetStyles;
import static science.unlicense.engine.ui.style.WidgetStyles.getOrEvaluate;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.math.impl.number.Int32Range;

/**
 *
 * @author Johann Sorel
 */
public class TextFieldView extends WidgetView implements TextGesture.TextView {

    private final TextModel model;
    private final TextGesture textControl;

    public TextFieldView(final WTextField widget) {
        super(widget);

        model = widget.getTextModel();
        textControl = new TextGesture(widget.getTextModel(), true);
        textControl.setView(this);

        widget.addEventListener(new PropertyPredicate(Widget.PROPERTY_FOCUSED), new EventListener() {
            public void receiveEvent(Event event) {
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                if (Boolean.FALSE.equals(pe.getNewValue())){
                    //textfield lost focus
                    model.setCaret(-1);
                } else {
                    //start edition
                    CharArray text = widget.getText();
                    model.setCaret(text==null ? 0 : text.getCharLength());
                }
            }
        });
        model.addEventListener(Predicate.TRUE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                widget.setDirty();
            }
        });

    }

    @Override
    public WTextField getWidget() {
        return (WTextField) super.getWidget();
    }

    @Override
    public void getExtents(Extents buffer, Extent constraint) {

        final FontStyle font = WidgetStyles.readFontStyle(getWidget().getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_FONT, getWidget());
        final Chars textSample = (Chars) getOrEvaluate(getWidget().getEffectiveStyleDoc(), StyleDocument.PROPERTY_TEXT_SAMPLE, getWidget());

        if (font != null) {
            final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
            final FontMetadata meta = fontStore.getFont(font.getFont()).getMetaData();
            final Extent ext = meta.getCharsBox(textSample).getExtent();
            ext.set(1, meta.getAscent()+meta.getDescent());
            buffer.setAll(ext);

        } else {
            buffer.setAll(0, 0);
        }

        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }

    protected CharArray getDisplayedText() {
        return getWidget().getEditedText();
    }

    @Override
    public void renderSelf(Painter2D painter, BBox innerBBox) {

        final FontStyle fontStyle = WidgetStyles.readFontStyle(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_FONT, widget);
        final boolean focused = widget.isFocused();

        final int caretPosition = model.getCaret();
        final Int32Range selection = model.getSelection();

        //render the text
        boolean previewText = false;
        CharArray text = getDisplayedText();
        if (text == null || text.isEmpty()) {
            text = getWidget().getPreviewText();
            previewText = true;
        }

        if (text != null && !text.isEmpty() && fontStyle != null) {
            final FontChoice fontChoice = fontStyle.getFont();
            if (fontChoice != null) {
                final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
                painter.setFont(fontChoice);
                //calculate appropriate text position
                final Font font = fontStore.getFont(fontChoice);
                final FontMetadata meta = font.getMetaData();
                final BBox bbox = meta.getCharsBox(text);
                final float xoffset = (float) (innerBBox.getMin(0)-bbox.getMin(0));
                final float yoffset = (float) (innerBBox.getMin(1)+meta.getAscent());

                if (selection != null) {
                    final int sx = selection.getMinimum().toInteger();
                    final int ex = selection.getMaximum().toInteger();
                    if ( (ex-sx) > 0) {
                        final double ascent = meta.getAscent();
                        final double startOffset = xoffset + Font.calculateOffset(font, text, sx);
                        final double endOffset = xoffset + Font.calculateOffset(font, text, ex);
                        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
                        painter.setPaint(fontStyle.getFillPaint());
                        final PlanarGeometry geom = new Rectangle(startOffset+1, yoffset-ascent-1, endOffset-startOffset, ascent);
                        //new Segment(startOffset+1, yoffset+1, startOffset+1, yoffset-ascent-1);
                        painter.stroke(geom);
                    }
                }

                if (previewText) {
                    painter.setAlphaBlending(AlphaBlending.create(AlphaBlending.SRC_OVER, 0.5f));
                }
                if (fontStyle.getFillPaint() != null) {
                    painter.setPaint(fontStyle.getFillPaint());
                    painter.fill(text, xoffset, yoffset);
                }
                if (fontStyle.getBrush() != null && fontStyle.getBrushPaint() != null) {
                    painter.setBrush(fontStyle.getBrush());
                    painter.setPaint(fontStyle.getBrushPaint());
                    painter.stroke(text, xoffset, yoffset);
                }
                if (previewText) {
                    painter.setAlphaBlending(AlphaBlending.DEFAULT);
                }

                //draw the caret
                if (focused && fontStyle.getFillPaint() != null && caretPosition >= 0) {
                    final double ascent = meta.getAscent();
                    final double offset = xoffset + Font.calculateOffset(font, text, caretPosition);
                    painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
                    painter.setPaint(fontStyle.getFillPaint());
                    final PlanarGeometry geom = new DefaultLine(offset+1, yoffset+1, offset+1, yoffset-ascent-1);
                    painter.stroke(geom);
                }

            }
        }
    }

    @Override
    public void receiveEvent(Event event) {
        if (!widget.isStackEnable()) return;

        final EventMessage message = event.getMessage();
        if (message instanceof KeyMessage && widget.isFocused()){
            textControl.update(event);
        } else if (message instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) message;
            if (me.getType() == MouseMessage.TYPE_TYPED){
                if (!widget.isFocused()) widget.requestFocus();
            } else if (widget.isFocused()) {
                textControl.update(event);
            }
        }
    }

    @Override
    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        final Chars name = event.getPropertyName();
        if (  WTextField.PROPERTY_TEXT.equals(name)
          || WTextField.PROPERTY_LANGUAGE.equals(name)
          || WTextField.PROPERTY_EDITED_TEXT.equals(name)
          || WTextField.PROPERTY_FRAME.equals(name)
          || WTextField.PROPERTY_PREVIEW_TEXT.equals(name)){
            widget.updateExtents();
            widget.setDirty();
        } else if (WTextField.PROPERTY_FOCUSED.equals(name)){
            widget.setDirty();
        }
    }

    @Override
    public int getCaretPosition(double mx, double my) {

        final FontStyle fontStyle = WidgetStyles.readFontStyle(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_FONT, widget);
        final BBox innerBBox = widget.getInnerExtent();
        final CharArray edited = getWidget().getEditedText();

        if (edited != null && !edited.isEmpty() && fontStyle != null) {
            final FontChoice font = fontStyle.getFont();
            if (font != null) {
                final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
                final FontMetadata meta = fontStore.getFont(font).getMetaData();

                float yoffset = (float) innerBBox.getMin(1);
                int caretPosition = 0;
                CharArray line = edited;
                final BBox bbox = meta.getCharsBox(line);
                final float xoffset = (float) innerBBox.getMin(0)+1;
                final float lineHeight = (float) (float) bbox.getSpan(1);
                yoffset += lineHeight;
                if (yoffset > my) {
                    //find caret x offset
                    final double spacing = meta.getAdvanceWidth(' ');
                    final CharIterator ite = line.createIterator();

                    double offset = 0.0;
                    int caretChar = 0;
                    while (ite.hasNext()) {
                        final int c = ite.nextToUnicode();
                        final double s;
                        if (c == ' ') {
                            //TODO handle control caracters
                            s = spacing;
                        } else {
                            s = meta.getAdvanceWidth(c);
                        }
                        if (offset+xoffset+s >= mx){
                            caretPosition += caretChar;
                            return caretPosition;
                        }
                        offset += s;
                        caretChar++;
                    }

                    //at this point we know the mouse click was after the end of the line
                    //we place the caret at the last character position
                    caretPosition += caretChar;
                    return caretPosition;
                }
            }
        }

        return -1;
    }

}
