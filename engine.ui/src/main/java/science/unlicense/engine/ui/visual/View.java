
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public interface View extends EventListener{

    /**
     * Render current widget.
     *
     * @param painter
     */
    void render(Painter2D painter);

    /**
     * Render current widget.
     *
     * @param painter
     */
    void renderBack(Painter2D painter);

    /**
     * Render current widget.
     *
     * @param painter
     */
    void renderFront(Painter2D painter);

    void renderEffects(Painter2D painter);

    /**
     * Calculate widget minimum, best and maximum size.
     *
     */
    void getExtents(Extents buffer, Extent constraint);

    /**
     * For the current size, get the visual extent.
     * The visual extent may be larger or smaller, this allows widgets to overlaps.
     *
     * @param buffer
     */
    BBox calculateVisualExtent(BBox buffer);

}
