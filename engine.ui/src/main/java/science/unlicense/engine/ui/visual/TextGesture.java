
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.character.DefaultLChars;
import science.unlicense.common.api.character.LChars;
import science.unlicense.common.api.character.Language;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.impl.GestureState;
import science.unlicense.engine.ui.model.TextModel;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.number.Int32Range;
import science.unlicense.system.api.desktop.Attachment;
import science.unlicense.system.api.desktop.DefaultAttachment;
import science.unlicense.system.api.desktop.TransferBag;

/**
 *
 * @author Johann Sorel
 */
public class TextGesture extends AbstractEventSource {

    public static final Chars PROPERTY_TEXT = Chars.constant("Text");
    public static final Chars PROPERTY_CARET = Chars.constant("Caret");
    public static final Chars PROPERTY_SELECTION = Chars.constant("Selection");

    private final GestureState state = new GestureState();

    private final TextModel model;
    private final boolean monoLine;
    private int linePerPage = 1;
    private TextView view = null;

    public TextGesture(TextModel model, boolean monoLine) {
        this.model = model;
        this.monoLine = monoLine;
    }

    public int getLinePerPage() {
        return linePerPage;
    }

    public void setLinePerPage(int linePerPage) {
        this.linePerPage = linePerPage;
    }

    public TextView getView() {
        return view;
    }

    public void setView(TextView view) {
        this.view = view;
    }


    /**
     *
     * @param event
     */
    public void update(Event event) {
        state.update(event);

        //left,right,up,down
        //begin,end,pageup,pagedown
        //Ctrl-C
        //Ctrl-V
        //Ctrl-X
        //Ctrl-A
        //Shift-left,right,up,down
        //Shift-begin,end,pageup,pagedown

        int caretPosition = model.getCaret();

        final EventMessage message = event.getMessage();
        if (message instanceof KeyMessage){
            final KeyMessage ke = (KeyMessage) message;
            final int type = ke.getType();
            final int code = ke.getCode();
            final int codePoint = ke.getCodePoint();

            if (KeyMessage.TYPE_RELEASE != type) return;

            CharArray edited = model.getText();
            if (edited==null) edited = new Chars("");

            if (state.keyboardPressedControls.contains(KeyMessage.KC_CONTROL)) {

                //TODO for some reason swing convert codepoint to alphabetic values when ctrl is active
                //TODO move this in swing module
                if (codePoint == 1) { // a
                    final CharArray text = model.getText();
                    model.setSelection(new Int32Range(0, text.getCharLength(), true, false));
                } else if (codePoint == 3) { // c
                    final TransferBag clipBoard = science.unlicense.system.System.get().getClipBoard();
                    clipBoard.getAttachments().removeAll();

                    final Attachment attachment = new DefaultAttachment(model.getSelectedText(), new Chars("text"));
                    clipBoard.getAttachments().add(attachment);
                } else if (codePoint == 22) { // v
                    final TransferBag clipBoard = science.unlicense.system.System.get().getClipBoard();

                    Attachment att = clipBoard.getAttachment(null, CharArray.class);
                    if (att != null) {
                        final CharArray toInsert = (CharArray) att.getObject();
                        Chars t = edited.truncate(0, caretPosition).concat(toInsert).concat(edited.truncate(caretPosition, edited.getCharLength())).toChars();
                        model.setText(copyAndChange(t, edited));
                        model.setCaret(caretPosition+toInsert.getCharLength());
                        //erase selection
                        model.setSelection(new Int32Range(0, 0, true, false));
                    }

                } else if (codePoint == 24) { // x
                    Int32Range selection = model.getSelection();

                    //copy selection to clipboard
                    final TransferBag clipBoard = science.unlicense.system.System.get().getClipBoard();
                    clipBoard.getAttachments().removeAll();
                    final Attachment attachment = new DefaultAttachment(model.getSelectedText(), new Chars("text"));
                    clipBoard.getAttachments().add(attachment);

                    //remove text
                    Chars t = edited.truncate(0, selection.getMinimum().toInteger()).concat(edited.truncate(selection.getMaximum().toInteger(), edited.getCharLength())).toChars();
                    model.setText(copyAndChange(t, edited));

                    //erase selection
                    model.setSelection(new Int32Range(0, 0, true, false));
                    //move caret
                    model.setCaret(selection.getMinimum().toInteger());

                }

            } else if (state.keyboardPressedControls.contains(KeyMessage.KC_SHIFT)) {

            } else {
                if (code==KeyMessage.KC_BACKSPACE){
                    if (caretPosition==0 || edited.isEmpty()) return;
                    //erase previous char
                    Chars t = edited.truncate(0, caretPosition-1).concat(edited.truncate(caretPosition, edited.getCharLength())).toChars();
                    model.setCaret(caretPosition-1);
                    model.setText(copyAndChange(t, edited));

                } else if (code==KeyMessage.KC_DELETE){
                    if (edited.isEmpty() || caretPosition>=edited.getCharLength()) return;
                    //erase next char
                    Chars t = edited.truncate(0, caretPosition).concat(edited.truncate(caretPosition+1, edited.getCharLength())).toChars();
                    model.setText(copyAndChange(t, edited));

                } else if (code==KeyMessage.KC_ENTER){
                    if (!monoLine) {
                        Chars t = edited.truncate(0, caretPosition).concat('\n').concat(edited.truncate(caretPosition, edited.getCharLength())).toChars();
                        model.setText(copyAndChange(t, edited));
                        model.setCaret(caretPosition+1);
                    }

                } else if (code==KeyMessage.KC_BEGIN){
                    //move caret to line start
                    final CharArray[] split = edited.split('\n');
                    int line = 0;
                    for (int i=0;i<split.length;i++){
                        if (line+split[i].getCharLength() >= caretPosition){
                            model.setCaret(line);
                            break;
                        }
                        line += split[i].getCharLength()+1;
                    }

                } else if (code==KeyMessage.KC_END){
                    //move caret to line end
                    final CharArray[] split = edited.split('\n');
                    int line = 0;
                    for (int i=0;i<split.length;i++){
                        if (line+split[i].getCharLength() >= caretPosition){
                            model.setCaret(line+split[i].getCharLength());
                            break;
                        }
                        line += split[i].getCharLength()+1;
                    }

                } else if (code==KeyMessage.KC_LEFT){
                    model.setCaret(Maths.max(caretPosition-1, 0));

                } else if (code==KeyMessage.KC_RIGHT){
                    model.setCaret(Maths.min(caretPosition+1, edited.getCharLength()));

                } else if (code==KeyMessage.KC_UP){
                    if (!monoLine) {
                        //move line caret position
                        final CharArray[] split = edited.split('\n');
                        int line = 0;
                        int car = 0;
                        for (int i=0;i<split.length;i++){
                            if (line+split[i].getCharLength() >= caretPosition){
                                car = caretPosition-line;
                                //move caret on the upper line
                                if (i>0){
                                    line -= (split[i-1].getCharLength()+1);
                                    car = Math.min(split[i-1].getCharLength(), car);
                                    model.setCaret(line+car);
                                }
                                break;
                            }
                            line += split[i].getCharLength()+1;
                        }
                    }

                } else if (code==KeyMessage.KC_DOWN){
                    if (!monoLine) {
                        //move line caret position
                        final CharArray[] split = edited.split('\n');
                        int line = 0;
                        int car = 0;
                        for (int i=0;i<split.length;i++){
                            if (line+split[i].getCharLength() >= caretPosition){
                                car = caretPosition-line;
                                line += split[i].getCharLength()+1;
                                //move caret on the lower line
                                if (i<split.length-1){
                                    car = Math.min(split[i+1].getCharLength(), car);
                                    model.setCaret(line+car);
                                }
                                break;
                            }
                            line += split[i].getCharLength()+1;
                        }
                    }
                } else if (codePoint>0){
                    Chars t = edited.truncate(0, caretPosition).concat(codePoint).concat(edited.truncate(caretPosition, edited.getCharLength())).toChars();
                    model.setText(copyAndChange(t, edited));
                    model.setCaret(caretPosition+1);
                }
            }


        } else if (message instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) message;
            if (me.getType() == MouseMessage.TYPE_TYPED){

                //move caret position to mouse click
                double mx = me.getMousePosition().get(0);
                double my = me.getMousePosition().get(1);

                if (view != null) {
                    int caretPos = view.getCaretPosition(mx, my);
                    if (caretPos >= 0) {
                        model.setCaret(caretPosition);
                    }
                }
            }
        }

    }

    private static CharArray copyAndChange(Chars newText, CharArray ca){

        if (ca instanceof LChars){
            final LChars lc = (LChars) ca;
            final Dictionary dico = new HashDictionary();
            final Language def = lc.getLanguage();
            final Iterator ite = lc.getLanguages();
            while (ite.hasNext()){
                final Language lg = (Language) ite.next();
                if (lg.equals(def)){
                    //change the default translation value
                    dico.add(lg, newText);
                } else {
                    dico.add(lg, lc.translate(lg).toChars());
                }
            }
            return new DefaultLChars(dico, def);
        } else {
            return newText;
        }

    }

    public interface TextView {

        public int getCaretPosition(double x, double y);

    }

}
