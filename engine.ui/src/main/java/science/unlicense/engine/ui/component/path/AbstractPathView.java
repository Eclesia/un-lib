
package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicText;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class AbstractPathView extends WContainer implements PathView {

    static final Chars FONT_STYLE = new Chars("subs : {\nfilter:true\ngraphic:none\nfont:{\nfill-paint:@color-main\nfamily:font('Unicon' 14 'none')\n}\n}");
    static final Path[] DEFAULT_SELECTED_PATHS = new Path[0];

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(PROPERTY_HIDDEN_FILE_VISIBLE, Boolean.FALSE);
        defs.add(PROPERTY_MULTISELECTION, Boolean.FALSE);
        defs.add(PROPERTY_SELECTED_PATHS, DEFAULT_SELECTED_PATHS);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private final Widget icon;
    private final Chars title;

    public AbstractPathView(Widget icon, Chars title) {
        this.icon = icon;
        this.title = title;
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Widget getIcon() {
        return icon;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Chars getTitle() {
        return title;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final void setHiddenFileVisible(boolean showHidden) {
        setPropertyValue(PROPERTY_HIDDEN_FILE_VISIBLE, showHidden);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final boolean isHiddenFileVisible() {
        return (Boolean) getPropertyValue(PROPERTY_HIDDEN_FILE_VISIBLE);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Property varHiddenFileVisible(){
        return getProperty(PROPERTY_HIDDEN_FILE_VISIBLE);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final void setMultiSelection(boolean multiple) {
        setPropertyValue(PROPERTY_MULTISELECTION, multiple);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final boolean isMultiSelection() {
        return (Boolean) getPropertyValue(PROPERTY_MULTISELECTION);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Property varMultiSelection(){
        return getProperty(PROPERTY_MULTISELECTION);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final void setViewRoot(Path viewRoot) {
        setPropertyValue(PROPERTY_VIEW_ROOT, viewRoot);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Path getViewRoot() {
        return (Path) getPropertyValue(PROPERTY_VIEW_ROOT);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Property varViewRoot(){
        return getProperty(PROPERTY_VIEW_ROOT);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final void setFilter(Predicate filter) {
        setPropertyValue(PROPERTY_FILTER, filter);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Predicate getFilter() {
        return (Predicate) getPropertyValue(PROPERTY_FILTER);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Property varFilter(){
        return getProperty(PROPERTY_FILTER);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final void setSelectedPath(Path[] path) {
        setPropertyValue(PROPERTY_SELECTED_PATHS, path);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Path[] getSelectedPath() {
        return (Path[]) getPropertyValue(PROPERTY_SELECTED_PATHS);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Property varSelectedPath(){
        return getProperty(PROPERTY_SELECTED_PATHS);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Widget getViewWidget() {
        return this;
    }

    /**
     * Combine hidden and filter predicates
     * @param folder
     * @return
     */
    protected Predicate getFilterInternal(final boolean folder){
        final boolean showHiddenFiles = isHiddenFileVisible();
        final Predicate filter = getFilter();
        return new Predicate() {
            public Boolean evaluate(Object candidate) {
                final Path p = (Path) candidate;
                if (!(showHiddenFiles || !Boolean.TRUE.equals(p.getPathInfo(Path.INFO_HIDDEN)))){
                    return false;
                }
                try {
                    if (folder && p.isContainer()) return true;
                } catch (IOException ex) {}
                return (filter==null || filter.evaluate(candidate));
            }
        };
    }

    static boolean equals(Path[] p1, Path[] p2){
        if (p1==null && p2==null) return true;
        if (p1==null || p2==null) return false;
        return Arrays.equals(p1, p2);
    }

    static WGraphicText createGlyph(String code){
        final WGraphicText t = new WGraphicText(new Chars(code));
        t.setInlineStyle(FONT_STYLE);
        return t;
    }

}
