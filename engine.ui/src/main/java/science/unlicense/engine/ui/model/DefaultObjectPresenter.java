
package science.unlicense.engine.ui.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.DefaultNode;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.ViewNode;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;

/**
 * Default cell presenter, does not provide edition support.
 * Objects are display as Chars in a WLabel.
 *
 * @author Johann Sorel
 */
public class DefaultObjectPresenter implements ObjectPresenter {

    public Widget createWidget(Object candidate) {
        final WLabel label = new WLabel();
        label.setVerticalAlignment(WLabel.VALIGN_CENTER);

        if (candidate instanceof Node){
            candidate = ViewNode.unWrap((Node) candidate);
        }

        if (candidate instanceof NamedNode){
            label.setText( ((NamedNode) candidate).getName());
        } else if (candidate instanceof Path){
            label.setText( new Chars(((Path) candidate).getName()));
        } else if (candidate instanceof DefaultNode){
            label.setText( ((DefaultNode) candidate).toChars());
        } else {
            label.setText(new Chars(String.valueOf(candidate)));
        }
        if (label.getText().getCharLength()==0){
            label.setText(new Chars(" "));
        }

        return label;
    }

}
