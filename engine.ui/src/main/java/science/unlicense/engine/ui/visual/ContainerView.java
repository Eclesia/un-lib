
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.model.doc.DocMessage;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.Layout;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public class ContainerView extends WidgetView {

    public ContainerView(WContainer widget) {
        super(widget);
    }

    @Override
    public WContainer getWidget() {
        return (WContainer) super.getWidget();
    }

    @Override
    public void getExtents(Extents buffer, Extent constraint) {
        final Layout layout = getWidget().getLayout();
        if (layout == null) return;
        layout.getExtents(buffer, constraint);
    }

    @Override
    public void receiveStyleEvent(DocMessage event) {
        super.receiveStyleEvent(event);
        Layout layout = getWidget().getLayout();
        layout.setView(getWidget().getInnerExtent());
    }

//    public void receiveWidgetEvent(PropertyMessage event) {
//        super.receiveWidgetEvent(event);
//
//        final Chars name = event.getPropertyName();
//        if ( AbstractLabeled.PROPERTY_FRAME.equals(name)
//                ){
//            graphic = null;
//            widget.updateExtents();
//            widget.setDirty();
//        }
//    }
}
