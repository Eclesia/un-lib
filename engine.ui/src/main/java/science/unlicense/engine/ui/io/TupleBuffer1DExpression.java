
package science.unlicense.engine.ui.io;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.AbstractTupleGrid1D;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public class TupleBuffer1DExpression extends AbstractTupleGrid1D {

    private final Sequence coords;
    private Widget widget;

    public TupleBuffer1DExpression() {
        this.coords = null;
    }

    public TupleBuffer1DExpression(Sequence tuples) {
        this.coords = tuples;
        updateModel(new Extent.Long((long) tuples.getSize()), Float64.TYPE, ((Tuple) tuples.get(0)).getSampleCount());
    }

    public Widget getWidget() {
        return widget;
    }

    public void setWidget(Widget widget) {
        this.widget = widget;
    }

    @Override
    public int getDimension() {
        return (int) getExtent().getL(0);
    }

    public void evaluate(Object widget){
        for (int i=0,n=coords.getSize();i<n;i++){
            final TupleExpression te = (TupleExpression) coords.get(i);
            te.evaluate(widget);
        }
    }

    @Override
    public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
        final Tuple t = (Tuple) coords.get( (int) coordinate.get(0));
        buffer.set(t);
        return buffer;
    }

    @Override
    public void setTuple(Tuple coordinate, Tuple buffer) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
