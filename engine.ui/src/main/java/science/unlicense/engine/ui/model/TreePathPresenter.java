
package science.unlicense.engine.ui.model;

import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public interface TreePathPresenter {

    public static final int TYPE_EMPTY = 0;
    public static final int TYPE_MIDDLE = 1;
    public static final int TYPE_LINE = 2;
    public static final int TYPE_END = 3;

    double getDepthWidth();

    Widget createPathWidget(int[] depths);

}
