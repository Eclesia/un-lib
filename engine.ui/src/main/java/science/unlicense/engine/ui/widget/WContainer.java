
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Hasher;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Triplet;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.api.layout.Layout;
import science.unlicense.display.api.layout.LayoutConstraint;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.visual.ContainerView;
import static science.unlicense.engine.ui.widget.Widget.PROPERTY_CHILDFOCUSED;
import static science.unlicense.engine.ui.widget.Widget.PROPERTY_DIRTY;
import static science.unlicense.engine.ui.widget.Widget.PROPERTY_FOCUSED;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2f64;

/**
 * Widget containing other widgets.
 *
 * @author Johann Sorel
 */
public class WContainer extends AbstractWidget {

    /**
     * Property indicating when the layout changed.
     */
    public static final Chars PROPERTY_LAYOUT = Chars.constant("Layout");
    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(AbstractWidget.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("container")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private Layout layout;

    //rendering state
    private volatile boolean updatingLayout = false;
    private volatile boolean updatingContent = false;

    //last mouse in widget
    private HashSet oldMouseWidgets = new HashSet(Hasher.IDENTITY,1);
    private HashSet newMouseWidgets = new HashSet(Hasher.IDENTITY,1);

    private final EventListener layoutListener = new EventListener() {
        @Override
        public void receiveEvent(Event event) {
            if (isUpdating() || updatingLayout) return;
            updateLayoutUpdate();
        }
    };

    public WContainer() {
        this(new AbsoluteLayout());
    }

    public WContainer(Layout layout) {
        super(true);
        setView(new ContainerView(this));
        setLayout(layout);
    }

    @Override
    public void notifyInlineStyleChanged() {
        super.notifyInlineStyleChanged();
        //margins may have changed
        updateLayoutSize(getInnerExtent());
    }

    @Override
    public void notifyStyleChanged() {
        super.notifyStyleChanged();
        //margins may have changed
        updateLayoutSize(getInnerExtent());
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public void addChild(Widget child, LayoutConstraint constraint) {
        child.setLayoutConstraint(constraint);
        super.getChildren().add(child);
    }

    /**
     * Get container layout.
     * @return Layout never null
     */
    public Layout getLayout() {
        return layout;
    }

    /**
     * Set container layout.
     * @param layout must not be null
     */
    public void setLayout(Layout layout) {
        if (this.layout!=null){
            this.layout.removeEventListener(new PropertyPredicate(Layout.PROPERTY_DIRTY), layoutListener);
            this.layout.setPositionables(null);
        }
        final Layout old = this.layout;
        this.layout = layout;
        this.layout.addEventListener(new PropertyPredicate(Layout.PROPERTY_DIRTY), layoutListener);
        this.layout.setPositionables(children);
        updateLayoutSize(getInnerExtent());
        sendPropertyEvent(PROPERTY_LAYOUT, old, layout);
    }

    private void updateLayoutUpdate(){
        updatingLayout = true;
        updateExtents();
        layout.update();
        setDirty();
        updatingLayout = false;
    }

    private boolean isUpdating() {
        if (updatingContent) return true;
        if (parent instanceof WContainer) {
            return ((WContainer) parent).isUpdating();
        }
        return false;
    }

    protected void startUpdatingContent(){
        updatingContent = true;
    }

    protected void endUpdatingContent(){
        updatingContent = false;
        updateLayoutUpdate();
    }

    private void updateLayoutSize(BBox innerExtent){
        //TODO : listen to style changes
        if (layout!=null){
            this.layout.setView(innerExtent);
        }
    }

    /**
     * Passthrough event to children.
     *
     * @param event
     */
    @Override
    protected void receiveEventParent(Event event) {

        //event comes from the parent
        if (event.getMessage() instanceof MouseMessage){
            final MouseMessage mevent = (MouseMessage) event.getMessage();

            HashSet temp = oldMouseWidgets;
            oldMouseWidgets = newMouseWidgets;
            newMouseWidgets = temp;
            newMouseWidgets.removeAll();

            //clip mouse events to forward to correct childrens
            final BBox searchBBox = new BBox((TupleRW) mevent.getMousePosition(), (TupleRW) mevent.getMousePosition());

            final Layout layout = getLayout();
            for (Iterator ite=layout.getPositionables(searchBBox);ite.hasNext();){
                final Triplet trio                  = (Triplet) ite.next();
                final Widget child                  = (Widget) trio.getValue1();
                if (Widgets.forwardEvent(this, child, mevent, oldMouseWidgets.remove(child))){
                    newMouseWidgets.add(child);
                }
            }

            //send exit event on old widgets
            final Iterator oldIte = oldMouseWidgets.createIterator();
            while (oldIte.hasNext()){
                Widget old = (Widget) oldIte.next();
                Widgets.forwardEvent(this, old, mevent, true);
            }
            oldMouseWidgets.removeAll();
        }

        //send event to listeners
        super.receiveEventParent(event);
    }

    @Override
    protected void receiveEventChildren(Event event) {
        if (isUpdating() || updatingLayout){
            //when updating a layout there may be a lot of such events
            //we wait until update is finished to send a global event
            return;
        }

        if (event.getMessage() instanceof PropertyMessage){
            final PropertyMessage pevent = (PropertyMessage) event.getMessage();
            final Widget widget = (Widget) event.getSource();

            if (Similarity.PROPERTY_MATRIX.equals(pevent.getPropertyName())){
                //child transform changed
                //calculate the dirty area
                final Affine2 oldMatrix = new Affine2((Matrix) pevent.getOldValue());
                final Affine2 newMatrix = new Affine2((Matrix) pevent.getNewValue());
                final BBox bbox = widget.getView().calculateVisualExtent(null);
                final BBox dirty = Geometries.transform(bbox, oldMatrix, null);
                dirty.expand(Geometries.transform(bbox, newMatrix, bbox));
                setDirty(dirty);

            } else if (PROPERTY_DIRTY.equals(pevent.getPropertyName())){
                final BBox dirtyExt = (BBox) pevent.getNewValue();
                if (!widget.getEffectiveExtent().isEmpty()){
                    //convert from child space to this widget coordinate space
                    final Affine m = widget.getNodeTransform();
                    setDirty(Geometries.transform(dirtyExt, m, null));
                } else {
                    setDirty();
                }
            } else if (PROPERTY_CHILDFOCUSED.equals(pevent.getPropertyName())){
                setChildFocused((Boolean) pevent.getNewValue());
            } else if (PROPERTY_FOCUSED.equals(pevent.getPropertyName())){
                setChildFocused((Boolean) pevent.getNewValue());
            }
        }
    }

    @Override
    public Tuple getOnSreenPosition(Widget child) {
        final Tuple point = getOnSreenPosition();
        if (point==null){
            //not displayed
            return null;
        }

        final Similarity trs = child.getNodeTransform();
        final Extent extent = child.getEffectiveExtent();
        final Extent thisExt = getEffectiveExtent();

        if (!extent.isEmpty()){
            double[] position = new double[]{0, 0, 1};
            trs.transform(position, 0, position, 0, 1);
            return new Vector2f64(
                    point.get(0)+((thisExt.get(0)/2.0)+position[0]-extent.get(0)/2.0),
                    point.get(1)+((thisExt.get(1)/2.0)+position[1]-extent.get(1)/2.0));

        } else {
            return null;
        }
    }

    @Override
    protected void sendPropertyEvent(Chars name, Object oldValue, Object newValue) {
        super.sendPropertyEvent(name, oldValue, newValue);
        if (PROPERTY_EFFECTIVE_EXTENT.equals(name)){
            updateLayoutSize(getInnerExtent(this, (Extent) newValue));
        }
    }

}
