
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.anim.DefaultTimer;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.PairLayout;
import science.unlicense.display.impl.anim.TransformAnimation;
import science.unlicense.display.impl.anim.TransformTimeSerieHelper;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.geometry.impl.transform.Projections;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.transform.Transform;

/**
 * Widget loader.
 *
 * @author Johann Sorel
 */
public class WLoader extends WContainer {

    public static final int GRAPHIC_TOP = 0;
    public static final int GRAPHIC_RIGHT = 1;
    public static final int GRAPHIC_BOTTOM = 2;
    public static final int GRAPHIC_LEFT = 3;

    public static final int HALIGN_LEFT = 0;
    public static final int HALIGN_CENTER = 1;
    public static final int HALIGN_RIGHT = 2;

    public static final int VALIGN_TOP = 0;
    public static final int VALIGN_CENTER = 1;
    public static final int VALIGN_BOTTOM = 2;

    /**
     * Property for the widget text.
     */
    public static final Chars PROPERTY_TEXT = Chars.constant("Text");
    /**
     * Property for the widget image.
     */
    public static final Chars PROPERTY_GRAPHIC = Chars.constant("Graphic");
    /**
     * Property for the image placement.
     */
    public static final Chars PROPERTY_IMAGE_PLACEMENT = Chars.constant("GraphicPlacement");
    /**
     * Property for the horizontal alignement.
     */
    public static final Chars PROPERTY_HALIGN = Chars.constant("HorizontalAlignment");
    /**
     * Property for the vertical alignment.
     */
    public static final Chars PROPERTY_VALIGN = Chars.constant("VerticalAlignment");

    private final WGraphicText textWidget;
    private final WGraphicGeometry imgWidget;
    private final PairLayout pairLayout;

    public WLoader() {
        this(null);
    }

    public WLoader(CharArray text) {
        textWidget = new WGraphicText(text);
        varText().sync(textWidget.varText());

        final WContainer freePane = new WContainer(new AbsoluteLayout());
        freePane.setOverrideExtents(new Extents(30, 30));

        final FontChoice font = new FontChoice(new Chars[]{new Chars("Unicon")}, 48, FontChoice.WEIGHT_NONE);
        PlanarGeometry glyph = getFrame().getPainter().getFontStore().getFont(font).getGlyph('\uE029');
        Transform mtr = Projections.scaled(glyph.getBoundingBox(), new BBox(new double[]{-15,-15}, new double[]{15,15}));
        glyph = TransformedGeometry.create(glyph, mtr);
        imgWidget = new WGraphicGeometry();
        imgWidget.setGeometry(glyph);
        imgWidget.setOverrideExtents(new Extents(30, 30));
        freePane.getChildren().add(imgWidget);

        TransformTimeSerieHelper helper = new TransformTimeSerieHelper(2);
        TransformAnimation trs = helper.on(imgWidget).repeat(0)
                .at(0).rotation(Maths.HALF_PI)
                .at(1000).rotation(Maths.HALF_PI)
                .at(1500).rotation(Maths.HALF_PI)
                .at(2000).rotation(Maths.HALF_PI)
                .build();
        trs.setTimer(new DefaultTimer(50));
        trs.start();

        pairLayout = new PairLayout();
        setLayout(pairLayout);
        if (text!=null && !text.isEmpty()){
            children.add(textWidget);
        }
        children.add(freePane);
    }

    /**
     * Get label text.
     * @return CharArray, can be null
     */
    public final CharArray getText() {
        return (CharArray) getPropertyValue(PROPERTY_TEXT);
    }

    /**
     * Set label text.
     * @param text can be null
     */
    public final void setText(CharArray text) {
        setPropertyValue(PROPERTY_TEXT, text);
    }

    /**
     * Get text variable.
     * @return Variable.
     */
    public final Property varText() {
        return getProperty(PROPERTY_TEXT);
    }

    /**
     * Get graphic placement relative to text
     * @return int
     */
    public int getGraphicPlacement() {
        return pairLayout.getRelativePosition();
    }

    /**
     * Set graphic placement relative to text
     * @param place
     */
    public void setGraphicPlacement(int place) {
        pairLayout.setRelativePosition(place);
    }

    /**
     * Get text and graphic horizontal alignment.
     * @return int
     */
    public int getHorizontalAlignment() {
        return pairLayout.getHorizontalAlignement();
    }

    /**
     * Set text and graphic horizontal alignment.
     * @param align
     */
    public void setHorizontalAlignment(int align) {
       pairLayout.setHorizontalAlignement(align);
    }

    /**
     * Get text and graphic vertical alignment.
     * @return int
     */
    public int getVerticalAlignment() {
        return pairLayout.getVerticalAlignement();
    }

    /**
     * Set text and graphic vertical alignment.
     * @param align
     */
    public void setVerticalAlignment(int align) {
        pairLayout.setVerticalAlignement(align);
    }

    @Override
    protected void receiveEventChildren(Event event) {
        super.receiveEventChildren(event);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_TEXT.equals(name)) {
            CharArray text = (CharArray) value;
            if (text == null || text.isEmpty()) {
                children.remove(textWidget);
            } else {
                if (getChildren().getSize() == 0 || getChildren().get(0) != textWidget){
                    children.add(0,textWidget);
                }
            }
        }
    }

}
