
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.visual.GraphicTextView;

/**
 * Widget holding a single text.
 *
 * @author Johann Sorel
 */
public class WGraphicText extends WLeaf {

    /**
     * Property for the widget text.
     */
    public static final Chars PROPERTY_TEXT = Chars.constant("Text");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLeaf.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("graphictext")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WGraphicText() {
        this(null);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public WGraphicText(CharArray text) {
        setPropertyValue(PROPERTY_TEXT, text);
        setView(new GraphicTextView(this));
    }

    /**
     * Get graphic text.
     * @return CharArray, can be null
     */
    public final CharArray getText() {
        return (CharArray) getPropertyValue(PROPERTY_TEXT);
    }

    /**
     * Set graphic text.
     * @param text can be null
     */
    public final void setText(CharArray text) {
        setPropertyValue(PROPERTY_TEXT,text);
    }

    /**
     * Get text variable.
     * @return Variable.
     */
    public final Property varText() {
        return getProperty(PROPERTY_TEXT);
    }

}
