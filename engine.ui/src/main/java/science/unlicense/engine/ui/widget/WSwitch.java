
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.ievent.ActionExecutable;
import science.unlicense.engine.ui.style.StyleDocument;

/**
 * A two state button.
 *
 * @author Johann Sorel
 */
public class WSwitch extends WButton{

    /**
     * Property for the widget check value.
     */
    public static final Chars PROPERTY_CHECK = WCheckBox.PROPERTY_CHECK;

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WButton.CLASS_DEFAULTS);
        defs.add(PROPERTY_CHECK, Boolean.FALSE);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("switch")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WSwitch() {
    }

    public WSwitch(CharArray text){
        super(text);
    }

    public WSwitch(CharArray text, Widget graphic){
        super(text, graphic);
    }

    public WSwitch(CharArray text, Widget graphic, EventListener lst){
        super(text, graphic, lst);
    }

    public WSwitch(ActionExecutable exec){
        super(exec);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    /**
     * Set check value.
     * @param check
     */
    public final void setCheck(boolean check) {
        setPropertyValue(PROPERTY_CHECK,check);
    }

    /**
     * Get check value.
     * @return boolean
     */
    public final boolean isCheck() {
        //test equality, value could be null at first
        return (Boolean) getPropertyValue(PROPERTY_CHECK);
    }

    /**
     * Get check variable.
     * @return Variable.
     */
    public final Property varCheck(){
        return getProperty(PROPERTY_CHECK);
    }

    /**
     * Overload click event to change button state.
     */
    public void doClick() {
        setCheck(!isCheck());
        super.doClick();
    }

}
