
package science.unlicense.engine.ui.style;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.engine.ui.widget.Widget;

/**
 * Redirect expression evaluation to referenced expression.
 *
 * @author Johann Sorel
 */
public class ExpressionReference implements Expression {

    private final Chars referenceProperty;

    public ExpressionReference(Chars.Constant referenceProperty) {
        this.referenceProperty = referenceProperty;
    }

    public Chars getReferenceProperty() {
        return referenceProperty;
    }

    @Override
    public Object evaluate(Object candidate) {
        final Widget w = (Widget) candidate;
        final Document style = w.getEffectiveStyleSheet();
        Object value = style.getPropertyValue(referenceProperty);
        if (value instanceof Expression) {
            value = ((Expression) value).evaluate(candidate);
        }
        return value;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.referenceProperty != null ? this.referenceProperty.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExpressionReference other = (ExpressionReference) obj;
        if (this.referenceProperty != other.referenceProperty && (this.referenceProperty == null || !this.referenceProperty.equals(other.referenceProperty))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return referenceProperty.toString();
    }

}
