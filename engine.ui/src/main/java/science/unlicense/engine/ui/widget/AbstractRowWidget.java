
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.CollectionMessage;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Hasher;
import science.unlicense.common.api.collection.HasherDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.DisplacementLayout;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.model.Column;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.DefaultTreeColumn;
import science.unlicense.engine.ui.model.RowModel;
import science.unlicense.engine.ui.model.TreeRowModel;
import science.unlicense.engine.ui.model.WRow;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.Tuple;

/**
 * Abstract row widget.
 * Use by :
 * - WTree
 * - WTable
 * - WTreeTable
 *
 * @author Johann Sorel
 */
public abstract class AbstractRowWidget extends WContainer {

    public static final Chars STYLE_HEADERS = Chars.constant("RowWidget-headers");
    public static final Chars STYLE_HEADER = Chars.constant("RowWidget-header");
    public static final Chars STYLE_ROW = Chars.constant("RowWidget-row");
    public static final Chars STYLE_CELL = Chars.constant("RowWidget-cell");

    public static final int SELECTION_SINGLE = 1;
    public static final int SELECTION_MULTIPLE = 2;
    public static final int SCROLL_VISIBLE_NEEDED = 0;
    public static final int SCROLL_VISIBLE_ALWAYS = 1;
    public static final int SCROLL_VISIBLE_NEVER = 2;

    public static final Chars PROPERTY_ROWHEIGHT = Chars.constant("RowHeight");
    public static final Chars PROPERTY_ROWMODEL = Chars.constant("RowModel");
    public static final Chars PROPERTY_SELECTIONTYPE = Chars.constant("SelectionType");
    public static final Chars PROPERTY_VERTICAL_SCROLL_VISIBILITY = Chars.constant("VerticalScrollVisibility");
    public static final Chars PROPERTY_HORIZONTAL_SCROLL_VISIBILITY = Chars.constant("HorizontalScrollVisibility");

    private static final double DEFAULT_ROW_HEIGHT = 24;

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(PROPERTY_SELECTIONTYPE, SELECTION_SINGLE);
        defs.add(PROPERTY_VERTICAL_SCROLL_VISIBILITY, SCROLL_VISIBLE_NEEDED);
        defs.add(PROPERTY_HORIZONTAL_SCROLL_VISIBILITY, SCROLL_VISIBLE_NEEDED);
        defs.add(PROPERTY_ROWHEIGHT, DEFAULT_ROW_HEIGHT);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private final Sequence columns = new ArraySequence();
    private final Sequence selection = new ArraySequence();

    //currently visible rows
    private final WContainer headerContainer = new WContainer(new DisplacementLayout());
    private final WContainer innerContainer = new WContainer(new DisplacementLayout());
    private final WScrollBar verticalScrollBar = new WScrollBar(false);
    private final WScrollBar horizontalScrollBar = new WScrollBar(true);
    private final WContainer fill = new WContainer();

    private final FormLayout headersLayout = new FormLayout();
    private final WContainer headersPane = new WContainer(headersLayout);
    private final FormLayout cellLayout = new FormLayout();
    private final WContainer cellPane = new WContainer(cellLayout);


    //listener for varrious events that may update the content
    private final EventListener eventListener = new EventListener() {
        public void receiveEvent(Event event) {
            if (event.getSource().equals(horizontalScrollBar)){
                updateHorizontalDisplacement();
            } else {
                updateContent();
            }
        }
    };
    private final EventListener columnWidthListener = new EventListener() {
        public void receiveEvent(Event event) {
            final Widget header = (Widget) event.getSource();
            final Sequence columns = getColumns();
            for (int i=0,n=columns.getSize();i<n;i++){
                final Column col = (Column) columns.get(i);
                if (col.getHeader()==header){
                    cellLayout.setColumnSize(i, header.getEffectiveExtent().get(0));
                    break;
                }
            }
        }
    };
    private final EventListener rowSelectionListener = new EventListener() {
        public void receiveEvent(Event event) {
            final MouseMessage me = (MouseMessage) event.getMessage();
            if (!me.isConsumed() && me.getButton()==MouseMessage.BUTTON_1 && me.getType()==MouseMessage.TYPE_RELEASE){
                final WRow row = ((Over) event.getSource()).row;
                selection.replaceAll(new Object[]{row.getValue()});
            }
        }
    };

    //gesture informations
    protected boolean ctrlActive = false;

    //resizing column info
    private Widget resizingHeader = null;
    private Extent baseSize = null;
    private double startx = 0;


    public AbstractRowWidget() {
        this(new DefaultRowModel());
    }

    public AbstractRowWidget(final RowModel rowModel) {
        CObjects.ensureNotNull(rowModel);
        rowModel.addEventListener(CollectionMessage.PREDICATE, eventListener);
        setPropertyValue(PROPERTY_ROWMODEL, rowModel);

        final FormLayout layout = new FormLayout();
        layout.setColumnSize(0, FormLayout.SIZE_EXPAND);
        layout.setColumnSize(1, FormLayout.SIZE_AUTO);
        layout.setRowSize(1, FormLayout.SIZE_EXPAND);
        layout.setRowSize(2, FormLayout.SIZE_AUTO);
        setLayout(layout);

        fill.setPropertyValue(XPROP_STYLEMARKER, STYLE_HEADERS);
        fill.setOverrideExtents(new Extents(0, 0, 0, 0, Double.MAX_VALUE, Double.MAX_VALUE));

        //pull scroll bars out of the scroll container and place them on the sides
        addChild(headerContainer,       FillConstraint.builder().coord(0, 0).fill(true, true).align(0, 0.5f).build());
        addChild(fill,                  FillConstraint.builder().coord(1, 0).fill(true, true).build());
        addChild(innerContainer,        FillConstraint.builder().coord(0, 1).fill(true, true).build());
        addChild(verticalScrollBar,     FillConstraint.builder().coord(1, 1).fill(true, true).build());
        addChild(horizontalScrollBar,   FillConstraint.builder().coord(0, 2).fill(true, true).build());
        horizontalScrollBar.setReserveSpace(false);
        verticalScrollBar.setReserveSpace(false);

        //fill corners
        final WSpace cornerBottom = new WSpace();
        addChild(cornerBottom,FillConstraint.builder().coord(1, 2).build());

        //listen to colun change
        columns.addEventListener(CollectionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                columnChanged((CollectionMessage) event.getMessage());
            }
        });

        //organisation
        verticalScrollBar.addEventListener(new PropertyPredicate(WScrollBar.PROPERTY_RATIO), eventListener);
        horizontalScrollBar.addEventListener(new PropertyPredicate(WScrollBar.PROPERTY_RATIO), eventListener);
        innerContainer.addEventListener(new PropertyPredicate(Widget.PROPERTY_EFFECTIVE_EXTENT), eventListener);
        selection.addEventListener(CollectionMessage.PREDICATE, eventListener);

        //configure header pane
        ((DisplacementLayout) headerContainer.getLayout()).setFillWidth(true);
        headerContainer.getChildren().add(headersPane);
        headerContainer.setOverrideExtents(new Extents(1,Double.NaN, 1, Double.NaN, Double.NaN, Double.NaN));

        //configure inner pane
        ((DisplacementLayout) innerContainer.getLayout()).setFillWidth(true);
        innerContainer.setOverrideExtents(new Extents(1,1, Double.NaN, Double.NaN, Double.NaN, Double.NaN));
        innerContainer.getChildren().add(cellPane);


        //attach listener for header column resizing
        headerContainer.setPropertyValue(XPROP_STYLEMARKER, STYLE_HEADERS);

        headersPane.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final EventMessage message = event.getMessage();
                if (message.isConsumed()) return;
                final MouseMessage me = (MouseMessage) message;
                final Tuple te = me.getMousePosition();
                final int type = me.getType();
                if (MouseMessage.TYPE_PRESS==type){
                    resizingHeader = Widgets.getChildAt(headersPane, te.get(0), te.get(1));

                    if (resizingHeader!=null){
                        //check we clicked close to the border
                        final Similarity cst = resizingHeader.getNodeTransform();
                        final Tuple inWidgetPos = cst.inverseTransform(te, null);
                        final Extent resizingSize = resizingHeader.getEffectiveExtent();
                        final double p = inWidgetPos.get(0)-resizingSize.get(0)/2.0;
                        if (p>-10 && p<0){
                            baseSize = resizingHeader.getExtents(null).getBest(null);
                            startx = te.get(0);
                            me.consume();
                        } else {
                            resizingHeader = null;
                        }
                    }
                } else if (MouseMessage.TYPE_MOVE==type && resizingHeader!=null){
                    final double diff = te.get(0)-startx;
                    final Extent.Double nextext = new Extent.Double(baseSize.get(0)+diff, baseSize.get(1));
                    //ensure we don't go under the minimum size
                    final Extents extents = resizingHeader.getExtents(null,null);
                    nextext.set(0, Maths.max(extents.minX,nextext.get(0)));
                    final Extents overExt = resizingHeader.getOverrideExtents();
                    overExt.bestX=nextext.get(0);
                    resizingHeader.setOverrideExtents(overExt);
                    me.consume();
                } else {
                    //release or exit
                    resizingHeader = null;
                }
            }
        });

        cellPane.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final MouseMessage me = (MouseMessage) event.getMessage();
                if (!me.isConsumed() && me.getType()==MouseMessage.TYPE_WHEEL){
                    final double offset = me.getWheelOffset();
                    final double r = verticalScrollBar.getRatio()+offset*-verticalScrollBar.getStep();
                    verticalScrollBar.setRatio(Maths.clamp(r, 0, 1));
                    me.consume();
                }
            }
        });

        //listen to column change
        getColumns().addEventListener(CollectionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                updateHeader();
            }
        });

        updateHeader();
        updateContent();
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    protected void updateHeader(){
        final Sequence cols = getColumns();

        headersPane.getChildren().removeAll();
        headersLayout.resetColumnSizes();

        boolean hasExpand = false;
        int i=0;
        for (int n=cols.getSize();i<n;i++){
            final Column cm = (Column) cols.get(i);
            if (cm instanceof DefaultTreeColumn && getRowModel() instanceof TreeRowModel){
                ((DefaultTreeColumn) cm).setRowModel((TreeRowModel) getRowModel());
            }
            final Widget header = cm.getHeader();
            header.setPropertyValue(XPROP_STYLEMARKER, STYLE_HEADER);

            cellLayout.setColumnSize(i, header.getEffectiveExtent().get(0));
            header.varEffectiveExtent().removeListener(columnWidthListener);
            header.varEffectiveExtent().addListener(columnWidthListener);
            headersPane.addChild(header, FillConstraint.builder().coord(i, 0).fill(true, true).build());

            headersLayout.setColumnSize(i, cm.getBestWidth());
            if (cm.getBestWidth()==FormLayout.SIZE_EXPAND) hasExpand = true;
        }

        if (!hasExpand){
            //last empty element to fill space when needed
            final WSpace fill = new WSpace();
            fill.setInlineStyle(new Chars("background:none,graphic:none"));
            headersPane.addChild(fill, FillConstraint.builder().coord(i, 0).build());
            headersLayout.setColumnSize(i, FormLayout.SIZE_EXPAND);
            cellLayout.setColumnSize(i, FormLayout.SIZE_EXPAND);
        }

    }

    private final Dictionary knownRows = new HasherDictionary(Hasher.IDENTITY);
    private final Dictionary buffer = new HasherDictionary(Hasher.IDENTITY);

    private void updateHorizontalDisplacement(){
        final Sequence colSeq = getColumns();
        final Column[] columns = new Column[colSeq.getSize()];
        Collections.copy(colSeq, columns, 0);
        final Extent viewbbox = innerContainer.getEffectiveExtent();
        final double ratioH = horizontalScrollBar.getRatio();

        //Calculate total width and height
        final double totalWidth = headersPane.getExtents(null).bestX;

        //calculate scroll to pixel ratio
        final double scrollSpanX = Math.max(0,totalWidth-viewbbox.get(0));

        //apply displacement
        final double dispX = -(ratioH*scrollSpanX);
        final DisplacementLayout cellDispLayout = (DisplacementLayout) innerContainer.getLayout();
        cellDispLayout.setOffsetX(dispX);
        final DisplacementLayout headerDispLayout = (DisplacementLayout) headerContainer.getLayout();
        headerDispLayout.setOffsetX(dispX);
    }

    private void updateContent(){
        recalculateStep();

        final Sequence colSeq = getColumns();
        final Column[] columns = new Column[colSeq.getSize()];
        Collections.copy(colSeq, columns, 0);
        final RowModel rowModel = getRowModel();
        final Object[] rows = rowModel.asSequence().toArray();
        final int nbRow = rows.length;
        final double rowHeight = getRowHeight();
        final Extent viewbbox = innerContainer.getEffectiveExtent();
        final double ratioH = horizontalScrollBar.getRatio();
        final double ratioV = verticalScrollBar.getRatio();

        //Calculate total width and height
        final double totalHeight = nbRow*rowHeight;
        final double totalWidth = headersPane.getExtents(null).bestX;

        //calculate scroll to pixel ratio
        final double scrollSpanX = Math.max(0,totalWidth-viewbbox.get(0));
        final double scrollSpanY = Math.max(0,totalHeight-viewbbox.get(1));



        //calculate start y
        final double starty = ratioV*scrollSpanY;
        int firstRow = (int) Math.floor(starty/rowHeight);
        int lastRow = (int) Math.ceil((starty+viewbbox.get(1))/rowHeight);
        firstRow = Maths.clamp(firstRow, 0, nbRow);
        lastRow = Maths.clamp(lastRow, 0, nbRow);

        //apply displacement
        final double dispX = -(ratioH*scrollSpanX);
        final double dispY = -(starty%rowHeight);
        final DisplacementLayout cellDispLayout = (DisplacementLayout) innerContainer.getLayout();
        cellDispLayout.setOffsetX(dispX);
        cellDispLayout.setOffsetY(dispY);
        final DisplacementLayout headerDispLayout = (DisplacementLayout) headerContainer.getLayout();
        headerDispLayout.setOffsetX(dispX);

        cellPane.startUpdatingContent();
        final Sequence newCells = new ArraySequence();
        for (int rowIndex=firstRow;rowIndex<lastRow;rowIndex++){
            final int cellY = rowIndex-firstRow;
            cellLayout.setRowSize(cellY, rowHeight);
            final Object rowObject = rows[rowIndex];

            WRow row = (WRow) knownRows.remove(rowObject);
            if (row == null) {
                row = new WRow(this);
                row.setValue(rowObject);
            }
            buffer.add(rowObject, row);

            final FillConstraint rowConstraint = FillConstraint.builder().coord(0, cellY).span(columns.length+1, 1).fill(true, true).build();
            row.setLayoutConstraint(rowConstraint);
            row.setIndex(rowIndex);
            row.setSelected(selection.contains(rowObject));
            newCells.add(row);
            for (int c=0;c<columns.length;c++){
                final WCell cell = row.getCell(columns[c]);
                cell.setLayoutConstraint(FillConstraint.builder().coord(c, cellY).fill(true, true).build());
                newCells.add(cell);
            }

            final Over over = new Over(row);
            over.addEventListener(MouseMessage.PREDICATE, rowSelectionListener);
            over.setLayoutConstraint(rowConstraint);
            newCells.add(over);
        }

        knownRows.removeAll();
        knownRows.addAll(buffer);
        buffer.removeAll();
        cellPane.getChildren().replaceAll(newCells);
        cellPane.endUpdatingContent();
    }

    private void recalculateStep() {
        final Sequence colSeq = getColumns();
        final Column[] columns = new Column[colSeq.getSize()];
        Collections.copy(colSeq, columns, 0);
        final RowModel rowModel = getRowModel();
        final Object[] rows = rowModel.asSequence().toArray();
        final int nbRow = rows.length;
        final double rowHeight = getRowHeight();
        final Extent viewbbox = innerContainer.getEffectiveExtent();

        //Calculate total width and height
        final double totalHeight = nbRow*rowHeight;
        final double totalWidth = headersPane.getExtents(null).bestX;

        //calculate scroll to pixel ratio
        final double scrollSpanX = Math.max(0,totalWidth-viewbbox.get(0));
        final double scrollSpanY = Math.max(0,totalHeight-viewbbox.get(1));

        final double stepV = rowHeight / scrollSpanY;
        verticalScrollBar.setStep(stepV);
        horizontalScrollBar.setStep(0.05);

        //hide scroll bars if they are not necessary
        final int vstate = getVerticalScrollVisibility();
        final int hstate = getHorizontalScrollVisibility();
        final boolean vvisible = (vstate==AbstractRowWidget.SCROLL_VISIBLE_ALWAYS) ? true :
                                 (vstate==AbstractRowWidget.SCROLL_VISIBLE_NEVER) ? false :
                                 (scrollSpanY>0);
        final boolean hvisible = (hstate==AbstractRowWidget.SCROLL_VISIBLE_ALWAYS) ? true :
                                 (hstate==AbstractRowWidget.SCROLL_VISIBLE_NEVER) ? false :
                                 (scrollSpanX>0);

//        if (hvisible){
//            ((FormLayout) getLayout()).setRowSize(2, FormLayout.SIZE_AUTO);
//        } else {
//            ((FormLayout) getLayout()).setRowSize(2, 0);
//        }
//        if (vvisible){
//            ((FormLayout) getLayout()).setColumnSize(1, FormLayout.SIZE_AUTO);
//        } else {
//            ((FormLayout) getLayout()).setColumnSize(1, 0);
//        }
//        addChild(verticalScrollBar,new FormConstraint(2, 1));
//        addChild(horizontalScrollBar,new FormConstraint(1, 2));
        //TODO something is not working as expect with this, it should work
        horizontalScrollBar.setVisible(hvisible);
        verticalScrollBar.setVisible(vvisible);
    }

    public void setHeaderVisible(boolean visible){
        final Extents ext = getHeaderContainer().getOverrideExtents();
        if (visible){
            ext.bestY = Double.NaN;
            ext.minY = Double.NaN;
            ext.maxY = Double.NaN;
        } else {
            ext.bestY = Double.MIN_VALUE;
            ext.minY = Double.MIN_VALUE;
            ext.maxY = Double.MIN_VALUE;
        }
        getHeaderContainer().setOverrideExtents(ext);
    }

    public Sequence getSelection(){
        return selection;
    }

    /**
     * Set selection type, one of SELECTION_X.
     * @param type
     */
    public final void setSelectionType(int type) {
        setPropertyValue(PROPERTY_SELECTIONTYPE, type);
    }

    /**
     * Get selection type.
     * @return one of SELECTION_X
     */
    public final int getSelectionType() {
        return (Integer) getPropertyValue(PROPERTY_SELECTIONTYPE);
    }

    /**
     * Set vertical scroll visibility state.
     * @param state
     */
    public final void setVerticalScrollVisibility(int state) {
        setPropertyValue(PROPERTY_VERTICAL_SCROLL_VISIBILITY, state);
    }

    /**
     * @return vertical scroll visibility state
     */
    public final int getVerticalScrollVisibility() {
        return (Integer) getPropertyValue(PROPERTY_VERTICAL_SCROLL_VISIBILITY);
    }

    /**
     * Set horizontal scroll visibility state.
     * @param state
     */
    public final void setHorizontalScrollVisibility(int state) {
        setPropertyValue(PROPERTY_HORIZONTAL_SCROLL_VISIBILITY, state);
    }

    /**
     * @return horizontal scroll visibility state
     */
    public final int getHorizontalScrollVisibility() {
        return (Integer) getPropertyValue(PROPERTY_HORIZONTAL_SCROLL_VISIBILITY);
    }

    protected void columnChanged(CollectionMessage event){
    }

    protected RowModel getRowModel() {
        return (RowModel) getPropertyValue(PROPERTY_ROWMODEL);
    }

    protected void setRowModel(final RowModel rowModel) {
        CObjects.ensureNotNull(rowModel);
        setPropertyValue(PROPERTY_ROWMODEL, rowModel);
    }

    Sequence getColumns(){
        return columns;
    }

    WScrollBar getVerticalScrollBar() {
        return verticalScrollBar;
    }

    WScrollBar getHorizontalScrollBar() {
        return horizontalScrollBar;
    }

    WContainer getInnerContainer() {
        return innerContainer;
    }

    WContainer getHeaderContainer() {
        return headerContainer;
    }

    /**
     * Set row height.
     *
     * @param rowHeight , use -1 for dynamic row heights
     */
    public final void setRowHeight(double rowHeight) {
        setPropertyValue(PROPERTY_ROWHEIGHT, rowHeight);
    }

    /**
     *
     * @return row heights, -1 for dynamic
     */
    public final double getRowHeight() {
        return (Double) getPropertyValue(PROPERTY_ROWHEIGHT);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_ROWMODEL.equals(name)) {
            RowModel oldModel = (RowModel) oldValue;
            RowModel rowModel = (RowModel) value;
            if (oldModel!=null) oldModel.removeEventListener(CollectionMessage.PREDICATE, eventListener);
            rowModel.addEventListener(CollectionMessage.PREDICATE, eventListener);
            //erase selection
            getSelection().removeAll();
            //reset scroll positions
            verticalScrollBar.setRatio(0);
            horizontalScrollBar.setRatio(0);
            //update table content
            updateContent();
        } else if (PROPERTY_HORIZONTAL_SCROLL_VISIBILITY.equals(name)) {
            recalculateStep();
        } else if (PROPERTY_VERTICAL_SCROLL_VISIBILITY.equals(name)) {
            recalculateStep();
        }
    }

    @Override
    protected void receiveEventParent(Event event) {
        super.receiveEventParent(event);

        //selection
        final RowModel treeModel = getRowModel();

        final EventMessage message = event.getMessage();
        if (message instanceof KeyMessage){
            //check for CTRL key
            final KeyMessage ke = (KeyMessage) message;
            final int keyCode = ke.getCode();
            final int keyType = ke.getType();
            if (keyCode == KeyMessage.KC_CONTROL){
                if (keyType == KeyMessage.TYPE_PRESS){
                    ctrlActive = true;
                } else if (keyType == KeyMessage.TYPE_RELEASE){
                    ctrlActive = false;
                }
            }
        }

        if (message instanceof KeyMessage){
            final KeyMessage ke = (KeyMessage) message;
            final int keyCode = ke.getCode();
            final int keyType = ke.getType();
            if (keyCode == KeyMessage.KC_DOWN && keyType == KeyMessage.TYPE_RELEASE){
                if (selection.getSize()==1){
                    final Object selected = selection.get(0);
                    final Sequence rows = treeModel.asSequence();
                    final int index = rows.search(selected);
                    if (index>=0 && index<rows.getSize()-2){
                        selection.replaceAll(new Object[]{rows.get(index+1)});
                    }
                }
            } else if (keyCode == KeyMessage.KC_UP && keyType == KeyMessage.TYPE_RELEASE){
                if (selection.getSize()==1){
                    final Object selected = selection.get(0);
                    final Sequence rows = treeModel.asSequence();
                    final int index = rows.search(selected);
                    if (index>0){
                        selection.replaceAll(new Object[]{rows.get(index-1)});
                    }
                }
            }
        }
    }

    private static final class Over extends WLeaf {

        private final WRow row;

        public Over(WRow row) {
            this.row = row;
        }
    }
}
