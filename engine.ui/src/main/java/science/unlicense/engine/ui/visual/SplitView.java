
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.SplitLayout;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.Tuple;

/**
 *
 * @author Johann Sorel
 */
public class SplitView extends ContainerView {

    public SplitView(WSplitContainer widget) {
        super(widget);
    }

    public WSplitContainer getWidget() {
        return (WSplitContainer) super.getWidget();
    }


    private boolean dragging = false;

    private void updateDividerPosition(Tuple mousePosition){
        final WSplitContainer split = getWidget();
        if (split.isVerticalSplit()){
            split.setSplitPosition(mousePosition.get(1)-split.getBoundingBox(null).getMin(1));
        } else {
            split.setSplitPosition(mousePosition.get(0)-split.getBoundingBox(null).getMin(0));
        }
    }

    /**
     * Passthrought event to childrens.
     *
     * @param event
     */
    public void receiveEvent(Event event) {
        final WSplitContainer split = getWidget();
        final EventSource source = event.getSource();
        if (split.getChildren().contains(source)) return;

        //event comes from the parent
        final EventMessage message = event.getMessage();
        if (message instanceof MouseMessage){
            final MouseMessage mevent = (MouseMessage) message;
            final int type = mevent.getType();

            final Tuple mp = mevent.getMousePosition();

            final SplitLayout layout = ((SplitLayout) split.getLayout());
            final BBox ext = split.getBoundingBox(null);
            final double separatorSize = layout.getSplitGap();
            final double separatorPosition = layout.getSplitPosition();

            final BBox separator = new BBox(ext);
            if (split.isVerticalSplit()){
                separator.setRange(1, ext.getMin(1)+separatorPosition, ext.getMin(1)+separatorPosition+separatorSize);
            } else {
                separator.setRange(0, ext.getMin(0)+separatorPosition, ext.getMin(0)+separatorPosition+separatorSize);
            }

            final boolean inSeparator = separator.intersects(mp);

            //check separator dragging actions
            if (type == MouseMessage.TYPE_RELEASE && dragging){
                mevent.consume();
                dragging = false;
                return;
            } else if (type == MouseMessage.TYPE_PRESS && !dragging && inSeparator){
                mevent.consume();
                dragging = true;
                return;
            } else if (type == MouseMessage.TYPE_MOVE && dragging){
                mevent.consume();
                updateDividerPosition(mp);
                return;
            }
            dragging = false;
        }

    }

}
