
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.Point;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class WPopup extends WContainer{

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("popup")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WPopup() {
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    /**
     * Display the popup at target widget location.
     *
     * @param target
     * @param offsetX X offset from target upper left corner
     * @param offsetY Y offset from target upper left corner
     */
    public void showAt(final Widget target, final int offsetX, final int offsetY){
        new Thread(){
            @Override
            public void run() {
                final UIFrame frame;
                if (target==null){
                    frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
                } else {
                    frame = (UIFrame) target.getFrame().getManager().createFrame(target.getFrame(), false, false);
                }

                final WContainer container = frame.getContainer();
                container.setLayout(new BorderLayout());
                container.addChild(WPopup.this, BorderConstraint.CENTER);

                final Extents ext = WPopup.this.getExtents(null);

                final TupleRW pt;
                if (target==null){
                    pt = new Vector2f64(offsetX, offsetY);
                } else {
                    pt = (TupleRW) target.getOnSreenPosition();
                    if (pt==null){
                        //target is not visible,
                        Loggers.get().info(new Chars("Trying to display a popup for an unvisible parent : "+target.getId()));
                        return;
                    }
                    pt.set(0,pt.get(0)+offsetX);
                    pt.set(1,pt.get(1)+offsetY);
                }

                frame.setDecoration(null);
                frame.setSize((int) ext.bestX, (int) ext.bestY);
                frame.setOnScreenLocation(pt);

                //listen to mouse exit to close popup
                container.addEventListener(MouseMessage.PREDICATE, new EventListener() {
                    public void receiveEvent(Event event) {
                        final MouseMessage me = (MouseMessage) event.getMessage();
                        if (me.getType() == MouseMessage.TYPE_EXIT){
                            container.getChildren().remove(WPopup.this);
                            frame.dispose();
                        }
                    }
                });

                frame.setVisible(true);
            }
        }.start();

    }

    /**
     * Display given widget (popup/tooltip/other) at component position.
     *
     * @param parent
     * @param candidate widget to display
     * @param position offset from component upper left corner.
     */
    public static void showAt(Widget parent, Widget candidate, Point position){
        if (candidate==null) return;
        final UIFrame frame = parent.getFrame();
        if (frame==null) return;

        final TupleRW pt = (TupleRW) parent.getOnSreenPosition();
        if (pt == null) return;

        final TupleRW coord = position.getCoordinate();
        pt.set(0,pt.get(0)+coord.get(0));
        pt.set(1,pt.get(1)+coord.get(1));

        final Extent bestExtent = candidate.getExtents(null).getBest(null);
        final UIFrame dialog = (UIFrame) frame.getManager().createFrame(parent.getFrame(), true, false);

        candidate.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final MouseMessage me = (MouseMessage) event.getMessage();
                if (me.getType() == MouseMessage.TYPE_EXIT){
                    dialog.dispose();
                }
            }
        });

        //show the popup
        final WContainer content = dialog.getContainer();
        content.setLayout(new BorderLayout());
        content.addChild(candidate, BorderConstraint.CENTER);

        dialog.setDecoration(null);
        dialog.setAlwaysonTop(true);
        dialog.setSize((int) bestExtent.get(0), (int) bestExtent.get(1));
        dialog.setOnScreenLocation(pt);
        dialog.setVisible(true);
    }

}
