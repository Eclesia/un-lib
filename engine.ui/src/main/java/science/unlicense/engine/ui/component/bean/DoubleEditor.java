
package science.unlicense.engine.ui.component.bean;

import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.WValueWidget;

/**
 *
 * @author Johann Sorel
 */
public class DoubleEditor implements PropertyEditor {

    public static final DoubleEditor INSTANCE = new DoubleEditor();

    private DoubleEditor(){}

    @Override
    public WValueWidget create(Property property) {
        if (Double.class.isAssignableFrom(property.getType().getValueClass())){
            return new WSpinner(new NumberSpinnerModel(Double.class, 0.0, null, null, 1.0));
        }
        return null;
    }

}
