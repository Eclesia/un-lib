
package science.unlicense.engine.ui.widget.frame;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.desktop.Frame;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.widget.AbstractWidget;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicText;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public class WSystemDecoration extends WFrameDecoration {

    private static final Chars BUTTON_STYLE = new Chars("graphic:none\nmargin:2");
    private static final Chars FONT_STYLE = new Chars("font:{family:font('Unicon' 9 'none')\n}");

    private final WLabel title = new WLabel();
    private final WFrameDrag drag = new WFrameDrag(new BorderLayout());
    private final WFrameResize resizeBottom = new WFrameResize();

    private final WButton minimize = new WButton(null, new WGraphicText(new Chars("\ue01D")), new EventListener(){
        @Override
        public void receiveEvent(Event event) {
            getFrame().setState(Frame.STATE_MINIMIZED);
        }
    });
    private final WButton pin = new WButton(null, new WGraphicText(new Chars("\ue020")),new EventListener(){
        @Override
        public void receiveEvent(Event event) {
            getFrame().setAlwaysonTop(!getFrame().isAlwaysOnTop());
        }
    });
    private final WButton maximize = new WButton(null, new WGraphicText(new Chars("\ue01E")),new EventListener(){
        @Override
        public void receiveEvent(Event event) {
            if (getFrame().getState()==Frame.STATE_MAXIMIZED){
                getFrame().setState(Frame.STATE_NORMAL);
            } else {
                getFrame().setState(Frame.STATE_MAXIMIZED);
            }
        }
    });
    private final WButton close = new WButton(null, new WGraphicText(new Chars("\ue01F")),new EventListener(){
        @Override
        public void receiveEvent(Event event) {
            getFrame().dispose();
        }
    });

    public WSystemDecoration(){

        content.setPropertyValue(AbstractWidget.XPROP_STYLEMARKER, new Chars("systemframe"));

        final WContainer header = new WContainer(new FormLayout());
        header.setPropertyValue(AbstractWidget.XPROP_STYLEMARKER, new Chars("systemframe-header"));

        minimize.setInlineStyle(BUTTON_STYLE);
        pin.setInlineStyle(BUTTON_STYLE);
        maximize.setInlineStyle(BUTTON_STYLE);
        close.setInlineStyle(BUTTON_STYLE);
        minimize.getGraphic().setInlineStyle(FONT_STYLE);
        pin.getGraphic().setInlineStyle(FONT_STYLE);
        maximize.getGraphic().setInlineStyle(FONT_STYLE);
        close.getGraphic().setInlineStyle(FONT_STYLE);
        minimize.setReserveSpace(false);
        pin.setReserveSpace(false);
        maximize.setReserveSpace(false);
        close.setReserveSpace(false);

        ((FormLayout) header.getLayout()).setColumnSize(0, FormLayout.SIZE_EXPAND);

        drag.addChild(title,BorderConstraint.CENTER);
        header.addChild(drag,       FillConstraint.builder().coord(0, 0).build());
        header.addChild(minimize,   FillConstraint.builder().coord(1, 0).build());
        header.addChild(pin,        FillConstraint.builder().coord(2, 0).build());
        header.addChild(maximize,   FillConstraint.builder().coord(3, 0).build());
        header.addChild(close,      FillConstraint.builder().coord(4, 0).build());

        final Extents overrideExtents = resizeBottom.getOverrideExtents();
        overrideExtents.bestX = 2;overrideExtents.bestY = 3;
        resizeBottom.setOverrideExtents(overrideExtents);

        content.addChild(header,BorderConstraint.TOP);
        content.addChild(resizeBottom,BorderConstraint.BOTTOM);
        content.addChild(new WSpace(new Extent.Double(1, 1)),BorderConstraint.LEFT);
        content.addChild(new WSpace(new Extent.Double(1, 1)),BorderConstraint.RIGHT);
    }

    @Override
    public void setFrame(Frame frame) {
        super.setFrame(frame);
        if (frame!=null){
            title.varText().sync(frame.varTitle());
            minimize.varVisible().sync(frame.varMinimizable());
            maximize.varVisible().sync(frame.varMaximizable());
            close.varVisible().sync(frame.varClosable());
        } else {
            title.varText().unsync();
            minimize.varVisible().unsync();
            pin.varVisible().unsync();
            maximize.varVisible().unsync();
            close.varVisible().unsync();
        }
    }

}
