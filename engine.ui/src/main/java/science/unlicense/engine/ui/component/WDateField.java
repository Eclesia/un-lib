
package science.unlicense.engine.ui.component;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.widget.AbstractControlWidget;
import science.unlicense.engine.ui.widget.WAction;

/**
 * TODO make a proper date/time chooser.
 * TODO need to finish date api.
 *
 * @author Johann Sorel
 */
public class WDateField extends AbstractControlWidget {

    private static final Chars MARKER = Chars.constant("WDateChooser-Button");

    private final WAction choose = new WAction(null, null, new EventListener() {
        public void receiveEvent(Event event) {
        }
    });

    public WDateField(){
        setLayout(new BorderLayout());
        addChild(choose, BorderConstraint.RIGHT);
        choose.setPropertyValue(XPROP_STYLEMARKER, MARKER);

    }

}
