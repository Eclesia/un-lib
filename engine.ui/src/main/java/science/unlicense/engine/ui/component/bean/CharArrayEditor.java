
package science.unlicense.engine.ui.component.bean;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.WValueWidget;

/**
 *
 * @author Johann Sorel
 */
public class CharArrayEditor implements PropertyEditor {

    public static final CharArrayEditor INSTANCE = new CharArrayEditor();

    private CharArrayEditor(){}

    public WValueWidget create(Property property) {
        if (CharArray.class.isAssignableFrom(property.getType().getValueClass())){
            return new WTextField();
        }
        return null;
    }

}
