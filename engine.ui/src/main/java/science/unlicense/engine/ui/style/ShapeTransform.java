
package science.unlicense.engine.ui.style;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.geometry.impl.transform.Projections;
import science.unlicense.math.impl.Affine2;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 *
 * @author Johann Sorel
 */
public interface ShapeTransform {

    Transform getTransform(BBox extent, PlanarGeometry geom);

    public static class Affine implements ShapeTransform {

        private final Affine2 affine;

        public Affine(Affine2 affine) {
            this.affine = affine;
        }

        @Override
        public Transform getTransform(BBox bbox, PlanarGeometry geom) {
            return affine;
        }

    }

    public static class Fit implements ShapeTransform {

        private final int axis;

        public Fit(int axis) {
            this.axis = axis;
        }

        @Override
        public Transform getTransform(BBox bbox, PlanarGeometry geom) {
            return Projections.fit(geom.getBoundingBox(), bbox, axis);
        }
    }

    public static class Scale implements ShapeTransform {
        @Override
        public Transform getTransform(BBox bbox, PlanarGeometry geom) {
            return Projections.scaled(geom.getBoundingBox(), bbox);
        }
    }

    public static class Center implements ShapeTransform {
        @Override
        public Transform getTransform(BBox bbox, PlanarGeometry geom) {
            return Projections.centered(geom.getBoundingBox(), bbox);
        }
    }

    public static class Stretch implements ShapeTransform {
        @Override
        public Transform getTransform(BBox bbox, PlanarGeometry geom) {
            return Projections.stretched(geom.getBoundingBox(), bbox);
        }
    }

    public static class Zoom implements ShapeTransform {
        @Override
        public Transform getTransform(BBox bbox, PlanarGeometry geom) {
            return Projections.zoomed(geom.getBoundingBox(), bbox);
        }
    }

    public static class Align implements ShapeTransform {

        private final double ratiox;
        private final double ratioy;

        public Align(double ratiox, double ratioy) {
            this.ratiox = ratiox;
            this.ratioy = ratioy;
        }
        @Override
        public Transform getTransform(BBox bbox, PlanarGeometry geom) {
            return Projections.align(geom.getBoundingBox(), bbox, new double[]{ratiox,ratioy});
        }
    }

}
