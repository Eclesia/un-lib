
package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.widget.AbstractControlWidget;
import science.unlicense.engine.ui.widget.WAction;
import science.unlicense.engine.ui.widget.WTextField;
import static science.unlicense.engine.ui.widget.WTextField.PROPERTY_PREVIEW_TEXT;

/**
 *
 * @author Johann Sorel
 */
public class WPathField extends AbstractControlWidget {

    /**
     * Property for the widget path.
     */
    public static final Chars PROPERTY_PATH = Chars.constant("Path");

    private final WTextField textField = new WTextField();
    private final WAction openChooser = new WAction();

    public WPathField() {
        setLayout(new BorderLayout());
        addChild(textField, BorderConstraint.CENTER);
        addChild(openChooser, BorderConstraint.RIGHT);
        textField.varPreviewText().sync(varPreviewText(), true);
        textField.setPropertyValue(XPROP_STYLEMARKER, new Chars("WPathField-Editor"));
        openChooser.setPropertyValue(XPROP_STYLEMARKER, new Chars("WPathField-Button"));

        openChooser.addEventListener(ActionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                new Thread() {
                    public void run() {
                        final WPathChooser chooser = new WPathChooser();
                        Path path = getPath();
                        if (path!=null) {
                            chooser.setSelectedPath(new Path[]{path});
                        }
                        path = chooser.showOpenDialog(WPathField.this.getFrame());
                        if (path!=null) setPath(path);
                    }
                }.start();
            }
        });

        textField.varText().addListener(new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final PropertyMessage pm = (PropertyMessage) event.getMessage();
                final Chars chars = (Chars) pm.getNewValue();
                final Path path;
                try {
                    path = Paths.resolve(chars);
                } catch (Exception ex){
                    setEditionValid(false);
                    return;
                }
                setPath(path);
            }
        });
    }

    public final Path getPath() {
        return (Path) getPropertyValue(PROPERTY_PATH);
    }

    public final void setPath(Path path){
        setPropertyValue(PROPERTY_PATH, path);
    }

    /**
     * Get path variable.
     * @return Variable.
     */
    public final Property varPath(){
        return getProperty(PROPERTY_PATH);
    }

    public final void setPreviewText(CharArray text) {
        setPropertyValue(PROPERTY_PREVIEW_TEXT,text);
    }

    public final CharArray getPreviewText() {
        return (CharArray) getPropertyValue(PROPERTY_PREVIEW_TEXT);
    }

    /**
     * Get preview text variable.
     * @return Variable.
     */
    public final Property varPreviewText(){
        return getProperty(PROPERTY_PREVIEW_TEXT);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_PATH.equals(name)) {
            final Path path = (Path) value;
            if (path == null){
                textField.setText(Chars.EMPTY);
                setEditionValid(false);
            } else {
                textField.setText(new Chars(path.toURI()));
                setEditionValid(true);
            }
        }
    }

}
