
package science.unlicense.engine.ui.component;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.color.Color;

/**
 *
 * @author Johann Sorel
 */
public class WColorSquare extends WSpace implements WValueWidget {

    public static final Chars PROPERTY_COLOR = Chars.constant("Color");

    public WColorSquare() {
        this(new Extent.Double(10, 10));
    }

    public WColorSquare(Extent extent) {
        super(extent);
        setInlineStyle(new Chars("graphic:{\nfill-paint : $Color\n}"));
    }

    @Override
    public final Chars getValuePropertyName() {
        return PROPERTY_COLOR;
    }

    public final Color getColor() {
        return (Color) getPropertyValue(PROPERTY_COLOR);
    }

    public final void setColor(Color color) {
        setPropertyValue(PROPERTY_COLOR, color);
    }

    public final Property varColor() {
        return getProperty(PROPERTY_COLOR);
    }
}
