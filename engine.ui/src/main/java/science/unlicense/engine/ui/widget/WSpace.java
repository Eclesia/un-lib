
package science.unlicense.engine.ui.widget;

import science.unlicense.geometry.api.Extent;
import science.unlicense.display.api.layout.Extents;

/**
 * Widget with a fixed size.
 * Can be used in layout to reserve space or preserve distance between widgets.
 *
 * @author Johann Sorel
 */
public class WSpace extends WLeaf{

    public WSpace() {
        this(new Extent.Double(Double.NaN, Double.NaN));
    }

    public WSpace(double width, double height) {
        this(new Extent.Double(width, height));
    }

    public WSpace(Extent ext) {
        setOverrideExtents(new Extents(
                ext.get(0), ext.get(1),
                ext.get(0), ext.get(1),
                Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY));
    }

}
