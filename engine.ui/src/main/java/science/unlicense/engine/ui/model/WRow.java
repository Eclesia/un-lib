
package science.unlicense.engine.ui.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Hasher;
import science.unlicense.common.api.collection.HasherDictionary;
import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.widget.AbstractRowWidget;
import static science.unlicense.engine.ui.widget.AbstractRowWidget.STYLE_ROW;
import science.unlicense.engine.ui.widget.WCell;
import science.unlicense.engine.ui.widget.WLeaf;

/**
 *
 * @author Johann Sorel
 */
public class WRow extends WLeaf {

    public static final Chars PROPERTY_SELECTED = Chars.constant("Selected");
    public static final Chars PROPERTY_INDEX = Chars.constant("Index");
    public static final Chars PROPERTY_VALUE = Chars.constant("Value");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLeaf.CLASS_DEFAULTS);
        defs.add(PROPERTY_SELECTED, Boolean.FALSE);
        defs.add(PROPERTY_INDEX, 0);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("row")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    // Column -> WCell
    private final Dictionary cells = new HasherDictionary(Hasher.IDENTITY);
    private final AbstractRowWidget rowWidget;

    public WRow(AbstractRowWidget rowWidget) {
        setPropertyValue(XPROP_STYLEMARKER, STYLE_ROW);
        this.rowWidget = rowWidget;
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public final AbstractRowWidget getRowWidget() {
        return rowWidget;
    }

    public final Object getValue() {
        return getPropertyValue(PROPERTY_VALUE);
    }

    public final void setValue(Object value){
        setPropertyValue(PROPERTY_VALUE, value);
    }

    public final Property varValue(){
        return getProperty(PROPERTY_VALUE);
    }

    public final boolean isSelected(){
        //test equality, value could be null at first
        return (Boolean) getPropertyValue(PROPERTY_SELECTED);
    }

    public final void setSelected(boolean selected){
        setPropertyValue(PROPERTY_SELECTED, selected);
    }

    public final Property varSelected() {
        return getProperty(PROPERTY_SELECTED);
    }

    public final int getIndex(){
        return (Integer) getPropertyValue(PROPERTY_INDEX);
    }

    public final void setIndex(int index){
        setPropertyValue(PROPERTY_INDEX, index);
    }

    public final Property varIndex() {
        return getProperty(PROPERTY_INDEX);
    }

    public WCell getCell(Column column){
        WCell cell = (WCell) cells.getValue(column);
        if (cell == null){
            cell = (WCell) column.createCell(this);
            cell.varValue().sync(varValue());
            cells.add(column, cell);
        }
        return cell;
    }

}
