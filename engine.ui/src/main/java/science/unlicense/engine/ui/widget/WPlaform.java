
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.engine.ui.style.StyleDocument;

/**
 *
 * @author Johann Sorel
 */
public class WPlaform extends WContainer {

    private static final Chars TABBAR = Chars.constant("platformbar");
    private static final Chars TAB = Chars.constant("platformtab");

    public static final int HORIZONTAL_LEFT = 0;
    public static final int HORIZONTAL_CENTER = 1;
    public static final int HORIZONTAL_RIGHT = 2;
    public static final int VERTICAL_TOP = 0;
    public static final int VERTICAL_CENTER = 1;
    public static final int VERTICAL_BOTTOM = 2;

    private static Sequence blocks = new ArraySequence();


    public WPlaform() {
        setLayout(new BorderLayout());
    }

    public void addBlock(Block b) {
        blocks.add(b);

        final Sequence lefts = new ArraySequence();
        final Sequence centers = new ArraySequence();
        final Sequence rights = new ArraySequence();


        for (int i=0,n=blocks.getSize();i<n;i++) {
            final Block block = (Block) blocks.get(i);
            switch(block.horizontal) {
                case HORIZONTAL_LEFT : lefts.add(block); break;
                case HORIZONTAL_CENTER : centers.add(block); break;
                case HORIZONTAL_RIGHT : rights.add(block); break;
            }
        }

        final WContainer leftPane = dispatchVertical(lefts);
        final WContainer centerPane = dispatchVertical(centers);
        final WContainer rightPane = dispatchVertical(rights);

        WContainer pane = null;
        if (leftPane != null) {
            pane = leftPane;
        }
        if (centerPane != null) {
            if (pane != null) {
                final WSplitContainer split = new WSplitContainer(false);
                split.addChild(pane, SplitConstraint.TOP);
                split.addChild(centerPane, SplitConstraint.BOTTOM);
                split.setSplitPosition(300);
                pane = split;
            } else {
                pane = centerPane;
            }
        }
        if (rightPane != null) {
            if (pane != null) {
                final WSplitContainer split = new WSplitContainer(false);
                split.addChild(pane, SplitConstraint.TOP);
                split.addChild(rightPane, SplitConstraint.BOTTOM);
                split.setSplitPosition(300);
                pane = split;
            } else {
                pane = rightPane;
            }
        }

        if (pane == null) {
            removeChildren();
        } else {
            pane.setLayoutConstraint(BorderConstraint.CENTER);
            getChildren().replaceAll(new Node[]{pane});
        }
    }

    private WContainer dispatchVertical(Sequence blocks) {
        if (blocks.isEmpty()) return null;

        final Sequence tops = new ArraySequence();
        final Sequence centers = new ArraySequence();
        final Sequence bottoms = new ArraySequence();

        for (int i=0,n=blocks.getSize();i<n;i++) {
            final Block block = (Block) blocks.get(i);
            switch(block.vertical) {
                case VERTICAL_TOP : tops.add(block); break;
                case VERTICAL_CENTER : centers.add(block); break;
                case VERTICAL_BOTTOM : bottoms.add(block); break;
            }
        }

        final WContainer topPane = buildTabs(tops);
        final WContainer centerPane = buildTabs(centers);
        final WContainer bottomPane = buildTabs(bottoms);

        WContainer pane = null;
        if (topPane != null) {
            pane = topPane;
        }
        if (centerPane != null) {
            if (pane != null) {
                final WSplitContainer split = new WSplitContainer(true);
                split.addChild(pane, SplitConstraint.TOP);
                split.addChild(centerPane, SplitConstraint.BOTTOM);
                split.setSplitPosition(300);
                pane = split;
            } else {
                pane = centerPane;
            }
        }
        if (bottomPane != null) {
            if (pane != null) {
                final WSplitContainer split = new WSplitContainer(true);
                split.addChild(pane, SplitConstraint.TOP);
                split.addChild(bottomPane, SplitConstraint.BOTTOM);
                split.setSplitPosition(300);
                pane = split;
            } else {
                pane = bottomPane;
            }
        }

        return pane;
    }

    private WContainer buildTabs(Sequence blocks) {
        if (blocks.isEmpty()) return null;

        final WTabContainer tabs = new WTabContainer();
        final Widget bar = (Widget) tabs.getChildren().get(0);
        bar.setPropertyValue(StyleDocument.PROPERTY_STYLE_CLASS, TABBAR);

        for (int i=0,n=blocks.getSize();i<n;i++) {
            final Block block = (Block) blocks.get(i);
            final WTabContainer.WTab tab = tabs.addTab(block, new WLabel(block.title));
            tab.setPropertyValue(StyleDocument.PROPERTY_STYLE_CLASS, TAB);
        }

        return tabs;
    }

    public static final class Block extends WContainer {
        public int horizontal = HORIZONTAL_CENTER;
        public int vertical = VERTICAL_CENTER;
        public Chars title;

        public Block() {
            this(Chars.EMPTY, HORIZONTAL_CENTER, VERTICAL_CENTER);
        }

        public Block(Chars title, int horizontal, int vertical) {
            super(new BorderLayout());
            this.title = title;
            this.horizontal = horizontal;
            this.vertical = vertical;
        }

    }

}
