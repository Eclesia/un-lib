

package science.unlicense.engine.ui.io;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public class InPredicate extends CObject implements Predicate {

    private final Expression exp1;
    private final Expression exp2;

    public InPredicate(Expression exp1, Expression exp2) {
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    public Expression getExp1() {
        return exp1;
    }

    public Expression getExp2() {
        return exp2;
    }

    public Chars getPropertyName() {
        return ((PropertyName) exp1).getName();
    }

    public Boolean evaluate(Object candidate) {
        if (candidate==null) return false;

        final Object obj1 = exp1.evaluate(candidate);
        if (obj1==null) return false;
        final Object obj2 = exp2.evaluate(candidate);
        return (obj2 instanceof Collection) && ((Collection) obj2).contains(obj1);
    }

    public Chars toChars() {
        return Nodes.toChars(new Chars("In"), new Object[]{exp1,exp2});
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InPredicate other = (InPredicate) obj;
        if (this.exp1 != other.exp1 && (this.exp1 == null || !this.exp1.equals(other.exp1))) {
            return false;
        }
        if (this.exp2 != other.exp2 && (this.exp2 == null || !this.exp2.equals(other.exp2))) {
            return false;
        }
        return true;
    }

    public int getHash() {
        int hash = 3;
        hash = 41 * hash + (this.exp1 != null ? this.exp1.hashCode() : 0);
        hash = 41 * hash + (this.exp2 != null ? this.exp2.hashCode() : 0);
        return hash;
    }

}
