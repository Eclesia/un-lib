
package science.unlicense.engine.ui.model;

import science.unlicense.engine.ui.visual.TreePathView;
import science.unlicense.engine.ui.widget.WLeaf;

/**
 *
 * @author Johann Sorel
 */
public class WTreePath extends WLeaf {

    public static final double DEPTH = 12;

    public static final int TYPE_ROOT = -1; //for the root node
    public static final int TYPE_EMPTY = 0;
    public static final int TYPE_MIDDLE = 1;
    public static final int TYPE_LINE = 2;
    public static final int TYPE_END = 3;

    private int[] depths = null;

    public WTreePath() {
        setView(new TreePathView(this));
    }

    public double getDepthWidth(){
        return DEPTH;
    }

    public void setDepths(int[] depths) {
        this.depths = depths;
        updateExtents();
        setDirty();
    }

    public int[] getDepths() {
        return depths;
    }

}
