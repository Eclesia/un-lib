
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.style.GraphicStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WGraphicGeometry;
import static science.unlicense.engine.ui.widget.WGraphicGeometry.STYLE_GEOM_PREFIX;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 *
 * @author Johann Sorel
 */
public class GraphicGeometryView extends WidgetView {

    public GraphicGeometryView(WGraphicGeometry widget) {
        super(widget);
    }

    @Override
    public WGraphicGeometry getWidget() {
        return (WGraphicGeometry) super.getWidget();
    }

    @Override
    public void getExtents(Extents buffer, Extent constraint) {

        //get shape size
        final PlanarGeometry geom = getWidget().getGeometry();
        final Extent extent;
        if (geom != null) {
            final BBox bbox = geom.getBoundingBox();
            extent = new Extent.Double(bbox.getSpan(0),bbox.getSpan(1));
        } else {
            extent = new Extent.Double(0, 0);
        }

        buffer.setAll(extent);
    }

    @Override
    public void renderSelf(Painter2D painter, BBox innerBBox) {

        final Document style = widget.getEffectiveStyleDoc();
        final GraphicStyle[] graphics = WidgetStyles.readShapeStyle(style, STYLE_GEOM_PREFIX, widget);

        final PlanarGeometry baseGeom = getWidget().getGeometry();
        for (GraphicStyle ss : graphics) {
            renderShape(painter, ss, baseGeom);
        }
    }

    @Override
    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        if (WGraphicGeometry.PROPERTY_GEOMETRY.equals(event.getPropertyName())) {
            widget.updateExtents();
            widget.setDirty();
        }
    }

}
