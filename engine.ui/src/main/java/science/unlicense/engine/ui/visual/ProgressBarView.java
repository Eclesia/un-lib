
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.style.FontStyle;
import science.unlicense.engine.ui.style.GraphicStyle;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WProgressBar;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.Rectangle;

/**
 *
 * @author Johann Sorel
 */
public class ProgressBarView extends WidgetView {

    public ProgressBarView(WProgressBar widget) {
        super(widget);
    }

    @Override
    public WProgressBar getWidget() {
        return (WProgressBar) super.getWidget();
    }

    @Override
    public void getExtents(Extents buffer, Extent constraint) {

        final FontStyle font = WidgetStyles.readFontStyle(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_FONT, widget);
        final Extent extent = new Extent.Double(20,20);

        //get text size
        final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
        final FontMetadata meta = fontStore.getFont(font.getFont()).getMetaData();

        final CharArray text = getTranslatedText(getWidget().getText());
        if (meta!=null && text != null){
            final BBox bbox = meta.getCharsBox(text);
            extent.set(0, bbox.getSpan(0));
            extent.set(1, bbox.getSpan(1));
        }

        buffer.setAll(extent);
    }

    @Override
    public void renderSelf(Painter2D painter, BBox innerBBox) {
        final Extent extent = widget.getEffectiveExtent();
        final double width = extent.get(0);
        final double height = extent.get(1);

        final Document style = widget.getEffectiveStyleDoc();
        final FontStyle fontStyle = WidgetStyles.readFontStyle(style, StyleDocument.STYLE_PROP_FONT, widget);
        final GraphicStyle[] graphics = WidgetStyles.readShapeStyle(style, new Chars("progress"), widget);

        //render the border and fill
        final double progress = getWidget().getProgress();
        final Rectangle fillgeom;
        if (progress == 1) {
            fillgeom = new Rectangle(innerBBox);
        } else if (getWidget().isUndetermined()) {
            fillgeom = new Rectangle(innerBBox.getMin(0),innerBBox.getMin(1),extent.get(0), extent.get(1)/2d);
            //TODO some kind of animation needed
        } else {
            fillgeom = new Rectangle(innerBBox.getMin(0),innerBBox.getMin(1),extent.get(0)*progress, extent.get(1));
        }
        renderShape(painter, graphics, fillgeom);

        //render the text
        final CharArray text = getTranslatedText(getWidget().getText());
        if (text!=null && !text.isEmpty() && fontStyle != null){
            final FontChoice font = fontStyle.getFont();
            if (font!=null){
                painter.setFont(font);
                //calculate appropriate text position
                //TODO : for now horizontal centered, vertical centered
                final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
                final FontMetadata meta = fontStore.getFont(font).getMetaData();
                final BBox bbox = meta.getCharsBox(text);
                final float xoffset = (float) (-bbox.getSpan(0))/2;
                final float yoffset = (float) ((-bbox.getSpan(1))/2 + meta.getAscent());

                //TODO calculate accurate position based on font
                if (fontStyle.getFillPaint()!=null){
                    painter.setPaint(fontStyle.getFillPaint());
                    painter.fill(text, xoffset, yoffset);
                }
                if (fontStyle.getBrush()!=null && fontStyle.getBrushPaint()!=null){
                    painter.setBrush(fontStyle.getBrush());
                    painter.setPaint(fontStyle.getBrushPaint());
                    painter.stroke(text, xoffset, yoffset);
                }
            }
        }
    }

    @Override
    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        final Chars name = event.getPropertyName();
        if (WProgressBar.PROPERTY_TEXT.equals(name) ||
           WProgressBar.PROPERTY_LANGUAGE.equals(name)){
            widget.updateExtents();
            widget.setDirty();
        } else if (WProgressBar.PROPERTY_PROGRESS.equals(name) ||
                WProgressBar.PROPERTY_UNDETERMINED.equals(name)){
            widget.setDirty();
        }

    }

}
