

package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.layout.PairLayout;

/**
 * Internal class, parent of Label and button.
 *
 * @author Johann Sorel
 */
public class AbstractLabeled extends WContainer {

    public static final int GRAPHIC_TOP = 0;
    public static final int GRAPHIC_RIGHT = 1;
    public static final int GRAPHIC_BOTTOM = 2;
    public static final int GRAPHIC_LEFT = 3;

    public static final int HALIGN_LEFT = 0;
    public static final int HALIGN_CENTER = 1;
    public static final int HALIGN_RIGHT = 2;

    public static final int VALIGN_TOP = 0;
    public static final int VALIGN_CENTER = 1;
    public static final int VALIGN_BOTTOM = 2;

    /**
     * Property for the widget text.
     */
    public static final Chars PROPERTY_TEXT = Chars.constant("Text");
    /**
     * Property for the widget image.
     */
    public static final Chars PROPERTY_GRAPHIC = Chars.constant("Graphic");
    /**
     * Property for the image placement.
     */
    public static final Chars PROPERTY_IMAGE_PLACEMENT = Chars.constant("GraphicPlacement");
    /**
     * Property for the horizontal alignement.
     */
    public static final Chars PROPERTY_HALIGN = Chars.constant("HorizontalAlignment");
    /**
     * Property for the vertical alignment.
     */
    public static final Chars PROPERTY_VALIGN = Chars.constant("VerticalAlignment");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private final WGraphicText textWidget = new WGraphicText();
    private final PairLayout pairLayout;

    public AbstractLabeled() {
        this(null);
    }

    public AbstractLabeled(CharArray text) {
        this(text,null);
    }

    public AbstractLabeled(CharArray text, Widget graphic) {
        pairLayout = new PairLayout();
        setLayout(pairLayout);

        varText().sync(textWidget.varText());
        setText(text);
        setGraphic(graphic);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    /**
     * Get label text.
     * @return CharArray, can be null
     */
    public final CharArray getText() {
        return (CharArray) getPropertyValue(PROPERTY_TEXT);
    }

    /**
     * Set label text.
     * @param text can be null
     */
    public final void setText(CharArray text) {
        setPropertyValue(PROPERTY_TEXT, text);
    }

    /**
     * Get text variable.
     * @return Variable.
     */
    public final Property varText(){
        return getProperty(PROPERTY_TEXT);
    }

    /**
     * Get label graphic.
     * @return Widget, can be null
     */
    public final Widget getGraphic() {
        return (Widget) getPropertyValue(PROPERTY_GRAPHIC);
    }

    /**
     * Set label graphic.
     * @param graphic can be null
     */
    public final void setGraphic(Widget graphic) {
        setPropertyValue(PROPERTY_GRAPHIC, graphic);
    }

    /**
     * Get image variable.
     * @return Variable.
     */
    public final Property varGraphic(){
        return getProperty(PROPERTY_GRAPHIC);
    }

    /**
     * Get graphic placement relative to text
     * @return int
     */
    public int getGraphicPlacement() {
        return pairLayout.getRelativePosition();
    }

    /**
     * Set graphic placement relative to text
     * @param place
     */
    public void setGraphicPlacement(int place) {
        pairLayout.setRelativePosition(place);
    }

    /**
     * Get text and graphic horizontal alignment.
     * @return int
     */
    public int getHorizontalAlignment() {
        return pairLayout.getHorizontalAlignement();
    }

    /**
     * Set text and graphic horizontal alignment.
     * @param align
     */
    public void setHorizontalAlignment(int align) {
       pairLayout.setHorizontalAlignement(align);
    }

    /**
     * Get text and graphic vertical alignment.
     * @return int
     */
    public int getVerticalAlignment() {
        return pairLayout.getVerticalAlignement();
    }

    /**
     * Set text and graphic vertical alignment.
     * @param align
     */
    public void setVerticalAlignment(int align) {
        pairLayout.setVerticalAlignement(align);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_TEXT.equals(name)) {
            CharArray text = (CharArray) value;
            if (text == null || text.isEmpty()) {
                children.remove(textWidget);
            } else {
                if (getChildren().getSize() == 0 || getChildren().get(0) != textWidget){
                    children.add(0, textWidget);
                }
            }
        } else if (PROPERTY_GRAPHIC.equals(name)) {
            Widget graphic = (Widget) value;
            children.remove(oldValue);
            if (graphic != null){
                children.add(graphic);
            }
        }
    }

    @Override
    protected void receiveEventChildren(Event event) {
        super.receiveEventChildren(event);
    }

}
