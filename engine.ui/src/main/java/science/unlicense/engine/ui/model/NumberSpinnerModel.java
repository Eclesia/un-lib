
package science.unlicense.engine.ui.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.ParseRuntimeException;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int16;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int64;
import science.unlicense.common.api.number.Int8;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class NumberSpinnerModel extends CObject implements SpinnerModel{

    private final Class numberClass;
    private final double def;
    private final Double min;
    private final Double max;
    private final double step;

    public NumberSpinnerModel(Class numberClass, Number def, Number min, Number max, Number step){
        this.numberClass = numberClass;
        this.def = def.doubleValue();
        this.min = (min!=null) ? min.doubleValue() : null;
        this.max = (max!=null) ? max.doubleValue() : null;
        this.step = step.doubleValue();
    }

    public boolean isValid(Object value) {
        if (!(value instanceof Number)) return false;
        double i = ((Number) value).doubleValue();
        if (max!=null && i>max) return false;
        if (min!=null && i<min) return false;
        return true;
    }

    public Number getDefaultValue() {
        return toType(def);
    }

    public SpinnerEditor createEditor() {
        return new NumberEditor();
    }

    public boolean hasNextValue(Object currentValue) {
        if (!isValid(currentValue)) return false;
        double i = ((Number) currentValue).doubleValue();
        return (max==null) ? true : (i<max);
    }

    public boolean hasPreviousValue(Object currentValue) {
        if (!isValid(currentValue)) return false;
        double i = ((Number) currentValue).doubleValue();
        return (min==null) ? true : (i>min);
    }

    public Number nextValue(Object currentValue) {
        if (!isValid(currentValue)) throw new InvalidArgumentException("value is not model range");
        double i = ((Number) currentValue).doubleValue();
        i += step;
        if (max != null && i>max) throw new InvalidArgumentException("moving out of model range, use hasNextValue to ensure remaining in range");
        return toType(i);
    }

    public Number previousValue(Object currentValue) {
        if (!isValid(currentValue)) throw new InvalidArgumentException("value is not model range");
        double i = ((Number) currentValue).doubleValue();
        i -= step;
        if (min != null && i<min) throw new InvalidArgumentException("moving out of model range, use hasPreviousValue to ensure remaining in range");
        return toType(i);
    }

    private Number toType(Number value){
        if (numberClass == Int8.class || numberClass == Byte.class){
            return value.byteValue();
        } else if (numberClass == Int16.class || numberClass == Short.class){
            return value.shortValue();
        } else if (numberClass == Int32.class || numberClass == Integer.class){
            return value.intValue();
        } else if (numberClass == Int64.class || numberClass == Long.class){
            return value.longValue();
        } else if (numberClass == Float32.class || numberClass == Float.class){
            return value.floatValue();
        } else if (numberClass == Float64.class || numberClass == Double.class){
            return value.doubleValue();
        } else {
            throw new RuntimeException("Unsupported number class :"+numberClass.getName());
        }
    }

    public Chars toChars(){
        final CharBuffer cb = new CharBuffer();
        cb.append("NumberSpinnerModel[");
        cb.append(numberClass.getSimpleName());
        cb.append(',');
        cb.append(min);
        cb.append(',');
        cb.append(max);
        cb.append("]");
        return cb.toChars();
    }

    private final class NumberEditor extends WTextField implements SpinnerEditor{

        private boolean updating = false;
        private Object value = 0d;

        public NumberEditor() {
            setPropertyValue(XPROP_STYLEMARKER, new Chars("WSpinner-NumberEditor"));
            setValidator(new Predicate() {
                public Boolean evaluate(Object candidate) {
                    try {
                        if (numberClass == Int8.class || numberClass == Byte.class){
                            candidate = Int8.decode((CharArray) candidate);
                        } else if (numberClass == Int16.class || numberClass == Short.class){
                            candidate = Int16.decode((CharArray) candidate);
                        } else if (numberClass == Int32.class || numberClass == Integer.class){
                            candidate = Int32.decode((CharArray) candidate);
                        } else if (numberClass == Int64.class || numberClass == Long.class){
                            candidate = Int64.decode((CharArray) candidate);
                        } else if (numberClass == Float32.class || numberClass == Float.class){
                            candidate = Float32.decode((CharArray) candidate);
                        } else if (numberClass == Float64.class || numberClass == Double.class){
                            candidate = Float64.decode((CharArray) candidate);
                        }
                    } catch (ParseRuntimeException ex) {
                        return false;
                    }
                    return isValid(candidate);
                }
            });
        }

        @Override
        public Widget getWidget() {
            return this;
        }

        @Override
        public Number getValue() {
            return toType((Number) value);
        }

        @Override
        public void setValue(Object value) {
            updating=true;
            this.value = value;

            Number v = (Number) value;
            Chars text;
            if (numberClass == Int8.class || numberClass == Byte.class){
                text = Int32.encode(v.intValue());
            } else if (numberClass == Int16.class || numberClass == Short.class){
                text = Int32.encode(v.intValue());
            } else if (numberClass == Int32.class || numberClass == Integer.class){
                text = Int32.encode(v.intValue());
            } else if (numberClass == Int64.class || numberClass == Long.class){
                text = Int32.encode(v.intValue());
            } else if (numberClass == Float32.class || numberClass == Float.class){
                text = Float64.encode(v.floatValue());
            } else if (numberClass == Float64.class || numberClass == Double.class){
                text = Float64.encode(v.doubleValue());
            } else {
                throw new InvalidArgumentException("Unvalid number class "+ numberClass);
            }

            setText(text);
            updating=false;
        }

        @Override
        protected void valueChanged(Chars name, Object oldValue, Object value) {
            super.valueChanged(name, oldValue, value);

            if (PROPERTY_TEXT.equals(name)) {
                CharArray text = (CharArray) value;
                if (!updating){
                    Object old = value;
                    try {
                        if (numberClass == Int8.class || numberClass == Byte.class){
                            value = Int8.decode((CharArray) text);
                        } else if (numberClass == Int16.class || numberClass == Short.class){
                            value = Int16.decode((CharArray) text);
                        } else if (numberClass == Int32.class || numberClass == Integer.class){
                            value = Int32.decode((CharArray) text);
                        } else if (numberClass == Int64.class || numberClass == Long.class){
                            value = Int64.decode((CharArray) text);
                        } else if (numberClass == Float32.class || numberClass == Float.class){
                            value = Float32.decode((CharArray) text);
                        } else if (numberClass == Float64.class || numberClass == Double.class){
                            value = Float64.decode((CharArray) text);
                        }
                        sendPropertyEvent(PROPERTY_VALUE, old, value);
                    } catch (ParseRuntimeException ex) {
                    }
                }
            } else if (PROPERTY_VALUE.equals(name)) {

            }
        }

    }

}
