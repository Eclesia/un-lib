
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.visual.GraphicImageView;
import science.unlicense.image.api.Image;

/**
 * Widget holding a single image.
 *
 * @author Johann Sorel
 */
public class WGraphicImage extends WLeaf{

    public static final int FITTING_SCALED = 0;
    public static final int FITTING_CENTERED = 1;
    public static final int FITTING_STRETCHED = 2;
    public static final int FITTING_ZOOMED = 3;
    public static final int FITTING_CORNER = 4;

    /**
     * Property for the widget image.
     */
    public static final Chars PROPERTY_IMAGE = Chars.constant("Image");
    /**
     * Property for the widget image fitting method.
     */
    public static final Chars PROPERTY_FITTING = Chars.constant("Fitting");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLeaf.CLASS_DEFAULTS);
        defs.add(PROPERTY_FITTING, FITTING_SCALED);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WGraphicImage() {
        this(null);
    }

    public WGraphicImage(Image image) {
        setPropertyValue(PROPERTY_IMAGE, image);
        setView(new GraphicImageView(this));
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    /**
     * Get graphic image.
     * @return Image, can be null
     */
    public final Image getImage() {
        return (Image) getPropertyValue(PROPERTY_IMAGE);
    }

    /**
     * Set graphic image.
     * @param image can be null
     */
    public final void setImage(Image image) {
       setPropertyValue(PROPERTY_IMAGE, image);
    }

    /**
     * Get image variable.
     * @return Variable.
     */
    public final Property varImage(){
        return getProperty(PROPERTY_IMAGE);
    }

    /**
     * Get image fitting mode.
     * @return Image fitting mode, default is
     */
    public final int getFitting() {
        return (Integer) getPropertyValue(PROPERTY_FITTING);
    }

    /**
     * Set image fitting mode.
     * @param fitting
     */
    public final void setFitting(int fitting) {
       setPropertyValue(PROPERTY_FITTING, fitting);
    }

    /**
     * Get fitting variable.
     * @return Variable.
     */
    public final Property varFitting(){
        return getProperty(PROPERTY_FITTING);
    }
}
