
package science.unlicense.engine.ui.component;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.widget.WContainer;

/**
 * A Breadcrumb bar or WCrumbBar is a button bar which visually shows a path
 * made of different children.
 *
 * @author Johann Sorel
 */
public class WCrumbBar extends WContainer {

    protected static final Dictionary CRUMBCLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant("crumb"));
        CRUMBCLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WCrumbBar() {
        setLayout(new FormLayout());
    }

    public static class WCrumb extends WContainer {

        public WCrumb() {
            super(new BorderLayout());
        }

        /**
         * Get crumb index in it's parent.
         * @return index.
         */
        public int getIndex(){
            final SceneNode parent = getParent();
            if (parent==null) return 0;
            return parent.getChildren().search(this);
        }

        @Override
        protected Dictionary getClassDefaultProperties() {
            return CRUMBCLASS_DEFAULTS;
        }
    }

}
