package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.character.Language;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.country.Country;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventManager;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.Properties;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeMessage;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.desktop.DragAndDropMessage;
import science.unlicense.display.api.desktop.FrameDecoration;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.desktop.cursor.Cursor;
import science.unlicense.display.api.layout.AbstractPositionableNode;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.visual.View;
import science.unlicense.engine.ui.visual.VirtualFrame;
import science.unlicense.engine.ui.visual.WidgetView;
import static science.unlicense.engine.ui.widget.Widget.PROPERTY_DIRTY;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.VectorNf64;

/**
 * Base widget class.
 * Extend Widget class to make a container widget.
 * Extend WLeaf class to make a leaf widget which won't have any children.
 *
 * Widgets all share common properties which are :
 * - min/best/max extent
 * - enable state
 * - visible state
 * - tooltip
 * - popup
 * - style
 *
 * @author Johann Sorel
 */
public abstract class AbstractWidget extends AbstractPositionableNode implements EventListener,Widget {

    /**
     * A prototype property used by some widget to identify a specific visual style.
     */
    public static final Chars XPROP_STYLEMARKER = Chars.constant("StyleMarker");
    private static final VirtualFrame VIRTUAL_FRAME = new VirtualFrame();

    private static final Language DEFAULT_LANGUAGE = Country.GBR.asLanguage();
    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant("widget"));
        defs.add(PROPERTY_ID, Chars.EMPTY);
        defs.add(PROPERTY_ENABLE, Boolean.TRUE);
        defs.add(PROPERTY_STACKENABLE, Boolean.TRUE);
        defs.add(PROPERTY_CHILDFOCUSED, Boolean.FALSE);
        defs.add(PROPERTY_FOCUSED, Boolean.FALSE);
        defs.add(PROPERTY_MOUSEOVER, Boolean.FALSE);
        defs.add(PROPERTY_MOUSEPRESS, Boolean.FALSE);
        defs.add(PROPERTY_LANGUAGE, DEFAULT_LANGUAGE);
        defs.add(PROPERTY_FRAME, VIRTUAL_FRAME);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    // properties
    protected final Properties properties = new Properties(this,getEventManager()) {
        @Override
        protected boolean prevalidateChange(Chars name, Object oldValue, Object value) {
            return AbstractWidget.this.prevalidateChange(name, oldValue, value);
        }

        @Override
        protected void valueChanged(Chars name, Object oldValue, Object value) {
            AbstractWidget.this.valueChanged(name, oldValue, value);
        }
    };
    //mouse state, used by styles
    protected int mouseState = -1;

    private final EventListener childrenListener;

    private StyleDocument cachedStyleSheet;
    private StyleDocument cachedStyleDoc;
    private final Set styleChangingProperties = new HashSet();
    private final Set filterChangingProperties = new HashSet();

    public AbstractWidget(boolean allowChildren) {
        super(allowChildren);
        properties.setDefaults(getClassDefaultProperties());
        //set default property values
        properties.setPropertyValue(PROPERTY_VIEW, new WidgetView(this));

        //the only things that make a widget dirty are :
        // - node transform change : only for parents
        // - node effective extent change
        // - a visual change
        modelTransform.addEventListener(null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                //transform has change, send the event to the parent
                final SceneNode parent = getParent();
                if (parent != null) {
                    final PropertyMessage pe = (PropertyMessage) event.getMessage();
                    AbstractWidget.this.sendPropertyEvent(Similarity.PROPERTY_MATRIX, pe.getOldValue(), pe.getNewValue());
                }
            }
        });

        if (allowChildren) {
            childrenListener = new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    receiveEventChildren(event);
                }
            };
        } else {
            childrenListener = null;
        }
    }

    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    /**
     * Override SceneNode method to store parent in properties dictionnary.
     * @param parent
     */
    @Override
    protected void setParent(SceneNode parent) {
        final boolean changed = !CObjects.equals(this.parent, parent);
        if (changed) {
            this.parent = parent;
            setCacheTransformsDirty();
            setPropertyValue(PROPERTY_PARENT, parent);
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setEffectiveExtent(Extent extent) {
//        //enforce user extents
//        extent = extent.copy();
//        getExtents().constraint(extent);
        if (effExtent!=null && effExtent.equals(extent)) return;
        final Extent old = this.effExtent.copy();
        this.effExtent.set(extent);
        sendPropertyEvent(PROPERTY_EFFECTIVE_EXTENT, old, extent);
        //send a dirty event
        final BBox bbox = new BBox(2);
        double maxSpanX = Maths.max(extent.get(0),old.get(0));
        double maxSpanY = Maths.max(extent.get(1),old.get(1));
        bbox.setRange(0, -maxSpanX/2.0, maxSpanX/2.0);
        bbox.setRange(1, -maxSpanY/2.0, maxSpanY/2.0);
        setDirty(bbox);
    }

    @Override
    public Property varExtents() {
        return getProperty(PROPERTY_EXTENTS);
    }

    @Override
    public Property varReserveSpace() {
        return getProperty(PROPERTY_RESERVE_SPACE);
    }

    @Override
    public Property varEffectiveExtent() {
        return getProperty(PROPERTY_EFFECTIVE_EXTENT);
    }

    /**
     * Set widget id.
     *
     * @param id not null
     */
    @Override
    public final void setId(Object id) {
        setPropertyValue(PROPERTY_ID, id);
    }

    /**
     * @return widget id, never null
     */
    @Override
    public final Object getId() {
        return getPropertyValue(PROPERTY_ID);
    }

    /**
     * Set widget state, enable or disable.
     * @param enable
     */
    @Override
    public void setEnable(boolean enable) {
        if (setPropertyValue(PROPERTY_ENABLE, enable)){
            Widget parent = (Widget) getParent();
            if (parent != null) enable &= parent.isStackEnable();
            setPropertyValue(PROPERTY_STACKENABLE, enable);
        }
    }

    /**
     * @return true if widget is active
     */
    @Override
    public boolean isEnable() {
        //test equality, value could be null at first
        return (Boolean) getPropertyValue(PROPERTY_ENABLE);
    }

    @Override
    public Property varEnable() {
        return getProperty(PROPERTY_ENABLE);
    }

    /**
     * search throw parents to find if any of them is disable.
     * @return true if all parents in the stack are enable.
     */
    @Override
    public final boolean isStackEnable(){
        return (Boolean) getPropertyValue(PROPERTY_STACKENABLE);
    }

    private void recomputeIsEnable() {
        boolean enable = isEnable();
        Widget parent = (Widget) getParent();
        if (parent != null) enable &= parent.isStackEnable();
        setPropertyValue(PROPERTY_STACKENABLE, enable);
    }

    /**
     * Indicate if the widget has a focused child.
     *
     * @return true if widget has a focused child
     */
    @Override
    public final boolean isChildFocused(){
        //test equality, value could be null at first
        return (Boolean) getPropertyValue(PROPERTY_CHILDFOCUSED);
    }

    /**
     * WARNING : INTERNAL USE ONLY, DO NOT CALL IT YOURSELF.
     * USE method requestFocus.
     *
     * @param focused
     */
    protected void setChildFocused(boolean focused){
        setPropertyValue(PROPERTY_CHILDFOCUSED, focused);
    }

    /**
     * Indicate if the widget is focused.
     *
     * @return true if widget is focused
     */
    @Override
    public final boolean isFocused(){
        //test equality, value could be null at first
        return (Boolean) getPropertyValue(PROPERTY_FOCUSED);
    }

    /**
     * WARNING : INTERNAL USE ONLY, DO NOT CALL IT YOURSELF.
     * USE method requestFocus.
     *
     * @param focused
     */
    @Override
    public final void setFocused(boolean focused){
        setPropertyValue(PROPERTY_FOCUSED, focused);
    }

    @Override
    public Property varFocused() {
        return getProperty(PROPERTY_FOCUSED);
    }

    /**
     * Request focus on this widget.
     */
    @Override
    public void requestFocus(){
        final UIFrame frame = getFrame();
        if (frame==null) return;
        frame.setFocusedWidget(this);
    }

    /**
     * Set widget tooltip.
     * @param tooltip, can be null
     */
    @Override
    public final void setToolTip(Widget tooltip) {
        setPropertyValue(PROPERTY_TOOLTIP, tooltip);
    }

    /**
     * @return popup tooltip, can be null.
     */
    @Override
    public final Widget getToolTip() {
        return (Widget) getPropertyValue(PROPERTY_TOOLTIP);
    }

    /**
     * Set widget popup.
     * @param popup, can be null
     */
    @Override
    public final void setPopup(Widget popup) {
        setPropertyValue(PROPERTY_POPUP, popup);
    }

    /**
     * @return popup widget, can be null.
     */
    @Override
    public final Widget getPopup() {
        return (Widget) getPropertyValue(PROPERTY_POPUP);
    }

    /**
     * Set widget language.
     * @param language, can not be null
     * @param recursive, loop on childrens to set language
     */
    @Override
    public void setLanguage(Language language, boolean recursive) {
        setLanguage(language);
        if (recursive) {
            final Iterator ite = children.createIterator();
            while (ite.hasNext()) {
                ((Widget) ite.next()).setLanguage(language, recursive);
            }
        }
    }

    /**
     * @return language, can not be null.
     */
    @Override
    public final Language getLanguage() {
        return (Language) getPropertyValue(PROPERTY_LANGUAGE);
    }

    @Override
    public final void setLanguage(Language language) {
        setPropertyValue(PROPERTY_LANGUAGE, language);
    }

    /**
     * Set widget cursor.
     * @param cursor, can be null
     */
    @Override
    public final void setCursor(Cursor cursor) {
        setPropertyValue(PROPERTY_CURSOR, cursor);
    }

    /**
     * @return cursor  can be null.
     */
    @Override
    public final Cursor getCursor() {
        return (Cursor) getPropertyValue(PROPERTY_CURSOR);
    }

    public final Property varCursor() {
        return getProperty(PROPERTY_CURSOR);
    }

    /**
     * search throw parents to find the first cursor
     * @return Cursor, can be null
     */
    @Override
    public final Cursor getStackCursor(){
        Cursor cursor = getCursor();
        Widget parent = (Widget) getParent();
        while (parent!=null && cursor==null){
            cursor = parent.getCursor();
            parent = (Widget) parent.getParent();
        }
        return cursor;
    }

    @Override
    protected void getExtentsInternal(Extents extents, Extent constraint) {
        final View view = getView();
        if (view != null) view.getExtents(extents, constraint);

        //append graphic margins
        final Document doc = getEffectiveStyleDoc();
        final Margin margin = WidgetStyles.readMargin(doc, StyleDocument.STYLE_PROP_MARGIN, this);
        final double marginSumX = margin.right+margin.left;
        final double marginSumY = margin.top+margin.bottom;
        extents.minX = extents.minX+marginSumX;
        extents.minY = extents.minY+marginSumY;
        extents.bestX = extents.bestX+marginSumX;
        extents.bestY = extents.bestY+marginSumY;
        extents.maxX = extents.maxX+marginSumX;
        extents.maxY = extents.maxY+marginSumY;
    }

    /**
     * WARNING : INTERNAL USE ONLY, DO NOT CALL IT YOURSELF.
     * Reaffect extents which are not overridden.
     */
    @Override
    public boolean updateExtents(){
        return super.updateExtents();
    }

    /**
     * The widget effective inner bounding box, excluding margins and overlaps.
     * @return BoundingBox
     */
    @Override
    public final BBox getInnerExtent(){
        return getInnerExtent(this, getEffectiveExtent());
    }

    public static final BBox getInnerExtent(Widget widget, Extent extent){
        final BBox ext = AbstractWidget.centeredBBox(extent,null);
        final double middleX = ext.getMiddle(0);
        final double middleY = ext.getMiddle(1);
        final Margin margin = WidgetStyles.readMargin(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_MARGIN, widget);

        double minX = ext.getMin(0)+margin.left;
        if (minX > middleX) minX = middleX;
        double maxX = ext.getMax(0)-margin.right;
        if (maxX < middleX) maxX = middleX;

        double minY = ext.getMin(1)+margin.top;
        if (minY > middleY) minY = middleY;
        double maxY = ext.getMax(1)-margin.bottom;
        if (maxY < middleY) maxY = middleY;

        ext.setRange(0, minX,maxX);
        ext.setRange(1, minY,maxY);
        return ext;
    }

    /**
     * INTERNAL USE ONLY, DO NOT CALL THIS METHOD YOURSELF !
     * @param frame
     */
    @Override
    public final void setFrame(UIFrame frame){
        setPropertyValue(PROPERTY_FRAME, frame);
    }

    @Override
    public UIFrame getFrame(){
        return (UIFrame) getPropertyValue(PROPERTY_FRAME);
    }

    @Override
    public Property varFrame() {
        return getProperty(PROPERTY_FRAME);
    }

    /**
     * Style definition,
     * @return
     */
    @Override
    public final StyleDocument getStyle(){
        return (StyleDocument) getPropertyValue(PROPERTY_STYLE);
    }

    @Override
    public void setStyle(StyleDocument style) {
        if (setPropertyValue(PROPERTY_STYLE, style)) {
            notifyStyleChanged();
        }
    }

    /**
     * Style definition,
     * @return
     */
    @Override
    public final StyleDocument getInlineStyle(){
        return (StyleDocument) getPropertyValue(PROPERTY_INLINESTYLE);
    }

    @Override
    public final void setInlineStyle(StyleDocument doc) {
        if (setPropertyValue(PROPERTY_INLINESTYLE, doc)) {
            notifyInlineStyleChanged();
        }
    }

    @Override
    public final void setInlineStyle(CharArray style) {
        if (style != null) {
            final StyleDocument doc = new StyleDocument();
            doc.setProperties(style);
            setInlineStyle(doc);
        } else {
            setInlineStyle((StyleDocument) null);
        }
    }

    @Override
    public void notifyStyleChanged() {
        cachedStyleSheet = null;
        cachedStyleDoc = null;
    }

    @Override
    public void notifyInlineStyleChanged() {
        cachedStyleSheet = null;
        cachedStyleDoc = null;
    }

    @Override
    public StyleDocument getEffectiveStyleSheet() {
        StyleDocument sd = cachedStyleSheet;
        if (sd == null) {
            StyleDocument system = SystemStyle.INSTANCE;
            StyleDocument doc = null;

            StyleDocument style = getStyle();
            if (style != null && !style.getPropertyNames().isEmpty()) {
                doc = new StyleDocument();
                doc.set(system);
                WidgetStyles.mergeDoc(doc, style, false, false);
            }

            StyleDocument inlineStyle = getInlineStyle();
            if (inlineStyle != null && !inlineStyle.getPropertyNames().isEmpty()) {
                if (doc == null) {
                    doc = new StyleDocument();
                    doc.set(system);
                }
                StyleDocument parent = new StyleDocument();
                inlineStyle.setFilter(Predicate.TRUE);
                parent.setPropertyValue(StyleDocument.INLINE_RULE, inlineStyle);
                WidgetStyles.mergeDoc(doc, parent, false, false);
            }

            sd = (doc == null) ? system : doc;
        }
        cachedStyleSheet = sd;
        styleChangingProperties.removeAll();
        filterChangingProperties.removeAll();
        WidgetStyles.listProperties(sd, this, styleChangingProperties, filterChangingProperties);
        return sd;
    }

    @Override
    public StyleDocument getEffectiveStyleDoc() {
        StyleDocument sd = cachedStyleDoc;
        if (sd == null) {
            final StyleDocument stylesheet = getEffectiveStyleSheet();
            sd = new StyleDocument();
            WidgetStyles.evaluate(stylesheet, sd, this);
        }
        cachedStyleDoc = sd;
        return sd;
    }

    @Override
    public Chars toChars() {
        return new Chars(this.getClass().getSimpleName());
    }

    ////////////////////////////////////////////////////////////////////////////
    // properties ///////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public Set getPropertyNames() {
        return properties.getPropertyNames();
    }

    @Override
    public final Object getPropertyValue(Chars name) {
        return properties.getPropertyValue(name);
    }

    @Override
    public final boolean setPropertyValue(Chars name, Object value) {
        return properties.setPropertyValue(name, value);
    }

    @Override
    public final Property getProperty(Chars name) {
        return properties.getProperty(name);
    }

    /**
     * Called before replacing value.
     *
     * @param name
     * @param oldValue
     * @param value
     * @return true if value can be replaced
     */
    protected boolean prevalidateChange(Chars name, Object oldValue, Object value) {
        return true;
    }

    /**
     * Called after a value has been changed.
     * @param name
     * @param oldValue
     * @param newValue
     */
    @Override
    protected void valueChanged(Chars name, Object oldValue, Object newValue) {
        super.valueChanged(name, oldValue, newValue);

        if (PROPERTY_PARENT.equals(name)) {
            if (newValue == null) {
                setFrame(VIRTUAL_FRAME);
            } else {
                setFrame(((Widget) newValue).getFrame());
            }
        } else if (PROPERTY_FRAME.equals(name)) {
            //propagate event to children
            if (!children.isEmpty()) {
                final Iterator ite = children.createIterator();
                while (ite.hasNext()) {
                    final Widget child = (Widget) ite.next();
                    child.setPropertyValue(PROPERTY_FRAME, newValue);
                }
            }
        } else if (PROPERTY_ENABLE.equals(name)) {
            boolean enable = (boolean) newValue;
            Widget parent = (Widget) getParent();
            if (parent != null) enable &= parent.isStackEnable();
            setPropertyValue(PROPERTY_STACKENABLE, enable);
        } else if (PROPERTY_STACKENABLE.equals(name)) {
            //propagate event to children
            if (!children.isEmpty()) {
                final Iterator ite = children.createIterator();
                while (ite.hasNext()) {
                    final AbstractWidget child = (AbstractWidget) ite.next();
                    child.recomputeIsEnable();
                }
            }
        }

        final WidgetView view = (WidgetView) getView();
        view.receiveWidgetEvent(new PropertyMessage(name, oldValue, newValue));

        if (filterChangingProperties.contains(name)) {
            cachedStyleSheet = null;
            cachedStyleDoc = null;
            setDirty();
        } else if (styleChangingProperties.contains(name)) {
            setDirty();
        }
    }

    @Override
    public Dictionary asDictionary() {
        return properties.asDictionary();
    }

    ////////////////////////////////////////////////////////////////////////////
    // rendering ///////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean isDisplayed() {
        return getFrame() != VIRTUAL_FRAME;
    }

    @Override
    public final synchronized void setView(View view) {
        final View old = getView();
        boolean diff = old!=null && !old.equals(view);
        setPropertyValue(PROPERTY_VIEW, view);
        if (diff) {
            updateExtents();
            setDirty();
        }
    }

    @Override
    public final View getView(){
        return (View) getPropertyValue(PROPERTY_VIEW);
    }

    @Override
    public void setDirty(){
        setDirty(null);
    }

    /**
     * Set dirty.
     * @param dirtyArea
     */
    @Override
    public void setDirty(BBox dirtyArea){
        if (!isDisplayed()) return;

        final BBox dirty = new BBox(2);
        if (dirtyArea == null) {
            getView().calculateVisualExtent(dirty);
        } else {
            dirty.set(dirtyArea);
        }

        sendPropertyEvent(PROPERTY_DIRTY, null, dirty);
    }

    ////////////////////////////////////////////////////////////////////////////
    // popup and tooltip ///////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public abstract Tuple getOnSreenPosition(Widget child);

    @Override
    public Tuple getOnSreenPosition(){
        final Widget parent = (Widget) getParent();
        if (parent != null) {
            return parent.getOnSreenPosition(this);
        } else if (getFrame()!=null){
            final UIFrame frame = getFrame();
            final FrameDecoration decoration = frame.getDecoration();
            if (decoration==null) {
                return frame.getOnScreenLocation();
            } else {
                final VectorRW pos = VectorNf64.create(frame.getOnScreenLocation());
                //add frame decoration margin
                final Margin margin = decoration.getMargin();
                pos.setX(pos.get(0)+margin.left);
                pos.setY(pos.get(1)+margin.top);
                return pos;
            }
        } else {
            //not displayed
            return null;
        }
    }

    /**
     * Force showing tooltip.
     */
    @Override
    public void showTooltip(){
        WPopup.showAt(this, getToolTip(), new DefaultPoint(new Vector2f64(0, 0)));
    }

    /**
     * Force showing popup.
     */
    @Override
    public void showPopup(){
        WPopup.showAt(this, getPopup(), new DefaultPoint(new Vector2f64(0, 0)));
    }


    ////////////////////////////////////////////////////////////////////////////
    // events //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////


    @Override
    protected void childAdded(Node child) {
        if (!(child instanceof Widget)) {
            throw new RuntimeException("Unvalid object, expected a Widget but was "+child);
        }
        super.childAdded(child);
        child.addEventListener(PropertyMessage.PREDICATE, childrenListener);
    }

    @Override
    protected void childRemoved(Node child) {
        child.removeEventListener(PropertyMessage.PREDICATE, childrenListener);
        super.childRemoved(child);
    }

    /**
     * All widgets provide at least events of type :
     * - MouseEvent
     * - KeyEvent
     * - PropertyEvent
     * @return Class[]
     */
    @Override
    public Class[] getEventClasses() {
        return new Class[]{
            PropertyMessage.class,
            MouseMessage.class,
            NodeMessage.class,
            KeyMessage.class};
    }

    /**
     * Catch event from children if rerendering is needed.
     * @param event
     */
    @Override
    public final void receiveEvent(Event event) {

        if (event.getMessage() instanceof MouseMessage) {
            final MouseMessage me = (MouseMessage) event.getMessage();
            final int type = me.getType();
            if (type != mouseState) {
                if (type == MouseMessage.TYPE_EXIT){
                    mouseState = -1;
                } else {
                    mouseState = type;
                }
                setPropertyValue(PROPERTY_MOUSEOVER, mouseState != -1);
                setPropertyValue(PROPERTY_MOUSEPRESS, mouseState == MouseMessage.TYPE_PRESS);
            }
        }
        receiveEventParent(event);

        //check triggers
        final StyleDocument style = getEffectiveStyleDoc();
        final Sequence triggers = style.getTriggers();
        for (int i=0,n=triggers.getSize();i<n;i++) {
            final StyleDocument trigger = (StyleDocument) triggers.get(i);
            //check if trigger should be activated
            if (Boolean.TRUE.equals(trigger.getFilter().evaluate(event))) {
                //execute properties, those should be functions.
                final Iterator ite = trigger.getPropertyNames().createIterator();
                while (ite.hasNext()) {
                    Object val = trigger.getPropertyValue((Chars) ite.next());
                    if (val instanceof Expression) {
                        ((Expression) val).evaluate(this);
                    }
                }
            }
        }
    }

    protected void receiveEventParent(Event event) {

        final View view = getView();
        if (view!=null) view.receiveEvent(event);

        //special case for MouseEvent or KeyEvent.
        final EventMessage message = event.getMessage();
        if (message instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) message;
            if (!me.isConsumed()
               && me.getType() == MouseMessage.TYPE_RELEASE
               && me.getButton() == MouseMessage.BUTTON_3){
                //show the popup
                WPopup.showAt(this, getPopup(), new DefaultPoint((TupleRW) me.getMousePosition()));
            }

            if (hasListeners()){
                getEventManager().sendEvent(new Event(this, me));
            }
        } else if (message instanceof KeyMessage){
            if (hasListeners()){
                final KeyMessage ke = (KeyMessage) message;
                getEventManager().sendEvent(new Event(this, ke));
            }
        } else if (message instanceof DragAndDropMessage){
            if (hasListeners()){
                getEventManager().sendEvent(new Event(this, message));
            }
        } else if (message instanceof PropertyMessage) {
            final PropertyMessage pe = (PropertyMessage) message;
            final Chars propertyName = pe.getPropertyName();
            final Object newValue = pe.getNewValue();

            if (PROPERTY_STACKENABLE.equals(propertyName)) {
                if ((Boolean) newValue){
                    if (isEnable()){
                        //stack enable state has changed
                        sendPropertyEvent(PROPERTY_STACKENABLE, false, true);
                    }
                } else {
                    if (isEnable()){
                        //stack enable state has changed
                        sendPropertyEvent(PROPERTY_STACKENABLE, true, false);
                    }
                }
            }
        }
    }

    protected void receiveEventChildren(Event event) {

    }

    @Override
    protected void sendPropertyEvent(Chars name, Object oldValue, Object newValue) {
        super.sendPropertyEvent(name, oldValue, newValue);

        //stack events are passed down to children
        if (PROPERTY_STACKENABLE.equals(name)){
            if (!children.isEmpty()) {
                final PropertyMessage pe = new PropertyMessage(name, oldValue, newValue);
                final Event event = new Event(this, pe);
                final Iterator ite = children.createIterator();
                while (ite.hasNext()) {
                    ((Widget) ite.next()).receiveEvent(event);
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // mouse event state ///////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public final Property varMouseOver() {
        return getProperty(PROPERTY_MOUSEOVER);
    }

    @Override
    public final boolean isMouseOver(){
        return (Boolean) getPropertyValue(PROPERTY_MOUSEOVER);
    }

    @Override
    public final Property varMousePress() {
        return getProperty(PROPERTY_MOUSEPRESS);
    }

    @Override
    public final boolean isMousePress(){
        return (Boolean) getPropertyValue(PROPERTY_MOUSEPRESS);
    }


    ////////////////////////////////////////////////////////////////////////////
    // children properties and listener ////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    protected void addChildren(int index, Object[] array) {
        for (Object n : array){
            final Widget widget = (Widget) n;
            //update children language
            widget.setLanguage(getLanguage(), true);
        }
        super.addChildren(index,array);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Utils for all widgets ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * FOR INTERNAL USE ONLY.
     *
     * @return
     */
    @Override
    public EventManager getEventManager() {
        return super.getEventManager();
    }

}
