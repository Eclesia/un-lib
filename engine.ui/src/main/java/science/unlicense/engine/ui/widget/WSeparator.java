package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.engine.ui.style.StyleDocument;

/**
 * A WSeparator is a horizontal of vertical line.
 *
 * @author Johann Sorel
 */
public class WSeparator extends WLeaf{

    public static final Chars PROPERTY_HORIZONTAL = Chars.constant("Horizontal");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLeaf.CLASS_DEFAULTS);
        defs.add(PROPERTY_HORIZONTAL, Boolean.TRUE);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("separator")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    /**
     * Create a new horizontal separator.
     */
    public WSeparator() {
        this(true);
    }

    /**
     * Create a new separator.
     *
     * @param horizontal
     */
    public WSeparator(boolean horizontal) {
        setHorizontal(horizontal);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public void setHorizontal(boolean horizontal) {
        setPropertyValue(PROPERTY_HORIZONTAL, horizontal);
    }

    public boolean isHorizontal() {
        return (Boolean) getPropertyValue(PROPERTY_HORIZONTAL);
    }

}
