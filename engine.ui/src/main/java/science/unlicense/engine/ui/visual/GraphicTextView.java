
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.model.doc.DocMessage;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.s2d.Graphic2DFactory;
import science.unlicense.display.api.scene.s2d.TextNode2D;
import science.unlicense.engine.ui.style.FontStyle;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.AbstractLabeled;
import science.unlicense.engine.ui.widget.WGraphicText;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class GraphicTextView extends WidgetView {

    private final WGraphicText wtext;

    //visual cache
    private SceneNode graphic = null;

    public GraphicTextView(WGraphicText graphicText) {
        super(graphicText);
        this.wtext = graphicText;
    }

    @Override
    public WGraphicText getWidget() {
        return (WGraphicText) super.getWidget();
    }

    @Override
    public void getExtents(Extents buffer, Extent constraint) {

        final FontStyle font = WidgetStyles.readFontStyle(getWidget().getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_FONT, getWidget());
        if (font != null) {
            final CharArray text = getTranslatedText(getWidget().getText());
            final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
            final FontMetadata meta = fontStore.getFont(font.getFont()).getMetaData();
            final Extent extent = meta.getCharsBox(getTranslatedText(text)).getExtent();
            extent.set(1, meta.getAscent()+meta.getDescent());
            buffer.setAll(extent);

        } else {
            buffer.setAll(0, 0);
        }

    }

    private SceneNode getGraphic(Painter2D painter){
        if (graphic != null) return graphic;

        final BBox innerBBox = getWidget().getInnerExtent();

        final Graphic2DFactory factory = painter.getGraphicFactory();
        final SceneNode node = factory.createNode();
        graphic = node;

        final CharArray text = wtext.getText();
        final FontStyle fontStyle = WidgetStyles.readFontStyle(wtext.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_FONT, wtext);

        //prepare the informations we need
        FontChoice font = null;
        FontMetadata meta = null;

        if (text!=null && fontStyle != null){
            font = fontStyle.getFont();
            if (font!=null){
                //calculate appropriate text position
                final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
                meta = fontStore.getFont(font).getMetaData();
                final CharArray finalText = getTranslatedText(text);
                final BBox textExtent = meta.getCharsBox(finalText);

                final double textHeight = meta.getAscent()+meta.getDescent();

                final double[] trs = new double[]{
                    (innerBBox.getSpan(0)-textExtent.getSpan(0))/2.0
                          + innerBBox.getMin(0)
                          -textExtent.getMin(0),
                    (innerBBox.getSpan(1)-textHeight)/2.0
                          + innerBBox.getMin(1)
                          +meta.getAscent()
                };

                final TextNode2D txtNode = new TextNode2D();
                txtNode.setFont(font);
                txtNode.setText(finalText);
                txtNode.setAnchor(VectorNf64.createDouble(2));
                txtNode.setFills(new Paint[]{fontStyle.getFillPaint()});
                txtNode.getNodeTransform().setToTranslation(trs);
                node.getChildren().add(txtNode);
            }
        }

        return graphic;
    }

    @Override
    protected void renderSelf(Painter2D painter, BBox innerBBox) {
        final SceneNode node = getGraphic(painter);
        if (node!=null) painter.render(node);
    }

    @Override
    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        final Chars name = event.getPropertyName();
        if (  WGraphicText.PROPERTY_TEXT.equals(name)
          || AbstractLabeled.PROPERTY_LANGUAGE.equals(name)
          || AbstractLabeled.PROPERTY_FRAME.equals(name)
                ){
            graphic = null;
            widget.updateExtents();
            widget.setDirty();
        }
    }

    @Override
    public void receiveStyleEvent(DocMessage event) {
        super.receiveStyleEvent(event);
        graphic = null;
    }

}
