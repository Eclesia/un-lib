
package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.widget.WGraphicImage;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractPathPresenter implements PathPresenter {

    public Image createMimeImage(Extent.Long size) {
        return Images.create(size, Images.IMAGE_TYPE_RGBA);
    }

    public Image createImage(Path path, Extent.Long size) {
        return createMimeImage(size);
    }

    /**
     * Create a WLabel displaying an image of the most appropriate size.
     *
     * @param path
     * @return WLabel
     */
    public Widget createInteractive(final Path path) {
        final WLabel lbl = new WLabel();
        lbl.setVerticalAlignment(WLabel.VALIGN_CENTER);
        lbl.setHorizontalAlignment(WLabel.HALIGN_CENTER);
        lbl.addEventListener(PropertyMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                if (!Widget.PROPERTY_EFFECTIVE_EXTENT.equals(pe.getPropertyName())) return;

                new Thread(){
                    public void run() {
                        final Extent ext = ((Extent) pe.getNewValue()).copy();
                        ext.set(0, ext.get(0)-20);
                        ext.set(1, ext.get(1)-20);
                        final Image img = createImage(path, new Extent.Long((long) ext.get(0),(long) ext.get(1)));
                        WGraphicImage graphic = new WGraphicImage(img);
                        graphic.setOverrideExtents(new Extents(1, 1, Double.NaN, Double.NaN, Double.MAX_VALUE, Double.MAX_VALUE));
                        lbl.setGraphic(graphic);
                    }
                }.start();

            }
        });

        return lbl;
    }

}
