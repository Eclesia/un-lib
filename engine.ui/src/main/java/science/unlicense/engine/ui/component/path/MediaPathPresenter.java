

package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.ImagePainter2D;
import science.unlicense.display.api.painter2d.Painters;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Resources;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.RoundedRectangle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.impl.process.geometric.RescaleOperator;
import science.unlicense.media.api.ImagePacket;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.Medias;

/**
 *
 * @author Johann Sorel
 */
public class MediaPathPresenter extends AbstractPathPresenter{

    public static final MediaPathPresenter INSTANCE = new MediaPathPresenter();

    private Image mimeImage = null;

    @Override
    public float getPriority() {
        return 3;
    }

    @Override
    public boolean canHandle(Path path) {
        try {
            return Medias.canDecode(path);
        } catch (IOException ex) {
            return false;
        }
    }

    @Override
    public Image createMimeImage(Extent.Long size) {
        if (mimeImage != null && mimeImage.getExtent().get(0)== size.get(0)){
            return mimeImage;
        }
        //create a small mime image
        final ImagePainter2D painter = Painters.getPainterManager().createPainter((int) size.get(0), (int) size.get(1));
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
        painter.setPaint(new ColorPaint(Color.BLACK));
        painter.stroke(new RoundedRectangle(2, 2, size.get(0)-4, size.get(1)-4));
        painter.flush();
        mimeImage = painter.getImage();
        painter.dispose();
        return mimeImage;
    }

    @Override
    public Image createImage(Path path, Extent.Long size) {

        Image image = null;
        try {
            //open a video media
            final Store store = Medias.open(path);
            final Media media = (Media) Resources.findFirst(store, Media.class);
            if (media != null) {
                final MediaReadParameters mediaParams = new MediaReadParameters();
                mediaParams.setStreamIndexes(new int[]{0});
                final MediaReadStream reader = ((Media) store).createReader(mediaParams);

                for (MediaPacket pack=reader.next();pack!=null;pack=reader.next()){
                    image = ((ImagePacket) pack).getImage();
                    break;
                }
            }
        } catch(IOException | StoreException ex) {
            ex.printStackTrace();
        }

        if (image != null) {
            try {
                //TODO image is not centered
                final double scalex = size.get(0) / image.getExtent().get(0);
                final double scaley = size.get(1) / image.getExtent().get(1);
                final Extent ext;
                if (scalex<scaley){
                    ext = new Extent.Double(size.get(0), image.getExtent().get(1)*scalex);
                } else {
                    ext = new Extent.Double(image.getExtent().get(0)*scaley, size.get(1));
                }
                image = RescaleOperator.execute(image, ext);

                return image;
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        return null;
    }

    @Override
    public Widget createInteractive(Path path) {
        try {
            WMediaViewer prev =  new WMediaViewer(path);
            return prev;
        } catch (IOException | StoreException ex) {
            ex.printStackTrace();
            return new WLabel(new Chars(ex.getMessage()));
        }
    }

}
