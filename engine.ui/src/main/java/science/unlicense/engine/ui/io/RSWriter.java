
package science.unlicense.engine.ui.io;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.doc.Field;
import science.unlicense.common.api.predicate.And;
import science.unlicense.common.api.predicate.Constant;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.common.api.predicate.Function;
import science.unlicense.common.api.predicate.Or;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.engine.ui.style.ExpressionReference;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.Colors;

/**
 * Writing Ruled Site files.
 *
 * @author Johann Sorel
 */
public class RSWriter {

    private RSWriter(){}

    public static Chars toChars(StyleDocument doc) {
        final CharBuffer buffer = new CharBuffer();
        Chars name = doc.getName();
        if (name!=null && !name.isEmpty()){
            buffer.append(name);
            buffer.append(" : ");
        }

        buffer.append("{\n");
        buffer.append(propertiesToChars(doc));
        buffer.append("\n}");
        return buffer.toChars();
    }

    public static Chars propertiesToChars(StyleDocument doc) {
        final CharBuffer buffer = new CharBuffer();

        final Iterator ite = doc.getPropertyNames().createIterator();
        while (ite.hasNext()){
            final Field pair = doc.getProperty((Chars) ite.next());
            final Chars id = pair.getName();
            if (!buffer.isEmpty()){
                buffer.append("\n");
            }
            final Chars txt = propertyToChars(pair);
            buffer.append(txt);
        }

        return buffer.toChars();
    }

    public static Chars rulesToChars(StyleDocument doc){
        final CharBuffer cb = new CharBuffer();
        cb.append('{');
        final Sequence rules = doc.getRules();
        for (int i=0,n=rules.getSize();i<n;i++){
            if (i>0) cb.append('\n');
            final StyleDocument rule = (StyleDocument) rules.get(i);
            cb.append(toChars(rule));
        }
        cb.append('}');
        return cb.toChars();
    }

    private static Chars propertyToChars(final Field pair) {
        final CharBuffer buffer = new CharBuffer();
        final Chars id = pair.getName();
        Object[] values = WidgetStyles.getFieldValues(pair);
        Object value = values;
        if (values.length==1){
            value = values[0];
        }

        buffer.append(id);
        buffer.append(" : ");
        Chars txt = toChars(value);
        Chars[] parts = txt.split('\n');
        buffer.append(parts[0]);
        for (int k=1;k<parts.length;k++){
            buffer.append('\n');
            buffer.append("  ");
            buffer.append(parts[k]);
        }

        return buffer.toChars();
    }

    private static Chars toChars(Object candidate){
        if (WidgetStyles.isNone(candidate)){
            return WidgetStyles.NONE;
        } else if (candidate instanceof Constant){
            candidate = ((Constant) candidate).getValue();
        } else if (candidate instanceof PropertyName){
            return new Chars("$"+((PropertyName) candidate).getName());
        } else if (candidate instanceof ExpressionReference){
            return new Chars("@"+((ExpressionReference) candidate).getReferenceProperty());
        } else if (candidate instanceof Function){
            final Function fct = (Function) candidate;
            final CharBuffer cb = new CharBuffer();
            cb.append(fct.getName());
            cb.append('(');
            final Expression[] parameters = fct.getParameters();
            for (int i=0;i<parameters.length;i++){
                if (i>0) cb.append(' ');
                cb.append(toChars(parameters[i]));
            }
            cb.append(')');

            return cb.toChars();
        } else if (candidate instanceof StyleDocument){
            return toChars(((StyleDocument) candidate));
        }
        if (candidate instanceof Sequence){
            candidate = ((Sequence) candidate).toArray();
        }

        if (candidate instanceof Color){
            Color c = (Color) candidate;
            int[] argb = Colors.toARGB(c.toARGB(), null);
            candidate = new Chars("rgba("+argb[1]+" "+argb[2]+" "+argb[3]+" "+argb[0]+")");
        } else if (candidate instanceof CharArray){
            candidate = new Chars("'"+candidate+"'");
        } else if (candidate instanceof Predicate){
            final CharBuffer cb = new CharBuffer();
            if (candidate instanceof EqualsPredicate){
                cb.append(toChars(((EqualsPredicate) candidate).getExp1()));
                cb.append(" = ");
                cb.append(toChars(((EqualsPredicate) candidate).getExp2()));
            } else if (candidate instanceof InPredicate){
                cb.append(toChars(((InPredicate) candidate).getExp1()));
                cb.append(" IN ");
                cb.append(toChars(((InPredicate) candidate).getExp2()));
            } else if (candidate instanceof And){
                final Predicate[] subs = ((And) candidate).getPrecidates();
                cb.append(toChars(subs[0]));
                for (int i=1;i<subs.length;i++){
                    cb.append(" AND ");
                    cb.append(toChars(subs[i]));
                }
            } else if (candidate instanceof Or){
                final Predicate[] subs = ((And) candidate).getPrecidates();
                cb.append(toChars(subs[0]));
                for (int i=1;i<subs.length;i++){
                    cb.append(" OR ");
                    cb.append(toChars(subs[i]));
                }
            } else if (candidate instanceof ModExpression){
                cb.append(toChars(((ModExpression) candidate).getExp1()));
                cb.append(" % ");
                cb.append(toChars(((ModExpression) candidate).getExp2()));
            } else {
                throw new InvalidArgumentException("Unexpected predicate type "+candidate);
            }

            return cb.toChars();
        } else if (candidate instanceof Object[]){
            Object[] array = (Object[]) candidate;
            final CharBuffer cb = new CharBuffer();
            cb.append('[');
            for (int i=0;i<array.length;i++){
                if (i>0) cb.append(',');
                cb.append(toChars(array[i]));
            }
            cb.append(']');
            return cb.toChars();
        }

        return CObjects.toChars(candidate);
    }

}
