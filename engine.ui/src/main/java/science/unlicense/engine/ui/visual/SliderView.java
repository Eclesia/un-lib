
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.style.FontStyle;
import science.unlicense.engine.ui.style.GraphicStyle;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WSlider;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;

/**
 *
 * @author Johann Sorel
 */
public class SliderView extends WidgetView {

    private static final Chars MARKER = Chars.constant("marker");

    private static final double MARK_RADIUS = 7;
    private boolean pressed = false;

    public SliderView(WSlider widget) {
        super(widget);
    }

    @Override
    public WSlider getWidget() {
        return (WSlider) super.getWidget();
    }

    @Override
    public void getExtents(Extents buffer, Extent constraint) {

        final FontStyle font = WidgetStyles.readFontStyle(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_FONT, widget);

        final Extent extent = new Extent.Double(0,0);

        //get text size
        final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
        final FontMetadata meta = font!=null ? fontStore.getFont(font.getFont()).getMetaData() : null;

        CharArray text = (CharArray) WidgetStyles.getOrEvaluate(widget.getEffectiveStyleDoc(), StyleDocument.PROPERTY_TEXT_SAMPLE, widget);

        if (meta != null) {
            final BBox bbox = meta.getCharsBox(text);
            extent.set(0, bbox.getSpan(0));
            extent.set(1, meta.getAscent()+meta.getDescent());
        }

        buffer.setAll(extent);
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }

    @Override
    protected void renderSelf(Painter2D painter, BBox innerBBox) {

        //paint the marker
        final GraphicStyle[] markerGraphics = WidgetStyles.readShapeStyle(widget.getEffectiveStyleDoc(), MARKER, widget);

        final double ratio = getWidget().getModel().getRatio(getWidget().getValue());

        final Rectangle rectangle = new Rectangle(innerBBox);
        rectangle.setX(innerBBox.getMin(0) + innerBBox.getSpan(0) * ratio);
        rectangle.setWidth(1);

        renderShape(painter, markerGraphics, rectangle);
    }

    @Override
    public void receiveEvent(Event event) {

        final EventMessage message = event.getMessage();
        if (!message.isConsumed() && message instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) message;
            if (me.getType() == MouseMessage.TYPE_PRESS){
                pressed = true;
                updateValue(me);
                me.consume();
            } else if (me.getType() == MouseMessage.TYPE_MOVE){
                if (pressed){
                    updateValue(me);
                    me.consume();
                }
            } else if (me.getType() == MouseMessage.TYPE_RELEASE){
                pressed = false;
                me.consume();
            } else if (me.getType() == MouseMessage.TYPE_EXIT){
                pressed = false;
            }
        }

    }

    @Override
    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        final Chars pname = event.getPropertyName();
        if (   WSlider.PROPERTY_MODEL.equals(pname)
           || WSlider.PROPERTY_VALUE.equals(pname) ){
            widget.setDirty();
        }
    }

    private void updateValue(MouseMessage me){
        final Tuple tuple = me.getMousePosition();
        final BBox bbox = widget.getBoundingBox(null);
        final double width = bbox.getSpan(0) - MARK_RADIUS*2;
        final double x = Maths.clamp(tuple.get(0)-MARK_RADIUS-bbox.getMin(0), 0, width);
        final Object value = getWidget().getModel().getValue(x/width);
        getWidget().setValue(value);
    }

}
