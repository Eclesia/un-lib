

package science.unlicense.engine.ui.io;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.common.api.predicate.Predicate;

/**
 *
 * @author Johann Sorel
 */
public class EqualsPredicate extends CObject implements Predicate {

    public static final Chars.Constant CLASS = Chars.constant("Class",CharEncodings.US_ASCII);

    private final Expression exp1;
    private final Expression exp2;

    public EqualsPredicate(Expression exp1, Expression exp2) {
        CObjects.ensureNotNull(exp1);
        CObjects.ensureNotNull(exp2);
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    public Expression getExp1() {
        return exp1;
    }

    public Expression getExp2() {
        return exp2;
    }

    public Chars getPropertyName() {
        return ((PropertyName) exp1).getName();
    }

    @Override
    public Boolean evaluate(Object candidate) {
        if (candidate == null) return false;

        Object obj1 = exp1.evaluate(candidate);
        Object obj2 = exp2.evaluate(candidate);

        if (obj1 instanceof Class) {
            return ((Class) obj2).isAssignableFrom((Class) obj1);
        }

        if (obj1 instanceof science.unlicense.common.api.number.Number) {
            obj1 = ((science.unlicense.common.api.number.Number) obj1).toDouble();
        }
        if (obj2 instanceof science.unlicense.common.api.number.Number) {
            obj2 = ((science.unlicense.common.api.number.Number) obj2).toDouble();
        }

        if (obj1 instanceof Number && obj2 instanceof Number){
            return ((Number) obj1).doubleValue() == ((Number) obj2).doubleValue();
        }

        return CObjects.equals(obj1, obj2);
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("Equals"), new Object[]{exp1,exp2});
    }

    @Override
    public int getHash() {
        int hash = 7;
        hash = 29 * hash + (this.exp1 != null ? this.exp1.hashCode() : 0);
        hash = 29 * hash + (this.exp2 != null ? this.exp2.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EqualsPredicate other = (EqualsPredicate) obj;
        if (this.exp1 != other.exp1 && (this.exp1 == null || !this.exp1.equals(other.exp1))) {
            return false;
        }
        if (this.exp2 != other.exp2 && (this.exp2 == null || !this.exp2.equals(other.exp2))) {
            return false;
        }
        return true;
    }

}
