
package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.FrameManager;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.display.api.layout.SplitLayout;
import science.unlicense.display.impl.RecentHistory;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.component.WButtonBar;
import static science.unlicense.engine.ui.component.path.PathView.PROPERTY_FILTER;
import static science.unlicense.engine.ui.component.path.PathView.PROPERTY_SELECTED_PATHS;
import static science.unlicense.engine.ui.component.path.PathView.PROPERTY_VIEW_ROOT;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.model.CheckGroup;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.RowModel;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicText;
import science.unlicense.engine.ui.widget.WSelect;
import science.unlicense.engine.ui.widget.WSeparator;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.engine.ui.widget.WSwitch;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.engine.ui.widget.menu.WMenuSwitch;

/**
 * Path chooser component.
 *
 * @author Johann Sorel
 */
public class WPathChooser extends WContainer {

    private static final Chars BUTTON_STYLE = new Chars("graphic:none\nmargin:none");
    private static final Chars BUTTON_STYLE_SMALL = new Chars("graphic:none\nmargin:[5,0,5,0]");
    private static final Chars FONT_STYLE = new Chars("font:{fill-paint:@color-main\nfamily:font('Unicon' 1.5em 'none')\n}");
    private static final Chars FONT_STYLE_SMALL = new Chars("font:{\nfill-paint:@color-background\nfamily:font('Unicon' 10 'none')\n}");


    static final Chars STYLE_FILE = new Chars("graphic.0:{\nshape:glyph('Unicon' '\uE02A' false)\nfill-paint:@color-main\ntransform:scaleTransform()\n}");
    static final Chars STYLE_FOLDER = new Chars("graphic.0:{shape:glyph('Unicon' '\uE016' false)\nfill-paint:@color-focus\ntransform:scaleTransform()\n}");
    static final Chars STYLE_BACK = new Chars("graphic.0:{shape:glyph('Unicon' '\uE016' false)\nfill-paint:@color-focus\ntransform:scaleTransform()\n}"
                                            + "graphic.1:{shape:glyph('Unicon' '\uE00F' false)\nfill-paint:@color-background\ntransform:scaleTransform()\nmargin:[-40,-5,-5,-20]\n}");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(PROPERTY_SELECTED_PATHS, AbstractPathView.DEFAULT_SELECTED_PATHS);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    static final Sorter FILE_SORTER = new Sorter() {
        public int sort(Object first, Object second) {
            try{
                if (first instanceof Path && second instanceof Path){
                    final Path p1 = (Path) first;
                    final Path p2 = (Path) second;
                    if (p1.isContainer() && p2.isContainer()){
                        return p1.getName().toString().compareToIgnoreCase(p2.getName().toString());
                    } else if (p1.isContainer()){
                        return -1;
                    } else if (p2.isContainer()){
                        return +1;
                    } else {
                        return p1.getName().toString().compareToIgnoreCase(p2.getName().toString());
                    }
                }
            }catch(IOException ex){
                ex.printStackTrace();
                return 0;
            }
            return 0;
        }
    };

    private final RecentHistory history = new RecentHistory();

    //top bar
    private final WMenuButton goToHome = new WMenuButton(null, new WGraphicText(new Chars("\uE024")), new EventListener() {
        public void receiveEvent(Event event) {
            setViewRoot(Paths.resolve((Chars) science.unlicense.system.System.get().getProperties()
                    .getSystemTree().search(new Chars("system/user/home")).getValue()));
        }
    });
    private final WTextField pathText = new WTextField();
    private final WMenuButton showHidden = new WMenuButton(null, new WGraphicText(new Chars("\uE029")),new EventListener() {
        public void receiveEvent(Event event) {
            final boolean viewHidden = !leftView.isHiddenFileVisible();
            leftView.setHiddenFileVisible(viewHidden);
            centerView.setHiddenFileVisible(viewHidden);
        }
    });
    private final WMenuButton previous = new WMenuButton(null, new WGraphicText(new Chars("\uE02F")),new EventListener() {
        public void receiveEvent(Event event) {
            setViewRoot((Path) history.getPrevious());
        }
    });
    private final WMenuButton next = new WMenuButton(null, new WGraphicText(new Chars("\uE02E")),new EventListener() {
        public void receiveEvent(Event event) {
            setViewRoot((Path) history.getNext());
        }
    });
    private final WMenuButton retract = new WMenuButton(null, new WGraphicText(new Chars("\uE004")),new EventListener() {
        double previous = 100;
        public void receiveEvent(Event event) {
            final SplitLayout l = (SplitLayout) centerSplit.getLayout();
            if (l.getSplitPosition()==0){
                l.setSplitPosition(previous);
            } else {
                previous = l.getSplitPosition();
                l.setSplitPosition(0);
            }
        }
    });
    private final WContainer leftViewsButtons = new WContainer(new GridLayout(1, -1));
    private final WContainer centerViewsButtons = new WContainer(new GridLayout(1, -1));
    private final WContainer shortcutButtons = new WContainer(new GridLayout(-1, 1));
    private final WContainer leftSplit = new WContainer(new BorderLayout());

    //bottom bar
    private final WButton ok = new WButton(new Chars("Ok"), null,new EventListener() {
        public void receiveEvent(Event event) {
            new Thread(){
                public void run() {
                    if (dialog!=null){
                        dialog.setVisible(false);
                        dialog.dispose();
                        dialog = null;
                    }
                }
            }.start();
        }
    });
    private final WButton cancel = new WButton(new Chars("Cancel"), null,new EventListener() {
        public void receiveEvent(Event event) {
            setSelectedPath(new Path[0]);
            new Thread(){
                public void run() {
                    if (dialog!=null){
                        dialog.setVisible(false);
                        dialog.dispose();
                        dialog = null;
                    }
                }
            }.start();
        }
    });

    //selection informations
    private UIFrame dialog = null;

    //predicate informations
    private final Sequence predicates = new ArraySequence();
    private final RowModel predModel = new DefaultRowModel(predicates);
    private final WSelect predSelect = new WSelect(predModel);

    //views
    private final WSplitContainer centerSplit = new WSplitContainer();
    private final WContainer bottomPane = new WContainer(new BorderLayout());
    private PathView[] leftViews = new PathView[0];
    private WSwitch[] leftSwitchs = new WSwitch[0];
    private PathView[] centerViews = new PathView[0];
    private WSwitch[] centerSwitchs = new WSwitch[0];
    //currently active views
    private PathView leftView = null;
    private PathView centerView = null;

    public WPathChooser() {
        super(new BorderLayout());
        predicates.add(Predicate.TRUE);

        previous.varEnable().sync(history.varPreviousExist(),true);
        next.varEnable().sync(history.varNextExist(),true);

        goToHome.setInlineStyle(BUTTON_STYLE);
        showHidden.setInlineStyle(BUTTON_STYLE);
        previous.setInlineStyle(BUTTON_STYLE);
        next.setInlineStyle(BUTTON_STYLE);
        retract.setInlineStyle(BUTTON_STYLE_SMALL);
        goToHome.getGraphic().setInlineStyle(FONT_STYLE);
        showHidden.getGraphic().setInlineStyle(FONT_STYLE);
        previous.getGraphic().setInlineStyle(FONT_STYLE);
        next.getGraphic().setInlineStyle(FONT_STYLE);
        retract.getGraphic().setInlineStyle(FONT_STYLE_SMALL);
        predSelect.setVisible(false);

        //change the split to have a retract button
        final WContainer space = new WContainer(new LineLayout());
        space.setPropertyValue(XPROP_STYLEMARKER, new Chars("SplitSeparator"));
        space.addChild(retract, null);
        space.setInlineStyle(new Chars("background:{\nfill-paint:@color-delimiter\n}"));
        ((SplitLayout) centerSplit.getLayout()).setSplitGap(space.getExtents(null).bestX);
        centerSplit.getChildren().removeAll();
        centerSplit.addChild(space, SplitConstraint.SPLITTER);

        //build shortcuts
        if (science.unlicense.system.System.get().getType().equals(science.unlicense.system.System.TYPE_UNIX)){
            final String userName = science.unlicense.system.System.get().getProperties().getSystemTree().search(new Chars("system/user/name")).getValue().toString();
            final Path p1 = Paths.resolve(new Chars("file:/media/"+userName)); //debian-ubuntu
            final Path p2 = Paths.resolve(new Chars("file:/run/media/"+userName)); //archlinux-manjaro
            final Sequence points = new ArraySequence();
            points.addAll(p1.getChildren());
            points.addAll(p2.getChildren());
            final Path[] mounts = (Path[]) points.toArray(Path.class);
            for (int i=0;i<mounts.length;i++){
                final Path mp = mounts[i];
                final WSpace leaf = new WSpace(14, 14);
                leaf.setInlineStyle(WPathChooser.STYLE_FOLDER);
                leaf.getInlineStyle().setProperties(new Chars("margin:0"));
                leaf.setOverrideExtents(new Extents(14, 14));
                final WMenuButton go = new WMenuButton(new Chars(mp.getName()),leaf,new EventListener() {
                    public void receiveEvent(Event event) {
                        setViewRoot(mp);
                    }
                });
                shortcutButtons.getChildren().add(go);
            }
            shortcutButtons.addChild(new WSeparator(true), null);
        }
        shortcutButtons.setInlineStyle(new Chars("background:{fill-paint   : lineargradientfill('%' 0 0 0 1 0 rotateHSL(@color-background 0 1 0.95) 1 rotateHSL(@color-background 0 1 1.05))\n}"));
        leftSplit.addChild(shortcutButtons, BorderConstraint.TOP);

        //top panel
        final WButtonBar topPane = new WButtonBar();
        ((FormLayout) topPane.getLayout()).setColumnSize(4, FormLayout.SIZE_EXPAND);
        topPane.addChild(leftViewsButtons,  FillConstraint.builder().build());
        topPane.addChild(previous,          FillConstraint.builder().coord(1, 0).fill(true, true).build());
        topPane.addChild(next,              FillConstraint.builder().coord(2, 0).fill(true, true).build());
        topPane.addChild(goToHome,          FillConstraint.builder().coord(3, 0).fill(true, true).build());
        topPane.addChild(pathText,          FillConstraint.builder().coord(4, 0).fill(true, true).build());
        topPane.addChild(centerViewsButtons,FillConstraint.builder().coord(5, 0).fill(true, true).build());
        topPane.setInlineStyle(new Chars("margin:8"));

        //bottom panel
        //predicates list
        final WContainer predPane = new WContainer(new BorderLayout());
        predPane.addChild(predSelect,BorderConstraint.CENTER);
        //ok/cancel pane
        final WContainer rightPane = new WContainer(new GridLayout(1, 2, 10, 10));
        rightPane.getChildren().add(cancel);
        rightPane.getChildren().add(ok);
        final WContainer okcancelPane = new WContainer(new BorderLayout());
        okcancelPane.setInlineStyle(new Chars("margin:[8,8,8,8]"));
        okcancelPane.addChild(rightPane, BorderConstraint.RIGHT);
        bottomPane.addChild(predPane, BorderConstraint.CENTER);
        bottomPane.addChild(okcancelPane, BorderConstraint.RIGHT);

        //add all blocks
        addChild(topPane,BorderConstraint.TOP);
        addChild(centerSplit,BorderConstraint.CENTER);

        setLeftViews(new PathView[]{new WTreePathView()});
        setCenterViews(new PathView[]{
            new WListView(),
            new WTableView(),
            new WPreviewView(),
            new WInteractiveView()});

        final Chars home = (Chars) science.unlicense.system.System.get().getProperties()
                    .getSystemTree().search(new Chars("system/user/home")).getValue();
        setViewRoot(Paths.resolve(home));
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    /**
     * Set the possible left side explorer views.
     * If there is more then one, buttons will be visible to switch between them.
     * @param views can be null
     */
    public void setLeftViews(PathView[] views){
        leftViewsButtons.getChildren().removeAll();
        setSelectedLeftView(null);

        if (views==null) views = new PathView[0];
        this.leftViews = Arrays.copy(views, new PathView[views.length]);

        if (leftViews.length>0){
            //rebuild chooser buttons
            final CheckGroup group = new CheckGroup();
            leftSwitchs = new WSwitch[views.length];
            for (int i=0;i<views.length;i++){
                final PathView pv = views[i];
                pv.varFilter().sync(varFilter());
                leftSwitchs[i] = new WMenuSwitch(null, pv.getIcon(), new EventListener() {
                    public void receiveEvent(Event event) {
                        if (((WSwitch) event.getSource()).isCheck()){
                            setSelectedLeftView(pv);
                        }
                    }
                });
                group.add(leftSwitchs[i]);
                if (views.length>1) leftViewsButtons.getChildren().add(leftSwitchs[i]);
            }

            setSelectedLeftView(leftViews[0]);
        }
    }

    /**
     * Get currently used left side views.
     * @return PathView array, never null but can be empty
     */
    public PathView[] getLeftViews(){
        return Arrays.copy(leftViews, new PathView[leftViews.length]);
    }

    /**
     * Set active left side view.
     * @param view , use null to desactivate left view
     */
    public void setSelectedLeftView(PathView view){
        if (leftView==view) return;
        if (view!=null && !Arrays.contains(leftViews, view)){
            throw new InvalidArgumentException("View is not in the list");
        }
        //remove previous view
        if (leftView!=null){
            leftView.varViewRoot().unsync();
            leftView.varSelectedPath().unsync();
            leftSplit.getChildren().remove(leftView.getViewWidget());
        }
        this.leftView = view;
        if (view!=null){
            leftSwitchs[Arrays.getFirstOccurence(leftViews, 0, leftViews.length, view)].setCheck(true);
            //add new view
            view.varViewRoot().sync(varViewRoot());
            view.varSelectedPath().sync(varSelectedPath());
            leftSplit.addChild(view.getViewWidget(), BorderConstraint.CENTER);
            if (!centerSplit.getChildren().contains(leftSplit)){
                centerSplit.addChild(leftSplit, SplitConstraint.LEFT);
            }
        } else {
            centerSplit.getChildren().remove(leftSplit);
        }
    }

    /**
     * Get active left side view.
     * @return PathView, can be null
     */
    public PathView getSelectedLeftView(){
        return leftView;
    }

    /**
     * Set the possible center side explorer views.
     * If there is more then one, buttons will be visible to switch between them.
     * @param views can be null
     */
    public void setCenterViews(PathView[] views){
        centerViewsButtons.getChildren().removeAll();
        setSelectedCenterView(null);

        if (views==null) views = new PathView[0];
        this.centerViews = Arrays.copy(views, new PathView[views.length]);

        if (centerViews.length>0){
            //rebuild chooser buttons
            final CheckGroup group = new CheckGroup();
            centerSwitchs = new WSwitch[views.length];
            for (int i=0;i<views.length;i++){
                final PathView pv = views[i];
                pv.varFilter().sync(varFilter());
                centerSwitchs[i] = new WMenuSwitch(null, pv.getIcon(), new EventListener() {
                    public void receiveEvent(Event event) {
                        if (((WSwitch) event.getSource()).isCheck()){
                            setSelectedCenterView(pv);
                        }
                    }
                });
                group.add(centerSwitchs[i]);
                if (views.length>1) centerViewsButtons.getChildren().add(centerSwitchs[i]);
            }

            setSelectedCenterView(centerViews[0]);
        }
    }

    /**
     * Get currently used center side views.
     * @return PathView array, never null but can be empty
     */
    public PathView[] getCenterViews(){
        return Arrays.copy(centerViews, new PathView[centerViews.length]);
    }

    /**
     * Set active center side view.
     * @param view , use null to desactivate left view
     */
    public void setSelectedCenterView(PathView view){
        if (centerView==view) return;
        if (view!=null && !Arrays.contains(centerViews, view)){
            throw new InvalidArgumentException("View is not in the list");
        }
        //remove previous view
        if (centerView!=null){
            centerView.varViewRoot().unsync();
            centerView.varSelectedPath().unsync();
            centerSplit.getChildren().remove(centerView.getViewWidget());
        }
        this.centerView = view;
        if (view!=null){
            centerSwitchs[Arrays.getFirstOccurence(centerViews, 0, centerViews.length, view)].setCheck(true);
            //add new view
            if (centerView!=null){
                centerView.varViewRoot().sync(varViewRoot());
                centerView.varSelectedPath().sync(varSelectedPath());
                centerSplit.addChild(centerView.getViewWidget(), SplitConstraint.RIGHT);
            }
        }
    }

    /**
     * Get active center side view.
     * @return PathView, can be null
     */
    public PathView getSelectedCenterView(){
        return centerView;
    }

    /**
     * {@inheritDoc }
     */
    public final void setViewRoot(Path viewRoot) {
        setPropertyValue(PROPERTY_VIEW_ROOT, viewRoot);
    }

    /**
     * {@inheritDoc }
     */
    public final Path getViewRoot() {
        return (Path) getPropertyValue(PROPERTY_VIEW_ROOT);
    }

    /**
     * {@inheritDoc }
     */
    public final Property varViewRoot(){
        return getProperty(PROPERTY_VIEW_ROOT);
    }

    /**
     * {@inheritDoc }
     */
    public final void setSelectedPath(Path[] path) {
        setPropertyValue(PROPERTY_SELECTED_PATHS, path);
    }

    /**
     * {@inheritDoc }
     */
    public final Path[] getSelectedPath() {
        return (Path[]) getPropertyValue(PROPERTY_SELECTED_PATHS);
    }

    /**
     * {@inheritDoc }
     */
    public final Property varSelectedPath(){
        return getProperty(PROPERTY_SELECTED_PATHS);
    }

    public final void setFilter(Predicate filter) {
        setPropertyValue(PROPERTY_FILTER, filter);
    }

    public final Predicate getFilter() {
        return (Predicate) getPropertyValue(PROPERTY_FILTER);
    }

    public final Property varFilter(){
        return getProperty(PROPERTY_FILTER);
    }

    public void setPredicates(Sequence predicates){
        final Predicate selected = getFilter();
        this.predicates.replaceAll(predicates);
        setFilter(selected);
        if (predicates.isEmpty() || (predicates.getSize()==1 && predicates.get(0) == Predicate.TRUE)){
            predSelect.setVisible(false);
        } else {
            predSelect.setVisible(true);
        }
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_FILTER.equals(name)) {
            predSelect.setSelection(value);
        } else if (PROPERTY_SELECTED_PATHS.equals(name)) {
            final Path[] path = (Path[]) value;
            if (path!=null && path.length>0){
                pathText.setText(new Chars(path[0].toURI()));
            }
        } else if (PROPERTY_VIEW_ROOT.equals(name)){
            Path viewRoot = (Path) value;
            history.put(viewRoot);
            if (viewRoot==null){
                pathText.setText(Chars.EMPTY);
            } else {
                pathText.setText(new Chars(viewRoot.toURI()));
            }
        }
    }

    public Path showOpenDialog(UIFrame parent){
        if (parent!=null){
            return showOpenDialog(parent.getManager(), parent);
        } else {
            return showOpenDialog(FrameManagers.getFrameManager());
        }
    }

    public Path showOpenDialog(FrameManager framemanager){
        return showOpenDialog(framemanager!=null?framemanager:FrameManagers.getFrameManager(), (UIFrame) null);
    }

    private Path showOpenDialog(FrameManager frameManager, UIFrame parent){
        addChild(bottomPane,BorderConstraint.BOTTOM);

        final UIFrame dialog;
        if (parent!=null){
            dialog = (UIFrame) parent.getManager().createFrame(parent, true, false);
        } else if (frameManager!=null){
            dialog = (UIFrame) frameManager.createFrame(parent, true, false);
        } else {
            dialog = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        }
        final WContainer contentPane = dialog.getContainer();
        contentPane.setLayout(new BorderLayout());
        contentPane.addChild(this, BorderConstraint.CENTER);
        this.dialog = dialog;

        dialog.setSize(640, 480);
        dialog.setTitle(new Chars("Path chooser"));
        dialog.setVisible(true);
        dialog.waitForDisposal();

        return Paths.resolve(pathText.getText().toChars());
    }

    public Path showSaveDialog(UIFrame parent){
        if (parent!=null){
            return showSaveDialog(parent.getManager(), parent);
        } else {
            return showSaveDialog(FrameManagers.getFrameManager());
        }
    }

    public Path showSaveDialog(FrameManager framemanager){
        return showOpenDialog(framemanager!=null?framemanager:FrameManagers.getFrameManager(), (UIFrame) null);
    }

    private Path showSaveDialog(FrameManager frameManager, UIFrame parent){
        addChild(bottomPane,BorderConstraint.BOTTOM);

        final UIFrame dialog;
        if (parent!=null){
            dialog = (UIFrame) parent.getManager().createFrame(parent, true, false);
        } else if (frameManager!=null){
            dialog = (UIFrame) frameManager.createFrame(parent, true, false);
        } else {
            dialog = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        }
        final WContainer contentPane = dialog.getContainer();
        contentPane.setLayout(new BorderLayout());
        contentPane.addChild(this, BorderConstraint.CENTER);
        this.dialog = dialog;

        dialog.setSize(640, 480);
        dialog.setTitle(new Chars("Path chooser"));
        dialog.setVisible(true);
        dialog.waitForDisposal();

        return Paths.resolve(pathText.getText().toChars());
    }

    public static Path showOpenDialog(Predicate predicate){
        return showOpenDialog(FrameManagers.getFrameManager(),predicate);
    }

    public static Path showOpenDialog(FrameManager frameManager, Predicate predicate){
        final WPathChooser chooser = new WPathChooser();
        if (predicate!=null){
            chooser.setFilter(predicate);
        }
        return chooser.showOpenDialog(frameManager);
    }

    public static Path showOpenDialog(UIFrame parent, Predicate predicate){
        final WPathChooser chooser = new WPathChooser();
        if (predicate!=null){
            chooser.setFilter(predicate);
        }
        return chooser.showOpenDialog(parent);
    }

    public static Path showSaveDialog(Predicate predicate){
        return showSaveDialog(FrameManagers.getFrameManager(),predicate);
    }

    public static Path showSaveDialog(FrameManager frameManager, Predicate predicate){
        final WPathChooser chooser = new WPathChooser();
        if (predicate!=null){
            chooser.setFilter(predicate);
        }
        return chooser.showSaveDialog(frameManager);
    }

    public static Path showSaveDialog(UIFrame parent, Predicate predicate){
        final WPathChooser chooser = new WPathChooser();
        if (predicate!=null){
            chooser.setFilter(predicate);
        }
        return chooser.showSaveDialog(parent);
    }


}
