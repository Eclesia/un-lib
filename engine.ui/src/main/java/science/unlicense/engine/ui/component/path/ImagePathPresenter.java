
package science.unlicense.engine.ui.component.path;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.impl.process.geometric.RescaleOperator;

/**
 *
 * @author Johann Sorel
 */
public class ImagePathPresenter extends AbstractPathPresenter {

    public static final ImagePathPresenter INSTANCE = new ImagePathPresenter();

    private Image mimeImage = null;

    @Override
    public float getPriority() {
        return 1;
    }

    @Override
    public boolean canHandle(Path path) {
        try {
            Store store = Images.open(path);
            store.dispose();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    @Override
    public Image createMimeImage(Extent.Long size) {
        return null;
//        if (mimeImage != null && mimeImage.getExtent().get(0)== size.get(0)){
//            return mimeImage;
//        }
//        //create a small mime image
//        final ImagePainter2D painter = Painters.getPainterManager().createPainter((int) size.get(0), (int) size.get(1));
//        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
//        painter.setPaint(new ColorPaint(Color.BLACK));
//        painter.stroke(new RoundedRectangle(2, 2, size.get(0)-4, size.get(1)-4));
//        painter.flush();
//        mimeImage = painter.getImage();
//        painter.dispose();
//        return mimeImage;
    }

    @Override
    public Image createImage(Path path, Extent.Long size) {
        try {
            //TODO image is not centered
            Image image = Images.read(path);
            final double scalex = size.get(0) / image.getExtent().get(0);
            final double scaley = size.get(1) / image.getExtent().get(1);
            final Extent ext;
            if (scalex<scaley){
                ext = new Extent.Double(size.get(0), image.getExtent().get(1)*scalex);
            } else {
                ext = new Extent.Double(image.getExtent().get(0)*scaley, size.get(1));
            }
            image = RescaleOperator.execute(image, ext);

            return image;
        } catch (Exception ex) {
            System.out.println("No image for path : "+path+" "+ex.getMessage());
        }
        return null;
    }

    @Override
    public Widget createInteractive(Path path) {
        final WImageViewer viewer = new WImageViewer();
        try {
            Image image = Images.read(path);
            viewer.setImage(image);
        } catch (Exception ex) {
            System.out.println("No image for path : "+path+" "+ex.getMessage());
        }
        return viewer;
    }

}
