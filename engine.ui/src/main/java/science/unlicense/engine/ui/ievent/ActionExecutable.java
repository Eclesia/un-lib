
package science.unlicense.engine.ui.ievent;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.functional.OutFunction;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;

/**
 * Executable associated to ActionEvents.
 *
 * @author Johann Sorel
 */
public interface ActionExecutable extends OutFunction, EventSource{

    Image getImage(Extent extent);

    Chars getText();

    boolean isActive();

}
