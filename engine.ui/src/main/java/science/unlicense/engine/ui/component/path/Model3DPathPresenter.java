

package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.software.SoftwarePainter3D;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.impl.process.geometric.FlipVerticalOperator;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.api.Model3Ds;
import science.unlicense.model3d.api.SceneResource;

/**
 *
 * @author Johann Sorel
 */
public class Model3DPathPresenter extends AbstractPathPresenter{

    public static final Model3DPathPresenter INSTANCE = new Model3DPathPresenter();

    public float getPriority() {
        return 2;
    }

    public boolean canHandle(Path path) {
        try {
            return Model3Ds.canDecode(path);
        } catch (IOException ex) {
            return false;
        }
    }

    @Override
    public Image createImage(Path path, Extent.Long size) {

        try {
            final Store store = Model3Ds.open(path);
            final Stage stage = new Stage(store);
            return stage.createImage(size);
        }catch(IOException ex){
            ex.printStackTrace();
        }catch(StoreException ex){
            ex.printStackTrace();
        }

        return null;
    }

    public Widget createInteractive(Path path) {
        return new WModel3DPreview(path);
    }



    static class Stage {

        public Store store;
        public SceneNode scene;
        public SceneNode target;
        public MonoCamera camera;
        public OrbitUpdater controller;

        public Stage(Store store) throws StoreException {
            this.store = store;

            scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
            final GraphicNode base = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
            scene.getChildren().add(base);

            //calculate bbox of objet
            final SceneResource res = (SceneResource) store;
            final Collection col = res.getElements();
            final Iterator ite = col.createIterator();
            while (ite.hasNext()){
                final Object obj = ite.next();
                if (obj instanceof SceneNode){
                    base.getChildren().add(obj);
                }
            }

            BBox bbox = base.getBBox();
            if (bbox == null) bbox = new BBox(3);

            //create a target node at the center of the bbox
            target = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
            target.getNodeTransform().getTranslation().set(bbox.getMiddle());
            target.getNodeTransform().notifyChanged();
            scene.getChildren().add(target);

            //some lightning
            scene.getChildren().add(new AmbientLight(Color.WHITE,Color.WHITE));

            final PointLight point = new PointLight();
            point.getNodeTransform().getTranslation().set(bbox.getUpper());
            point.getNodeTransform().getTranslation().localScale(1.2);
            point.getNodeTransform().notifyChanged();
            scene.getChildren().add(point);


            //build the scene
            final GraphicNode camNode = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
            camera = new MonoCamera();
            target.getChildren().add(camera);

            //calculate camera position from object bbox
            controller = new OrbitUpdater((EventSource) null, new Vector3f64(0, 1, 0), new Vector3f64(1, 0, 0), camera, target);
            camNode.getUpdaters().add(controller);

            //set camera at good distance
            final double fov = camera.getFieldOfView();
            final double spanX = bbox.getSpan(0);
            final double distX = spanX / Math.tan(fov);
            final double spanY = bbox.getSpan(1);
            final double distY = spanY / Math.tan(fov);
            // x2 because screen space is [-1...+1]
            // x1.2 to compensate perspective effect
            final float dist = (float) (Maths.max(distX,distY) * 2.0 * 1.2);

            controller.setDistance(dist);
            controller.setVerticalAngle(Angles.degreeToRadian(15));
            controller.setHorizontalAngle(Angles.degreeToRadian(15));
            controller.update(null);

        }

        Image createImage(Extent.Long size) {
            controller.update(null);
            SoftwarePainter3D painter = new SoftwarePainter3D(size);
            painter.setCamera(camera);
            painter.setScene(scene);
            Image image = painter.render(false);
            return new FlipVerticalOperator().execute(image);
        }


    }

}
