
package science.unlicense.engine.ui.io;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Number;
import science.unlicense.common.api.predicate.Expression;
import static science.unlicense.engine.ui.io.RSUnits.*;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.math.api.unitold.Unit;

/**
 * A measure is a numeric value associated with a unit.
 *
 * @author Johann Sorel
 */
public class UnitNumber implements Expression {

    private static final Chars FONT_SIZE = Chars.constant("font-size");

    private final Unit unit;
    private final Number value;

    public UnitNumber(Number value, Unit unit) {
        this.value = value;
        this.unit = unit;
    }

    public Unit getUnit() {
        return unit;
    }

    public Number getValue() {
        return value;
    }

    @Override
    public Object evaluate(Object candidate) {

        Widget w = (Widget) candidate;

        if (unit == EM) {
            Object size = w.getEffectiveStyleDoc().getProperty(FONT_SIZE).getValue();
            if (size instanceof Expression) {
                size = ((Expression) size).evaluate(candidate);
            }
            if (size instanceof Number) {
                return value.toDouble()*((Number) size).toDouble();
            } else if (size instanceof java.lang.Number){
                return value.toDouble()*((java.lang.Number) size).doubleValue();
            } else {
                throw new IllegalStateException("missing font-size field");
            }
        }

        double dpm = w.getFrame().getScreen().getDotPerMillimeter();
        double inch = 25.4 * dpm;
        if (unit == CM) return value.toDouble()*dpm*10.0;
        if (unit == MM) return value.toDouble()*dpm;
        if (unit == PX) return value.toDouble()*(inch/96.0); // 1/96 of an inch
        if (unit == PT) return value.toDouble()*(inch/72.0); // 1/72 of an inch
        if (unit == PC) return value.toDouble()*(inch/72.0)*12; // 12pt
        return value.toDouble();
    }

}
