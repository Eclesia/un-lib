package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.visual.GraphicGeometryView;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 * Widget displaying a geometry.
 *
 * @author Johann Sorel
 */
public class WGraphicGeometry extends WLeaf{

    /**
     * Property for the widget geometry.
     */
    public static final Chars PROPERTY_GEOMETRY = Chars.constant("geometry");

    // style classes
    public static final Chars STYLE_ENABLE = Chars.constant("wgeometry-enable");
    public static final Chars STYLE_DISABLE = Chars.constant("wgeometry-disable");

    /** Geometry border and fill : BorderStyle class */
    public static final Chars STYLE_GEOM_PREFIX = Chars.constant("geom");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLeaf.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("graphicgeometry")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WGraphicGeometry() {
        setView(new GraphicGeometryView(this));
    }

    /**
     * Get geometry.
     * @return Geometry2D, can be null
     */
    public final PlanarGeometry getGeometry() {
        return (PlanarGeometry) getPropertyValue(PROPERTY_GEOMETRY);
    }

    /**
     * Set geometry.
     * @param geom can be null
     */
    public final void setGeometry(PlanarGeometry geom) {
        setPropertyValue(PROPERTY_GEOMETRY,geom);
    }

    /**
     * Get geometry variable.
     * @return Variable.
     */
    public final Property varGeometry(){
        return getProperty(PROPERTY_GEOMETRY);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }
}
