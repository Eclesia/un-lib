
package science.unlicense.engine.ui.component.bean;

import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.widget.WValueWidget;

/**
 *
 * @author Johann Sorel
 */
public interface PropertyEditor {

    /**
     * Create an editor for given property
     *
     * The returned editor must not be synchronized with given property.
     *
     * @param property
     * @return editor or null if property is not supported
     */
    WValueWidget create(Property property);

}
