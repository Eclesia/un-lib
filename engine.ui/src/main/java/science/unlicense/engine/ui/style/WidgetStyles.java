
package science.unlicense.engine.ui.style;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.Field;
import science.unlicense.common.api.predicate.Constant;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.common.api.predicate.Function;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.display.api.painter2d.Brush;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.io.EqualsPredicate;
import science.unlicense.engine.ui.io.InPredicate;
import science.unlicense.engine.ui.io.ModExpression;
import science.unlicense.engine.ui.io.PropertyName;
import science.unlicense.engine.ui.io.TupleBuffer1DExpression;
import science.unlicense.engine.ui.io.UWKTReader;
import science.unlicense.geometry.api.Curve;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.impl.CurvePolygon;
import science.unlicense.geometry.impl.Path;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import science.unlicense.image.impl.process.ConvolutionMatrices;
import science.unlicense.image.impl.process.ConvolveOperator;
import science.unlicense.math.api.Maths;
import science.unlicense.task.api.Task;
import science.unlicense.task.api.TaskDescriptor;
import science.unlicense.task.api.Tasks;

/**
 * convenient methods to manipulate widget styles.
 *
 * @author Johann Sorel
 */
public final class WidgetStyles {

    public static final Chars NONE = Chars.constant("none");
    public static final Constant NULL_CST = new Constant(null);

    private WidgetStyles() {
    }

    public static Margin readMargin(Document doc, Chars propName, Object candidate) {
        final Object marginDef = getOrEvaluate(doc, propName, candidate);

        final Margin margin;
        if (marginDef instanceof Number) {
            //same margin in all directions
            final double m = ((Number) marginDef).doubleValue();
            margin = new Margin(m, m, m, m);
        } else if (marginDef instanceof Sequence) {
            final Sequence seq = (Sequence) marginDef;
            if (seq.getSize() == 1) {
                final double m = ((Number) ((Constant) seq.get(0)).getValue()).doubleValue();
                margin = new Margin(m, m, m, m);
            } else {
                //size for each side
                margin = new Margin(
                        ((Number) ((Constant) seq.get(0)).getValue()).doubleValue(),
                        ((Number) ((Constant) seq.get(1)).getValue()).doubleValue(),
                        ((Number) ((Constant) seq.get(2)).getValue()).doubleValue(),
                        ((Number) ((Constant) seq.get(3)).getValue()).doubleValue()
                );
            }
        } else {
            //default or unvalid, set to zero
            margin = new Margin(0);
        }

        return margin;
    }

    public static Object getOrEvaluate(Document doc, Chars propName, Object candidate) {
        Expression exp = (Expression) doc.getPropertyValue(propName);
        if (exp == null) return null;
        return exp.evaluate(candidate);
    }

    private static ShapeTransform readTransform(Document doc, Chars propName, Object candidate) {
        return (ShapeTransform) getOrEvaluate(doc, propName, candidate);
    }

    /**
     * Rebuild shape styles from style.
     *
     * @param propName the property name containing the definition
     * @return ShapeStyle array
     */
    public static GraphicStyle[] readShapeStyle(Document doc, Chars propName, Object candidate) {
        final Object[] values = WidgetStyles.getFieldValues(doc.getProperty(propName));
        final GraphicStyle[] graphics = new GraphicStyle[values.length];
        for (int i = 0;i < values.length; i++) {
            if (values[i] instanceof Document) {
                graphics[i] = readShapeStyle((Document) values[i], candidate);
            }
        }

        //sort graphics by z order
        Arrays.sort(graphics);

        return graphics;
    }

    /**
     * Rebuild font style from style.
     *
     * @param style
     * @param propName the property name containing the definition
     * @return FontStyle
     */
    public static FontStyle readFontStyle(final Document doc, Chars propName, Object candidate) {
        final Object value = doc.getProperty(propName).getValue();
        if (value instanceof Document) {
            return readFontStyle((Document) value, candidate);
        }
        return null;
    }

    public static GraphicStyle readShapeStyle(final Document doc, Object candidate) {
        final GraphicStyle container = new GraphicStyle();
        readElementStyle(doc, container, candidate);
        return container;
    }

    public static FontStyle readFontStyle(final Document doc, Object candidate) {
        final FontStyle container = new FontStyle();
        readElementStyle(doc, container, candidate);
        return container;
    }

    public static Task[] readEffects(final Document doc, Chars propName, Object candidate) {
        final Object[] values = WidgetStyles.getFieldValues(doc.getProperty(propName));
        final Task[] effects = new Task[values.length];
        for (int i = 0;i < values.length; i++) {
            //TODO replace this by something configurable
            final TaskDescriptor desc = Tasks.getDescriptor(new Chars("convolve"));
            final Task op = desc.create();
            op.inputs().setPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),null);
            op.inputs().setPropertyValue(ConvolveOperator.INPUT_CONV_MATRIX.getId(),ConvolutionMatrices.SOBEL_HORIZONTAL);
            effects[i] = op;
        }
        return effects;
    }

    public static void readElementStyle(final Document doc, GeomStyle container, Object candidate) {
        if (doc == null) return;

        Brush brush = (Brush) getOrEvaluate(doc, StyleDocument.STYLE_PROP_BRUSH, candidate);
        final Paint brushPaint = asPaint(doc, StyleDocument.STYLE_PROP_BRUSH_PAINT, candidate);
        final Paint fillPaint = asPaint(doc, StyleDocument.STYLE_PROP_FILL_PAINT, candidate);
        container.setBrush(brush);
        container.setBrushPaint(brushPaint);
        container.setFillPaint(fillPaint);

        if (container instanceof GraphicStyle) {
            Object radius = getOrEvaluate(doc, StyleDocument.STYLE_PROP_RADIUS, candidate);
            Object shape = getOrEvaluate(doc, StyleDocument.STYLE_PROP_SHAPE, candidate);
            Object zorder = getOrEvaluate(doc, StyleDocument.STYLE_PROP_ZORDER, candidate);

            final Margin margin = readMargin(doc, StyleDocument.STYLE_PROP_MARGIN, candidate);
            final GraphicStyle graphic = (GraphicStyle) container;
            graphic.setMargin(margin);
            graphic.setTransform(readTransform(doc, StyleDocument.STYLE_PROP_TRANSFORM, candidate));

            if (zorder instanceof Number) {
                graphic.setZorder(((Number) zorder).floatValue());
            }

            if (shape != null){
                final PlanarGeometry geom;
                if (shape instanceof PlanarGeometry) {
                    geom = (PlanarGeometry) shape;
                } else {
                    try {
                        geom = (PlanarGeometry) new UWKTReader().read((Chars) shape);
                    } catch (IOException ex) {
                        throw new InvalidArgumentException("Unvalide shape : "+shape);
                    }
                }
                graphic.setCorners(null);
                graphic.setGeometry(geom);
            } else if (radius instanceof Number) {
                final double r = ((Number) radius).doubleValue();
                graphic.setCorners(new double[]{r,r,r,r});
            } else if (radius instanceof Sequence) {
                final Sequence seq = (Sequence) radius;
                double[] corners = new double[4];
                for (int i = 0; i < seq.getSize(); i++) {
                    corners[i] = ((Number) ((Constant) seq.get(i)).getValue()).doubleValue();
                }
                graphic.setCorners(corners);
            } else {
                graphic.setCorners(new double[]{0,0,0,0});
            }

        } else if (container instanceof FontStyle) {
            final FontChoice font = (FontChoice) getOrEvaluate(doc, StyleDocument.STYLE_PROP_FAMILY, candidate);
            final FontStyle fs = (FontStyle) container;
            fs.setFont(font);
        }

    }

    public static Paint asPaint(final Document doc, Chars propName, Object candidate) {
        Object value = getOrEvaluate(doc, propName, candidate);
        if (value instanceof Paint) {
            return (Paint) value;
        } else if (value instanceof Color) {
            return new ColorPaint((Color) value);
        } else {
            return null;
        }
    }

    public static Document mergeDoc(Document base, Document diff, boolean createNew) {
        return mergeDoc(base, diff, createNew, false);
    }

    /**
     *
     * @param base
     * @param diff
     * @param createNew
     * @param skipRules
     * @return
     */
    public static Document mergeDoc(Document base, Document diff, boolean createNew, boolean skipRules) {
        //we don't use this since it will completly replace Document type values
        //we want to merge them
        //base.overrideProperties(over);

        if (base == null) {
            if (createNew){
                return (Document) smartCopy(diff);
            } else {
                throw new InvalidArgumentException("Can not have base document null and createNew at false");
            }
        } else if (diff == null) {
            if (createNew) {
                return (Document) smartCopy(base);
            } else {
                return base;
            }
        }

        //merge docs
        final Document output;
        if (createNew) {
            output = (Document) smartCopy(base);
        } else {
            output = base;
        }

        final Iterator ite = diff.getPropertyNames().createIterator();
        while (ite.hasNext()) {
            final Chars name = (Chars) ite.next();
            final Field baseProp = base.getProperty(name);
            final Field diffProp = diff.getProperty(name);
            final Field outProp = output.getProperty(name);
            final Object[] baseValues = getFieldValues(baseProp);
            final Object[] diffValues = getFieldValues(diffProp);

            //remove any rule docs
            if (skipRules && diffValues != null) {
                for (int i = 0;i < diffValues.length; i++) {
                    if (diffValues[i] instanceof Document && isRuleOrTrigger((Document) diffValues[i])) {
                        diffValues[i] = null;
                    }
                }
            }


            if (diffValues.length == 0) {
                //nothing to replace
            } else if (baseValues.length == 0) {
                //replace values
                setFieldValues(outProp, diffValues);
            } else {
                //smart merge
                final int size = Maths.max(baseValues.length,diffValues.length);
                final Object[] res = new Object[size];
                for (int i=0;i<size;i++) {
                    if (baseValues.length <= i) {
                        //no more base values
                        res[i] = smartCopy(diffValues[i]);
                    } else if (diffValues.length <= i) {
                        //no more diff values
                        res[i] = smartCopy(baseValues[i]);
                    } else {
                        ///merge
                        Object bv = baseValues[i];
                        Object dv = diffValues[i];
                        if (isNone(dv)) {
                            //force null value
                            res[i] = NULL_CST;
                        } else if (isNone(bv)) {
                            res[i] = smartCopy(dv);
                        } else if (bv instanceof Document || dv instanceof Document) {
                            //merge doc
                            if (skipRules && isRuleOrTrigger((Document) dv)) {
                                res[i] = smartCopy(bv);
                            } else {
                                res[i] = mergeDoc((Document) bv, (Document) dv, true, skipRules);
                            }
                        } else {
                            //replace value
                            res[i] = smartCopy(dv);
                        }
                    }
                }
                setFieldValues(outProp, res);
            }
        }

        return output;
    }

    public static void evaluate(StyleDocument doc, Document dico, Object candidate) {
        WidgetStyles.mergeDoc(dico, doc, false, true);
        loop(dico, doc.getRules().createIterator(), candidate);
    }

    private static void loop(Document dico, Iterator ite, Object candidate){
        while (ite.hasNext()) {
            final StyleDocument rule = (StyleDocument) ite.next();
            mergeStyleDocs(dico, rule, candidate);
        }
    }

    private static void mergeStyleDocs(Document base, StyleDocument diff, Object candidate) {
        if (Boolean.TRUE.equals(diff.getFilter().evaluate(candidate))) {
            WidgetStyles.mergeDoc(base, diff, false, true);
            Iterator ite = diff.getRules().createIterator();
            while (ite.hasNext()) {
                StyleDocument subRule = (StyleDocument) ite.next();
                mergeStyleDocs(base, subRule, candidate);
            }
        }
    }

    private static Object smartCopy(Object obj) {
        if (obj instanceof Document) {
            final Document copy;
            if (obj instanceof StyleDocument) {
                copy = new StyleDocument();
                ((StyleDocument) copy).setName(((StyleDocument) obj).getName());
            } else {
                copy = new DefaultDocument();
            }

            final Document doc = (Document) obj;
            final Iterator ite = doc.getPropertyNames().createIterator();
            while (ite.hasNext()) {
                final Chars name = (Chars) ite.next();
                final Object[] values = WidgetStyles.getFieldValues(doc.getProperty(name));
                final Object[] cp = new Object[values.length];
                for (int i = 0; i < cp.length; i++) {
                    cp[i] = smartCopy(values[i]);
                }
                setFieldValues(copy.getProperty(name), cp);
            }
            return copy;
        } else if (isNone(obj)) {
            return null;
        } else {
            return obj;
        }
    }

    /**
     * Test if an expression is a 'none'.
     * None expressions are used to erase field values.
     *
     * @param candidate
     * @return true if given object is a non expression
     */
    public static boolean isNone(Object candidate) {
        return NONE.equals(candidate) ||
         (candidate instanceof Constant && ((Constant) candidate).getValue() == null);
    }

    private static boolean isRuleOrTrigger(Document doc) {
        if (doc instanceof StyleDocument) {
            return ((StyleDocument) doc).isRule() || ((StyleDocument) doc).isTrigger();
        }
        return false;
    }

    public static void evaluate(PlanarGeometry geom, Object widget) {
        if (geom instanceof Polygon) {
            final Polygon p = (Polygon) geom;
            evaluate(p.getExterior(),widget);
        } else if (geom instanceof Polyline) {
            final Polyline p = (Polyline) geom;
            evaluate(p,widget);
        } else if (geom instanceof CurvePolygon) {
            final CurvePolygon p = (CurvePolygon) geom;
            evaluate(p.getExterior(),widget);
        } else if (geom instanceof TransformedGeometry) {
            final TransformedGeometry p = (TransformedGeometry) geom;
            //TODO
        } else if (geom instanceof Curve) {
            //TODO
        } else if (geom instanceof Path) {
            final Path p = (Path) geom;
            //TODO
        } else if (geom instanceof Rectangle) {
            final Rectangle p = (Rectangle) geom;
            //TODO
        } else {
            throw new UnimplementedException("Geometry type not supported yet : "+geom.getClass());
        }
    }

    private static void evaluate(Polyline geom, Object widget) {
        final TupleGrid1D coords = geom.getCoordinates();
        if (coords instanceof TupleBuffer1DExpression) {
            ((TupleBuffer1DExpression) coords).evaluate(widget);
        }
    }

    public static Object getFieldValue(Field field) {
        Object value = field.getValue();
        if (value == null) {
            return null;
        } else if (value.getClass().isArray()) {
            return Arrays.getValue(value,0);
        } else {
            return value;
        }
    }

    public static Object[] getFieldValues(Field field) {
        Object value = field.getValue();
        if (value == null) {
            return Arrays.ARRAY_OBJECT_EMPTY;
        } else if (value.getClass().isArray()) {
            return (Object[]) value;
        } else {
            return new Object[]{value};
        }
    }

    public static void setFieldValues(Field field, Object value) {
        if (value == null) {
            field.setValue(null);
        } else if (value instanceof Object[] && ((Object[]) value).length == 1) {
            field.setValue(((Object[]) value)[0]);
        } else if (value.getClass().isArray() && Arrays.getSize(value) == 1) {
            field.setValue(Arrays.getValue(value,0));
        } else {
            field.setValue(value);
        }
    }

    /**
     * List the properties used in the document.
     *
     * @param doc style document to evaluate
     * @param candidate widget against which to evaluate
     * @param setValueProps properties used by the style
     * @param setFilterProps properties which may change the style definition
     */
    public static void listProperties(StyleDocument doc, Object candidate, Set setValueProps, Set setFilterProps){

        final Expression pe = doc.getFilter();
        if (pe != null) {
            extractPropertyNames(pe, setFilterProps);
        }

        if (pe == null || Boolean.TRUE.equals(pe.evaluate(candidate))){
            //loop on all properties
            final Iterator propIte = doc.getPropertyNames().createIterator();
            while (propIte.hasNext()){
                final Chars name = (Chars) propIte.next();
                if (StyleDocument.PROP_FILTER.equals(name)) {
                    continue;
                }
                Object value = doc.getPropertyValue(name);
                listProperties(value, candidate, setValueProps, setFilterProps);
            }
        }
    }

    public static void listProperties(Object value, Object candidate, Set setValueProps, Set setFilterProps) {
        if (value instanceof Expression) {
            extractPropertyNames((Expression) value, setValueProps);
        } else if (value instanceof StyleDocument) {
            final StyleDocument subdoc = (StyleDocument) value;
            listProperties(subdoc, candidate, setValueProps, setFilterProps);

        } else if (value instanceof Collection) {
            final Collection col = (Collection) value;
            final Iterator ite = col.createIterator();
            while (ite.hasNext()) {
                value = ite.next();
                listProperties(value, candidate, setValueProps, setFilterProps);
            }
        }
    }

    private static void extractPropertyNames(Expression exp, Set names){
        if (exp instanceof Predicate) {
            if (exp instanceof EqualsPredicate) {
                final EqualsPredicate p = (EqualsPredicate) exp;
                extractPropertyNames(p.getExp1(), names);
                extractPropertyNames(p.getExp2(), names);
            } else if (exp instanceof InPredicate){
                final InPredicate p = (InPredicate) exp;
                extractPropertyNames(p.getExp1(), names);
                extractPropertyNames(p.getExp2(), names);
            }
        } else if (exp instanceof PropertyName) {
            names.add(((PropertyName) exp).getName());
        } else if (exp instanceof ModExpression) {
            final ModExpression p = (ModExpression) exp;
            extractPropertyNames(p.getExp1(), names);
            extractPropertyNames(p.getExp2(), names);
        } else if (exp instanceof Function) {
            final Function f = (Function) exp;
            final Expression[] exps = f.getParameters();
            for (int i=0;i<exps.length;i++) {
                extractPropertyNames(exps[i], names);
            }
        }
    }

}
