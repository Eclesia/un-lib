
package science.unlicense.engine.ui.component.bean;

import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.WValueWidget;

/**
 *
 * @author Johann Sorel
 */
public class BooleanEditor implements PropertyEditor {

    public static final BooleanEditor INSTANCE = new BooleanEditor();

    private BooleanEditor(){}

    public WValueWidget create(Property property) {
        if (Boolean.class.isAssignableFrom(property.getType().getValueClass())){
            return new WCheckBox();
        }
        return null;
    }

}
