
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;

/**
 * Edition widget such as check boxes, text fields, spinners work and edit a main
 * value object.
 * This is true for primitive types but also for more complex editors like
 * path chooser or color chooser.
 *
 * It is encouraged to extend this class when a widget follow this model.
 * It is also encouraged to support null values.
 *
 * @author Johann Sorel
 */
public interface WValueWidget extends Widget{

    /**
     * Get widget value property name.
     *
     * The property can then be accesed using getProperty, getPropertyValue and setPropertyValue.
     *
     * @return value, can be null.
     */
    Chars getValuePropertyName();

}
