
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.api.anim.DefaultTimer;
import science.unlicense.display.api.anim.Timer;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.painter3d.Painter3D;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.software.SoftwarePainter3D;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;

/**
 * On CPU rendered scene.
 *
 * @author Johann Sorel
 */
public class WScene3D extends WContainer {

    public static final Chars PROPERTY_SCENE = Chars.constant("Scene");
    public static final Chars PROPERTY_CAMERA = Chars.constant("Camera");
    public static final Chars PROPERTY_LIGHTENABLE = Chars.constant("LightEnable");

    private final WGraphicImage wimg = new WGraphicImage();
    private final WLabel werrors = new WLabel();
    private final Timer timer;
    private Painter3D painter;
    private boolean needUpdate = true;

    public WScene3D() {
        this(null,null);
    }

    public WScene3D(SceneNode scene, MonoCamera camera) {
        setLayout(new BorderLayout());
        wimg.setLayoutConstraint(BorderConstraint.CENTER);
        werrors.setLayoutConstraint(BorderConstraint.CENTER);
        getChildren().add(wimg);

        timer = new DefaultTimer(20);
        timer.addEventListener(null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                update();
            }
        });

        setScene(scene);
        setCamera(camera);
    }

    public Painter3D getPainter() {
        return painter;
    }

    public void setPainter(Painter3D painter) {
        this.painter = painter;
    }

    public final void setScene(SceneNode scene) {
        setPropertyValue(PROPERTY_SCENE, scene);
    }

    public final SceneNode getScene() {
        return (SceneNode) getPropertyValue(PROPERTY_SCENE);
    }

    public final Property varScene() {
        return getProperty(PROPERTY_SCENE);
    }

    public final void setCamera(MonoCamera camera) {
        setPropertyValue(PROPERTY_CAMERA, camera);
    }

    public final MonoCamera getCamera() {
        return (MonoCamera) getPropertyValue(PROPERTY_CAMERA);
    }

    public final Property varCamera() {
        return getProperty(PROPERTY_CAMERA);
    }

    public final boolean isLightEnable() {
        return Boolean.TRUE.equals(getPropertyValue(PROPERTY_LIGHTENABLE));
    }

    public final void setLightEnable(boolean lightEnable) {
        setPropertyValue(PROPERTY_LIGHTENABLE, lightEnable);
    }

    public final Property varLightEnable() {
        return getProperty(PROPERTY_LIGHTENABLE);
    }

    private void update() {
        final SceneNode scene = getScene();
        final MonoCamera camera = getCamera();
        if (scene == null || camera == null) {
            wimg.setImage(null);
            return;
        }
        final Extent size = wimg.getEffectiveExtent();
        int w = (int) size.get(0);
        int h = (int) size.get(1);
        if (w<=0) w = 1;
        if (h<=0) h = 1;
        final Extent.Long extent = new Extent.Long(w,h);

        if (painter == null || !painter.getCanvasSize().equals(extent)) {
            if (painter == null) {
                painter = new SoftwarePainter3D(extent);
            } else {
                painter.setCanvasSize(extent);
            }
            needUpdate = true;
        }
        //painter.setEnableLight(isLightEnable());

        needUpdate |= preUpdate(timer.getUpdateTime());

        if (needUpdate) {
            //avoid glitchs copy the image
            painter.setCamera(camera);
            painter.setScene(scene);

            if (painter instanceof SoftwarePainter3D) {
                ((SoftwarePainter3D) painter).setGlobalLightning(isLightEnable());
            }

            try {
                Image image = painter.render();
                wimg.setImage(image);
                getChildren().replaceAll(new Node[]{wimg});
            } catch (Exception ex) {
                String message = ex.getMessage();
                werrors.setText(CObjects.toChars(message));
                getChildren().replaceAll(new Node[]{werrors});
                ex.printStackTrace();
            }
            needUpdate = false;

        }
    }

    /**
     * Called before rendering.
     * @param time time in millisecond
     * @return true to continue rendering, false to abort, nothing changed
     */
    protected boolean preUpdate(long time) {
        return false;
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_SCENE.equals(name)) {
            needUpdate = true;
        } else if (PROPERTY_CAMERA.equals(name)) {
            needUpdate = true;
        } else if (PROPERTY_LIGHTENABLE.equals(name)) {
            needUpdate = true;
        }

    }

}
