
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.engine.ui.widget.WPasswordField;
import science.unlicense.engine.ui.widget.WTextField;

/**
 *
 * @author Johann Sorel
 */
public class PasswordFieldView extends TextFieldView {

    public PasswordFieldView(final WTextField widget) {
        super(widget);
    }

    @Override
    public WPasswordField getWidget() {
        return (WPasswordField) super.getWidget();
    }

    @Override
    protected CharArray getDisplayedText() {
        final CharArray ca = getWidget().getEditedText();
        if (ca == null) return null;

        final CharBuffer cb = new CharBuffer();
        for (int i = 0, n = ca.getCharLength(); i < n; i++) {
            cb.append('·');
        }

        return cb.toChars();

    }
}
