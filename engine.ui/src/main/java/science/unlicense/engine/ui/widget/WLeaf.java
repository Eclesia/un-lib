
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Tuple;

/**
 * a leaf widget, can not have any children.
 * @author Johann Sorel
 */
public abstract class WLeaf extends AbstractWidget{

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(AbstractWidget.CLASS_DEFAULTS);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WLeaf() {
        super(false);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public Tuple getOnSreenPosition(Widget child) {
        throw new RuntimeException("Leaf widget, does not have children.");
    }

    /**
     * Overide method to send a dirty event when value changes.
     *
     * @param extent
     */
    public void setEffectiveExtent(Extent extent) {
        if (!this.effExtent.equals(extent)){
            super.setEffectiveExtent(extent);
            setDirty();
        }
    }
}
