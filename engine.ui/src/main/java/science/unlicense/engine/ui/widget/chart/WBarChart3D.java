
package science.unlicense.engine.ui.widget.chart;

import science.unlicense.common.api.event.EventSource;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WScene3D;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.operation.TriangulateOp;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.geometry.impl.Mesh;

/**
 * Draft
 *
 * @author Johann Sorel
 */
public class WBarChart3D extends WContainer {

    private final WScene3D wscene;
    private final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
    private final MonoCamera camera = new MonoCamera();
    private final GraphicNode cameraTarget = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
    private final OrbitUpdater cameraController;

    public WBarChart3D() {
        super(new BorderLayout());
        this.wscene = new WScene3D() {
            @Override
            protected boolean preUpdate(long time) {
                cameraController.update(null);
                return true;
            }
        };

        cameraController = new OrbitUpdater((EventSource) this, new Vector3f64(0, 1, 0), new Vector3f64(1, 0, 0), camera, cameraTarget);
        cameraController.configureDefault();
        cameraTarget.getUpdaters().add(cameraController);
        cameraTarget.getChildren().add(camera);

        addChild(wscene, BorderConstraint.CENTER);
    }

    public void setBars(double[] values) {

        scene.getChildren().removeAll();

        final GraphicNode base = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        scene.getChildren().add(base);

        double x = 0;
        double step = 1.1;
        for (int i=0;i<values.length;i++, x+=step) {

            final BBox bbox = new BBox(new double[]{x+0,0,0}, new double[]{x+1,values[i],1});

            final TriangulateOp op = new TriangulateOp(bbox, 0);
            try {
                Mesh g = (Mesh) op.execute();

                Mesh.calculateNormals(g);

                final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
                material.setDiffuse(new ColorRGB(
                        (float) Math.random(),
                        (float) Math.random(),
                        (float) Math.random()));

                final DefaultModel mesh = new DefaultModel(CoordinateSystems.UNDEFINED_3D);
                mesh.setShape(g);
                mesh.getMaterials().add(material);

                base.getChildren().add(mesh);
            } catch(Exception ex ){
                ex.printStackTrace();
            }
        }

        //calculate bbox of objet
        BBox bbox = base.getBBox();
        if (bbox == null) bbox = new BBox(3);

        //create a target node at the center of the bbox
        cameraTarget.getNodeTransform().getTranslation().set(bbox.getMiddle());
        cameraTarget.getNodeTransform().notifyChanged();
        scene.getChildren().add(cameraTarget);

        //some lightning
        scene.getChildren().add(new AmbientLight(Color.GRAY_NORMAL,Color.GRAY_NORMAL));

        final PointLight point = new PointLight();
        point.getNodeTransform().getTranslation().set(bbox.getUpper());
        point.getNodeTransform().getTranslation().localScale(1.2);
        point.getNodeTransform().notifyChanged();
        scene.getChildren().add(point);

        wscene.setLightEnable(true);

        //calculate camera position from object bbox
        //set camera at good distance
        final double fov = camera.getFieldOfView();
        final double spanX = bbox.getSpan(0);
        final double distX = spanX / Math.tan(fov);
        final double spanY = bbox.getSpan(1);
        final double distY = spanY / Math.tan(fov);
        // x2 because screen space is [-1...+1]
        // x1.2 to compensate perspective effect
        final float dist = (float) (Maths.max(distX,distY) * 2.0 * 1.2);

        cameraController.setDistance(dist);
        cameraController.setVerticalAngle(Angles.degreeToRadian(15));
        cameraController.setHorizontalAngle(Angles.degreeToRadian(15));
        cameraController.update(null);

        wscene.setScene(scene);
        wscene.setCamera(camera);

    }
}
