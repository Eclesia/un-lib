
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.model.Column;
import science.unlicense.engine.ui.model.WRow;
import science.unlicense.engine.ui.style.StyleDocument;
import static science.unlicense.engine.ui.widget.AbstractRowWidget.STYLE_CELL;

/**
 *
 * @author Johann Sorel
 */
public class WCell extends WContainer{

    public static final Chars PROPERTY_VALUE = Chars.constant("Value");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("cell")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private final WRow row;
    private final Column column;

    public WCell(WRow row, Column column) {
        super(new BorderLayout());
        setPropertyValue(XPROP_STYLEMARKER, STYLE_CELL);
        this.row = row;
        this.column = column;
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public final WRow getRow() {
        return row;
    }

    public final Column getColumn() {
        return column;
    }

    public final Object getValue(){
        return getPropertyValue(PROPERTY_VALUE);
    }

    public final void setValue(Object value){
        setPropertyValue(PROPERTY_VALUE, value);
    }

    public final Property varValue(){
        return getProperty(PROPERTY_VALUE);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_VALUE.equals(name)) {
            update(value);
        }
    }

    protected void update(Object newValue){
        final Widget content = column.getPresenter().createWidget(column.getCellValue(newValue));
        content.setLayoutConstraint(BorderConstraint.CENTER);
        children.replaceAll(new Node[]{content});
    }

}
