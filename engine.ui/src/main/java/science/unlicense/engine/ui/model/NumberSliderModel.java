
package science.unlicense.engine.ui.model;

import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int16;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int64;
import science.unlicense.common.api.number.Int8;
import science.unlicense.math.api.Maths;

/**
 *
 * @author Johann Sorel
 */
public class NumberSliderModel implements SliderModel{

    private final Class numberClass;
    private final Number min;
    private final Number max;
    private final Number def;

    public NumberSliderModel(Class numberClass, Number min, Number max, Number def) {
        this.numberClass = numberClass;
        this.min = min;
        this.max = max;
        this.def = def;
    }

    public Object getDefaultValue() {
        return toType(def);
    }

    public boolean hasNextValue(Object currentValue) {
        double i = ((Number) currentValue).doubleValue();
        return i < max.doubleValue();
    }

    public boolean hasPreviousValue(Object currentValue) {
        double i = ((Number) currentValue).doubleValue();
        return i > min.doubleValue();
    }

    public Object nextValue(Object currentValue) {
        double i = ((Number) currentValue).doubleValue() +1;
        i = Maths.clamp(i, min.doubleValue(), max.doubleValue());
        return toType(i);
    }

    public Object previousValue(Object currentValue) {
        double i = ((Number) currentValue).doubleValue() -1;
        i = Maths.clamp(i, min.doubleValue(), max.doubleValue());
        return toType(i);
    }

    public Object getValue(double ratio) {
        double v = ratio * (max.doubleValue()-min.doubleValue());
        return toType(v + min.doubleValue());
    }

    public double getRatio(Object value) {
        double v = ((Number) value).doubleValue();
        v -= min.doubleValue();
        v /= (max.doubleValue()-min.doubleValue());
        return v;
    }

    private Number toType(Number value){
        if (numberClass == Byte.class || numberClass == Int8.class){
            return value.byteValue();
        } else if (numberClass == Short.class || numberClass == Int16.class){
            return value.shortValue();
        } else if (numberClass == Integer.class || numberClass == Int32.class){
            return value.intValue();
        } else if (numberClass == Long.class || numberClass == Int64.class){
            return value.longValue();
        } else if (numberClass == Float.class || numberClass == Float32.class){
            return value.floatValue();
        } else if (numberClass == Double.class || numberClass == Float64.class){
            return value.doubleValue();
        } else {
            throw new RuntimeException("Unsupported number class :"+numberClass.getName());
        }
    }

}
