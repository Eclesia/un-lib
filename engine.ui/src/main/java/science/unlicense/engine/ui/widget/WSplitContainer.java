
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.display.api.layout.SplitLayout;
import science.unlicense.engine.ui.visual.SplitView;

/**
 * Container, displaying two child component spaced by a separated.
 *
 * @author Johann Sorel
 */
public class WSplitContainer extends WContainer {

    /**
     * Property for the separator axe.
     */
    public static final Chars PROPERTY_SEPARATOR_AXE = Chars.constant("separatorAxe");
    /**
     * Property for the separator position.
     */
    public static final Chars PROPERTY_SEPARATOR_POSITION = Chars.constant("separatorPosition");


    public WSplitContainer() {
        this(false);
    }

    public WSplitContainer(boolean vertical) {
        super(new SplitLayout());
        setView(new SplitView(this));
        ((SplitLayout) getLayout()).setVerticalSplit(vertical);
        final WSpace space = new WSpace(3, 3);
        space.setPropertyValue(XPROP_STYLEMARKER, new Chars("SplitSeparator"));
        addChild(space, SplitConstraint.SPLITTER);
    }

    /**
     * Set separator position.
     * @param separatorPosition separator position
     */
    public void setSplitPosition(double separatorPosition) {
        ((SplitLayout) getLayout()).setSplitPosition(separatorPosition);
    }

    /**
     * Get separator position.
     * @return double
     */
    public double getSplitPosition(){
        return ((SplitLayout) getLayout()).getSplitPosition();
    }

    /**
     * Set separator axe.
     */
    public void setVerticalSplit(boolean vertical) {
        ((SplitLayout) getLayout()).setVerticalSplit(vertical);
    }

    /**
     * Get separator axe.
     */
    public boolean isVerticalSplit(){
        return ((SplitLayout) getLayout()).isVerticalSplit();
    }

}
