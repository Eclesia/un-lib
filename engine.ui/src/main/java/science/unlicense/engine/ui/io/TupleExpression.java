
package science.unlicense.engine.ui.io;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Vector2f64;

/**
 * Tuple which values are expressions.
 *
 * @author Johann Sorel
 */
public class TupleExpression extends Vector2f64 implements Expression{

    private final Expression exp0;
    private final Expression exp1;

    public TupleExpression(Expression exp0, Expression exp1) {
        this.exp0 = exp0;
        this.exp1 = exp1;
    }

    @Override
    public Tuple evaluate(Object candidate) {
        if (exp0 != null) x = (Double) exp0.evaluate(candidate);
        if (exp1 != null) y = (Double) exp1.evaluate(candidate);
        return this;
    }

    //TODO other units
    public static final class PercentValue implements Expression{

        private final double value;
        private final boolean vertical;

        public PercentValue(double value,boolean vertical) {
            this.value = value;
            this.vertical = vertical;
        }

        @Override
        public Object evaluate(Object candidate) {
            if (candidate instanceof Widget) {
                final Extent ext = ((Widget) candidate).getEffectiveExtent();
                return ext.get(vertical?1:0) * value;
            } else if (candidate instanceof Rectangle) {
                final Rectangle ext = (Rectangle) candidate;
                if (vertical) {
                    return ext.getY() + ext.getHeight()*value;
                } else {
                    return ext.getX() + ext.getWidth()*value;
                }
            }
            throw new UnimplementedException("TODO");
        }
    }

    public static final class Add implements Expression{

        private final Expression exp0;
        private final Expression exp1;

        public Add(Expression exp0, Expression exp1) {
            this.exp0 = exp0;
            this.exp1 = exp1;
        }

        @Override
        public Object evaluate(Object candidate) {
            return (Double) exp0.evaluate(candidate) + (Double) exp1.evaluate(candidate);
        }
    }

    public static final class Sub implements Expression{

        private final Expression exp0;
        private final Expression exp1;

        public Sub(Expression exp0, Expression exp1) {
            this.exp0 = exp0;
            this.exp1 = exp1;
        }

        @Override
        public Object evaluate(Object candidate) {
            return (Double) exp0.evaluate(candidate) - (Double) exp1.evaluate(candidate);
        }
    }

}
