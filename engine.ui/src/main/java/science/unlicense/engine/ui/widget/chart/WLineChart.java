
package science.unlicense.engine.ui.widget.chart;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.primitive.DoubleSequence;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.style.GeomStyle;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.geometry.impl.transform.Projections;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.impl.Scalarf64;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class WLineChart extends WLeaf {

    private static final GeomStyle DEFAULT_STYLE = new GeomStyle(new PlainBrush(1, PlainBrush.LINECAP_BUTT), new ColorPaint(Color.GRAY_DARK), null);

    private Dictionary datas = new HashDictionary();
    private Dictionary styles = new HashDictionary();
    private BBox viewBox = new BBox(new Vector2f64(0, 0), new Vector2f64(1, 1));

    public WLineChart() {
        setView(new View(this));
    }

    public Dictionary getDatas() {
        return datas;
    }

    public void setDatas(Dictionary datas) {
        CObjects.ensureNotNull(datas);
        this.datas = datas;
    }

    public Dictionary getStyles() {
        return styles;
    }

    public void setStyles(Dictionary styles) {
        CObjects.ensureNotNull(styles);
        this.styles = styles;
    }

    public BBox getViewBox() {
        return viewBox;
    }

    public void setViewBox(BBox viewBox) {
        this.viewBox = viewBox;
    }

    private class View extends WidgetView {

        public View(Widget widget) {
            super(widget);
        }

        @Override
        protected void renderSelf(Painter2D painter, BBox innerBBox) {
            super.renderSelf(painter, innerBBox);

            drawGrid(painter, innerBBox);

            //painter.setClip(new Rectangle(innerBBox));
            painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
            painter.setPaint(new ColorPaint(Color.BLACK));

            //we want Y upward
            final AffineRW widgetToView = Projections.zoomed(innerBBox, viewBox);
            final AffineRW viewToWidget = widgetToView.invert(null);

            final Iterator ite = datas.getPairs().createIterator();
            while (ite.hasNext()) {
                final Pair pair = (Pair) ite.next();
                final Chars key = (Chars) pair.getValue1();
                final TupleSpace space = (TupleSpace) pair.getValue2();

                //build a geometry from given tuple space
                final DoubleSequence points = new DoubleSequence();
                final TupleSpaceCursor cursor = space.cursor();

                final Vector2f64 v = new Vector2f64();
                for (double x = innerBBox.getMin(0),xn=innerBBox.getMax(0); x<=xn;x++) {
                    v.x = x;
                    v.y = 0;
                    widgetToView.transform(v, v);
                    cursor.moveTo(new Scalarf64(v.x));
                    v.y = cursor.samples().get(0);
                    viewToWidget.transform(v, v);
                    points.put(v.x);
                    points.put(-v.y);
                }

                final TupleGrid1D coords = InterleavedTupleGrid1D.create(points.toArrayDouble(), 2);
                final Polyline line = new Polyline(coords);

                GeomStyle style = (GeomStyle) styles.getValue(key);
                if (style == null) style = DEFAULT_STYLE;

                if (style.getBrushPaint() != null) {
                    painter.setBrush(style.getBrush());
                    painter.setPaint(style.getBrushPaint());
                    painter.stroke(line);
                }
                if (style.getFillPaint() != null) {
                    painter.setPaint(style.getFillPaint());
                    painter.fill(line);
                }
            }

        }

        private void drawGrid(Painter2D painter, BBox innerBBox) {

            painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
            painter.setPaint(new ColorPaint(Color.GRAY_LIGHT));

            final AffineRW widgetToView = Projections.zoomed(innerBBox, viewBox);
            final AffineRW viewToWidget = widgetToView.invert(null);

            final BBox bbox = Geometries.transform(innerBBox, widgetToView, null);


            double minx = bbox.getMin(0);
            double maxx = bbox.getMax(0);
            double miny = bbox.getMin(1);
            double maxy = bbox.getMax(1);
            double step = 1.0;

            minx = ((int) (minx * step)) / step - step;
            maxx = ((int) (maxx * step)) / step + step;
            miny = ((int) (miny * step)) / step - step;
            maxy = ((int) (maxy * step)) / step + step;


            final Vector2f64 v = new Vector2f64();
            final Line segment = new DefaultLine(0, 0, 0, 0);
            for (double y=miny; y<=maxy; y+=step) {
                v.x = minx;
                v.y = y;
                viewToWidget.transform(v, v);
                segment.getStart().set(v);
                v.x = maxx;
                v.y = y;
                viewToWidget.transform(v, v);
                segment.getEnd().set(v);

                painter.stroke(segment);
            }

            for (double x=minx; x<=maxx; x+=step) {
                v.x = x;
                v.y = miny;
                viewToWidget.transform(v, v);
                segment.getStart().set(v);
                v.x = x;
                v.y = maxy;
                viewToWidget.transform(v, v);
                segment.getEnd().set(v);

                painter.stroke(segment);
            }

        }

    }

}
