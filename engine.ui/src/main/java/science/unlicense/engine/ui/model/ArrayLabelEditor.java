
package science.unlicense.engine.ui.model;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class ArrayLabelEditor extends WLabel implements SpinnerEditor {
    private Object value = "";

    public ArrayLabelEditor() {
        setPropertyValue(XPROP_STYLEMARKER, new Chars("WSpinner-LabelEditor"));
    }

    public void setText(Chars text) {
        super.setText(text);
        sendPropertyEvent(PROPERTY_VALUE, null, getValue());
    }

    public Widget getWidget() {
        return this;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
        setText(toText(value));
    }

    protected Chars toText(Object value){
        return CObjects.toChars(value);
    }

}
