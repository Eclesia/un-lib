
package science.unlicense.engine.ui.ievent;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;

/**
 *
 * @author Johann Sorel
 */
public class AbstractActionExecutable extends AbstractEventSource implements ActionExecutable{

    public static final Chars PROPERTY_IMAGE = Chars.constant("image");
    public static final Chars PROPERTY_TEXT = Chars.constant("text");
    public static final Chars PROPERTY_ACTIVE = Chars.constant("active");

    private Image image = null;
    private Chars text = Chars.EMPTY;
    private boolean active = true;

    public Image getImage(Extent extent) {
        return image;
    }

    public void setImage(Image image) {
        if (CObjects.equals(this.image,image)) return;
        Image old = this.image;
        this.image = image;
        sendPropertyEvent(PROPERTY_IMAGE, old, image);
    }

    public Chars getText() {
        return text;
    }

    public void setText(Chars text) {
        if (CObjects.equals(this.text,text)) return;
        Chars old = this.text;
        this.text = text;
        sendPropertyEvent(PROPERTY_TEXT, old, text);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        if (this.active == active) return;
        boolean old = this.active;
        this.active = active;
        sendPropertyEvent(PROPERTY_ACTIVE, old, active);
    }

    public Object perform() {
        return null;
    }

    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

}
