
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.DisplacementLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Maths;

/**
 * Container, displaying a scroll bars whenever inner component
 * is to wide for rendering in given space.
 *
 * @author Johann Sorel
 */
public class WScrollContainer extends WContainer{

    public static final int SLIDERSIZE = 20;

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("scrollcontainer")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private final WScrollBar horizontalBar = new WScrollBar(true);
    private final WScrollBar verticalBar = new WScrollBar(false);
    private final EventListener scrollsListener = new EventListener() {
        public void receiveEvent(Event event) {
            final PropertyMessage pe = (PropertyMessage) event.getMessage();
            if (WScrollBar.PROPERTY_RATIO.equals(pe.getPropertyName())){
                final EventSource source = event.getSource();
                final Extent ext = container.getEffectiveExtent();
                final Widget w = getScrolledWidget();
                if (w==null) return;
                final Extent wext = w.getEffectiveExtent();
                if (source==horizontalBar){
                    final double ratio = horizontalBar.getRatio();
                    final double width = ext.get(0);
                    final double remain = Maths.max(0,wext.get(0)-width);
                    scrollLayout.setOffsetX(-(remain*ratio));
                } else {
                    final double ratio = verticalBar.getRatio();
                    final double height = ext.get(1);
                    final double remain = Maths.max(0,wext.get(1)-height);
                    scrollLayout.setOffsetY(-(remain*ratio));
                }
            }
        }
    };
    private final DisplacementLayout scrollLayout = new DisplacementLayout();
    private final WContainer container = new WContainer(scrollLayout){
        protected void receiveEventChildren(Event event) {
            super.receiveEventChildren(event);

            if (event.getMessage() instanceof PropertyMessage
               && Widget.PROPERTY_EFFECTIVE_EXTENT.equals(((PropertyMessage) event.getMessage()).getPropertyName())
               && event.getSource() == getScrolledWidget()){
                checkScrollVisibility();
            }
        }
    };

    public WScrollContainer() {
        this(null);
    }

    public WScrollContainer(Widget scrolledWidget) {
        final FormLayout formLayout = new FormLayout();
        formLayout.setColumnSize(0, FormLayout.SIZE_EXPAND);
        formLayout.setRowSize(0, FormLayout.SIZE_EXPAND);
        setLayout(formLayout);
        addChild(verticalBar, FillConstraint.builder().coord(1, 0).fill(true, true).span(1, 2).build());
        addChild(horizontalBar, FillConstraint.builder().coord(0, 1).fill(true, true).build());
        addChild(container, FillConstraint.builder().coord(0, 0).fill(true, true).build());

        horizontalBar.addEventListener(PropertyMessage.PREDICATE, scrollsListener);
        verticalBar.addEventListener(PropertyMessage.PREDICATE, scrollsListener);
        setScolledWidget(scrolledWidget);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public void setScolledWidget(Widget scrolledWidget){
        container.getChildren().removeAll();
        if (scrolledWidget!=null){
            container.getChildren().add(scrolledWidget);
        }

        checkScrollVisibility();
    }

    public Widget getScrolledWidget(){
        final Iterator ite = container.getChildren().createIterator();
        while (ite.hasNext()) {
            final Object n = ite.next();
            if (n instanceof Widget){
                return (Widget) n;
            }
        }
        return null;
    }

    public WContainer getScrollingContainer() {
        return container;
    }

    public WScrollBar getHorizontalScrollBar() {
        return horizontalBar;
    }

    public WScrollBar getVerticalScrollBar() {
        return verticalBar;
    }

    private void checkScrollVisibility(){
        final Extent ext = container.getEffectiveExtent();
        final Widget w = getScrolledWidget();
        if (w==null) return;
        final Extent wext = w.getEffectiveExtent();
        horizontalBar.setVisible(wext.get(0)>ext.get(0));
        verticalBar.setVisible(wext.get(1)>ext.get(1));
    }

    protected void receiveEventParent(Event event) {
        super.receiveEventParent(event);

        if (event.getMessage() instanceof MouseMessage && !event.getMessage().isConsumed()){
            //scroll if mouse wheel event
            final MouseMessage me = (MouseMessage) event.getMessage();
            final int type = me.getType();

            if (type == MouseMessage.TYPE_WHEEL){
                final double ratio = getVerticalScrollBar().getRatio();
                final double step = getVerticalScrollBar().getStep();
                double r = ratio - me.getWheelOffset()*step;
                r = Maths.clamp(r, 0, 1);
                getVerticalScrollBar().setRatio(r);
                me.consume();
            }
        }
    }

}
