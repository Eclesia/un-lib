package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.component.WColorChooser;
import science.unlicense.engine.ui.component.WColorSquare;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Maths;

/**
 *
 * @author Johann Sorel
 */
public class WColorField extends AbstractControlWidget implements WValueWidget {

    public static final Chars PROPERTY_COLOR = Chars.constant("Color");

    private final WGraphicText label = new WGraphicText();
    private final WColorSquare square = new WColorSquare(new Extent.Double(12, 1));

    public WColorField() {
        setLayout(new BorderLayout());
        addChild(label, BorderConstraint.CENTER);
        addChild(square, BorderConstraint.RIGHT);

        square.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final MouseMessage mm = (MouseMessage) event.getMessage();
                if (mm.getType() == MouseMessage.TYPE_TYPED) {
                    Color color = getColor();
                    if (color == null) color = Color.BLACK;
                    final WColorChooser chooser = new WColorChooser(color);
                    final Color c = chooser.showOpenDialog(getFrame());
                    setColor(c);
                }
            }
        });
        square.varColor().sync(varColor());
    }

    @Override
    public final Chars getValuePropertyName() {
        return PROPERTY_COLOR;
    }

    public final Color getColor() {
        return (Color) getPropertyValue(PROPERTY_COLOR);
    }

    public final void setColor(Color color) {
        setPropertyValue(PROPERTY_COLOR, color);
    }

    public final Property varColor() {
        return getProperty(PROPERTY_COLOR);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_COLOR.equals(name)) {
            if (value == null) {
                label.setText(Chars.EMPTY);
            } else {
                final Color color = (Color) value;
                final float[] array = color.toFloat();

                final CharBuffer cb = new CharBuffer();
                for (int i=0; i<array.length; i++) {
                    if (i > 0) cb.append(" | ");
                    float f = Maths.round(array[i] * 100f) / 100f;
                    cb.append(""+f);
                }
                label.setText(cb.toChars());
            }
        }

    }

}
