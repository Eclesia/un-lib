
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.style.StyleDocument;
import static science.unlicense.engine.ui.widget.AbstractControlWidget.PROPERTY_VALUE;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Scalarf64;

/**
 *
 * @author Johann Sorel
 */
public class WTupleField extends WContainer implements WValueWidget {

    public static final Chars PROPERTY_TUPLE = Chars.constant("Tuple");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("tuplefield")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WTupleField() {
        super(new GridLayout(1, -1));
        //placeholder to avoid height changing
        final WCellEditor edit = new WCellEditor(new Scalarf64(), 0);
        edit.setVisible(true);
        edit.setReserveSpace(true);
        addChild(edit, null);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public final Tuple getTuple() {
        return (Tuple) getPropertyValue(PROPERTY_TUPLE);
    }

    public final void setTuple(Tuple tuple) {
        setPropertyValue(PROPERTY_TUPLE, tuple);
    }

    public final Property varTuple() {
        return getProperty(PROPERTY_TUPLE);
    }

    @Override
    public final Chars getValuePropertyName() {
        return PROPERTY_TUPLE;
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        if (PROPERTY_TUPLE.equals(name)) {
            getChildren().removeAll();
            if (value == null) {
                //placeholder to avoid height changing
                final WCellEditor edit = new WCellEditor(new Scalarf64(), 0);
                edit.setVisible(false);
                edit.setReserveSpace(true);
                addChild(edit, null);
                return;
            } else {
                final Tuple tuple = (Tuple) value;
                final int nbElement = tuple.getSampleCount();

                for (int i=0; i<nbElement; i++) {
                    final WCellEditor edit = new WCellEditor(tuple, i);
                    addChild(edit, null);
                }
            }
        }
        super.valueChanged(name, oldValue, value);
    }

    private final class WCellEditor extends WSpinner{

        public WCellEditor(final Tuple tuple, final int index) {
            super(new NumberSpinnerModel(tuple.getNumericType().getValueClass(),0,null,null,1));
            WCellEditor.this.setPropertyValue(StyleDocument.PROPERTY_STYLE_CLASS, Chars.EMPTY);
            setValue(tuple.get(index));
            if (tuple instanceof TupleRW) {
                varValue().addListener(new EventListener() {
                    public void receiveEvent(Event event) {
                        try {
                            final double val = ((Number) getValue()).doubleValue();
                            ((TupleRW) tuple).set(index, val);
                            WTupleField.this.getEventManager().sendPropertyEvent(WTupleField.this, PROPERTY_VALUE, tuple, tuple);
                        } catch (RuntimeException ex) {
                            Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                        }
                    }
                });
            }
        }

    }

}
