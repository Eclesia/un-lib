
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.style.StyleDocument;

/**
 *
 * @author Johann Sorel
 */
public class AbstractControlWidget extends WContainer implements WValueWidget {

    public static final Chars PROPERTY_VALUE = Chars.constant("Value");
    public static final Chars PROPERTY_EDITION_VALID = Chars.constant("EditionValid");
    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.add(PROPERTY_EDITION_VALID, Boolean.TRUE);
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("controlwidget")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    protected AbstractControlWidget() {
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    protected void setEditionValid(boolean valid) {
        setPropertyValue(PROPERTY_EDITION_VALID,valid);
    }

    public boolean isEditionValid() {
        return (Boolean) getPropertyValue(PROPERTY_EDITION_VALID);
    }

    @Override
    public Chars getValuePropertyName() {
        return PROPERTY_VALUE;
    }

    public Object getValue() {
        return getPropertyValue(PROPERTY_VALUE);
    }

    public void setValue(Object value) {
        setPropertyValue(PROPERTY_VALUE, value);
    }

    public Property varValue() {
        return getProperty(PROPERTY_VALUE);
    }

}
