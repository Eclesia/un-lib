
package science.unlicense.engine.ui.visual;

import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.model.WTreePath;
import static science.unlicense.engine.ui.model.WTreePath.DEPTH;
import static science.unlicense.engine.ui.model.WTreePath.TYPE_END;
import static science.unlicense.engine.ui.model.WTreePath.TYPE_LINE;
import static science.unlicense.engine.ui.model.WTreePath.TYPE_MIDDLE;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.SystemStyle;
import static science.unlicense.engine.ui.style.WidgetStyles.asPaint;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class TreePathView extends WidgetView {

    private final VectorRW lineStart = new Vector2f64();
    private final VectorRW lineEnd = new Vector2f64();
    private final Line line = new DefaultLine(lineStart, lineEnd);

    public TreePathView(WTreePath widget) {
        super(widget);
    }

    @Override
    public WTreePath getWidget() {
        return (WTreePath) super.getWidget();
    }

    @Override
    public void getExtents(Extents buffer, Extent constraint) {
        final int[] depths = getWidget().getDepths();
        if (depths == null) {
            buffer.setAll(0, DEPTH);
        } else {
            buffer.setAll(depths.length*DEPTH, DEPTH);
        }
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }

    @Override
    protected void renderSelf(Painter2D painter, BBox innerBBox) {

        final int[] depths = getWidget().getDepths();
        final BBox bbox = widget.getBoundingBox(null);
        final double minx = bbox.getMin(0);
        final double miny = bbox.getMin(1);
        final double maxx = bbox.getMax(0);
        final double maxy = bbox.getMax(1);
        final StyleDocument doc = getWidget().getEffectiveStyleDoc();
        final Paint color = asPaint(doc, SystemStyle.COLOR_DELIMITER, widget);
        painter.setPaint(color);
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));

        //draw the graph visual
        for (int i = 0; i < depths.length; i++) {
            if (depths[i] == TYPE_MIDDLE) {
                lineStart.setX(minx + i*DEPTH + (DEPTH/2));
                lineStart.setY(miny);
                lineEnd.setX(minx + i*DEPTH + (DEPTH/2));
                lineEnd.setY(maxy);
                painter.stroke(line);
                lineStart.setX(minx + i*DEPTH + (DEPTH/2));
                lineStart.setY(0);
                lineEnd.setX(minx + i*DEPTH + DEPTH);
                lineEnd.setY(0);
                painter.stroke(line);
            } else if (depths[i] == TYPE_LINE) {
                lineStart.setX(minx + i*DEPTH + (DEPTH/2));
                lineStart.setY(miny);
                lineEnd.setX(minx + i*DEPTH + (DEPTH/2));
                lineEnd.setY(maxy);
                painter.stroke(line);
            } else if (depths[i] == TYPE_END) {
                lineStart.setX(minx + i*DEPTH + (DEPTH/2));
                lineStart.setY(miny);
                lineEnd.setX(minx + i*DEPTH + (DEPTH/2));
                lineEnd.setY(0);
                painter.stroke(line);
                lineStart.setX(minx + i*DEPTH + (DEPTH/2));
                lineStart.setY(0);
                lineEnd.setX(minx + i*DEPTH + DEPTH);
                lineEnd.setY(0);
                painter.stroke(line);
            }
        }
    }
}
