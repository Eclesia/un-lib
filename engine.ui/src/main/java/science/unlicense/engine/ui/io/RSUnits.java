
package science.unlicense.engine.ui.io;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.unitold.BaseUnit;
import science.unlicense.math.impl.unitold.DerivedUnit;
import science.unlicense.math.api.unitold.Unit;

/**
 *
 * @author Johann Sorel
 */
public final class RSUnits {

    public static final Unit EM = new DerivedUnit(BaseUnit.METER, 1, new Chars("em"));
    public static final Unit CM = new DerivedUnit(BaseUnit.METER, 1, new Chars("cm"));
    public static final Unit MM = new DerivedUnit(BaseUnit.METER, 1, new Chars("mm"));
    public static final Unit PX = new DerivedUnit(BaseUnit.METER, 1, new Chars("px"));
    public static final Unit PT = new DerivedUnit(BaseUnit.METER, 1, new Chars("pt"));
    public static final Unit PC = new DerivedUnit(BaseUnit.METER, 1, new Chars("pc"));

    private RSUnits(){}

    public static Unit forName(Chars name) {
        if (name.equals(EM.getUnitName())) return EM;
        if (name.equals(CM.getUnitName())) return CM;
        if (name.equals(MM.getUnitName())) return MM;
        if (name.equals(PX.getUnitName())) return PX;
        if (name.equals(PT.getUnitName())) return PT;
        if (name.equals(PC.getUnitName())) return PC;
        throw new IllegalArgumentException("Unvalid unit "+name);
    }

}
