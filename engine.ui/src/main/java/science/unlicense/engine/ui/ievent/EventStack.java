
package science.unlicense.engine.ui.ievent;

import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;

/**
 * Stores event and merge those which can to reduce their number.
 *
 *
 * @author Johann Sorel
 */
public class EventStack {

    private final Sequence stack = new ArraySequence();

    /**
     * Add event in the stack.
     *
     * @param event
     */
    public synchronized void put(Event event){
        if (event==null) return;
        final EventMessage message = event.getMessage();
        if (message instanceof MouseMessage && !stack.isEmpty() && ((MouseMessage) message).getType()==MouseMessage.TYPE_MOVE){
            //try to merge with previous event
            final int index = stack.getSize()-1;
            final EventMessage previous = ((Event) stack.get(index)).getMessage();
            if (previous instanceof MouseMessage && ((MouseMessage) previous).getType()==MouseMessage.TYPE_MOVE){
                //replace the event
                stack.replace(index, event);
                return;
            }
        }

        stack.add(event);
    }

    /**
     * Forward all stored events to given listener.
     *
     * @param listener
     */
    public synchronized void forward(EventListener listener){
        if (listener!=null && !stack.isEmpty()){
            for (int i=0,n=stack.getSize();i<n;i++){
                listener.receiveEvent((Event) stack.get(i));
            }
        }
    }

    /**
     * Flush stacked events.
     * @return stacked events
     */
    public synchronized Object[] flush(){
        final Object[] array = stack.toArray();
        stack.removeAll();
        return array;
    }
}
