
package science.unlicense.engine.ui.desktop;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.desktop.Frame;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;

/**
 * A UI Frame is Frame with a container panel.
 *
 * @author Johann Sorel
 */
public interface UIFrame extends Frame{

    public static final Chars PROP_FOCUSED_WIDGET = Chars.constant("FocusedWidget");

    Painter2D getPainter();

    /**
     * Get frame main container.
     *
     * @return WContainer
     */
    WContainer getContainer();

    /**
     * Get focused widget.
     *
     * @return Widget, can be null
     */
    Widget getFocusedWidget();

    /**
     * Set focused widget.
     *
     * @param widget can be null.
     */
    void setFocusedWidget(Widget widget);

}
