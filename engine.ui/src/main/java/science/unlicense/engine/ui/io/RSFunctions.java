
package science.unlicense.engine.ui.io;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Number;
import science.unlicense.common.api.predicate.AbstractFunction;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.common.api.predicate.Function;
import science.unlicense.common.api.predicate.FunctionResolver;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.font.Font;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.LinearGradientPaint;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.display.api.painter2d.PatternPaint;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.display.api.painter2d.RadialGradientPaint;
import science.unlicense.display.api.painter2d.SpreadMode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.style.ShapeTransform;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.impl.process.paint.RotateColorOperator;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class RSFunctions implements FunctionResolver {

    public static final FunctionResolver INSTANCE;
    static {
        try {
            INSTANCE = new RSFunctions();
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    private static final Chars POURCENT = Chars.constant("%");
    private static final Chars PIXEL = Chars.constant("pixel");

    private static final Dictionary nameToCode = new HashDictionary();
    static {
        nameToCode.add(new Chars("rotateHSL"),1);
        nameToCode.add(new Chars("rotateAlpha"),2);
        nameToCode.add(new Chars("colorfill"),3);
        nameToCode.add(new Chars("rgb"),4);
        nameToCode.add(new Chars("rgba"),5);
        nameToCode.add(new Chars("modulo"),6);
        nameToCode.add(new Chars("lineargradientfill"),8);
        nameToCode.add(new Chars("radialgradientfill"),9);
        nameToCode.add(new Chars("plainbrush"),10);
        nameToCode.add(new Chars("font"),11);
        nameToCode.add(new Chars("glyph"),12);
        nameToCode.add(new Chars("imagefill"),13);
        nameToCode.add(new Chars("affineTransform"),14);
        nameToCode.add(new Chars("fitTransform"),15);
        nameToCode.add(new Chars("scaleTransform"),16);
        nameToCode.add(new Chars("centerTransform"),17);
        nameToCode.add(new Chars("zoomTransform"),18);
        nameToCode.add(new Chars("stretchTransform"),19);
        nameToCode.add(new Chars("alignTransform"),20);
    }

    private RSFunctions() throws ClassNotFoundException {
    }

    @Override
    public Function resolve(Chars name, Expression[] parameters) {

        Object code = nameToCode.getValue(name);
        if (code == null) return null;

        final int ic = (Integer) code;

        return new AbstractFunction(name, parameters) {
            @Override
            public Object evaluate(Object candidate) {

                final Object[] args = evaluateParameters(candidate);

                switch (ic) {
                    case  1 : return rotateHSL((Color) args[0], toNumber(args[1]), toNumber(args[2]), toNumber(args[3]));
                    case  2 : return rotateAlpha((Color) args[0], toNumber(args[1]));
                    case  3 : return colorfill((Color) args[0]);
                    case  4 : return rgb(toNumber(args[0]), toNumber(args[1]), toNumber(args[2]));
                    case  5 : return rgba(toNumber(args[0]), toNumber(args[1]), toNumber(args[2]), toNumber(args[3]));
                    case  6 : return modulo(toNumber(args[0]),toNumber(args[1]));
                    case  8 : return lineargradientfill(candidate, parameters);
                    case  9 : return radialgradientfill(candidate, parameters);
                    case 10 : return plainbrush(toNumber(args[0]), (Chars) args[1]);
                    case 11 : return font((Chars) args[0], toNumber(args[1]), (Chars) args[2]);
                    case 12 : return glyph(candidate, (Chars) args[0], (Chars) args[1], (Boolean) args[2]);
                    case 13 : return imagefill(candidate, parameters);
                    case 14 : return affineTransform(candidate, parameters);
                    case 15 : return fitTransform(toNumber(args[0]));
                    case 16 : return scaleTransform();
                    case 17 : return centerTransform();
                    case 18 : return zoomTransform();
                    case 19 : return stretchTransform();
                    case 20 : return alignTransform((Chars) args[0], toNumber(args[1]), toNumber(args[2]));
                    default: return null;
                }
            }
        };

    }

    /**
     * Rotate color in HSL.
     * This operation is similar to image RotateColorOperator.
     *
     * @param color
     * @param h , This value will be added to the hue and wrapped to 0 - 360
     * @param s , This value will be multiplied to the pixel saturation and clipped to 0 - 1
     * @param l , This value will be multiplied to the pixel lightning and clipped to 0 - 1
     * @return rotated color
     */
    public static Color rotateHSL(Color color, Number h, Number s, Number l) {
        return RotateColorOperator.rotate(color, h.toDouble(), s.toDouble(), l.toDouble());
    }

    /**
     * Adjust color alpha component.
     *
     * @param color
     * @param r, This value will be multiplied to the pixel alpha and clipped to 0 - 1
     * @return
     */
    public static Color rotateAlpha(Color color, Number r) {
        float[] rgba = color.toRGBA();
        rgba[3] *= r.toDouble();
        rgba[3] = Maths.clamp(rgba[3], 0, 1);
        return new ColorRGB(rgba);
    }

    public static Paint colorfill(Color color) {
        return new ColorPaint(color);
    }

    /**
     * Create a color from red, green and blue components.
     *
     * @param r red component, between 0 and 255
     * @param g green component, between 0 and 255
     * @param b blue component, between 0 and 255
     * @return Color
     */
    public static Color rgb(Number r, Number g, Number b) {
        return new ColorRGB(r.toInteger(), g.toInteger(), b.toInteger(),255);
    }

    /**
     * Create a color from red, green and blue components.
     *
     * @param r red component, between 0 and 255
     * @param g green component, between 0 and 255
     * @param b blue component, between 0 and 255
     * @return Color
     */
    public static Color rgba(Number r, Number g, Number b, Number a) {
        return new ColorRGB(r.toInteger(), g.toInteger(), b.toInteger(),a.toInteger());
    }

    public static int modulo(Number v, Number m) {
        return v.toInteger() % m.toInteger();
    }

    public static Paint lineargradientfill(Object candidate, Expression[] expressions) {

        final Object[] values = evaluateParameters(candidate,expressions);

        final Chars unit   = (Chars) values[0];
        double startx = ((Number) values[1]).toDouble();
        double starty = ((Number) values[2]).toDouble();
        double endx   = ((Number) values[3]).toDouble();
        double endy   = ((Number) values[4]).toDouble();

        final int nbSteps = (values.length-5)/2;
        final double[] steps = new double[nbSteps];
        final Color[] colors = new Color[nbSteps];
        for (int k=5,i=0;k<values.length;k+=2,i++){
            steps[i] = ((Number) values[k]).toDouble();
            colors[i] = (Color) values[k+1];
        }

        if (POURCENT.equals(unit) && candidate instanceof Widget){
            final Widget widget = (Widget) candidate;
            final BBox size = widget.getBoundingBox(null);
            startx = size.getMin(0) + size.getSpan(0)*startx;
            starty = size.getMin(1) + size.getSpan(1)*starty;
            endx = size.getMin(0) + size.getSpan(0)*endx;
            endy = size.getMin(1) + size.getSpan(1)*endy;
        }

        return new LinearGradientPaint(startx, starty, endx, endy, steps, colors,SpreadMode.PAD);
    }

    public static Paint radialgradientfill(Object candidate, Expression[] expressions) {
        final Object[] values = evaluateParameters(candidate, expressions);

        final Chars unit   = (Chars) values[0];
        double centerx  = ((Number) values[1]).toDouble();
        double centery  = ((Number) values[2]).toDouble();
        double radius   = ((Number) values[3]).toDouble();
        double focusx   = ((Number) values[4]).toDouble();
        double focusy   = ((Number) values[5]).toDouble();

        final int nbSteps = (values.length-6)/2;
        final double[] steps = new double[nbSteps];
        final Color[] colors = new Color[nbSteps];
        for (int k=6,i=0;k<values.length;k+=2,i++){
            steps[i] = ((Number) values[k]).toDouble();
            colors[i] = (Color) values[k+1];
        }

        if (POURCENT.equals(unit) && candidate instanceof Widget){
            final Widget widget = (Widget) candidate;
            final Extent size = widget.getEffectiveExtent();
            centerx *= size.get(0);
            centery *= size.get(1);
            focusx *= size.get(0);
            focusy *= size.get(1);
        }

        return new RadialGradientPaint(centerx, centery, radius, focusx, focusy, steps, colors,SpreadMode.PAD);
    }

    public static PlainBrush plainbrush(Number width, Chars cap) {
        return new PlainBrush(width.toFloat(), PlainBrush.LINECAP_ROUND);
    }

    public static FontChoice font(Chars families, Number size, Chars weight){
        return new FontChoice(families.split(','), size.toFloat(), FontChoice.WEIGHT_NONE);
    }

    public static Geometry glyph(Object candidate, Chars family, Chars unicode, boolean reverseY){

        final FontChoice fontChoice = font(family, new Int32(1), null);
        final FontStore store = ((Widget) candidate).getFrame().getPainter().getFontStore();
        final Font font = store.getFont(fontChoice);
        PlanarGeometry geom = font.getGlyph(unicode.getUnicode(0));
        //flip it
        if (reverseY) {
            final Affine2 trs = new Affine2(1, 0, 0, 0,-1, 0);
            return TransformedGeometry.create(geom, trs);
        } else {
            return geom;
        }
    }

    public static PatternPaint imagefill(Object candidate, Expression[] expressions) {

        final Object[] values = evaluateParameters(candidate, expressions);

        final Chars path = (Chars) values[0];
        double x         = ((Number) values[1]).toDouble();
        double y         = ((Number) values[2]).toDouble();
        double width     = ((Number) values[3]).toDouble();
        double height    = ((Number) values[4]).toDouble();
        final Chars mode = (Chars) values[5];

        final Image image;
        try {
            image = Images.read(Paths.resolve(path));
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }

        SpreadMode spread = SpreadMode.FILL;
        if (new Chars("FILL").equals(mode, true, true)) {
            spread = SpreadMode.FILL;
        } else if (new Chars("REPEAT").equals(mode, true, true)) {
            spread = SpreadMode.REPEAT;
        } else if (new Chars("REFLECT").equals(mode, true, true)) {
            spread = SpreadMode.REFLECT;
        }

        return new PatternPaint(image,new Rectangle(x, y, width, height),null,spread);
    }

    public static ShapeTransform affineTransform(Object candidate, Expression[] expressions) {
        final Object[] params = evaluateParameters(candidate, expressions);
        final double scaleX = ((Number) params[0]).toDouble();
        final double shearY = ((Number) params[1]).toDouble();
        final double shearX = ((Number) params[2]).toDouble();
        final double scaleY = ((Number) params[3]).toDouble();
        final double trsX   = ((Number) params[4]).toDouble();
        final double trsY   = ((Number) params[5]).toDouble();
        final Affine2 trs = new Affine2(
                scaleX,shearX,trsX,
                shearY,scaleY,trsY
        );
        return new ShapeTransform.Affine(trs);
    }

    public static ShapeTransform fitTransform(Number axis) {
        return new ShapeTransform.Fit(axis.toInteger());
    }

    public static ShapeTransform scaleTransform() {
        return new ShapeTransform.Scale();
    }

    public static ShapeTransform centerTransform() {
        return new ShapeTransform.Center();
    }

    public static ShapeTransform zoomTransform() {
        return new ShapeTransform.Zoom();
    }

    private static Number toNumber(Object num) {
        if (num instanceof Number) {
            return (Number) num;
        } else if (num instanceof java.lang.Number) {
            return new Float64( ((java.lang.Number) num).doubleValue() );
        } else {
            throw new IllegalArgumentException("Not a number "+ num);
        }
    }

    public static ShapeTransform stretchTransform() {
        return new ShapeTransform.Stretch();
    }

    public static ShapeTransform alignTransform(Chars corner, Number x, Number y) {
        return new ShapeTransform.Align(x.toDouble(),y.toDouble());
    }

    private static Object[] evaluateParameters(Object candidate, Expression[] parameters){
        final Object[] values = new Object[parameters.length];
        for (int i=0;i<values.length;i++){
            values[i] = parameters[i].evaluate(candidate);
        }
        return values;
    }

}
