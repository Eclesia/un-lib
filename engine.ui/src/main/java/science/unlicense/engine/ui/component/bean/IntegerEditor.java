
package science.unlicense.engine.ui.component.bean;

import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.WValueWidget;

/**
 *
 * @author Johann Sorel
 */
public class IntegerEditor implements PropertyEditor {

    public static final IntegerEditor INSTANCE = new IntegerEditor();

    private IntegerEditor(){}

    @Override
    public WValueWidget create(Property property) {
        if (Integer.class.isAssignableFrom(property.getType().getValueClass())){
            return new WSpinner(new NumberSpinnerModel(Integer.class, 0, null, null, 1));
        }
        return null;
    }

}
