

package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.DisplacementLayout;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.WScrollContainer;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public class WInteractiveView extends AbstractPathView {

    private final PathPresenter[] presenters;

    private final Extent blockSize = new Extent.Double(128, 128);
    private final WContainer blockContainer = new WContainer(new LineLayout());
    private final WContainer interContainer;
    private final WScrollContainer scroll;

    private final EventListener selectionListener = new EventListener() {
        public void receiveEvent(Event event) {
            if (event.getMessage() instanceof PropertyMessage){
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                if (WThumbnail.PROPERTY_SELECTED.equals(pe.getPropertyName())) {
                    if (Boolean.TRUE.equals(pe.getNewValue())) {
                        //path selected
                        final Path p = ((WThumbnail) event.getSource()).getPath();
                        setSelectedPath(new Path[]{p});
                    } else {
                        //path unselected
                        final Path p = ((WThumbnail) event.getSource()).getPath();
                        Path[] selectedPaths = getSelectedPath();
                        int idx = Arrays.getFirstOccurence(selectedPaths, 0, selectedPaths.length, p);
                        if (idx >= 0) {
                            final Path[] sel = (Path[]) Arrays.remove(selectedPaths, idx);
                            setSelectedPath(sel);
                        }
                    }
                }
            } else if (event.getMessage() instanceof MouseMessage) {
                final MouseMessage me = (MouseMessage) event.getMessage();
                final WThumbnail source = (WThumbnail) event.getSource();
                try {
                    if (me.getType()==MouseMessage.TYPE_PRESS && me.getNbClick()>1 && source.getPath().isContainer()){
                        //open selected container
                        me.consume();
                        setViewRoot(source.getPath());
                    }
                }catch(IOException ex){}
            }
        }
    };


    public WInteractiveView() {
        this(createGlyph("\uE028"), new Chars("Interactive"),true);
    }

    protected WInteractiveView(Widget graphic, Chars name, boolean showInterractiveArea) {
        super(graphic,name);
        setLayout(new BorderLayout());
        presenters = PathPresenters.getPresenters();
        final WSplitContainer split = new WSplitContainer(true);

        ((LineLayout) blockContainer.getLayout()).setDistance(7);
        blockContainer.setInlineStyle(new Chars("margin:5"));

        scroll = new WScrollContainer(blockContainer);
        ((DisplacementLayout) scroll.getScrollingContainer().getLayout()).setFillWidth(true);

        if (showInterractiveArea){
            interContainer = new WContainer(new BorderLayout());
            split.addChild(scroll, SplitConstraint.TOP);
            split.addChild(interContainer, SplitConstraint.BOTTOM);
            split.setSplitPosition(120);
            addChild(split, BorderConstraint.CENTER);
        } else {
            interContainer = null;
            addChild(scroll, BorderConstraint.CENTER);
        }

        //adapt scroll step to match one line
        blockContainer.varEffectiveExtent().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                final double step = (
                        blockContainer.getEffectiveExtent().get(1)
                        -scroll.getScrollingContainer().getEffectiveExtent().get(1))
                        /(blockSize.get(1)+((LineLayout) blockContainer.getLayout()).getDistance());
                scroll.getVerticalScrollBar().setStep(1.0/step);
            }
        });
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_VIEW_ROOT.equals(name)) {
            update(true);
        } else if (PROPERTY_SELECTED_PATHS.equals(name)) {
            update(false);
        } else if (PROPERTY_FILTER.equals(name)) {
            update(true);
        }
    }

    private void update(boolean all) {
        final Path viewRoot = getViewRoot();
        final Path[] selectedPaths = getSelectedPath();

        if (all){
            //reset scroll position
            scroll.getVerticalScrollBar().setRatio(0);
            scroll.getHorizontalScrollBar().setRatio(0);

            final Path[] datas;
            if (viewRoot==null){
                datas = new Path[0];
            } else {
                datas = (Path[]) viewRoot.getChildren().toArray(Path.class);
            }

            //sort by name and type(folder/file)
            Arrays.sort(datas, WPathChooser.FILE_SORTER);
            final Predicate filter = getFilterInternal(true);

            final Sequence widgets = new ArraySequence();

            //special widget to go back to parent
            final Extents exts = new Extents(blockSize);
            if (viewRoot!=null && viewRoot.getParent()!=null){
                final WLeaf leaf = new WSpace(1,1);
                leaf.setInlineStyle(WPathChooser.STYLE_BACK);
                leaf.getInlineStyle().setProperties(new Chars("margin:3"));
                final WLabel back = new WLabel(new Chars(".."), leaf);
                back.setOverrideExtents(exts);
                back.setVerticalAlignment(WLabel.VALIGN_CENTER);
                back.setHorizontalAlignment(WLabel.HALIGN_CENTER);
                back.setGraphicPlacement(WLabel.GRAPHIC_TOP);
                back.addEventListener(MouseMessage.PREDICATE, new EventListener() {
                    public void receiveEvent(Event event) {
                        final MouseMessage me = (MouseMessage) event.getMessage();
                        if (me.getType() == MouseMessage.TYPE_PRESS && me.getNbClick() > 1) {
                            //move to parent folder
                            me.consume();
                            setViewRoot(viewRoot.getParent());
                        }
                    }
                });
                widgets.add(back);
            }

            //children
            for (int i=0;i<datas.length;i++){
                if (datas[i] instanceof Path){
                    final Path p = (Path) datas[i];
                    if (!filter.evaluate(p)) continue;
                    final WThumbnail thumb = new WThumbnail(p, true, null,true, true, filter);
                    thumb.setOverrideExtents(exts);
                    widgets.add(thumb);
                }
            }
            blockContainer.getChildren().replaceAll(widgets);
        }

        //set selected paths
        final Object[] widgets = blockContainer.getChildren().toArray();
        for (int i=0,n=widgets.length;i<n;i++){
            Widget prev = (Widget) widgets[i];
            if (prev instanceof WThumbnail){
                prev.removeEventListener(Predicate.TRUE, selectionListener);
                ((WThumbnail) prev).setSelected(Arrays.contains(selectedPaths, ((WThumbnail) prev).getPath()));
                prev.addEventListener(Predicate.TRUE, selectionListener);
            }
        }

        //set the interactive panel
        if (interContainer!=null){
            if (selectedPaths!=null && selectedPaths.length>0){
                final Path path = selectedPaths[0];
                for (int i=0;i<presenters.length;i++){
                    final PathPresenter presenter = presenters[i];
                    if (presenter.canHandle(path)) {
                        try {
                            final Widget widget = presenter.createInteractive(path);
                            widget.setLayoutConstraint(BorderConstraint.CENTER);
                            interContainer.getChildren().replaceAll(new Node[]{widget});
                            break;
                        } catch (Exception ex) {
                            //check next presenter
                        }
                    }
                }
            }
        }

    }

}
