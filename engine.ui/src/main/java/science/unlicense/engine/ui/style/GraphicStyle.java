
package science.unlicense.engine.ui.style;

import science.unlicense.common.api.Orderable;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.display.api.painter2d.Brush;
import science.unlicense.display.api.painter2d.Contour;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.s2d.GeometryNode2D;
import science.unlicense.display.api.scene.s2d.Graphic2DFactory;
import science.unlicense.display.api.scene.s2d.ImageNode2D;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.Path;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.impl.RoundedRectangle;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.image.api.Image;
import science.unlicense.math.api.Affine;
import static science.unlicense.math.api.Maths.min;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Affine2;

/**
 * Define a widget graphic.
 *
 * @author Johann Sorel
 */
public class GraphicStyle extends GeomStyle implements Orderable{

    private static final ShapeTransform DEFAULT_TRANSFORM = new ShapeTransform.Stretch();
    private static final Affine2 IDENTITY = (Affine2) new Affine2().setToIdentity();

    public static final int MARGIN_TOP = 0;
    public static final int MARGIN_RIGHT = 1;
    public static final int MARGIN_BOTTOM = 2;
    public static final int MARGIN_LEFT = 3;

    // RUL, RUR, RLR, RLL
    private double[] cns;
    private Margin margin;
    private PlanarGeometry geometry;
    private Image image;
    private ShapeTransform transform;
    private float zorder;

    public GraphicStyle() {
        this(0,0,0,0,null,null,null,null,null);
    }

    public GraphicStyle(double round, Brush brush, Paint brushPaint, Paint fillPaint, Margin margin, PlanarGeometry geometry) {
        this(round,round,round,round,brush,brushPaint,fillPaint,margin, geometry);
    }

    public GraphicStyle(double rul, double rur, double rlr, double rll,
            Brush brush, Paint brushPaint, Paint fillPaint, Margin margin,PlanarGeometry geometry) {
        super(brush,brushPaint,fillPaint);
        this.cns = new double[]{rul,rur,rlr,rll};
        this.margin = margin;
        this.geometry = geometry;
    }

    /**
     * Create the graphic shape.
     * @param extent
     * @return Geometry2D, can be null
     */
    public SceneNode toSceneNode(Painter2D painter, Rectangle extent) {

        if (fillPaint == null && (brush == null && brushPaint == null) && image == null) {
            return null;
        }

        //append margins
        extent = new Rectangle(extent);
        if (margin != null) {
            extent.setX(extent.getX()-margin.left);
            extent.setY(extent.getY()-margin.top);
            extent.setWidth(extent.getWidth() + margin.right + margin.left);
            extent.setHeight(extent.getHeight() + margin.top + margin.bottom);
        }

        PlanarGeometry geom;
        if (geometry != null) {
            WidgetStyles.evaluate(geometry, extent);
            geom = geometry;
        } else if (image != null) {
            geom = new Rectangle(image.getExtent());
        } else if (cns[0] == cns[1] && cns[0] == cns[2] && cns[0] == cns[3]) {
            if (cns[0] == 0.0) {
                geom = new Rectangle(extent.getX(), extent.getY(),
                    extent.getWidth(), extent.getHeight());
            } else {
                geom = new RoundedRectangle(extent.getX(), extent.getY(),
                    extent.getWidth(), extent.getHeight(),cns[0],cns[0]);
            }
        } else {
            //various radius, make a custom path
            final double x = extent.getX();
            final double y = extent.getY();
            final double width = extent.getWidth();
            final double height = extent.getHeight();
            final double mx = width/2.0;
            final double my = height/2.0;

            final Path path = new Path();
            path.appendMoveTo(x,                   y+min(my,cns[0]));
            path.appendQuadTo(x,                   y,           x+min(mx,cns[0]),      y);
            path.appendLineTo(x+width-min(mx,cns[1]), y);
            path.appendQuadTo(x+width,             y,           x+width,            y+min(my,cns[1]));
            path.appendLineTo(x+width,             y+height-min(my,cns[2]));
            path.appendQuadTo(x+width,             y+height,    x+width-min(mx,cns[2]),y+height);
            path.appendLineTo(x+min(mx,cns[3]),    y+height);
            path.appendQuadTo(x,                   y+height,    x,                  y+height-min(my,cns[3]));
            path.appendClose();
            geom = path;
        }

        //apply transform
        final BBox target = extent.getBoundingBox();
        final Transform trs;
        if (transform != null) {
            trs = transform.getTransform(target, geom);
        } else {
            trs = IDENTITY;
        }

        final Graphic2DFactory factory = painter.getGraphicFactory();
        ImageNode2D imgNode = null;
        GeometryNode2D geomNode = null;
        if (image != null) {
            imgNode = factory.createImageNode();
            imgNode.setImage(image);
            imgNode.getNodeTransform().set((Affine) trs);
        }
        if (geom != null) {
            geomNode = factory.createGeometryNode();
            if (!(trs instanceof Affine && ((Affine) trs).isIdentity())) {
                geom = TransformedGeometry.create(geom, trs);
            }
            geomNode.setGeometry(geom);
            if (fillPaint != null) geomNode.setFills(new Paint[]{this.fillPaint});
            if (brush != null && brushPaint != null) geomNode.setContours(new Contour[]{new Contour(brush, brushPaint)});
        }

        if (imgNode != null && geomNode != null) {
            final SceneNode scene = factory.createNode();
            scene.getChildren().add(imgNode);
            scene.getChildren().add(geomNode);
            return scene;
        } else {
            return imgNode == null ? geomNode : imgNode;
        }
    }

    /**
     * Get the graphic expected margin, this is used to offset the inner
     * parts of the widget.
     * @return Margin, never null
     */
    public Margin getMargin() {
        return margin;
    }

    public void setMargin(Margin margin) {
        this.margin = margin;
    }

    public PlanarGeometry getGeometry() {
        return geometry;
    }

    public void setGeometry(PlanarGeometry geometry) {
        this.geometry = geometry;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public double[] getCorners() {
        return cns;
    }

    public void setCorners(double[] corners) {
        this.cns = corners;
    }

    public ShapeTransform getTransform() {
        return transform;
    }

    public void setTransform(ShapeTransform transform) {
        this.transform = transform;
    }

    public float getZorder() {
        return zorder;
    }

    public void setZorder(float zorder) {
        this.zorder = zorder;
    }

    @Override
    public int order(Object other) {
        return Float.compare(this.zorder, ((GraphicStyle) other).zorder);
    }

}
