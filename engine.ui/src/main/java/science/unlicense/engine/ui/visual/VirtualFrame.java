
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.display.api.desktop.Frame;
import science.unlicense.display.api.desktop.FrameDecoration;
import science.unlicense.display.api.desktop.FrameManager;
import science.unlicense.display.api.desktop.Screen;
import science.unlicense.display.api.desktop.cursor.Cursor;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.desktop.AbstractUIFrame;
import static science.unlicense.engine.ui.desktop.UIFrame.PROP_FOCUSED_WIDGET;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class VirtualFrame extends AbstractUIFrame {

    private final TupleRW position = new Vector2f64();
    private final Extent.Double size = new Extent.Double(2);
    private WContainer container = null;
    private FrameDecoration decoration = null;
    private Widget focused = null;

    public VirtualFrame() {
        super(null);
    }

    @Override
    public Painter2D getPainter() {
        return new VirtualPainter2D();
    }

    @Override
    public FrameManager getManager() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CharArray getTitle() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setTitle(CharArray title) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Property varTitle() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Cursor getCursor() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCursor(Cursor cursor) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setOnScreenLocation(Tuple location) {
        position.set(location);
    }

    @Override
    public Tuple getOnScreenLocation() {
        return position.copy();
    }

    @Override
    public void setSize(int width, int height) {
        size.set(0, width);
        size.set(1, height);
    }

    @Override
    public Extent getSize() {
        return size.copy();
    }

    @Override
    public void setVisible(boolean visible) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isVisible() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Property varVisible() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isModale() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public FrameDecoration getSystemDecoration() {
        return new FrameDecoration() {
            @Override
            public Margin getMargin() {
                return new Margin(20, 5, 10, 5);
            }

            @Override
            public Frame getFrame() {
                return VirtualFrame.this;
            }

            @Override
            public void setFrame(Frame frame) {
            }
        };
    }

    @Override
    public void setDecoration(FrameDecoration decoration) {
        this.decoration = decoration;
    }

    @Override
    public FrameDecoration getDecoration() {
        return decoration;
    }

    @Override
    public int getState() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setState(int state) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMaximizable(boolean maximizable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isMaximizable() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Property varMaximizable() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMinimizable(boolean minimizable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isMinimizable() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Property varMinimizable() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setClosable(boolean closable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isClosable() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Property varClosable() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setAlwaysonTop(boolean ontop) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isAlwaysOnTop() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isTranslucent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void dispose() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isDisposed() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void waitForDisposal() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public synchronized WContainer getContainer() {
        if (container == null) {
            container = new WContainer();
            container.setFrame(this);
            container.addEventListener(new PropertyPredicate(WContainer.PROPERTY_DIRTY), new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    BBox bbox = (BBox) ((PropertyMessage) event.getMessage()).getNewValue();
                    container.setEffectiveExtent(size);
                    VirtualPainter2D painter = new VirtualPainter2D();
                    ViewRenderLoop.render(container, painter, bbox);
                }
            });
        }
        return container;
    }

    @Override
    public Widget getFocusedWidget() {
        return focused;
    }

    @Override
    public void setFocusedWidget(Widget focused) {
        if (CObjects.equals(this.focused, focused)) return;
        final Widget old = this.focused;
        if (old!=null) old.setFocused(false);
        this.focused = focused;
        if (this.focused!=null) this.focused.setFocused(true);
        sendPropertyEvent(PROP_FOCUSED_WIDGET, old, this.focused);
    }

    @Override
    public Screen getScreen() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
