
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.collection.primitive.IntSet;
import science.unlicense.display.api.font.AbstractFontMetadata;
import science.unlicense.display.api.font.DerivateFont;
import science.unlicense.display.api.font.Font;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class VirtualFontStore implements FontStore {

    @Override
    public Set getFamilies() {
        return Collections.emptySet();
    }

    @Override
    public FontMetadata getMetadata(Chars family) {
        final FontMetadata meta = new VirtualFontMetadata();
        return meta;
    }

    @Override
    public Font getFont(Chars family) {
        return new VirtualFont(this, family);
    }

    @Override
    public Font getFont(FontChoice font) {
        return getFont(font.getFamilies()[0]);
    }

    @Override
    public void registerFont(Path path) throws IOException {
        throw new UnsupportedOperationException("Not supported.");
    }

    private static class VirtualFont implements Font {

        private final FontStore store;
        private final Chars family;

        public VirtualFont(FontStore store, Chars family) {
            this.store = store;
            this.family = family;
        }

        @Override
        public Chars getFamily() {
            return family;
        }

        @Override
        public int getWeight() {
            return Font.WEIGHT_NONE;
        }

        @Override
        public float getSize() {
            return 10;
        }

        @Override
        public FontStore getStore() {
            return store;
        }

        @Override
        public FontMetadata getMetaData() {
            return store.getMetadata(family);
        }

        @Override
        public IntSet listCharacters() {
            return new IntSet();
        }

        @Override
        public PlanarGeometry getGlyph(int unicode) {
            return new Rectangle(getMetaData().getGlyphBox());
        }

        @Override
        public PlanarGeometry getGlyph(Char character) {
            return getGlyph(character.toUnicode());
        }

        @Override
        public Font derivate(float fontSize) {
            if (fontSize==getSize()) return this;
            final double scaling = fontSize/getSize();
            final Affine2 trs = new Affine2(scaling, 0, 0, 0, scaling, 0);
            return new DerivateFont(this, trs, fontSize);
        }

    }

    private static class VirtualFontMetadata extends AbstractFontMetadata {

        public VirtualFontMetadata() {
            advanceWidthMax = 10;
            ascent = 1;
            descent = 1;
            glyphBox = new BBox(new double[]{0, 0}, new double[]{10, 10});
            height = 10;
            lineGap = 1;
            minLeftSideBearing = 0;
            minRightSideBearing = 0;
            weight = Font.WEIGHT_NONE;
            xMaxExtent = 11;
        }

        @Override
        public double getAdvanceWidth(int unicode) {
            return 10;
        }
    }

}
