
package science.unlicense.engine.ui.model;

import science.unlicense.common.api.event.EventSource;
import science.unlicense.engine.ui.widget.WCell;
import science.unlicense.engine.ui.widget.Widget;

/**
 * Column model user to present a single column,
 * Used by TableModel and TreeTableModel.
 *
 * @author Johann Sorel
 */
public interface Column extends EventSource {

    /**
     * Column header widget.
     *
     * @return Widget
     */
    Widget getHeader();

    int getBestWidth();

    void setBestWidth(int width);

    ObjectPresenter getPresenter();

    /**
     * Get column value for given index.
     *
     * @param rowObject
     * @return Object
     */
    Object getCellValue(Object rowObject);

    /**
     * Create a cell for given row.
     *
     * @return ObjectPresenter
     */
    WCell createCell(WRow row);

}
