
package science.unlicense.engine.ui.model;

import science.unlicense.common.api.Arrays;

/**
 *
 * @author Johann Sorel
 */
public class ArraySpinnerModel implements SpinnerModel {

    private final Object[] values;

    public ArraySpinnerModel(Object[] values) {
        this.values = values;
    }

    @Override
    public boolean isValid(Object value) {
        return Arrays.contains(values, value);
    }

    @Override
    public Object getDefaultValue() {
        return values[0];
    }

    @Override
    public SpinnerEditor createEditor() {
        return new ArrayLabelEditor();
    }

    @Override
    public boolean hasNextValue(Object currentValue) {
        return !values[values.length - 1].equals(currentValue);
    }

    @Override
    public boolean hasPreviousValue(Object currentValue) {
        return !values[0].equals(currentValue);
    }

    @Override
    public Object nextValue(Object currentValue) {
        final int index = Arrays.getFirstOccurence(values, 0, values.length, currentValue);
        return values[index + 1];
    }

    @Override
    public Object previousValue(Object currentValue) {
        final int index = Arrays.getFirstOccurence(values, 0, values.length, currentValue);
        return values[index - 1];
    }

}
