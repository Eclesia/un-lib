
package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.predicate.Predicates;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.impl.cryptography.hash.MD5;
import science.unlicense.encoding.impl.io.ur.URLUtilities;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.system.ModuleSeeker;

/**
 *
 * @author Johann Sorel
 */
public class PathPresenters {

    private static final Sorter PRIORITY_COMPARATOR = new Sorter() {

        public int sort(Object first, Object second) {
            final float p1 = ((PathPresenter) first).getPriority();
            final float p2 = ((PathPresenter) second).getPriority();
            return Float.compare(p2, p1);
        }
    };

    private PathPresenters(){}

    /**
     * Lists available presenters.
     * @return array of PathPresenter, never null but can be empty.
     */
    public static PathPresenter[] getPresenters(){
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/widget/science.unlicense.engine.ui.component.path.PathPresenter"),
                Predicates.instanceOf(PathPresenter.class));
        final PathPresenter[] formats = new PathPresenter[results.getSize()];
        Collections.copy(results, formats, 0);

        Arrays.sort(formats, PRIORITY_COMPARATOR);

        return formats;
    }


    /**
     * Allow only one image creation at the time.
     */
    public static synchronized Image generateImage(Path path, Extent extent){
        final double size = extent.get(0);
        final PathPresenter[] presenters = getPresenters();
        for (int i=0,n=presenters.length;i<n;i++){
            final PathPresenter pp = (PathPresenter) presenters[i];
            if (pp.canHandle(path)) {
                final Extent.Long ext = new Extent.Long((long) size-10, (long) size-10);
                try {
                    Image img = pp.createImage(path, ext);
                    if (img != null) return img;
                } catch (Exception ex) {
                    //check next path presenter
                }
            }
        }
        return null;
    }

    /**
     * Delete thumbnails folder at :
     * userhome/.thumbnails
     */
    public static void deleteThumbnails() throws IOException{
        final NamedNode userHome = science.unlicense.system.System.get().getProperties()
                    .getSystemTree().search(new Chars("system/user/home"));
        Path path = Paths.resolve((Chars) userHome.getValue());
        path = path.resolve(new Chars(".thumbnails"));
        path.delete();
    }

    public static Path createThumbnailPath(Path candidate) throws IOException{

        final NamedNode userHome = science.unlicense.system.System.get().getProperties()
                    .getSystemTree().search(new Chars("system/user/home"));
        Path path = Paths.resolve((Chars) userHome.getValue());
        path = path.resolve(new Chars(".thumbnails"));
        path = path.resolve(new Chars("normal"));
        path.createContainer();

        final Chars str = candidate.toURI().replaceAll(new Chars("file:"), new Chars("file://"));
        final Chars url = URLUtilities.encode(str);

        final MD5 md5 = new MD5();
        md5.update(url.toBytes());
        final Chars fileName = Int32.encodeHexa(md5.getResultBytes()).toLowerCase().concat(new Chars(".png"));

        return path.resolve(fileName);
    }


}
