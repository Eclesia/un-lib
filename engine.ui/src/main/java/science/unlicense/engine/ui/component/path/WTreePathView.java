

package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.CollectionMessage;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.model.tree.AbstractNode;
import science.unlicense.common.api.model.tree.DefaultNamedNode;
import science.unlicense.common.api.model.tree.DefaultNode;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.ViewNode;
import science.unlicense.common.api.predicate.AbstractPredicate;
import science.unlicense.common.api.predicate.And;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.model.TreeRowModel;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WTree;
import science.unlicense.engine.ui.widget.Widget;

/**
 * Tree view.
 *
 * @author Johann Sorel
 */
public class WTreePathView extends AbstractPathView{

    private static final Predicate ONLY_CONTAINER = new AbstractPredicate() {
        public Boolean evaluate(Object candidate) {
            if (!(candidate instanceof Path)) return true;
            try {
                return ((Path) candidate).isContainer();
            } catch (IOException ex) {
                Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
            }
            return false;
        }
    };
    private static final Predicate ONLY_NOHIDDEN = new AbstractPredicate() {
        public Boolean evaluate(Object candidate) {
            if (!(candidate instanceof Path)) return true;
            final Object res = ((Path) candidate).getPathInfo(Path.INFO_HIDDEN);
            return res==null || !Boolean.TRUE.equals(res);
        }
    };

    private final WTree tree = new WTree(new FolderPresenter());

    private final TreeRowModel viewModel;

    public WTreePathView() {
        super(null,new Chars("Tree"));
        setLayout(new BorderLayout());
        addChild(tree, BorderConstraint.CENTER);

        final Node root = new DefaultNamedNode(new Chars("host"),true);

        final NamedNode fileRoots = science.unlicense.system.System.get().getProperties()
                .getSystemTree().search(new Chars("system/fileRoots"));
        final Iterator ite = fileRoots.getChildren().createIterator();
        while (ite.hasNext()) {
            Path path = (Path) ((NamedNode) ite.next()).getValue();
//            //we want to see inside archives and mount points
//            path = new CrossingPath(path);
//            final ViewNode rootView = new ViewNode(path, Predicate.TRUE, WPathChooser.FILE_SORTER);
//            rootView.setCacheChildren(true);
            root.getChildren().add(new PathNode(path));
        }

        viewModel = new TreeRowModel(root,Predicate.TRUE,false);
        tree.setRowModel(viewModel);
        tree.setVerticalScrollVisibility(WTree.SCROLL_VISIBLE_NEEDED);
        tree.setHorizontalScrollVisibility(WTree.SCROLL_VISIBLE_NEVER);

        //listen to node selection
        tree.getSelection().addEventListener(CollectionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final Sequence sel = tree.getSelection();
                if (!sel.isEmpty()){
                    Object obj = sel.get(0);
                    if (obj instanceof PathNode){
                        setViewRoot( ((PathNode) obj).path);
                    }
                }
            }
        });
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_VIEW_ROOT.equals(name)){
            Path viewRoot = (Path) value;
            //open path
            final TreeRowModel rowModel = (TreeRowModel) tree.getRowModel();
            for (Path p=viewRoot.getParent();p!=null;p=p.getParent()){
                rowModel.setFold(new PathNode(p), true);
            }
            //TODO does not work as expected, because of view nodes ?
            tree.getSelection().replaceAll(new Object[]{new PathNode(viewRoot)});
        }
    }

    private static class PathNode extends AbstractNode{

        private static final Predicate FILTER = new And(new Predicate[]{ONLY_NOHIDDEN,ONLY_CONTAINER});

        private final Path path;
        private Sequence childs;
        private Boolean canHaveChildren;

        public PathNode(Path path) {
            this.path = path;
        }

        @Override
        public Sequence getChildren() {
            if (childs==null){
                final Path[] subs = (Path[]) path.getChildren().toArray(Path.class);
                childs = new ArraySequence();
                for (int i=0;i<subs.length;i++){
                    if (FILTER.evaluate(subs[i])){
                        childs.add(subs[i]);
                    }
                }
                //sort by name and type(folder/file)
                Collections.sort(childs, WPathChooser.FILE_SORTER);
                //convert to PathNode objects
                for (int i=0,n=childs.getSize();i<n;i++){
                    childs.replace(i, new PathNode((Path) childs.get(i)));
                }
            }
            return childs;
        }

        @Override
        public boolean canHaveChildren() {
            if (canHaveChildren==null){
                try {
                    canHaveChildren = path.isContainer();
                } catch (IOException ex) {
                    ex.printStackTrace();
                    canHaveChildren = false;
                }
            }
            return canHaveChildren;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof PathNode){
                return ((PathNode) obj).path.equals(path);
            }
            return false;
        }

        @Override
        public Chars toChars() {
            if (path.getName().isEmpty() && path.getParent()==null){
                return new Chars("/");
            } else {
                return new Chars(path.getName());
            }
        }

    }

    private static final class FolderPresenter implements ObjectPresenter{

        @Override
        public Widget createWidget(Object candidate) {

            final WLeaf leaf = new WSpace(14,14);
            leaf.setInlineStyle(WPathChooser.STYLE_FOLDER);
            leaf.getInlineStyle().setProperties(new Chars("margin:0"));
            leaf.setOverrideExtents(new Extents(14, 14));

            if (candidate instanceof Node){
                candidate = ViewNode.unWrap((Node) candidate);
            }

            Chars text;
            if (candidate instanceof NamedNode){
                text = ((NamedNode) candidate).getName();
            } else if (candidate instanceof Path){
                text =  new Chars(((Path) candidate).getName());
            } else if (candidate instanceof DefaultNode){
                text =  ((DefaultNode) candidate).toChars();
            } else {
                text = new Chars(String.valueOf(candidate));
            }
            if (text.getCharLength()==0){
                text = new Chars(" ");
            }

            final WLabel label = new WLabel(text, leaf);
            label.setVerticalAlignment(WLabel.VALIGN_CENTER);
            label.setHorizontalAlignment(WLabel.HALIGN_LEFT);
            label.setGraphicPlacement(WLabel.GRAPHIC_LEFT);
            return label;
        }

    }

}
