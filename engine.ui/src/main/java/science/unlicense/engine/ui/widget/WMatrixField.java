
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.style.StyleDocument;
import static science.unlicense.engine.ui.widget.AbstractControlWidget.PROPERTY_VALUE;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.MatrixRW;

/**
 *
 * @author Johann Sorel
 */
public class WMatrixField extends WContainer implements WValueWidget {

    public static final Chars PROPERTY_MATRIX = Chars.constant("Matrix");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("matrixfield")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WMatrixField() {
        super(new GridLayout(1, -1));
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public final Matrix getMatrix() {
        return (Matrix) getPropertyValue(PROPERTY_MATRIX);
    }

    public final void setMatrix(Matrix matrix) {
        setPropertyValue(PROPERTY_MATRIX, matrix);
    }

    public final Property varMatrix() {
        return getProperty(PROPERTY_MATRIX);
    }

    @Override
    public final Chars getValuePropertyName() {
        return PROPERTY_MATRIX;
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        if (PROPERTY_MATRIX.equals(name)) {
            getChildren().removeAll();
            if (value == null) return;

            final Matrix matrix = (Matrix) value;
            final int nbCol = matrix.getNbCol();
            final int nbRow = matrix.getNbRow();
            setLayout(new GridLayout(nbRow, nbCol));

            for (int c=0; c<nbCol; c++) {
                for (int r=0; r<nbCol; r++) {
                    final WCellEditor edit = new WCellEditor(matrix, r, c);
                    addChild(edit, null);
                }
            }
        }
        super.valueChanged(name, oldValue, value);
    }

    private final class WCellEditor extends WSpinner{

        public WCellEditor(final Matrix matrix, final int row, final int col) {
            super(new NumberSpinnerModel(Double.class,0,null,null,1));
            WMatrixField.WCellEditor.this.setPropertyValue(StyleDocument.PROPERTY_STYLE_CLASS, Chars.EMPTY);
            setValue(matrix.get(row, col));
            if (matrix instanceof MatrixRW) {
                varValue().addListener(new EventListener() {
                    public void receiveEvent(Event event) {
                        try {
                            final double val = ((Number) getValue()).doubleValue();
                            ((MatrixRW) matrix).set(row, col, val);
                            WMatrixField.this.getEventManager().sendPropertyEvent(WMatrixField.this, PROPERTY_VALUE, matrix, matrix);
                        } catch(RuntimeException ex) {
                            Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                        }
                    }
                });
            }
        }

    }

}
