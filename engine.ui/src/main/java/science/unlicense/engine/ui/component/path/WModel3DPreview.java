

package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WScene3D;
import science.unlicense.model3d.api.Model3Ds;

/**
 *
 * @author Johann Sorel
 */
public class WModel3DPreview extends WContainer{

    private final Path path;
    private final WScene wscene = new WScene();
    private Model3DPathPresenter.Stage stage;
    private boolean needUpdate = false;

    public WModel3DPreview(final Path path) {
        super(new BorderLayout());
        this.path = path;

        final WButton wload = new WButton(new Chars("Load model"), null,new EventListener() {
            public void receiveEvent(Event event) {
                new Thread(){
                    public void run() {
                        Store store;
                        try {
                            store = Model3Ds.open(path);
                            stage = new Model3DPathPresenter.Stage(store);
                            wscene.setScene(stage.scene);
                            wscene.setCamera(stage.camera);
                            stage.controller.configureDefault();
                            removeChildren();
                            addChild(wscene, BorderConstraint.CENTER);

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }.start();
            }
        });

        final WContainer absContainer = new WContainer(new AbsoluteLayout());
        absContainer.getChildren().add(wload);
        addChild(absContainer, BorderConstraint.CENTER);

        wscene.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                wscene.requestFocus();
            }
        });

        wscene.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                if (stage == null) return;
                stage.controller.receiveEvent(event);
                needUpdate = true;
            }
        });
        wscene.addEventListener(KeyMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                if (stage == null) return;
                stage.controller.receiveEvent(event);
                needUpdate = true;
            }
        });
    }

    private class WScene extends WScene3D {

        @Override
        protected boolean preUpdate(long time) {
            boolean n = needUpdate;
            if (n) {
                stage.controller.update(null);
            }
            needUpdate = false;
            return n;
        }

    }


}
