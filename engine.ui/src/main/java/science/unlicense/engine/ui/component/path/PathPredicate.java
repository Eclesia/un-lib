

package science.unlicense.engine.ui.component.path;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.predicate.Predicate;

/**
 *
 * @author Johann Sorel
 */
public interface PathPredicate extends Predicate{

    /**
     *
     * @return Chars
     */
    Chars getText();

}
