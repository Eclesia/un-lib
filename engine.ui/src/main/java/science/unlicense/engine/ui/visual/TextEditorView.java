
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharIterator;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.font.Font;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.model.TextModel;
import science.unlicense.engine.ui.style.FontStyle;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WTextEditor;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.number.Int32Range;

/**
 *
 * @author Johann Sorel
 */
public class TextEditorView extends WidgetView implements TextGesture.TextView {

    private final TextModel model;
    private final TextGesture textControl;

    private static final Chars A = Chars.constant("A");

    public TextEditorView(final WTextEditor widget) {
        super(widget);

        model = widget.getTextModel();
        textControl = new TextGesture(widget.getTextModel(), true);
        textControl.setView(this);

        widget.addEventListener(new PropertyPredicate(Widget.PROPERTY_FOCUSED), new EventListener() {
            public void receiveEvent(Event event) {
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                if (Boolean.FALSE.equals(pe.getNewValue())){
                    //textfield lost focus
                    model.setCaret(-1);
                } else {
                    //start edition
                    CharArray text = widget.getText();
                    model.setCaret(text==null ? 0 : text.getCharLength());
                }
            }
        });
        model.addEventListener(Predicate.TRUE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                widget.setDirty();
            }
        });
    }

    public WTextEditor getWidget() {
        return (WTextEditor) super.getWidget();
    }

    public void getExtents(Extents buffer, Extent constraint) {

        final FontStyle font = WidgetStyles.readFontStyle(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_FONT, widget);

        final Extent extent = new Extent.Double(0,0);

        //get text size
        final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
        final FontMetadata meta = font!=null ? fontStore.getFont(font.getFont()).getMetaData() : null;

        CharArray text = getWidget().getEditedText();
        if (text == null || text.isEmpty()) {
            text = getWidget().getPreviewText();
        }
        if (text == null || text.isEmpty()) {
            text = new Chars(" ");
        }

        if (meta != null) {
            CharArray[] split = text.split('\n');
            for (CharArray c : split){
                final BBox bbox;
                if (c.isEmpty()){
                    bbox = meta.getCharsBox(A);
                } else {
                    bbox = meta.getCharsBox(c);
                }
                extent.set(0, Maths.max(extent.get(0),bbox.getSpan(0)));
                extent.set(1, extent.get(1) + bbox.getSpan(1));
            }
            extent.set(1, extent.get(1) - meta.getDescent());
        }

        buffer.setAll(extent);
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }

    public void renderSelf(Painter2D painter, BBox innerBBox) {

        final FontStyle fontStyle = WidgetStyles.readFontStyle(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_FONT, widget);

        int caretPosition = model.getCaret();
        final Int32Range selection = model.getSelection();

        //render the text
        boolean previewText = false;
        CharArray text = getWidget().getEditedText();
        if (text == null || text.isEmpty()) {
            text = getWidget().getPreviewText();
            previewText = true;
        }

        if (text!=null && !text.isEmpty() && fontStyle != null) {
            final FontChoice fontChoice = fontStyle.getFont();
            if (fontChoice != null) {
                final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
                painter.setFont(fontChoice);
                final Font font = fontStore.getFont(fontChoice);
                final FontMetadata meta = font.getMetaData();

                final CharArray[] split = text.split('\n');
                float yoffset = (float) innerBBox.getMin(1);
                for (CharArray line : split) {
                    //calculate appropriate text position
                    final BBox bbox = meta.getCharsBox(line);
                    final float xoffset = (float) innerBBox.getMin(0)+1;
                    if (bbox.getSpan(1) <= 0) {
                        yoffset += (float) meta.getCharsBox(A).getSpan(1);
                    } else {
                        yoffset += (float) (float) bbox.getSpan(1);
                    }

                    if (previewText) {
                        painter.setAlphaBlending(AlphaBlending.create(AlphaBlending.SRC_OVER, 0.5f));
                    }
                    if (fontStyle.getFillPaint() != null) {
                        painter.setPaint(fontStyle.getFillPaint());
                        painter.fill(line, xoffset, yoffset);
                    }
                    if (fontStyle.getBrush() != null && fontStyle.getBrushPaint() != null) {
                        painter.setBrush(fontStyle.getBrush());
                        painter.setPaint(fontStyle.getBrushPaint());
                        painter.stroke(line, xoffset, yoffset);
                    }
                    if (previewText) {
                        painter.setAlphaBlending(AlphaBlending.DEFAULT);
                    }

                    //draw the caret
                    if (fontStyle.getFillPaint() != null && caretPosition >= 0 && caretPosition <= line.getCharLength()) {
                        final double ascent = meta.getAscent();
                        final double offset = xoffset + Font.calculateOffset(font, line, caretPosition);
                        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
                        painter.setPaint(fontStyle.getFillPaint());
                        final PlanarGeometry geom = new DefaultLine(offset+1, yoffset+1, offset+1, yoffset-ascent-1);
                        painter.stroke(geom);
                    }
                    caretPosition -= line.getCharLength()+1;
                }

            }
        }
    }

    public void receiveEvent(Event event) {
        if (!widget.isStackEnable()) return;

        final EventMessage message = event.getMessage();
        if (message instanceof KeyMessage && widget.isFocused()){
            textControl.update(event);
        } else if (message instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) message;
            if (me.getType() == MouseMessage.TYPE_TYPED){
                if (!widget.isFocused()) widget.requestFocus();
            } else if (widget.isFocused()) {
                textControl.update(event);
            }
        }
    }

    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        final Chars name = event.getPropertyName();
        if (  WTextEditor.PROPERTY_TEXT.equals(name)
          || WTextEditor.PROPERTY_LANGUAGE.equals(name)
          || WTextEditor.PROPERTY_EDITED_TEXT.equals(name)
          || WTextEditor.PROPERTY_PREVIEW_TEXT.equals(name)){
            widget.updateExtents();
            widget.setDirty();
        }
    }


    @Override
    public int getCaretPosition(double mx, double my) {

        final FontStyle fontStyle = WidgetStyles.readFontStyle(widget.getEffectiveStyleDoc(), StyleDocument.STYLE_PROP_FONT, widget);
        final BBox innerBBox = widget.getInnerExtent();
        final CharArray edited = getWidget().getEditedText();

        if (edited!=null && !edited.isEmpty() && fontStyle != null){
            final FontChoice font = fontStyle.getFont();
            if (font!=null){
                final FontStore fontStore = widget.getFrame().getPainter().getFontStore();
                final FontMetadata meta = fontStore.getFont(font).getMetaData();

                final CharArray[] split = edited.split('\n');
                float yoffset = (float) innerBBox.getMin(1);
                int caretPosition = 0;
                caretPositionSearch:
                for (CharArray line : split){
                    final BBox bbox = meta.getCharsBox(line);
                    final float xoffset = (float) innerBBox.getMin(0)+1;
                    final float lineHeight;
                    if (bbox.getSpan(1) <= 0){
                        lineHeight = (float) meta.getCharsBox(A).getSpan(1);
                    } else {
                        lineHeight = (float) (float) bbox.getSpan(1);
                    }
                    yoffset += lineHeight;
                    if (yoffset>my){
                        //find caret x offset
                        final double spacing = meta.getAdvanceWidth(' ');
                        final CharIterator ite = line.createIterator();

                        double offset = 0.0;
                        int caretChar = 0;
                        while (ite.hasNext()){
                            final int c = ite.nextToUnicode();
                            final double s;
                            if (c == ' '){
                                //TODO handle control caracters
                                s = spacing;
                            } else {
                                s = meta.getAdvanceWidth(c);
                            }
                            if (offset+xoffset+s >= mx){
                                caretPosition += caretChar;
                                return caretPosition;
                            }
                            offset += s;
                            caretChar++;
                        }

                        //at this point we know the mouse click was after the end of the line
                        //we place the caret at the last character position
                        caretPosition += caretChar;
                        return caretPosition;
                    }

                    caretPosition += line.getCharLength()+1;
                }

            }
        }

        return -1;
    }

}
