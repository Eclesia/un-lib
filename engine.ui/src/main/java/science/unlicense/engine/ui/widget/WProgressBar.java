
package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Property;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.visual.ProgressBarView;

/**
 * Widget progress bar.
 *
 * @author Johann Sorel
 */
public class WProgressBar extends WLeaf {

    /**
     * Property for the widget text.
     */
    public static final Chars PROPERTY_TEXT = Chars.constant("text");
    /**
     * Property for the progress bar undetermined state.
     */
    public static final Chars PROPERTY_UNDETERMINED = Chars.constant("undetermined");
    /**
     * Property for the progress bar progression state.
     */
    public static final Chars PROPERTY_PROGRESS = Chars.constant("progress");

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WLeaf.CLASS_DEFAULTS);
        defs.add(PROPERTY_PROGRESS, 0.0);
        defs.add(PROPERTY_UNDETERMINED, Boolean.FALSE);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("progressbar")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WProgressBar() {
        this(false);
    }

    public WProgressBar(boolean undetermined) {
        setUndetermined(undetermined);
        setView(new ProgressBarView(this));
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    /**
     * Get progress text.
     * @return CharArray, can be null
     */
    public final CharArray getText() {
        return (CharArray) getPropertyValue(PROPERTY_TEXT);
    }

    /**
     * Set progress bar text.
     *
     * @param text can be null
     */
    public final void setText(CharArray text) {
        setPropertyValue(PROPERTY_TEXT,text);
    }

    /**
     * Get text variable.
     * @return Variable.
     */
    public final Property varText(){
        return getProperty(PROPERTY_TEXT);
    }

    /**
     * Get progress value.
     * @return double, between 0 and 1
     */
    public final double getProgress() {
        return (Double) getPropertyValue(PROPERTY_PROGRESS);
    }

    public final void setProgress(double progress){
        setPropertyValue(PROPERTY_PROGRESS,progress);
    }

    /**
     * Get text variable.
     * @return Variable.
     */
    public final Property varProgress(){
        return getProperty(PROPERTY_PROGRESS);
    }

    /**
     * Set undeterminated state.
     * @param undetermined
     */
    public final void setUndetermined(boolean undetermined) {
        setPropertyValue(PROPERTY_UNDETERMINED,undetermined);
    }

    /**
     * Get undeterminated state.
     * @return boolean true if undeterminated
     */
    public final boolean isUndetermined() {
        return (Boolean) getPropertyValue(PROPERTY_UNDETERMINED);
    }

    /**
     * Get text variable.
     * @return Variable.
     */
    public final Property varUndetermined(){
        return getProperty(PROPERTY_UNDETERMINED);
    }

}
