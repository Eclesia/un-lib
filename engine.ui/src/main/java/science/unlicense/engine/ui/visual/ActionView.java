
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.engine.ui.widget.WAction;

/**
 *
 * @author Johann Sorel
 */
public class ActionView extends ContainerView{

    private boolean possiblePress = false;

    public ActionView(WAction labeled) {
        super(labeled);
    }

    @Override
    public WAction getWidget() {
        return (WAction) super.getWidget();
    }

    @Override
    public void receiveEvent(Event event) {
        final EventMessage message = event.getMessage();
        if (message instanceof MouseMessage && !message.isConsumed() && getWidget().isStackEnable()){
            final MouseMessage me = (MouseMessage) message;
            final int type = me.getType();

            if (MouseMessage.TYPE_PRESS == type && me.getButton() == MouseMessage.BUTTON_1){
                possiblePress = true;
                getWidget().requestFocus();
                me.consume();
            } else if (MouseMessage.TYPE_RELEASE == type && me.getButton() == MouseMessage.BUTTON_1 && possiblePress){
                me.consume();
                possiblePress = false;
                //fire action event
                getWidget().doClick();
            } else if (possiblePress) {
                if (MouseMessage.TYPE_EXIT == type) {
                    possiblePress = false;
                } else {
                    //consume any move event whil pressing button
                    me.consume();
                }
            }
        }
    }

}
