
package science.unlicense.engine.ui.model;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.AbstractProperty;
import science.unlicense.common.api.event.Property;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.number.Int32Range;

/**
 *
 * @author Johann Sorel
 */
public class TextModel extends AbstractEventSource {

    public static final Chars PROPERTY_TEXT = Chars.constant("Text");
    public static final Chars PROPERTY_CARET = Chars.constant("Caret");
    public static final Chars PROPERTY_SELECTION = Chars.constant("Selection");

    private CharArray text;
    private int caretPosition = 0;
    private Int32Range selection = new Int32Range(0, 0, true, false);

    public TextModel() {
    }

    /**
     * Set text.
     * @param value can be null
     */
    public void setText(CharArray value) {
        final CharArray oldValue = this.text;
        if (!CObjects.equals(oldValue, value)){
            this.text = value;

            //check selection is not out of range
            final int sx = Maths.min(text.getCharLength(), selection.getMinimum().toInteger());
            final int ex = Maths.min(text.getCharLength(), selection.getMaximum().toInteger());
            setSelection(new Int32Range(sx, ex, true, false));

            sendPropertyEvent(PROPERTY_TEXT, oldValue, value);
        }
    }

    /**
     * Get text.
     * @return CharArray, can be null
     */
    public CharArray getText() {
        return text;
    }

    /**
     * Get selected text.
     *
     * @return Selected text
     */
    public CharArray getSelectedText() {
        if (selection == null || selection.getMaximum().toInteger() <= 0) return Chars.EMPTY;
        final int ss = selection.getMinimum().toInteger();
        final int se = selection.getMinimum().toInteger();
        return getText().truncate(ss, se);
    }

    /**
     * Get text variable.
     * @return Variable.
     */
    public Property varText(){
        return new AbstractProperty(this, PROPERTY_TEXT, CharArray.class, true, true) {
            @Override
            public Object getValue() throws RuntimeException {
                return getText();
            }
            @Override
            public void setValue(Object value) throws RuntimeException {
                setText((CharArray) value);
            }
        };
    }

    /**
     * Set caret position.
     * @param caretPosition
     */
    public void setCaret(int caretPosition) {
        final int oldValue = this.caretPosition;
        if (!CObjects.equals(oldValue, caretPosition)){
            this.caretPosition = caretPosition;
            sendPropertyEvent(PROPERTY_CARET, oldValue, caretPosition);
        }
    }

    /**
     * Get caret position.
     * @return int position
     */
    public int getCaret() {
        return caretPosition;
    }

    /**
     * Get caret variable.
     * @return Variable.
     */
    public Property varCaret(){
        return new AbstractProperty(this, PROPERTY_CARET, Integer.class, true, true) {
            @Override
            public Object getValue() throws RuntimeException {
                return getCaret();
            }
            @Override
            public void setValue(Object value) throws RuntimeException {
                setCaret((Integer) value);
            }
        };
    }

    /**
     * Set selection.
     * @param selection range
     */
    public void setSelection(Int32Range selection) {
        final Int32Range oldValue = this.selection;
        if (!CObjects.equals(oldValue, selection)) {
            this.selection = selection;
            sendPropertyEvent(PROPERTY_SELECTION, oldValue, selection);
        }
    }

    /**
     * Get selection.
     * @return selection range
     */
    public Int32Range getSelection() {
        return selection;
    }

    /**
     * Get selection.
     * @return Variable.
     */
    public Property varSelection(){
        return new AbstractProperty(this, PROPERTY_SELECTION, Int32Range.class, true, true) {
            @Override
            public Object getValue() throws RuntimeException {
                return getSelection();
            }
            @Override
            public void setValue(Object value) throws RuntimeException {
                setSelection((Int32Range) value);
            }
        };
    }

}
