
package science.unlicense.engine.ui.visual;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PainterState;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public final class ViewRenderLoop {

    private ViewRenderLoop(){}

    /**
     *
     * @param widget widget to render
     * @param painter painter
     * @param parentDirty dirty box
     */
    public static void render(final Widget widget, final Painter2D painter, final BBox parentDirty) {
        if (!widget.isVisible()) return;

        final View view = widget.getView();
        if (view == null) return;

        //check intersection
        final Similarity trs = widget.getNodeTransform();

        final BBox childDirty = Geometries.transform(parentDirty, trs.inverse(), null);

        //test intersection
        final BBox visualDimension = view.calculateVisualExtent(null);
        if (!childDirty.intersects(visualDimension,false)) return;

        final PainterState stateOld = painter.saveState();

        //reduce dirty area to widget bounds
        childDirty.localIntersect(visualDimension);

        painter.setTransform(new Affine2(painter.getTransform().multiply(trs)));

        final PlanarGeometry clip = painter.getClip();
        final BBox oldClip = (clip == null) ? null : clip.getBoundingBox();
        final BBox newClip = Geometries.transform(childDirty, painter.getTransform(), null);
        if (oldClip != null) newClip.localIntersect(oldClip);

        if (!newClip.isValid()) {
            return;
        }

        painter.setClip(new Rectangle(newClip));

        final PainterState stateThis = painter.saveState();

        //render back of the widget
        view.renderBack(painter);

        if (widget instanceof WContainer) {
            //render children
            final WContainer container = (WContainer) widget;
            final Iterator ite = container.getChildren().createIterator();

            while (ite.hasNext()) {
                painter.restoreState(stateThis);
                final Widget child = (Widget) ite.next();
                render(child, painter, childDirty);
            }
        }

        //render front of the widget
        painter.restoreState(stateOld);
        view.renderFront(painter);
        view.renderEffects(painter);

        painter.restoreState(stateOld);
    }

}
