package science.unlicense.engine.ui.widget;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.engine.ui.ievent.ActionExecutable;
import science.unlicense.engine.ui.style.StyleDocument;

/**
 * Button type widget.
 *
 * @author Johann Sorel
 */
public class WButton extends WAction {

    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WAction.CLASS_DEFAULTS);
        defs.add(StyleDocument.PROPERTY_STYLE_CLASS, Chars.constant(new Chars("button")));
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    public WButton() {
        this(null,(Widget) null);
    }

    public WButton(CharArray text) {
        this(text,(Widget) null);
    }

    public WButton(CharArray text, Widget graphic) {
        this(text,graphic,null);
    }

    public WButton(CharArray text, Widget graphic, EventListener listener) {
        super(text,graphic,listener);
    }

    public WButton(final ActionExecutable exec){
        super(exec);
    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

}
