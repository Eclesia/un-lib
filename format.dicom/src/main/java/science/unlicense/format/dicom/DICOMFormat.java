
package science.unlicense.format.dicom;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * @author Johann Sorel
 */
public class DICOMFormat extends AbstractImageFormat {

    public static final DICOMFormat INSTANCE = new DICOMFormat();

    private DICOMFormat() {
        super(new Chars("dicom"));
        shortName = new Chars("DICOM");
        longName = new Chars("Digital Imaging and Communications in Medicine");
        mimeTypes.add(new Chars("application/dicom"));
        extensions.add(new Chars("dcm"));
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new DICOMStore(this, source);
    }

}
