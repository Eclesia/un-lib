
package science.unlicense.format.grib.grib1;

import science.unlicense.format.grib.grib1.Grib1Format;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class Grib1FormatTest {

    @Test
    public void testRegistered() throws Exception{
        final ImageFormat[] formats = Images.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof Grib1Format){
                //ok, found it
                return;
            }
        }

        Assert.fail("Image format not found.");
    }

}
