
package science.unlicense.format.grib.grib2;

import science.unlicense.format.grib.grib2.Grib2Format;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class Grib2FormatTest {

    @Test
    public void testRegistered() throws Exception{
        final ImageFormat[] formats = Images.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof Grib2Format){
                //ok, found it
                return;
            }
        }

        Assert.fail("Image format not found.");
    }

}
