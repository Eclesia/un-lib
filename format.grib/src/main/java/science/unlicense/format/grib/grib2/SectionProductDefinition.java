
package science.unlicense.format.grib.grib2;

/**
 *
 * 1-4  Length of the section in octets (nn)
 * 5    Number of the section (4)
 * 6-7  Number of coordinate values after template (See note 1 below)
 * 8-9  Product definition template number (See Table 4.0)
 * 10-xx    Product definition template (See product template 4.X, where X is the number given in octets 8-9)
 * [xx+1]-nn    Optional list of coordinate values (See notes 2 and 3 below)
 *
 * @author Johann Sorel
 */
public class SectionProductDefinition implements Section {

    public short nbCoordinateValues;
    public short templateNumber;
    public byte[] template;
    public byte[] optionalCoordinateList;

    @Override
    public String toString() {
        return "SectionProductDefinition{" + "nbCoordinateValues=" + nbCoordinateValues + ", templateNumber=" + templateNumber + '}';
    }

}
