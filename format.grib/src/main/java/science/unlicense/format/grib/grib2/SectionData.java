
package science.unlicense.format.grib.grib2;

/**
 *
 * 1-4  Length of the section in octets (nn)
 * 5    Number of the section (7)
 * 6-nn Data in a format described by data Template 7.X, where X is the data representation template number
 *      given in octets 10-11 of Section 5.
 *
 * @author Johann Sorel
 */
public class SectionData implements Section {

    public byte[] data;

    @Override
    public String toString() {
        return "SectionData{" + '}';
    }

}
