
package science.unlicense.format.grib.grib2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.ImageResource;
import science.unlicense.image.api.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class Grib2Store extends AbstractStore implements ImageResource {

    public Grib2Store(ImageFormat format, Object input) {
        super(format, input);
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public ImageReader createReader() throws IOException {
        throw new IOException(getInput(), "Not supported");
    }

    @Override
    public ImageWriter createWriter() throws IOException {
        throw new IOException(getInput(), "Not supported");
    }

    @Override
    public Chars getId() {
        return null;
    }

}
