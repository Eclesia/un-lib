
package science.unlicense.format.grib.grib1;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * @author Johann Sorel
 */
public class Grib1Format extends AbstractImageFormat {

    public static final Grib1Format INSTANCE = new Grib1Format();

    private Grib1Format() {
        super(new Chars("grib1"));
        shortName = new Chars("GRIdded Binary 1");
        longName = new Chars("General Regularly-distributed Information in Binary form 1");
        extensions.add(new Chars("grd"));
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new Grib1Store(this, source);
    }

}
