package science.unlicense.format.grib.grib2;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.EOSException;
import science.unlicense.encoding.api.io.IOException;

/**
 * Iterate over grib2 sections.
 *
 * @author Johann Sorel
 */
public class Grib2Iterator {

    private DataInputStream ds;
    private Section next = null;

    public void Grib2Reader() {
    }

    public void setInput(ByteInputStream input) {
        this.ds = new DataInputStream(input);
    }

    public Section next() throws IOException {
        findNext();
        if (next == null) {
            throw new IOException(ds, "No more section to read");
        }

        final Section s = next;
        next = null;
        return s;
    }

    public boolean hasNext() throws IOException {
        findNext();
        return next != null;
    }

    private void findNext() throws IOException {
        if (next != null) {
            return;
        }

        final byte[] base = new byte[4];
        try {
            ds.readFully(base);
        } catch (EOSException ex) {
            //nothing left
            return;
        }

        final String str = new String(base);
        if ("GRIB".equals(str)) {
            //section 0 : Indicator Section
            next = readSection0();
            return;
        } else if ("7777".equals(str)) {
            //section 8 : End Section
            next = new SectionEnd();
            return;
        }

        //other section types
        final int length = (base[3] & 0xFF) | ((base[2] & 0xFF) << 8) | ((base[1] & 0xFF) << 16) | ((base[0] & 0xFF) << 24);
        final int type = ds.read();
        if (type == 1) {
            next = readSection1(length);
        } else if (type == 2) {
            next = readSection2(length);
        } else if (type == 3) {
            next = readSection3(length);
        } else if (type == 4) {
            next = readSection4(length);
        } else if (type == 5) {
            next = readSection5(length);
        } else if (type == 6) {
            next = readSection6(length);
        } else if (type == 7) {
            next = readSection7(length);
        } else {
            throw new IOException(ds, "Section type " + type + " not supported yet.");
        }

    }

    public Section readSection0() throws IOException {
        //1-4    'GRIB' (Coded according to the International Alphabet Number 5)
        //5-6    Reserved
        //7    Discipline (From Table 0.0)
        //8    Edition number - 2 for GRIB2
        //9-16    Total length of GRIB message in octets (All sections);

        final SectionIndicator section = new SectionIndicator();
        section.reserved = ds.readShort();
        section.discipline = ds.read();
        section.edition = ds.read();
        section.length = ds.readLong();

        return section;
    }

    private Section readSection1(int length) throws IOException {
        //1-4    Length of the section in octets (21 or N)
        //5    Number of the section (1)
        //6-7    Identification of originating/generating center (See Table 0) (See note 4)
        //8-9    Identification of originating/generating subcenter (See Table C)
        //10    GRIB master tables version number (currently 2) (See Table 1.0) (See note 1)
        //11    Version number of GRIB local tables used to augment Master Tables (see Table 1.1)
        //12    Significance of reference time (See Table 1.2)
        //13-14    Year (4 digits)
        //15    Month
        //16    Day
        //17    Hour
        //18    Minute
        //19    Second
        //20    Production Status of Processed data in the GRIB message (See Table 1.3)
        //21    Type of processed data in this GRIB message (See Table 1.4)
        //22-N    Reserved

        final SectionIdentification section = new SectionIdentification();
        section.center = ds.readShort();
        section.subcenter = ds.readShort();
        section.masterTableVersion = ds.read();
        section.localTableVersion = ds.read();
        section.timeSignificance = ds.read();
        section.year = ds.readShort();
        section.month = ds.read();
        section.day = ds.read();
        section.hour = ds.read();
        section.minute = ds.read();
        section.second = ds.read();
        section.status = ds.read();
        section.type = ds.read();
        section.reserved = new byte[length - 21];
        ds.readFully(section.reserved);

        return section;
    }

    private Section readSection2(int length) throws IOException {
        //1-4    Length of the section in octets (N)
        //5    Number of the section (2)
        //6-N    Local Use

        final SectionLocalUse section = new SectionLocalUse();
        section.localUse = new byte[length - 5];
        ds.readFully(section.localUse);

        return section;
    }

    private Section readSection3(int length) throws IOException {
        //1-4    Length of the section in octets (nn)
        //5    Number of the section (3)
        //6    Source of grid definition (See Table 3.0) (See note 1 below)
        //7-10    Number of data points
        //11    Number of octets for optional list of numbers defining number of points (See note 2 below)
        //12    Interpolation of list of numbers defining number of points (See Table 3.11)
        //13-14    Grid definition template number (= N) (See Table 3.1)
        //15-xx    Grid definition template (See Template 3.N, where N is the grid definition template
        //      number given in octets 13-14)
        //[xx+1]-nn    Optional list of numbers defining number of points (See notes 2, 3, and 4 below)

        final SectionGridDefinition section = new SectionGridDefinition();
        section.source = ds.read();
        section.nbDataPoint = ds.readInt();
        section.nbByteOptionalPointList = ds.read();
        section.interpolation = ds.read();
        section.templateNumber = ds.readShort();
        section.template = new byte[length - 14]; //TODO must parse all templates
        //section.optionalPointList = new byte[section.nbByteOptionalPointList];
        ds.readFully(section.template);
        //ds.readFully(section.optionalPointList);

        return section;
    }

    private Section readSection4(int length) throws IOException {
        //1-4    Length of the section in octets (nn)
        //5    Number of the section (4)
        //6-7    Number of coordinate values after template (See note 1 below)
        //8-9    Product definition template number (See Table 4.0)
        //10-xx    Product definition template (See product template 4.X, where X is
        //      the number given in octets 8-9)
        //[xx+1]-nn    Optional list of coordinate values (See notes 2 and 3 below)

        final SectionProductDefinition section = new SectionProductDefinition();
        section.nbCoordinateValues = ds.readShort();
        section.templateNumber = ds.readShort();
        section.template = new byte[length - 9]; //TODO must parse all templates
        //section.optionalCoordinateList = new byte[section.nbByteOptionalPointList];
        ds.readFully(section.template);
        //ds.readFully(section.optionalPointList);

        return section;
    }

    private Section readSection5(int length) throws IOException {
        //1-4    Length of the section in octets (nn)
        //5    Number of the section (5)
        //6-9    Number of data points where one or more values are specified in Section 7 when a bit map is present,
        //      total number of data points when a bit map is absent.
        //10-11    Data representation template number (See Table 5.0)
        //12-nn    Data representation template (See Template 5.X, where X is the number given in octets 10-11)

        final SectionDataRepresentation section = new SectionDataRepresentation();
        section.nbDataPoint = ds.readInt();
        section.templateNumber = ds.readShort();
        section.template = new byte[length - 11]; //TODO must parse all templates
        ds.readFully(section.template);

        return section;
    }

    private Section readSection6(int length) throws IOException {
        //1-4    Length of the section in octets (nn)
        //5    Number of the section (6)
        //6    Bit-map indicator (See Table 6.0) (See note 1 below)
        //7-nn    Bit-map

        final SectionBitMap section = new SectionBitMap();
        section.bitmapIndicator = ds.read();
        section.bitmap = new byte[length - 6];
        ds.readFully(section.bitmap);

        return section;
    }

    private Section readSection7(int length) throws IOException {
        //1-4    Length of the section in octets (nn)
        //5    Number of the section (7)
        //6-nn    Data in a format described by data Template 7.X, where X is the data representation template number
        //      given in octets 10-11 of Section 5.

        final SectionData section = new SectionData();
        section.data = new byte[length - 5];
        ds.readFully(section.data);

        return section;
    }
}
