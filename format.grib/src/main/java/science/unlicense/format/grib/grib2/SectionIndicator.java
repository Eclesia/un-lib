
package science.unlicense.format.grib.grib2;

/**
 *
 * 1-4  'GRIB' (Coded according to the International Alphabet Number 5)
 * 5-6  Reserved
 * 7    Discipline (From Table 0.0)
 * 8    Edition number - 2 for GRIB2
 * 9-16 Total length of GRIB message in octets (All sections);
 *
 * @author Johann Sorel
 */
public class SectionIndicator implements Section {

    public short reserved;
    public int discipline;
    public int edition;
    public long length;

    @Override
    public String toString() {
        return "SectionIndicator{" + "reserved=" + reserved + ", discipline=" + discipline + ", edition=" + edition + ", length=" + length + '}';
    }

}
