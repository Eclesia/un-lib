
package science.unlicense.format.grib.grib2;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;

/**
 * GRIB 2 Image Reader
 *
 * @author Johann Sorel
 */
public class Grib2Reader extends AbstractImageReader{

    @Override
    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
