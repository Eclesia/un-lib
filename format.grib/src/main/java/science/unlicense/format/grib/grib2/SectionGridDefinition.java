
package science.unlicense.format.grib.grib2;

/**
 *
 * 1-4  Length of the section in octets (nn)
 * 5    Number of the section (3)
 * 6    Source of grid definition (See Table 3.0) (See note 1 below)
 * 7-10 Number of data points
 * 11   Number of octets for optional list of numbers defining number of points (See note 2 below)
 * 12   Interpolation of list of numbers defining number of points (See Table 3.11)
 * 13-14    Grid definition template number (= N) (See Table 3.1)
 * 15-xx    Grid definition template (See Template 3.N, where N is the grid definition template number given in octets 13-14)
 * [xx+1]-nn    Optional list of numbers defining number of points (See notes 2, 3, and 4 below)
 *
 * @author Johann Sorel
 */
public class SectionGridDefinition implements Section {

    public int source;
    public int nbDataPoint;
    public int nbByteOptionalPointList;
    public int interpolation;
    public short templateNumber;
    public byte[] template;
    public byte[] optionalPointList;

    @Override
    public String toString() {
        return "SectionGridDefinition{" + "source=" + source + ", nbDataPoint=" + nbDataPoint + ", nbByteOptionalPointList=" + nbByteOptionalPointList + ", interpolation=" + interpolation + ", templateNumber=" + templateNumber + '}';
    }

}
