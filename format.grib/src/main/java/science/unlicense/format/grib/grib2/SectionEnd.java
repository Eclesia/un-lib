
package science.unlicense.format.grib.grib2;

/**
 *
 * @author Johann Sorel
 */
public class SectionEnd implements Section {

    @Override
    public String toString() {
        return "SectionEnd{" + '}';
    }

}
