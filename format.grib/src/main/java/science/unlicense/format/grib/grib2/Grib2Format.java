
package science.unlicense.format.grib.grib2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * @author Johann Sorel
 */
public class Grib2Format extends AbstractImageFormat {

    public static final Grib2Format INSTANCE = new Grib2Format();

    private Grib2Format() {
        super(new Chars("grib2"));
        shortName = new Chars("GRIdded Binary 2");
        longName = new Chars("General Regularly-distributed Information in Binary form 2");
        extensions.add(new Chars("grd"));
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new Grib2Store(this, source);
    }

}
