
package science.unlicense.format.csv;

import science.unlicense.format.csv.CSVDocument;
import science.unlicense.format.csv.CSVReader;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 * CSV reader test.
 *
 * @author Johann Sorel
 */
public class CSVReaderTest {

    /**
     * Read csv with header as first line.
     *
     * @throws IOException
     */
    @Test
    public void testReadDefault() throws IOException {

        Chars csv = new Chars("a;b;c;d;e\n1;2;3;4;5");

        CSVReader reader = new CSVReader();
        reader.setInput(csv.toBytes());

        CSVDocument doc = reader.next();
        Assert.assertEquals(new Chars("1"), doc.getFieldValue(0));
        Assert.assertEquals(new Chars("1"), doc.getPropertyValue(new Chars("a")));
        Assert.assertEquals(new Chars("2"), doc.getFieldValue(1));
        Assert.assertEquals(new Chars("2"), doc.getPropertyValue(new Chars("b")));
        Assert.assertEquals(new Chars("3"), doc.getFieldValue(2));
        Assert.assertEquals(new Chars("3"), doc.getPropertyValue(new Chars("c")));
        Assert.assertEquals(new Chars("4"), doc.getFieldValue(3));
        Assert.assertEquals(new Chars("4"), doc.getPropertyValue(new Chars("d")));
        Assert.assertEquals(new Chars("5"), doc.getFieldValue(4));
        Assert.assertEquals(new Chars("5"), doc.getPropertyValue(new Chars("e")));

        Assert.assertNull(reader.next());
    }

    /**
     * Read csv with escaped values.
     *
     * @throws IOException
     */
    @Test
    public void testReadEscaped() throws IOException {

        Chars csv = new Chars("a;\"b\";c\n1;\"hello\n\"\"the\"\"\nworld\";3");

        CSVReader reader = new CSVReader();
        reader.setInput(csv.toBytes());

        CSVDocument doc = reader.next();
        Assert.assertEquals(new Chars("1"), doc.getFieldValue(0));
        Assert.assertEquals(new Chars("1"), doc.getPropertyValue(new Chars("a")));
        Assert.assertEquals(new Chars("hello\n\"the\"\nworld"), doc.getFieldValue(1));
        Assert.assertEquals(new Chars("hello\n\"the\"\nworld"), doc.getPropertyValue(new Chars("b")));
        Assert.assertEquals(new Chars("3"), doc.getFieldValue(2));
        Assert.assertEquals(new Chars("3"), doc.getPropertyValue(new Chars("c")));

        Assert.assertNull(reader.next());


    }

}
