
package science.unlicense.format.csv;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArrayOrderedSet;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.model.doc.AbstractDocument;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.FieldNotFoundException;
import science.unlicense.common.api.model.doc.FieldType;

/**
 *
 * @author Johann Sorel
 */
public class CSVDocument extends AbstractDocument {

    private final DocumentType type;
    private final Sequence values;

    public CSVDocument(DocumentType type, Sequence values) {
        this.type = type;
        this.values = values;
    }

    @Override
    public Set getPropertyNames() {
        final Set fieldNames = new ArrayOrderedSet();
        FieldType[] fields = type.getFields();
        for (int i=0;i<fields.length;i++) fieldNames.add(fields[i].getId());
        return fieldNames;
    }

    @Override
    public Object getPropertyValue(Chars name) {
        final int idx = Arrays.getFirstOccurenceIdentity(type.getFields(),type.getField(name));
        if (idx<0) throw new FieldNotFoundException(name);
        return getFieldValue(idx);
    }

    public Object getFieldValue(int index) {
        return values.get(index);
    }

    @Override
    public boolean setPropertyValue(Chars name, Object value) throws FieldNotFoundException {
        final int idx = Arrays.getFirstOccurenceIdentity(type.getFields(),type.getField(name));
        if (idx<0) throw new FieldNotFoundException(name);
        return setFieldValue(idx, value);
    }

    public boolean setFieldValue(int index, Object value) throws FieldNotFoundException {
        final Object old = values.replace(index, value);
        final boolean changed = (old!=value);
        if (changed &&  hasListeners()) {
            sendPropertyEvent(type.getFields()[index].getId(), old, value);
        }
        return changed;
    }

}
