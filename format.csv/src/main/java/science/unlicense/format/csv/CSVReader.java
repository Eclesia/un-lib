
package science.unlicense.format.csv;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.DocumentTypeBuilder;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class CSVReader extends AbstractReader {

    private static final Char END_LINE = new Char('\n');
    private static final Char ESCAPE = new Char('\"');

    private final CharEncoding encoding;
    private final Char separator;
    private final byte[] sep;
    private final byte[] elb;
    private final byte[] esc;
    private DocumentType type;
    private boolean firstLineHeader;
    private int nbCol = 10;

    private final ByteSequence buffer = new ByteSequence() {
        @Override
        public boolean removeAll() {
            nextIndex = 0;
            size = 0;
            return true;
        }

    };

    public CSVReader() {
        this(CharEncodings.UTF_8,new Char(';'),true);
    }

    public CSVReader(CharEncoding encoding, Char separator, DocumentType docType) {
        this.encoding = encoding;
        this.separator = separator.recode(encoding);
        this.sep = this.separator.toBytes();
        this.type = docType;
        this.nbCol = type.getFields().length;
        this.elb = END_LINE.recode(encoding).toBytes();
        this.esc = ESCAPE.recode(encoding).toBytes();
        if (elb.length != 1) throw new RuntimeException("End line size != 1, not supported yet.");
    }

    public CSVReader(CharEncoding encoding, Char separator, boolean firstLineHeader) {
        this.encoding = encoding;
        this.separator = separator.recode(encoding);
        this.sep = this.separator.toBytes();
        this.firstLineHeader = firstLineHeader;
        this.elb = END_LINE.recode(encoding).toBytes();
        this.esc = ESCAPE.recode(encoding).toBytes();
        if (elb.length != 1) throw new RuntimeException("End line size != 1, not supported yet.");
    }

    public CSVDocument next() throws IOException{
        final CharInputStream cs = getInputAsCharStream(encoding);
        Sequence rowValues = readRow(cs,nbCol);
        if (rowValues == null) return null;

        if (type == null) {
            //build header from first line
            final DocumentTypeBuilder dtb = new DocumentTypeBuilder(new Chars("Unnamed"));
            if (firstLineHeader) {
                for (int i=0,n=rowValues.getSize(); i<n; i++) {
                    dtb.addField((Chars) rowValues.get(i)).valueClass(Chars.class);
                }
                type = dtb.build();
                this.nbCol = type.getFields().length;
                //read next line
                rowValues = readRow(cs,nbCol);
            } else {
                for (int i=0,n=rowValues.getSize(); i<n; i++) {
                    dtb.addField(Int32.encode(i)).valueClass(Chars.class);
                }
                type = dtb.build();
                this.nbCol = type.getFields().length;
            }
        }

        if (rowValues == null) return null;
        return new CSVDocument(type,rowValues);
    }

    /**
     * Read row values.
     *
     * @param cs
     * @param line
     * @return
     * @throws IOException
     */
    private Sequence readRow(CharInputStream cs, int size) throws IOException {
        final Sequence rowValues = new ArraySequence(size);

        boolean inEscape = false;
        int nbb = cs.readChar(buffer);
        if (nbb == -1) return null;

        loop:
        for (;;) {

            if (inEscape) {
                if (nbb <= 0) {
                    throw new IOException(cs, "Unfinished escaped string");
                } else if (buffer.endWidth(esc)) {
                    nbb = cs.readChar(buffer);
                    if (nbb <= 0) {
                        //end of stream
                        int length = buffer.getSize()-esc.length-esc.length;
                        byte[] data = cp(esc.length,length);
                        buffer.removeAll();
                        rowValues.add(new Chars(data,encoding));
                        return rowValues;
                    } else if (buffer.endWidth(esc)) {
                        //escaped quote, remove it
                        buffer.trimEnd(esc.length);
                    } else {
                        //end escaped block
                        inEscape = false;
                        int length = buffer.getSize()-esc.length-esc.length-nbb;
                        byte[] data = cp(esc.length, length);
                        rowValues.add(new Chars(data,encoding));
                        //move to next separator or end of line
                        for (;;) {
                            if (buffer.endWidth(sep) || buffer.endWidth(elb) || nbb <= 0) {
                                buffer.removeAll();
                                continue loop;
                            }
                            nbb = cs.readChar(buffer);
                        }
                    }
                }
            } else {
                if (nbb <= 0) {
                    byte[] data = buffer.toArrayByte();
                    buffer.removeAll();
                    rowValues.add(new Chars(data,encoding));
                    return rowValues;
                } else if (buffer.endWidth(sep)) {
                    byte[] data = cp(0, buffer.getSize()-sep.length);
                    buffer.removeAll();
                    rowValues.add(new Chars(data,encoding));
                } else if (buffer.endWidth(elb)) {
                    byte[] data = cp(0, buffer.getSize()-elb.length);
                    buffer.removeAll();
                    rowValues.add(new Chars(data,encoding));
                    return rowValues;
                } else if (buffer.endWidth(esc)) {
                    inEscape = true;
                }
            }

            nbb = cs.readChar(buffer);
        }

    }

    private byte[] cp(int start, int length) {
        byte[] dst = new byte[length];
        System.arraycopy(buffer.getBackArray(), start, dst, 0, length);
        return dst;
    }

}
