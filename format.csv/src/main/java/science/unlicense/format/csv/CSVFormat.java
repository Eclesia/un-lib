
package science.unlicense.format.csv;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * Resources :
 * https://en.wikipedia.org/wiki/Comma-separated_values
 * https://www.ietf.org/rfc/rfc4180.txt
 *
 * @author Johann Sorel
 */
public class CSVFormat extends DefaultFormat {

    public CSVFormat() {
        super(new Chars("csv"));
        shortName = new Chars("CSV");
        longName = new Chars("Comma-separated values");
        mimeTypes.add(new Chars("text/csv"));
        extensions.add(new Chars("csv"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
