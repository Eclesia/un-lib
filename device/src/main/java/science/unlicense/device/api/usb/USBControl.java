

package science.unlicense.device.api.usb;

import java.nio.ByteBuffer;
import science.unlicense.device.api.DeviceException;

/**
 * Send and receive data from device.
 *
 * @author Johann Sorel
 */
public interface USBControl {

    /**
     * Device manipulated by the control.
     * @return
     */
    USBDevice getDevice();

    /**
     * activate the control.
     */
    void open();

    /**
     * Send data to device.
     *
     * @param endPoint device end point
     * @param data to write
     * @param timeout in milliseconds, 0 for unlimited
     * @return number of bytes written
     * @throws DeviceException
     */
    public int writeSync(final byte endPoint, final ByteBuffer data, final long timeout) throws DeviceException;

    /**
     * Read data from device
     *
     * @param endPoint device end point
     * @param data to store read bytes
     * @param timeout in milliseconds, 0 for unlimited
     * @return number of bytes read
     * @throws DeviceException
     */
    public int readSync(byte endPoint, final ByteBuffer data, final long timeout) throws DeviceException;

    /**
     * Release this control.
     */
    void dispose();

}
