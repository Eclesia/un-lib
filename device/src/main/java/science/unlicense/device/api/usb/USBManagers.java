

package science.unlicense.device.api.usb;

import science.unlicense.system.ModuleSeeker;


/**
 *
 * @author Johann Sorel
 */
public final class USBManagers {


    private USBManagers() {}


    /**
     * Lists available usb managers.
     * There is normaly only one.
     *
     * @return array of ImageFormat, never null but can be empty.
     */
    public static USBManager[] getManagers() {
        return (USBManager[]) ModuleSeeker.findServices(USBManager.class);
    }

    /**
     * Get first USB Manager.
     * @return
     */
    public static USBManager getManager() {
        final USBManager[] array = getManagers();
        if (array.length==0) return null;
        return array[0];
    }

}
