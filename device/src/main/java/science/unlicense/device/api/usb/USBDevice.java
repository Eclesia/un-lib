

package science.unlicense.device.api.usb;

import science.unlicense.device.api.DeviceException;
import science.unlicense.common.api.model.tree.TypedNode;

/**
 *
 * @author Johann Sorel
 */
public interface USBDevice {

    short getProductId();

    short getVendorId();

    USBControl createControl() throws DeviceException;

    TypedNode getDescription();

}
