

package science.unlicense.device.api.usb;

import science.unlicense.common.api.character.Chars;
import science.unlicense.device.api.DeviceException;

/**
 * Entry point to acces USB devices.
 *
 * @author Johann Sorel
 */
public interface USBManager {

    /**
     * Get USB manager name.
     * @return Chars never null
     */
    Chars getName();

    /**
     * List available USB devices.
     * @return USBDevice array, never null but can be empty.
     * @throws science.unlicense.device.api.DeviceException
     */
    USBDevice[] getDevices() throws DeviceException;

}
