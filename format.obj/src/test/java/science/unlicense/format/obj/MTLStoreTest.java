
package science.unlicense.format.obj;

import org.junit.Test;
import static science.unlicense.common.api.Assert.*;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.obj.mtl.MTLStore;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.impl.Scalarf64;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author Johann Sorel
 */
public class MTLStoreTest {

    @Test
    public void testMaterialRead() throws StoreException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/obj/cube.mtl"));
        final MTLStore store = new MTLStore(path);

        final Material mat1 = store.getMaterial(new Chars("Material")).createMaterial();
        final Material mat2 = store.getMaterial(new Chars("m2")).createMaterial();

        assertEquals(new Scalarf64(96.078431), mat1.properties().getPropertyValue(new Chars("Ns")));
        assertEquals(new ColorRGB(0.00f,0.00f,0.00f), mat1.properties().getPropertyValue(SimpleBlinnPhong.MATERIAL_AMBIANT)); // Ka
        assertEquals(new ColorRGB(0.64f,0.64f,0.64f), mat1.properties().getPropertyValue(SimpleBlinnPhong.MATERIAL_DIFFUSE)); // Kd
        assertEquals(new ColorRGB(0.50f,0.50f,0.50f), mat1.properties().getPropertyValue(SimpleBlinnPhong.MATERIAL_SPECULAR)); // Ks
        assertEquals(new Scalarf64(1.0), mat1.properties().getPropertyValue(new Chars("Ni")));
        assertEquals(new Scalarf64(1.0), mat1.properties().getPropertyValue(new Chars("d")));
        assertEquals(new Scalarf64(2.0), mat1.properties().getPropertyValue(new Chars("illum")));

        assertEquals(new Scalarf64(5.0), mat2.properties().getPropertyValue(new Chars("Ns")));
        assertEquals(new ColorRGB(1.00f,0.50f,0.20f), mat2.properties().getPropertyValue(SimpleBlinnPhong.MATERIAL_AMBIANT)); // Ka
        assertEquals(new Scalarf64(0.75), mat2.properties().getPropertyValue(new Chars("d")));
        assertEquals(new Scalarf64(3.0), mat2.properties().getPropertyValue(new Chars("illum")));
    }

}
