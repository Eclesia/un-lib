
package science.unlicense.format.obj;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.obj.obj.OBJStore;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public class OBJReaderTest {

    @Ignore
    @Test
    public void testRead() throws Exception{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/obj/cube.obj"));

        final OBJStore store = new OBJStore(path);

        final Collection elements = store.getElements();
        Assert.assertEquals(1, elements.getSize());

        final Object obj = elements.createIterator().next();
        Assert.assertTrue(obj instanceof MotionModel);
        final MotionModel mpm = (MotionModel) obj;

        Assert.assertEquals(1, mpm.getChildren().getSize());
        final DefaultModel mesh = (DefaultModel) mpm.getChildren().get(0);

        final DefaultMesh shell = (DefaultMesh) mesh.getShape();
        final float[] vertices = Geometries.toFloat32(shell.getPositions());
        final float[] normals = Geometries.toFloat32(shell.getNormals());
        final float[] uvs = Geometries.toFloat32(shell.getUVs());
        final IndexedRange[] ranges = shell.getRanges();
        final int[] indices = Geometries.toInt32(shell.getIndex());

        Assert.assertArrayEquals(new float[]{
//             1,-1, 0,     1,-1, 2,    -1,-1, 2, //1,2,3
//             1,-1, 0,    -1,-1, 2,    -1,-1, 0, //1,3,4
//
//             1, 1, 0,    -1, 1, 0,    -1, 1, 2, //5,8,7
//             1, 1, 0,    -1, 1, 2,     1, 1, 2, //5,7,6
//
//             1,-1, 0,     1, 1, 0,     1, 1, 2, //1,5,6
//             1,-1, 0,     1, 1, 2,     1,-1, 2, //1,6,2
//
//             1,-1, 2,     1, 1, 2,    -1, 1, 2, //2,6,7
//             1,-1, 2,    -1, 1, 2,    -1,-1, 2, //2,7,3
//
//            -1,-1, 2,    -1, 1, 2,    -1, 1, 0, //3,7,8
//            -1,-1, 2,    -1, 1, 0,    -1,-1, 0, //3,8,4
//
//             1, 1, 0,     1,-1, 0,    -1,-1, 0, //5,1,4
//             1, 1, 0,    -1,-1, 0,    -1, 1, 0  //5,4,8

            //new approach use less elements
             1,-1, 0,     1,-1, 2,    -1,-1, 2,    -1,-1, 0, //1,2,3,4
             1, 1, 0,    -1, 1, 0,    -1, 1, 2,     1, 1, 2, //5,8,7,6
             1,-1, 0,     1, 1, 0,     1, 1, 2,     1,-1, 2, //1,5,6,2
             1,-1, 2,     1, 1, 2,    -1, 1, 2,    -1,-1, 2, //2,6,7,3
            -1,-1, 2,    -1, 1, 2,    -1, 1, 0,    -1,-1, 0, //3,7,8,4
             1, 1, 0,     1,-1, 0,    -1,-1, 0,    -1, 1, 0  //5,1,4,8
                },
                vertices, 0.00001f);
        Assert.assertArrayEquals(new int[]{
//             0, 1, 2,
//             3, 4, 5,
//             6, 7, 8,
//             9,10,11,
//            12,13,14,
//            15,16,17,
//            18,19,20,
//            21,22,23,
//            24,25,26,
//            27,28,29,
//            30,31,32,
//            33,34,35
            //new approach use less elements
              0,  1,  2,
              0,  2,  3,
              4,  5,  6,
              4,  6,  7,
              8,  9, 10,
              8, 10, 11,
             12, 13, 14,
             12, 14, 15,
             16, 17, 18,
             16, 18, 19,
             20, 21, 22,
             20, 22, 23

                },
                indices);


    }

}
