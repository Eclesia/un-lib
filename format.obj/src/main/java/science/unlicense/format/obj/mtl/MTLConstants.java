package science.unlicense.format.obj.mtl;

import science.unlicense.common.api.character.Chars;

/**
 * MTL file constants.
 *
 * @author Johann Sorel
 */
public final class MTLConstants {

    public static final Chars COMMENT = Chars.constant("#");

    public static final Chars NEW = Chars.constant("newmtl");
    public static final Chars COLOR_AMBIANT = Chars.constant("Ka");
    public static final Chars COLOR_DIFFUSE = Chars.constant("Kd");
    public static final Chars COLOR_SPECULAR = Chars.constant("Ks");

    /**
     * Illumination types :
     * 0 - use color, no ambiant
     * 1 - use color, with ambiant
     * 2 - use lights
     * 3 - ?
     * 4 - ?
     * 5 - ?
     * 6 - ?
     * 7 - ?
     * 8 - ?
     * 9 - ?
     * 10- ?
     */
    public static final Chars ILLUMINATION = Chars.constant("illum");
    /** Specular coefficient of diffuse color */
    public static final Chars DIFFUSE_COEFF = Chars.constant("Ns");
    public static final Chars Ni = Chars.constant("Ni");

    /** 'd' and 'Tr' are both the same thing */
    public static final Chars DISOLVE = Chars.constant("d");
    public static final Chars TRANSPARENCY = Chars.constant("Tr");

    public static final Chars SHARPNESS = Chars.constant("sharpness");

    public static final Chars MAP_AMBIANT = Chars.constant("map_Ka");
    public static final Chars MAP_DIFFUSE = Chars.constant("map_Kd");
    public static final Chars MAP_SPECULAR = Chars.constant("map_Ks");
    public static final Chars MAP_ALPHA = Chars.constant("map_d");
    /** displacement map */
    public static final Chars DISP = Chars.constant("disp");
    /** stencil decal ? */
    public static final Chars DECAL = Chars.constant("decal");

    /** 'bump' or 'map_bump' for bump paiing */
    public static final Chars BUMP = Chars.constant("bump");
    public static final Chars MAP_BUMP = Chars.constant("map_bump");

    private MTLConstants() {}

}
