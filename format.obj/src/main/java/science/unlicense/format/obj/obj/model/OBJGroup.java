
package science.unlicense.format.obj.obj.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class OBJGroup {

    public static class MaterialRange{
        public Chars name;
        public int start;
        public int end;

    }

    public Chars name;

    /** Sequence of points : OBJPoint */
    public final Sequence points = new ArraySequence();
    /** Sequence of lines : OBJLine */
    public final Sequence lines = new ArraySequence();
    /** Sequence of faces : OBJFace */
    public final Sequence faces = new ArraySequence();

    public boolean smooth = false;

    public final Sequence materialRanges = new ArraySequence();

    public OBJGroup() {
    }

    public OBJGroup(Chars name) {
        this.name = name;
    }

}
