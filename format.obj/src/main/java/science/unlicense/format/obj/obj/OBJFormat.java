
package science.unlicense.format.obj.obj;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class OBJFormat extends AbstractModel3DFormat{

    public static final OBJFormat INSTANCE = new OBJFormat();

    private OBJFormat() {
        super(new Chars("OBJ"));
        shortName = new Chars("OBJ");
        longName = new Chars("OBJ");
        extensions.add(new Chars("obj"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new OBJStore(input);
    }

}
