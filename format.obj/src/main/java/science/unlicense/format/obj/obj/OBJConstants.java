
package science.unlicense.format.obj.obj;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class OBJConstants {

    public static final Chars COMMENT = Chars.constant("#");

    //vertex data
    public static final Chars VERTEX_COORD      = Chars.constant("v");
    public static final Chars VERTEX_NORMAL     = Chars.constant("vn");
    public static final Chars VERTEX_TEXTURE    = Chars.constant("vt");
    public static final Chars VERTEX_PARAMETER  = Chars.constant("vp");
    public static final Chars CURVE_SURFACE_TYPE= Chars.constant("cstype");
    public static final Chars DEGREE            = Chars.constant("deg");
    public static final Chars BASIC_MATRIX      = Chars.constant("bmat");
    public static final Chars STEP              = Chars.constant("step");

    //elements
    public static final Chars POINT     = Chars.constant("p");
    public static final Chars LINE      = Chars.constant("l");
    public static final Chars FACE      = Chars.constant("f");
    public static final Chars CURVE     = Chars.constant("curv");
    public static final Chars CURVE2D   = Chars.constant("curv2");
    public static final Chars SURFACE   = Chars.constant("surf");

    public static final Chars ON        = Chars.constant("on");
    public static final Chars OFF       = Chars.constant("off");

    //Free-form curve/surface body statements
    /** parameter values */
    public static final Chars PARAMETER     = Chars.constant("parm");
    /** outer trimming loop */
    public static final Chars TRIM          = Chars.constant("trim");
    /** inner trimming loop */
    public static final Chars HOLE          = Chars.constant("hole");
    /** special curve */
    public static final Chars SPECIAL_CURVE = Chars.constant("scrv");
    /** special point */
    public static final Chars SPECIAL_POINT = Chars.constant("sp");
    /** end statement */
    public static final Chars END           = Chars.constant("end");

    //Connectivity between free-form surfaces
    /** connect */
    public static final Chars CONNECT = Chars.constant("con");

    //Grouping
    /** group name */
    public static final Chars GROUP             = Chars.constant("g");
    /** smoothing group */
    public static final Chars SMOOTHING_GROUP   = Chars.constant("s");
    /** merging group */
    public static final Chars MERGING_GROUP     = Chars.constant("mg");
    /** object name */
    public static final Chars OBJECT            = Chars.constant("o");

    //Display/render attributes
    /**  bevel interpolation */
    public static final Chars INTERP_BEVEL      = Chars.constant("bevel");
    /**  color interpolation */
    public static final Chars INTERP_COLOR      = Chars.constant("c_interp");
    /**  dissolve interpolation */
    public static final Chars INTERP_DISSOLVE   = Chars.constant("d_interp");
    /**  level of detail */
    public static final Chars LOD               = Chars.constant("lod");
    /**  material name */
    public static final Chars MTL_NAME          = Chars.constant("usemtl");
    /**  material library */
    public static final Chars MTL_LIB           = Chars.constant("mtllib");
    /**  shadow casting */
    public static final Chars SHADOW            = Chars.constant("shadow_obj");
    /**  ray tracing */
    public static final Chars RAYTRACE          = Chars.constant("trace_obj");
    /**  curve approximation technique */
    public static final Chars APP_CURVE         = Chars.constant("ctech");
    /**  surface approximation technique */
    public static final Chars APP_SURF          = Chars.constant("stech");


    private OBJConstants(){}

}
