
package science.unlicense.format.obj.obj;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Vectors;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.format.obj.mtl.MTLMaterial;
import science.unlicense.format.obj.mtl.MTLStore;
import science.unlicense.format.obj.obj.model.OBJFace;
import science.unlicense.format.obj.obj.model.OBJFile;
import science.unlicense.format.obj.obj.model.OBJGroup;
import science.unlicense.format.obj.obj.model.OBJObject;

/**
 *
 * @author Johann Sorel
 */
public class OBJStore extends AbstractModel3DStore {

    private OBJFile obj = null;

    public OBJStore(Object input) {
        super(OBJFormat.INSTANCE,input);
    }

    public Collection getElements() throws StoreException {

        if (obj==null){
            obj = new OBJFile();
            try {
                obj.read(this);
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }

        final Collection col = new ArraySequence();
        final Dictionary materialCache = new HashDictionary();
        final Dictionary textureCache = new HashDictionary();

        int nb=0;
        for (int i=0,n=obj.objects.getSize();i<n;i++){
            final OBJObject obj = (OBJObject) this.obj.objects.get(i);
            final GraphicNode mpm = rebuildObject(obj,materialCache,textureCache);
            nb+= mpm.getChildren().getSize();
            col.add(mpm);
        }

        return col;
    }

    private GraphicNode rebuildObject(OBJObject obj, Dictionary materialCache, Dictionary textureCache) throws StoreException{
        final GraphicNode mpm = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);

        for (int i=0,n=obj.groups.getSize();i<n;i++){
            final OBJGroup group = (OBJGroup) obj.groups.get(i);
            final GraphicNode mesh = rebuildGroup(group,materialCache,textureCache);
            mpm.getChildren().add(mesh);
        }

        return mpm;
    }

    private GraphicNode rebuildGroup(OBJGroup group, Dictionary materialCache, Dictionary textureCache) throws StoreException{

        final Sequence faces = group.faces;

        //count number of triangles
        //some files contain quad or polygons in face definition
        int nbPoint=0;
        int nbIndices=0;
        for (int i=0,n=faces.getSize();i<n;i++){
            final OBJFace face = (OBJFace) faces.get(i);
            nbPoint += face.vertexIdx.length;
            nbIndices += (face.vertexIdx.length-2)*3;
        }

        //size of each data array, used to flip negative references
        final int[] sizes = new int[]{obj.vertexCoord.getSize(),obj.vertexNormal.getSize(),obj.vertexTexture.getSize()};

        //prepare buffers
        final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(nbPoint*3).cursor();
        final Float32Cursor normals = DefaultBufferFactory.INSTANCE.createFloat32(nbPoint*3).cursor();
        final Float32Cursor uv = DefaultBufferFactory.INSTANCE.createFloat32(nbPoint*2).cursor();
        final Int32Cursor indices = DefaultBufferFactory.INSTANCE.createInt32(nbIndices).cursor();
        final VBO meshVertex = new VBO(vertices.getBuffer(),3);
        final VBO meshNormal = new VBO(normals.getBuffer(),3);
        final VBO meshUV = new VBO(uv.getBuffer(),2);
        final IBO meshIndice = new IBO(indices.getBuffer());

        int indice = 0;
        for (int i=0,n=faces.getSize();i<n;i++){
            final OBJFace face = (OBJFace) faces.get(i);


            flipNegative(face.vertexIdx,sizes);
            if (face.uvIdx!=null) flipNegative(face.uvIdx,sizes);
            if (face.normalIdx!=null) flipNegative(face.normalIdx,sizes);

            final int[] temp = new int[face.vertexIdx.length];
            for (int k=0;k<temp.length;k++){
                temp[k] = indice++;
            }

            for (int k=2,kn=face.vertexIdx.length;k<kn;k++){
                final boolean firstTriangle = k==2;
                final Tuple v1 = (Tuple) obj.vertexCoord.get(face.vertexIdx[0  ]-1);
                final Tuple v2 = (Tuple) obj.vertexCoord.get(face.vertexIdx[k-1]-1);
                final Tuple v3 = (Tuple) obj.vertexCoord.get(face.vertexIdx[k  ]-1);
                if (firstTriangle){
                    vertices.write(v1.toFloat());
                    vertices.write(v2.toFloat());
                }
                vertices.write(v3.toFloat());

                if (face.uvIdx!=null){
                    if (firstTriangle){
                        final Tuple uv1 = (Tuple) obj.vertexTexture.get(face.uvIdx[0  ]-1);
                        final Tuple uv2 = (Tuple) obj.vertexTexture.get(face.uvIdx[k-1]-1);
                        uv.write((float) uv1.get(0));uv.write(1f-(float) uv1.get(1));
                        uv.write((float) uv2.get(0));uv.write(1f-(float) uv2.get(1));
                    }
                    final Tuple uv3 = (Tuple) obj.vertexTexture.get(face.uvIdx[k  ]-1);
                    uv.write((float) uv3.get(0));uv.write(1f-(float) uv3.get(1));
                }

                if (face.normalIdx!=null){
                    if (firstTriangle){
                        final Tuple n1 = (Tuple) obj.vertexNormal.get(face.normalIdx[0  ]-1);
                        final Tuple n2 = (Tuple) obj.vertexNormal.get(face.normalIdx[k-1]-1);
                        normals.write(n1.toFloat());
                        normals.write(n2.toFloat());
                    }
                    final Tuple n3 = (Tuple) obj.vertexNormal.get(face.normalIdx[k  ]-1);
                    normals.write(n3.toFloat());
                } else {
                    //calculate normal
                    double[] v2v1 = Vectors.subtract(v2.toDouble(), v1.toDouble(), null);
                    double[] v3v1 = Vectors.subtract(v3.toDouble(), v1.toDouble(), null);
                    double[] normal = Vectors.cross(v2v1, v3v1, null);
                    if (k==2){
                        normals.write((float) normal[0]);
                        normals.write((float) normal[1]);
                        normals.write((float) normal[2]);
                        normals.write((float) normal[0]);
                        normals.write((float) normal[1]);
                        normals.write((float) normal[2]);
                    }
                    normals.write((float) normal[0]);
                    normals.write((float) normal[1]);
                    normals.write((float) normal[2]);
                }

                indices.write(temp[0  ]);
                indices.write(temp[k-1]);
                indices.write(temp[k  ]);
            }
        }

        if (group.materialRanges.isEmpty()){
            final DefaultModel mesh = new DefaultModel();
            mesh.getTechniques().add(new SimpleBlinnPhong());
            final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
            shell.setPositions(meshVertex);
            shell.setNormals(meshNormal);
            shell.setUVs(meshUV);
            shell.setIndex(meshIndice);
            shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, meshIndice.getCapacity())});
            mesh.setShape(shell);
            mesh.setId(group.name);
            mesh.setTitle(group.name);
            mesh.updateBoundingBox();
            return mesh;
        } else {
            //rebuild one mesh for each material
            final GraphicNode base = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
            base.setId(group.name);
            base.setTitle(group.name);

            if (obj.materialStores!=null && !obj.materialStores.isEmpty()){
                final MTLStore mtl = (MTLStore) obj.materialStores.get(0);
                for (int i=0,n=group.materialRanges.getSize();i<n;i++){
                    final OBJGroup.MaterialRange range = (OBJGroup.MaterialRange) group.materialRanges.get(i);
                    if (range.end==-1) range.end = group.faces.getSize();
                    if (range.end-range.start <= 0) continue;

                    Material material = (Material) materialCache.getValue(range.name);
                    if (material==null){
                        //load the material
                        final MTLMaterial mtlMat = mtl.getMaterial(range.name);
                        if (mtlMat!=null){
                            material = mtlMat.createMaterial(textureCache);
                            materialCache.add(range.name, material);
                        }
                    }

                    //build mesh
                    final DefaultModel mesh = new DefaultModel();
                    mesh.getTechniques().add(new SimpleBlinnPhong());
                    final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
                    shell.setPositions(meshVertex);
                    shell.setNormals(meshNormal);
                    shell.setUVs(meshUV);
                    shell.setIndex(meshIndice);
                    shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(range.start*3, range.end*3)});
                    mesh.setShape(shell);
                    mesh.setId(range.name);
                    mesh.setTitle(range.name);
                    if (material!=null){
                        mesh.getMaterials().add(material);
                    }
                    mesh.updateBoundingBox();
                    base.getChildren().add(mesh);
                }
            }

            return base;
        }

    }

    /**
     * If indexes are negative, they are starting from the end of the list.
     * @param index
     * @return
     */
    private static void flipNegative(int[] index, int[] sizes){
        for (int i=0;i<index.length;i++){
            if (index[i]<0){
                index[i] += sizes[i];
            }
        }
    }

    /**
     * If indexes are negative, they are starting from the end of the list.
     * @param index
     * @return
     */
    private static int flipNegative(int index, int size){
        if (index<0){
            return size+index;
        }
        return index;
    }

}
