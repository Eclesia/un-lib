
package science.unlicense.format.obj.obj.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class OBJObject {

    public Chars name;
    public Sequence groups = new ArraySequence();

    public OBJObject() {
    }

    public OBJObject(final Chars name) {
        this.name = name;
    }

}
