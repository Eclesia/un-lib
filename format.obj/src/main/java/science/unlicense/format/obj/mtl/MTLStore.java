
package science.unlicense.format.obj.mtl;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.encoding.api.store.StoreException;
import static science.unlicense.format.obj.mtl.MTLConstants.COMMENT;
import static science.unlicense.format.obj.mtl.MTLConstants.NEW;

/**
 * MTL library of materials.
 *
 * Resource :
 * http://en.wikipedia.org/wiki/Wavefront_.obj_file
 * http://paulbourke.net/dataformats/mtl/
 *
 * @author Johann Sorel
 */
public class MTLStore extends AbstractStore{

    private final Path basePath;

    private boolean readed = false;

    /** sequence of MTLMaterial */
    private final Dictionary materials = new HashDictionary();

    public MTLStore(Path path) {
        super(null, path);
        this.basePath = path.getParent();
    }

    public MTLMaterial getMaterial(Chars name) throws StoreException{
        if (!readed){
            readed = true;
            try {
                read();
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }
        return (MTLMaterial) materials.getValue(name);
    }

    private void read() throws IOException{

        final boolean[] mustClose = new boolean[1];
        final ByteInputStream bi = IOUtilities.toInputStream(getInput(), mustClose);
        final CharInputStream charStream = new CharInputStream(bi, CharEncodings.US_ASCII);

        MTLMaterial material = null;
        Chars cs = null;
        while ( (cs=charStream.readLine()) != null){
            cs = cs.trim();
            if (cs.isEmpty()){
                continue;
            }
            if (cs.startsWith(COMMENT)){
                continue;
            }

            int pe = cs.getFirstOccurence(' ');
            int pt = cs.getFirstOccurence('\t');
            if (pt>0 && pt<pe) pe = pt;
            if (pe<0) {pe = cs.getByteLength();}
            final Chars prefix = cs.truncate(0, pe);
            cs = cs.truncate(pe+1,cs.getCharLength()).trim();


            if (NEW.equals(prefix)) {
                //new material
                material = new MTLMaterial(cs);
                material.setBasePath(basePath);
                materials.add(cs,material);
            } else {
                //material property
                material.getProperties().add(prefix, cs);
            }
        }
    }


}
