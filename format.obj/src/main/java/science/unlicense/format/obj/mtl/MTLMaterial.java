
package science.unlicense.format.obj.mtl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.exception.ParseRuntimeException;
import science.unlicense.common.api.number.Float64;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Scalarf64;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class MTLMaterial {

    private Chars name;
    private Path basePath;
    private final Dictionary properties = new HashDictionary();

    public MTLMaterial() {
    }

    public MTLMaterial(Chars name) {
        this.name = name;
    }

    public Chars getName() {
        return name;
    }

    public Path getBasePath() {
        return basePath;
    }

    public void setBasePath(Path basePath) {
        this.basePath = basePath;
    }

    public Dictionary getProperties() {
        return properties;
    }

    /**
     * Ambiant color : Ka
     * @return Color, may be null
     */
    public Color getAmbiantColor(){
        return asColor(MTLConstants.COLOR_AMBIANT);
    }

    /**
     * Diffuse color : Kd
     * @return Color, may be null
     */
    public Color getDiffuseColor(){
        return asColor(MTLConstants.COLOR_DIFFUSE);
    }

    /**
     * Specular color : Ks
     * @return Color, may be null
     */
    public Color getSpecularColor(){
        return asColor(MTLConstants.COLOR_SPECULAR);
    }

    /**
     * Ambiant texture map : map_Ka
     * @return Path, may be null
     */
    public Path getAmbiantMap(){
        return asPath(MTLConstants.MAP_AMBIANT);
    }

    /**
     * Diffuse texture map : map_Kd
     * @return Path, may be null
     */
    public Path getDiffuseMap(){
        return asPath(MTLConstants.MAP_DIFFUSE);
    }

    /**
     * Specular texture map : map_Ks
     * @return Path, may be null
     */
    public Path getSpecularMap(){
        return asPath(MTLConstants.MAP_SPECULAR);
    }

    public Material createMaterial() throws StoreException{
        return createMaterial(null);
    }

    public Material createMaterial(Dictionary textureCache) throws StoreException{

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setName(name);

        final Iterator ite = properties.getPairs().createIterator();
        while (ite.hasNext()) {
            final Pair pair = (Pair) ite.next();
            final Chars key = (Chars) pair.getValue1();
            Chars value = (Chars) pair.getValue2();

            if ( MTLConstants.COLOR_AMBIANT.equals(key)) {
                material.setAmbient(asColor(key));
            } else if ( MTLConstants.COLOR_DIFFUSE.equals(key)) {
                material.setDiffuse(asColor(key));
            } else if ( MTLConstants.COLOR_SPECULAR.equals(key)) {
                material.setSpecular(asColor(key));
            } else if ( MTLConstants.MAP_AMBIANT.equals(key)) {
                material.setAmbiantTexture(asTextureMapping(key, textureCache));
            } else if ( MTLConstants.MAP_DIFFUSE.equals(key)) {
                material.setDiffuseTexture(asTextureMapping(key, textureCache));
            } else if ( MTLConstants.MAP_SPECULAR.equals(key)) {
                material.setSpecularTexture(asTextureMapping(key, textureCache));
            } else {

                //try to convert value to a tuple
                try {
                    VectorRW v = null;
                    Chars str = value.trim();
                    while (!str.isEmpty()) {
                        int pe = str.getFirstOccurence(' ');
                        int pt = str.getFirstOccurence('\t');
                        if (pt>0 && pt<pe) pe = pt;

                        if (pe < 0) {
                            double d = Float64.decode(str);
                            if (v == null) {
                                v = new Scalarf64(d);
                            } else {
                                v = v.extend(d);
                            }
                            str = Chars.EMPTY;
                        } else {
                            double d = Float64.decode(str.truncate(0, pe));
                            if (v == null) {
                                v = new Scalarf64(d);
                            } else {
                                v = v.extend(d);
                            }
                            str = str.truncate(pe, -1);
                        }
                    }
                    material.properties().setPropertyValue(key, v);
                } catch (ParseRuntimeException ex) {
                    //not a number
                    material.properties().setPropertyValue(key, value);
                }
            }
        }

        return material;
    }

    private Color asColor(Chars property){
        final Chars val = (Chars) properties.getValue(property);
        if (val==null) return null;
        final Chars[] rgb = val.split(' ');
        return new ColorRGB((float) Float64.decode(rgb[0]), (float) Float64.decode(rgb[1]), (float) Float64.decode(rgb[2]));
    }

    private Path asPath(Chars property){
        final Chars path = (Chars) properties.getValue(property);
        if (path==null) return null;
        return basePath.resolve(path);
    }

    private TextureMapping asTextureMapping(Chars property, Dictionary textureCache) throws StoreException {

        //try to find file
        final Chars subPath = (Chars) properties.getValue(property);
        Path path = null;

        //try absolue path
        try {
            path = Paths.resolve(subPath);
            if (!path.exists()) path = null;
        } catch (Exception ex) {}
        //try relative path
        if (path == null) {
            try {
                path = basePath.resolve(subPath);
                if (!path.exists()) path = null;
            } catch (Exception ex) {}
        }
        //try local folder
        if (path == null) {
            try {
                Chars[] parts = subPath.split('/');
                path = basePath.resolve(parts[parts.length-1]);
                if (!path.exists()) path = null;
            } catch (Exception ex) {}
        }
        if (path == null) {
            try {
                Chars[] parts = subPath.split('\\');
                path = basePath.resolve(parts[parts.length-1]);
                if (!path.exists()) path = null;
            } catch (Exception ex) {}
        }

        if (path != null) {
            Texture2D texture = null;
            if (textureCache != null) {
                texture = (Texture2D) textureCache.getValue(path);
            }
            if (texture == null) {
                try {
                    final Image image = Images.read(path);
                    texture = new Texture2D(image);
                    if (textureCache != null) {
                        textureCache.add(path, texture);
                    }
                    return new TextureMapping(texture);
                } catch (IOException ex) {
                    throw new StoreException(ex);
                }
            }
            return new TextureMapping(texture);
        }

        return null;
    }

}
