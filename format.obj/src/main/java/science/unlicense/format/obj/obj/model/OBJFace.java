
package science.unlicense.format.obj.obj.model;


/**
 *
 * @author Johann Sorel
 */
public class OBJFace {

    /**
     * sequence of int[]
     * faces, 1 face is composed of 9 int : v/vt/vn v/vt/vn v/vt/vn
     */
    public int[] vertexIdx;
    public int[] uvIdx    ;
    public int[] normalIdx;

}
