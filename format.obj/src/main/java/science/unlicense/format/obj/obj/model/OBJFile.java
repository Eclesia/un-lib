
package science.unlicense.format.obj.obj.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.obj.mtl.MTLStore;
import static science.unlicense.format.obj.obj.OBJConstants.*;
import science.unlicense.format.obj.obj.OBJStore;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;

/**
 * OBJ File.
 *
 * @author Johann Sorel
 */
public class OBJFile extends AbstractReader{

    private Path basePath;
    private OBJObject currentObject;
    private OBJGroup currentGroup;
    //private Chars currentMaterial;

    /** Sequence of vertex coordinate : Tuple 3 */
    public final Sequence vertexCoord = new ArraySequence();
    /** Sequence of vertex normal : Tuple 3 */
    public final Sequence vertexNormal = new ArraySequence();
    /** Sequence of vertex texture coordinate : Tuple 2 */
    public final Sequence vertexTexture = new ArraySequence();

    /** Sequence of objects : OBJObject */
    public final Sequence objects = new ArraySequence();
    /** Sequence of points : OBJPoint */
    public final Sequence materialStores = new ArraySequence();

    public void read(OBJStore store) throws IOException {
        setInput(store.getInput());
        basePath = ((Path) store.getInput()).getParent();

        final CharInputStream charStream = getInputAsCharStream(CharEncodings.US_ASCII);

        Chars cs = null;
        int lineIndex=0;//keep track of line index for debugging
        try{
            while ( (cs=charStream.readLine()) != null){
                lineIndex++;
                cs = cs.trim();
                if (cs.isEmpty() || cs.startsWith(COMMENT)){
                    continue;
                }

                int pe = cs.getFirstOccurence(' ');
                int pt = cs.getFirstOccurence('\t');
                if (pt>0 && pt<pe) pe = pt;
                if (pe<0) {pe = cs.getCharLength();}
                final Chars prefix = cs.truncate(0, pe);
                cs = cs.truncate(prefix.getCharLength(),cs.getCharLength()).trim();

                //vertex data
                if (VERTEX_COORD.equals(prefix,true,true)) {
                    parseVertexCoord(cs);
                } else if (VERTEX_NORMAL.equals(prefix,true,true)) {
                    parseVertexNormal(cs);
                } else if (VERTEX_TEXTURE.equals(prefix,true,true)) {
                    parseVertexTexture(cs);
                } else if (VERTEX_PARAMETER.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (CURVE_SURFACE_TYPE.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (DEGREE.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (BASIC_MATRIX.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (STEP.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                }
                //elements
                else if (POINT.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (LINE.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (FACE.equals(prefix,true,true)) {
                    parseFace(cs);
                } else if (CURVE.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (CURVE2D.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (SURFACE.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                }
                //Free-form curve/surface body statements
                else if (PARAMETER.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (TRIM.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (HOLE.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (SPECIAL_CURVE.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (SPECIAL_POINT.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (END.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                }
                //Connectivity between free-form surfaces
                else if (CONNECT.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                }
                //Grouping
                else if (GROUP.equals(prefix,true,true)) {
                    parseGroup(cs);
                } else if (SMOOTHING_GROUP.equals(prefix,true,true)) {
                    parseSmoothingGroup(cs);
                } else if (MERGING_GROUP.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (OBJECT.equals(prefix,true,true)) {
                    parseObject(cs);
                }
                //Display/render attributes
                else if (INTERP_BEVEL.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (INTERP_COLOR.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (INTERP_DISSOLVE.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (LOD.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (MTL_NAME.equals(prefix,true,true)) {
                    parseMaterial(cs);
                } else if (MTL_LIB.equals(prefix,true,true)) {
                    parseMaterialStore(cs);
                } else if (SHADOW.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (RAYTRACE.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (APP_CURVE.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else if (APP_SURF.equals(prefix,true,true)) {
                    throw new IOException(cs, "tag "+prefix+" not supported yet.");
                } else {
                    throw new IOException(cs, "unreconized tag "+ prefix);
                }
            }
        }catch(Exception ex){
            throw new IOException(ex.getMessage()+" (line="+lineIndex+")",ex);
        }

    }

    private OBJObject getCurrentObject(){
        if (currentObject==null){
            currentObject = new OBJObject();
            objects.add(currentObject);
        }
        return currentObject;
    }

    private OBJGroup getCurrentGroup(){

        if (currentGroup==null){
            currentGroup = new OBJGroup();
            //currentGroup.materialName = currentMaterial;
            final OBJObject obj = getCurrentObject();
            obj.groups.add(currentGroup);
        }

        return currentGroup;
    }

    private void parseVertexCoord(Chars line){
        final Chars[] parts = line.split(' ');
        final Vector3f64 coords = new Vector3f64();
        for (int i=0;i<parts.length;i++){
            coords.set(i, Float64.decode(parts[i].trim()));
        }
        vertexCoord.add(coords);
    }

    private void parseVertexNormal(Chars line){
        final Chars[] parts = line.split(' ');
        final Vector3f64 coords = new Vector3f64();
        for (int i=0;i<parts.length;i++){
            coords.set(i, Float64.decode(parts[i].trim()));
        }
        vertexNormal.add(coords);
    }

    private void parseVertexTexture(Chars line){
        final Chars[] parts = line.split(' ');
        final TupleRW coords = VectorNf64.createDouble(parts.length);
        for (int i=0;i<parts.length;i++){
            coords.set(i, Float64.decode(parts[i].trim()));
        }
        vertexTexture.add(coords);
    }

    private void parseFace(Chars line){
        final OBJFace face = new OBJFace();
        final Chars[] parts = line.split(' ');

        boolean hasuv = false;
        boolean hasnormal = false;

        for (int i=0;i<parts.length;i++){
            final Chars[] indexes = parts[i].split('/');
            if (face.vertexIdx==null){
                face.vertexIdx = new int[parts.length];
                hasuv = indexes.length>1 && !indexes[1].isEmpty();
                hasnormal = indexes.length>2 && !indexes[2].isEmpty();
                if (hasuv) face.uvIdx = new int[parts.length];
                if (hasnormal) face.normalIdx = new int[parts.length];
            }

            face.vertexIdx[i] = Int32.decode(indexes[0]);
            if (hasuv) face.uvIdx[i] = Int32.decode(indexes[1]);
            if (hasnormal) face.normalIdx[i] = Int32.decode(indexes[2]);
        }

        final OBJGroup group = getCurrentGroup();
        group.faces.add(face);
    }

    private void parseObject(Chars line){
        currentObject = new OBJObject(line);
        currentGroup = null;
        objects.add(currentObject);
    }

    private void parseGroup(Chars line) throws IOException{
        currentGroup = new OBJGroup(line);
        //currentGroup.materialName = currentMaterial;

        OBJObject obj = getCurrentObject();
        obj.groups.add(currentGroup);
    }

    private void parseSmoothingGroup(Chars line){
        boolean smooth = false;

        if (line.equals(ON,true,true)){
            smooth = true;
        } else if (line.equals(OFF,true,true)){
            smooth = false;
        } else {
            try{
                smooth = Int32.decode(line) != 0;
            }catch(NumberFormatException ex){
                smooth = false;
            }
        }

        getCurrentGroup().smooth = smooth;
    }

    private void parseMaterialStore(Chars line) throws IOException{
        final Path p = basePath.resolve(line);
        materialStores.add(new MTLStore(p));
    }

    private void parseMaterial(Chars line){
        final OBJGroup group = getCurrentGroup();
        final OBJGroup.MaterialRange range = new OBJGroup.MaterialRange();
        range.name = line;
        range.start=group.faces.getSize();
        range.end=-1;

        if (!group.materialRanges.isEmpty()){
            OBJGroup.MaterialRange previous = (OBJGroup.MaterialRange) group.materialRanges.get(group.materialRanges.getSize());
            previous.end = group.faces.getSize();
        }

        group.materialRanges.add(range);

    }

}
