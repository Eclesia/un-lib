
package science.unlicense.code.spir;

import org.junit.Test;
import science.unlicense.code.spir.model.Module;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class SpirReaderTest {

    @Test
    public void testRead() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/code/spir/simpleFragment.spv"));
        final SpirReader reader = new SpirReader();
        reader.setInput(path);
        final Module module = reader.read();

        final Chars text = module.debug();
        Assert.assertEquals(
                new Chars(
            "SPIR-V Module\n" +
            "OpCapability                               [1 words]  Shader                            (Enumerated<Capability>)\n" +
            "OpExtInstImport                            [5 words]  1 'GLSL.std.450'                  (IdResult, LiteralString)\n" +
            "OpMemoryModel                              [2 words]  Logical GLSL450                   (Enumerated<AddressingModel>, Enumerated<MemoryModel>)\n" +
            "OpEntryPoint                               [6 words]  Fragment 4 'main' [9,12]          (Enumerated<ExecutionModel>, IdRef, LiteralString, IdRef*)\n" +
            "OpExecutionMode                            [2 words]  4 OriginUpperLeft                 (IdRef, Enumerated<ExecutionMode>)\n" +
            "OpSource                                   [2 words]  GLSL 450 - -                      (Enumerated<SourceLanguage>, LiteralInteger, IdRef?, LiteralString?)\n" +
            "OpSourceExtension                          [8 words]  'GL_ARB_separate_shader_objects'  (LiteralString)\n" +
            "OpName                                     [3 words]  4 'main'                          (IdRef, LiteralString)\n" +
            "OpName                                     [4 words]  9 'outColor'                      (IdRef, LiteralString)\n" +
            "OpName                                     [4 words]  12 'fragColor'                    (IdRef, LiteralString)\n" +
            "OpDecorate                                 [3 words]  9 Location                        (IdRef, Enumerated<Decoration>)\n" +
            "OpDecorate                                 [3 words]  12 Location                       (IdRef, Enumerated<Decoration>)\n" +
            "OpTypeVoid                                 [1 words]  2                                 (IdResult)\n" +
            "OpTypeFunction                             [2 words]  3 2 -                             (IdResult, IdRef, IdRef*)\n" +
            "OpTypeFloat                                [2 words]  6 32                              (IdResult, LiteralInteger)\n" +
            "OpTypeVector                               [3 words]  7 6 4                             (IdResult, IdRef, LiteralInteger)\n" +
            "OpTypePointer                              [3 words]  8 Output 7                        (IdResult, Enumerated<StorageClass>, IdRef)\n" +
            "OpVariable                                 [3 words]  8 9 Output -                      (IdResultType, IdResult, Enumerated<StorageClass>, IdRef?)\n" +
            "OpTypeVector                               [3 words]  10 6 3                            (IdResult, IdRef, LiteralInteger)\n" +
            "OpTypePointer                              [3 words]  11 Input 10                       (IdResult, Enumerated<StorageClass>, IdRef)\n" +
            "OpVariable                                 [3 words]  11 12 Input -                     (IdResultType, IdResult, Enumerated<StorageClass>, IdRef?)\n" +
            "OpConstant                                 [3 words]  6 14 1065353216                   (IdResultType, IdResult, LiteralContextDependentNumber)\n" +
            "OpFunction                                 [4 words]  2 4 None 3                        (IdResultType, IdResult, Enumerated<FunctionControl>, IdRef)\n" +
            "OpLabel                                    [1 words]  5                                 (IdResult)\n" +
            "OpLoad                                     [3 words]  10 13 12 -                        (IdResultType, IdResult, IdRef, Enumerated<MemoryAccess>?)\n" +
            "OpCompositeExtract                         [4 words]  6 15 13 [0]                       (IdResultType, IdResult, IdRef, LiteralInteger*)\n" +
            "OpCompositeExtract                         [4 words]  6 16 13 [1]                       (IdResultType, IdResult, IdRef, LiteralInteger*)\n" +
            "OpCompositeExtract                         [4 words]  6 17 13 [2]                       (IdResultType, IdResult, IdRef, LiteralInteger*)\n" +
            "OpCompositeConstruct                       [6 words]  7 18 [15,16,17,14]                (IdResultType, IdResult, IdRef*)\n" +
            "OpStore                                    [2 words]  9 18 -                            (IdRef, IdRef, Enumerated<MemoryAccess>?)\n" +
            "OpReturn                                   [0 words]                                    ()\n" +
            "OpFunctionEnd                              [0 words]                                    ()"),
                text);

    }

}
