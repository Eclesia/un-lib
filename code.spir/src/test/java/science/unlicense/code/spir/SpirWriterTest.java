
package science.unlicense.code.spir;

import org.junit.Test;
import science.unlicense.code.spir.model.Module;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class SpirWriterTest {

    @Test
    public void testWriter() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/code/spir/simpleFragment.spv"));
        final SpirReader reader = new SpirReader();
        reader.setInput(path);
        final Module module = reader.read();

        final ArrayOutputStream out = new ArrayOutputStream();
        final SpirWriter writer = new SpirWriter();
        writer.setOutput(out);
        writer.write(module);

        //compare content
        final ByteInputStream in = path.createInputStream();
        final byte[] expectedData = IOUtilities.readAll(in);
        final byte[] outData = out.getBuffer().toArrayByte();

        Assert.assertArrayEquals(expectedData, outData);


    }

}
