
package science.unlicense.code.spir;

import science.unlicense.format.json.JSONUtilities;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.concurrent.api.Paths;

/**
 * Utility class to generate java code.
 *
 * @author Johann Sorel
 */
public class SpirVCodeGenerator {

    private static final Chars ATT_MAJOR_VERSION = Chars.constant("major_version");
    private static final Chars ATT_MINOR_VERSION = Chars.constant("minor_version");
    private static final Chars ATT_REVISION = Chars.constant("revision");
    private static final Chars ATT_INSTRUCTIONS = Chars.constant("instructions");
    private static final Chars ATT_OPERANDS = Chars.constant("operands");
    private static final Chars ATT_CAPABILITIES = Chars.constant("capabilities");
    private static final Chars ATT_OPNAME = Chars.constant("opname");
    private static final Chars ATT_OPCODE = Chars.constant("opcode");
    private static final Chars ATT_KIND = Chars.constant("kind");
    private static final Chars ATT_NAME = Chars.constant("name");
    private static final Chars ATT_QUANTIFIER = Chars.constant("quantifier");

    private static final Chars ATT_OPERAND_KINDS = Chars.constant("operand_kinds");
    private static final Chars ATT_CATEGORY = Chars.constant("category");
    private static final Chars ATT_ENUMERANTS = Chars.constant("enumerants");
    private static final Chars ATT_ENUMERANT = Chars.constant("enumerant");
    private static final Chars ATT_VALUE = Chars.constant("value");
    private static final Chars ATT_PARAMETERS = Chars.constant("parameters");
    private static final Chars ATT_VERSION = Chars.constant("version");

   private static final Chars VAL_IDRESULTTYPE = Chars.constant("IdResultType");
   private static final Chars VAL_IDRESULT = Chars.constant("IdResult");
   private static final Chars VAL_IDMEMORYSEMANTICS = Chars.constant("IdMemorySemantics");
   private static final Chars VAL_IDSCOPE = Chars.constant("IdScope");
   private static final Chars VAL_IDREF = Chars.constant("IdRef");
   private static final Chars VAL_LITERALINTEGER = Chars.constant("LiteralInteger");
   private static final Chars VAL_LITERALSTRING = Chars.constant("LiteralString");
   private static final Chars VAL_LITERALCONTEXTDEPENDENTNUMBER = Chars.constant("LiteralContextDependentNumber");
   private static final Chars VAL_LITERALEXTINSTINTEGER = Chars.constant("LiteralExtInstInteger");
   private static final Chars VAL_LITERALSPECCONSTANTOPINTEGER = Chars.constant("LiteralSpecConstantOpInteger");
   private static final Chars VAL_PAIRLITERALINTEGERIDREF = Chars.constant("PairLiteralIntegerIdRef");
   private static final Chars VAL_PAIRIDREFLITERALINTEGER = Chars.constant("PairIdRefLiteralInteger");
   private static final Chars VAL_PAIRIDREFIDREF = Chars.constant("PairIdRefIdRef");
   private static final Set KINDS = new HashSet();
   static {
       KINDS.add(VAL_IDRESULTTYPE);
       KINDS.add(VAL_IDRESULT);
       KINDS.add(VAL_IDMEMORYSEMANTICS);
       KINDS.add(VAL_IDSCOPE);
       KINDS.add(VAL_IDREF);
       KINDS.add(VAL_LITERALINTEGER);
       KINDS.add(VAL_LITERALSTRING);
       KINDS.add(VAL_LITERALCONTEXTDEPENDENTNUMBER);
       KINDS.add(VAL_LITERALEXTINSTINTEGER);
       KINDS.add(VAL_LITERALSPECCONSTANTOPINTEGER);
       KINDS.add(VAL_PAIRLITERALINTEGERIDREF);
       KINDS.add(VAL_PAIRIDREFLITERALINTEGER);
       KINDS.add(VAL_PAIRIDREFIDREF);
   }

    private static final Chars CAT_VALUEENUM = Chars.constant("ValueEnum");
    private static final Chars CAT_BITENUM = Chars.constant("BitEnum");
    private static final Chars CAT_ID = Chars.constant("Id");
    private static final Chars CAT_LITERAL = Chars.constant("Literal");
    private static final Chars CAT_COMPOSITE = Chars.constant("Composite");


    public static void main(String[] args) throws Exception {

        final Path path = Paths.resolve(new Chars("file:/run/media/eclesia/SATA2_7200/dev/0_Specifications/Khronos_SpirV/spirv.core.grammar.json"));
        final Document doc = JSONUtilities.readAsDocument(path, null);

        final Integer major_version = (Integer) doc.getPropertyValue(ATT_MAJOR_VERSION);
        final Integer minor_version = (Integer) doc.getPropertyValue(ATT_MINOR_VERSION);
        final Integer revision = (Integer) doc.getPropertyValue(ATT_REVISION);
        final Object[] instructions = (Object[]) doc.getPropertyValue(ATT_INSTRUCTIONS);
        final Object[] operand_kinds = (Object[]) doc.getPropertyValue(ATT_OPERAND_KINDS);

        final Set dico = new HashSet();

        final CharBuffer cb = new CharBuffer();
        cb.append("public final class Ops {\n");

        for (int i=0;i<operand_kinds.length;i++) {
            final Document kdDoc = (Document) operand_kinds[i];
            final Chars category = (Chars) kdDoc.getPropertyValue(ATT_CATEGORY);
            final Chars kind = (Chars) kdDoc.getPropertyValue(ATT_KIND);
            final Object[] enumerants = (Object[]) kdDoc.getPropertyValue(ATT_ENUMERANTS);

            if (CAT_VALUEENUM.equals(category) || CAT_BITENUM.equals(category)) {
                dico.add(kind);

                cb.append('\n');
                cb.append("    public static enum "+kind+" {\n");

                final Dictionary values = new OrderedHashDictionary();
                for (int k=0;k<enumerants.length;k++) {
                    final Document enumDoc = (Document) enumerants[k];
                    final Chars enumerant = (Chars) enumDoc.getPropertyValue(ATT_ENUMERANT);
                    final Object value = enumDoc.getPropertyValue(ATT_VALUE);
                    cb.append("        "+enumerant).append("(").append(value).append(")");
                    values.add(enumerant, value);
                    if (k<enumerants.length-1) cb.append(",");
                    else cb.append(";");
                    cb.append('\n');
                }

                cb.append("        public final int value;\n");
                cb.append("        private "+kind+"(int value){\n");
                cb.append("            this.value = value;\n");
                cb.append("        }\n");
                cb.append("        public static "+kind+" valueOf(int value){\n");
                cb.append("            switch (value) {\n");
                final Iterator ite = values.getPairs().createIterator();
                while (ite.hasNext()) {
                    final Pair pair = (Pair) ite.next();
                    cb.append("                case "+pair.getValue2()+": return "+pair.getValue1()+";\n");
                }
                cb.append("                default : throw new InvalidArgumentException(\"Unknown operand value : \"+value);\n");
                cb.append("            }\n");
                cb.append("        }\n");

                cb.append("    }\n");

            } else {
                cb.append('\n');
                cb.append(kind);
                cb.append('\n');
            }
        }

        final CharBuffer index = new CharBuffer();
        index.append("    private static final Dictionary OPCODES = new HashDictionary();\n");
        index.append("    static {\n");

        for (int i=0;i<instructions.length;i++) {
            final Document insDoc = (Document) instructions[i];
            final Chars opname = (Chars) insDoc.getPropertyValue(ATT_OPNAME);
            final Integer opcode = (Integer) insDoc.getPropertyValue(ATT_OPCODE);
            final Object[] operands = (Object[]) insDoc.getPropertyValue(ATT_OPERANDS);
            final Object[] capabilities = (Object[]) insDoc.getPropertyValue(ATT_CAPABILITIES);

            cb.append("\n\n    public static final InstructionType ").append(opname).append(" = new InstructionType(\n");
            cb.append("        new Chars(\"").append(opname).append("\"),").append(Int32.encode(opcode)).append(", new OperandType[]{\n");

            index.append("        OPCODES.add("+opcode+", "+opname+");\n");

            if (operands != null) {
                for (int k=0;k<operands.length;k++) {
                    final Document opDoc = (Document) operands[k];
                    final Chars name = trimQuotes((Chars) opDoc.getPropertyValue(ATT_NAME));
                    final Chars kind = (Chars) opDoc.getPropertyValue(ATT_KIND);
                    Chars quantifier = (Chars) opDoc.getPropertyValue(ATT_QUANTIFIER);
                    if (quantifier == null || quantifier.isEmpty()) quantifier = new Chars("\u0000");

                    if (dico.contains(kind)) {
                        cb.append("            new OperandType.Enumerated(new Chars(\"").append(name).append("\"),"+kind+".class,"+quantifier.getUnicode(0)+")");
                    } else if (KINDS.contains(kind)) {
                        cb.append("            new OperandType."+kind+"(new Chars(\"").append(name).append("\"),"+quantifier.getUnicode(0)+")");
                    } else {
                        throw new Exception("Unexpected operand type "+kind);
                    }

                    if (k<operands.length-1) cb.append(",");
                    cb.append('\n');
                }
            }
            cb.append("        });\n");
        }

        index.append("    }\n");

        cb.append("\n}\n");

        System.out.println(cb.toString());
        System.out.println(index.toString());

    }

    private static Chars trimQuotes(Chars candidate) {
        if (candidate == null) return null;
        Chars str = candidate.truncate(1, candidate.getCharLength()-1);
        return str;
    }

}
