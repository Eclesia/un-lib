
package science.unlicense.code.spir.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;

/**
 *
 * @author Johann Sorel
 */
public final class Ops {

    private Ops(){}

    public static enum ImageOperands {
        None(0x0000),
        Bias(0x0001),
        Lod(0x0002),
        Grad(0x0004),
        ConstOffset(0x0008),
        Offset(0x0010),
        ConstOffsets(0x0020),
        Sample(0x0040),
        MinLod(0x0080);
        public final int value;
        private ImageOperands(int value){
            this.value = value;
        }
        public static ImageOperands valueOf(int value){
            switch (value) {
                case 0x0000: return None;
                case 0x0001: return Bias;
                case 0x0002: return Lod;
                case 0x0004: return Grad;
                case 0x0008: return ConstOffset;
                case 0x0010: return Offset;
                case 0x0020: return ConstOffsets;
                case 0x0040: return Sample;
                case 0x0080: return MinLod;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum FPFastMathMode {
        None(0x0000),
        NotNaN(0x0001),
        NotInf(0x0002),
        NSZ(0x0004),
        AllowRecip(0x0008),
        Fast(0x0010);
        public final int value;
        private FPFastMathMode(int value){
            this.value = value;
        }
        public static FPFastMathMode valueOf(int value){
            switch (value) {
                case 0x0000: return None;
                case 0x0001: return NotNaN;
                case 0x0002: return NotInf;
                case 0x0004: return NSZ;
                case 0x0008: return AllowRecip;
                case 0x0010: return Fast;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum SelectionControl {
        None(0x0000),
        Flatten(0x0001),
        DontFlatten(0x0002);
        public final int value;
        private SelectionControl(int value){
            this.value = value;
        }
        public static SelectionControl valueOf(int value){
            switch (value) {
                case 0x0000: return None;
                case 0x0001: return Flatten;
                case 0x0002: return DontFlatten;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum LoopControl {
        None(0x0000),
        Unroll(0x0001),
        DontUnroll(0x0002),
        DependencyInfinite(0x0004),
        DependencyLength(0x0008);
        public final int value;
        private LoopControl(int value){
            this.value = value;
        }
        public static LoopControl valueOf(int value){
            switch (value) {
                case 0x0000: return None;
                case 0x0001: return Unroll;
                case 0x0002: return DontUnroll;
                case 0x0004: return DependencyInfinite;
                case 0x0008: return DependencyLength;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum FunctionControl {
        None(0x0000),
        Inline(0x0001),
        DontInline(0x0002),
        Pure(0x0004),
        Const(0x0008);
        public final int value;
        private FunctionControl(int value){
            this.value = value;
        }
        public static FunctionControl valueOf(int value){
            switch (value) {
                case 0x0000: return None;
                case 0x0001: return Inline;
                case 0x0002: return DontInline;
                case 0x0004: return Pure;
                case 0x0008: return Const;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum MemorySemantics {
        Relaxed(0x0000),
        None(0x0000),
        Acquire(0x0002),
        Release(0x0004),
        AcquireRelease(0x0008),
        SequentiallyConsistent(0x0010),
        UniformMemory(0x0040),
        SubgroupMemory(0x0080),
        WorkgroupMemory(0x0100),
        CrossWorkgroupMemory(0x0200),
        AtomicCounterMemory(0x0400),
        ImageMemory(0x0800);
        public final int value;
        private MemorySemantics(int value){
            this.value = value;
        }
        public static MemorySemantics valueOf(int value){
            switch (value) {
                case 0x0000: return Relaxed;
//                case 0x0000: return None;
                case 0x0002: return Acquire;
                case 0x0004: return Release;
                case 0x0008: return AcquireRelease;
                case 0x0010: return SequentiallyConsistent;
                case 0x0040: return UniformMemory;
                case 0x0080: return SubgroupMemory;
                case 0x0100: return WorkgroupMemory;
                case 0x0200: return CrossWorkgroupMemory;
                case 0x0400: return AtomicCounterMemory;
                case 0x0800: return ImageMemory;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum MemoryAccess {
        None(0x0000),
        Volatile(0x0001),
        Aligned(0x0002),
        Nontemporal(0x0004);
        public final int value;
        private MemoryAccess(int value){
            this.value = value;
        }
        public static MemoryAccess valueOf(int value){
            switch (value) {
                case 0x0000: return None;
                case 0x0001: return Volatile;
                case 0x0002: return Aligned;
                case 0x0004: return Nontemporal;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum KernelProfilingInfo {
        None(0x0000),
        CmdExecTime(0x0001);
        public final int value;
        private KernelProfilingInfo(int value){
            this.value = value;
        }
        public static KernelProfilingInfo valueOf(int value){
            switch (value) {
                case 0x0000: return None;
                case 0x0001: return CmdExecTime;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum SourceLanguage {
        Unknown(0),
        ESSL(1),
        GLSL(2),
        OpenCL_C(3),
        OpenCL_CPP(4),
        HLSL(5);
        public final int value;
        private SourceLanguage(int value){
            this.value = value;
        }
        public static SourceLanguage valueOf(int value){
            switch (value) {
                case 0: return Unknown;
                case 1: return ESSL;
                case 2: return GLSL;
                case 3: return OpenCL_C;
                case 4: return OpenCL_CPP;
                case 5: return HLSL;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum ExecutionModel {
        Vertex(0),
        TessellationControl(1),
        TessellationEvaluation(2),
        Geometry(3),
        Fragment(4),
        GLCompute(5),
        Kernel(6);
        public final int value;
        private ExecutionModel(int value){
            this.value = value;
        }
        public static ExecutionModel valueOf(int value){
            switch (value) {
                case 0: return Vertex;
                case 1: return TessellationControl;
                case 2: return TessellationEvaluation;
                case 3: return Geometry;
                case 4: return Fragment;
                case 5: return GLCompute;
                case 6: return Kernel;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum AddressingModel {
        Logical(0),
        Physical32(1),
        Physical64(2);
        public final int value;
        private AddressingModel(int value){
            this.value = value;
        }
        public static AddressingModel valueOf(int value){
            switch (value) {
                case 0: return Logical;
                case 1: return Physical32;
                case 2: return Physical64;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum MemoryModel {
        Simple(0),
        GLSL450(1),
        OpenCL(2);
        public final int value;
        private MemoryModel(int value){
            this.value = value;
        }
        public static MemoryModel valueOf(int value){
            switch (value) {
                case 0: return Simple;
                case 1: return GLSL450;
                case 2: return OpenCL;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum ExecutionMode {
        Invocations(0),
        SpacingEqual(1),
        SpacingFractionalEven(2),
        SpacingFractionalOdd(3),
        VertexOrderCw(4),
        VertexOrderCcw(5),
        PixelCenterInteger(6),
        OriginUpperLeft(7),
        OriginLowerLeft(8),
        EarlyFragmentTests(9),
        PointMode(10),
        Xfb(11),
        DepthReplacing(12),
        DepthGreater(14),
        DepthLess(15),
        DepthUnchanged(16),
        LocalSize(17),
        LocalSizeHint(18),
        InputPoints(19),
        InputLines(20),
        InputLinesAdjacency(21),
        Triangles(22),
        InputTrianglesAdjacency(23),
        Quads(24),
        Isolines(25),
        OutputVertices(26),
        OutputPoints(27),
        OutputLineStrip(28),
        OutputTriangleStrip(29),
        VecTypeHint(30),
        ContractionOff(31),
        Initializer(33),
        Finalizer(34),
        SubgroupSize(35),
        SubgroupsPerWorkgroup(36),
        SubgroupsPerWorkgroupId(37),
        LocalSizeId(38),
        LocalSizeHintId(39),
        PostDepthCoverage(4446),
        StencilRefReplacingEXT(5027);
        public final int value;
        private ExecutionMode(int value){
            this.value = value;
        }
        public static ExecutionMode valueOf(int value){
            switch (value) {
                case 0: return Invocations;
                case 1: return SpacingEqual;
                case 2: return SpacingFractionalEven;
                case 3: return SpacingFractionalOdd;
                case 4: return VertexOrderCw;
                case 5: return VertexOrderCcw;
                case 6: return PixelCenterInteger;
                case 7: return OriginUpperLeft;
                case 8: return OriginLowerLeft;
                case 9: return EarlyFragmentTests;
                case 10: return PointMode;
                case 11: return Xfb;
                case 12: return DepthReplacing;
                case 14: return DepthGreater;
                case 15: return DepthLess;
                case 16: return DepthUnchanged;
                case 17: return LocalSize;
                case 18: return LocalSizeHint;
                case 19: return InputPoints;
                case 20: return InputLines;
                case 21: return InputLinesAdjacency;
                case 22: return Triangles;
                case 23: return InputTrianglesAdjacency;
                case 24: return Quads;
                case 25: return Isolines;
                case 26: return OutputVertices;
                case 27: return OutputPoints;
                case 28: return OutputLineStrip;
                case 29: return OutputTriangleStrip;
                case 30: return VecTypeHint;
                case 31: return ContractionOff;
                case 33: return Initializer;
                case 34: return Finalizer;
                case 35: return SubgroupSize;
                case 36: return SubgroupsPerWorkgroup;
                case 37: return SubgroupsPerWorkgroupId;
                case 38: return LocalSizeId;
                case 39: return LocalSizeHintId;
                case 4446: return PostDepthCoverage;
                case 5027: return StencilRefReplacingEXT;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum StorageClass {
        UniformConstant(0),
        Input(1),
        Uniform(2),
        Output(3),
        Workgroup(4),
        CrossWorkgroup(5),
        Private(6),
        Function(7),
        Generic(8),
        PushConstant(9),
        AtomicCounter(10),
        Image(11),
        StorageBuffer(12);
        public final int value;
        private StorageClass(int value){
            this.value = value;
        }
        public static StorageClass valueOf(int value){
            switch (value) {
                case 0: return UniformConstant;
                case 1: return Input;
                case 2: return Uniform;
                case 3: return Output;
                case 4: return Workgroup;
                case 5: return CrossWorkgroup;
                case 6: return Private;
                case 7: return Function;
                case 8: return Generic;
                case 9: return PushConstant;
                case 10: return AtomicCounter;
                case 11: return Image;
                case 12: return StorageBuffer;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum Dim {
        n1D(0),
        n2D(1),
        n3D(2),
        Cube(3),
        Rect(4),
        Buffer(5),
        SubpassData(6);
        public final int value;
        private Dim(int value){
            this.value = value;
        }
        public static Dim valueOf(int value){
            switch (value) {
                case 0: return n1D;
                case 1: return n2D;
                case 2: return n3D;
                case 3: return Cube;
                case 4: return Rect;
                case 5: return Buffer;
                case 6: return SubpassData;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum SamplerAddressingMode {
        None(0),
        ClampToEdge(1),
        Clamp(2),
        Repeat(3),
        RepeatMirrored(4);
        public final int value;
        private SamplerAddressingMode(int value){
            this.value = value;
        }
        public static SamplerAddressingMode valueOf(int value){
            switch (value) {
                case 0: return None;
                case 1: return ClampToEdge;
                case 2: return Clamp;
                case 3: return Repeat;
                case 4: return RepeatMirrored;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum SamplerFilterMode {
        Nearest(0),
        Linear(1);
        public final int value;
        private SamplerFilterMode(int value){
            this.value = value;
        }
        public static SamplerFilterMode valueOf(int value){
            switch (value) {
                case 0: return Nearest;
                case 1: return Linear;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum ImageFormat {
        Unknown(0),
        Rgba32f(1),
        Rgba16f(2),
        R32f(3),
        Rgba8(4),
        Rgba8Snorm(5),
        Rg32f(6),
        Rg16f(7),
        R11fG11fB10f(8),
        R16f(9),
        Rgba16(10),
        Rgb10A2(11),
        Rg16(12),
        Rg8(13),
        R16(14),
        R8(15),
        Rgba16Snorm(16),
        Rg16Snorm(17),
        Rg8Snorm(18),
        R16Snorm(19),
        R8Snorm(20),
        Rgba32i(21),
        Rgba16i(22),
        Rgba8i(23),
        R32i(24),
        Rg32i(25),
        Rg16i(26),
        Rg8i(27),
        R16i(28),
        R8i(29),
        Rgba32ui(30),
        Rgba16ui(31),
        Rgba8ui(32),
        R32ui(33),
        Rgb10a2ui(34),
        Rg32ui(35),
        Rg16ui(36),
        Rg8ui(37),
        R16ui(38),
        R8ui(39);
        public final int value;
        private ImageFormat(int value){
            this.value = value;
        }
        public static ImageFormat valueOf(int value){
            switch (value) {
                case 0: return Unknown;
                case 1: return Rgba32f;
                case 2: return Rgba16f;
                case 3: return R32f;
                case 4: return Rgba8;
                case 5: return Rgba8Snorm;
                case 6: return Rg32f;
                case 7: return Rg16f;
                case 8: return R11fG11fB10f;
                case 9: return R16f;
                case 10: return Rgba16;
                case 11: return Rgb10A2;
                case 12: return Rg16;
                case 13: return Rg8;
                case 14: return R16;
                case 15: return R8;
                case 16: return Rgba16Snorm;
                case 17: return Rg16Snorm;
                case 18: return Rg8Snorm;
                case 19: return R16Snorm;
                case 20: return R8Snorm;
                case 21: return Rgba32i;
                case 22: return Rgba16i;
                case 23: return Rgba8i;
                case 24: return R32i;
                case 25: return Rg32i;
                case 26: return Rg16i;
                case 27: return Rg8i;
                case 28: return R16i;
                case 29: return R8i;
                case 30: return Rgba32ui;
                case 31: return Rgba16ui;
                case 32: return Rgba8ui;
                case 33: return R32ui;
                case 34: return Rgb10a2ui;
                case 35: return Rg32ui;
                case 36: return Rg16ui;
                case 37: return Rg8ui;
                case 38: return R16ui;
                case 39: return R8ui;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum ImageChannelOrder {
        R(0),
        A(1),
        RG(2),
        RA(3),
        RGB(4),
        RGBA(5),
        BGRA(6),
        ARGB(7),
        Intensity(8),
        Luminance(9),
        Rx(10),
        RGx(11),
        RGBx(12),
        Depth(13),
        DepthStencil(14),
        sRGB(15),
        sRGBx(16),
        sRGBA(17),
        sBGRA(18),
        ABGR(19);
        public final int value;
        private ImageChannelOrder(int value){
            this.value = value;
        }
        public static ImageChannelOrder valueOf(int value){
            switch (value) {
                case 0: return R;
                case 1: return A;
                case 2: return RG;
                case 3: return RA;
                case 4: return RGB;
                case 5: return RGBA;
                case 6: return BGRA;
                case 7: return ARGB;
                case 8: return Intensity;
                case 9: return Luminance;
                case 10: return Rx;
                case 11: return RGx;
                case 12: return RGBx;
                case 13: return Depth;
                case 14: return DepthStencil;
                case 15: return sRGB;
                case 16: return sRGBx;
                case 17: return sRGBA;
                case 18: return sBGRA;
                case 19: return ABGR;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum ImageChannelDataType {
        SnormInt8(0),
        SnormInt16(1),
        UnormInt8(2),
        UnormInt16(3),
        UnormShort565(4),
        UnormShort555(5),
        UnormInt101010(6),
        SignedInt8(7),
        SignedInt16(8),
        SignedInt32(9),
        UnsignedInt8(10),
        UnsignedInt16(11),
        UnsignedInt32(12),
        HalfFloat(13),
        Float(14),
        UnormInt24(15),
        UnormInt101010_2(16);
        public final int value;
        private ImageChannelDataType(int value){
            this.value = value;
        }
        public static ImageChannelDataType valueOf(int value){
            switch (value) {
                case 0: return SnormInt8;
                case 1: return SnormInt16;
                case 2: return UnormInt8;
                case 3: return UnormInt16;
                case 4: return UnormShort565;
                case 5: return UnormShort555;
                case 6: return UnormInt101010;
                case 7: return SignedInt8;
                case 8: return SignedInt16;
                case 9: return SignedInt32;
                case 10: return UnsignedInt8;
                case 11: return UnsignedInt16;
                case 12: return UnsignedInt32;
                case 13: return HalfFloat;
                case 14: return Float;
                case 15: return UnormInt24;
                case 16: return UnormInt101010_2;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum FPRoundingMode {
        RTE(0),
        RTZ(1),
        RTP(2),
        RTN(3);
        public final int value;
        private FPRoundingMode(int value){
            this.value = value;
        }
        public static FPRoundingMode valueOf(int value){
            switch (value) {
                case 0: return RTE;
                case 1: return RTZ;
                case 2: return RTP;
                case 3: return RTN;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum LinkageType {
        Export(0),
        Import(1);
        public final int value;
        private LinkageType(int value){
            this.value = value;
        }
        public static LinkageType valueOf(int value){
            switch (value) {
                case 0: return Export;
                case 1: return Import;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum AccessQualifier {
        ReadOnly(0),
        WriteOnly(1),
        ReadWrite(2);
        public final int value;
        private AccessQualifier(int value){
            this.value = value;
        }
        public static AccessQualifier valueOf(int value){
            switch (value) {
                case 0: return ReadOnly;
                case 1: return WriteOnly;
                case 2: return ReadWrite;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum FunctionParameterAttribute {
        Zext(0),
        Sext(1),
        ByVal(2),
        Sret(3),
        NoAlias(4),
        NoCapture(5),
        NoWrite(6),
        NoReadWrite(7);
        public final int value;
        private FunctionParameterAttribute(int value){
            this.value = value;
        }
        public static FunctionParameterAttribute valueOf(int value){
            switch (value) {
                case 0: return Zext;
                case 1: return Sext;
                case 2: return ByVal;
                case 3: return Sret;
                case 4: return NoAlias;
                case 5: return NoCapture;
                case 6: return NoWrite;
                case 7: return NoReadWrite;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum Decoration {
        RelaxedPrecision(0),
        SpecId(1),
        Block(2),
        BufferBlock(3),
        RowMajor(4),
        ColMajor(5),
        ArrayStride(6),
        MatrixStride(7),
        GLSLShared(8),
        GLSLPacked(9),
        CPacked(10),
        BuiltIn(11),
        NoPerspective(13),
        Flat(14),
        Patch(15),
        Centroid(16),
        Sample(17),
        Invariant(18),
        Restrict(19),
        Aliased(20),
        Volatile(21),
        Constant(22),
        Coherent(23),
        NonWritable(24),
        NonReadable(25),
        Uniform(26),
        SaturatedConversion(28),
        Stream(29),
        Location(30),
        Component(31),
        Index(32),
        Binding(33),
        DescriptorSet(34),
        Offset(35),
        XfbBuffer(36),
        XfbStride(37),
        FuncParamAttr(38),
        FPRoundingMode(39),
        FPFastMathMode(40),
        LinkageAttributes(41),
        NoContraction(42),
        InputAttachmentIndex(43),
        Alignment(44),
        MaxByteOffset(45),
        AlignmentId(46),
        MaxByteOffsetId(47),
        ExplicitInterpAMD(4999),
        OverrideCoverageNV(5248),
        PassthroughNV(5250),
        ViewportRelativeNV(5252),
        SecondaryViewportRelativeNV(5256),
        HlslCounterBufferGOOGLE(5634),
        HlslSemanticGOOGLE(5635);
        public final int value;
        private Decoration(int value){
            this.value = value;
        }
        public static Decoration valueOf(int value){
            switch (value) {
                case 0: return RelaxedPrecision;
                case 1: return SpecId;
                case 2: return Block;
                case 3: return BufferBlock;
                case 4: return RowMajor;
                case 5: return ColMajor;
                case 6: return ArrayStride;
                case 7: return MatrixStride;
                case 8: return GLSLShared;
                case 9: return GLSLPacked;
                case 10: return CPacked;
                case 11: return BuiltIn;
                case 13: return NoPerspective;
                case 14: return Flat;
                case 15: return Patch;
                case 16: return Centroid;
                case 17: return Sample;
                case 18: return Invariant;
                case 19: return Restrict;
                case 20: return Aliased;
                case 21: return Volatile;
                case 22: return Constant;
                case 23: return Coherent;
                case 24: return NonWritable;
                case 25: return NonReadable;
                case 26: return Uniform;
                case 28: return SaturatedConversion;
                case 29: return Stream;
                case 30: return Location;
                case 31: return Component;
                case 32: return Index;
                case 33: return Binding;
                case 34: return DescriptorSet;
                case 35: return Offset;
                case 36: return XfbBuffer;
                case 37: return XfbStride;
                case 38: return FuncParamAttr;
                case 39: return FPRoundingMode;
                case 40: return FPFastMathMode;
                case 41: return LinkageAttributes;
                case 42: return NoContraction;
                case 43: return InputAttachmentIndex;
                case 44: return Alignment;
                case 45: return MaxByteOffset;
                case 46: return AlignmentId;
                case 47: return MaxByteOffsetId;
                case 4999: return ExplicitInterpAMD;
                case 5248: return OverrideCoverageNV;
                case 5250: return PassthroughNV;
                case 5252: return ViewportRelativeNV;
                case 5256: return SecondaryViewportRelativeNV;
                case 5634: return HlslCounterBufferGOOGLE;
                case 5635: return HlslSemanticGOOGLE;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum BuiltIn {
        Position(0),
        PointSize(1),
        ClipDistance(3),
        CullDistance(4),
        VertexId(5),
        InstanceId(6),
        PrimitiveId(7),
        InvocationId(8),
        Layer(9),
        ViewportIndex(10),
        TessLevelOuter(11),
        TessLevelInner(12),
        TessCoord(13),
        PatchVertices(14),
        FragCoord(15),
        PointCoord(16),
        FrontFacing(17),
        SampleId(18),
        SamplePosition(19),
        SampleMask(20),
        FragDepth(22),
        HelperInvocation(23),
        NumWorkgroups(24),
        WorkgroupSize(25),
        WorkgroupId(26),
        LocalInvocationId(27),
        GlobalInvocationId(28),
        LocalInvocationIndex(29),
        WorkDim(30),
        GlobalSize(31),
        EnqueuedWorkgroupSize(32),
        GlobalOffset(33),
        GlobalLinearId(34),
        SubgroupSize(36),
        SubgroupMaxSize(37),
        NumSubgroups(38),
        NumEnqueuedSubgroups(39),
        SubgroupId(40),
        SubgroupLocalInvocationId(41),
        VertexIndex(42),
        InstanceIndex(43),
        SubgroupEqMask(4416),
        SubgroupGeMask(4417),
        SubgroupGtMask(4418),
        SubgroupLeMask(4419),
        SubgroupLtMask(4420),
        SubgroupEqMaskKHR(4416),
        SubgroupGeMaskKHR(4417),
        SubgroupGtMaskKHR(4418),
        SubgroupLeMaskKHR(4419),
        SubgroupLtMaskKHR(4420),
        BaseVertex(4424),
        BaseInstance(4425),
        DrawIndex(4426),
        DeviceIndex(4438),
        ViewIndex(4440),
        BaryCoordNoPerspAMD(4992),
        BaryCoordNoPerspCentroidAMD(4993),
        BaryCoordNoPerspSampleAMD(4994),
        BaryCoordSmoothAMD(4995),
        BaryCoordSmoothCentroidAMD(4996),
        BaryCoordSmoothSampleAMD(4997),
        BaryCoordPullModelAMD(4998),
        FragStencilRefEXT(5014),
        ViewportMaskNV(5253),
        SecondaryPositionNV(5257),
        SecondaryViewportMaskNV(5258),
        PositionPerViewNV(5261),
        ViewportMaskPerViewNV(5262),
        FullyCoveredEXT(5264);
        public final int value;
        private BuiltIn(int value){
            this.value = value;
        }
        public static BuiltIn valueOf(int value){
            switch (value) {
                case 0: return Position;
                case 1: return PointSize;
                case 3: return ClipDistance;
                case 4: return CullDistance;
                case 5: return VertexId;
                case 6: return InstanceId;
                case 7: return PrimitiveId;
                case 8: return InvocationId;
                case 9: return Layer;
                case 10: return ViewportIndex;
                case 11: return TessLevelOuter;
                case 12: return TessLevelInner;
                case 13: return TessCoord;
                case 14: return PatchVertices;
                case 15: return FragCoord;
                case 16: return PointCoord;
                case 17: return FrontFacing;
                case 18: return SampleId;
                case 19: return SamplePosition;
                case 20: return SampleMask;
                case 22: return FragDepth;
                case 23: return HelperInvocation;
                case 24: return NumWorkgroups;
                case 25: return WorkgroupSize;
                case 26: return WorkgroupId;
                case 27: return LocalInvocationId;
                case 28: return GlobalInvocationId;
                case 29: return LocalInvocationIndex;
                case 30: return WorkDim;
                case 31: return GlobalSize;
                case 32: return EnqueuedWorkgroupSize;
                case 33: return GlobalOffset;
                case 34: return GlobalLinearId;
                case 36: return SubgroupSize;
                case 37: return SubgroupMaxSize;
                case 38: return NumSubgroups;
                case 39: return NumEnqueuedSubgroups;
                case 40: return SubgroupId;
                case 41: return SubgroupLocalInvocationId;
                case 42: return VertexIndex;
                case 43: return InstanceIndex;
                case 4416: return SubgroupEqMask;
                case 4417: return SubgroupGeMask;
                case 4418: return SubgroupGtMask;
                case 4419: return SubgroupLeMask;
                case 4420: return SubgroupLtMask;
//                case 4416: return SubgroupEqMaskKHR;
//                case 4417: return SubgroupGeMaskKHR;
//                case 4418: return SubgroupGtMaskKHR;
//                case 4419: return SubgroupLeMaskKHR;
//                case 4420: return SubgroupLtMaskKHR;
                case 4424: return BaseVertex;
                case 4425: return BaseInstance;
                case 4426: return DrawIndex;
                case 4438: return DeviceIndex;
                case 4440: return ViewIndex;
                case 4992: return BaryCoordNoPerspAMD;
                case 4993: return BaryCoordNoPerspCentroidAMD;
                case 4994: return BaryCoordNoPerspSampleAMD;
                case 4995: return BaryCoordSmoothAMD;
                case 4996: return BaryCoordSmoothCentroidAMD;
                case 4997: return BaryCoordSmoothSampleAMD;
                case 4998: return BaryCoordPullModelAMD;
                case 5014: return FragStencilRefEXT;
                case 5253: return ViewportMaskNV;
                case 5257: return SecondaryPositionNV;
                case 5258: return SecondaryViewportMaskNV;
                case 5261: return PositionPerViewNV;
                case 5262: return ViewportMaskPerViewNV;
                case 5264: return FullyCoveredEXT;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum Scope {
        CrossDevice(0),
        Device(1),
        Workgroup(2),
        Subgroup(3),
        Invocation(4);
        public final int value;
        private Scope(int value){
            this.value = value;
        }
        public static Scope valueOf(int value){
            switch (value) {
                case 0: return CrossDevice;
                case 1: return Device;
                case 2: return Workgroup;
                case 3: return Subgroup;
                case 4: return Invocation;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum GroupOperation {
        Reduce(0),
        InclusiveScan(1),
        ExclusiveScan(2),
        ClusteredReduce(3);
        public final int value;
        private GroupOperation(int value){
            this.value = value;
        }
        public static GroupOperation valueOf(int value){
            switch (value) {
                case 0: return Reduce;
                case 1: return InclusiveScan;
                case 2: return ExclusiveScan;
                case 3: return ClusteredReduce;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum KernelEnqueueFlags {
        NoWait(0),
        WaitKernel(1),
        WaitWorkGroup(2);
        public final int value;
        private KernelEnqueueFlags(int value){
            this.value = value;
        }
        public static KernelEnqueueFlags valueOf(int value){
            switch (value) {
                case 0: return NoWait;
                case 1: return WaitKernel;
                case 2: return WaitWorkGroup;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static enum Capability {
        Matrix(0),
        Shader(1),
        Geometry(2),
        Tessellation(3),
        Addresses(4),
        Linkage(5),
        Kernel(6),
        Vector16(7),
        Float16Buffer(8),
        Float16(9),
        Float64(10),
        Int64(11),
        Int64Atomics(12),
        ImageBasic(13),
        ImageReadWrite(14),
        ImageMipmap(15),
        Pipes(17),
        Groups(18),
        DeviceEnqueue(19),
        LiteralSampler(20),
        AtomicStorage(21),
        Int16(22),
        TessellationPointSize(23),
        GeometryPointSize(24),
        ImageGatherExtended(25),
        StorageImageMultisample(27),
        UniformBufferArrayDynamicIndexing(28),
        SampledImageArrayDynamicIndexing(29),
        StorageBufferArrayDynamicIndexing(30),
        StorageImageArrayDynamicIndexing(31),
        ClipDistance(32),
        CullDistance(33),
        ImageCubeArray(34),
        SampleRateShading(35),
        ImageRect(36),
        SampledRect(37),
        GenericPointer(38),
        Int8(39),
        InputAttachment(40),
        SparseResidency(41),
        MinLod(42),
        Sampled1D(43),
        Image1D(44),
        SampledCubeArray(45),
        SampledBuffer(46),
        ImageBuffer(47),
        ImageMSArray(48),
        StorageImageExtendedFormats(49),
        ImageQuery(50),
        DerivativeControl(51),
        InterpolationFunction(52),
        TransformFeedback(53),
        GeometryStreams(54),
        StorageImageReadWithoutFormat(55),
        StorageImageWriteWithoutFormat(56),
        MultiViewport(57),
        SubgroupDispatch(58),
        NamedBarrier(59),
        PipeStorage(60),
        GroupNonUniform(61),
        GroupNonUniformVote(62),
        GroupNonUniformArithmetic(63),
        GroupNonUniformBallot(64),
        GroupNonUniformShuffle(65),
        GroupNonUniformShuffleRelative(66),
        GroupNonUniformClustered(67),
        GroupNonUniformQuad(68),
        SubgroupBallotKHR(4423),
        DrawParameters(4427),
        SubgroupVoteKHR(4431),
        StorageBuffer16BitAccess(4433),
        StorageUniformBufferBlock16(4433),
        UniformAndStorageBuffer16BitAccess(4434),
        StorageUniform16(4434),
        StoragePushConstant16(4435),
        StorageInputOutput16(4436),
        DeviceGroup(4437),
        MultiView(4439),
        VariablePointersStorageBuffer(4441),
        VariablePointers(4442),
        AtomicStorageOps(4445),
        SampleMaskPostDepthCoverage(4447),
        Float16ImageAMD(5008),
        ImageGatherBiasLodAMD(5009),
        FragmentMaskAMD(5010),
        StencilExportEXT(5013),
        ImageReadWriteLodAMD(5015),
        SampleMaskOverrideCoverageNV(5249),
        GeometryShaderPassthroughNV(5251),
        ShaderViewportIndexLayerEXT(5254),
        ShaderViewportIndexLayerNV(5254),
        ShaderViewportMaskNV(5255),
        ShaderStereoViewNV(5259),
        PerViewAttributesNV(5260),
        FragmentFullyCoveredEXT(5265),
        SubgroupShuffleINTEL(5568),
        SubgroupBufferBlockIOINTEL(5569),
        SubgroupImageBlockIOINTEL(5570);
        public final int value;
        private Capability(int value){
            this.value = value;
        }
        public static Capability valueOf(int value){
            switch (value) {
                case 0: return Matrix;
                case 1: return Shader;
                case 2: return Geometry;
                case 3: return Tessellation;
                case 4: return Addresses;
                case 5: return Linkage;
                case 6: return Kernel;
                case 7: return Vector16;
                case 8: return Float16Buffer;
                case 9: return Float16;
                case 10: return Float64;
                case 11: return Int64;
                case 12: return Int64Atomics;
                case 13: return ImageBasic;
                case 14: return ImageReadWrite;
                case 15: return ImageMipmap;
                case 17: return Pipes;
                case 18: return Groups;
                case 19: return DeviceEnqueue;
                case 20: return LiteralSampler;
                case 21: return AtomicStorage;
                case 22: return Int16;
                case 23: return TessellationPointSize;
                case 24: return GeometryPointSize;
                case 25: return ImageGatherExtended;
                case 27: return StorageImageMultisample;
                case 28: return UniformBufferArrayDynamicIndexing;
                case 29: return SampledImageArrayDynamicIndexing;
                case 30: return StorageBufferArrayDynamicIndexing;
                case 31: return StorageImageArrayDynamicIndexing;
                case 32: return ClipDistance;
                case 33: return CullDistance;
                case 34: return ImageCubeArray;
                case 35: return SampleRateShading;
                case 36: return ImageRect;
                case 37: return SampledRect;
                case 38: return GenericPointer;
                case 39: return Int8;
                case 40: return InputAttachment;
                case 41: return SparseResidency;
                case 42: return MinLod;
                case 43: return Sampled1D;
                case 44: return Image1D;
                case 45: return SampledCubeArray;
                case 46: return SampledBuffer;
                case 47: return ImageBuffer;
                case 48: return ImageMSArray;
                case 49: return StorageImageExtendedFormats;
                case 50: return ImageQuery;
                case 51: return DerivativeControl;
                case 52: return InterpolationFunction;
                case 53: return TransformFeedback;
                case 54: return GeometryStreams;
                case 55: return StorageImageReadWithoutFormat;
                case 56: return StorageImageWriteWithoutFormat;
                case 57: return MultiViewport;
                case 58: return SubgroupDispatch;
                case 59: return NamedBarrier;
                case 60: return PipeStorage;
                case 61: return GroupNonUniform;
                case 62: return GroupNonUniformVote;
                case 63: return GroupNonUniformArithmetic;
                case 64: return GroupNonUniformBallot;
                case 65: return GroupNonUniformShuffle;
                case 66: return GroupNonUniformShuffleRelative;
                case 67: return GroupNonUniformClustered;
                case 68: return GroupNonUniformQuad;
                case 4423: return SubgroupBallotKHR;
                case 4427: return DrawParameters;
                case 4431: return SubgroupVoteKHR;
                case 4433: return StorageBuffer16BitAccess;
//                case 4433: return StorageUniformBufferBlock16;
                case 4434: return UniformAndStorageBuffer16BitAccess;
//                case 4434: return StorageUniform16;
                case 4435: return StoragePushConstant16;
                case 4436: return StorageInputOutput16;
                case 4437: return DeviceGroup;
                case 4439: return MultiView;
                case 4441: return VariablePointersStorageBuffer;
                case 4442: return VariablePointers;
                case 4445: return AtomicStorageOps;
                case 4447: return SampleMaskPostDepthCoverage;
                case 5008: return Float16ImageAMD;
                case 5009: return ImageGatherBiasLodAMD;
                case 5010: return FragmentMaskAMD;
                case 5013: return StencilExportEXT;
                case 5015: return ImageReadWriteLodAMD;
                case 5249: return SampleMaskOverrideCoverageNV;
                case 5251: return GeometryShaderPassthroughNV;
                case 5254: return ShaderViewportIndexLayerEXT;
//                case 5254: return ShaderViewportIndexLayerNV;
                case 5255: return ShaderViewportMaskNV;
                case 5259: return ShaderStereoViewNV;
                case 5260: return PerViewAttributesNV;
                case 5265: return FragmentFullyCoveredEXT;
                case 5568: return SubgroupShuffleINTEL;
                case 5569: return SubgroupBufferBlockIOINTEL;
                case 5570: return SubgroupImageBlockIOINTEL;
                default : throw new InvalidArgumentException("Unknown operand value : "+value);
            }
        }
    }

    public static final InstructionType OpNop = new InstructionType(
        new Chars("OpNop"),0, new OperandType[]{
        });


    public static final InstructionType OpUndef = new InstructionType(
        new Chars("OpUndef"),1, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpSourceContinued = new InstructionType(
        new Chars("OpSourceContinued"),2, new OperandType[]{
            new OperandType.LiteralString(new Chars("Continued Source"),0)
        });


    public static final InstructionType OpSource = new InstructionType(
        new Chars("OpSource"),3, new OperandType[]{
            new OperandType.Enumerated(new Chars(""),SourceLanguage.class,0),
            new OperandType.LiteralInteger(new Chars("Version"),0),
            new OperandType.IdRef(new Chars("File"),63),
            new OperandType.LiteralString(new Chars("Source"),63)
        });


    public static final InstructionType OpSourceExtension = new InstructionType(
        new Chars("OpSourceExtension"),4, new OperandType[]{
            new OperandType.LiteralString(new Chars("Extension"),0)
        });


    public static final InstructionType OpName = new InstructionType(
        new Chars("OpName"),5, new OperandType[]{
            new OperandType.IdRef(new Chars("Target"),0),
            new OperandType.LiteralString(new Chars("Name"),0)
        });


    public static final InstructionType OpMemberName = new InstructionType(
        new Chars("OpMemberName"),6, new OperandType[]{
            new OperandType.IdRef(new Chars("Type"),0),
            new OperandType.LiteralInteger(new Chars("Member"),0),
            new OperandType.LiteralString(new Chars("Name"),0)
        });


    public static final InstructionType OpString = new InstructionType(
        new Chars("OpString"),7, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.LiteralString(new Chars("String"),0)
        });


    public static final InstructionType OpLine = new InstructionType(
        new Chars("OpLine"),8, new OperandType[]{
            new OperandType.IdRef(new Chars("File"),0),
            new OperandType.LiteralInteger(new Chars("Line"),0),
            new OperandType.LiteralInteger(new Chars("Column"),0)
        });


    public static final InstructionType OpExtension = new InstructionType(
        new Chars("OpExtension"),10, new OperandType[]{
            new OperandType.LiteralString(new Chars("Name"),0)
        });


    public static final InstructionType OpExtInstImport = new InstructionType(
        new Chars("OpExtInstImport"),11, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.LiteralString(new Chars("Name"),0)
        });


    public static final InstructionType OpExtInst = new InstructionType(
        new Chars("OpExtInst"),12, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Set"),0),
            new OperandType.LiteralExtInstInteger(new Chars("Instruction"),0),
            new OperandType.IdRef(new Chars("Operand 1', +\n'Operand 2', +\n.."),42)
        });


    public static final InstructionType OpMemoryModel = new InstructionType(
        new Chars("OpMemoryModel"),14, new OperandType[]{
            new OperandType.Enumerated(new Chars(""),AddressingModel.class,0),
            new OperandType.Enumerated(new Chars(""),MemoryModel.class,0)
        });


    public static final InstructionType OpEntryPoint = new InstructionType(
        new Chars("OpEntryPoint"),15, new OperandType[]{
            new OperandType.Enumerated(new Chars(""),ExecutionModel.class,0),
            new OperandType.IdRef(new Chars("Entry Point"),0),
            new OperandType.LiteralString(new Chars("Name"),0),
            new OperandType.IdRef(new Chars("Interface"),42)
        });


    public static final InstructionType OpExecutionMode = new InstructionType(
        new Chars("OpExecutionMode"),16, new OperandType[]{
            new OperandType.IdRef(new Chars("Entry Point"),0),
            new OperandType.Enumerated(new Chars("Mode"),ExecutionMode.class,0)
        });


    public static final InstructionType OpCapability = new InstructionType(
        new Chars("OpCapability"),17, new OperandType[]{
            new OperandType.Enumerated(new Chars("Capability"),Capability.class,0)
        });


    public static final InstructionType OpTypeVoid = new InstructionType(
        new Chars("OpTypeVoid"),19, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpTypeBool = new InstructionType(
        new Chars("OpTypeBool"),20, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpTypeInt = new InstructionType(
        new Chars("OpTypeInt"),21, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.LiteralInteger(new Chars("Width"),0),
            new OperandType.LiteralInteger(new Chars("Signedness"),0)
        });


    public static final InstructionType OpTypeFloat = new InstructionType(
        new Chars("OpTypeFloat"),22, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.LiteralInteger(new Chars("Width"),0)
        });


    public static final InstructionType OpTypeVector = new InstructionType(
        new Chars("OpTypeVector"),23, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Component Type"),0),
            new OperandType.LiteralInteger(new Chars("Component Count"),0)
        });


    public static final InstructionType OpTypeMatrix = new InstructionType(
        new Chars("OpTypeMatrix"),24, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Column Type"),0),
            new OperandType.LiteralInteger(new Chars("Column Count"),0)
        });


    public static final InstructionType OpTypeImage = new InstructionType(
        new Chars("OpTypeImage"),25, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Type"),0),
            new OperandType.Enumerated(new Chars(""),Dim.class,0),
            new OperandType.LiteralInteger(new Chars("Depth"),0),
            new OperandType.LiteralInteger(new Chars("Arrayed"),0),
            new OperandType.LiteralInteger(new Chars("MS"),0),
            new OperandType.LiteralInteger(new Chars("Sampled"),0),
            new OperandType.Enumerated(new Chars(""),ImageFormat.class,0),
            new OperandType.Enumerated(new Chars(""),AccessQualifier.class,63)
        });


    public static final InstructionType OpTypeSampler = new InstructionType(
        new Chars("OpTypeSampler"),26, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpTypeSampledImage = new InstructionType(
        new Chars("OpTypeSampledImage"),27, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image Type"),0)
        });


    public static final InstructionType OpTypeArray = new InstructionType(
        new Chars("OpTypeArray"),28, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Element Type"),0),
            new OperandType.IdRef(new Chars("Length"),0)
        });


    public static final InstructionType OpTypeRuntimeArray = new InstructionType(
        new Chars("OpTypeRuntimeArray"),29, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Element Type"),0)
        });


    public static final InstructionType OpTypeStruct = new InstructionType(
        new Chars("OpTypeStruct"),30, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Member 0 type', +\n'member 1 type', +\n.."),42)
        });


    public static final InstructionType OpTypeOpaque = new InstructionType(
        new Chars("OpTypeOpaque"),31, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.LiteralString(new Chars("he name of the opaque type"),0)
        });


    public static final InstructionType OpTypePointer = new InstructionType(
        new Chars("OpTypePointer"),32, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.Enumerated(new Chars(""),StorageClass.class,0),
            new OperandType.IdRef(new Chars("Type"),0)
        });


    public static final InstructionType OpTypeFunction = new InstructionType(
        new Chars("OpTypeFunction"),33, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Return Type"),0),
            new OperandType.IdRef(new Chars("Parameter 0 Type', +\n'Parameter 1 Type', +\n.."),42)
        });


    public static final InstructionType OpTypeEvent = new InstructionType(
        new Chars("OpTypeEvent"),34, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpTypeDeviceEvent = new InstructionType(
        new Chars("OpTypeDeviceEvent"),35, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpTypeReserveId = new InstructionType(
        new Chars("OpTypeReserveId"),36, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpTypeQueue = new InstructionType(
        new Chars("OpTypeQueue"),37, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpTypePipe = new InstructionType(
        new Chars("OpTypePipe"),38, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.Enumerated(new Chars("Qualifier"),AccessQualifier.class,0)
        });


    public static final InstructionType OpTypeForwardPointer = new InstructionType(
        new Chars("OpTypeForwardPointer"),39, new OperandType[]{
            new OperandType.IdRef(new Chars("Pointer Type"),0),
            new OperandType.Enumerated(new Chars(""),StorageClass.class,0)
        });


    public static final InstructionType OpConstantTrue = new InstructionType(
        new Chars("OpConstantTrue"),41, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpConstantFalse = new InstructionType(
        new Chars("OpConstantFalse"),42, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpConstant = new InstructionType(
        new Chars("OpConstant"),43, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.LiteralContextDependentNumber(new Chars("Value"),0)
        });


    public static final InstructionType OpConstantComposite = new InstructionType(
        new Chars("OpConstantComposite"),44, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Constituents"),42)
        });


    public static final InstructionType OpConstantSampler = new InstructionType(
        new Chars("OpConstantSampler"),45, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.Enumerated(new Chars(""),SamplerAddressingMode.class,0),
            new OperandType.LiteralInteger(new Chars("Param"),0),
            new OperandType.Enumerated(new Chars(""),SamplerFilterMode.class,0)
        });


    public static final InstructionType OpConstantNull = new InstructionType(
        new Chars("OpConstantNull"),46, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpSpecConstantTrue = new InstructionType(
        new Chars("OpSpecConstantTrue"),48, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpSpecConstantFalse = new InstructionType(
        new Chars("OpSpecConstantFalse"),49, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpSpecConstant = new InstructionType(
        new Chars("OpSpecConstant"),50, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.LiteralContextDependentNumber(new Chars("Value"),0)
        });


    public static final InstructionType OpSpecConstantComposite = new InstructionType(
        new Chars("OpSpecConstantComposite"),51, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Constituents"),42)
        });


    public static final InstructionType OpSpecConstantOp = new InstructionType(
        new Chars("OpSpecConstantOp"),52, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.LiteralSpecConstantOpInteger(new Chars("Opcode"),0)
        });


    public static final InstructionType OpFunction = new InstructionType(
        new Chars("OpFunction"),54, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.Enumerated(new Chars(""),FunctionControl.class,0),
            new OperandType.IdRef(new Chars("Function Type"),0)
        });


    public static final InstructionType OpFunctionParameter = new InstructionType(
        new Chars("OpFunctionParameter"),55, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpFunctionEnd = new InstructionType(
        new Chars("OpFunctionEnd"),56, new OperandType[]{
        });


    public static final InstructionType OpFunctionCall = new InstructionType(
        new Chars("OpFunctionCall"),57, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Function"),0),
            new OperandType.IdRef(new Chars("Argument 0', +\n'Argument 1', +\n.."),42)
        });


    public static final InstructionType OpVariable = new InstructionType(
        new Chars("OpVariable"),59, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.Enumerated(new Chars(""),StorageClass.class,0),
            new OperandType.IdRef(new Chars("Initializer"),63)
        });


    public static final InstructionType OpImageTexelPointer = new InstructionType(
        new Chars("OpImageTexelPointer"),60, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("Sample"),0)
        });


    public static final InstructionType OpLoad = new InstructionType(
        new Chars("OpLoad"),61, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.Enumerated(new Chars(""),MemoryAccess.class,63)
        });


    public static final InstructionType OpStore = new InstructionType(
        new Chars("OpStore"),62, new OperandType[]{
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdRef(new Chars("Object"),0),
            new OperandType.Enumerated(new Chars(""),MemoryAccess.class,63)
        });


    public static final InstructionType OpCopyMemory = new InstructionType(
        new Chars("OpCopyMemory"),63, new OperandType[]{
            new OperandType.IdRef(new Chars("Target"),0),
            new OperandType.IdRef(new Chars("Source"),0),
            new OperandType.Enumerated(new Chars(""),MemoryAccess.class,63)
        });


    public static final InstructionType OpCopyMemorySized = new InstructionType(
        new Chars("OpCopyMemorySized"),64, new OperandType[]{
            new OperandType.IdRef(new Chars("Target"),0),
            new OperandType.IdRef(new Chars("Source"),0),
            new OperandType.IdRef(new Chars("Size"),0),
            new OperandType.Enumerated(new Chars(""),MemoryAccess.class,63)
        });


    public static final InstructionType OpAccessChain = new InstructionType(
        new Chars("OpAccessChain"),65, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0),
            new OperandType.IdRef(new Chars("Indexes"),42)
        });


    public static final InstructionType OpInBoundsAccessChain = new InstructionType(
        new Chars("OpInBoundsAccessChain"),66, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0),
            new OperandType.IdRef(new Chars("Indexes"),42)
        });


    public static final InstructionType OpPtrAccessChain = new InstructionType(
        new Chars("OpPtrAccessChain"),67, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0),
            new OperandType.IdRef(new Chars("Element"),0),
            new OperandType.IdRef(new Chars("Indexes"),42)
        });


    public static final InstructionType OpArrayLength = new InstructionType(
        new Chars("OpArrayLength"),68, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Structure"),0),
            new OperandType.LiteralInteger(new Chars("Array member"),0)
        });


    public static final InstructionType OpGenericPtrMemSemantics = new InstructionType(
        new Chars("OpGenericPtrMemSemantics"),69, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0)
        });


    public static final InstructionType OpInBoundsPtrAccessChain = new InstructionType(
        new Chars("OpInBoundsPtrAccessChain"),70, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0),
            new OperandType.IdRef(new Chars("Element"),0),
            new OperandType.IdRef(new Chars("Indexes"),42)
        });


    public static final InstructionType OpDecorate = new InstructionType(
        new Chars("OpDecorate"),71, new OperandType[]{
            new OperandType.IdRef(new Chars("Target"),0),
            new OperandType.Enumerated(new Chars(""),Decoration.class,0)
        });


    public static final InstructionType OpMemberDecorate = new InstructionType(
        new Chars("OpMemberDecorate"),72, new OperandType[]{
            new OperandType.IdRef(new Chars("Structure Type"),0),
            new OperandType.LiteralInteger(new Chars("Member"),0),
            new OperandType.Enumerated(new Chars(""),Decoration.class,0)
        });


    public static final InstructionType OpDecorationGroup = new InstructionType(
        new Chars("OpDecorationGroup"),73, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpGroupDecorate = new InstructionType(
        new Chars("OpGroupDecorate"),74, new OperandType[]{
            new OperandType.IdRef(new Chars("Decoration Group"),0),
            new OperandType.IdRef(new Chars("Targets"),42)
        });


    public static final InstructionType OpGroupMemberDecorate = new InstructionType(
        new Chars("OpGroupMemberDecorate"),75, new OperandType[]{
            new OperandType.IdRef(new Chars("Decoration Group"),0),
            new OperandType.PairIdRefLiteralInteger(new Chars("Targets"),42)
        });


    public static final InstructionType OpVectorExtractDynamic = new InstructionType(
        new Chars("OpVectorExtractDynamic"),77, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Vector"),0),
            new OperandType.IdRef(new Chars("Index"),0)
        });


    public static final InstructionType OpVectorInsertDynamic = new InstructionType(
        new Chars("OpVectorInsertDynamic"),78, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Vector"),0),
            new OperandType.IdRef(new Chars("Component"),0),
            new OperandType.IdRef(new Chars("Index"),0)
        });


    public static final InstructionType OpVectorShuffle = new InstructionType(
        new Chars("OpVectorShuffle"),79, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Vector 1"),0),
            new OperandType.IdRef(new Chars("Vector 2"),0),
            new OperandType.LiteralInteger(new Chars("Components"),42)
        });


    public static final InstructionType OpCompositeConstruct = new InstructionType(
        new Chars("OpCompositeConstruct"),80, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Constituents"),42)
        });


    public static final InstructionType OpCompositeExtract = new InstructionType(
        new Chars("OpCompositeExtract"),81, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Composite"),0),
            new OperandType.LiteralInteger(new Chars("Indexes"),42)
        });


    public static final InstructionType OpCompositeInsert = new InstructionType(
        new Chars("OpCompositeInsert"),82, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Object"),0),
            new OperandType.IdRef(new Chars("Composite"),0),
            new OperandType.LiteralInteger(new Chars("Indexes"),42)
        });


    public static final InstructionType OpCopyObject = new InstructionType(
        new Chars("OpCopyObject"),83, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand"),0)
        });


    public static final InstructionType OpTranspose = new InstructionType(
        new Chars("OpTranspose"),84, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Matrix"),0)
        });


    public static final InstructionType OpSampledImage = new InstructionType(
        new Chars("OpSampledImage"),86, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Sampler"),0)
        });


    public static final InstructionType OpImageSampleImplicitLod = new InstructionType(
        new Chars("OpImageSampleImplicitLod"),87, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageSampleExplicitLod = new InstructionType(
        new Chars("OpImageSampleExplicitLod"),88, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,0)
        });


    public static final InstructionType OpImageSampleDrefImplicitLod = new InstructionType(
        new Chars("OpImageSampleDrefImplicitLod"),89, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("D~ref~"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageSampleDrefExplicitLod = new InstructionType(
        new Chars("OpImageSampleDrefExplicitLod"),90, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("D~ref~"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,0)
        });


    public static final InstructionType OpImageSampleProjImplicitLod = new InstructionType(
        new Chars("OpImageSampleProjImplicitLod"),91, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageSampleProjExplicitLod = new InstructionType(
        new Chars("OpImageSampleProjExplicitLod"),92, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,0)
        });


    public static final InstructionType OpImageSampleProjDrefImplicitLod = new InstructionType(
        new Chars("OpImageSampleProjDrefImplicitLod"),93, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("D~ref~"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageSampleProjDrefExplicitLod = new InstructionType(
        new Chars("OpImageSampleProjDrefExplicitLod"),94, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("D~ref~"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,0)
        });


    public static final InstructionType OpImageFetch = new InstructionType(
        new Chars("OpImageFetch"),95, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageGather = new InstructionType(
        new Chars("OpImageGather"),96, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("Component"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageDrefGather = new InstructionType(
        new Chars("OpImageDrefGather"),97, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("D~ref~"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageRead = new InstructionType(
        new Chars("OpImageRead"),98, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageWrite = new InstructionType(
        new Chars("OpImageWrite"),99, new OperandType[]{
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("Texel"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImage = new InstructionType(
        new Chars("OpImage"),100, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0)
        });


    public static final InstructionType OpImageQueryFormat = new InstructionType(
        new Chars("OpImageQueryFormat"),101, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0)
        });


    public static final InstructionType OpImageQueryOrder = new InstructionType(
        new Chars("OpImageQueryOrder"),102, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0)
        });


    public static final InstructionType OpImageQuerySizeLod = new InstructionType(
        new Chars("OpImageQuerySizeLod"),103, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Level of Detail"),0)
        });


    public static final InstructionType OpImageQuerySize = new InstructionType(
        new Chars("OpImageQuerySize"),104, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0)
        });


    public static final InstructionType OpImageQueryLod = new InstructionType(
        new Chars("OpImageQueryLod"),105, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0)
        });


    public static final InstructionType OpImageQueryLevels = new InstructionType(
        new Chars("OpImageQueryLevels"),106, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0)
        });


    public static final InstructionType OpImageQuerySamples = new InstructionType(
        new Chars("OpImageQuerySamples"),107, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0)
        });


    public static final InstructionType OpConvertFToU = new InstructionType(
        new Chars("OpConvertFToU"),109, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Float Value"),0)
        });


    public static final InstructionType OpConvertFToS = new InstructionType(
        new Chars("OpConvertFToS"),110, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Float Value"),0)
        });


    public static final InstructionType OpConvertSToF = new InstructionType(
        new Chars("OpConvertSToF"),111, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Signed Value"),0)
        });


    public static final InstructionType OpConvertUToF = new InstructionType(
        new Chars("OpConvertUToF"),112, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Unsigned Value"),0)
        });


    public static final InstructionType OpUConvert = new InstructionType(
        new Chars("OpUConvert"),113, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Unsigned Value"),0)
        });


    public static final InstructionType OpSConvert = new InstructionType(
        new Chars("OpSConvert"),114, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Signed Value"),0)
        });


    public static final InstructionType OpFConvert = new InstructionType(
        new Chars("OpFConvert"),115, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Float Value"),0)
        });


    public static final InstructionType OpQuantizeToF16 = new InstructionType(
        new Chars("OpQuantizeToF16"),116, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpConvertPtrToU = new InstructionType(
        new Chars("OpConvertPtrToU"),117, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0)
        });


    public static final InstructionType OpSatConvertSToU = new InstructionType(
        new Chars("OpSatConvertSToU"),118, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Signed Value"),0)
        });


    public static final InstructionType OpSatConvertUToS = new InstructionType(
        new Chars("OpSatConvertUToS"),119, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Unsigned Value"),0)
        });


    public static final InstructionType OpConvertUToPtr = new InstructionType(
        new Chars("OpConvertUToPtr"),120, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Integer Value"),0)
        });


    public static final InstructionType OpPtrCastToGeneric = new InstructionType(
        new Chars("OpPtrCastToGeneric"),121, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0)
        });


    public static final InstructionType OpGenericCastToPtr = new InstructionType(
        new Chars("OpGenericCastToPtr"),122, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0)
        });


    public static final InstructionType OpGenericCastToPtrExplicit = new InstructionType(
        new Chars("OpGenericCastToPtrExplicit"),123, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.Enumerated(new Chars("Storage"),StorageClass.class,0)
        });


    public static final InstructionType OpBitcast = new InstructionType(
        new Chars("OpBitcast"),124, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand"),0)
        });


    public static final InstructionType OpSNegate = new InstructionType(
        new Chars("OpSNegate"),126, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand"),0)
        });


    public static final InstructionType OpFNegate = new InstructionType(
        new Chars("OpFNegate"),127, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand"),0)
        });


    public static final InstructionType OpIAdd = new InstructionType(
        new Chars("OpIAdd"),128, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFAdd = new InstructionType(
        new Chars("OpFAdd"),129, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpISub = new InstructionType(
        new Chars("OpISub"),130, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFSub = new InstructionType(
        new Chars("OpFSub"),131, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpIMul = new InstructionType(
        new Chars("OpIMul"),132, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFMul = new InstructionType(
        new Chars("OpFMul"),133, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpUDiv = new InstructionType(
        new Chars("OpUDiv"),134, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpSDiv = new InstructionType(
        new Chars("OpSDiv"),135, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFDiv = new InstructionType(
        new Chars("OpFDiv"),136, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpUMod = new InstructionType(
        new Chars("OpUMod"),137, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpSRem = new InstructionType(
        new Chars("OpSRem"),138, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpSMod = new InstructionType(
        new Chars("OpSMod"),139, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFRem = new InstructionType(
        new Chars("OpFRem"),140, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFMod = new InstructionType(
        new Chars("OpFMod"),141, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpVectorTimesScalar = new InstructionType(
        new Chars("OpVectorTimesScalar"),142, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Vector"),0),
            new OperandType.IdRef(new Chars("Scalar"),0)
        });


    public static final InstructionType OpMatrixTimesScalar = new InstructionType(
        new Chars("OpMatrixTimesScalar"),143, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Matrix"),0),
            new OperandType.IdRef(new Chars("Scalar"),0)
        });


    public static final InstructionType OpVectorTimesMatrix = new InstructionType(
        new Chars("OpVectorTimesMatrix"),144, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Vector"),0),
            new OperandType.IdRef(new Chars("Matrix"),0)
        });


    public static final InstructionType OpMatrixTimesVector = new InstructionType(
        new Chars("OpMatrixTimesVector"),145, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Matrix"),0),
            new OperandType.IdRef(new Chars("Vector"),0)
        });


    public static final InstructionType OpMatrixTimesMatrix = new InstructionType(
        new Chars("OpMatrixTimesMatrix"),146, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("LeftMatrix"),0),
            new OperandType.IdRef(new Chars("RightMatrix"),0)
        });


    public static final InstructionType OpOuterProduct = new InstructionType(
        new Chars("OpOuterProduct"),147, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Vector 1"),0),
            new OperandType.IdRef(new Chars("Vector 2"),0)
        });


    public static final InstructionType OpDot = new InstructionType(
        new Chars("OpDot"),148, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Vector 1"),0),
            new OperandType.IdRef(new Chars("Vector 2"),0)
        });


    public static final InstructionType OpIAddCarry = new InstructionType(
        new Chars("OpIAddCarry"),149, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpISubBorrow = new InstructionType(
        new Chars("OpISubBorrow"),150, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpUMulExtended = new InstructionType(
        new Chars("OpUMulExtended"),151, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpSMulExtended = new InstructionType(
        new Chars("OpSMulExtended"),152, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpAny = new InstructionType(
        new Chars("OpAny"),154, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Vector"),0)
        });


    public static final InstructionType OpAll = new InstructionType(
        new Chars("OpAll"),155, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Vector"),0)
        });


    public static final InstructionType OpIsNan = new InstructionType(
        new Chars("OpIsNan"),156, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("x"),0)
        });


    public static final InstructionType OpIsInf = new InstructionType(
        new Chars("OpIsInf"),157, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("x"),0)
        });


    public static final InstructionType OpIsFinite = new InstructionType(
        new Chars("OpIsFinite"),158, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("x"),0)
        });


    public static final InstructionType OpIsNormal = new InstructionType(
        new Chars("OpIsNormal"),159, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("x"),0)
        });


    public static final InstructionType OpSignBitSet = new InstructionType(
        new Chars("OpSignBitSet"),160, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("x"),0)
        });


    public static final InstructionType OpLessOrGreater = new InstructionType(
        new Chars("OpLessOrGreater"),161, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("x"),0),
            new OperandType.IdRef(new Chars("y"),0)
        });


    public static final InstructionType OpOrdered = new InstructionType(
        new Chars("OpOrdered"),162, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("x"),0),
            new OperandType.IdRef(new Chars("y"),0)
        });


    public static final InstructionType OpUnordered = new InstructionType(
        new Chars("OpUnordered"),163, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("x"),0),
            new OperandType.IdRef(new Chars("y"),0)
        });


    public static final InstructionType OpLogicalEqual = new InstructionType(
        new Chars("OpLogicalEqual"),164, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpLogicalNotEqual = new InstructionType(
        new Chars("OpLogicalNotEqual"),165, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpLogicalOr = new InstructionType(
        new Chars("OpLogicalOr"),166, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpLogicalAnd = new InstructionType(
        new Chars("OpLogicalAnd"),167, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpLogicalNot = new InstructionType(
        new Chars("OpLogicalNot"),168, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand"),0)
        });


    public static final InstructionType OpSelect = new InstructionType(
        new Chars("OpSelect"),169, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Condition"),0),
            new OperandType.IdRef(new Chars("Object 1"),0),
            new OperandType.IdRef(new Chars("Object 2"),0)
        });


    public static final InstructionType OpIEqual = new InstructionType(
        new Chars("OpIEqual"),170, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpINotEqual = new InstructionType(
        new Chars("OpINotEqual"),171, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpUGreaterThan = new InstructionType(
        new Chars("OpUGreaterThan"),172, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpSGreaterThan = new InstructionType(
        new Chars("OpSGreaterThan"),173, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpUGreaterThanEqual = new InstructionType(
        new Chars("OpUGreaterThanEqual"),174, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpSGreaterThanEqual = new InstructionType(
        new Chars("OpSGreaterThanEqual"),175, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpULessThan = new InstructionType(
        new Chars("OpULessThan"),176, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpSLessThan = new InstructionType(
        new Chars("OpSLessThan"),177, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpULessThanEqual = new InstructionType(
        new Chars("OpULessThanEqual"),178, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpSLessThanEqual = new InstructionType(
        new Chars("OpSLessThanEqual"),179, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFOrdEqual = new InstructionType(
        new Chars("OpFOrdEqual"),180, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFUnordEqual = new InstructionType(
        new Chars("OpFUnordEqual"),181, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFOrdNotEqual = new InstructionType(
        new Chars("OpFOrdNotEqual"),182, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFUnordNotEqual = new InstructionType(
        new Chars("OpFUnordNotEqual"),183, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFOrdLessThan = new InstructionType(
        new Chars("OpFOrdLessThan"),184, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFUnordLessThan = new InstructionType(
        new Chars("OpFUnordLessThan"),185, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFOrdGreaterThan = new InstructionType(
        new Chars("OpFOrdGreaterThan"),186, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFUnordGreaterThan = new InstructionType(
        new Chars("OpFUnordGreaterThan"),187, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFOrdLessThanEqual = new InstructionType(
        new Chars("OpFOrdLessThanEqual"),188, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFUnordLessThanEqual = new InstructionType(
        new Chars("OpFUnordLessThanEqual"),189, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFOrdGreaterThanEqual = new InstructionType(
        new Chars("OpFOrdGreaterThanEqual"),190, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpFUnordGreaterThanEqual = new InstructionType(
        new Chars("OpFUnordGreaterThanEqual"),191, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpShiftRightLogical = new InstructionType(
        new Chars("OpShiftRightLogical"),194, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0),
            new OperandType.IdRef(new Chars("Shift"),0)
        });


    public static final InstructionType OpShiftRightArithmetic = new InstructionType(
        new Chars("OpShiftRightArithmetic"),195, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0),
            new OperandType.IdRef(new Chars("Shift"),0)
        });


    public static final InstructionType OpShiftLeftLogical = new InstructionType(
        new Chars("OpShiftLeftLogical"),196, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0),
            new OperandType.IdRef(new Chars("Shift"),0)
        });


    public static final InstructionType OpBitwiseOr = new InstructionType(
        new Chars("OpBitwiseOr"),197, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpBitwiseXor = new InstructionType(
        new Chars("OpBitwiseXor"),198, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpBitwiseAnd = new InstructionType(
        new Chars("OpBitwiseAnd"),199, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand 1"),0),
            new OperandType.IdRef(new Chars("Operand 2"),0)
        });


    public static final InstructionType OpNot = new InstructionType(
        new Chars("OpNot"),200, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Operand"),0)
        });


    public static final InstructionType OpBitFieldInsert = new InstructionType(
        new Chars("OpBitFieldInsert"),201, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0),
            new OperandType.IdRef(new Chars("Insert"),0),
            new OperandType.IdRef(new Chars("Offset"),0),
            new OperandType.IdRef(new Chars("Count"),0)
        });


    public static final InstructionType OpBitFieldSExtract = new InstructionType(
        new Chars("OpBitFieldSExtract"),202, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0),
            new OperandType.IdRef(new Chars("Offset"),0),
            new OperandType.IdRef(new Chars("Count"),0)
        });


    public static final InstructionType OpBitFieldUExtract = new InstructionType(
        new Chars("OpBitFieldUExtract"),203, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0),
            new OperandType.IdRef(new Chars("Offset"),0),
            new OperandType.IdRef(new Chars("Count"),0)
        });


    public static final InstructionType OpBitReverse = new InstructionType(
        new Chars("OpBitReverse"),204, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0)
        });


    public static final InstructionType OpBitCount = new InstructionType(
        new Chars("OpBitCount"),205, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Base"),0)
        });


    public static final InstructionType OpDPdx = new InstructionType(
        new Chars("OpDPdx"),207, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("P"),0)
        });


    public static final InstructionType OpDPdy = new InstructionType(
        new Chars("OpDPdy"),208, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("P"),0)
        });


    public static final InstructionType OpFwidth = new InstructionType(
        new Chars("OpFwidth"),209, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("P"),0)
        });


    public static final InstructionType OpDPdxFine = new InstructionType(
        new Chars("OpDPdxFine"),210, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("P"),0)
        });


    public static final InstructionType OpDPdyFine = new InstructionType(
        new Chars("OpDPdyFine"),211, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("P"),0)
        });


    public static final InstructionType OpFwidthFine = new InstructionType(
        new Chars("OpFwidthFine"),212, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("P"),0)
        });


    public static final InstructionType OpDPdxCoarse = new InstructionType(
        new Chars("OpDPdxCoarse"),213, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("P"),0)
        });


    public static final InstructionType OpDPdyCoarse = new InstructionType(
        new Chars("OpDPdyCoarse"),214, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("P"),0)
        });


    public static final InstructionType OpFwidthCoarse = new InstructionType(
        new Chars("OpFwidthCoarse"),215, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("P"),0)
        });


    public static final InstructionType OpEmitVertex = new InstructionType(
        new Chars("OpEmitVertex"),218, new OperandType[]{
        });


    public static final InstructionType OpEndPrimitive = new InstructionType(
        new Chars("OpEndPrimitive"),219, new OperandType[]{
        });


    public static final InstructionType OpEmitStreamVertex = new InstructionType(
        new Chars("OpEmitStreamVertex"),220, new OperandType[]{
            new OperandType.IdRef(new Chars("Stream"),0)
        });


    public static final InstructionType OpEndStreamPrimitive = new InstructionType(
        new Chars("OpEndStreamPrimitive"),221, new OperandType[]{
            new OperandType.IdRef(new Chars("Stream"),0)
        });


    public static final InstructionType OpControlBarrier = new InstructionType(
        new Chars("OpControlBarrier"),224, new OperandType[]{
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdScope(new Chars("Memory"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0)
        });


    public static final InstructionType OpMemoryBarrier = new InstructionType(
        new Chars("OpMemoryBarrier"),225, new OperandType[]{
            new OperandType.IdScope(new Chars("Memory"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0)
        });


    public static final InstructionType OpAtomicLoad = new InstructionType(
        new Chars("OpAtomicLoad"),227, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0)
        });


    public static final InstructionType OpAtomicStore = new InstructionType(
        new Chars("OpAtomicStore"),228, new OperandType[]{
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpAtomicExchange = new InstructionType(
        new Chars("OpAtomicExchange"),229, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpAtomicCompareExchange = new InstructionType(
        new Chars("OpAtomicCompareExchange"),230, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Equal"),0),
            new OperandType.IdMemorySemantics(new Chars("Unequal"),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("Comparator"),0)
        });


    public static final InstructionType OpAtomicCompareExchangeWeak = new InstructionType(
        new Chars("OpAtomicCompareExchangeWeak"),231, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Equal"),0),
            new OperandType.IdMemorySemantics(new Chars("Unequal"),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("Comparator"),0)
        });


    public static final InstructionType OpAtomicIIncrement = new InstructionType(
        new Chars("OpAtomicIIncrement"),232, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0)
        });


    public static final InstructionType OpAtomicIDecrement = new InstructionType(
        new Chars("OpAtomicIDecrement"),233, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0)
        });


    public static final InstructionType OpAtomicIAdd = new InstructionType(
        new Chars("OpAtomicIAdd"),234, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpAtomicISub = new InstructionType(
        new Chars("OpAtomicISub"),235, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpAtomicSMin = new InstructionType(
        new Chars("OpAtomicSMin"),236, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpAtomicUMin = new InstructionType(
        new Chars("OpAtomicUMin"),237, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpAtomicSMax = new InstructionType(
        new Chars("OpAtomicSMax"),238, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpAtomicUMax = new InstructionType(
        new Chars("OpAtomicUMax"),239, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpAtomicAnd = new InstructionType(
        new Chars("OpAtomicAnd"),240, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpAtomicOr = new InstructionType(
        new Chars("OpAtomicOr"),241, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpAtomicXor = new InstructionType(
        new Chars("OpAtomicXor"),242, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpPhi = new InstructionType(
        new Chars("OpPhi"),245, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.PairIdRefIdRef(new Chars("Variable, Parent, ..."),42)
        });


    public static final InstructionType OpLoopMerge = new InstructionType(
        new Chars("OpLoopMerge"),246, new OperandType[]{
            new OperandType.IdRef(new Chars("Merge Block"),0),
            new OperandType.IdRef(new Chars("Continue Target"),0),
            new OperandType.Enumerated(new Chars(""),LoopControl.class,0)
        });


    public static final InstructionType OpSelectionMerge = new InstructionType(
        new Chars("OpSelectionMerge"),247, new OperandType[]{
            new OperandType.IdRef(new Chars("Merge Block"),0),
            new OperandType.Enumerated(new Chars(""),SelectionControl.class,0)
        });


    public static final InstructionType OpLabel = new InstructionType(
        new Chars("OpLabel"),248, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpBranch = new InstructionType(
        new Chars("OpBranch"),249, new OperandType[]{
            new OperandType.IdRef(new Chars("Target Label"),0)
        });


    public static final InstructionType OpBranchConditional = new InstructionType(
        new Chars("OpBranchConditional"),250, new OperandType[]{
            new OperandType.IdRef(new Chars("Condition"),0),
            new OperandType.IdRef(new Chars("True Label"),0),
            new OperandType.IdRef(new Chars("False Label"),0),
            new OperandType.LiteralInteger(new Chars("Branch weights"),42)
        });


    public static final InstructionType OpSwitch = new InstructionType(
        new Chars("OpSwitch"),251, new OperandType[]{
            new OperandType.IdRef(new Chars("Selector"),0),
            new OperandType.IdRef(new Chars("Default"),0),
            new OperandType.PairLiteralIntegerIdRef(new Chars("Target"),42)
        });


    public static final InstructionType OpKill = new InstructionType(
        new Chars("OpKill"),252, new OperandType[]{
        });


    public static final InstructionType OpReturn = new InstructionType(
        new Chars("OpReturn"),253, new OperandType[]{
        });


    public static final InstructionType OpReturnValue = new InstructionType(
        new Chars("OpReturnValue"),254, new OperandType[]{
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpUnreachable = new InstructionType(
        new Chars("OpUnreachable"),255, new OperandType[]{
        });


    public static final InstructionType OpLifetimeStart = new InstructionType(
        new Chars("OpLifetimeStart"),256, new OperandType[]{
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.LiteralInteger(new Chars("Size"),0)
        });


    public static final InstructionType OpLifetimeStop = new InstructionType(
        new Chars("OpLifetimeStop"),257, new OperandType[]{
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.LiteralInteger(new Chars("Size"),0)
        });


    public static final InstructionType OpGroupAsyncCopy = new InstructionType(
        new Chars("OpGroupAsyncCopy"),259, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Destination"),0),
            new OperandType.IdRef(new Chars("Source"),0),
            new OperandType.IdRef(new Chars("Num Elements"),0),
            new OperandType.IdRef(new Chars("Stride"),0),
            new OperandType.IdRef(new Chars("Event"),0)
        });


    public static final InstructionType OpGroupWaitEvents = new InstructionType(
        new Chars("OpGroupWaitEvents"),260, new OperandType[]{
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Num Events"),0),
            new OperandType.IdRef(new Chars("Events List"),0)
        });


    public static final InstructionType OpGroupAll = new InstructionType(
        new Chars("OpGroupAll"),261, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Predicate"),0)
        });


    public static final InstructionType OpGroupAny = new InstructionType(
        new Chars("OpGroupAny"),262, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Predicate"),0)
        });


    public static final InstructionType OpGroupBroadcast = new InstructionType(
        new Chars("OpGroupBroadcast"),263, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("LocalId"),0)
        });


    public static final InstructionType OpGroupIAdd = new InstructionType(
        new Chars("OpGroupIAdd"),264, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupFAdd = new InstructionType(
        new Chars("OpGroupFAdd"),265, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupFMin = new InstructionType(
        new Chars("OpGroupFMin"),266, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupUMin = new InstructionType(
        new Chars("OpGroupUMin"),267, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupSMin = new InstructionType(
        new Chars("OpGroupSMin"),268, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupFMax = new InstructionType(
        new Chars("OpGroupFMax"),269, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupUMax = new InstructionType(
        new Chars("OpGroupUMax"),270, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupSMax = new InstructionType(
        new Chars("OpGroupSMax"),271, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpReadPipe = new InstructionType(
        new Chars("OpReadPipe"),274, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpWritePipe = new InstructionType(
        new Chars("OpWritePipe"),275, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpReservedReadPipe = new InstructionType(
        new Chars("OpReservedReadPipe"),276, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Reserve Id"),0),
            new OperandType.IdRef(new Chars("Index"),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpReservedWritePipe = new InstructionType(
        new Chars("OpReservedWritePipe"),277, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Reserve Id"),0),
            new OperandType.IdRef(new Chars("Index"),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpReserveReadPipePackets = new InstructionType(
        new Chars("OpReserveReadPipePackets"),278, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Num Packets"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpReserveWritePipePackets = new InstructionType(
        new Chars("OpReserveWritePipePackets"),279, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Num Packets"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpCommitReadPipe = new InstructionType(
        new Chars("OpCommitReadPipe"),280, new OperandType[]{
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Reserve Id"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpCommitWritePipe = new InstructionType(
        new Chars("OpCommitWritePipe"),281, new OperandType[]{
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Reserve Id"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpIsValidReserveId = new InstructionType(
        new Chars("OpIsValidReserveId"),282, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Reserve Id"),0)
        });


    public static final InstructionType OpGetNumPipePackets = new InstructionType(
        new Chars("OpGetNumPipePackets"),283, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpGetMaxPipePackets = new InstructionType(
        new Chars("OpGetMaxPipePackets"),284, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpGroupReserveReadPipePackets = new InstructionType(
        new Chars("OpGroupReserveReadPipePackets"),285, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Num Packets"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpGroupReserveWritePipePackets = new InstructionType(
        new Chars("OpGroupReserveWritePipePackets"),286, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Num Packets"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpGroupCommitReadPipe = new InstructionType(
        new Chars("OpGroupCommitReadPipe"),287, new OperandType[]{
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Reserve Id"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpGroupCommitWritePipe = new InstructionType(
        new Chars("OpGroupCommitWritePipe"),288, new OperandType[]{
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Pipe"),0),
            new OperandType.IdRef(new Chars("Reserve Id"),0),
            new OperandType.IdRef(new Chars("Packet Size"),0),
            new OperandType.IdRef(new Chars("Packet Alignment"),0)
        });


    public static final InstructionType OpEnqueueMarker = new InstructionType(
        new Chars("OpEnqueueMarker"),291, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Queue"),0),
            new OperandType.IdRef(new Chars("Num Events"),0),
            new OperandType.IdRef(new Chars("Wait Events"),0),
            new OperandType.IdRef(new Chars("Ret Event"),0)
        });


    public static final InstructionType OpEnqueueKernel = new InstructionType(
        new Chars("OpEnqueueKernel"),292, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Queue"),0),
            new OperandType.IdRef(new Chars("Flags"),0),
            new OperandType.IdRef(new Chars("ND Range"),0),
            new OperandType.IdRef(new Chars("Num Events"),0),
            new OperandType.IdRef(new Chars("Wait Events"),0),
            new OperandType.IdRef(new Chars("Ret Event"),0),
            new OperandType.IdRef(new Chars("Invoke"),0),
            new OperandType.IdRef(new Chars("Param"),0),
            new OperandType.IdRef(new Chars("Param Size"),0),
            new OperandType.IdRef(new Chars("Param Align"),0),
            new OperandType.IdRef(new Chars("Local Size"),42)
        });


    public static final InstructionType OpGetKernelNDrangeSubGroupCount = new InstructionType(
        new Chars("OpGetKernelNDrangeSubGroupCount"),293, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("ND Range"),0),
            new OperandType.IdRef(new Chars("Invoke"),0),
            new OperandType.IdRef(new Chars("Param"),0),
            new OperandType.IdRef(new Chars("Param Size"),0),
            new OperandType.IdRef(new Chars("Param Align"),0)
        });


    public static final InstructionType OpGetKernelNDrangeMaxSubGroupSize = new InstructionType(
        new Chars("OpGetKernelNDrangeMaxSubGroupSize"),294, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("ND Range"),0),
            new OperandType.IdRef(new Chars("Invoke"),0),
            new OperandType.IdRef(new Chars("Param"),0),
            new OperandType.IdRef(new Chars("Param Size"),0),
            new OperandType.IdRef(new Chars("Param Align"),0)
        });


    public static final InstructionType OpGetKernelWorkGroupSize = new InstructionType(
        new Chars("OpGetKernelWorkGroupSize"),295, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Invoke"),0),
            new OperandType.IdRef(new Chars("Param"),0),
            new OperandType.IdRef(new Chars("Param Size"),0),
            new OperandType.IdRef(new Chars("Param Align"),0)
        });


    public static final InstructionType OpGetKernelPreferredWorkGroupSizeMultiple = new InstructionType(
        new Chars("OpGetKernelPreferredWorkGroupSizeMultiple"),296, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Invoke"),0),
            new OperandType.IdRef(new Chars("Param"),0),
            new OperandType.IdRef(new Chars("Param Size"),0),
            new OperandType.IdRef(new Chars("Param Align"),0)
        });


    public static final InstructionType OpRetainEvent = new InstructionType(
        new Chars("OpRetainEvent"),297, new OperandType[]{
            new OperandType.IdRef(new Chars("Event"),0)
        });


    public static final InstructionType OpReleaseEvent = new InstructionType(
        new Chars("OpReleaseEvent"),298, new OperandType[]{
            new OperandType.IdRef(new Chars("Event"),0)
        });


    public static final InstructionType OpCreateUserEvent = new InstructionType(
        new Chars("OpCreateUserEvent"),299, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpIsValidEvent = new InstructionType(
        new Chars("OpIsValidEvent"),300, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Event"),0)
        });


    public static final InstructionType OpSetUserEventStatus = new InstructionType(
        new Chars("OpSetUserEventStatus"),301, new OperandType[]{
            new OperandType.IdRef(new Chars("Event"),0),
            new OperandType.IdRef(new Chars("Status"),0)
        });


    public static final InstructionType OpCaptureEventProfilingInfo = new InstructionType(
        new Chars("OpCaptureEventProfilingInfo"),302, new OperandType[]{
            new OperandType.IdRef(new Chars("Event"),0),
            new OperandType.IdRef(new Chars("Profiling Info"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpGetDefaultQueue = new InstructionType(
        new Chars("OpGetDefaultQueue"),303, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpBuildNDRange = new InstructionType(
        new Chars("OpBuildNDRange"),304, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("GlobalWorkSize"),0),
            new OperandType.IdRef(new Chars("LocalWorkSize"),0),
            new OperandType.IdRef(new Chars("GlobalWorkOffset"),0)
        });


    public static final InstructionType OpImageSparseSampleImplicitLod = new InstructionType(
        new Chars("OpImageSparseSampleImplicitLod"),305, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageSparseSampleExplicitLod = new InstructionType(
        new Chars("OpImageSparseSampleExplicitLod"),306, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,0)
        });


    public static final InstructionType OpImageSparseSampleDrefImplicitLod = new InstructionType(
        new Chars("OpImageSparseSampleDrefImplicitLod"),307, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("D~ref~"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageSparseSampleDrefExplicitLod = new InstructionType(
        new Chars("OpImageSparseSampleDrefExplicitLod"),308, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("D~ref~"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,0)
        });


    public static final InstructionType OpImageSparseSampleProjImplicitLod = new InstructionType(
        new Chars("OpImageSparseSampleProjImplicitLod"),309, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageSparseSampleProjExplicitLod = new InstructionType(
        new Chars("OpImageSparseSampleProjExplicitLod"),310, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,0)
        });


    public static final InstructionType OpImageSparseSampleProjDrefImplicitLod = new InstructionType(
        new Chars("OpImageSparseSampleProjDrefImplicitLod"),311, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("D~ref~"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageSparseSampleProjDrefExplicitLod = new InstructionType(
        new Chars("OpImageSparseSampleProjDrefExplicitLod"),312, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("D~ref~"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,0)
        });


    public static final InstructionType OpImageSparseFetch = new InstructionType(
        new Chars("OpImageSparseFetch"),313, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageSparseGather = new InstructionType(
        new Chars("OpImageSparseGather"),314, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("Component"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageSparseDrefGather = new InstructionType(
        new Chars("OpImageSparseDrefGather"),315, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Sampled Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("D~ref~"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpImageSparseTexelsResident = new InstructionType(
        new Chars("OpImageSparseTexelsResident"),316, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Resident Code"),0)
        });


    public static final InstructionType OpNoLine = new InstructionType(
        new Chars("OpNoLine"),317, new OperandType[]{
        });


    public static final InstructionType OpAtomicFlagTestAndSet = new InstructionType(
        new Chars("OpAtomicFlagTestAndSet"),318, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0)
        });


    public static final InstructionType OpAtomicFlagClear = new InstructionType(
        new Chars("OpAtomicFlagClear"),319, new OperandType[]{
            new OperandType.IdRef(new Chars("Pointer"),0),
            new OperandType.IdScope(new Chars("Scope"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0)
        });


    public static final InstructionType OpImageSparseRead = new InstructionType(
        new Chars("OpImageSparseRead"),320, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.Enumerated(new Chars(""),ImageOperands.class,63)
        });


    public static final InstructionType OpSizeOf = new InstructionType(
        new Chars("OpSizeOf"),321, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pointer"),0)
        });


    public static final InstructionType OpTypePipeStorage = new InstructionType(
        new Chars("OpTypePipeStorage"),322, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpConstantPipeStorage = new InstructionType(
        new Chars("OpConstantPipeStorage"),323, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.LiteralInteger(new Chars("Packet Size"),0),
            new OperandType.LiteralInteger(new Chars("Packet Alignment"),0),
            new OperandType.LiteralInteger(new Chars("Capacity"),0)
        });


    public static final InstructionType OpCreatePipeFromPipeStorage = new InstructionType(
        new Chars("OpCreatePipeFromPipeStorage"),324, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Pipe Storage"),0)
        });


    public static final InstructionType OpGetKernelLocalSizeForSubgroupCount = new InstructionType(
        new Chars("OpGetKernelLocalSizeForSubgroupCount"),325, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Subgroup Count"),0),
            new OperandType.IdRef(new Chars("Invoke"),0),
            new OperandType.IdRef(new Chars("Param"),0),
            new OperandType.IdRef(new Chars("Param Size"),0),
            new OperandType.IdRef(new Chars("Param Align"),0)
        });


    public static final InstructionType OpGetKernelMaxNumSubgroups = new InstructionType(
        new Chars("OpGetKernelMaxNumSubgroups"),326, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Invoke"),0),
            new OperandType.IdRef(new Chars("Param"),0),
            new OperandType.IdRef(new Chars("Param Size"),0),
            new OperandType.IdRef(new Chars("Param Align"),0)
        });


    public static final InstructionType OpTypeNamedBarrier = new InstructionType(
        new Chars("OpTypeNamedBarrier"),327, new OperandType[]{
            new OperandType.IdResult(new Chars(""),0)
        });


    public static final InstructionType OpNamedBarrierInitialize = new InstructionType(
        new Chars("OpNamedBarrierInitialize"),328, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Subgroup Count"),0)
        });


    public static final InstructionType OpMemoryNamedBarrier = new InstructionType(
        new Chars("OpMemoryNamedBarrier"),329, new OperandType[]{
            new OperandType.IdRef(new Chars("Named Barrier"),0),
            new OperandType.IdScope(new Chars("Memory"),0),
            new OperandType.IdMemorySemantics(new Chars("Semantics"),0)
        });


    public static final InstructionType OpModuleProcessed = new InstructionType(
        new Chars("OpModuleProcessed"),330, new OperandType[]{
            new OperandType.LiteralString(new Chars("Process"),0)
        });


    public static final InstructionType OpExecutionModeId = new InstructionType(
        new Chars("OpExecutionModeId"),331, new OperandType[]{
            new OperandType.IdRef(new Chars("Entry Point"),0),
            new OperandType.Enumerated(new Chars("Mode"),ExecutionMode.class,0)
        });


    public static final InstructionType OpDecorateId = new InstructionType(
        new Chars("OpDecorateId"),332, new OperandType[]{
            new OperandType.IdRef(new Chars("Target"),0),
            new OperandType.Enumerated(new Chars(""),Decoration.class,0)
        });


    public static final InstructionType OpGroupNonUniformElect = new InstructionType(
        new Chars("OpGroupNonUniformElect"),333, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0)
        });


    public static final InstructionType OpGroupNonUniformAll = new InstructionType(
        new Chars("OpGroupNonUniformAll"),334, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Predicate"),0)
        });


    public static final InstructionType OpGroupNonUniformAny = new InstructionType(
        new Chars("OpGroupNonUniformAny"),335, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Predicate"),0)
        });


    public static final InstructionType OpGroupNonUniformAllEqual = new InstructionType(
        new Chars("OpGroupNonUniformAllEqual"),336, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpGroupNonUniformBroadcast = new InstructionType(
        new Chars("OpGroupNonUniformBroadcast"),337, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("Id"),0)
        });


    public static final InstructionType OpGroupNonUniformBroadcastFirst = new InstructionType(
        new Chars("OpGroupNonUniformBroadcastFirst"),338, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpGroupNonUniformBallot = new InstructionType(
        new Chars("OpGroupNonUniformBallot"),339, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Predicate"),0)
        });


    public static final InstructionType OpGroupNonUniformInverseBallot = new InstructionType(
        new Chars("OpGroupNonUniformInverseBallot"),340, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpGroupNonUniformBallotBitExtract = new InstructionType(
        new Chars("OpGroupNonUniformBallotBitExtract"),341, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("Index"),0)
        });


    public static final InstructionType OpGroupNonUniformBallotBitCount = new InstructionType(
        new Chars("OpGroupNonUniformBallotBitCount"),342, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpGroupNonUniformBallotFindLSB = new InstructionType(
        new Chars("OpGroupNonUniformBallotFindLSB"),343, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpGroupNonUniformBallotFindMSB = new InstructionType(
        new Chars("OpGroupNonUniformBallotFindMSB"),344, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpGroupNonUniformShuffle = new InstructionType(
        new Chars("OpGroupNonUniformShuffle"),345, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("Id"),0)
        });


    public static final InstructionType OpGroupNonUniformShuffleXor = new InstructionType(
        new Chars("OpGroupNonUniformShuffleXor"),346, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("Mask"),0)
        });


    public static final InstructionType OpGroupNonUniformShuffleUp = new InstructionType(
        new Chars("OpGroupNonUniformShuffleUp"),347, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("Delta"),0)
        });


    public static final InstructionType OpGroupNonUniformShuffleDown = new InstructionType(
        new Chars("OpGroupNonUniformShuffleDown"),348, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("Delta"),0)
        });


    public static final InstructionType OpGroupNonUniformIAdd = new InstructionType(
        new Chars("OpGroupNonUniformIAdd"),349, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformFAdd = new InstructionType(
        new Chars("OpGroupNonUniformFAdd"),350, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformIMul = new InstructionType(
        new Chars("OpGroupNonUniformIMul"),351, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformFMul = new InstructionType(
        new Chars("OpGroupNonUniformFMul"),352, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformSMin = new InstructionType(
        new Chars("OpGroupNonUniformSMin"),353, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformUMin = new InstructionType(
        new Chars("OpGroupNonUniformUMin"),354, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformFMin = new InstructionType(
        new Chars("OpGroupNonUniformFMin"),355, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformSMax = new InstructionType(
        new Chars("OpGroupNonUniformSMax"),356, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformUMax = new InstructionType(
        new Chars("OpGroupNonUniformUMax"),357, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformFMax = new InstructionType(
        new Chars("OpGroupNonUniformFMax"),358, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformBitwiseAnd = new InstructionType(
        new Chars("OpGroupNonUniformBitwiseAnd"),359, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformBitwiseOr = new InstructionType(
        new Chars("OpGroupNonUniformBitwiseOr"),360, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformBitwiseXor = new InstructionType(
        new Chars("OpGroupNonUniformBitwiseXor"),361, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformLogicalAnd = new InstructionType(
        new Chars("OpGroupNonUniformLogicalAnd"),362, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformLogicalOr = new InstructionType(
        new Chars("OpGroupNonUniformLogicalOr"),363, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformLogicalXor = new InstructionType(
        new Chars("OpGroupNonUniformLogicalXor"),364, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("ClusterSize"),63)
        });


    public static final InstructionType OpGroupNonUniformQuadBroadcast = new InstructionType(
        new Chars("OpGroupNonUniformQuadBroadcast"),365, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("Index"),0)
        });


    public static final InstructionType OpGroupNonUniformQuadSwap = new InstructionType(
        new Chars("OpGroupNonUniformQuadSwap"),366, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("Direction"),0)
        });


    public static final InstructionType OpSubgroupBallotKHR = new InstructionType(
        new Chars("OpSubgroupBallotKHR"),4421, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Predicate"),0)
        });


    public static final InstructionType OpSubgroupFirstInvocationKHR = new InstructionType(
        new Chars("OpSubgroupFirstInvocationKHR"),4422, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpSubgroupAllKHR = new InstructionType(
        new Chars("OpSubgroupAllKHR"),4428, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Predicate"),0)
        });


    public static final InstructionType OpSubgroupAnyKHR = new InstructionType(
        new Chars("OpSubgroupAnyKHR"),4429, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Predicate"),0)
        });


    public static final InstructionType OpSubgroupAllEqualKHR = new InstructionType(
        new Chars("OpSubgroupAllEqualKHR"),4430, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Predicate"),0)
        });


    public static final InstructionType OpSubgroupReadInvocationKHR = new InstructionType(
        new Chars("OpSubgroupReadInvocationKHR"),4432, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Value"),0),
            new OperandType.IdRef(new Chars("Index"),0)
        });


    public static final InstructionType OpGroupIAddNonUniformAMD = new InstructionType(
        new Chars("OpGroupIAddNonUniformAMD"),5000, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupFAddNonUniformAMD = new InstructionType(
        new Chars("OpGroupFAddNonUniformAMD"),5001, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupFMinNonUniformAMD = new InstructionType(
        new Chars("OpGroupFMinNonUniformAMD"),5002, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupUMinNonUniformAMD = new InstructionType(
        new Chars("OpGroupUMinNonUniformAMD"),5003, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupSMinNonUniformAMD = new InstructionType(
        new Chars("OpGroupSMinNonUniformAMD"),5004, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupFMaxNonUniformAMD = new InstructionType(
        new Chars("OpGroupFMaxNonUniformAMD"),5005, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupUMaxNonUniformAMD = new InstructionType(
        new Chars("OpGroupUMaxNonUniformAMD"),5006, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpGroupSMaxNonUniformAMD = new InstructionType(
        new Chars("OpGroupSMaxNonUniformAMD"),5007, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdScope(new Chars("Execution"),0),
            new OperandType.Enumerated(new Chars("Operation"),GroupOperation.class,0),
            new OperandType.IdRef(new Chars("X"),0)
        });


    public static final InstructionType OpFragmentMaskFetchAMD = new InstructionType(
        new Chars("OpFragmentMaskFetchAMD"),5011, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0)
        });


    public static final InstructionType OpFragmentFetchAMD = new InstructionType(
        new Chars("OpFragmentFetchAMD"),5012, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("Fragment Index"),0)
        });


    public static final InstructionType OpSubgroupShuffleINTEL = new InstructionType(
        new Chars("OpSubgroupShuffleINTEL"),5571, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Data"),0),
            new OperandType.IdRef(new Chars("InvocationId"),0)
        });


    public static final InstructionType OpSubgroupShuffleDownINTEL = new InstructionType(
        new Chars("OpSubgroupShuffleDownINTEL"),5572, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Current"),0),
            new OperandType.IdRef(new Chars("Next"),0),
            new OperandType.IdRef(new Chars("Delta"),0)
        });


    public static final InstructionType OpSubgroupShuffleUpINTEL = new InstructionType(
        new Chars("OpSubgroupShuffleUpINTEL"),5573, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Previous"),0),
            new OperandType.IdRef(new Chars("Current"),0),
            new OperandType.IdRef(new Chars("Delta"),0)
        });


    public static final InstructionType OpSubgroupShuffleXorINTEL = new InstructionType(
        new Chars("OpSubgroupShuffleXorINTEL"),5574, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Data"),0),
            new OperandType.IdRef(new Chars("Value"),0)
        });


    public static final InstructionType OpSubgroupBlockReadINTEL = new InstructionType(
        new Chars("OpSubgroupBlockReadINTEL"),5575, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Ptr"),0)
        });


    public static final InstructionType OpSubgroupBlockWriteINTEL = new InstructionType(
        new Chars("OpSubgroupBlockWriteINTEL"),5576, new OperandType[]{
            new OperandType.IdRef(new Chars("Ptr"),0),
            new OperandType.IdRef(new Chars("Data"),0)
        });


    public static final InstructionType OpSubgroupImageBlockReadINTEL = new InstructionType(
        new Chars("OpSubgroupImageBlockReadINTEL"),5577, new OperandType[]{
            new OperandType.IdResultType(new Chars(""),0),
            new OperandType.IdResult(new Chars(""),0),
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0)
        });


    public static final InstructionType OpSubgroupImageBlockWriteINTEL = new InstructionType(
        new Chars("OpSubgroupImageBlockWriteINTEL"),5578, new OperandType[]{
            new OperandType.IdRef(new Chars("Image"),0),
            new OperandType.IdRef(new Chars("Coordinate"),0),
            new OperandType.IdRef(new Chars("Data"),0)
        });


    public static final InstructionType OpDecorateStringGOOGLE = new InstructionType(
        new Chars("OpDecorateStringGOOGLE"),5632, new OperandType[]{
            new OperandType.IdRef(new Chars("Target"),0),
            new OperandType.Enumerated(new Chars(""),Decoration.class,0)
        });


    public static final InstructionType OpMemberDecorateStringGOOGLE = new InstructionType(
        new Chars("OpMemberDecorateStringGOOGLE"),5633, new OperandType[]{
            new OperandType.IdRef(new Chars("Struct Type"),0),
            new OperandType.LiteralInteger(new Chars("Member"),0),
            new OperandType.Enumerated(new Chars(""),Decoration.class,0)
        });

}
