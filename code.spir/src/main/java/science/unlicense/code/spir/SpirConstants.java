
package science.unlicense.code.spir;

import science.unlicense.common.api.number.Endianness;

/**
 * List of all SPIR-V Constants.
 *
 * @author Johann Sorel
 */
public final class SpirConstants {

    public static final int SIGNATURE = 0x07230203;
    public static final byte[] SIGNATURE_BE = new byte[4];
    public static final byte[] SIGNATURE_LE = new byte[4];
    static {
        Endianness.BIG_ENDIAN.writeUInt(SIGNATURE, SIGNATURE_BE, 0);
        Endianness.LITTLE_ENDIAN.writeUInt(SIGNATURE, SIGNATURE_LE, 0);
    }

    /**
     * Spec : 3.2 Source Language  p.20
     */
    public static interface SOURCE_LANGUAGE {
        public static final int UNKNOWN  = 0;
        public static final int ESSL     = 1;
        public static final int GLSL     = 2;
        public static final int OPENCL   = 3;
    }

    /**
     * Spec : 3.3 Execution Model p.21
     */
    public static interface EXECUTION_MODEL {
        public static final int VERTEX       = 0;
        public static final int TESSCONTROL  = 1;
        public static final int TESSEVAL     = 2;
        public static final int GEOMETRY     = 3;
        public static final int FRAGMENT     = 4;
        public static final int GLCOMPUTE    = 5;
        public static final int KERNEL       = 6;
    }

    /**
     * Spec : 3.4 Addressing Model p.21
     */
    public static interface ADDRESSING_MODEL {
        public static final int LOGICAL     = 0;
        public static final int PHYSICAL32  = 1;
        public static final int PHYSICAL64  = 2;
    }

    /**
     * Spec : 3.5 Memory Model p.21
     */
    public static interface MEMORY_MODEL {
        public static final int SIMPLE     = 0;
        public static final int GLSL450    = 1;
        public static final int OPENCL1_2  = 2;
        public static final int OPENCL2_0  = 3;
        public static final int OPENCL2_1  = 4;
    }

    /**
     * Spec : 3.5 Execution Mode p.22
     */
    public static interface EXECUTION_MODE {
        public static final int INVOCATIONS              =  0;
        public static final int SPACING_EQUAL            =  1;
        public static final int SPACING_FRACTIONAL_EVEN  =  2;
        public static final int SPACING_FRACTIONAL_ODD   =  3;
        public static final int VERTEX_ORDER_CW          =  4;
        public static final int VERTEX_ORDER_CCW         =  5;
        public static final int PIXEL_CENTER_INTEGER     =  6;
        public static final int ORIGINE_UPPER_LEFT       =  7;
        public static final int EARLY_FRAGMENT_TESTS     =  8;
        public static final int POINT_MODE               =  9;
        public static final int XFB                      = 10;
        public static final int DEPTH_REPLACING          = 11;
        public static final int DEPTH_ANY                = 12;
        public static final int DEPTH_GREATER            = 13;
        public static final int DEPTH_LESS               = 14;
        public static final int DEPTH_UNCHANGED          = 15;
        public static final int LOCAL_SIZE               = 16;
        public static final int LOCAL_SIZE_HINT          = 17;
        public static final int INPUT_POINTS             = 18;
        public static final int INPUT_LINES              = 19;
        public static final int INPUT_LINES_ADJACENCY    = 20;
        public static final int INPUT_TRIANGLES          = 21;
        public static final int INPUT_TRIANGLES_ADJACENCY= 22;
        public static final int INPUT_QUADS              = 23;
        public static final int INPUT_ISOLINES           = 24;
        public static final int OUTPUT_VERTICES          = 25;
        public static final int OUTPUT_POINTS            = 26;
        public static final int OUTPUT_LINESTRIP         = 27;
        public static final int OUTPUT_TRIANGLESTRIP     = 28;
        public static final int VEC_TYPE_HINT            = 29;
        public static final int CONTRACTION_OFF          = 30;
    }

    /**
     * Spec : 3.7 Storage Class p.24
     */
    public static interface STORAGE_CLASS {
        public static final int UNIFORM_CONSTANT     =  0;
        public static final int INPUT                =  1;
        public static final int UNIFORM              =  2;
        public static final int OUTPUT               =  3;
        public static final int WORK_GROUP_LOCAL     =  4;
        public static final int WORK_GROUP_GLOBAL    =  5;
        public static final int PRIVATE_GLOBAL       =  6;
        public static final int FUNCTION             =  7;
        public static final int GENERIC              =  8;
        public static final int PRIVATE              =  9;
        public static final int ATOMIC_COUNTER       = 10;
    }

    /**
     * Spec : 3.8 Dim p.25
     */
    public static interface DIM {
        public static final int d1D      = 0;
        public static final int d2D      = 1;
        public static final int d3D      = 2;
        public static final int CUBE     = 3;
        public static final int RECT     = 4;
        public static final int BUFFER   = 5;
    }

    /**
     * Spec : 3.9 Sampler Addressing Mode p.25
     */
    public static interface SAMPLER_ADDRESSING_MODE {
        public static final int NONE            = 0;
        public static final int CLAMP_EDGE      = 2;
        public static final int CLAMP           = 4;
        public static final int REPEAT          = 6;
        public static final int REPEAT_MIRRORED = 8;
    }

    /**
     * Spec : 3.10 SSampler Filter Mode p.26
     */
    public static interface SAMPLER_FILTER_MODE {
        public static final int NEAREST = 16;
        public static final int LINEAR  = 32;
    }

    /**
     * Spec : 3.11 FP Fast Math Mode p.26
     */
    public static interface FP_FAST_MATH_MODE {
        public static final int NOT_NAN     = 0;
        public static final int NOT_INF     = 2;
        public static final int NSZ         = 4;
        public static final int ALLOW_RECIP = 8;
        public static final int FAST        = 16;
    }

    /**
     * Spec : 3.12 FP Rounding Mode p.27
     */
    public static interface FP_ROUNDING_MODE {
        public static final int RTE = 0;
        public static final int RTZ = 1;
        public static final int RTP = 2;
        public static final int RTN = 3;
    }

    /**
     * Spec : 3.13 Linkage Type p.27
     */
    public static interface LINKAGE_TYPE {
        public static final int EXPORT  = 0;
        public static final int IMPORT  = 1;
    }

    /**
     * Spec : 3.14 Access Qualifier p.27
     */
    public static interface ACCESS_QUALIFIER {
        public static final int READ_ONLY   = 0;
        public static final int WRITE_ONLY  = 1;
        public static final int READ_WRITE  = 2;
    }

    /**
     * Spec : 3.15 Function Parameter Attribute p.27
     */
    public static interface FUNCTION_PARAMETER_ATTRIBUTE {
        public static final int ZEST            = 0;
        public static final int SEXT            = 1;
        public static final int BY_VAL          = 2;
        public static final int SRET            = 3;
        public static final int NO_ALIAS        = 4;
        public static final int NO_CAPTURE      = 5;
        public static final int SVM             = 6;
        public static final int NO_WRITE        = 7;
        public static final int NO_READ_WRITE   = 8;
    }

    /**
     * Spec : 3.16 Decoration p.28
     */
    public static interface DECORATION {
        public static final int PRECISION_LOW           =  0;
        public static final int PRECISION_MEDIUM        =  1;
        public static final int PRECISION_HIGH          =  2;
        public static final int BLOCK                   =  3;
        public static final int BUFFER_BLOCK            =  4;
        public static final int ROW_MAJOR               =  5;
        public static final int COL_MAJOR               =  6;
        public static final int GLSL_SHARED             =  7;
        public static final int GLSL_STD_140            =  8;
        public static final int GLSL_STD_430            =  9;
        public static final int GLSL_PACKED             = 10;
        public static final int SMOOTH                  = 11;
        public static final int NO_PERSPECTIVE          = 12;
        public static final int FLAT                    = 13;
        public static final int PATCH                   = 14;
        public static final int CENTROID                = 15;
        public static final int SAMPLE                  = 16;
        public static final int INVARIANT               = 17;
        public static final int RESTRICT                = 18;
        public static final int ALIASED                 = 19;
        public static final int VOLATILE                = 20;
        public static final int CONSTANT                = 21;
        public static final int COHERENT                = 22;
        public static final int NON_WRITABLE            = 23;
        public static final int NON_READABLE            = 24;
        public static final int UNIFORM                 = 25;
        public static final int NO_STATIC_USE           = 26;
        public static final int CPACKED                 = 27;
        public static final int FP_SATURATED_CONVERSION = 28;
        public static final int STREAM                  = 29;
        public static final int LOCATION                = 30;
        public static final int COMPONENT               = 31;
        public static final int INDEX                   = 32;
        public static final int BINDING                 = 33;
        public static final int DESCRIPTOR_SET          = 34;
        public static final int OFFSET                  = 35;
        public static final int ALIGNEMENT              = 36;
        public static final int XFB_BUFFER              = 37;
        public static final int STRIDE                  = 38;
        public static final int BUILT_IN                = 39;
        public static final int FUNC_PARAM_ATTR         = 40;
        public static final int FP_ROUNDING_MODE        = 41;
        public static final int FP_FAST_MATH_MODE       = 42;
        public static final int LINKAGE_TYPE            = 43;
        public static final int SPEC_ID                 = 44;
    }

    /**
     * Spec : 3.17 Built-In p.32
     */
    public static interface BUILT_IN {
        public static final int POSITION                        =  0;
        public static final int POINT_SIZE                      =  1;
        public static final int CLIP_VERTEX                     =  2;
        public static final int CLIP_DISTANCE                   =  3;
        public static final int CULL_DISTANCE                   =  4;
        public static final int VERTEX_ID                       =  5;
        public static final int INSTANCE_ID                     =  6;
        public static final int PRIMITIVE_ID                    =  7;
        public static final int INVOCATION_ID                   =  8;
        public static final int LAYER                           =  9;
        public static final int VIEWPORT_INDEX                  = 10;
        public static final int TESS_LEVEL_OUTER                = 11;
        public static final int TESS_LEVEL_INNER                = 12;
        public static final int TESS_COORD                      = 13;
        public static final int PATCH_VERTICES                  = 14;
        public static final int FRAG_COORD                      = 15;
        public static final int POINT_COORD                     = 16;
        public static final int FRONT_FACING                    = 17;
        public static final int SAMPLE_ID                       = 18;
        public static final int SAMPLE_POSITION                 = 19;
        public static final int SAMPLE_MASK                     = 20;
        public static final int FRAG_COLOR                      = 21;
        public static final int FRAG_DEPTH                      = 22;
        public static final int HELPER_INVOCATION               = 23;
        public static final int NUM_WORKGROUPS                  = 24;
        public static final int WORKGROUP_SIZE                  = 25;
        public static final int WORKGROUP_ID                    = 26;
        public static final int LOCAL_INVOCATION_ID             = 27;
        public static final int GLOBAL_INVOCATION_ID            = 28;
        public static final int LOCAL_INVOCATION_INDEX          = 29;
        public static final int WORKDIM                         = 30;
        public static final int GLOBAL_SIZE                     = 31;
        public static final int ENQUEUED_WORKGROUP_SIZE         = 32;
        public static final int GLOBAL_OFFSET                   = 33;
        public static final int GLOBAL_LINEAR_ID                = 34;
        public static final int WORKGROUP_LINEAR_ID             = 35;
        public static final int SUBGROUP_SIZE                   = 36;
        public static final int SUBGROUP_MAXSIZE                = 37;
        public static final int NUM_SUBGROUPS                   = 38;
        public static final int NUM_ENQUEUED_SUBGROUPS          = 39;
        public static final int SUBGROUP_ID                     = 40;
        public static final int SUBGROUP_LOCAL_INVOCATION_ID    = 41;
    }

    /**
     * Spec : 3.18 Selection Control p.33
     */
    public static interface SELECTION_CONTROL {
        public static final int NOCONTROL   = 0;
        public static final int FLATTEN     = 1;
        public static final int DONTFLATTEN = 2;
    }

    /**
     * Spec : 3.19 Loop Control p.33
     */
    public static interface LOOP_CONTROL {
        public static final int NOCONTROL   = 0;
        public static final int UNROLL      = 1;
        public static final int DONTUNROLL  = 2;
    }

    /**
     * Spec : 3.20 Function Control Mask p.34
     */
    public static interface FUNCTION_CONTROL_MASK {
        public static final int INLINE      = 1;
        public static final int DONTINLINE  = 2;
        public static final int PURE        = 4;
        public static final int CONST       = 8;
    }

    /**
     * Spec : 3.21 Memory Semantics p.34
     */
    public static interface MEMORY_SEMANTICS {
        public static final int RELAXED                 = 1;
        public static final int SEQUENTIALLY_CONSISTENT = 2;
        public static final int AQUIRE                  = 4;
        public static final int RELEASE                 = 8;
        public static final int UNIFORM_MEMORY          = 16;
        public static final int SUBGROUP_MEMORY         = 32;
        public static final int WORKGROUP_LOCAL_MEMORY  = 64;
        public static final int WORKGROUP_GLOBAL_MEMORY = 128;
        public static final int ATOMIC_COUNTER_MEMORY   = 256;
        public static final int IMAGE_MEMORY            = 512;
    }

    /**
     * Spec : 3.22 Memory Access p.36
     */
    public static interface MEMORY_ACCESS {
        public static final int VOLATILE    = 1;
        public static final int ALIGNED     = 2;
    }

    /**
     * Spec : 3.23 Execution Scope p.36
     */
    public static interface EXECUTION_SCOPE {
        public static final int CROSS_DEVICE    = 0;
        public static final int DEVICE          = 1;
        public static final int WORKGROUP       = 2;
        public static final int SUBGROUP        = 3;
    }

    /**
     * Spec : 3.24 Group Operation p.38
     */
    public static interface GROUP_OPERATION {
        public static final int REDUCE          = 0;
        public static final int INCLUSIVE_SCAN  = 1;
        public static final int EXCLUSIVE_SCAN  = 2;
    }

    /**
     * Spec : 3.25 Kernel Enqueue Flags p.38
     */
    public static interface KERNEL_ENQUEUE_FLAGS {
        public static final int NO_WAIT         = 0;
        public static final int WAIT_KERNEL     = 1;
        public static final int WAIT_WORKGROUP  = 2;
    }

    /**
     * Spec : 3.26 Kernel Profiling Info p.39
     */
    public static interface KERNEL_PROFILING_INFO {
        public static final int CMD_EXEC_TIME = 1;
    }


    private SpirConstants() {}

}
