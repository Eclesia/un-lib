
package science.unlicense.code.spir;

import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.code.spir.model.Instruction;
import science.unlicense.code.spir.model.Module;

/**
 * SPIR-V Writer.
 *
 * @author Johann Sorel
 */
public class SpirWriter extends AbstractWriter {

    public void write(Module module) throws IOException{

        final DataOutputStream ds = getOutputAsDataStream(module.endiannness);

        //header
        if (module.endiannness == Endianness.BIG_ENDIAN) {
            ds.write(SpirConstants.SIGNATURE_BE);
        } else {
            ds.write(SpirConstants.SIGNATURE_LE);
        }
        ds.writeInt(module.version);
        ds.writeInt(module.generator);
        ds.writeInt(module.bound);
        ds.writeInt(module.schema);

        //instructions
        for (int i=0,n=module.instructions.getSize();i<n;i++) {
            final Instruction inst = (Instruction) module.instructions.get(i);

            final int word = ((inst.operands.length+1) << 16) | inst.type.getOpcode();
            ds.writeInt(word);
            ds.writeInt(inst.operands);
        }

    }

}
