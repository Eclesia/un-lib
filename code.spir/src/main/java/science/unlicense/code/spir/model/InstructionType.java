
package science.unlicense.code.spir.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;

/**
 *
 * @author Johann Sorel
 */
public class InstructionType extends CObject {

    public static final OperandType[] OPERAND_EMPTY = new OperandType[0];

    private final Chars name;
    private final int opcode;
    private final OperandType[] operands;

    public InstructionType(Chars name, int opcode, OperandType[] operands) {
        this.name = name;
        this.opcode = opcode;
        this.operands = operands;
    }

    public Chars getName() {
        return name;
    }

    public int getOpcode() {
        return opcode;
    }

    public OperandType[] getOperands() {
        return operands;
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(Int32.encode(opcode));
        cb.append(' ');
        cb.append(name);
        return cb.toChars();
    }

}
