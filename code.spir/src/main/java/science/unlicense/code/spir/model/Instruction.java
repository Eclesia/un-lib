
package science.unlicense.code.spir.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;

/**
 * Spir-V instruction.
 *
 * @author Johann Sorel
 */
public class Instruction extends CObject {

    public final InstructionType type;
    public int[] operands;

    public Instruction(InstructionType type) {
        this(type,Arrays.ARRAY_INT_EMPTY);
    }

    public Instruction(InstructionType type, int[] operands) {
        this.type = type;
        this.operands = operands;
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        final CharBuffer desc = new CharBuffer();
        desc.append('(');
        //align all instruction names
        cb.append(type.getName());
        for (int i=type.getName().getCharLength();i<42;i++) cb.append(' ');
        cb.append(" ["+operands.length+" words] ").append(' ');

        final int[] wordUsed = new int[1];
        int idx = 0;
        for (OperandType opType : type.getOperands()) {
            if (idx != 0) {
                cb.append(" ");
                desc.append(", ");
            }
            desc.append(opType.toChars());

            if (idx >= operands.length) {
                cb.append('-');
                continue;
            } else if (opType.isMultiple()) {
                //read until no more word left
                cb.append('[');
                boolean first = true;
                while (idx < operands.length) {
                    if (!first) cb.append(',');
                    wordUsed[0] = 0;
                    Object value = opType.readOneValue(operands, idx, wordUsed);
                    idx += wordUsed[0];
                    if (value instanceof Chars) cb.append('\'');
                    cb.append(value);
                    if (value instanceof Chars) cb.append('\'');
                    first = false;
                }
                cb.append(']');
            } else {
                wordUsed[0] = 0;
                Object value = opType.readOneValue(operands, idx, wordUsed);
                idx += wordUsed[0];
                if (value instanceof Chars) cb.append('\'');
                cb.append(value);
                if (value instanceof Chars) cb.append('\'');
            }

        }
        desc.append(')');
        cb.append(" ");
        for (int i=(cb.toChars().getCharLength()-56)%32; i<32;i++) cb.append(' ');
        cb.append(desc.toChars());
        return cb.toChars();
    }

}
