
package science.unlicense.code.spir.model;

import java.lang.reflect.Method;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public abstract class OperandType extends CObject{

    private final Chars name;
    private final Class valueClass;
    private final int quantifier;

    public OperandType(Chars name, Class valueClass, int quantifier) {
        this.name = name;
        this.valueClass = valueClass;
        this.quantifier = quantifier;
    }

    public Chars getName() {
        return name;
    }

    public Class getValueClass() {
        return valueClass;
    }

    public boolean isOptional() {
        return quantifier == '?';
    }

    public boolean isMultiple() {
        return quantifier == '*';
    }

    public abstract Object readOneValue(int[] words, int offset, int[] wordUsed);

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(getClass().getSimpleName());
        if (isOptional()) cb.append('?');
        if (isMultiple()) cb.append('*');
        return cb.toChars();
    }


    public static class IdResultType extends OperandType {

        public IdResultType(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            wordUsed[0] = 1;
            return words[offset];
        }
    }

    public static class IdResult extends OperandType {

        public IdResult(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            wordUsed[0] = 1;
            return words[offset];
        }
    }

    public static class IdMemorySemantics extends OperandType {

        public IdMemorySemantics(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            throw new UnimplementedException();
        }
    }

    public static class IdScope extends OperandType {

        public IdScope(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            throw new UnimplementedException();
        }
    }

    public static class IdRef extends OperandType {

        public IdRef(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            wordUsed[0] = 1;
            return words[offset];
        }
    }

    public static class LiteralInteger extends OperandType {

        public LiteralInteger(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            wordUsed[0] = 10;
            return words[offset];
        }
    }

    public static class LiteralString extends OperandType {

        public LiteralString(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            final ByteSequence seq = new ByteSequence();
            final byte[] buffer = new byte[4];
            for (;;) {
                wordUsed[0]++;
                Endianness.LITTLE_ENDIAN.writeInt(words[offset], buffer, 0);
                if (buffer[0] == 0) break;
                if (buffer[1] == 0) {seq.put(buffer,0,1); break;}
                if (buffer[2] == 0) {seq.put(buffer,0,2); break;}
                if (buffer[3] == 0) {seq.put(buffer,0,3); break;}
                seq.put(buffer);
                offset++;
            }
            return new Chars(seq.toArrayByte(),CharEncodings.UTF_8);
        }
    }

    public static class LiteralContextDependentNumber extends OperandType {

        public LiteralContextDependentNumber(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            wordUsed[0] = 1;
            return words[offset];
        }
    }

    public static class LiteralExtInstInteger extends OperandType {

        public LiteralExtInstInteger(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            throw new UnimplementedException();
        }
    }

    public static class LiteralSpecConstantOpInteger extends OperandType {

        public LiteralSpecConstantOpInteger(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            throw new UnimplementedException();
        }
    }

    public static class PairLiteralIntegerIdRef extends OperandType {

        public PairLiteralIntegerIdRef(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            throw new UnimplementedException();
        }
    }

    public static class PairIdRefLiteralInteger extends OperandType {

        public PairIdRefLiteralInteger(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            throw new UnimplementedException();
        }
    }

    public static class PairIdRefIdRef extends OperandType {

        public PairIdRefIdRef(Chars name, int quantifier) {
            super(name, Integer.class, quantifier);
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            throw new UnimplementedException();
        }
    }

    public static class Enumerated extends OperandType {

        public final Class enumClass;
        public Enumerated(Chars name, Class clazz, int quantifier) {
            super(name, Integer.class, quantifier);
            this.enumClass = clazz;
        }

        @Override
        public Object readOneValue(int[] words, int offset, int[] wordUsed) {
            wordUsed[0] = 1;
            try {
                final Method method = enumClass.getMethod("valueOf", int.class);
                return method.invoke(null, words[offset]);
            } catch (Throwable ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }

        @Override
        public Chars toChars() {
            final CharBuffer cb = new CharBuffer();
            cb.append(getClass().getSimpleName());
            cb.append('<');
            cb.append(enumClass.getSimpleName());
            cb.append('>');
            if (isOptional()) cb.append('?');
            if (isMultiple()) cb.append('*');
            return cb.toChars();
        }
    }

}
