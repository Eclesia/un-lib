
package science.unlicense.code.spir;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * Standard Portable Intermediate Representation format.
 *
 * Specification :
 * https://www.khronos.org/registry/spir-v/specs/1.0/SPIRV.pdf
 *
 * NOTE/TODO : make a common API Format/Store for language ?
 *
 * @author Johann Sorel
 */
public class SpirFormat extends DefaultFormat {

    public static final SpirFormat INSTANCE = new SpirFormat();

    public SpirFormat() {
        super(new Chars("spirv"));
        shortName = new Chars("Spir-V");
        longName = new Chars("Standard Portable Intermediate Representation");
        signatures.add(SpirConstants.SIGNATURE_BE);
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
