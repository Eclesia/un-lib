
package science.unlicense.code.spir.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Endianness;

/**
 * SPIR Module.
 * Similar to a opengl shader program or opencl kernel.
 *
 * @author Johann Sorel
 */
public class Module extends CObject {

    public Endianness endiannness;

    /**
     * SPIR-V version :
     * -  99 pre-release
     * - 100 v1.00
     */
    public int version;
    /**
     * Value associated with a compiler.
     * Metadata information which has no impact on the code.
     * Can be 0 even if such value is discouraged.
     */
    public int generator;
    /**
     * Maximum number used for an id.
     */
    public int bound;
    /**
     * Reserved for instruction schema.
     */
    public int schema;

    /**
     * Sequence of instructions
     */
    public final Sequence instructions = new ArraySequence();

    public Chars debug() {
        final CharBuffer cb = new CharBuffer();
        cb.append("SPIR-V Module");
        for (int i=0,n=instructions.getSize();i<n;i++) {
            cb.append("\n").append(instructions.get(i));
        }
        return cb.toChars();
    }
}
