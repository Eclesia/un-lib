
package science.unlicense.format.mqo;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.IntSequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.regex.NFAStep;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import static science.unlicense.format.mqo.MQOConstants.*;
import science.unlicense.format.mqo.model.MQOFace;
import science.unlicense.format.mqo.model.MQOMaterial;
import science.unlicense.format.mqo.model.MQOObject;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.VectorRW;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.RenderState;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.Technique;
import science.unlicense.syntax.api.lexer.Lexer;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenType;
import static science.unlicense.syntax.api.parser.NFARuleState.*;
import science.unlicense.syntax.api.parser.NFATokenState;
import static science.unlicense.syntax.api.parser.NFATokenState.*;
import science.unlicense.syntax.api.parser.ParserStream;
import science.unlicense.syntax.api.parser.Rule;
import science.unlicense.syntax.impl.grammar.io.UNGrammarReader;

/**
 *
 * @author Johann Sorel
 */
public class MQOStore extends AbstractModel3DStore {

    private final Path base;

    private int matId = 0;
    private final Dictionary materials = new HashDictionary();
    private final Dictionary materialTexs = new HashDictionary();
    private final Sequence objects = new ArraySequence();


    public MQOStore(Object input) {
        super(MQOFormat.INSTANCE,input);
        this.base = (Path) input;
    }

    public Collection getElements() throws StoreException {
        if (objects.isEmpty()){
            try {
                read();
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }
        final SceneNode node = rebuildRoot();
        final Sequence elements = new ArraySequence();
        elements.add(node);
        return elements;
    }

    private void read() throws IOException, StoreException{

        matId=0;
        materials.removeAll();
        materialTexs.removeAll();
        objects.removeAll();

        final ByteInputStream in = getSourceAsInputStream();
        //parse text content
        final UNGrammarReader reader = new UNGrammarReader();
        reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/format/mqo/mqo.gr")));
        final OrderedHashDictionary tokens = new OrderedHashDictionary();
        final OrderedHashDictionary rules = new OrderedHashDictionary();
        reader.read(tokens, rules);

        // grammar parser is already tested
        final Rule rule = (Rule) rules.getValue(new Chars("file"));

        //prepare lexer
        final Lexer lexer = new Lexer(CharEncodings.SHIFT_JIS);
        lexer.setInput(in);


        final TokenType[] toExclude = new TokenType[]{
            (TokenType) tokens.getValue(new Chars("EOF")),
            (TokenType) tokens.getValue(new Chars("HEADERA")),
            (TokenType) tokens.getValue(new Chars("HEADERB")),
            (TokenType) tokens.getValue(new Chars("WS")),
            (TokenType) tokens.getValue(new Chars("CRLF")),
            (TokenType) tokens.getValue(new Chars("AL")),
            (TokenType) tokens.getValue(new Chars("AR")),
            (TokenType) tokens.getValue(new Chars("PL")),
            (TokenType) tokens.getValue(new Chars("PR"))
        };

        final Predicate exclude = new Predicate() {
            public Boolean evaluate(Object candidate) {
                return candidate instanceof NFATokenState &&
                        Arrays.containsIdentity(toExclude, 0, toExclude.length, ((NFATokenState) candidate).getTokenType());
            }
        };

        final Rule lsRule = (Rule) rules.getValue(new Chars("ls"));

        //prepare parser
        final ParserStream stream = new ParserStream(lexer, rule, exclude);
        NFAStep step = stream.next();
        if (!isRuleStart(step.state, RULE_FILE)){
            throw new IOException(getInput(), "File rule not found");
        }

        for (step=stream.next();step!=null;step=stream.next()){
            if (isRuleStart(step.state, RULE_MATERIALS)){
                readMaterials(stream);
            } else if (isRuleStart(step.state, RULE_OBJECT)){
                final MQOObject obj = new MQOObject();
                obj.read(stream);
                objects.add(obj);
            }
        }

        //read textures
        final Iterator matite = materials.getPairs().createIterator();
        while (matite.hasNext()){
            final Pair pair = (Pair) matite.next();
            final int id = (Integer) pair.getValue1();
            final MQOMaterial material = (MQOMaterial) pair.getValue2();

            final Chars tex = material.getTex();
            if (tex!=null){
                final Path imgPath = base.getParent().resolve(tex);
                System.out.println(imgPath);
                final Image img;
                try {
                    img = Images.read(imgPath);
                    final Texture2D texture = new Texture2D(img);
                    materialTexs.add(id, texture);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void readMaterials(ParserStream stream) throws IOException{
        NFAStep step;
        for (step=stream.next();step!=null;step=stream.next()){
            if (isRuleStart(step.state, RULE_MATERIAL)){
                final MQOMaterial material = new MQOMaterial();
                materials.add(matId, material);
                matId++;
                material.read(stream);
            } else if (isRuleEnd(step.state, RULE_MATERIALS)){
                break;
            }
        }
    }

    public static Object readValue(ParserStream stream) throws IOException{
        Object val = null;
        NFAStep step;
        for (step=stream.next();step!=null;step=stream.next()){
            if (isToken(step.state, TOKEN_NUMBER)){
                val = Float64.decode(((Token) step.value).value);
            } else if (isToken(step.state, TOKEN_ESCWORD)){
                val = ((Token) step.value).value;
            } else if (isRuleEnd(step.state, MQOConstants.RULE_VALUE)){
                break;
            }
        }
        return val;
    }

    private SceneNode rebuildRoot() throws StoreException{
        final SceneNode root = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        for (int i=0,n=objects.getSize();i<n;i++){
            final MQOObject mo = (MQOObject) objects.get(i);
            root.getChildren().add(rebuildMesh(mo));
        }
        return root;
    }

    private SceneNode rebuildMesh(MQOObject obj) throws StoreException{

        final SceneNode mpm = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);

        //rebuild vertex buffer
        final int vertSize = obj.vertex.getSize();
        final Float32Cursor vertBuffer = DefaultBufferFactory.INSTANCE.createFloat32(vertSize*3).cursor();
        final float[] fb = new float[3];
        for (int i=0,n=obj.vertex.getSize();i<n;i++){
            final VectorRW v = (VectorRW) obj.vertex.get(i);
            v.toFloat(fb,0);
            vertBuffer.write(fb);
        }
        final VBO vbo = new VBO(vertBuffer.getBuffer(), 3);

        //regroup faces by material
        final int nbFaces = obj.faces.getSize();
        final Dictionary groups = new HashDictionary();
        for (int i=0;i<nbFaces;i++){
            final MQOFace face = (MQOFace) obj.faces.get(i);
            Sequence seq = (Sequence) groups.getValue(face.materialIndex);
            if (seq==null){
                seq = new ArraySequence();
                groups.add(face.materialIndex, seq);
            }
            seq.add(face);
        }

        final Iterator ite = groups.getPairs().createIterator();
        while (ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final int matIndex = (Integer) pair.getValue1();
            final Sequence faces = (Sequence) pair.getValue2();

            //rebuild faces
            final Float32Cursor uvBuffer = DefaultBufferFactory.INSTANCE.createFloat32(vertSize*2).cursor();
            final IntSequence indexBuffer = new IntSequence();

            for (int i=0,n=faces.getSize();i<n;i++){
                final MQOFace face = (MQOFace) faces.get(i);

                if (face.size==3){
                    indexBuffer.put(face.vertexIndex);
                } else if (face.size==4){
                    indexBuffer.put(face.vertexIndex[0]);
                    indexBuffer.put(face.vertexIndex[1]);
                    indexBuffer.put(face.vertexIndex[2]);

                    indexBuffer.put(face.vertexIndex[0]);
                    indexBuffer.put(face.vertexIndex[2]);
                    indexBuffer.put(face.vertexIndex[3]);

                } else {
                    indexBuffer.put(face.vertexIndex[0]);
                    indexBuffer.put(face.vertexIndex[1]);
                    indexBuffer.put(face.vertexIndex[1]);
                    //throw new StoreException("Unsupported number of element in face : "+face.size);
                }

                if (face.uv!=null){
                    int k=0;
                    for (int id : face.vertexIndex){
                        uvBuffer.offset(id*2);
                        uvBuffer.write(face.uv[k]);
                        uvBuffer.write(face.uv[k+1]);
                        k+=2;
                    }
                }
            }

            //rebuild mesh
            final DefaultModel mesh = new DefaultModel();
            mesh.getTechniques().add(new SimpleBlinnPhong());
            ((Technique) mesh.getTechniques().get(0)).getState().setCulling(RenderState.CULLING_NONE);
            final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
            shell.setPositions(vbo);
            shell.setUVs(new VBO(uvBuffer.getBuffer(), 2));
            shell.setIndex(new IBO(indexBuffer.toArrayInt()));
            shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, indexBuffer.getSize())});
            Mesh.calculateNormals(shell);
            mesh.setShape(shell);

            //rebuild material
            final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
            mesh.getMaterials().add(material);
            final Texture2D tex = (Texture2D) materialTexs.getValue(matIndex);
            if (tex!=null) {
                material.setDiffuseTexture(new TextureMapping(tex));
            }

            mpm.getChildren().add(mesh);
        }

        return mpm;
    }

}
