
package science.unlicense.format.mqo.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.regex.NFAStep;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import static science.unlicense.format.mqo.MQOConstants.*;
import science.unlicense.syntax.api.lexer.Token;
import static science.unlicense.syntax.api.parser.NFARuleState.*;
import static science.unlicense.syntax.api.parser.NFATokenState.*;
import science.unlicense.syntax.api.parser.ParserStream;
import science.unlicense.syntax.api.parser.SyntaxNode;

/**
 *
 * @author Johann Sorel
 */
public class MQOFace {

    private static final Chars FCT_VERTEX = Chars.constant("V");
    private static final Chars FCT_MATERIAL = Chars.constant("M");
    private static final Chars FCT_UV = Chars.constant("UV");

    //3 or 4
    public int size;
    public int[] vertexIndex;
    public int materialIndex;
    public float[] uv;
    public int colorMask;
    //Catmull-Clark
    public int crs;

    void read(SyntaxNode ast) throws StoreException {

        size = Int32.decode( ((SyntaxNode) ast.getChildren().get(0)).getToken().value);

        final Sequence functions = ast.getChildrenByRule(RULE_FUNCTION);
        for (int i=0,n=functions.getSize();i<n;i++){
            final SyntaxNode sn = (SyntaxNode) functions.get(i);
            final Chars name = ((SyntaxNode) sn.getChildren().get(0)).getToken().value;

            if (FCT_VERTEX.equals(name)){
                vertexIndex = childValueAsInt(sn);
            } else if (FCT_UV.endsWith(name)){
                uv = childValueAsFloat(sn);
            } else if (FCT_MATERIAL.endsWith(name)){
                materialIndex = childValueAsInt(sn)[0];
            }
        }
    }

    public void read(ParserStream stream) throws IOException{
        for (NFAStep step=stream.next();step!=null;step=stream.next()){
            if (isToken(step.state, TOKEN_NUMBER)){
                size = Int32.decode( ((Token) step.value).value);
            }if (isRuleStart(step.state, RULE_FUNCTION)){
                final MQOFunction fct = new MQOFunction();
                fct.read(stream);
                if (FCT_VERTEX.equals(fct.name)){
                    vertexIndex = new int[fct.params.length];
                    for (int i=0;i<vertexIndex.length;i++){
                        vertexIndex[i] = ((Double) fct.params[i]).intValue();
                    }
                } else if (FCT_UV.endsWith(fct.name)){
                    uv = new float[fct.params.length];
                    for (int i=0;i<vertexIndex.length;i++){
                        uv[i] = ((Double) fct.params[i]).floatValue();
                    }
                } else if (FCT_MATERIAL.endsWith(fct.name)){
                    materialIndex = ((Double) fct.params[0]).intValue();
                }

            } else if (isRuleEnd(step.state, RULE_FACE)){
                break;
            }
        }
    }

}
