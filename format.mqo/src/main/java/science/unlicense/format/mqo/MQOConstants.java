
package science.unlicense.format.mqo;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.parser.SyntaxNode;

/**
 * Metasequoia constants.
 *
 * @author Johann Sorel
 */
public final class MQOConstants {

    public static final Chars HEADER_TYPE = Chars.constant("Metasequoia Document");
    public static final Chars HEADER_VERSION = Chars.constant("Format Text");

    public static final Chars FORMAT_TEXT = Chars.constant("text");
    public static final Chars FORMAT_COMPRESS = Chars.constant("compress");


    public static final Chars TOKEN_SCENE       = Chars.constant("SCENE");
    public static final Chars TOKEN_BACKIMAGE   = Chars.constant("BACKIMAGE");
    public static final Chars TOKEN_OBJECT      = Chars.constant("OBJECT");
    public static final Chars TOKEN_MATERIAL    = Chars.constant("MATERIAL");
    public static final Chars TOKEN_VERTEX      = Chars.constant("VERTEX");
    public static final Chars TOKEN_FACE        = Chars.constant("FACE");
    public static final Chars TOKEN_NUMBER      = Chars.constant("NUMBER");
    public static final Chars TOKEN_WORD        = Chars.constant("WORD");
    public static final Chars TOKEN_ESCWORD     = Chars.constant("ESCWORD");

    public static final Chars RULE_VALUE       = Chars.constant("value");
    public static final Chars RULE_FUNCTION    = Chars.constant("function");
    public static final Chars RULE_PROPERTY    = Chars.constant("property");
    public static final Chars RULE_SCENE       = Chars.constant("scene");
    public static final Chars RULE_BACKIMAGE   = Chars.constant("backimage");
    public static final Chars RULE_MATERIAL    = Chars.constant("material");
    public static final Chars RULE_MATERIALS   = Chars.constant("materials");
    public static final Chars RULE_VERTEX      = Chars.constant("vertex");
    public static final Chars RULE_VERTICES    = Chars.constant("vertices");
    public static final Chars RULE_FACE        = Chars.constant("face");
    public static final Chars RULE_FACES       = Chars.constant("faces");
    public static final Chars RULE_OBJECT      = Chars.constant("object");
    public static final Chars RULE_INCLUDE     = Chars.constant("include");
    public static final Chars RULE_FILE        = Chars.constant("file");


    public static final Chars SCENE = Chars.constant("scene");
    public static final Chars SCENE_POS = Chars.constant("pos");
    public static final Chars SCENE_LOOKAT = Chars.constant("lookat");
    public static final Chars SCENE_HEAD = Chars.constant("head");
    public static final Chars SCENE_PICH = Chars.constant("pich");
    public static final Chars SCENE_ORTHO = Chars.constant("ortho");
    public static final Chars SCENE_ZOOM = Chars.constant("zoom");
    public static final Chars SCENE_AMB = Chars.constant("amb");

    public static final Chars MATERIAL = Chars.constant("material");
    public static final Chars MATERIAL_SHADER = Chars.constant("shader");
    public static final Chars MATERIAL_VCOL = Chars.constant("vcol");
    public static final Chars MATERIAL_DBLS = Chars.constant("dbls");
    public static final Chars MATERIAL_COL = Chars.constant("col");
    public static final Chars MATERIAL_DIF = Chars.constant("dif");
    public static final Chars MATERIAL_AMB = Chars.constant("amb");
    public static final Chars MATERIAL_EMI = Chars.constant("emi");
    public static final Chars MATERIAL_SPC = Chars.constant("spc");
    public static final Chars MATERIAL_POWER = Chars.constant("power");
    public static final Chars MATERIAL_TEX = Chars.constant("tex");
    public static final Chars MATERIAL_APLANE = Chars.constant("aplane");
    public static final Chars MATERIAL_BUMP = Chars.constant("bump");
    public static final Chars MATERIAL_PROJ_TYPE = Chars.constant("proj_type");
    public static final Chars MATERIAL_PROJ_POS = Chars.constant("proj_pos");
    public static final Chars MATERIAL_PROJ_SCALE = Chars.constant("proj_scale");
    public static final Chars MATERIAL_PROJ_ANGLE = Chars.constant("proj_angle");

    public static final Chars OBJECT = Chars.constant("object");
    public static final Chars OBJECT_UID = Chars.constant("uid");
    public static final Chars OBJECT_DEPTH = Chars.constant("depth");
    public static final Chars OBJECT_FOLDING = Chars.constant("folding");
    public static final Chars OBJECT_SCALE = Chars.constant("scale");
    public static final Chars OBJECT_ROTATION = Chars.constant("rotation");
    public static final Chars OBJECT_TRANSLATION = Chars.constant("translation");
    public static final Chars OBJECT_PATCH = Chars.constant("patch");
    public static final Chars OBJECT_PATCHTRI = Chars.constant("patchtri");
    public static final Chars OBJECT_SEGMENT = Chars.constant("segment");
    public static final Chars OBJECT_VISIBLE = Chars.constant("visible");
    public static final Chars OBJECT_LOCKING = Chars.constant("locking");
    public static final Chars OBJECT_SHADING = Chars.constant("shading");
    public static final Chars OBJECT_FACET = Chars.constant("facet");
    public static final Chars OBJECT_COLOR = Chars.constant("color");
    public static final Chars OBJECT_COLOR_TYPE = Chars.constant("color_type");
    public static final Chars OBJECT_MIRROR = Chars.constant("mirror");
    public static final Chars OBJECT_MIRROR_AXIS = Chars.constant("mirror_axis");
    public static final Chars OBJECT_MIRROR_DIS = Chars.constant("mirror_dis");
    public static final Chars OBJECT_LATHE = Chars.constant("lathe");
    public static final Chars OBJECT_LATHE_AXIS = Chars.constant("lathe_axis");
    public static final Chars OBJECT_LATHE_SEG = Chars.constant("lathe_seg");
    public static final Chars OBJECT_VERTEX = Chars.constant("vertex");
    public static final Chars OBJECT_BVERTEX = Chars.constant("bvertex");
    public static final Chars OBJECT_FACE = Chars.constant("face");

    public static final Chars EOF = Chars.constant("eof");

    private MQOConstants(){}


    public static float[] childAsFloat(SyntaxNode n) throws StoreException{
        final float[] array = new float[n.getChildren().getSize()];
        for (int i=0;i<array.length;i++){
            final SyntaxNode sn = (SyntaxNode) n.getChildren().get(i);
            final Token token = sn.getToken();
            if (token!=null){
                array[i] = (float) Float64.decode(token.value);
            } else {
                throw new StoreException("Unexpected node "+n);
            }

        }
        return array;
    }

    public static int[] childAsInt(SyntaxNode n) throws StoreException{
        final int[] array = new int[n.getChildren().getSize()];
        for (int i=0;i<array.length;i++){
            final SyntaxNode sn = (SyntaxNode) n.getChildren().get(i);
            final Token token = sn.getToken();
            if (token!=null){
                array[i] = Int32.decode(token.value);
            } else {
                throw new StoreException("Unexpected node "+n);
            }
        }
        return array;
    }

    public static float[] childValueAsFloat(SyntaxNode n) throws StoreException{
        final Sequence children = n.getChildrenByRule(RULE_VALUE);
        final float[] array = new float[children.getSize()];
        for (int i=0;i<array.length;i++){
            SyntaxNode sn = (SyntaxNode) children.get(i);
            sn = (SyntaxNode) sn.getChildren().get(0);
            final Token token = sn.getToken();
            if (token!=null){
                array[i] = (float) Float64.decode(token.value);
            } else {
                throw new StoreException("Unexpected node "+n);
            }
        }
        return array;
    }

    public static int[] childValueAsInt(SyntaxNode n) throws StoreException{
        final Sequence children = n.getChildrenByRule(RULE_VALUE);
        final int[] array = new int[children.getSize()];
        for (int i=0;i<array.length;i++){
            SyntaxNode sn = (SyntaxNode) children.get(i);
            sn = (SyntaxNode) sn.getChildren().get(0);
            final Token token = sn.getToken();
            if (token!=null){
                array[i] = Int32.decode(token.value);
            } else {
                throw new StoreException("Unexpected node "+n);
            }
        }
        return array;
    }

}
