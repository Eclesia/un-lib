
package science.unlicense.format.mqo.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.syntax.api.parser.ParserStream;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.syntax.api.parser.SyntaxNode;
import science.unlicense.common.api.regex.NFAStep;
import static science.unlicense.syntax.api.parser.NFARuleState.*;
import static science.unlicense.syntax.api.parser.NFATokenState.*;
import science.unlicense.math.impl.VectorNf64;
import static science.unlicense.format.mqo.MQOConstants.*;


/**
 *
 * @author Johann Sorel
 */
public class MQOObject {

    public Chars name;

    public int uid;
    public int depth;
    public int folding;
    public Tuple scale;
    public Tuple rotation;
    public Tuple translation;
    public int patch;
    public int patchtri;
    public int segment;
    public int visible;
    public int locking;
    public int shading;
    public float facet;
    public Tuple color;
    public int color_type;
    public int mirror;
    public int mirror_axis;
    public float mirror_dis;
    public int lathe;
    public int lathe_axis;
    public int lathe_seg;

    /** sequence of vertice tuple : Tuple*/
    public final Sequence vertex = new ArraySequence();
    /** sequence of faces tuple MQOFace */
    public final Sequence faces = new ArraySequence();


    public void read(SyntaxNode ast) throws StoreException{

        //rebuild vertex buffer
        final SyntaxNode verticesNode = ast.getChildByRule(RULE_VERTICES);
        final Sequence verticeNodes = verticesNode.getChildrenByRule(RULE_VERTEX);
        final int nbVertices = Int32.decode(verticesNode.getChildByToken(TOKEN_NUMBER).getToken().value);

        for (int i=0;i<nbVertices;i++){
            final SyntaxNode vertexNode = (SyntaxNode) verticeNodes.get(i);
            vertex.add(VectorNf64.create(childAsFloat(vertexNode)));
        }

        //rebuild faces
        final SyntaxNode facesNode = ast.getChildByRule(RULE_FACES);
        final Sequence faceNodes = facesNode.getChildrenByRule(RULE_FACE);
        final int nbFaces = Int32.decode(facesNode.getChildByToken(TOKEN_NUMBER).getToken().value);

        for (int i=0;i<nbFaces;i++){
            final SyntaxNode faceNode = (SyntaxNode) faceNodes.get(i);

            final MQOFace face = new MQOFace();
            face.read(faceNode);
            faces.add(face);

        }

    }

    public void read(ParserStream stream) throws IOException{
        for (NFAStep step=stream.next();step!=null;step=stream.next()){
            if (isToken(step.state, TOKEN_ESCWORD)){
                name = ((Token) step.value).value;
                System.out.println(">>>>> "+name);
            } else if (isRuleStart(step.state, RULE_PROPERTY)){
                //TODO
            } else if (isRuleStart(step.state, RULE_VERTICES)){
                readVertices(stream);
            } else if (isRuleStart(step.state, RULE_FACES)){
                readFaces(stream);
            } else if (isRuleEnd(step.state, RULE_OBJECT)){
                break;
            }
        }
    }

    private void readVertices(ParserStream stream) throws IOException{
        for (NFAStep step=stream.next();step!=null;step=stream.next()){
            if (isRuleStart(step.state, RULE_VERTEX)){
                readVertex(stream);
            } else if (isRuleEnd(step.state, RULE_VERTICES)){
                break;
            }
        }
    }

    private void readVertex(ParserStream stream) throws IOException{
        final VectorRW v = VectorNf64.createDouble(3);
        int i = 0;
        for (NFAStep step=stream.next();step!=null;step=stream.next()){
            if (isToken(step.state, TOKEN_NUMBER)){
                v.set(i, Float64.decode( ((Token) step.value).value) );
                i++;
            } else if (isRuleEnd(step.state, RULE_VERTEX)){
                break;
            }
        }
        vertex.add(v);
    }

    private void readFaces(ParserStream stream) throws IOException{
        for (NFAStep step=stream.next();step!=null;step=stream.next()){
            if (isRuleStart(step.state, RULE_FACE)){
                final MQOFace face = new MQOFace();
                face.read(stream);
                faces.add(face);
            } else if (isRuleEnd(step.state, RULE_FACES)){
                break;
            }
        }
    }

}
