
package science.unlicense.format.mqo.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.regex.NFAStep;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import static science.unlicense.format.mqo.MQOConstants.*;
import static science.unlicense.syntax.api.parser.NFARuleState.*;
import science.unlicense.syntax.api.parser.ParserStream;
import science.unlicense.syntax.api.parser.SyntaxNode;


/**
 *
 * @author Johann Sorel
 */
public class MQOMaterial {

    private static final Chars FCT_TEXTURE = Chars.constant("tex");

    public final Dictionary functions = new HashDictionary();

//    //0 Classic
//    //1 Constant
//    //2 Lambert
//    //3 Phong
//    //4 Blinn
//    public int shader;
//
//    public float vcol;
//    public float dbls;
//    public Tuple color;
//
//    // 0-1
//    public float dif;
//    public float amb;
//    public float emi;
//    public float spc;
//
//    //0-100
//    public float power;
//
//    public Chars tex;
//    public Chars aplane;
//    public Chars bump;
//
//    //0 UV
//    //1
//    //2
//    //3
//    public int proj_type;
//    public Tuple proj_pos;
//    public Tuple proj_scale;
//    //-180,+180
//    public Tuple proj_angle;


    public Chars getTex() {
        final MQOFunction fct = (MQOFunction) functions.getValue(FCT_TEXTURE);
        if (fct!=null){
            Chars path = (Chars) fct.params[0];
            path = path.truncate(1, path.getCharLength()-1);
            return path;
        }
        return null;
    }

    public void read(SyntaxNode ast) throws StoreException{

//        final SyntaxNode matName = ast.getChildByToken(TOKEN_ESCWORD);
//
//        final Sequence functions = ast.getChildrenByRule(RULE_FUNCTION);
//        for (int i=0,n=functions.getSize();i<n;i++){
//            final SyntaxNode sn = (SyntaxNode) functions.get(i);
//            final Chars name = ((SyntaxNode) sn.getChild(0)).getToken().value;
//            final Sequence values = sn.getChildrenByRule(RULE_VALUE);
//
//            if (FCT_TEXTURE.equals(name)){
//                final SyntaxNode value = (SyntaxNode) values.get(0);
//                tex = ((SyntaxNode) value.getChild(0)).getToken().value;
//                //remove ""
//                tex = tex.clip(1, tex.getCharLength()-1);
//            }
//
//        }

    }

    public void read(ParserStream stream) throws IOException{
        for (NFAStep step=stream.next();step!=null;step=stream.next()){
            if (isRuleStart(step.state, RULE_FUNCTION)){
                final MQOFunction fct = new MQOFunction();
                fct.read(stream);
                functions.add(fct.name, fct);
            } else if (isRuleEnd(step.state, RULE_MATERIAL)){
                break;
            }
        }
    }

}
