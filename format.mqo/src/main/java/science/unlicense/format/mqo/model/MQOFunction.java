
package science.unlicense.format.mqo.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.parser.ParserStream;
import science.unlicense.common.api.regex.NFAStep;
import static science.unlicense.syntax.api.parser.NFARuleState.*;
import static science.unlicense.syntax.api.parser.NFATokenState.*;
import static science.unlicense.format.mqo.MQOConstants.*;
import science.unlicense.format.mqo.MQOStore;

/**
 *
 * @author Johann Sorel
 */
public class MQOFunction {

    public Chars name;
    public Object[] params = new Object[0];

    public void read(ParserStream stream) throws IOException{
        for (NFAStep step=stream.next();step!=null;step=stream.next()){
            if (isToken(step.state, TOKEN_WORD)){
                name = ((Token) step.value).value;
            } else if (isRuleStart(step.state, RULE_VALUE)){
                params = Arrays.insert(params, params.length, MQOStore.readValue(stream));
            } else if (isRuleEnd(step.state, RULE_FUNCTION)){
                break;
            }
        }
    }

}
