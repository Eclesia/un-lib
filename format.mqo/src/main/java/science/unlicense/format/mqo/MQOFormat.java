
package science.unlicense.format.mqo;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * Specification :
 * http://metaseq.net/en/format.html
 *
 * @author Johann Sorel
 */
public class MQOFormat extends AbstractModel3DFormat {

    public static final MQOFormat INSTANCE = new MQOFormat();

    private MQOFormat() {
        super(new Chars("MQO"));
        shortName = new Chars("MQO");
        longName = new Chars("MQO");
        extensions.add(new Chars("mqo"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new MQOStore(input);
    }

}
