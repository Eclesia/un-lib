
package science.unlicense.format.tiff;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * @author Johann Sorel
 */
public class TIFFFormat extends AbstractImageFormat {

    public static final TIFFFormat INSTANCE = new TIFFFormat();

    private TIFFFormat() {
        super(new Chars("tiff"));
        shortName = new Chars("TIFF");
        longName = new Chars("Tagged Image File Format");
        mimeTypes.add(new Chars("image/tiff"));
        extensions.add(new Chars("tif"));
        extensions.add(new Chars("tiff"));
        signatures.add(new byte[]{0x49,0x49,0x2A,0x00});
        signatures.add(new byte[]{0x4D,0x4D,0x00,0x2A});
        signatures.add(new byte[]{0x4D,0x4D,0x00,0x2B});
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new TIFFStore(this, source);
    }

}
