

package science.unlicense.format.tiff;

import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.encoding.api.io.AbstractInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;

/**
 * Stream concatenating tiff strips.
 *
 * @author Johann Sorel
 */
public class StripInputStream extends AbstractInputStream {

    private final BacktrackInputStream bs;
    private final TypedNode[] stripOffsets;
    private int stripOffset = -1;
    private final int stripSize;
    private int bufferOffset = -1;

    public StripInputStream(BacktrackInputStream bs, TypedNode[] stripOffsets, int stripSize) {
        this.bs = bs;
        this.stripOffsets = stripOffsets;
        this.stripSize = stripSize;
    }

    @Override
    public int read() throws IOException {
        if (bufferOffset>=stripSize){
            bufferOffset=-1;
        }

        if (bufferOffset==-1){
            stripOffset++;
            if (stripOffset>=stripOffsets.length){
                //no more strips
                return -1;
            }
            bs.rewind();
            bs.skip( ((Number) stripOffsets[stripOffset].getValue()).longValue() );
        }
        bufferOffset++;
        return bs.read();
    }

    @Override
    public void dispose() throws IOException {

    }

}
