package science.unlicense.format.tiff;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.DefaultNodeCardinality;
import science.unlicense.common.api.model.tree.DefaultNodeType;
import science.unlicense.common.api.model.tree.NodeCardinality;
import science.unlicense.common.api.model.tree.NodeType;

/**
 *
 * @author Johann Sorel
 */
public final class TIFFMetaModel {

    /** 2-3 bytes of all tiff files */
    public static final int SIGNATURE = 42;

    /** BYTE 8-bit unsigned integer. */
    public static final int TYPE_BYTE = 1;
    /** ASCII 8-bit byte that contains a 7-bit ASCII code; the last byte must be NUL (binary zero). */
    public static final int TYPE_ASCII = 2;
    /** SHORT 16-bit (2-byte) unsigned integer. */
    public static final int TYPE_SHORT = 3;
    /** LONG 32-bit (4-byte) unsigned integer. */
    public static final int TYPE_LONG = 4;
    /**
     * RATIONAL Two LONGs: the first represents the numerator of a
     *           fraction; the second, the denominator.
     * The value of the Count part of an ASCII field entry includes the NUL. If padding
     * is necessary, the Count does not include the pad byte. Note that there is no initial
     * “count byte” as in Pascal-style strings.
     * Any ASCII field can contain multiple strings, each terminated with a NUL. A
     * single string is preferred whenever possible. The Count for multi-string fields is
     * the number of bytes in all the strings in that field plus their terminating NUL
     * bytes. Only one NUL is allowed between strings, so that the strings following the
     * first string will often begin on an odd byte.
     * The reader must check the type to verify that it contains an expected value. TIFF
     * currently allows more than 1 valid type for some fields. For example, ImageWidth
     * and ImageLength are usually specified as having type SHORT. But images with
     * more than 64K rows or columns must use the LONG field type.
     * TIFF readers should accept BYTE, SHORT, or LONG values for any unsigned
     * integer field. This allows a single procedure to retrieve any integer value, makes
     * reading more robust, and saves disk space in some situations.
     * In TIFF 6.0, some new field types have been defined:
     */
    public static final int TYPE_RATIONAL = 5;
    /** SBYTE An 8-bit signed (twos-complement) integer. */
    public static final int TYPE_SBYTE = 6;
    /** UNDEFINED An 8-bit byte that may contain anything, depending on the definition of the field. */
    public static final int TYPE_UNDEFINED = 7;
    /** SSHORT A 16-bit (2-byte) signed (twos-complement) integer. */
    public static final int TYPE_SSHORT = 8;
    /** SLONG A 32-bit (4-byte) signed (twos-complement) integer. */
    public static final int TYPE_SLONG = 9;
    /** SRATIONAL Two SLONG’s: the first represents the numerator of a fraction, the second the denominator. */
    public static final int TYPE_SRATIONAL = 10;
    /** FLOAT Single precision (4-byte) IEEE format.*/
    public static final int TYPE_FLOAT = 11;
    /** DOUBLE Double precision (8-byte) IEEE format.*/
    public static final int TYPE_DOUBLE = 12;


    public static final int TAG_NEW_SUBFILE_TYPE = 254;
    public static final int TAG_SUBFILE_TYPE = 255;
    public static final int TAG_IMAGE_WIDTH = 256;
    public static final int TAG_IMAGE_LENGTH = 257;
    public static final int TAG_BITS_PER_SAMPLE = 258;
    public static final int TAG_COMPRESSION = 259;
    public static final int TAG_PHOTOMETRIC_INTERPRETATION = 262;
    public static final int TAG_THRESHHOLDING = 263;
    public static final int TAG_CELL_WIDTH = 264;
    public static final int TAG_CELL_LENGTH = 265;
    public static final int TAG_FILL_ORDER = 266;
    public static final int TAG_DOCUMENT_NAME = 269;
    public static final int TAG_IMAGE_DESCRIPTION = 270;
    public static final int TAG_MAKE = 271;
    public static final int TAG_MODEL = 272;
    public static final int TAG_STRIP_OFFSETS = 273;
    public static final int TAG_ORIENTATION = 274;
    public static final int TAG_SAMPLES_PER_PIXEL = 277;
    public static final int TAG_ROWS_PER_STRIP = 278;
    public static final int TAG_STRIP_BYTE_COUNTS = 279;
    public static final int TAG_MIN_SAMPLE_VALUE = 280;
    public static final int TAG_MAX_SAMPLE_VALUE = 281;
    public static final int TAG_X_RESOLUTION = 282;
    public static final int TAG_Y_RESOLUTION = 283;
    public static final int TAG_PLANAR_CONFIGURATION = 284;
    public static final int TAG_PAGE_NAME = 285;
    public static final int TAG_X_POSITION = 286;
    public static final int TAG_Y_POSITION = 287;
    public static final int TAG_FREE_OFFSETS = 288;
    public static final int TAG_FREE_BYTE_COUNTS = 289;
    public static final int TAG_GRAY_RESPONSE_UNIT = 290;
    public static final int TAG_GRAY_RESPONSE_CURVE = 291;
    public static final int TAG_T4_OPTIONS = 292;
    public static final int TAG_T6_OPTIONS = 293;
    public static final int TAG_RESOLUTION_UNIT = 296;
    public static final int TAG_PAGE_NUMBER = 297;
    public static final int TAG_TRANSFER_FUNCTION = 301;
    public static final int TAG_SOFTWARE = 305;
    public static final int TAG_DATETIME = 306;
    public static final int TAG_ARTIST = 315;
    public static final int TAG_HOST_COMPUTER = 316;
    public static final int TAG_PREDICTOR = 317;
    public static final int TAG_WHITE_POINT = 318;
    public static final int TAG_PRIMARY_CHROMATICITIES = 319;
    public static final int TAG_COLORMAP = 320;
    public static final int TAG_HALFTONE_HINTS = 321;
    public static final int TAG_TILE_WIDTH = 322;
    public static final int TAG_TILE_LENGTH = 323;
    public static final int TAG_TILE_OFFSETS = 324;
    public static final int TAG_TILE_BYTE_COUNTS = 325;
    public static final int TAG_INK_SET = 332;
    public static final int TAG_INK_NAMES = 333;
    public static final int TAG_NUMBER_OF_INKS = 334;
    public static final int TAG_DOT_RANGE = 336;
    public static final int TAG_TARGET_PRINTER = 337;
    public static final int TAG_EXTRA_SAMPLE = 338;
    public static final int TAG_SAMPLE_FORMAT = 339;
    public static final int TAG_SMIN_SAMPLE_VALUE = 340;
    public static final int TAG_SMAX_SAMPLE_VALUE = 341;
    public static final int TAG_TRANSFER_RANGE = 342;
    public static final int TAG_JPEG_PROC = 512;
    public static final int TAG_JPEG_INTERCHANGE_FORMAT = 513;
    public static final int TAG_JPEG_INTERCHANGE_FORMAT_LENGTH = 514;
    public static final int TAG_JPEG_RESTART_INTERVAL = 515;
    public static final int TAG_JPEG_LOSSLESS_PREDICTORS = 517;
    public static final int TAG_JPEG_POINT_TRANSFORMS = 518;
    public static final int TAG_JPEG_QTABLES = 519;
    public static final int TAG_JPEG_DCTABLES = 520;
    public static final int TAG_JPEG_ACTABLES = 521;
    public static final int TAG_YCbCr_COEFFICIENTS = 529;
    public static final int TAG_YCbCr_SUBSAMPLING = 530;
    public static final int TAG_YCbCr_POSITIONING = 531;
    public static final int TAG_REFERENCE_BLACK_WHITE = 532;
    public static final int TAG_COPYRIGHT = 33432;

    public static final int COMPRESSED_UNCOMPRESSED = 1;
    public static final int COMPRESSED_CCITT_1D = 2;
    public static final int COMPRESSED_GROUP_3_FAX = 3;
    public static final int COMPRESSED_GROUP_4_FAX = 4;
    public static final int COMPRESSED_LZW = 5;
    public static final int COMPRESSED_JPEG = 6;
    public static final int COMPRESSED_PACKBITS = 32773;

    public static final int PI_WHITE_IS_ZERO = 0;
    public static final int PI_BLACK_IS_ZERO = 1;
    public static final int PI_RGB = 2;
    public static final int PI_RGB_PALETTE = 3;
    public static final int PI_TRANSPARENCY_MASK = 4;
    public static final int PI_CMYK = 5;
    public static final int PI_YCbCr = 6;
    public static final int PI_CIELAB = 8;

    public static final NodeType MD_TIFF;
    public static final NodeCardinality MD_NEW_SUBFILE_TYPE;
    public static final NodeCardinality MD_SUBFILE_TYPE;
    public static final NodeCardinality MD_IMAGE_WIDTH;
    public static final NodeCardinality MD_IMAGE_LENGTH;
    public static final NodeCardinality MD_BITS_PER_SAMPLE;
    public static final NodeCardinality MD_COMPRESSION;
    public static final NodeCardinality MD_PHOTOMETRIC_INTERPRETATION;
    public static final NodeCardinality MD_THRESHHOLDING;
    public static final NodeCardinality MD_CELL_WIDTH;
    public static final NodeCardinality MD_CELL_LENGTH;
    public static final NodeCardinality MD_FILL_ORDER;
    public static final NodeCardinality MD_DOCUMENT_NAME;
    public static final NodeCardinality MD_IMAGE_DESCRIPTION;
    public static final NodeCardinality MD_MAKE;
    public static final NodeCardinality MD_MODEL;
    public static final NodeCardinality MD_STRIP_OFFSETS;
    public static final NodeCardinality MD_ORIENTATION;
    public static final NodeCardinality MD_SAMPLES_PER_PIXEL;
    public static final NodeCardinality MD_ROWS_PER_STRIP;
    public static final NodeCardinality MD_STRIP_BYTE_COUNTS;
    public static final NodeCardinality MD_MIN_SAMPLE_VALUE;
    public static final NodeCardinality MD_MAX_SAMPLE_VALUE;
    public static final NodeCardinality MD_X_RESOLUTION;
    public static final NodeCardinality MD_Y_RESOLUTION;
    public static final NodeCardinality MD_PLANAR_CONFIGURATION;
    public static final NodeCardinality MD_PAGE_NAME;
    public static final NodeCardinality MD_X_POSITION;
    public static final NodeCardinality MD_Y_POSITION;
    public static final NodeCardinality MD_FREE_OFFSETS;
    public static final NodeCardinality MD_FREE_BYTE_COUNTS;
    public static final NodeCardinality MD_GRAY_RESPONSE_UNIT;
    public static final NodeCardinality MD_GRAY_RESPONSE_CURVE;
    public static final NodeCardinality MD_T4_OPTIONS;
    public static final NodeCardinality MD_T6_OPTIONS;
    public static final NodeCardinality MD_RESOLUTION_UNIT;
    public static final NodeCardinality MD_PAGE_NUMBER;
    public static final NodeCardinality MD_TRANSFER_FUNCTION;
    public static final NodeCardinality MD_SOFTWARE;
    public static final NodeCardinality MD_DATETIME;
    public static final NodeCardinality MD_ARTIST;
    public static final NodeCardinality MD_HOST_COMPUTER;
    public static final NodeCardinality MD_PREDICTOR;
    public static final NodeCardinality MD_WHITE_POINT;
    public static final NodeCardinality MD_PRIMARY_CHROMATICITIES;
    public static final NodeCardinality MD_COLORMAP;
    public static final NodeCardinality MD_HALFTONE_HINTS;
    public static final NodeCardinality MD_TILE_WIDTH;
    public static final NodeCardinality MD_TILE_LENGTH;
    public static final NodeCardinality MD_TILE_OFFSETS;
    public static final NodeCardinality MD_TILE_BYTE_COUNTS;
    public static final NodeCardinality MD_INK_SET;
    public static final NodeCardinality MD_INK_NAMES;
    public static final NodeCardinality MD_NUMBER_OF_INKS;
    public static final NodeCardinality MD_DOT_RANGE;
    public static final NodeCardinality MD_TARGET_PRINTER;
    public static final NodeCardinality MD_EXTRA_SAMPLE;
    public static final NodeCardinality MD_SAMPLE_FORMAT;
    public static final NodeCardinality MD_SMIN_SAMPLE_VALUE;
    public static final NodeCardinality MD_SMAX_SAMPLE_VALUE;
    public static final NodeCardinality MD_TRANSFER_RANGE;
    public static final NodeCardinality MD_JPEG_PROC;
    public static final NodeCardinality MD_JPEG_INTERCHANGE_FORMAT;
    public static final NodeCardinality MD_JPEG_INTERCHANGE_FORMAT_LENGTH;
    public static final NodeCardinality MD_JPEG_RESTART_INTERVAL;
    public static final NodeCardinality MD_JPEG_LOSSLESS_PREDICTORS;
    public static final NodeCardinality MD_JPEG_POINT_TRANSFORMS;
    public static final NodeCardinality MD_JPEG_QTABLES;
    public static final NodeCardinality MD_JPEG_DCTABLES;
    public static final NodeCardinality MD_JPEG_ACTABLES;
    public static final NodeCardinality MD_YCbCr_COEFFICIENTS;
    public static final NodeCardinality MD_YCbCr_SUBSAMPLING;
    public static final NodeCardinality MD_YCbCr_POSITIONING;
    public static final NodeCardinality MD_REFERENCE_BLACK_WHITE;
    public static final NodeCardinality MD_COPYRIGHT;
    public static final NodeCardinality MD_UNKNOWNED;


    private static final Dictionary TAG_INDEX = new HashDictionary();

    static {

        MD_NEW_SUBFILE_TYPE = new DefaultNodeCardinality(new Chars("new_subfile_type"),null, null,Object.class, false, null);
        MD_SUBFILE_TYPE = new DefaultNodeCardinality(new Chars("subfile_type"),null, null,Object.class, false, null);
        MD_IMAGE_WIDTH = new DefaultNodeCardinality(new Chars("image_width"),null, null,Object.class, false, null);
        MD_IMAGE_LENGTH = new DefaultNodeCardinality(new Chars("image_length"),null, null,Object.class, false, null);
        MD_BITS_PER_SAMPLE = new DefaultNodeCardinality(new Chars("bits_per_sample"),null, null,Object.class, false, null);
        MD_COMPRESSION = new DefaultNodeCardinality(new Chars("compression"),null, null,Object.class, false, null);
        MD_PHOTOMETRIC_INTERPRETATION = new DefaultNodeCardinality(new Chars("photometric_interpretation"),null, null,Object.class, false, null);
        MD_THRESHHOLDING = new DefaultNodeCardinality(new Chars("threshholding"),null, null,Object.class, false, null);
        MD_CELL_WIDTH = new DefaultNodeCardinality(new Chars("cell_width"),null, null,Object.class, false, null);
        MD_CELL_LENGTH = new DefaultNodeCardinality(new Chars("cell_length"),null, null,Object.class, false, null);
        MD_FILL_ORDER = new DefaultNodeCardinality(new Chars("fill_order"),null, null,Object.class, false, null);
        MD_DOCUMENT_NAME = new DefaultNodeCardinality(new Chars("document_name"),null, null,Object.class, false, null);
        MD_IMAGE_DESCRIPTION = new DefaultNodeCardinality(new Chars("image_description"),null, null,Object.class, false, null);
        MD_MAKE = new DefaultNodeCardinality(new Chars("make"),null, null,Object.class, false, null);
        MD_MODEL = new DefaultNodeCardinality(new Chars("model"),null, null,Object.class, false, null);
        MD_STRIP_OFFSETS = new DefaultNodeCardinality(new Chars("strip_offsets"),null, null,Object.class, false, null);
        MD_ORIENTATION = new DefaultNodeCardinality(new Chars("orientation"),null, null,Object.class, false, null);
        MD_SAMPLES_PER_PIXEL = new DefaultNodeCardinality(new Chars("samples_per_pixel"),null, null,Object.class, false, null);
        MD_ROWS_PER_STRIP = new DefaultNodeCardinality(new Chars("rows_per_strip"),null, null,Object.class, false, null);
        MD_STRIP_BYTE_COUNTS = new DefaultNodeCardinality(new Chars("strip_byte_counts"),null, null,Object.class, false, null);
        MD_MIN_SAMPLE_VALUE = new DefaultNodeCardinality(new Chars("min_sample_value"),null, null,Object.class, false, null);
        MD_MAX_SAMPLE_VALUE = new DefaultNodeCardinality(new Chars("max_sample_value"),null, null,Object.class, false, null);
        MD_X_RESOLUTION = new DefaultNodeCardinality(new Chars("x_resolution"),null, null,Object.class, false, null);
        MD_Y_RESOLUTION = new DefaultNodeCardinality(new Chars("y_resolution"),null, null,Object.class, false, null);
        MD_PLANAR_CONFIGURATION = new DefaultNodeCardinality(new Chars("planar_configuration"),null, null,Object.class, false, null);
        MD_PAGE_NAME = new DefaultNodeCardinality(new Chars("page_name"),null, null,Object.class, false, null);
        MD_X_POSITION = new DefaultNodeCardinality(new Chars("x_position"),null, null,Object.class, false, null);
        MD_Y_POSITION = new DefaultNodeCardinality(new Chars("y_position"),null, null,Object.class, false, null);
        MD_FREE_OFFSETS = new DefaultNodeCardinality(new Chars("free_offsets"),null, null,Object.class, false, null);
        MD_FREE_BYTE_COUNTS = new DefaultNodeCardinality(new Chars("free_byte_counts"),null, null,Object.class, false, null);
        MD_GRAY_RESPONSE_UNIT = new DefaultNodeCardinality(new Chars("gray_response_unit"),null, null,Object.class, false, null);
        MD_GRAY_RESPONSE_CURVE = new DefaultNodeCardinality(new Chars("gray_response_curve"),null, null,Object.class, false, null);
        MD_T4_OPTIONS = new DefaultNodeCardinality(new Chars("t4_options"),null, null,Object.class, false, null);
        MD_T6_OPTIONS = new DefaultNodeCardinality(new Chars("t6_options"),null, null,Object.class, false, null);
        MD_RESOLUTION_UNIT = new DefaultNodeCardinality(new Chars("resolution_unit"),null, null,Object.class, false, null);
        MD_PAGE_NUMBER = new DefaultNodeCardinality(new Chars("page_number"),null, null,Object.class, false, null);
        MD_TRANSFER_FUNCTION = new DefaultNodeCardinality(new Chars("transfer_function"),null, null,Object.class, false, null);
        MD_SOFTWARE = new DefaultNodeCardinality(new Chars("software"),null, null,Object.class, false, null);
        MD_DATETIME = new DefaultNodeCardinality(new Chars("datetime"),null, null,Object.class, false, null);
        MD_ARTIST = new DefaultNodeCardinality(new Chars("artist"),null, null,Object.class, false, null);
        MD_HOST_COMPUTER = new DefaultNodeCardinality(new Chars("host_computer"),null, null,Object.class, false, null);
        MD_PREDICTOR = new DefaultNodeCardinality(new Chars("predictor"),null, null,Object.class, false, null);
        MD_WHITE_POINT = new DefaultNodeCardinality(new Chars("white_point"),null, null,Object.class, false, null);
        MD_PRIMARY_CHROMATICITIES = new DefaultNodeCardinality(new Chars("primary_chromaticities"),null, null,Object.class, false, null);
        MD_COLORMAP = new DefaultNodeCardinality(new Chars("colormap"),null, null,Object.class, false, null);
        MD_HALFTONE_HINTS = new DefaultNodeCardinality(new Chars("halftone_hints"),null, null,Object.class, false, null);
        MD_TILE_WIDTH = new DefaultNodeCardinality(new Chars("tile_width"),null, null,Object.class, false, null);
        MD_TILE_LENGTH = new DefaultNodeCardinality(new Chars("tile_length"),null, null,Object.class, false, null);
        MD_TILE_OFFSETS = new DefaultNodeCardinality(new Chars("tile_offsets"),null, null,Object.class, false, null);
        MD_TILE_BYTE_COUNTS = new DefaultNodeCardinality(new Chars("tile_byte_counts"),null, null,Object.class, false, null);
        MD_INK_SET = new DefaultNodeCardinality(new Chars("ink_set"),null, null,Object.class, false, null);
        MD_INK_NAMES = new DefaultNodeCardinality(new Chars("ink_names"),null, null,Object.class, false, null);
        MD_NUMBER_OF_INKS = new DefaultNodeCardinality(new Chars("number_of_inks"),null, null,Object.class, false, null);
        MD_DOT_RANGE = new DefaultNodeCardinality(new Chars("dot_range"),null, null,Object.class, false, null);
        MD_TARGET_PRINTER = new DefaultNodeCardinality(new Chars("target_printer"),null, null,Object.class, false, null);
        MD_EXTRA_SAMPLE = new DefaultNodeCardinality(new Chars("extra_sample"),null, null,Object.class, false, null);
        MD_SAMPLE_FORMAT = new DefaultNodeCardinality(new Chars("sample_format"),null, null,Object.class, false, null);
        MD_SMIN_SAMPLE_VALUE = new DefaultNodeCardinality(new Chars("smin_sample_value"),null, null,Object.class, false, null);
        MD_SMAX_SAMPLE_VALUE = new DefaultNodeCardinality(new Chars("smax_sample_value"),null, null,Object.class, false, null);
        MD_TRANSFER_RANGE = new DefaultNodeCardinality(new Chars("transfer_range"),null, null,Object.class, false, null);
        MD_JPEG_PROC = new DefaultNodeCardinality(new Chars("jpeg_proc"),null, null,Object.class, false, null);
        MD_JPEG_INTERCHANGE_FORMAT = new DefaultNodeCardinality(new Chars("jpeg_interchange_format"),null, null,Object.class, false, null);
        MD_JPEG_INTERCHANGE_FORMAT_LENGTH = new DefaultNodeCardinality(new Chars("jpeg_interchange_format_length"),null, null,Object.class, false, null);
        MD_JPEG_RESTART_INTERVAL = new DefaultNodeCardinality(new Chars("jpeg_restart_interval"),null, null,Object.class, false, null);
        MD_JPEG_LOSSLESS_PREDICTORS = new DefaultNodeCardinality(new Chars("jpeg_lossless_predictors"),null, null,Object.class, false, null);
        MD_JPEG_POINT_TRANSFORMS = new DefaultNodeCardinality(new Chars("jpeg_point_transforms"),null, null,Object.class, false, null);
        MD_JPEG_QTABLES = new DefaultNodeCardinality(new Chars("jpeg_qtables"),null, null,Object.class, false, null);
        MD_JPEG_DCTABLES = new DefaultNodeCardinality(new Chars("jpeg_dctables"),null, null,Object.class, false, null);
        MD_JPEG_ACTABLES = new DefaultNodeCardinality(new Chars("jpeg_actables"),null, null,Object.class, false, null);
        MD_YCbCr_COEFFICIENTS = new DefaultNodeCardinality(new Chars("ycbcr_coefficients"),null, null,Object.class, false, null);
        MD_YCbCr_SUBSAMPLING = new DefaultNodeCardinality(new Chars("ycbcr_subsampling"),null, null,Object.class, false, null);
        MD_YCbCr_POSITIONING = new DefaultNodeCardinality(new Chars("ycbcr_positioning"),null, null,Object.class, false, null);
        MD_REFERENCE_BLACK_WHITE = new DefaultNodeCardinality(new Chars("reference_black_white"),null, null,Object.class, false, null);
        MD_COPYRIGHT = new DefaultNodeCardinality(new Chars("copyright"),null, null,Object.class, false, null);
        MD_UNKNOWNED = new DefaultNodeCardinality(new Chars("unknowned"),null, null,Object.class, false, null);

        TAG_INDEX.add(TAG_NEW_SUBFILE_TYPE,MD_NEW_SUBFILE_TYPE);
        TAG_INDEX.add(TAG_SUBFILE_TYPE,MD_SUBFILE_TYPE);
        TAG_INDEX.add(TAG_IMAGE_WIDTH,MD_IMAGE_WIDTH);
        TAG_INDEX.add(TAG_IMAGE_LENGTH,MD_IMAGE_LENGTH);
        TAG_INDEX.add(TAG_BITS_PER_SAMPLE,MD_BITS_PER_SAMPLE);
        TAG_INDEX.add(TAG_COMPRESSION,MD_COMPRESSION);
        TAG_INDEX.add(TAG_PHOTOMETRIC_INTERPRETATION,MD_PHOTOMETRIC_INTERPRETATION);
        TAG_INDEX.add(TAG_THRESHHOLDING,MD_THRESHHOLDING);
        TAG_INDEX.add(TAG_CELL_WIDTH,MD_CELL_WIDTH);
        TAG_INDEX.add(TAG_CELL_LENGTH,MD_CELL_LENGTH);
        TAG_INDEX.add(TAG_FILL_ORDER,MD_FILL_ORDER);
        TAG_INDEX.add(TAG_DOCUMENT_NAME,MD_DOCUMENT_NAME);
        TAG_INDEX.add(TAG_IMAGE_DESCRIPTION,MD_IMAGE_DESCRIPTION);
        TAG_INDEX.add(TAG_MAKE,MD_MAKE);
        TAG_INDEX.add(TAG_MODEL,MD_MODEL);
        TAG_INDEX.add(TAG_STRIP_OFFSETS,MD_STRIP_OFFSETS);
        TAG_INDEX.add(TAG_ORIENTATION,MD_ORIENTATION);
        TAG_INDEX.add(TAG_SAMPLES_PER_PIXEL,MD_SAMPLES_PER_PIXEL);
        TAG_INDEX.add(TAG_ROWS_PER_STRIP,MD_ROWS_PER_STRIP);
        TAG_INDEX.add(TAG_STRIP_BYTE_COUNTS,MD_STRIP_BYTE_COUNTS);
        TAG_INDEX.add(TAG_MIN_SAMPLE_VALUE,MD_MIN_SAMPLE_VALUE);
        TAG_INDEX.add(TAG_MAX_SAMPLE_VALUE,MD_MAX_SAMPLE_VALUE);
        TAG_INDEX.add(TAG_X_RESOLUTION,MD_X_RESOLUTION);
        TAG_INDEX.add(TAG_Y_RESOLUTION,MD_Y_RESOLUTION);
        TAG_INDEX.add(TAG_PLANAR_CONFIGURATION,MD_PLANAR_CONFIGURATION);
        TAG_INDEX.add(TAG_PAGE_NAME,MD_PAGE_NAME);
        TAG_INDEX.add(TAG_X_POSITION,MD_X_POSITION);
        TAG_INDEX.add(TAG_Y_POSITION,MD_Y_POSITION);
        TAG_INDEX.add(TAG_FREE_OFFSETS,MD_FREE_OFFSETS);
        TAG_INDEX.add(TAG_FREE_BYTE_COUNTS,MD_FREE_BYTE_COUNTS);
        TAG_INDEX.add(TAG_GRAY_RESPONSE_UNIT,MD_GRAY_RESPONSE_UNIT);
        TAG_INDEX.add(TAG_GRAY_RESPONSE_CURVE,MD_GRAY_RESPONSE_CURVE);
        TAG_INDEX.add(TAG_T4_OPTIONS,MD_T4_OPTIONS);
        TAG_INDEX.add(TAG_T6_OPTIONS,MD_T6_OPTIONS);
        TAG_INDEX.add(TAG_RESOLUTION_UNIT,MD_RESOLUTION_UNIT);
        TAG_INDEX.add(TAG_PAGE_NUMBER,MD_PAGE_NUMBER);
        TAG_INDEX.add(TAG_TRANSFER_FUNCTION,MD_TRANSFER_FUNCTION);
        TAG_INDEX.add(TAG_SOFTWARE,MD_SOFTWARE);
        TAG_INDEX.add(TAG_DATETIME,MD_DATETIME);
        TAG_INDEX.add(TAG_ARTIST,MD_ARTIST);
        TAG_INDEX.add(TAG_HOST_COMPUTER,MD_HOST_COMPUTER);
        TAG_INDEX.add(TAG_PREDICTOR,MD_PREDICTOR);
        TAG_INDEX.add(TAG_WHITE_POINT,MD_WHITE_POINT);
        TAG_INDEX.add(TAG_PRIMARY_CHROMATICITIES,MD_PRIMARY_CHROMATICITIES);
        TAG_INDEX.add(TAG_COLORMAP,MD_COLORMAP);
        TAG_INDEX.add(TAG_HALFTONE_HINTS,MD_HALFTONE_HINTS);
        TAG_INDEX.add(TAG_TILE_WIDTH,MD_TILE_WIDTH);
        TAG_INDEX.add(TAG_TILE_LENGTH,MD_TILE_LENGTH);
        TAG_INDEX.add(TAG_TILE_OFFSETS,MD_TILE_OFFSETS);
        TAG_INDEX.add(TAG_TILE_BYTE_COUNTS,MD_TILE_BYTE_COUNTS);
        TAG_INDEX.add(TAG_INK_SET,MD_INK_SET);
        TAG_INDEX.add(TAG_INK_NAMES,MD_INK_NAMES);
        TAG_INDEX.add(TAG_NUMBER_OF_INKS,MD_NUMBER_OF_INKS);
        TAG_INDEX.add(TAG_DOT_RANGE,MD_DOT_RANGE);
        TAG_INDEX.add(TAG_TARGET_PRINTER,MD_TARGET_PRINTER);
        TAG_INDEX.add(TAG_EXTRA_SAMPLE,MD_EXTRA_SAMPLE);
        TAG_INDEX.add(TAG_SAMPLE_FORMAT,MD_SAMPLE_FORMAT);
        TAG_INDEX.add(TAG_SMIN_SAMPLE_VALUE,MD_SMIN_SAMPLE_VALUE);
        TAG_INDEX.add(TAG_SMAX_SAMPLE_VALUE,MD_SMAX_SAMPLE_VALUE);
        TAG_INDEX.add(TAG_TRANSFER_RANGE,MD_TRANSFER_RANGE);
        TAG_INDEX.add(TAG_JPEG_PROC,MD_JPEG_PROC);
        TAG_INDEX.add(TAG_JPEG_INTERCHANGE_FORMAT,MD_JPEG_INTERCHANGE_FORMAT);
        TAG_INDEX.add(TAG_JPEG_INTERCHANGE_FORMAT_LENGTH,MD_JPEG_INTERCHANGE_FORMAT_LENGTH);
        TAG_INDEX.add(TAG_JPEG_RESTART_INTERVAL,MD_JPEG_RESTART_INTERVAL);
        TAG_INDEX.add(TAG_JPEG_LOSSLESS_PREDICTORS,MD_JPEG_LOSSLESS_PREDICTORS);
        TAG_INDEX.add(TAG_JPEG_POINT_TRANSFORMS,MD_JPEG_POINT_TRANSFORMS);
        TAG_INDEX.add(TAG_JPEG_QTABLES,MD_JPEG_QTABLES);
        TAG_INDEX.add(TAG_JPEG_DCTABLES,MD_JPEG_DCTABLES);
        TAG_INDEX.add(TAG_JPEG_ACTABLES,MD_JPEG_ACTABLES);
        TAG_INDEX.add(TAG_YCbCr_COEFFICIENTS,MD_YCbCr_COEFFICIENTS);
        TAG_INDEX.add(TAG_YCbCr_SUBSAMPLING,MD_YCbCr_SUBSAMPLING);
        TAG_INDEX.add(TAG_YCbCr_POSITIONING,MD_YCbCr_POSITIONING);
        TAG_INDEX.add(TAG_REFERENCE_BLACK_WHITE,MD_REFERENCE_BLACK_WHITE);
        TAG_INDEX.add(TAG_COPYRIGHT,MD_COPYRIGHT);

        MD_TIFF = new DefaultNodeType(new Chars("tiff"),null, null,Sequence.class, null, false, new NodeCardinality[]{
            MD_NEW_SUBFILE_TYPE,
            MD_SUBFILE_TYPE,
            MD_IMAGE_WIDTH,
            MD_IMAGE_LENGTH,
            MD_BITS_PER_SAMPLE,
            MD_COMPRESSION,
            MD_PHOTOMETRIC_INTERPRETATION,
            MD_THRESHHOLDING,
            MD_CELL_WIDTH,
            MD_CELL_LENGTH,
            MD_FILL_ORDER,
            MD_DOCUMENT_NAME,
            MD_IMAGE_DESCRIPTION,
            MD_MAKE,
            MD_MODEL,
            MD_STRIP_OFFSETS,
            MD_ORIENTATION,
            MD_SAMPLES_PER_PIXEL,
            MD_ROWS_PER_STRIP,
            MD_STRIP_BYTE_COUNTS,
            MD_MIN_SAMPLE_VALUE,
            MD_MAX_SAMPLE_VALUE,
            MD_X_RESOLUTION,
            MD_Y_RESOLUTION,
            MD_PLANAR_CONFIGURATION,
            MD_PAGE_NAME,
            MD_X_POSITION,
            MD_Y_POSITION,
            MD_FREE_OFFSETS,
            MD_FREE_BYTE_COUNTS,
            MD_GRAY_RESPONSE_UNIT,
            MD_GRAY_RESPONSE_CURVE,
            MD_T4_OPTIONS,
            MD_T6_OPTIONS,
            MD_RESOLUTION_UNIT,
            MD_PAGE_NUMBER,
            MD_TRANSFER_FUNCTION,
            MD_SOFTWARE,
            MD_DATETIME,
            MD_ARTIST,
            MD_HOST_COMPUTER,
            MD_PREDICTOR,
            MD_WHITE_POINT,
            MD_PRIMARY_CHROMATICITIES,
            MD_COLORMAP,
            MD_HALFTONE_HINTS,
            MD_TILE_WIDTH,
            MD_TILE_LENGTH,
            MD_TILE_OFFSETS,
            MD_TILE_BYTE_COUNTS,
            MD_INK_SET,
            MD_INK_NAMES,
            MD_NUMBER_OF_INKS,
            MD_DOT_RANGE,
            MD_TARGET_PRINTER,
            MD_EXTRA_SAMPLE,
            MD_SAMPLE_FORMAT,
            MD_SMIN_SAMPLE_VALUE,
            MD_SMAX_SAMPLE_VALUE,
            MD_TRANSFER_RANGE,
            MD_JPEG_PROC,
            MD_JPEG_INTERCHANGE_FORMAT,
            MD_JPEG_INTERCHANGE_FORMAT_LENGTH,
            MD_JPEG_RESTART_INTERVAL,
            MD_JPEG_LOSSLESS_PREDICTORS,
            MD_JPEG_POINT_TRANSFORMS,
            MD_JPEG_QTABLES,
            MD_JPEG_DCTABLES,
            MD_JPEG_ACTABLES,
            MD_YCbCr_COEFFICIENTS,
            MD_YCbCr_SUBSAMPLING,
            MD_YCbCr_POSITIONING,
            MD_REFERENCE_BLACK_WHITE,
            MD_COPYRIGHT
        });
    }

    private TIFFMetaModel(){}

    public static NodeCardinality findType(int tag){
        final Object col = TAG_INDEX.getValue(tag);
        return (NodeCardinality) col;
    }

}
