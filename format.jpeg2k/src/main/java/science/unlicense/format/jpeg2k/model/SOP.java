
package science.unlicense.format.jpeg2k.model;

import science.unlicense.format.jpeg2k.JPEG2KConstants;

/**
 * Start of packet.
 *
 * Marks the beginning of a partition and the index of that partition within a codestream.
 *
 * @author Johann Sorel
 */
public class SOP extends Marker {

    public SOP() {
        super(JPEG2KConstants.MK_SOP);
    }

}
