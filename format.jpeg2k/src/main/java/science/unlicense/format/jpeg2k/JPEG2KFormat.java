
package science.unlicense.format.jpeg2k;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * JPEG2000 specification :
 * only drafts are public, not final spec,
 * must pay ISO for the full spec, I'm not that rich :D
 *
 * http://www.jpeg.org/public/fcd15444-1.pdf
 * http://www.jpeg.org/public/fcd15444-2.pdf
 * http://www.jpeg.org/public/fcd15444-3.pdf
 * http://www.jpeg.org/public/fcd15444-4.pdf
 * http://www.jpeg.org/public/fcd15444-6.pdf
 * http://www.jpeg.org/public/fcd15444-8.pdf
 * http://www.jpeg.org/public/fcd15444-10.pdf
 * http://www.jpeg.org/public/fcd15444-11.pdf
 * http://www.jpeg.org/public/15444-1annexi.pdf
 *
 *
 * @author Johann Sorel
 */
public class JPEG2KFormat extends AbstractImageFormat {

    public static final JPEG2KFormat INSTANCE = new JPEG2KFormat();

    private JPEG2KFormat() {
        super(new Chars("jpeg2k"));
        shortName = new Chars("JPEG-2000");
        longName = new Chars("Joint Photographic Experts Group - 2000");
        mimeTypes.add(new Chars("image/jp2"));
        mimeTypes.add(new Chars("image/jpx"));
        mimeTypes.add(new Chars("image/jpm"));
        mimeTypes.add(new Chars("video/mj2"));
        extensions.add(new Chars("dcm"));
        extensions.add(new Chars("jp2"));
        extensions.add(new Chars("j2k"));
        extensions.add(new Chars("jpf"));
        extensions.add(new Chars("jpx"));
        extensions.add(new Chars("jpm"));
        extensions.add(new Chars("mj2"));
        signatures.add(JPEG2KConstants.SIGNATURE);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new JPEG2KStore(this, source);
    }

}
