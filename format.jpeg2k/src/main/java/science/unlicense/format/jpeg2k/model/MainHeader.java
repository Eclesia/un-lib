
package science.unlicense.format.jpeg2k.model;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class MainHeader {

    public final Sequence markers = new ArraySequence();

}
