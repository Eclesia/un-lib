

package science.unlicense.format.jpeg2k;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.format.jpeg2k.model.Marker;
import science.unlicense.format.jpeg2k.model.SIZ;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;

/**
 *
 * @author Johann Sorel
 */
public class JPEG2KReader extends AbstractImageReader {


    private final Sequence markers = new ArraySequence();
    private SIZ imageSize;

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {

        final DataInputStream ds = new DataInputStream(stream,Endianness.BIG_ENDIAN);

        if (!Arrays.equals(JPEG2KConstants.SIGNATURE,ds.readFully(new byte[12]))){
            throw new IOException(ds, "Wrong signature, stream is not a valid jpeg 2000 image.");
        }

        stream.mark();

        readMarkers(stream);


        return null;
    }

    private void readMarkers(final BacktrackInputStream stream) throws IOException{

        final DataInputStream ds = new DataInputStream(stream,Endianness.BIG_ENDIAN);

        for (;;){
            final int b1 = ds.read();
            if (b1==-1) break;
            final int b2 = ds.read();
            final int markerId = b1 << 8 | b2;

            final Marker marker = JPEG2KUtilities.createMarker(markerId);
            if (marker instanceof SIZ){
                imageSize = (SIZ) marker;
            }
            marker.read(ds,imageSize);
            System.out.println(marker.getClass());

        }

    }

}
