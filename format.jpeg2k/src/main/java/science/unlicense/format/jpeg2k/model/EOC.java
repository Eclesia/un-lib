
package science.unlicense.format.jpeg2k.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.jpeg2k.JPEG2KConstants;

/**
 * End of codestream.
 *
 * Indicates the end of the codestream.
 *
 * @author Johann Sorel
 */
public class EOC extends Marker {

    public EOC() {
        super(JPEG2KConstants.MK_EOC);
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException {
        parametersLength = 0;
    }

}
