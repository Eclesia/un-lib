
package science.unlicense.format.jpeg2k.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.jpeg2k.JPEG2KConstants;

/**
 * Image and tile size.
 *
 * Provides information about the uncompressed image such as the width and height
 * of the reference grid, the width and height of the tiles, the number of components,
 * component bit depth, and the separation of component samples with respect to
 * the reference grid.
 *
 * @author Johann Sorel
 */
public class SIZ extends Marker {

    public int codeStreamCapabilities;
    public int gridWidth;
    public int gridHeight;
    public int gridOffsetX;
    public int gridOffsetY;
    public int tileWidth;
    public int tileHeight;
    public int tileOffsetX;
    public int tileOffsetY;
    public int nbComponent;
    public Component[] components;

    public SIZ() {
        super(JPEG2KConstants.MK_SIZ);
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException{
        parametersLength = ds.readUShort();
        codeStreamCapabilities = ds.readUShort();
        gridWidth   = ds.readInt();
        gridHeight  = ds.readInt();
        gridOffsetX = ds.readInt();
        gridOffsetY = ds.readInt();
        tileWidth   = ds.readInt();
        tileHeight  = ds.readInt();
        tileOffsetX = ds.readInt();
        tileOffsetY = ds.readInt();
        nbComponent = ds.readUShort();
        components = new Component[nbComponent];
        for (int i=0;i<nbComponent;i++){
            components[i] = new Component();
            components[i].precision = ds.readUByte();
            components[i].separationX = ds.readUByte();
            components[i].separationY = ds.readUByte();
        }
    }

    public static class Component{
        public int precision;
        public int separationX;
        public int separationY;
    }

}
