
package science.unlicense.format.jpeg2k.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.jpeg2k.JPEG2KConstants;

/**
 * Start of data.
 *
 * Indicates the beginning of bit stream data for the current tile-part.
 * The SOD also indicates the end of a tile-part header.
 *
 * @author Johann Sorel
 */
public class SOD extends Marker {

    public SOD() {
        super(JPEG2KConstants.MK_SOD);
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException {
        parametersLength = 0;
    }

}
