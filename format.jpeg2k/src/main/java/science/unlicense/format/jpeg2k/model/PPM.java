
package science.unlicense.format.jpeg2k.model;

import science.unlicense.format.jpeg2k.JPEG2KConstants;

/**
 * Packed packet headers, main header.
 *
 * A collection of the packet headers so multiple reads are not required to decode headers.
 *
 * @author Johann Sorel
 */
public class PPM extends Marker {

    public PPM() {
        super(JPEG2KConstants.MK_PPM);
    }

}
