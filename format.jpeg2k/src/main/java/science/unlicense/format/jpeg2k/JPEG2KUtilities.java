
package science.unlicense.format.jpeg2k;

import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.format.jpeg2k.model.CME;
import science.unlicense.format.jpeg2k.model.COC;
import science.unlicense.format.jpeg2k.model.COD;
import science.unlicense.format.jpeg2k.model.EOC;
import science.unlicense.format.jpeg2k.model.EPH;
import science.unlicense.format.jpeg2k.model.Marker;
import science.unlicense.format.jpeg2k.model.PLM;
import science.unlicense.format.jpeg2k.model.PLT;
import science.unlicense.format.jpeg2k.model.POD;
import science.unlicense.format.jpeg2k.model.PPM;
import science.unlicense.format.jpeg2k.model.PPT;
import science.unlicense.format.jpeg2k.model.QCC;
import science.unlicense.format.jpeg2k.model.QCD;
import science.unlicense.format.jpeg2k.model.RGN;
import science.unlicense.format.jpeg2k.model.SIZ;
import science.unlicense.format.jpeg2k.model.SOC;
import science.unlicense.format.jpeg2k.model.SOD;
import science.unlicense.format.jpeg2k.model.SOP;
import science.unlicense.format.jpeg2k.model.SOT;
import science.unlicense.format.jpeg2k.model.TLM;

/**
 *
 * @author Johann Sorel
 */
public final class JPEG2KUtilities {


    private static final Dictionary MKS = new HashDictionary();
    static {
        MKS.add(JPEG2KConstants.MK_CME, CME.class);
        MKS.add(JPEG2KConstants.MK_COC, COC.class);
        MKS.add(JPEG2KConstants.MK_COD, COD.class);
        MKS.add(JPEG2KConstants.MK_EOC, EOC.class);
        MKS.add(JPEG2KConstants.MK_EPH, EPH.class);
        MKS.add(JPEG2KConstants.MK_PLM, PLM.class);
        MKS.add(JPEG2KConstants.MK_PLT, PLT.class);
        MKS.add(JPEG2KConstants.MK_POD, POD.class);
        MKS.add(JPEG2KConstants.MK_PPM, PPM.class);
        MKS.add(JPEG2KConstants.MK_PPT, PPT.class);
        MKS.add(JPEG2KConstants.MK_QCC, QCC.class);
        MKS.add(JPEG2KConstants.MK_QCD, QCD.class);
        MKS.add(JPEG2KConstants.MK_RGN, RGN.class);
        MKS.add(JPEG2KConstants.MK_SIZ, SIZ.class);
        MKS.add(JPEG2KConstants.MK_SOC, SOC.class);
        MKS.add(JPEG2KConstants.MK_SOD, SOD.class);
        MKS.add(JPEG2KConstants.MK_SOP, SOP.class);
        MKS.add(JPEG2KConstants.MK_SOT, SOT.class);
        MKS.add(JPEG2KConstants.MK_TLM, TLM.class);
    }

    private JPEG2KUtilities(){}

    /**
     * Indicate if this marker is zero length.
     *
     * @param code
     * @return
     */
    public static boolean isZeroLengthMarker(int code){
        return code >= 0xFF30 && code <= 0xFF3F;
    }

    public static Marker createMarker(int code){
        final Class clazz = (Class) MKS.getValue(code);
        if (clazz == null){
            //unknowned marker, can be a JPEG-2000 extension
            return new Marker(code);
        } else {
            try {
                return (Marker) clazz.newInstance();
            } catch (InstantiationException ex) {
                throw new RuntimeException(ex);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

}
