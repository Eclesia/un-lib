
package science.unlicense.format.jpeg2k.model;

import science.unlicense.format.jpeg2k.JPEG2KConstants;

/**
 * Comment and extension.
 *
 * Allows unstructured data in the header.
 *
 * @author Johann Sorel
 */
public class CME extends Marker {

    public CME() {
        super(JPEG2KConstants.MK_CME);
    }

}
