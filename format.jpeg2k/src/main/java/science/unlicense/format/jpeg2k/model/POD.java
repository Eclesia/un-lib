
package science.unlicense.format.jpeg2k.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.jpeg2k.JPEG2KConstants;

/**
 * Progression order change, default.
 *
 * Describes the bounds and progression order for any progression order other
 * than default in the codestream.
 *
 * @author Johann Sorel
 */
public class POD extends Marker {

    public int nbProgressiveChange;
    public ProgressiveChange[] changes;

    public POD() {
        super(JPEG2KConstants.MK_POD);
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException{
        parametersLength = ds.readUShort();

        if (siz.nbComponent<257){
            nbProgressiveChange = (parametersLength-2) / 7;
        } else {
            nbProgressiveChange = (parametersLength-2) / 9;
        }

        changes = new ProgressiveChange[nbProgressiveChange];
        for (int i=0;i<nbProgressiveChange;i++){
            changes[i] = new ProgressiveChange();
            changes[i].resolutionStart = ds.readUByte();
            changes[i].componentStart = (siz.nbComponent<257) ? ds.readUByte() : ds.readUShort();
            changes[i].layer = ds.readUShort();
            changes[i].resolutionEnd = ds.readUByte();
            changes[i].componentEnd = (siz.nbComponent<257) ? ds.readUByte() : ds.readUShort();
            changes[i].order = ds.readUByte();
        }

    }

    public static class ProgressiveChange{
        public int resolutionStart;
        public int componentStart;
        public int layer;
        public int resolutionEnd;
        public int componentEnd;
        public int order;
    }

}
