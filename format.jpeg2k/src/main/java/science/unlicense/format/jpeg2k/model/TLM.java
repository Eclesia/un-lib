
package science.unlicense.format.jpeg2k.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.jpeg2k.JPEG2KConstants;

/**
 * Tile-part lengths, main header.
 *
 * Describes the length of every tile-part in the codestream. Each tile-part’s
 * length is measured from the first byte of the SOT marker segment to the end
 * of the data of that tile-part. The value of each individual tile-part length
 * in the TLM marker segment is the same as the value in the corresponding Psot
 * in the SOT marker segment.
 *
 * @author Johann Sorel
 */
public class TLM extends Marker {

    public int markerIndex;
    public int paramSize;

    public TLM() {
        super(JPEG2KConstants.MK_TLM);
    }

    // TODO how do we calculate ST and SP for the size ????
    // where are those defined ? spec is not precise, we have to read it again ...
//    public void read(DataInputStream ds, SIZ siz) throws IOException{
//        parametersLength = ds.readUnsignedShort();
//        markerIndex = ds.readUnsignedByte();
//        paramSize = ds.readUnsignedByte();
//
//        final int nbPart;
//        if (paramSize==0){
//
//        } else if (paramSize==1){
//
//        }if (paramSize==2){
//
//        }
//
//    }

    public static class TilePart{
        public int number;
        public int length;
    }

}
