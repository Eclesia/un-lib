
package science.unlicense.format.jpeg2k.model;

import science.unlicense.format.jpeg2k.JPEG2KConstants;

/**
 * Packets length, tile-part header.
 *
 * A list of packet lengths in the tile-part.
 *
 * @author Johann Sorel
 */
public class PLT extends Marker {

    public PLT() {
        super(JPEG2KConstants.MK_PLT);
    }

}
