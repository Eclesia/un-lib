
package science.unlicense.code.cql;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.predicate.Constant;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.common.api.Assert.*;

/**
 *
 * @author Johann Sorel
 */
public class CQLReaderTest {

    private static final double DELTA = 0.00001;

    @Test
    public void testName() throws IOException {
        Expression exp = new CQLReader().read("name1");
        assertTrue(exp instanceof PropertyName);
        PropertyName cdt = (PropertyName) exp;
        assertEquals(new Chars("name1"),cdt.getName());
    }

    @Test
    public void testEscapedName() throws IOException {
        Expression exp = new CQLReader().read("\"name2\"");
        assertTrue(exp instanceof PropertyName);
        PropertyName cdt = (PropertyName) exp;
        assertEquals(new Chars("name2"),cdt.getName());
    }

    @Test
    public void testLiteralInteger() throws IOException {
        Expression exp = new CQLReader().read("123456");
        assertTrue(exp instanceof Constant);
        Constant cdt = (Constant) exp;
        assertEquals(123456,(Double) cdt.getValue(),DELTA);
    }

    @Test
    public void testLiteralNegativeInteger() throws IOException {
        Expression exp = new CQLReader().read("-123456");
        assertTrue(exp instanceof Constant);
        Constant cdt = (Constant) exp;
        assertEquals(-123456,(Double) cdt.getValue(),DELTA);
    }

    @Test
    public void testLiteralFloat() throws IOException {
        Expression exp = new CQLReader().read("123.456");
        assertTrue(exp instanceof Constant);
        Constant cdt = (Constant) exp;
        assertEquals(123.456,(Double) cdt.getValue(),DELTA);
    }

    @Test
    public void testLiteralExponent() throws IOException {
        Expression exp = new CQLReader().read("-123.456e-5");
        assertTrue(exp instanceof Constant);
        Constant cdt = (Constant) exp;
        assertEquals(-123.456e-5,(Double) cdt.getValue(),DELTA);
    }

    @Test
    public void testLiteralText() throws IOException {
        Expression exp = new CQLReader().read("'Unlicense \\'Rocks\\'!'");
        assertTrue(exp instanceof Constant);
        Constant cdt = (Constant) exp;
        assertEquals(new Chars("Unlicense 'Rocks'!"),cdt.getValue());
    }
}
