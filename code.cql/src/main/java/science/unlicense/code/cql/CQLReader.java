
package science.unlicense.code.cql;

import static science.unlicense.code.cql.CQLConstants.*;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.predicate.Constant;
import science.unlicense.common.api.predicate.Expression;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.parser.Parser;
import science.unlicense.syntax.api.parser.Rule;
import science.unlicense.syntax.api.parser.SyntaxNode;

/**
 *
 * @author Johann Sorel
 */
public class CQLReader extends AbstractReader {

    private final Parser greader = new Parser(RULE_FILE);

    @Override
    public void setInput(Object input) throws IOException {
        super.setInput(input);
        greader.setInput(input);
    }

    public Expression read(String cql) throws IOException {
        setInput(new ArrayInputStream(cql.getBytes()));
        return read();
    }

    public Expression read() throws IOException {
        SyntaxNode cf = greader.parse();
        cf.trim(TRIM);
        System.out.println(cf);

        Rule rule = cf.getRule();
        if (rule == RULE_FILE) {
            return parseFile(cf);
        } else {
            throw new IOException(greader, "Unexpected rule "+rule);
        }

    }

    private Expression parseFile(SyntaxNode sn) throws IOException {
        SyntaxNode childNode = (SyntaxNode) sn.getChildren().get(0);
        Rule rule = childNode.getRule();
        if (rule == RULE_PROPERTYNAME) {
            return parsePropertyName(childNode);
        } else if (rule == RULE_LITERAL) {
            return parseLiteral(childNode);
        } else {
            throw new IOException(greader, "Unexpected rule "+rule);
        }
    }

    private Expression parsePropertyName(SyntaxNode sn) throws IOException {
        final SyntaxNode childNode = (SyntaxNode) sn.getChildren().get(0);
        final Token token = childNode.getToken();
        if (token.type == TOKEN_NAME) {
            return new PropertyName(token.value);
        } else if (token.type == TOKEN_ESCNAME) {
            return new PropertyName(token.value.truncate(1, token.value.getCharLength()-1));
        } else {
            throw new IOException(greader, "Unexpected token "+token);
        }
    }

    private Expression parseLiteral(SyntaxNode sn) throws IOException {
        final SyntaxNode childNode = (SyntaxNode) sn.getChildren().get(0);
        final Rule rule = childNode.getRule();
        final Token token = childNode.getToken();

        if (rule == RULE_NUMBER) {
            final SyntaxNode signNode = childNode.getChildByToken(TOKEN_SIGN);
            final SyntaxNode numberNode = childNode.getChildByToken(TOKEN_NUMBER);

            double value = Float64.decode(numberNode.getToken().value);
            if (signNode != null) {
                if (new Chars("-").equals(signNode.getToken().value)) {
                    value = -value;
                }
            }

            return new Constant(value);
        } else if (token.type == TOKEN_TEXT) {
            final Chars text = token.value.truncate(1, token.value.getCharLength()-1);
            return new Constant(text.replaceAll(new Chars("\\'"), new Chars("'")));
        }


        throw new IOException(greader, "Unexpected literal "+sn);
    }
}
