
package science.unlicense.code.cql;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.predicate.Expression;

/**
 *
 * @author Johann Sorel
 */
public class PropertyName implements Expression {

    private final Chars name;

    public PropertyName(Chars name) {
        this.name = name;
    }

    public Chars getName() {
        return name;
    }

    @Override
    public Object evaluate(Object candidate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
