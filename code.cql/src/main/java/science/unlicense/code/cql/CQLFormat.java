
package science.unlicense.code.cql;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public class CQLFormat extends DefaultFormat {

    public static final CQLFormat INSTANCE = new CQLFormat();

    private CQLFormat() {
        super(new Chars("cql"));
        shortName = new Chars("CQL");
        longName = new Chars("Common Query Language file");
        extensions.add(new Chars("cql"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
