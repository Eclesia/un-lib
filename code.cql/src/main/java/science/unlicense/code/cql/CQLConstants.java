
package science.unlicense.code.cql;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.OrderedHashDictionary;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.lexer.Token;
import science.unlicense.syntax.api.lexer.TokenGroup;
import science.unlicense.syntax.api.lexer.TokenType;
import science.unlicense.syntax.api.parser.Rule;
import science.unlicense.syntax.api.parser.SyntaxNode;
import science.unlicense.syntax.impl.grammar.io.UNGrammarReader;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public final class CQLConstants {

    public static final TokenType
            TOKEN_NAME,
            TOKEN_ESCNAME,
            TOKEN_TEXT,
            TOKEN_NUMBER,
            TOKEN_SIGN,
            TOKEN_WS;

    public static final Rule
            RULE_FILE,
            RULE_PROPERTYNAME,
            RULE_LITERAL,
            RULE_NUMBER;

    private static final TokenGroup TOKENS;
    private static final OrderedHashDictionary RULES = new OrderedHashDictionary();
    public static final Predicate TRIM;

    static {
        try {
            //parse grammar
            final UNGrammarReader reader = new UNGrammarReader();
            final HashDictionary tokenGroups = new HashDictionary();
            reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/cql/cql.gr")));
            reader.read(tokenGroups, RULES);

            TOKENS = (TokenGroup) tokenGroups.getValue(new Chars("DEFAULT"));

            TOKEN_NAME    = (TokenType) TOKENS.getValue(new Chars("NAME"));
            TOKEN_ESCNAME = (TokenType) TOKENS.getValue(new Chars("ESCNAME"));
            TOKEN_TEXT    = (TokenType) TOKENS.getValue(new Chars("TEXT"));
            TOKEN_NUMBER  = (TokenType) TOKENS.getValue(new Chars("NUMBER"));
            TOKEN_SIGN    = (TokenType) TOKENS.getValue(new Chars("SIGN"));
            TOKEN_WS      = (TokenType) TOKENS.getValue(new Chars("WS"));

            RULE_FILE         = (Rule) RULES.getValue(new Chars("file"));
            RULE_PROPERTYNAME = (Rule) RULES.getValue(new Chars("propertyname"));
            RULE_LITERAL      = (Rule) RULES.getValue(new Chars("literal"));
            RULE_NUMBER       = (Rule) RULES.getValue(new Chars("number"));

            final TokenType[] toExclude = new TokenType[]{
                TOKEN_WS
            };

            TRIM = new Predicate() {
                public Boolean evaluate(Object candidate) {
                    final SyntaxNode sn = (SyntaxNode) candidate;
                    final Token token = sn.getToken();
                    return token!=null && Arrays.containsIdentity(toExclude, 0, toExclude.length, token.type);
                }
            };

        } catch(IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private CQLConstants() {}


}
