
package science.unlicense.format.wkt;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.geometry.impl.MultiPoint;
import science.unlicense.geometry.impl.MultiPolygon;
import science.unlicense.geometry.impl.MultiPolyline;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.math.api.Tuple;
import science.unlicense.common.api.number.Float64;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public class WKTWriter extends AbstractWriter{

    private CharOutputStream ds;
    protected WKTGeometryInfo currentGeom = null;

    void write(Geometry candidate) throws IOException {
        ds = getOutputAsCharStream(CharEncodings.US_ASCII);

        if (candidate instanceof Point){
            final Point geom = (Point) candidate;
            currentGeom = new WKTGeometryInfo();
            currentGeom.geomType = WKTConstants.KW_POINT;
            currentGeom.coordType = WKTConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            writeCoordinate(geom.getCoordinate());

        } else if (candidate instanceof Polyline){
            final Polyline geom = (Polyline) candidate;
            currentGeom = new WKTGeometryInfo();
            currentGeom.geomType = WKTConstants.KW_LINESTRING;
            currentGeom.coordType = WKTConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            writeCoordinates(geom.getCoordinates());

        } else if (candidate instanceof Polygon){
            final Polygon geom = (Polygon) candidate;
            currentGeom = new WKTGeometryInfo();
            currentGeom.geomType = WKTConstants.KW_POLYGON;
            currentGeom.coordType = WKTConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            ds.write('(');
            writeCoordinates(geom.getExterior().getCoordinates());
            final Sequence holes = geom.getInteriors();
            if (!holes.isEmpty()){
                Polyline h = (Polyline) holes.get(0);
                writeCoordinates(h.getCoordinates());
                for (int i=1,n=holes.getSize();i<n;i++){
                    h = (Polyline) holes.get(i);
                    ds.write(',');
                    writeCoordinates(h.getCoordinates());
                }
            }
            ds.write(')');

        } else if (candidate instanceof MultiPoint){
//            final MultiPoint geom = (MultiPoint) candidate;
//            currentGeom = new WKTGeometryInfo();
//            currentGeom.geomType = WKTConstants.KW_MULTIPOINT;
//            currentGeom.coordType = WKTConstants.COORD_TYPE_2D;
//            currentGeom.dimension = 2;
//            writeFlag();
//            ds.writeInt(geom.getGeometries().getSize(),currentGeom.encoding);
//            final WKTGeometryInfo stack = currentGeom;
//            for (int i=0,n=geom.getGeometries().getSize();i<n;i++){
//                write((Geometry) geom.getGeometries().get(i));
//            }
//            currentGeom = stack;
        } else if (candidate instanceof MultiPolyline){
//            final MultiPolyline geom = (MultiPolyline) candidate;
//            currentGeom = new WKTGeometryInfo();
//            currentGeom.geomType = WKTConstants.KW_MULTILINESTRING;
//            currentGeom.coordType = WKTConstants.COORD_TYPE_2D;
//            currentGeom.dimension = 2;
//            writeFlag();
//            ds.writeInt(geom.getGeometries().getSize(),currentGeom.encoding);
//            final WKTGeometryInfo stack = currentGeom;
//            for (int i=0,n=geom.getGeometries().getSize();i<n;i++){
//                write((Geometry) geom.getGeometries().get(i));
//            }
//            currentGeom = stack;
        } else if (candidate instanceof MultiPolygon){
//            final MultiPolygon geom = (MultiPolygon) candidate;
//            currentGeom = new WKTGeometryInfo();
//            currentGeom.geomType = WKTConstants.KW_MULTIPOLYGON;
//            currentGeom.coordType = WKTConstants.COORD_TYPE_2D;
//            currentGeom.dimension = 2;
//            writeFlag();
//            ds.writeInt(geom.getGeometries().getSize(),currentGeom.encoding);
//            final WKTGeometryInfo stack = currentGeom;
//            for (int i=0,n=geom.getGeometries().getSize();i<n;i++){
//                write((Geometry) geom.getGeometries().get(i));
//            }
//            currentGeom = stack;
        } else {
            throw new IOException("Unsupported geometry : "+candidate);
        }
    }

    private void writeFlag() throws IOException{
        ds.write(currentGeom.geomType);
    }

    private void writeCoordinate(Tuple coord) throws IOException{
        ds.write('(');
        ds.write(Float64.encode(coord.get(0)));
        for (int i=1;i<currentGeom.dimension;i++){
            ds.write(' ');
            ds.write(Float64.encode(coord.get(i)));
        }
        ds.write(')');
    }

    private void writeCoordinates(TupleGrid1D coords) throws IOException {
        final TupleRW tuple = coords.createTuple();
        ds.write('(');
        for (int i=0,n=coords.getDimension();i<n;i++){
            coords.getTuple(i,tuple);
            ds.write(Float64.encode(tuple.get(0)));
            for (int k=1;k<currentGeom.dimension;k++){
                ds.write(' ');
                ds.write(Float64.encode(tuple.get(k)));
            }
            if (i<n-1){
                ds.write(',');
            }
        }
        ds.write(')');
    }

    public static Chars toWKT(Geometry geom) throws IOException{
        final ArrayOutputStream stream = new ArrayOutputStream();
        final WKTWriter writer = new WKTWriter();
        writer.setOutput(stream);
        writer.write(geom);
        return new Chars(stream.getBuffer().toArrayByte(),CharEncodings.US_ASCII);
    }

}
