
package science.unlicense.format.wkt;

import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.impl.MultiPoint;
import science.unlicense.geometry.impl.MultiPolygon;
import science.unlicense.geometry.impl.MultiPolyline;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public class WKBWriter extends AbstractWriter{

    private Endianness globalEndianness = Endianness.BIG_ENDIAN;
    private DataOutputStream ds;
    protected WKBGeometryInfo currentGeom = null;

    void write(Geometry candidate) throws IOException {
        ds = getOutputAsDataStream(globalEndianness);

        if (candidate instanceof Point){
            final Point geom = (Point) candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.endianness = globalEndianness;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_POINT;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            writeCoordinate(geom.getCoordinate().toDouble());

        } else if (candidate instanceof Polyline){
            final Polyline geom = (Polyline) candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.endianness = globalEndianness;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_LINESTRING;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            writeCoordinates(geom.getCoordinates());

        } else if (candidate instanceof Polygon){
            final Polygon geom = (Polygon) candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.endianness = globalEndianness;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_POLYGON;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            ds.writeInt(geom.getInteriors().getSize()+1,currentGeom.endianness);
            writeCoordinates(geom.getExterior().getCoordinates());
            for (int i=0,n=geom.getInteriors().getSize();i<n;i++){
                writeCoordinates(((Polyline) geom.getInteriors().get(i)).getCoordinates());
            }

        } else if (candidate instanceof MultiPoint){
            final MultiPoint geom = (MultiPoint) candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.endianness = globalEndianness;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_MULTIPOINT;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            ds.writeInt(geom.getGeometries().getSize(),currentGeom.endianness);
            final WKBGeometryInfo stack = currentGeom;
            for (int i=0,n=geom.getGeometries().getSize();i<n;i++){
                write((Geometry) geom.getGeometries().get(i));
            }
            currentGeom = stack;
        } else if (candidate instanceof MultiPolyline){
            final MultiPolyline geom = (MultiPolyline) candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.endianness = globalEndianness;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_MULTILINESTRING;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            ds.writeInt(geom.getGeometries().getSize(),currentGeom.endianness);
            final WKBGeometryInfo stack = currentGeom;
            for (int i=0,n=geom.getGeometries().getSize();i<n;i++){
                write((Geometry) geom.getGeometries().get(i));
            }
            currentGeom = stack;
        } else if (candidate instanceof MultiPolygon){
            final MultiPolygon geom = (MultiPolygon) candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.endianness = globalEndianness;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_MULTIPOLYGON;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            ds.writeInt(geom.getGeometries().getSize(),currentGeom.endianness);
            final WKBGeometryInfo stack = currentGeom;
            for (int i=0,n=geom.getGeometries().getSize();i<n;i++){
                write((Geometry) geom.getGeometries().get(i));
            }
            currentGeom = stack;
        } else {
            throw new IOException("Unsupported geometry : "+candidate);
        }
    }

    private void writeFlag() throws IOException{
        //endian
        ds.write(WKBConstants.ENCODING_BIG_ENDIAN);

        //type and dimensions
        int val = currentGeom.geomType;
        if (currentGeom.coordType == WKBConstants.COORD_TYPE_2D){
            //2d no change
        } else if (currentGeom.coordType == WKBConstants.COORD_TYPE_2DM){
            val += 2000;
        } else if (currentGeom.coordType == WKBConstants.COORD_TYPE_3D){
            val += 1000;
        } else if (currentGeom.coordType == WKBConstants.COORD_TYPE_3DM){
            val += 3000;
        }

        ds.writeInt(val, currentGeom.endianness);
    }

    private void writeCoordinate(double[] coord) throws IOException{
        ds.writeDouble(coord, currentGeom.endianness);
    }

    private void writeCoordinates(TupleGrid1D coords) throws IOException{
        final TupleRW tuple = coords.createTuple();
        ds.writeInt(coords.getDimension(),currentGeom.endianness);
        final double[] t = new double[coords.getSampleSystem().getNumComponents()];
        for (int i=0,n=coords.getDimension();i<n;i++){
            coords.getTuple(i, tuple);
            tuple.toDouble(t,0);
            writeCoordinate(t);
        }
    }

}
