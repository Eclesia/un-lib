
package science.unlicense.format.wkt;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class WKBConstants {

    public static final byte ENCODING_BIG_ENDIAN = 0;
    public static final byte ENCODING_LITTLE_ENDIAN = 1;

    public static final int GEOM_TYPE_GEOMETRY = 0;
    public static final int GEOM_TYPE_POINT = 1;
    public static final int GEOM_TYPE_LINESTRING = 2;
    public static final int GEOM_TYPE_POLYGON = 3;
    public static final int GEOM_TYPE_MULTIPOINT = 4;
    public static final int GEOM_TYPE_MULTILINESTRING = 5;
    public static final int GEOM_TYPE_MULTIPOLYGON = 6;
    public static final int GEOM_TYPE_GEOMETRYCOLLECTION = 7;
    public static final int GEOM_TYPE_CIRCULARSTRING = 8;
    public static final int GEOM_TYPE_COMPOUNDCURVE = 9;
    public static final int GEOM_TYPE_CURVEPOLYGON = 10;
    public static final int GEOM_TYPE_MULTICURVE = 11;
    public static final int GEOM_TYPE_MULTISURFACE = 12;
    public static final int GEOM_TYPE_CURVE = 13;
    public static final int GEOM_TYPE_SURFACE = 14;
    public static final int GEOM_TYPE_POLYHEDRALSURFACE = 15;
    public static final int GEOM_TYPE_TIN = 16 ;
    public static final int GEOM_TYPE_TRIANGLE = 17 ;

    public static final int COORD_TYPE_2D = 0 ;
    public static final int COORD_TYPE_2DM = 1 ;
    public static final int COORD_TYPE_3D = 2 ;
    public static final int COORD_TYPE_3DM = 3 ;

    /** Extended WKB, geometry property index key to store the srid */
    public static final Chars SRID = Chars.constant(
            new byte[]{'S','R','I','D'});
    /** Geometry coordinate type key */
    public static final Chars COORD_TYPE = Chars.constant(
            new byte[]{'C','O','O','R','D','_','T','Y','P','E'});

    private WKBConstants() {
    }

}
