
package science.unlicense.format.wkt;

import science.unlicense.common.api.character.Chars;


/**
 *
 * @author Johann Sorel
 */
class WKTGeometryInfo {
    //current geometry informations
    protected int flags;
    protected Chars geomType;
    protected int coordType;
    protected int dimension;

}
