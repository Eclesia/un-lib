
package science.unlicense.format.wkt;

import science.unlicense.common.api.character.CharIterator;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.DoubleSequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.impl.CircularString;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.MultiPoint;
import science.unlicense.geometry.impl.MultiPolygon;
import science.unlicense.geometry.impl.MultiPolyline;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.TupleNf64;
import science.unlicense.math.impl.VectorNf64;

/**
 * Well Known Text reader.
 * @author Johann Sorel
 */
public class WKTReader {

    private static final int TYPE_POINT                 = 1;
    private static final int TYPE_LINESTRING            = 2;
    private static final int TYPE_CIRCULARSTRING        = 3;
    private static final int TYPE_COMPOUNDCURVE         = 4;
    private static final int TYPE_CURVEPOLYGON          = 5;
    private static final int TYPE_POLYGON               = 6;
    private static final int TYPE_TRIANGLE              = 7;
    private static final int TYPE_POLYHEDRALSURFACE     = 8;
    private static final int TYPE_TIN                   = 9;
    private static final int TYPE_MULTIPOINT            = 10;
    private static final int TYPE_MULTICURVE            = 11;
    private static final int TYPE_MULTILINESTRING       = 12;
    private static final int TYPE_MULTISURFACE          = 13;
    private static final int TYPE_MULTIPOLYGON          = 14;
    private static final int TYPE_GEOMETRYCOLLECTION    = 15;

    private static final int LPAREN = 40;
    private static final int RPAREN = 41;
    private static final int COMMA = 44;

    protected static class Current{
        //current geometry informations
        public int geomType;
        public int coordType;
        public int dimension;
    }

    protected CharIterator ite;
    protected Current currentGeom = null;

    public WKTReader() {
    }

    public Geometry read(Chars wkt) throws IOException{
        ite = wkt.createIterator();
        return readGeometry();
    }

    private Geometry readGeometry() throws IOException{
        currentGeom = new Current();
        readGeometryType();
        skipBlanks();
        readDimension();

        Geometry geom = null;
        if (TYPE_POINT == currentGeom.geomType){
            geom = readPointText();
        } else if (TYPE_LINESTRING == currentGeom.geomType){
            geom = readLineStringText();
        } else if (TYPE_CIRCULARSTRING == currentGeom.geomType){
            geom = readCircularStringText();
        } else if (TYPE_COMPOUNDCURVE == currentGeom.geomType){

        } else if (TYPE_CURVEPOLYGON == currentGeom.geomType){

        } else if (TYPE_POLYGON == currentGeom.geomType){
            geom = readPolygonText();
        } else if (TYPE_TRIANGLE == currentGeom.geomType){

        } else if (TYPE_POLYHEDRALSURFACE == currentGeom.geomType){

        } else if (TYPE_TIN == currentGeom.geomType){

        } else if (TYPE_MULTIPOINT == currentGeom.geomType){
            skipBlanks();
            final Sequence subs;
            if (isEmpty()){
                subs = null;
            } else {
                expect(LPAREN);
                subs = new ArraySequence();
                while (true){
                    subs.add(readPointText());
                    skipBlanks();
                    int c = ite.peekToUnicode();
                    if (c == COMMA){
                        ite.skip();
                    } else {
                        break;
                    }
                }
                expect(RPAREN);
            }
            geom = new MultiPoint(subs);
        } else if (TYPE_MULTICURVE == currentGeom.geomType){

        } else if (TYPE_MULTILINESTRING == currentGeom.geomType){
            skipBlanks();
            final Sequence subs;
            if (isEmpty()){
                subs = null;
            } else {
                expect(LPAREN);
                subs = new ArraySequence();
                while (true){
                    subs.add(readLineStringText());
                    skipBlanks();
                    int c = ite.peekToUnicode();
                    if (c == COMMA){
                        ite.skip();
                    } else {
                        break;
                    }
                }
                expect(RPAREN);
            }
            geom = new MultiPolyline(subs);
        } else if (TYPE_MULTISURFACE == currentGeom.geomType){

        } else if (TYPE_MULTIPOLYGON == currentGeom.geomType){
            skipBlanks();
            final Sequence subs;
            if (isEmpty()){
                subs = null;
            } else {
                expect(LPAREN);
                subs = new ArraySequence();
                while (true){
                    subs.add(readPolygonText());
                    skipBlanks();
                    int c = ite.peekToUnicode();
                    if (c == COMMA){
                        ite.skip();
                    } else {
                        break;
                    }
                }
                expect(RPAREN);
            }
            geom = new MultiPolygon(subs);
        } else if (TYPE_GEOMETRYCOLLECTION == currentGeom.geomType){

        }

        geom.getUserProperties().add(WKTConstants.COORD_TYPE, currentGeom.coordType);
        return geom;
    }

    private Point readPointText() throws IOException{
        skipBlanks();
        final TupleRW c;
        if (isEmpty()) {
            c = VectorNf64.createDouble(currentGeom.dimension);
            c.setAll(Double.NaN);
        } else {
            expect(LPAREN);
            c = new TupleNf64(readCoordinate());
            expect(RPAREN);
        }
        return new DefaultPoint(c);
    }

    private Polyline readLineStringText() throws IOException{
        skipBlanks();
        final TupleGrid1D coords;
        if (isEmpty()) {
            final double[] emp = new double[0];
            coords = InterleavedTupleGrid1D.create(emp, currentGeom.dimension);
        } else {
            expect(LPAREN);
            coords = readCoordinates();
            expect(RPAREN);
        }
        return new Polyline(coords);
    }

    private CircularString readCircularStringText() throws IOException{
        skipBlanks();
        final TupleGrid1D coords;
        if (isEmpty()) {
            coords = null;
        } else {
            expect(LPAREN);
            coords = readCoordinates();
            expect(RPAREN);
        }
        return new CircularString(coords);
    }

    private Polygon readPolygonText() throws IOException{
        skipBlanks();
        Polyline outter = null;
        Sequence inner;
        if (isEmpty()){
            outter = null;
            inner = null;
        } else {
            expect(LPAREN);
            inner = new ArraySequence();
            while (true){
                int c = ite.peekToUnicode();
                if (c==LPAREN){
                    ite.skip();
                    final TupleGrid1D coords = readCoordinates();
                    if (outter == null){
                        outter = new Polyline(coords);
                    } else {
                        inner.add(new Polyline(coords));
                    }
                    expect(RPAREN);
                } else {
                    break;
                }
                skipBlanks();
                c = ite.peekToUnicode();
                if (c == COMMA){
                    ite.skip();
                } else {
                    break;
                }
            }
            expect(RPAREN);
        }
        return new Polygon(outter,inner);
    }

    private void readGeometryType() throws IOException{

        int c = nexToUpper();

        typetest:
        if (WKTConstants.B_CIRCULARSTRING[0] == c){//can be CIRCULARSTRING,COMPOUNDCURVE,CURVEPOLYGON
            c = nexToUpper();
            if (WKTConstants.B_CIRCULARSTRING[1] == c){
                expect(WKTConstants.B_CIRCULARSTRING, 2);
                currentGeom.geomType = TYPE_CIRCULARSTRING;
                return;
            } else if (WKTConstants.B_COMPOUNDCURVE[1] == c){
                expect(WKTConstants.B_COMPOUNDCURVE, 2);
                currentGeom.geomType = TYPE_COMPOUNDCURVE;
                return;
            } else if (WKTConstants.B_CURVEPOLYGON[1] == c){
                expect(WKTConstants.B_CURVEPOLYGON, 2);
                currentGeom.geomType = TYPE_CURVEPOLYGON;
                return;
            }
        } else if (WKTConstants.B_GEOMETRYCOLLECTION[0] == c){//can be GEOMETRYCOLLECTION
            expect(WKTConstants.B_GEOMETRYCOLLECTION, 1);
            currentGeom.geomType = TYPE_GEOMETRYCOLLECTION;
            return;
        } else if (WKTConstants.B_LINESTRING[0] == c){//can be LINESTRING
            expect(WKTConstants.B_LINESTRING, 1);
            currentGeom.geomType = TYPE_LINESTRING;
            return;
        } else if (WKTConstants.B_MULTICURVE[0] == c){//can be MULTICURVE,MULTILINESTRING,MULTIPOINT,MULTIPOLYGON,MULTISURFACE
            if (WKTConstants.B_MULTICURVE[1] != nexToUpper()) break typetest;
            if (WKTConstants.B_MULTICURVE[2] != nexToUpper()) break typetest;
            if (WKTConstants.B_MULTICURVE[3] != nexToUpper()) break typetest;
            if (WKTConstants.B_MULTICURVE[4] != nexToUpper()) break typetest;
            c = nexToUpper();
            if (WKTConstants.B_MULTICURVE[5] == c){
                expect(WKTConstants.B_MULTICURVE, 6);
                currentGeom.geomType = TYPE_MULTICURVE;
                return;
            } else if (WKTConstants.B_MULTILINESTRING[5] == c){
                expect(WKTConstants.B_MULTILINESTRING, 6);
                currentGeom.geomType = TYPE_MULTILINESTRING;
                return;
            } else if (WKTConstants.B_MULTIPOINT[5] == c){
                if (WKTConstants.B_MULTIPOINT[6] != nexToUpper()) break typetest;
                c = nexToUpper();
                if (WKTConstants.B_MULTIPOINT[7] == c){
                    expect(WKTConstants.B_MULTIPOINT, 8);
                    currentGeom.geomType = TYPE_MULTIPOINT;
                    return;
                } else if (WKTConstants.B_MULTIPOLYGON[7] == c){
                    expect(WKTConstants.B_MULTIPOLYGON, 8);
                    currentGeom.geomType = TYPE_MULTIPOLYGON;
                    return;
                }
            } else if (WKTConstants.B_MULTISURFACE[5] == c){
                expect(WKTConstants.B_MULTISURFACE, 6);
                currentGeom.geomType = TYPE_MULTISURFACE;
                return;
            }

        } else if (WKTConstants.B_POINT[0] == c){//can be POINT,POLYGON,POLYHEDRALSURFACE
            if (WKTConstants.B_POINT[1] != nexToUpper()) break typetest;
            c = nexToUpper();
            if (WKTConstants.B_POINT[2] == c){
                expect(WKTConstants.B_POINT, 3);
                currentGeom.geomType = TYPE_POINT;
                return;
            }
            if (WKTConstants.B_POLYGON[2] != c) break typetest;
            if (WKTConstants.B_POLYGON[3] != nexToUpper()) break typetest;
            c = nexToUpper();
            if (WKTConstants.B_POLYGON[4] == c){
                expect(WKTConstants.B_POLYGON, 5);
                currentGeom.geomType = TYPE_POLYGON;
                return;
            } else if (WKTConstants.B_POLYHEDRALSURFACE[4] == c){
                expect(WKTConstants.B_POLYHEDRALSURFACE, 5);
                currentGeom.geomType = TYPE_POLYHEDRALSURFACE;
                return;
            }
        } else if (WKTConstants.B_TIN[0] == c){//can be TIN,TRIANGLE
            c = nexToUpper();
            if (WKTConstants.B_TIN[1] == c){
                expect(WKTConstants.B_TIN, 2);
                currentGeom.geomType = TYPE_TIN;
                return;
            } else if (WKTConstants.B_TRIANGLE[1] == c){
                expect(WKTConstants.B_TRIANGLE, 2);
                currentGeom.geomType = TYPE_TRIANGLE;
                return;
            }
        }

        throw new IOException("unknowned type");
    }

    private void readDimension() throws IOException{
        if (!ite.hasNext()) throw new IOException("No enough characters");
        int c =ite.peekToUnicode();
        if (WKTConstants.B_Z[0] == c){
            ite.skip();
            if (!ite.hasNext()) throw new IOException("No enough characters");
            c = ite.peekToUnicode();
            if (WKTConstants.B_M[0] == c){
                ite.skip();
                currentGeom.coordType = WKTConstants.COORD_TYPE_3DM;
                currentGeom.dimension = 4;
            } else {
                currentGeom.coordType = WKTConstants.COORD_TYPE_3D;
                currentGeom.dimension = 3;
            }
        } else if (WKTConstants.B_M[0] == c){
            ite.skip();
            currentGeom.coordType = WKTConstants.COORD_TYPE_2DM;
            currentGeom.dimension = 3;
        } else {
            currentGeom.coordType = WKTConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
        }
    }

    protected double[] readCoordinate(){
        final double[] c = new double[currentGeom.dimension];
        for (int i=0;i<currentGeom.dimension;i++){
            skipBlanks();
            c[i] = Float64.decode(ite, false);
        }
        return c;
    }

    protected TupleGrid1D readCoordinates() throws IOException{
        final DoubleSequence coords = new DoubleSequence();
        while (true){
            skipBlanks();
            coords.put(readCoordinate());
            skipBlanks();
            if (!ite.hasNext()) throw new IOException("No enough characters");
            int c =ite.peekToUnicode();
            if (c != COMMA) break;
            ite.skip();
        }
        return InterleavedTupleGrid1D.create(coords.toArrayDouble(), currentGeom.dimension);
    }

    private boolean isEmpty() throws IOException{
        skipBlanks();
        if (!ite.hasNext()) throw new IOException("No enough characters");
        int c =ite.peekToUnicode();
        if (WKTConstants.B_EMPTY[0] == c){
            ite.skip();
            expect(WKTConstants.B_EMPTY,1);
            return true;
        }
        return false;
    }

    private int next() throws IOException{
        if (!ite.hasNext()) throw new IOException("No enough characters");
        return ite.nextToUnicode();
    }

    private int nexToUpper() throws IOException{
        if (!ite.hasNext()) throw new IOException("No enough characters");
        int c = ite.nextToUnicode();
        if (c>96) c-=32; //to uppercase
        return c;
    }

    protected void skipBlanks(){
        while (ite.hasNext()){
            final int c = ite.peekToUnicode();
            if (c==9||c==10||c==32){
                ite.skip();
            } else {
                return;
            }
        }
    }

    private void expect(int c) throws IOException{
        if (!ite.hasNext()) throw new IOException("No enough characters");
        if (ite.nextToUnicode() != c) throw new IOException("Unvalid char.");
    }

    private void expect(byte[] uppercasearray, int offset) throws IOException{
        for (;offset<uppercasearray.length;offset++){
            if (!ite.hasNext()) throw new IOException("was expecting : "+new Chars(uppercasearray));
            int c = ite.nextToUnicode();
            if (c>96) c-=32; //to uppercase
            if (uppercasearray[offset]!=c) break;
        }
    }

}
