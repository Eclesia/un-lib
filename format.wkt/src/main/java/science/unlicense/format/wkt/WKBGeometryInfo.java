
package science.unlicense.format.wkt;

import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
class WKBGeometryInfo {
    //current geometry informations
    protected Endianness endianness = null;
    protected int flags;
    protected int geomType;
    protected int coordType;
    protected int dimension;

}
