
package science.unlicense.format.wkt;

import science.unlicense.common.api.character.Chars;

/**
 * WKT constants
 *
 * @author Johann Sorel
 */
public final class WKTConstants {

    private WKTConstants() {}

    static final byte[] B_POINT                 = new byte[]{'P','O','I','N','T'};
    static final byte[] B_LINESTRING            = new byte[]{'L','I','N','E','S','T','R','I','N','G'};
    static final byte[] B_CIRCULARSTRING        = new byte[]{'C','I','R','C','U','L','A','R','S','T','R','I','N','G'};
    static final byte[] B_COMPOUNDCURVE         = new byte[]{'C','O','M','P','O','U','N','D','C','U','R','V','E'};
    static final byte[] B_CURVEPOLYGON          = new byte[]{'C','U','R','V','E','P','O','L','Y','G','O','N'};
    static final byte[] B_POLYGON               = new byte[]{'P','O','L','Y','G','O','N'};
    static final byte[] B_TRIANGLE              = new byte[]{'T','R','I','A','N','G','L','E'};
    static final byte[] B_POLYHEDRALSURFACE     = new byte[]{'P','O','L','Y','H','E','D','R','A','L','S','U','R','F','A','C','E'};
    static final byte[] B_TIN                   = new byte[]{'T','I','N'};
    static final byte[] B_MULTIPOINT            = new byte[]{'M','U','L','T','I','P','O','I','N','T'};
    static final byte[] B_MULTICURVE            = new byte[]{'M','U','L','T','I','C','U','R','V','E'};
    static final byte[] B_MULTILINESTRING       = new byte[]{'M','U','L','T','I','L','I','N','E','S','T','R','I','N','G'};
    static final byte[] B_MULTISURFACE          = new byte[]{'M','U','L','T','I','S','U','R','F','A','C','E'};
    static final byte[] B_MULTIPOLYGON          = new byte[]{'M','U','L','T','I','P','O','L','Y','G','O','N'};
    static final byte[] B_GEOMETRYCOLLECTION    = new byte[]{'G','E','O','M','E','T','R','Y','C','O','L','L','E','C','T','I','O','N'};

    static final byte[] B_PATCHES               = new byte[]{'P','A','T','C','H','E','S'};
    static final byte[] B_ELEMENTS              = new byte[]{'E','L','E','M','E','N','T','S'};
    static final byte[] B_POINTS                = new byte[]{'P','O','I','N','T','S'};
    static final byte[] B_GROUPSPOT             = new byte[]{'G','R','O','U','P','S','P','O','T'};
    static final byte[] B_BOUNDARY              = new byte[]{'B','O','U','N','D','A','R','Y'};
    static final byte[] B_BREAKLINE             = new byte[]{'B','R','E','A','K','L','I','N','E'};
    static final byte[] B_SOFTBREAK             = new byte[]{'S','O','F','T','B','R','E','A','K'};
    static final byte[] B_CONTROLCONTOUR        = new byte[]{'C','O','N','T','R','O','L','C','O','N','T','O','U','R'};
    static final byte[] B_BREAKVOID             = new byte[]{'B','R','E','A','K','V','O','I','D'};
    static final byte[] B_DRAPEVOID             = new byte[]{'D','R','A','P','E','V','O','I','D'};
    static final byte[] B_VOID                  = new byte[]{'V','O','I','D'};
    static final byte[] B_HOLE                  = new byte[]{'H','O','L','E'};
    static final byte[] B_STOPLINE              = new byte[]{'S','T','O','P','L','I','N','E'};
    static final byte[] B_ID                    = new byte[]{'I','D'};
    static final byte[] B_TAG                   = new byte[]{'T','A','G'};
    static final byte[] B_EMPTY                 = new byte[]{'E','M','P','T','Y'};
    static final byte[] B_ZM                    = new byte[]{'Z','M'};
    static final byte[] B_Z                     = new byte[]{'Z'};
    static final byte[] B_M                     = new byte[]{'M'};
    static final byte[] B_MAXSIDELENGTH         = new byte[]{'M','A','X','S','I','D','E','L','E','N','G','T','H'};

    public static final Chars KW_POINT                 = Chars.constant(B_POINT);
    public static final Chars KW_LINESTRING            = Chars.constant(B_LINESTRING);
    public static final Chars KW_CIRCULARSTRING        = Chars.constant(B_CIRCULARSTRING);
    public static final Chars KW_COMPOUNDCURVE         = Chars.constant(B_COMPOUNDCURVE);
    public static final Chars KW_CURVEPOLYGON          = Chars.constant(B_CURVEPOLYGON);
    public static final Chars KW_POLYGON               = Chars.constant(B_POLYGON);
    public static final Chars KW_TRIANGLE              = Chars.constant(B_TRIANGLE);
    public static final Chars KW_POLYHEDRALSURFACE     = Chars.constant(B_POLYHEDRALSURFACE);
    public static final Chars KW_TIN                   = Chars.constant(B_TIN);
    public static final Chars KW_MULTIPOINT            = Chars.constant(B_MULTIPOINT);
    public static final Chars KW_MULTICURVE            = Chars.constant(B_MULTICURVE);
    public static final Chars KW_MULTILINESTRING       = Chars.constant(B_MULTILINESTRING);
    public static final Chars KW_MULTISURFACE          = Chars.constant(B_MULTISURFACE);
    public static final Chars KW_MULTIPOLYGON          = Chars.constant(B_MULTIPOLYGON);
    public static final Chars KW_GEOMETRYCOLLECTION    = Chars.constant(B_GEOMETRYCOLLECTION);

    public static final Chars KW_PATCHES               = Chars.constant(B_PATCHES);
    public static final Chars KW_ELEMENTS              = Chars.constant(B_ELEMENTS);
    public static final Chars KW_POINTS                = Chars.constant(B_POINTS);
    public static final Chars KW_GROUPSPOT             = Chars.constant(B_GROUPSPOT);
    public static final Chars KW_BOUNDARY              = Chars.constant(B_BOUNDARY);
    public static final Chars KW_BREAKLINE             = Chars.constant(B_BREAKLINE);
    public static final Chars KW_SOFTBREAK             = Chars.constant(B_SOFTBREAK);
    public static final Chars KW_CONTROLCONTOUR        = Chars.constant(B_CONTROLCONTOUR);
    public static final Chars KW_BREAKVOID             = Chars.constant(B_BREAKVOID);
    public static final Chars KW_DRAPEVOID             = Chars.constant(B_DRAPEVOID);
    public static final Chars KW_VOID                  = Chars.constant(B_VOID);
    public static final Chars KW_HOLE                  = Chars.constant(B_HOLE);
    public static final Chars KW_STOPLINE              = Chars.constant(B_STOPLINE);
    public static final Chars KW_ID                    = Chars.constant(B_ID);
    public static final Chars KW_TAG                   = Chars.constant(B_TAG);
    public static final Chars KW_EMPTY                 = Chars.constant(B_EMPTY);
    public static final Chars KW_ZM                    = Chars.constant(B_ZM);
    public static final Chars KW_Z                     = Chars.constant(B_Z);
    public static final Chars KW_M                     = Chars.constant(B_M);
    public static final Chars KW_MAXSIDELENGTH         = Chars.constant(B_MAXSIDELENGTH);

    public static final int COORD_TYPE_2D = 0 ;
    public static final int COORD_TYPE_2DM = 1 ;
    public static final int COORD_TYPE_3D = 2 ;
    public static final int COORD_TYPE_3DM = 3 ;

    /** Extended WKT, geometry property index key to store the srid */
    public static final Chars SRID = Chars.constant(
            new byte[]{'S','R','I','D'});
    /** Geometry coordinate type key */
    public static final Chars COORD_TYPE = Chars.constant(
            new byte[]{'C','O','O','R','D','_','T','Y','P','E'});

}
