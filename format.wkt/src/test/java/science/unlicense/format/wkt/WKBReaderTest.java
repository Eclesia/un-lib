
package science.unlicense.format.wkt;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class WKBReaderTest {


    private Geometry read(byte[] data) throws IOException{
        final ByteInputStream stream = new ArrayInputStream(data);
        final WKBReader reader = new WKBReader();
        reader.setInput(stream);
        return reader.read();
    }

    @Test
    public void testReadPoint() throws IOException{
        Assert.assertEquals(WKBTestConstants.G_POINT2D, read(WKBTestConstants.B_POINT2D));
    }

    @Test
    public void testReadLineString() throws IOException{
        Assert.assertEquals(WKBTestConstants.G_LINESTRING2D, read(WKBTestConstants.B_LINESTRING2D));
    }

    @Test
    public void testReadPolygon() throws IOException{
        Assert.assertEquals(WKBTestConstants.G_POLYGON2D, read(WKBTestConstants.B_POLYGON2D));
    }

    @Test
    public void testReadMultiPoint() throws IOException{
        Assert.assertEquals(WKBTestConstants.G_MPOINT2D, read(WKBTestConstants.B_MPOINT2D));
    }

    @Test
    public void testReadMultiLineString() throws IOException{
        Assert.assertEquals(WKBTestConstants.G_MLINESTRING2D, read(WKBTestConstants.B_MLINESTRING2D));
    }

    @Test
    public void testReadMultiPolygon() throws IOException{
        Assert.assertEquals(WKBTestConstants.G_MPOLYGON2D, read(WKBTestConstants.B_MPOLYGON2D));
    }

    public void readGeometryCollection() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void readCircularString() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void readCompoundCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void readCurvePolygon() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void readMultiCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void readMultiSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void readPolyhedralSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void readTIN() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void readTriangle() throws IOException{
        throw new IOException("Not supported yet.");
    }

}
