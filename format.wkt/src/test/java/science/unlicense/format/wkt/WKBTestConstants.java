
package science.unlicense.format.wkt;

import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.MultiPoint;
import science.unlicense.geometry.impl.MultiPolygon;
import science.unlicense.geometry.impl.MultiPolyline;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.math.impl.Vector2f64;

/**
 * WKB constants used for tests.
 * @author Johann Sorel
 */
public class WKBTestConstants {

    private WKBTestConstants(){}

    private static final CharEncoding ENC = CharEncodings.US_ASCII;

    public static final Chars T_POINT2D;
    public static final Point G_POINT2D;
    public static final byte[] B_POINT2D;

    public static final Chars T_LINESTRING2D;
    public static final Polyline G_LINESTRING2D;
    public static final byte[] B_LINESTRING2D;

    public static final Chars T_POLYGON2D;
    public static final Polygon G_POLYGON2D;
    public static final byte[] B_POLYGON2D;

    public static final Chars T_MPOINT2D;
    public static final MultiPoint G_MPOINT2D;
    public static final byte[] B_MPOINT2D;

    public static final Chars T_MLINESTRING2D;
    public static final MultiPolyline G_MLINESTRING2D;
    public static final byte[] B_MLINESTRING2D;

    public static final Chars T_MPOLYGON2D;
    public static final MultiPolygon G_MPOLYGON2D;
    public static final byte[] B_MPOLYGON2D;

    static {
        T_POINT2D = new Chars("POINT (14.3 9.5)", ENC);
        G_POINT2D = new DefaultPoint(new Vector2f64(14.3, 9.5));
        B_POINT2D = new byte[]{0, 0, 0, 0, 1, 64, 44, -103, -103, -103, -103, -103, -102, 64, 35, 0, 0, 0, 0, 0, 0};

        T_LINESTRING2D = new Chars("LINESTRING (47.5 21.54, 56.3 84, -67 -12)", ENC);
        final Sequence l2ds = new ArraySequence();
        l2ds.add(new Vector2f64(47.5, 21.54));
        l2ds.add(new Vector2f64(56.3, 84));
        l2ds.add(new Vector2f64(-67, -12));
        G_LINESTRING2D = new Polyline(Geometries.toTupleBuffer(l2ds));
        B_LINESTRING2D = new byte[]{0, 0, 0, 0, 2, 0, 0, 0, 3, 64, 71, -64, 0, 0, 0, 0, 0, 64, 53, -118, 61, 112, -93, -41, 10, 64, 76, 38, 102, 102, 102, 102, 102, 64, 85, 0, 0, 0, 0, 0, 0, -64, 80, -64, 0, 0, 0, 0, 0, -64, 40, 0, 0, 0, 0, 0, 0};

        T_POLYGON2D = new Chars("POLYGON ((14 7, -56.3 39.1, 80 50.2, 14 7))", ENC);
        final Sequence p2ds = new ArraySequence();
        p2ds.add(new Vector2f64(14, 7));
        p2ds.add(new Vector2f64(-56.3, 39.1));
        p2ds.add(new Vector2f64(80, 50.2));
        p2ds.add(new Vector2f64(14, 7));
        G_POLYGON2D = new Polygon(new Polyline(Geometries.toTupleBuffer(p2ds)), new ArraySequence());
        B_POLYGON2D = new byte[]{0, 0, 0, 0, 3, 0, 0, 0, 1, 0, 0, 0, 4, 64, 44, 0, 0, 0, 0, 0, 0, 64, 28, 0, 0, 0, 0, 0, 0, -64, 76, 38, 102, 102, 102, 102, 102, 64, 67, -116, -52, -52, -52, -52, -51, 64, 84, 0, 0, 0, 0, 0, 0, 64, 73, 25, -103, -103, -103, -103, -102, 64, 44, 0, 0, 0, 0, 0, 0, 64, 28, 0, 0, 0, 0, 0, 0};

        T_MPOINT2D = new Chars("MULTIPOINT ((59.1 92), (-5.2 3.56), (36 -89))", ENC);
        final Sequence mp2ds = new ArraySequence();
        mp2ds.add(new DefaultPoint(new Vector2f64(59.1, 92)));
        mp2ds.add(new DefaultPoint(new Vector2f64(-5.2, 3.56)));
        mp2ds.add(new DefaultPoint(new Vector2f64(36, -89)));
        G_MPOINT2D = new MultiPoint(mp2ds);
        B_MPOINT2D = new byte[]{0, 0, 0, 0, 4, 0, 0, 0, 3, 0, 0, 0, 0, 1, 64, 77, -116, -52, -52, -52, -52, -51, 64, 87, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -64, 20, -52, -52, -52, -52, -52, -51, 64, 12, 122, -31, 71, -82, 20, 123, 0, 0, 0, 0, 1, 64, 66, 0, 0, 0, 0, 0, 0, -64, 86, 64, 0, 0, 0, 0, 0};

        T_MLINESTRING2D = new Chars("MULTILINESTRING ((-14.8 -15, 54 31), (56.9 -1.1, -10 21))", ENC);
        final Sequence mls2d1 = new ArraySequence();
        mls2d1.add(new Vector2f64(-14.8, -15));
        mls2d1.add(new Vector2f64(54, 31));
        final Polyline l1 = new Polyline(Geometries.toTupleBuffer(mls2d1));
        final Sequence mls2d2 = new ArraySequence();
        mls2d2.add(new Vector2f64(56.9, -1.1));
        mls2d2.add(new Vector2f64(-10, 21));
        final Polyline l2 = new Polyline(Geometries.toTupleBuffer(mls2d2));

        final Sequence ml2ds = new ArraySequence();
        ml2ds.add(l1);
        ml2ds.add(l2);
        G_MLINESTRING2D = new MultiPolyline(ml2ds);
        B_MLINESTRING2D = new byte[]{0, 0, 0, 0, 5, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 2, -64, 45, -103, -103, -103, -103, -103, -102, -64, 46, 0, 0, 0, 0, 0, 0, 64, 75, 0, 0, 0, 0, 0, 0, 64, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 2, 64, 76, 115, 51, 51, 51, 51, 51, -65, -15, -103, -103, -103, -103, -103, -102, -64, 36, 0, 0, 0, 0, 0, 0, 64, 53, 0, 0, 0, 0, 0, 0};

        T_MPOLYGON2D = new Chars("MULTIPOLYGON (((14 7, -56.3 39.1, 80 50.2, 14 7)))", ENC);
        final Sequence mpg2ds = new ArraySequence();
        mpg2ds.add(G_POLYGON2D);
        G_MPOLYGON2D = new MultiPolygon(mpg2ds);
        B_MPOLYGON2D = new byte[]{0, 0, 0, 0, 6, 0, 0, 0, 1, 0, 0, 0, 0, 3, 0, 0, 0, 1, 0, 0, 0, 4, 64, 44, 0, 0, 0, 0, 0, 0, 64, 28, 0, 0, 0, 0, 0, 0, -64, 76, 38, 102, 102, 102, 102, 102, 64, 67, -116, -52, -52, -52, -52, -51, 64, 84, 0, 0, 0, 0, 0, 0, 64, 73, 25, -103, -103, -103, -103, -102, 64, 44, 0, 0, 0, 0, 0, 0, 64, 28, 0, 0, 0, 0, 0, 0};

    }

}
