
package science.unlicense.format.wkt;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.impl.CircularString;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.MultiPoint;
import science.unlicense.geometry.impl.MultiPolygon;
import science.unlicense.geometry.impl.MultiPolyline;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class WKTReaderTest {

    private static final double DELTA = 0.000001;

    private Geometry read(Chars data) throws IOException{
        final WKTReader reader = new WKTReader();
        return reader.read(data);
    }

    public void testReadGeometry() throws IOException{
        throw new IOException("Not supported yet.");
    }

    @Test
    public void testReadPoint() throws IOException{
        final Geometry geometry = read(
                new Chars("POINT (14.3 9.5)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Point);
        final Point geom = (Point) geometry;
        Assert.assertEquals(2, geom.getCoordinate().getSampleCount());
        Assert.assertEquals(14.3d, geom.getCoordinate().get(0),DELTA);
        Assert.assertEquals(9.5d, geom.getCoordinate().get(1),DELTA);
        Assert.assertEquals(WKTConstants.COORD_TYPE_2D, geom.getUserProperties().getValue(WKTConstants.COORD_TYPE));
    }

    @Test
    public void testReadPointM() throws IOException{
        final Geometry geometry = read(
                new Chars("POINT M (14.3 9.5 4.8)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Point);
        final Point geom = (Point) geometry;
        Assert.assertEquals(3, geom.getCoordinate().getSampleCount());
        Assert.assertEquals(14.3d, geom.getCoordinate().get(0),DELTA);
        Assert.assertEquals(9.5d, geom.getCoordinate().get(1),DELTA);
        Assert.assertEquals(4.8d, geom.getCoordinate().get(2),DELTA);
        Assert.assertEquals(WKTConstants.COORD_TYPE_2DM, geom.getUserProperties().getValue(WKTConstants.COORD_TYPE));
    }

    @Test
    public void testReadPointZ() throws IOException{
        final Geometry geometry = read(
                new Chars("POINT Z (14.3 9.5 6.1)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Point);
        final Point geom = (Point) geometry;
        Assert.assertEquals(3, geom.getCoordinate().getSampleCount());
        Assert.assertEquals(14.3d, geom.getCoordinate().get(0),DELTA);
        Assert.assertEquals(9.5d, geom.getCoordinate().get(1),DELTA);
        Assert.assertEquals(6.1d, geom.getCoordinate().get(2),DELTA);
        Assert.assertEquals(WKTConstants.COORD_TYPE_3D, geom.getUserProperties().getValue(WKTConstants.COORD_TYPE));
    }

    @Test
    public void testReadPointZM() throws IOException{
        final Geometry geometry = read(
                new Chars("POINT ZM (14.3 9.5 6.1 4.8)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Point);
        final Point geom = (Point) geometry;
        Assert.assertEquals(4, geom.getCoordinate().getSampleCount());
        Assert.assertEquals(14.3d, geom.getCoordinate().get(0),DELTA);
        Assert.assertEquals(9.5d, geom.getCoordinate().get(1),DELTA);
        Assert.assertEquals(6.1d, geom.getCoordinate().get(2),DELTA);
        Assert.assertEquals(4.8d, geom.getCoordinate().get(3),DELTA);
        Assert.assertEquals(WKTConstants.COORD_TYPE_3DM, geom.getUserProperties().getValue(WKTConstants.COORD_TYPE));
    }

    @Test
    public void testReadPointEmpty() throws IOException{
        final Geometry geometry = read(
                new Chars("POINT ZM EMPTY", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Point);
        final Point geom = (Point) geometry;
        VectorRW v = VectorNf64.createDouble(4);
        v.setAll(Double.NaN);
        Assert.assertEquals(v, geom.getCoordinate());
        Assert.assertEquals(WKTConstants.COORD_TYPE_3DM, geom.getUserProperties().getValue(WKTConstants.COORD_TYPE));
    }

    @Test
    public void testreadLineString() throws IOException{
        final Geometry geometry = read(
                new Chars("LINESTRING (47.5 21.54, 56.3 84, -67 -12)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Polyline);
        final Polyline ls = (Polyline) geometry;
        Assert.assertEquals(3, ls.getCoordinates().getDimension());
        Vector2f64 v = new Vector2f64();
        ls.getCoordinates().getTuple(0,v);
        Assert.assertEquals(new Vector2f64(47.5,21.54), v);
        ls.getCoordinates().getTuple(1,v);
        Assert.assertEquals(new Vector2f64(56.3,84   ), v);
        ls.getCoordinates().getTuple(2,v);
        Assert.assertEquals(new Vector2f64(-67,-12  ), v);
    }

    @Test
    public void testReadPolygon() throws IOException{
        final Geometry geometry = read(
                new Chars("POLYGON ((14 7, -56.3 39.1, 80 50.2, 14 7))", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Polygon);
        final Polygon poly = (Polygon) geometry;
        Assert.assertEquals(0, poly.getInteriors().getSize());
        final Polyline exterior = poly.getExterior();
        Assert.assertEquals(4, exterior.getCoordinates().getDimension());
        Vector2f64 v = new Vector2f64();

        exterior.getCoordinates().getTuple(0,v);
        Assert.assertEquals(new Vector2f64(14,7   ), v);
        exterior.getCoordinates().getTuple(1,v);
        Assert.assertEquals(new Vector2f64(-56.3,39.1), v);
        exterior.getCoordinates().getTuple(2,v);
        Assert.assertEquals(new Vector2f64(80,50.2), v);
        exterior.getCoordinates().getTuple(3,v);
        Assert.assertEquals(new Vector2f64(14,7   ), v);
    }

    @Test
    public void testReadMultiPoint() throws IOException{
        final Geometry geometry = read(
                new Chars("MULTIPOINT ((59.1 92), (-5.2 3.56), (36 -89))", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof MultiPoint);
        final MultiPoint mul = (MultiPoint) geometry;
        Assert.assertEquals(3, mul.getGeometries().getSize());
        Vector2f64 v = new Vector2f64();

        Assert.assertEquals(new DefaultPoint(new Vector2f64(59.1,92  )), mul.getGeometries().get(0));
        Assert.assertEquals(new DefaultPoint(new Vector2f64(-5.2,3.56)), mul.getGeometries().get(1));
        Assert.assertEquals(new DefaultPoint(new Vector2f64(36,-89 )), mul.getGeometries().get(2));
    }

    @Test
    public void testReadMultiLineString() throws IOException{
        final Geometry geometry = read(
                new Chars("MULTILINESTRING ((-14.8 -15, 54 31), (56.9 -1.1, -10 21))", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof MultiPolyline);
        final MultiPolyline mul = (MultiPolyline) geometry;
        Assert.assertEquals(2, mul.getGeometries().getSize());
        Assert.assertEquals(2,((Polyline) mul.getGeometries().get(0)).getCoordinates().getDimension());
        Assert.assertEquals(2,((Polyline) mul.getGeometries().get(1)).getCoordinates().getDimension());
        Vector2f64 v = new Vector2f64();

        ((Polyline) mul.getGeometries().get(0)).getCoordinates().getTuple(0,v);
        Assert.assertEquals(new Vector2f64(-14.8,-15 ),v );
        ((Polyline) mul.getGeometries().get(0)).getCoordinates().getTuple(1,v);
        Assert.assertEquals(new Vector2f64(54,31  ),v );
        ((Polyline) mul.getGeometries().get(1)).getCoordinates().getTuple(0,v);
        Assert.assertEquals(new Vector2f64(56.9,-1.1),v );
        ((Polyline) mul.getGeometries().get(1)).getCoordinates().getTuple(1,v);
        Assert.assertEquals(new Vector2f64(-10,21  ),v );
    }

    @Test
    public void testReadMultiPolygon() throws IOException{
        final Geometry geometry = read(
                new Chars("MULTIPOLYGON (((14 7, -56.3 39.1, 80 50.2, 14 7)))", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof MultiPolygon);
        final MultiPolygon mul = (MultiPolygon) geometry;
        Assert.assertEquals(1, mul.getGeometries().getSize());
        final Polygon poly = (Polygon) mul.getGeometries().get(0);
        Assert.assertEquals(0, poly.getInteriors().getSize());
        final Polyline exterior = poly.getExterior();
        Assert.assertEquals(4, exterior.getCoordinates().getDimension());
        Vector2f64 v = new Vector2f64();

        exterior.getCoordinates().getTuple(0,v);
        Assert.assertEquals(new Vector2f64(14,7   ), v );
        exterior.getCoordinates().getTuple(1,v);
        Assert.assertEquals(new Vector2f64(-56.3,39.1), v );
        exterior.getCoordinates().getTuple(2,v);
        Assert.assertEquals(new Vector2f64(80,50.2), v );
        exterior.getCoordinates().getTuple(3,v);
        Assert.assertEquals(new Vector2f64(14,7   ), v );
    }

    public void testReadGeometryCollection() throws IOException{
        throw new IOException("Not supported yet.");
    }

    @Test
    public void testReadCircularString() throws IOException{
        final Geometry geometry = read(
                new Chars("CIRCULARSTRING (47.5 21.54, 56.3 84, -67 -12)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof CircularString);
        final CircularString ls = (CircularString) geometry;
        Assert.assertEquals(3, ls.getCoordinates().getDimension());
        Vector2f64 v = new Vector2f64();

        ls.getCoordinates().getTuple(0,v);
        Assert.assertEquals(new Vector2f64(47.5,21.54), v );
        ls.getCoordinates().getTuple(1,v);
        Assert.assertEquals(new Vector2f64(56.3,84   ), v );
        ls.getCoordinates().getTuple(2,v);
        Assert.assertEquals(new Vector2f64(-67,-12   ), v );
    }

    public void testReadCompoundCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testReadCurvePolygon() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testReadMultiCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testReadMultiSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testReadPolyhedralSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testReadTIN() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testReadTriangle() throws IOException{
        throw new IOException("Not supported yet.");
    }

}
