
package science.unlicense.format.wkt;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class WKBWriterTest {

    private byte[] write(Geometry geom) throws IOException{
        final ArrayOutputStream stream = new ArrayOutputStream();
        final WKBWriter writer = new WKBWriter();
        writer.setOutput(stream);
        writer.write(geom);
        return stream.getBuffer().toArrayByte();
    }

    @Test
    public void testWritePoint() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_POINT2D, write(WKBTestConstants.G_POINT2D));
    }

    @Test
    public void testWriteLineString() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_LINESTRING2D, write(WKBTestConstants.G_LINESTRING2D));
    }

    @Test
    public void testWritePolygon() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_POLYGON2D, write(WKBTestConstants.G_POLYGON2D));
    }

    @Test
    public void testWriteMultiPoint() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_MPOINT2D, write(WKBTestConstants.G_MPOINT2D));
    }

    @Test
    public void testWriteMultiLineString() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_MLINESTRING2D, write(WKBTestConstants.G_MLINESTRING2D));
    }

    @Test
    public void testWriteMultiPolygon() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_MPOLYGON2D, write(WKBTestConstants.G_MPOLYGON2D));
    }

    public void writeGeometryCollection() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void writeCircularString() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void writeCompoundCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void writeCurvePolygon() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void writeMultiCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void writeMultiSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void writePolyhedralSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void writeTIN() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void writeTriangle() throws IOException{
        throw new IOException("Not supported yet.");
    }

}
