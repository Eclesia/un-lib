
package science.unlicense.format.wkt;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class WKTWriterTest {

    private Chars write(Geometry geom) throws IOException{
        return WKTWriter.toWKT(geom);
    }

    @Ignore
    @Test
    public void testWritePoint() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_POINT2D, write(WKTTestConstants.G_POINT2D));
    }

    @Ignore
    @Test
    public void testWriteLineString() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_LINESTRING2D, write(WKTTestConstants.G_LINESTRING2D));
    }

    @Test
    public void testWritePolygon() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_POLYGON2D, write(WKTTestConstants.G_POLYGON2D));
    }

    @Ignore
    @Test
    public void testWriteMultiPoint() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_MPOINT2D, write(WKTTestConstants.G_MPOINT2D));
    }

    @Ignore
    @Test
    public void testWriteMultiLineString() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_MLINESTRING2D, write(WKTTestConstants.G_MLINESTRING2D));
    }

    @Ignore
    @Test
    public void testWriteMultiPolygon() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_MPOLYGON2D, write(WKTTestConstants.G_MPOLYGON2D));
    }

    public void testWriteGeometryCollection() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testWriteCircularString() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testWriteCompoundCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testWriteCurvePolygon() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testWriteMultiCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testWriteMultiSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testWritePolyhedralSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testWriteTIN() throws IOException{
        throw new IOException("Not supported yet.");
    }

    public void testWriteTriangle() throws IOException{
        throw new IOException("Not supported yet.");
    }
}
