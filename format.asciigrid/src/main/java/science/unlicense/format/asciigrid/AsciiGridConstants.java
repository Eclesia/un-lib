
package science.unlicense.format.asciigrid;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;

/**
 * AsciiGrid constants.
 *
 * @author Johann Sorel
 */
public final class AsciiGridConstants {

    public static final Chars NCOLS     = Chars.constant("NCOLS",CharEncodings.US_ASCII);
    public static final Chars NROWS     = Chars.constant("NROWS",CharEncodings.US_ASCII);
    public static final Chars XLLCENTER = Chars.constant("XLLCENTER",CharEncodings.US_ASCII);
    public static final Chars YLLCENTER = Chars.constant("YLLCENTER",CharEncodings.US_ASCII);
    public static final Chars XLLCORNER = Chars.constant("XLLCORNER",CharEncodings.US_ASCII);
    public static final Chars YLLCORNER = Chars.constant("YLLCORNER",CharEncodings.US_ASCII);
    public static final Chars CELLSIZE  = Chars.constant("CELLSIZE",CharEncodings.US_ASCII);
    public static final Chars NODATA_VALUE = Chars.constant("NODATA_VALUE",CharEncodings.US_ASCII);

    private AsciiGridConstants(){}

}
