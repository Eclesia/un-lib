
package science.unlicense.format.asciigrid;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.AbstractImageWriter;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageWriteParameters;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class AsciiGridWriter extends AbstractImageWriter{

    protected void write(Image image, ImageWriteParameters params, ByteOutputStream stream) throws IOException {

        final CharOutputStream cs = new CharOutputStream(stream,CharEncodings.US_ASCII);
        final int ncols = (int) image.getExtent().getL(0);
        final int nrows = (int) image.getExtent().getL(1);

        //write header
        cs.write(AsciiGridConstants.NCOLS).write(' ').write(Int32.encode(ncols)).endLine();
        cs.write(AsciiGridConstants.NROWS).write(' ').write(Int32.encode(nrows)).endLine();
        cs.write(AsciiGridConstants.XLLCORNER).write(' ').write('0').endLine();
        cs.write(AsciiGridConstants.YLLCORNER).write(' ').write('0').endLine();
        cs.write(AsciiGridConstants.CELLSIZE).write(' ').write('1').endLine();
        cs.write(AsciiGridConstants.NODATA_VALUE).write(new Chars(" -9999")).endLine();

        //write datas
        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final TupleRW sample = sm.createTuple();
        final Vector2i32 coord = new Vector2i32();
        for (coord.y=0;coord.y<nrows;coord.y++){
            for (coord.x=0;coord.x<ncols;coord.x++){
                sm.getTuple(coord, sample);
                cs.write(Int32.encode((int) sample.get(0))).write(' ');
            }
            cs.endLine();
        }

    }

}
