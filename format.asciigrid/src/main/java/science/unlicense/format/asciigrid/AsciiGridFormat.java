
package science.unlicense.format.asciigrid;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * Ascii grid image format.
 *
 * References :
 * http://daac.ornl.gov/MODIS/ASCII_Grid_Format_Description.html
 * http://resources.arcgis.com/en/help/main/10.1/index.html#//009t0000000z000000
 *
 * @author Johann Sorel
 */
public class AsciiGridFormat extends AbstractImageFormat {

    public static final AsciiGridFormat INSTANCE = new AsciiGridFormat();

    private AsciiGridFormat() {
        super(new Chars("asciigrid"));
        shortName = new Chars("ASCIIGRID");
        longName = new Chars("AsciiGrid");
        signatures.add(AsciiGridConstants.NCOLS.toBytes());
        signatures.add(AsciiGridConstants.NCOLS.toLowerCase().toBytes());
        signatures.add(AsciiGridConstants.NROWS.toBytes());
        signatures.add(AsciiGridConstants.NROWS.toLowerCase().toBytes());
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return true;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new AsciiGridStore(source);
    }
}
