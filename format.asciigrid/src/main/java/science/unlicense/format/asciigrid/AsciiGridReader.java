
package science.unlicense.format.asciigrid;

import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class AsciiGridReader extends AbstractImageReader{

    private final CharBuffer cb = new CharBuffer(CharEncodings.US_ASCII);

    public AsciiGridReader() {
    }

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        final Image img = read(null,stream);
        return img.getMetadatas();
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        int ncols = -1;
        int nrows = -1;
        int xllcenter = -1;
        int yllcenter = -1;
        int xllcorner = -1;
        int yllcorner = -1;
        int cellsize = -1;
        int nodata = -9999;

        final DataInputStream ds = new DataInputStream(stream);
        //read header
        int val = 0;
        while (true){
            final Chars word = readWord(stream);
            try{
                val = Int32.decode(word);
                //end of header
                break;
            }catch(RuntimeException ex){}

            final Chars value = readWord(stream);
            if (AsciiGridConstants.NCOLS.equals(word, true, true)){
                ncols = Int32.decode(value);
            } else if (AsciiGridConstants.NROWS.equals(word, true, true)){
                nrows = Int32.decode(value);
            } else if (AsciiGridConstants.XLLCENTER.equals(word, true, true)){
                xllcenter = Int32.decode(value);
            } else if (AsciiGridConstants.YLLCENTER.equals(word, true, true)){
                yllcenter = Int32.decode(value);
            } else if (AsciiGridConstants.XLLCORNER.equals(word, true, true)){
                xllcorner = Int32.decode(value);
            } else if (AsciiGridConstants.XLLCORNER.equals(word, true, true)){
                yllcorner = Int32.decode(value);
            } else if (AsciiGridConstants.CELLSIZE.equals(word, true, true)){
                cellsize = Int32.decode(value);
            } else if (AsciiGridConstants.NODATA_VALUE.equals(word, true, true)){
                nodata = Int32.decode(value);
            } else {
                //unknown skip it
            }
        }

        //read datas
        final Image image = Images.createCustomBand(new Extent.Long(ncols, nrows), 1, Int32.TYPE);
        final Int32Cursor cursor = image.getDataBuffer().asInt32().cursor();
        cursor.write(val);
        for (int i=1,n=ncols*nrows;i<n;i++){
            val = Int32.decode(readWord(stream));
            cursor.write(val);
        }

        return image;
    }

    private Chars readWord(BacktrackInputStream stream) throws IOException{
        cb.reset();
        //skip all white spaces
        for (int b = stream.read();b!=-1;b=stream.read()){
            if (b==' '||b=='\n'||b=='\t'||b=='\r'){
                if (cb.isEmpty()){
                    //prefix whitespaces
                    continue;
                } else {
                    //suffix whitespaces, end of word
                    break;
                }
            }
            cb.append(b);
        }
        return cb.toChars();
    }

}
