
package science.unlicense.format.asciigrid;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.ImageResource;
import science.unlicense.image.api.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class AsciiGridStore extends AbstractStore implements ImageResource {

    public AsciiGridStore(Object input) {
        super(AsciiGridFormat.INSTANCE, input);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return true;
    }

    @Override
    public ImageReader createReader() throws IOException {
        final AsciiGridReader reader = new AsciiGridReader();
        reader.setInput(source);
        return reader;
    }

    @Override
    public ImageWriter createWriter() throws IOException {
        final AsciiGridWriter writer = new AsciiGridWriter();
        writer.setOutput(source);
        return writer;
    }

    @Override
    public Chars getId() {
        return null;
    }

}
