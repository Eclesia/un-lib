
package science.unlicense.format.asciigrid;

import science.unlicense.format.asciigrid.AsciiGridReader;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.Image;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class AsciiGridReaderTest {

    @Test
    public void testRead() throws IOException{

        final AsciiGridReader reader = new AsciiGridReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/imagery/asciigrid/image.txt")));

        final Image image = reader.read(null);
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final int[] data = image.getDataBuffer().toIntArray();
        Assert.assertArrayEquals(new int[]{0,1,2,3},data);

    }

}
