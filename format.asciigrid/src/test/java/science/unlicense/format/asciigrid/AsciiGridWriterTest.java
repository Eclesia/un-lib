
package science.unlicense.format.asciigrid;

import science.unlicense.format.asciigrid.AsciiGridWriter;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class AsciiGridWriterTest {

    @Test
    public void testWrite() throws IOException{

        final Image image = Images.createCustomBand(new Extent.Long(2, 2), 1, Int32.TYPE);
        image.getDataBuffer().asInt32().write(0, new int[]{0,1,2,3});

        final AsciiGridWriter writer = new AsciiGridWriter();
        final ArrayOutputStream out = new ArrayOutputStream();
        writer.setOutput(out);
        writer.write(image, null);
        writer.dispose();

        final Chars ascii = new Chars(out.getBuffer().toArrayByte());

        Assert.assertEquals(new Chars(
                "NCOLS 2\n"
              + "NROWS 2\n"
              + "XLLCORNER 0\n"
              + "YLLCORNER 0\n"
              + "CELLSIZE 1\n"
              + "NODATA_VALUE -9999\n"
              + "0 1 \n"
              + "2 3 \n",CharEncodings.US_ASCII), ascii);
    }

}
