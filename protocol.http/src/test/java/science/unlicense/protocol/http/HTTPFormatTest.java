
package science.unlicense.protocol.http;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class HTTPFormatTest {

    /**
     * Test format is declared.
     */
    @Test
    public void testFormat(){

        final PathFormat[] formats = Paths.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof HTTPFormat){
                return;
            }
        }

        Assert.fail("HTTP Format not found");
    }

}
