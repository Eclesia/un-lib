
package science.unlicense.protocol.http;

import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class EmptyRequestException extends IOException {

    public EmptyRequestException() {
    }

    public EmptyRequestException(String message) {
        super(message);
    }

    public EmptyRequestException(Throwable cause) {
        super(cause);
    }

    public EmptyRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
