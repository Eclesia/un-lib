
package science.unlicense.protocol.http;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;

/**
 * HTTP version is composed of a major and minor integer value.
 *
 * @author Johann Sorel
 */
public final class HTTPVersion extends CObject{

    public static final HTTPVersion V0_9 = new HTTPVersion(0, 9);
    public static final HTTPVersion V1_0 = new HTTPVersion(1, 0);
    public static final HTTPVersion V1_1 = new HTTPVersion(1, 1);

    private final int major;
    private final int minor;
    private final Chars text;

    public HTTPVersion(int major, int minor) {
        this.major = major;
        this.minor = minor;
        text = new Chars("HTTP/"+major+"."+minor);
    }

    /**
     * Get version major value.
     * @return int
     */
    public int getMajor() {
        return major;
    }

    /**
     * Get version mjnor value.
     * @return int
     */
    public int getMinor() {
        return minor;
    }

    public Chars toChars(){
        return text;
    }

    public static HTTPVersion read(Chars text) {
        if (V0_9.text.equals(text)) return V0_9;
        else if (V1_0.text.equals(text)) return V1_0;
        else if (V1_1.text.equals(text)) return V1_1;
        else throw new IllegalArgumentException("Unknown version "+ text);
    }

}
