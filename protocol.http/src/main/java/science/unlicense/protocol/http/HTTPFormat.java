
package science.unlicense.protocol.http;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class HTTPFormat implements PathFormat {

    private final HTTPResolver resolver = new HTTPResolver(this);

    public boolean isAbsolute() {
        return true;
    }

    public boolean canCreate(Path path) throws IOException {
        return false;
    }

    public PathResolver createResolver(Path path) throws IOException {
        return resolver;
    }

    @Override
    public Chars getPrefix() {
        return new Chars("http");
    }

}
