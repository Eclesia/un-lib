
package science.unlicense.protocol.http;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class HTTPRequest implements HTTPConstants.Request {

    private final Dictionary headers = new HashDictionary();
    private HTTPVersion version = HTTPVersion.V1_1;
    private Chars method = METHOD_GET;
    private Chars url = new Chars("/");
    private HTTPMessageBody body;

    public HTTPVersion getVersion() {
        return version;
    }

    public void setVersion(HTTPVersion version) {
        this.version = version;
    }

    public Chars getMethod() {
        return method;
    }

    public void setMethod(Chars method) {
        this.method = method;
    }

    public Chars getUrl() {
        return url;
    }

    public void setUrl(Chars url) {
        this.url = url;
    }

    public Dictionary getHeaders() {
        return headers;
    }

    public HTTPMessageBody getMessageBody() {
        return body;
    }

    public void setMessageBody(HTTPMessageBody body) {
        this.body = body;
    }

    public void read(ByteInputStream in) throws IOException {

        final CharInputStream cs = new CharInputStream(in, CharEncodings.US_ASCII, new Char('\n'));
        final Sequence lines = new ArraySequence();
        for (;;) {
            Chars line = cs.readLine();
            if (line == null) {
                throw new EmptyRequestException();
            }
            lines.add(line.trim());
            if (line.endsWith(HTTPConstants.CRLF)) {
                break;
            }
        }

        //read request header
        final Iterator ite = lines.createIterator();
        final Chars header = (Chars) ite.next();
        final Chars[] params = header.split(' ');
        setMethod(params[0].trim());
        setUrl(params[1].trim());
        setVersion(HTTPVersion.read(params[2].trim()));

        //read request parameters
        while (ite.hasNext()) {
            final Chars line = (Chars) ite.next();
            final Chars[] keyValue = line.split(':');
            getHeaders().add(keyValue[0], keyValue[1].trim());
        }

        //read request body
        //TODO
    }

    public void write(ByteOutputStream out ) throws IOException {

        final CharOutputStream cs = new CharOutputStream(out, CharEncodings.US_ASCII);

        //write request header
        cs.write(getMethod());
        cs.write(' ');
        cs.write(getUrl());
        cs.write(' ');
        cs.write(getVersion().toChars());
        cs.write('\n');
        //write parameters
        final Dictionary params = getHeaders();
        final Iterator ite = params.getPairs().createIterator();
        while (ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final Chars key = (Chars) pair.getValue1();
            final Chars value = (Chars) pair.getValue2();
            cs.write(key);
            cs.write(':').write(' ');
            cs.write(value);
            cs.write('\n');
        }
        cs.flush();

        //write request body
        final HTTPMessageBody body = getMessageBody();
        if (body != null) {
            body.write(out);
        }

        //end request
        cs.write(HTTPConstants.CRLF);
        out.flush();
    }

}
