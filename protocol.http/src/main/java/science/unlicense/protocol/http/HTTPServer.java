
package science.unlicense.protocol.http;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.system.ServerSocket;

/**
 *
 * @author Johann Sorel
 */
public abstract class HTTPServer implements ServerSocket.ClientHandler {

    private int port;
    private ServerSocket socket;

    /**
     * Create an http server on default port (80)
     */
    public HTTPServer(){
        this(80);
    }

    /**
     * Create an http server on given port.
     * @param port port to listen
     */
    public HTTPServer(int port) {
        this.port = port;
    }

    /**
     * Server port.
     * If zero was passed at creation, the port will change after starting server.
     *
     * @return int
     */
    public int getPort() {
        return port;
    }

    /**
     * Start the server.
     */
    public void start() throws IOException{
        if (socket==null){
            socket = science.unlicense.system.System.get().getSocketManager().createServerSocket(port, this);
            port = socket.getPort();//port might have change if 0.
        }
    }

    /**
     * Stop the server.
     */
    public void stop() throws IOException{
        if (socket==null) return;

        socket.close();
        socket = null;
    }

}
