
package science.unlicense.protocol.http;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Int32;

/**
 *
 * @author Johann Sorel
 */
public class HTTPResponse {

    private static final Dictionary CODE_MESSAGE = new HashDictionary();
    static {
        CODE_MESSAGE.add(100, new Chars("Continue"));
        CODE_MESSAGE.add(101, new Chars("Switching Protocols"));
        CODE_MESSAGE.add(200, new Chars("OK"));
        CODE_MESSAGE.add(201, new Chars("Created"));
        CODE_MESSAGE.add(202, new Chars("Accepted"));
        CODE_MESSAGE.add(203, new Chars("Non-Authoritative Information"));
        CODE_MESSAGE.add(204, new Chars("No Content"));
        CODE_MESSAGE.add(205, new Chars("Reset Content"));
        CODE_MESSAGE.add(206, new Chars("Partial Content"));
        CODE_MESSAGE.add(300, new Chars("Multiple Choices"));
        CODE_MESSAGE.add(301, new Chars("Moved Permanently"));
        CODE_MESSAGE.add(302, new Chars("Found"));
        CODE_MESSAGE.add(303, new Chars("See Other"));
        CODE_MESSAGE.add(304, new Chars("Not Modified"));
        CODE_MESSAGE.add(305, new Chars("Use Proxy"));
        CODE_MESSAGE.add(306, new Chars("Switch Proxy"));
        CODE_MESSAGE.add(307, new Chars("Temporary Redirect"));
        CODE_MESSAGE.add(308, new Chars("Permanent Redirect"));
        CODE_MESSAGE.add(400, new Chars("Bad Request"));
        CODE_MESSAGE.add(401, new Chars("Unauthorized"));
        CODE_MESSAGE.add(402, new Chars("Payment Required"));
        CODE_MESSAGE.add(403, new Chars("Forbidden"));
        CODE_MESSAGE.add(404, new Chars("Not Found"));
        CODE_MESSAGE.add(405, new Chars("Method Not Allowed"));
        CODE_MESSAGE.add(406, new Chars("Not Acceptable"));
        CODE_MESSAGE.add(407, new Chars("Proxy Authentication Required"));
        CODE_MESSAGE.add(408, new Chars("Request Timeout"));
        CODE_MESSAGE.add(409, new Chars("Conflict"));
        CODE_MESSAGE.add(410, new Chars("Gone"));
        CODE_MESSAGE.add(411, new Chars("Length Required"));
        CODE_MESSAGE.add(412, new Chars("Precondition Failed"));
        CODE_MESSAGE.add(413, new Chars("Payload Too Large"));
        CODE_MESSAGE.add(414, new Chars("URI Too Long"));
        CODE_MESSAGE.add(415, new Chars("Unsupported Media Type"));
        CODE_MESSAGE.add(416, new Chars("Range Not Satisfiable"));
        CODE_MESSAGE.add(417, new Chars("Expectation Failed"));
        CODE_MESSAGE.add(500, new Chars("Internal Server Error"));
        CODE_MESSAGE.add(501, new Chars("Not Implemented"));
        CODE_MESSAGE.add(502, new Chars("Bad Gateway"));
        CODE_MESSAGE.add(503, new Chars("Service Unavailable"));
        CODE_MESSAGE.add(504, new Chars("Gateway Timeout"));
        CODE_MESSAGE.add(505, new Chars("HTTP Version Not Supported"));
    }

    private final Dictionary headers = new HashDictionary();
    private HTTPVersion version = HTTPVersion.V1_1;
    private int code;
    private Chars content;
    private HTTPMessageBody body;

    public HTTPVersion getVersion() {
        return version;
    }

    public void setVersion(HTTPVersion version) {
        this.version = version;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Dictionary getHeaders() {
        return headers;
    }

    public Chars getContent() {
        return content;
    }

    public void setContent(Chars content) {
        this.content = content;
    }

    public HTTPMessageBody getMessageBody() {
        return body;
    }

    public void setMessageBody(HTTPMessageBody body) {
        this.body = body;
    }


    public void read(ByteInputStream in) throws IOException{
        final byte[] data = science.unlicense.encoding.api.io.IOUtilities.readAll(in);
        Chars content = new Chars(data);
        final int headerLimit = content.getFirstOccurence(HTTPConstants.CRLF.concat(HTTPConstants.CRLF));
        if (headerLimit<0){
            throw new IOException("Header delimiter CRLF not found, invalid http response.");
        }
        final Chars header = content.truncate(0, headerLimit);
        final Chars[] lines = header.split('\n');
        for (Chars l : lines){
            System.out.println(">> "+l);
        }

        content = content.truncate(headerLimit+1, content.getCharLength());
        setContent(content);
    }

    public void write(ByteOutputStream out) throws IOException {

        final CharOutputStream cs = new CharOutputStream(out, CharEncodings.US_ASCII, new Char('\n'));

        //write request header
        cs.write(getVersion().toChars());
        cs.write(' ');
        cs.write(Int32.encode(getCode()));
        cs.write(' ');
        cs.write((Chars) CODE_MESSAGE.getValue(getCode()));
        cs.write('\n');
        //write parameters
        final Dictionary params = getHeaders();
        final Iterator ite = params.getPairs().createIterator();
        while (ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final Chars key = (Chars) pair.getValue1();
            final Chars value = (Chars) pair.getValue2();
            cs.write(key);
            cs.write(':').write(' ');
            cs.write(value);
            cs.write('\n');
        }
        cs.write(HTTPConstants.CRLF);

        //write content
        Chars content = getContent();
        if (content!=null) {
            cs.write(content);
        }
        cs.flush();

        HTTPMessageBody body = getMessageBody();
        if (body!=null) {
            body.write(out);
        }
        out.flush();

        cs.write(HTTPConstants.CRLF);
        cs.flush();
    }

}
