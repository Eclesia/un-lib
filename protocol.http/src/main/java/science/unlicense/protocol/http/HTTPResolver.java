
package science.unlicense.protocol.http;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class HTTPResolver implements PathResolver{

    private final HTTPFormat format;

    public HTTPResolver(HTTPFormat format) {
        this.format = format;
    }

    public PathFormat getPathFormat() {
        return format;
    }

    public Path resolve(Chars path) {
        if (!path.startsWith(new Chars("http:"))){
            return null;
        }

        return null;
    }

}
