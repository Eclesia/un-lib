
package science.unlicense.protocol.http;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Request and response message body.
 *
 * @author Johann Sorel
 */
public interface HTTPMessageBody {

    void write(ByteOutputStream out) throws IOException;

    void read(ByteInputStream out) throws IOException;

}
