
package science.unlicense.format.pbf;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class PBFInputStream extends DataInputStream {

    public PBFInputStream(ByteInputStream in) {
        super(in);
    }

    public Chars readLengthChars() throws IOException {
        final int length = (int) readVarLengthUInt();
        return new Chars(readBytes(length), CharEncodings.UTF_8);
    }

}
