
package science.unlicense.format.pbf;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public class PBFFormat extends DefaultFormat {

    public static final PBFFormat INSTANCE = new PBFFormat();

    public PBFFormat() {
        super(new Chars("pbf"));
        shortName = new Chars("PBF");
        longName = new Chars("ProtoBuffer");
        extensions.add(new Chars("pbf"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
