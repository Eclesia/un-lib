

package science.unlicense.format.ar;

import science.unlicense.archive.api.AbstractArchiveFormat;
import science.unlicense.archive.api.Archive;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class ARFormat extends AbstractArchiveFormat{

    public static final ARFormat INSTANCE = new ARFormat();
    private static final Chars SUFFIX = Chars.constant(".ar");

    private ARFormat() {
        super(new Chars("ar"));
        this.shortName = new Chars("AR");
        this.longName = new Chars("AR");
        this.extensions.add(new Chars("ar"));
        this.signatures.add(ARConstants.SIGNATURE);
    }

    @Override
    public boolean canDecode(Object candidate) {
        if (candidate instanceof Path) {
            return ((Path) candidate).getName().toLowerCase().endsWith(SUFFIX);
        }
        return false;
    }

    @Override
    public Archive open(Object candidate) throws IOException {
        if (candidate instanceof Path) {
            final ARArchive arc = new ARArchive((Path) candidate);
            return arc;
        }
        throw new IOException(candidate, "Unsupported input");
    }

    @Override
    public boolean isAbsolute() {
        return false;
    }

    @Override
    public boolean canCreate(Path base) throws IOException {
        return base.getName().toLowerCase().endsWith(SUFFIX);
    }

    @Override
    public PathResolver createResolver(Path base) throws IOException {
        return open(base);
    }

}
