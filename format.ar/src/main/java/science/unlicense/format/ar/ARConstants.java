
package science.unlicense.format.ar;

/**
 *
 * Resources :
 * https://en.wikipedia.org/wiki/Ar_(Unix)
 *
 * @author Johann Sorel
 */
public final class ARConstants {

    public static final byte[] SIGNATURE = new byte[]{'!','<','a','r','c','h','>'};

    private ARConstants(){}

}
