

package science.unlicense.format.ar;

import science.unlicense.archive.api.Archive;
import science.unlicense.archive.api.ArchiveFormat;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.tree.NodeMessage;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.AbstractPath;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.encoding.api.store.Resource;
import science.unlicense.encoding.api.store.StoreException;

/**
 *
 * @author Johann Sorel
 */
public class ARArchive extends AbstractPath implements Archive{

    private final Path base;

    public ARArchive(Path base) {
        this.base = base;
    }

    public ArchiveFormat getFormat() {
        return ARFormat.INSTANCE;
    }

    public PathFormat getPathFormat() {
        return ARFormat.INSTANCE;
    }

    public boolean canHaveChildren() {
        return true;
    }

    public Chars getName() {
        return base.getName();
    }

    public Path getParent() {
        return base.getParent();
    }

    public boolean isContainer() throws IOException {
        return true;
    }

    public boolean exists() throws IOException {
        return base.exists();
    }

    public Class[] getEventClasses() {
        return new Class[]{NodeMessage.class};
    }

    public Collection getChildren() {
        throw new RuntimeException("todo");
    }

    public Path resolve(Chars address) {
        if (address==null) return this;

        if (address.startsWith('/')) {
            address = address.truncate(1,-1);
        }

        throw new RuntimeException("todo");
    }

    public PathResolver getResolver() {
        return this;
    }

    public ByteInputStream createInputStream() throws IOException {
        return base.createInputStream();
    }

    public Chars toURI() {
        return base.toURI();
    }

    public Chars toChars() {
        return new Chars("AR archive : "+getName());
    }

    // WRITING OPERATIONS NOT SUPPORTED YET ////////////////////////////////////

    public boolean createContainer() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean createLeaf() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public ByteOutputStream createOutputStream() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Logger getLogger() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLogger(Logger logger) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getInput() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void dispose() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Resource find(Chars id) throws StoreException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
