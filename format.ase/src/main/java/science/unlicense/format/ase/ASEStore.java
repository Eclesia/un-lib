

package science.unlicense.format.ase;

import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.model3d.api.AbstractModel3DStore;

/**
 *
 * @author Johann Sorel
 */
public class ASEStore extends AbstractModel3DStore{

    public ASEStore(Object input) {
        super(ASEFormat.INSTANCE,input);
    }

    public Collection getElements() {
        throw new UnimplementedException("Not supported yet.");
    }

}
