
package science.unlicense.format.ase;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * Docs :
 * http://wiki.beyondunreal.com/Legacy:ASE_File_Format
 *
 * @author Johann Sorel
 */
public class ASEFormat extends AbstractModel3DFormat{

    public static final ASEFormat INSTANCE = new ASEFormat();

    private ASEFormat() {
        super(new Chars("ASE"));
        shortName = new Chars("ASE");
        longName = new Chars("ASCII Scene Exporter");
        extensions.add(new Chars("ase"));
        signatures.add(ASEConstants.SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new ASEStore(input);
    }

}
