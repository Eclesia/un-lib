
package science.unlicense.code.wasm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class FunctionSection extends Section {

    public int[] types;

    @Override
    public void read(DataInputStream ds) throws IOException {
        types = new int[(int) ds.readVarLengthUInt()];
        for (int i=0;i<types.length;i++) types[i] = (int) ds.readVarLengthUInt();
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeVarLengthUInt(types.length);
        for (int i=0;i<types.length;i++) ds.writeVarLengthUInt(types[i]);
    }

}
