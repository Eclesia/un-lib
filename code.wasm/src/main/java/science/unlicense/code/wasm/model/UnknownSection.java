
package science.unlicense.code.wasm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class UnknownSection extends Section {

    @Override
    public void read(DataInputStream ds) throws IOException {
        ds.skipFully(payload_len);
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.skipFully(payload_len);
    }

}
