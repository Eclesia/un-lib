
package science.unlicense.code.wasm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ResizableLimits {

    public int initial;
    public Integer maximum;

    public void read(DataInputStream ds) throws IOException {
        boolean hasMax = ds.readVarLengthUInt() != 0;
        initial = (int) ds.readVarLengthUInt();
        if (hasMax) maximum = (int) ds.readVarLengthUInt();
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeVarLengthUInt(maximum==null ? 0 : 1);
        ds.writeVarLengthUInt(initial);
        if (maximum!=null) ds.writeVarLengthUInt(maximum);
    }

}
