
package science.unlicense.code.wasm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class TableType {

    public byte element_type;
    public ResizableLimits limits = new ResizableLimits();

    public void read(DataInputStream ds) throws IOException {
        element_type = ds.readByte();
        limits.read(ds);
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeByte(element_type);
        limits.write(ds);
    }

}
