
package science.unlicense.code.wasm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class MemoryType {

    public ResizableLimits limits = new ResizableLimits();

    public void read(DataInputStream ds) throws IOException {
        limits.read(ds);
    }

    public void write(DataOutputStream ds) throws IOException {
        limits.write(ds);
    }

}
