
package science.unlicense.code.wasm.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public class GlobalSection extends Section {

    public Entry[] entries;

    @Override
    public void read(DataInputStream ds) throws IOException {
        entries = new Entry[(int) ds.readVarLengthUInt()];
        for (int i=0;i<entries.length;i++) {
            entries[i] = new Entry();
            entries[i].read(ds);
        }
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeVarLengthUInt(entries.length);
        for (int i=0;i<entries.length;i++) {
            entries[i].write(ds);
        }
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars(this.getClass().getSimpleName()), entries);
    }

    public static class Entry extends CObject{

        public GlobalType type;
        public Operands init;

        public void read(DataInputStream ds) throws IOException {
            type = new GlobalType();
            type.read(ds);
            init = new Operands();
            init.read(ds);
        }

        public void write(DataOutputStream ds) throws IOException {
            type.write(ds);
            init.write(ds);
        }

        @Override
        public Chars toChars() {
            final CharBuffer cb = new CharBuffer();
            cb.append(type.toChars());
            cb.append('\n');
            cb.append(init.toChars());
            return cb.toChars();
        }

    }

}
