
package science.unlicense.code.wasm;

import science.unlicense.code.api.CodeFileReader;
import science.unlicense.code.api.CodeFormat;
import science.unlicense.code.api.CodeProducer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * Resources :
 * http://webassembly.org/docs/binary-encoding/#high-level-structure
 *
 * @author Johann Sorel
 */
public class WasmFormat extends DefaultFormat implements CodeFormat {

    public static final WasmFormat INSTANCE = new WasmFormat();

    private WasmFormat() {
        super(new Chars("wasm"));
        shortName = new Chars("WASM");
        longName = new Chars("Web assembly");
        extensions.add(new Chars("wasm"));
        signatures.add(WasmConstants.SIGNATURE);
    }

    @Override
    public CodeFileReader createReader() {
        throw new UnimplementedException();
    }

    @Override
    public CodeProducer createProducer() {
        throw new UnimplementedException();
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
