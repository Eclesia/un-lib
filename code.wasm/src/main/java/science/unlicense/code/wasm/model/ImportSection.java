
package science.unlicense.code.wasm.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public class ImportSection extends Section {

    public Entry[] entries;

    @Override
    public void read(DataInputStream ds) throws IOException {
        final int count = (int) ds.readVarLengthUInt();
        entries = new Entry[count];
        for (int i=0;i<count;i++) {
            entries[i] = new Entry();
            entries[i].read(ds);
        }
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeVarLengthUInt(entries.length);
        for (int i=0;i<entries.length;i++) {
            entries[i].write(ds);
        }
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars(this.getClass().getSimpleName()), entries);
    }

    public static class Entry extends CObject {

        public Chars module_str;
        public Chars field_str;
        public byte kind;
        public Object type;

        public void read(DataInputStream ds) throws IOException {
            module_str = new Chars(ds.readBytes((int) ds.readVarLengthUInt()));
            field_str = new Chars(ds.readBytes((int) ds.readVarLengthUInt()));
            kind = ds.readByte();
            if (kind==0) {
                //function
                type = ds.readVarLengthUInt();
            } else if (kind==1) {
                //Table
                type = new TableType();
                ((TableType) type).read(ds);
            } else if (kind==2) {
                //Memory
                type = new MemoryType();
                ((MemoryType) type).read(ds);
            } else if (kind==3) {
                //Global
                type = new GlobalType();
                ((GlobalType) type).read(ds);
            }
        }

        public void write(DataOutputStream ds) throws IOException {
            final byte[] moduleBytes = module_str.toBytes(CharEncodings.UTF_8);
            final byte[] fieldBytes = field_str.toBytes(CharEncodings.UTF_8);
            ds.writeVarLengthUInt(moduleBytes.length);
            ds.write(moduleBytes);
            ds.writeVarLengthUInt(fieldBytes.length);
            ds.write(fieldBytes);
            ds.writeByte(kind);
            if (kind==0) {
                //function
                ds.writeVarLengthUInt((Long) type);
            } else if (kind==1) {
                //Table
                ((TableType) type).write(ds);
            } else if (kind==2) {
                //Memory
                ((MemoryType) type).write(ds);
            } else if (kind==3) {
                //Global
                ((GlobalType) type).write(ds);
            }
        }

        @Override
        public Chars toChars() {
            return module_str.concat(' ').concat(field_str);
        }

    }

}
