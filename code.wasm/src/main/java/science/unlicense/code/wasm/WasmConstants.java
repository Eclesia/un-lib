
package science.unlicense.code.wasm;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class WasmConstants {

    public static final byte[] SIGNATURE = new byte[]{0,'a','s','m'};

    //Languages types
    public static final byte TYPE_I32         = 0x7F;
    public static final byte TYPE_I64         = 0x7E;
    public static final byte TYPE_F32         = 0x7D;
    public static final byte TYPE_F64         = 0x7C;
    public static final byte TYPE_ANYFUNC     = 0x70;
    public static final byte TYPE_FUNC        = 0x60;
    public static final byte TYPE_EMPTY_BLOCK = 0x40;


    //Flow operators
    public static final byte OP_UNREACHABLE = 0x00;
    public static final byte OP_NOP         = 0x01;
    public static final byte OP_BLOCK       = 0x02;
    public static final byte OP_LOOP        = 0x03;
    public static final byte OP_IF          = 0x04;
    public static final byte OP_ELSE        = 0x05;
    public static final byte OP_END         = 0x0b;
    public static final byte OP_BR          = 0x0c;
    public static final byte OP_BR_IF       = 0x0d;
    public static final byte OP_BR_TABLE    = 0x0e;
    public static final byte OP_RETURN      = 0x0f;

    //Call operators
    public static final byte OP_CALL            = 0x10;
    public static final byte OP_CALL_INDIRECT   = 0x11;

    //Parametric operators
    public static final byte OP_DROP    = 0x1A;
    public static final byte OP_SELECT  = 0x1B;

    //Variable access
    public static final byte OP_GET_LOCAL   = (byte) 0x20;
    public static final byte OP_SET_LOCAL   = (byte) 0x21;
    public static final byte OP_TEE_LOCAL   = (byte) 0x22;
    public static final byte OP_GET_GLOBAL  = (byte) 0x23;
    public static final byte OP_SET_GLOBAL  = (byte) 0x24;

    //Memory related operators
    public static final byte OP_I32_LOAD        = (byte) 0x28;
    public static final byte OP_I64_LOAD        = (byte) 0x29;
    public static final byte OP_F32_LOAD        = (byte) 0x2A;
    public static final byte OP_F64_LOAD        = (byte) 0x2B;
    public static final byte OP_I32_LOAD8_S     = (byte) 0x2C;
    public static final byte OP_I32_LOAD8_U     = (byte) 0x2D;
    public static final byte OP_I32_LOAD16_S    = (byte) 0x2E;
    public static final byte OP_I32_LOAD16_U    = (byte) 0x2F;
    public static final byte OP_I64_LOAD8_S     = (byte) 0x30;
    public static final byte OP_I64_LOAD8_U     = (byte) 0x31;
    public static final byte OP_I64_LOAD16_S    = (byte) 0x32;
    public static final byte OP_I64_LOAD16_U    = (byte) 0x33;
    public static final byte OP_I64_LOAD32_S    = (byte) 0x34;
    public static final byte OP_I64_LOAD32_U    = (byte) 0x35;
    public static final byte OP_I32_STORE       = (byte) 0x36;
    public static final byte OP_I64_STORE       = (byte) 0x37;
    public static final byte OP_F32_STORE       = (byte) 0x38;
    public static final byte OP_F64_STORE       = (byte) 0x39;
    public static final byte OP_I32_STORE8      = (byte) 0x3A;
    public static final byte OP_I32_STORE16     = (byte) 0x3B;
    public static final byte OP_I64_STORE8      = (byte) 0x3C;
    public static final byte OP_I64_STORE16     = (byte) 0x3D;
    public static final byte OP_I64_STORE32     = (byte) 0x3E;
    public static final byte OP_CURRENT_MEMORY  = (byte) 0x3F;
    public static final byte OP_GROW_MEMORY     = (byte) 0x40;

    //Constants
    public static final byte OP_I32_CONST  = (byte) 0x41;
    public static final byte OP_I64_CONST  = (byte) 0x42;
    public static final byte OP_F32_CONST  = (byte) 0x43;
    public static final byte OP_F64_CONST  = (byte) 0x44;

    //Comparison operators
    public static final byte OP_I32_EQZ   = (byte) 0x45;
    public static final byte OP_I32_EQ    = (byte) 0x46;
    public static final byte OP_I32_NE    = (byte) 0x47;
    public static final byte OP_I32_LT_S  = (byte) 0x48;
    public static final byte OP_I32_LT_U  = (byte) 0x49;
    public static final byte OP_I32_GT_S  = (byte) 0x4A;
    public static final byte OP_I32_GT_U  = (byte) 0x4B;
    public static final byte OP_I32_LE_S  = (byte) 0x4C;
    public static final byte OP_I32_LE_U  = (byte) 0x4D;
    public static final byte OP_I32_GE_S  = (byte) 0x4E;
    public static final byte OP_I32_GE_U  = (byte) 0x4F;
    public static final byte OP_I64_EQZ   = (byte) 0x50;
    public static final byte OP_I64_EQ    = (byte) 0x51;
    public static final byte OP_I64_NE    = (byte) 0x52;
    public static final byte OP_I64_LT_S  = (byte) 0x53;
    public static final byte OP_I64_LT_U  = (byte) 0x54;
    public static final byte OP_I64_GT_S  = (byte) 0x55;
    public static final byte OP_I64_GT_U  = (byte) 0x56;
    public static final byte OP_I64_LE_S  = (byte) 0x57;
    public static final byte OP_I64_LE_U  = (byte) 0x58;
    public static final byte OP_I64_GE_S  = (byte) 0x59;
    public static final byte OP_I64_GE_U  = (byte) 0x5A;
    public static final byte OP_F32_EQ    = (byte) 0x5B;
    public static final byte OP_F32_NE    = (byte) 0x5C;
    public static final byte OP_F32_LT    = (byte) 0x5D;
    public static final byte OP_F32_GT    = (byte) 0x5E;
    public static final byte OP_F32_LE    = (byte) 0x5F;
    public static final byte OP_F32_GE    = (byte) 0x60;
    public static final byte OP_F64_EQ    = (byte) 0x61;
    public static final byte OP_F64_NE    = (byte) 0x62;
    public static final byte OP_F64_LT    = (byte) 0x63;
    public static final byte OP_F64_GT    = (byte) 0x64;
    public static final byte OP_F64_LE    = (byte) 0x65;
    public static final byte OP_F64_GE    = (byte) 0x66;

    //Numeric operators
    public static final byte OP_I32_CLZ         = (byte) 0x67;
    public static final byte OP_I32_CTZ         = (byte) 0x68;
    public static final byte OP_I32_POPCNT      = (byte) 0x69;
    public static final byte OP_I32_ADD         = (byte) 0x6A;
    public static final byte OP_I32_SUB         = (byte) 0x6B;
    public static final byte OP_I32_MUL         = (byte) 0x6C;
    public static final byte OP_I32_DIV_S       = (byte) 0x6D;
    public static final byte OP_I32_DIV_U       = (byte) 0x6E;
    public static final byte OP_I32_REM_S       = (byte) 0x6F;
    public static final byte OP_I32_REM_U       = (byte) 0x70;
    public static final byte OP_I32_AND         = (byte) 0x71;
    public static final byte OP_I32_OR          = (byte) 0x72;
    public static final byte OP_I32_XOR         = (byte) 0x73;
    public static final byte OP_I32_SHL         = (byte) 0x74;
    public static final byte OP_I32_SHR_S       = (byte) 0x75;
    public static final byte OP_I32_SHR_U       = (byte) 0x76;
    public static final byte OP_I32_ROTL        = (byte) 0x77;
    public static final byte OP_I32_ROTR        = (byte) 0x78;
    public static final byte OP_I64_CLZ         = (byte) 0x79;
    public static final byte OP_I64_CTZ         = (byte) 0x7A;
    public static final byte OP_I64_POPCNT      = (byte) 0x7B;
    public static final byte OP_I64_ADD         = (byte) 0x7C;
    public static final byte OP_I64_SUB         = (byte) 0x7D;
    public static final byte OP_I64_MUL         = (byte) 0x7E;
    public static final byte OP_I64_DIV_S       = (byte) 0x7F;
    public static final byte OP_I64_DIV_U       = (byte) 0x80;
    public static final byte OP_I64_REM_S       = (byte) 0x81;
    public static final byte OP_I64_REM_U       = (byte) 0x82;
    public static final byte OP_I64_AND         = (byte) 0x83;
    public static final byte OP_I64_OR          = (byte) 0x84;
    public static final byte OP_I64_XOR         = (byte) 0x85;
    public static final byte OP_I64_SHL         = (byte) 0x86;
    public static final byte OP_I64_SHR_S       = (byte) 0x87;
    public static final byte OP_I64_SHR_U       = (byte) 0x88;
    public static final byte OP_I64_ROTL        = (byte) 0x89;
    public static final byte OP_I64_ROTR        = (byte) 0x8A;
    public static final byte OP_F32_ABS         = (byte) 0x8B;
    public static final byte OP_F32_NEG         = (byte) 0x8C;
    public static final byte OP_F32_CEIL        = (byte) 0x8D;
    public static final byte OP_F32_FLOOR       = (byte) 0x8E;
    public static final byte OP_F32_TRUNC       = (byte) 0x8F;
    public static final byte OP_F32_NEAREST     = (byte) 0x90;
    public static final byte OP_F32_SQRT        = (byte) 0x91;
    public static final byte OP_F32_ADD         = (byte) 0x92;
    public static final byte OP_F32_SUB         = (byte) 0x93;
    public static final byte OP_F32_MUL         = (byte) 0x94;
    public static final byte OP_F32_DIV         = (byte) 0x95;
    public static final byte OP_F32_MIN         = (byte) 0x96;
    public static final byte OP_F32_MAX         = (byte) 0x97;
    public static final byte OP_F32_COPYSIGN    = (byte) 0x98;
    public static final byte OP_F64_ABS         = (byte) 0x99;
    public static final byte OP_F64_NEG         = (byte) 0x9A;
    public static final byte OP_F64_CEIL        = (byte) 0x9B;
    public static final byte OP_F64_FLOOR       = (byte) 0x9C;
    public static final byte OP_F64_TRUNC       = (byte) 0x9D;
    public static final byte OP_F64_NEAREST     = (byte) 0x9E;
    public static final byte OP_F64_SQRT        = (byte) 0x9F;
    public static final byte OP_F64_ADD         = (byte) 0xA0;
    public static final byte OP_F64_SUB         = (byte) 0xA1;
    public static final byte OP_F64_MUL         = (byte) 0xA2;
    public static final byte OP_F64_DIV         = (byte) 0xA3;
    public static final byte OP_F64_MIN         = (byte) 0xA4;
    public static final byte OP_F64_MAX         = (byte) 0xA5;
    public static final byte OP_F64_COPYSIGN    = (byte) 0xA6;

    //Conversions
    public static final byte OP_I32_WRAP_I64        =(byte) 0xA7;
    public static final byte OP_I32_TRUNC_S_F32     =(byte) 0xA8;
    public static final byte OP_I32_TRUNC_U_F32     =(byte) 0xA9;
    public static final byte OP_I32_TRUNC_S_F64     =(byte) 0xAA;
    public static final byte OP_I32_TRUNC_U_F64     =(byte) 0xAB;
    public static final byte OP_I64_EXTEND_S_I32    =(byte) 0xAC;
    public static final byte OP_I64_EXTEND_U_I32    =(byte) 0xAD;
    public static final byte OP_I64_TRUNC_S_F32     =(byte) 0xAE;
    public static final byte OP_I64_TRUNC_U_F32     =(byte) 0xAF;
    public static final byte OP_I64_TRUNC_S_F64     =(byte) 0xB0;
    public static final byte OP_I64_TRUNC_U_F64     =(byte) 0xB1;
    public static final byte OP_F32_CONVERT_S_I32   =(byte) 0xB2;
    public static final byte OP_F32_CONVERT_U_I32   =(byte) 0xB3;
    public static final byte OP_F32_CONVERT_S_I64   =(byte) 0xB4;
    public static final byte OP_F32_CONVERT_U_I64   =(byte) 0xB5;
    public static final byte OP_F32_DEMOTE_F64      =(byte) 0xB6;
    public static final byte OP_F64_CONVERT_S_I32   =(byte) 0xB7;
    public static final byte OP_F64_CONVERT_U_I32   =(byte) 0xB8;
    public static final byte OP_F64_CONVERT_S_I64   =(byte) 0xB9;
    public static final byte OP_F64_CONVERT_U_I64   =(byte) 0xBA;
    public static final byte OP_F64_PROMOTE_F32     =(byte) 0xBB;

    //Reinterpretations
    public static final byte OP_I32_REINTERPRET_F32 = (byte) 0xBC;
    public static final byte OP_I64_REINTERPRET_F64 = (byte) 0xBD;
    public static final byte OP_F32_REINTERPRET_I32 = (byte) 0xBE;
    public static final byte OP_F64_REINTERPRET_I64 = (byte) 0xBF;


    private WasmConstants() {}



    private static final Chars I32       = Chars.constant("I32");
    private static final Chars I64       = Chars.constant("I64");
    private static final Chars F32       = Chars.constant("F32");
    private static final Chars F64       = Chars.constant("F64");
    private static final Chars ANYFUNC   = Chars.constant("ANYFUNC");
    private static final Chars FUNC      = Chars.constant("FUNC");
    private static final Chars BE        = Chars.constant("BLOCK");

    private static final Chars FUNCTION = Chars.constant("Function");
    private static final Chars TABLE    = Chars.constant("Table");
    private static final Chars MEMORY   = Chars.constant("Memory");
    private static final Chars GLOBAL   = Chars.constant("Global");

    public static Chars typeName(Byte code) {
        if (code==null) {
            return Chars.EMPTY;
        }
        switch(code) {
            case TYPE_I32 : return I32;
            case TYPE_I64 : return I64;
            case TYPE_F32 : return F32;
            case TYPE_F64 : return F64;
            case TYPE_ANYFUNC : return ANYFUNC;
            case TYPE_FUNC : return FUNC;
            case TYPE_EMPTY_BLOCK : return BE;
        }
        return Chars.EMPTY;
    }

    public static Chars externalKindName(Byte code) {
        if (code==null) {
            return Chars.EMPTY;
        }
        switch(code) {
            case 0 : return FUNCTION;
            case 1 : return TABLE;
            case 2 : return MEMORY;
            case 3 : return GLOBAL;
        }
        return Chars.EMPTY;
    }



}
