
package science.unlicense.code.wasm.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class Section extends CObject {

    public int id;
    public int payload_len;

    public abstract void read(DataInputStream ds) throws IOException;

    public abstract void write(DataOutputStream ds) throws IOException;

    @Override
    public Chars toChars() {
        return new Chars(this.getClass().getSimpleName());
    }

}
