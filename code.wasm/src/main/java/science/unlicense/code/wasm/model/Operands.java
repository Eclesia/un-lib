
package science.unlicense.code.wasm.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.code.wasm.WasmConstants;

/**
 *
 * @author Johann Sorel
 */
public class Operands extends CObject{

    public final Sequence ops = new ArraySequence();

    public void read(DataInputStream ds) throws IOException {

        int depth = 0;
        for (;;) {
            final Operand op = new Operand();
            op.read(ds);
            ops.add(op);

            if (op.code == WasmConstants.OP_BLOCK ||
                op.code == WasmConstants.OP_LOOP ||
                op.code == WasmConstants.OP_IF) {
                depth++;
            }
            if (op.code == WasmConstants.OP_END) {
                depth--;
                if (depth<0) break;
            }
        }

    }

    public void write(DataOutputStream ds) throws IOException {
        for (int i=0,n=ops.getSize();i<n;i++) {
            final Operand op = (Operand) ops.get(i);
            op.write(ds);
        }
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        int depth = 0;
        for (int i=0,n=ops.getSize();i<n;i++) {
            if (i!=0) cb.append('\n');

            final Operand op = (Operand) ops.get(i);
            if (op.code == WasmConstants.OP_BLOCK ||
                op.code == WasmConstants.OP_LOOP ||
                op.code == WasmConstants.OP_IF) {

                for (int k=0;k<depth;k++) cb.append("  ");
                depth++;
                cb.append(CObjects.toChars(op));
            } else if (op.code == WasmConstants.OP_ELSE) {
                depth--;
                for (int k=0;k<depth;k++) cb.append("  ");
                depth++;
                cb.append(CObjects.toChars(op));
            } else if (op.code == WasmConstants.OP_END) {
                depth--;
                for (int k=0;k<depth;k++) cb.append("  ");
                cb.append(CObjects.toChars(op));
            } else {
                for (int k=0;k<depth;k++) cb.append("  ");
                cb.append(CObjects.toChars(op));
            }

        }
        return cb.toChars();
    }

}
