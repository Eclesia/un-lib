
package science.unlicense.code.wasm.model;

import science.unlicense.code.wasm.WasmConstants;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class Module extends CObject{

    public int version;
    public final Sequence sections = new ArraySequence();

    public void read(DataInputStream ds) throws IOException {
        if (!Arrays.equals(WasmConstants.SIGNATURE,ds.readBytes(4))) {
            throw new IOException(ds, "Unvalid signature");
        }
        version = ds.readInt();

        for (int b=ds.read();b>=0;b=ds.read()) {
            final Section section;
            switch(b) {
                case  0 : section = new CustomSection(); break;
                case  1 : section = new TypeSection(); break;
                case  2 : section = new ImportSection(); break;
                case  3 : section = new FunctionSection(); break;
                case  4 : section = new TableSection(); break;
                case  5 : section = new MemorySection(); break;
                case  6 : section = new GlobalSection(); break;
                case  7 : section = new ExportSection(); break;
                case  8 : section = new StartSection(); break;
                case  9 : section = new ElementSection(); break;
                case 10 : section = new CodeSection(); break;
                case 11 : section = new DataSection(); break;
                default : throw new IOException(ds, "Unexpected section "+b);
            }
            sections.add(section);
            section.id = b;
            section.payload_len = (int) ds.readVarLengthUInt();
            section.read(ds);
        }

    }

    public void write(DataOutputStream ds) throws IOException {
        ds.write(WasmConstants.SIGNATURE);
        ds.writeInt(version);

        for (int i=0,n=sections.getSize();i<n;i++) {
            final Section section = (Section) sections.get(i);
            ds.write((byte) section.id);

            final ArrayOutputStream out = new ArrayOutputStream();
            final DataOutputStream ods = new DataOutputStream(out,Endianness.LITTLE_ENDIAN);
            section.write(ods);
            ods.flush();
            final byte[] data = out.getBuffer().toArrayByte();
            ds.writeVarLengthUInt(data.length); //payload_len
            ds.write(data);
        }
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("Module"), sections);
    }

}
