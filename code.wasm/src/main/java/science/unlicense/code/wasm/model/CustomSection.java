
package science.unlicense.code.wasm.model;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class CustomSection extends Section {

    public Chars name;
    public byte[] skipped;

    @Override
    public final void read(DataInputStream ds) throws IOException {
        long offset = ds.getByteOffset();
        name = new Chars(ds.readBytes((int) ds.readVarLengthUInt()));
        skipped = ds.readBytes((int) (payload_len - ds.getByteOffset() - offset));
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        final byte[] namebytes = name.toBytes(CharEncodings.UTF_8);
        ds.writeVarLengthUInt(namebytes.length);
        ds.write(namebytes);
        ds.write(skipped);
    }

}
