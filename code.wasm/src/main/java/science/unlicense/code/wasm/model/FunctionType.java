
package science.unlicense.code.wasm.model;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.code.wasm.WasmConstants;

/**
 *
 * @author Johann Sorel
 */
public class FunctionType extends CObject {

    public byte form;
    public byte[] param_types;
    public Byte return_type;

    public void read(DataInputStream ds) throws IOException {
        form = ds.readByte();
        final int param_count = (int) ds.readVarLengthUInt();
        param_types = ds.readBytes(param_count);
        if (ds.readVarLengthUInt()!=0) {
            return_type = ds.readByte();
        }
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeByte(form);
        ds.writeVarLengthUInt(param_types.length);
        ds.write(param_types);
        ds.writeVarLengthUInt(return_type==null ? 0 : 1);
        if (return_type!=null) {
            ds.writeByte(return_type);
        }
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(WasmConstants.typeName(form));
        cb.append('(');
        for (int i=0; i<param_types.length; i++) {
            if (i>0) cb.append(',');
            cb.append(WasmConstants.typeName(param_types[i]));
        }
        cb.append(')');
        if (return_type!=null) {
            cb.append(" -> (");
            cb.append(WasmConstants.typeName(return_type));
            cb.append(')');
        }
        return cb.toChars();
    }

}
