
package science.unlicense.code.wasm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class StartSection extends Section {

    public int index;

    @Override
    public void read(DataInputStream ds) throws IOException {
        index = (int) ds.readVarLengthUInt();
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeVarLengthUInt(index);
    }

}
