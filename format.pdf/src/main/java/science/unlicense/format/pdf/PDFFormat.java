

package science.unlicense.format.pdf;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * PDF format.
 *
 * Resource :
 * http://en.wikipedia.org/wiki/Portable_Document_Format
 *
 * @author Johann Sorel
 */
public class PDFFormat extends DefaultFormat {

    public PDFFormat() {
        super(new Chars("PDF"));
        shortName = new Chars("PDF");
        longName = new Chars("Portable Document Format");
        mimeTypes.add(new Chars("application/pdf"));
        mimeTypes.add(new Chars("application/x-pdf"));
        mimeTypes.add(new Chars("application/x-bzpdf"));
        mimeTypes.add(new Chars("application/x-gzpdf"));
        extensions.add(new Chars("pdf"));
        signatures.add(PDFConstants.SIGNATURE);
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
