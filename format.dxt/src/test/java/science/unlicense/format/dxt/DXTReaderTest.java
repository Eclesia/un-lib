
package science.unlicense.format.dxt;

import science.unlicense.format.dxt.DXTReader;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.Colors;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class DXTReaderTest {

    @Test
    public void testReadDXT1() throws IOException{

        final int r1 = Colors.toARGB(255, 255, 12, 8);
        final int r2 = Colors.toARGB(255, 255, 75, 74);
        final int r3 = Colors.toARGB(255, 255, 139, 140);
        final int r4 = Colors.toARGB(255, 255, 202, 206);

        final int bg1 = Colors.toARGB(255, 0,  57, 197);
        final int bg4 = Colors.toARGB(255, 0, 148, 107);
        final int bg5 = Colors.toARGB(  0, 0,   0,   0);

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/dxt/dxt1.dds")).createInputStream();
        final ImageReader reader = new DXTReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(4, image.getExtent().getL(0));
        Assert.assertEquals(8, image.getExtent().getL(1));

        pixelEquals(image,0,0,new int[][]{
            {r1,r1,r2,r2},
            {r1,r2,r2,r3},
            {r2,r2,r3,r3},
            {r2,r3,r3,r4},
        });

        pixelEquals(image,0,4,new int[][]{
            {bg4,bg1,bg1,bg1},
            {bg5,bg4,bg1,bg1},
            {bg5,bg5,bg4,bg1},
            {bg5,bg5,bg5,bg4},
        });

    }

    @Test
    public void testReadDXT3() throws IOException{

        final int r1 = Colors.toARGB(255, 255, 12, 8);
        final int r2 = Colors.toARGB(255, 255, 75, 74);
        final int r3 = Colors.toARGB(255, 255, 139, 140);
        final int r4 = Colors.toARGB(255, 255, 202, 206);

        final int bg1 = Colors.toARGB(238, 0,  57, 197);
        final int bg2 = Colors.toARGB(221, 0,  57, 197);
        final int bg3 = Colors.toARGB(187, 0, 117, 137);
        final int bg4 = Colors.toARGB(153, 0, 117, 137);
        final int bg41= Colors.toARGB(153, 0, 117, 137);
        final int bg5 = Colors.toARGB(119, 0, 179,  76);
        final int bg6 = Colors.toARGB(102, 0, 179,  76);
        final int bg7 = Colors.toARGB( 68, 0, 239,  16);

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/dxt/dxt3.dds")).createInputStream();

        final ImageReader reader = new DXTReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(4, image.getExtent().getL(0));
        Assert.assertEquals(8, image.getExtent().getL(1));

        pixelEquals(image,0,0,new int[][]{
            {r1,r1,r2,r2},
            {r1,r2,r2,r3},
            {r2,r2,r3,r3},
            {r2,r3,r3,r4},
        });

        pixelEquals(image,0,4,new int[][]{
            {bg4,bg3, bg2, bg1},
            {bg5,bg41,bg3, bg2},
            {bg6,bg5, bg41,bg3},
            {bg7,bg6, bg5, bg41},
        });

    }

    @Test
    public void testReadDXT5() throws IOException{

        final int r1 = Colors.toARGB(255, 255, 12, 8);
        final int r2 = Colors.toARGB(255, 255, 75, 74);
        final int r3 = Colors.toARGB(255, 255, 139, 140);
        final int r4 = Colors.toARGB(255, 255, 202, 206);

        final int bg1 = Colors.toARGB(239, 0,  57, 197);
        final int bg2 = Colors.toARGB(214, 0,  57, 197);
        final int bg3 = Colors.toARGB(189, 0, 117, 137);
        final int bg4 = Colors.toARGB(165, 0, 117, 137);
        final int bg41= Colors.toARGB(140, 0, 117, 137);
        final int bg5 = Colors.toARGB(116, 0, 179,  76);
        final int bg6 = Colors.toARGB( 91, 0, 179,  76);
        final int bg7 = Colors.toARGB( 67, 0, 239,  16);

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/dxt/dxt5.dds")).createInputStream();

        final ImageReader reader = new DXTReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(4, image.getExtent().getL(0));
        Assert.assertEquals(8, image.getExtent().getL(1));

        pixelEquals(image,0,0,new int[][]{
            {r1,r1,r2,r2},
            {r1,r2,r2,r3},
            {r2,r2,r3,r3},
            {r2,r3,r3,r4},
        });

        pixelEquals(image,0,4,new int[][]{
            {bg4,bg3, bg2, bg1},
            {bg5,bg41,bg3, bg2},
            {bg6,bg5, bg41,bg3},
            {bg7,bg6, bg5, bg41},
        });

    }

    private static void pixelEquals(Image image, int offsetx, int offsety, int[][] argb){
        for (int y=0;y<4;y++){
            for (int x=0;x<4;x++){
                final Color color = image.getColor(new Vector2i32(offsetx+x,offsety+y));
                final int candidate = color.toARGB();
                Assert.assertEquals(argb[y][x], candidate,
                        "wrong prixel at ["+x+","+y+"] expected "+
                        new ColorRGB(argb[y][x])+" but found "+new ColorRGB(candidate));
            }
        }
    }

}
