
package science.unlicense.format.dxt;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * DXT/DDS image format.
 *
 * @author Johann Sorel
 */
public class DXTFormat extends AbstractImageFormat {

    public static final DXTFormat INSTANCE = new DXTFormat();

    private DXTFormat() {
        super(new Chars("dxt"));
        shortName = new Chars("S3TC/DXT");
        longName = new Chars("S3 Texture Compression");
        mimeTypes.add(new Chars("image/dxt"));
        extensions.add(new Chars("dxt"));
        extensions.add(new Chars("dds"));
        signatures.add(DXTConstants.SIGNATURE);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new DXTStore(this, source);
    }

}
