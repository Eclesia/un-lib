
package science.unlicense.format.dxt;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class DXTHeader {

    /** unsigned integer : structure size, must be 124 */
    public int size;
    /** Flags to indicate which members contain valid data.*/
    public int flags;
    /** image height */
    public int height;
    /** image width */
    public int width;
    public int pitchOrLinearSize;
    public int depth;
    /** number of mipmap */
    public int mipMapCount;
    /** unused reserver, size 11 */
    public int[] reserved1;
    public DXTPixelFormat pixelFormat;
    public int caps1;
    public int caps2;
    /** unused, reserved */
    public int caps3;
    /** unused, reserved */
    public int caps4;
    /** unused, reserved */
    public int reserved2;

    public DXT10Header header10;

    /**
     *
     * @param mask one of DXTConstants.DDSD_X;
     * @return true if flag is set
     */
    public boolean isFlagSet(int mask){
        return (flags & mask)  != 0;
    }

    public void read(DataInputStream ds) throws IOException{
        size = ds.readInt();
        flags = ds.readInt();
        height = ds.readInt();
        width = ds.readInt();
        pitchOrLinearSize = ds.readInt();
        depth = ds.readInt();
        mipMapCount = ds.readInt();
        reserved1 = ds.readInt(11);
        pixelFormat = new DXTPixelFormat();
        pixelFormat.size = ds.readInt();
        pixelFormat.flags = ds.readInt();

        final byte[] bytes = ds.readBytes(4);
        if (bytes[0] == 0 && bytes[1] == 0 && bytes[2] == 0 && bytes[3] == 0) {
            pixelFormat.fourCC = Chars.EMPTY;
        } else {
            pixelFormat.fourCC = new Chars(bytes);
        }
        pixelFormat.RGBBitCount = ds.readInt();
        pixelFormat.RBitMask = ds.readInt();
        pixelFormat.GBitMask = ds.readInt();
        pixelFormat.BBitMask = ds.readInt();
        pixelFormat.ABitMask = ds.readInt();
        caps1 = ds.readInt();
        caps2 = ds.readInt();
        caps3 = ds.readInt();
        caps4 = ds.readInt();
        reserved2 = ds.readInt();

//        if (header.pixelFormat.fourCC.toBytes()[0] == 0){
//            //TODO fourcc is not set, detec type from parameters
//            header.pixelFormat.fourCC = DXTConstants.D3DFMT_DXT5;
//        }

        //check if we have a DXT10 header
        header10 = null;
        if (  pixelFormat.flags == DXTConstants.DDPF_FOURCC
           && pixelFormat.fourCC.equals(DXTConstants.D3DFMT_DX10)){
            header10 = new DXT10Header();
            header10.dxgiFormat = ds.readInt();
            header10.resourceDimension = ds.readInt();
            header10.miscFlag = ds.readUInt();
            header10.arraySize = ds.readUInt();
            header10.reserved = ds.readUInt();
        }
    }

}
