
package science.unlicense.format.dxt;

import science.unlicense.image.api.DefaultImageReadParameters;

/**
 *
 * @author Johann Sorel
 */
public class DXTReadParameters extends DefaultImageReadParameters{

    private DXTHeader header;

    public DXTReadParameters() {
    }

    public void setHeader(DXTHeader header) {
        this.header = header;
    }

    public DXTHeader getHeader() {
        return header;
    }

}
