
package science.unlicense.format.dxt;

/**
 *
 * @author Johann Sorel
 */
public class DXT10Header {

    public int dxgiFormat;
    public int resourceDimension;
    public long miscFlag;
    public long arraySize;
    public long reserved;

}
