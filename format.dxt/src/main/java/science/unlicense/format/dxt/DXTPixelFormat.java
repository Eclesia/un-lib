
package science.unlicense.format.dxt;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DXTPixelFormat {

    public int size;
    public int flags;
    public Chars fourCC;
    public int RGBBitCount;
    public int RBitMask;
    public int GBitMask;
    public int BBitMask;
    public int ABitMask;
}
