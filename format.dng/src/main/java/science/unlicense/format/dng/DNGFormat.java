
package science.unlicense.format.dng;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * http://en.wikipedia.org/wiki/Digital_Negative
 *
 * @author Johann Sorel
 */
public class DNGFormat extends AbstractImageFormat {

    public static final DNGFormat INSTANCE = new DNGFormat();

    private DNGFormat() {
        super(new Chars("dng"));
        shortName = new Chars("DNG");
        longName = new Chars("Digital Negative");
        mimeTypes.add(new Chars("dng"));
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new DNGStore(this, source);
    }

}
