
package science.unlicense.format.dng;

import science.unlicense.format.dng.DNGFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class DNGFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final ImageFormat[] formats = Images.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof DNGFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("Image format not found.");
    }

}
