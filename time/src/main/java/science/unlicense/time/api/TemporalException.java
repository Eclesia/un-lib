
package science.unlicense.time.api;

/**
 *
 * @author Johann Sorel
 */
public class TemporalException extends RuntimeException {

    public TemporalException() {
    }

    public TemporalException(String s) {
        super(s);
    }

    public TemporalException(Throwable cause) {
        super(cause);
    }

    public TemporalException(String message, Throwable cause) {
        super(message, cause);
    }

}
