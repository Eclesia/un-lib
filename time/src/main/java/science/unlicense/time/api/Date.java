
package science.unlicense.time.api;

/**
 * A Date is a short period of time defined in calendar divisions.
 *
 * @author Johann Sorel
 */
public interface Date extends Period {

    /**
     * Date calendar system.
     *
     * @return calendar, never null
     */
    Calendar getCalendar();

    /**
     *
     * @param division
     * @param defaultValue if the component is not defined, returns this value
     * @return component value or default value if undefined
     */
    Object getValue(Division division, Object defaultValue);

}
