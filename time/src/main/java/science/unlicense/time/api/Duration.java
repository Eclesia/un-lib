
package science.unlicense.time.api;

import science.unlicense.math.api.unitold.Unit;
import science.unlicense.math.impl.number.LargeDecimal;

/**
 * A duration is a measured elapse of time.
 *
 * @author Johann Sorel
 */
public interface Duration {

    /**
     * Convert duration to a single unit value.
     *
     * @param unit temporal unit, not null
     * @return duration length in given unit
     */
    LargeDecimal getLength(Unit unit);

}
