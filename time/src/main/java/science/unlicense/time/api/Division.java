
package science.unlicense.time.api;

import science.unlicense.common.api.character.CharArray;

/**
 * Calendars divide time in repetitive periods, those periods often have a fixed
 * duration.
 *
 * @author Johnan Sorel
 */
public interface Division {

    /**
     * Returns the division common name.
     *
     * @return division name, not null
     */
    CharArray getName();

}
