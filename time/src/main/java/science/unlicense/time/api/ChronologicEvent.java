
package science.unlicense.time.api;

/**
 * An identified event in the time.
 *
 * @author Johann Sorel
 */
public interface ChronologicEvent {

    /**
     * Reference event.
     *
     * @return origin event
     */
    ChronologicEvent getAnchor();

    /**
     * Duration from anchor event to this one.
     *
     * @return duration, null only if anchor is null.
     */
    Duration getAnchorDistance();

    /**
     * Create a new chronologic event which anchor is the given one.
     *
     * @param anchor new anchor, not null
     * @return ChronologicEvent, not null
     */
    ChronologicEvent toAbsolute(ChronologicEvent anchor);

}
