
package science.unlicense.time.impl;

/**
 *
 * @author Johann Sorel
 */
public class PetrinianUtils {

    private PetrinianUtils(){}

    public static boolean isLeapYear(long prolepticYear) {
        return (prolepticYear & 3) == 0;
    }

}
