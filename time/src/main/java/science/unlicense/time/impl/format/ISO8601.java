
package science.unlicense.time.impl.format;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;


/**
 * Read and write temporal objects in ISO-8601 format.
 *
 * @author Johann Sorel
 * @author Samuel Andrés
 */
public final class ISO8601 {

    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    private ISO8601(){}

    public static Chars toChars(long time){
        //todo
        return new Chars(df.format(new Date(time)));
    }

    /**
     *
     * @param yearValue
     * @param monthValue
     * @param dayValue
     * @return
     */
    public static Chars toChars(final int yearValue, final int monthValue, final int dayValue) {
        final int absYear = Math.abs(yearValue);
        final CharBuffer buf = new CharBuffer();
        if (absYear < 1000) {
            buf.appendNumber(yearValue);
        } else {
            if (yearValue > 9999) {
                buf.append('+');
            }
            buf.append(yearValue);
        }
        return buf.append(monthValue < 10 ? "-0" : "-")
            .append(monthValue)
            .append(dayValue < 10 ? "-0" : "-")
            .append(dayValue)
            .toChars();
    }

}
