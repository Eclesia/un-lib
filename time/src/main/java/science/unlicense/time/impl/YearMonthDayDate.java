package science.unlicense.time.impl;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.time.api.TemporalException;

/**
 *
 * @author Samuel Andrés
 */
public class YearMonthDayDate {

    protected final int year;
    protected final short month;
    protected final short day;

    public YearMonthDayDate(int year, int month, int dayOfMonth) {
        this.year = year;
        this.month = (short) month;
        this.day = (short) dayOfMonth;
    }

    public int getYear() {
        return year;
    }

    public int getMonthValue() {
        return month;
    }

    public int getDayOfMonth() {
        return day;
    }

    public int getDayOfYear() {
        throw new UnimplementedException();
    }

    public long toEpochDay() {
        throw new UnimplementedException();
    }

}
