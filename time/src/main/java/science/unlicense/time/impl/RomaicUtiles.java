
package science.unlicense.time.impl;

/**
 *
 * @author Johann Sorel
 */
public class RomaicUtiles {

    private RomaicUtiles(){}

    public static boolean isLeapYear(long prolepticYear) {
        return (prolepticYear & 3) == 0;
    }

}
