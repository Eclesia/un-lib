package science.unlicense.time.impl;

/**
 *
 * @author Samuel Andrés
 */
public final class GregorianUtils {

    public static final int YEARS_PER_CYCLE = 400;

    /**
     * Nombre de jour dans un cycle grégorien de 400 ans dont trois des
     * premières années de siècles sur 4 ne sont pas bissextile.
     *
     * Ainsi, 1 siècle =  75 années normales    = 75*365 = 27375
     *                  + 25 années bissextiles = 25*366 =  9150
     *                  -----------------------------------------
     *                                                     36525
     *
     * Donc, 4 siècles juliens font 36525*4 = 146100 jours.
     *
     * Pour prendre en compte la réforme grégorienne, il faut retirer 3 jours à
     * ce total, ce qui donne 146097 jours.
     *
     */
    public static final int DAYS_PER_CYCLE = 146097;

    // Nombre de jours par année grégorienne en moyenne : 365,2425
    public static final float DAYS_PER_GREGORIAN_YEAR = ((float) DAYS_PER_CYCLE)/YEARS_PER_CYCLE;

    private GregorianUtils(){}


    /**
     * Jour de l'année étant donné le nombre de jours depuis l'époque de départ,
     * pour une année donnée, dans le calendrier grégorien en considérant le
     * début de l'année au 1er mars.
     *
     * On commence par appliquer la transformation julienne dans l'année
     * débutant en mars.
     *
     * Puis :
     * - on ajoute un jour tous les siècles à cause des premières années de siècles qui ne sont pas bissextiles,
     * - et on retire un jour tous 400 ans parce qu'une année de siècle sur 4 est malgré tout bissextile.
     *
     * @param zeroDay
     * @param year
     * @return
     */
    public static long dayOfYear(final long zeroDay, final long year){
        return JulianUtils.dayOfYear(zeroDay, year) + year / 100 - year / 400;
    }

    /**
     *
     * @param zeroDay
     * @return
     */
    public static YearDayDate toDayOfYear(long zeroDay){

        /*
        Nombre nul pour les nombre de jours positifs et négatif pour les nombres
        de jours négatifs. Ces derniers étant transformés en nombre de jours
        positifs pendant le premier cycle le temps du calcul, "adjust" indique
        la correction à apporter pour retrouver l'année négative véritable.
        */
        long adjust = 0;
        if (zeroDay < 0) {
            /*
             zeroDay = 0 : c'est le 01/03/0000


             Si on est avant le 01/03/0000, on fait en sorte de retomber sur un
             calcul identique que pour les valeurs de jours positives et on
             repassera en négatif après le calcul.

             Cette correction commence donc à partir de zeroDay = -1 et pour
             toutes les valeurs négatives, c'est-à-dire avant le 01/03/0000.

             ____________________________________
             cycles n° :    4     3     2     1  |

                           01/03/-300        01/03/0000              01/03/400
                               v                 v                       v
             |_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|
             adjustCycles: -4    -3    -2    -1     /     /     /     /     /
             adjust:      -1600 -1200 -800  -400    0     0     0     0     0

             adjust negative years to positive for calculation

             Commentaire à propos de adjustCycles.
             =====================================
             Il reste nul pour les années positives.
             Pour les années négatives, il commence à -1 et est décrémenté de 1
             chaque premier jour de nouveau cycle dans la direction du passé.
             Le premier jour du premier cycle des années négatives vaut -1.
             Pour effectuer le calcul de adjustCycles, il faut le ramener à 0,
             c'est-à-dire y ajouter 1 et ainsi de suite pour tous les jours
             antérieurs pour lesquels adjustCycles sera calculé.

             Commentaire à propos de adjust.
             ===============================
             adjust reste nul pour les années positives.
             Pour les années négatives, il n'intervient pas directement mais
             sera ajouté au nombre d'années final pour revenir en négatif.
            */

            long adjustCycles = (zeroDay + 1) / DAYS_PER_CYCLE - 1;
            adjust = adjustCycles * YEARS_PER_CYCLE;// Nombre d'années à retirer

            /*
             Commentaire sur le nombre de jours.
             ===================================
             De sa valeur négative, il est transformé en sa valeur positive au
             cours du premier cycle :

                                                 01/03/0000
                                                     V
                                     d               | d'
                      <----------------------------->|<->
             _|______x___|__________|_ _ _ _ _ _ _ _ |___x______|__________|____
                      <-> <-------------------------> <->
                            "-adjustCycles" cycles
            */
            zeroDay += -adjustCycles * DAYS_PER_CYCLE;
        }

        // écart approché en entre le nombre de jour en moyenne par année grégorienne réel (365,2425) et sa valeur approchée par division entière (365)


        // DAYS_PER_CYCLE : nombre de jours par cycle grégorien de 400 ans
        // zeroDay : nombre de jours écoulés depuis le 01/03/0000
        /*
        => 591 : moyen de compenser l'écart induit par l'inégalité en nombre de
        jours des années composant la période de 400 ans. Ceci garantit qu'aucun
        jour n'indique en première approximation une année inférieure à l'année
        grégorienne à laquelle il appartient. Toutefois, ceci peut avoir comme
        effet de bord que l'année peut être légèrement surestimée.

        Pour corriger cette erreur, il suffit de vérifier que le jour ne tombe
        pas avant le début de l'année.
        */
        long yearEst = (YEARS_PER_CYCLE * zeroDay + 591) / DAYS_PER_CYCLE;
        long doyEst = dayOfYear(zeroDay, yearEst);
        if (doyEst < 0) {
            // fix estimate
            yearEst--;
            doyEst = dayOfYear(zeroDay, yearEst);
        }
        yearEst += adjust;  // reset any negative year

        return new YearDayDate((int) doyEst, yearEst);
    }

    public static boolean isLeapYear(long prolepticYear) {
        return ((prolepticYear & 3) == 0) && ((prolepticYear % 100) != 0 || (prolepticYear % 400) == 0);
    }


    public static final int[] MIN = {Integer.MIN_VALUE, 1, 1};
    public static final int[] MAX = {Integer.MAX_VALUE, 12, 31};

    /**
     * Nombre de jours entre le début de l'ère chrétienne (1er janvier 0) et le
     * début de l'ère Java (1er janvier 1970).
     *
     * De 0 à 2000 = 5 cycles grégoriens de 4 siècles.
     * De 2000 à 1970, on retire 30 années de 365 jours et encore 7 jours
     * supplémentaires correspondants aux jours des 7 années bissextiles entre
     * 1970 et 2000.
     *
     * Vaut 719728 jours.
     */
    public static final long DAYS_0000_TO_1970 = (DAYS_PER_CYCLE * 5L) - (30L * 365L + 7L);

}
