
package science.unlicense.time.impl;

/**
 *
 * @author Johann Sorel
 */
public final class RangeUnit {

    public static final int DAYS = 1;
    public static final int WEEKS = 2;
    public static final int MONTHS = 3;
    public static final int YEARS = 4;

    private RangeUnit(){}

}
