
package science.unlicense.time.impl;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.time.api.Division;

/**
 * Default division implementation.
 *
 * @author Johann Sorel
 */
public class DefaultDivision implements Division {

    private final CharArray name;

    public DefaultDivision(CharArray name) {
        this.name = name;
    }

    public CharArray getName() {
        return name;
    }

}
