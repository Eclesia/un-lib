
package science.unlicense.time.impl;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.time.api.Duration;
import science.unlicense.math.api.unitold.Unit;
import science.unlicense.math.api.unitold.Units;
import science.unlicense.math.impl.number.LargeDecimal;
import science.unlicense.math.impl.number.LargeInteger;

/**
 *
 * @author Johann Sorel
 */
public class UnitDuration implements Duration{

    private final Unit unit;
    private final LargeInteger length;

    public UnitDuration(Unit unit, LargeInteger length) {
        this.unit = unit;
        this.length = length;
    }

    public Unit getUnit() {
        return unit;
    }

    public LargeInteger getLength() {
        return length;
    }

    public LargeDecimal getLength(Unit unit) {
        if (unit==this.unit){
            throw new UnimplementedException("TODO");
        }
        Transform transform = Units.getTransform(this.unit, unit);
        throw new UnimplementedException("TODO");
    }

}
