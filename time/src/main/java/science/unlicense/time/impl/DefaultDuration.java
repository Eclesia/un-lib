
package science.unlicense.time.impl;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.time.api.Calendar;
import science.unlicense.time.api.Duration;
import science.unlicense.math.api.unitold.Unit;
import science.unlicense.math.impl.number.LargeDecimal;

/**
 *
 * @author Johann Sorel
 */
public class DefaultDuration implements Duration{

    private final Calendar calendar;
    private final Dictionary components;

    public DefaultDuration(Calendar calendar, Dictionary components) {
        this.calendar = calendar;
        this.components = components;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public Dictionary getComponents() {
        return components;
    }

    public LargeDecimal getLength(Unit unit) {
        throw new UnimplementedException("Not supported yet.");
    }

}
