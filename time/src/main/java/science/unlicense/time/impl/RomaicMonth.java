package science.unlicense.time.impl;

import science.unlicense.time.api.TemporalException;


/**
 *
 * @author Samuel Andrés
 */
public enum RomaicMonth {

    /**
     * The singleton instance for the month of September with 30 days.
     * This has the numeric value of {@code 1}.
     */
    SEPTEMBER,
    /**
     * The singleton instance for the month of October with 31 days.
     * This has the numeric value of {@code 2}.
     */
    OCTOBER,
    /**
     * The singleton instance for the month of November with 30 days.
     * This has the numeric value of {@code 3}.
     */
    NOVEMBER,
    /**
     * The singleton instance for the month of December with 31 days.
     * This has the numeric value of {@code 4}.
     */
    DECEMBER,
    /**
     * The singleton instance for the month of January with 31 days.
     * This has the numeric value of {@code 5}.
     */
    JANUARY,
    /**
     * The singleton instance for the month of February with 28 days, or 29 in a leap year.
     * This has the numeric value of {@code 6}.
     */
    FEBRUARY,
    /**
     * The singleton instance for the month of March with 31 days.
     * This has the numeric value of {@code 7}.
     */
    MARCH,
    /**
     * The singleton instance for the month of April with 30 days.
     * This has the numeric value of {@code 8}.
     */
    APRIL,
    /**
     * The singleton instance for the month of May with 31 days.
     * This has the numeric value of {@code 9}.
     */
    MAY,
    /**
     * The singleton instance for the month of June with 30 days.
     * This has the numeric value of {@code 10}.
     */
    JUNE,
    /**
     * The singleton instance for the month of July with 31 days.
     * This has the numeric value of {@code 11}.
     */
    JULY,
    /**
     * The singleton instance for the month of August with 31 days.
     * This has the numeric value of {@code 12}.
     */
    AUGUST;
    /**
     * Private cache of all the constants.
     */
    private static final RomaicMonth[] ENUMS = RomaicMonth.values();


    public static RomaicMonth of(int month) {
        if (month < 1 || month > 12) {
            throw new TemporalException("Invalid value for MonthOfYear: " + month);
        }
        return ENUMS[month - 1];
    }

    public int getValue() {
        return ordinal() + 1;
    }

    public RomaicMonth plus(long months) {
        int amount = (int) (months % 12);
        return ENUMS[(ordinal() + (amount + 12)) % 12];
    }

    public RomaicMonth minus(long months) {
        return plus(-(months % 12));
    }

    public int length(boolean leapYear) {
        switch (this) {
            case FEBRUARY:
                return (leapYear ? 29 : 28);
            case APRIL:
            case JUNE:
            case SEPTEMBER:
            case NOVEMBER:
                return 30;
            default:
                return 31;
        }
    }

    public int minLength() {
        switch (this) {
            case FEBRUARY:
                return 28;
            case APRIL:
            case JUNE:
            case SEPTEMBER:
            case NOVEMBER:
                return 30;
            default:
                return 31;
        }
    }

    public int maxLength() {
        switch (this) {
            case FEBRUARY:
                return 29;
            case APRIL:
            case JUNE:
            case SEPTEMBER:
            case NOVEMBER:
                return 30;
            default:
                return 31;
        }
    }

    public int firstDayOfYear(boolean leapYear) {
        int leap = leapYear ? 1 : 0;
        switch (this) {
            case SEPTEMBER:
                return 1;
            case OCTOBER:
                return 31;
            case NOVEMBER:
                return 62;
            case DECEMBER:
                return 92;
            case JANUARY:
                return 123;
            case FEBRUARY:
                return 154;
            case MARCH:
                return 182 + leap;
            case APRIL:
                return 213 + leap;
            case MAY:
                return 243 + leap;
            case JUNE:
                return 274 + leap;
            case JULY:
                return 304 + leap;
            case AUGUST:
            default:
                return 335 + leap;
        }
    }

    public RomaicMonth firstMonthOfQuarter() {
        return ENUMS[(ordinal() / 3) * 3];
    }

}
