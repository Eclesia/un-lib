package science.unlicense.time.impl.timejvm;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.time.impl.RomaicDate;
import science.unlicense.time.impl.RomaicMonth;

/**
 *
 * @author Samuel Andrés
 */
public class RomaicDateTest {

//    @Test
//    public void testOf0() {
//        final LocalDate local = LocalDate.of(2016, Month.JANUARY, 16);
//        final RomaicDate romaic = RomaicDate.ofEpochDay(local.toEpochDay());
//        Assert.assertEquals(3, romaic.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.JANUARY, romaic.getMonth());
//        Assert.assertEquals(7524, romaic.getYear());
//
//        Assert.assertTrue(IsoChronology.INSTANCE.isLeapYear(2016));
//        Assert.assertTrue(RomaicChronology.INSTANCE.isLeapYear(7524));
//
//
//        Assert.assertTrue(IsoChronology.INSTANCE.isLeapYear(4));
//        Assert.assertTrue(RomaicChronology.INSTANCE.isLeapYear(5512));
//        Assert.assertTrue(IsoChronology.INSTANCE.isLeapYear(0));
//        Assert.assertTrue(RomaicChronology.INSTANCE.isLeapYear(5508));
//        Assert.assertTrue(IsoChronology.INSTANCE.isLeapYear(-4));
//        Assert.assertTrue(RomaicChronology.INSTANCE.isLeapYear(5504));
//        Assert.assertTrue(RomaicChronology.INSTANCE.isLeapYear(-5500));
//
//
////        final LocalDate local2 = LocalDate.of(-5509, Month.SEPTEMBER, 16);
////        final RomaicDate romaic2 = RomaicDate.ofEpochDay(local.toEpochDay());
////        Assert.assertEquals(1, romaic.getDayOfMonth());
////        Assert.assertEquals(RomaicMonth.SEPTEMBER, romaic.getMonth());
////        Assert.assertEquals(0, romaic.getYear());
//    }
//
//    /**
//     *
//     */
//    @Test @Ignore
//    public void testOfEpochDay() {
//
//        final RomaicDate romaic1 = RomaicDate.ofEpochDay(LocalDate.of(2016, Month.JANUARY, 16).toEpochDay());
//        Assert.assertEquals(3, romaic1.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.JANUARY, romaic1.getMonth());
//        Assert.assertEquals(2016, romaic1.getYear());
//
//        final RomaicDate romaic2200 = RomaicDate.ofEpochDay(LocalDate.of(2200, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(18, romaic2200.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic2200.getMonth());
//        Assert.assertEquals(2199, romaic2200.getYear());
//
//        final RomaicDate romaic2100 = RomaicDate.ofEpochDay(LocalDate.of(2100, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(19, romaic2100.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic2100.getMonth());
//        Assert.assertEquals(2099, romaic2100.getYear());
//
//        final RomaicDate romaic2000 = RomaicDate.ofEpochDay(LocalDate.of(2000, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(19, romaic2000.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic2000.getMonth());
//        Assert.assertEquals(1999, romaic2000.getYear());
//
//        final RomaicDate romaic1900 = RomaicDate.ofEpochDay(LocalDate.of(1900, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(20, romaic1900.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic1900.getMonth());
//        Assert.assertEquals(1899, romaic1900.getYear());
//
//        final RomaicDate romaic1800 = RomaicDate.ofEpochDay(LocalDate.of(1800, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(21, romaic1800.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic1800.getMonth());
//        Assert.assertEquals(1799, romaic1800.getYear());
//
//        final RomaicDate romaic1700 = RomaicDate.ofEpochDay(LocalDate.of(1700, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(22, romaic1700.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic1700.getMonth());
//        Assert.assertEquals(1699, romaic1700.getYear());
//
//        final RomaicDate romaic1600 = RomaicDate.ofEpochDay(LocalDate.of(1600, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(22, romaic1600.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic1600.getMonth());
//        Assert.assertEquals(1599, romaic1600.getYear());
//
//        final RomaicDate romaic1500 = RomaicDate.ofEpochDay(LocalDate.of(1500, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(23, romaic1500.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic1500.getMonth());
//        Assert.assertEquals(1499, romaic1500.getYear());
//
//        final RomaicDate romaic1400 = RomaicDate.ofEpochDay(LocalDate.of(1400, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(24, romaic1400.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic1400.getMonth());
//        Assert.assertEquals(1399, romaic1400.getYear());
//
//        final RomaicDate romaic1300 = RomaicDate.ofEpochDay(LocalDate.of(1300, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(25, romaic1300.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic1300.getMonth());
//        Assert.assertEquals(1299, romaic1300.getYear());
//
//        final RomaicDate romaic1200 = RomaicDate.ofEpochDay(LocalDate.of(1200, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(25, romaic1200.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic1200.getMonth());
//        Assert.assertEquals(1199, romaic1200.getYear());
//
//        final RomaicDate romaic1100 = RomaicDate.ofEpochDay(LocalDate.of(1100, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(26, romaic1100.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic1100.getMonth());
//        Assert.assertEquals(1099, romaic1100.getYear());
//
//        final RomaicDate romaic1000 = RomaicDate.ofEpochDay(LocalDate.of(1000, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(27, romaic1000.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic1000.getMonth());
//        Assert.assertEquals(999, romaic1000.getYear());
//
//        final RomaicDate romaic900 = RomaicDate.ofEpochDay(LocalDate.of(900, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(28, romaic900.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic900.getMonth());
//        Assert.assertEquals(899, romaic900.getYear());
//
//        final RomaicDate romaic800 = RomaicDate.ofEpochDay(LocalDate.of(800, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(28, romaic800.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic800.getMonth());
//        Assert.assertEquals(799, romaic800.getYear());
//
//        final RomaicDate romaic700 = RomaicDate.ofEpochDay(LocalDate.of(700, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(29, romaic700.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic700.getMonth());
//        Assert.assertEquals(699, romaic700.getYear());
//
//        final RomaicDate romaic600 = RomaicDate.ofEpochDay(LocalDate.of(600, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(30, romaic600.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic600.getMonth());
//        Assert.assertEquals(599, romaic600.getYear());
//
//        final RomaicDate romaic500 = RomaicDate.ofEpochDay(LocalDate.of(500, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(31, romaic500.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic500.getMonth());
//        Assert.assertEquals(499, romaic500.getYear());
//
//        final RomaicDate romaic400 = RomaicDate.ofEpochDay(LocalDate.of(400, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(31, romaic400.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.DECEMBER, romaic400.getMonth());
//        Assert.assertEquals(399, romaic400.getYear());
//
//        final RomaicDate romaic300 = RomaicDate.ofEpochDay(LocalDate.of(300, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(1, romaic300.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.JANUARY, romaic300.getMonth());
//        Assert.assertEquals(300, romaic300.getYear());
//
//        final RomaicDate romaic200 = RomaicDate.ofEpochDay(LocalDate.of(200, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(2, romaic200.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.JANUARY, romaic200.getMonth());
//        Assert.assertEquals(200, romaic200.getYear());
//
//        final RomaicDate romaic100 = RomaicDate.ofEpochDay(LocalDate.of(100, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(3, romaic100.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.JANUARY, romaic100.getMonth());
//        Assert.assertEquals(100, romaic100.getYear());
////
//        final RomaicDate romaic0 = RomaicDate.ofEpochDay(LocalDate.of(0, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(3, romaic0.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.JANUARY, romaic0.getMonth());
//        Assert.assertEquals(0, romaic0.getYear());
//
//        final RomaicDate romaic_100 = RomaicDate.ofEpochDay(LocalDate.of(-100, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(4, romaic_100.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.JANUARY, romaic_100.getMonth());
//        Assert.assertEquals(-100, romaic_100.getYear());
//
//        final RomaicDate romaic_200 = RomaicDate.ofEpochDay(LocalDate.of(-200, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(5, romaic_200.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.JANUARY, romaic_200.getMonth());
//        Assert.assertEquals(-200, romaic_200.getYear());
//
//        final RomaicDate romaic_300 = RomaicDate.ofEpochDay(LocalDate.of(-300, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(6, romaic_300.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.JANUARY, romaic_300.getMonth());
//        Assert.assertEquals(-300, romaic_300.getYear());
////
//        final RomaicDate romaic_400 = RomaicDate.ofEpochDay(LocalDate.of(-400, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(6, romaic_400.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.JANUARY, romaic_400.getMonth());
//        Assert.assertEquals(-400, romaic_400.getYear());
//
//        final RomaicDate romaic_500 = RomaicDate.ofEpochDay(LocalDate.of(-500, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(7, romaic_500.getDayOfMonth());
//        Assert.assertEquals(RomaicMonth.JANUARY, romaic_500.getMonth());
//        Assert.assertEquals(-500, romaic_500.getYear());
//    }
//
//    @Test
//    public void testToEpochDay() {
//        Assert.assertEquals(0, RomaicDate.of(1969+5509, 4, 19).toEpochDay());//1-1-1970 grégorien
//        Assert.assertEquals(-RomaicDate.DAYS_0000_ROMAIC_TO_1970_GREGORIAN, RomaicDate.of(0000, 1, 1).toEpochDay());
//        Assert.assertEquals(-RomaicDate.DAYS_0000_ROMAIC_TO_1970_GREGORIAN-365, RomaicDate.of(-1, 1, 1).toEpochDay());
//    }
//
//    @Test
//    public void testDayOfYear() {
//        Assert.assertEquals(1, RomaicDate.of(2016, 1, 1).getDayOfYear());
//        Assert.assertEquals(62, RomaicDate.of(2016, 3, 1).getDayOfYear());
//        Assert.assertEquals(1, RomaicDate.of(2015, 1, 1).getDayOfYear());
//        Assert.assertEquals(62, RomaicDate.of(2015, 3, 1).getDayOfYear());
//    }
//
//    @Test
//    public void testDayOfWeek() {
//        Assert.assertEquals(DayOfWeek.THURSDAY, RomaicDate.of(1969+5509, 4, 19).getDayOfWeek());//1-1-1970 grégorien
//        Assert.assertEquals(LocalDate.now().getDayOfWeek(), RomaicDate.from(LocalDate.now()).getDayOfWeek());
//    }
//
//    /**
//     * Test of of method, of class GregorianDate.
//     */
//    @Test
//    public void testLengthOfMonth() {
//        Assert.assertEquals(30, RomaicDate.of(200, 1, 1).lengthOfMonth());
//        Assert.assertEquals(31, RomaicDate.of(2016, 2, 1).lengthOfMonth());
//        Assert.assertEquals(30, RomaicDate.of(2016, 3, 1).lengthOfMonth());
//        Assert.assertEquals(31, RomaicDate.of(2016, 4, 1).lengthOfMonth());
//        Assert.assertEquals(31, RomaicDate.of(2016, 5, 1).lengthOfMonth());
//
//        Assert.assertEquals(29, RomaicDate.of(2016, 6, 1).lengthOfMonth());
//        Assert.assertEquals(28, RomaicDate.of(2015, 6, 1).lengthOfMonth());
//        Assert.assertEquals(29, RomaicDate.of(2000, 6, 1).lengthOfMonth());
//        Assert.assertEquals(29, RomaicDate.of(1900, 6, 1).lengthOfMonth());
//        Assert.assertEquals(29, RomaicDate.of(1700, 6, 1).lengthOfMonth());
//        Assert.assertEquals(29, RomaicDate.of(1600, 6, 1).lengthOfMonth());
//        Assert.assertEquals(29, RomaicDate.of(1500, 6, 1).lengthOfMonth());
//
//        Assert.assertEquals(31, RomaicDate.of(2016, 7, 1).lengthOfMonth());
//        Assert.assertEquals(30, RomaicDate.of(2016, 8, 1).lengthOfMonth());
//        Assert.assertEquals(31, RomaicDate.of(2016, 9, 1).lengthOfMonth());
//        Assert.assertEquals(30, RomaicDate.of(2016, 10, 1).lengthOfMonth());
//        Assert.assertEquals(31, RomaicDate.of(2016, 11, 1).lengthOfMonth());
//        Assert.assertEquals(31, RomaicDate.of(2016, 12, 1).lengthOfMonth());
//    }
//
//    /**
//     * Test of of method, of class GregorianDate.
//     */
//    @Test
//    public void testLengthOfYear() {
//        Assert.assertEquals(366, RomaicDate.of(2000, 12, 31).lengthOfYear());
//        Assert.assertEquals(366, RomaicDate.of(2000, 1, 1).lengthOfYear());
//        Assert.assertEquals(366, RomaicDate.of(1900, 12, 31).lengthOfYear());
//        Assert.assertEquals(366, RomaicDate.of(1900, 1, 1).lengthOfYear());
//        Assert.assertEquals(366, RomaicDate.of(2016, 12, 31).lengthOfYear());
//        Assert.assertEquals(366, RomaicDate.of(2016, 1, 1).lengthOfYear());
//        Assert.assertEquals(365, RomaicDate.of(2015, 12, 31).lengthOfYear());
//        Assert.assertEquals(365, RomaicDate.of(2015, 1, 1).lengthOfYear());
//    }
}
