package science.unlicense.time.impl.timejvm;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.time.impl.PetrinianDate;

/**
 *
 * @author Samuel Andrés
 */
public class PetrinianDateTest {

//    @Test
//    public void testOf0() {
//        final LocalDate local = LocalDate.of(2016, Month.JANUARY, 16);
//        final PetrinianDate petrinian = PetrinianDate.ofEpochDay(local.toEpochDay());
//        Assert.assertEquals(3, petrinian.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, petrinian.getMonth());
//        Assert.assertEquals(2016, petrinian.getYear());
//    }
//
//    /**
//     *
//     */
//    @Test
//    public void testOfEpochDay() {
//
//        final PetrinianDate petrinian1 = PetrinianDate.ofEpochDay(LocalDate.of(2016, Month.JANUARY, 16).toEpochDay());
//        Assert.assertEquals(3, petrinian1.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, petrinian1.getMonth());
//        Assert.assertEquals(2016, petrinian1.getYear());
//
//        final PetrinianDate petrinian2200 = PetrinianDate.ofEpochDay(LocalDate.of(2200, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(18, petrinian2200.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian2200.getMonth());
//        Assert.assertEquals(2199, petrinian2200.getYear());
//
//        final PetrinianDate petrinian2100 = PetrinianDate.ofEpochDay(LocalDate.of(2100, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(19, petrinian2100.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian2100.getMonth());
//        Assert.assertEquals(2099, petrinian2100.getYear());
//
//        final PetrinianDate petrinian2000 = PetrinianDate.ofEpochDay(LocalDate.of(2000, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(19, petrinian2000.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian2000.getMonth());
//        Assert.assertEquals(1999, petrinian2000.getYear());
//
//        final PetrinianDate petrinian1900 = PetrinianDate.ofEpochDay(LocalDate.of(1900, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(20, petrinian1900.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian1900.getMonth());
//        Assert.assertEquals(1899, petrinian1900.getYear());
//
//        final PetrinianDate petrinian1800 = PetrinianDate.ofEpochDay(LocalDate.of(1800, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(21, petrinian1800.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian1800.getMonth());
//        Assert.assertEquals(1799, petrinian1800.getYear());
//
//        final PetrinianDate petrinian1700 = PetrinianDate.ofEpochDay(LocalDate.of(1700, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(22, petrinian1700.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian1700.getMonth());
//        Assert.assertEquals(1699, petrinian1700.getYear());
//
//        final PetrinianDate petrinian1600 = PetrinianDate.ofEpochDay(LocalDate.of(1600, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(22, petrinian1600.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian1600.getMonth());
//        Assert.assertEquals(1599, petrinian1600.getYear());
//
//        final PetrinianDate petrinian1500 = PetrinianDate.ofEpochDay(LocalDate.of(1500, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(23, petrinian1500.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian1500.getMonth());
//        Assert.assertEquals(1499, petrinian1500.getYear());
//
//        final PetrinianDate petrinian1400 = PetrinianDate.ofEpochDay(LocalDate.of(1400, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(24, petrinian1400.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian1400.getMonth());
//        Assert.assertEquals(1399, petrinian1400.getYear());
//
//        final PetrinianDate petrinian1300 = PetrinianDate.ofEpochDay(LocalDate.of(1300, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(25, petrinian1300.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian1300.getMonth());
//        Assert.assertEquals(1299, petrinian1300.getYear());
//
//        final PetrinianDate petrinian1200 = PetrinianDate.ofEpochDay(LocalDate.of(1200, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(25, petrinian1200.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian1200.getMonth());
//        Assert.assertEquals(1199, petrinian1200.getYear());
//
//        final PetrinianDate petrinian1100 = PetrinianDate.ofEpochDay(LocalDate.of(1100, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(26, petrinian1100.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian1100.getMonth());
//        Assert.assertEquals(1099, petrinian1100.getYear());
//
//        final PetrinianDate petrinian1000 = PetrinianDate.ofEpochDay(LocalDate.of(1000, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(27, petrinian1000.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian1000.getMonth());
//        Assert.assertEquals(999, petrinian1000.getYear());
//
//        final PetrinianDate petrinian900 = PetrinianDate.ofEpochDay(LocalDate.of(900, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(28, petrinian900.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian900.getMonth());
//        Assert.assertEquals(899, petrinian900.getYear());
//
//        final PetrinianDate petrinian800 = PetrinianDate.ofEpochDay(LocalDate.of(800, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(28, petrinian800.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian800.getMonth());
//        Assert.assertEquals(799, petrinian800.getYear());
//
//        final PetrinianDate petrinian700 = PetrinianDate.ofEpochDay(LocalDate.of(700, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(29, petrinian700.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian700.getMonth());
//        Assert.assertEquals(699, petrinian700.getYear());
//
//        final PetrinianDate petrinian600 = PetrinianDate.ofEpochDay(LocalDate.of(600, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(30, petrinian600.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian600.getMonth());
//        Assert.assertEquals(599, petrinian600.getYear());
//
//        final PetrinianDate petrinian500 = PetrinianDate.ofEpochDay(LocalDate.of(500, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(31, petrinian500.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian500.getMonth());
//        Assert.assertEquals(499, petrinian500.getYear());
//
//        final PetrinianDate petrinian400 = PetrinianDate.ofEpochDay(LocalDate.of(400, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(31, petrinian400.getDayOfMonth());
//        Assert.assertEquals(Month.DECEMBER, petrinian400.getMonth());
//        Assert.assertEquals(399, petrinian400.getYear());
//
//        final PetrinianDate petrinian300 = PetrinianDate.ofEpochDay(LocalDate.of(300, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(1, petrinian300.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, petrinian300.getMonth());
//        Assert.assertEquals(300, petrinian300.getYear());
//
//        final PetrinianDate petrinian200 = PetrinianDate.ofEpochDay(LocalDate.of(200, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(2, petrinian200.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, petrinian200.getMonth());
//        Assert.assertEquals(200, petrinian200.getYear());
//
//        final PetrinianDate petrinian100 = PetrinianDate.ofEpochDay(LocalDate.of(100, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(3, petrinian100.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, petrinian100.getMonth());
//        Assert.assertEquals(100, petrinian100.getYear());
////
//        final PetrinianDate petrinian0 = PetrinianDate.ofEpochDay(LocalDate.of(0, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(3, petrinian0.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, petrinian0.getMonth());
//        Assert.assertEquals(0, petrinian0.getYear());
//
//        final PetrinianDate petrinian_100 = PetrinianDate.ofEpochDay(LocalDate.of(-100, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(4, petrinian_100.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, petrinian_100.getMonth());
//        Assert.assertEquals(-100, petrinian_100.getYear());
//
//        final PetrinianDate petrinian_200 = PetrinianDate.ofEpochDay(LocalDate.of(-200, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(5, petrinian_200.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, petrinian_200.getMonth());
//        Assert.assertEquals(-200, petrinian_200.getYear());
//
//        final PetrinianDate petrinian_300 = PetrinianDate.ofEpochDay(LocalDate.of(-300, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(6, petrinian_300.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, petrinian_300.getMonth());
//        Assert.assertEquals(-300, petrinian_300.getYear());
////
//        final PetrinianDate petrinian_400 = PetrinianDate.ofEpochDay(LocalDate.of(-400, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(6, petrinian_400.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, petrinian_400.getMonth());
//        Assert.assertEquals(-400, petrinian_400.getYear());
//
//        final PetrinianDate petrinian_500 = PetrinianDate.ofEpochDay(LocalDate.of(-500, Month.JANUARY, 1).toEpochDay());
//        Assert.assertEquals(7, petrinian_500.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, petrinian_500.getMonth());
//        Assert.assertEquals(-500, petrinian_500.getYear());
//    }
//
//    @Test
//    public void testToEpochDay() {
//        Assert.assertEquals(0, PetrinianDate.of(1969, 12, 19).toEpochDay());//1-1-1970 grégorien
//        Assert.assertEquals(-PetrinianDate.DAYS_0000_PETRINIAN_TO_1970_GREGORIAN, PetrinianDate.of(0000, 1, 1).toEpochDay());
//        Assert.assertEquals(-PetrinianDate.DAYS_0000_PETRINIAN_TO_1970_GREGORIAN-365, PetrinianDate.of(-1, 1, 1).toEpochDay());
//    }
//
//    @Test
//    public void testDayOfYear() {
//        Assert.assertEquals(1, PetrinianDate.of(2016, 1, 1).getDayOfYear());
//        Assert.assertEquals(61, PetrinianDate.of(2016, 3, 1).getDayOfYear());
//        Assert.assertEquals(1, PetrinianDate.of(2015, 1, 1).getDayOfYear());
//        Assert.assertEquals(60, PetrinianDate.of(2015, 3, 1).getDayOfYear());
//    }
//
//    @Test
//    public void testDayOfWeek() {
//        Assert.assertEquals(DayOfWeek.THURSDAY, PetrinianDate.of(1969, 12, 19).getDayOfWeek());//1-1-1970 grégorien
//        Assert.assertEquals(LocalDate.now().getDayOfWeek(), PetrinianDate.from(LocalDate.now()).getDayOfWeek());
//    }
//
//    /**
//     * Test of of method, of class GregorianDate.
//     */
//    @Test
//    public void testLengthOfMonth() {
//        Assert.assertEquals(31, PetrinianDate.of(2016, 1, 1).lengthOfMonth());
//
//        Assert.assertEquals(29, PetrinianDate.of(2016, 2, 1).lengthOfMonth());
//        Assert.assertEquals(28, PetrinianDate.of(2015, 2, 1).lengthOfMonth());
//        Assert.assertEquals(29, PetrinianDate.of(2000, 2, 1).lengthOfMonth());
//        Assert.assertEquals(29, PetrinianDate.of(1900, 2, 1).lengthOfMonth());
//        Assert.assertEquals(29, PetrinianDate.of(1700, 2, 1).lengthOfMonth());
//        Assert.assertEquals(29, PetrinianDate.of(1600, 2, 1).lengthOfMonth());
//        Assert.assertEquals(29, PetrinianDate.of(1500, 2, 1).lengthOfMonth());
//
//        Assert.assertEquals(31, PetrinianDate.of(2016, 3, 1).lengthOfMonth());
//        Assert.assertEquals(30, PetrinianDate.of(2016, 4, 1).lengthOfMonth());
//        Assert.assertEquals(31, PetrinianDate.of(2016, 5, 1).lengthOfMonth());
//        Assert.assertEquals(30, PetrinianDate.of(2016, 6, 1).lengthOfMonth());
//        Assert.assertEquals(31, PetrinianDate.of(2016, 7, 1).lengthOfMonth());
//        Assert.assertEquals(31, PetrinianDate.of(2016, 8, 1).lengthOfMonth());
//        Assert.assertEquals(30, PetrinianDate.of(2016, 9, 1).lengthOfMonth());
//        Assert.assertEquals(31, PetrinianDate.of(2016, 10, 1).lengthOfMonth());
//        Assert.assertEquals(30, PetrinianDate.of(2016, 11, 1).lengthOfMonth());
//        Assert.assertEquals(31, PetrinianDate.of(2016, 12, 1).lengthOfMonth());
//    }
//
//    /**
//     * Test of of method, of class GregorianDate.
//     */
//    @Test
//    public void testLengthOfYear() {
//        Assert.assertEquals(366, PetrinianDate.of(2000, 12, 31).lengthOfYear());
//        Assert.assertEquals(366, PetrinianDate.of(2000, 1, 1).lengthOfYear());
//        Assert.assertEquals(366, PetrinianDate.of(1900, 12, 31).lengthOfYear());
//        Assert.assertEquals(366, PetrinianDate.of(1900, 1, 1).lengthOfYear());
//        Assert.assertEquals(366, PetrinianDate.of(2016, 12, 31).lengthOfYear());
//        Assert.assertEquals(366, PetrinianDate.of(2016, 1, 1).lengthOfYear());
//        Assert.assertEquals(365, PetrinianDate.of(2015, 12, 31).lengthOfYear());
//        Assert.assertEquals(365, PetrinianDate.of(2015, 1, 1).lengthOfYear());
//    }
}
