

package science.unlicense.jvm.device.usb;

import org.usb4java.Context;
import org.usb4java.DeviceList;
import org.usb4java.LibUsb;
import science.unlicense.common.api.character.Chars;
import science.unlicense.device.api.DeviceException;
import science.unlicense.device.api.usb.USBDevice;
import science.unlicense.device.api.usb.USBManager;

/**
 *
 * doc :
 * http://usb4java.org/quickstart/libusb.html
 *
 * @author Johann Sorel
 */
public class USB4JManager implements USBManager {

    private static final Chars NAME = Chars.constant("USB4J");

    private Context context;

    public USB4JManager() {
    }

    public Chars getName() {
        return NAME;
    }

    private synchronized Context getContext() throws DeviceException{
        if (context==null){
            context = new Context();
            int result = LibUsb.init(context);
            if (result != LibUsb.SUCCESS){
                throw new DeviceException("Unable to initialize libusb. "+ USB4JUtils.getErrorMessage(result));
            }
        }
        return context;
    }

    public USBDevice[] getDevices() throws DeviceException {

        final Context context = getContext();
        // Read the USB device list
        final DeviceList list = new DeviceList();
        int result = LibUsb.getDeviceList(context, list);
        if (result <0) {
            throw new DeviceException("Unable to get device list. "+ USB4JUtils.getErrorMessage(result));
        }

        final USBDevice[] devices = new USBDevice[list.getSize()];
        try {
            // Iterate over all devices and scan for the right one
            for (int i=0;i<devices.length;i++) {
                devices[i] = new USB4JDevice(context, list.get(i));
            }
        } finally {
            // Ensure the allocated device list is freed
            LibUsb.freeDeviceList(list, true);
        }

        return devices;
    }

    /**
     * Release Libusb context.
     * @throws Throwable
     */
    protected void finalize() throws Throwable {
        if (context!=null){
            LibUsb.exit(context);
        }
        super.finalize();
    }

}
