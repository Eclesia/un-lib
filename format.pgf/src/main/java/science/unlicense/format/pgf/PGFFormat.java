
package science.unlicense.format.pgf;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * http://en.wikipedia.org/wiki/Progressive_Graphics_File
 *
 * @author Johann Sorel
 */
public class PGFFormat extends AbstractImageFormat {

    public static final PGFFormat INSTANCE = new PGFFormat();

    private PGFFormat() {
        super(new Chars("pgf"));
        shortName = new Chars("PGF");
        longName = new Chars("Progressive Graphics File");
        extensions.add(new Chars("pgf"));
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new PGFStore(this, source);
    }

}
