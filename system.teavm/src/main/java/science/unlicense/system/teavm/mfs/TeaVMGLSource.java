
package science.unlicense.system.teavm.mfs;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.gpu.api.SharedContext;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GLBinding;
import science.unlicense.gpu.api.opengl.GLCallback;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.system.teavm.TeaVMGLBinding;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMGLSource implements GLSource {

    private final TeaVMGLBinding binding;
    private final SharedContext context = new TeaVMContext();
    private final GL gl;
    private final Sequence callbacks = new ArraySequence();

    public TeaVMGLSource(TeaVMGLBinding binding, GL gl) {
        this.binding = binding;
        this.gl = gl;
    }

    @Override
    public GLBinding getBinding() {
        return binding;
    }

    @Override
    public SharedContext getContext() {
        return context;
    }

    @Override
    public GL getGL() {
        return gl;
    }

    @Override
    public int getX() {
        return 0;
    }

    @Override
    public int getY() {
        return 0;
    }

    @Override
    public int getWidth() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getHeight() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Sequence getCallbacks() {
        return callbacks;
    }

    @Override
    public void render() {
        for (int i=0,n=callbacks.getSize();i<n;i++) {
            ((GLCallback) callbacks.get(i)).execute(this);
        }
    }

    @Override
    public void dispose() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
