
package science.unlicense.system.teavm;

import org.teavm.jso.browser.Window;
import org.teavm.jso.canvas.CanvasRenderingContext2D;
import org.teavm.jso.canvas.ImageData;
import org.teavm.jso.dom.html.HTMLCanvasElement;
import org.teavm.jso.dom.html.HTMLDocument;
import org.teavm.jso.typedarrays.Uint8ClampedArray;
import science.unlicense.display.api.font.AbstractFontMetadata;
import science.unlicense.display.api.font.Font;
import science.unlicense.geometry.api.BBox;

/**
 *
 * @author Johann Sorel
 */
class TeaVMFontMetadata extends AbstractFontMetadata {

    private final String definition;

    public TeaVMFontMetadata(String definition, int size) {
        this.definition = definition;
        height = size;
        glyphBox = new BBox(new double[]{-1, -1}, new double[]{8, 12});
        advanceWidthMax = 8;
        ascent = 12;
        descent = 2;
        lineGap = 4;
        minLeftSideBearing = 1;
        minRightSideBearing = 1;
        weight = Font.WEIGHT_NONE;
        xMaxExtent = 9;
    }

    @Override
    public double getAdvanceWidth(int unicode) {
        return advanceWidthMax;
    }

    private void getMetrics() {
        final HTMLDocument doc = Window.current().getDocument();
        final HTMLCanvasElement canvas = (HTMLCanvasElement) doc.getElementById("metric");
        final CanvasRenderingContext2D context = (CanvasRenderingContext2D) canvas.getContext("2d");
        context.setFont(definition);

        final int w = canvas.getWidth();
        final int h = canvas.getHeight();

        context.setFillStyle("#FFFFFF");
        context.fillRect(0, 0, w, h);

        context.setFillStyle("#000000");
        context.setTextAlign("left");
        context.setTextBaseline("Alphabetic");
        context.fillText("H", w/2, h/2);

        ImageData imageData = context.getImageData(0, 0, canvas.getWidth(), canvas.getHeight());
        Uint8ClampedArray pixelData = imageData.getData();

        double fontSize = height;
        double baseline = w / 2;
        double padding = 0;

        lineGap = 1.2 * fontSize;

        int i = 0;
        int w4 = w * 4;
        int len = pixelData.getLength();

        while (++i < len && pixelData.get(i) == 255) {
        }
        double ascent = (i / w4) | 0;

        i = len - 1;
        while (--i > 0 && pixelData.get(i) == 255) {
        }
        double descent = (i / w4) | 0;

        for (i = 0; i < len && pixelData.get(i) == 255;) {
            i += w4;
            if (i >= len) {
                i = (i - len) + 4;
            }
        }
        double minx = ((i % w4) / 4) | 0;

        int step = 1;
        for (i = len - 3; i >= 0 && pixelData.get(i) == 255;) {
            i -= w4;
            if (i < 0) {
                i = (len - 3) - (step++) * 4;
            }
        }
        double maxx = ((i % w4) / 4) + 1 | 0;

        // set font metrics
        this.ascent = (baseline - ascent);
        this.descent = (descent - baseline);
        this.glyphBox = new BBox(
                new double[]{minx - (padding / 2), 0},
                new double[]{maxx - (padding / 2), descent - ascent});
        this.height = 1 + (descent - ascent);

    }

}
