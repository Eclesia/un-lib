
package science.unlicense.system.teavm;

import org.teavm.jso.canvas.CanvasRenderingContext2D;
import org.teavm.jso.dom.html.HTMLCanvasElement;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Set;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.engine.ui.visual.VirtualFontStore;

/**
 *
 * @author Johann Sorel
 */
class TeaVMFontStore extends VirtualFontStore {

    final HTMLCanvasElement canvas;
    final CanvasRenderingContext2D g;

    public TeaVMFontStore(HTMLCanvasElement canvas, CanvasRenderingContext2D g) {
        this.canvas = canvas;
        this.g = g;
    }

    @Override
    public Set getFamilies() {
        return Collections.emptySet();
    }

    @Override
    public FontMetadata getMetadata(Chars family) {
        return new TeaVMFontMetadata(family.toString()+" 12px", 12);
    }

}
