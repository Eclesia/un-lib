
package science.unlicense.system.teavm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.model.tree.DefaultNamedNode;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.common.api.model.tree.NodeMessage;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.system.DefaultMetaTree;
import science.unlicense.system.GlobalProperties;
import science.unlicense.system.MetaTree;
import science.unlicense.system.MetadataTreeIO;
import science.unlicense.system.teavm.mfs.TeaVMFileFormat;
import science.unlicense.system.teavm.mfs.TeaVMFileResolver;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMGlobalProperties implements GlobalProperties {

    private final MetaTree systemTree;
    private final MetaTree persTree;
    private final MetaTree execTree;

    public TeaVMGlobalProperties() {
        final NamedNode systemRoot = new DefaultNamedNode(true);
        final NamedNode persRoot = new DefaultNamedNode(true);
        final NamedNode execRoot = new DefaultNamedNode(true);
        systemTree = new DefaultMetaTree(systemRoot);
        persTree = new DefaultMetaTree(persRoot);
        execTree = new DefaultMetaTree(execRoot);

        //system informations --------------------------------------------------
        final NamedNode system = new DefaultNamedNode(new Chars("system"),true);
        systemRoot.getChildren().add(system);

        //list root files
        final TeaVMFileFormat format = TeaVMFileFormat.INSTANCE;
        final PathResolver resolver = format.createResolver(null);
        final NamedNode fileRoots = new DefaultNamedNode(new Chars("fileRoots"),true);
        system.getChildren().add(fileRoots);

        final NamedNode fr = new DefaultNamedNode(new Chars("/"), TeaVMFileResolver.ROOT,true);
        fileRoots.getChildren().add(fr);

        //user informations
        final NamedNode user = new DefaultNamedNode(new Chars("user"),true);
        system.getChildren().add(user);

        Chars userHomeStr = new Chars("/home/user/");
        Chars userNameStr = new Chars("user");

        final NamedNode userHome = new DefaultNamedNode(new Chars("home"),
                new Chars("file:"+userHomeStr),true);
        final NamedNode userName = new DefaultNamedNode(new Chars("name"),
                userNameStr,true);
        user.getChildren().add(userHome);
        user.getChildren().add(userName);

        //load persisted informations ------------------------------------------
        Path persUrl = null;
//        try {
//            Path path = resolver.resolve((Chars) userHome.getValue());
//            path = path.resolve(new Chars(".unlicense"));
//            path.createContainer();
//            path = path.resolve(new Chars("persistant.properties"));
//            persUrl = path;
//            if (path.exists()) {
//                final NamedNode pers = MetadataTreeIO.read(persUrl);
//                MetadataTreeIO.merge(persRoot, pers);
//            }
//        } catch(Exception ex) {
//            ex.printStackTrace();
//        }

        //save changes
        final Path p = persUrl;
        persRoot.addEventListener(NodeMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                try {
                    MetadataTreeIO.write(persRoot, p);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

    }

    @Override
    public MetaTree getSystemTree() {
        return systemTree;
    }

    @Override
    public MetaTree getPersistantTree() {
        return persTree;
    }

    @Override
    public MetaTree getExecutionTree() {
        return execTree;
    }

}
