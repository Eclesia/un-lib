
package science.unlicense.system.teavm;

import org.teavm.jso.JSObject;
import org.teavm.jso.core.JSArrayReader;
import org.teavm.jso.dom.html.HTMLCanvasElement;
import org.teavm.jso.webgl.WebGLActiveInfo;
import org.teavm.jso.webgl.WebGLBuffer;
import org.teavm.jso.webgl.WebGLFramebuffer;
import org.teavm.jso.webgl.WebGLProgram;
import org.teavm.jso.webgl.WebGLRenderbuffer;
import org.teavm.jso.webgl.WebGLRenderingContext;
import org.teavm.jso.webgl.WebGLShader;
import org.teavm.jso.webgl.WebGLTexture;
import org.teavm.jso.webgl.WebGLUniformLocation;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.gpu.api.opengl.GL1;
import science.unlicense.gpu.api.opengl.GL1ES;
import science.unlicense.gpu.api.opengl.GL2;
import science.unlicense.gpu.api.opengl.GL2ES2;
import science.unlicense.gpu.api.opengl.GL2ES3;
import science.unlicense.gpu.api.opengl.GL3;
import science.unlicense.gpu.api.opengl.GL4;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMGL implements GL2ES2 {

    private class ProgramRef {
        public final WebGLProgram program;
        private int uniformInc = 1;
        public final Dictionary uniforms = new HashDictionary();

        public ProgramRef(WebGLProgram program) {
            this.program = program;
        }

        public int getUniformLocation(Chars name) {
            final WebGLUniformLocation l = context.getUniformLocation(program, name.toString());
            int id = uniformInc++;
            uniforms.add(id, l);
            return id;
        }

        public WebGLUniformLocation getUniformLocation(int l) {
            return (WebGLUniformLocation) uniforms.getValue(l);
        }
    }

    private final HTMLCanvasElement canvas;
    private final WebGLRenderingContext context;

    // int <-> WebGlProgram
    private int programsInc = 1;
    private final Dictionary programs = new HashDictionary();
    // int <-> WebGLShader
    private int shadersInc = 1;
    private final Dictionary shaders = new HashDictionary();
    // int <-> WebGlBuffer
    private int buffersInc = 1;
    private final Dictionary buffers = new HashDictionary();
    // int <-> WebGlFrameBuffer
    private int frameBuffersInc = 1;
    private final Dictionary frameBuffers = new HashDictionary();
    // int <-> WebGlRenderBuffer
    private int renderBuffersInc = 1;
    private final Dictionary renderBuffers = new HashDictionary();
    // int <-> WebGlTexture
    private int texturesInc = 1;
    private final Dictionary textures = new HashDictionary();

    private final int[] arrayi1 = new int[1];
    private final int[] arrayi2 = new int[2];
    private final int[] arrayi3 = new int[3];
    private final int[] arrayi4 = new int[4];
    private final float[] arrayf1 = new float[1];
    private final float[] arrayf2 = new float[2];
    private final float[] arrayf3 = new float[3];
    private final float[] arrayf4 = new float[4];

    //current active program
    private ProgramRef currentProgram;

    public TeaVMGL(HTMLCanvasElement canvas) {
        this.canvas = canvas;
        context = (WebGLRenderingContext) canvas.getContext("webgl");
    }

    private WebGLUniformLocation getUniLoc(int location) {
        return currentProgram.getUniformLocation(location);
    }

    @Override
    public BufferFactory getBufferFactory() {
        return TeaVMBufferFactory.INSTANCE;
    }

    @Override
    public boolean isGL1() {
        return false;
    }

    @Override
    public boolean isGL2() {
        return false;
    }

    @Override
    public boolean isGL3() {
        return false;
    }

    @Override
    public boolean isGL4() {
        return false;
    }

    @Override
    public boolean isGL1ES() {
        return false;
    }

    @Override
    public boolean isGL2ES2() {
        return true;
    }

    @Override
    public boolean isGL2ES3() {
        return false;
    }

    @Override
    public GL1 asGL1() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public GL2 asGL2() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public GL3 asGL3() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public GL4 asGL4() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public GL1ES asGL1ES() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public GL2ES2 asGL2ES2() {
        return this;
    }

    @Override
    public GL2ES3 asGL2ES3() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glActiveTexture(int texture) {
        context.activeTexture(texture);
    }

    @Override
    public void glAttachShader(int program, int shader) {
        final ProgramRef prog = (ProgramRef) programs.getValue(program);
        final WebGLShader shad = (WebGLShader) shaders.getValue(shader);
        context.attachShader(prog.program, shad);
    }

    @Override
    public void glBindAttribLocation(int program, int index, CharArray name) {
        final ProgramRef prog = (ProgramRef) programs.getValue(program);
        context.bindAttribLocation(prog.program, index, name.toString());
    }

    @Override
    public void glBindBuffer(int target, int buffer) {
        final WebGLBuffer buf = (WebGLBuffer) buffers.getValue(buffer);
        context.bindBuffer(target, buf);
    }

    @Override
    public void glBindFramebuffer(int target, int framebuffer) {
        final WebGLFramebuffer buf = (WebGLFramebuffer) frameBuffers.getValue(framebuffer);
        context.bindFramebuffer(target, buf);
    }

    @Override
    public void glBindRenderbuffer(int target, int renderbuffer) {
        final WebGLRenderbuffer buf = (WebGLRenderbuffer) renderBuffers.getValue(renderbuffer);
        context.bindRenderbuffer(target, buf);
    }

    @Override
    public void glBindTexture(int target, int texture) {
        final WebGLTexture tex = (WebGLTexture) textures.getValue(texture);
        context.bindTexture(target, tex);
    }

    @Override
    public void glBlendColor(float red, float green, float blue, float alpha) {
        context.blendColor(red, green, blue, alpha);
    }

    @Override
    public void glBlendEquation(int mode) {
        context.blendEquation(mode);
    }

    @Override
    public void glBlendEquationSeparate(int modeRGB, int modeAlpha) {
        context.blendEquationSeparate(modeRGB, modeAlpha);
    }

    @Override
    public void glBlendFunc(int sfactor, int dfactor) {
        context.blendFunc(sfactor, dfactor);
    }

    @Override
    public void glBlendFuncSeparate(int sfactorRGB, int dfactorRGB, int sfactorAlpha, int dfactorAlpha) {
        context.blendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha);
    }

    @Override
    public void glBufferData(int target, Buffer data, int usage) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glBufferSubData(int target, long offset, Buffer data) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public int glCheckFramebufferStatus(int target) {
        return context.checkFramebufferStatus(target);
    }

    @Override
    public void glClear(int mask) {
        context.clear(mask);
    }

    @Override
    public void glClearColor(float red, float green, float blue, float alpha) {
        context.clearColor(red, green, blue, alpha);
    }

    @Override
    public void glClearDepthf(float d) {
        context.clearDepth(d);
    }

    @Override
    public void glClearStencil(int s) {
        context.clearStencil(s);
    }

    @Override
    public void glColorMask(boolean red, boolean green, boolean blue, boolean alpha) {
        context.colorMask(red, green, blue, alpha);
    }

    @Override
    public void glCompileShader(int shader) {
        final WebGLShader shad = (WebGLShader) shaders.getValue(shader);
        context.compileShader(shad);
    }

    @Override
    public void glCompressedTexImage2D(int target, int level, int internalformat, int width, int height, int border, Buffer data) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glCompressedTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, Buffer data) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glCopyTexImage2D(int target, int level, int internalformat, int x, int y, int width, int height, int border) {
        context.copyTexImage2D(target, level, internalformat, x, y, width, height, border);
    }

    @Override
    public void glCopyTexSubImage2D(int target, int level, int xoffset, int yoffset, int x, int y, int width, int height) {
        context.copyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height);
    }

    @Override
    public int glCreateProgram() {
        final WebGLProgram prog = context.createProgram();
        final int id = programsInc++;
        programs.add(id, new ProgramRef(prog));
        return id;
    }

    @Override
    public int glCreateShader(int type) {
        final WebGLShader shad = context.createShader(type);
        final int id = shadersInc++;
        shaders.add(id, shad);
        return id;
    }

    @Override
    public void glCullFace(int mode) {
        context.cullFace(mode);
    }

    @Override
    public void glDeleteBuffers(Buffer.Int32 buffers) {
        for (int i=0,n=(int) buffers.getSize();i<n;i++) {
            final WebGLBuffer buf = (WebGLBuffer) this.buffers.remove(buffers.read(i));
            context.deleteBuffer(buf);
        }
    }

    @Override
    public void glDeleteBuffers(int[] buffers) {
        for (int id : buffers) {
            final WebGLBuffer buf = (WebGLBuffer) this.buffers.remove(id);
            context.deleteBuffer(buf);
        }
    }

    @Override
    public void glDeleteFramebuffers(Buffer.Int32 framebuffers) {
        for (int i=0,n=(int) framebuffers.getSize();i<n;i++) {
            final WebGLFramebuffer buf = (WebGLFramebuffer) this.frameBuffers.remove(framebuffers.read(i));
            context.deleteFramebuffer(buf);
        }
    }

    @Override
    public void glDeleteFramebuffers(int[] framebuffers) {
        for (int id : framebuffers) {
            final WebGLFramebuffer buf = (WebGLFramebuffer) this.frameBuffers.remove(id);
            context.deleteFramebuffer(buf);
        }
    }

    @Override
    public void glDeleteProgram(int program) {
        final ProgramRef prog = (ProgramRef) programs.remove(program);
        context.deleteProgram(prog.program);
    }

    @Override
    public void glDeleteRenderbuffers(Buffer.Int32 renderbuffers) {
        for (int i=0,n=(int) renderbuffers.getSize();i<n;i++) {
            final WebGLRenderbuffer buf = (WebGLRenderbuffer) this.renderBuffers.remove(renderbuffers.read(i));
            context.deleteRenderbuffer(buf);
        }
    }

    @Override
    public void glDeleteShader(int shader) {
        final WebGLShader prog = (WebGLShader) shaders.remove(shader);
        context.deleteShader(prog);
    }

    @Override
    public void glDeleteTextures(Buffer.Int32 textures) {
        for (int i=0,n=(int) textures.getSize();i<n;i++) {
            final WebGLTexture tex = (WebGLTexture) this.textures.remove(textures.read(i));
            context.deleteTexture(tex);
        }
    }

    @Override
    public void glDepthFunc(int func) {
        context.depthFunc(func);
    }

    @Override
    public void glDepthMask(boolean flag) {
        context.depthMask(flag);
    }

    @Override
    public void glDepthRangef(float n, float f) {
        context.depthRange(n, f);
    }

    @Override
    public void glDetachShader(int program, int shader) {
        final ProgramRef prog = (ProgramRef) programs.remove(program);
        final WebGLShader shad = (WebGLShader) shaders.remove(shader);
        context.detachShader(prog.program, shad);
    }

    @Override
    public void glDisable(int cap) {
        context.disable(cap);
    }

    @Override
    public void glDisableVertexAttribArray(int index) {
        context.disableVertexAttribArray(index);
    }

    @Override
    public void glDrawArrays(int mode, int first, int count) {
        context.drawArrays(mode, first, count);
    }

    @Override
    public void glDrawElements(int mode, int count, int type, long indices) {
        context.drawElements(mode, count, type, (int) indices);
    }

    @Override
    public void glEnable(int cap) {
        context.enable(cap);
    }

    @Override
    public void glEnableVertexAttribArray(int index) {
        context.enableVertexAttribArray(index);
    }

    @Override
    public void glFinish() {
        context.finish();
    }

    @Override
    public void glFlush() {
        context.flush();
    }

    @Override
    public void glFramebufferRenderbuffer(int target, int attachment, int renderbuffertarget, int renderbuffer) {
        final WebGLRenderbuffer buf = (WebGLRenderbuffer) renderBuffers.getValue(renderbuffer);
        context.framebufferRenderbuffer(target, attachment, renderbuffertarget, buf);
    }

    @Override
    public void glFramebufferTexture2D(int target, int attachment, int textarget, int texture, int level) {
        final WebGLTexture tex = (WebGLTexture) textures.getValue(texture);
        context.framebufferTexture2D(target, attachment, textarget, tex, level);
    }

    @Override
    public void glFrontFace(int mode) {
        context.frontFace(mode);
    }

    @Override
    public void glGenBuffers(Buffer.Int32 buffers) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGenBuffers(int[] buffers) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGenerateMipmap(int target) {
        context.generateMipmap(target);
    }

    @Override
    public void glGenFramebuffers(Buffer.Int32 framebuffers) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGenFramebuffers(int[] framebuffers) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGenRenderbuffers(Buffer.Int32 renderbuffers) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGenTextures(Buffer.Int32 textures) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGenTextures(int[] textures) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetActiveAttrib(int program, int index, Buffer.Int32 length, Buffer.Int32 size, Buffer.Int32 type, Buffer.Int8 name) {
        final ProgramRef prog = (ProgramRef) programs.getValue(program);
        WebGLActiveInfo a = context.getActiveAttrib(prog.program, index);
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetActiveUniform(int program, int index, Buffer.Int32 length, Buffer.Int32 size, Buffer.Int32 type, Buffer.Int8 name) {
        final ProgramRef prog = (ProgramRef) programs.getValue(program);
        WebGLActiveInfo a = context.getActiveUniform(prog.program, index);
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetAttachedShaders(int program, Buffer.Int32 count, Buffer.Int32 shaders) {
        final ProgramRef prog = (ProgramRef) programs.getValue(program);
        JSArrayReader<WebGLShader> s = context.getAttachedShaders(prog.program);
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public int glGetAttribLocation(int program, CharArray name) {
        final ProgramRef prog = (ProgramRef) programs.getValue(program);
        return context.getAttribLocation(prog.program, name.toString());
    }

    @Override
    public void glGetBooleanv(int pname, Buffer.Int8 data) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetBufferParameteriv(int target, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public int glGetError() {
        return context.getError();
    }

    @Override
    public void glGetFloatv(int pname, Buffer.Float32 data) {
        JSObject obj = context.getParameter(pname);
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetFloatv(int pname, float[] data) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetFramebufferAttachmentParameteriv(int target, int attachment, int pname, Buffer.Int32 params) {
        JSObject att = context.getFramebufferAttachmentParameter(target, attachment, pname);
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetIntegerv(int pname, Buffer.Int32 data) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetIntegerv(int pname, int[] data) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetProgramiv(int program, int pname, Buffer.Int32 params) {
        final ProgramRef prog = (ProgramRef) programs.getValue(program);
        context.getProgramParameteri(prog.program, pname);
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetProgramiv(int program, int pname, int[] params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetProgramInfoLog(int program, int[] length, Buffer.Int8 infoLog) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetRenderbufferParameteriv(int target, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetShaderiv(int shader, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetShaderiv(int shader, int pname, int[] params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetShaderInfoLog(int shader, Buffer.Int32 length, Buffer.Int8 infoLog) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetShaderPrecisionFormat(int shadertype, int precisiontype, Buffer.Int32 range, Buffer.Int32 precision) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetShaderSource(int shader, Buffer.Int32 length, Buffer.Int8 source) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public byte glGetString(int name) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetTexParameterfv(int target, int pname, Buffer.Float32 params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetTexParameteriv(int target, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetUniformfv(int program, int location, Buffer.Float32 params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetUniformiv(int program, int location, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public int glGetUniformLocation(int program, CharArray name) {
        final ProgramRef prog = (ProgramRef) programs.getValue(program);
        context.getUniformLocation(prog.program, name.toString());
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetVertexAttribfv(int index, int pname, Buffer.Float32 params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetVertexAttribiv(int index, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glGetVertexAttribPointerv(int index, int pname, Buffer.Int8 pointer) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glHint(int target, int mode) {
        context.hint(target, mode);
    }

    @Override
    public boolean glIsBuffer(int buffer) {
        final WebGLBuffer cdt = (WebGLBuffer) buffers.getValue(buffer);
        return context.isBuffer(cdt);
    }

    @Override
    public boolean glIsEnabled(int cap) {
        return context.isEnabled(cap);
    }

    @Override
    public boolean glIsFramebuffer(int framebuffer) {
        final WebGLFramebuffer cdt = (WebGLFramebuffer) frameBuffers.getValue(framebuffer);
        return context.isFramebuffer(cdt);
    }

    @Override
    public boolean glIsProgram(int program) {
        final ProgramRef cdt = (ProgramRef) programs.getValue(program);
        return context.isProgram(cdt.program);
    }

    @Override
    public boolean glIsRenderbuffer(int renderbuffer) {
        final WebGLRenderbuffer cdt = (WebGLRenderbuffer) renderBuffers.getValue(renderbuffer);
        return context.isRenderbuffer(cdt);
    }

    @Override
    public boolean glIsShader(int shader) {
        final WebGLShader cdt = (WebGLShader) shaders.getValue(shader);
        return context.isShader(cdt);
    }

    @Override
    public boolean glIsTexture(int texture) {
        final WebGLTexture cdt = (WebGLTexture) textures.getValue(texture);
        return context.isTexture(cdt);
    }

    @Override
    public void glLineWidth(float width) {
        context.lineWidth(width);
    }

    @Override
    public void glLinkProgram(int program) {
        final ProgramRef prog = (ProgramRef) programs.getValue(program);
        context.linkProgram(prog.program);
    }

    @Override
    public void glPixelStorei(int pname, int param) {
        context.pixelStorei(pname, param);
    }

    @Override
    public void glPolygonOffset(float factor, float units) {
        context.polygonOffset(factor, units);
    }

    @Override
    public void glReadPixels(int x, int y, int width, int height, int format, int type, Buffer.Int8 pixels) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glReleaseShaderCompiler() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glRenderbufferStorage(int target, int internalformat, int width, int height) {
        context.renderbufferStorage(target, internalformat, width, height);
    }

    @Override
    public void glSampleCoverage(float value, boolean invert) {
        context.sampleCoverage(value, invert);
    }

    @Override
    public void glScissor(int x, int y, int width, int height) {
        context.scissor(x, y, width, height);
    }

    @Override
    public void glShaderBinary(Buffer.Int32 shaders, int binaryformat, Buffer.Int8 binary) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glShaderSource(int shader, CharArray[] strings) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glStencilFunc(int func, int ref, int mask) {
        context.stencilFunc(func, ref, mask);
    }

    @Override
    public void glStencilFuncSeparate(int face, int func, int ref, int mask) {
        context.stencilFuncSeparate(face, func, ref, mask);
    }

    @Override
    public void glStencilMask(int mask) {
        context.stencilMask(mask);
    }

    @Override
    public void glStencilMaskSeparate(int face, int mask) {
        context.stencilMaskSeparate(face, mask);
    }

    @Override
    public void glStencilOp(int fail, int zfail, int zpass) {
        context.stencilOp(fail, zfail, zpass);
    }

    @Override
    public void glStencilOpSeparate(int face, int sfail, int dpfail, int dppass) {
        context.stencilOpSeparate(face, sfail, dpfail, dppass);
    }

    @Override
    public void glTexImage2D(int target, int level, int internalformat, int width, int height, int border, int format, int type, Buffer pixels) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glTexParameterf(int target, int pname, float param) {
        context.texParameterf(target, pname, param);
    }

    @Override
    public void glTexParameterfv(int target, int pname, Buffer.Float32 params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glTexParameteri(int target, int pname, int param) {
        context.texParameteri(target, pname, param);
    }

    @Override
    public void glTexParameteriv(int target, int pname, Buffer.Int32 params) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer.Int8 pixels) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniform1f(int location, float v0) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform1f(loc, v0);
    }

    @Override
    public void glUniform1fv(int location, Buffer.Float32 value) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform1fv(loc, value.toFloatArray());
    }

    @Override
    public void glUniform1fv(int location, float[] value) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform1fv(loc, value);
    }

    @Override
    public void glUniform1i(int location, int v0) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform1i(loc, v0);
    }

    @Override
    public void glUniform1iv(int location, Buffer.Int32 value) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform1iv(loc, value.toIntArray());
    }

    @Override
    public void glUniform1iv(int location, int[] value) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform1iv(loc, value);
    }

    @Override
    public void glUniform2f(int location, float v0, float v1) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform2f(loc, v0, v1);
    }

    @Override
    public void glUniform2fv(int location, Buffer.Float32 value) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform2fv(loc, value.toFloatArray());
    }

    @Override
    public void glUniform2fv(int location, float[] value) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform2fv(loc, value);
    }

    @Override
    public void glUniform2i(int location, int v0, int v1) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform2i(loc, v0, v1);
    }

    @Override
    public void glUniform2iv(int location, Buffer.Int32 value) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform2iv(loc, value.toIntArray());
    }

    @Override
    public void glUniform2iv(int location, int[] value) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform2iv(loc, value);
    }

    @Override
    public void glUniform3f(int location, float v0, float v1, float v2) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform3f(loc, v0, v1, v2);
    }

    @Override
    public void glUniform3fv(int location, Buffer.Float32 value) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform3fv(loc, value.toFloatArray());
    }

    @Override
    public void glUniform3fv(int location, float[] value) {
        final WebGLUniformLocation loc = currentProgram.getUniformLocation(location);
        context.uniform3fv(loc, value);
    }

    @Override
    public void glUniform3i(int location, int v0, int v1, int v2) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniform3iv(int location, Buffer.Int32 value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniform3iv(int location, int[] value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniform4f(int location, float v0, float v1, float v2, float v3) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniform4fv(int location, Buffer.Float32 value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniform4fv(int location, float[] value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniform4i(int location, int v0, int v1, int v2, int v3) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniform4iv(int location, Buffer.Int32 value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniform4iv(int location, int[] value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniformMatrix2fv(int location, boolean transpose, Buffer.Float32 value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniformMatrix2fv(int location, boolean transpose, float[] value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniformMatrix3fv(int location, boolean transpose, Buffer.Float32 value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniformMatrix3fv(int location, boolean transpose, float[] value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniformMatrix4fv(int location, boolean transpose, Buffer.Float32 value) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUniformMatrix4fv(int location, boolean transpose, float[] value) {
        final WebGLUniformLocation loc = getUniLoc(location);
        context.uniformMatrix4fv(loc, transpose, value);
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void glUseProgram(int program) {
        final ProgramRef prog = (ProgramRef) programs.getValue(program);
        context.useProgram(prog.program);
    }

    @Override
    public void glValidateProgram(int program) {
        final ProgramRef prog = (ProgramRef) programs.getValue(program);
        context.validateProgram(prog.program);
    }

    @Override
    public void glVertexAttrib1f(int index, float x) {
        context.vertexAttrib1f(index, x);
    }

    @Override
    public void glVertexAttrib1fv(int index, Buffer.Float32 v) {
        context.vertexAttrib1fv(index, arrayf1);
        v.write(0, arrayf1);
    }

    @Override
    public void glVertexAttrib2f(int index, float x, float y) {
        context.vertexAttrib2f(index, x, y);
    }

    @Override
    public void glVertexAttrib2fv(int index, Buffer.Float32 v) {
        context.vertexAttrib2fv(index, arrayf2);
        v.write(0, arrayf2);
    }

    @Override
    public void glVertexAttrib3f(int index, float x, float y, float z) {
        context.vertexAttrib3f(index, x, y, z);
    }

    @Override
    public void glVertexAttrib3fv(int index, Buffer.Float32 v) {
        context.vertexAttrib3fv(index, arrayf3);
        v.write(0, arrayf3);
    }

    @Override
    public void glVertexAttrib4f(int index, float x, float y, float z, float w) {
        context.vertexAttrib4f(index, x, y, z, w);
    }

    @Override
    public void glVertexAttrib4fv(int index, Buffer.Float32 v) {
        context.vertexAttrib4fv(index, arrayf4);
        v.write(0, arrayf4);
    }

    @Override
    public void glVertexAttribPointer(int index, int size, int type, boolean normalized, int stride, long pointer) {
        context.vertexAttribPointer(index, size, type, normalized, stride, (int) pointer);
    }

    @Override
    public void glViewport(int x, int y, int width, int height) {
        context.viewport(x, y, width, height);
    }



}
