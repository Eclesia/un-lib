
package science.unlicense.system.teavm;

import org.teavm.jso.browser.TimerHandler;
import org.teavm.jso.browser.Window;
import org.teavm.jso.dom.events.EventListener;
import org.teavm.jso.dom.events.KeyboardEvent;
import org.teavm.jso.dom.events.MouseEvent;
import org.teavm.jso.dom.events.WheelEvent;
import org.teavm.jso.dom.html.HTMLCanvasElement;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.visual.ViewRenderLoop;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMPane {

    final HTMLCanvasElement canvas;
    final TeaVMPainter painter;
    final WContainer container = new WContainer();
    final TeaVMFrame frame;
    private final BBox dirty = new BBox(2);
    private boolean needRepaint = false;
    private boolean firstPaint = true;

    public TeaVMPane(TeaVMFrame frame, HTMLCanvasElement canvas) {
        container.setInlineStyle(new Chars("background:{fill-paint:@color-background}"));
        this.canvas = canvas;
        this.painter = new TeaVMPainter(canvas);
        this.frame = frame;
        container.setFrame(frame);
        container.addEventListener(new PropertyPredicate(WContainer.PROPERTY_DIRTY), new science.unlicense.common.api.event.EventListener() {
            @Override
            public void receiveEvent(science.unlicense.common.api.event.Event event) {
                //                    final BBox bbox = (BBox) ((PropertyMessage) event.getMessage()).getNewValue();
                //                    final int x = (int) (bbox.getMin(0) + canvas.getWidth()/2.0);
                //                    final int y = (int) (bbox.getMin(1) + canvas.getHeight()/2.0);
                //                    final int w = (int) (bbox.getSpan(0)+1); //+1 because of x/y and span integer rounding
                //                    final int h = (int) (bbox.getSpan(1)+1);
                //                    if (dirty.contains(bbox)) {
                //
                //                    } else {
                //                        dirty.expand(bbox);
                needRepaint = true;
                //                    }
                //System.out.println("dirty");
            }
        });
        canvas.addEventListener("click", new EventListener<MouseEvent>() {
            @Override
            public void handleEvent(MouseEvent evt) {
                container.receiveEvent(convertMouseEvent(evt, frame, canvas.getWidth(), canvas.getHeight()));
            }
        });
        canvas.addEventListener("mousemove", new EventListener<MouseEvent>() {
            @Override
            public void handleEvent(MouseEvent evt) {
                container.receiveEvent(convertMouseEvent(evt, frame, canvas.getWidth(), canvas.getHeight()));
            }
        });
        canvas.addEventListener("mousedown", new EventListener<MouseEvent>() {
            @Override
            public void handleEvent(MouseEvent evt) {
                container.receiveEvent(convertMouseEvent(evt, frame, canvas.getWidth(), canvas.getHeight()));
            }
        });
        canvas.addEventListener("mouseup", new EventListener<MouseEvent>() {
            @Override
            public void handleEvent(MouseEvent evt) {
                container.receiveEvent(convertMouseEvent(evt, frame, canvas.getWidth(), canvas.getHeight()));
            }
        });
        canvas.addEventListener("mouseover", new EventListener<MouseEvent>() {
            @Override
            public void handleEvent(MouseEvent evt) {
                container.receiveEvent(convertMouseEvent(evt, frame, canvas.getWidth(), canvas.getHeight()));
            }
        });
        canvas.addEventListener("mouseout", new EventListener<MouseEvent>() {
            @Override
            public void handleEvent(MouseEvent evt) {
                container.receiveEvent(convertMouseEvent(evt, frame, canvas.getWidth(), canvas.getHeight()));
            }
        });
        canvas.addEventListener("wheel", new EventListener<WheelEvent>() {
            @Override
            public void handleEvent(WheelEvent evt) {
                container.receiveEvent(convertMouseEvent(evt, frame, canvas.getWidth(), canvas.getHeight()));
            }
        });
        Window.current().addEventListener("keydown", new EventListener<KeyboardEvent>() {
            @Override
            public void handleEvent(KeyboardEvent evt) {
                final Widget focused = container.getFrame().getFocusedWidget();
                if (focused != null) {
                    focused.receiveEvent(convertKeyEvent(evt, frame, canvas.getWidth(), canvas.getHeight()));
                }
            }
        });
        Window.current().addEventListener("keyup", new EventListener<KeyboardEvent>() {
            @Override
            public void handleEvent(KeyboardEvent evt) {
                final Widget focused = container.getFrame().getFocusedWidget();
                if (focused != null) {
                    focused.receiveEvent(convertKeyEvent(evt, frame, canvas.getWidth(), canvas.getHeight()));
                }
            }
        });
        needRepaint = true;
        Window.setInterval(new TimerHandler() {
            @Override
            public void onTimer() {
                if (needRepaint) {
                    paintComponent();
                }
            }
        }, 30);
    } //                    final BBox bbox = (BBox) ((PropertyMessage) event.getMessage()).getNewValue();
    //                    final int x = (int) (bbox.getMin(0) + canvas.getWidth()/2.0);
    //                    final int y = (int) (bbox.getMin(1) + canvas.getHeight()/2.0);
    //                    final int w = (int) (bbox.getSpan(0)+1); //+1 because of x/y and span integer rounding
    //                    final int h = (int) (bbox.getSpan(1)+1);
    //                    if (dirty.contains(bbox)) {
    //
    //                    } else {
    //                        dirty.expand(bbox);
    //                    }
    //System.out.println("dirty");

    public void resized() {
        container.setEffectiveExtent(new Extent.Long(canvas.getWidth(), canvas.getHeight()));
    }

    protected synchronized void paintComponent() {
        needRepaint = false;
        dirty.getLower().setAll(0);
        dirty.getUpper().setAll(0);
        final int trsX = (int) (canvas.getWidth() / 2.0);
        final int trsY = (int) (canvas.getHeight() / 2.0);
        final int clipX = 0;
        final int clipY = 0;
        final int clipW = canvas.getWidth();
        final int clipH = canvas.getHeight();
        final BBox box = new BBox(2);
        box.setRange(0, clipX - trsX, clipX + clipW - trsX);
        box.setRange(1, clipY - trsY, clipY + clipH - trsY);
        //swing graphics 2d already has the offset
        final BBox clipBox = new BBox(2);
        clipBox.setRange(0, 0, clipW);
        clipBox.setRange(1, 0, clipH);
        final Affine2 trs = new Affine2();
        trs.setM02(trsX);
        trs.setM12(trsY);
        //            painter.setClip(new science.unlicense.geometry.impl.Rectangle(clipBox));
        //            painter.g.clearRect(0, 0, clipW, clipH);
        painter.setTransform(trs);
        ViewRenderLoop.render(container, painter, box);
        if (firstPaint) {
            firstPaint = false;
            canvas.getStyle().setCssText("");
        }
    }

    private science.unlicense.common.api.event.Event convertMouseEvent(MouseEvent evt, UIFrame frame, int width, int height) {
        double dx = evt.getClientX() - width / 2;
        double dy = evt.getClientY() - height / 2;
        final String type = evt.getType();
        final int button = evt.getButton() + 1;
        final MouseMessage mm;
        if ("click".equals(type)) {
            mm = new MouseMessage(MouseMessage.TYPE_TYPED, button, 1, new Vector2f64(dx, dy), new Vector2f64(dx, dy), 0, false);
        } else if ("mousemove".equals(type)) {
            mm = new MouseMessage(MouseMessage.TYPE_MOVE, 0, 0, new Vector2f64(dx, dy), new Vector2f64(dx, dy), 0, false);
        } else if ("mousedown".equals(type)) {
            mm = new MouseMessage(MouseMessage.TYPE_PRESS, button, 1, new Vector2f64(dx, dy), new Vector2f64(dx, dy), 0, false);
        } else if ("mouseup".equals(type)) {
            mm = new MouseMessage(MouseMessage.TYPE_RELEASE, button, 1, new Vector2f64(dx, dy), new Vector2f64(dx, dy), 0, false);
        } else if ("mouseover".equals(type)) {
            mm = new MouseMessage(MouseMessage.TYPE_ENTER, 0, 0, new Vector2f64(dx, dy), new Vector2f64(dx, dy), 0, false);
        } else if ("mouseout".equals(type)) {
            mm = new MouseMessage(MouseMessage.TYPE_EXIT, 0, 0, new Vector2f64(dx, dy), new Vector2f64(dx, dy), 0, false);
        } else if ("wheel".equals(type)) {
            final WheelEvent we = (WheelEvent) evt;
            mm = new MouseMessage(MouseMessage.TYPE_WHEEL, 0, 0, new Vector2f64(dx, dy), new Vector2f64(dx, dy), -(we.getDeltaY() / 20), false);
        } else {
            throw new RuntimeException("Unexpected type " + type);
        }
        final science.unlicense.common.api.event.Event event = new science.unlicense.common.api.event.Event(frame, mm);
        return event;
    }

    private science.unlicense.common.api.event.Event convertKeyEvent(KeyboardEvent evt, UIFrame frame, int width, int height) {
        final String type = evt.getType();
        final int charCode = evt.getCharCode();
        final int location = evt.getLocation();
        final String code = evt.getCode();
        final int keyCode = evt.getKeyCode();
        final KeyMessage mm;
        if ("keydown".equals(type)) {
            mm = new KeyMessage(KeyMessage.TYPE_PRESS, keyCode, charCode);
        } else if ("keyup".equals(type)) {
            mm = new KeyMessage(KeyMessage.TYPE_RELEASE, keyCode, charCode);
        } else {
            throw new RuntimeException("Unexpected type " + type);
        }
        final science.unlicense.common.api.event.Event event = new science.unlicense.common.api.event.Event(frame, mm);
        return event;
    }

}
