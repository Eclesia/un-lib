

package science.unlicense.system.teavm.mfs;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public final class TeaVMFileFormat implements PathFormat{

    public static final TeaVMFileFormat INSTANCE = new TeaVMFileFormat();

    private final TeaVMFileResolver resolver;

    private TeaVMFileFormat() {
        this.resolver = new TeaVMFileResolver(this);
    }

    @Override
    public boolean isAbsolute() {
        return true;
    }

    @Override
    public boolean canCreate(Path base) throws IOException {
        return false;
    }

    @Override
    public PathResolver createResolver(Path base) {
        return resolver;
    }

    @Override
    public Chars getPrefix() {
        return new Chars("file");
    }

}
