
package science.unlicense.system.teavm;

import org.teavm.jso.typedarrays.ArrayBufferView;
import science.unlicense.common.api.buffer.AbstractBuffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMBuffer extends AbstractBuffer {

    private final ArrayBufferView tvmBuffer;

    public TeaVMBuffer(ArrayBufferView tvmBuffer, NumberType primitiveType, Endianness encoding, BufferFactory factory) {
        super(primitiveType, encoding, factory);
        this.tvmBuffer = tvmBuffer;
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public long getByteSize() {
        return tvmBuffer.getByteLength();
    }

    @Override
    public Object getBackEnd() {
        return tvmBuffer;
    }

    @Override
    public byte readInt8(long offset) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void writeInt8(byte value, long offset) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
