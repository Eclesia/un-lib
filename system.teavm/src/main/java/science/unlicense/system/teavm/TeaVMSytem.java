
package science.unlicense.system.teavm;

import science.unlicense.system.GlobalProperties;
import science.unlicense.system.ModuleManager;
import science.unlicense.system.SocketManager;
import science.unlicense.system.System;
import science.unlicense.system.Work;
import science.unlicense.system.WorkUnits;
import science.unlicense.system.api.desktop.DefaultTransferBag;
import science.unlicense.system.api.desktop.TransferBag;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMSytem extends System {

    private static final ModuleManager MODULE_MANAGER = new TeaVMModuleManager();
    private static final SocketManager SOCKET_MANAGER = new TeaVMSocketManager();
    private static final GlobalProperties PROPERTIES = new TeaVMGlobalProperties();
    private static final TransferBag CLIPBOARD = new TeaVMClipBoard();
    private static final TransferBag DRAGANDDROP = new DefaultTransferBag();

    @Override
    public String getType() {
        return "teavm";
    }

    @Override
    public ModuleManager getModuleManager() {
        return MODULE_MANAGER;
    }

    @Override
    public SocketManager getSocketManager() {
        return SOCKET_MANAGER;
    }

    @Override
    public GlobalProperties getProperties() {
        return PROPERTIES;
    }

    @Override
    public TransferBag getClipBoard() {
        return CLIPBOARD;
    }

    @Override
    public TransferBag getDragAndDrapBag() {
        return DRAGANDDROP;
    }

    @Override
    public WorkUnits createWorkUnits(float priority) {
        return new TeaVMWorkUnits();
    }

    private static class TeaVMWorkUnits implements WorkUnits {

        @Override
        public void submit(long range, Work work) {
            for (long i=0;i<range;i++) {
                work.run(i);
            }
        }

        @Override
        public void dispose() {
        }

    }
}
