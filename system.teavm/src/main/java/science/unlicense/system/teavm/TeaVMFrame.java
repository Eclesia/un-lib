
package science.unlicense.system.teavm;

import org.teavm.jso.browser.Window;
import org.teavm.jso.dom.html.HTMLCanvasElement;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.visual.VirtualFrame;
import science.unlicense.engine.ui.widget.WContainer;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMFrame extends VirtualFrame {

    private final TeaVMPane pane;

    public TeaVMFrame(HTMLCanvasElement canvas) {
        this.pane = new TeaVMPane(this, canvas);
    }

    @Override
    public Painter2D getPainter() {
        return pane.painter;
    }

    @Override
    public synchronized WContainer getContainer() {
        return pane.container;
    }

    @Override
    public void setSize(int width, int height) {
        super.setSize(width, height);
        pane.canvas.setWidth(Window.current().getInnerWidth());
        pane.canvas.setHeight(Window.current().getInnerHeight());
        pane.resized();
    }

}
