
package science.unlicense.system.teavm;

import org.teavm.jso.canvas.CanvasGradient;
import org.teavm.jso.canvas.CanvasRenderingContext2D;
import org.teavm.jso.canvas.ImageData;
import org.teavm.jso.dom.html.HTMLCanvasElement;
import org.teavm.jso.typedarrays.Uint8ClampedArray;
import science.unlicense.common.api.buffer.DataCursor;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.painter2d.AbstractPainter2D;
import science.unlicense.display.api.painter2d.Brush;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.DefaultPainterState;
import science.unlicense.display.api.painter2d.LinearGradientPaint;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.display.api.painter2d.PainterState;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.math.api.Affine;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector4f64;

/**
 *
 * @author Johann Sorel
 */
class TeaVMPainter extends AbstractPainter2D {

    private final HTMLCanvasElement canvas;
    private final CanvasRenderingContext2D g;
    private final FontStore fs;

    public TeaVMPainter(HTMLCanvasElement canvas) {
        this.canvas = canvas;
        g = (CanvasRenderingContext2D) canvas.getContext("2d");
        g.save();
        fs = new TeaVMFontStore(canvas, g);
    }

    @Override
    public FontStore getFontStore() {
        return fs;
    }

    @Override
    public void setClip(PlanarGeometry geom) {
        super.setClip(geom);
        g.restore();
        g.save();
        if (geom != null) {
            g.setTransform(1, 0, 0, 1, 0, 0);
            pushGeometry(geom, true);
        }
    }

    @Override
    public void fill(PlanarGeometry geom) {
        g.setTransform(transform.getM00(), transform.getM10(), transform.getM01(), transform.getM11(), transform.getM02(), transform.getM12());
        if (fillPaint instanceof ColorPaint) {
            Color color = ((ColorPaint) fillPaint).getColor();
            g.setFillStyle(rgba(color));
            pushGeometry(geom, false);
            g.fill();
        } else if (fillPaint instanceof LinearGradientPaint) {
            final LinearGradientPaint lgp = (LinearGradientPaint) fillPaint;
            final CanvasGradient gradient = g.createLinearGradient(lgp.getStartX(), lgp.getStartY(), lgp.getEndX(), lgp.getEndY());
            final Color[] stopColors = lgp.getStopColors();
            final double[] stopOffsets = lgp.getStopOffsets();
            for (int i = 0; i < stopColors.length; i++) {
                gradient.addColorStop(stopOffsets[i], rgba(stopColors[i]));
            }
            g.setFillStyle(gradient);
            pushGeometry(geom, false);
            g.fill();
        }
    }

    @Override
    public void stroke(PlanarGeometry geom) {
        if (brush == null || geom == null) return;

        g.setLineWidth(brush.getMaxWidth());
        g.setTransform(transform.getM00(), transform.getM10(), transform.getM01(), transform.getM11(), transform.getM02(), transform.getM12());
        if (fillPaint instanceof ColorPaint) {
            final Color color = ((ColorPaint) fillPaint).getColor();
            g.setStrokeStyle(rgba(color));
            pushGeometry(geom, false);
            g.stroke();
        }
    }

    @Override
    public void paint(Image image, Affine transform) {

        final Affine2 base = getTransform();
        if (transform != null) {
            Affine2 t = (Affine2) base.multiply(transform);
            g.setTransform(t.getM00(), t.getM10(), t.getM01(), t.getM11(), t.getM02(), t.getM12());
        } else {
            g.setTransform(base.getM00(), base.getM10(), base.getM01(), base.getM11(), base.getM02(), base.getM12());
        }

        image = makeCompatible(image);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final byte[] datas = (byte[]) image.getDataBuffer().getBackEnd();

        final HTMLCanvasElement c = (HTMLCanvasElement) canvas.getOwnerDocument().createElement("canvas");
        c.setWidth(width);
        c.setHeight(height);
        final CanvasRenderingContext2D ctx = (CanvasRenderingContext2D) c.getContext("2d");
        final ImageData id = ctx.createImageData(width, height);
        final Uint8ClampedArray array = id.getData();
        for (int i=0;i<datas.length;i++) {
            array.set(i, datas[i] & 0xFF);
        }
        ctx.putImageData(id, 0, 0);
        g.drawImage(c, 0, 0);
        setTransform(base);
    }

    private static Image makeCompatible(Image image) {
        final ImageModel cm = image.getColorModel();
        if (cm instanceof InterleavedModel) {
            final InterleavedModel dcm = (InterleavedModel) cm;
            final ColorSystem cs = (ColorSystem) dcm.getSampleSystem();

            if (ColorSystem.RGBA_8BITS.equals(cs)) {
                return image;
            }
        }

        //format can not be supported directly by canvas.
        //convert it to a RGBA buffer.
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final Image rgba = Images.create(new Extent.Long(width, height),
                Images.IMAGE_TYPE_RGBA);

        final DataCursor imageData = rgba.getDataBuffer().dataCursor();
        final Vector2i32 coord = new Vector2i32();
        final float[] rgbapre = new float[4];
        for (coord.y = 0; coord.y <height; coord.y++) {
            for (coord.x = 0; coord.x < width; coord.x++) {
                final Color color = image.getColor(coord);
                color.toRGBA(rgbapre, 0);
                imageData.writeByte((byte) (rgbapre[0] * 255));
                imageData.writeByte((byte) (rgbapre[1] * 255));
                imageData.writeByte((byte) (rgbapre[2] * 255));
                imageData.writeByte((byte) (rgbapre[3] * 255));
            }
        }

        return rgba;
    }

    @Override
    public void fill(CharArray text, float x, float y) {
        g.setTransform(transform.getM00(), transform.getM10(), transform.getM01(), transform.getM11(), transform.getM02(), transform.getM12());
        g.setTextBaseline("Alphabetic");
        g.setTextAlign("left");
        if (fillPaint instanceof ColorPaint) {
            final Color color = ((ColorPaint) fillPaint).getColor();
            g.setFillStyle(rgba(color));
        }
        g.setFont("12px Arial");
        g.fillText(text.toString(), x, y);
    }

    @Override
    public void dispose() {
    }

    @Override
    public PainterState saveState() {
        g.save();
        return new TeaVMPainterState(
                getTransform(),
                getClip(),
                getAlphaBlending(),
                getPaint(),
                getBrush(),
                getFont());
    }

    @Override
    public void restoreState(PainterState state) {
        g.restore();
        //store values but not cause any canvas action
        super.setTransform(state.getTransform());
        super.setClip(state.getClip());
        super.setAlphaBlending(state.getAlphaBlending());
        super.setPaint(state.getPaint());
        super.setBrush(state.getBrush());
        super.setFont(state.getFont());
    }

    private void pushGeometry(PlanarGeometry geom, boolean clip) {
        if (geom == null) {
            return;
        }
        if (geom instanceof Rectangle) {
            //most common case
            final Rectangle rec = (Rectangle) geom;
            g.beginPath();
            g.rect(rec.getX(), rec.getY(), rec.getWidth(), rec.getHeight());
            if (clip) {
                g.clip();
            }
        } else if (geom instanceof Point) {
            final Point p = (Point) geom;
            g.beginPath();
            g.moveTo(p.getCoordinate().get(0), p.getCoordinate().get(1));
            g.closePath();
        } else {
            final PathIterator ite = geom.createPathIterator();
            final Vector4f64 p = new Vector4f64();
            final Vector4f64 c1 = new Vector4f64();
            final Vector4f64 c2 = new Vector4f64();
            g.beginPath();
            while (ite.next()) {
                final int type = ite.getType();
                if (type == PathIterator.TYPE_MOVE_TO) {
                    ite.getPosition(p);
                    g.moveTo(p.x, p.y);
                } else if (type == PathIterator.TYPE_LINE_TO) {
                    ite.getPosition(p);
                    g.lineTo(p.x, p.y);
                } else if (type == PathIterator.TYPE_ARC) {
                    ite.getPosition(p);
                    g.lineTo(p.x, p.y);
                } else if (type == PathIterator.TYPE_CUBIC) {
                    ite.getPosition(p);
                    ite.getFirstControl(c1);
                    ite.getSecondControl(c2);
                    g.bezierCurveTo(c1.x, c1.y, c2.x, c2.y, p.x, p.y);
                } else if (type == PathIterator.TYPE_QUADRATIC) {
                    ite.getPosition(p);
                    ite.getFirstControl(c1);
                    g.quadraticCurveTo(c1.x, c1.y, p.x, p.y);
                } else if (type == PathIterator.TYPE_CLOSE) {
                    if (clip) {
                        g.clip();
                    } else {
                        g.closePath();
                    }
                }
            }
        }
    }

    private static String rgba(Color color) {
        final StringBuilder sb = new StringBuilder("rgba(");
        sb.append(color.getRed()*255f);
        sb.append(',');
        sb.append(color.getGreen()*255f);
        sb.append(',');
        sb.append(color.getBlue()*255f);
        sb.append(',');
        sb.append(color.getAlpha());
        sb.append(')');
        return sb.toString();
    }


    private static class TeaVMPainterState extends DefaultPainterState {

        public TeaVMPainterState(Affine2 transform, PlanarGeometry clip, AlphaBlending blending, Paint paint, Brush brush, FontChoice font) {
            super(transform, clip, blending, paint, brush, font);
        }

    }
}
