
package science.unlicense.system.teavm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.IPAddress;
import science.unlicense.system.ServerSocket;
import science.unlicense.system.SocketManager;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMSocketManager implements SocketManager {

    @Override
    public IPAddress resolve(Chars name) throws IOException {
        throw new IOException("Not supported.");
    }

    @Override
    public ClientSocket createClientSocket(Chars host, int port) throws IOException {
        throw new IOException("Not supported.");
    }

    @Override
    public ClientSocket createClientSocket(IPAddress host, int port) throws IOException {
        throw new IOException("Not supported.");
    }

    @Override
    public ServerSocket createServerSocket(int port, ServerSocket.ClientHandler handler) throws IOException {
        throw new IOException("Not supported.");
    }

}
