
package science.unlicense.system.teavm.mfs;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMFileResolver implements PathResolver{

    private static final Chars PREFIX = Chars.constant("file:");
    private final PathFormat format;

    public static final TeaVMPath ROOT = new TeaVMPath(null, new Chars("/"));

    public TeaVMFileResolver(PathFormat format) {
        this.format = format;
    }

    @Override
    public PathFormat getPathFormat() {
        return format;
    }

    @Override
    public Path resolve(Chars path) {
        if (!path.startsWith(PREFIX)){
            return null;
        }
        path = path.truncate(5,-1);
        return ROOT.resolve(path);
    }

}
