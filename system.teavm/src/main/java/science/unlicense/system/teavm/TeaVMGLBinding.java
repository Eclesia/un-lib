
package science.unlicense.system.teavm;

import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.display.api.layout.Margin;
import science.unlicense.gpu.api.SharedContext;
import science.unlicense.gpu.api.SharedContextException;
import science.unlicense.gpu.api.opengl.GLBinding;
import science.unlicense.gpu.api.opengl.GLFrame;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.system.teavm.mfs.TeaVMGLSource;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMGLBinding implements GLBinding {

    @Override
    public GLSource createOffScreen(int width, int height, SharedContext context) {
        if (context != null) throw new SharedContextException();
        return new TeaVMGLSource(this, null);
    }

    @Override
    public GLFrame createFrame(boolean opaque, SharedContext context) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public Margin getFrameMargin() {
        return new Margin();
    }

    @Override
    public BufferFactory getBufferFactory() {
        return TeaVMBufferFactory.INSTANCE;
    }

}
