
package science.unlicense.system.teavm;

import org.teavm.jso.typedarrays.Int8Array;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Int8;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMBufferFactory implements BufferFactory {

    public static final TeaVMBufferFactory INSTANCE = new TeaVMBufferFactory();

    private TeaVMBufferFactory(){}

    @Override
    public Buffer.Int8 createInt8(long nbValue) {
        final Int8Array tvmBuffer = Int8Array.create((int) nbValue);
        return new TeaVMBuffer(tvmBuffer, Int8.TYPE, Endianness.BIG_ENDIAN, this).asInt8();
    }

    @Override
    public Buffer.Int32 createInt32(long nbValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Buffer.Int64 createInt64(long nbValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Buffer.Float32 createFloat32(long nbValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Buffer.Float64 createFloat64(long nbValue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Buffer create(long nbValue, NumberType numType, Endianness endianness) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
