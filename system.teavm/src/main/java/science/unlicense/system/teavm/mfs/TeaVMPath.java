
package science.unlicense.system.teavm.mfs;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.AbstractPath;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMPath extends AbstractPath {

    private final String vuid = "V"+System.identityHashCode(this);
    private final Chars name;
    private final Sequence children = new ArraySequence();
    private final TeaVMPath parent;

    private boolean isContainer = false;
    private boolean exist = false;
    private ByteSequence data = null;

//    public TeaVMPath(Chars name) {
//        this.parent = null;
//        this.name = name;
//    }

    public TeaVMPath(TeaVMPath parent, Chars name) {
        this.parent = parent;
        this.name = name;
    }

    public void removeChild(int index) {
        children.remove(index);
    }

    @Override
    public Sequence getChildren() {
        return Collections.readOnlySequence(children);
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[]{};
    }

    @Override
    public Chars getName() {
        return name;
    }

    @Override
    public Path getParent() {
        return null;
    }

    @Override
    public boolean isContainer() {
        return isContainer;
    }

    @Override
    public boolean exists() {
        return exist;
    }

    @Override
    public Object getPathInfo(Chars key) {
        if (!isContainer && Path.INFO_OCTETSIZE.equals(key) && data!=null) {
            return data.getSize();
        }
        return super.getPathInfo(key);
    }

    @Override
    public boolean createContainer() throws IOException {
        if (exist) {
            if (isContainer) {
                return true;
            } else {
                throw new IOException("Path already exist and is not a container.");
            }
        }
        exist = true;
        isContainer = true;
        return true;
    }

    @Override
    public boolean createLeaf() throws IOException {
        if (exist) {
            if (isContainer) {
                throw new IOException("Path already exist and is a container.");
            } else {
                return true;
            }
        }
        exist = true;
        isContainer = false;
        data = new ByteSequence();
        return true;
    }

    @Override
    public Path resolve(Chars address) {
        Chars[] segments = address.split('/');
        Path base = this;

        loopsegment:
        for (int i=0;i<segments.length;i++){
            if (segments[i].isEmpty() || segments[i].equals(new Chars("."))) continue;
            if (segments[i].equals(new Chars(".."))){
                base = base.getParent();
            } else {
                Iterator ite = base.getChildren().createIterator();
                while (ite.hasNext()){
                    Path next = (Path) ite.next();
                    if (next.getName().equals(segments[i])){
                        base = next;
                        continue loopsegment;
                    }
                }
                //segment do not exist create it.
                //TODO should not create it
                final TeaVMPath p = new TeaVMPath(this, segments[i]);
                children.add(p);
                base = p;
            }
        }

        return base;
    }

    @Override
    public PathResolver getResolver() {
        return TeaVMFileFormat.INSTANCE.createResolver(null);
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        if (exist && !isContainer){
            return new ArrayInputStream(data.toArrayByte());
        } else {
            throw new IOException("Path do not exist");
        }
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        if (exist) {
            if (isContainer) {
                throw new IOException("Path is a container.");
            } else {
                return new ArrayOutputStream(data);
            }
        } else {
            createLeaf();
            return new ArrayOutputStream(data);
        }
    }

    @Override
    public Chars toURI() {
        final PathResolver resolver = getResolver();
        final Chars relativePath = Paths.relativePath(TeaVMFileResolver.ROOT, this);
        return new Chars(resolver.getPathFormat().getPrefix()+":/"+relativePath);
    }

}
