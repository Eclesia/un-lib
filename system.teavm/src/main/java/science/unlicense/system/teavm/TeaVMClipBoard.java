
package science.unlicense.system.teavm;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.system.api.desktop.AbstractTransferBag;

/**
 *
 * @author Johann Sorel
 */
public class TeaVMClipBoard extends AbstractTransferBag {

    private final Sequence attachments = new ArraySequence();

    @Override
    public Sequence getAttachments() {
        return attachments;
    }


}
