
package science.unlicense.runtime;

import science.unlicense.system.System;
import science.unlicense.system.teavm.TeaVMSytem;

/**
 *
 * @author Johann Sorel
 */
public final class Runtime {

    private static final TeaVMSytem SYSTEM = new TeaVMSytem();

    public static void init() {
        System.set(SYSTEM);
    }

    private Runtime(){}

}
