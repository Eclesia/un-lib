
package science.unlicense.format.openmath;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.openmath.model.OMObject;

/**
 *
 * @author Johann Sorel
 */
public class OpenMathBinaryReader extends AbstractReader {

    public OMObject read() throws IOException {
        throw new UnimplementedException();
    }

}
