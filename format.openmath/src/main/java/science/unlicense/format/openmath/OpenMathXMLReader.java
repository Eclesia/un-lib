
package science.unlicense.format.openmath;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.format.openmath.model.OMObject;

/**
 *
 * @author Johann Sorel
 */
public class OpenMathXMLReader extends AbstractReader {

    public static final int OBJ_START1 = 24;
    public static final int OBJ_START2 = 88;
    public static final int OBJ_END = 25;

    public OMObject read() throws IOException {

        final DataInputStream ds = getInputAsDataStream(Endianness.LITTLE_ENDIAN);

        int o = ds.readUByte();
        if (o==OBJ_START1) {

        }


        throw new UnimplementedException();
    }

}
