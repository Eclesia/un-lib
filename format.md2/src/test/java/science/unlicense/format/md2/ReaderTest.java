
package science.unlicense.format.md2;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.model3d.api.Model3Ds;

/**
 *
 * @author Johann Sorel
 */
public class ReaderTest {

    /**
     * Reading test
     */
    @Test
    @Ignore
    public void testRead() throws IOException, StoreException{

        //TODO must find a public model to use in tests
        final Path path = Paths.resolve(new Chars("..."));

        final MD2Store store = (MD2Store) Model3Ds.open(path);
        final SceneNode node = (SceneNode) store.getElements().createIterator().next();

    }

}
