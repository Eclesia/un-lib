
package science.unlicense.format.md2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Quake 2 MD2 format.
 *
 * Documentation from :
 * https://en.wikipedia.org/wiki/MD2_(file_format)
 * http://linux.ucla.edu/~phaethon/q3a/formats/md2-schoenblum.html
 * Thanks to Daniel E. Schoenblum.
 *
 * @author Johann Sorel
 */
public class MD2Format extends AbstractModel3DFormat {

    public static final MD2Format INSTANCE = new MD2Format();

    private MD2Format() {
        super(new Chars("MD2"));
        shortName = new Chars("MD2");
        longName = new Chars("MD2");
        extensions.add(new Chars("md2"));
        signatures.add(MD2Constants.BSIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new MD2Store(input);
    }

}
