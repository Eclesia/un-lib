
package science.unlicense.format.md2.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * There is an array of numSkins skin names stored at offsetSkins into the file.
 * Each skin name is a char[64]. The name is really a path to the skin,
 * relative to the base game directory (baseq2 f or "standard" Quake2).
 * The skin files are regular pcx files.
 *
 * @author Johann Sorel
 */
public class MD2TextureCoordinate {

    /**
     * These two shorts are used to map a vertex onto a skin.
     * The horizontal axis position is given by s, and the vertical axis position is given by t.
     * The range for s is greater than or equal to 0 and less than skinWidth (0 <= s < skinWidth).
     * The range for t is greater than or equal to 0 and less than skinHeight (0 <= s < skinHeight).
     * Note that the ranges are different than in the s and t members of the glCommandVertex structure.
     */
    public short s;
    public short t;

    public void read(DataInputStream ds) throws IOException{
        s = ds.readShort();
        t = ds.readShort();
    }

}
