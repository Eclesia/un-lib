
package science.unlicense.format.md2.model;


/**
 * At offsetGlCommands bytes into the file, there is the gl command list,
 * which is made up of a series of numGlCommands int's and float's,
 * organized into groups. Each group starts with an int. If it is positive,
 * it is followed by that many glCommandVertex_t structures, which form a triangle strip.
 * If it is negative, it is followed by -x glCommandVertex_t structures,
 * which fo rm a triangle fan. A 0 indicates the end of the list.
 * The list is an optimized way of issuing commands when rendering with OpenGl.
 *
 * @author Johann Sorel
 */
public class MD2GLCommand {

    public static final int TYPE_TRIANGLE_STRIP = 0;
    public static final int TYPE_TRIANGLE_FAN = 1;
    public static final int TYPE_END = 2;

    public int type;
    public MD2GLCommandVertex[] vertCommand;

}
