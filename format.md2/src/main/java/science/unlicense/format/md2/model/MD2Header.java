
package science.unlicense.format.md2.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.md2.MD2Constants;

/**
 * MD2 File header.
 * The header comes right at the start of the file. The information in
 * the header is needed to load different parts of the model.
 *
 * @author Johann Sorel (Java code)
 */
public class MD2Header {

    /**
     * A "magic number" used to identify the file. The magic number
     * is 844121161 in decimal (0x32504449 in hexadecimal).
     * The magic number is equal to the int "IDP2" (id polygon 2),
     * which is formed by ('I' + ('D' << 8) + ('P' << 16) + ('2' << 24)).
     */
    public int magic;
    /**
     * Version number of the file. Always 8.
     */
    public int version;
    /**
     * Width of the skin(s) in pixels.
     */
    public int skinWidth;
    /**
     * Height of the skin(s) in pixels.
     */
    public int skinHeight;
    /**
     * Size of each frame in bytes.
     */
    public int frameSize;
    /**
     * Number of skins associated with this model.
     */
    public int numSkins;
    /**
     * Number of vertices in each frame.
     */
    public int numVertices;
    /**
     * Number of texture coordinates (not necessarily the same as the number of vertices).
     */
    public int numTexCoords;
    /**
     * Number of triangles in each frame.
     */
    public int numTriangles;
    /**
     * Number of dwords (4 bytes) in the gl command list.
     */
    public int numGlCommands;
    /**
     * Number of frames.
     */
    public int numFrames;
    /**
     * Offset, in bytes from the start of the file, to the list of skin names.
     */
    public int offsetSkins;
    /**
     * Offset, in bytes from the start of the file, to the list of texture coordinates.
     */
    public int offsetTexCoords;
    /**
     * Offset, in bytes from the start of the file, to the list of triangles.
     */
    public int offsetTriangles;
    /**
     * Offset, in bytes from the start of the file, to the list of frames.
     */
    public int offsetFrames;
    /**
     * Offset, in bytes from the start of the file, to the gl command list.
     */
    public int offsetGlCommands;
    /**
     * Offset, in bytes from the start of the file, to the end (size of the file).
     */
    public int offsetEnd;

    public void read(DataInputStream ds) throws IOException{
        magic            = MD2Constants.SIGNATURE;
        version          = ds.readInt();
        skinWidth        = ds.readInt();
        skinHeight       = ds.readInt();
        frameSize        = ds.readInt();
        numSkins         = ds.readInt();
        numVertices      = ds.readInt();
        numTexCoords     = ds.readInt();
        numTriangles     = ds.readInt();
        numGlCommands    = ds.readInt();
        numFrames        = ds.readInt();
        offsetSkins      = ds.readInt();
        offsetTexCoords  = ds.readInt();
        offsetTriangles  = ds.readInt();
        offsetFrames     = ds.readInt();
        offsetGlCommands = ds.readInt();
        offsetEnd        = ds.readInt();

        if (version != 8){
            throw new IOException(ds, "Unsupported file version "+version+" was expecting 8.");
        }
    }

}
