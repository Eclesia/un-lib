
package science.unlicense.format.md2.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Quake 2 models are made up of only triangles.
 * At offsetTriangles in the file is an array of triangle_t structures.
 * The array has numTriangles structures in it.
 *
 * @author Johann Sorel
 */
public class MD2Triangle {

    /**
     * Size 3 :
     * These three shorts are indices into the array of vertices in each frames.
     * In other words, the number of triangles in a md2 file is fixed, and each
     * triangle is always made of the same three indices into each frame's array
     * of vertices. So, in each frame, the triangles themselves stay intact,
     * their vertices are just moved around.
     */
    public short[] vertexIndices;
    /**
     * Size 3 :
     * These three shorts are indices into the array of texture coordinates.
     */
    public short[] textureIndices;

    public void read(DataInputStream ds) throws IOException {
        vertexIndices = ds.readShort(3);
        textureIndices = ds.readShort(3);
    }

}
