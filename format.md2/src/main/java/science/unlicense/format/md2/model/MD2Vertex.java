
package science.unlicense.format.md2.model;

/**
 * Each frame contains the positions in 3D space for each vertex of each triangle
 * that makes up the model. Quake 2 (and Quake) models contain only triangles.
 *
 * @author Johann Sorel
 */
public class MD2Vertex {

    /**
     * Size 3 :
     * The three bytes represent the x, y, and z coordinates of this vertex.
     * This is not the "real" vertex coordinate. This is a scaled version of the
     * coordinate, scaled so that each of the three numbers fit within one byte.
     * To scale the vertex back to the "real" coordinate, you need to first multiply
     * each of the bytes by their respective float scale in the frame_t structure,
     * and then add the respective float translation< /a>, also in the frame_t structure.
     * This will give you the vertex coordinate relative to the model's origin,
     * which is at the origin, (0, 0, 0).
     */
    public byte[] vertex;
    /**
     * Unsigned byte :
     * This is an index into a table of normals kept by Quake2.
     * To get the table, you need to download this zip file (1.7 MB), released by id,
     * that has the source code to all of the tools they used for quake2.
     */
    public int lightNormalIndex;

}
