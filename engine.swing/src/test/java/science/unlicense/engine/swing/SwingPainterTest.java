package science.unlicense.engine.swing;

import science.unlicense.engine.swing.SwingPainter;
import science.unlicense.display.api.painter2d.ImagePainter2D;
import science.unlicense.display.api.painter2d.ImagePainter2DTest;

/**
 *
 * @author Johann Sorel
 */
public class SwingPainterTest extends ImagePainter2DTest{

    @Override
    protected ImagePainter2D createPainter(int width, int height) {
        return new SwingPainter(width, height);
    }

}
