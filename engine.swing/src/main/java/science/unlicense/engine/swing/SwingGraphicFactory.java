
package science.unlicense.engine.swing;

import java.awt.image.BufferedImage;
import science.unlicense.image.api.Image;
import science.unlicense.display.api.scene.s2d.DefaultGraphic2DFactory;
import science.unlicense.display.api.scene.s2d.ImageNode2D;

/**
 *
 * @author Johann Sorel
 */
public class SwingGraphicFactory extends DefaultGraphic2DFactory {

    @Override
    public ImageNode2D createImageNode() {
        return new SwingImageNode();
    }

    class SwingImageNode extends ImageNode2D {

        private BufferedImage swingImg;

        public BufferedImage getSwingImage() {
            if (swingImg == null && image != null) {
                swingImg = AWTUtils.toAwtImage(image);
            }
            return swingImg;
        }

        @Override
        public void setImage(Image image) {
            if (this.image != image) {
                swingImg = null;
            }
            super.setImage(image);
        }

    }

}
