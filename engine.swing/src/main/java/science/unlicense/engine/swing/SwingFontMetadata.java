
package science.unlicense.engine.swing;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.geom.Rectangle2D;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.geometry.api.BBox;

/**
 *
 * @author Johann Sorel
 */
public class SwingFontMetadata implements FontMetadata {

    private final Font font;
    private final FontMetrics metrics;

    public SwingFontMetadata(Font font) {
        this.font = font;
        this.metrics = SwingFontStore.GRAPHICS.getFontMetrics(font);
    }

    @Override
    public BBox getGlyphBox() {
        final Rectangle2D rect = font.getMaxCharBounds(SwingFontStore.CTX);
        final BBox bbox = new BBox(2);
        bbox.setRange(0, rect.getMinX(), rect.getMaxX());
        bbox.setRange(1, rect.getMinX(), rect.getMaxX());
        return bbox;
    }

    @Override
    public double getHeight() {
        return font.getSize2D();
    }

    @Override
    public double getAscent() {
        return metrics.getMaxAscent();
    }

    @Override
    public double getDescent() {
        //we add +1 because of Graphics2D rendering of glyph which are often
        //rounded to nearest pixel, causing something partial glyphs hiding
        return metrics.getMaxDescent() +1;
    }

    @Override
    public double getLineGap() {
        return metrics.getLeading();
    }

    @Override
    public double getAdvanceWidthMax() {
        return metrics.getMaxAdvance();
    }

    @Override
    public double getAdvanceWidth(int unicode) {
        return metrics.charWidth(unicode);
    }

    @Override
    public double getAdvanceWidth(Char c) {
        return getAdvanceWidth(c.toUnicode());
    }

    @Override
    public double getMinLeftSideBearing() {
        return 0;
    }

    @Override
    public double getMinRightSideBearing() {
        return 0;
    }

    @Override
    public double getXMaxExtent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BBox getCharBox(Char c) {
        final Rectangle2D rec = metrics.getStringBounds(""+(char) c.toUnicode(), SwingFontStore.GRAPHICS);
        return new BBox(new double[]{rec.getMinX(),rec.getMinY()}, new double[]{rec.getMaxX(),rec.getMaxY()});
    }

    @Override
    public BBox getCharsBox(CharArray text) {
        if (text == null) text = Chars.EMPTY;
        final Rectangle2D rec = metrics.getStringBounds(text.toString(), SwingFontStore.GRAPHICS);
        return new BBox(new double[]{rec.getMinX(),rec.getMinY()}, new double[]{rec.getMaxX(),rec.getMaxY()});
    }

    @Override
    public FontMetadata derivate(double fontSize) {
        return new SwingFontMetadata(font.deriveFont((float) fontSize));
    }

}
