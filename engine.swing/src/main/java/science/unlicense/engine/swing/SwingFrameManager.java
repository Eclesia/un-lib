
package science.unlicense.engine.swing;

import java.awt.Dimension;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.desktop.Frame;
import science.unlicense.display.api.desktop.FrameManager;
import science.unlicense.display.api.desktop.cursor.NamedCursor;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public class SwingFrameManager implements FrameManager {

    public static final SwingFrameManager INSTANCE = new SwingFrameManager();
    private static final Chars NAME = Chars.constant("Swing");

    public SwingFrameManager() {
    }

    @Override
    public Chars getName() {
        return NAME;
    }

    @Override
    public boolean isAvailable() {
        return true;
    }

    @Override
    public Chars[] getCursorNames() {
        return new Chars[]{NamedCursor.DEFAULT.getName(),NamedCursor.NULL.getName()};
    }

    @Override
    public UIFrame createFrame(boolean translucent) {
        return createFrame(null, false, translucent);
    }

    @Override
    public UIFrame createFrame(Frame parent, boolean modale, boolean translucent) {
        return new SwingFrame((SwingFrame) parent, modale, translucent);
    }

    @Override
    public Extent getDisplaySize() {
        final Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        return new Extent.Double(screenSize.width, screenSize.height);
    }

    @Override
    public void setIcon(Chars imagePath) {
        //TODO
    }


}
