
package science.unlicense.engine.swing;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import javax.swing.JComponent;
import javax.swing.TransferHandler;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.display.api.desktop.DragAndDropMessage;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.visual.ViewRenderLoop;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.engine.ui.widget.Widgets;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class SwingWContainer extends JComponent {

    final WContainer container = new WContainer();
    final Vector2f64 mousePos = new Vector2f64();

    public SwingWContainer(){

        container.addEventListener(new PropertyPredicate(WContainer.PROPERTY_DIRTY), new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final BBox bbox = (BBox) ((PropertyMessage) event.getMessage()).getNewValue();
                final int x = (int) (bbox.getMin(0) + getWidth()/2.0);
                final int y = (int) (bbox.getMin(1) + getHeight()/2.0);
                final int w = (int) (bbox.getSpan(0)+1); //+1 because of x/y and span integer rounding
                final int h = (int) (bbox.getSpan(1)+1);
                repaint(x,y,w,h);
            }
        });
        container.setInlineStyle(new Chars("background:{fill-paint:@color-background}"));

        setFocusable(true);

        final EventAdaptor adaptor = new EventAdaptor();
        addComponentListener(adaptor);
        addMouseListener(adaptor);
        addMouseMotionListener(adaptor);
        addMouseWheelListener(adaptor);
        addKeyListener(adaptor);

        final Sequence drops = science.unlicense.system.System.get().getDragAndDrapBag().getAttachments();

        setTransferHandler(new TransferHandler() {
            @Override
            public boolean canImport(TransferHandler.TransferSupport support) {
                drops.removeAll();
                final DataFlavor[] dataFlavors = support.getDataFlavors();
                final Transferable transferable = support.getTransferable();
                for (DataFlavor df : dataFlavors) {
                    drops.add(new SwingAttachment(new Chars(df.getMimeType()), df, transferable));
                    System.out.println(new Chars(df.getMimeType()));
                }
                return processDragAndDrop(mousePos, MouseMessage.TYPE_MOVE);

            }

            @Override
            public boolean importData(TransferHandler.TransferSupport support) {
                drops.removeAll();
                final DataFlavor[] dataFlavors = support.getDataFlavors();
                final Transferable transferable = support.getTransferable();
                for (DataFlavor df : dataFlavors) {
                    drops.add(new SwingAttachment(new Chars(df.getMimeType()), df, transferable));
                }
                return processDragAndDrop(mousePos, MouseMessage.TYPE_RELEASE);
            }
        });
    }

    @Override
    protected synchronized void paintComponent(Graphics g) {
        super.paintComponent(g);
        final Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        final int trsX = (int) (getWidth()/2.0);
        final int trsY = (int) (getHeight()/2.0);

        Rectangle clip = g2d.getClipBounds();

        final BBox box = new BBox(2);
        box.setRange(0, clip.x-trsX, clip.x+clip.width-trsX);
        box.setRange(1, clip.y-trsY, clip.y+clip.height-trsY);

        //swing graphics 2d already has the offset
        final BBox clipBox = new BBox(2);
        clipBox.setRange(0, 0, clip.width);
        clipBox.setRange(1, 0, clip.height);

        final AffineTransform trs = new AffineTransform(g2d.getTransform());
        trs.translate(trsX,trsY);

        final SwingPainter painter = new SwingPainter(g2d);
        painter.setClip(new science.unlicense.geometry.impl.Rectangle(clipBox));
        painter.setTransform(AWTUtils.toAffine(trs));
        ViewRenderLoop.render(container, painter, box);
    }

    private Tuple getMousePosition(MouseEvent e) {
        Vector2f64 t = new Vector2f64(
                e.getX()-e.getComponent().getWidth()/2.0,
                e.getY()-e.getComponent().getHeight()/2.0);
        mousePos.set(t);
        return t;
    }

    private static Tuple getScreenPosition(MouseEvent e) {
        return new Vector2f64(e.getXOnScreen(), e.getYOnScreen());
    }

    private static int getMouseButton(MouseEvent e) {
        int button = e.getButton();
        if (button==MouseEvent.BUTTON1) {
            return MouseMessage.BUTTON_1;
        } else if (button==MouseEvent.BUTTON2) {
            return MouseMessage.BUTTON_2;
        } else if (button==MouseEvent.BUTTON3) {
            return MouseMessage.BUTTON_3;
        } else {
            //todo
            return -1;
        }
    }

    private boolean processDragAndDrop(Tuple inWidgetPosition, int type) {

        //handle drag & drops events
        final Sequence drops = science.unlicense.system.System.get().getDragAndDrapBag().getAttachments();
        if (!drops.isEmpty()) {
            if (type==MouseMessage.TYPE_RELEASE) {
                final DragAndDropMessage dropEvent = new DragAndDropMessage(DragAndDropMessage.TYPE_DROP);
                //check for drag & drop objects
                final Sequence stack = Widgets.pruneStackAt(container, inWidgetPosition.get(0), inWidgetPosition.get(1));
                //search for the first widget to accept the drop
                for (int i=stack.getSize()-1;i>=0;i--){
                    final Widget comp = (Widget) stack.get(i);
                    comp.receiveEvent(new Event(null,dropEvent));
                    if (dropEvent.isConsumed()){
                        break;
                    }
                }
                //remove all drop requests consumed or not
                drops.removeAll();

            } else if (type==MouseMessage.TYPE_MOVE) {
                //TODO sen drag&drop enter/exit events
                final DragAndDropMessage dropEvent = new DragAndDropMessage(DragAndDropMessage.TYPE_ENTER);
                //check for drag & drop objects
                final Sequence stack = Widgets.pruneStackAt(container, inWidgetPosition.get(0), inWidgetPosition.get(1));
                //search for the first widget to accept the drop
                for (int i=stack.getSize()-1;i>=0;i--){
                    final Widget comp = (Widget) stack.get(i);
                    comp.receiveEvent(new Event(null,dropEvent));
                    if (dropEvent.isConsumed()){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private class EventAdaptor implements MouseListener, MouseMotionListener, MouseWheelListener, ComponentListener, KeyListener {

        @Override
        public void componentResized(ComponentEvent e) {
            container.setEffectiveExtent(new Extent.Long(getWidth(), getHeight()));
        }

        @Override
        public void componentMoved(ComponentEvent e) {
        }

        @Override
        public void componentShown(ComponentEvent e) {
        }

        @Override
        public void componentHidden(ComponentEvent e) {
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_MOVE,
                    getMouseButton(e), 0, getMousePosition(e), getScreenPosition(e), 0, true);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_MOVE,
                    getMouseButton(e), 0, getMousePosition(e), getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_TYPED,
                    getMouseButton(e), e.getClickCount(), getMousePosition(e), getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mousePressed(MouseEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_PRESS,
                    getMouseButton(e), e.getClickCount(), getMousePosition(e), getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            final Tuple mousePos = getMousePosition(e);
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_RELEASE,
                    getMouseButton(e), e.getClickCount(), mousePos, getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            requestFocus();
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_ENTER,
                    getMouseButton(e), e.getClickCount(), getMousePosition(e), getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mouseExited(MouseEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_EXIT,
                    getMouseButton(e), e.getClickCount(), getMousePosition(e), getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_WHEEL,
                    getMouseButton(e), e.getClickCount(), getMousePosition(e), getScreenPosition(e), -e.getWheelRotation(), false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            //send the keyboard event only to the focused widget
            System.out.println(e);
            final UIFrame frame = container.getFrame();
            if (frame==null) return;
            final Widget focusedWidget = frame.getFocusedWidget();
            if (focusedWidget==null) return;
            final KeyMessage message = new KeyMessage(KeyMessage.TYPE_PRESS,toCodePoint(e),toUnKeyCode(e.getKeyCode()));
            focusedWidget.receiveEvent(new Event(null, message));
        }

        @Override
        public void keyReleased(KeyEvent e) {
            //send the keyboard event only to the focused widget
            System.out.println(e);
            final UIFrame frame = container.getFrame();
            if (frame==null) return;
            final Widget focusedWidget = frame.getFocusedWidget();
            if (focusedWidget==null) return;
            final KeyMessage message = new KeyMessage(KeyMessage.TYPE_RELEASE,toCodePoint(e),toUnKeyCode(e.getKeyCode()));
            focusedWidget.receiveEvent(new Event(null, message));
        }

        private int toCodePoint(KeyEvent e) {
            char t = e.getKeyChar();
            return t == KeyEvent.CHAR_UNDEFINED ? 0 : (t & 0xFFFF);
        }

    }

    private static int toUnKeyCode(int keyCode){
        switch(keyCode){
            case KeyEvent.VK_BEGIN :        return KeyMessage.KC_BEGIN;
            case KeyEvent.VK_END :          return KeyMessage.KC_END;
            case KeyEvent.VK_ENTER :        return KeyMessage.KC_ENTER;
            case KeyEvent.VK_DELETE :       return KeyMessage.KC_DELETE;
            case KeyEvent.VK_TAB :          return KeyMessage.KC_TAB;
            case KeyEvent.VK_CONTROL :      return KeyMessage.KC_CONTROL;
            case KeyEvent.VK_ALT :          return KeyMessage.KC_ALT;
            case KeyEvent.VK_SHIFT :        return KeyMessage.KC_SHIFT;
            case KeyEvent.VK_BACK_SPACE :   return KeyMessage.KC_BACKSPACE;
            case KeyEvent.VK_LEFT :         return KeyMessage.KC_LEFT;
            case KeyEvent.VK_UP :           return KeyMessage.KC_UP;
            case KeyEvent.VK_RIGHT :        return KeyMessage.KC_RIGHT;
            case KeyEvent.VK_DOWN :         return KeyMessage.KC_DOWN;
            default: return 0;
        }
    }

}
