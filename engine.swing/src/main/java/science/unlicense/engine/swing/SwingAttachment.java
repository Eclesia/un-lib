
package science.unlicense.engine.swing;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.system.api.desktop.Attachment;
import science.unlicense.system.jvm.file.FilePath;
import science.unlicense.system.jvm.file.FileResolver;

/**
 *
 * @author Johann Sorel
 */
public class SwingAttachment implements Attachment {

    private final Dictionary metas = new HashDictionary();
    private final DataFlavor flavor;
    private final Transferable transferable;

    public SwingAttachment(Chars mimeType, DataFlavor flavor, Transferable transferable) {
        this.metas.add(META_MIME_TYPE, mimeType);
        this.flavor = flavor;
        this.transferable = transferable;
    }

    @Override
    public Dictionary getMetadatas() {
        return metas;
    }

    @Override
    public Object getObject() {
        Object value = null;
        try {
            value = transferable.getTransferData(flavor);
        } catch (UnsupportedFlavorException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        if (flavor.isFlavorJavaFileListType()) {
            final List lst = (List) value;
            final Sequence seq = new ArraySequence();
            for (int i=0,n=lst.size(); i<n; i++) {
                final Path path = new FilePath(new FileResolver(), (File) lst.get(i));
                seq.add(path);
            }
            value = seq;
        }
        return value;
    }

}
