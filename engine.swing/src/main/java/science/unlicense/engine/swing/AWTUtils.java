
package science.unlicense.engine.swing;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.TexturePaint;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.display.api.painter2d.Brush;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.LinearGradientPaint;
import science.unlicense.display.api.painter2d.PatternPaint;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.display.api.painter2d.RadialGradientPaint;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.Path;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector4f64;

/**
 *
 * @author Johann Sorel
 */
public class AWTUtils {

    private static final int[] MAPPING_RGBA_1230 = new int[]{1,2,3,0};
    private static final int[] MAPPING_RGBA_0123 = new int[]{0,1,2,3};

    private AWTUtils(){}

    public static Shape toShape(PlanarGeometry geom){
        if (geom==null) return null;

        if (geom instanceof Point) {
            final Point g = (Point) geom;
            final TupleRW coord = g.getCoordinate();
            return new Rectangle2D.Double(coord.get(0), coord.get(1), 0, 0);
        } else if (geom instanceof Rectangle) {
            final Rectangle g = (Rectangle) geom;
            return new Rectangle2D.Double(g.getX(), g.getY(), g.getWidth(), g.getHeight());
        } else {
            final PathIterator ite = geom.createPathIterator();
            final GeneralPath path = new GeneralPath();

            final Vector4f64 p = new Vector4f64();
            final Vector4f64 c1 = new Vector4f64();
            final Vector4f64 c2 = new Vector4f64();
            while (ite.next()){
                final int type = ite.getType();
                if (type==PathIterator.TYPE_MOVE_TO) {
                    ite.getPosition(p);
                    path.moveTo(p.get(0), p.get(1));
                } else if (type==PathIterator.TYPE_LINE_TO) {
                    ite.getPosition(p);
                    path.lineTo(p.get(0), p.get(1));
                } else if (type==PathIterator.TYPE_ARC) {
                    ite.getPosition(p);
                    path.lineTo(p.get(0), p.get(1));
                } else if (type==PathIterator.TYPE_CUBIC) {
                    ite.getPosition(p);
                    ite.getFirstControl(c1);
                    ite.getSecondControl(c2);
                    path.curveTo(c1.get(0), c1.get(1), c2.get(0), c2.get(1), p.get(0), p.get(1));
                } else if (type==PathIterator.TYPE_QUADRATIC) {
                    ite.getPosition(p);
                    ite.getFirstControl(c1);
                    path.quadTo(c1.get(0), c1.get(1), p.get(0), p.get(1));
                } else if (type==PathIterator.TYPE_CLOSE) {
                    path.closePath();
                }
            }
            return path;
        }
    }

    public static BBox toBBox(Rectangle2D shape) {
        if (shape==null) return null;

        final BBox box = new BBox(2);
        box.setRange(0, shape.getX(), shape.getX()+shape.getWidth());
        box.setRange(1, shape.getY(), shape.getY()+shape.getHeight());
        return box;
    }

    public static PlanarGeometry toGeometry2D(Shape shape) {
        if (shape==null) return null;

        final java.awt.geom.PathIterator ite = shape.getPathIterator(null);

        final Path path = new Path();

        final double[] coords = new double[8];
        for (;!ite.isDone(); ite.next()) {
            final int type = ite.currentSegment(coords);
            if (type==java.awt.geom.PathIterator.SEG_MOVETO) {
                path.appendMoveTo(coords[0], coords[1]);
            } else if (type==java.awt.geom.PathIterator.SEG_LINETO) {
                path.appendLineTo(coords[0], coords[1]);
            } else if (type==java.awt.geom.PathIterator.SEG_CUBICTO) {
                path.appendCubicTo(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]);
            } else if (type==java.awt.geom.PathIterator.SEG_QUADTO) {
                path.appendQuadTo(coords[0], coords[1], coords[2], coords[3]);
            } else if (type==java.awt.geom.PathIterator.SEG_CLOSE) {
                path.appendClose();
            }
        }

        return path;
    }


    public static Paint toAwtPaint(science.unlicense.display.api.painter2d.Paint paint){
        if (paint==null) return null;

        Paint p;
        if (paint instanceof ColorPaint) {
            p = toAwtColor( ((ColorPaint) paint).getColor());
        } else if (paint instanceof LinearGradientPaint) {
            final LinearGradientPaint lgp = (LinearGradientPaint) paint;
            final science.unlicense.image.api.color.Color[] colors = lgp.getStopColors();
            final Color[] array = new Color[colors.length];
            for (int i=0;i<array.length;i++) array[i] = toAwtColor(colors[i]);

            return new java.awt.LinearGradientPaint(
                    new Point2D.Double(lgp.getStartX(), lgp.getStartY()),
                    new Point2D.Double(lgp.getEndX(),lgp.getEndY()),
                    Arrays.reformatToFloat(lgp.getStopOffsets()),
                    array);
        } else if (paint instanceof RadialGradientPaint) {
            p = new Color(((RadialGradientPaint) paint).getStopColors()[0].toARGB());
        } else if (paint instanceof PatternPaint) {
            final PatternPaint ip = (PatternPaint) paint;
            final BufferedImage img = toAwtImage(ip.getPattern());
            science.unlicense.geometry.impl.Rectangle rec = ip.getBox();
            final Rectangle2D.Double anchor = new Rectangle2D.Double();
            if (rec!=null) {
                anchor.x = rec.getX();
                anchor.y = rec.getY();
                anchor.width = rec.getWidth();
                anchor.height = rec.getHeight();
            } else {
                anchor.width = img.getWidth();
                anchor.height = img.getHeight();
            }
            return new TexturePaint(img, anchor);


        } else {
            throw new InvalidArgumentException("Unexpected type : "+paint.getClass());
        }
        return p;
    }

    public static Stroke toAwtStroke(Brush brush){
        if (brush instanceof PlainBrush) {
            final PlainBrush b = (PlainBrush) brush;
            return new BasicStroke(b.getWidth());
        } else {
            throw new InvalidArgumentException("Unexpected type : "+brush.getClass());
        }
    }

    public static Composite toAwtComposite(AlphaBlending blending){
        if (blending==null) return null;

        final float alpha = blending.getAlpha();
        final int type = blending.getType();
        //AlphaBlending.DST
        final int g2dtype;
        if (type==AlphaBlending.CLEAR)           g2dtype = AlphaComposite.CLEAR;
        else if (type==AlphaBlending.XOR)        g2dtype = AlphaComposite.XOR;
        else if (type==AlphaBlending.DST)        g2dtype = AlphaComposite.DST;
        else if (type==AlphaBlending.DST_ATOP)   g2dtype = AlphaComposite.DST_ATOP;
        else if (type==AlphaBlending.DST_IN)     g2dtype = AlphaComposite.DST_IN;
        else if (type==AlphaBlending.DST_OUT)    g2dtype = AlphaComposite.DST_OUT;
        else if (type==AlphaBlending.DST_OVER)   g2dtype = AlphaComposite.DST_OVER;
        else if (type==AlphaBlending.SRC)        g2dtype = AlphaComposite.SRC;
        else if (type==AlphaBlending.SRC_ATOP)   g2dtype = AlphaComposite.SRC_ATOP;
        else if (type==AlphaBlending.SRC_IN)     g2dtype = AlphaComposite.SRC_IN;
        else if (type==AlphaBlending.SRC_OUT)    g2dtype = AlphaComposite.SRC_OUT;
        else if (type==AlphaBlending.SRC_OVER)   g2dtype = AlphaComposite.SRC_OVER;
        else throw new InvalidArgumentException("Unexpected type : "+type);

        return AlphaComposite.getInstance(g2dtype, alpha);
    }

    public static AffineTransform toAwtTransform(Affine trs){
        if (trs==null) return null;

        return new AffineTransform(
                         trs.get(0, 0), trs.get(1, 0),
                         trs.get(0, 1), trs.get(1, 1),
                         trs.get(0, 2), trs.get(1, 2));
    }

    public static Affine2 toAffine(AffineTransform trs){
        if (trs==null) return null;

        return new Affine2(
                trs.getScaleX(), trs.getShearX(), trs.getTranslateX(),
                trs.getShearY(), trs.getScaleY(), trs.getTranslateY());
    }

    public static Color toAwtColor(science.unlicense.image.api.color.Color color) {
        return new Color(color.toARGB(),true);
    }

    public static Font toAwtFont(science.unlicense.display.api.font.FontChoice font) {
        final String family = font.getFamilies()[0].toString();
        final int weight = font.getWeight();
        int fw = Font.PLAIN;
        if (weight==science.unlicense.display.api.font.FontChoice.WEIGHT_BOLD){
            fw = Font.BOLD;
        }
        return new Font(family, fw, (int) font.getSize());
    }

    public static BufferedImage toAwtImage(Image image){
        if (image==null) return null;

        final ImageModel colorModel = image.getColorModel();
        final ImageModel rawModel = image.getRawModel();
        Object backEnd = image.getDataBuffer().getBackEnd();



//    private static final ImageModel RM = new InterleavedModel(new UndefinedSystem(4), NumericType.TYPE_UBYTE);
//    private static final ImageModel CM = DerivateModel.create(RM, new int[]{1,2,3,0}, null, null, ColorSystem.RGBA_8BITS);

        if (colorModel instanceof InterleavedModel) {
            final InterleavedModel interleaved = (InterleavedModel) colorModel;
            final NumberType numericType = interleaved.getNumericType();
            final int sampleCount = interleaved.getSampleCount();
            final SampleSystem ss = interleaved.getSampleSystem();

            if (ColorSystem.RGBA_8BITS.equals(ss) || ColorSystem.RGBA_8BITS_PREMUL.equals(ss)) {
                boolean premultiplied = ColorSystem.RGBA_8BITS_PREMUL.equals(ss);

                if (backEnd instanceof Buffer) {
                    final Buffer buf = (Buffer) backEnd;

                    if (buf.hasArray()) {
                        backEnd = buf.array();
                    } else if (buf instanceof ByteBuffer) {
                        byte[] arr = new byte[buf.capacity()];
                        buf.position(0);
                        ((ByteBuffer) buf).get(arr);
                        backEnd = arr;
                    } else if (buf instanceof IntBuffer) {
                        int[] arr = new int[buf.capacity()];
                        buf.position(0);
                        ((IntBuffer) buf).get(arr);
                        backEnd = arr;
                    }

                }

                if (backEnd instanceof byte[]) {
                    final int width = (int) image.getExtent().getL(0);
                    final int height = (int) image.getExtent().getL(1);

                    ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_sRGB);
                    int[] nBits = {8, 8, 8, 8};
                    int[] bOffs = MAPPING_RGBA_0123;
                    final java.awt.image.ColorModel cm = new ComponentColorModel(
                                                         cs, nBits, true, premultiplied,
                                                         Transparency.TRANSLUCENT,
                                                         DataBuffer.TYPE_BYTE);

                    final DataBuffer dataBuffer = new DataBufferByte((byte[]) backEnd, width*height);
                    final WritableRaster raster = Raster.createInterleavedRaster(dataBuffer,
                                                            width, height,
                                                            width*4, 4,
                                                            bOffs, null);
                    return new BufferedImage(cm, raster, false, null);
                } else if (backEnd instanceof int[]) {
                    final int width = (int) image.getExtent().getL(0);
                    final int height = (int) image.getExtent().getL(1);

                    final java.awt.image.ColorModel cm = new DirectColorModel(
                                             ColorSpace.getInstance(ColorSpace.CS_sRGB),
                                             32,
                                             0x00ff0000,// Red
                                             0x0000ff00,// Green
                                             0x000000ff,// Blue
                                             0xff000000,// Alpha
                                             premultiplied,       // Alpha Premultiplied
                                             DataBuffer.TYPE_INT
                                             );

                    final int[] bandOffset = new int[]{0};
                    final DataBuffer dataBuffer = new DataBufferInt((int[]) backEnd, width*height);
                    final int[] bandMask = new int[]{
                                             0x00ff0000,// Red
                                             0x0000ff00,// Green
                                             0x000000ff,// Blue
                                             0xff000000,// Alpha
                    };
                    final WritableRaster raster = WritableRaster.createPackedRaster(dataBuffer, width, height, width, bandMask, new java.awt.Point());

                    return new BufferedImage(cm, raster, false, null);
                }
            }

        }

        if (rawModel instanceof InterleavedModel) {
            final InterleavedModel interleaved = (InterleavedModel) rawModel;
            final NumberType numericType = interleaved.getNumericType();
            final int sampleCount = interleaved.getSampleCount();
            if (UInt8.TYPE == numericType && sampleCount == 4) {

                if (colorModel instanceof DerivateModel) {
                    final DerivateModel dm = (DerivateModel) colorModel;
                    final SampleSystem colorSystem = dm.getSampleSystem();
                    final int[] bitCount = dm.getBitCount();
                    final int[] bitOffset = dm.getBitOffset();
                    final int[] mapping = dm.getMapping();

                    if (bitCount == null && Arrays.equals(mapping, MAPPING_RGBA_1230) && ColorSystem.RGBA_8BITS.equals(colorSystem) ) {

                        if (backEnd instanceof byte[]) {
                            final int width = (int) image.getExtent().getL(0);
                            final int height = (int) image.getExtent().getL(1);

                            ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_sRGB);
                            int[] nBits = {8, 8, 8, 8};
                            int[] bOffs = mapping;
                            final java.awt.image.ColorModel cm = new ComponentColorModel(
                                                                 cs, nBits, true, false,
                                                                 Transparency.TRANSLUCENT,
                                                                 DataBuffer.TYPE_BYTE);

                            final DataBuffer dataBuffer = new DataBufferByte((byte[]) backEnd, width*height);
                            final WritableRaster raster = Raster.createInterleavedRaster(dataBuffer,
                                                                    width, height,
                                                                    width*4, 4,
                                                                    bOffs, null);
                            return new BufferedImage(cm, raster, false, null);
                        } else if (backEnd instanceof int[]) {
                            final int width = (int) image.getExtent().getL(0);
                            final int height = (int) image.getExtent().getL(1);

                            final java.awt.image.ColorModel cm = new DirectColorModel(
                                                     ColorSpace.getInstance(ColorSpace.CS_sRGB),
                                                     32,
                                                     0x00ff0000,// Red
                                                     0x0000ff00,// Green
                                                     0x000000ff,// Blue
                                                     0xff000000,// Alpha
                                                     false,       // Alpha Premultiplied
                                                     DataBuffer.TYPE_INT
                                                     );

                            final int[] bandOffset = new int[]{0};
                            final DataBuffer dataBuffer = new DataBufferInt((int[]) backEnd, width*height);
                            final int[] bandMask = new int[]{
                                                     0x00ff0000,// Red
                                                     0x0000ff00,// Green
                                                     0x000000ff,// Blue
                                                     0xff000000,// Alpha
                            };
                            final WritableRaster raster = WritableRaster.createPackedRaster(dataBuffer, width, height, width, bandMask, new java.awt.Point());

                            return new BufferedImage(cm, raster, false, null);
                        }
                    }
                }
            }
        }

        //format can not be supported directly convert it to a RGBA buffer.
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final TupleGridCursor pb = image.getColorModel().asTupleBuffer(image).cursor();
        final ColorSystem cs = (ColorSystem) image.getColorModel().getSampleSystem();
        final Vector2i32 coord = new Vector2i32();

        final BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        for (coord.y = 0; coord.y <height; coord.y++) {
            for (coord.x = 0; coord.x < width; coord.x++) {
                pb.moveTo(coord);
                bi.setRGB(coord.x, coord.y, new science.unlicense.image.api.color.ColorRGB(pb.samples(), cs).toARGB());
            }
        }

        return bi;
    }

    public static Image toImage(BufferedImage awtImg) {
        final int width = awtImg.getWidth();
        final int height = awtImg.getHeight();
        final Image image = Images.create(new Extent.Long(width,height), Images.IMAGE_TYPE_RGBA);
        final ColorSystem cs = (ColorSystem) image.getColorModel().getSampleSystem();
        final TupleGridCursor pb = image.getColorModel().asTupleBuffer(image).cursor();
        final Vector2i32 coord = new Vector2i32();

        for (coord.y = 0; coord.y <height; coord.y++) {
            for (coord.x = 0; coord.x < width; coord.x++) {
                pb.moveTo(coord);
                int rgb = awtImg.getRGB(coord.x, coord.y);
                pb.samples().set(new science.unlicense.image.api.color.ColorRGB(rgb).toColorSystem(cs));
            }
        }

        return image;
    }

}
