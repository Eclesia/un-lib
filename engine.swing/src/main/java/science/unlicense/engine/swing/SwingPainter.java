
package science.unlicense.engine.swing;

import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.painter2d.AbstractPainter2D;
import science.unlicense.display.api.painter2d.ImagePainter2D;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.s2d.Graphic2DFactory;
import static science.unlicense.engine.swing.AWTUtils.*;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.math.api.Affine;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class SwingPainter extends AbstractPainter2D implements ImagePainter2D{

    private static final SwingGraphicFactory FACTORY = new SwingGraphicFactory();
    private static final AffineTransform IDENTITY = new AffineTransform();
    private BufferedImage image;
    private Graphics2D g;

    public SwingPainter(int width, int height) {
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        g = image.createGraphics();
    }

    public SwingPainter(Graphics2D g) {
        this.image = null;
        this.g = g;
    }

    @Override
    public Graphic2DFactory getGraphicFactory() {
        return FACTORY;
    }

    @Override
    public FontStore getFontStore() {
        return SwingFontStore.INSTANCE;
    }

    @Override
    public void setSize(Extent.Long size) {
        image = new BufferedImage((int) size.getL(0), (int) size.getL(1), BufferedImage.TYPE_INT_ARGB);
        g = image.createGraphics();
    }

    @Override
    public Extent.Long getSize() {
        return new Extent.Long(image.getWidth(), image.getHeight());
    }

    @Override
    public Image getImage() {
        return AWTUtils.toImage(image);
    }

    @Override
    public void setClip(PlanarGeometry geom) {
        super.setClip(geom);
        if (geom!=null) {
            g.setTransform(IDENTITY);
            g.setClip(AWTUtils.toShape(geom));
        } else {
            g.setClip(null);
        }
    }

    @Override
    public void fill(CharArray text, float x, float y) {
        final Paint fill = toAwtPaint(fillPaint);
        final Composite composite = toAwtComposite(alphaBlending);
        g.setFont(toAwtFont(font));
        g.setComposite(composite);
        g.setPaint(fill);
        g.setTransform(toAwtTransform(transform));
        g.drawString(text.toString(), x, y);
    }

    @Override
    public void fill(PlanarGeometry geom) {

        final Shape shp = AWTUtils.toShape(geom);
        final Paint fill = toAwtPaint(fillPaint);
        final Composite composite = toAwtComposite(alphaBlending);
        final AffineTransform trs = toAwtTransform(transform);

        if (shp!=null && fill!=null){
            g.setTransform(trs);
            g.setComposite(composite);
            g.setPaint(fill);
            g.fill(shp);
        }

    }

    @Override
    public void stroke(PlanarGeometry geom) {
        final Shape shp = AWTUtils.toShape(geom);
        final Paint fill = toAwtPaint(fillPaint);
        final Stroke stroke = toAwtStroke(brush);
        final Composite composite = toAwtComposite(alphaBlending);
        final AffineTransform trs = toAwtTransform(transform);

        if (shp!=null && fill!=null){
            g.setTransform(trs);
            g.setComposite(composite);
            g.setPaint(fill);
            g.setStroke(stroke);
            g.draw(shp);
        }
    }

    @Override
    public void paint(Image image, Affine transform) {
        final BufferedImage img = toAwtImage(image);
        paint(img, transform);
    }

    private void paint(BufferedImage img, Affine transform) {
        final Composite composite = toAwtComposite(alphaBlending);
        AffineTransform trs = toAwtTransform(transform);
        if (trs==null) trs = new AffineTransform();

        if (img!=null){
            g.setTransform(toAwtTransform(this.transform));
            g.setComposite(composite);
            g.drawImage(img, trs, null);
        }
    }

    public void render(SceneNode node){
        visitor.visit(node, null);
    }

    private final PainterVisitor visitor = new PainterVisitor();
    private class PainterVisitor extends DefaultPainterVisitor{
        public Object visit(Node node, Object context) {
            if (node instanceof SwingGraphicFactory.SwingImageNode){
                final SwingGraphicFactory.SwingImageNode gln = (SwingGraphicFactory.SwingImageNode) node;
                final BufferedImage texture = gln.getSwingImage();
                if (texture != null) {
                    paint(texture, new Affine2(gln.getNodeToRootSpace()));
                }
            } else {
                return super.visit(node, context);
            }
            return null;
        }
    }

    @Override
    public Painter2D derivate(boolean copyState) {
        final SwingPainter cp = new SwingPainter((Graphics2D) g.create());
        if (copyState) {
           cp.setTransform(getTransform());
           cp.setClip(getClip());
           cp.setFont(getFont());
           cp.setAlphaBlending(getAlphaBlending());
           cp.setPaint(getPaint());
           cp.setBrush(getBrush());
        } else {
           cp.setTransform(new Affine2());
           cp.setClip(null);
           cp.setFont(null);
           cp.setAlphaBlending(AlphaBlending.DEFAULT);
           cp.setPaint(null);
           cp.setBrush(null);
        }
        return cp;
    }

    @Override
    public void dispose() {
        g.dispose();
    }


}
