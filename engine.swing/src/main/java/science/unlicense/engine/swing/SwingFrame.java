
package science.unlicense.engine.swing;

import java.awt.Dialog;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.JDialog;
import javax.swing.JFrame;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.display.api.desktop.DefaultScreen;
import static science.unlicense.display.api.desktop.Frame.PROP_CLOSABLE;
import science.unlicense.display.api.desktop.FrameDecoration;
import science.unlicense.display.api.desktop.FrameManager;
import science.unlicense.display.api.desktop.FrameMessage;
import science.unlicense.display.api.desktop.Screen;
import science.unlicense.display.api.desktop.cursor.Cursor;
import science.unlicense.display.api.desktop.cursor.NamedCursor;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.desktop.UIFrame;
import static science.unlicense.engine.ui.desktop.UIFrame.PROP_FOCUSED_WIDGET;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class SwingFrame extends AbstractEventSource implements UIFrame {

    static {
        //force loading class and registering default fonts
        SwingFontStore v = SwingFontStore.INSTANCE;
    }
    private final SwingFrameDecoration DEFAULT_DECO = new SwingFrameDecoration();
    private final SwingFrame parent;
    JDialog jdialog;
    JFrame jframe;
    Window window;
    private final SwingWContainer pane = new SwingWContainer();
    private Widget focused = null;
    private Cursor defaultCursor = NamedCursor.DEFAULT;
    private Cursor currentCursor = NamedCursor.DEFAULT;
    private FrameDecoration decoration = DEFAULT_DECO;


    private final AtomicBoolean disposed = new AtomicBoolean(false);

    public SwingFrame(SwingFrame parent, boolean modal, boolean translucent) {
        this.parent = parent;
        if (parent!=null) {
            jdialog = new JDialog(parent.window, "", modal ? Dialog.ModalityType.APPLICATION_MODAL : Dialog.ModalityType.MODELESS);
            jdialog.setContentPane(pane);
            jdialog.setLocationRelativeTo(null);
            window = jdialog;
        } else {
            jframe = new JFrame();
            jframe.setContentPane(pane);
            jframe.setLocationRelativeTo(null);
            window = jframe;
        }
        pane.container.setFrame(this);
        DEFAULT_DECO.setFrame(this);

        window.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                getEventManager().sendEvent(new Event(SwingFrame.this, new FrameMessage(FrameMessage.TYPE_PREDISPOSE)));
            }

            @Override
            public void windowClosed(WindowEvent e) {
                getEventManager().sendEvent(new Event(SwingFrame.this, new FrameMessage(FrameMessage.TYPE_DISPOSED)));
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });
    }

    @Override
    public Painter2D getPainter() {
        Graphics g = window.getGraphics();
        if (g==null) {
           //create a fake one until frame gets displayed
           g = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB).getGraphics();
        }
        return new SwingPainter((Graphics2D) g);
    }

    @Override
    public Screen getScreen() {
        if (parent!=null) {
            return parent.getScreen();
        } else {
            return new DefaultScreen(
                Toolkit.getDefaultToolkit().getScreenResolution() / 25.4, //inch to millimeter
                SwingFrameManager.INSTANCE.getDisplaySize());
        }
    }

    @Override
    public FrameManager getManager() {
        return SwingFrameManager.INSTANCE;
    }

    @Override
    public WContainer getContainer() {
        return pane.container;
    }

    @Override
    public Widget getFocusedWidget() {
        return focused;
    }

    @Override
    public void setFocusedWidget(Widget focused) {
        if (CObjects.equals(this.focused, focused)) return;
        final Widget old = this.focused;
        if (old!=null) old.setFocused(false);
        this.focused = focused;
        if (this.focused!=null) this.focused.setFocused(true);
        sendPropertyEvent(PROP_FOCUSED_WIDGET, old, this.focused);
    }

    @Override
    public CharArray getTitle() {
        if (jframe!=null) {
            return new Chars(jframe.getTitle());
        } else {
            return new Chars(jdialog.getTitle());
        }
    }

    @Override
    public void setTitle(CharArray title) {
        if (jframe!=null) {
            jframe.setTitle(title==null ? "" : title.toString());
        } else {
            jdialog.setTitle(title==null ? "" : title.toString());
        }
    }

    @Override
    public Property varTitle() {
        return getProperty(PROP_TITLE);
    }

    @Override
    public Cursor getCursor() {
        return defaultCursor;
    }

    @Override
    public void setCursor(Cursor defaultCursor) {
        final boolean updateCurrent = currentCursor == this.defaultCursor;
        this.defaultCursor = defaultCursor;
        if (updateCurrent) ;//TODO
    }

    @Override
    public void setOnScreenLocation(Tuple location) {
        window.setLocation((int) location.get(0), (int) location.get(1));
    }

    @Override
    public Tuple getOnScreenLocation() {
        final Point pos = window.getLocationOnScreen();
        return new Vector2f64(pos.x, pos.y);
    }

    @Override
    public void setSize(int width, int height) {
        window.setSize(width, height);
    }

    @Override
    public Extent getSize() {
        return new Extent.Double(window.getWidth(),window.getHeight());
    }

    @Override
    public void setVisible(boolean visible) {
        window.setVisible(visible);
    }

    @Override
    public boolean isVisible() {
        return window.isVisible();
    }

    @Override
    public Property varVisible() {
        return getProperty(PROP_VISIBLE);
    }

    @Override
    public FrameDecoration getSystemDecoration() {
        return DEFAULT_DECO;
    }

    @Override
    public void setDecoration(FrameDecoration decoration) {
        this.decoration = decoration;
        if (jframe!=null) {
            jframe.setUndecorated(this.decoration==null);
        } else {
            jdialog.setUndecorated(this.decoration==null);
        }
    }

    @Override
    public FrameDecoration getDecoration() {
        return decoration;
    }

    @Override
    public void setAlwaysonTop(boolean ontop) {
        window.setAlwaysOnTop(ontop);
    }

    @Override
    public boolean isAlwaysOnTop() {
        return window.isAlwaysOnTop();
    }

    @Override
    public boolean isTranslucent() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void dispose() {
        window.dispose();
        synchronized(disposed){
            disposed.set(true);
            disposed.notify();
        }
    }

    @Override
    public int getState() {
        final int state;
        if (jframe!=null) {
           state = jframe.getExtendedState();
        } else {
            state = 0;
        }
        if (state==JFrame.MAXIMIZED_BOTH) {
            if (decoration==null) {
                return UIFrame.STATE_FULLSCREEN;
            } else {
                return UIFrame.STATE_MAXIMIZED;
            }
        } else if (state==JFrame.MAXIMIZED_HORIZ) {
            return UIFrame.STATE_MAXIMIZED_HORIZONTAL;
        } else if (state==JFrame.MAXIMIZED_VERT) {
            return UIFrame.STATE_MAXIMIZED_VERTICAL;
        } else if (state==JFrame.ICONIFIED) {
            return UIFrame.STATE_MINIMIZED;
        }
        return UIFrame.STATE_NORMAL;
    }

    @Override
    public void setState(int state) {
        if (jdialog!=null) {
            //not possible with dialogs
            return;
        }

        if (state==UIFrame.STATE_FULLSCREEN) {
            jframe.setExtendedState(JFrame.MAXIMIZED_BOTH);
            jframe.dispose();
            jframe.setUndecorated(true);
            jframe.setVisible(true);
        } else if (state==UIFrame.STATE_MAXIMIZED) {
            jframe.setExtendedState(JFrame.MAXIMIZED_BOTH);
        } else if (state==UIFrame.STATE_MAXIMIZED_HORIZONTAL) {
            jframe.setExtendedState(JFrame.MAXIMIZED_HORIZ);
        } else if (state==UIFrame.STATE_MAXIMIZED_VERTICAL) {
            jframe.setExtendedState(JFrame.MAXIMIZED_VERT);
        } else if (state==UIFrame.STATE_NORMAL) {
            jframe.setExtendedState(JFrame.NORMAL);
        } else if (state==UIFrame.STATE_MINIMIZED) {
            jframe.setExtendedState(JFrame.ICONIFIED);
        }
    }

    @Override
    public void setMaximizable(boolean maximizable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isMaximizable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Property varMaximizable() {
        return getProperty(PROP_MAXIMIZABLE);
    }

    @Override
    public void setMinimizable(boolean minimizable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isMinimizable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Property varMinimizable() {
        return getProperty(PROP_MINIMIZABLE);
    }

    @Override
    public void setClosable(boolean closable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isClosable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Property varClosable() {
        return getProperty(PROP_CLOSABLE);
    }

    @Override
    public UIFrame getParentFrame() {
        return parent;
    }

    @Override
    public Sequence getChildrenFrames() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isModale() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isDisposed() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void waitForDisposal() {
        synchronized(disposed) {
            while (!disposed.get()) {
                try {
                    disposed.wait();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

}
