
package science.unlicense.engine.swing;

import java.awt.Shape;
import java.awt.font.GlyphVector;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.IntSet;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.display.api.font.Font;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.font.FontStore;
import static science.unlicense.engine.swing.SwingFontStore.CTX;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 *
 * @author Johann Sorel
 */
public class SwingFont implements Font {

    private final java.awt.Font font;

    public SwingFont(java.awt.Font font) {
        this.font = font;
    }

    @Override
    public Chars getFamily() {
        return new Chars(font.getFamily());
    }

    @Override
    public int getWeight() {
        if (java.awt.Font.BOLD == font.getStyle()) {
            return WEIGHT_BOLD;
        } else {
            return WEIGHT_NONE;
        }
    }

    @Override
    public float getSize() {
        return font.getSize2D();
    }

    @Override
    public FontStore getStore() {
        return SwingFontStore.INSTANCE;
    }

    @Override
    public FontMetadata getMetaData() {
        return new SwingFontMetadata(font).derivate(getSize());
    }

    @Override
    public IntSet listCharacters() {
        throw new UnimplementedException();
    }

    @Override
    public PlanarGeometry getGlyph(int unicode) {
        final GlyphVector glyph = font.createGlyphVector(CTX, new char[]{(char) unicode});
        final Shape shape = glyph.getGlyphOutline(0);
        return AWTUtils.toGeometry2D(shape);
    }

    @Override
    public PlanarGeometry getGlyph(Char character) {
        return getGlyph(character.toUnicode());
    }

    @Override
    public Font derivate(float fontSize) {
        if (fontSize==getSize()) return this;
        return new SwingFont(font.deriveFont(fontSize));
    }

}
