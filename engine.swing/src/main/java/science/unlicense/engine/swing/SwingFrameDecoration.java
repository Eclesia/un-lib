
package science.unlicense.engine.swing;

import java.awt.Insets;
import science.unlicense.display.api.desktop.Frame;
import science.unlicense.display.api.desktop.FrameDecoration;
import science.unlicense.display.api.layout.Margin;

/**
 *
 * @author Johann Sorel
 */
public class SwingFrameDecoration implements FrameDecoration {

    private SwingFrame frame;

    public SwingFrame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = (SwingFrame) frame;
    }

    public Margin getMargin() {
        final Insets insets = frame.jframe.getInsets();
        return new Margin(insets.top, insets.right, insets.bottom, insets.left);
    }

}
