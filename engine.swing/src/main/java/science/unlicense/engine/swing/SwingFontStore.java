
package science.unlicense.engine.swing;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.font.FontRenderContext;
import java.awt.image.BufferedImage;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Set;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.font.AbstractFontStore;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.system.jvm.JVMSystem;

/**
 *
 * @author Johann Sorel
 */
public class SwingFontStore extends AbstractFontStore {


    static final GraphicsEnvironment ENV = GraphicsEnvironment.getLocalGraphicsEnvironment();
    static final Graphics2D GRAPHICS = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB).createGraphics();
    static final FontRenderContext CTX = GRAPHICS.getFontRenderContext();

    public static final SwingFontStore INSTANCE = new SwingFontStore();

    private Set families;
    private Dictionary fonts;

    private SwingFontStore() {
        try {
            registerFont(Paths.resolve(new Chars("mod:/module-res/Tuffy.ttf")));
            registerFont(Paths.resolve(new Chars("mod:/module-res/Mona.ttf")));
            registerFont(Paths.resolve(new Chars("mod:/module-res/Unicon.ttf")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

    }

    @Override
    public synchronized Set getFamilies() {
        if (families==null) {
            families = new HashSet();
            final String[] names = ENV.getAvailableFontFamilyNames();
            for (int i=0;i<names.length;i++) {
                families.add(Chars.constant(names[i]));
            }
            fonts = new HashDictionary();
            final Font[] allFonts = ENV.getAllFonts();
            for (int i=0;i<allFonts.length;i++) {
                fonts.add(Chars.constant(allFonts[i].getFamily()), allFonts[i]);
            }
        }
        return families;
    }

    private synchronized Dictionary getFonts() {
        getFamilies();
        return fonts;
    }

    @Override
    public FontMetadata getMetadata(Chars family) {
        final Font font = (Font) getFonts().getValue(family);
        return new SwingFontMetadata(font);
    }

    @Override
    public science.unlicense.display.api.font.Font getFont(Chars family) {
        final Font font = (Font) getFonts().getValue(family);
        return new SwingFont(font);
    }

    @Override
    public synchronized void registerFont(Path path) throws IOException {
        final ByteInputStream is = path.createInputStream();
        try {
            final Font font = Font.createFont(Font.TRUETYPE_FONT, JVMSystem.toInputStream(is));
            ENV.registerFont(font);
            families = null;
        } catch (FontFormatException ex) {
            throw new IOException(path, ex);
        } catch (java.io.IOException ex) {
            throw new IOException(path, ex);
        }
    }

}
