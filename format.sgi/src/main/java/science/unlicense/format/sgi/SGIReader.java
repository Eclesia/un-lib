
package science.unlicense.format.sgi;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.common.api.number.UInt16;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.encoding.impl.io.ClipInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGESET;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION_ID;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.PlanarModel;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 * SGI Image reader.
 *
 * @author Johann Sorel
 */
public class SGIReader extends AbstractImageReader {

    private SGIHeader header;
    private TypedNode mdImage;

    protected Dictionary readMetadatas(final BacktrackInputStream stream) throws IOException {

        final DataInputStream ds = new DataInputStream(stream, Endianness.BIG_ENDIAN);
        if (SGIConstants.SIGNATURE != ds.readUShort()){
            throw new IOException(ds, "Stream is not a valid SGI Image.");
        }

        header = new SGIHeader();
        header.read(stream);

        mdImage =
        new DefaultTypedNode(MD_IMAGE,new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,header.xsize)}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,header.ysize)})
        });

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        stream.mark();
        readMetadatas(stream);

        stream.rewind();
        final DataInputStream ds = new DataInputStream(stream, Endianness.BIG_ENDIAN);
        ds.skipFully(header.dataOffset);

        //read pixel datas
        final int scanlineSize = header.bpc*header.xsize;
        final Buffer bank = parameters.getBufferFactory().createInt8(scanlineSize*header.ysize*header.zsize).getBuffer();

        //the image data start from the lower left corner so we need to
        //flip the image while reading
        if (SGIConstants.STORAGE_VERBATIM == header.storage){
            for (int z=0;z<header.zsize;z++){
                int offset = header.xsize*header.ysize*z;
                for (int y=header.ysize-1;y>=0;y--){
                    ds.readFully(bank, offset+y*scanlineSize, scanlineSize);
                }
            }

        } else if (SGIConstants.STORAGE_RLE == header.storage){
            final int[] info = new int[2];

            for (int z=0;z<header.zsize;z++){
                int offset = header.xsize*header.ysize*z;
                for (int y=0;y<header.ysize;y++){
                    //get the row offset
                    header.getRowRLEInfo(y, z, info);
                    //move the stream at correct location
                    stream.rewind();
                    ds.skipFully(info[0]);
                    //decompress row
                    final ClipInputStream ci = new ClipInputStream(stream, info[1]);
                    final SGIRLEInputStream rleis = new SGIRLEInputStream(ci, header.bpc);
                    final DataInputStream rds = new DataInputStream(rleis, Endianness.BIG_ENDIAN);
                    rds.readFully(bank, offset+(header.ysize-1-y)*scanlineSize, scanlineSize);
                }
            }
        }

        //rebuild sample model
        final NumberType sampleType = (header.bpc == 1) ? UInt8.TYPE : UInt16.TYPE;
        final ImageModel sm = new PlanarModel(new UndefinedSystem(header.zsize), sampleType);

        //rebuild color model
        final ImageModel cm;
        if (SGIConstants.COLORMAP_NORMAL == header.colormap){
            if (header.zsize == 1){
                //bleack and white or grayscale
                cm = DerivateModel.create(sm, new int[]{0,0,0}, null, null, ColorSystem.RGB_8BITS);
            } else if (header.zsize == 3){
                //RGB image
                cm = DerivateModel.create(sm, new int[]{0,1,2}, null, null, ColorSystem.RGB_8BITS);
            } else if (header.zsize == 4){
                //RGBA image
                cm = DerivateModel.create(sm, new int[]{0,1,2,3}, null, null, ColorSystem.RGBA_8BITS);
            } else {
                //anything else, just use the first channel as grayscale
                cm = DerivateModel.create(sm, new int[]{0,0,0}, null, null, ColorSystem.RGB_8BITS);
            }
        } else if (SGIConstants.COLORMAP_DITHERED == header.colormap){
            throw new IOException(ds, "Dithered colormap not supported yet.");
        } else if (SGIConstants.COLORMAP_SCREEN == header.colormap){
            throw new IOException(ds, "Screen colormap not supported yet.");
        } else if (SGIConstants.COLORMAP_COLORMAP == header.colormap){
            throw new IOException(ds, "Colormap type colormap not supported yet.");
        } else {
            throw new IOException(ds, "Unknowned colormap type : "+ header.colormap);
        }

        return new DefaultImage(bank, new Extent.Long(header.xsize, header.ysize), sm,cm);
    }

}
