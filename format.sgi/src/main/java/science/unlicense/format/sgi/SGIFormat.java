
package science.unlicense.format.sgi;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * Format specification :
 * ftp://ftp.sgi.com/graphics/grafica/sgiimage.html
 *
 * @author Johann Sorel
 */
public class SGIFormat extends AbstractImageFormat {

    public static final SGIFormat INSTANCE = new SGIFormat();

    private SGIFormat() {
        super(new Chars("sgi"));
        shortName = new Chars("SGI");
        longName = new Chars("Silicon Graphics Image");
        mimeTypes.add(new Chars("image/sgi"));
        extensions.add(new Chars("sgi"));
        signatures.add(SGIConstants.BSIGNATURE);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new SGIStore(this, source);
    }

}
