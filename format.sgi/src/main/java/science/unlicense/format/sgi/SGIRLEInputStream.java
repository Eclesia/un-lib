package science.unlicense.format.sgi;

import science.unlicense.common.api.Arrays;
import science.unlicense.encoding.api.io.AbstractInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * SGI Run-Length Encoding input stream.
 * ftp://ftp.sgi.com/graphics/grafica/sgiimage.html
 *
 *
 * This stream is different from the RLE input stream in encoding package.
 *
 * @author Johann Sorel
 */
public final class SGIRLEInputStream extends  AbstractInputStream{

    private final DataInputStream in;
    private final int dataSize;
    private final byte[] buffer;
    private int index = 0;
    private int remaining = 0;

    public SGIRLEInputStream(final ByteInputStream in) {
        this(in,1);
    }

    /**
     * @param in
     * @param dataSize , number of byte used for rawdata and rle packet
     *  Common value is 1 (for one byte), but some storage format can take advantage of
     *  largeur blocks (like 4bytes for a float/int or 3bytes for a color).
     */
    public SGIRLEInputStream(final ByteInputStream in,final int dataSize) {
        this.in = (in instanceof DataInputStream) ? (DataInputStream) in : new DataInputStream(in);
        this.dataSize = dataSize;
        this.buffer = new byte[128*dataSize];
    }

    @Override
    public int read() throws IOException {

        read:
        if (remaining<=0){
            final int pixel = in.read();

            if (pixel<0){
                //nothing left in the wrapped stream
                break read;
            }

            index = 0;
            final int count = (pixel & 0x7f);
            if ((pixel & 0x80) != 0) {
                remaining = count*dataSize;
                in.readFully(buffer, 0, remaining);
            } else {
                remaining = count*dataSize;
                in.readFully(buffer, 0, dataSize);
                for (int i=1;i<count;i++){
                    Arrays.copy(buffer, 0, dataSize, buffer, dataSize*i);
                }
            }
        }

        if (remaining<=0){
            //nothing left to read
            return -1;
        }

        int b = buffer[index] & 0xff; //unsign it
        index++;
        remaining--;
        return b;
    }

    @Override
    public void dispose() throws IOException {
        in.dispose();
    }

}
