
package science.unlicense.format.sgi;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class SGIHeader {

    /** 2 bytes | short  | MAGIC     | IRIS image file magic number */
    /** 1 byte  | char   | STORAGE   | Storage format */
    public byte storage;
    /** 1 byte  | char   | BPC       | Number of bytes per pixel channel  */
    public byte bpc;
    /** 2 bytes | ushort | DIMENSION | Number of dimensions */
    public int dimension;
    /** 2 bytes | ushort | XSIZE     | X size in pixels  */
    public int xsize;
    /** 2 bytes | ushort | YSIZE     | Y size in pixels  */
    public int ysize;
    /** 2 bytes | ushort | ZSIZE     | Number of channels */
    public int zsize;
    /** 4 bytes | long   | PIXMIN    | Minimum pixel value */
    public int pixmin;
    /** 4 bytes | long   | PIXMAX    | Maximum pixel value */
    public int pixmax;
    /** 4 bytes | char   | DUMMY     | Ignored */
    /** 80 bytes | char   | IMAGENAME | Image name */
    public Chars imageName;
    /** 4 bytes | long   | COLORMAP  | Colormap ID */
    public int colormap;
    /** 404 bytes | char   | DUMMY     | Ignored */

    public int tablen;
    /** tablen longs | long   | STARTTAB  | Start table */
    public int[] starttabs;
    /** tablen longs | long   | LENGTHTAB | Length table */
    public int[] lengthtabs;

    /** offset from file start to image pixel datas */
    public int dataOffset;

    public void read(BacktrackInputStream bs) throws IOException{
        final DataInputStream ds = new DataInputStream(bs, Endianness.BIG_ENDIAN);
        storage = ds.readByte();
        bpc = ds.readByte();
        dimension = ds.readUShort();
        xsize = ds.readUShort();
        ysize = ds.readUShort();
        zsize = ds.readUShort();
        pixmin = ds.readInt();
        pixmax = ds.readInt();
        ds.skipFully(4);
        imageName = new Chars(ds.readFully(new byte[80]));
        colormap = ds.readInt();
        ds.skipFully(404);

        if (storage == SGIConstants.STORAGE_RLE){
            tablen = ysize * zsize;
            starttabs = ds.readInt(tablen);
            lengthtabs = ds.readInt(tablen);
        }

        dataOffset = bs.position();
    }

    /**
     *
     * @param rownum row number
     * @param channum channel number
     * @param info used to stored informations : [0] offset, [1] length
     */
    public void getRowRLEInfo(int rownum, int channum, int[] info){
        info[0] = starttabs[rownum+channum*ysize];
        info[1] = lengthtabs[rownum+channum*ysize];
    }

}
