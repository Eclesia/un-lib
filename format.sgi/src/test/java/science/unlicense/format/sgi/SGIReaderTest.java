package science.unlicense.format.sgi;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class SGIReaderTest {

    private static final Vector2i32 b00 = new Vector2i32(0,0);
    private static final Vector2i32 b10 = new Vector2i32(1,0);
    private static final Vector2i32 b01 = new Vector2i32(0,1);
    private static final Vector2i32 b11 = new Vector2i32(1,1);

    @Test
    public void testRGBNoCompression() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/sgi/SampleNoCompression.rgb")).createInputStream();

        final ImageReader reader = new SGIReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final ImageModel sm = image.getRawModel();
        final ImageModel cm = image.getColorModel();

        //values are in RGB
        Assert.assertEquals(new Vector3f64(255,  0,  0), image.getTuple(b00,sm)); //RED
        Assert.assertEquals(new Vector3f64(255,255,  0), image.getTuple(b10,sm)); //YELLOW
        Assert.assertEquals(new Vector3f64(  0,255,255), image.getTuple(b01,sm)); //CYAN
        Assert.assertEquals(new Vector3f64(  0,  0,255), image.getTuple(b11,sm)); //BLUE

        Assert.assertEquals(new ColorRGB(255,   0,   0, 255), image.getColor(b00));
        Assert.assertEquals(new ColorRGB(255, 255,   0, 255), image.getColor(b10));
        Assert.assertEquals(new ColorRGB(0,   255, 255, 255), image.getColor(b01));
        Assert.assertEquals(new ColorRGB(0,     0, 255, 255), image.getColor(b11));

    }

    @Test
    public void testRGBRLE() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/sgi/SampleRLE.rgb")).createInputStream();

        final ImageReader reader = new SGIReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final ImageModel sm = image.getRawModel();
        final ImageModel cm = image.getColorModel();

        //values are in RGB
        Assert.assertEquals(new Vector3f64(255,  0,  0), image.getTuple(b00,sm)); //RED
        Assert.assertEquals(new Vector3f64(255,255,  0), image.getTuple(b10,sm)); //YELLOW
        Assert.assertEquals(new Vector3f64(  0,255,255), image.getTuple(b01,sm)); //CYAN
        Assert.assertEquals(new Vector3f64(  0,  0,255), image.getTuple(b11,sm)); //BLUE

        Assert.assertEquals(new ColorRGB(255,   0,   0, 255), image.getColor(b00));
        Assert.assertEquals(new ColorRGB(255, 255,   0, 255), image.getColor(b10));
        Assert.assertEquals(new ColorRGB(0,   255, 255, 255), image.getColor(b01));
        Assert.assertEquals(new ColorRGB(0,     0, 255, 255), image.getColor(b11));

    }

}
