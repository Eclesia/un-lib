
package science.unlicense.protocol.echo;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 * Test echo client and server.
 *
 * @author Johann Sorel
 */
public class EchoTest {

    @Test
    public void testEcho() throws IOException{

        //port 0 for automatic.
        final EchoServer server = new EchoServer(0);
        server.start();
        final int port = server.getPort();

        final EchoClient client = new EchoClient(new Chars("localhost"),port);
        final byte[] message = new Chars("Hello echo!").toBytes();
        final byte[] result = client.send(message);

        Assert.assertArrayEquals(message, result);

        server.stop();
        client.close();
    }


}
