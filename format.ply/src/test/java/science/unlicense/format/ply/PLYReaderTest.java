
package science.unlicense.format.ply;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.model3d.api.Model3Ds;

/**
 *
 * @author Johann Sorel
 */
public class PLYReaderTest {

    @Test
    public void testRead() throws Exception{

        final Chars address = new Chars("mod:/science/unlicense/format/ply/cube.ply");

        final Path path = Paths.resolve(address);
        final PLYStore store = (PLYStore) Model3Ds.open(path);
        final SceneNode node = (SceneNode) store.getElements().createIterator().next();

    }

}
