
package science.unlicense.format.ply;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class PLYProperty {

    public Chars name;
    public Chars type;
    //if list type
    public Chars sizeType;
    public Chars valueType;

}
