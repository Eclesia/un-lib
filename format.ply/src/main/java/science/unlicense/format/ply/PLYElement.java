
package science.unlicense.format.ply;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;

/**
 * A ply element is a set of properties
 * @author Johann Sorel
 */
public class PLYElement {

    public Chars name;
    public long number;
    public final Sequence properties = new ArraySequence();

}
