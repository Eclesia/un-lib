
package science.unlicense.format.ply;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import static science.unlicense.format.ply.PLYConstants.FORMAT_ASCII;
import static science.unlicense.format.ply.PLYConstants.FORMAT_BIG_ENDIAN;
import static science.unlicense.format.ply.PLYConstants.FORMAT_LITTLE_ENDIAN;
import static science.unlicense.format.ply.PLYConstants.TAG_HEADER_END;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.Techniques;


/**
 *
 * @author Johann Sorel
 */
public class PLYStore extends AbstractModel3DStore {

    public final PLYHeader header = new PLYHeader();

    public float[] vertices;
    public float[] normals;
    public Buffer faces;
    private Int32Cursor facecursor;

    private int nbVertex;
    private int nbFace;

    public PLYStore(Object base) {
        super(PLYFormat.INSTANCE,base);
    }

    public Collection getElements() throws StoreException {
        try {
            read((Path) source);
        } catch (IOException ex) {
            throw new StoreException(ex.getMessage(),ex);
        }

        final VBO vertexVBO = new VBO(vertices, 3);
        final VBO normalVBO = new VBO(normals, 3);

        final SceneNode root = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final DefaultModel mesh = new DefaultModel();
        mesh.getTechniques().add(new SimpleBlinnPhong());
        root.getChildren().add(mesh);

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(vertexVBO);
        shell.setNormals(normalVBO);
        shell.setIndex(new IBO(faces));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) faces.getNumbersSize())});
        mesh.setShape(shell);
        mesh.updateBoundingBox();

        Techniques.styleCAD(mesh);

        final Collection col = new ArraySequence();
        col.add(root);
        return col;
    }

    private void read(final Path path) throws IOException {

        final ByteInputStream inStream = path.createInputStream();

        final ByteSequence headerData = findHeader(inStream);

        //parse header
        final CharInputStream charStream = new CharInputStream(
                new ArrayInputStream(headerData.toArrayByte()),
                CharEncodings.US_ASCII, new Char("\n".getBytes(),CharEncodings.US_ASCII));
        header.read(charStream);

        //get number of vertex and faces
        nbVertex = (int) header.getElement(new Chars("vertex")).number;
        nbFace = (int) header.getElement(new Chars("face")).number;


        if (FORMAT_ASCII.equals(header.format)){
            final CharInputStream ds = new CharInputStream(inStream, CharEncodings.US_ASCII,
                    new Char("\n".getBytes(),CharEncodings.US_ASCII));
            parseASCII(ds);
        } else if (FORMAT_BIG_ENDIAN.equals(header.format)){
            final DataInputStream ds = new DataInputStream(inStream, Endianness.BIG_ENDIAN);
            parseBinary(ds,Endianness.BIG_ENDIAN);
        } else if (FORMAT_LITTLE_ENDIAN.equals(header.format)){
            final DataInputStream ds = new DataInputStream(inStream, Endianness.LITTLE_ENDIAN);
            parseBinary(ds,Endianness.LITTLE_ENDIAN);
        }

    }

    private void parseASCII(CharInputStream charStream) throws IOException{
        Chars line = null;
        Chars[] parts = null;

        vertices = new float[nbVertex*3];

        int k=0;
        for (int i=0;i<nbVertex;){
            line = charStream.readLine().trim();
            if (line.isEmpty()) continue;
            i++;
            parts = line.split(' ');
            vertices[k++] = (float) Float64.decode(parts[0]);
            vertices[k++] = (float) Float64.decode(parts[1]);
            vertices[k++] = (float) Float64.decode(parts[2]);
        }

        for (int i=0;i<nbFace;){
            line = charStream.readLine().trim();
            if (line.isEmpty()) continue;
            i++;
            parts = line.split(' ');

            final int nb = Int32.decode(parts[0]);
            final int[] indexes = new int[nb];
            for (int p=0;p<nb;p++){
                indexes[p] = Int32.decode(parts[p]);
            }
            addFace(indexes);
        }
    }

    private void parseBinary(DataInputStream ds, final Endianness encoding) throws IOException{

        vertices = ds.readFloat(nbVertex*3);

        for (int i=0;i<nbFace;i++){
            final int nb = ds.readUByte();
            addFace(ds.readInt(nb));
        }
    }

    private void addFace(int[] indices) throws IOException{
        if (faces == null){
            if (indices.length==3){
                faces = DefaultBufferFactory.INSTANCE.createInt32(nbFace*3).getBuffer();
            } else {
                faces = DefaultBufferFactory.INSTANCE.createInt32(nbFace*2*3).getBuffer();
            }
            facecursor = faces.asInt32().cursor();
            normals = new float[nbVertex*3];
        }

        if (indices.length==3){
            final float[] normal = calculateNormal(indices[0], indices[1], indices[2]);
            Arrays.copy(normal,0,3,normals,indices[0]*3);
            Arrays.copy(normal,0,3,normals,indices[1]*3);
            Arrays.copy(normal,0,3,normals,indices[2]*3);
            facecursor.write(indices);
        } else if (indices.length==4){
            float[] normal = calculateNormal(indices[0], indices[1], indices[2]);
            Arrays.copy(normal,0,3,normals,indices[0]*3);
            Arrays.copy(normal,0,3,normals,indices[1]*3);
            Arrays.copy(normal,0,3,normals,indices[2]*3);
            facecursor.write(indices[0]).write(indices[1]).write(indices[2]);

            normal = calculateNormal(indices[2], indices[3], indices[0]);
            Arrays.copy(normal,0,3,normals,indices[2]*3);
            Arrays.copy(normal,0,3,normals,indices[3]*3);
            Arrays.copy(normal,0,3,normals,indices[0]*3);
            facecursor.write(indices[2]).write(indices[3]).write(indices[0]);
        } else {
            throw new IOException(getInput(), "Unsupported number of elements : "+indices.length);
        }

    }

    private float[] calculateNormal(int i1, int i2, int i3){
        final VectorRW v1 = new Vector3f64(vertices[i1*3], vertices[i1*3+1], vertices[i1*3+2]);
        final VectorRW v2 = new Vector3f64(vertices[i2*3], vertices[i2*3+1], vertices[i2*3+2]);
        final VectorRW v3 = new Vector3f64(vertices[i3*3], vertices[i3*3+1], vertices[i3*3+2]);
        final VectorRW v12 = v1.subtract(v2, null);
        final VectorRW v13 = v1.subtract(v3, null);
        return v12.cross(v13, null).localNormalize().toFloat();
    }

    private static ByteSequence findHeader(ByteInputStream stream) throws IOException{
        final ByteSequence headerData = new ByteSequence();

        final byte[] tagByte = TAG_HEADER_END.toBytes();
        final byte[] end = TAG_HEADER_END.getCharacter(TAG_HEADER_END.getCharLength()-1).toBytes();

        for (int b=stream.read();b!=-1;b=stream.read()){
            headerData.put((byte) b);
            if (headerData.endWidth(tagByte)){
                //search for line end
                for (b=stream.read();b!='\n';b=stream.read());
                return headerData;
            }
        }

        throw new IOException(stream, "Reached end of file before findind header end.");
    }



}
