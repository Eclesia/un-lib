
package science.unlicense.format.ply;

import science.unlicense.common.api.character.Chars;

/**
 * PLY constants.
 *
 * @author Johann Sorel
 */
public final class PLYConstants {

    private PLYConstants(){}

    public static final Chars TAG_HEADER_START = Chars.constant(new byte[]{'p','l','y'});
    public static final Chars TAG_HEADER_END = Chars.constant(new byte[]{'e','n','d','_','h','e','a','d','e','r'});
    public static final Chars TAG_FORMAT = Chars.constant(new byte[]{'f','o','r','m','a','t'});
    public static final Chars TAG_COMMENT = Chars.constant(new byte[]{'c','o','m','m','e','n','t'});
    public static final Chars TAG_ELEMENT = Chars.constant(new byte[]{'e','l','e','m','e','n','t'});
    public static final Chars TAG_PROPERTY = Chars.constant(new byte[]{'p','r','o','p','e','r','t','y'});

    public static final Chars FORMAT_ASCII = Chars.constant(new byte[]{'a','s','c','i','i'});
    public static final Chars FORMAT_LITTLE_ENDIAN = Chars.constant(new byte[]{'b','i','n','a','r','y','_','l','i','t','t','l','e','_','e','n','d','i','a','n'});
    public static final Chars FORMAT_BIG_ENDIAN = Chars.constant(new byte[]{'b','i','n','a','r','y','_','b','i','g','_','e','n','d','i','a','n'});

    public static final Chars TYPE_CHAR = Chars.constant(new byte[]{'c','h','a','r'});
    public static final Chars TYPE_UCHAR = Chars.constant(new byte[]{'u','c','h','a','r'});
    public static final Chars TYPE_SHORT = Chars.constant(new byte[]{'s','h','o','r','t'});
    public static final Chars TYPE_USHORT = Chars.constant(new byte[]{'u','s','h','o','r','t'});
    public static final Chars TYPE_INT = Chars.constant(new byte[]{'i','n','t'});
    public static final Chars TYPE_UINT = Chars.constant(new byte[]{'u','i','n','t'});
    public static final Chars TYPE_FLOAT = Chars.constant(new byte[]{'f','l','o','a','t'});
    public static final Chars TYPE_DOUBLE = Chars.constant(new byte[]{'d','o','u','b','l','e'});
    public static final Chars TYPE_INT8 = Chars.constant(new byte[]{'i','n','t','8'});
    public static final Chars TYPE_UINT8 = Chars.constant(new byte[]{'u','i','n','t','8'});
    public static final Chars TYPE_INT16 = Chars.constant(new byte[]{'i','n','t','1','6'});
    public static final Chars TYPE_UINT16 = Chars.constant(new byte[]{'u','i','n','t','1','6'});
    public static final Chars TYPE_INT32 = Chars.constant(new byte[]{'i','n','t','3','2'});
    public static final Chars TYPE_UINT32 = Chars.constant(new byte[]{'u','i','n','t','3','2'});
    public static final Chars TYPE_FLOAT32 = Chars.constant(new byte[]{'f','l','o','a','t','3','2'});
    public static final Chars TYPE_FLOAT64 = Chars.constant(new byte[]{'f','l','o','a','t','6','4'});
    public static final Chars TYPE_LIST = Chars.constant(new byte[]{'l','i','s','t'});




}
