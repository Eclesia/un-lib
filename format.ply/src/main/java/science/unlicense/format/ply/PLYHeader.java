
package science.unlicense.format.ply;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.ply.PLYConstants.TAG_COMMENT;
import static science.unlicense.format.ply.PLYConstants.TAG_ELEMENT;
import static science.unlicense.format.ply.PLYConstants.TAG_FORMAT;
import static science.unlicense.format.ply.PLYConstants.TAG_HEADER_END;
import static science.unlicense.format.ply.PLYConstants.TAG_PROPERTY;

/**
 *
 * @author Johann Sorel
 */
public class PLYHeader {

    public Chars format;
    public final Sequence comments = new ArraySequence();
    public final Sequence elements = new ArraySequence();

    /**
     * Get element for given name.
     * @param name
     * @return PLYElement or null
     */
    public PLYElement getElement(Chars name){
        for (int i=0,n=elements.getSize();i<n;i++){
            final PLYElement ele = (PLYElement) elements.get(i);
            if (ele.name.equals(name)){
                return ele;
            }
        }
        return null;
    }

    /**
     *
     * @param charStream
     * @throws IOException
     */
    public void read(CharInputStream charStream) throws IOException {

        PLYElement currentElement = null;
        Chars line = null;
        Chars[] parts = null;
        while ( (line=charStream.readLine()) != null){
            line = line.trim();
            parts = line.split(' ');

            if (TAG_FORMAT.equals(parts[0])){
                format = parts[1];

            } else if (TAG_COMMENT.equals(parts[0])){
                comments.add(parts[1]);

            } else if (TAG_ELEMENT.equals(parts[0])){
                currentElement = new PLYElement();
                currentElement.name = parts[1];
                currentElement.number = Int32.decode(parts[2]);
                elements.add(currentElement);

            } else if (TAG_PROPERTY.equals(parts[0])){
                final PLYProperty prop = new PLYProperty();
                prop.type = parts[1];
                if (parts.length==3){
                    //simple type
                    prop.name = parts[2];
                } else {
                    //list type
                    prop.sizeType = parts[2];
                    prop.valueType = parts[3];
                    prop.name = parts[4];
                }
                currentElement.properties.add(prop);

            } else if (TAG_HEADER_END.equals(parts[0])){
                return;
            }

        }

        throw new IOException(charStream, "Reached end of file before findind header end.");
    }
}
