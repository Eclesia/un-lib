
package science.unlicense.format.ply;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Specification :
 * http://paulbourke.net/dataformats/ply/
 *
 * @author Johann Sorel
 */
public class PLYFormat extends AbstractModel3DFormat {

    public static final PLYFormat INSTANCE = new PLYFormat();

    private PLYFormat() {
        super(new Chars("PLY"));
        shortName = new Chars("PLY");
        longName = new Chars("Polygon File Format");
        extensions.add(new Chars("ply"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new PLYStore(input);
    }

}
