# Unlicense Library #

This project focus on collecting and writing source code in the Public Domain and organize them in a self sufficient framework.

Web site : [http://unlicense.developpez.com](http://unlicense.developpez.com)

### Objectives ###

* Be **F**ree of **All** constraints, copy/paste whatever you want, it's public so it's already your's.
* Build an independent framework.

### Prerequise to build the projet ###

* JVM 1.8 (project is aiming for 1.5)
* Maven 3+
* Git

### Build project ###

1. Clone project with [Git](https://git-scm.com) : 
git clone https://bitbucket.org/Eclesia/un-lib.git
2. Compile using [Maven](http://maven.apache.org) :
mvn clean install