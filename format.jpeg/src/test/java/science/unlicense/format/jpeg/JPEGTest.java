package science.unlicense.format.jpeg;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class JPEGTest {

    private static final Vector2i32 b00 = new Vector2i32(0,0);
    private static final Vector2i32 b10 = new Vector2i32(1,0);
    private static final Vector2i32 b20 = new Vector2i32(2,0);
    private static final Vector2i32 b30 = new Vector2i32(3,0);
    private static final Vector2i32 b01 = new Vector2i32(0,1);
    private static final Vector2i32 b11 = new Vector2i32(1,1);
    private static final Vector2i32 b21 = new Vector2i32(2,1);
    private static final Vector2i32 b31 = new Vector2i32(3,1);

    @Test
    public void testRead() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/jpeg/Sample.jpg")).createInputStream();

        final ImageReader reader = new JPEGReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(4, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();
        final TupleRW storage = sm.createTuple();

        //values are in RGB
        //colors are not exact because of compression and YUV storage mode

        //BLUE
        sm.getTuple(new Vector2i32(0,0),storage);
        Assert.assertArrayEquals(new int[]{0,0,254}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 254, 255), image.getColor(b00));
        //BLACK
        sm.getTuple(new Vector2i32(1,0),storage);
        Assert.assertArrayEquals(new int[]{0,0,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 0, 255), image.getColor(b10));
        //WHITE
        sm.getTuple(new Vector2i32(2,0),storage);
        Assert.assertArrayEquals(new int[]{255,255,255}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 255, 255, 255), image.getColor(b20));
        //YELLOW
        sm.getTuple(new Vector2i32(3,0),storage);
        Assert.assertArrayEquals(new int[]{255,255,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(255, 255, 0, 255), image.getColor(b30));
        //RED
        sm.getTuple(new Vector2i32(0,1),storage);
        Assert.assertArrayEquals(new int[]{254,0,0}, storage.toInt());
        Assert.assertEquals(new ColorRGB(254, 0, 0, 255), image.getColor(b01));
        //GREEN
        sm.getTuple(new Vector2i32(1,1),storage);
        Assert.assertArrayEquals(new int[]{0,254,2}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 254, 2, 255), image.getColor(b11));
        //CYAN
        sm.getTuple(new Vector2i32(2,1),storage);
        Assert.assertArrayEquals(new int[]{0,255,255}, storage.toInt());
        Assert.assertEquals(new ColorRGB(0, 255, 255, 255), image.getColor(b21));
        //GRAY
        sm.getTuple(new Vector2i32(3,1),storage);
        Assert.assertArrayEquals(new int[]{100,100,100}, storage.toInt());
        Assert.assertEquals(new ColorRGB(100, 100, 100, 255), image.getColor(b31));

    }

}
