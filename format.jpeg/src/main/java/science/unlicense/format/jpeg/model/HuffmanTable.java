
package science.unlicense.format.jpeg.model;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.jpeg.JPEGDataStream;

/**
 * JPEG huffman table.
 *
 * TODO merge if possible with huffman decoding from PNG.
 *
 * @author Johann Sorel
 */
public final class HuffmanTable {

    public int tableClass;
    public int tableId;
    public int[] bits;
    public int[] huffVal;

    //rebuilded values
    public int[] huffSize;
    public int[] huffCode;
    public int[] eHufCo;
    public int[] eHufSi;
    //for decoding
    public int[] minCode;
    public int[] maxCode;
    public int[] valPtr;


    /**
     * C.2 Conversion of Huffman table specifications to tables of codes and code lengths.
     * Page 50
     */
    public void rebuild(){

        ////////////////////////////////////////////////////////////////////////
        //Conversion of Huffman table specifications to tables of codes and code lengths uses three procedures.
        //The first procedure (Figure C.1) generates a table of Huffman code sizes.
        //
        //Given a list BITS (1 to 16) containing the number of codes of each size, and a list HUFFVAL containing the symbol
        //values to be associated with those codes as described above, two tables are generated. The HUFFSIZE table contains a list
        //of code lengths; the HUFFCODE table contains the Huffman codes corresponding to those lengths.
        //Note that the variable LASTK is set to the index of the last entry in the table.
        huffSize = new int[4096];

        int k=0;
        int i=1;
        int j=1;
        int lastK=-1;

        for (;;){
            if (!(j>bits[i])){
                huffSize[k]=i;
                k++;
                j++;
            } else {
                i++;
                j=1;
                if (i>16){
                    huffSize[k]=0;
                    lastK = k;
                    break;
                }
            }
        }



        ////////////////////////////////////////////////////////////////////////
        //The second procedure (Figure C.2) generates the Huffman codes from the table built in Figure C.1.

        k=0;
        int code=0;
        int si=huffSize[0];
        huffCode = new int[lastK];

        for (;;){
            huffCode[k]=code;
            code++;
            k++;

            if (huffSize[k]==si){
                continue;
            } else if (huffSize[k]==0){
                break;
            }

            do {
                code = code << 1;
                si++;
            } while (huffSize[k]!=si);
        }

        //Two tables, HUFFCODE and HUFFSIZE, have now been generated. The entries in the tables are ordered according to
        //increasing Huffman code numeric value and length.
        //The encoding procedure code tables, EHUFCO and EHUFSI, are created by reordering the codes specified by
        //HUFFCODE and HUFFSIZE according to the symbol values assigned to each code in HUFFVAL.


        ////////////////////////////////////////////////////////////////////////
        //The third procedure (Figure C.3) generates the Huffman codes in symbol value order.

        //TODO bug here, not used yet, do this later
//        eHufCo = new int[huffCode.length];
//        eHufSi = new int[huffSize.length];
//        k=0;
//
//        do {
//            i=huffVal[k];
//            eHufCo[i]=huffCode[k];
//            eHufSi[i]=huffSize[k];
//            k++;
//        } while (k<lastK);


        ////////////////////////////////////////////////////////////////////////
        // Used later for decoding
        // F.2.2.3 The DECODE procedure Page 107
        // Figure F.15 – Decoder table generation
        minCode = new int[16+1];
        maxCode = new int[16+1];
        valPtr = new int[16+1];

        i=0;
        j=0;

        for (;;){
            i++;
            if (i>16){
                break;
            } else if (bits[i]==0){
                maxCode[i] = -1;
                continue;
            }

            valPtr[i] = j;
            minCode[i] = huffCode[j];
            j = j + bits[i]-1;
            maxCode[i] = huffCode[j];
            j++;
        }


    }

    /**
     * Decode next value from stream.
     * Spec : Figure F.15 – Decoder table generation
     * @param ds
     * @return
     */
    public int decode(final JPEGDataStream ds) throws IOException{
        int i=1;
        int code = ds.read();

        while (code > maxCode[i]){
            i++;
            code <<= 1;
            code |= ds.read();
        }

        int j = valPtr[i];
        j = j + code - minCode[i];
        return huffVal[j];
    }


}
