
package science.unlicense.format.jpeg.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public final class SOSComponent {

    public int cpSelector;
    public int dcSelector;
    public int acSelector;

    public void read(DataInputStream ds) throws IOException {
        cpSelector = ds.read();
        dcSelector = ds.readBits(4);
        acSelector = ds.readBits(4);
    }

}
