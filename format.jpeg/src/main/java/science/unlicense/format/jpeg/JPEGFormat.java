
package science.unlicense.format.jpeg;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;
import static science.unlicense.format.jpeg.JPEGConstants.SIGNATURE;

/**
 * JPEG image format.
 *
 * @author Johann Sorel
 */
public class JPEGFormat extends AbstractImageFormat{

    public static final JPEGFormat INSTANCE = new JPEGFormat();

    private JPEGFormat() {
        super(new Chars("jpeg"));
        shortName = new Chars("JPEG");
        longName = new Chars("Joint Photographic Experts Group");
        mimeTypes.add(new Chars("image/jpeg"));
        mimeTypes.add(new Chars("image/jpg"));
        extensions.add(new Chars("jpg"));
        extensions.add(new Chars("jpeg"));
        extensions.add(new Chars("jpe"));
        signatures.add(SIGNATURE);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new JPEGStore(this, source);
    }

}
