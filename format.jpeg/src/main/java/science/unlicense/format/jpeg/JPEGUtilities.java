
package science.unlicense.format.jpeg;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.jpeg.model.HuffmanTable;

/**
 * Various jpeg encode and decode methods.
 *
 * @author Johann Sorel
 */
public final class JPEGUtilities {

    private JPEGUtilities() {
    }

    /**
     * EXTEND is a procedure which converts the partially decoded DIFF
     * value of precision T to the full precision difference.
     * EXTEND is shown in Figure F.12. P 105
     *
     * @return
     */
    public static int extend(int v, int t){
        int vpt = (1 << (t - 1));
        while (v<vpt){
            int d = -1 << t;
            vpt = d + 1;
            v += vpt;
        }
        return v;
    }

    /**
     * F.2.2.2 Decoding procedure for AC coefficients. Page 105
     * The decoding procedure for AC coefficients is shown in Figures F.13 and F.14.
     */
    public static void decodeACCoeff(final JPEGDataStream ds,
            final int[] zz, final HuffmanTable acTable) throws IOException{

        int k=1;

        for (;;){
            final int rs = acTable.decode(ds);
            final int ssss = rs % 16;
            final int rrrr = rs >> 4;
            final int r = rrrr;

            if (ssss==0){
                if (r==15){
                    k += 16;
                    continue;
                } else {
                    break;
                }
            }

            k += r;

            // Decode_ZZ
            // Figure F.14 – Decoding a non-zero AC coefficient
            zz[k]=receive(ds,ssss);
            zz[k]=extend(zz[k],ssss);

            if (k==63) break;
            k++;
        }

    }


    /**
     * Spec F.2.2.4 The RECEIVE procedure. Page 110
     *
     * RECEIVE(SSSS) is a procedure which places the next SSSS bits of the entropy-coded segment into the low order bits of
     * DIFF, MSB first. It calls NEXTBIT and it returns the value of DIFF to the calling procedure (see Figure F.17).
     *
     * @param ssss
     * @param ds
     * @return
     */
    public static int receive(final JPEGDataStream ds, final int ssss) throws IOException{
        int i = 0;
        int v = 0;

        while (i!=ssss){
            i++;
            v <<= 1;
            v += ds.read();
        }
        return v;
    }

    /**
     * A.1.1 Dimensions and sampling factors.
     * Used to scale data units matrices.
     * inversed
     */
    public static void scale(int[][] in, int[][] out){
        final int X = out[0].length;
        final int Y = out.length;
        final int Hmax = in[0].length;
        final int Vmax = in.length;

        for (int vi=0; vi<Y; vi++){
            for (int hi=0; hi<X; hi++) {
                out[hi][vi] = in[hi / (X / Hmax)] [vi / (Y / Vmax)];
            }
        }
    }

}
