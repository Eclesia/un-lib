package science.unlicense.format.jpeg;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.common.api.number.UInt8;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import static science.unlicense.format.jpeg.JPEGConstants.*;
import science.unlicense.format.jpeg.model.DefineRestartIntervalMarker;
import science.unlicense.format.jpeg.model.HuffmanTable;
import science.unlicense.format.jpeg.model.SOFComponent;
import science.unlicense.format.jpeg.model.SOFFrameHeader;
import science.unlicense.format.jpeg.model.SOSComponent;
import science.unlicense.format.jpeg.model.SOSScanHeader;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import static science.unlicense.image.api.ImageSetMetadata.*;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.InterleavedModel;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.transform.DCT;
import science.unlicense.math.impl.transform.Quantization;
import science.unlicense.math.impl.transform.ZigZag;

/**
 * JPEG Image reader.
 *
 * Special thanks to http://nothings.org/ where stb_image.c where the debug
 * mode was a big help to figure out the decoding steps.
 * Thanks to Sean Barrett for being awesome ;).
 *
 * @author Johann Sorel
 */
public class JPEGReader extends AbstractImageReader{

    private static final ZigZag ZIGZAG = new ZigZag(JPEGConstants.ZIGZAG);

    private final Quantization[] qtables = new Quantization[4];
    private final HuffmanTable[] huffmantablesDC = new HuffmanTable[4];
    private final HuffmanTable[] huffmantablesAC = new HuffmanTable[4];
    private final DCT DCT = new DCT(8);
    private int encodingType;
    //frame header infos
    private SOFFrameHeader sof;
    //scan header infos
    private SOSScanHeader sos;
    //restart interval, may be null
    private DefineRestartIntervalMarker dri;

    //rebuilded image
    private TypedNode mdImage = null;
    private Image image = null;

    //temporary buffers used for decoding
    private final int[] ibuffer1 = new int[64];
    private final int[] ibuffer2 = new int[64];
    private final float[] fbuffer1 = new float[64];

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    protected Image read(ImageReadParameters params, BacktrackInputStream stream) throws IOException {

        final DataInputStream ds = new DataInputStream(stream);

        final int soi = ds.readUShort();
        if (soi != MARKER_SOI){
            throw new IOException(this, "Stream is not a JPEG image.");
        }

        while (true){
            final int marker = ds.readUShort();

            if (  MARKER_SOF_0==marker
              || MARKER_SOF_1==marker
              || MARKER_SOF_2==marker
              || MARKER_SOF_3==marker
              || MARKER_SOF_5==marker
              || MARKER_SOF_6==marker
              || MARKER_SOF_7==marker){
                //encoding type
                encodingType = marker;
                sof = new SOFFrameHeader();
                sof.read(ds);
            } else if (MARKER_JPG==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_SOF_9==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_SOF_10==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_SOF_11==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_SOF_13==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_SOF_14==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_SOF_15==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_DHT==marker){
                readHuffmanTable(ds);
            } else if (MARKER_DAC==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_RST_ST<=marker && MARKER_RST_ED>=marker){
                System.out.println("la");
                //skip those
            } else if (MARKER_EOI==marker){
                readEndOfImage();
                return image;
            } else if (MARKER_SOS==marker){
                readScan(ds);
                //return image;
            } else if (MARKER_DQT==marker){
                readQuantizationTables(ds);
            } else if (MARKER_DNL==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_DRI==marker){
                dri = new DefineRestartIntervalMarker();
                dri.read(ds);
            } else if (MARKER_DHP==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_EXP==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_APP_ST<=marker && MARKER_APP_ED>=marker){
                final byte[] app = readApp(ds);
            } else if (MARKER_JPG_ST<=marker && MARKER_JPG_ED>=marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_COM==marker){
                final Chars comment = readComment(ds);
            } else if (MARKER_TEM==marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else if (MARKER_RES_ST<=marker && MARKER_RES_ED>=marker){
                throw new IOException(this, "Not supported yet : " + marker);
            } else {
                throw new IOException(this, "Unknwoned or unexpected marker : " + marker);
            }

        }

    }

    /**
     * Read application specific, unknowned type.
     */
    private static byte[] readApp(final DataInputStream ds) throws IOException {
        final int size = ds.readUShort();
        return ds.readFully(new byte[size-2]);
    }

    /**
     * Read comment.
     */
    private static Chars readComment(final DataInputStream ds) throws IOException {
        final int size = ds.readUShort();
        final byte[] array = new byte[size-2];
        ds.readFully(array);
        return new Chars(array);
    }

    /**
     * Read Quantization tables.
     */
    private void readQuantizationTables(final DataInputStream ds) throws IOException {

        int size = ds.readUShort();
        size -=2;

        while (size>0){
            final int Pq = ds.readBits(4);
            final int Tq = ds.readBits(4);
            size--;
            if (Tq > 3) {
                throw new IOException(this, "Invalid table index : " + Tq +" value should not exceed 3");
            }

            final int[] itable = new int[64];
            final float[] ftable = new float[64];
            if (Pq==0){
                //8bit precision
                ds.readUByte(itable);
                size -= 64;
            } else if (Pq==1){
                //16 bit precision
                ds.readUShort(itable);
                size -= 128;
            } else {
                throw new IOException(this, "Invalid precision : " + Pq +" value should be 0 or 1");
            }
            ZIGZAG.unZigZag(itable, ftable);
            qtables[Tq] = new Quantization(ftable);
        }
    }

    /**
     * Read huffman tables.
     */
    private void readHuffmanTable(final DataInputStream ds) throws IOException{
        int size = ds.readUShort();
        size-=2;

        while (size>0){
            final HuffmanTable table = new HuffmanTable();
            table.tableClass = ds.readBits(4);
            table.tableId = ds.readBits(4);
            size -= 1;
            if (table.tableId > 3){
                throw new IOException(this, "Invalid table index : " + table.tableId +" value should not exceed 3");
            }
            if (table.tableClass > 1){
                throw new IOException(this, "Invalid table class : " + table.tableClass +" value should not exceed 1");
            }

            int nbval=0;

            //1 offset to follow algorithm in spec.
            //algos are too akward for me so I decided to stick to the spec.
            table.bits = new int[16+1];
            for (int i=1;i<17;i++){
                table.bits[i] = ds.readUByte();
                nbval+=table.bits[i];
            }
            size -= 16;

            table.huffVal = new int[nbval];
            ds.readUByte(table.huffVal);
            size -= nbval;

            table.rebuild();
            if (table.tableClass==0){
                huffmantablesDC[table.tableId] = table;
            } else {
                huffmantablesAC[table.tableId] = table;
            }
        }
    }

    /**
     * Read scan header.
     */
    private void readScan(final DataInputStream ds) throws IOException {

        //read header informations /////////////////////////////////////////////
        sos = new SOSScanHeader();
        sos.read(ds);

        //decode image /////////////////////////////////////////////////////////
        if (encodingType == MARKER_SOF_0){
            //baseline
            if (sos.scans.length == 1){
                decodeBaselineNonInterleaved(ds);
            } else {
                decodeBaselineInterleaved(ds);
            }
        } else if (encodingType == MARKER_SOF_1){
            throw new IOException(this, "Unsupported encoding SOF15");
        } else if (encodingType == MARKER_SOF_2){
            //progressive
            decodeProgressive();
        } else if (encodingType == MARKER_SOF_3){
            throw new IOException(this, "Unsupported encoding SOF15");
        } else if (encodingType == MARKER_SOF_5){
            throw new IOException(this, "Unsupported encoding SOF15");
        } else if (encodingType == MARKER_SOF_6){
            throw new IOException(this, "Unsupported encoding SOF15");
        } else if (encodingType == MARKER_SOF_7){
            throw new IOException(this, "Unsupported encoding SOF15");
        } else if (encodingType == MARKER_SOF_9){
            throw new IOException(this, "Unsupported encoding SOF15");
        } else if (encodingType == MARKER_SOF_10){
            throw new IOException(this, "Unsupported encoding SOF15");
        } else if (encodingType == MARKER_SOF_11){
            throw new IOException(this, "Unsupported encoding SOF15");
        } else if (encodingType == MARKER_SOF_13){
            throw new IOException(this, "Unsupported encoding SOF15");
        } else if (encodingType == MARKER_SOF_14){
            throw new IOException(this, "Unsupported encoding SOF15");
        } else if (encodingType == MARKER_SOF_15){
            throw new IOException(this, "Unsupported encoding SOF15");
        } else {
            throw new IOException(this, "Unknowed encoding");
        }

    }

    private void decodeBaselineNonInterleaved(final DataInputStream ds) throws IOException {
        throw new IOException(this, "Not supported yet");

    }

    private void decodeBaselineInterleaved(final DataInputStream dis) throws IOException {

        final JPEGDataStream ds = new JPEGDataStream(dis);

        //rebuild image, we will fill values using sample model since
        //reading is not line by line but block by block
        mdImage =
        new DefaultTypedNode(MD_IMAGE,new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,sof.width)}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,sof.height)})
        });

        final byte[] data = new byte[sof.width*sof.height*3];
        final Buffer bank = DefaultBufferFactory.wrap(data);
        final ImageModel sm = new InterleavedModel(new UndefinedSystem(3), UInt8.TYPE);
        final ImageModel cm = DerivateModel.create(sm, new int[]{0,1,2}, null, null, ColorSystem.RGB_8BITS);
        image = new DefaultImage(bank, new Extent.Long(sof.width, sof.height), sm, cm);
        final TupleGridCursor cursor = sm.asTupleBuffer(image).cursor();

        //find MCU appropriate size
        final int mcuWidth = 8 * sof.getMaxHorizontalSamplingFactor();
        final int mcuHeight = 8 * sof.getMaxVertitalSampligFactor();
        final int nbMcuX = (int) Math.ceil( (float) sof.width / mcuWidth );
        final int nbMcuY = (int) Math.ceil( (float) sof.height / mcuHeight );

        //build the MCU
        final MCU mcu = new MCU(sos.scans.length,mcuWidth,mcuHeight);
        for (int i=0; i<sos.scans.length; i++){
            final SOSComponent scomp = sos.scans[i];
            final SOFComponent fcomp = sof.getComponent(scomp.cpSelector);
            mcu.setUnit(i, fcomp, scomp);
        }

        final int[] dcPredictions = new int[sof.cspectables.length];
        //some caches
        final int[] yCbCr = new int[3];
        final Vector2i32 coords = new Vector2i32();
        final int[] rgb = new int[3];

        //loop on MCU and build values
        int restartCounter = (dri==null) ? -1 : dri.ri;
        int offsety = 0;
        int offsetx = 0;
        for (int mcuy=0; mcuy<nbMcuY; mcuy++,offsety+=mcuHeight){
            offsetx=0;
            for (int mcux=0; mcux<nbMcuX; mcux++,offsetx+=mcuWidth){

                //decode mcu
                for (int i=0; i<mcu.units.length; i++){
                    decodeUnit(ds, dcPredictions, mcu.units[i], i);
                }

                //handle restart intervals
                if (restartCounter>=0){
                    if (--restartCounter==0){
                        //reset counter
                        restartCounter = dri.ri;
                        //Reset dc predictions
                        Arrays.fill(dcPredictions, 0);
                        //skip remaining bits if any
                        ds.skipRemainingBits();
                    }
                }

                mcu.scale();

                for (int py=0; py<mcuHeight; py++){
                    if (offsety+py >= sof.height) continue; //out of image range

                    for (int px=0; px<mcuWidth; px++){
                        if (offsetx+px >= sof.width) continue; //out of image range

                        //convert to RGB color
                        //TODO add new color model for direct YCbCr
                        yCbCr[0] = mcu.units[0].result[px][py];
                        yCbCr[1] = mcu.units[1].result[px][py];
                        yCbCr[2] = mcu.units[2].result[px][py];
                        Colors.YCbCrtoRGB(yCbCr,rgb);

                        //set in image
                        coords.x = offsetx+px;
                        coords.y = offsety+py;
                        cursor.moveTo(coords);
                        cursor.samples().set(0,rgb[0]);
                        cursor.samples().set(1,rgb[1]);
                        cursor.samples().set(2,rgb[2]);
                    }
                }
            }
        }

    }

    private void decodeUnit(final JPEGDataStream ds, final int[] preds, MCU.DataUnit unit, int index) throws IOException{

        for (int sy=0; sy<unit.SOFComp.verticalSamplingFactor; sy++){
            for (int sx=0; sx<unit.SOFComp.horizontalSamplingFactor; sx++){

                //reset buffers
                Arrays.fill(ibuffer1, 0);
                Arrays.fill(ibuffer2, 0);
                Arrays.fill(fbuffer1, 0);

                // F.2.2.1 Huffman decoding of DC coefficients Page 104
                final int t = huffmantablesDC[unit.SOSComp.dcSelector].decode(ds);
                int diff = JPEGUtilities.receive(ds,t);
                diff = JPEGUtilities.extend(diff, t);
                ibuffer1[0] = preds[index] + diff;
                preds[index] = ibuffer1[0];

                // F.2.2.2 Decoding procedure for AC coefficients
                JPEGUtilities.decodeACCoeff(ds, ibuffer1, huffmantablesAC[unit.SOSComp.acSelector]);

                final Quantization qtable = qtables[unit.SOFComp.quantizationTable];
                ZIGZAG.unZigZag(ibuffer1, ibuffer2);
                qtable.unQuantify(ibuffer2, fbuffer1);
                DCT.idct(fbuffer1);


                // A.3.1 Level shift Page 26 : shift to apply on each value
                final int shift = (1 << (sof.samplePrecision - 1));
                // A.3.1 Level shift Page 26 : clamp to apply on each value
                final int max = (1 << sof.samplePrecision) - 1;

                int inc = 0;
                for (int py=0; py<8; py++){
                    for (int px=0; px<8; px++){
                        final float sample = fbuffer1[inc++] + shift;
                        final int result = Maths.round(Maths.clamp(sample, 0, max));
                        unit.data[8*sx+px][8*sy+py] = result;
                    }
                }
            }
        }
    }

    private void decodeProgressive() throws IOException {
        throw new IOException(this, "Not supported yet");
    }

    private void readEndOfImage() throws IOException {
    }

}
