
package science.unlicense.format.jpeg.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * JPEG Scan informations : SOS tag
 *
 * @author Johann Sorel
 */
public class SOSScanHeader {

    public SOSComponent[] scans;
    public int spectralStart;
    public int spectralEnd;
    public int bitPositionHigh;
    public int bitPositionLow;

    public void read(DataInputStream ds) throws IOException{
        final int size = ds.readUShort();
        final int nbComp = ds.readUByte();

        //read header informations /////////////////////////////////////////////
        scans = new SOSComponent[nbComp];

        for (int i=0; i<nbComp; i++){
            scans[i] = new SOSComponent();
            scans[i].read(ds);
        }
        spectralStart = ds.readUByte();
        spectralEnd = ds.readUByte();
        bitPositionHigh = ds.readBits(4);
        bitPositionLow = ds.readBits(4);
    }

}
