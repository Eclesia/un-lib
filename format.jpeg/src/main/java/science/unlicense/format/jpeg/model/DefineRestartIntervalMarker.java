

package science.unlicense.format.jpeg.model;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class DefineRestartIntervalMarker implements Marker {

    public int lr;
    public int ri;

    public void read(DataInputStream ds) throws IOException {
        lr = ds.readUShort();
        ri = ds.readUShort();
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
