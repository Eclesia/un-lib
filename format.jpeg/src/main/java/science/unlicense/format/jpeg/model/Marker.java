

package science.unlicense.format.jpeg.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Global interface for all jpeg markers.
 *
 * @author Johann Sorel
 */
public interface Marker {

    void read(DataInputStream ds) throws IOException;

    void write(DataOutputStream ds) throws IOException;

}
