
package science.unlicense.format.jpeg.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.Maths;

/**
 * JPEG Frame header : SOF tag
 *
 * @author Johann Sorel
 */
public class SOFFrameHeader {

    public SOFComponent[] cspectables;
    public int samplePrecision;
    public int height;
    public int width;

    /**
     * Get SOF component for given id.
     * @param id
     * @return
     */
    public SOFComponent getComponent(int id){
        for (int j=0; j<cspectables.length; j++){
            if (cspectables[j].componentId == id){
                return cspectables[j];
            }
        }
        return null;
    }

    public int getMaxHorizontalSamplingFactor(){
        int max = 0;
        for (int i=0;i<cspectables.length;i++){
            max = Maths.max(max,cspectables[i].horizontalSamplingFactor);
        }
        return max;
    }

    public int getMaxVertitalSampligFactor(){
        int max = 0;
        for (int i=0; i<cspectables.length; i++){
            max = Math.max(max,cspectables[i].verticalSamplingFactor);
        }
        return max;
    }

    public void read(DataInputStream ds) throws IOException{
        final int size = ds.readUShort();
        samplePrecision = ds.readUByte();
        height = ds.readUShort();
        width = ds.readUShort();
        final int nbcspec = ds.read();
        cspectables = new SOFComponent[nbcspec];
        for (int i=0;i<nbcspec;i++){
            final SOFComponent spec = new SOFComponent();
            spec.componentId = ds.read();
            spec.horizontalSamplingFactor = ds.readBits(4);
            spec.verticalSamplingFactor = ds.readBits(4);
            spec.quantizationTable = ds.readUByte();
            cspectables[i] = spec;
        }
    }

}
