
package science.unlicense.format.jpeg;

import science.unlicense.format.jpeg.model.SOSComponent;
import science.unlicense.format.jpeg.model.SOFComponent;

/**
 * JPEG encoding works by blocks factor of 8x8 pixels, those are called MCU.
 * Minimum Coded Unit; MCU: The smallest group of data units that is coded.
 *
 * @author Johann Sorel
 */
public class MCU {

    public final DataUnit[] units;
    public final int scaledWidth;
    public final int scaledHeight;

    public MCU(int size, int scaledWidth, int scaledHeight) {
        this.units = new DataUnit[size];
        this.scaledWidth = scaledWidth;
        this.scaledHeight = scaledHeight;
    }

    public void setUnit(int index, SOFComponent SOFComp, SOSComponent SOSComp){
        units[index] = new DataUnit(SOFComp, SOSComp);
    }

    public void scale() {
        for (int i=0; i<units.length; i++){
            JPEGUtilities.scale(units[i].data, units[i].result);
        }
    }

    public class DataUnit{
        //width and height will depend on component sample.
        public final int[][] data;
        //containing final result
        public final int[][] result;

        public final SOFComponent SOFComp;
        public final SOSComponent SOSComp;

        public DataUnit(final SOFComponent SOFComp, final SOSComponent SOSComp) {
            this.data = new int[8*SOFComp.horizontalSamplingFactor][8*SOFComp.verticalSamplingFactor];
            this.result = new int[scaledWidth][scaledHeight];
            this.SOFComp = SOFComp;
            this.SOSComp = SOSComp;
        }
    }

}
