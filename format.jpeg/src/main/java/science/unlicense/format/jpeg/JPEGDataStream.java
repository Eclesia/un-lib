
package science.unlicense.format.jpeg;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.WrapInputStream;

/**
 * F.2.2.5The NEXTBIT procedure.
 * Page 110
 *
 * @author Johann Sorel
 */
public class JPEGDataStream extends WrapInputStream {

    private int b = 0;
    private int cnt = 0;

    public JPEGDataStream(ByteInputStream in) {
        super(in);
    }

    public void skipRemainingBits(){
        cnt=0;
    }

    @Override
    public int read() throws IOException {

        test:
        if (cnt==0){
            b=in.read();
            cnt=8;

            if (b==0xFF){
                final int b2 = in.read();
                if (b2==0) break test;

                if (b2==(JPEGConstants.MARKER_DNL&0xFF)){
                    throw new IOException(this, "TODO DNL");
                } else if (b2>=(JPEGConstants.MARKER_RST_ST&0xFF) && b2<=(JPEGConstants.MARKER_RST_ED&0xFF)){
                    //skip those
                    b=in.read();
                } else {
                    throw new IOException(this, "Unexpected byte value :" +b2);
                }
            }
        }
        final int bit = (b>>7 & 0x01);
        cnt--;
        b <<= 1;
        return bit;
    }

}
