
package science.unlicense.archive.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.store.DefaultFormat;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractArchiveFormat extends DefaultFormat implements ArchiveFormat {

    public AbstractArchiveFormat(Chars identifier) {
        super(identifier);
        resourceTypes.add(Archive.class);
    }

    @Override
    public Chars getPrefix() {
        return null;
    }

}
