

package science.unlicense.archive.api;

import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.path.PathResolver;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public interface Archive extends Path, PathResolver, Store {


}
