
package science.unlicense.archive.api;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.DocumentType;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.concurrent.api.Formats;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;

/**
 * Convenient methods for archive manipulation.
 *
 * @author Johann Sorel
 */
public final class Archives {

    private Archives() {}

    /**
     * Lists available media formats.
     * @return array of MediaFormat, never null but can be empty.
     */
    public static ArchiveFormat[] getFormats(){
        return (ArchiveFormat[]) Formats.getFormatsOfType(ArchiveFormat.class);
    }

    public static boolean canDecode(Object input) throws IOException {
        return Formats.canDecode(input, new ArraySequence(getFormats()));
    }

    /**
     * Convinient method to open a archive of unknowned format.
     * The method will loop on available formats until one can decode the source.
     *
     * @param input
     * @return Archive, never null
     * @throws IOException if not format could support the source.
     */
    public static Archive open(Object input) throws IOException {
        return (Archive) Formats.open(input, new ArraySequence(getFormats()));
    }

    /**
     * Convenient method to obtain an archive path.
     * The method will loop on available formats until one can open the input.
     *
     * @param path
     * @return Archive, never null
     * @throws IOException if not format could support the source.
     */
    public static Archive open(final Path path, Dictionary parameters) throws IOException {
        final ArchiveFormat[] formats = getFormats();
        if (formats.length == 0) {
            throw new IOException(path, "No archive formats available.");
        }

        for (int i=0;i<formats.length;i++) {
            if (formats[i].canDecode(path)) {
                //prepare parameters
                final DocumentType type = formats[i].getParameters();
                final Document p = new DefaultDocument(type);
                if (parameters!=null) {
                    for (FieldType param : p.getType().getFields()) {
                        Object value = parameters.getValue(param.getId());
                        if (value!=null) {
                            p.setPropertyValue(param.getId(), value);
                        }
                    }
                }

                return formats[i].open(p);
            }
        }

        throw new IOException(path, "No archive formats can read given input");
    }

    public static void extract(Path archive, final Path outFolder) throws IOException{
        if (!(archive instanceof Archive)) {
            archive = open(archive);
        }

        recursiveExtract(archive, outFolder);

    }

    public static void extract(Path archive, final Path outFolder, Dictionary parameters) throws IOException{
        if (!(archive instanceof Archive)) {
            archive = open(archive,parameters);
        }

        final Path outPath = outFolder;
        outPath.createContainer();
        final Iterator ite = archive.getChildren().createIterator();
        while (ite.hasNext()) {
            recursiveExtract((Path) ite.next(), outPath);
        }
    }

    public static void recursiveExtract(Path source, Path target) throws IOException{

        if (source.isContainer()) {
            final Path outPath = target.resolve(source.getName());
            outPath.createContainer();

            final Iterator ite = source.getChildren().createIterator();
            while (ite.hasNext()) {
                recursiveExtract((Path) ite.next(), outPath);
            }

        } else {
            final Path outPath = target.resolve(source.getName());
            final ByteInputStream in = source.createInputStream();
            final ByteOutputStream out = outPath.createOutputStream();
            IOUtilities.copy(in,out);
        }

    }

}
