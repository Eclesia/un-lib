

package science.unlicense.archive.api;

import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.PathFormat;
import science.unlicense.encoding.api.store.Format;

/**
 *
 * @author Johann Sorel
 */
public interface ArchiveFormat extends PathFormat, Format {

    public static final FieldType PARAM_ENCODING = new FieldTypeBuilder(new Chars("encoding")).valueClass(CharEncoding.class).build();

    /**
     * Test if a path can open by an archive.
     * @param candidate
     * @return true if given path can be opened
     */
    @Override
    boolean canDecode(Object candidate) throws IOException;

    /**
     * Open archive at given path.The archive is not necessarly open and read right away.
     *
     * @param candidate
     * @return Archive path
     * @throws science.unlicense.encoding.api.io.IOException
     */
    Archive open(Object candidate) throws IOException;

}
