
package science.unlicense.grammar.bnf;

import org.junit.Test;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;

/**
 *
 * @author Johann Sorel
 */
public class BNFReaderTest {

     private static final CharEncoding ENC = CharEncodings.US_ASCII;

    @Test
    public void testRead() throws Exception {

        final String bnf = "<somerule> ::= <r1> <r2> ";
        final Chars cs = new Chars(bnf,ENC);
        final ByteInputStream in = new ArrayInputStream(cs.toBytes());


        BNFReader reader = new BNFReader();
        reader.setInput(in);
        reader.setEncoding(ENC);

        Dictionary dico = reader.read();
        final Iterator ite = dico.getPairs().createIterator();
        while (ite.hasNext()) {
            Pair pair = (Pair) ite.next();
            System.out.println(pair.getValue1());
            System.out.println(pair.getValue2());
        }

    }
}
