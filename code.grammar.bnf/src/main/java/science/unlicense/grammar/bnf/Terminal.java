package science.unlicense.grammar.bnf;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.DefaultNode;

/**
 * @author Johann Sorel
 */
public class Terminal extends DefaultNode {

    private Chars name;
    private Object value;

    public Terminal() {
        super(false);
    }

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Chars toChars() {
        return new Chars("T:").concat(CObjects.toChars(value));
    }

}
