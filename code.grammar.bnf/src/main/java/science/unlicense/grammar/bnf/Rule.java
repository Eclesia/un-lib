package science.unlicense.grammar.bnf;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.model.tree.DefaultNode;
import science.unlicense.common.api.model.tree.Node;

/**
 * A Rule is a serie of rules or terminal nodes.
 * See BNF,ABNF,AntLR for more accurate definition.
 *
 * @author Johann Sorel
 */
public class Rule extends DefaultNode {

    private Chars name;
    private boolean isChoice = false;

    public Rule() {
        super(true);
    }

    public Rule(Node child) {
        super(true);
        getChildren().add(child);
    }

    public Rule(Collection children) {
        super(true);
        getChildren().addAll(children);
    }

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        this.name = name;
    }

    public Chars toChars() {
        if (isChoice) {
            return new Chars("C;").concat(name);
        } else {
            return new Chars("R:").concat(name);
        }
    }

    public boolean isChoice() {
        return isChoice;
    }

    public void setChoice(boolean choice) {
        this.isChoice = choice;
    }
}
