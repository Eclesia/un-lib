package science.unlicense.grammar.bnf;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.DefaultNode;
import science.unlicense.common.api.model.tree.Node;

/**
 * @author Johann Sorel
 */
public class Cardinal extends DefaultNode {

    private int minOccurences = 1;
    private int maxOccurences = 1;

    public Cardinal(Node child) {
        super(true);
        getChildren().add(child);
    }

    public Cardinal(Node child, int min, int max) {
        super(true);
        getChildren().add(child);
        this.minOccurences = min;
        this.maxOccurences = max;
    }

    public int getMinOccurences() {
        return minOccurences;
    }

    public void setMinOccurences(int minOccurences) {
        this.minOccurences = minOccurences;
    }

    public int getMaxOccurences() {
        return maxOccurences;
    }

    public void setMaxOccurences(int maxOccurences) {
        this.maxOccurences = maxOccurences;
    }

    public Chars toChars() {
        Chars str = new Chars(""+minOccurences+":"+maxOccurences);
        return str.concat('-').concat(((Node) getChildren().get(0)).toChars());
    }

}
