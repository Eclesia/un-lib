package science.unlicense.grammar.bnf;

/**
 * @author Johann Sorel
 */
public final class BNFConstants {

    //commun to all bnf variants

    /** space character in unicode */
    public static final int CHAR_SPACE = 32;
    /** tab character in unicode */
    public static final int CHAR_TAB = 9;
    /** new line character in unicode */
    public static final int CHAR_NEWLINE = 10;

    //specific to BNF

    /** < character in unicode */
    public static final int CHAR_BNF_RULENAME_START = 60;
    /** > character in unicode */
    public static final int CHAR_BNF_RULENAME_END = 62;
    /** | character in unicode */
    public static final int CHAR_BNF_OR = 124;
    /** : character in unicode */
    public static final int CHAR_BNF_DOT = 46;
    /** : character in unicode */
    public static final int CHAR_BNF_DOUBLEDOT = 58;
    /** = character in unicode */
    public static final int CHAR_BNF_EQUAL = 61;
    /** [ character in unicode */
    public static final int CHAR_BNF_OPT_START = 91;
    /** ] character in unicode */
    public static final int CHAR_BNF_OPT_END = 93;
    /** { character in unicode */
    public static final int CHAR_BNF_GROUP1_START = 123;
    /** } character in unicode */
    public static final int CHAR_BNF_GROUP1_END = 125;
    /** ( character in unicode */
    public static final int CHAR_BNF_GROUP2_START = 40;
    /** ) character in unicode */
    public static final int CHAR_BNF_GROUP2_END = 41;

    private BNFConstants(){}

}
