

package science.unlicense.format.webp;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.RIFFElement;
import science.unlicense.format.riff.RIFFReader;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import science.unlicense.format.webp.model.ALPHChunk;
import science.unlicense.format.webp.model.ANIMChunk;
import science.unlicense.format.webp.model.ANMFChunk;
import science.unlicense.format.webp.model.EXIFChunk;
import science.unlicense.format.webp.model.ICCPChunk;
import science.unlicense.format.webp.model.VP8Chunk;
import science.unlicense.format.webp.model.VP8LChunk;
import science.unlicense.format.webp.model.VP8XChunk;
import science.unlicense.format.webp.model.XMPChunk;

/**
 *
 * @author Johann Sorel
 */
public class WebPReader extends AbstractImageReader{

    private TypedNode mdWebp = null;

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {

        final Dictionary chunkMap = new HashDictionary();
        chunkMap.add(WebPConstants.CHUNK_VP8, VP8Chunk.class);
        chunkMap.add(WebPConstants.CHUNK_VP8L, VP8LChunk.class);
        chunkMap.add(WebPConstants.CHUNK_VP8X, VP8XChunk.class);
        chunkMap.add(WebPConstants.CHUNK_ALPH, ALPHChunk.class);
        chunkMap.add(WebPConstants.CHUNK_ANIM, ANIMChunk.class);
        chunkMap.add(WebPConstants.CHUNK_ANMF, ANMFChunk.class);
        chunkMap.add(WebPConstants.CHUNK_EXIF, EXIFChunk.class);
        chunkMap.add(WebPConstants.CHUNK_ICCP, ICCPChunk.class);
        chunkMap.add(WebPConstants.CHUNK_XMP, XMPChunk.class);

        final RIFFReader riffreader = new RIFFReader(chunkMap);
        riffreader.setInput(getInput());

        while (riffreader.hasNext()){
            final RIFFElement ele = riffreader.next();
            System.out.println(ele);
        }

        return null;
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {

        if (mdWebp == null){
            //read metas
            readMetadatas(stream);
            stream.rewind();
        }

        return null;
    }

}
