

package science.unlicense.format.webp.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.webp.WebPConstants;

/**
 * Animation chunk.
 *
 * @author Johann Sorel
 */
public class ANIMChunk extends DefaultChunk{

    /** 32bits : animation background color in RGBA */
    public int backgroundColorRGBA;
    /** 16bits : number of loops, 0 for infinite */
    public int nbLoop;

    public ANIMChunk() {
        super(WebPConstants.CHUNK_ANIM);
    }

    protected void readInternal(DataInputStream ds) throws IOException {
        backgroundColorRGBA = ds.readInt();
        nbLoop = ds.readUShort();
    }

}
