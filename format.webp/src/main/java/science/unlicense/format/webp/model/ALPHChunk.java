

package science.unlicense.format.webp.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.webp.WebPConstants;

/**
 * Alpha chunk.
 *
 * @author Johann Sorel
 */
public class ALPHChunk extends DefaultChunk{

    /** 2bits : reserved */
    public int reserved;
    /** 2bits : preprocessing , see specification */
    public int preprocessing;
    /** 2bits : filter , see specification */
    public int filter;
    /** 2bits : compression, see specification */
    public int compression;
    /** chunksize - 1 bytes : alpha datas */
    public byte[] alphadata;

    public ALPHChunk() {
        super(WebPConstants.CHUNK_ALPH);
    }

    protected void readInternal(DataInputStream ds) throws IOException {
        reserved = ds.readBits(2);
        preprocessing = ds.readBits(2);
        filter = ds.readBits(2);
        compression = ds.readBits(2);
        alphadata = ds.readFully(new byte[(int) size-1]);
    }

}
