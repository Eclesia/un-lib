

package science.unlicense.format.webp;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class WebPConstants {

    public static final Chars CHUNK_VP8 = Chars.constant(new byte[]{'V','P','8',' '});
    public static final Chars CHUNK_VP8L = Chars.constant(new byte[]{'V','P','8','L'});
    public static final Chars CHUNK_VP8X = Chars.constant(new byte[]{'V','P','8','X'});
    public static final Chars CHUNK_ICCP = Chars.constant(new byte[]{'I','C','C','P'});
    public static final Chars CHUNK_ALPH = Chars.constant(new byte[]{'A','L','P','H'});
    public static final Chars CHUNK_ANIM = Chars.constant(new byte[]{'A','N','I','M'});
    public static final Chars CHUNK_ANMF = Chars.constant(new byte[]{'A','N','M','F'});
    public static final Chars CHUNK_EXIF = Chars.constant(new byte[]{'E','X','I','F'});
    public static final Chars CHUNK_XMP = Chars.constant(new byte[]{'X','M','P',' '});

    private WebPConstants(){}

}
