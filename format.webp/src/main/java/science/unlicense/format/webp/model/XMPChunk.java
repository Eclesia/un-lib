

package science.unlicense.format.webp.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.webp.WebPConstants;

/**
 * XMP metadata chunk.
 *
 * @author Johann Sorel
 */
public class XMPChunk extends DefaultChunk{

    //TODO XMP data decoder
    public byte[] xmpData;

    public XMPChunk() {
        super(WebPConstants.CHUNK_XMP);
    }

    protected void readInternal(DataInputStream ds) throws IOException {
        xmpData = ds.readFully(new byte[(int) size]);
    }

}
