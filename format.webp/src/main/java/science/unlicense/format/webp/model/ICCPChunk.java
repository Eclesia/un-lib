

package science.unlicense.format.webp.model;

import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.webp.WebPConstants;

/**
 * ICCP color profile chunk.
 *
 * @author Johann Sorel
 */
public class ICCPChunk extends DefaultChunk{

    public ICCPChunk() {
        super(WebPConstants.CHUNK_ICCP);
    }

}
