

package science.unlicense.format.webp.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.webp.WebPConstants;

/**
 * EXIF metadata chunk.
 *
 * @author Johann Sorel
 */
public class EXIFChunk extends DefaultChunk{

    //TODO exif data decoder
    public byte[] exifData;

    public EXIFChunk() {
        super(WebPConstants.CHUNK_EXIF);
    }

    protected void readInternal(DataInputStream ds) throws IOException {
        exifData = ds.readFully(new byte[(int) size]);
    }

}
