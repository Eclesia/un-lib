

package science.unlicense.format.webp.model;

import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.webp.WebPConstants;

/**
 *
 * Containts a VP8 frame.
 *
 * specification :
 * http://tools.ietf.org/html/rfc6386
 *
 * @author Johann Sorel
 */
public class VP8Chunk extends DefaultChunk{

    public VP8Chunk() {
        super(WebPConstants.CHUNK_VP8);
    }

}
