
package science.unlicense.format.webp;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * docs, info and specification :
 * http://en.wikipedia.org/wiki/Progressive_Graphics_File
 * https://developers.google.com/speed/webp/docs/riff_container
 * http://tools.ietf.org/html/rfc6386
 *
 * @author Johann Sorel
 */
public class WebPFormat extends AbstractImageFormat {

    public static final WebPFormat INSTANCE = new WebPFormat();

    private WebPFormat() {
        super(new Chars("webp"));
        shortName = new Chars("WEBP");
        longName = new Chars("WebP");
        mimeTypes.add(new Chars("image/webp"));
        extensions.add(new Chars("webp"));
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new WebPStore(this, source);
    }

}
