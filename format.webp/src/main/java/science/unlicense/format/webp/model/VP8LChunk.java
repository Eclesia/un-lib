

package science.unlicense.format.webp.model;

import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.vp8.vp8l.VP8LReader;
import science.unlicense.format.webp.WebPConstants;

/**
 * Contains a lossless image.
 *
 * specification :
 * https://gerrit.chromium.org/gerrit/gitweb?p=webm/libwebp.git;a=blob;f=doc/webp-lossless-bitstream-spec.txt;hb=master
 *
 * @author Johann Sorel
 */
public class VP8LChunk extends DefaultChunk{

    public VP8LChunk() {
        super(WebPConstants.CHUNK_VP8L);
    }

    @Override
    protected void readInternal(DataInputStream ds) throws IOException {
        VP8LReader reader = new VP8LReader();
        reader.setInput(ds);
        reader.read();
    }

}
