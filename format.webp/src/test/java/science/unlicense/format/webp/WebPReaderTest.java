

package science.unlicense.format.webp;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.impl.Vector2i32;
/**
 *
 * @author Johann Sorel
 */
public class WebPReaderTest {

    private static final Vector2i32 b00 = new Vector2i32(0,0);
    private static final Vector2i32 b10 = new Vector2i32(1,0);
    private static final Vector2i32 b20 = new Vector2i32(2,0);
    private static final Vector2i32 b30 = new Vector2i32(3,0);
    private static final Vector2i32 b01 = new Vector2i32(0,1);
    private static final Vector2i32 b11 = new Vector2i32(1,1);
    private static final Vector2i32 b21 = new Vector2i32(2,1);
    private static final Vector2i32 b31 = new Vector2i32(3,1);

    @Ignore
    @Test
    public void testAnimated() throws IOException{
        Path p = Paths.resolve(new Chars("mod:/science/unlicense/format/webp/dancing_banana.webp"));

        WebPReader reader = new WebPReader();
        reader.setInput(p);

        Image image = reader.read(null);

    }

    @Ignore
    @Test
    public void testLosslessAlpha() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/science/unlicense/format/webp/LosslessAlpha.webp")).createInputStream();

        final ImageReader reader = new WebPReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        //values are in RGB
        Assert.assertArrayEquals(new int[]{255,  0,  0,255}, image.getTuple(b00,null).toInt()); //RED
        Assert.assertArrayEquals(new int[]{255,255,  0,206}, image.getTuple(b10,null).toInt()); //YELLOW
        Assert.assertArrayEquals(new int[]{  0,255,255,  0}, image.getTuple(b01,null).toInt()); //TRANSLUCENT
        Assert.assertArrayEquals(new int[]{  0,  0,255,255}, image.getTuple(b11,null).toInt()); //BLUE

        Assert.assertEquals(new ColorRGB(255,   0,   0, 255), image.getColor(b00));
        Assert.assertEquals(new ColorRGB(255, 255,   0, 206), image.getColor(b10));
        Assert.assertEquals(new ColorRGB(  0, 255, 255,   0), image.getColor(b01));
        Assert.assertEquals(new ColorRGB(  0,   0, 255, 255), image.getColor(b11));

    }

    @Ignore
    @Test
    public void testCompressedAlpha() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/science/unlicense/format/png/CompressedAlpha.webp")).createInputStream();

        final ImageReader reader = new WebPReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        //values are in RGB
        Assert.assertArrayEquals(new int[]{255,  0,  0,255}, image.getTuple(b00,null).toInt()); //RED
        Assert.assertArrayEquals(new int[]{255,255,  0,206}, image.getTuple(b10,null).toInt()); //YELLOW
        Assert.assertArrayEquals(new int[]{  0,255,255,  0}, image.getTuple(b01,null).toInt()); //TRANSLUCENT
        Assert.assertArrayEquals(new int[]{  0,  0,255,255}, image.getTuple(b11,null).toInt()); //BLUE

        Assert.assertEquals(new ColorRGB(255,   0,   0, 255), image.getColor(b00));
        Assert.assertEquals(new ColorRGB(255, 255,   0, 206), image.getColor(b10));
        Assert.assertEquals(new ColorRGB(  0, 255, 255,   0), image.getColor(b01));
        Assert.assertEquals(new ColorRGB(  0,   0, 255, 255), image.getColor(b11));

    }

}
