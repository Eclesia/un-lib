
package science.unlicense.format.sqlite;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SQLiteHeader {

    /**
     * The database page size in bytes. Must be a power of two between 512 and 32768 inclusive, or the value 1 representing a page size of 65536.
     */
    public int pageSize;

    /**
     * File format write version. 1 for legacy; 2 for WAL.
     */
    public int writeVersion;

    /**
     * File format read version. 1 for legacy; 2 for WAL.
     */
    public int readVersion;

    /**
     * Bytes of unused "reserved" space at the end of each page. Usually 0.
     */
    public int reservedEndPageBytes;

    /**
     * Maximum embedded payload fraction. Must be 64.
     */
    public int maxPayloadFraction;

    /**
     * Minimum embedded payload fraction. Must be 32.
     */
    public int minPayloadFraction;

    /**
     * Leaf payload fraction. Must be 32.
     */
    public int leafPayloadFraction;

    /**
     * File change counter.
     */
    public long changeCounter;
    /**
     * Size of the database file in pages. The "in-header database size".
     */
    public long nbPages;
    /**
     * Page number of the first freelist trunk page.
     */
    public long firstFreeList;
    /**
     * Total number of freelist pages.
     */
    public long nbFreeList;
    /**
     * The schema cookie.
     */
    public long schemaCookie;
    /**
     * The schema format number. Supported schema formats are 1, 2, 3, and 4.
     */
    public long schemaFormatNumber;
    /**
     * Default page cache size.
     */
    public long defaultPageCacheSize;
    /**
     * The page number of the largest root b-tree page when in auto-vacuum or incremental-vacuum modes, or zero otherwise.
     */
    public long rootBtreePageNumber;
    /**
     * The database text encoding. A value of 1 means UTF-8. A value of 2 means UTF-16le. A value of 3 means UTF-16be.
     */
    public int textEncoding;
    /**
     * The "user version" as read and set by the user_version pragma.
     */
    public int userVersion;
    /**
     * True (non-zero) for incremental-vacuum mode. False (zero) otherwise.
     */
    /**
     * "Application ID" set by PRAGMA application_id./
     */
    public long appId;
    /**
     * Reserved for expansion. Must be zero.
     */
    public byte[] reserved;
    /**
     * The version-valid-for number.
     */
    public long versionValidNumber;
    /**
     * SQLITE_VERSION_NUMBER
     */
    public long version;

    public void read(DataInputStream ds) throws IOException {

        if (!SQLiteConstants.SIGNATURE.equals(ds.readFully(new byte[16]))) {
            throw new IOException(ds, "File signature do not match sqlite.");
        }

        pageSize = ds.readUShort();
        writeVersion = ds.readUByte();
        readVersion = ds.readUByte();
        reservedEndPageBytes = ds.readUByte();
        maxPayloadFraction = ds.readUByte();
        minPayloadFraction = ds.readUByte();
        leafPayloadFraction = ds.readUByte();
        changeCounter = ds.readUInt();
        nbPages = ds.readUInt();
        firstFreeList = ds.readUInt();
        nbFreeList = ds.readUInt();
        schemaCookie = ds.readUInt();
        schemaFormatNumber = ds.readUInt();
        defaultPageCacheSize = ds.readUInt();
        rootBtreePageNumber = ds.readUInt();
        textEncoding = ds.readInt();
        userVersion = ds.readInt();
        appId = ds.readUInt();
        reserved = ds.readBytes(20);
        versionValidNumber = ds.readUInt();
        version = ds.readUInt();

    }
}
