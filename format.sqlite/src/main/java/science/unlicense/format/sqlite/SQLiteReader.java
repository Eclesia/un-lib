
package science.unlicense.format.sqlite;

import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SQLiteReader extends AbstractReader {

    public SQLiteReader() {
    }

    public void read() throws IOException {

        final DataInputStream ds = getInputAsDataStream(Endianness.BIG_ENDIAN);

        //read header
        final SQLiteHeader header = new SQLiteHeader();
        header.read(ds);

    }


}
