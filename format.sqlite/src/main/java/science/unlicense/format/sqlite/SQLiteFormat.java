
package science.unlicense.format.sqlite;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public final class SQLiteFormat extends DefaultFormat {

    public static final SQLiteFormat INSTANCE = new SQLiteFormat();

    public SQLiteFormat() {
        super(new Chars("sqlite"));
        shortName = new Chars("SQLite");
        longName = new Chars("SQLite database");
        mimeTypes.add(new Chars("application/x-sqlite3"));
        extensions.add(new Chars("sqlite"));
        signatures.add(SQLiteConstants.SIGNATURE);
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
