
package science.unlicense.format.sqlite;

/**
 *
 * @author Johann Sorel
 */
public class SQLiteConstants {

    public static final byte[] SIGNATURE = new byte[]{'S','Q','L','i','t','e',' ','f','o','r','m','a','t',' ','3','0'};

    private SQLiteConstants(){}

}
