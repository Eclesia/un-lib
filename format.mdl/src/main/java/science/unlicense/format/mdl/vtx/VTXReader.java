
package science.unlicense.format.mdl.vtx;

import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;

/**
 * Doc and example decompiler in php :
 * https://developer.valvesoftware.com/wiki/VTX
 *
 * @author Johann Sorel
 */
public class VTXReader extends AbstractReader {

    public VTXHeader header;
    //same structure as in MDL
    public VTXMeshPart[] parts;

    public void read() throws IOException{

        final BacktrackInputStream bs = getInputAsBacktrackStream();
        bs.mark();
        final DataInputStream ds = new DataInputStream(bs, Endianness.LITTLE_ENDIAN);

        //read header
        header = new VTXHeader();
        header.read(ds);

        //read mesh parts
        parts = new VTXMeshPart[header.nbBodyParts];
        for (int i=0;i<parts.length;i++){
            final int subOffset = header.bodyPartOffset+i*VTXMeshPart.BYTE_SIZE;
            bs.rewind();
            ds.skipFully(subOffset);
            parts[i] = new VTXMeshPart();
            parts[i].read(bs,ds,subOffset);
        }

    }

}
