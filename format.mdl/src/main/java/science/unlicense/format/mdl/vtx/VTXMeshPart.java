

package science.unlicense.format.mdl.vtx;

import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class VTXMeshPart {
    public static final int BYTE_SIZE = 8;
    public int modelNb;
    public int modelOffset;

    //read afterward
    public VTXModel[] models;

    public void read(BacktrackInputStream bs, DataInputStream ds, int offset) throws IOException{
        modelNb     = ds.readInt();
        modelOffset = ds.readInt();
        models      = new VTXModel[modelNb];
        for (int i=0;i<modelNb;i++){
            final int subOffset = offset+modelOffset+i*VTXModel.BYTE_SIZE;
            bs.rewind();
            ds.skipFully(subOffset);
            models[i] = new VTXModel();
            models[i].read(bs,ds,subOffset);
        }
    }

    public static class VTXModel{
        public static final int BYTE_SIZE = 8;
        public int lodNb;
        public int lodOffset;

        //read afterward
        public VTXLod[] lods;

        public void read(BacktrackInputStream bs, DataInputStream ds, int offset) throws IOException{
            lodNb       = ds.readInt();
            lodOffset   = ds.readInt();
            lods        = new VTXLod[lodNb];
            for (int i=0;i<lodNb;i++){
                final int subOffset = offset+lodOffset+i*VTXLod.BYTE_SIZE;
                bs.rewind();
                ds.skipFully(subOffset);
                lods[i] = new VTXLod();
                lods[i].read(bs,ds,subOffset);
            }
        }
    }

    public static class VTXLod{
        public static final int BYTE_SIZE = 12;
        public int meshNb;
        public int meshOffset;
        public float swt;

        //read afterward
        public VTXMesh[] meshes;

        public void read(BacktrackInputStream bs, DataInputStream ds, int offset) throws IOException{
            meshNb      = ds.readInt();
            meshOffset  = ds.readInt();
            swt         = ds.readFloat();
            meshes      = new VTXMesh[meshNb];
            for (int i=0;i<meshNb;i++){
                final int subOffset = offset+meshOffset+i*VTXMesh.BYTE_SIZE;
                bs.rewind();
                ds.skipFully(subOffset);
                meshes[i] = new VTXMesh();
                meshes[i].read(bs,ds,subOffset);
            }
        }
    }

    public static class VTXMesh{
        public static final int BYTE_SIZE = 9;
        public int stripGroupNb;
        public int stripGroupOffset;
        public int flags;

        //read afterward
        public VTXStripGroup[] stripGroups;

        public void read(BacktrackInputStream bs, DataInputStream ds, int offset) throws IOException{
            stripGroupNb     = ds.readInt();
            stripGroupOffset = ds.readInt();
            flags            = ds.readUByte();
            stripGroups      = new VTXStripGroup[stripGroupNb];
            for (int i=0;i<stripGroupNb;i++){
                final int subOffset = offset+stripGroupOffset+i*VTXStripGroup.BYTE_SIZE;
                bs.rewind();
                ds.skipFully(subOffset);
                stripGroups[i] = new VTXStripGroup();
                stripGroups[i].read(bs,ds,subOffset);
            }
        }
    }

    public static class VTXStripGroup{
        public static final int BYTE_SIZE = 25;
        public int vertexNb;
        public int vertexOffset;
        public int indexNb;
        public int indexOffset;
        public int stripNb;
        public int stripOffset;
        public int flags;

        //read afterward
        public VTXVertex[] vertices;
        public int[] indices;
        public VTXStrip[] strips;

        public void read(BacktrackInputStream bs, DataInputStream ds, int offset) throws IOException{
            vertexNb        = ds.readInt();
            vertexOffset    = ds.readInt();
            indexNb         = ds.readInt();
            indexOffset     = ds.readInt();
            stripNb         = ds.readInt();
            stripOffset     = ds.readInt();
            flags           = ds.readUByte();
            strips          = new VTXStrip[stripNb];

            //read vertices
            bs.rewind();
            ds.skipFully(offset+vertexOffset);
            vertices = new VTXVertex[vertexNb];
            for (int i=0;i<vertices.length;i++){
                vertices[i] = new VTXVertex();
                vertices[i].read(ds);
            }

            //read indices
            bs.rewind();
            ds.skipFully(offset+indexOffset);
            indices = new int[indexNb];
            for (int i=0;i<indices.length;i++){
                indices[i] = ds.readUShort();
            }

            //read strips
            for (int i=0;i<stripNb;i++){
                final int subOffset = offset+i*VTXStrip.BYTE_SIZE;
                bs.rewind();
                ds.skipFully(subOffset);
                strips[i] = new VTXStrip();
                strips[i].read(bs,ds,subOffset);
            }
        }
    }

    public static class VTXStrip{
        public static final int BYTE_SIZE = 27;
        public int indexNb;
        public int indexOffset;
        public int vertexNb;
        public int vertexOffset;
        public int boneNb;
        public int flags;
        public int boneChangeNb;
        public int boneChangeOffset;

        public void read(BacktrackInputStream bs, DataInputStream ds, int offset) throws IOException{
            indexNb         = ds.readInt();
            indexOffset     = ds.readInt();
            vertexNb        = ds.readInt();
            vertexOffset    = ds.readInt();
            boneNb          = ds.readUShort();
            flags           = ds.readUByte();
            boneChangeNb    = ds.readInt();
            boneChangeOffset= ds.readInt();
        }
    }

    public static class VTXVertex{
        public static final int BYTE_SIZE = 9;
        public int[] boneWeightIndex;
        public int boneNb;
        public int meshVertexId;
        public int[] boneIds;

        public void read(DataInputStream ds) throws IOException{
            boneWeightIndex = ds.readUByte(3);
            boneNb          = ds.readUByte();
            meshVertexId    = ds.readUShort();
            boneIds         = ds.readUByte(3);
        }
    }

}
