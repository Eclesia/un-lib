

package science.unlicense.format.mdl.vvd;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class VVDFixup {

    public int lod;
    public int sourceVertexId;
    public int nbVertex;

    public void read(DataInputStream ds) throws IOException {
        lod             = ds.readInt();
        sourceVertexId  = ds.readInt();
        nbVertex        = ds.readInt();
    }

}
