package science.unlicense.format.mdl.mdl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.math.impl.Vector3f64;

/**
 * @author Johann Sorel
 */
public final class MDLReader {

    private static final Chars SIGNATURE = Chars.constant(new byte[]{'I','D','P','O'});

    private MDLReader(){}

    public static MDLHeader read(final Path f) throws Exception {

        final ByteInputStream inStream = f.createInputStream();
        final DataInputStream ds = new DataInputStream(inStream, Endianness.BIG_ENDIAN);

        final MDLHeader store = new MDLHeader();

        store.signature = new Chars(ds.readFully(new byte[4]));
        store.version = ds.readInt();

        if (!SIGNATURE.equals(store.signature) || store.version != 6){
            throw new IOException(f, "File is not a MDL.");
        }

        store.scale = new Vector3f64(ds.readFloat(), ds.readFloat(), ds.readFloat());
        store.translate = new Vector3f64(ds.readFloat(), ds.readFloat(), ds.readFloat());
        store.boundingradius = ds.readFloat();
        store.somethingPosition = new Vector3f64(ds.readFloat(), ds.readFloat(), ds.readFloat());
        store.nbSkin = ds.readInt();
        store.textureWidth = ds.readInt();
        store.textureHeigth = ds.readInt();
        store.nbVertex = ds.readInt();
        store.nbTriangle = ds.readInt();
        store.nbFrame = ds.readInt();
        store.sync = ds.readInt();
        store.stat = ds.readInt();
        store.size = ds.readFloat();

        //calculate the size of a texture in bytes
        final int textureSize = store.textureWidth * store.textureHeigth;

        //read skins
        for (int i=0;i<store.nbSkin;i++){
            final MDLSkin skin = new MDLSkin();
            //read textures
            final int group = ds.readInt();
            if (group == 0){
                //single texture
                final MDLTexture texture = new MDLTexture();
                texture.data = new byte[textureSize];
                ds.readFully(texture.data);
                skin.textures.add(texture);

            } else if (group==1){
                //multiple
                int nb = ds.readInt();
                for (int k=0;k<nb;k++){
                    final MDLTexture texture = new MDLTexture();
                    texture.time = ds.readFloat();
                    texture.data = new byte[textureSize];
                    ds.readFully(texture.data);
                    skin.textures.add(texture);
                }
            } else {
                throw new IOException(ds, "unexpected texture group type :"+ group);
            }
        }

        //TODO read texture coordinates
        //TODO read triangles
        //TODO read vertices
        //TODO read frames


        return store;
    }

}
