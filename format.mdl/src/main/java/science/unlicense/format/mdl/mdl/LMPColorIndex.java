
package science.unlicense.format.mdl.mdl;

import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;

/**
 * LMP color palette.
 *
 * @author Johann Sorel
 */
public class LMPColorIndex {

    public final Color[] index = new Color[256];

    public void read(ByteInputStream stream) throws IOException{
        for (int i=0;i<256;i++){
            int r = stream.read() & 0xff;
            int g = stream.read() & 0xff;
            int b = stream.read() & 0xff;
            index[i] = new ColorRGB(r, g, b, 255);
        }
    }


}
