

package science.unlicense.format.mdl.mdl2;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class MDLMeshPart {

    public static final int BYTE_SIZE = 16;

    public int nameOffset;
    public int subPartNb;
    public int bodyPartBase;
    public int subPartOffset;

    //readed by MDLReader afterward
    public Chars name;
    public MDLModel[] datas;

    public void read(DataInputStream ds) throws IOException{
        nameOffset    = ds.readInt();
        subPartNb     = ds.readInt();
        bodyPartBase  = ds.readInt();
        subPartOffset = ds.readInt();
    }

    public static class MDLModel{
        public Chars name;
        public int type;
        public float sphereRadius;
        public int meshNb;
        public int meshOffset;
        public int vertexNb;
        public int vertexIndex;
        public int tangentsIndex;
        public int attachementNb;
        public int attachementOffset;
        public int eyeballNb;
        public int eyeballOffset;
        public int vertexRef;
        public int tangentRef;
        public int[] unused;

        //read afterward
        public MDLMesh[] meshes;

        public void read(DataInputStream ds) throws IOException{
            name                = ds.readBlockZeroTerminatedChars(64, CharEncodings.UTF_8);
            type                = ds.readInt();
            sphereRadius        = ds.readFloat();
            meshNb              = ds.readInt();
            meshOffset          = ds.readInt();
            vertexNb            = ds.readInt();
            vertexIndex         = ds.readInt();
            tangentsIndex       = ds.readInt();
            attachementNb       = ds.readInt();
            attachementOffset   = ds.readInt();
            eyeballNb           = ds.readInt();
            eyeballOffset       = ds.readInt();
            vertexRef           = ds.readInt();
            tangentRef          = ds.readInt();
            unused              = ds.readInt(8);
        }

    }

    public static class MDLMesh{
        public int materialIndex;
        public int modelIndex;
        public int vertexNb;
        public int vertexOffset;
        public int flexNb;
        public int flexOffset;
        public int materialType;
        public int materialParam;
        public int meshId;
        public VectorRW meshCenter;
        public int vertexDataVRef;
        public int[] nbVerticePerLod;
        public int[] unused;

        public void read(DataInputStream ds) throws IOException{
            materialIndex  = ds.readInt();
            modelIndex     = ds.readInt();
            vertexNb       = ds.readInt();
            vertexOffset   = ds.readInt();
            flexNb         = ds.readInt();
            flexOffset     = ds.readInt();
            materialType   = ds.readInt();
            materialParam  = ds.readInt();
            meshId         = ds.readInt();
            meshCenter     = MDLReader.readVector3(ds);
            vertexDataVRef = ds.readInt();
            nbVerticePerLod= ds.readInt(8);
            unused         = ds.readInt(8);
        }
    }


}
