

package science.unlicense.format.mdl.mdl2;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class MDLTexture {

    public static final int BYTE_SIZE = 64;

    public int nameOffset;
    public int flags;
    public int used;
    public int unused1;
    public int material;
    public int clientMaterial;
    /** size 10 */
    public int[] unused2;

    //readed by MDLReader afterward
    public Chars name;


    public void read(DataInputStream ds) throws IOException{
        nameOffset      = ds.readInt();
        flags           = ds.readInt();
        used            = ds.readInt();
        unused1         = ds.readInt();
        material        = ds.readInt();
        clientMaterial  = ds.readInt();
        unused2         = ds.readInt(10);
    }

}
