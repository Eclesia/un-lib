

package science.unlicense.format.mdl;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.model3d.impl.technique.AbstractTechnique;
import science.unlicense.model3d.impl.technique.RenderState;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.Technique;
import science.unlicense.format.mdl.mdl2.MDLBone;
import science.unlicense.format.mdl.mdl2.MDLMeshPart;
import science.unlicense.format.mdl.mdl2.MDLReader;
import science.unlicense.format.mdl.vtf.VTFReader;
import science.unlicense.format.mdl.vtx.VTXMeshPart;
import science.unlicense.format.mdl.vtx.VTXReader;
import science.unlicense.format.mdl.vvd.VVDLod;
import science.unlicense.format.mdl.vvd.VVDReader;
import science.unlicense.format.mdl.vvd.VVDVertex;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public class SourceStore extends AbstractModel3DStore {

    public SourceStore(Object input) {
        super(SourceFormat.INSTANCE, input);
    }

    public Collection getElements() throws StoreException {
        final Sequence elements = new ArraySequence();

        final Path mdlPath = (Path) getInput();

        final Path parentPath = mdlPath.getParent();
        final Path vvdPath = parentPath.resolve(mdlPath.getName().replaceAll(new Chars("mdl"), new Chars("vvd")));
        final Path vtxPath = parentPath.resolve(mdlPath.getName().replaceAll(new Chars("mdl"), new Chars("sw.vtx")));

        final MDLReader mdlReader = new MDLReader();
        final VVDReader vvdReader = new VVDReader();
        final VTXReader vtxReader = new VTXReader();

        final Skeleton skeleton = new Skeleton();
        try {
            mdlReader.setInput(mdlPath);
            mdlReader.read();
            vvdReader.setInput(vvdPath);
            vvdReader.read();
            vtxReader.setInput(vtxPath);
            vtxReader.read();

            //TODO add LOD api in 3d engine, or at least provide something
            //to select the loaded LOD level
            final int lodLevel = 0;
            final VVDLod vvdLod = vvdReader.lods[lodLevel];

            final MotionModel mpm = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);
            elements.add(mpm);

            //rebuild joints
            final Dictionary dico = new HashDictionary();
            for (int i=0;i<mdlReader.bones.length;i++){
                final MDLBone mdlbone = mdlReader.bones[i];
                final Joint joint = new Joint(3);
                joint.setTitle(mdlbone.name);
                joint.getNodeTransform().getTranslation().set(mdlbone.position);
                joint.getNodeTransform().getRotation().set(mdlbone.rotation.toMatrix3());
                joint.getNodeTransform().notifyChanged();
                dico.add(i, joint);

                if (mdlbone.parentBone>=0){
                    final Joint parent = (Joint) dico.getValue(mdlbone.parentBone);
                    parent.getChildren().add(joint);
                } else {
                    //root joint
                    skeleton.getChildren().add(joint);
                }
            }
            //build skeleton
            skeleton.updateBindPose();
            skeleton.updateInvBindPose();
            mpm.setSkeleton(skeleton);

            //keep track of index since the same structure is in MDL/VTX/VVD
            for (int p=0;p<mdlReader.parts.length;p++){
                final MDLMeshPart mdlPart = mdlReader.parts[p];
                final VTXMeshPart vtxPart = vtxReader.parts[p];

                for (int d=0;d<mdlPart.datas.length;d++){
                    final MDLMeshPart.MDLModel mdlModel = mdlPart.datas[d];
                    final VTXMeshPart.VTXModel vtxModel = vtxPart.models[d];

                    for (int m=0;m<mdlModel.meshes.length;m++){
                        final MDLMeshPart.MDLMesh mdlMesh = mdlModel.meshes[m];

                        final VTXMeshPart.VTXLod vtxLod = vtxModel.lods[lodLevel];
                        final VTXMeshPart.VTXMesh vtxMesh = vtxLod.meshes[m];

                        //Now link VTX indexes with VVD datas
                        //Note : Source engine models are really a pain to rebuild.
                        // This storage format is definitly not man made, it's more
                        // like an automatic binary binding.

                        final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(mdlMesh.vertexNb*3).cursor();
                        final Float32Cursor normals = DefaultBufferFactory.INSTANCE.createFloat32(mdlMesh.vertexNb*3).cursor();
                        final Float32Cursor uvs = DefaultBufferFactory.INSTANCE.createFloat32(mdlMesh.vertexNb*2).cursor();
                        final Int32Cursor jointIndexes = DefaultBufferFactory.INSTANCE.createInt32(mdlMesh.vertexNb*3).cursor();
                        final Float32Cursor jointWeights = DefaultBufferFactory.INSTANCE.createFloat32(mdlMesh.vertexNb*3).cursor();
                        science.unlicense.common.api.collection.primitive.IntSequence indexBuf = new science.unlicense.common.api.collection.primitive.IntSequence();

                        int idxOffset = 0;
                        for (VTXMeshPart.VTXStripGroup stripGroup : vtxMesh.stripGroups){
                            for (VTXMeshPart.VTXVertex vtxVertex : stripGroup.vertices){
                                final int vertexIdx = mdlMesh.vertexOffset + vtxVertex.meshVertexId;
                                final VVDVertex vvdVertex = vvdLod.vertices[vertexIdx];
                                vertices.write(vvdVertex.position.toFloat());
                                normals.write(vvdVertex.normal.toFloat());
                                uvs.write(vvdVertex.uv.toFloat());
                                jointIndexes.write(vvdVertex.weights.bones);
                                jointWeights.write(vvdVertex.weights.weights);
                            }

                            for (int idx : stripGroup.indices) indexBuf.put(idx+idxOffset);
                            idxOffset += stripGroup.indices.length;
                        }
                        final Buffer index = DefaultBufferFactory.wrap(indexBuf.toArrayInt());

                        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
                        shell.setPositions(new VBO(vertices.getBuffer(), 3));
                        shell.setNormals(new VBO(normals.getBuffer(), 3));
                        shell.setUVs(new VBO(uvs.getBuffer(), 2));
                        shell.setIndex(new IBO(index));
                        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) index.getNumbersSize())});
                        shell.getAttributes().add(Skin.ATT_JOINTS_0, new VBO(jointIndexes.getBuffer(), 3));
                        shell.getAttributes().add(Skin.ATT_WEIGHTS_0, new VBO(jointWeights.getBuffer(), 3));

                        final Skin skin = new Skin();
                        skin.setSkeleton(skeleton);
                        skin.setMaxWeightPerVertex(3);

                        final DefaultModel mesh = new DefaultModel();
                        mesh.getTechniques().add(new SimpleBlinnPhong());
                        ((Technique) mesh.getTechniques().get(0)).getState().setCulling(RenderState.CULLING_NONE);
                        mesh.setShape(shell);
                        mesh.setSkin(skin);

                        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
                        mesh.getMaterials().add(material);


                        if (true){
                            try{
                                //draft rebuild texture
                                final Path baseTextPath = parentPath.resolve(mdlReader.textureLibPaths[0].replaceAll('\\', '/'));
                                final Path vtfDiffusePath = baseTextPath.resolve(mdlReader.textures[m].name.concat(new Chars(".vtf")));
                                final VTFReader vtfDiffuseReader = new VTFReader();
                                vtfDiffuseReader.setInput(vtfDiffusePath);
                                vtfDiffuseReader.read();
                                material.setDiffuseTexture(new TextureMapping(new Texture2D(vtfDiffuseReader.image)));

                                //search for a normal file
                                {
                                    final Path vtfNormalPath = baseTextPath.resolve(mdlReader.textures[m].name.replaceAll(new Chars("diff"), new Chars("norm")).concat(new Chars(".vtf")));
                                    final VTFReader vtfNormalReader = new VTFReader();
                                    vtfNormalReader.setInput(vtfNormalPath);
                                    vtfNormalReader.read();
                                    material.properties().setPropertyValue(AbstractTechnique.NORMAL, new TextureMapping(new Texture2D(vtfNormalReader.image)));
                                }

                            }catch(IOException ex){
                                ex.printStackTrace();
                            }
                        }

                        Mesh.calculateTangents(shell);

                        mpm.getChildren().add(mesh);
                    }
                }
            }

        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        return elements;
    }

}
