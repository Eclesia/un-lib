

package science.unlicense.format.mdl.vtx;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class VTXHeader {

    public int version;
    public int vertCacheSize;
    public int maxBonesPerStrip;
    public int maxBonesPerFace;
    public int maxBonesPerVert;
    public int checkSum;
    public int nbLODs;
    public int materialReplacementListOffset;
    public int nbBodyParts;
    public int bodyPartOffset;

    public void read(DataInputStream ds) throws IOException{
        version                       = ds.readInt();
        vertCacheSize                 = ds.readInt();
        maxBonesPerStrip              = ds.readUShort();
        maxBonesPerFace               = ds.readUShort();
        maxBonesPerVert               = ds.readInt();
        checkSum                      = ds.readInt();
        nbLODs                       = ds.readInt();
        materialReplacementListOffset = ds.readInt();
        nbBodyParts                  = ds.readInt();
        bodyPartOffset                = ds.readInt();
    }

}
