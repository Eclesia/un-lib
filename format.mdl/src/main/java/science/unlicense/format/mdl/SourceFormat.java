

package science.unlicense.format.mdl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * Source MDL format.
 *
 * Docs :
 * https://developer.valvesoftware.com/wiki/MDL (uncomplete and contains bugs)
 *
 *
 * @author Johann Sorel
 */
public class SourceFormat extends AbstractModel3DFormat {

    public static final SourceFormat INSTANCE = new SourceFormat();

    private SourceFormat() {
        super(new Chars("SourceMDL"));
        shortName = new Chars("SourceMDL");
        longName = new Chars("SourceMDL");
        extensions.add(new Chars("mdl"));
        signatures.add(new byte[]{'I','D','S','T'});
    }

    @Override
    public Store open(Object input) throws IOException {
        return new SourceStore(input);
    }

}
