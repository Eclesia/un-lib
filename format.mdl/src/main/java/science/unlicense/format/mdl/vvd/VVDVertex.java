

package science.unlicense.format.mdl.vvd;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.VectorRW;
import science.unlicense.format.mdl.mdl2.MDLReader;

/**
 *
 * @author Johann Sorel
 */
public class VVDVertex {

    public VVDBoneWeight weights;
    /** Note : strange format, position are in inches */
    public VectorRW position;
    public VectorRW normal;
    public VectorRW uv;

    public void read(DataInputStream ds) throws IOException{
        weights  = new VVDBoneWeight();
        weights.read(ds);
        position = MDLReader.readVector3(ds);
        normal   = MDLReader.readVector3(ds);
        uv       = MDLReader.readVector2(ds);
    }

}
