
package science.unlicense.format.mdl.mdl;

/**
 *
 * @author Johann Sorel
 */
public class MDLTexture {

    public float time;
    public byte[] data;

}
