

package science.unlicense.format.mdl.vvd;

import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class VVDLod {

    public VVDVertex[] vertices;
    /** vectors of size 4 */
    public VectorRW[] tangents;

}
