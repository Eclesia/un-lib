

package science.unlicense.format.hpgl;

/**
 * HPGL constants.
 *
 * @author Johann Sorel
 */
public final class HPGLConstants {

    private HPGLConstants(){}

}
