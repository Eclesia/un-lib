

package science.unlicense.format.hpgl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * Hewlett-Packard Graphics Language.
 *
 * Resource :
 * http://en.wikipedia.org/wiki/HPGL
 * http://www.hpmuseum.net/document.php?catfile=213
 *
 * @author Johann Sorel
 */
public class HPGLFormat extends DefaultFormat {

    public HPGLFormat() {
        super(new Chars("hpgl"));
        shortName = new Chars("HPGL");
        longName = new Chars("Hewlett-Packard Graphics Language");
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
