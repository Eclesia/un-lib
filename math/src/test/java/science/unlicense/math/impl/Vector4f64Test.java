
package science.unlicense.math.impl;


/**
 *
 * @author Johann Sorel
 */
public class Vector4f64Test extends AbstractVectorTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{4};
    }

    @Override
    protected Vector4f64 create(int dim) {
        return new Vector4f64();
    }

}
