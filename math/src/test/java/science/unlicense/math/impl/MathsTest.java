
package science.unlicense.math.impl;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.Maths;

/**
 * @author Bertrand COTE
 */
public class MathsTest {

    static final double EPSILON = 1e-6;

    // ===== helperfunction ====================================================

    public static char[] byteArrayToCharArray(byte[] a) {
        char[] rep = new char[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (char) a[i];
        }
        return rep;
    }

    public static short[] byteArrayToShortArray(byte[] a) {
        short[] rep = new short[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (short) a[i];
        }
        return rep;
    }

    public static int[] byteArrayToIntArray(byte[] a) {
        int[] rep = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (int) a[i];
        }
        return rep;
    }

    public static long[] byteArrayToLongArray(byte[] a) {
        long[] rep = new long[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (long) a[i];
        }
        return rep;
    }

    public static float[] byteArrayToFloatArray(byte[] a) {
        float[] rep = new float[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (float) a[i];
        }
        return rep;
    }

    public static double[] byteArrayToDoubleArray(byte[] a) {
        double[] rep = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (double) a[i];
        }
        return rep;
    }

    // =========================================================================

    static final byte[][][] minMaxSumProductArrayTest = new byte[][][]{
        // { given array of values }, { min }, { max }, { sum }, { product } },
        { { 1, 2, 3, 4, 5 }, { 1 }, { 5 }, { 15 }, { 120 } },
        { { 5, 4, 3, 2, 1 }, { 1 }, { 5 }, { 15 }, { 120 } },
    };

    public MathsTest() {
    }

    @Test
    public void testMin_XArr() {
        for ( int i=0; i<minMaxSumProductArrayTest.length; i++ ) {
            byte[] array = minMaxSumProductArrayTest[i][0];
            byte expResult = minMaxSumProductArrayTest[i][1][0];
            {   // test byte
                byte result = Maths.min(array);
                Assert.assertEquals( expResult, result );
            }
            {   // test short
                short result = Maths.min(byteArrayToShortArray(array));
                Assert.assertEquals( (short) expResult, result );
            }
            {   // test int
                int result = Maths.min(byteArrayToIntArray(array));
                Assert.assertEquals( (int) expResult, result );
            }
            {   // test long
                long result = Maths.min(byteArrayToLongArray(array));
                Assert.assertEquals( (long) expResult, result );
            }
            {   // test float
                float result = Maths.min(byteArrayToFloatArray(array));
                Assert.assertEquals( (float) expResult, result, 0. );
            }
            {   // test double
                double result = Maths.min(byteArrayToDoubleArray(array));
                Assert.assertEquals( (double) expResult, result, 0. );
            }
        }
    }

    @Test
    public void testMax_XArr() {
        for ( int i=0; i<minMaxSumProductArrayTest.length; i++ ) {
            byte[] array = minMaxSumProductArrayTest[i][0];
            byte expResult = minMaxSumProductArrayTest[i][2][0];
            {   // test byte
                byte result = Maths.max(array);
                Assert.assertEquals( expResult, result );
            }
            {   // test short
                short result = Maths.max(byteArrayToShortArray(array));
                Assert.assertEquals( (short) expResult, result );
            }
            {   // test int
                int result = Maths.max(byteArrayToIntArray(array));
                Assert.assertEquals( (int) expResult, result );
            }
            {   // test long
                long result = Maths.max(byteArrayToLongArray(array));
                Assert.assertEquals( (long) expResult, result );
            }
            {   // test float
                float result = Maths.max(byteArrayToFloatArray(array));
                Assert.assertEquals( (float) expResult, result, 0. );
            }
            {   // test double
                double result = Maths.max(byteArrayToDoubleArray(array));
                Assert.assertEquals( (double) expResult, result, 0. );
            }
        }
    }

    @Test
    public void testSum_XArr() {
        for ( int i=0; i<minMaxSumProductArrayTest.length; i++ ) {
            byte[] array = minMaxSumProductArrayTest[i][0];
            byte expResult = minMaxSumProductArrayTest[i][3][0];
            {   // test byte
                byte result = Maths.sum(array);
                Assert.assertEquals( expResult, result );
            }
            {   // test short
                short result = Maths.sum(byteArrayToShortArray(array));
                Assert.assertEquals( (short) expResult, result );
            }
            {   // test int
                int result = Maths.sum(byteArrayToIntArray(array));
                Assert.assertEquals( (int) expResult, result );
            }
            {   // test long
                long result = Maths.sum(byteArrayToLongArray(array));
                Assert.assertEquals( (long) expResult, result );
            }
            {   // test float
                float result = Maths.sum(byteArrayToFloatArray(array));
                Assert.assertEquals( (float) expResult, result, 0. );
            }
            {   // test double
                double result = Maths.sum(byteArrayToDoubleArray(array));
                Assert.assertEquals( (double) expResult, result, 0. );
            }
        }
    }

    @Test
    public void testProduct_XArr() {
        for ( int i=0; i<minMaxSumProductArrayTest.length; i++ ) {
            byte[] array = minMaxSumProductArrayTest[i][0];
            byte expResult = minMaxSumProductArrayTest[i][4][0];
            {   // test byte
                byte result = Maths.product(array);
                Assert.assertEquals( expResult, result );
            }
            {   // test short
                short result = Maths.product(byteArrayToShortArray(array));
                Assert.assertEquals( (short) expResult, result );
            }
            {   // test int
                int result = Maths.product(byteArrayToIntArray(array));
                Assert.assertEquals( (int) expResult, result );
            }
            {   // test long
                long result = Maths.product(byteArrayToLongArray(array));
                Assert.assertEquals( (long) expResult, result );
            }
            {   // test float
                float result = Maths.product(byteArrayToFloatArray(array));
                Assert.assertEquals( (float) expResult, result, 0. );
            }
            {   // test double
                double result = Maths.product(byteArrayToDoubleArray(array));
                Assert.assertEquals( (double) expResult, result, 0. );
            }
        }
    }

    /**
     * Test of pow method, of class Interval.
     */
    @Test
    public void testPow_double_int() {
        final int nMax = 11;
        final double xMax = 10.;
        final double step = 0.5;

        for ( double x=-xMax; x<xMax; x+= step ) {

            double expResult = 1.;
            for ( int n=1; n<nMax; n++ ) {
                expResult *= x;
                Assert.assertEquals( Maths.pow(x,  n),  expResult,   EPSILON);
                Assert.assertEquals( Maths.pow(x, -n), 1./expResult, EPSILON);
            }
        }
    }

    /**
     * Test of pow method, of class Interval.
     */
    @Test
    public void testPow_float_int() {
        final int nMax = 7;
        final float xMax = 10.f;
        final float step = 0.5f;

        for ( float x=-xMax; x<xMax; x+= step ) {

            float expResult = 1.f;
            for ( int n=1; n<nMax; n++ ) {
                expResult *= x;
                Assert.assertEquals( Maths.pow(x,  n),  expResult,   EPSILON);
                Assert.assertEquals( Maths.pow(x, -n), 1./expResult, EPSILON);
            }
        }
    }

    @Test
    public void testWrap(){
        Assert.assertEquals(  5, Maths.wrap(  5,-10,+10));
        Assert.assertEquals(-10, Maths.wrap(-10,-10,+10));
        Assert.assertEquals(+10, Maths.wrap( 10,-10,+10));
        Assert.assertEquals(  9, Maths.wrap(-11,-10,+10));
        Assert.assertEquals(  9, Maths.wrap(-31,-10,+10));
        Assert.assertEquals( -9, Maths.wrap( 11,-10,+10));
        Assert.assertEquals( -9, Maths.wrap( 31,-10,+10));
        Assert.assertEquals( 15, Maths.wrap( 15,+10,+20));
        Assert.assertEquals( 10, Maths.wrap( 10,+10,+20));
        Assert.assertEquals( 20, Maths.wrap( 20,+10,+20));
        Assert.assertEquals( 15, Maths.wrap(  5,+10,+20));
        Assert.assertEquals( 15, Maths.wrap(-65,+10,+20));
        Assert.assertEquals( 15, Maths.wrap( 25,+10,+20));
        Assert.assertEquals( 15, Maths.wrap( 55,+10,+20));
    }

}
