
package science.unlicense.math.impl;

import science.unlicense.math.api.VectorRW;


/**
 *
 * @author Johann Sorel
 */
public class DefaultVectorTest extends AbstractVectorTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{1,2,3,4,5};
    }

    @Override
    protected VectorRW create(int dim) {
        return new VectorNf64(dim);
    }

}
