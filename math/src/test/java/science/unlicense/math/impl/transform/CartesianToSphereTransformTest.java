
package science.unlicense.math.impl.transform;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class CartesianToSphereTransformTest {

    @Test
    public void testTransform() {

        final Vector3f64 cartesian = new Vector3f64();
        final Vector2f64 spheric = new Vector2f64();
        final CartesianToSphereTransform trs = new CartesianToSphereTransform(1);

        cartesian.setXYZ(1, 0, 0);
        trs.transform(cartesian, spheric);
        Assert.assertEquals(new Vector2f64(0, 0), spheric);

        cartesian.setXYZ(0, 1, 0);
        trs.transform(cartesian, spheric);
        Assert.assertEquals(new Vector2f64(Maths.HALF_PI, 0), spheric);

        cartesian.setXYZ(0, -1, 0);
        trs.transform(cartesian, spheric);
        Assert.assertEquals(new Vector2f64(-Maths.HALF_PI, 0), spheric);

        cartesian.setXYZ(-1, 0, 0);
        trs.transform(cartesian, spheric);
        Assert.assertEquals(new Vector2f64(Maths.PI, 0), spheric);

        cartesian.setXYZ(0, 0, 1);
        trs.transform(cartesian, spheric);
        Assert.assertEquals(new Vector2f64(0, Maths.HALF_PI), spheric);

        cartesian.setXYZ(0, 0, -1);
        trs.transform(cartesian, spheric);
        Assert.assertEquals(new Vector2f64(0, -Maths.HALF_PI), spheric);

        cartesian.setXYZ(Angles.degreeToRadian(45.0), Angles.degreeToRadian(45.0), 0);
        trs.transform(cartesian, spheric);
        Assert.assertEquals(new Vector2f64(Maths.QUATER_PI, 0), spheric);

        cartesian.setXYZ(Angles.degreeToRadian(45.0), -Angles.degreeToRadian(45.0), 0);
        trs.transform(cartesian, spheric);
        Assert.assertEquals(new Vector2f64(-Maths.QUATER_PI, 0), spheric);

        cartesian.setXYZ(-Angles.degreeToRadian(45.0), Angles.degreeToRadian(45.0), 0);
        trs.transform(cartesian, spheric);
        Assert.assertEquals(new Vector2f64(Maths.QUATER_PI+Maths.HALF_PI, 0), spheric);

    }

}
