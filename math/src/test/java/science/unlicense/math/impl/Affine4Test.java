
package science.unlicense.math.impl;

import science.unlicense.math.api.AffineRW;

/**
 * Test Affine3.
 *
 * @author Johann Sorel
 */
public class Affine4Test extends AbstractAffineTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{4};
    }

    @Override
    protected AffineRW create(int dim) {
        return new Affine4(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    }

}
