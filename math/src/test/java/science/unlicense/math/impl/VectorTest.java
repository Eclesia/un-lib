package science.unlicense.math.impl;

import org.junit.Test;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Bertrand COTE
 */
public class VectorTest {

    private static final double EPSILON = 1e-6;

    // =========================================================================
    // ===== Helper functions ==================================================

    private static boolean isNotZero( double[] v ) {
        for ( int i=0; i<v.length; i++ ) {
            if ( v[i] == 0. ) return false;
        }
        return true;
    }

    // =========================================================================
    // ===== data for tests ====================================================

    private static final double[][][] vectorsDataTest = new double[][][]{
    //  {{ vector.values }, { scale factor }, { scale expResult.values }, { normalize expResult } },
        { {  0. },                {  2. }, { 0. },                     { 0. } },
        { {  5. },                {  3. }, { 15. },                    { 1. } },
        { {  0.,  0. },           { -4. }, { 0., 0. },                 { 0., 0. } },
        { {  2.,  4. },           {  5. }, { 10., 20. },               { 0.447213595499958, 0.894427190999916 } },
        { {  0.,  0.,  0. },      {  6. }, { 0.,  0.,  0. },           { 0.,  0.,  0. } },
        { { -5.,  8., -2. },      { -7. }, { 35.,  -56.,  14. },       { -0.518475847365213, 0.829561355784340, -0.207390338946085} },
        { {  0.,  0.,  0.,  0. }, { -9. }, { 0.,  0.,  0.,  0. },      { 0.,  0.,  0.,  0. } },
        { {  3., -9.,  4., -1. }, { -4. }, { -12.,  36.,  -16.,  4. }, { 0.2900209467136990, -0.8700628401410971, 0.3866945956182654, -0.0966736489045664 } },
        { {  },                   { -2. }, {  },                       {  } },
    };

    private static final double[][][] addSubMultDivLerpTest = new double[][][]{
    // { { vector.values }, { other.values }, { add.values }, { subtract.values }, { multiply.values }, { divide.values }, { lerp ratio }, { lerp expResult } },
        { { 3. },           { 2. },           { 5. },         { 1. },              { 6. },              { 1.5 },            {  0.5 }, { 2.5 } },
        { { 3., 5. },       { 2., 10. },      { 5., 15. },    { 1., -5. },         { 6., 50. },         { 1.5, 0.5 },       { 0.25 }, {  2.75, 6.25 } },
        { { 3., 5., 9. },   { 2., 10., -3. }, { 5., 15., 6. },{ 1., -5., 12. },    { 6., 50., -27. },   { 1.5, 0.5, -3. },  { 0.75 }, {  2.25, 8.75, 0.0 } },
    };

    private static final double[][][] crossTest = new double[][][]{
        // { { v1 },       { v2 },          { v1 cross v2 } },
        { { -2., 0., 0. }, { 5., 0.,  0. }, {  0.,  0.,  0.} },
        { {  0., 9., 0. }, { 0., 3.,  0. }, {  0.,  0.,  0.} },
        { {  0., 0., 7. }, { 0., 0., -8. }, {  0.,  0.,  0.} },
        { {  2., 3., 4. }, { 7., 6.,  5. }, { -9., 18., -9.} },
    };

    private static final double[][][] angleTest = new double[][][]{
        //{ { v1 }, { v2 }, { angle }, },
        { { 1., 0. }, { 0., 1. }, { Maths.HALF_PI }, },
        { { 0., 1. }, { 1., 0. }, { Maths.HALF_PI }, },
        { { -2., 0. }, { 0., -2. }, { Maths.HALF_PI }, },
        { { 0., -2. }, { -2., 0. }, { Maths.HALF_PI }, },
        { { -2., -2. }, { -2., 0. }, { Maths.QUATER_PI }, },

        { { 5.2147, -19.2415 }, { -0.36510, 3.25050 }, { 2.98878910249942 } },

        { { 3., 0., 0. }, { 0., 2., 0. }, { Maths.HALF_PI }, },
        { { 3., 0., 0. }, { 0., 0., -5. }, { Maths.HALF_PI }, },
        { { 0., -7., 0. }, { 0., 0., 9. }, { Maths.HALF_PI }, },

        { { 5.2147, -19.2415, -0.3699 }, { -0.36510, 3.25050, 1.5747 }, { 2.68695055311836 }, },
    };

    private static final double[][][] projectTest = new double[][][]{
        //{ { v1 }, { v2 }, { projected vector }, },
        { { 1., 0. }, { 0., 1. }, { 0., 0. }, },
        { { 0., 1. }, { 0., 1. }, { 0., 1. }, },
        { { 0., 1. }, { 0.,-1. }, { 0.,-1. }, },
        { { 0., 1. }, {0.5, 0.5}, { 0., 0.5}, },
        { { 0., 12.}, { 21, 7.3}, { 0., 7.3}, }
    };

    private static final double[][][] cosSinTest = new double[][][]{
        // { { vec1 }, { vec2 }, { cosine, sinus },},
        { { 2., 0. }, { 3., 3. }, { Math.sqrt(2.)/2,  Math.sqrt(2.)/2 },},
        { { 3., 3. }, { 2., 0. }, { Math.sqrt(2.)/2, -Math.sqrt(2.)/2 },},

        { { 2., 0., 0. }, { 3., 3., 0. }, { Math.sqrt(2.)/2, Math.sqrt(2.)/2 },},
        { { 3., 3., 0. }, { 2., 0., 0. }, { Math.sqrt(2.)/2, Math.sqrt(2.)/2 },},

        { { 2., 2., 0. }, { 3., 3., 3. }, { Math.sqrt(6./9.), Math.sqrt(3./9.) },},
        { { 3., 3., 3. }, { 2., 2., 0. }, { Math.sqrt(6./9.), Math.sqrt(3./9.) },},

        { { 2., 0., 0. }, { 3., 3., 3. }, { Math.sqrt(3./9.), Math.sqrt(6./9.) },},
        { { 3., 3., 3. }, { 2., 0., 0. }, { Math.sqrt(3./9.), Math.sqrt(6./9.) },},

    };

    // =========================================================================
    // =========================================================================

    /**
     * Test of length method, of class Vector.
     */
    @Test
    public void testLength() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            VectorNf64 instance = new VectorNf64(vector);
            double expResult = 0.0;
            for (int k=0; k<vector.length; k++) expResult += vector[k]*vector[k];
            expResult = Math.sqrt(expResult);
            double result = instance.length();
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of lengthSquare method, of class Vector.
     */
    @Test
    public void testLengthSquare() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            VectorNf64 instance = new VectorNf64(vector);
            double expResult = 0.0;
            for (int k=0; k<vector.length; k++) expResult += vector[k]*vector[k];
            double result = instance.lengthSquare();
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of dot method, of class Vector.
     */
    @Test
    public void testDot() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] v1 = addSubMultDivLerpTest[i][0];
            double[] v2 = addSubMultDivLerpTest[i][0];
            VectorNf64 instance = new VectorNf64(v1);
            Tuple other = new TupleNf64(v2);
            double result = instance.dot(other);
            double expResult = 0.0;
            for ( int k=0; k<v1.length; k++ ) expResult += v1[k]*v2[k];
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of add method, of class Vector.
     */
    @Test
    public void testAdd_Tuple() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            VectorNf64 instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);

            double[] expResult = addSubMultDivLerpTest[i][2];
            double[] result = instance.add(otherInstance).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of subtract method, of class Vector.
     */
    @Test
    public void testSubtract_Tuple() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            VectorNf64 instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);

            double[] expResult = addSubMultDivLerpTest[i][3];
            double[] result = instance.subtract(otherInstance).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of multiply method, of class Vector.
     */
    @Test
    public void testMultiply_Tuple() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            VectorNf64 instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);

            double[] expResult = addSubMultDivLerpTest[i][4];
            double[] result = instance.multiply(otherInstance).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of divide method, of class Vector.
     */
    @Test
    public void testDivide_Tuple() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            VectorNf64 instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);

            double[] expResult = addSubMultDivLerpTest[i][5];
            double[] result = instance.divide(otherInstance).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of scale method, of class Vector.
     */
    @Test
    public void testScale_double() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double scale =  vectorsDataTest[i][1][0];
            double[] expResult = vectorsDataTest[i][2];

            VectorNf64 instance = new VectorNf64(vector);
            double[] result = instance.scale(scale).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of cross method, of class Vector.
     */
    @Test
    public void testCross_Tuple() {
        for ( int i=0; i<crossTest.length; i++ ) {
            double[] v1 = crossTest[i][0];
            double[] v2 = crossTest[i][1];
            double[] expResult = crossTest[i][2];

            VectorNf64 instance = new VectorNf64(v1);
            Tuple other = new TupleNf64(v2);
            double[] result = instance.cross(other).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of lerp method, of class Vector.
     */
    @Test
    public void testLerp_Tuple_double() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] start = addSubMultDivLerpTest[i][0];
            double[] end = addSubMultDivLerpTest[i][1];
            double ratio = addSubMultDivLerpTest[i][6][0];
            double[] expResult = addSubMultDivLerpTest[i][7];

            VectorNf64 instance = new VectorNf64(start);
            Tuple other = new TupleNf64(end);

            double[] result = instance.lerp(other, ratio).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result) );
        }
    }

    /**
     * Test of normalize method, of class Vector.
     */
    @Test
    public void testNormalize_0args() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            if ( isNotZero(vector) ){
                double[] expResult = vectorsDataTest[i][3];

                VectorNf64 instance = new VectorNf64(vector);

                double[] result = instance.normalize().toDouble();
                Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
            }
        }
    }

    /**
     * Test of negate method, of class Vector.
     */
    @Test
    public void testNegate_0args() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double[] expResult = new double[vector.length];
            for ( int k=0; k<vector.length; k++ ) expResult[k] = -vector[k];

            VectorNf64 instance = new VectorNf64(vector);

            double[] result = instance.negate().toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of shortestAngle method, of class Vector.
     */
    @Test
    public void testShortestAngle() {
        for ( int i=0; i<angleTest.length; i++ ) {
            double[] vec1 = angleTest[i][0];
            double[] vec2 = angleTest[i][1];
            double expResult = angleTest[i][2][0];

            VectorNf64 instance = new VectorNf64(vec1);
            VectorNf64 second = new VectorNf64(vec2);

            double result = instance.shortestAngle(second);
            Assert.assertEquals(expResult, result, EPSILON);
        }
    }

    /**
     * Test of shortestAngle method, of class Vector.
     */
    @Test
    public void testIsParallel() {
        Vector3f64 v1 = new Vector3f64(1, 0, 0);
        Vector3f64 v2 = new Vector3f64(1, 0, 0);
        Assert.assertTrue(v1.isParallel(v2, 0));
        v2 = new Vector3f64(-1, 0, 0);
        Assert.assertTrue(v1.isParallel(v2, 0));
        v2 = new Vector3f64(23, 0, 0);
        Assert.assertTrue(v1.isParallel(v2, 0));
        v2 = new Vector3f64(1, 0, 1);
        Assert.assertTrue(!v1.isParallel(v2, 0));
    }

    /**
     * Test of project method, of class Vector.
     */
    @Test
    public void testProject() {
        for ( int i=0; i<projectTest.length; i++ ) {
            double[] vec1 = projectTest[i][0];
            double[] vec2 = projectTest[i][1];
            double[] expResult = projectTest[i][2];

            VectorNf64 instance = new VectorNf64(vec1);
            VectorNf64 second = new VectorNf64(vec2);

            VectorRW result = instance.project(second);
            Assert.assertArrayEquals(expResult,result.toDouble(), EPSILON);
        }
    }

    /**
     * Test of add method, of class Vector.
     */
    @Test
    public void testAdd_Tuple_Vector() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            double[] expResult = addSubMultDivLerpTest[i][2];

            VectorNf64 instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);
            VectorNf64 buffer = null;

            VectorRW resultVector = instance.add(otherInstance, buffer);
            double[] result = resultVector.toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new VectorNf64(instance.getSampleCount());
            resultVector = instance.add(otherInstance, buffer);
            result = resultVector.toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.toDouble()));
        }
    }

    /**
     * Test of subtract method, of class Vector.
     */
    @Test
    public void testSubtract_Tuple_Vector() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            double[] expResult = addSubMultDivLerpTest[i][3];

            VectorNf64 instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);
            VectorNf64 buffer = null;

            VectorRW resultVector = instance.subtract(otherInstance, buffer);
            double[] result = resultVector.toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new VectorNf64(instance.getSampleCount());
            resultVector = instance.subtract(otherInstance, buffer);
            result = resultVector.toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.toDouble()));
        }
    }

    /**
     * Test of multiply method, of class Vector.
     */
    @Test
    public void testMultiply_Tuple_Vector() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            double[] expResult = addSubMultDivLerpTest[i][4];

            VectorNf64 instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);
            VectorNf64 buffer = null;

            VectorRW resultVector = instance.multiply(otherInstance, buffer);
            double[] result = resultVector.toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new VectorNf64(instance.getSampleCount());
            resultVector = instance.multiply(otherInstance, buffer);
            result = resultVector.toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.toDouble()));
        }
    }

    /**
     * Test of divide method, of class Vector.
     */
    @Test
    public void testDivide_Tuple_Vector() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            double[] expResult = addSubMultDivLerpTest[i][5];

            VectorRW instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);

            VectorRW buffer = null;
            VectorRW resultVector = instance.divide(otherInstance, buffer);
            double[] result = resultVector.toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new VectorNf64(instance.getSampleCount());
            resultVector = instance.divide(otherInstance, buffer);
            result = resultVector.toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.toDouble()));
        }
    }

    /**
     * Test of scale method, of class Vector.
     */
    @Test
    public void testScale_double_Vector() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double scale =  vectorsDataTest[i][1][0];
            double[] expResult = vectorsDataTest[i][2];
            VectorNf64 instance = new VectorNf64(vector);

            VectorNf64 buffer = null;
            double[] result = instance.scale(scale, buffer).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new VectorNf64(instance.getSampleCount());
            result = instance.scale(scale, buffer).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.toDouble()));
        }
    }

    /**
     * Test of cross method, of class Vector.
     */
    @Test
    public void testCross_Tuple_Vector() {
        for ( int i=0; i<crossTest.length; i++ ) {
            double[] v1 = crossTest[i][0];
            double[] v2 = crossTest[i][1];
            double[] expResult = crossTest[i][2];

            VectorNf64 instance = new VectorNf64(v1);
            Tuple other = new TupleNf64(v2);

            VectorNf64 buffer = null;
            double[] result = instance.cross(other, buffer).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new VectorNf64(instance.getSampleCount());
            result = instance.cross(other, buffer).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.toDouble()));
        }
    }

    /**
     * Test of lerp method, of class Vector.
     */
    @Test
    public void testLerp_3args() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] start = addSubMultDivLerpTest[i][0];
            double[] end = addSubMultDivLerpTest[i][1];
            double ratio = addSubMultDivLerpTest[i][6][0];
            double[] expResult = addSubMultDivLerpTest[i][7];

            VectorNf64 instance = new VectorNf64(start);
            Tuple other = new TupleNf64(end);

            VectorNf64 buffer = null;
            double[] result = instance.lerp(other, ratio, buffer).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result) );

            buffer = new VectorNf64(instance.getSampleCount());
            result = instance.lerp(other, ratio, buffer).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result) );
            Assert.assertTrue( Arrays.equals(expResult, buffer.toDouble()));
        }
    }

    /**
     * Test of normalize method, of class Vector.
     */
    @Test
    public void testNormalize_Vector() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            if ( isNotZero(vector) ){
                double[] expResult = vectorsDataTest[i][3];
                VectorNf64 instance = new VectorNf64(vector);

                VectorNf64 buffer = null;
                double[] result = instance.normalize(buffer).toDouble();
                Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));

                buffer = new VectorNf64(instance.getSampleCount());
                result = instance.normalize(buffer).toDouble();
                Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
                Assert.assertTrue( Arrays.equals(expResult, buffer.toDouble(), EPSILON));
            }
        }
    }

    /**
     * Test of negate method, of class Vector.
     */
    @Test
    public void testNegate_Vector() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double[] expResult = new double[vector.length];
            for ( int k=0; k<vector.length; k++ ) expResult[k] = -vector[k];

            VectorNf64 instance = new VectorNf64(vector);

            VectorNf64 buffer = null;
            double[] result = instance.negate(buffer).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new VectorNf64(instance.getSampleCount());
            result = instance.negate(buffer).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.toDouble()));
        }
    }

    /**
     * Test of localAdd method, of class Vector.
     */
    @Test
    public void testLocalAdd_double_double() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);

            if ( vector.length>=2 ) {
                double[] other = addSubMultDivLerpTest[i][1];
                double x = other[0];
                double y = other[1];
                VectorNf64 instance = new VectorNf64(vector);

                double[] expResult = Arrays.copy(vector);
                expResult[0] += x;
                expResult[1] += y;
                double[] result = instance.localAdd(x, y).toDouble();
                Assert.assertTrue( Arrays.equals(expResult, result));
            }
        }
    }

    /**
     * Test of localAdd method, of class Vector.
     */
    @Test
    public void testLocalAdd_3args() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);

            if ( vector.length>=3 ) {
                double[] other = addSubMultDivLerpTest[i][1];
                double x = other[0];
                double y = other[1];
                double z = other[2];
                VectorNf64 instance = new VectorNf64(vector);

                double[] expResult = Arrays.copy(vector);
                expResult[0] += x;
                expResult[1] += y;
                expResult[2] += z;
                double[] result = instance.localAdd(x, y, z).toDouble();
                Assert.assertTrue( Arrays.equals(expResult, result));
            }
        }
    }

    /**
     * Test of localAdd method, of class Vector.
     */
    @Test
    public void testLocalAdd_4args() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);

            if ( vector.length>=4 ) {
                double[] other = addSubMultDivLerpTest[i][1];
                double x = other[0];
                double y = other[1];
                double z = other[2];
                double w = other[3];
                VectorNf64 instance = new VectorNf64(vector);

                double[] expResult = Arrays.copy(vector);
                expResult[0] += x;
                expResult[1] += y;
                expResult[2] += z;
                expResult[3] += w;
                double[] result = instance.localAdd(x, y, z, w).toDouble();
                Assert.assertTrue( Arrays.equals(expResult, result));
            }
        }
    }

    /**
     * Test of localAdd method, of class Vector.
     */
    @Test
    public void testLocalAdd_Tuple() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);
            double[] other = addSubMultDivLerpTest[i][1];

            VectorNf64 instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);

            double[] expResult = addSubMultDivLerpTest[i][2];
            double[] result = instance.localAdd(otherInstance).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localSubtract method, of class Vector.
     */
    @Test
    public void testLocalSubtract() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);
            double[] other = addSubMultDivLerpTest[i][1];

            VectorNf64 instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);

            double[] expResult = addSubMultDivLerpTest[i][3];
            double[] result = instance.localSubtract(otherInstance).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localMultiply method, of class Vector.
     */
    @Test
    public void testLocalMultiply() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);
            double[] other = addSubMultDivLerpTest[i][1];

            VectorNf64 instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);

            double[] expResult = addSubMultDivLerpTest[i][4];
            double[] result = instance.localMultiply(otherInstance).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localDivide method, of class Vector.
     */
    @Test
    public void testLocalDivide() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);
            double[] other = addSubMultDivLerpTest[i][1];

            VectorNf64 instance = new VectorNf64(vector);
            Tuple otherInstance = new TupleNf64(other);

            double[] expResult = addSubMultDivLerpTest[i][5];
            double[] result = instance.localDivide(otherInstance).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localScale method, of class Vector.
     */
    @Test
    public void testLocalScale() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = Arrays.copy(vectorsDataTest[i][0]);
            double scale =  vectorsDataTest[i][1][0];
            double[] expResult = vectorsDataTest[i][2];

            VectorNf64 instance = new VectorNf64(vector);
            double[] result = instance.localScale(scale).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localCross method, of class Vector.
     */
    @Test
    public void testLocalCross() {
        for ( int i=0; i<crossTest.length; i++ ) {
            double[] v1 = Arrays.copy(crossTest[i][0]);
            double[] v2 = crossTest[i][1];
            double[] expResult = crossTest[i][2];

            VectorNf64 instance = new VectorNf64(v1);
            Tuple other = new TupleNf64(v2);
            double[] result = instance.localCross(other).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localNormalize method, of class Vector.
     */
    @Test
    public void testLocalNormalize() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = Arrays.copy(vectorsDataTest[i][0]);
            if ( isNotZero(vector) ){
                double[] expResult = vectorsDataTest[i][3];

                VectorNf64 instance = new VectorNf64(vector);

                double[] result = instance.localNormalize().toDouble();
                Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
            }
        }
    }

    /**
     * Test of localNegate method, of class Vector.
     */
    @Test
    public void testLocalNegate() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = Arrays.copy(vectorsDataTest[i][0]);
            double[] expResult = new double[vector.length];
            for ( int k=0; k<vector.length; k++ ) expResult[k] = -vector[k];

            VectorNf64 instance = new VectorNf64(vector);

            double[] result = instance.localNegate().toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localLerp method, of class Vector.
     */
    @Test
    public void testLocalLerp() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] start = Arrays.copy(addSubMultDivLerpTest[i][0]);
            double[] end = Arrays.copy(addSubMultDivLerpTest[i][1]);
            double ratio = addSubMultDivLerpTest[i][6][0];
            double[] expResult = addSubMultDivLerpTest[i][7];

            VectorNf64 instance = new VectorNf64(start);
            Tuple other = new TupleNf64(end);

            double[] result = instance.localLerp(other, ratio).toDouble();
            Assert.assertTrue( Arrays.equals(expResult, result) );
        }
    }

    /**
     * Test of copy method, of class Vector.
     */
    @Test
    public void testCopy() {
        for ( int i=0; i<vectorsDataTest.length; i++) {
            double[] vec = vectorsDataTest[i][0];
            VectorNf64 instance = new VectorNf64( Arrays.copy(vec) );
            double[] expResult = vec;
            double[] result = instance.copy().toDouble();
            Assert.assertTrue(Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of extend method, of class Vector.
     */
    @Test
    public void testExtend() {
        for ( int i=0; i<vectorsDataTest.length; i++) {
            double[] vec = vectorsDataTest[i][0];
            if ( vec.length>1 ) {
                VectorNf64 instance = new VectorNf64( Arrays.copy(vec, 0, vec.length-1 ) );
                double[] expResult = vec;
                double value = vec[vec.length-1];
                double[] result = instance.extend(value).toDouble();
                Assert.assertTrue(Arrays.equals(expResult, result));
            }
        }
    }

}
