
package science.unlicense.math.impl;

import science.unlicense.math.api.AffineRW;

/**
 * Test Affine3.
 *
 * @author Johann Sorel
 */
public class Affine3Test extends AbstractAffineTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{3};
    }

    @Override
    protected AffineRW create(int dim) {
        return new Affine3(0,0,0,0,0,0,0,0,0,0,0,0);
    }

}
