
package science.unlicense.math.impl;


/**
 *
 * @author Johann Sorel
 */
public class Vector4i32Test extends AbstractVectorTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{4};
    }

    @Override
    protected Vector4i32 create(int dim) {
        return new Vector4i32();
    }

}
