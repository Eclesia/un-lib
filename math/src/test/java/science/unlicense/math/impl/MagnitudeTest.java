package science.unlicense.math.impl;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.Arrays;
import science.unlicense.math.api.Maths;

/**
 *
 * @author Bertrand cOTE
 */
public class MagnitudeTest {

    private static final long[][][] reductMagnitudeDataTest = new long[][][]{
        {
            // mag
            {1, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, },
            // expResult: { factorise, magResult[0], magResult[1], ... }
            {0, 0x0000000000000000L, },
        },
        {
            // mag
            { 1, },
            // expResult: { factorise, magResult[0], magResult[1], ... }
            { 0, 0x0000000000000000L},
        },
        {
            // mag
            { 1, 0x0000000000000000L},
            // expResult: { factorise, magResult[0], magResult[1], ... }
            { 0, 0x0000000000000000L},
        },
        {
            // mag
            { 1, 0x123456789abcdL },
            // expResult: { factorise, magResult[0], magResult[1], ... }
            { 0, 0x123456789abcdL },
        },
        {
            // mag
            {1, 0x0000000000000000L, 0x0000000000000000L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // expResult: { factorise, magResult[0], magResult[1], ... }
            {128, 0x0000000000000000L, 0x0000000000000000L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
        },
        {
            // mag
            {1, 0x0000000000000000L, 0x0000000000000000L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL, 0x0000000000000000L, 0x0000000000000000L, },
            // expResult: { factorise, magResult[0], magResult[1], ... }
            {128, 0x0000000000000000L, 0x0000000000000000L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
        },
        {
            // mag
            {1, 0x0000000000000000L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL, 0x0000000000000000L, 0x0000000000000000L, },
            // expResult: { factorise, magResult[0], magResult[1], ... }
            {64, 0x0000000000000000L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
        },
        {
            // mag
            {1, 0x0000000000000000L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL, 0x0000000000000000L, },
            // expResult: { factorise, magResult[0], magResult[1], ... }
            {64, 0x0000000000000000L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
        },

        {
            // mag
            {1, 0x0000000000000000L, 0x1238ddbb27ab4eL, 0x24916eafbaacbcc5L, 0x0000000000000000L, },
            // expResult: { factorise, magResult[0], magResult[1], ... }
            {65, 0x0000000000000000L, 0x1238ddbb27ab4eL, 0x24916eafbaacbcc5L, },
        },{
            // mag
            {1, 0x0000000000000000L, 0x1238ddbb27ab40L, 0x24916eafbaacbcc5L, 0x0000000000000000L, },
            // expResult: { factorise, magResult[0], magResult[1], ... }
            {70, 0x0000000000000000L, 0x1238ddbb27ab40L, 0x24916eafbaacbcc5L, },
        },

    };

    /**
     * Test of reductMagnitude method, of class Magnitude.
     */
    @Test
    public void testReductMagnitude() {

        for (long[][] data : reductMagnitudeDataTest) {
            long[] mag = Arrays.copy(data[0], 1, data[0].length - 1);
            long[] expResult = Arrays.copy(data[1], 1, data[1].length - 1);
            long[] result = Magnitude.reductMagnitude(mag);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of factoriseMagnitude method, of class Magnitude.
     */
    @Test
    public void testSimplifyMagnitude() {
        for (long[][] data : reductMagnitudeDataTest) {
            long[] mag = Arrays.copy(data[0], 1, data[0].length - 1);
            long expResult = data[1][0];
            long result = Magnitude.simplifyMagnitude(mag);
            Assert.assertEquals(expResult, result);
        }
    }

    private static final long[][][] compareMagnitudesDataTest = new long[][][]{
        {
            // magSource
            {1, 0x0000000000000001L, 0x0000000000000000L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // magTarget
            {1, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // { offset, expResult }
            {1, 1},},
        {
            // magSource
            {1, 0x0000000000000000L, 0x0000000000000000L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // magTarget
            {1, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // { offset, expResult }
            {2, 0},},
         {
            // magSource
            {1, 0x0000000000000001L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // magTarget
            {1, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // { offset, expResult }
            {1, 1},},
        {
            // magSource
            {1, 0x0000000000000000L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // magTarget
            {1, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // { offset, expResult }
            {1, 0},},
        {
            // magSource
            {1, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // magTarget
            {1, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // { offset, expResult }
            {0, 0},},
        {
            // magSource
            {1, 0x6b208527263abbc6L, 0xf2c4771bc650336dL, 0x3L,},
            // magTarget
            {1, 0xb80eeL,},
            // { offset, expResult }
            {3, -1},},
        {
            // magSource
            {1, 0x7aac57245205785aL, 0xc4fb1c7dae82991aL, 0x53a8df6cfaf97464L, 0x24916eafbaacbcc5L, 0xdea5294287f87352L, 0x1238ddbb27ab4eL,},
            // magTarget
            {1, 0xc471bfaf435509d8L, 0x7d0fef91f6734414L, 0x1L,},
            // { offset, expResult }
            {0, 1},},
        {
            // magSource
            {1, 0x9a92e3edc20b51f0L, 0xe428ea187ccd2626L, 0x4ea4521801b8771dL, 0x61b08919b19f538aL,},
            // magTarget
            {1, 0x3e4587cbfcd791bbL, 0x0fa28fd5b6d47028L, 0x4be76e5ae4fc2144L, 0x673d78a0852aL,},
            // { offset, expResult }
            {5, -1},},
        {
            // magSource
            {1, 0xc577e163ae954dfaL, 0x502ab27f01f913d8L, 0x2f1a166fb3771d49L, 0xc4008716e1cc7a83L, 0x3adce2e9c81e3083L, 0x300edf77f5038L,},
            // magTarget
            {1, 0x7a7570b570bf6b23L, 0xc955L,},
            // { offset, expResult }
            {4, 1}},
        {
            // magSource
            {1, 0x9914644b234ee2d8L, 0xd9d23e1e9889f845L, 0xb5707c2e7f5d0b87L, 0x4c968d911a92c61cL, 0x95L,},
            // magTarget
            {1, 0x9f5fd2d8f8dd113aL, 0x74a00306L,},
            // { offset, expResult }
            {3, -1}}
    };

    /**
     * Test of compareMagnitudes method, of class LargeInteger.
     */
    @Test
    public void testCompareMagnitudes_3args() {
        for (long[][] data : compareMagnitudesDataTest) {
            long[] mag0 = Arrays.copy(data[0], 1, data[0].length - 1);
            long[] mag1 = Arrays.copy(data[1], 1, data[1].length - 1);
            int offset = (int) data[2][0];
            int expResult = (int) data[2][1];
            int result = Magnitude.compareMagnitudes(mag0, mag1, offset);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of compareMagnitudes method, of class LargeInteger.
     */
    @Test
    public void testCompareMagnitudes_longArr_longArr() {

        for (long[][] data : compareMagnitudesDataTest) {
            int offset = (int) data[2][0];
            if ( offset == 0 ) {
                long[] mag0 = Arrays.copy(data[0], 1, data[0].length - 1);
                long[] mag1 = Arrays.copy(data[1], 1, data[1].length - 1);
                int expResult = (int) data[2][1];
                int result = Magnitude.compareMagnitudes(mag0, mag1);
                Assert.assertEquals(expResult, result);
            }
        }
    }

    private static final long[][][] addMagnitudeDataTest = new long[][][]{
        {
            // magSource
            {1, 0x5L,},
            // magTarget
            {1, 0x2L,},
            // { offset }
            {0},
            // add: sign==true
            {1, 0x7L,},
            // sub: sign==false
            {1, 0x3L,}},
        {
            // magSource
            {1, 0x0L, 0x5L,},
            // magTarget
            {1, 0x2L,},
            // { offset }
            {1},
            // add: sign==true
            {1, 0x0L, 0x7L,},
            // sub: sign==false
            {1, 0x0L, 0x3L,}},
        {
            // magSource
            {1, 0x709c2efd3706ad51L, 0x11c2694a40344354L, 0xc320194ed4019573L, 0x09b2226bab6c1d53L, 0xea0a962c500547aaL, 0x19beb17L,},
            // magTarget
            {1, 0x16693db2c4bddf48L, 0xc2eb3315864L,},
            // { offset }
            {2},
            // add: sign==true
            {1, 0x709c2efd3706ad51L, 0x11c2694a40344354L, 0xd989570198bf74bbL, 0x09b22e9a5e9d75b7L, 0xea0a962c500547aaL, 0x19beb17L,},
            // sub: sign==false
            {1, 0x709c2efd3706ad51L, 0x11c2694a40344354L, 0xacb6db9c0f43b62bL, 0x09b2163cf83ac4efL, 0xea0a962c500547aaL, 0x19beb17L,}},
        {
            // magSource
            {1, 0x8ccf7dd7eba73ab3L, 0x05bb3033ddad6e5dL, 0xf8bea500ab7b28f7L, 0x66087b1db70605e5L, 0x244ee4a4aefd4f26L,},
            // magTarget
            {1, 0xf26fae7L,},
            // { offset }
            {4},
            // add: sign==true
            {1, 0x8ccf7dd7eba73ab3L, 0x05bb3033ddad6e5dL, 0xf8bea500ab7b28f7L, 0x66087b1db70605e5L, 0x244ee4a4be244a0dL,},
            // sub: sign==false
            {1, 0x8ccf7dd7eba73ab3L, 0x05bb3033ddad6e5dL, 0xf8bea500ab7b28f7L, 0x66087b1db70605e5L, 0x244ee4a49fd6543fL,}},
        {
            // magSource
            {1, 0xc79408f92867da2eL, 0xc8e890f48856a6a7L, 0x8L,},
            // magTarget
            {1, 0x768e80768d1d6bfdL, 0x69f190e5ef8d38f4L, 0x3L,},
            // { offset }
            {0},
            // add: sign==true
            {1, 0x3e22896fb585462bL, 0x32da21da77e3df9cL, 0xcL,},
            // sub: sign==false
            {1, 0x510588829b4a6e31L, 0x5ef7000e98c96db3L, 0x5L,} },
        {
            // magSource
            {1, 0x6acb492603690333L, 0x7cabd72341bcf1cdL, 0xcbd9315b0bc29d79L, 0xc8a0d65b415d9d91L, 0x33b0af0b0a9666d6L, 0x6274d0d93a4297abL, 0x2L,},
            // magTarget
            {1, 0x7c6c0453d7533696L, 0x1bdbL,},
            // { offset }
            {3},
            // add: sign==true
            {1, 0x6acb492603690333L, 0x7cabd72341bcf1cdL, 0xcbd9315b0bc29d79L, 0x450cdaaf18b0d427L, 0x33b0af0b0a9682b2L, 0x6274d0d93a4297abL, 0x2L,},
            // sub: sign==false
            {1, 0x6acb492603690333L, 0x7cabd72341bcf1cdL, 0xcbd9315b0bc29d79L, 0x4c34d2076a0a66fbL, 0x33b0af0b0a964afbL, 0x6274d0d93a4297abL, 0x2L,}},
        {
            // magSource
            {1, 0xd9f5f9baf2b11b86L, 0x38ca082cbL,},
            // magTarget
            {1, 0xbd9eL,},
            // { offset }
            {1},
            // add: sign==true
            {1, 0xd9f5f9baf2b11b86L, 0x38ca14069L,},
            // sub: sign==false
            {1, 0xd9f5f9baf2b11b86L, 0x38c9fc52dL,}},
        {
            // magSource
            {1, 0x71e4d9abbb6978L,},
            // magTarget
            {1, 0x1380ddc1b0fL,},
            // { offset }
            {0},
            // add: sign==true
            {1, 0x71e611b9978487L,},
            // sub: sign==false
            {1, 0x71e3a19ddf4e69L,}}
    };

    /**
     * Test of addMagnitudes method, of class LargeInteger.
     */
    @Test
    public void testAddMagnitudes() {
        for ( long[][] data : addMagnitudeDataTest ) {
            long[] mag0 = Arrays.copy(data[0], 1, data[0].length - 1);
            long[] mag1 = Arrays.copy(data[1], 1, data[1].length - 1);
            int offset = (int) data[2][0];

            boolean sign = true;
            long[] expResult = Arrays.copy(data[3], 1, data[3].length - 1);
            long[] result = Magnitude.addMagnitudes(mag0, mag1, offset, sign);
            Assert.assertArrayEquals(expResult, result);

            sign = false;
            expResult = Arrays.copy(data[4], 1, data[4].length - 1);
            result = Magnitude.addMagnitudes(mag0, mag1, offset, sign);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of addMagnitudesBuffered method, of class Magnitude.
     */
    @Test
    public void testAddMagnitudesBuffered() {

        for ( long[][] data : addMagnitudeDataTest ) {
            int offset = (int) data[2][0];
            long[] magSource = Arrays.copy(data[1], 1, data[1].length - 1);
            long[] magTarget = Arrays.copy(data[0], 1, Maths.max(data[0].length - 1, offset+magSource.length)+1);

            boolean sign = true;
            long[] expResult = Arrays.copy(data[3], 1, data[3].length - 1);
            Magnitude.addMagnitudesBuffered(magSource, magTarget, offset, sign);
            Assert.assertArrayEquals(expResult, Magnitude.reductMagnitude(magTarget));

            sign = false;
            magTarget = Arrays.copy(data[0], 1, Maths.max(data[0].length - 1, offset+magSource.length)+1);
            expResult = Arrays.copy(data[4], 1, data[4].length - 1);
            Magnitude.addMagnitudesBuffered(magSource, magTarget, offset, sign);
            Assert.assertArrayEquals(expResult, Magnitude.reductMagnitude(magTarget));
        }
    }

//    private static final long[][][] prodLocalDataTest = new long[][][]{
//        { //  m0LowHalf, m0HighHalf,  m1
//            {0x76543210L, 0xba98L, 0x89abcdefL},
//            //  prod0LowHalf, prod0HighHalf, carry
//            {0xe5618cf0L, 0xd144641fL, 0x6458L}
//        },
//        { //  m0LowHalf, m0HighHalf,  m1
//            {0x76543210L, 0xba98L, 0x1234567L},
//            //  prod0LowHalf, prod0HighHalf, carry
//            {0x358e7470L, 0x4de4acf1L, 0xd4L}
//        }
//    };
//
//    /**
//     * Test of prodLocal method, of class LargeInteger.
//     */
//    @Test
//    public void testProdLocal() {
//        for (long[][] data : prodLocalDataTest) {
//            long m0LowHalf = data[0][0];
//            long m0HighHalf = data[0][1];
//            long m1 = data[0][2];
//            //LargeInteger instance = new LargeInteger();
//            long[] expResult = data[1];
//            long[] result = Magnitude.prodLocal(m0LowHalf, m0HighHalf, m1);
//            Assert.assertArrayEquals(expResult, result);
//        }
//    }
//
//    private static final long[][][] prodTwoLongDataTest = new long[][][]{
//        { //        m0,              m1
//            {0xc78e028d8f0bfbe4L, 0x44ae7b06e6571325L},
//            //      prod0,         prod1
//            {0x4f9db4cd02e953f4L, 0x3589bb19f251f294L}
//        },
//        { //        m0,              m1
//            {0xba9876543210L, 0x123456789abcdefL},
//            //      prod0,         prod1
//            {0x06d2d88fe5618cf0L, 0xd44de5114aL}
//        }
//    };
//
//    /**
//     * Test of prodTwoLong method, of class LargeInteger.
//     */
//    @Test
//    public void testProdTwoLong() {
//        for (long[][] data : prodTwoLongDataTest) {
//            long m0 = data[0][0];
//            long m1 = data[0][1];
//            //LargeInteger instance = new LargeInteger();
//            long[] expResult = data[1];
//            long[] result = Magnitude.prodTwoLong(m0, m1);
//            Assert.assertArrayEquals(expResult, result);
//        }
//    }

    private static final long[][][] multMagnitudeByLongDataTest = new long[][][] {
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xa0beeb31627bda07L, 0x2430L,},
            // { multiplicator, offset, isSignedLong }
            {0x1c171c1c05b138c7L, 1, 0},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0000000000000000L, 0xf7f2c09687cf0371L, 0x95ec9a94ff9a28b4L, 0x3f8L,},},
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x5ce2f536L,},
            // { multiplicator, offset, isSignedLong }
            {0xbda2b4110d6b907L, 0, 0},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xdce86ce4e88cba7aL, 0x44ce97fL,},},
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x449d3846b4b61cebL, 0x89ba5b8e9142cbfeL, 0xb61581L,},
            // { multiplicator, offset, isSignedLong }
            {0xad7aa7e0ef627326L, 3, 0},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0xe099eb0318fbdbe2L, 0x080be9a23e0b1a5fL, 0x31d328fa20c1921cL, 0x7b63c6L,},},
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1ed85dfdfdd3a7cfL, 0xb65b3d090ae3c8e6L, 0x44d074992887fbL,},
            // { multiplicator, offset, isSignedLong }
            {0x62c63bdc7a73ab45L, 1, 1},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0000000000000000L, 0x0cd4ccf2d3207fcbL, 0x27754df94de17c55L, 0xbdd20704ce5f5817L, 0x1a8d15f41ee756L,},},
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x77b83f72c0b6d894L, 0xd71ea3L,},
            // { multiplicator, offset, isSignedLong }
            {0x10449c58608c8906L, 0, 1},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x95707de84f204778L, 0xbbd697a2a7411e70L, 0xdab91L,},},
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x92f9ada939f9822dL, 0xdee87d61ede84f2fL,},
            // { multiplicator, offset, isSignedLong }
            {0xe8348da1cdd11847L, 3, 1},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x6e222f6337dbad85L, 0x66b4562921ffd749L, 0x14b80934c5ed6c43L,},},
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x4L,},
            // { multiplicator, offset, isSignedLong }
            {0xfffffffffffffffeL, 0, 0}, // here multplicator is <0
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xfffffffffffffff8L, 0x3L,}
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x4L,},
            // { multiplicator, offset, isSignedLong }
            {0x2L, 0, 0}, // here multplicator is <0
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x8L, 0x0L}
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x4L,},
            // { multiplicator, offset, isSignedLong }
            {0xfffffffffffffffeL, 0, 1}, // here multplicator is <0
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x8L, 0x0L}
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x4L,},
            // { multiplicator, offset, isSignedLong }
            {0x2L, 0, 1}, // here multplicator is <0
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x8L, 0x0L}
        },
    };

    /**
     * Test of multMagnitudeByLong method, of class LargeInteger.
     */
    @Test
    public void testMultMagnitudeByLong() {
        for (long[][] data : multMagnitudeByLongDataTest) {

            long[] mag = Arrays.copy( data[0], 1, data[0].length-1 );
            long multiplicator = data[1][0];
            int offset = (int) data[1][1];
            boolean isSignedLong = (data[1][2]==1L);
            long[] expResult = Arrays.copy( data[2], 1, data[2].length-1 );

            long[] result = Magnitude.multMagnitudeByLong(mag, multiplicator, offset, isSignedLong);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    private static final long[][][] multMagnitudeDataTest = new long[][][]{
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xc54ad9L,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xb1dfadca951708b7L, 0x7cL,},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2bbb9a8abdde491fL, 0x6019563eL,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x08f9ec519cdbbc18L, 0x2da9a0c56245L,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xf09c6de8639b1e79L, 0x97dc99ee1f95e970L, 0x7fbceebec30532f5L,},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x01bde37d0e6eb758L, 0x63c42536624741e1L, 0xcfb037136680982dL, 0x60df1ee45e48fccdL, 0x16c8d9e9b6daL,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xb8affc82274a681dL, 0x601cae879c49b2fcL, 0x5a7939f2204730a5L, 0x46e8L,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xa49457860d726b82L, 0x23L,},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x04f5411de336fdbaL, 0xe1b94271ebacb8e1L, 0x022314c667ee8fb4L, 0x4f1afb6712611a5fL, 0x9df5aL,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xcdee5a1e2d46441cL, 0x0db7113b39db9ea2L, 0x928bc9959136a2f0L, 0x8bd21214fb8253L,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x298d1bf0900bdc38L, 0x0b6ecda78416a0aaL, 0xd3b44ad8c7e8cc2cL, 0xd6c4ad77868678L,},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x94d3ec16f71af620L, 0x872bdb2ab9ab1ec9L, 0x52178daba4ca157eL, 0x841e938e22ae8473L, 0xbd51a6f3d2a43b7dL, 0xf1c23723e98811cdL, 0x7ec704d59bf15cb5L, 0x754d06b1a0a7L,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xd5615944f67f75ccL, 0x3da8d3cf62601f16L, 0x66aeea8a8c7ec3f4L, 0xbdfcL,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x162f6890e850b766L, 0x2b4132912b94e679L, 0x641212L,},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x988b8b0503bdc348L, 0xa35a6e5198eb02d4L, 0x59c104f440283a83L, 0x7c136c275dc96f44L, 0xeac5994e61f74802L, 0x4a44015764L,},},
    };

    /**
     * Test of multMagnitude method, of class LargeInteger.
     */
    @Test
    public void testMultMagnitude() {

        for (long[][] data : multMagnitudeDataTest) {
            long[] mag0 = Arrays.copy( data[0], 1, data[0].length-1 );
            long[] mag1 = Arrays.copy( data[1], 1, data[1].length-1 );
            long[] expResult = Arrays.copy( data[2], 1, data[2].length-1 );
            long[] result = Magnitude.multMagnitude(mag0, mag1);
            Assert.assertArrayEquals(expResult, result);
        }
    }

//    /**
//     * Test of multMagnitudeBuffered method, of class Magnitude.
//     */
//    @Test
//    public void testMultMagnitudeBuffered() {
//
//        for (long[][] data : multMagnitudeDataTest) {
//            long[] mag0 = Arrays.copy( data[0], 1, data[0].length-1 );
//            long[] mag1 = Arrays.copy( data[1], 1, data[1].length+mag0.length-1 );
//            long[] expResult = Arrays.copy( data[2], 1, data[2].length-1 );
//            Magnitude.multMagnitudeBuffered(mag0, mag1, data[1].length);
//            Assert.assertArrayEquals(expResult, Magnitude.reductMagnitude(mag1));
//        }
//    }


    private static final long[][][] divideSameOrderMagnitudeDataTest = new long[][][]{
         {
            // numerator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x05c5305ccd0000L,},
            // denominator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x10000L,},
            // quotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x5c5305ccdL,},},
        {
            // numerator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x05c5305ccd2a24L,},
            // denominator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2L,},
            // quotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2e2982e669512L,},},
        {
            // numerator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x9f0a6b64abb7ff43L, 0x73a67c122d5fb039L, 0xeff7abc2a1bae26dL, 0x8cbe77ffd4L,},
            // denominator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xe3a2f767af5b3c03L, 0xa6ad088129f88a5dL, 0x54fec6ba9614feb0L, 0x6be7051c7736L,},
            // quotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0L,},},
        {
            // numerator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xe3a2f767af5b3c03L, 0xa6ad088129f88a5dL, 0x54fec6ba9614feb0L, 0x6be7051c7736L,},
            // denominator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x9f0a6b64abb7ff43L, 0x73a67c122d5fb039L, 0xeff7abc2a1bae26dL, 0x8cbe77ffd4L,},
            // quotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xc4L,},},
        {
            // numerator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x6d92f257c858c6f1L, 0x39543dL,},
            // denominator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xd21fc112281939L,},
            // quotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x45d87957L,},},
        {
            // numerator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x6L},
            // denominator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2L,},
            // quotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x3L,},},
        {
            // numerator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x00000000000002c40cL},
            // denominator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x4L,},
            // quotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x00000000000000b103L,},},
        {
            // numerator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x62L},
            // denominator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x19L,},
            // quotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x3L,},},
        {
            // numerator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x62L},
            // denominator: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x4L,},
            // quotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x18L,},},
    };

    /**
     * Test of divideSameOrderMagnitude method, of class LargeInteger.
     */
    @Test
    public void testDivideSameOrderMagnitude() {
        for (long[][] data : divideSameOrderMagnitudeDataTest) {
            long[] numerator = Arrays.copy( data[0], 1, data[0].length-1 );
            long[] denominator = Arrays.copy( data[1], 1, data[1].length-1 );
            long expResult = data[2][1];
            long result = Magnitude.divideSameOrderMagnitude(numerator, denominator);
            Assert.assertEquals(expResult, result);
        }
    }

    private static final long[][][] divideAndRemainderMagnitudeDataTest = new long[][][]{

        {   // ========== bug ========== >>>>>>>>>> fixed <<<<<<<<<<
            { 1, 0xffffffffffffffa0L, 0xffffffffffffffefL },
            { 1, 0xfffffffffe332b6aL },
            { 1, 0x0000000001ccd486L, 0x1L },
            { 1, 0x33d8c8ecb7e24L }
        },
        {   // ========== bug ========== >>>>>>>>>> fixed <<<<<<<<<<
                // numerator: { sign, magnitude_0, magnitude_1, ... }
                { -1, 0xfc95c7b59290e18fL, 0x24L,  },
                { -1, 0x37443cf97eL,  },
                { 1, 0xab536591L,  },
                { -1, 0xfcfbedb31L,  }, },

        {   // ========== bug ========== >>>>>>>>>> fixed <<<<<<<<<<
                // numerator: { sign, magnitude_0, magnitude_1, ... }
                { 1, 0xd7bfdc0942757127L, 0x8d044e285f9fa19aL, 0xab5e5622534b9a7cL, 0xa3137ffe4a9b2839L, 0x752b52L, },
                // denominator: { sign, magnitude_0, magnitude_1, ... }
                { 1, 0xc918180b63e89559L, 0x6bed50d4e574fde6L, 0xb3d5fa4a8fa1c6b8L, 0x1a889dde1befd53fL, 0x6ac174L, },
                // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x1L, },
                // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x0ea7c3fdde8cdbceL, 0x2116fd537a2aa3b4L, 0xf7885bd7c3a9d3c4L, 0x888ae2202eab52f9L, 0xa69deL },},

        {
                // magSource: { sign, magnitude_0, magnitude_1, ... }
                {1, 0xffffffffffffffffL, },
                // magTarget: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x6L,},
                // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x2aaaaaaaaaaaaaaaL, },
                // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x3L,},},
        {
                // magSource: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x62L, },
                // magTarget: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x19L,},
                // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x3L, },
                // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x17L,},},

        {
                // magSource: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x62L, },
                // magTarget: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x5L,},
                // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x13L, },
                // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x3L,},},

        {
                // magSource: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x62L, },
                // magTarget: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x4L,},
                // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x18L, },
                // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x2L,},},

        {
                // magSource: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x138L, },
                // magTarget: { sign, magnitude_0, magnitude_1, ... }
                {1, 0xdL,},
                // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x18L, },
                // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x0L,},},

        {
                // magSource: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x60L, },
                // magTarget: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x6L,},
                // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x10L, },
                // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x0L,},},


        {
                // magSource: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x8000025a17ab472dL, },
                // magTarget: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x2L,},
                // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x4000012d0bd5a396L, },
                // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x1L,},},


            {
                // magSource: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x62L, },
                // magTarget: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x9L,},
                // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
                {1, 0xaL, },
                // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x8L,},},
            {
                // magSource: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x62L, },
                // magTarget: { sign, magnitude_0, magnitude_1, ... }
                {1, 0xaL,},
                // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x9L, },
                // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
                {1, 0x8L,},},


        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L, 0x2L},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2L,},
            // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0L, 0x1L,},
            // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xa9c30749f7356a95L, 0x786b229c10L},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2L,},
            // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x54e183a4fb9ab54aL, 0x3c35914e08L,},
            // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x05d65fac68fdb270L, 0xd573909041f6f0d6L, 0xc119f88a362602caL, 0xeefecdac1db8c3f5L, 0x35df30f71870868bL, 0x42d903cc73d3d676L, 0xcfb79deafcafcba4L, 0x04a5970958b17082L, 0xf07cd1a779cb12e4L, 0x8b5a2eL,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xd558d8ca69f53361L, 0xfa49e5efdd5b9109L, 0x682f19464d459bf0L, 0xf42ef10e97857acfL, 0xb08e801379dacd91L, 0x945175d0b0efL,},
            // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x12f85cc9016fc87cL, 0x6a71ea31f09f0264L, 0x9faabff4dc171ae2L, 0xf0864f2293L,},
            // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x934d212bde060774L, 0xa170ab8bbb917afaL, 0x46ec122db2be3a23L, 0xbdbf67d9e8995be4L, 0xf71096c423f86711L, 0x5dba7f26987bL,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x9a8ca1ce47310447L, 0x70906724bc8cb717L, 0x5c373cf99661b600L, 0x5b26268c4642856bL, 0x010bf97730bf9e6bL, 0x79dcd22352ab5ee2L, 0x524eff7c25615139L, 0x0dcea988ff7a90f8L, 0xeeL,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x26L,},
            // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x40b2dbd6453debe6L, 0xe803ccd1cf112d3dL, 0x677ab77fd4ccad35L, 0x59fa446201d9545aL, 0xe51486909c3aeff5L, 0x68427151c58b3f20L, 0x4588d0d41bee59b7L, 0x43bb554db5e18a8dL, 0x6L,},
            // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x23L,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xf4d98b1de4e13d7dL, 0x244deb3e2c97f03aL, 0x6cc4f76673c7f931L, 0x8d161f4d65c91f04L, 0x45480b5f730b803bL, 0xd7ab7129b0d7efcdL, 0xb7972a163dc688ceL, 0xec06c1f744739922L, 0x65b6532abd1630f0L, 0x0e9d31b4fa03604dL, 0xfdd821f6d54b0c4fL, 0xc558533196ecb81aL, 0x52791aL,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x371e1a85a7dfb6d8L, 0xe2ef77c393f77b97L, 0xbf8d9d082c473aacL, 0x7dff76721664b7b6L, 0xd1b05a2b84f65ca4L, 0x366441a10aa0L,},
            // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xc7963d001cb88069L, 0x461549577df9aafcL, 0x977fccc505a7b4aaL, 0x7018002367571e50L, 0xcaa92345d60be845L, 0x83093061ecd69272L, 0x1842b150016L,},
            // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x44af3d9123733ee5L, 0xb672beeda3b73145L, 0x8e14d69df89ebb7eL, 0x4ff935b2a945eb1aL, 0x17ad153563889825L, 0x23e20811944L,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x8f0c3d9be7ba285cL, 0x3c4cbbcb87b46f08L, 0x57027d8d39089b55L, 0xfd90764aa28564L,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xa9c1fc357e3f58cbL, 0xeed597f144ea3d9cL, 0xbbb32f1206L,},
            // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xbe386d454e1edc64L, 0x159d4L,},
            // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x99824a06e4e30510L, 0xffda398a36fb3e55L, 0x822f5ec832L,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2d88aef417ca924bL, 0xf2068356c01ec2d1L, 0xce43b468884f21e0L, 0xe98af6ecf2L,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xd55a121db7f76968L, 0x5eb509L,},
            // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x5e93ef06c051a0c6L, 0x42f4fa94f4ae549fL, 0x27748L,},
            // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xd73ec436d2a60bdbL, 0x449f7L,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xfe71740fe6e24780L, 0x57966dd5a8e07dfeL, 0x9b616a835f24a7e8L, 0xeaL,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xd38d1b5496131830L, 0x28848adae29eededL, 0xc4949a7a8696e91bL,},
            // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x131L,},
            // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xf353e4491a227650L, 0x11acff0da98705a5L, 0x66555e890558ee8dL,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0xd2b1d3b2L,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x5a5cdf693136ac89L, 0x93e4e62e56L,},
            // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x16cb4a53f5c716dL,},
            // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x4683b66345f70f3bL, 0x28549fcebaL,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0000000000000000L, 0x62L,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0000000000000000L, 0x4L,},
            // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x18L,},
            // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0000000000000000L, 0x2L,},},
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x4eb38be566184d6aL, 0xcbc45403a010a54cL,},
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x610555a31dd12b55L, 0xf83b49841235e550L, 0x010d82dccc080eacL, 0x97L,},
            // expResultQuotient: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0L,},
            // expResultRemainder: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x4eb38be566184d6aL, 0xcbc45403a010a54cL,},},
    };

    /**
     * Test of divideMagnitude method, of class LargeInteger.
     */
    @Test
    public void testDivideAndRemainderMagnitude() {

        for (long[][] data : divideSameOrderMagnitudeDataTest) {
            long[] numerator = Arrays.copy( data[0], 1, data[0].length-1 );
            long[] denominator = Arrays.copy( data[1], 1, data[1].length-1 );
            long[] expQuotient = new long[]{ data[2][1] };
            long[][] result = Magnitude.divideAndRemainderMagnitude(numerator, denominator);
            long[] resultQuotient = result[0];
            Assert.assertArrayEquals(expQuotient, resultQuotient);
        }

        for (long[][] data : divideAndRemainderMagnitudeDataTest) {
            long[] numerator = Arrays.copy( data[0], 1, data[0].length-1 );
            long[] denominator = Arrays.copy( data[1], 1, data[1].length-1 );
            long[] expResultQuotient = Arrays.copy( data[2], 1, data[2].length-1 );
            long[] expResultRemainder = Arrays.copy( data[3], 1, data[3].length-1 );
            long[][] result = Magnitude.divideAndRemainderMagnitude(numerator, denominator);
            Assert.assertArrayEquals(expResultQuotient, result[0]);
            Assert.assertArrayEquals(expResultRemainder, result[1]);
        }

//        long[][][] debug = new long[][][]{
//
//            {
//                { -1, 0xf6181f0fee43f4b6L, 0x40e0c6bd1dc91b1dL, 0x1252c2L,  },
//                { 1, 0xcf12ffc9c5fc9036L, 0x8c18a3a3195bL,  },
//                { -1, 0x217b7eabf5L,  },
//                { -1, 0xbb0f2926d2a5df08L, 0x118f47a07dacL,  },
//            },
//        };
//
//        for (long[][] data : debug) {
//            long[] numerator = Arrays.copy( data[0], 1, data[0].length-1 );
//            long[] denominator = Arrays.copy( data[1], 1, data[1].length-1 );
//            long[] expResultQuotient = Arrays.copy( data[2], 1, data[2].length-1 );
//            long[] expResultRemainder = Arrays.copy( data[3], 1, data[3].length-1 );
//            long[][] result = Magnitude.divideAndRemainderMagnitude(numerator, denominator);
//            Assert.assertArrayEquals(expResultQuotient, result[0]);
//            Assert.assertArrayEquals(expResultRemainder, result[1]);
//        }
    }

    private static final long[][][] notAndOrXorDataTest = new long[][][] {
        {
            // magSource: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x4eb38be566184d6aL, 0xcbc45403a010a54cL, },
            // magTarget: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x610555a31dd12b55L, 0xf83b49841235e550L, 0x010d82dccc080eacL, 0x0000000000000097L, },
            // not magSource
            {1, 0xb14c741a99e7b295L, 0x343babfc5fef5ab3L, },
            // not magTarget
            {1, 0x9efaaa5ce22ed4aaL, 0x07c4b67bedca1aafL, 0xfef27d2333f7f153L, 0xffffffffffffff68L, },
            // magSource and magTarget
            {1, 0x400101a104100940L, 0xc80040000010a540L, 0x0000000000000000L, 0x0000000000000000L, },
            // magSource or magTarget
            {1, 0x6fb7dfe77fd96f7fL, 0xfbff5d87b235e55cL, 0x010d82dccc080eacL, 0x0000000000000097L, },
            // magSource or magTarget
            {1, 0x2fb6de467bc9663fL, 0x33ff1d87b225401cL, 0x010d82dccc080eacL, 0x0000000000000097L, },
        },
    };

    /**
     * Test of not method, of class Magnitude.
     */
    @Test
    public void testNot() {
        for (long[][] data : notAndOrXorDataTest) {
            // not magSource
            long[] mag = Arrays.copy( data[0], 1, data[0].length-1 );
            long[] expResult = Arrays.copy( data[2], 1, data[2].length-1 );
            long[] result = Magnitude.not(mag);
            Assert.assertArrayEquals(expResult, result);
            // not magTarget
            mag = Arrays.copy( data[1], 1, data[1].length-1 );
            expResult = Arrays.copy( data[3], 1, data[3].length-1 );
            result = Magnitude.not(mag);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of and method, of class Magnitude.
     */
    @Test
    public void testAnd() {
        for (long[][] data : notAndOrXorDataTest) {
            long[] mag0 = Arrays.copy( data[0], 1, data[0].length-1 );
            long[] mag1 = Arrays.copy( data[1], 1, data[1].length-1 );
            long[] expResult = Arrays.copy( data[4], 1, data[4].length-1 );
            long[] result = Magnitude.and(mag0, mag1);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of andNot method, of class Magnitude.
     */
    @Test
    public void testAndNot() {
        for (long[][] data : notAndOrXorDataTest) {
            long[] mag0 = Arrays.copy( data[0], 1, data[0].length-1 );
            long[] mag1 = Arrays.copy( data[1], 1, data[1].length-1 );
            long[] notMag1 = Arrays.copy( data[3], 1, data[3].length-1 );
            long[] expResult = Magnitude.and(mag0, notMag1);
            long[] result = Magnitude.andNot(mag0, mag1);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of or method, of class Magnitude.
     */
    @Test
    public void testOr() {
        for (long[][] data : notAndOrXorDataTest) {
            long[] mag0 = Arrays.copy( data[0], 1, data[0].length-1 );
            long[] mag1 = Arrays.copy( data[1], 1, data[1].length-1 );
            long[] expResult = Arrays.copy( data[5], 1, data[5].length-1 );
            long[] result = Magnitude.or(mag0, mag1);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of orNot method, of class Magnitude.
     */
    @Test
    public void testOrNot() {
        for (long[][] data : notAndOrXorDataTest) {
            long[] mag0 = Arrays.copy( data[0], 1, data[0].length-1 );
            long[] mag1 = Arrays.copy( data[1], 1, data[1].length-1 );
            long[] notMag1 = Arrays.copy( data[3], 1, data[3].length-1 );
            long[] expResult = Magnitude.or(mag0, notMag1);
            long[] result = Magnitude.orNot(mag0, mag1);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of xor method, of class Magnitude.
     */
    @Test
    public void testXor() {
        for (long[][] data : notAndOrXorDataTest) {
            long[] mag0 = Arrays.copy( data[0], 1, data[0].length-1 );
            long[] mag1 = Arrays.copy( data[1], 1, data[1].length-1 );
            long[] expResult = Arrays.copy( data[6], 1, data[6].length-1 );
            long[] result = Magnitude.xor(mag0, mag1);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of xorNot method, of class Magnitude.
     */
    @Test
    public void testXorNot() {
        for (long[][] data : notAndOrXorDataTest) {
            long[] mag0 = Arrays.copy( data[0], 1, data[0].length-1 );
            long[] mag1 = Arrays.copy( data[1], 1, data[1].length-1 );
            long[] notMag1 = Arrays.copy( data[3], 1, data[3].length-1 );
            long[] expResult = Magnitude.xor(mag0, notMag1);
            long[] result = Magnitude.xorNot(mag0, mag1);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    private static final long[][][] shiftDataTest = new long[][][]{

        {   // ========== BUG ========== >>>>>>>>>> fixed <<<<<<<<<<
            // mag: { n, magnitude_0, magnitude_1, ... }
            { 64, 0xfffffffffe332b6aL, 0x0L, 0x0L,},
            // mag >>> n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0L, },
            // mag << n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0L, 0xfffffffffe332b6aL, },
        },

        {   // ========== BUG ========== >>>>>>>>>> fixed <<<<<<<<<<
            // mag: { n, magnitude_0, magnitude_1, ... }
            { 0, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
            // mag >>> n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
            // mag << n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
        },
        {
            // mag: { n, magnitude_0, magnitude_1, ... }
            { 0, 0xfL, },
            // mag >>> n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0xfL, },
            // mag << n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0xfL, },
        },



        {
            // mag: { n, magnitude_0, magnitude_1, ... }
            { 64, 0x2L, },
            // mag >>> n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0L, },
            // mag << n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0L, 0x2L, },
        },
        {
            // mag: { n, magnitude_0, magnitude_1, ... }
            { 4, 0xfL, },
            // mag >>> n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0L, },
            // mag << n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0xf0L, },
        },
        {
            // mag: { n, magnitude_0, magnitude_1, ... }
            { 4, 0x0000000000000000L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L, },
            // mag >>> n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x1000000000000000L, 0x2de75808980deeaeL, 0x000000000d2b1d3bL, },
            // mag << n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0000000000000000L, 0xe75808980deeae10L, 0x0000000d2b1d3b2dL, },
        },
        {
            // mag: { n, magnitude_0, magnitude_1, ... }
            { 4, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
            // mag >>> n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x167eff14f14a2fe9L, 0x2de75808980deeaeL, 0x000000000d2b1d3bL,},
            // mag << n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x7eff14f14a2fe900L, 0xe75808980deeae16L, 0x0000000d2b1d3b2dL,},
        },
        {
            // mag: { n, magnitude_0, magnitude_1, ... }
            {72, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
            // mag >>> n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0xb2de75808980deeaL, 0x0000000000d2b1d3L,},
            // mag << n: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0000000000000000L, 0xeff14f14a2fe9000L, 0x75808980deeae167L, 0x000000d2b1d3b2deL,},
        },
    };

    /**
     * Test of shiftRight method, of class Magnitude.
     */
    @Test
    public void testShiftRight() {
        for (long[][] data : shiftDataTest) {
            long[] mag = Arrays.copy( data[0], 1, data[0].length-1 );
            int n = (int) data[0][0];
            long[] expResult = Arrays.copy( data[1], 1, data[1].length-1 );
            long[] result = Magnitude.shiftRight(mag, n);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of shiftRightBuffered method, of class Magnitude.
     */
    @Test
    public void testShiftRightBuffered() {
        for (long[][] data : shiftDataTest) {
            long[] buffer = Arrays.copy( data[0], 1, data[0].length-1 );
            int n = (int) data[0][0];
            long[] expResult = Arrays.copy( data[1], 1, data[0].length-1 );
            Magnitude.shiftRightBuffered(buffer, n);
            Assert.assertArrayEquals(expResult, buffer);
        }
    }

    /**
     * Test of shiftLeft method, of class Magnitude.
     */
    @Test
    public void testShiftLeft() {
        for (long[][] data : shiftDataTest) {
            long[] mag = Arrays.copy( data[0], 1, data[0].length-1 );
            int n = (int) data[0][0];
            long[] expResult = Arrays.copy( data[2], 1, data[2].length-1 );
            long[] result = Magnitude.shiftLeft(mag, n);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of shiftLeftBuffered method, of class Magnitude.
     */
    @Test
    public void testShiftLeftBuffered() {

        for (long[][] data : shiftDataTest) {
            long[] buffer = Arrays.copy( data[0], 1, data[0].length-1 );
            int n = (int) data[0][0];
            long[] expResult = Arrays.copy( data[2], 1, data[0].length-1 );
            Magnitude.shiftLeftBuffered(buffer, n);
            Assert.assertArrayEquals(expResult, buffer);
        }
    }

    private static final long[][][] toNegativeDataTest = new long[][][] {
        {
            // mag: { newLength, magnitude_0, magnitude_1, ... }
            { 2, 0x0000000000000000L, 0x1L },
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0000000000000000L, 0xffffffffffffffffL },
        },
        {
            // mag: { newLength, magnitude_0, magnitude_1, ... }
            { 4, 0x0000000000000000L, 0x1L },
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0000000000000000L, 0xffffffffffffffffL, 0xffffffffffffffffL, 0xffffffffffffffffL },
        },
        {
            // mag: { newLength, magnitude_0, magnitude_1, ... }
            { 4, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
            // expResult: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x98100eb0eb5d0170L, 0x218a7f767f21151eL, 0xffffffff2d4e2c4dL, 0xffffffffffffffffL },
        },
    };

    /**
     * Test of toNegative method, of class Magnitude.
     */
    @Test
    public void testToNegative() {
        for ( long[][] data : toNegativeDataTest ) {
            long[] mag = Arrays.copy( data[0], 1, data[0].length-1 );
            int newLength = (int) data[0][0];
            long[] expResult = Arrays.copy( data[1], 1, data[1].length-1 );
            long[] result = Magnitude.toNegative(mag, newLength);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    private static final long[][][] getBitDataTest = new long[][][] {
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0000000000000000L, },
            // { sign, magnitude_0, magnitude_1, ... }
            { 30, 0 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0000000000000000L, },
            // { sign, magnitude_0, magnitude_1, ... }
            { 120, 0 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
            // { sign, magnitude_0, magnitude_1, ... }
            { 71, 1 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
            // { sign, magnitude_0, magnitude_1, ... }
            { 190, 0 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
            // { sign, magnitude_0, magnitude_1, ... }
            { 200, 0 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { -1, 0x0000000000000146L, },
            // { sign, magnitude_0, magnitude_1, ... }
            { 6, 0 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { -1, 0x0000000000000146L, },
            // { sign, magnitude_0, magnitude_1, ... }
            { 5, 1 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { -1, 0x0000000000000146L, },
            // { sign, magnitude_0, magnitude_1, ... }
            { 70, 1 },
        },
    };

    /**
     * Test of getBit method, of class Magnitude.
     */
    @Test
    public void testGetBit() {
        for ( long[][] data : getBitDataTest ) {
            long[] mag = Arrays.copy( data[0], 1, data[0].length-1 );
            int sign = (int) data[0][0];
            int index = (int) data[1][0];
            boolean expResult = data[1][1]==1L;
            boolean result = Magnitude.getBit(sign, mag, index);
            Assert.assertEquals(expResult, result);
        }
    }

    private static final long[][][] getByteLengthDataSample = new long[][][]{
        {
            // { sign, mag0, mag1, ..., mag_n-1 }
            { 1, 0x79d78eb0250c5546L, 0xecfa786db18cf72fL, 0x28e68bf9a5d18b55L, 0x18dbc2965L,},
            // { byteLength, bitLength }
            { 29, 225 }
        },
        {
            // { sign, mag0, mag1, ..., mag_n-1 }
            { -1, 0x414a1baf1ac638a9L, 0x6bec1d0eb611743cL, 0xccdf9e7238fab92eL, 0x39096a20b936158dL, 0xb45b0b6777a199adL, 0xcd0d0a00906cc980L, 0xfa6L,  },
            // { byteLength, bitLength }
            { 50, 396 }
        },
        {
            // { sign, mag0, mag1, ..., mag_n-1 }
            { -1, 0x04352291122faa1cL, 0x11fcbf1495f69b6bL, 0x354efe93006fdc4dL, 0x1a2a0a5b1cf442cbL, 0x90ada58b8a739872L, 0x16b2L,  },
            // { byteLength, bitLength }
            { 42, 333 }
        },
        {
            // { sign, mag0, mag1, ..., mag_n-1 }
            {  1, 0x4d43fc5f13276336L, 0x482ac6f9f2a08ba9L, 0xd6a06dbcc303dbb4L, 0xdfd8343f74ffe22eL, 0xfe56db3988f7095L,  },
            // { byteLength, bitLength }
            { 40, 316 }
        },
        {
            // { sign, mag0, mag1, ..., mag_n-1 }
            { 1, 0x0L,  },
            // { byteLength, bitLength }
            { 1, 1 }
        },
        {
            // { sign, mag0, mag1, ..., mag_n-1 }
            { 1, 0xcfdc8322d6001f44L, 0xce8fa006018b522aL, 0x267da078d89d0387L, 0xc1cdd4f04b4bf3c0L, 0x9e4df3183942559aL, 0xa551cae0a8a07ad5L, 0x6L,  },
            // { byteLength, bitLength }
            { 49, 387 }
        },
        {
            // { sign, mag0, mag1, ..., mag_n-1 }
            { -1, 0x8cd56debbd821ea9L, 0xb23ceL,  },
            // { byteLength, bitLength }
            { 11, 84 }
        },
        {
            // { sign, mag0, mag1, ..., mag_n-1 }
            { 1, 0x26f61L,  },
            // { byteLength, bitLength }
            { 3, 18 }
        },
        {
            // { sign, mag0, mag1, ..., mag_n-1 }
            { 1, 0x2d0542febaef4e4cL, 0x56b4e2bf6adL, },
            // { byteLength, bitLength }
            { 14, 107 }
        },
    };

    /**
     * Test of getBitLength method, of class Magnitude.
     */
    @Test
    public void testGetBitLength() {
        for (long[][] data : getByteLengthDataSample) {
            long[] mag = Arrays.copy(data[0], 1, data[0].length - 1);
            int expResult = (int) data[1][1];
            int result = Magnitude.getBitLength(mag);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of getByteLength method, of class Magnitude.
     */
    @Test
    public void testGetByteLength() {
        for (long[][] data : getByteLengthDataSample) {
            long[] mag = Arrays.copy(data[0], 1, data[0].length - 1);
            int expResult = (int) data[1][0];
            int result = Magnitude.getByteLength( mag );
            Assert.assertEquals(expResult, result);
        }
    }

}
