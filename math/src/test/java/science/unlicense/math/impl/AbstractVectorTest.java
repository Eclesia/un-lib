
package science.unlicense.math.impl;

import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractVectorTest extends AbstractTupleTest {

    @Override
    protected abstract VectorRW create(int dim);

}
