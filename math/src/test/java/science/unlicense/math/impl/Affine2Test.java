
package science.unlicense.math.impl;

import science.unlicense.math.api.AffineRW;

/**
 * Test Affine2.
 *
 * @author Johann Sorel
 */
public class Affine2Test extends AbstractAffineTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{2};
    }

    @Override
    protected AffineRW create(int dim) {
        return new Affine2(0,0,0,0,0,0);
    }

}
