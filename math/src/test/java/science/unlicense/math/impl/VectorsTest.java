/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package science.unlicense.math.impl;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.Arrays;
import science.unlicense.math.api.Maths;

/**
 * @author Bertrand COTE
 */
public class VectorsTest {

    private static final double EPSILON = 1e-6;

    // =========================================================================
    // ===== Helper functions ==================================================

    /**
     * Helper function
     *
     * @param in a double[]
     * @return out a float[]
     */
    private static float[] doubleArrayToFloatArray( double[] in ) {
        float[] out = new float[in.length];
        for ( int i=0; i<in.length; i++ ) out[i] = (float) in[i];
        return out;
    }

    private static boolean isNotZero( double[] v ) {
        for ( int i=0; i<v.length; i++ ) {
            if ( v[i] == 0. ) return false;
        }
        return true;
    }

    private static boolean isNotZero( float[] v ) {
        for ( int i=0; i<v.length; i++ ) {
            if ( v[i] == 0. ) return false;
        }
        return true;
    }

    // =========================================================================
    // ===== data for tests ====================================================

    private static final double[][][] vectorsDataTest = new double[][][]{
    //  {{ vector.values }, { scale factor }, { scale expResult.values }, { normalize expResult } },
        { {  0. },                {  2. }, { 0. },                     { 0. } },
        { {  5. },                {  3. }, { 15. },                    { 1. } },
        { {  0.,  0. },           { -4. }, { 0., 0. },                 { 0., 0. } },
        { {  2.,  4. },           {  5. }, { 10., 20. },               { 0.447213595499958, 0.894427190999916 } },
        { {  0.,  0.,  0. },      {  6. }, { 0.,  0.,  0. },           { 0.,  0.,  0. } },
        { { -5.,  8., -2. },      { -7. }, { 35.,  -56.,  14. },       { -0.518475847365213, 0.829561355784340, -0.207390338946085} },
        { {  0.,  0.,  0.,  0. }, { -9. }, { 0.,  0.,  0.,  0. },      { 0.,  0.,  0.,  0. } },
        { {  3., -9.,  4., -1. }, { -4. }, { -12.,  36.,  -16.,  4. }, { 0.2900209467136990, -0.8700628401410971, 0.3866945956182654, -0.0966736489045664 } },
        { {  },                   { -2. }, {  },                       {  } },
    };

    private static double[][][] addSubMultDivLerpTest = new double[][][]{
    // { { vector.values }, { other.values }, { add.values }, { subtract.values }, { multiply.values }, { divide.values }, { lerp ratio }, { lerp expResult } },
        { { 3. },           { 2. },           { 5. },         { 1. },              { 6. },              { 1.5 },            {  0.5 }, { 2.5 } },
        { { 3., 5. },       { 2., 10. },      { 5., 15. },    { 1., -5. },         { 6., 50. },         { 1.5, 0.5 },       { 0.25 }, {  2.75, 6.25 } },
        { { 3., 5., 9. },   { 2., 10., -3. }, { 5., 15., 6. },{ 1., -5., 12. },    { 6., 50., -27. },   { 1.5, 0.5, -3. },  { 0.75 }, {  2.25, 8.75, 0.0 } },
    };

    private static double[][][] crossTest = new double[][][]{
        // { { v1 },       { v2 },          { v1 cross v2 } },
        { { -2., 0., 0. }, { 5., 0.,  0. }, {  0.,  0.,  0.} },
        { {  0., 9., 0. }, { 0., 3.,  0. }, {  0.,  0.,  0.} },
        { {  0., 0., 7. }, { 0., 0., -8. }, {  0.,  0.,  0.} },
        { {  2., 3., 4. }, { 7., 6.,  5. }, { -9., 18., -9.} },
    };

    private static double[][][] angleTest = new double[][][]{
        //{ { v1 }, { v2 }, { angle }, },
        { { 1., 0. }, { 0., 1. }, { Maths.HALF_PI }, },
        { { 0., 1. }, { 1., 0. }, { Maths.HALF_PI }, },
        { { -2., 0. }, { 0., -2. }, { Maths.HALF_PI }, },
        { { 0., -2. }, { -2., 0. }, { Maths.HALF_PI }, },
        { { -2., -2. }, { -2., 0. }, { Maths.QUATER_PI }, },

        { { 5.2147, -19.2415 }, { -0.36510, 3.25050 }, { 2.98878910249942 } },

        { { 3., 0., 0. }, { 0., 2., 0. }, { Maths.HALF_PI }, },
        { { 3., 0., 0. }, { 0., 0., -5. }, { Maths.HALF_PI }, },
        { { 0., -7., 0. }, { 0., 0., 9. }, { Maths.HALF_PI }, },

        { { 5.2147, -19.2415, -0.3699 }, { -0.36510, 3.25050, 1.5747 }, { 2.68695055311836 }, },
    };

    double[][][] cosSinTest = new double[][][]{
        // { { vec1 }, { vec2 }, { cosine, sinus },},
        { { 2., 0. }, { 3., 3. }, { Math.sqrt(2.)/2,  Math.sqrt(2.)/2 },},
        { { 3., 3. }, { 2., 0. }, { Math.sqrt(2.)/2, -Math.sqrt(2.)/2 },},

        { { 2., 0., 0. }, { 3., 3., 0. }, { Math.sqrt(2.)/2, Math.sqrt(2.)/2 },},
        { { 3., 3., 0. }, { 2., 0., 0. }, { Math.sqrt(2.)/2, Math.sqrt(2.)/2 },},

        { { 2., 2., 0. }, { 3., 3., 3. }, { Math.sqrt(6./9.), Math.sqrt(3./9.) },},
        { { 3., 3., 3. }, { 2., 2., 0. }, { Math.sqrt(6./9.), Math.sqrt(3./9.) },},

        { { 2., 0., 0. }, { 3., 3., 3. }, { Math.sqrt(3./9.), Math.sqrt(6./9.) },},
        { { 3., 3., 3. }, { 2., 0., 0. }, { Math.sqrt(3./9.), Math.sqrt(6./9.) },},

    };

    // =========================================================================
    // =========================================================================

    public VectorsTest() {
    }

    @Test
    public void testReflect(){

        //vertical reflection
        Assert.assertArrayEquals(new double[]{0,+1,0}, Vectors.reflect(new double[]{0,-1,0}, new double[]{0,1,0}),EPSILON);
        //45° reflection
        Assert.assertArrayEquals(new double[]{-1,+1,0}, Vectors.reflect(new double[]{-1,-1,0}, new double[]{0,1,0}),EPSILON);

    }

    /**
     * Test of length method, of class Vectors.
     */
    @Test
    public void testLength_doubleArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double expResult = 0.0;
            for (int k=0; k<vector.length; k++) expResult += vector[k]*vector[k];
            expResult = Math.sqrt(expResult);
            double result = Vectors.length(vector);
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of length method, of class Vectors.
     */
    @Test
    public void testLength_floatArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(vectorsDataTest[i][0]);
            float expResult = 0.0f;
            for (int k=0; k<vector.length; k++) expResult += vector[k]*vector[k];
            expResult = (float) Math.sqrt(expResult);
            float result = Vectors.length(vector);
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of lengthSquare method, of class Vectors.
     */
    @Test
    public void testLengthSquare_doubleArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double expResult = 0.0;
            for (int k=0; k<vector.length; k++) expResult += vector[k]*vector[k];
            double result = Vectors.lengthSquare(vector);
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of lengthSquare method, of class Vectors.
     */
    @Test
    public void testLengthSquare_floatArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(vectorsDataTest[i][0]);
            float expResult = 0.0f;
            for (int k=0; k<vector.length; k++) expResult += vector[k]*vector[k];
            float result = Vectors.lengthSquare(vector);
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of shortestAngle method, of class Vectors.
     */
    @Test
    public void testShortestAngle_doubleArr_doubleArr() {
        for ( int i=0; i<angleTest.length; i++ ) {
            double[] vec1 = angleTest[i][0];
            double[] vec2 = angleTest[i][1];
            double expResult = angleTest[i][2][0];
            double result = Vectors.shortestAngle(vec1, vec2);
            Assert.assertEquals(expResult, result, EPSILON);
        }
    }

    /**
     * Test of shortestAngle method, of class Vectors.
     */
    @Test
    public void testShortestAngle_floatArr_floatArr() {
        for ( int i=0; i<angleTest.length; i++ ) {
            float[] vec1 = doubleArrayToFloatArray(angleTest[i][0]);
            float[] vec2 = doubleArrayToFloatArray(angleTest[i][1]);
            float expResult = (float) angleTest[i][2][0];
            float result = Vectors.shortestAngle(vec1, vec2);
            Assert.assertEquals(expResult, result, EPSILON);
        }
    }

    /**
     * Test of clampEuler method, of class Vectors.
     */
    @Test
    public void testClampEuler() {
//        System.out.println("clampEuler");
//        Vectors.clampEuler(euler);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of add method, of class Vectors.
     */
    @Test
    public void testAdd_doubleArr_doubleArr() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            double[] expResult = addSubMultDivLerpTest[i][2];
            double[] result = Vectors.add(vector, other);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of add method, of class Vectors.
     */
    @Test
    public void testAdd_floatArr_floatArr() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float[] other = doubleArrayToFloatArray(addSubMultDivLerpTest[i][1]);

            float[] expResult = doubleArrayToFloatArray(addSubMultDivLerpTest[i][2]);
            float[] result = Vectors.add(vector, other);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of subtract method, of class Vectors.
     */
    @Test
    public void testSubtract_doubleArr_doubleArr() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            double[] expResult = addSubMultDivLerpTest[i][3];
            double[] result = Vectors.subtract(vector, other);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of subtract method, of class Vectors.
     */
    @Test
    public void testSubtract_floatArr_floatArr() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float[] other = doubleArrayToFloatArray(addSubMultDivLerpTest[i][1]);

            float[] expResult = doubleArrayToFloatArray(addSubMultDivLerpTest[i][3]);
            float[] result = Vectors.subtract(vector, other);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of multiply method, of class Vectors.
     */
    @Test
    public void testMultiply_doubleArr_doubleArr() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            double[] expResult = addSubMultDivLerpTest[i][4];
            double[] result = Vectors.multiply(vector, other);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of multiply method, of class Vectors.
     */
    @Test
    public void testMultiply_floatArr_floatArr() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float[] other = doubleArrayToFloatArray(addSubMultDivLerpTest[i][1]);

            float[] expResult = doubleArrayToFloatArray(addSubMultDivLerpTest[i][4]);
            float[] result = Vectors.multiply(vector, other);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of divide method, of class Vectors.
     */
    @Test
    public void testDivide_doubleArr_doubleArr() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            double[] expResult = addSubMultDivLerpTest[i][5];
            double[] result = Vectors.divide(vector, other);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of divide method, of class Vectors.
     */
    @Test
    public void testDivide_floatArr_floatArr() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float[] other = doubleArrayToFloatArray(addSubMultDivLerpTest[i][1]);

            float[] expResult = doubleArrayToFloatArray(addSubMultDivLerpTest[i][5]);
            float[] result = Vectors.divide(vector, other);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of scale method, of class Vectors.
     */
    @Test
    public void testScale_doubleArr_double() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double scale =  vectorsDataTest[i][1][0];

            double[] expResult = vectorsDataTest[i][2];
            double[] result = Vectors.scale(vector, scale);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of scale method, of class Vectors.
     */
    @Test
    public void testScale_floatArr_float() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(vectorsDataTest[i][0]);
            float scale =  (float) vectorsDataTest[i][1][0];

            float[] expResult = doubleArrayToFloatArray(vectorsDataTest[i][2]);
            float[] result = Vectors.scale(vector, scale);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of cross method, of class Vectors.
     */
    @Test
    public void testCross_doubleArr_doubleArr() {

        for ( int i=0; i<crossTest.length; i++ ) {
            double[] v1 = crossTest[i][0];
            double[] v2 = crossTest[i][1];
            double[] expResult = crossTest[i][2];

            double[] result = Vectors.cross(v1, v2);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of cross method, of class Vectors.
     */
    @Test
    public void testCross_floatArr_floatArr() {
        for ( int i=0; i<crossTest.length; i++ ) {
            float[] v1 = doubleArrayToFloatArray(crossTest[i][0]);
            float[] v2 = doubleArrayToFloatArray(crossTest[i][1]);
            float[] expResult = doubleArrayToFloatArray(crossTest[i][2]);

            float[] result = Vectors.cross(v1, v2);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of dot method, of class Vectors.
     */
    @Test
    public void testDot_doubleArr_doubleArr() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] v1 = addSubMultDivLerpTest[i][0];
            double[] v2 = addSubMultDivLerpTest[i][0];
            double result = Vectors.dot(v1, v2);
            double expResult = 0.0;
            for ( int k=0; k<v1.length; k++ ) expResult += v1[k]*v2[k];
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of dot method, of class Vectors.
     */
    @Test
    public void testDot_floatArr_floatArr() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            float[] v1 = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float[] v2 = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float result = Vectors.dot(v1, v2);
            float expResult = 0.0f;
            for ( int k=0; k<v1.length; k++ ) expResult += v1[k]*v2[k];
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of normalize method, of class Vectors.
     */
    @Test
    public void testNormalize_doubleArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            if ( isNotZero(vector) ){
                double[] expResult = vectorsDataTest[i][3];

                double[] result = Vectors.normalize(vector);
                Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
            }
        }
    }

    /**
     * Test of normalize method, of class Vectors.
     */
    @Test
    public void testNormalize_floatArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(vectorsDataTest[i][0]);
            if ( isNotZero(vector) ){
                float[] expResult = doubleArrayToFloatArray(vectorsDataTest[i][3]);

                float[] result = Vectors.normalize(vector);
                Assert.assertTrue( Arrays.equals(expResult, result, (float) EPSILON));
            }
        }
    }

    /**
     * Test of negate method, of class Vectors.
     */
    @Test
    public void testNegate_doubleArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double[] expResult = new double[vector.length];
            for ( int k=0; k<vector.length; k++ ) expResult[k] = -vector[k];

            double[] result = Vectors.negate(vector);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of negate method, of class Vectors.
     */
    @Test
    public void testNegate_floatArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(vectorsDataTest[i][0]);
            float[] expResult = new float[vector.length];
            for ( int k=0; k<vector.length; k++ ) expResult[k] = -vector[k];

            float[] result = Vectors.negate(vector);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of lerp method, of class Vectors.
     */
    @Test
    public void testLerp_3args_1() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] start = addSubMultDivLerpTest[i][0];
            double[] end = addSubMultDivLerpTest[i][1];
            double ratio = addSubMultDivLerpTest[i][6][0];
            double[] expResult = addSubMultDivLerpTest[i][7];

            double[] result = Vectors.lerp(start, end, ratio);
            Assert.assertTrue( Arrays.equals(expResult, result) );
        }
    }

    /**
     * Test of lerp method, of class Vectors.
     */
    @Test
    public void testLerp_3args_2() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            float[] start = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float[] end = doubleArrayToFloatArray(addSubMultDivLerpTest[i][1]);
            float ratio = (float) addSubMultDivLerpTest[i][6][0];
            float[] expResult = doubleArrayToFloatArray(addSubMultDivLerpTest[i][7]);

            float[] result = Vectors.lerp(start, end, ratio);
            Assert.assertTrue( Arrays.equals(expResult, result) );
        }
    }

    /**
     * Test of add method, of class Vectors.
     */
    @Test
    public void testAdd_3args_1() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            double[] buffer = null;
            double[] expResult = addSubMultDivLerpTest[i][2];
            double[] result = Vectors.add(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new double[addSubMultDivLerpTest[i][0].length];
            Arrays.fill(buffer, -10000);
            result = Vectors.add(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of add method, of class Vectors.
     */
    @Test
    public void testAdd_3args_2() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float[] other = doubleArrayToFloatArray(addSubMultDivLerpTest[i][1]);

            float[] buffer = null;
            float[] expResult = doubleArrayToFloatArray(addSubMultDivLerpTest[i][2]);
            float[] result = Vectors.add(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new float[addSubMultDivLerpTest[i][0].length];
            Arrays.fill(buffer, -10000);
            result = Vectors.add(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of subtract method, of class Vectors.
     */
    @Test
    public void testSubtract_3args_1() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            double[] buffer = null;
            double[] expResult = addSubMultDivLerpTest[i][3];
            double[] result = Vectors.subtract(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new double[addSubMultDivLerpTest[i][0].length];
            Arrays.fill(buffer, -10000);
            result = Vectors.subtract(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of subtract method, of class Vectors.
     */
    @Test
    public void testSubtract_3args_2() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float[] other = doubleArrayToFloatArray(addSubMultDivLerpTest[i][1]);

            float[] buffer = null;
            float[] expResult = doubleArrayToFloatArray(addSubMultDivLerpTest[i][3]);
            float[] result = Vectors.subtract(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new float[addSubMultDivLerpTest[i][0].length];
            Arrays.fill(buffer, -10000);
            result = Vectors.subtract(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of multiply method, of class Vectors.
     */
    @Test
    public void testMultiply_3args_1() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            double[] buffer = null;
            double[] expResult = addSubMultDivLerpTest[i][4];
            double[] result = Vectors.multiply(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new double[addSubMultDivLerpTest[i][0].length];
            Arrays.fill(buffer, -10000);
            result = Vectors.multiply(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of multiply method, of class Vectors.
     */
    @Test
    public void testMultiply_3args_2() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float[] other = doubleArrayToFloatArray(addSubMultDivLerpTest[i][1]);

            float[] buffer = null;
            float[] expResult = doubleArrayToFloatArray(addSubMultDivLerpTest[i][4]);
            float[] result = Vectors.multiply(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new float[addSubMultDivLerpTest[i][0].length];
            Arrays.fill(buffer, -10000);
            result = Vectors.multiply(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of divide method, of class Vectors.
     */
    @Test
    public void testDivide_3args_1() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];

            double[] buffer = null;
            double[] expResult = addSubMultDivLerpTest[i][5];
            double[] result = Vectors.divide(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new double[addSubMultDivLerpTest[i][0].length];
            Arrays.fill(buffer, -10000);
            result = Vectors.divide(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of divide method, of class Vectors.
     */
    @Test
    public void testDivide_3args_2() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float[] other = doubleArrayToFloatArray(addSubMultDivLerpTest[i][1]);

            float[] buffer = null;
            float[] expResult = doubleArrayToFloatArray(addSubMultDivLerpTest[i][5]);
            float[] result = Vectors.divide(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new float[addSubMultDivLerpTest[i][0].length];
            Arrays.fill(buffer, -10000);
            result = Vectors.divide(vector, other, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of scale method, of class Vectors.
     */
    @Test
    public void testScale_3args_1() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double scale =  vectorsDataTest[i][1][0];
            double[] expResult = vectorsDataTest[i][2];

            double[] buffer = null;
            double[] result = Vectors.scale(vector, scale, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new double[vector.length];
            Arrays.fill(buffer, -10000);
            result = Vectors.scale(vector, scale, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of scale method, of class Vectors.
     */
    @Test
    public void testScale_3args_2() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(vectorsDataTest[i][0]);
            float scale =  (float) vectorsDataTest[i][1][0];
            float[] expResult = doubleArrayToFloatArray(vectorsDataTest[i][2]);

            float[] buffer = null;
            float[] result = Vectors.scale(vector, scale, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new float[vector.length];
            Arrays.fill(buffer, -10000);
            result = Vectors.scale(vector, scale, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of cross method, of class Vectors.
     */
    @Test
    public void testCross_3args_1() {
        for ( int i=0; i<crossTest.length; i++ ) {
            double[] v1 = crossTest[i][0];
            double[] v2 = crossTest[i][1];
            double[] expResult = crossTest[i][2];

            double[] buffer = null;
            double[] result = Vectors.cross(v1, v2, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new double[v1.length];
            result = Vectors.cross(v1, v2, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of cross method, of class Vectors.
     */
    @Test
    public void testCross_3args_2() {
        for ( int i=0; i<crossTest.length; i++ ) {
            float[] v1 = doubleArrayToFloatArray(crossTest[i][0]);
            float[] v2 = doubleArrayToFloatArray(crossTest[i][1]);
            float[] expResult = doubleArrayToFloatArray(crossTest[i][2]);

            float[] buffer = null;
            float[] result = Vectors.cross(v1, v2, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new float[v1.length];
            result = Vectors.cross(v1, v2, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of normalize method, of class Vectors.
     */
    @Test
    public void testNormalize_doubleArr_doubleArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            if ( isNotZero(vector) ){
                double[] expResult = vectorsDataTest[i][3];

                double[] buffer = null;
                double[] result = Vectors.normalize(vector, buffer);
                Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));

                buffer = new double[vector.length];
                Arrays.fill(buffer, -10000);
                result = Vectors.normalize(vector, buffer);
                Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
            }
        }
    }

    /**
     * Test of normalize method, of class Vectors.
     */
    @Test
    public void testNormalize_floatArr_floatArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(vectorsDataTest[i][0]);
            if ( isNotZero(vector) ){
                float[] expResult = doubleArrayToFloatArray(vectorsDataTest[i][3]);

                float[] buffer = null;
                float[] result = Vectors.normalize(vector, buffer);
                Assert.assertTrue( Arrays.equals(expResult, result, (float) EPSILON));

                buffer = new float[vector.length];
                Arrays.fill(buffer, -10000);
                result = Vectors.normalize(vector, buffer);
                Assert.assertTrue( Arrays.equals(expResult, result, (float) EPSILON));
            }
        }
    }

    /**
     * Test of negate method, of class Vectors.
     */
    @Test
    public void testNegate_doubleArr_doubleArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double[] expResult = new double[vector.length];
            for ( int k=0; k<vector.length; k++ ) expResult[k] = -vector[k];

            double[] buffer = null;
            double[] result = Vectors.negate(vector, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new double[vector.length];
            Arrays.fill(buffer, -10000);
            result = Vectors.negate(vector, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of negate method, of class Vectors.
     */
    @Test
    public void testNegate_floatArr_floatArr() {
        for ( int i=0; i<vectorsDataTest.length; i++ ) {
            float[] vector = doubleArrayToFloatArray(vectorsDataTest[i][0]);
            float[] expResult = new float[vector.length];
            for ( int k=0; k<vector.length; k++ ) expResult[k] = -vector[k];

            float[] buffer = null;
            float[] result = Vectors.negate(vector, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));

            buffer = new float[vector.length];
            Arrays.fill(buffer, -10000);
            result = Vectors.negate(vector, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of lerp method, of class Vectors.
     */
    @Test
    public void testLerp_4args_1() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] start = addSubMultDivLerpTest[i][0];
            double[] end = addSubMultDivLerpTest[i][1];
            double ratio = addSubMultDivLerpTest[i][6][0];
            double[] expResult = addSubMultDivLerpTest[i][7];

            double[] buffer = null;
            double[] result = Vectors.lerp(start, end, ratio, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result) );

            buffer = new double[start.length];
            Arrays.fill(buffer, -10000);
            result = Vectors.lerp(start, end, ratio, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result) );
        }
    }

    /**
     * Test of lerp method, of class Vectors.
     */
    @Test
    public void testLerp_4args_2() {
        for ( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            float[] start = doubleArrayToFloatArray(addSubMultDivLerpTest[i][0]);
            float[] end = doubleArrayToFloatArray(addSubMultDivLerpTest[i][1]);
            float ratio = (float) addSubMultDivLerpTest[i][6][0];
            float[] expResult = doubleArrayToFloatArray(addSubMultDivLerpTest[i][7]);

            float[] buffer = null;
            float[] result = Vectors.lerp(start, end, ratio, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result) );

            buffer = new float[start.length];
            Arrays.fill(buffer, -10000);
            result = Vectors.lerp(start, end, ratio, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result) );
        }
    }

    /**
     * Test of cos method, of class Vectors.
     */
    @Test
    public void testCos_doubleArr_doubleArr() {
        double[] vec1, vec2;
        double result, expResult;
        for ( int i=0; i<cosSinTest.length; i++ ) {
            vec1 = cosSinTest[i][0];
            vec2 = cosSinTest[i][1];
            expResult = cosSinTest[i][2][0];
            result = Vectors.cos(vec1, vec2);
            Assert.assertEquals(expResult, result, EPSILON);
        }
    }

    /**
     * Test of cos method, of class Vectors.
     */
    @Test
    public void testCos_floatArr_floatArr() {
        float[] vec1, vec2;
        float result, expResult;
        for ( int i=0; i<cosSinTest.length; i++ ) {
            vec1 = doubleArrayToFloatArray(cosSinTest[i][0]);
            vec2 = doubleArrayToFloatArray(cosSinTest[i][1]);
            expResult = (float) cosSinTest[i][2][0];
            result = Vectors.cos(vec1, vec2);
            Assert.assertEquals(expResult, result, EPSILON);
        }
    }

    /**
     * Test of sin method, of class Vectors.
     */
    @Test
    public void testSin_doubleArr_doubleArr() {
        for ( int i=0; i<cosSinTest.length; i++ ) {
            double[] vec1 = cosSinTest[i][0];
            double[] vec2 = cosSinTest[i][1];
            double expResult = cosSinTest[i][2][1];
            double result = Vectors.sin(vec1, vec2);
            Assert.assertEquals(expResult, result, EPSILON);
        }
    }

    /**
     * Test of sin method, of class Vectors.
     */
    @Test
    public void testSin_floatArr_floatArr() {
        for ( int i=0; i<cosSinTest.length; i++ ) {
            float[] vec1 = doubleArrayToFloatArray(cosSinTest[i][0]);
            float[] vec2 = doubleArrayToFloatArray(cosSinTest[i][1]);
            float expResult = (float) cosSinTest[i][2][1];
            float result = Vectors.sin(vec1, vec2);
            Assert.assertEquals(expResult, result, EPSILON);
        }
    }

    // =========================================================================

    private static final double[][][] cartPolCylSphTest = new double[][][]{
        {
            { 1.000000000000000,  1.000000000000000,  1.000000000000000 }, //           cartesian: { x, y, z }
            { 1.414213562373095,  0.785398163397448,  1.000000000000000 }, // polar / cylindrical: { r, theta, height }
            { 1.732050807568877,  0.785398163397448,  0.615479708670387 }, //           spherical: { rho, theta, phi }
        },
        {
            {  2.449489742783178, -2.449489742783178, -2.000000000000000 },
            {  3.464101615137754, -0.785398163397448, -2.000000000000000 },
            {  4.000000000000000, -0.785398163397448, -0.523598775598299 },
        },
        {
            { -1.000000000000000,  1.000000000000000, -1.414213562373095 },
            {  1.414213562373095,  2.356194490192345, -1.414213562373095 },
            {  2.000000000000000,  2.356194490192345, -0.785398163397448 },
        },
    };
    /**
     * Test of cartesianToPolar method, of class Vectors.
     */
    @Test
    public void testCartesianToPolar_doubleArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            double[] cartesian = new double[2];
            Arrays.copy(cartPolCylSphTest[i][0], 0, 2, cartesian, 0 );
            double[] expResult = new double[2];
            Arrays.copy(cartPolCylSphTest[i][1], 0, 2, expResult, 0 );
            double[] result = Vectors.cartesianToPolar(cartesian);
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of cartesianToPolar method, of class Vectors.
     */
    @Test
    public void testCartesianToPolar_floatArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            float[] cartesian = new float[2];
            Arrays.copy(doubleArrayToFloatArray(cartPolCylSphTest[i][0]), 0, 2, cartesian, 0 );
            float[] expResult = new float[2];
            Arrays.copy(doubleArrayToFloatArray(cartPolCylSphTest[i][1]), 0, 2, expResult, 0 );
            float[] result = Vectors.cartesianToPolar(cartesian);
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of polarToCartesian method, of class Vectors.
     */
    @Test
    public void testPolarToCartesian_doubleArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            double[] polar = new double[2];
            Arrays.copy(cartPolCylSphTest[i][1], 0, 2, polar, 0 );
            double[] expResult = new double[2];
            Arrays.copy(cartPolCylSphTest[i][0], 0, 2, expResult, 0 );
            double[] result = Vectors.polarToCartesian(polar);
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of polarToCartesian method, of class Vectors.
     */
    @Test
    public void testPolarToCartesian_floatArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            float[] polar = new float[2];
            Arrays.copy(doubleArrayToFloatArray(cartPolCylSphTest[i][1]), 0, 2, polar, 0 );
            float[] expResult = new float[2];
            Arrays.copy(doubleArrayToFloatArray(cartPolCylSphTest[i][0]), 0, 2, expResult, 0 );
            float[] result = Vectors.polarToCartesian(polar);
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of cartesianToCylindrical method, of class Vectors.
     */
    @Test
    public void testCartesianToCylindrical_doubleArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            double[] cartesian =  cartPolCylSphTest[i][0];
            double[] expResult = cartPolCylSphTest[i][1];
            double[] result = Vectors.cartesianToCylindrical(cartesian);
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of cartesianToCylindrical method, of class Vectors.
     */
    @Test
    public void testCartesianToCylindrical_floatArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            float[] cartesian =  doubleArrayToFloatArray(cartPolCylSphTest[i][0]);
            float[] expResult = doubleArrayToFloatArray(cartPolCylSphTest[i][1]);
            float[] result = Vectors.cartesianToCylindrical(cartesian);
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of cylindricalToCartesian method, of class Vectors.
     */
    @Test
    public void testCylindricalToCartesian_doubleArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            double[] cylindrical = cartPolCylSphTest[i][1];
            double[] expResult = cartPolCylSphTest[i][0];
            double[] result = Vectors.cylindricalToCartesian(cylindrical);
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of cylindricalToCartesian method, of class Vectors.
     */
    @Test
    public void testCylindricalToCartesian_floatArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            float[] cylindrical = doubleArrayToFloatArray(cartPolCylSphTest[i][1]);
            float[] expResult = doubleArrayToFloatArray(cartPolCylSphTest[i][0]);
            float[] result = Vectors.cylindricalToCartesian(cylindrical);
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of cartesianToSpherical method, of class Vectors.
     */
    @Test
    public void testCartesianToSpherical_doubleArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            double[] cartesian =  cartPolCylSphTest[i][0];
            double[] expResult =  cartPolCylSphTest[i][2];
            double[] result = Vectors.cartesianToSpherical(cartesian);
            Assert.assertEquals( expResult[0], result[0], EPSILON );
            Assert.assertEquals( expResult[1], result[1], EPSILON );
            Assert.assertEquals( expResult[2], result[2], EPSILON );
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of cartesianToSpherical method, of class Vectors.
     */
    @Test
    public void testCartesianToSpherical_floatArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            float[] cartesian =  doubleArrayToFloatArray(cartPolCylSphTest[i][0]);
            float[] expResult =  doubleArrayToFloatArray(cartPolCylSphTest[i][2]);
            float[] result = Vectors.cartesianToSpherical(cartesian);
            Assert.assertEquals( expResult[0], result[0], EPSILON );
            Assert.assertEquals( expResult[1], result[1], EPSILON );
            Assert.assertEquals( expResult[2], result[2], EPSILON );
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of sphericalToCartesian method, of class Vectors.
     */
    @Test
    public void testSphericalToCartesian_doubleArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            double[] spherical =  cartPolCylSphTest[i][2];
            double[] expResult =  cartPolCylSphTest[i][0];
            double[] result = Vectors.sphericalToCartesian(spherical);
            Assert.assertEquals( expResult[0], result[0], EPSILON );
            Assert.assertEquals( expResult[1], result[1], EPSILON );
            Assert.assertEquals( expResult[2], result[2], EPSILON );
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of sphericalToCartesian method, of class Vectors.
     */
    @Test
    public void testSphericalToCartesian_floatArr() {
        for ( int i=0; i<cartPolCylSphTest.length; i++ ) {
            float[] spherical =  doubleArrayToFloatArray(cartPolCylSphTest[i][2]);
            float[] expResult =  doubleArrayToFloatArray(cartPolCylSphTest[i][0]);
            float[] result = Vectors.sphericalToCartesian(spherical);
            Assert.assertEquals( expResult[0], result[0], EPSILON );
            Assert.assertEquals( expResult[1], result[1], EPSILON );
            Assert.assertEquals( expResult[2], result[2], EPSILON );
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

}
