
package science.unlicense.math.impl;


/**
 *
 * @author Johann Sorel
 */
public class Scalari32Test extends AbstractVectorTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{1};
    }

    @Override
    protected Scalari32 create(int dim) {
        return new Scalari32();
    }

}
