
package science.unlicense.math.impl;

import org.junit.Test;
import static science.unlicense.common.api.Assert.*;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.MatrixRW;

/**
 * Abstract affine test.
 * This class works comparing result with DefaultMatrix implementation.
 *
 * @author Johann Sorel
 */
public abstract class AbstractAffineTest {

    private static final double TOLERANCE = 0.0000001;
    private static final String UNVALID_INDEX_EXPECTED = "Accessing value our of affine size must cause an InvalidArgumentException";

    protected abstract int[] getSupportedDimensions();

    /**
     * Created affine must have all values at zero.
     */
    protected abstract AffineRW create(int dim);

    @Test
    public void testAffine(){

        final int[] supportedDimensions = getSupportedDimensions();

        for (int i=0; i<supportedDimensions.length; i++) {
            final int dim = supportedDimensions[i];
            final AffineRW affine = create(dim);
            assertEquals(dim, affine.getInputDimensions());
            assertEquals(dim, affine.getOutputDimensions());
            testAllValue(affine, 0.0);
            testCellGetSet(affine);
            testRowGetSet(affine);
            testColGetSet(affine);
            testToMatrix(affine);
            testMultiply(affine);
        }

    }

    /**
     * Test all affine values equal the expected value.
     */
    private void testAllValue(AffineRW affine, double expectedValue) {
        final int dim = affine.getInputDimensions();

        for (int r=0; r<dim; r++) {
            for (int c=0; c<=dim; c++) {
                double value = affine.get(r, c);
                assertEquals(expectedValue, value, TOLERANCE, "Value different at ["+r+","+c+"]");
            }
        }
    }

    /**
     * Test cell value getters and setters.
     */
    private void testCellGetSet(AffineRW affine) {
        final int dim = affine.getInputDimensions();

        for (int r=0; r<dim; r++) {
            for (int c=0; c<=dim; c++) {
                affine.set(r, c, 15.0);
                double value = affine.get(r, c);
                assertEquals(15.0, value);
            }
        }

        //get value out of range
        for (int r=0; r<dim; r++) {
            //get value out of range
            try {
                affine.get(r, dim+1);
                fail(UNVALID_INDEX_EXPECTED);
            } catch (InvalidArgumentException ex) {
                //ok
            }
            try {
                affine.get(r, -1);
                fail(UNVALID_INDEX_EXPECTED);
            } catch (InvalidArgumentException ex) {
                //ok
            }
        }
        try {
            affine.get(-1, 0);
            fail(UNVALID_INDEX_EXPECTED);
        } catch (InvalidArgumentException ex) {
            //ok
        }
        try {
            affine.get(dim, 0);
            fail(UNVALID_INDEX_EXPECTED);
        } catch (InvalidArgumentException ex) {
            //ok
        }

    }

    /**
     * Test row value getters and setters.
     */
    private void testRowGetSet(AffineRW affine) {
        final int dim = affine.getInputDimensions();

        final double[] expected = new double[dim+1];
        for(int i=0;i<expected.length;i++) expected[i] = i;

        for (int r=0; r<dim; r++) {
            affine.setRow(r, expected);
            double[] row = affine.getRow(r);
            assertArrayEquals(expected, row);
        }

        //get row out of range
        try {
            affine.getRow(-1);
            fail(UNVALID_INDEX_EXPECTED);
        } catch (InvalidArgumentException ex) {
            //ok
        }
        try {
            affine.getRow(dim);
            fail(UNVALID_INDEX_EXPECTED);
        } catch (InvalidArgumentException ex) {
            //ok
        }

        //set unvalid size
        try {
            affine.setRow(0, new double[0]);
            fail(UNVALID_INDEX_EXPECTED);
        } catch (InvalidArgumentException ex) {
            //ok
        }

    }

    /**
     * Test column value getters and setters.
     */
    private void testColGetSet(AffineRW affine) {
        final int dim = affine.getInputDimensions();

        for (int r=0; r<dim; r++) {
            for (int c=0; c<=dim; c++) {
                affine.set(r, c, 15.0);
                double value = affine.get(r, c);
                assertEquals(15.0, value);
            }
        }

        //get col out of range
        try {
            affine.getCol(-1);
            fail(UNVALID_INDEX_EXPECTED);
        } catch (InvalidArgumentException ex) {
            //ok
        }
        try {
            affine.getCol(dim+1);
            fail(UNVALID_INDEX_EXPECTED);
        } catch (InvalidArgumentException ex) {
            //ok
        }

        //set unvalid size
        try {
            affine.setCol(0, new double[0]);
            fail(UNVALID_INDEX_EXPECTED);
        } catch (InvalidArgumentException ex) {
            //ok
        }
    }

    /**
     * Test conversion to matrix.
     */
    private void testToMatrix(AffineRW affine) {
        final int dim = affine.getInputDimensions();
        final MatrixRW reference = MatrixNxN.create(dim+1, dim+1).setToIdentity();
        for (int r=0; r<dim; r++) {
            for (int c=0; c<=dim; c++) {
                affine.set(r, c, r*10+c);
                reference.set(r, c, r*10+c);
            }
        }

        assertEquals(reference, affine.toMatrix());
        assertEquals(reference, affine.toMatrix(null));
        assertEquals(reference, affine.toMatrix(MatrixNxN.create(dim+1, dim+1)));

    }

    /**
     * Test affine multiplication.
     */
    private void testMultiply(AffineRW affine1) {
        final int dim = affine1.getInputDimensions();
        final AffineRW affine2 = create(dim);

        final MatrixRW matrix1 = MatrixNxN.create(dim+1, dim+1).setToIdentity();
        final MatrixRW matrix2 = MatrixNxN.create(dim+1, dim+1).setToIdentity();
        for (int r=0; r<dim; r++) {
            for (int c=0; c<=dim; c++) {
                affine1.set(r, c, 2+r*10+c);
                affine2.set(r, c, 8+-5*r+c*7);
                matrix1.set(r, c, 2+r*10+c);
                matrix2.set(r, c, 8+-5*r+c*7);
            }
        }

        assertEquals(affine1.multiply(affine2).toMatrix(), matrix1.multiply(matrix2));

        affine1.localMultiply(affine2);
        matrix1.localMultiply(matrix2);
        assertEquals(affine1.toMatrix(), matrix1);
    }


    /**
     * Test affine inverse.
     */
    protected void testInverse(AffineRW affine1) {
        final int dim = affine1.getInputDimensions();

        final MatrixRW matrix1 = MatrixNxN.create(dim+1, dim+1).setToIdentity();
        for (int r=0; r<dim; r++) {
            for (int c=0; c<=dim; c++) {
                affine1.set(r, c, 2+r*10+c);
                matrix1.set(r, c, 2+r*10+c);
            }
        }

        MatrixRW av1 = affine1.invert().toMatrix();
        assertEquals(av1, matrix1.invert());
    }
}
