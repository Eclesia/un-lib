
package science.unlicense.math.impl.number;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.math.api.Maths;

/**
 * Complex numbers
 *
 * @author Bertrand COTE
 */
public class ComplexTest {

    final double epsilon = 1e-4;

    Complex zero = new Complex();

    double[][] complexNumbers;
    double[][] complexOperations;
    double[][] complexPow;

    public ComplexTest() {

        //                             { {  re,              im,              modulus,         argument,         reciprocal.re,  reciprocal.im,   sqrt.re,         sqrt.im }, ...}
        complexNumbers = new double[][]{ {  1.,              0.,              1.,              0.,              1.,                0.,                1.,                0.               },
                                         {  0.,              2.,              2.,              Maths.PI/2,       0.,               -0.5,               1.,                1.               },
                                         { -3.,              0.,              3.,              Maths.PI,       -0.333333333333333, 0.,                0.,                1.732050807568877 },
                                         {  0.,             -4.,              4.,             -Maths.PI/2,       0.,                0.25,              1.414213562373095,-1.414213562373095 },
                                         {  7.321232084808, -7.925304873631, 10.789388146675, -0.824997778881,  0.062891295402,  0.068080438400,  3.009204232973, -1.316843966054 },
                                         { -6.229130013414,  0.675610671542,  6.265661218380,  3.033555091265, -0.158669545368, -0.017209279284,  0.135150295905,  2.499479068906 } };

        //                                          a.re            a.im             b.re            b.im            (a+b).re         (a+b).im       (a-b).re       (a-b).im         (a*b).re        (a*b).im          (a/b).re        (a/b).im
        complexOperations = new double[][]{ { -2.906909741773,  7.740241040967, 6.703571038735, -9.290198880465, 3.796661296962, -1.549957839499,-9.610480780507, 17.030439921432,52.421702696161, 78.893025303681,-0.696366300213,  0.189579495912 },
                                            { -7.457783677158,  5.234548073636, 1.147838867869, -1.604060568108,-6.309944809289,  3.630487505529,-8.605622545027,  6.838608641744,-0.163801816015, 17.971154456653,-4.358481617215, -1.530459086668 },
                                            {  0.367331688282,  5.973146915685,-4.279319379998,  9.035768416191,-3.911987691716, 15.008915331877, 4.646651068280, -3.062621500506,-55.543901858572, -22.241879288625, 0.524222249789, -0.288923531567 },
                                            {  3.133217736308,  9.279589037469,-4.214149262903, -9.161126312338,-1.080931526595,  0.118462725131, 7.347366999212, 18.440715349807,71.807640084861, -67.809376748673,-0.965875547673, -0.102293752866 },
                                            { -6.874512004617, -9.895479124472, 9.292331358914,  8.135357038139, 2.417819354297, -1.760122086333,-16.166843363530, -18.030836162610,16.623012263294, -147.878680600330,-0.946581792340, -0.236183815795 } };

       //                                      c                       c⁻⁴                     c³                      c²                      c¹                      c⁰                      c¹                      c²                     c³                       c⁴
        complexPow = new double[][]{ { 1.00000,    0.00000,    1.00000,    0.00000,    1.00000,    0.00000,    1.00000,    0.00000,    1.00000,    0.00000,    1.00000,    0.00000,    1.00000,    0.00000,    1.00000,    0.00000,    1.00000,    0.00000,    1.00000,    0.00000 },
                                     { 0.00000,    1.00000,    1.00000,    0.00000,   -0.00000,    1.00000,   -1.00000,   -0.00000,    0.00000,   -1.00000,    1.00000,    0.00000,    0.00000,    1.00000,   -1.00000,    0.00000,   -0.00000,   -1.00000,    1.00000,    0.00000 },
                                     {-3.00000,    2.00000,   -0.00417,    0.00420,    0.00410,   -0.02094,    0.02959,    0.07101,   -0.23077,   -0.15385,    1.00000,    0.00000,   -3.00000,    2.00000,    5.00000,  -12.00000,    9.00000,   46.00000, -119.00000, -120.00000 },
                                     { 3.00000,   -4.00000,   -0.00135,   -0.00086,   -0.00749,    0.00282,   -0.01120,    0.03840,    0.12000,    0.16000,    1.00000,    0.00000,    3.00000,   -4.00000,   -7.00000,  -24.00000, -117.00000,  -44.00000, -527.00000,  336.00000 },
                                     { 5.00000,    2.00000,    0.00006,   -0.00119,    0.00267,   -0.00582,    0.02497,   -0.02378,    0.17241,   -0.06897,    1.00000,    0.00000,    5.00000,    2.00000,   21.00000,   20.00000,   65.00000,  142.00000,   41.00000,  840.00000 },
                                     {-3.00000,   -4.00000,   -0.00135,    0.00086,    0.00749,    0.00282,   -0.01120,   -0.03840,   -0.12000,    0.16000,    1.00000,    0.00000,   -3.00000,   -4.00000,   -7.00000,   24.00000,  117.00000,  -44.00000, -527.00000, -336.00000 } };
    }

    /**
     * Test of getRe method, of class Complex.
     */
    @Test
    public void testGetRe() {
        for ( double[] cmplx : complexNumbers ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            double expResult = cmplx[0];
            double result = instance.getRe();
            Assert.assertEquals(expResult, result, epsilon);
        }
    }

    /**
     * Test of getIm method, of class Complex.
     */
    @Test
    public void testGetIm() {
        for ( double[] cmplx : complexNumbers ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            double expResult = cmplx[1];
            double result = instance.getIm();
            Assert.assertEquals(expResult, result, epsilon);
        }
    }

    /**
     * Test of modulus method, of class Complex.
     */
    @Test
    public void testAbs() {
        for ( double[] cmplx : complexNumbers ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            double expResult = cmplx[2];
            double result = instance.abs();
            Assert.assertEquals(expResult, result, epsilon);
        }
    }

    /**
     * Test of argument method, of class Complex.
     */
    @Test
    public void testArgument() {
        boolean test = true;
        try {
            zero.argument();
        } catch (ArithmeticException ae) {
            test = false;
        }
        if ( test ) Assert.fail();

        for ( double[] cmplx : complexNumbers ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            double expResult = cmplx[3];
            double result = instance.argument();
            Assert.assertEquals(expResult, result, epsilon);
        }
    }

    /**
     * Test of conjugate method, of class Complex.
     */
    @Test
    public void testConjugate() {
        for ( double[] cmplx : complexNumbers ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            Complex result = instance.conjugate();
            Assert.assertEquals(instance.getRe(), result.getRe(), epsilon);
            Assert.assertEquals(instance.getIm(), -result.getIm(), epsilon);
        }
    }

    /**
     * Test of reciprocal method, of class Complex.
     */
    @Test
    public void testReciprocal() {
        boolean test = true;
        try {
            zero.reciprocal();
        } catch (ArithmeticException ae) {
            test = false;
        }
        if ( test ) Assert.fail();

        for ( double[] cmplx : complexNumbers ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            Complex result = instance.reciprocal();
            Assert.assertEquals(result.getRe(), cmplx[4], epsilon);
            Assert.assertEquals(result.getIm(), cmplx[5], epsilon);
        }
    }

    /**
     * Test of add method, of class Complex.
     */
    @Test
    public void testAdd_Complex() {
        for ( double[] cmplx : complexOperations ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            Complex   other = new Complex(cmplx[2], cmplx[3]);
            Complex result = instance.add(other);
            Assert.assertEquals(result.getRe(), cmplx[4], epsilon);
            Assert.assertEquals(result.getIm(), cmplx[5], epsilon);
        }
    }

    /**
     * Test of add method, of class Complex.
     */
    @Test
    public void testAdd_double() {
        for ( double[] cmplx : complexOperations ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            Complex result = instance.add(cmplx[2]);
            Assert.assertEquals(result.getRe(), cmplx[4], epsilon);
            Assert.assertEquals(result.getIm(), cmplx[1], epsilon);
        }
    }

    /**
     * Test of subtract method, of class Complex.
     */
    @Test
    public void testSubtract_Complex() {
        for ( double[] cmplx : complexOperations ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            Complex   other = new Complex(cmplx[2], cmplx[3]);
            Complex result = instance.subtract(other);
            Assert.assertEquals(result.getRe(), cmplx[6], epsilon);
            Assert.assertEquals(result.getIm(), cmplx[7], epsilon);
        }
    }

    /**
     * Test of subtract method, of class Complex.
     */
    @Test
    public void testSubtract_double() {
        for ( double[] cmplx : complexOperations ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            Complex result = instance.subtract(cmplx[2]);
            Assert.assertEquals(result.getRe(), cmplx[6], epsilon);
            Assert.assertEquals(result.getIm(), cmplx[1], epsilon);
        }
    }

    /**
     * Test of mult method, of class Complex.
     */
    @Test
    public void testMult_Complex() {
        for ( double[] cmplx : complexOperations ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            Complex   other = new Complex(cmplx[2], cmplx[3]);
            Complex result = instance.mult(other);
            Assert.assertEquals(result.getRe(), cmplx[8], epsilon);
            Assert.assertEquals(result.getIm(), cmplx[9], epsilon);
        }
    }

    /**
     * Test of mult method, of class Complex.
     */
    @Test
    public void testMult_double() {
        for ( double[] cmplx : complexOperations ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            Complex result = instance.mult(cmplx[2]);
            Assert.assertEquals(result.getRe(), cmplx[0]*cmplx[2], epsilon);
            Assert.assertEquals(result.getIm(), cmplx[1]*cmplx[2], epsilon);
        }
    }

    /**
     * Test of divide method, of class Complex.
     */
    @Test
    public void testDivide_Complex() {
        Complex.resetEpsilon();
        for ( double[] cmplx : complexOperations ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            Complex    other = new Complex(cmplx[2], cmplx[3]);
            Complex result = instance.divide(other);
            Assert.assertEquals(result.getRe(), cmplx[10], epsilon);
            Assert.assertEquals(result.getIm(), cmplx[11], epsilon);

            boolean test = true;
            try {
                instance.divide(zero);
            } catch (InvalidArgumentException iae) {
                test = false;
            }
            if ( test ) Assert.fail();
        }
    }

    /**
     * Test of divide method, of class Complex.
     */
    @Test
    public void testDivide_double() {
        for ( double[] cmplx : complexOperations ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            Complex result = instance.divide(cmplx[2]);
            Assert.assertEquals(result.getRe(), cmplx[0]/cmplx[2], epsilon);
            Assert.assertEquals(result.getIm(), cmplx[1]/cmplx[2], epsilon);
        }
    }

    /**
     * Test of sqrt method, of class Complex.
     */
    @Test
    public void testSqrt() {
        for ( double[] cmplx : complexNumbers ) {
            Complex instance = new Complex(cmplx[0], cmplx[1]);
            Complex result = instance.sqrt();
            Assert.assertEquals( cmplx[6], result.getRe(), epsilon );
            Assert.assertEquals( cmplx[7], result.getIm(), epsilon );
        }
    }

    /**
     * Test of toMAtrix method, of class Complex.
     */
    @Test
    public void testToMAtrix() {
        /*Complex instance = null;
        Matrix expResult = null;
        Matrix result = instance.toMAtrix();
        Assert.assertEquals(expResult, result);
        */
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Complex.
     */
    @Test
    public void testToString() {
    }

    /**
     * Test of zero method, of class Complex.
     */
    @Test
    public void testZero() {
        Complex instance = new Complex();
        Complex expResult = new Complex(0., 0.);
        Complex result = instance.zero();
        Assert.assertEquals(expResult.getRe(), result.getRe(), epsilon);
        Assert.assertEquals(expResult.getIm(), result.getIm(), epsilon);
    }

    /**
     * Test of one method, of class Complex.
     */
    @Test
    public void testOne() {
        Complex instance = new Complex();
        Complex expResult = new Complex(1., 0.);
        Complex result = instance.one();
        Assert.assertEquals(expResult.getRe(), result.getRe(), epsilon);
        Assert.assertEquals(expResult.getIm(), result.getIm(), epsilon);
    }

    /**
     * Test of pow method, of class Complex.
     */
    @Test
    public void testPow() {
        Complex.resetEpsilon();
        Complex a = new Complex( 0., 0. );
        boolean t = false;
        try {
            a.pow(-1);
        } catch ( InvalidArgumentException iae ) {
            t = true;
        }
        if ( !t ) { // here an exception must de thrown
            Assert.fail("pow fail");
        }

        a = new Complex( 2., 3. );
        t = false;
        try {
            a.pow(-1);
        } catch ( InvalidArgumentException iae ) {
            t = true;
        }
        if ( t ) { // here an exception mustn't de thrown
            Assert.fail("pow fail");
        }

        Complex.setEpsilon(0.1);
        a = new Complex( 0.02, 0.03 );
        t = false;
        try {
            a.pow(-1);
        } catch ( InvalidArgumentException iae ) {
            t = true;
        }
        if ( !t ) { // here an exception must de thrown
            Assert.fail("pow fail");
        }

        a = new Complex( 2., 3. );
        t = false;
        try {
            a.pow(-1);
        } catch ( InvalidArgumentException iae ) {
            t = true;
        }
        if ( t ) { // here an exception mustn't de thrown
            Assert.fail("pow fail");
        }

        for ( double[] test : complexPow) {
            Complex instance = new Complex( test[0], test[1] );
            for ( int i=2; i<test.length; i+=2 ) {
                Complex expResult = new Complex(test[i], test[i+1]);
                Complex result = null;
                boolean testEcep = false;
                int n = i/2 - 5;
                try {

                    result = instance.pow(n);
                } catch ( InvalidArgumentException iae ) {
                    testEcep = true;
                }
                if ( testEcep ) {
                    // Exception thrown
                    if ( instance.isZero() && n<0 ) {
                        // OK: Exception trown
                    } else {
                        Assert.fail("InvalidArgumentException not thrown in pow(n) with n<0 and zero is element of this");
                    }

                } else {
                    // Exception not thrown
                    if ( !instance.isZero()|| n>=0) {
                        Assert.assertEquals(expResult.getRe(), result.getRe(), epsilon);
                        Assert.assertEquals(expResult.getIm(), result.getIm(), epsilon);
                    } else {
                        // never reached
                        Assert.fail("this line should never be reached");
                    }
                }
            }
        }
    }

    /**
     * Test of isZero method, of class Complex.
     */
    @Test
    public void testIsZero() {
        Complex.resetEpsilon();
        Complex instance = (new Complex()).zero();
        boolean expResult = true;
        boolean result = instance.isZero();
        Assert.assertEquals(expResult, result);

        instance = instance.one();
        expResult = false;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);

        Complex.resetEpsilon();
        instance = (new Complex(5., 7.));
        expResult = false;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(1.);
        expResult = false;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(10.);
        expResult = true;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);

        Complex.resetEpsilon();
        instance = (new Complex(5., -7.));
        expResult = false;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(1.);
        expResult = false;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(10.);
        expResult = true;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);

        Complex.resetEpsilon();
        instance = (new Complex(-5., -7.));
        expResult = false;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(1.);
        expResult = false;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(10.);
        expResult = true;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);

        Complex.resetEpsilon();
        instance = (new Complex(-5., 7.));
        expResult = false;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(1.);
        expResult = false;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(10.);
        expResult = true;
        result = instance.isZero();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of isOne method, of class Complex.
     */
    @Test
    public void testIsOne() {
        Complex.resetEpsilon();
        Complex instance = (new Complex()).one();
        boolean expResult = true;
        boolean result = instance.isOne();
        Assert.assertEquals(expResult, result);

        instance = instance.zero();
        expResult = false;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);

        Complex.resetEpsilon();
        instance = (new Complex(5., 7.));
        expResult = false;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(1.);
        expResult = false;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(10.);
        expResult = true;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);

        Complex.resetEpsilon();
        instance = (new Complex(5., -7.));
        expResult = false;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(1.);
        expResult = false;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(10.);
        expResult = true;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);

        Complex.resetEpsilon();
        instance = (new Complex(-5., -7.));
        expResult = false;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(1.);
        expResult = false;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(10.);
        expResult = true;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);

        Complex.resetEpsilon();
        instance = (new Complex(-5., 7.));
        expResult = false;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(1.);
        expResult = false;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);
        Complex.setEpsilon(10.);
        expResult = true;
        result = instance.isOne();
        Assert.assertEquals(expResult, result);
    }

}
