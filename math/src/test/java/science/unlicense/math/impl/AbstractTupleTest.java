
package science.unlicense.math.impl;

import org.junit.Test;
import static science.unlicense.common.api.Assert.*;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractTupleTest {

    private static final double TOLERANCE = 0.0000001;
    private static final String UNVALID_INDEX_EXPECTED = "Accessing value our of tuple size must cause an InvalidIndexException";

    protected abstract int[] getSupportedDimensions();

    /**
     * Created tuple must have all values at zero.
     */
    protected abstract TupleRW create(int dim);

    @Test
    public void testTuple(){

        final int[] supportedDimensions = getSupportedDimensions();

        for (int i=0; i<supportedDimensions.length; i++) {
            final int dim = supportedDimensions[i];
            final TupleRW tuple = create(dim);
            assertEquals(dim, tuple.getSampleCount());
            testAllValue(tuple, 0.0);
            assertTrue(tuple.isAll(0.0));
            testCellGetSet(tuple);
            testEquality(tuple);
        }

    }

    /**
     * Test all tuple values equal the expected value.
     */
    private void testAllValue(TupleRW tuple, double expectedValue) {
        final int dim = tuple.getSampleCount();

        for (int c=0; c<dim; c++) {
            double value = tuple.get(c);
            assertEquals(expectedValue, value, TOLERANCE, "Value different at ["+c+"]");
        }

        assertTrue(tuple.isAll(expectedValue));
        assertFalse(tuple.isAll(expectedValue+1));
    }

    /**
     * Test tuple value getters and setters.
     */
    private void testCellGetSet(TupleRW tuple) {
        final int dim = tuple.getSampleCount();

        for (int i=0; i<dim; i++) {
            tuple.set(i, i+1);
            assertEquals(i+1, tuple.get(i), TOLERANCE);
        }

        //test shortcuts
        //TODO

        //test out of range
        try {
            tuple.set(-1, 10);
            fail(UNVALID_INDEX_EXPECTED);
        } catch (InvalidIndexException ex) {
            //ok
        }
        try {
            tuple.set(dim, 10);
            fail(UNVALID_INDEX_EXPECTED);
        } catch (InvalidIndexException ex) {
            //ok
        }

    }

    private void testEquality(TupleRW tuple) {
        for (int i=0;i<tuple.getSampleCount();i++) {
            tuple.set(i, i+1);
        }

        //test equals itself
        assertEquals(tuple, tuple);

        //test equals a copy
        TupleRW copy = tuple.copy();
        assertEquals(tuple, copy);

        //test a equals a newly created tuple
        TupleRW newtuple = create(tuple.getSampleCount());
        assertFalse(newtuple.equals(tuple));
        newtuple.set(tuple);
        assertEquals(tuple, copy);

        if (tuple.getNumericType() == Float32.TYPE
         || tuple.getNumericType() == Float64.TYPE) {
            //test NaN equality
            copy.set(0, Double.NaN);
            assertFalse(copy.equals(tuple));
            tuple.set(0, Double.NaN);
            assertEquals(copy, tuple);
        }

    }

}
