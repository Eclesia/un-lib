
package science.unlicense.math.impl.util;

import org.junit.Test;
import static science.unlicense.common.api.Assert.*;

/**
 *
 * @author Johann Sorel
 */
public class LinearTransformDetectorTest {

    private static final double DELTA = 0.0;

    @Test
    public void testConstant() {

        double[] values = new double[]{1,1,1,1,1};
                                    // 0 1 2 3 4

        final LinearTransformDetector detector = new LinearTransformDetector(1.0);
        final LinearTransformDetector.Range1S[] ranges = detector.detect(values, 0, values.length);
        assertEquals(1,ranges.length);

        LinearTransformDetector.Range1S range;
        range = ranges[0];
        assertEquals(0, range.start);
        assertEquals(5, range.end);
        assertEquals(0.0, range.a, DELTA);
        assertEquals(1.0, range.b, DELTA);

    }

    @Test
    public void testUpward() {

        double[] values = new double[]{-3,-2,-1, 0,+1,+2};
                                     // 0  1  2  3  4  5

        final LinearTransformDetector detector = new LinearTransformDetector(1.0);
        final LinearTransformDetector.Range1S[] ranges = detector.detect(values, 0, values.length);
        assertEquals(1,ranges.length);

        LinearTransformDetector.Range1S range;
        range = ranges[0];
        assertEquals(0, range.start);
        assertEquals(6, range.end);
        assertEquals(1.0, range.a, DELTA);
        assertEquals(-3.0, range.b, DELTA);

    }

    @Test
    public void testMultiple() {

        double[] values = new double[]{1,1,1,1,2,3,4,5,6,7,5,3,1};
                                    // 0 1 2 3 4 5 6 7 8 9 0 1 2

        final LinearTransformDetector detector = new LinearTransformDetector(1.0);
        final LinearTransformDetector.Range1S[] ranges = detector.detect(values, 0, values.length);
        assertEquals(3,ranges.length);

        LinearTransformDetector.Range1S range;
        range = ranges[0];
        assertEquals(0, range.start);
        assertEquals(4, range.end);
        assertEquals(0.0, range.a, DELTA);
        assertEquals(1.0, range.b, DELTA);

        range = ranges[1];
        assertEquals(4, range.start);
        assertEquals(9, range.end);
        assertEquals(1.0, range.a, DELTA);
        assertEquals(2.0, range.b, DELTA);

        range = ranges[2];
        assertEquals(9, range.start);
        assertEquals(13, range.end);
        assertEquals(-2.0, range.a, DELTA);
        assertEquals(7.0, range.b, DELTA);

    }

}
