
package science.unlicense.math.impl;


/**
 *
 * @author Johann Sorel
 */
public class Scalarf64Test extends AbstractVectorTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{1};
    }

    @Override
    protected Scalarf64 create(int dim) {
        return new Scalarf64();
    }

}
