

package science.unlicense.math.impl.transform;

import science.unlicense.math.api.Similarity;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.impl.SimilarityNd;
import science.unlicense.math.impl.Matrix2x2;

/**
 *
 * @author Johann Sorel
 */
public class NodeTransformTest {

    private static final double DELTA = 0.00000001;

    @Test
    public void testTransformIdentityDouble(){
        Similarity trs = SimilarityNd.create(2);

        final double[] in = {0,0, 1,0, 1,1};
        double[] out = new double[8];
        trs.transform(in, 0, out, 2, 3);
        Assert.assertArrayEquals(new double[]{0,0, 0,0, 1,0, 1,1}, out, DELTA);

        out = new double[8];
        trs.invert().transform(in, 0, out, 2, 3);
        Assert.assertArrayEquals(new double[]{0,0, 0,0, 1,0, 1,1}, out, DELTA);

    }

    @Test
    public void testTransformDouble(){
        SimilarityRW trs = SimilarityNd.create(2);
        trs.getTranslation().setXY(1, 2);
        trs.getScale().setXY(3, 5);
        trs.notifyChanged();

        final double[] in = {0,0, 1,0, 1,1};
        double[] out = new double[8];
        trs.transform(in, 0, out, 2, 3);
        Assert.assertArrayEquals(new double[]{0,0, 1,2, 4,2, 4,7}, out, DELTA);

        out = new double[8];
        trs.invert().transform(in, 0, out, 2, 3);
        Assert.assertArrayEquals(new double[]{0,0,-1.0/3.0, -2.0/5.0, 0, -2.0/5.0, 0, -0.2}, out, DELTA);

    }

    @Test
    public void testRotation(){
        SimilarityRW trs = SimilarityNd.create(2);

        Matrix2x2 rotation = Matrix2x2.fromAngle(60);
        trs.getRotation().set(rotation);
        trs.notifyChanged();

        System.out.println(trs.viewMatrix());

        Assert.assertEquals(rotation,trs.getRotation());


    }

}
