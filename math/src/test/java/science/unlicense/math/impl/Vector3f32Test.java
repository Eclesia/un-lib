
package science.unlicense.math.impl;


/**
 *
 * @author Johann Sorel
 */
public class Vector3f32Test extends AbstractVectorTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{3};
    }

    @Override
    protected Vector3f32 create(int dim) {
        return new Vector3f32();
    }

}
