package science.unlicense.math.impl;

import org.junit.Test;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;


/**
 *
 * @author Johann Sorel
 */
public class MatrixTest {

    private static final double DELTA = 0.000000001;

    @Test
    public void testIsIdentity(){
        MatrixNxN m = new MatrixNxN(
                1,0,0,0,
                0,1,0,0,
                0,0,1,0,
                0,0,0,1);
        Assert.assertTrue(m.isIdentity());

        m = new MatrixNxN(
                1,0,0,0,
                0,1,0,2,
                0,0,-1,0,
                0,0,0,1);
        Assert.assertFalse(m.isIdentity());
    }

    @Test
    public void testAdd(){

        final MatrixNxN expected = new MatrixNxN(
                11,22,33,44,
                0,55,66,77,
                0,0,88,99,
                0,0,0,0);

        MatrixNxN m1 = new MatrixNxN(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,0);
        MatrixNxN m2 = new MatrixNxN(
                10,20,30,40,
                0,50,60,70,
                0,0,80,90,
                0,0,0,0);
        Matrix res = m1.add(m2,null);

        Assert.assertEquals(expected, res);
    }

    @Test
    public void testAddArrayCreation(){

        final double[][] m1 = new double[][]{
            {1,2,3,4},
            {0,5,6,7},
            {0,0,8,9}};

        final double[][] m2 = new double[][]{
            {10,20,30,40},
            {0,50,60,70},
            {0,0,80,90}};

        final double[][] expected = new double[][]{
            {11,22,33,44},
            {0,55,66,77},
            {0,0,88,99}};

        double[][] res = Matrices.add(m1, m2, null);

        Assert.assertArrayEquals(expected, res);
    }

    @Test
    public void testAddLocal(){

        final MatrixNxN expected = new MatrixNxN(
                11,22,33,44,
                0,55,66,77,
                0,0,88,99,
                0,0,0,0);

        MatrixNxN m1 = new MatrixNxN(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,0);
        MatrixNxN m2 = new MatrixNxN(
                10,20,30,40,
                0,50,60,70,
                0,0,80,90,
                0,0,0,0);
        m1.localAdd(m2);

        Assert.assertEquals(expected, m1);
    }

    @Test
    public void testSubtract(){

        final MatrixNxN expected = new MatrixNxN(
                 9,18,27,36,
                0,45,54,63,
                0,0,72,81,
                0,0,0,0);

        MatrixNxN m1 = new MatrixNxN(
                10,20,30,40,
                0,50,60,70,
                0,0,80,90,
                0,0,0,0);
        MatrixNxN m2 = new MatrixNxN(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,0);
        Matrix res = m1.subtract(m2, null);

        Assert.assertEquals(expected, res);
    }

    @Test
    public void testSubtractLocal(){

        final MatrixNxN expected = new MatrixNxN(
                 9,18,27,36,
                0,45,54,63,
                0,0,72,81,
                0,0,0,0);

        MatrixNxN m1 = new MatrixNxN(
                10,20,30,40,
                0,50,60,70,
                0,0,80,90,
                0,0,0,0);
        MatrixNxN m2 = new MatrixNxN(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,0);
        m1.localSubtract(m2);

        Assert.assertEquals(expected, m1);
    }

    @Test
    public void testScale(){

        final MatrixNxN expected = new MatrixNxN(
                2,4,6,8,
                0,10,12,14,
                0,0,16,18,
                0,0,0,20);

        MatrixNxN m1 = new MatrixNxN(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,10);
        Matrix res = m1.scale(2, null);

        Assert.assertEquals(expected, res);
    }

    @Test
    public void testScaleLocal(){

        final MatrixNxN expected = new MatrixNxN(
                2,4,6,8,
                0,10,12,14,
                0,0,16,18,
                0,0,0,20);

        MatrixNxN m1 = new MatrixNxN(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,10);
        m1.localScale(2);

        Assert.assertEquals(expected, m1);
    }

    @Test
    public void testTranspose(){

        final MatrixNxN expected = new MatrixNxN(4,3);
        expected.set(new double[]{
                        1,0,0,
                        2,5,0,
                        3,6,8,
                        4,7,9 });

        MatrixNxN m1 = new MatrixNxN(3,4);
        m1.set(new double[]{
                1,2,3,4,
                0,5,6,7,
                0,0,8,9});
        MatrixNxN res = m1.transpose();

        Assert.assertEquals(expected, res);
    }

    @Test
    public void testVectorTransform(){

        final Vector4f64 v = new Vector4f64(1,2,3,1);
        final MatrixNxN m = new MatrixNxN(4,4);
        final Vector4f64 res = new Vector4f64();


        //identity test
        m.set(new double[]{
                1,0,0,0,
                0,1,0,0,
                0,0,1,0,
                0,0,0,1});
        m.transform(v, res);
        Assert.assertEquals(1, res.get(0), DELTA);
        Assert.assertEquals(2, res.get(1), DELTA);
        Assert.assertEquals(3, res.get(2), DELTA);

        //translation test
        m.set(new double[]{
                1,0,0,50,
                0,1,0,60,
                0,0,1,70,
                0,0,0,1});
        m.transform(v, res);
        Assert.assertEquals(51, res.get(0), DELTA);
        Assert.assertEquals(62, res.get(1), DELTA);
        Assert.assertEquals(73, res.get(2), DELTA);

        //scale test
        m.set(new double[]{
                10,0,0,0,
                0,100,0,0,
                0,0,1000,0,
                0,0,0,1});
        m.transform(v, res);
        Assert.assertEquals(10, res.get(0), DELTA);
        Assert.assertEquals(200, res.get(1), DELTA);
        Assert.assertEquals(3000, res.get(2), DELTA);

    }

    @Test
    public void testMultiply(){
        final MatrixNxN expected = new MatrixNxN(2,2);
        expected.set(new double[]{
                        9, 7,
                        23, 9
                    });

        final MatrixNxN m1 = new MatrixNxN(2,3);
        m1.set(new double[]{
                1, 2, 0,
                4, 3,-1
                });
        final MatrixNxN m2 = new MatrixNxN(3,2);
        m2.set(new double[]{
                5,1,
                2,3,
                3,4
                });

        Matrix res = m1.multiply(m2, null);

        Assert.assertEquals(expected, res);
    }

    @Test
    public void testRotationMatrix(){
        final double angle = 12.37;
        double[][] res;

        double[][] expected;
        expected = new double[][]{
        {1,    0,                  0,                  0},
        {0,    Math.cos(angle),    -Math.sin(angle),   0},
        {0,    Math.sin(angle),    Math.cos(angle),    0},
        {0,    0,                  0,                  1}
        };

        res = Matrices.createRotation4(angle, new Vector3f64(1, 0, 0),null);
        Assert.assertEquals(new MatrixNxN(expected), new MatrixNxN(res));

        expected = new double[][]{
            {Math.cos(angle),  0,    Math.sin(angle),    0},
            {0,                1,    0,                  0},
            {-Math.sin(angle), 0,    Math.cos(angle),    0},
            {0,                0,    0,                  1}
        };

        res = Matrices.createRotation4(angle, new Vector3f64(0, 1, 0),null);
        Assert.assertEquals(new MatrixNxN(expected), new MatrixNxN(res));

        expected = new double[][]{
          {Math.cos(angle),  -Math.sin(angle),   0,  0},
          {Math.sin(angle),  Math.cos(angle),    0,  0},
          {0,                0,                  1,  0},
          {0,                0,                  0,  1}
        };
        res = Matrices.createRotation4(angle, new Vector3f64(0, 0, 1),null);
        Assert.assertEquals(new MatrixNxN(expected), new MatrixNxN(res));

    }

    @Test
    public void testTransform2(){
        final double angle = Math.toRadians(90);

        MatrixNxN matrix = new MatrixNxN(Matrices.createRotation4(angle, new Vector3f64(0, 1, 0), null));


        final Vector4f64 v = new Vector4f64(0, 0, 1, 0);

        matrix.transform(v,v);

        Assert.assertEquals(4, v.getSampleCount());
        Assert.assertEquals(1, v.get(0), DELTA);
        Assert.assertEquals(0, v.get(1), DELTA);
        Assert.assertEquals(0, v.get(2), DELTA);
        Assert.assertEquals(0, v.get(2), DELTA);

    }

    @Test
    public void testFromEuler(){
        final TupleRW euler = VectorNf64.createDouble(3);
        final Matrix3x3 rotation = new Matrix3x3();

        //(0, 0, 0) test identity
        euler.setAll(0.0);
        rotation.fromEuler(euler);
        Assert.assertTrue(
                Arrays.equals(
                new double[][]{
                    {+1,  0,  0},
                    { 0, +1,  0},
                    { 0,  0, +1}},
                rotation.getValuesCopy(),
                DELTA)
                );

        // 90° around X
        euler.set(new double[]{0, 0, Angles.degreeToRadian(90)});
        rotation.fromEuler(euler);
        Assert.assertTrue(
                Arrays.equals(
                new double[][]{
                    {+1,  0,  0},
                    { 0,  0, -1},
                    { 0, +1,  0}},
                rotation.getValuesCopy(),
                DELTA)
                );

        // 90° around Y
        euler.set(new double[]{0, Angles.degreeToRadian(90), 0});
        rotation.fromEuler(euler);
        Assert.assertTrue(
                Arrays.equals(
                new double[][]{
                    { 0,  0, +1},
                    { 0, +1,  0},
                    {-1,  0,  0}},
                rotation.getValuesCopy(),
                DELTA)
                );

        // -90° around Z
        euler.set(new double[]{Angles.degreeToRadian(-90), 0, 0});
        rotation.fromEuler(euler);
        Assert.assertTrue(
                Arrays.equals(
                new double[][]{
                    { 0, +1,  0},
                    {-1,  0,  0},
                    { 0,  0, +1}},
                rotation.getValuesCopy(),
                DELTA)
                );

        // 30° around X, 55° around Z
        euler.set(new double[]{Angles.degreeToRadian(55), 0, Angles.degreeToRadian(30)});
        rotation.fromEuler(euler);
        Assert.assertTrue(
                Arrays.equals(
                    Matrix3x3.createRotation3(
                        Angles.degreeToRadian(55), new Vector3f64(0,0,1))
                    .multiply(
                        Matrix3x3.createRotation3(
                            Angles.degreeToRadian(30), new Vector3f64(1,0,0))
                    ).getValuesCopy(),
                    rotation.getValuesCopy(),
                    DELTA)
                );

    }

    @Test
    public void testFromAndToEuler(){
        Vector3f64 euler;
        VectorNf64 res;

        euler = new Vector3f64(0, 0, 0);
        res = Matrix3x3.createRotationEuler(euler).toEuler();
        Assert.assertArrayEquals(euler.toDouble(), res.toDouble(), DELTA);

        //test full range on each axis YAW -180/+180
        for (int i=-180;i<=180;i++){
            euler = new Vector3f64(tr(i),0,0);
            res = Matrix3x3.createRotationEuler(euler).toEuler();
            Assert.assertArrayEquals(euler.toDouble(), res.toDouble(), DELTA, "fail for "+euler);
        }

        //PITCH -90/+90
        for (int i=-90;i<=90;i++){
            euler = new Vector3f64(0,tr(i),0);
            res = Matrix3x3.createRotationEuler(euler).toEuler();
            Assert.assertArrayEquals(euler.toDouble(), res.toDouble(), DELTA, "fail for "+euler);
        }

        //ROLL -180/+180
        for (int i=-180;i<=180;i++){
            euler = new Vector3f64(0,0,tr(i));
            res = Matrix3x3.createRotationEuler(euler).toEuler();
            Assert.assertArrayEquals(euler.toDouble(), res.toDouble(), DELTA, "fail for "+euler);
        }

    }

    @Test
    public void testRotationVectorToVector(){

        Vector3f64 v1 = new Vector3f64(1, 0, 0);
        Vector3f64 v2 = new Vector3f64(0, 0, 1);

        Matrix3x3 m = Matrix3x3.createRotation(v1, v2);

        Tuple res = m.transform(v1);
        Assert.assertArrayEquals(res.toDouble(), v2.toDouble(), DELTA);

        //test colinear vectors, we must obtain an identity matrix
        v1 = new Vector3f64(0, 1, 0);
        v2 = new Vector3f64(0, 5, 0);

        m = Matrix3x3.createRotation(v1, v2);
        Assert.assertTrue(m.isIdentity());

    }

    private static double tr(double value){
        return Angles.degreeToRadian(value);
    }
}
