
package science.unlicense.math.impl;


/**
 *
 * @author Johann Sorel
 */
public class Vector3i32Test extends AbstractVectorTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{3};
    }

    @Override
    protected Vector3i32 create(int dim) {
        return new Vector3i32();
    }

}
