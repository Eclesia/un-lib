
package science.unlicense.math.impl;

import org.junit.Test;
import science.unlicense.math.api.AffineRW;

/**
 * Test Affine1.
 *
 * @author Johann Sorel
 */
public class Affine1Test extends AbstractAffineTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{1};
    }

    @Override
    protected AffineRW create(int dim) {
        return new Affine1(0,0);
    }

    @Test
    public void testInverse() {
        testInverse(new Affine1(5,-7));
    }
}
