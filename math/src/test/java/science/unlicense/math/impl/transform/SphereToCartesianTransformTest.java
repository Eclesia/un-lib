
package science.unlicense.math.impl.transform;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class SphereToCartesianTransformTest {

    private static final double DELTA = 0.000000001;

    @Test
    public void testTransform() {

        final Vector2f64 spheric = new Vector2f64();
        final Vector3f64 cartesian = new Vector3f64();
        final SphereToCartesianTransform trs = new SphereToCartesianTransform(1);

        spheric.setXY(0, 0);
        trs.transform(spheric, cartesian);
        Assert.assertArrayEquals(new Vector3f64(1, 0, 0).toDouble(), cartesian.toDouble(), DELTA);

        spheric.setXY(Maths.HALF_PI, 0);
        trs.transform(spheric, cartesian);
        Assert.assertArrayEquals(new Vector3f64(0, 1, 0).toDouble(), cartesian.toDouble(), DELTA);

        spheric.setXY(-Maths.HALF_PI, 0);
        trs.transform(spheric, cartesian);
        Assert.assertArrayEquals(new Vector3f64(0, -1, 0).toDouble(), cartesian.toDouble(), DELTA);

        spheric.setXY(Maths.PI, 0);
        trs.transform(spheric, cartesian);
        Assert.assertArrayEquals(new Vector3f64(-1, 0, 0).toDouble(), cartesian.toDouble(), DELTA);

        spheric.setXY(0, Maths.HALF_PI);
        trs.transform(spheric, cartesian);
        Assert.assertArrayEquals(new Vector3f64(0, 0, 1).toDouble(), cartesian.toDouble(), DELTA);

        spheric.setXY(0, -Maths.HALF_PI);
        trs.transform(spheric, cartesian);
        Assert.assertArrayEquals(new Vector3f64(0, 0, -1).toDouble(), cartesian.toDouble(), DELTA);

        spheric.setXY(Maths.QUATER_PI, 0);
        trs.transform(spheric, cartesian);
        Assert.assertArrayEquals(new Vector3f64(Angles.degreeToRadian(45.0), Angles.degreeToRadian(45.0), 0).localNormalize().toDouble(), cartesian.toDouble(), DELTA);

        spheric.setXY(-Maths.QUATER_PI, 0);
        trs.transform(spheric, cartesian);
        Assert.assertArrayEquals(new Vector3f64(Angles.degreeToRadian(45.0), -Angles.degreeToRadian(45.0), 0).localNormalize().toDouble(), cartesian.toDouble(), DELTA);

        spheric.setXY(Maths.QUATER_PI+Maths.HALF_PI, 0);
        trs.transform(spheric, cartesian);
        Assert.assertArrayEquals(new Vector3f64(-Angles.degreeToRadian(45.0), Angles.degreeToRadian(45.0), 0).localNormalize().toDouble(), cartesian.toDouble(), DELTA);

    }
}
