
package science.unlicense.math.impl;


/**
 *
 * @author Johann Sorel
 */
public class Vector2f64Test extends AbstractVectorTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{2};
    }

    @Override
    protected Vector2f64 create(int dim) {
        return new Vector2f64();
    }

}
