
package science.unlicense.math.impl;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.MatrixRW;

/**
 * Test Matrix2 class.
 *
 * @author Johann Sorel
 */
public class Matrix2x2Test extends AbstractMatrixTest{

    private static final double DELTA = 0.0000001;

    @Override
    protected MatrixRW createMatrix(int nbRow, int nbCol) {
        return new Matrix2x2();
    }

    @Test
    public void testFromAngle() {
        Assert.assertArrayEquals(new double[]{ 0,-1, 1, 0}, Matrix2x2.fromAngle(Maths.HALF_PI).toRowPackedCopy(), DELTA);
        Assert.assertArrayEquals(new double[]{-1, 0, 0,-1}, Matrix2x2.fromAngle(Maths.PI).toRowPackedCopy(), DELTA);
        Assert.assertArrayEquals(new double[]{ 0, 1,-1, 0}, Matrix2x2.fromAngle(Maths.PI+Maths.HALF_PI).toRowPackedCopy(), DELTA);
        Assert.assertArrayEquals(new double[]{ 1, 0, 0, 1}, Matrix2x2.fromAngle(Maths.TWO_PI).toRowPackedCopy(), DELTA);
    }

}
