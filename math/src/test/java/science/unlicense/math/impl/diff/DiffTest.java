
package science.unlicense.math.impl.diff;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class DiffTest {

    @Test
    public void testDiff(){

        final Sequence src = new ArraySequence(new Object[]{
            1, 2, 3, 4, 5, 6, 7, 8, 9
        });
        final Sequence dst = new ArraySequence(new Object[]{
            1, 2,-1, 4, 6, 7, 5, 8, 9
        });

        final DiffEngine engine = new DiffEngine();
        final Sequence report = engine.processDiff(src, dst);

        //NoChange src:0 dst:0 lght:2
        //Replace src:2 dst:2 lght:1
        //NoChange src:3 dst:3 lght:1
        //DeleteSource src:4 dst:-1 lght:1
        //NoChange src:5 dst:4 lght:2
        //AddDestination src:-1 dst:6 lght:1
        //NoChange src:7 dst:7 lght:2
        Assert.assertEquals(7,report.getSize());
        final DiffEngine.DiffResultSpan diff0 = (DiffEngine.DiffResultSpan) report.get(0);
        final DiffEngine.DiffResultSpan diff1 = (DiffEngine.DiffResultSpan) report.get(1);
        final DiffEngine.DiffResultSpan diff2 = (DiffEngine.DiffResultSpan) report.get(2);
        final DiffEngine.DiffResultSpan diff3 = (DiffEngine.DiffResultSpan) report.get(3);
        final DiffEngine.DiffResultSpan diff4 = (DiffEngine.DiffResultSpan) report.get(4);
        final DiffEngine.DiffResultSpan diff5 = (DiffEngine.DiffResultSpan) report.get(5);
        final DiffEngine.DiffResultSpan diff6 = (DiffEngine.DiffResultSpan) report.get(6);
        Assert.assertEquals(DiffEngine.DiffResultSpanStatus.NoChange,      diff0.getStatus()); // 1,2
        Assert.assertEquals(DiffEngine.DiffResultSpanStatus.Replace,       diff1.getStatus()); // 3 -> -1
        Assert.assertEquals(DiffEngine.DiffResultSpanStatus.NoChange,      diff2.getStatus()); // 4
        Assert.assertEquals(DiffEngine.DiffResultSpanStatus.DeleteSource,  diff3.getStatus()); // -5
        Assert.assertEquals(DiffEngine.DiffResultSpanStatus.NoChange,      diff4.getStatus()); // 6, 7
        Assert.assertEquals(DiffEngine.DiffResultSpanStatus.AddDestination,diff5.getStatus()); // +5
        Assert.assertEquals(DiffEngine.DiffResultSpanStatus.NoChange,      diff6.getStatus()); // 8, 9

//        for (int i=0,n=report.getSize();i<n;i++){
//            System.out.println(report.get(i));
//        }

    }

}
