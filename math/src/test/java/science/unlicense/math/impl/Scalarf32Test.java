
package science.unlicense.math.impl;


/**
 *
 * @author Johann Sorel
 */
public class Scalarf32Test extends AbstractVectorTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{1};
    }

    @Override
    protected Scalarf32 create(int dim) {
        return new Scalarf32();
    }

}
