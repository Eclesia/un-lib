
package science.unlicense.math.impl;

import org.junit.Test;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Assert;

/**
 * @author Bertrand COTE
 */
public class DefaultTupleTest {

    private static final double[][] tupleDataTest = new double[][] {
        {  0. },
        {  5. },
        {  0.,  0. },
        {  2.,  4. },
        {  0.,  0.,  0. },
        { -5.,  8., -2. },
        {  0.,  0.,  0.,  0. },
        {  3., -9.,  4., -1. },
        {},
    };

    private static final boolean[][] tupleIsTest = new boolean[][] {
        {  true,  true }, // { isZero(), isValid() }
        {  false, true }, // { isZero(), isValid() }
        {  true,  true }, // { isZero(), isValid() }
        {  false, true }, // { isZero(), isValid() }
        {  true,  true }, // { isZero(), isValid() }
        {  false, true }, // { isZero(), isValid() }
        {  true,  true }, // { isZero(), isValid() }
        {  false, true }, // { isZero(), isValid() }
        {  false, true }, // { isZero(), isValid() }
    };

    private static final TupleNf64[] tupleTest;
    static {
        tupleTest = new TupleNf64[tupleDataTest.length];
        for ( int i=0; i<tupleDataTest.length; i++ ) {
            tupleTest[i] = new TupleNf64( tupleDataTest[i] );
        }
    }

    /**
     * Test constructors
     */
    public DefaultTupleTest() {
        TupleNf64 instance;

        for ( int i=0; i<tupleDataTest.length; i++ ) {

            // public DefaultTuple(int size)
            instance = new TupleNf64( tupleDataTest[i].length );
            Assert.assertEquals(tupleDataTest[i].length, instance.toDouble().length);

            if ( tupleDataTest[i].length >=1 ) {
                // public DefaultTuple(double x)
                instance = new TupleNf64( new double[]{tupleDataTest[i][0]} );
                Assert.assertTrue( Arrays.equals(
                        Arrays.copy( tupleDataTest[i], 0, 1 ),
                        instance.toDouble()));
            }
            if ( tupleDataTest[i].length >=2 ) {
                // public DefaultTuple(double x, double y)
                instance = new TupleNf64( new double[]{tupleDataTest[i][0], tupleDataTest[i][1]} );
                Assert.assertTrue( Arrays.equals(
                        Arrays.copy( tupleDataTest[i], 0, 2 ),
                        instance.toDouble()));
            }
            if ( tupleDataTest[i].length >=3 ) {
                // public DefaultTuple(double x, double y, double z)
                instance = new TupleNf64( new double[]{tupleDataTest[i][0], tupleDataTest[i][1], tupleDataTest[i][2]} );
                Assert.assertTrue( Arrays.equals(
                        Arrays.copy( tupleDataTest[i], 0, 3 ),
                        instance.toDouble()));
            }
            if ( tupleDataTest[i].length >=4 ) {
                // public DefaultTuple(double x, double y, double z, double w)
                instance = new TupleNf64( new double[]{tupleDataTest[i][0], tupleDataTest[i][1], tupleDataTest[i][2], tupleDataTest[i][3]} );
                Assert.assertTrue( Arrays.equals(
                        Arrays.copy( tupleDataTest[i], 0, 4 ),
                        instance.toDouble()));
            }

            // public DefaultTuple(final double[] values)
            instance = new TupleNf64( tupleDataTest[i] );
            Assert.assertTrue( Arrays.equals(tupleDataTest[i], instance.toDouble()));

            // public DefaultTuple(final Tuple v)
            instance = new TupleNf64( tupleTest[i] );
            Assert.assertTrue( Arrays.equals(tupleDataTest[i], instance.toDouble()));
        }
    }

    /**
     * Test of getValues method, of class DefaultTuple.
     */
    @Test
    public void testGetValues() {
        for ( int i=0; i<tupleDataTest.length; i++ ) {
            double[] result = tupleTest[i].toDouble();
            double[] expResult = tupleDataTest[i];
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of getSize method, of class DefaultTuple.
     */
    @Test
    public void testGetSize() {
        for ( int i=0; i<tupleDataTest.length; i++ ) {
            int result = tupleTest[i].getSampleCount();
            int expResult = tupleDataTest[i].length;
            Assert.assertEquals( expResult, result);
        }
    }

    /**
     * Test of setToZero method, of class DefaultTuple.
     */
    @Test
    public void testSetToZero() {

        for ( int i=0; i<tupleDataTest.length; i++ ) {
            TupleNf64 instance = new TupleNf64(tupleTest[i]);
            instance.setAll(0.0);
            Assert.assertTrue( Arrays.equals(new double[tupleDataTest[i].length], instance.toDouble()));
        }
    }

    /**
     * Test of isZero method, of class DefaultTuple.
     */
    @Test
    public void testIsZero() {
        for ( int i=0; i<tupleDataTest.length; i++ ) {
            TupleNf64 instance = tupleTest[i];
            boolean result = instance.isAll(0.0);
            boolean expResult = tupleIsTest[i][0];
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of isValid method, of class DefaultTuple.
     */
    @Test
    public void testIsValid() {
        for ( int i=0; i<tupleDataTest.length; i++ ) {
            TupleNf64 instance = tupleTest[i];
            boolean result = instance.isValid();
            boolean expResult = tupleIsTest[i][1];
            Assert.assertEquals(expResult, result);
        }
        {
            TupleNf64 instance = new TupleNf64( new double[]{ 0., 0., 0., Double.NEGATIVE_INFINITY} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            TupleNf64 instance = new TupleNf64( new double[]{ 0., 0., Double.POSITIVE_INFINITY, 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            TupleNf64 instance = new TupleNf64( new double[]{ 0., Double.NaN, 0., 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            TupleNf64 instance = new TupleNf64( new double[]{ Double.NEGATIVE_INFINITY, 0., 0., 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            TupleNf64 instance = new TupleNf64( new double[]{ 0., 0., Double.POSITIVE_INFINITY} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            TupleNf64 instance = new TupleNf64( new double[]{ 0., Double.NaN, 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            TupleNf64 instance = new TupleNf64( new double[]{ Double.NEGATIVE_INFINITY, 0., 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            TupleNf64 instance = new TupleNf64( new double[]{ 0., Double.POSITIVE_INFINITY} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            TupleNf64 instance = new TupleNf64( new double[]{ Double.NaN, 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            TupleNf64 instance = new TupleNf64( new double[]{ Double.NEGATIVE_INFINITY } );
            Assert.assertEquals(false, instance.isValid());
        }
    }

    /**
     * Test of get method, of class DefaultTuple.
     */
    @Test
    public void testGet() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            TupleNf64 instance = tupleTest[i];
            for ( int indice = 0; indice<tupleDataTest[i].length; indice++ ) {
                double result = instance.get(indice);
                double expResult = tupleDataTest[i][indice];
                Assert.assertEquals( expResult, result, 0.);
            }
        }
    }

    /**
     * Test of set method, of class DefaultTuple.
     */
    @Test
    public void testSet_int_double() {

        for (int i = 0; i < tupleDataTest.length; i++) {
            TupleNf64 instance = new TupleNf64( new double[tupleDataTest[i].length] );
            for ( int indice = 0; indice<tupleDataTest[i].length; indice++ ) {
                instance.set(indice, tupleDataTest[i][indice]);
                double expResult = tupleDataTest[i][indice];
                double result = instance.get(indice);
                Assert.assertEquals( expResult, result, 0.);
            }
        }
    }

    /**
     * Test of set method, of class DefaultTuple.
     */
    @Test
    public void testSet_Tuple() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            TupleNf64 instance = new TupleNf64( new double[tupleDataTest[i].length] );
            instance.set( tupleTest[i] );
            Assert.assertTrue( Arrays.equals(tupleDataTest[i], instance.toDouble(), 0.));
        }
    }

    /**
     * Test of set method, of class DefaultTuple.
     */
    @Test
    public void testSet_doubleArr() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            TupleNf64 instance = new TupleNf64( new double[tupleDataTest[i].length] );
            instance.set( tupleDataTest[i] );
            Assert.assertTrue( Arrays.equals(tupleDataTest[i], instance.toDouble(), 0.));
        }
    }

    /**
     * Test of toArrayDouble method, of class DefaultTuple.
     */
    @Test
    public void testToArrayDouble() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            TupleNf64 instance = tupleTest[i];
            double[] result = instance.toDouble();
            double[] expResult = tupleDataTest[i];
            Assert.assertTrue( Arrays.equals(expResult, result, 0.));
        }
    }

    /**
     * Test of toArrayFloat method, of class DefaultTuple.
     */
    @Test
    public void testToArrayFloat() {

        for (int i = 0; i < tupleDataTest.length; i++) {
            TupleNf64 instance = tupleTest[i];
            float[] result = instance.toFloat();
            float[] expResult = new float[tupleDataTest[i].length];
            for ( int indice=0; indice<tupleDataTest[i].length; indice++ ) expResult[indice] = (float) tupleDataTest[i][indice];
            Assert.assertTrue( Arrays.equals(expResult, result, 0.f));
        }
    }

    /**
     * Test of copy method, of class DefaultTuple.
     */
    @Test
    public void testCopy() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            TupleNf64 instance = tupleTest[i];
            double[] result = instance.copy().toDouble();
            double[] expResult = tupleDataTest[i];
            instance.set( tupleDataTest[i] );
            Assert.assertTrue( Arrays.equals(expResult, result, 0.));
        }
    }

    /**
     * Test of extend method, of class DefaultTuple.
     */
    @Test
    public void testExtend() {

        final double value = -25.36;

        for ( int i=0; i<tupleDataTest.length; i++ ) {
            TupleNf64 instance = tupleTest[i].extend(value);
            double[] result = instance.toDouble();
            double[] expResult = Arrays.copy(tupleDataTest[i], 0, tupleDataTest[i].length+1);
            expResult[tupleDataTest[i].length] = value;
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertEquals( result.length, tupleTest[i].toDouble().length+1 );
        }
    }

    /**
     * Test of toChars method, of class DefaultTuple.
     */
    @Test
    public void testToChars() {
        TupleNf64 instance = new TupleNf64( new double[]{ -2., 9. });
        byte[] expResult = new byte[]{'[', '-', '2', '.', '0', ',', '9', '.', '0', ']'};
        byte[] result = instance.toChars().toBytes();
        Assert.assertTrue( Arrays.equals( expResult, result ) );
    }

    /**
     * Test of equals method, of class DefaultTuple.
     */
    @Test
    public void testEquals() {

        for ( int i=0; i<tupleDataTest.length; i++ ) {
            // test for equals null
            Object obj = null;
            TupleNf64 instance = tupleTest[i];
            boolean expResult = false;
            boolean result = instance.equals(obj);
            Assert.assertEquals(expResult, result);

            // test for equals other type of object
            obj = -36.0;
            expResult = false;
            result = instance.equals(obj);
            Assert.assertEquals(expResult, result);

            // test for equals DefaultTuple
            for ( int j=0; j<tupleDataTest.length; j++ ) {
                TupleNf64 other = tupleTest[j];
                result = instance.equals(other);
                if ( i == j ) {
                    expResult = true;
                } else {
                    expResult = false;
                }
                Assert.assertEquals(expResult, result);
            }
        }
    }

    /**
     * Test of expand method, of class DefaultTuple.
     */
    @Test
    public void testExpand() {
        /*
        The method:
            protected static double[] expand(final double[] values, double value)
        is tested throught the test of the method public DefaultTuple extend(double value)
        */
    }

}
