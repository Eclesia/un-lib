package science.unlicense.math.impl.number;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.exception.InvalidArgumentException;

/**
 *
 * @author Bertrand COTE
 */
public class LargeIntegerTest {

    public LargeIntegerTest() {

        // ========== test constructors ========================================

        long[] mag;
        byte[] magByte;

        // test LargeInteger() default constructor

        LargeInteger instance = new LargeInteger();
        LargeInteger expResult = new LargeInteger( 0, new long[]{ 0x0 } );
        Assert.assertTrue( instance.order(expResult) == 0 );

        // tes LargeInteger( long n ) Constructor from a signed 64bits integer (long)

        instance = new LargeInteger( 0L );
        expResult = new LargeInteger( 0, new long[]{ 0L} );
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( 36L );
        expResult = new LargeInteger( 1, new long[]{ 36L} );
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( -783L );
        expResult = new LargeInteger( -1, new long[]{ 783L } );
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( 9223372036854775807L );
        expResult = new LargeInteger( 1, new long[]{ 0x7fffffffffffffffL } );
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( -9223372036854775808L );
        expResult = new LargeInteger( -1, new long[]{ 0x8000000000000000L } );
        Assert.assertTrue( instance.order(expResult) == 0 );

        // test LargeInteger( int sign, long[] magnitude )

        mag = null;
        instance = new LargeInteger( 1, mag );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( 0, mag );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( -1, mag );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );

        mag = new long[0];
        instance = new LargeInteger( 1, mag );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( 0, mag );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( -1, mag );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );

        mag = new long[1];
        instance = new LargeInteger( 1, mag );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( 0, mag );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( -1, mag );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );

        mag = new long[]{ 0x37bL };
        instance = new LargeInteger( 1, mag );
        expResult = new LargeInteger( 1, new long[]{ 0x37bL });
        Assert.assertTrue( instance.order(expResult) == 0 );
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( -1, mag );
        expResult = new LargeInteger( -1, new long[]{ 0x37bL });
        Assert.assertTrue( instance.order(expResult) == 0 );

        // test LargeInteger( int sign, byte[] magnitudeBytes ) constructor

        magByte = null;
        instance = new LargeInteger( 1, magByte );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( 0, magByte );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( -1, magByte );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );

        magByte = new byte[0];
        instance = new LargeInteger( 1, magByte );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( 0, magByte );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( -1, magByte );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );

        magByte = new byte[1];
        instance = new LargeInteger( 1, magByte );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( 0, magByte );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( -1, magByte );
        expResult = new LargeInteger();
        Assert.assertTrue( instance.order(expResult) == 0 );

        magByte = new byte[]{ (byte) 0x7b, (byte) 0x3 };
        instance = new LargeInteger( 1, magByte );
        expResult = new LargeInteger( 1, new long[]{ 0x37bL });
        Assert.assertTrue( instance.order(expResult) == 0 );
        instance = new LargeInteger( -1, magByte );
        expResult = new LargeInteger( -1, new long[]{ 0x37bL });
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( 1, new byte[]{ 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b } );
        expResult = new LargeInteger( 1, new long[]{ 0x0706050403020100L, 0x0b0a0908L } );
        Assert.assertTrue( instance.order(expResult) == 0 );

        // test LargeInteger( String s, int radix ) constructor

        instance = new LargeInteger( "0", 10 );
        expResult = LargeInteger.ZERO;
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "+0", 10 );
        expResult = LargeInteger.ZERO;
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "-0", 10 );
        expResult = LargeInteger.ZERO;
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "25369874", 10 );
        expResult = new LargeInteger( 1, new long[]{ 0x1831d12L, } );
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "+25369874", 10 );
        expResult = new LargeInteger( 1, new long[]{ 0x1831d12L, } );
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "-25369874", 10 );
        expResult = new LargeInteger( -1, new long[]{ 0x1831d12L, } );
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "0", 16 );
        expResult = LargeInteger.ZERO;
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "+0", 16 );
        expResult = LargeInteger.ZERO;
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "-0", 16 );
        expResult = LargeInteger.ZERO;
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "975d21f6acb90a10", 16 );
        expResult = new LargeInteger( 1, new long[]{ 0x975d21f6acb90a10L, } );
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "+975d21f6acb90a10", 16 );
        expResult = new LargeInteger( 1, new long[]{ 0x975d21f6acb90a10L, } );
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "-975d21f6acb90a10", 16 );
        expResult = new LargeInteger( -1, new long[]{ 0x975d21f6acb90a10L, } );
        Assert.assertTrue( instance.order(expResult) == 0 );


        instance = new LargeInteger( "0", 8 );
        expResult = LargeInteger.ZERO;
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "+0", 8 );
        expResult = LargeInteger.ZERO;
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "-0", 8 );
        expResult = LargeInteger.ZERO;
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "1135351037325456205020", 8 );
        expResult = new LargeInteger( 1, new long[]{ 0x975d21f6acb90a10L, } );
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "+1135351037325456205020", 8 );
        expResult = new LargeInteger( 1, new long[]{ 0x975d21f6acb90a10L, } );
        Assert.assertTrue( instance.order(expResult) == 0 );

        instance = new LargeInteger( "-1135351037325456205020", 8 );
        expResult = new LargeInteger( -1, new long[]{ 0x975d21f6acb90a10L, } );
        Assert.assertTrue( instance.order(expResult) == 0 );

        // =====================================================================
    }

    /**
     * Test of toHexString method, of class LargeInteger.
     */
    @Test
    public void testToHexString() {
//        LargeInteger instance = new LargeInteger();
//        String expResult = "";
//        String result = instance.toHexString();
//        Assert.assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class LargeInteger.
     */
    @Test
    public void testToString() {
//        LargeInteger instance = new LargeInteger();
//        String expResult = "";
//        String result = instance.toString();
//        Assert.assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
    }

    private static final long[][][] equalsCompareToDataTest = new long[][][]{
        { // { sign, mag0, mag1, ... , mag_n-1 }
            // this
            {-1, 0xffffffffffffffffL},
            // other
            {1, 0x0000000000000006L},
            // expResult: equals (1 for true, 0 for false)
            {0, -1}
        },
        { // { sign, mag0, mag1, ... , mag_n-1 }
            // this
            {-1, 0x00fff00fffffffffL},
            // other
            {-1, 0x00fff00fffffffffL},
            // expResult 1 for true, 0 for false
            {1, 0}
        },
        { // { sign, mag0, mag1, ... , mag_n-1 }
            // this
            {1, 0xfedcba9876543210L},
            // other
            {1, 0x00fff00fffffffffL},
            // expResult 1 for true, 0 for false
            {0, 1}
        },
        { // { sign, mag0, mag1, ... , mag_n-1 }
            // this
            {1, 0xfedcba9876543210L},
            // other
            {1, 0x00fff00fffffffffL, 0x975d21f6acb90a10L},
            // expResult 1 for true, 0 for false
            {0, -1}
        },
        { // { sign, mag0, mag1, ... , mag_n-1 }
            // this
            {-1, 0xfedcba9876543210L, 0x975d21f6acb90a10L},
            // other
            {-1, 0xfedcba9876543210L, 0x975d21f6acb90a10L},
            // expResult 1 for true, 0 for false
            {1, 0}
        },};

    /**
     * Test of getSign method, of class LargeInteger.
     */
    @Test
    public void testGetSign() {
        for (long[][] data : equalsCompareToDataTest) {
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            int expResult = (int) data[0][0];
            int result = instance.getSign();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of getMagnitude method, of class LargeInteger.
     */
    @Test
    public void testGetMagnitude() {
        for (long[][] data : equalsCompareToDataTest) {
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            long[] expResult = Arrays.copy(data[0], 1, data[0].length - 1);
            long[] result = instance.getMagnitude();
            Assert.assertArrayEquals(expResult, result);
        }
    }

    private static final long[][][] getByteLengthDataSample = new long[][][]{
        {
            // { sign, mag0, mag1, ... , mag_n-1 }
            { 1, 0x79d78eb0250c5546L, 0xecfa786db18cf72fL, 0x28e68bf9a5d18b55L, 0x18dbc2965L,},
            // { byteLength, bitLength }
            { 29, 225 }
        },
        {
            // { sign, mag0, mag1, ... , mag_n-1 }
            { -1, 0x414a1baf1ac638a9L, 0x6bec1d0eb611743cL, 0xccdf9e7238fab92eL, 0x39096a20b936158dL, 0xb45b0b6777a199adL, 0xcd0d0a00906cc980L, 0xfa6L,  },
            // { byteLength, bitLength }
            { 50, 396 }
        },
        {
            // { sign, mag0, mag1, ... , mag_n-1 }
            { -1, 0x04352291122faa1cL, 0x11fcbf1495f69b6bL, 0x354efe93006fdc4dL, 0x1a2a0a5b1cf442cbL, 0x90ada58b8a739872L, 0x16b2L,  },
            // { byteLength, bitLength }
            { 42, 333 }
        },
        {
            // { sign, mag0, mag1, ... , mag_n-1 }
            {  1, 0x4d43fc5f13276336L, 0x482ac6f9f2a08ba9L, 0xd6a06dbcc303dbb4L, 0xdfd8343f74ffe22eL, 0xfe56db3988f7095L,  },
            // { byteLength, bitLength }
            { 40, 316 }
        },
        {
            // { sign, mag0, mag1, ... , mag_n-1 }
            { 1, 0x0L,  },
            // { byteLength, bitLength }
            { 1, 1 }
        },
        {
            // { sign, mag0, mag1, ... , mag_n-1 }
            { 1, 0xcfdc8322d6001f44L, 0xce8fa006018b522aL, 0x267da078d89d0387L, 0xc1cdd4f04b4bf3c0L, 0x9e4df3183942559aL, 0xa551cae0a8a07ad5L, 0x6L,  },
            // { byteLength, bitLength }
            { 49, 387 }
        },
        {
            // { sign, mag0, mag1, ... , mag_n-1 }
            { -1, 0x8cd56debbd821ea9L, 0xb23ceL,  },
            // { byteLength, bitLength }
            { 11, 84 }
        },
        {
            // { sign, mag0, mag1, ... , mag_n-1 }
            { 1, 0x26f61L,  },
            // { byteLength, bitLength }
            { 3, 18 }
        },
        {
            // { sign, mag0, mag1, ... , mag_n-1 }
            { 1, 0x2d0542febaef4e4cL, 0x56b4e2bf6adL, },
            // { byteLength, bitLength }
            { 14, 107 }
        },
    };

    /**
     * Test of getByteLength method, of class LargeInteger.
     */
    @Test
    public void testGetByteLength() {
        for (long[][] data : getByteLengthDataSample) {
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            int expResult = (int) data[1][0];
            int result = instance.getByteLength();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of getBitLength method, of class LargeInteger.
     */
    @Test
    public void testGetBitLength() {
        for (long[][] data : getByteLengthDataSample) {
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            int expResult = (int) data[1][1];
            int result = instance.getBitLength();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of equals method, of class LargeInteger.
     */
    @Test
    public void testEquals() {
        for (long[][] data : equalsCompareToDataTest) {
            Object otherObject = new LargeInteger((int) data[1][0], Arrays.copy(data[1], 1, data[1].length - 1));
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            boolean expResult = data[2][0] == 1L;
            boolean result = instance.equals(otherObject);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of hashCode method, of class LargeInteger.
     */
    @Test
    public void testHashCode() {
//        LargeInteger instance = new LargeInteger();
//        int expResult = 0;
//        int result = instance.hashCode();
//        Assert.assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
    }

    /**
     * Test of order method, of class LargeInteger.
     */
    @Test
    public void testOrder() {
        for (long[][] data : equalsCompareToDataTest) {
            LargeInteger o = new LargeInteger((int) data[1][0], Arrays.copy(data[1], 1, data[1].length - 1));
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            int expResult = (int) data[2][1];
            int result = instance.order(o);
            Assert.assertEquals(expResult, result);
        }
    }

    // =========================================================================
    // =========================================================================
    // =========================================================================
    // =========================================================================
    // =========================================================================


    private static final long[][][] addSbtractMultDividePowDataTest = new long[][][]{
        {
            // li0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xba290b769c8e5906L, 0xbec1262b012f1340L, 0x4cd93b1b1c32f772L, 0x71cd0779caeb10f6L, 0xfffc0fee5af6c894L, 0xbf47ce0e79a1875cL, 0xa546b31b01d12665L, 0xcabaa6557d7abea5L, 0x30eb2bf48df82a5fL, 0x50e4ba7e19f3d665L, 0x88d40861b1976213L, 0xa40031d13a38c836L, 0x5c5305ccd2a24L,},
            // li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xfe2df59187b5e974L, 0xabd2d92266c46f28L, 0xd647e0L,},
            // li0+li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xb85701082444427aL, 0x6a93ff4d67f38269L, 0x4cd93b1b1d093f53L, 0x71cd0779caeb10f6L, 0xfffc0fee5af6c894L, 0xbf47ce0e79a1875cL, 0xa546b31b01d12665L, 0xcabaa6557d7abea5L, 0x30eb2bf48df82a5fL, 0x50e4ba7e19f3d665L, 0x88d40861b1976213L, 0xa40031d13a38c836L, 0x5c5305ccd2a24L,},
            // li0-li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xbbfb15e514d86f92L, 0x12ee4d089a6aa417L, 0x4cd93b1b1b5caf92L, 0x71cd0779caeb10f6L, 0xfffc0fee5af6c894L, 0xbf47ce0e79a1875cL, 0xa546b31b01d12665L, 0xcabaa6557d7abea5L, 0x30eb2bf48df82a5fL, 0x50e4ba7e19f3d665L, 0x88d40861b1976213L, 0xa40031d13a38c836L, 0x5c5305ccd2a24L,},
            // li0*li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xdf5512399ac4ccb8L, 0xa2531b1448a02d20L, 0x92c608b70f6e10efL, 0x28e9dc2870cb789eL, 0x156fb2ed1bd2e91eL, 0x01b904e1a6217f72L, 0x01ca843704ada5d1L, 0xba67f77488cd1fc2L, 0x5e798b5b84e3ef54L, 0x333cd9f412d21c8fL, 0x836193505f0ea5a5L, 0xb3e479e6c601b7fdL, 0x23d7690d75d63767L, 0x39e3e95b21e05e5fL, 0x752e6707496bcd3dL, 0x4d4L,},
            // li0/li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x42ef065d14bd8ecaL, 0x3cdb9ff39b0fc059L, 0xaa610a1d4e6d5af7L, 0x21bb7e7adbc2b7acL, 0x444c02af66441543L, 0x1d4fcbb2e583a0b3L, 0x841cabb7e3e55bd5L, 0x985bd2d542e1e0d7L, 0xbc390120c70e0175L, 0xf3f2ed317b95a981L, 0x6e4c9ffL,},
            // li0%li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x94c89eac34e1cb7eL, 0xe3fdc3bff5f7ff67L, 0x43ac53L,},
            // floorSqrt(li0): { sign, magnitude_0, magnitude_1, ... }
            {1, 0x64d89455565c5091L, 0xfad1e76263106763L, 0x5b3fe7faa4666a4dL, 0x786ab0441186c2baL, 0xdb8920498096c34cL, 0xe210531382240958L, 0x266f28bL,},},
        {
            // li0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li0+li1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xa70c1d27dcd5aa4eL, 0x1e1ac8a7042L,},
            // li0-li1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // li0*li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xcadfdfb3b9c5ebf1L, 0x80d9792d801d65abL, 0x75210b0ba14514e3L, 0xe292L,},
            // li0/li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L,},
            // li0%li1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // floorSqrt(li0): { sign, magnitude_0, magnitude_1, ... }
            {1, 0xf84d82f7c8e95L,},},
        {
            // li0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li0+li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xa70c1d27dcd5aa4eL, 0x1e1ac8a7042L,},
            // li0-li1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // li0*li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xcadfdfb3b9c5ebf1L, 0x80d9792d801d65abL, 0x75210b0ba14514e3L, 0xe292L,},
            // li0/li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L,},
            // li0%li1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // floorSqrt(li0): { sign, magnitude_0, magnitude_1, ... }
            {1, 0xf84d82f7c8e95L,},},
        {
            // li0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li0+li1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // li0-li1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xa70c1d27dcd5aa4eL, 0x1e1ac8a7042L,},
            // li0*li1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xcadfdfb3b9c5ebf1L, 0x80d9792d801d65abL, 0x75210b0ba14514e3L, 0xe292L,},
            // li0/li1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x1L,},
            // li0%li1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // floorSqrt(li0): { sign, magnitude_0, magnitude_1, ... }
            {1, 0xf84d82f7c8e95L,},},
        {
            // li0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li0+li1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // li0-li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xa70c1d27dcd5aa4eL, 0x1e1ac8a7042L,},
            // li0*li1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xcadfdfb3b9c5ebf1L, 0x80d9792d801d65abL, 0x75210b0ba14514e3L, 0xe292L,},
            // li0/li1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x1L,},
            // li0%li1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // floorSqrt(li0): { sign, magnitude_0, magnitude_1, ... }
            {1, 0xf84d82f7c8e95L,},},
        {
            // li0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L,},
            // li0+li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad528L, 0xf0d6453821L,},
            // li0-li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad526L, 0xf0d6453821L,},
            // li0*li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li0/li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li0%li1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // floorSqrt(li0): { sign, magnitude_0, magnitude_1, ... }
            {1, 0xf84d82f7c8e95L,},},
        {
            // li0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L,},
            // li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li0+li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad528L, 0xf0d6453821L,},
            // li0-li1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x53860e93ee6ad526L, 0xf0d6453821L,},
            // li0*li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li0/li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0L,},
            // li0%li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L,},
            // floorSqrt(li0): { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L,},},
        {
            // li0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0L,},
            // li1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li0+li1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li0-li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x53860e93ee6ad527L, 0xf0d6453821L,},
            // li0*li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0L},
            // li0/li1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0L,},
            // li0%li1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // floorSqrt(li0): { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0L,},},
    };

    /**
     * Test of add method, of class LargeInteger.
     */
    @Test
    public void testAdd() {
        for ( long[][] data : addSbtractMultDividePowDataTest ) {
            LargeInteger     other = new LargeInteger( (int) data[1][0], Arrays.copy( data[1], 1, data[1].length-1 ) );
            LargeInteger  instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger expResult = new LargeInteger( (int) data[2][0], Arrays.copy( data[2], 1, data[2].length-1 ) );
            LargeInteger result = instance.add(other);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of subtract method, of class LargeInteger.
     */
    @Test
    public void testSubtract() {
        for ( long[][] data : addSbtractMultDividePowDataTest ) {
            LargeInteger     other = new LargeInteger( (int) data[1][0], Arrays.copy( data[1], 1, data[1].length-1 ) );
            LargeInteger  instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger expResult = new LargeInteger( (int) data[3][0], Arrays.copy( data[3], 1, data[3].length-1 ) );
            LargeInteger result = instance.subtract(other);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of mult method, of class LargeInteger.
     */
    @Test
    public void testMult() {
        for ( long[][] data : addSbtractMultDividePowDataTest ) {
            LargeInteger     other = new LargeInteger( (int) data[1][0], Arrays.copy( data[1], 1, data[1].length-1 ) );
            LargeInteger  instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger expResult = new LargeInteger( (int) data[4][0], Arrays.copy( data[4], 1, data[4].length-1 ) );
            LargeInteger result = instance.mult(other);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of divide method, of class LargeInteger.
     */
    @Test
    public void testDivide() {
        for ( long[][] data : addSbtractMultDividePowDataTest ) {
            LargeInteger     other = new LargeInteger( (int) data[1][0], Arrays.copy( data[1], 1, data[1].length-1 ) );
            LargeInteger  instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger expResult = new LargeInteger( (int) data[5][0], Arrays.copy( data[5], 1, data[5].length-1 ) );
            LargeInteger result = instance.divide(other);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of divideAndRemainder method, of class LargeInteger.
     */
    @Test
    public void testDivideAndRemainder() {
        for ( long[][] data : addSbtractMultDividePowDataTest ) {
            LargeInteger     other = new LargeInteger( (int) data[1][0], Arrays.copy( data[1], 1, data[1].length-1 ) );
            LargeInteger  instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger divExpResult = new LargeInteger( (int) data[5][0], Arrays.copy( data[5], 1, data[5].length-1 ) );
            LargeInteger modExpResult = new LargeInteger( (int) data[6][0], Arrays.copy( data[6], 1, data[6].length-1 ) );
            LargeInteger[] result = instance.divideAndRemainder(other);
            Assert.assertTrue( result.length==2);
            Assert.assertEquals(divExpResult, result[0]);
            Assert.assertEquals(modExpResult, result[1]);
        }

        long[][][] debug = new long[][][]{
            {
                { 1, 0x1fbf560697b4a9a1L, 0x482d9641f02cf874L, 0xe52998c0e5L,  },
                { 1, 0xff3e04e67946334dL, 0xad701ef68bbbab8aL,  },
                { 1, 0x152404c855cL,  },
                { 1, 0xfb7c8cdd0ef738f5L, 0x61e37af08ba65bf5L,  },
            },
            {   // ========== bug ========== FIXED
                // numerator: { sign, magnitude_0, magnitude_1, ... }
                {-1, 0xfc95c7b59290e18fL, 0x24L,},
                {-1, 0x37443cf97eL,},
                {1, 0xab536591L,},
                {-1, 0xfcfbedb31L,},},

            {   // ========== bug ========== FIXED
                {1, 0x79c7e8029cf4bffbL, 0xddL,},
                {-1, 0xf6e9cbb3c60ab989L,},
                {-1, 0xe5L,},
                {1, 0x9aa4b032755cc86eL,},},
        };

        for ( long[][] data : debug ) {
            LargeInteger     instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger        other = new LargeInteger( (int) data[1][0], Arrays.copy( data[1], 1, data[1].length-1 ) );
            LargeInteger divExpResult = new LargeInteger( (int) data[2][0], Arrays.copy( data[2], 1, data[2].length-1 ) );
            LargeInteger modExpResult = new LargeInteger( (int) data[3][0], Arrays.copy( data[3], 1, data[3].length-1 ) );
            LargeInteger[] result = instance.divideAndRemainder(other);
            Assert.assertTrue( result.length==2);
            Assert.assertEquals(divExpResult, result[0]);
            Assert.assertEquals(modExpResult, result[1]);
        }
    }

    /**
     * Test of mod method, of class LargeInteger.
     */
    @Test
    public void testMod() {
        for ( long[][] data : addSbtractMultDividePowDataTest ) {
            LargeInteger     other = new LargeInteger( (int) data[1][0], Arrays.copy( data[1], 1, data[1].length-1 ) );
            LargeInteger  instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger expResult = new LargeInteger( (int) data[6][0], Arrays.copy( data[6], 1, data[6].length-1 ) );
            LargeInteger result = instance.mod(other);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of negate method, of class LargeInteger.
     */
    @Test
    public void testNegate() {
        for ( long[][] data : addSbtractMultDividePowDataTest ) {
            LargeInteger  instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger expResult = new LargeInteger( -(int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger result = instance.negate();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of abs method, of class LargeInteger.
     */
    @Test
    public void testAbs() {
        for ( long[][] data : addSbtractMultDividePowDataTest ) {
            LargeInteger  instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            int sign = (int) data[0][0];
            LargeInteger expResult = new LargeInteger( sign<0 ? 1: sign, Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger result = instance.abs();
            Assert.assertEquals(expResult, result);
        }
    }

    private static final long[][][] isZeroIsOneDataTest = new long[][][]{
        {
            // instance: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L },
            // { isZero, isOne }
            { 1, 0 }
        },
        {
            // instance: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x1L },
            // { isZero, isOne }
            { 0, 1 }
        },
        {
            // instance: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x00005c59f6e61212L },
            // { isZero, isOne }
            { 0, 0 }
        },
        {
            // instance: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x00005c59f6e61212L },
            // { isZero, isOne }
            { 0, 0 }
        },
        {
            // instance: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x7b45792fa1e71767L, 0xb4915c59f6e61212L, 0xa660L,},
            // { isZero, isOne }
            { 0, 0 }
        },
        {
            // instance: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x7b45792fa1e71767L, 0xb4915c59f6e61212L, 0xa660L,},
            // { isZero, isOne }
            { 0, 0 }
        },
    };

    /**
     * Test of isZero method, of class LargeInteger.
     */
    @Test
    public void testIsZero() {
        for ( long[][] data : isZeroIsOneDataTest ) {
            LargeInteger instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            boolean expResult = data[1][0] == 1L;
            boolean result = instance.isZero();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of isOne method, of class LargeInteger.
     */
    @Test
    public void testIsOne() {
        for ( long[][] data : isZeroIsOneDataTest ) {
            LargeInteger instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            boolean expResult = data[1][1] == 1L;
            boolean result = instance.isOne();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of zero method, of class LargeInteger.
     */
    @Test
    public void testZero() {
        LargeInteger instance = new LargeInteger();
        LargeInteger expResult = new LargeInteger();
        LargeInteger result = instance.zero();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of one method, of class LargeInteger.
     */
    @Test
    public void testOne() {
        LargeInteger instance = new LargeInteger();
        LargeInteger expResult = new LargeInteger( 1 );
        LargeInteger result = instance.one();
        Assert.assertEquals(expResult, result);
    }

    private static final long[][][] powIntDataTest = new long[][][]{
        {
            // m: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x8caeceb8f3c53cf6L, 0xeadb6e79b886a914L, 0x03845d4e21e6dd10L, 0x20914c8b721262b3L, 0x570fL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {0, 1, 0x1L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {1, -1, 0x8caeceb8f3c53cf6L, 0xeadb6e79b886a914L, 0x03845d4e21e6dd10L, 0x20914c8b721262b3L, 0x570fL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {2, 1, 0xb02b9351e5203c64L, 0x5355eae1e7cf9b2cL, 0x8e138b3ffa57118bL, 0xd6382db93b7b257bL, 0x79b60ef27f35c2b0L, 0x2e69bee48fa1830bL, 0xedfef72dfdcb2af7L, 0x9732657a869c268eL, 0x1d9b4907L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {3, -1, 0xf6a58d6720157818L, 0xb43c5abb840a6696L, 0x654837a09f3b7408L, 0x616d3f285ba1ea6cL, 0x095e145f834d4e98L, 0xcf4a1edc0364bc11L, 0xd2f103663ab6b756L, 0xc2b1d8d91cfb8b92L, 0xa863e3c71bf5c3edL, 0x512bc4eed3e71d2cL, 0xa5f94ac79545ebffL, 0x86d3b50477c63da5L, 0xa1185af12c3L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {4, 1, 0x9a6bbf620f3f0710L, 0x7a05707d5b444d93L, 0xb6c7e5ecb0214774L, 0x55070ae8baa5c3e3L, 0xeb1bd17ac425c10fL, 0xdedd170deafb115dL, 0xb9474bc6aab33709L, 0x6ff3c97f9357f9f7L, 0xcd9bd8acb8b99645L, 0x50b887e69d3cce77L, 0xa1c0a780a207ecc1L, 0xd3fa66baf5c43c03L, 0x97a48ee4223cf05fL, 0x5c0e20f6b7012c2fL, 0xa0bc39d74205e564L, 0x00cc4cedd46a49a3L, 0x36c8cbd3c47d679L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {5, -1, 0x392f66820b888960L, 0xe68a23a6e960a84fL, 0x3b9807c404ec6994L, 0xf80260468c95efa2L, 0xa6a3a071fe8d4f46L, 0x22f47422ffbb4f7eL, 0x8d09d5f899d84dcfL, 0xafc2b340dd4dec2dL, 0x3a212d4e1888b5e7L, 0x64123a02389d491fL, 0x524393d80aa9e392L, 0x9ffdf6b9449ca34aL, 0x296a1b8120eda005L, 0x9e2199056b5b7109L, 0x6c31a8ffc5c43d7fL, 0xbf4e976e223c1c72L, 0xf07e194f6058a526L, 0x1a9b5f9ebcae6f71L, 0xc40e9ec59472a7b0L, 0x4066d136d569d915L, 0x17a011ac680ffe21L, 0x12aL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {6, 1, 0x6a867322ec468240L, 0x4c8571d29d6f3d7dL, 0x68dd5b3c873e37fdL, 0x88ed965da232b808L, 0x88064bc9a0b8a6f0L, 0x526363e433c43e78L, 0x1703b250b8973108L, 0xfb74ed52c9301b50L, 0x257ccbda6d22e5f7L, 0xba51a80bd235c1d7L, 0x1163fa704d5960c3L, 0xbcf67592f41aea69L, 0x94095593ff3d0ad0L, 0xa4e8c6c6a6880f3dL, 0x766a5519d451e46cL, 0xa69c71d2532f2853L, 0x9275581ec5edaee0L, 0xd26fac9c62ab45cfL, 0xd7c443e15b1757a1L, 0x4c81e32db8af6df0L, 0x4601086cef5ec4c2L, 0xa9e79338dc2c52f0L, 0x2236a808b681642eL, 0xf8b19448d74ef92aL, 0xb48725ba450cc7c7L, 0x655fa4L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {7, -1, 0xb752b0d48d882980L, 0xb703fef30429839eL, 0xd4e3f7617fef3d07L, 0x25e93b3693a538eaL, 0x887de9e325c8c659L, 0x0282269a28b7e088L, 0x83ac2b04f9ef89e5L, 0x32dd00a0f8f2340aL, 0xd64cdc30ee52e194L, 0x5507daa1b5304cefL, 0x2fa3b72fbfe85de7L, 0xa41a1673f536e7bbL, 0x1afeddc1633b36d1L, 0x8293032ec83d1f7bL, 0xa01b6cb626598915L, 0x925413dd5dd145bbL, 0x3ccda527365ea8a5L, 0x8df971c5321387ecL, 0x0380be2fd6cd6815L, 0x4055048eccded9aaL, 0x5f0431609cd33affL, 0x38e15f4361131208L, 0x8a1a2f01f2159086L, 0x8bae3959da563e66L, 0x934c1f8c7c2268b9L, 0xb674065de4391d3dL, 0x1335620b58946403L, 0x1ee9a52602646280L, 0x9435a88a56d55a32L, 0x22797e797eL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {8, 1, 0xe113ba985a11e100L, 0x794e65861285c7deL, 0x37d5580adbe96818L, 0xe9ee93867af940aeL, 0xaa88e781240fb09aL, 0x5370c49f666dd460L, 0xc1ad9e42b6d3b96cL, 0x46adaac0c0a0b9d5L, 0xbc53600117bb0515L, 0xaab4cd430221ac6bL, 0x440ce59d475859f4L, 0x52bdedfc8c7be5e8L, 0x9a48847930c733b8L, 0x373227b060aa4427L, 0x1aff4fdee317ec6eL, 0xbb9cf8be80c8fdfdL, 0x1f62e66a0b70c4adL, 0x7776cfe55c376daeL, 0xa68eeecb3a13ea28L, 0x62755543f28a8f04L, 0xa9ad9ba565823033L, 0x21776000b6792ae3L, 0x28f7fb4464644eb4L, 0x0fdc10bc583a4f25L, 0x3f70a6bdae3abd2fL, 0x9f55cbdabca128aaL, 0x3b6d2b0d35284dcfL, 0xd2c99f97a219a850L, 0x0fc657780473be33L, 0xa9268526a79f7dddL, 0xeafa50b7281afda1L, 0x568c5c0dcc37ddccL, 0x6ef63e6a80a02ad1L, 0xbb9537c741174L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {9, -1, 0x2b5503d7e2ea3600L, 0x8cc97f0ceda18468L, 0xc063d7a27f51ff42L, 0x0c7742f36c094a0eL, 0xc7369e8ca55d2b75L, 0xad0774b5ff390e00L, 0x3aa16dc9aa5920ccL, 0xe998f88d3b2c4c5dL, 0x5509199355955e5bL, 0x820dd6d5172ed975L, 0x089d8bf31da23d9eL, 0xfb8a1eecb4d91e60L, 0xa3785b40cd265260L, 0x37a765a6deb44b90L, 0xf86e08033578fd60L, 0x47f0d0e575bdd13eL, 0x5010ad1b827be68cL, 0xcd7eaa77f5955fe1L, 0x10c5e0ef6a46ea4cL, 0x20c9a2bc11a7fabbL, 0xcd4a4dccfee2bc14L, 0x283aa2cd5b9d54beL, 0x00ebae893e9cc8e9L, 0xc3e62a78bc8068bfL, 0x7357766825c86c1dL, 0x5077e2aafa6a0d28L, 0xfd13454f7dc0c34cL, 0x55d1162519361382L, 0x3299713307adb0f1L, 0x525375fe3b242880L, 0xa48bc3e39fe9c08bL, 0xb35ab7945e821a3cL, 0x4d8b7ca9ab113488L, 0x731c32a9485b6709L, 0x73afd05310ee7c0dL, 0x1644babc3ceb75e2L, 0x6318aa93536b25edL, 0xfcacb901a5fb3a1aL, 0x3L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {10, 1, 0xb5e90a207fb7e400L, 0x0f3bb0cad081b5cfL, 0x4fe83167d0291c93L, 0xf7da384c94f0b11cL, 0x4adf17e08a4a4f23L, 0x39c30dad03a31c66L, 0xba76b3ddca2d8785L, 0x76b78acb5f409671L, 0xffffa75360d3e285L, 0x4e71c61212342a61L, 0x513d27d53752104eL, 0x561fafdb232ca616L, 0xe44211ccf027ed32L, 0xd062296b1145271eL, 0xaf3eed3fb89a5fa2L, 0x63626f9ae319a525L, 0x1f4783647ee9677bL, 0xe3698abff2e12dd1L, 0xa63cbc61d788186fL, 0xc5cbdd3cc77c1853L, 0x72c3efda35b9239bL, 0xdd0c1158a2235109L, 0x596c72f4fe475fe0L, 0x614468127cb1461aL, 0xabc49354de2cf0e6L, 0xcfa211c7bfeef6a9L, 0x332872678824c5b6L, 0x0ee47396177d45a2L, 0xd17fe79fa1d8d20dL, 0xa067226517b17112L, 0x3237f1464b5c5854L, 0x3617980eac1367b4L, 0x507c42f3947b808cL, 0x850461486072a36eL, 0x08be6639ea3e62a2L, 0x542f3a9e2119e064L, 0x9b12e06e70170cc1L, 0xd2bfb6d6a852b37bL, 0x07fa0bae25c28d2cL, 0x4f3b4465f49bbb22L, 0xb20cf47d33c2b02bL, 0x02d74ca558a8f2ffL, 0x15b1bL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {11, -1, 0xce310f1848251800L, 0xbd8fefbe7b3b32fbL, 0x03a1626dbddd9f50L, 0xd092730068ac0b27L, 0x57301d0ff76be37aL, 0x35aa9b9d115e120fL, 0xf4e6fee865737cd0L, 0x86ad9d6096d6057dL, 0x0054125f5de2b372L, 0xbc9fe2fbb401b364L, 0xb46905dbd74cbbbfL, 0x09e6c0ce7a8631d6L, 0x0fb740adc4ba57ecL, 0x6bfbcbd4b99b74c2L, 0xcacb683dad4333bfL, 0x733a6a90ead27ca6L, 0x90861e0c51919b98L, 0x2085c8cd56406eebL, 0xc0eb25aa8a185140L, 0x35fe0a91e8abafe7L, 0x9a6b17c9d121c361L, 0x8367919a9359de41L, 0xfbff6a8ed80f9f8cL, 0x0703060a516b1091L, 0xa98e4f2e45759f8bL, 0x96cf5fe0838393c5L, 0x3280008fb4609904L, 0x204042bc0a9d2b18L, 0x6f2a51aad7610245L, 0xe7f6ae1a596b4246L, 0x2c19595622e00981L, 0x0b3a6d1e483b1071L, 0x743fc3332ef1de49L, 0xb689db9631c267d7L, 0x3d7b1a7d9e695b66L, 0x3abf68c2e2d7bec3L, 0x38d267f864573f65L, 0x8f38c8d8ed0b380aL, 0x8b4f3f9b3f5dd41dL, 0x29ea8c36eeb4e000L, 0x164fb7085eee5ff9L, 0x55b2c2ed1f5659d0L, 0x31fcb9972c9e3410L, 0x6573654d77fda92eL, 0xf1444a4d82d05f47L, 0xb81a4b861bdc4fabL, 0x760ab0b4L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {12, 1, 0x50d22c917d451000L, 0x2dc04491726fd538L, 0xeca7b1b3f1525701L, 0x758246be3e1c3e7dL, 0x92b06ebe23440346L, 0x7449ef8276e13fafL, 0xd7d2d7fae77cadc5L, 0xa647c07ae05f493cL, 0x403c229859972a92L, 0xcf68915187f76d55L, 0xe617647da2d55d42L, 0x0a24fda1e9ec8c69L, 0x033e86a16e3425d4L, 0x85145a03602d24c0L, 0xb371671dfa90a10eL, 0xca1ea51e3237f94cL, 0x9108a146bd8e35fbL, 0x015d1fd08724c55bL, 0x442c9af06b9b908bL, 0x87f68c4769f4710cL, 0x6c7e20da55964492L, 0xe2910e74af0ccb08L, 0x462adb6d9cdcb35fL, 0xd937b8f3a6e74d2eL, 0x0a4a88ccd6c9fea4L, 0xd00619701a2477e3L, 0xe58fea9db4f5606eL, 0x587440c264eb2ca6L, 0xf27384aa33139291L, 0xa9bd6918914da324L, 0xc3e4f404e8eef10eL, 0xcf99c8ada57c4102L, 0xe3b1183874a9dcfcL, 0x12663c83fd8a4179L, 0x2fc00398a2cd14caL, 0x6b8bbb989dc93ab1L, 0x965f9c6fdb9aaa6aL, 0x1e8a06150d3018ebL, 0x61a8a7e86c95b834L, 0x1e756fbea0f438f2L, 0x0168cc7a9c871cc0L, 0xc3f8389bc0d74520L, 0xd27330368339012bL, 0x749fe11e5a632a9cL, 0xca39e324fc0fa808L, 0xa848bb609fa3992eL, 0x8b87f2222fcf1c07L, 0x900e09f5031cc96bL, 0x76a1f60fc295b9b2L, 0xdf716664598ecad3L, 0x28249bb21a97L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {13, -1, 0xceca617fe01d6000L, 0x595cef7d89c50e29L, 0x80df1e314f4eee3dL, 0x705668d0ee11d777L, 0x1129decf85090176L, 0xe12ad8663fe23bdcL, 0x3bdcb4834435ec2dL, 0xb0eb7384b8ccc7e7L, 0x176d20e0ade1ec15L, 0xbcd33a6bde40f64bL, 0xc8cfe0c9721386d1L, 0x3abd50ffe4344350L, 0x9d990abb6d10727fL, 0xe625bac8760ce3e8L, 0xf8de735245b49b2eL, 0xc305f2d69dace6cdL, 0x4f0404bd6521068cL, 0x733ba2e7ff8097b7L, 0x185022c68d1b79cdL, 0xfb6b827d35dcaecdL, 0xc5a20b2b3a97b3caL, 0x2db2dee27454d5dbL, 0x95ef78285d77d3fbL, 0x9757bc3519368837L, 0xcad358ee07e852e9L, 0xcf25bf38fe4c9e5aL, 0x0d708b14ca5e176bL, 0xc60cc4d8f89e1d96L, 0xc9624b9b70c0df1cL, 0xec4863378ecb89f7L, 0xcd8bb777c51fc57eL, 0x8b1ff55546aa075eL, 0xd4d7b389feb9e81eL, 0xf3323d18ade2b94cL, 0x57007862e84686c3L, 0x5c3d448e248e86bdL, 0xfb3fa7be20af3442L, 0x1df5a44deae47779L, 0xa1091db87ed3ac95L, 0xa83b63baee630f14L, 0x10c219ef202e6435L, 0x3c4175aabfe60504L, 0x31b31f7d7e13b9dcL, 0x2024ed4580d9e1e8L, 0x6f01d9945cd13d35L, 0x804a31423fa87078L, 0xbf8697cee1246e85L, 0x381dca9eca19ef8dL, 0x3cedaf0867489a3dL, 0xb2677ffc379b85c4L, 0x77962af5899f8ce4L, 0x37219771de1f0c85L, 0x695ded7ae8247663L, 0x4660b5420886927dL, 0xda6d02a02ac5b06L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {14, 1, 0x86d402231eba4000L, 0x92ad82a6fe3483a5L, 0x623d722da5abddedL, 0x83a1192b961e5063L, 0xc8da24cf131a6978L, 0xc9421d4c6509e16fL, 0x41c74f416ee1d1b5L, 0xf67d339b9d82a358L, 0xbd6f24402001d282L, 0xe8bef96768079e3dL, 0x95a2229a053c3567L, 0x55f0d10a52081d2bL, 0x3d777f9803f45399L, 0x986e660d311d31e2L, 0x1ac1e0d325fe8a84L, 0x78015268ffb153b4L, 0x60fda99325c02b68L, 0x5ab33d9e45958ba1L, 0x9b45e0784136c986L, 0x5ba193b997ef8066L, 0xc25a0f37f7bc0d0bL, 0x4ef485d4f5ee729bL, 0x3c0c66764800dab6L, 0xee3968385f45c349L, 0xb67f81ce0cb4550cL, 0x9618b49e49f05553L, 0x46773b54269be63bL, 0x62dc982eca049f59L, 0x380f44057f3c5b0bL, 0x888689a713e91528L, 0x222fa4a72a80a2f1L, 0x01f7763d06e7dfb5L, 0x104ba940d354d753L, 0x3c9c97707eaaf89aL, 0xde9834ec1d169da0L, 0x2d03932b5d9a560dL, 0x4e7fab6a08e5b90bL, 0x75dd31dc49e74ff6L, 0x4cbb2349b2379a05L, 0x87d72d96470c741fL, 0xecac41ad4f1a029fL, 0x1f0a57a92e815e1eL, 0xf34d394a616ecb93L, 0x82915056df036958L, 0xa66c906f73525d38L, 0x1fc4b37d5b10796fL, 0x7d3396951738a633L, 0x722b29558e2fa634L, 0x702ed6615e729809L, 0x6d92bc4025d2882cL, 0xa7cb675a6f1eb67cL, 0xa5d71fa38509b2b7L, 0x7f1ef529692c77cbL, 0x014f6aab007ad321L, 0x23f0c1f1911a1c60L, 0x124780f8cfb60baaL, 0x7ccdee579774255cL, 0xb2e1e2fab68bd61fL, 0x7f4112f4d89e4994L, 0x4a4L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {15, -1, 0xb0c9b9066df98000L, 0xcb0d1bad0840d565L, 0x1cc29c8b504a708fL, 0xe911b4ae880c442cL, 0x75b342af393d10a6L, 0xda92b14bc6b4c325L, 0xdeb5fa411ec79c6eL, 0x77285d78a791e1e0L, 0x2678a5f22d1a8e8bL, 0x4aeb09eff9ee885bL, 0x17a68d82e9f30ad6L, 0x9b05b8ec4f5409bcL, 0x247b50e050a8c266L, 0x3345e02c139d25bfL, 0xc392d3c27a282dd8L, 0xc096d0f7a788f38aL, 0x9d8f1a9f5e0d52fcL, 0x2ebbd22d8bb118f1L, 0x847979596bd8afd3L, 0x78f6f552c9add9e8L, 0xb2f04cdaac653a81L, 0x5cdcd4a6e98688e1L, 0xbec9686a44dd763fL, 0x22691f2a2d00fa19L, 0x1df6ba3eb4e07112L, 0xc02954aea58b6e9bL, 0x828aee53d4be8e3eL, 0xe204f5b0c783691cL, 0x5fdfebc2f2b39f9dL, 0x88abd0601c5b119eL, 0xdb6b46f84196af3eL, 0x7d3c45c4d69f2531L, 0x012775fe42d6cc31L, 0x3afc94364e7dc7daL, 0x0d6503c564e8f396L, 0xf1f5fbfb286960d6L, 0xd8081e0843600c74L, 0xd95124f85bd302dcL, 0xa232abea9a702bedL, 0xfa89443f6a73e996L, 0x2b3351128f3ad2beL, 0x57fa8bc4dc8d2a39L, 0xd6895bae19675fd1L, 0xb89db334e3b1b177L, 0xa0a5d7e4babdf36fL, 0xec30802a5ab638b8L, 0x228a1070fdac2506L, 0xa56e2f335ecb6025L, 0xa6f2c881b912cf6dL, 0xcbe6c92317903cb2L, 0x43615b991035c102L, 0x26ca79a8ae776dd5L, 0x624cc6d89ab1d158L, 0x823e56b8895970f1L, 0x581966e82f04c1b8L, 0x9f8e2deff1cd0f08L, 0x0ab558f1a488ef71L, 0x6bb4b20360888bf7L, 0xb1f7befc85831a9bL, 0x7221ee04bf5e7c4aL, 0x8cdab2c03eeac2ceL, 0xb42f69a6347ba40aL, 0xc4b8e55ad406736cL, 0x1942d79L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {16, 1, 0x75dfc373a7c10000L, 0xa16dd200d5c2c5b6L, 0xbe82e49c895270dcL, 0xdb534630489b9761L, 0x9ea4353da45e1281L, 0x5b5c1957c2786a84L, 0x1c8041e402627d16L, 0xff264cb6c2d9f41fL, 0xebd94d72529ee5d1L, 0x69d59f6ae97ea5e7L, 0x326021c19b8924e6L, 0xa3f3b7dc1f300df2L, 0xcd791f45434b69b0L, 0x59a038c9b60122c7L, 0xe0e23e20f6109f0aL, 0x8b0ccf106881b171L, 0xc2c7a7322bdb7350L, 0xb677a6a6dada0d3aL, 0x5b3e823e97dece3eL, 0xc6de7737d77abbd5L, 0xf4d75f7851d6d555L, 0x7b12d608ac509c37L, 0x217bfc5a9e5aa107L, 0xc17eb1680426c5d2L, 0x90976a14c41aa105L, 0xd8a7206571d7e716L, 0xe308be8bf8ff0c69L, 0x56864231618c36daL, 0x39849baa8b7e6801L, 0x602a6585021356f0L, 0x6bad40d52b53c481L, 0xb17b24ed2ac732f2L, 0xa5c48afa4ef5df6cL, 0x5dd124a930513469L, 0xa5875c44de1ded32L, 0x888e6d69553274c3L, 0x34315eac6acceb45L, 0x9735a420af6519d9L, 0x565c333f7c334cd4L, 0x8a345428d1cb5d74L, 0xd205f132f28cc14fL, 0x77d00e530b359003L, 0x668247c81cb68815L, 0xde09f660ed1939d9L, 0x5e59f73fdf9b9f82L, 0x56cd86f91f0d6fb5L, 0xda28962df1f4b8f8L, 0x3cb5a29186cf720eL, 0x25f0ccc361ae4d49L, 0x8e4f4f9e53555c0eL, 0x6287672fbe93b3d0L, 0x4c4bdda4b67d0596L, 0xbffa17a294329339L, 0x2f9f5fb1cc5ffd22L, 0x73b3be42e8b756bfL, 0x7748ea761c3bb514L, 0xeb609c6ff3b6ad3bL, 0x1065aa3c7cfb787bL, 0xfdcce6f293a962c4L, 0xd14bdb3c42aa2b02L, 0xe95f79c1b219fefdL, 0x28ef6f60c9a8c06eL, 0xdfdc523560e6f510L, 0x7df2d80596a4a1cdL, 0xa2e1024c6c5d7a69L, 0xc466bd9fe014cdb8L, 0x30ffc891d53d3c0bL, 0x8973567713L,},}, {
            // m: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x388994c1d48a51eeL, 0x1f8232593c8efc09L, 0x08d3ad2a9db08aafL, 0x4918584bd96c4ec8L, 0xed546c70721b4acdL, 0x558c6b609e78L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {0, 1, 0x1L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {1, 1, 0x388994c1d48a51eeL, 0x1f8232593c8efc09L, 0x08d3ad2a9db08aafL, 0x4918584bd96c4ec8L, 0xed546c70721b4acdL, 0x558c6b609e78L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {2, 1, 0xe87c1b4a9ed07944L, 0x6f951a6d11f10321L, 0xc163bb2e41a87b82L, 0x54eb72686c4cf615L, 0x231ebe88786485c9L, 0x77ae86a93de365e8L, 0x6ba643b9e49a1d35L, 0xe092b0477d37cb23L, 0x31250984a11cf2aeL, 0xd2446af60740202cL, 0xc7f38acdf78956cdL, 0x1c968c53L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {3, 1, 0xf06cf8b14ad74138L, 0x26aeac9aa84c4023L, 0x6111ba36cf80105fL, 0x6719afe0987bdc96L, 0xb5c852f0d250e272L, 0xffc798f4dbf46706L, 0xacdeff7a20348e30L, 0xf048a607103be413L, 0x45a3696c54038b41L, 0x3154b9a00f3d7503L, 0x0cde18710e6548fbL, 0x2a19c9285e4d1ccfL, 0x9b846bad38b23149L, 0x354762a9dff26aa8L, 0x65a0ae959b819f8eL, 0xd3de20a014f71de9L, 0xaaea460345f25c45L, 0x98dL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {4, 1, 0xe68072c537f15a10L, 0xaeabbf1ee1ab6488L, 0x57be5b96621493efL, 0xc309017a14e2ba0dL, 0x5569ba2701c36b9aL, 0x05a706384089b848L, 0x969b0643f7490e12L, 0x5d038483e351def7L, 0x1de3ac59b17d6cb2L, 0xac5857c79df9e8caL, 0xc18eff4de61e3dbaL, 0xf40698ee88929b9dL, 0xaae7de523da61effL, 0xacbdf22b02e6325bL, 0x5fd2e89666a7a957L, 0xd545359e345782f7L, 0xf8e3fc43f781afd1L, 0x97d83971d3d541f9L, 0x76fd928e9d695287L, 0xb63ed97cbbda02f4L, 0x91c9c1577a04e9e0L, 0x5b6df1b87e4eef49L, 0x331473b12d755e1L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {5, 1, 0x87f546cd2c80cae0L, 0xfd50a29f72d199f1L, 0xd7d26134e723762aL, 0xa23433b885b3fa76L, 0xa5b0ae38ee597991L, 0x4631219def2c3714L, 0x30e023142411c80dL, 0x0354bd11eee1b8bbL, 0x4aa2cbf8887c9038L, 0x43ce38251ca3e1dfL, 0xb979c7ce50aedc22L, 0xa7b21a09c2a475d5L, 0xb51b91b9115674d0L, 0x85187ee2a18050a5L, 0xd80e0c82c4928d6aL, 0xac0b9d55cd34e4baL, 0xa92af27559d75f20L, 0x228ebf9d07bda72eL, 0x47ab58347bbc05e9L, 0x76cd68c5f84342b0L, 0x87a36accbd5ac56aL, 0x7027cfd84ec450deL, 0x4b41487a710f447fL, 0xa3d36fc83488fe06L, 0xe55fb8addafa28ecL, 0x9b44828c7f48dd5dL, 0x46ee9973c6937074L, 0x20536207e3e8eff7L, 0x1111cf05ecaL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {6, 1, 0xa925d622fcad7c40L, 0x0a2dd35d843a9101L, 0x2c1b6275f16441c7L, 0x75698c61b5b6a358L, 0x020808696e66a6e9L, 0xe4496872c8bc4ceaL, 0x8bcf349cdd6968f5L, 0xf3d41270022f4ef1L, 0x394590d99951857aL, 0x6ae4b1f8d7527885L, 0x15ca813b7da4759cL, 0x5106d33a85d1ba35L, 0xa404cb1e3cd4bb07L, 0x8ded4557fe2aa450L, 0xac0250c9983874efL, 0xe04231b61cfcb8d3L, 0xabee560869c3ba71L, 0x82c4400ade5a2cd2L, 0x8e50aebc6e40492eL, 0xf4f8b3809a9f66eaL, 0x080b14c5bc9d5481L, 0x3ad2cfdcd9b58feaL, 0x650251b02315b2c1L, 0xba777a7f5f5d2acfL, 0xa7e68f2c56c28a9aL, 0x714bd225ae975c25L, 0x0dc9e5c785c4bb0cL, 0x20f919d44b73b321L, 0x01cc64d85e1cbf14L, 0x434a520256f7e85aL, 0x67cc4939245c4becL, 0x1191c79c4c66dfd4L, 0xaf466be96358c3d3L, 0x311955e6817e19b9L, 0x5b446aL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {7, 1, 0xeae9b323c819c380L, 0x9a196f203a870e84L, 0xc87ce60661bd0429L, 0x99be018257ede660L, 0xd725ec686db4dde4L, 0x2fe111c5bedf8b1eL, 0xb19940bf92e30fabL, 0x474e6aacc11e3ebcL, 0xa76142361c87913cL, 0xe9f5c39b715050b3L, 0xac9eaa63e9eadc2dL, 0xeaec3be3ddb91ce6L, 0xf1a3162be050b5c8L, 0xaf30b807c39fc05fL, 0x10cf8037a423c01bL, 0xc4c2ef9022618b6eL, 0xbe6b9435bc424282L, 0x8eedc127ce303649L, 0xd67f3f6b5e14d966L, 0xfae463a8617c208aL, 0x253f575a6f61d3b7L, 0xd4bbbfa33f7e463bL, 0xc7c32e0b5a1c1ea5L, 0x2607ab96fe207db8L, 0x7e69dfe7cfe9f9bcL, 0x791c48497ff79826L, 0x146846bf7c9bbe99L, 0xb50da67740d22c08L, 0x48c5b95c26f6f90aL, 0x4fd7295a496797a5L, 0x3e0c5e272a586f62L, 0x1e6e3ccb91c9c28dL, 0x846f00cb5908367bL, 0x98a2a001e18a59efL, 0xc6fa3d0b815a1337L, 0xf545eae4f45905c8L, 0xb8b4255db4155d5fL, 0x290b928a45f7d93aL, 0x50357ef5f216635cL, 0x7fc6f46acd0ebbf9L, 0x1eL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {8, 1, 0xe9e474dd91cf4100L, 0x63d30c6bc95e0df0L, 0x5d4059072d038311L, 0x5fce035d58950c5cL, 0x342493aaeab7caf2L, 0x04ed8dbaed26b0fbL, 0x1aae842c797d2a85L, 0xd7501e2a43a467fcL, 0x34ed94aa488d4ce9L, 0xc574180a9ea0ef59L, 0xeee0f29cf0befb99L, 0x8e423b274676684aL, 0x5ddb027bcd008e1dL, 0xe30c3f3573ae277aL, 0x0a6252461696c9d2L, 0x89ae4ce5e40de0aeL, 0xab78943934f7af26L, 0xcd2e898c5f64c719L, 0x70656339abce59c0L, 0x30b790bfca361460L, 0xc2746e490cf3c073L, 0x77acbe5cda4071c0L, 0xd6e54f6ddfb2df3eL, 0x702b7018f5fe9097L, 0xd799c19565ccfc6cL, 0x0e1d0536de38fa85L, 0xfd415a536be9b128L, 0x7f09db86d25728efL, 0x311f751ad5d24f68L, 0xe310fb3adcbd0e07L, 0x809daf7a9e3faf04L, 0xe523c5ae4e8e7986L, 0x8218c4f0f34e4abeL, 0xfd8468ec7076fbb2L, 0x1b1d451795abc978L, 0x1a8d2cf562c937bfL, 0x57e55e22e9485e5eL, 0x96d839ea161d895dL, 0x3690197ae44f86bbL, 0xd3d7f379bf00c5daL, 0x5893fb6a0496edb3L, 0x002de910380af8aeL, 0x920927289e13abf6L, 0x24c7aa06c975c1afL, 0x4ab1278d0a8a6ab0L, 0xa3127bae01487L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {9, 1, 0xbf62fdad2c3f6e00L, 0xedf44a5a9af35061L, 0xc443a04b916b5b4fL, 0x994b8356a980a01fL, 0xca040105453b9722L, 0xec08021388868576L, 0x01e1f05a73bc4310L, 0xe0ffcc570f6842ceL, 0x678d88317664a69bL, 0x25e8c858aa4559cdL, 0xa879fe183efcd8fbL, 0x5b311023f2914a40L, 0x0b8dab7396921604L, 0x8e3b9b18c0edb5d7L, 0xa58f714f17bafa1bL, 0x883115325fb35ab2L, 0xa8a0f7e8a7bdb6b2L, 0x4b9c680225269d44L, 0xed2c4a489d3d6464L, 0x5f6d1e7208d6406fL, 0x352d8600b549f732L, 0x71af3bd168f1aa70L, 0x2c3e26b98579665fL, 0x51bfe9f50ac51519L, 0xdb2c59b7232754a8L, 0xf31b80a74dfd891eL, 0xb2f41ab40107cc92L, 0x1dca355bb826960cL, 0x431e1450b7bc4325L, 0x86fe00e20cb09e9fL, 0xa2c57934391ddd16L, 0xde203b3315d668faL, 0x91e621603e5bacf9L, 0x13ff2947ea35042dL, 0xee8970c50fe31e03L, 0xc9dec017ca2a9539L, 0x443c91227415d504L, 0x4b9548c78fa6a9d0L, 0xcf424574a5e260f1L, 0x87c01fd1aa60cd64L, 0x2dda3227a25fa0e9L, 0x96314c1b095015d9L, 0x7999e03d653d4e7eL, 0x8669664f89215c4dL, 0x5832c3455924ab92L, 0x15ea3e500287422fL, 0x33a675fe06e3366bL, 0x2a1a8dbd75c0abd2L, 0x410fa4cf8a02d6abL, 0xfce50cb2a8dd0f18L, 0xf284e53a09ee36d6L, 0x367e9592aL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {10, 1, 0x4db3f14880c64400L, 0x9e0a0a677bf99703L, 0xda1d2ebfff0e1c1eL, 0x32d20d9702e35afbL, 0x6c0f5b408cbc9accL, 0x382eed383e986a03L, 0xfc81e3c42aa139e7L, 0x05562118b9edf369L, 0xad4c4db896c0a00fL, 0x3d4c1be1ca13377eL, 0x67f23d1453a78b08L, 0x6bb7111f13f8fb08L, 0xba0df94a3801fe00L, 0x827ccfc488602f60L, 0x3a4f40b05d8d411fL, 0x1b29e8b7ec1a5ab9L, 0x9cc29615fb489eedL, 0x46eb811a1be23470L, 0x7aa717f7406f1e42L, 0xa09c124ec55a6f4eL, 0xa701a97ddecb948dL, 0x028d615c1901d80dL, 0xc08b6a797b91d1e8L, 0xe76ae30b1bdbbd4aL, 0xf3561a9374876543L, 0xf9e93c875d59ffcfL, 0xc7e4ee6cd4ff559dL, 0xcba6757a2a4fdd17L, 0x258df95398cfe013L, 0x0324c0bc42754002L, 0x1ee6d10c31c50c85L, 0xc9f52384bb3679f0L, 0x559e7585989328aaL, 0x24025b3d7eac5d8dL, 0x8f454ec96f846256L, 0xffdec61a31a249b5L, 0x56ee6e9a565fd0aaL, 0x0ca3857105834aeaL, 0xd07b5853355af369L, 0x759706807dc833d4L, 0x151cad33665fd5fcL, 0xddef9f9e6cddda40L, 0x44fb42cf48e18dcdL, 0xcbcd4af09900bf0aL, 0xbe84e0caed7da6c2L, 0xb765f4b98aadc814L, 0x4b1492f9167be35dL, 0xf2e1e70c9fd50933L, 0x6158c4107b9fcb47L, 0x431cd5dae2db4dafL, 0xd635ac4868e9b323L, 0x8a0fdae3d86ab821L, 0xd0cb884f56483441L, 0x0fde33fa059bc303L, 0xc1a6a2b68627a8b1L, 0xe31f8a22bb82d3f7L, 0xbbefa18706fefdf1L, 0x1235eL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {11, 1, 0x612521571bd73800L, 0x6cc308c060fad68eL, 0x6c45419cf73cb808L, 0xe961fafa3c52d2abL, 0x9178fb328faabf1dL, 0xfe5b81325b0e356fL, 0x38e11e31c38de432L, 0x21511eb15d0ed304L, 0xbb1bc8c550d543ceL, 0x901068b559e0d48dL, 0xcbe171f1e99dc0deL, 0x830f1b3247f060ccL, 0xb410bd88383bf1bfL, 0x4df93a0457207eb5L, 0xf8cd35dfc1d3cc8eL, 0xa331ad6844cea105L, 0xc3544e30265d8fe9L, 0x27577950ece024efL, 0x1cde9943f5a375c6L, 0x9390dfebd653b2ecL, 0x6282a181eafea6d5L, 0xbd277dd7b18d6f0fL, 0xbc21ce707af9ee8dL, 0x607a0b4fe9f996e2L, 0x592ff4e442f99d6cL, 0x5747a7a640340bbbL, 0x160305e4cc55f220L, 0xd2ce44d54201ecb4L, 0x133cd986d8f2411fL, 0x52d42594cd22a525L, 0x530e7a5bf193fb73L, 0x7b5bf9e1723a393dL, 0x62069c358c45e81fL, 0x23c61442b57ea9cdL, 0x5dab701323c649caL, 0x0a3620d541deb06dL, 0x24fd7c86481e78a7L, 0xf8759850b77aac95L, 0xf666b4401bbf8ea1L, 0xd5547ca47cb6d6c0L, 0x4a4c9eec56cc3cc0L, 0x8721cd170d5d3d13L, 0x4c135daf7b174e82L, 0xc03109a14a9f1bc6L, 0x3fc475762fd3194cL, 0x22795cf0340eada4L, 0xfae1dfc978f52d93L, 0xd64d2964f1812d8bL, 0xd389cf937f3317c2L, 0x0eface44c3fbc51bL, 0x303c336da2604219L, 0xdda3a893fbf34ea1L, 0xc1ce7a4445221551L, 0x921538f54e7f60ccL, 0x89cc3d2abf69d85bL, 0x2e6afd425076f6d4L, 0x66b668f2ed9394faL, 0xb9ba271a3859aeb0L, 0x6d18c2273e62df9aL, 0x7225c9c3da5e821eL, 0xc799c0ce87ca2fc9L, 0x48d6e7d73d9850c1L, 0x615e466be81fe80fL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {12, 1, 0x8db6032f2ace1000L, 0xfa6d761297f413ecL, 0x578843af86a7dbbcL, 0x3dac4f559b726602L, 0x025c241981691f02L, 0x2f5d2229e02339adL, 0x80da342302cf99e6L, 0x50832f8b7ab542fbL, 0xae8f5f316b6d91ceL, 0xe4671d8701edb467L, 0x1f49715080c077a0L, 0x731372837b461289L, 0x94298862957939e8L, 0x0246649b21d707d8L, 0xff5c5edc51e024f9L, 0xa518ef8c88c80591L, 0x1fcd950a39f45c84L, 0x13b5714f38e01a55L, 0x061f9e5694fa89a9L, 0x4e3c901eb1682956L, 0x0b6439310b628d6eL, 0xcbdb257e6aaf902eL, 0x48ad74382bf2d862L, 0xca0d2cf8813a4fb0L, 0xfe01b1970aeb4ad5L, 0xd7f0ac9331162f58L, 0xd039584f4f257718L, 0x440dc7f92a8caabbL, 0xeff65c2735fde8f2L, 0x3beee418d9dbf71fL, 0x4669df4d112016e4L, 0x44123e6160692624L, 0xaaa937fa08fd398fL, 0xa713e740f914925bL, 0x3e17ebf2400974afL, 0xb229aa49aaf07ed9L, 0x24bc20af76be3494L, 0x64eb2dc78040dc18L, 0x9ea4b4ee7e5088d6L, 0xe9cccab66c04f7f3L, 0xbdafd7b82b84fab1L, 0x467416ae82b9bebeL, 0xc486af39907063edL, 0xc3c0157bfcdf635fL, 0x7de7317880cacef9L, 0xbddfb5a8717f4e70L, 0x2aa6b2a6fe935ce0L, 0x66aae23f69ce3eeeL, 0x32a924c2c2c97f87L, 0x80cc5edb7bc576daL, 0x2bb85fb7bfe35cb3L, 0x5df53c9c8e532ac3L, 0x75d118b183978b80L, 0x3fe996a08d643597L, 0xbb7945acfc8bccb7L, 0x9f09b4cf76573019L, 0xb829d76254f17aa7L, 0xa85ecf37e700c15bL, 0xc28ab994381ca52bL, 0x02b6ebbbf4027fbcL, 0x5f574a69adf3d67cL, 0x88b56c691daa64c3L, 0x0106e81c1d85a572L, 0x0bfeb13ab6daf50fL, 0xe73a12358a43ba5dL, 0x661a1556ee59d8e4L, 0x101b084f3f6c5c66L, 0x39e85cb8a58500f8L, 0x2089b5c77e25L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {13, 1, 0x13b2aab99ea2e000L, 0xcc9944a670c0b498L, 0x11340bb92bf4f869L, 0xbe46fe915f8869b2L, 0x59a17d2c24948641L, 0x4e8d6e5e92374756L, 0x1792942f62acf8d8L, 0x23ea7d9430b80ed1L, 0x41c5811b5a84923bL, 0xee91aef9267e0a7aL, 0x8c21bdae5f586de1L, 0x7b3c5cb6a5818934L, 0xc9af3549e0d33eadL, 0x132c20267349a7d6L, 0x43c2f47c0aa73319L, 0xdced0e80bef84cbdL, 0xbf0ec5d77d9282dfL, 0xcf11f43ecc9d40c6L, 0xed90069b992c9ae1L, 0xef8652113d2e832eL, 0x25d579235fbc80adL, 0x7c1713cb7797971bL, 0x870612f59a6e2ae8L, 0x8da42d8becf8cbb8L, 0x18418f2430b331ecL, 0xa62c05c4a7418befL, 0x1e385492372bce77L, 0xb92ca7555a825e29L, 0x9e4d13e5e90be55bL, 0x7ccab5b536b346e9L, 0xb7bc0d4a732aa1fcL, 0x29d422e3e123d20fL, 0xf9574c2a6cc8e7fcL, 0x4741ccb60fbb087aL, 0x2e5fb8bf148230f5L, 0x0d14be08ee264c81L, 0xb1daa7190eee1c86L, 0xb8beb7f3ca6dd6c3L, 0xe809beff49108c9cL, 0xbf45ec94c608413cL, 0x07ef65714c8f0aaeL, 0x913fc8c2c423c335L, 0x0985734f168b346dL, 0x81de302e78bda061L, 0xff8d5ae0d2741f6bL, 0xc28555f5df0a7a04L, 0xf5e1658c12560bcfL, 0xc858733752ee0e27L, 0xbf4de0ba9ca7b1c0L, 0x077cda9abc634961L, 0x9ed1c86f3516e5e6L, 0x7cec74d505960971L, 0xcea4bb57e3bc5619L, 0xbb07fed0175d4f00L, 0x30478aa71b67d741L, 0xbedd8c8d254af5f9L, 0xffff249708b72fe8L, 0x5cc4e21f6c30b1afL, 0xab5840464c61e9a0L, 0x9d55495e1dc06829L, 0xf376524098ca9675L, 0x5c7376223548d219L, 0xff5f098060e62ce0L, 0x8d5b68c7ad1c7badL, 0x82c75e9981a19d38L, 0x79171d877dbc2de4L, 0xdcb88dff85ec93f5L, 0x2e98edade8197984L, 0x5f518b161c35c0d6L, 0xd94fb7f14a8f7293L, 0x4f51aec0bd617fa1L, 0xf1eeb8b79f268eb5L, 0x84ede61fb5bbebdcL, 0x7cbe42073ca5adb2L, 0xadf9250L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {14, 1, 0x39d8b90fc44c4000L, 0x5b8b4f605b400320L, 0xcbc011da8a52fa12L, 0xf1ac706a8ab9130fL, 0x2eff0ce55573c7a3L, 0x87564faedfa59283L, 0xfe5168828a5faa5aL, 0xccd6e690a4bac592L, 0x2fac5e221348a806L, 0x51fee88834adecceL, 0x02851c64de9dda6fL, 0x49b6d2801ac38e91L, 0x93b1836ddcf788f2L, 0xd80bb8acaeb28e7fL, 0x17a66290002af1aeL, 0x56f3ccf7b691fc17L, 0xbb74194c6b3ece38L, 0x64997a7d5e9d8934L, 0xc97ffbf4e36348ecL, 0x9b38cbe08d90a3d0L, 0x85a8cf88c1c6097aL, 0x78e43b3f6401b53dL, 0xfd4edb659439da1aL, 0x629cc279c05b36ddL, 0x61b8ecb5dbc70380L, 0x37117931ec1708c8L, 0x24493c9187f64882L, 0x8024f56180a391c1L, 0x0785b09ba0351a19L, 0x33dd37dbcd25c98aL, 0x137fe359b7cfe37dL, 0x4710b858a168802bL, 0x8ca8de1bec30cf4bL, 0xbad03f2bb9250114L, 0xfc78e6768189a731L, 0xa18f5075db57b32eL, 0xaba9aa8cfa4beaafL, 0xc6b0d09f5ec989bbL, 0x5378792ccdf4616bL, 0x8c2f379013dc32daL, 0xc0d70f6ba7279effL, 0xc3221fd83b08c28fL, 0x079bb987c1441b8dL, 0xd34d19f428320198L, 0xcf551408f68a9abcL, 0x5185a7a9b01493c3L, 0x46db0cc32ba9d86cL, 0x8a30f8be044ccdbeL, 0x3abc399f60b5efa0L, 0x34c459247723c07eL, 0x253c3a268c8a3115L, 0x4cd14af826743747L, 0x9a21a9da5f619436L, 0xf6b3cd60624a00baL, 0x2aa516f4c235ef75L, 0xd4dbbc245d883cebL, 0xbede477f1da1b857L, 0x1decf0a85151eaeaL, 0x22524c5f4248f896L, 0x7f1f9309fd6cc63eL, 0xab9745405bac0550L, 0xf21d6ec20c00ef71L, 0x416a99b188e5022eL, 0x031f0bfe1c4de3bfL, 0x00c0309af6cf54efL, 0x18d90a642a98570cL, 0xe4310e8b3307e7d6L, 0x1c2c66f5238dc3ebL, 0x87832fc07157f620L, 0x3ea4e3b5013d9c65L, 0x8dc312b5dee5e40cL, 0x34b2411789e266a4L, 0x0287cc7d52a72a89L, 0x948067bc6f2debdfL, 0x36120d7e0340a683L, 0x45d5d59111003c9cL, 0x42d82da1de2c1e95L, 0xfb3d627df6f633bcL, 0x7a8e6d9ff1f69522L, 0x32684a290579a32dL, 0x3a2L,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {15, 1, 0x7d953fdf1f238000L, 0xa5a7ddd44f365f85L, 0x548c1e2018113127L, 0xb521264ed233c4f2L, 0x566d6c31cd380734L, 0xb68a2f7a4790c7bbL, 0xc05e970c7685057bL, 0x25e24e5d422a8567L, 0xf33fa40390aafabdL, 0x1119d2e13327bcfeL, 0x23a50c3f40c62b7cL, 0xf7ae1493f84bebd3L, 0x73606f3645afe940L, 0xd7ffc0b068c4d299L, 0x6cd8e1787ab09baaL, 0x15dd630c951062c1L, 0x1accb6aad412262fL, 0x95da54f83975776bL, 0xa74036064bf1b618L, 0x02fc9e0a2357662fL, 0xf311723c66f2d5faL, 0xd79e71998760a72dL, 0x59e9ab6bc4089440L, 0x6d96231db5768b52L, 0xf451412288681e98L, 0x7a961c2569572398L, 0xdcd5146d28302cf3L, 0x7e0b380fe509d5abL, 0xf564c709e199c988L, 0xfd5cec83ce79530dL, 0xbfc6fa9538b0c461L, 0x020a34b4d555c9adL, 0xec09732fe7373055L, 0x84e42c92e4d5b1c6L, 0xfac30feaa4c9e77fL, 0x78003e0d1ab830c3L, 0xe7cb7411e521c5b2L, 0xd0de84780c947cefL, 0x17a4a75a42a58f7eL, 0x92ddacd0ef2a696cL, 0x0aef305ce6af7ef6L, 0x05f17157f7337c2bL, 0xaefe3c4259cb07b2L, 0x155ff70bf10e002cL, 0x7cc1815de5aac64fL, 0x85f5f5e3e213d559L, 0x9a84d91fadf517aeL, 0x4be4d0c4170126e2L, 0xeadbd2ec4d132b90L, 0xc20af96c3c0e9e04L, 0xaaa3a8702252530bL, 0x649af0540a15b63bL, 0x5a4033a6f6b74bcfL, 0xa5f04602214fd970L, 0x06253f34da9f13bcL, 0xa0acd900162e02c8L, 0x03016c40ad3a5b59L, 0x809531e716e2d101L, 0xf89342e3ba229f21L, 0xd7d0ca081ad6ba38L, 0x08d7ccfa8e349da3L, 0x9ccf7b9d05f01092L, 0xa92d020561fceb17L, 0x575ccdba0f1e68b3L, 0xd6f3d8b1c0123fe3L, 0xb04892cd4c8e669eL, 0xfcf0632137df6620L, 0x59fa85231ced651dL, 0x1dbaa929648b96e0L, 0xc5aecb42a90cea28L, 0x940cf0074367462cL, 0x4232f125b2a85f5fL, 0x54a61e8b47f0cb62L, 0x9e7b18833c7fe0f9L, 0xd34444ec05ea6fe1L, 0x4a8fa7597abf8abeL, 0xa9e53fc0565f0f60L, 0x54a5c174721b466eL, 0x438e617a9810bec3L, 0x68fff1e428526c1eL, 0x997e8a2bd3583991L, 0x29a8fc8e2cc93e1aL, 0x51bf433bd794bce7L, 0x10e40244fbc04a24L, 0xb0746cb6215ad1c0L, 0x136d8f65bcc7dddL,},
            // mPowN: { n, sign, magnitude_0, magnitude_1, ... }
            {16, 1, 0xadcaa96c2e810000L, 0xcef22721e0a46966L, 0x2b49711c80f69e90L, 0x4fa614e9d231d640L, 0xc6cce38813156548L, 0x7d4ebaf6184c8a94L, 0xf5d331d90bed4f58L, 0x62e3fae22a0de951L, 0xf82d86b51fc479f1L, 0x3736fa8968cbcbb3L, 0x9c646ed87eb37a2eL, 0xb35e9dfc3541d1e5L, 0x86608c26f25f85afL, 0x9dafdf34ca424c29L, 0x7c61c8f490073313L, 0x3e9f4f44cc47275cL, 0x405971e07e95a324L, 0x3a8f6400de1e3d7fL, 0x2c2962acf9f65d05L, 0xc26448fc2423530fL, 0x52c8e145eb8fbb3dL, 0xb80c80f7427cf2aaL, 0x95544cb4b930cef8L, 0x03af637252f8e5fdL, 0x4dc53ac90f3e3b35L, 0xc5b9a1f098305cc7L, 0x08a8f4b77a6d9cf6L, 0x7e462217d1036e1cL, 0x3252840088c282f5L, 0x9530752217e5dcc2L, 0x190eea744c2f998bL, 0x1b6fb435a08665beL, 0xd62565cb3d723754L, 0x69abe921155589edL, 0x94e2cab98454a75dL, 0x1d61d1af79e6b4ffL, 0x49cdfd2568b22798L, 0xc96aba843c9b9bb4L, 0xed1df5fa8074d08cL, 0x93bf428a0c0b1bf8L, 0x5c99e798b490e376L, 0xeac0a6f667afc0bdL, 0x8ac048d51703934dL, 0x4a27f55560dc08daL, 0x48f599d833e252daL, 0x2b05f6397b7f5414L, 0xb0ae35a19a1d0c1fL, 0x7b28ff33f6c84749L, 0x0bd67a82e289b655L, 0xe935a4ac09344347L, 0x705e9eba3af39a71L, 0x7dfa2dabd5b2a4a1L, 0xdb3e9306f884e4c8L, 0xa2154b9aec35a418L, 0x0ab3a5c2e1592e4fL, 0x47c82c832ce82b0fL, 0x877eb9e4b5409ec9L, 0xb9c7c98f45dcc3c5L, 0xeaa79ec9cfb96b04L, 0x05af00c9dcbf94b6L, 0xd7371aa8692cf316L, 0x6911afc2d4992c89L, 0xbfbf42e9303d4509L, 0xf73d4b1085b4b976L, 0xb481118b0256c9c2L, 0x9a0323ba5e24912aL, 0x4f34be24de08327cL, 0x40c350c6119a4350L, 0x76c273397a19ca47L, 0x6681e81d1c5fa811L, 0x1e9079d2d25b25d6L, 0xb7c58216173aeeeeL, 0x225d682861b315adL, 0x6f0bd227b53581d4L, 0x8a9bd4686d866858L, 0x3f79c805402d84b8L, 0xd816e0e76a7a8370L, 0xd25dae3c34b9a13bL, 0x7a4eed50cb00fd95L, 0x500e1de5eafa5988L, 0x5bcf4ed3bf7b46e7L, 0x452497b08a2d5ee8L, 0xd2bd3e722d43afeeL, 0x1788c97f69d4018eL, 0xc9056031b7d9117bL, 0x4ec5bd66b14b3032L, 0x5ba4676c5d59b51fL, 0x0b38197211911dd6L, 0x23c447155bb60535L, 0x3dbb0a77960b9065L, 0xdb1e43a031146c7cL, 0x67e08ad535L,},},
    };

    /**
     * Test of pow method, of class LargeInteger.
     */
    @Test
    public void testPow_int() {
        for ( long[][] data : powIntDataTest ) {
            for ( int j=1; j<data.length; j++ ) {
                int n = (int) data[j][0];
                LargeInteger instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
                LargeInteger expResult = new LargeInteger( (int) data[j][1], Arrays.copy( data[j], 2, data[j].length-1 ) );
                LargeInteger result = instance.pow(n);
                Assert.assertEquals(expResult, result);
            }
        }
    }

    private static final long[][][] powLargeIntegerDataTest = new long[][][]{






        // TODO
        // TODO
        // TODO









    };

    /**
     * Test of pow method, of class LargeInteger.
     */
    @Test
    public void testPow_LargeInteger() {
        for ( long[][] data : powIntDataTest ) {
            for ( int j=1; j<data.length; j++ ) {
                LargeInteger n = new LargeInteger( data[j][0] );
                LargeInteger instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
                LargeInteger expResult = new LargeInteger( (int) data[j][1], Arrays.copy( data[j], 2, data[j].length-1 ) );
                LargeInteger result = instance.pow(n);
                Assert.assertEquals(expResult, result);
            }
        }
    }

    /**
     * Test of floorSqrt method, of class LargeInteger.
     */
    @Test
    public void testFloorSqrt() {
        for ( long[][] data : addSbtractMultDividePowDataTest ) {
            LargeInteger  instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger result, expResult;
            try{
                result = instance.floorSqrt();
                expResult = new LargeInteger( (int) data[7][0], Arrays.copy( data[7], 1, data[7].length-1 ) );
                Assert.assertEquals(expResult, result);
            } catch ( InvalidArgumentException iae ) {
                Assert.assertTrue( instance.order(LargeInteger.ZERO)<0);
            }
        }
    }

    private static final long[][][] extendenEuclidDataTest = new long[][][] {
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xbce7a1cd06502b29L, 0x78abe5a3b67bb168L, 0x118565d1cda83b5cL, 0x6b30880c5ae4d667L, 0xa179253974d16b0aL, 0xcb1946a25bf210b2L, 0x5f450533612dd769L, 0xfa54818d3db6aae3L, 0xd0a721d98f735e58L, 0x15856a9c6445b126L, 0x2bfa9cfc1241243cL, 0xf6f3be354c40bbb1L, 0x84a4633134646a1dL, 0xaaefe57f93c44684L, 0x651a6e1cfa78a600L, 0x62L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x659012fc9a9e4a5eL, 0x369fe347b9c852d4L, 0xbd578cd55d0cf7ccL, 0x21e88e696e09aed4L, 0x20b87c8a4bcf3911L, 0x82504036efb2ed1dL, 0xba3b4b4cbef75412L, 0xbc8183c4da01731cL, 0x079bc26566694fb2L, 0x2e79448812b68e5eL, 0x5cd703c54ba2L,},
            // a: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xdd47488a87dd4de1L, 0xa25f165642885c1aL, 0xdc39f37011651af6L, 0xd4170aeff996b410L, 0x516e0aa0009373e2L, 0x57a31a5c0931dbc8L, 0xe42b4e512e80be43L, 0xbf41bf731bd9ea69L, 0xf7553674b0d40318L, 0x5d5608f52aca7705L, 0x29a0984eb70L,},
            // b: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xe3e78f7654d23c31L, 0x8a7d92e8c770ad3bL, 0xb7c3fb8fd442b9d1L, 0xa9804ab9b75bf585L, 0xbea1e08eed90ffedL, 0x9c5eac53a65b4af2L, 0x2e243c23d8b8d9a1L, 0x7f6b64cc6b97ed33L, 0x3027e8b9afb27722L, 0x5f2b838065c56daaL, 0xc4e151b631472915L, 0xb3dfb9b9f09f94cfL, 0x745aa7e083506ed6L, 0x13dacebb5ffec7f7L, 0xc1e3a3aeed709e6cL, 0x2L,},
            // gcdM0M1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xbL,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xbf943524eda1e2a7L, 0x844be79be3643eccL, 0xfad2676ed3b4c9f7L, 0xcff6b137cb7bcf59L, 0x7fd27098ae1f98d1L, 0xdc8693fa7d9f68a8L, 0xac8e3a8cce188447L, 0x1c942cL,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xa0d7f414a65e0680L, 0xf8f19efd45149f81L, 0x5f34506ce2ed6134L, 0xbd9c1ddc668afd79L, 0xdfecc6L,},
            // a: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x56e41d00e3470069L, 0xb0cba1a43cbf0bf2L, 0xf6a1a35a67f9e460L, 0x7da1637203bdced2L, 0x60edddL,},
            // b: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xc6cfdbe8dbe2a161L, 0xa0a3d98e055f202dL, 0x8ee951c4dafbbcfaL, 0x3c28ca7869ab1108L, 0xd06d87ba9c40cadcL, 0xc3d97069c53f0215L, 0xad5cd0121de60e11L, 0xc5ee9L,},
            // gcdM0M1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x1L,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x4eb845e3fcL,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xc8L,},
            // a: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x1L,},
            // b: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x64c2e8d2L,},
            // gcdM0M1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x14L,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x537163fc3c242e8aL, 0x3b2cfeede971L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x8dL,},
            // a: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x11L,},
            // b: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x6a49960a708e5921L, 0x72279842c7cL,},
            // gcdM0M1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x3L,},},
    };

    /**
     * Test of extendedEuclid method, of class LargeInteger.
     */
    @Test
    public void testExtendedEuclid() {
        for ( long[][] data : extendenEuclidDataTest ) {
            LargeInteger instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger        y = new LargeInteger( (int) data[1][0], Arrays.copy( data[1], 1, data[1].length-1 ) );
            LargeInteger        a = new LargeInteger( (int) data[2][0], Arrays.copy( data[2], 1, data[2].length-1 ) );
            LargeInteger        b = new LargeInteger( (int) data[3][0], Arrays.copy( data[3], 1, data[3].length-1 ) );
            LargeInteger      gcd = new LargeInteger( (int) data[4][0], Arrays.copy( data[4], 1, data[4].length-1 ) );
            LargeInteger[] result = instance.extendedEuclid(y);
            Assert.assertEquals(a, result[0]);
            Assert.assertEquals(b, result[1]);
            Assert.assertEquals(gcd, result[2]);
        }
    }

    /**
     * Test of gcd method, of class LargeInteger.
     */
    @Test
    public void testGcd() {
        for ( long[][] data : extendenEuclidDataTest ) {
            LargeInteger  instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger         y = new LargeInteger( (int) data[1][0], Arrays.copy( data[1], 1, data[1].length-1 ) );
            LargeInteger expResult = new LargeInteger( (int) data[4][0], Arrays.copy( data[4], 1, data[4].length-1 ) );
            LargeInteger result = instance.gcd(y);
            Assert.assertEquals(expResult, result);
        }
    }

    private static final long[][][] notAndOrXorDataTest = new long[][][] {
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x77c963bf543f47e7L, 0xfb26827ae4709627L, 0x8bb9441bedfe3663L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x01ef2ce440e0e990L, 0xc9c4e1cccda4aecbL, 0x616f651a216266a8L, 0xcaef7f899aL,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x77c963bf543f47e8L, 0xfb26827ae4709627L, 0x8bb9441bedfe3663L,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x7600431b141f0660L, 0x3222023220501024L, 0x8a900001cc9c1043L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x01c920a440204187L, 0xc9048048c4208603L, 0x129441a21622620L,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x00260c4000c0a809L, 0x00c06184098428c8L, 0x6046210000004088L, 0xcaef7f899aL,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x76264f5b14dfae69L, 0x32e263b629d438ecL, 0xead62101cc9c50cbL, 0xcaef7f899aL,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x147L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x3b7b58140eL,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x146L,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x3b7b581408L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x3b7b58154fL,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x141L,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x3b7b581549L,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x08090a0b0c0d0e0fL, 0x1020304050607L,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x2L,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x08090a0b0c0d0e0fL, 0x1020304050607L,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x08090a0b0c0d0e0eL, 0x1020304050607L,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xeb8729c30924a6a6L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x7bb56a39a366833dL, 0x1829fbccf4L,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xeb8729c30924a6a5L,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xfbb76bfbab66a7beL, 0x1829fbccf4L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x10304238a2420118L, 0x1829fbccf4L,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x6b85280101248225L,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x903243faaa422599L, 0x1829fbccf4L,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x7e9e8216d0c48036L, 0x8478L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xb7e73f6f78c21290L, 0xb8300ee7e731eb1cL, 0xff30987ef7e0ef6cL, 0x3c748c07931af287L, 0x268f36e730L,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x7e9e8216d0c48035L, 0x8478L,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xffffbf7ff8c692c0L, 0xb8300ee7e731ef7cL, 0xff30987ef7e0ef6cL, 0x3c748c07931af287L, 0x268f36e730L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x81613d692802128aL, 0xb8300ee7e7316b04L, 0xff30987ef7e0ef6cL, 0x3c748c07931af287L, 0x268f36e730L,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x3686020650c00006L, 0x8018L,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xc979bd79a80692baL, 0xb8300ee7e7316f64L, 0xff30987ef7e0ef6cL, 0x3c748c07931af287L, 0x268f36e730L,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x7850b3e22cfbed86L, 0x6111122daa15c7b7L, 0xf0cf473c00be807fL, 0x18882044a5a3be8dL, 0xda19421f98b82126L, 0x8b5cb7beefceL,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0000000ed133c551cL,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x07850b3e22cfbed85L, 0x6111122daa15c7b7L, 0xf0cf473c00be807fL, 0x18882044a5a3be8dL, 0xda19421f98b82126L, 0x8b5cb7beefceL,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x00000000d13041018L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x7850b3ef3ffffd9eL, 0x6111122daa15c7b7L, 0xf0cf473c00be807fL, 0x18882044a5a3be8dL, 0xda19421f98b82126L, 0x8b5cb7beefceL,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x7850b3022cc3a882L, 0x6111122daa15c7b7L, 0xf0cf473c00be807fL, 0x18882044a5a3be8dL, 0xda19421f98b82126L, 0x8b5cb7beefceL,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x7850b30f3fc7b89aL, 0x6111122daa15c7b7L, 0xf0cf473c00be807fL, 0x18882044a5a3be8dL, 0xda19421f98b82126L, 0x8b5cb7beefceL,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x416b8a5189fac5deL,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x3b7b58140eL,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x416b8a5189fac5dfL,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x110958040eL,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x416b8a4080a2c1d0L,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x416b8a7bfbfad5deL,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x416b8a6af2a2d1d0L,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x1L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x24L,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x2L,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x1L,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x25L,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x25L,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xd96d7771f708e3a4L, 0x897dd0a9cc4dadc7L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xd96d7771f708e3a5L, 0x897dd0a9cc4dadc7L,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xd96d7771f708e3a4L, 0x897dd0a9cc4dadc7L,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xd96d7771f708e3a4L, 0x897dd0a9cc4dadc7L,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xd96d7771f708e3a4L, 0x897dd0a9cc4dadc7L,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x33a15a44f838ac98L, 0x93ca4fL,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x33a15a44f838ac97L, 0x93ca4fL,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x33a15a44f838ac98L, 0x93ca4fL,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x33a15a44f838ac98L, 0x93ca4fL,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x33a15a44f838ac98L, 0x93ca4fL,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x19326c5e3020f9d6L, 0xbe1bb537de85db6fL, 0xa8f581fb0bL,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x1L,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x19326c5e3020f9d6L, 0xbe1bb537de85db6fL, 0xa8f581fb0bL,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x19326c5e3020f9d6L, 0xbe1bb537de85db6fL, 0xa8f581fb0bL,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x24L,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x1L,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x24L,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x24L,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L,},
            // m1: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L,},
            // not m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x1L,},
            // m0 and m1: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L,},
            // m0 and not m1: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L,},
            // m0 or m1: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L,},
            // m0 xor m1: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L,},},
    };

    /**
     * Test of not method, of class LargeInteger.
     */
    @Test
    public void testNot() {
        for ( long[][] data : notAndOrXorDataTest ) {
            LargeInteger instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger expResult = new LargeInteger( (int) data[2][0], Arrays.copy( data[2], 1, data[2].length-1 ) );
            LargeInteger result = instance.not();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of and method, of class LargeInteger.
     */
    @Test
    public void testAnd() {
        for (long[][] data : notAndOrXorDataTest) {
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            LargeInteger other = new LargeInteger((int) data[1][0], Arrays.copy(data[1], 1, data[1].length - 1));
            LargeInteger expResult = new LargeInteger((int) data[3][0], Arrays.copy(data[3], 1, data[3].length - 1));
            LargeInteger result = instance.and(other);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of andNot method, of class LargeInteger.
     */
    @Test
    public void testAndNot() {
        for (long[][] data : notAndOrXorDataTest) {
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            LargeInteger other = new LargeInteger((int) data[1][0], Arrays.copy(data[1], 1, data[1].length - 1));
            LargeInteger expResult = new LargeInteger((int) data[4][0], Arrays.copy(data[4], 1, data[4].length - 1));
            LargeInteger result = instance.andNot(other);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of or method, of class LargeInteger.
     */
    @Test
    public void testOr() {
        for (long[][] data : notAndOrXorDataTest) {
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            LargeInteger other = new LargeInteger((int) data[1][0], Arrays.copy(data[1], 1, data[1].length - 1));
            LargeInteger expResult = new LargeInteger((int) data[5][0], Arrays.copy(data[5], 1, data[5].length - 1));
            LargeInteger result = instance.or(other);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of xor method, of class LargeInteger.
     */
    @Test
    public void testXor() {
        for (long[][] data : notAndOrXorDataTest) {
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            LargeInteger other = new LargeInteger((int) data[1][0], Arrays.copy(data[1], 1, data[1].length - 1));
            LargeInteger expResult = new LargeInteger((int) data[6][0], Arrays.copy(data[6], 1, data[6].length - 1));
            LargeInteger result = instance.xor(other);
            Assert.assertEquals(expResult, result);
        }
    }

    private static final long[][][] shiftDataTest = new long[][][] {
        {
            // this: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0xaf00L, },
            // { shift }
            { 8 },
            // left shift : { sign, magnitude_0, magnitude_1, ... }
            { 1, 0xaf0000L, },
            // right shift : { sign, magnitude_0, magnitude_1, ... }
            { 1, 0xafL, },
        },
        {
            // this: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0xaf00L, },
            // { shift }
            { 42 },
            // left shift : { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x02bc000000000000L, },
            // right shift : { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L },
        },
        {
            // this: { sign, magnitude_0, magnitude_1, ... }
            { -1, 0x19L, },
            // { shift }
            { 40 },
            // left shift : { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x0000190000000000L, },
            // right shift : { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x1L, },
        },
        {
            // this: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0xaf00L, },
            // { shift }
            { 68 },
            // left shift : { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0000000000000000L, 0xaf000L,},
            // right shift : { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0L },
        },
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xc0ce60bcc40340a0L, 0x46c9c1d1e6320f7bL, 0x4ff52c2cf39ec1fcL, 0x9f0bef41610e4684L, 0x9b0c1c4edecde693L, 0xda82d8e9f5303cc9L, 0x529ecdcd83ee7c27L, 0xd2L,},
            // { shift }
            {229},
            // << bitShift: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x8068140000000000L, 0xc641ef7819cc1798L, 0x73d83f88d9383a3cL, 0x21c8d089fea5859eL, 0xd9bcd273e17de82cL, 0xa6079933618389dbL, 0x7dcf84fb505b1d3eL, 0x1a4a53d9b9b0L,},
            // >> bitShift: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x76f66f349cf85f7bL, 0x4fa981e64cd860e2L, 0x6c1f73e13ed416c7L, 0x69294f66eL,},
        },
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x6ae247bb6e1cf3L,},
            // { shift }
            {152},
            // << bitShift: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x0000000000000000L, 0x0000000000000000L, 0x47bb6e1cf3000000L, 0x6ae2L,},
            // >> bitShift: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x1L,},
        },
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0xcd8a5d75ebacda29L, 0x5e731d3c5ba73c4bL, 0x7f61df5645780491L, 0xa1ab124f088d0fbbL, 0xfe5bb7007dL,  },
            // { shift }
            { 121 },
            // << bitShift: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0000000000000000L, 0x5200000000000000L, 0x979b14baebd759b4L, 0x22bce63a78b74e78L, 0x76fec3beac8af009L, 0xfb4356249e111a1fL, 0x1fcb76e00L,  },
            // >> bitShift: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0xb0efab22bc0248afL, 0xd58927844687ddbfL, 0x7f2ddb803ed0L,  },
        },
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2bfa96L,},
            // { shift }
            {220},
            // << bitShift: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x2bfa960000000L,},
            // >> bitShift: { sign, magnitude_0, magnitude_1, ... }
            {0, 0x0L,},},
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x505a3e3077b77272L, 0xc1410ff600e216L,},
            // { shift }
            {32},
            // << bitShift: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x77b7727200000000L, 0xf600e216505a3e30L, 0xc1410fL,},
            // >> bitShift: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xf600e216505a3e31L, 0xc1410fL,},
        },
        {
            // m0: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x290b35d5338c87baL, 0x10e3ad67L,},
            // { shift }
            {32},
            // << bitShift: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x338c87ba00000000L, 0x10e3ad67290b35d5L,},
            // >> bitShift: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x10e3ad67290b35d5L,},
        },
    };

    /**
     * Test of shiftRight method, of class LargeInteger.
     */
    @Test
    public void testShiftRight() {
        for (long[][] data : shiftDataTest) {
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            int bitShift = (int) data[1][0];
            LargeInteger expResult = new LargeInteger((int) data[3][0], Arrays.copy(data[3], 1, data[3].length - 1));
            LargeInteger result = instance.shiftRight(bitShift);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of shiftLeft method, of class LargeInteger.
     */
    @Test
    public void testShiftLeft() {
        for (long[][] data : shiftDataTest) {
            LargeInteger instance = new LargeInteger((int) data[0][0], Arrays.copy(data[0], 1, data[0].length - 1));
            int bitShift = (int) data[1][0];
            LargeInteger expResult = new LargeInteger((int) data[2][0], Arrays.copy(data[2], 1, data[2].length - 1));
            LargeInteger result = instance.shiftLeft(bitShift);
            Assert.assertEquals(expResult, result);
        }
    }

    private static final long[][][] toLongDataTest = new long[][][] {
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x0000004eb845e3fcL,},
            // { toLong }
            {    0x0000004eb845e3fcL},
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x0000004eb845e3fcL,},
            // { toLong }
            {    0xffffffb147ba1c04L},
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xbf943524eda1e2a7L, },
            // { toLong }
            {   0x7fffffffffffffffL},
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0xbf943524eda1e2a7L, },
            // { toLong }
            {    0x8000000000000000L},
        },
    };

    /**
     * Test of toLong method, of class LargeInteger.
     */
    @Test
    public void testToLong() {
        for ( long[][] data : toLongDataTest ) {
            LargeInteger instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            long expResult = data[1][0];
            long result = instance.toLong();
            Assert.assertEquals(expResult, result);
        }
    }

    private static final long[][][] modPowDataTest = new long[][][]{
        {
            // instance: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x3L, },
            // exponent: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x0L, },
            // modulus: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x13L, },
            // modPow expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x1L, },
            // modInverse expResult: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x6L, },
        },
        {
            // instance: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2L, },
            // exponent: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2711L, },
            // modulus: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xbL, },
            // modPow expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2L, },
            // modInverse expResult: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x5L, },
        },
        {
            // instance: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x2L, },
            // exponent: { sign, magnitude_0, magnitude_1, ... }
            {1, 0xf5L, },
            // modulus: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x23L, },
            // modPow expResult: { sign, magnitude_0, magnitude_1, ... }
            {1, 0x20L, },
            // modInverse expResult: { sign, magnitude_0, magnitude_1, ... }
            {-1, 0x11L, },
        },
    };

    /**
     * Test of modPow method, of class LargeInteger.
     */
    @Test
    public void testModPow() {
        for ( long[][] data : modPowDataTest ) {
            LargeInteger instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger exponent = new LargeInteger( (int) data[1][0], Arrays.copy( data[1], 1, data[1].length-1 ) );
            LargeInteger modulus = new LargeInteger( (int) data[2][0], Arrays.copy( data[2], 1, data[2].length-1 ) );
            LargeInteger expResult = new LargeInteger( (int) data[3][0], Arrays.copy( data[3], 1, data[3].length-1 ) );
            LargeInteger result = instance.modPow(exponent, modulus);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of modInverse method, of class LargeInteger.
     */
    @Test
    public void testModInverse() {
        for ( long[][] data : modPowDataTest ) {
            LargeInteger modulus = new LargeInteger( (int) data[2][0], Arrays.copy( data[2], 1, data[2].length-1 ) );
            LargeInteger instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ) );
            LargeInteger expResult = new LargeInteger( (int) data[4][0], Arrays.copy( data[4], 1, data[4].length-1 ) );
            LargeInteger result = instance.modInverse(modulus);
            Assert.assertEquals(expResult, result);
        }
    }

    private static final long[][][] getBitDataTest = new long[][][] {
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0000000000000000L, },
            // { sign, magnitude_0, magnitude_1, ... }
            { 30, 0 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { 0, 0x0000000000000000L, },
            // { sign, magnitude_0, magnitude_1, ... }
            { 120, 0 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
            // { sign, magnitude_0, magnitude_1, ... }
            { 71, 1 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
            // { sign, magnitude_0, magnitude_1, ... }
            { 190, 0 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { 1, 0x67eff14f14a2fe90L, 0xde75808980deeae1L, 0x00000000d2b1d3b2L,},
            // { sign, magnitude_0, magnitude_1, ... }
            { 200, 0 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { -1, 0x0000000000000146L, },
            // { sign, magnitude_0, magnitude_1, ... }
            { 6, 0 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { -1, 0x0000000000000146L, },
            // { sign, magnitude_0, magnitude_1, ... }
            { 5, 1 },
        },
        {
            // mag: { sign, magnitude_0, magnitude_1, ... }
            { -1, 0x0000000000000146L, },
            // { sign, magnitude_0, magnitude_1, ... }
            { 70, 1 },
        },
    };

    /**
     * Test of getBit method, of class LargeInteger.
     */
    @Test
    public void testGetBit() {
        for ( long[][] data : getBitDataTest ) {
            LargeInteger instance = new LargeInteger( (int) data[0][0], Arrays.copy( data[0], 1, data[0].length-1 ));
            int index = (int) data[1][0];
            boolean expResult = data[1][1]==1L;
            boolean result = instance.getBit(index);
            Assert.assertEquals(expResult, result);
        }
    }

    private static final long[][] toFloatToDoubleInputDataTest = new long[][]{
        {  0, 0x0L },
        {  1, 0x147L },
        { -1, 0x147L },
        { 1, 0x7c84110e96d3350eL, 0x91L,  },
        { -1, 0x34547fc3503cf0abL, 0x926f0b7eL,  },
    };
    private static final float[] toFloatOutputDataTest = new float[]{
        0.f,
        327.f,
        -327.f,
        2.6837502E21f,
        -4.5319054E28f,
    };
    private static final double[] toDoubleOutputDataTest = new double[]{
        0.,
        327.,
        -327.,
        2.6837502057998713E21,
        -4.531905647398545E28,
    };

    /**
     * Test of toFloat method, of class LargeInteger.
     */
    @Test
    public void testToFloat() {
        for ( int i=0; i<toFloatToDoubleInputDataTest.length; i++ ) {
            LargeInteger instance = new LargeInteger( (int) toFloatToDoubleInputDataTest[i][0], Arrays.copy( toFloatToDoubleInputDataTest[i], 1, toFloatToDoubleInputDataTest[i].length-1 ));
            float expResult = toFloatOutputDataTest[i];
            float result = instance.toFloat();
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of toDouble method, of class LargeInteger.
     */
    @Test
    public void testToDouble() {
        for ( int i=0; i<toFloatToDoubleInputDataTest.length; i++ ) {
            LargeInteger instance = new LargeInteger( (int) toFloatToDoubleInputDataTest[i][0], Arrays.copy( toFloatToDoubleInputDataTest[i], 1, toFloatToDoubleInputDataTest[i].length-1 ));
            double expResult = toDoubleOutputDataTest[i];
            double result = instance.toDouble();
            Assert.assertEquals(expResult, result, 0.0);
        }
    }
}
