
package science.unlicense.math.impl;


/**
 *
 * @author Johann Sorel
 */
public class Vector2i32Test extends AbstractVectorTest {

    @Override
    protected int[] getSupportedDimensions() {
        return new int[]{2};
    }

    @Override
    protected Vector2i32 create(int dim) {
        return new Vector2i32();
    }

}
