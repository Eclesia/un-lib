
package science.unlicense.math.api.transform;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.impl.Affine3;
import science.unlicense.math.impl.Matrix2x2;
import science.unlicense.math.impl.Scalarf64;

/**
 *
 * @author Johann Sorel
 */
public class ConcatenateTransformTest {

    private static final double DELTA = 0.0000001;

    /**
     * Check one tuple transform
     */
    @Test
    public void testOneValue(){
        final Transform trs1 = new Affine1(10, 0);
        final Transform trs2 = new Affine1(1, 5);
        final Transform concat = ConcatenateTransform.create(trs1, trs2);

        VectorRW tuple = new Scalarf64();
        tuple.setX(3);
        concat.transform(tuple, tuple);

        Assert.assertEquals(35.0, tuple.get(0), DELTA);
    }

    /**
     * Check a array transform.
     */
    @Test
    public void testArray(){

        final Transform trs1 = new Affine3(
                        2, 0, 0, 0,
                        0, 3, 0, 0,
                        0, 0, 4, 0
                );
        final Transform trs2 = new Affine3(
                        1, 0, 0, 10,
                        0,-1, 0, 20,
                        0, 0, 1, 30
                );
        final Transform concat = ConcatenateTransform.create(trs1, trs2);

        float[] array = {
            0,1,2,
            3,4,5,
            6,7,8
        };

        final float[] res = new float[9];
        concat.transform(array,0,res,0,3);

        Assert.assertArrayEquals(new float[]{
            10, 17, 38,
            16,  8, 50,
            22, -1, 62,

        }, res, (float) DELTA);
    }

    /**
     * Concatenate should be smart enough to merge the 2 affine transform
     */
    @Test
    public void testAffineMerge(){

        final Transform trs1 = new Affine1(10, 0);
        final Transform trs2 = new Affine1(1, 5);
        final Transform concat = ConcatenateTransform.create(trs1, trs2);

        Assert.assertTrue(concat instanceof Affine);
        final Matrix merge = ((Affine) concat).toMatrix();
        Assert.assertEquals(new Matrix2x2(10, 5, 0, 1), merge);

    }

}
