
package science.unlicense.math.api.transform;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class DimStackTransformTest {

    private static final double DELTA = 0.0000001;

    @Test
    public void testValue(){

        final Transform trs1 = new Affine1(10, 0);
        final Transform trs2 = new Affine1(1, 5);
        final Transform trs3 = new Affine1(2, -12);

        final Transform trs = DimStackTransform.create(new Transform[]{trs1,trs2,trs3});

        Tuple tuple = new Vector3f64(1,2,3);
        tuple = trs.transform(tuple, null);

        Assert.assertEquals(10, tuple.get(0), DELTA);
        Assert.assertEquals(7, tuple.get(1), DELTA);
        Assert.assertEquals(-6, tuple.get(2), DELTA);


    }

    /**
     * Stack should be smart enough to merge affine transforms
     */
    @Test
    public void testMerge(){

        final Transform trs1 = new Affine1(10, 0);
        final Transform trs2 = new Affine1(1, 5);
        final Transform trs3 = new Affine1(2, -12);

        final Transform concat = DimStackTransform.create(new Transform[]{trs1,trs2,trs3});

        Assert.assertTrue(concat instanceof Affine);
        final Matrix merge = ((Affine) concat).toMatrix();
        Assert.assertEquals(new Matrix4x4(
                10, 0, 0, 0,
                 0, 1, 0, 5,
                 0, 0, 2, -12,
                 0, 0, 0, 1),
                merge);

        Tuple tuple = new Vector3f64(1,2,3);
        tuple = concat.transform(tuple, null);

        Assert.assertEquals(10, tuple.get(0), DELTA);
        Assert.assertEquals(7, tuple.get(1), DELTA);
        Assert.assertEquals(-6, tuple.get(2), DELTA);


    }

}
