
package science.unlicense.math.api.transform;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;

/**
 *
 * @author Johann Sorel
 */
public class RearrangeTransformTest {

    private static final double DELTA = 0.0000001;

    /**
     * Check reordering values
     */
    @Test
    public void testReorder(){

        final int[] mapping = {2,0,1};
        final Transform trs = RearrangeTransform.create(mapping);
        Assert.assertEquals(3, trs.getInputDimensions());
        Assert.assertEquals(3, trs.getOutputDimensions());

        Tuple tuple = new Vector3f64(0,1,2);
        tuple = trs.transform(tuple, null);

        Assert.assertEquals(1, tuple.get(0), DELTA);
        Assert.assertEquals(2, tuple.get(1), DELTA);
        Assert.assertEquals(0, tuple.get(2), DELTA);

    }

    @Test
    public void testReorderAndReduce(){
        final int[] mapping = {-1,1,0};
        final Transform trs = RearrangeTransform.create(mapping);
        Assert.assertEquals(3, trs.getInputDimensions());
        Assert.assertEquals(2, trs.getOutputDimensions());

        Tuple tuple = new Vector3f64(0,1,2);
        tuple = trs.transform(tuple, null);
        Assert.assertEquals(2, tuple.getSampleCount());

        Assert.assertEquals(2, tuple.get(0), DELTA);
        Assert.assertEquals(1, tuple.get(1), DELTA);
    }

    @Test
    public void testToMatrix(){
        final int[] mapping = {2,0,1};
        final Affine trs = (Affine) RearrangeTransform.create(mapping);
        final Matrix m = trs.toMatrix();

        Tuple tuple = new Vector4f64(0,1,2,1);
        tuple = m.transform(tuple, null);

        Assert.assertEquals(1, tuple.get(0), DELTA);
        Assert.assertEquals(2, tuple.get(1), DELTA);
        Assert.assertEquals(0, tuple.get(2), DELTA);
        Assert.assertEquals(1, tuple.get(3), DELTA);
    }

}
