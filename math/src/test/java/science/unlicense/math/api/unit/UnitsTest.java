
package science.unlicense.math.api.unit;

import science.unlicense.math.api.unitold.Units;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Scalarf64;

/**
 *
 * @author Johann Sorel
 */
public class UnitsTest {

    private static final double DELTA = 0.0000001;

    @Test
    public void testCreateTransform(){

        Transform trs = Units.getTransform(Units.CENTIMETER, Units.METER);

        VectorRW t = new Scalarf64();
        t.setX(1);
        trs.transform(t, t);
        Assert.assertEquals(0.01, t.get(0),DELTA);

        trs = Units.getTransform(Units.KILOMETER, Units.METER);
        t = new Scalarf64();
        t.setX(1);
        trs.transform(t, t);
        Assert.assertEquals(1000, t.get(0),DELTA);

    }

}
