package science.unlicense.unit.systems.cgs;

import science.unlicense.unit.impl.Cgs;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.unit.api.Measure;
import science.unlicense.unit.api.UnitMapper;
import science.unlicense.unit.impl.CgsSiMapper;
import science.unlicense.unit.impl.SI;

/**
 *
 * @author Samuel Andrés
 */
public class UnitCsgTest {

    private static final UnitMapper CGS_TO_SI = CgsSiMapper.getInstance();

    @Test
    public void test1() {
        Assert.assertEquals(Cgs.DIMENSION.TEMPS, Cgs.SECONDE.dimension()[0]);
        Assert.assertEquals(Cgs.DIMENSION.LONGUEUR, Cgs.METRE.dimension()[0]);
        Assert.assertEquals(Cgs.DIMENSION.LONGUEUR, Cgs.CENTIMETRE.dimension()[0]);
        Assert.assertEquals(Cgs.DIMENSION.MASSE, Cgs.GRAMME.dimension()[0]);
    }

    @Test
    public void test2() {
        final Measure measure1 = new Measure(1., Cgs.SECONDE);
        Assert.assertEquals(1., measure1.to(SI.SECONDE, CGS_TO_SI).getValue(), 1e-14);
        final Measure measure2 = new Measure(1., Cgs.CENTIMETRE);
        Assert.assertEquals(1e-2, measure2.to(SI.METRE, CGS_TO_SI).getValue(), 1e-14);
        final Measure measure3 = new Measure(1., Cgs.GRAMME);
        Assert.assertEquals(1e-3, measure3.to(SI.KILOGRAMME, CGS_TO_SI).getValue(), 1e-14);
        final Measure measure4 = new Measure(1., Cgs.DYNE);
        Assert.assertEquals(1e-5, measure4.to(SI.NEWTON, CGS_TO_SI).getValue(), 1e-14);
        final Measure measure7 = new Measure(1., Cgs.ERG);
        Assert.assertEquals(1e-7, measure7.to(SI.JOULE, CGS_TO_SI).getValue(), 1e-14);
    }
}
