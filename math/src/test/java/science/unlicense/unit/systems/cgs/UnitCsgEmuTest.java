package science.unlicense.unit.systems.cgs;

import science.unlicense.unit.impl.Cgs;
import science.unlicense.unit.impl.CgsEmu;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.unit.api.Measure;
import science.unlicense.unit.api.UnitMapper;
import science.unlicense.unit.impl.CgsEmuSiMapper;
import science.unlicense.unit.impl.SI;

/**
 *
 * @author Samuel Andrés
 */
public class UnitCsgEmuTest {

    private static final UnitMapper CGS_EMU_TO_SI = CgsEmuSiMapper.getInstance();

    @Test
    public void test1() {
        Assert.assertEquals(Cgs.DIMENSION.TEMPS, CgsEmu.SECONDE.dimension()[0]);
        Assert.assertEquals(Cgs.DIMENSION.LONGUEUR, CgsEmu.METRE.dimension()[0]);
        Assert.assertEquals(Cgs.DIMENSION.LONGUEUR, CgsEmu.CENTIMETRE.dimension()[0]);
        Assert.assertEquals(Cgs.DIMENSION.MASSE, CgsEmu.GRAMME.dimension()[0]);
    }

    @Test
    public void test2() {
        final Measure measure1 = new Measure(1., CgsEmu.SECONDE);
        Assert.assertEquals(1., measure1.to(SI.SECONDE, CGS_EMU_TO_SI).getValue(), 1e-14);
        final Measure measure2 = new Measure(1., CgsEmu.CENTIMETRE);
        Assert.assertEquals(1e-2, measure2.to(SI.METRE, CGS_EMU_TO_SI).getValue(), 1e-14);
        final Measure measure3 = new Measure(1., CgsEmu.GRAMME);
        Assert.assertEquals(1e-3, measure3.to(SI.KILOGRAMME, CGS_EMU_TO_SI).getValue(), 1e-14);
        final Measure measure4 = new Measure(1., CgsEmu.DYNE);
        Assert.assertEquals(1e-5, measure4.to(SI.NEWTON, CGS_EMU_TO_SI).getValue(), 1e-14);
        final Measure measure5 = new Measure(1., CgsEmu.ERG);
        Assert.assertEquals(1e-7, measure5.to(SI.JOULE, CGS_EMU_TO_SI).getValue(), 1e-14);
        final Measure measure6 = new Measure(1., CgsEmu.ABCOULOMB);
        Assert.assertEquals(10, measure6.to(SI.COULOMB, CGS_EMU_TO_SI).getValue(), 1e-14);
        final Measure measure7 = new Measure(1., CgsEmu.ABAMPERE);
        Assert.assertEquals(10, measure7.to(SI.AMPERE, CGS_EMU_TO_SI).getValue(), 1e-14);
        final Measure measure8 = new Measure(1., CgsEmu.GAUSS);
        Assert.assertEquals(1e-4, measure8.to(SI.TESLA, CGS_EMU_TO_SI).getValue(), 1e-14);
        final Measure measure9 = new Measure(1., CgsEmu.MAXWELL);
        Assert.assertEquals(1e-8, measure9.to(SI.WEBER, CGS_EMU_TO_SI).getValue(), 1e-14);
        final Measure measure10 = new Measure(1., CgsEmu.OERSTED);
        Assert.assertEquals(7.957747e1, measure10.to(SI.AMPERE_PAR_METRE, CGS_EMU_TO_SI).getValue(), 1e-5);
    }
}
