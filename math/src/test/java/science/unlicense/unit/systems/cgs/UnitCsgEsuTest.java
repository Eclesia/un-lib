package science.unlicense.unit.systems.cgs;

import science.unlicense.unit.impl.CgsEsu;
import science.unlicense.unit.impl.Cgs;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.unit.api.Measure;
import science.unlicense.unit.api.UnitMapper;
import science.unlicense.unit.impl.CgsEsuSiMapper;
import science.unlicense.unit.impl.SI;

/**
 *
 * @author Samuel Andrés
 */
public class UnitCsgEsuTest {

    private static final UnitMapper CGS_ESU_TO_SI = CgsEsuSiMapper.getInstance();

    @Test
    public void test1() {
        Assert.assertEquals(Cgs.DIMENSION.TEMPS, CgsEsu.SECONDE.dimension()[0]);
        Assert.assertEquals(Cgs.DIMENSION.LONGUEUR, CgsEsu.METRE.dimension()[0]);
        Assert.assertEquals(Cgs.DIMENSION.LONGUEUR, CgsEsu.CENTIMETRE.dimension()[0]);
        Assert.assertEquals(Cgs.DIMENSION.MASSE, CgsEsu.GRAMME.dimension()[0]);
    }

    @Test
    public void test2() {
        final Measure measure1 = new Measure(1., CgsEsu.SECONDE);
        Assert.assertEquals(1., measure1.to(SI.SECONDE, CGS_ESU_TO_SI).getValue(), 1e-14);
        final Measure measure2 = new Measure(1., CgsEsu.CENTIMETRE);
        Assert.assertEquals(1e-2, measure2.to(SI.METRE, CGS_ESU_TO_SI).getValue(), 1e-14);
        final Measure measure3 = new Measure(1., CgsEsu.GRAMME);
        Assert.assertEquals(1e-3, measure3.to(SI.KILOGRAMME, CGS_ESU_TO_SI).getValue(), 1e-14);
        final Measure measure4 = new Measure(1., CgsEsu.DYNE);
        Assert.assertEquals(1e-5, measure4.to(SI.NEWTON, CGS_ESU_TO_SI).getValue(), 1e-14);
        final Measure measure7 = new Measure(1., CgsEsu.ERG);
        Assert.assertEquals(1e-7, measure7.to(SI.JOULE, CGS_ESU_TO_SI).getValue(), 1e-14);
        final Measure measure5 = new Measure(1., CgsEsu.STATCOULOMB);
        Assert.assertEquals(3.335_641e-10, measure5.to(SI.COULOMB, CGS_ESU_TO_SI).getValue(), 1e-14);
        final Measure measure6 = new Measure(1., CgsEsu.STATAMPERE);
        Assert.assertEquals(3.335_641e-10, measure6.to(SI.AMPERE, CGS_ESU_TO_SI).getValue(), 1e-14);
        final Measure measure8 = new Measure(1., CgsEsu.STATVOLT);
        Assert.assertEquals(299.792458, measure8.to(SI.VOLT, CGS_ESU_TO_SI).getValue(), 1e-14);
//        final Measure measure9= new Measure(1., UnitCgs.MAXWELL);
//        Assert.assertEquals(299.792458, measure9.measureIn(UnitSi.WEBER, CGS_TO_SI).definitionValue(), 1e-14);
//        final Measure measure7 = new Measure(1., UnitCgs.OERSTED);
//        Assert.assertEquals(3.335_641e-10, measure7.measureIn(UnitSi.AMPERE_PAR_METRE, CGS_TO_SI).definitionValue(), 1e-14);
    }
}
