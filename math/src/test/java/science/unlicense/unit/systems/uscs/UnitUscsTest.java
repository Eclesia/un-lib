package science.unlicense.unit.systems.uscs;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.unit.api.Measure;
import science.unlicense.math.api.unit.Quantity;
import science.unlicense.unit.api.UnitBuilder;
import science.unlicense.unit.api.UnitConverter;
import science.unlicense.unit.api.UnitMapper;
import science.unlicense.unit.api.Units;
import science.unlicense.math.api.unit.Dimension;
import science.unlicense.unit.api.unit.DerivedUnit;
import science.unlicense.unit.impl.SI;
import science.unlicense.unit.impl.Uscs;
import science.unlicense.unit.impl.UscsSiMapper;

/**
 *
 * @author Samuel Andrés
 */
public class UnitUscsTest {

    private static final UnitMapper USCS_TO_SI = UscsSiMapper.getInstance();

    @Deprecated
    private static final Dimension NULLDIM = new Dimension() {
        @Override
        public Chars name() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Quantity quantity() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    };

    // définitions d'unités bizarres pour les tests !

    public static final DerivedUnit FOOT_PER_DEGREE_FAHRENHEIT = new UnitBuilder()
            .addName(Chars.constant("pied par degré fahrenheit"))
            .addFactor(Uscs.INTERNATIONAL_FOOT)
            .addFactor(Uscs.FAHRENHEIT, -1)
            .buildDerived(NULLDIM);

    public static final DerivedUnit METRE_PER_DEGREE_KELVIN = new UnitBuilder()
            .addName(Chars.constant("mètre par kelvin"))
            .addFactor(SI.METRE)
            .addFactor(SI.KELVIN, -1)
            .buildDerived(NULLDIM);

    public static final DerivedUnit METRE_PER_DEGREE_CELSIUS = new UnitBuilder()
            .addName(Chars.constant("mètre par celsius"))
            .addFactor(SI.METRE)
            .addFactor(SI.CELSIUS, -1)
            .buildDerived(NULLDIM);

    @Test
    public void test1() {

        Assert.assertEquals(-17.778, new Measure(0, Uscs.FAHRENHEIT).to(SI.CELSIUS, USCS_TO_SI).getValue(), 1e-3);
        Assert.assertEquals(0., new Measure(32., Uscs.FAHRENHEIT).to(SI.CELSIUS, USCS_TO_SI).getValue(), 1e-14);
        Assert.assertEquals(37., new Measure(98.6, Uscs.FAHRENHEIT).to(SI.CELSIUS, USCS_TO_SI).getValue(), 1e-14);
        Assert.assertEquals(37.777, new Measure(100., Uscs.FAHRENHEIT).to(SI.CELSIUS, USCS_TO_SI).getValue(), 1e-3);
        Assert.assertEquals(99.975, new Measure(211.955, Uscs.FAHRENHEIT).to(SI.CELSIUS, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(100., new Measure(212., Uscs.FAHRENHEIT).to(SI.CELSIUS, USCS_TO_SI).getValue(), 1e-14);

        Assert.assertEquals(.3048, new Measure(1., Uscs.INTERNATIONAL_FOOT).to(SI.METRE, USCS_TO_SI).getValue(), 1e-14);
//        Assert.assertEquals(1., new Measure(.3048, UnitSi.METRE).to(UnitUscs.FOOT_INTERNATIONAL), 1e-14);

        Assert.assertEquals(
                .54864, new Measure(1., FOOT_PER_DEGREE_FAHRENHEIT).to(METRE_PER_DEGREE_KELVIN, USCS_TO_SI).getValue(), 1e-14);
        Assert.assertEquals(
                .54864, new Measure(1., FOOT_PER_DEGREE_FAHRENHEIT).to(METRE_PER_DEGREE_CELSIUS, USCS_TO_SI).getValue(), 1e-14);
        Assert.assertEquals(
                .54864, new Measure(1., FOOT_PER_DEGREE_FAHRENHEIT).to(METRE_PER_DEGREE_KELVIN, USCS_TO_SI).getValue(), 1e-14);
        Assert.assertEquals(0.3048006096012192, new Measure(1., Uscs.SURVEY_FOOT).to(SI.METRE, USCS_TO_SI).getValue(), 1e-14);
        Assert.assertEquals(0.30480061, new Measure(1., Uscs.SURVEY_FOOT).to(SI.METRE, USCS_TO_SI).getValue(), 1e-9);
        Assert.assertEquals(1_609.344, new Measure(1., Uscs.INTERNATIONAL_MILE).to(SI.METRE, USCS_TO_SI).getValue(), 1e-9);

        Assert.assertEquals(0.201_168_4, new Measure(1., Uscs.SURVEY_LINK).to(SI.METRE, USCS_TO_SI).getValue(), 1e-8);
        Assert.assertEquals(7.92, new Measure(1., Uscs.SURVEY_LINK).to(Uscs.INTERNATIONAL_INCH).getValue(), 1e-4);
        Assert.assertEquals(5.029_210, new Measure(1., Uscs.SURVEY_ROD).to(SI.METRE, USCS_TO_SI).getValue(), 1e-6);
        Assert.assertEquals(20.116_84, new Measure(1., Uscs.SURVEY_CHAIN).to(SI.METRE, USCS_TO_SI).getValue(), 1e-6);
        Assert.assertEquals(1_609.347, new Measure(1., Uscs.SURVEY_MILE).to(SI.METRE, USCS_TO_SI).getValue(), 1e-3);
        Assert.assertEquals(4_828.042, new Measure(1., Uscs.SURVEY_LEAGUE).to(SI.METRE, USCS_TO_SI).getValue(), 1e-3);

        Assert.assertEquals(1.8288, new Measure(1., Uscs.FATHOM).to(SI.METRE, USCS_TO_SI).getValue(), 1e-9);
        Assert.assertEquals(219.456, new Measure(1., Uscs.CABLE).to(SI.METRE, USCS_TO_SI).getValue(), 1e-9);
        Assert.assertEquals(1.091, new Measure(1., Uscs.CABLE).to(Uscs.SURVEY_FURLONG).getValue(), 1e-3);
        Assert.assertEquals(1_852, new Measure(1., Uscs.NAUTICAL_MILE).to(SI.METRE, USCS_TO_SI).getValue(), 1e-9);
        Assert.assertEquals(8.439, new Measure(1., Uscs.NAUTICAL_MILE).to(Uscs.CABLE).getValue(), 1e-4);
        Assert.assertEquals(1.151, new Measure(1., Uscs.NAUTICAL_MILE).to(Uscs.INTERNATIONAL_MILE).getValue(), 1e-3);

        // surface

        Assert.assertEquals(0.09290341, new Measure(1., Uscs.SQUARE_SURVEY_FOOT).to(SI.METRE_CARRE, USCS_TO_SI).getValue(), 1e-3);

        Assert.assertEquals(404.6873, new Measure(1., Uscs.SQUARE_SURVEY_CHAIN).to(SI.METRE_CARRE, USCS_TO_SI).getValue(), 1e-4);
        Assert.assertEquals(4356., new Measure(1., Uscs.SQUARE_SURVEY_CHAIN).to(Uscs.SQUARE_SURVEY_FOOT).getValue(), 1e-9);
//        Assert.assertEquals(4356., new Measure(1., UnitUscs.SQUARE_CHAIN).to(UnitUscs.SQUARE_ROD), 1e-9);
        Assert.assertEquals(4046.856, new Measure(1., Uscs.INTERNATIONAL_ACRE).to(SI.METRE_CARRE, USCS_TO_SI).getValue(), 1e-3);
        Assert.assertEquals(4046.873, new Measure(1., Uscs.SURVEY_ACRE).to(SI.METRE_CARRE, USCS_TO_SI).getValue(), 1e-3);
        Assert.assertEquals(43560., new Measure(1., Uscs.SURVEY_ACRE).to(Uscs.SQUARE_SURVEY_FOOT).getValue(), 1e-9);
        Assert.assertEquals(10., new Measure(1., Uscs.SURVEY_ACRE).to(Uscs.SQUARE_SURVEY_CHAIN).getValue(), 1e-9);
        Assert.assertEquals(2.589_998e6, new Measure(1., Uscs.SURVEY_SECTION).to(SI.METRE_CARRE, USCS_TO_SI).getValue(), 1);
        Assert.assertEquals(640., new Measure(1., Uscs.SURVEY_SECTION).to(Uscs.SURVEY_ACRE).getValue(), 1e-9);
        Assert.assertEquals(1., new Measure(1., Uscs.SURVEY_SECTION).to(Uscs.SQUARE_SURVEY_MILE).getValue(), 1e-9);
//        Assert.assertEquals(93.239_93e6, new Measure(1., UnitUscs.SURVEY_TOWNSHIP).to(UnitSi.METRE_CARRE, USCS_TO_SI), 1e1); // conversion donnée par Wikipedia
        Assert.assertEquals(93.239_940e6, new Measure(1., Uscs.SURVEY_TOWNSHIP).to(SI.METRE_CARRE, USCS_TO_SI).getValue(), 1e1); // conversion donnée par trustconverter.com
        Assert.assertEquals(36., new Measure(1., Uscs.SURVEY_TOWNSHIP).to(Uscs.SURVEY_SECTION).getValue(), 1e-9);
        Assert.assertEquals(4., new Measure(1., Uscs.SURVEY_TOWNSHIP).to(Uscs.SQUARE_SURVEY_LEAGUE).getValue(), 1e-9);

        // volume

        Assert.assertEquals(.000016387064, new Measure(1., Uscs.CUBIC_INCH).to(SI.METRE_CUBE, USCS_TO_SI).getValue(), 1e-17);
        Assert.assertEquals(16.387064, new Measure(1., Uscs.CUBIC_INCH).to(SI.VOLUME.MILLILITRE, USCS_TO_SI).getValue(), 1e-17);
        Assert.assertEquals(.028316846592, new Measure(1., Uscs.CUBIC_FOOT).to(SI.METRE_CUBE, USCS_TO_SI).getValue(), 1e-17);
        Assert.assertEquals(28.316846592, new Measure(1., Uscs.CUBIC_FOOT).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-14);
        Assert.assertEquals(1728., new Measure(1., Uscs.CUBIC_FOOT).to(Uscs.CUBIC_INCH).getValue(), 1e-17);
        Assert.assertEquals(.764554857984, new Measure(1., Uscs.CUBIC_YARD).to(SI.METRE_CUBE, USCS_TO_SI).getValue(), 1e-15);
        Assert.assertEquals(764.554857984, new Measure(1., Uscs.CUBIC_YARD).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-12);
        Assert.assertEquals(27., new Measure(1., Uscs.CUBIC_YARD).to(Uscs.CUBIC_FOOT).getValue(), 1e-17);
        Assert.assertEquals(43560., new Measure(1., Uscs.INTERNATIONAL_ACRE_FOOT).to(Uscs.CUBIC_FOOT).getValue(), 1e-17);
        Assert.assertEquals(1613.333, new Measure(1., Uscs.INTERNATIONAL_ACRE_FOOT).to(Uscs.CUBIC_YARD).getValue(), 1e-3);
        Assert.assertEquals(1233.482, new Measure(1., Uscs.INTERNATIONAL_ACRE_FOOT).to(SI.METRE_CUBE, USCS_TO_SI).getValue(), 1e-3);
        Assert.assertEquals(1.23348183754752, new Measure(1., Uscs.INTERNATIONAL_ACRE_FOOT).to(SI.VOLUME.MEGALITRE, USCS_TO_SI).getValue(), 1e-19);
        Assert.assertEquals(1233.489, new Measure(1., Uscs.SURVEY_ACRE_FOOT).to(SI.METRE_CUBE, USCS_TO_SI).getValue(), 1e-3);

        Assert.assertEquals(61.611519921875, new Measure(1., Uscs.MINIM).to(SI.VOLUME.MICROLITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(3.6966911953125, new Measure(1., Uscs.FLUID_DRAM).to(SI.VOLUME.MILLILITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(60., new Measure(1., Uscs.FLUID_DRAM).to(Uscs.MINIM).getValue(), 1e-13);
        Assert.assertEquals(4.92892159375, new Measure(1., Uscs.TEASPOON).to(SI.VOLUME.MILLILITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(80., new Measure(1., Uscs.TEASPOON).to(Uscs.MINIM).getValue(), 1e-13);
        Assert.assertEquals(14.78676478125, new Measure(1., Uscs.TABLESPOON).to(SI.VOLUME.MILLILITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(3., new Measure(1., Uscs.TABLESPOON).to(Uscs.TEASPOON).getValue(), 1e-13);
        Assert.assertEquals(4., new Measure(1., Uscs.TABLESPOON).to(Uscs.FLUID_DRAM).getValue(), 1e-13);
        Assert.assertEquals(29.5735295625, new Measure(1., Uscs.FLUID_OUNCE).to(SI.VOLUME.MILLILITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(2., new Measure(1., Uscs.FLUID_OUNCE).to(Uscs.TABLESPOON).getValue(), 1e-13);
        Assert.assertEquals(44.36029434375, new Measure(1., Uscs.SHOT).to(SI.VOLUME.MILLILITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(1.5, new Measure(1., Uscs.SHOT).to(Uscs.FLUID_OUNCE).getValue(), 1e-13);
        Assert.assertEquals(3., new Measure(1., Uscs.SHOT).to(Uscs.TABLESPOON).getValue(), 1e-13);
        Assert.assertEquals(118.29411825, new Measure(1., Uscs.GILL).to(SI.VOLUME.MILLILITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(8./3., new Measure(1., Uscs.GILL).to(Uscs.SHOT).getValue(), 1e-13);
        Assert.assertEquals(4., new Measure(1., Uscs.GILL).to(Uscs.FLUID_OUNCE).getValue(), 1e-13);
        Assert.assertEquals(236.5882365, new Measure(1., Uscs.CUP).to(SI.VOLUME.MILLILITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(2., new Measure(1., Uscs.CUP).to(Uscs.GILL).getValue(), 1e-13);
        Assert.assertEquals(8., new Measure(1., Uscs.CUP).to(Uscs.FLUID_OUNCE).getValue(), 1e-13);
        Assert.assertEquals(473.176473, new Measure(1., Uscs.LIQUID_PINT).to(SI.VOLUME.MILLILITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(2., new Measure(1., Uscs.LIQUID_PINT).to(Uscs.CUP).getValue(), 1e-13);
        Assert.assertEquals(0.946352946, new Measure(1., Uscs.LIQUID_QUART).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(2., new Measure(1., Uscs.LIQUID_QUART).to(Uscs.LIQUID_PINT).getValue(), 1e-13);
        Assert.assertEquals(3.785411784, new Measure(1., Uscs.LIQUID_GALLON).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(4., new Measure(1., Uscs.LIQUID_GALLON).to(Uscs.LIQUID_QUART).getValue(), 1e-13);
        Assert.assertEquals(231., new Measure(1., Uscs.LIQUID_GALLON).to(Uscs.CUBIC_INCH).getValue(), 1e-13);
        Assert.assertEquals(119.240471196, new Measure(1., Uscs.LIQUID_BARREL).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(31.5, new Measure(1., Uscs.LIQUID_BARREL).to(Uscs.LIQUID_GALLON).getValue(), 1e-13);
        Assert.assertEquals(.5, new Measure(1., Uscs.LIQUID_BARREL).to(Uscs.HOGSHEAD).getValue(), 1e-13);
        Assert.assertEquals(158.987294928, new Measure(1., Uscs.OIL_BARREL).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(4./3., new Measure(1., Uscs.OIL_BARREL).to(Uscs.LIQUID_BARREL).getValue(), 1e-13);
        Assert.assertEquals(42., new Measure(1., Uscs.OIL_BARREL).to(Uscs.LIQUID_GALLON).getValue(), 1e-13);
        Assert.assertEquals(2./3., new Measure(1., Uscs.OIL_BARREL).to(Uscs.HOGSHEAD).getValue(), 1e-13);
        Assert.assertEquals(238.480942392, new Measure(1., Uscs.HOGSHEAD).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(1.5, new Measure(1., Uscs.HOGSHEAD).to(Uscs.OIL_BARREL).getValue(), 1e-13);
        Assert.assertEquals(63., new Measure(1., Uscs.HOGSHEAD).to(Uscs.LIQUID_GALLON).getValue(), 1e-13);
        Assert.assertEquals(8.421875, new Measure(1., Uscs.HOGSHEAD).to(Uscs.CUBIC_FOOT).getValue(), 1e-13);

        Assert.assertEquals(.5506104713575, new Measure(1., Uscs.DRY_PINT).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(33.6003125, new Measure(1., Uscs.DRY_PINT).to(Uscs.CUBIC_INCH).getValue(), 1e-13);
        Assert.assertEquals(1.101221, new Measure(1., Uscs.DRY_QUART).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-7);
        Assert.assertEquals(2., new Measure(1., Uscs.DRY_QUART).to(Uscs.DRY_PINT).getValue(), 1e-13);
        Assert.assertEquals(4.404884, new Measure(1., Uscs.DRY_GALLON).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-6);
        Assert.assertEquals(4., new Measure(1., Uscs.DRY_GALLON).to(Uscs.DRY_QUART).getValue(), 1e-13);
        Assert.assertEquals(8.809768, new Measure(1., Uscs.PECK).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-6);
        Assert.assertEquals(2., new Measure(1., Uscs.PECK).to(Uscs.DRY_GALLON).getValue(), 1e-13);
        Assert.assertEquals(35.23907016688, new Measure(1., Uscs.BUSHEL).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(4., new Measure(1., Uscs.BUSHEL).to(Uscs.PECK).getValue(), 1e-13);
        Assert.assertEquals(115.6271, new Measure(1., Uscs.DRY_BARREL).to(SI.VOLUME.LITRE, USCS_TO_SI).getValue(), 1e-4);
        Assert.assertEquals(7056., new Measure(1., Uscs.DRY_BARREL).to(Uscs.CUBIC_INCH).getValue(), 1e-11);
        Assert.assertEquals(3.281, new Measure(1., Uscs.DRY_BARREL).to(Uscs.BUSHEL).getValue(), 1e-3);

        // mass -- avoirdupois

        Assert.assertEquals(64.79891, new Measure(1., Uscs.GRAIN).to(SI.MASS.MILLIGRAMME, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(1. / 7000., new Measure(1., Uscs.GRAIN).to(Uscs.AVOIRDUPOIS_POUND).getValue(), 1e-13);
        Assert.assertEquals(1.7718451953125, new Measure(1., Uscs.AVOIRDUPOIS_DRAM).to(SI.GRAMME, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(27. + 11./ 32., new Measure(1., Uscs.AVOIRDUPOIS_DRAM).to(Uscs.GRAIN).getValue(), 1e-13);
        Assert.assertEquals(28.349523125, new Measure(1., Uscs.AVOIRDUPOIS_OUNCE).to(SI.GRAMME, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(16., new Measure(1., Uscs.AVOIRDUPOIS_OUNCE).to(Uscs.AVOIRDUPOIS_DRAM).getValue(), 1e-13);
        Assert.assertEquals(453.59237, new Measure(1., Uscs.AVOIRDUPOIS_POUND).to(SI.GRAMME, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(16., new Measure(1., Uscs.AVOIRDUPOIS_POUND).to(Uscs.AVOIRDUPOIS_OUNCE).getValue(), 1e-13);
        Assert.assertEquals(45.359237, new Measure(1., Uscs.AVOIRDUPOIS_SHORT_HUNDREDWEIGHT).to(SI.KILOGRAMME, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(100., new Measure(1., Uscs.AVOIRDUPOIS_SHORT_HUNDREDWEIGHT).to(Uscs.AVOIRDUPOIS_POUND).getValue(), 1e-13);
        Assert.assertEquals(50.80234544, new Measure(1., Uscs.AVOIRDUPOIS_LONG_HUNDREDWEIGHT).to(SI.KILOGRAMME, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(112., new Measure(1., Uscs.AVOIRDUPOIS_LONG_HUNDREDWEIGHT).to(Uscs.AVOIRDUPOIS_POUND).getValue(), 1e-13);
        Assert.assertEquals(907.18474, new Measure(1., Uscs.AVOIRDUPOIS_SHORT_TON).to(SI.KILOGRAMME, USCS_TO_SI).getValue(), 1e-12);
        Assert.assertEquals(20., new Measure(1., Uscs.AVOIRDUPOIS_SHORT_TON).to(Uscs.AVOIRDUPOIS_SHORT_HUNDREDWEIGHT).getValue(), 1e-13);
        Assert.assertEquals(1016.0469088, new Measure(1., Uscs.AVOIRDUPOIS_LONG_TON).to(SI.KILOGRAMME, USCS_TO_SI).getValue(), 1e-12);
        Assert.assertEquals(20., new Measure(1., Uscs.AVOIRDUPOIS_LONG_TON).to(Uscs.AVOIRDUPOIS_LONG_HUNDREDWEIGHT).getValue(), 1e-13);

        // mass -- troy

        Assert.assertEquals(1. / 5760., new Measure(1., Uscs.GRAIN).to(Uscs.TROY_POUND).getValue(), 1e-13);
        Assert.assertEquals(1.55517384, new Measure(1., Uscs.TROY_PENNYWEIGHT).to(SI.GRAMME, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(24., new Measure(1., Uscs.TROY_PENNYWEIGHT).to(Uscs.GRAIN).getValue(), 1e-13);
        Assert.assertEquals(31.1034768, new Measure(1., Uscs.TROY_OUNCE).to(SI.GRAMME, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(20., new Measure(1., Uscs.TROY_OUNCE).to(Uscs.TROY_PENNYWEIGHT).getValue(), 1e-13);
        Assert.assertEquals(373.2417216, new Measure(1., Uscs.TROY_POUND).to(SI.GRAMME, USCS_TO_SI).getValue(), 1e-13);
        Assert.assertEquals(12., new Measure(1., Uscs.TROY_POUND).to(Uscs.TROY_OUNCE).getValue(), 1e-13);
    }

    @Test
    public void test2() {
        final Measure unMeasure1 = new Measure(0, Uscs.FAHRENHEIT);
        final UnitConverter unConverter1a = Units.converter(unMeasure1.getUnit(), SI.CELSIUS, USCS_TO_SI);
        Assert.assertEquals(-17.778, unConverter1a.convert(unMeasure1.getValue()), 1e-3);

        Assert.assertEquals(0., unConverter1a.convert(32.), 1e-14);
        Assert.assertEquals(37., unConverter1a.convert(98.6), 1e-14);
        Assert.assertEquals(37.777, unConverter1a.convert(100.), 1e-3);
        Assert.assertEquals(99.975, unConverter1a.convert(211.955), 1e-13);
        Assert.assertEquals(100., unConverter1a.convert(212.), 1e-13);

        final Measure unMeasure2 = new Measure(1., Uscs.INTERNATIONAL_ACRE_FOOT);
        final UnitConverter unConverter2a = Units.converter(unMeasure2.getUnit(), Uscs.CUBIC_YARD);
        Assert.assertEquals(1613.333, unConverter2a.convert(unMeasure2.getValue()), 1e-3);
        Assert.assertEquals(2 * 1613.333, unConverter2a.convert(2.), 1e-3);

        final Measure unMeasure3 = new Measure(1., Uscs.INTERNATIONAL_ACRE_FOOT);
        final UnitConverter unConverter3a = Units.converter(unMeasure3.getUnit(), SI.VOLUME.MEGALITRE, USCS_TO_SI);
        Assert.assertEquals(1.23348183754752, unConverter3a.convert(unMeasure3.getValue()), 1e-15);
        Assert.assertEquals(3. * 1.23348183754752, unConverter3a.convert(3.), 1e-15);

        final Measure unMeasure4 = new Measure(1., FOOT_PER_DEGREE_FAHRENHEIT);
        final UnitConverter unConverter4a = Units.converter(unMeasure4.getUnit(), METRE_PER_DEGREE_KELVIN, USCS_TO_SI);
        Assert.assertEquals(.54864, unConverter4a.convert(unMeasure4.getValue()), 1e-14);
        Assert.assertEquals(2. * .54864, unConverter4a.convert(2.), 1e-14);
    }
}
