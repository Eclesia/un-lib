package science.unlicense.unit.si;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.unit.api.Measure;
import science.unlicense.math.api.unit.Quantity;
import science.unlicense.unit.api.UnitBuilder;
import science.unlicense.unit.api.UnitConverter;
import science.unlicense.unit.api.Units;
import science.unlicense.math.api.unit.Dimension;
import science.unlicense.unit.api.unit.DerivedUnit;
import science.unlicense.unit.impl.SI;

/**
 *
 * @author Samuel Andrés
 */
public class UnitSiTest {

    @Deprecated
    private static final Dimension NULLDIM = new Dimension() {
        @Override
        public Chars name() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Quantity quantity() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    };

    // définitions d'unités bizarres pour les tests !

    public static final DerivedUnit METRE_PER_SQUARE_SECOND2 = new UnitBuilder()
            .addName(Chars.constant("mètre par seconde carrée"))
            .addFactor(SI.METRE_PAR_SECONDE)
            .addFactor(SI.SECONDE, -1)
            .buildDerived(SI.DIMENSION.ACCELERATION);

    public static final DerivedUnit KILOMETRE_PER_SQUARE_SECOND = new UnitBuilder()
            .addName(Chars.constant("kilomètre par seconde carrée"))
            .addFactor(SI.LENGTH.KILOMETRE)
            .addFactor(SI.SECONDE, -2)
            .buildDerived(SI.DIMENSION.ACCELERATION);

    public static final DerivedUnit KILOMETRE_PER_SQUARE_MINUTE = new UnitBuilder()
            .addName(Chars.constant("kilomètre par minute carrée"))
            .addFactor(SI.LENGTH.KILOMETRE)
            .addFactor(SI.MINUTE, -2)
            .buildDerived(SI.DIMENSION.ACCELERATION);

    public static final DerivedUnit KILOMETRE_PER_MINUTE_SECOND = new UnitBuilder()
            .addName(Chars.constant("kilomètre par minute seconde"))
            .addFactor(SI.LENGTH.KILOMETRE)
            .addFactor(SI.MINUTE, -1)
            .addFactor(SI.SECONDE, -1)
            .buildDerived(SI.DIMENSION.ACCELERATION);

    public static final DerivedUnit METRE_PER_DEGREE_KELVIN = new UnitBuilder()
            .addName(Chars.constant("mètre par kelvin"))
            .addFactor(SI.METRE)
            .addFactor(SI.KELVIN, -1)
            .buildDerived(NULLDIM);

    public static final DerivedUnit CELSIUS_PER_SQUARE_KILOMETRE = new UnitBuilder()
            .addName(Chars.constant("celsius par kilomètre carré"))
            .addFactor(SI.CELSIUS)
            .addFactor(SI.LENGTH.KILOMETRE, -2)
            .buildDerived(NULLDIM);

    // conversions par mesure
    @Test
    public void test1() {

        final Measure measure2 = new Measure(1., SI.KILOGRAMME);
        Assert.assertEquals(1., measure2.getValue(), 1e-14);
        Assert.assertEquals(1000., measure2.to(SI.GRAMME).getValue(), 1e-14);
        Assert.assertEquals(1., measure2.to(SI.GRAMME).to(SI.KILOGRAMME).getValue(), 1e-14);
        Assert.assertEquals(1000000., measure2.to(SI.GRAMME).to(SI.MASS.MILLIGRAMME).getValue(), 1e-14);
        Assert.assertEquals(1., measure2.to(SI.GRAMME).to(SI.MASS.MILLIGRAMME)
                .to(SI.KILOGRAMME).getValue(), 1e-14);
        Assert.assertEquals(1000000., measure2.to(SI.GRAMME).to(SI.MASS.MILLIGRAMME)
                .to(SI.KILOGRAMME).to(SI.MASS.MILLIGRAMME).getValue(), 1e-14);


        final Measure measure3 = new Measure(1., SI.VOLT);
        Assert.assertEquals(1., measure3.getValue(), 1e-14);
        Assert.assertEquals(1., measure3.to(SI.VOLT).getValue(), 1e-14);
        Assert.assertEquals(1000., measure3.to(SI.VOLT).to(SI.MILLIVOLT).getValue(), 1e-14);
        Assert.assertEquals(1., measure3.to(SI.VOLT).to(SI.MILLIVOLT)
                .to(SI.VOLT).getValue(), 1e-14);
        Assert.assertEquals(1000., measure3.to(SI.VOLT).to(SI.MILLIVOLT)
                .to(SI.VOLT).to(SI.MILLIVOLT).getValue(), 1e-14);
        Assert.assertEquals(.001, measure3.to(SI.VOLT).to(SI.KILOVOLT).getValue(), 1e-14);
        Assert.assertEquals(.001, measure3.to(SI.VOLT).to(SI.MILLIVOLT)
                .to(SI.KILOVOLT).getValue(), 1e-14);
        Assert.assertEquals(.001, measure3.to(SI.VOLT).to(SI.MILLIVOLT)
                .to(SI.KILOVOLT).to(SI.KILOVOLT).getValue(), 1e-14);



        final Measure measure4 = new Measure(1., SI.METRE_PAR_SECONDE);
        Assert.assertEquals(1., measure4.getValue(), 0.);

        final Measure measure5 = new Measure(1., SI.METRE_PAR_SECONDE_CARREE);
        Assert.assertEquals(1., measure5.getValue(), 0.);

        final Measure measure6 = new Measure(1., METRE_PER_SQUARE_SECOND2);
        Assert.assertEquals(1., measure6.getValue(), 0.);
        Assert.assertEquals(.001, measure6.to(KILOMETRE_PER_SQUARE_SECOND).getValue(), 1e-14);
        Assert.assertEquals(3.6, measure6.to(KILOMETRE_PER_SQUARE_MINUTE).getValue(), 1e-14);
        Assert.assertEquals(.06, measure6.to(KILOMETRE_PER_MINUTE_SECOND).getValue(), 1e-14);


        final Measure measure7 = new Measure(1., SI.SECONDE);
        Assert.assertEquals(1., measure7.getValue(), 0.);
        final Measure measure8 = new Measure(1., SI.MINUTE);
        Assert.assertEquals(1., measure8.getValue(), 0.);
        final Measure measure9 = new Measure(1., SI.HEURE);
        Assert.assertEquals(1., measure9.getValue(), 0.);
        Assert.assertEquals(60., measure9.to(SI.MINUTE).getValue(), 1e-14);
        Assert.assertEquals(3600., measure9.to(SI.SECONDE).getValue(), 1e-14);

        final Measure measure10 = new Measure(0., SI.KELVIN);
        Assert.assertEquals(0., measure10.getValue(), 0.);
        Assert.assertEquals(-273.15, measure10.to(SI.CELSIUS).getValue(), 0.);
        Assert.assertEquals("-273.15°C", measure10.to(SI.CELSIUS).toString());
        final Measure measure11 = new Measure(0., SI.CELSIUS);
        Assert.assertEquals(0., measure11.getValue(), 0.);
        Assert.assertEquals(273.15, measure11.to(SI.KELVIN).getValue(), 0.);
        Assert.assertEquals("273.15K", measure11.to(SI.KELVIN).toString());

        Assert.assertEquals(3600., new Measure(1., SI.WATTHEURE).to(SI.JOULE).getValue(), 1e-14);
        Assert.assertEquals(4.1868, new Measure(1., SI.CALORIE_IT).to(SI.JOULE).getValue(), 1e-14);
        Assert.assertEquals(
                3600. / 4.1868, new Measure(1., SI.WATTHEURE).to(SI.CALORIE_IT).getValue(), 1e-14);
        Assert.assertEquals(
                4.1868 / 3600., new Measure(1., SI.CALORIE_IT).to(SI.WATTHEURE).getValue(), 1e-14);

        // équivalence des deux unités de mesure du champ électrique
        Assert.assertEquals(4.6, new Measure(4.6, SI.VOLT_PAR_METRE).to(SI.NEWTON_PAR_COULOMB).getValue(), 1e-14);
        Assert.assertEquals(4.6, new Measure(4.6, SI.NEWTON_PAR_COULOMB).to(SI.VOLT_PAR_METRE).getValue(), 1e-14);
    }

    // récupération des dimensions
    @Test
    public void test2() {
        Assert.assertEquals(SI.DIMENSION.TEMPS, SI.SECONDE.dimension()[0]);
        Assert.assertEquals(SI.DIMENSION.LONGUEUR, SI.METRE.dimension()[0]);
        Assert.assertEquals(SI.DIMENSION.MASSE, SI.KILOGRAMME.dimension()[0]);
        Assert.assertEquals(SI.DIMENSION.MASSE, SI.GRAMME.dimension()[0]);
    }

    // bel, décibel, unité logarithmique de niveau
    @Test
    public void test4() {

        final Measure measure = new Measure(120., SI.DECIBEL);
        Assert.assertEquals(12., measure.to(SI.BEL).getValue(), 0.);
        Assert.assertEquals(1., measure.to(SI.WATT_PAR_METRE_CARRE).getValue(), 0.);

        final Measure measure1 = new Measure(1., SI.WATT_PAR_METRE_CARRE);
        Assert.assertEquals(120., measure1.to(SI.DECIBEL).getValue(), 0.);
        Assert.assertEquals(12., measure1.to(SI.BEL).getValue(), 0.);
    }

    // conversion via convertisseurs
    @Test
    public void test_5() {

        final Measure measure2 = new Measure(1., SI.KILOGRAMME);
        final UnitConverter converter2a = Units.converter(measure2.getUnit(), SI.GRAMME);
        Assert.assertEquals(1000., converter2a.convert(measure2.getValue()), 1e-14);
        final UnitConverter converter2b = Units.converter(measure2.getUnit(), SI.KILOGRAMME);
        Assert.assertEquals(1., converter2b.convert(measure2.getValue()), 1e-14);
        final UnitConverter converter2c = Units.converter(measure2.getUnit(), SI.MASS.MILLIGRAMME);
        Assert.assertEquals(1000000., converter2c.convert(measure2.getValue()), 1e-14);

        final Measure measure3 = new Measure(1000., SI.GRAMME);
        final UnitConverter converter3a = Units.converter(measure3.getUnit(), SI.GRAMME);
        Assert.assertEquals(1000., converter3a.convert(measure3.getValue()), 1e-14);
        final UnitConverter converter3b = Units.converter(measure3.getUnit(), SI.KILOGRAMME);
        Assert.assertEquals(1., converter3b.convert(measure3.getValue()), 1e-14);
        final UnitConverter converter3c = Units.converter(measure3.getUnit(), SI.MASS.MILLIGRAMME);
        Assert.assertEquals(1000000., converter3c.convert(measure3.getValue()), 1e-14);

        final Measure measure31 = new Measure(0., SI.KELVIN);
        final UnitConverter converter31a = Units.converter(measure31.getUnit(), SI.CELSIUS);
        Assert.assertEquals(-273.15, converter31a.convert(measure31.getValue()), 0.);
        final Measure measure32 = new Measure(0., SI.CELSIUS);
        final UnitConverter converter32a = Units.converter(measure32.getUnit(), SI.KELVIN);
        Assert.assertEquals(273.15, converter32a.convert(measure32.getValue()), 0.);

        final Measure measure4 = new Measure(1., SI.METRE_PAR_SECONDE);
        Assert.assertEquals(1., measure4.getValue(), 0.);

        final Measure measure5 = new Measure(1., SI.METRE_PAR_SECONDE_CARREE);
        Assert.assertEquals(1., measure5.getValue(), 0.);
        final UnitConverter converter5a = Units.converter(measure5.getUnit(), METRE_PER_SQUARE_SECOND2);
        Assert.assertEquals(1., converter5a.convert(measure5.getValue()), 0.);

        final Measure measure6 = new Measure(1., METRE_PER_SQUARE_SECOND2);
        Assert.assertEquals(1., measure6.getValue(), 0.);
        final UnitConverter converter6a = Units.converter(measure6.getUnit(), KILOMETRE_PER_SQUARE_SECOND);
        Assert.assertEquals(.001, converter6a.convert(measure6.getValue()), 1e-14);
        final UnitConverter converter6b = Units.converter(measure6.getUnit(), KILOMETRE_PER_SQUARE_MINUTE);
        Assert.assertEquals(3.6, converter6b.convert(measure6.getValue()), 1e-14);
        final UnitConverter converter6c = Units.converter(measure6.getUnit(), KILOMETRE_PER_MINUTE_SECOND);
        Assert.assertEquals(.06, converter6c.convert(measure6.getValue()), 1e-14);

        final Measure measure7 = new Measure(1., SI.WATTHEURE);
        final UnitConverter converter7a = Units.converter(measure7.getUnit(), SI.JOULE);
        Assert.assertEquals(3600., converter7a.convert(measure7.getValue()), 1e-14);

        final Measure measure8 = new Measure(1., SI.CALORIE_IT);
        final UnitConverter converter8a = Units.converter(measure8.getUnit(), SI.JOULE);
        Assert.assertEquals(4.1868, converter8a.convert(measure8.getValue()), 1e-14);

        final Measure measure9 = new Measure(1., SI.WATTHEURE);
        final UnitConverter converter9a = Units.converter(measure9.getUnit(), SI.CALORIE_IT);
        Assert.assertEquals(
                3600. / 4.1868, converter9a.convert(measure9.getValue()), 1e-14);

        final Measure measure10 = new Measure(1., SI.CALORIE_IT);
        final UnitConverter converter10a = Units.converter(measure10.getUnit(), SI.WATTHEURE);
        Assert.assertEquals(
                4.1868 / 3600., converter10a.convert(measure10.getValue()), 1e-14);

        // équivalence des deux unités de mesure du champ électrique
        final Measure measure11 = new Measure(4.6, SI.VOLT_PAR_METRE);
        final UnitConverter converter11a = Units.converter(measure11.getUnit(), SI.NEWTON_PAR_COULOMB);
        Assert.assertEquals(4.6, converter11a.convert(measure11.getValue()), 1e-14);

        final Measure measure12 = new Measure(4.6, SI.NEWTON_PAR_COULOMB);
        final UnitConverter converter12a = Units.converter(measure12.getUnit(), SI.VOLT_PAR_METRE);
        Assert.assertEquals(4.6, converter12a.convert(measure12.getValue()), 1e-14);
    }
}
