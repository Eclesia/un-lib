package science.unlicense.unit.si;

import science.unlicense.unit.impl.SI;
import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 *
 * @author Samuel Andrés
 */
public class ConstantSiTest {

    @Test
    public void test() {
        Assert.assertEquals(8.854187817620389E-12, SI.CONSTANT.EPSILON_0.getValue(), 1e-14);
    }
}
