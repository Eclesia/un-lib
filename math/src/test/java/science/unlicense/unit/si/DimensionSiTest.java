package science.unlicense.unit.si;

import science.unlicense.unit.impl.SI;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.Quantities;
import science.unlicense.math.api.unit.DefinedDimension;
import science.unlicense.math.api.unit.DimensionBuilder;

/**
 *
 * @author Samuel Andrés
 */
public class DimensionSiTest {

    // définition de dimensions bizarres pour les tests !

    public static final DefinedDimension ACCELERATION2 = new DimensionBuilder(Chars.constant("acceleration"),
            Quantities.ACCELERATION)
            .addFactor(SI.DIMENSION.LONGUEUR)
            .addFactor(SI.DIMENSION.TEMPS, -2).build();

    public static final DefinedDimension ACCELERATION3 = new DimensionBuilder(Chars.constant("acceleration"),
            Quantities.ACCELERATION)
            .addFactor(SI.DIMENSION.LONGUEUR)
            .addFactor(SI.DIMENSION.TEMPS, -1)
            .addFactor(SI.DIMENSION.TEMPS, -1).build();

    public static final DefinedDimension ACCELERATION4 = new DimensionBuilder(Chars.constant("acceleration"),
            Quantities.ACCELERATION)
            .addFactor(ACCELERATION3).build();

    @Test
    public void test_1() {

        Assert.assertEquals("L.T⁻¹", SI.DIMENSION.VITESSE.toString());
        Assert.assertEquals("L.T⁻²", SI.DIMENSION.ACCELERATION.toString());
        Assert.assertEquals("L.T⁻²", ACCELERATION2.toString());
        Assert.assertEquals("L.T⁻²", ACCELERATION3.toString());
        Assert.assertEquals("L.T⁻²", ACCELERATION4.toString());
        Assert.assertEquals("L².M.T⁻³", SI.DIMENSION.PUISSANCE.toString());
    }

}
