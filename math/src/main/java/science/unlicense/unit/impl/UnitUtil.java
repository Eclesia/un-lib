package science.unlicense.unit.impl;

import java.util.Map;
import science.unlicense.math.impl.Affine1;
import science.unlicense.unit.api.UnitMapper;
import science.unlicense.math.api.unit.Base;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.math.api.unit.BaseUnit;
import science.unlicense.unit.api.unit.DerivedUnit;
import science.unlicense.unit.api.unit.FundamentalUnit;
import science.unlicense.unit.api.unit.LevelUnit;
import science.unlicense.unit.api.unit.TransformedUnit;
import science.unlicense.math.api.unit.Unit;
import science.unlicense.unit.impl.base.DefaultBase;
import science.unlicense.unit.impl.base.UnitBaseBuilder;

/**
 *
 * @author Samuel Andrés
 */
public final class UnitUtil {

    public static final Affine1 IDENTITY = new Affine1(1, 0);

    private UnitUtil(){}

    public static final String exponent(final int value) {
        return String.valueOf(value)
                .replace('0', '\u2070')
                .replace('1', '\u00B9')
                .replace('2', '\u00B2')
                .replace('3', '\u00B3')
                .replace('4', '\u2074')
                .replace('5', '\u2075')
                .replace('6', '\u2076')
                .replace('7', '\u2077')
                .replace('8', '\u2078')
                .replace('9', '\u2079')
                .replace('+', '\u207A')
                .replace('-', '\u207B');
    }

    public static Base<BaseUnit> getBase(final Unit unit) {
        return new DefaultBase<>(new UnitBaseBuilder(unit, 1, 1).build());
    }

//    public static double mapAndTransform(final Factor<? extends Unit> thisEntry, final UnitMapper mapper) {
//
//        final Map.Entry<UnitTransformation, Unit> transformParam = mapper.transformation(thisEntry.dim());
//
//        // si on trouve une transformation connaissant l'unité de base de départ
//        if (transformParam != null) {
//
//            LOGGER.trace("transformation for {} ", thisEntry.dim());
//
//            // on applique la transformation
//            valueInOtherBase = Math.pow(transformParam.getKey().transform(valueInOtherBase), thisEntry.power());
//
//            /*
//            puis on exprime la valeur dans la base de l'unité cible à partir de l'unité résultant de la transformation
//            */
//            return UnitUtil.valueToBase(valueInOtherBase, transformParam.getValue());
//        }
//        throw new IllegalArgumentException("no mapping found for " + thisEntry.dim());
//    }

    public static double mapAndScale(final Factor<? extends Unit> thisEntry, final UnitMapper mapper) {

        final Map.Entry<Affine1, Unit> transformParam = mapper.transformation(thisEntry.dim());

        // si on trouve une transformation connaissant l'unité de base de départ
        if (transformParam != null) {

            // on applique la transformation
            final double mapScaling = Math.pow(transformParam.getKey().getScale(), thisEntry.power());

            /*
            puis on exprime la valeur dans la base de l'unité cible à partir de l'unité résultant de la transformation
             */
            final double scaleToBase = UnitUtil.scaleToBase(transformParam.getValue());

            return mapScaling * scaleToBase;
        }
        throw new IllegalArgumentException("no mapping found for " + thisEntry.dim());
    }

    private static double scaleToBase(final Unit unit) {
        if (unit instanceof FundamentalUnit) {
            return 1.;
        } else if (unit instanceof TransformedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            final double scale = scaleToBase(((TransformedUnit) unit).referenceUnit());
            final Affine1 tr = ((TransformedUnit) unit).transformation();
            return scale * tr.getScale();
        } else if (unit instanceof LevelUnit) {
            throw new IllegalStateException("unsuported unit type");
        } else if (unit instanceof DerivedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            double scaling = 1.;
            for (final Factor<Unit> factor : ((DerivedUnit) unit).definitionUnits()) {
                scaling *= Math.pow(scaleToBase(factor.dim()), factor.power());
            }
            return scaling;
        }
        throw new UnsupportedOperationException(unit.symbol().toString());
    }
}
