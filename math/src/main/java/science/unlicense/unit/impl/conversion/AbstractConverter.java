package science.unlicense.unit.impl.conversion;

import science.unlicense.unit.api.UnitConverter;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public abstract class AbstractConverter implements UnitConverter {

    protected final Unit source;
    protected final Unit target;

    public AbstractConverter(final Unit source, final Unit target) {
        this.source = source;
        this.target = target;
    }

    @Override
    public Unit getSource() {
        return this.source;
    }

    @Override
    public Unit getTarget() {
        return this.target;
    }

    @Override
    public double convert(double value) {
        return getTransform().transform(value);
    }
}
