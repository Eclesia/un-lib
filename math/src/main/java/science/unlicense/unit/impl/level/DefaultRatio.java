package science.unlicense.unit.impl.level;

import science.unlicense.unit.api.Measure;

/**
 *
 * @author Samuel Andrés
 */
public class DefaultRatio extends AbstractLevel {

    public DefaultRatio(final Measure reference) {
        super(reference);
    }

    @Override
    public double toLevel(final double value) {
        return value / this.referenceMeasure().getValue();
    }

    @Override
    public double toReferenceUnit(final double value) {
        return value * this.referenceMeasure().getValue();
    }
}
