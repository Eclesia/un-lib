package science.unlicense.unit.impl.unit;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.BaseDimension;
import science.unlicense.math.impl.unit.AbstractUnit;
import science.unlicense.unit.api.unit.FundamentalUnit;

/**
 *
 * @author Samuel Andrés
 */
public class DefaultFundamentalUnit extends AbstractUnit implements FundamentalUnit {

    public DefaultFundamentalUnit(final Chars name, final Chars[] alternativeNames, final Chars symbol,
            final Chars[] alternativeSymbols, final BaseDimension... baseDimension) {
        super(name, symbol, alternativeNames, alternativeSymbols, baseDimension);
    }

    @Override
    public BaseDimension[] dimension() {
        return (BaseDimension[]) super.dimension();
    }

}
