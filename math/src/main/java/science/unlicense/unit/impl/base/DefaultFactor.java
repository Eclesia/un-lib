package science.unlicense.unit.impl.base;

import java.util.Objects;
import science.unlicense.math.api.unit.Dimensional;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.unit.impl.UnitUtil;

/**
 *
 * @author Samuel Andrés
 * @param <E>
 */
public class DefaultFactor<E extends Dimensional<?>> implements Factor<E> {

    private final int numerator;
    private final int denominator;
    private final E entity;

    public DefaultFactor(final E entity, final int numerator, final int denominator) {
        this.entity = entity;
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public DefaultFactor(final E entity, final int numerator) {
        this(entity, numerator, 1);
    }

    public DefaultFactor(final E dimension) {
        this(dimension, 1);
    }

    @Override
    public E dim() {
        return this.entity;
    }

    @Override
    public double power() {
        return this.denominator == 1 ? this.numerator : this.numerator / (double) this.denominator;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.numerator;
        hash = 97 * hash + this.denominator;
        hash = 97 * hash + Objects.hashCode(this.entity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultFactor<?> other = (DefaultFactor<?>) obj;
        if (this.numerator != other.numerator) {
            return false;
        }
        if (this.denominator != other.denominator) {
            return false;
        }
        if (!Objects.equals(this.entity, other.entity)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        if (this.denominator == 1) {
            return this.entity.toString()+UnitUtil.exponent(this.powerNumerator());
        } else {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public int powerNumerator() {
        return this.numerator;
    }

    @Override
    public int powerDenominator() {
        return this.denominator;
    }
}
