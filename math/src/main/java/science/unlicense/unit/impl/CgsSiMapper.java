package science.unlicense.unit.impl;

import science.unlicense.math.impl.Affine1;


/**
 *
 * @author Samuel Andrés
 */
public class CgsSiMapper extends DefaultUnitMapper {

    private static final CgsSiMapper INSTANCE = new CgsSiMapper();

    public static final double C = 2_997_924_580.;

    protected CgsSiMapper() {
        addToMap(Cgs.CENTIMETRE, new Affine1(1.0 / 100.0, 0.0), SI.METRE);
        addToMap(Cgs.SECONDE, UnitUtil.IDENTITY, SI.SECONDE);
        addToMap(Cgs.GRAMME, new Affine1(1.0 / 1000.0, 0.0), SI.KILOGRAMME);
    }

    public static CgsSiMapper getInstance() {
        return INSTANCE;
    }
}
