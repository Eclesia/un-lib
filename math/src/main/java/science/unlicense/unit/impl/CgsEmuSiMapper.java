package science.unlicense.unit.impl;

import science.unlicense.math.impl.Affine1;

/**
 *
 * @author Samuel Andrés
 */
public class CgsEmuSiMapper extends CgsSiMapper {

    private static final CgsEmuSiMapper INSTANCE = new CgsEmuSiMapper();

    public static final double C = 2_997_924_580.;

    protected CgsEmuSiMapper() {
        addToMap(CgsEmu.ABAMPERE, new Affine1(10.0, 0.0), SI.AMPERE);
    }

    public static CgsEmuSiMapper getInstance() {
        return INSTANCE;
    }
}
