package science.unlicense.unit.impl.level;

import science.unlicense.unit.api.level.Level;
import science.unlicense.unit.api.Measure;

/**
 *
 * @author Samuel Andrés
 */
public abstract class AbstractLevel implements Level {

    private final Measure reference;

    public AbstractLevel(final Measure reference) {
        this.reference = reference;
    }

    @Override
    public Measure referenceMeasure() {
        return this.reference;
    }
}
