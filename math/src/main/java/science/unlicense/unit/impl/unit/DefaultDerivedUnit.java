package science.unlicense.unit.impl.unit;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.math.api.unit.Dimension;
import science.unlicense.math.impl.unit.AbstractUnit;
import science.unlicense.unit.api.unit.DerivedUnit;
import science.unlicense.math.api.unit.Unit;
import science.unlicense.unit.impl.UnitUtil;

/**
 *
 * @author Samuel Andrés
 */
public class DefaultDerivedUnit extends AbstractUnit implements DerivedUnit {

    private final Factor<Unit>[] definitionUnits;

    public DefaultDerivedUnit(final Chars name, final Chars[] alternativeNames, final Chars symbol,
            final Chars[] alternativeSymbols, final Dimension[] dimension,
            final Factor<Unit>... definitionUnits) {
        super(name, symbol, alternativeNames, alternativeSymbols, dimension);
        this.definitionUnits = definitionUnits;
    }

    @Override
    public Factor<Unit>[] definitionUnits() {
        return this.definitionUnits;
    }

    @Override
    public Chars symbol() {
        if (!symbol.isEmpty()) {
            return symbol;
        } else {
            final CharBuffer sb = new CharBuffer();
            boolean first = true;
            for (final Factor<Unit> b : definitionUnits()) {
                if (first == true){
                    first = false;
                } else {
                    sb.append('.');
                }
                final Unit unit = b.dim();
                final int power = b.powerNumerator();
                if (power != 1 && unit instanceof DerivedUnit && !((DerivedUnit) unit).hasProperSymbol()) {
                    sb.append('(').append(unit.symbol()).append(')');
                }
                else {
                    sb.append(unit.symbol());
                }
                if (power != 1) {
                    sb.append(UnitUtil.exponent(b.powerNumerator()));
                }
            }
            return sb.toChars();
        }
    }

    @Override
    public boolean hasProperSymbol() {
        return symbol != null;
    }

}
