package science.unlicense.unit.impl.unit;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.impl.unit.AbstractUnit;
import science.unlicense.unit.api.unit.TransformedUnit;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public class DefaultTransformedUnit extends AbstractUnit implements TransformedUnit {

    private final Unit definitionUnit;
    private final Affine1 transformation;

    public DefaultTransformedUnit(final Chars name, final Chars[] alternativeNames, final Chars symbol,
            final Chars[] alternativeSymbols, final Affine1 transformation, final Unit definitionUnit)
    {
        super(name, symbol, alternativeNames, alternativeSymbols, definitionUnit.dimension());
        this.definitionUnit = definitionUnit;
        this.transformation = transformation;
    }

    @Override
    public Unit referenceUnit() {
        return definitionUnit;
    }

    @Override
    public Affine1 transformation() {
        return transformation;
    }

}
