package science.unlicense.unit.impl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.api.unit.Quantities;
import science.unlicense.unit.api.UnitBuilder;
import science.unlicense.math.api.unit.DefinedDimension;
import science.unlicense.math.api.unit.DimensionBuilder;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public class CgsEmu extends Cgs {

    public static class DIMENSION extends Cgs.DIMENSION {

        public static final DefinedDimension COURANT_ELECTRIQUE = new DimensionBuilder(Chars.constant("courant électrique"),
                Quantities.ELECTRIC_CURRENT)
                .addFactor(MASSE, 1, 2)
                .addFactor(LONGUEUR, 1, 2)
                .addFactor(TEMPS, -1).build();

        public static final DefinedDimension CHARGE_ELECTRIQUE = new DimensionBuilder(Chars.constant("charge électrique"),
                Quantities.ELECTRIC_CHARGE)
                .addFactor(COURANT_ELECTRIQUE)
                .addFactor(TEMPS).build();

        public static final DefinedDimension INDUCTION_MAGNETIQUE = new DimensionBuilder(Chars.constant("induction magnétique"),
                Quantities.MAGNETIC_FLUX_DENSITY)
                .addFactor(MASSE)
                .addFactor(TEMPS, -2)
                .addFactor(COURANT_ELECTRIQUE, -1).build();

        public static final DefinedDimension FLUX_D_INDUCTION_MAGNETIQUE
                = new DimensionBuilder(Chars.constant("flux d'induction magnétique"), Quantities.MAGNETIC_FLUX)
                .addFactor(INDUCTION_MAGNETIQUE)
                .addFactor(LONGUEUR, 2).build();

        public static final DefinedDimension EXCITATION_MAGNETIQUE = new DimensionBuilder(Chars.constant("excitation magnétique"),
                // alt : "champ magnétique"
                // symbole : H
                Quantities.MAGNETIC_EXCITATION)
                .addFactor(FORCE)
                .addFactor(FLUX_D_INDUCTION_MAGNETIQUE, -1).build();

        public static final DefinedDimension SUSCEPTIBILITE_MAGNETIQUE = new DimensionBuilder(
                Chars.constant("susceptibilité magnétique"), Quantities.MAGNETIC_SUSCEPTIBILITY)
                .addFactor(ADIMENSION).build();

        DIMENSION() {}
    }

    /**
     * <div class="fr">Quatrième unité complémentaire dans le système CGS-UEM.</div>
     */
    public static final Unit ABAMPERE = new UnitBuilder()
            .addName(Chars.constant("abampere"))
            .addName(Chars.constant("biot"))
            .addSymbol(Chars.constant("abA"))
            .addSymbol(Chars.constant("Bi"))
            .addFactor(CENTIMETRE, 1, 2)
            .addFactor(GRAMME, 1, 2)
            .addFactor(SECONDE, -1)
            .buildDerived(DIMENSION.COURANT_ELECTRIQUE);

    public static final Unit ABCOULOMB = new UnitBuilder()
            .addName(Chars.constant("abcoulomb"))
            .addSymbol(Chars.constant("abC"))
            .addFactor(ABAMPERE)
            .addFactor(SECONDE)
            .buildDerived(DIMENSION.CHARGE_ELECTRIQUE);

    public static final Unit GAUSS = new UnitBuilder()
            .addName(Chars.constant("gauss"))
            .addSymbol(Chars.constant("G"))
            .addFactor(GRAMME)
            .addFactor(SECONDE, -2)
            .addFactor(ABAMPERE, -1)
            .buildDerived(DIMENSION.INDUCTION_MAGNETIQUE);

    public static final Unit MAXWELL = new UnitBuilder()
            .addName(Chars.constant("maxwell"))
            .addSymbol(Chars.constant("Mx"))
            .addFactor(GAUSS)
            .addFactor(CENTIMETRE, 2)
            .buildDerived(DIMENSION.FLUX_D_INDUCTION_MAGNETIQUE);

    public static final Unit ABAMPERE_PER_CENTIMETRE = new UnitBuilder()
            .addName(Chars.constant("maxwell"))
            .addFactor(ABAMPERE)
            .addFactor(CENTIMETRE, -1)
            .buildDerived(DIMENSION.FLUX_D_INDUCTION_MAGNETIQUE);

    /*
http://www.geo.cornell.edu/geology/classes/geol388/pdf_files/magunits.pdf
https://books.google.fr/books?id=Li8gAwAAQBAJ&pg=PA219&lpg=PA219&dq=oersted+4%CF%80&source=bl&ots=kGYQLcj9vK&sig=nlUWY_u
    JIxTwf8ObcxeCpN_mX_U&hl=fr&sa=X&ved=2ahUKEwjx0cT1l67eAhWJDSwKHVyaAtUQ6AEwB3oECAEQAQ#v=onepage&q=oersted%204%CF%80&f=
    false
https://books.google.fr/books?id=anRjDwAAQBAJ&pg=PA12&lpg=PA12&dq=oersted+4%CF%80&source=bl&ots=fx1g0wjZvZ&sig=FAWTYZWQ0
    jNoRWKNguEl62ZAPXg&hl=fr&sa=X&ved=2ahUKEwjx0cT1l67eAhWJDSwKHVyaAtUQ6AEwBnoECAYQAQ#v=onepage&q=oersted%204%CF%80&f=fa
    lse
https://www-d0.fnal.gov/hardware/cal/lvps_info/engineering/mag_conv.htm
*/
    public static final Unit OERSTED = new UnitBuilder()
            .addName(Chars.constant("œrsted"))
            .addSymbol(Chars.constant("Œ"))
            .buildTransformed(ABAMPERE_PER_CENTIMETRE, new Affine1(1.0 / (4.0 * Math.PI), 0.0));

    private static final CgsEmu INSTANCE = new CgsEmu();

    public static CgsEmu getInstance(){
        return INSTANCE;
    }
}
