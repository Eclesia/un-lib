package science.unlicense.unit.impl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.Quantities;
import science.unlicense.unit.api.UnitBuilder;
import science.unlicense.math.api.unit.DefinedDimension;
import science.unlicense.math.api.unit.DimensionBuilder;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public class CgsEsu extends Cgs {

    public static class DIMENSION extends Cgs.DIMENSION {

        public static final DefinedDimension CHARGE_ELECTRIQUE_ESU = new DimensionBuilder(Chars.constant("charge électrique"),
                Quantities.ELECTRIC_CHARGE)
                .addFactor(MASSE, 1, 2)
                .addFactor(LONGUEUR, 3, 2)
                .addFactor(TEMPS, -1).build();

        public static final DefinedDimension COURANT_ELECTRIQUE_ESU = new DimensionBuilder(Chars.constant("courant électrique"),
                Quantities.ELECTRIC_CURRENT)
                .addFactor(CHARGE_ELECTRIQUE_ESU)
                .addFactor(TEMPS, -1).build();

        public static final DefinedDimension POTENTIEL_ELECTRIQUE_ESU
                = new DimensionBuilder(Chars.constant("différence de potentiel"), Quantities.ELECTRIC_POTIENTIAL)
                .addFactor(ENERGIE)
                .addFactor(CHARGE_ELECTRIQUE_ESU, -1).build();

        public static final DefinedDimension TENSION_ELECTRIQUE_ESU = new DimensionBuilder(Chars.constant("tension électrique"),
                Quantities.VOLTAGE)
                .addFactor(ENERGIE)
                .addFactor(CHARGE_ELECTRIQUE_ESU, -1).build();

        public static final DefinedDimension COURANT_ELECTRIQUE_EMU = new DimensionBuilder(Chars.constant("courant électrique"),
                Quantities.ELECTRIC_CURRENT)
                .addFactor(MASSE, 1, 2)
                .addFactor(LONGUEUR, 1, 2)
                .addFactor(TEMPS, -1).build();

        public static final DefinedDimension CHARGE_ELECTRIQUE_EMU = new DimensionBuilder(Chars.constant("charge électrique"),
                Quantities.ELECTRIC_CHARGE)
                .addFactor(COURANT_ELECTRIQUE_EMU)
                .addFactor(TEMPS).build();

    //    public static final DefinedDimension FLUX_D_INDUCTION_MAGNETIQUE
    //            = new DimensionBuilder("flux d'induction magnétique", DefaultQuantity.MAGNETIC_FLUX)
    //            .factor(TENSION_ELECTRIQUE_ESU)
    //            .factor(TEMPS).build();

        DIMENSION() {}
    }

    /**
     * <div class="fr">Quatrième unité complémentaire dans le système CGS-UES.</div>
     */
    public static final Unit STATCOULOMB = new UnitBuilder()
            .addName(Chars.constant("statcoulomb"))
            .addName(Chars.constant("franklin"))
            .addSymbol(Chars.constant("statC"))
            .addSymbol(Chars.constant("Fr"))
            .addFactor(CENTIMETRE, 3, 2)
            .addFactor(GRAMME, 1, 2)
            .addFactor(SECONDE, -1)
            .buildDerived(DIMENSION.CHARGE_ELECTRIQUE_ESU);

    public static final Unit STATAMPERE = new UnitBuilder()
            .addName(Chars.constant("statampere"))
            .addSymbol(Chars.constant("statA"))
            .addFactor(STATCOULOMB)
            .addFactor(SECONDE, -1)
            .buildDerived(DIMENSION.COURANT_ELECTRIQUE_ESU);

    public static final Unit STATVOLT = new UnitBuilder()
            .addName(Chars.constant("statvolt"))
            .addSymbol(Chars.constant("statV"))
            .addFactor(ERG)
            .addFactor(STATCOULOMB, -1)
            .buildDerived(DIMENSION.POTENTIEL_ELECTRIQUE_ESU);

    private CgsEsu() {}

    private static final CgsEsu INSTANCE = new CgsEsu();

    public static CgsEsu getInstance(){
        return INSTANCE;
    }
}
