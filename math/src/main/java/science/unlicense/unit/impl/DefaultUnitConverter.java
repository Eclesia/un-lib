
package science.unlicense.unit.impl;

import science.unlicense.math.api.transform.Transform1;
import science.unlicense.unit.api.UnitConverter;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Johann Sorel
 */
public class DefaultUnitConverter implements UnitConverter {

    private final Unit source;
    private final Unit target;
    private final Transform1 transform;

    public DefaultUnitConverter(Unit source, Unit target, Transform1 transform) {
        this.source = source;
        this.target = target;
        this.transform = transform;
    }

    @Override
    public Unit getSource() {
        return source;
    }

    @Override
    public Unit getTarget() {
        return target;
    }

    @Override
    public Transform1 getTransform() {
        return transform;
    }

    @Override
    public double convert(double value) {
        return transform.transform(value);
    }

}
