package science.unlicense.unit.impl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.Affine1;
import science.unlicense.unit.api.Measure;
import science.unlicense.math.api.unit.Prefix;
import science.unlicense.math.api.unit.Quantities;
import science.unlicense.unit.api.UnitBuilder;
import science.unlicense.math.api.unit.BaseDimension;
import science.unlicense.math.api.unit.Dimension;
import science.unlicense.math.api.unit.DimensionBuilder;
import science.unlicense.unit.api.unit.FundamentalUnit;
import science.unlicense.unit.api.unit.LevelUnit;
import science.unlicense.math.api.unit.Unit;
import science.unlicense.unit.impl.level.DefaultRatioLog;
import science.unlicense.unit.impl.unit.PrebaseTransformedUnit;

/**
 *
 * @author Samuel Andrés
 */
public final class SI {

    public static final class DIMENSION {

        public static final BaseDimension ADIMENSION = new BaseDimension(Chars.constant("adimension"), null, Chars.constant("1"));

        /*================================================================================================================*/
        // Tableau 1 - Dimensions de base
        /*================================================================================================================*/

        public static final BaseDimension MASSE = new BaseDimension(Chars.constant("masse"), Quantities.MASS, Chars.constant("M"));

        public static final BaseDimension TEMPS = new BaseDimension(Chars.constant("temps"), Quantities.TIME, Chars.constant("T"));

        public static final BaseDimension LONGUEUR = new BaseDimension(Chars.constant("longueur"), Quantities.LENGTH, Chars.constant("L"));

        public static final BaseDimension TEMPERATURE_THERMODYNAMIQUE
                = new BaseDimension(Chars.constant("température thermodynamique"), Quantities.THERMODYNAMIC_TEMPERATURE, Chars.constant("\u0398"));

        public static final BaseDimension COURANT_ELECTRIQUE
                = new BaseDimension(Chars.constant("courant électrique"), Quantities.ELECTRIC_CURRENT, Chars.constant("I"));

        public static final BaseDimension QUANTITE_DE_MATIERE
                = new BaseDimension(Chars.constant("quantité de matière"), Quantities.AMOUNT_OF_SUBSTANCE, Chars.constant("N"));

        public static final BaseDimension INTENSITE_LUMINEUSE
                = new BaseDimension(Chars.constant("intensité lumineuse"), Quantities.LUMINOUS_INTENSITY, Chars.constant("J"));

        /*================================================================================================================*/
        // Tableau 2
        /*================================================================================================================*/

        public static final Dimension SUPERFICIE = new DimensionBuilder(Chars.constant("superficie"),
                Quantities.AREA)
                .addFactor(LONGUEUR, 2).build();

        public static final Dimension VOLUME = new DimensionBuilder(Chars.constant("volume"), Quantities.VOLUME)
                .addFactor(LONGUEUR, 3).build();

        public static final Dimension VITESSE = new DimensionBuilder(Chars.constant("vitesse"), Quantities.SPEED)
                .addFactor(LONGUEUR)
                .addFactor(TEMPS, -1).build();

        public static final Dimension ACCELERATION = new DimensionBuilder(Chars.constant("accélération"),
                Quantities.ACCELERATION)
                .addFactor(VITESSE)
                .addFactor(TEMPS, -1).build();

        public static final Dimension A_COUP = new DimensionBuilder(Chars.constant("à-coup"), Quantities.JERK)
                .addFactor(ACCELERATION)
                .addFactor(TEMPS, -1).build();

        public static final Dimension NOMBRE_D_ONDE = new DimensionBuilder(Chars.constant("nombre d'ondes"),
                Quantities.WAVENUMBER)
                .addFactor(LONGUEUR, -1).build();

        public static final Dimension MASSE_VOLUMIQUE = new DimensionBuilder(Chars.constant("masse volumique"),
                Quantities.DENSITY)
                .addFactor(MASSE)
                .addFactor(VOLUME, -1).build();

        public static final Dimension VOLUME_MASSIQUE = new DimensionBuilder(Chars.constant("masse volumique"),
                Quantities.SPECIFIC_VOLUME)
                .addFactor(VOLUME)
                .addFactor(MASSE, -1).build();

        public static final Dimension EXCITATION_MAGNETIQUE = new DimensionBuilder(Chars.constant("excitation magnétique"),
                // alt : "champ magnétique"
                // symbole : H
                Quantities.MAGNETIC_EXCITATION)
                .addFactor(COURANT_ELECTRIQUE)
                .addFactor(LONGUEUR, -1).build();

        public static final Dimension DENSITE_DE_COURANT = new DimensionBuilder(Chars.constant("densité de courant"),
                Quantities.CURRENT_DENSITY)
                .addFactor(COURANT_ELECTRIQUE)
                .addFactor(SUPERFICIE, -1).build();

        public static final Dimension CONCENTRATION_MOLAIRE = new DimensionBuilder(Chars.constant("concentration molaire"),
                Quantities.MOLAR_CONCENTRATION)
                .addFactor(QUANTITE_DE_MATIERE)
                .addFactor(VOLUME, -1).build();

        public static final Dimension LUMINANCE_LUMINEUSE = new DimensionBuilder(Chars.constant("luminance lumineuse"),
                Quantities.LUMINANCE)
                .addFactor(INTENSITE_LUMINEUSE)
                .addFactor(SUPERFICIE, -1).build();

        /*================================================================================================================*/
        // Tableau 5
        /*================================================================================================================*/

        public static final Dimension ANGLE_PLAN = new DimensionBuilder(Chars.constant("angle plan"),
                Quantities.ANGLE)
                .addFactor(LONGUEUR)
                .addFactor(LONGUEUR, -1).build();

        public static final Dimension ANGLE_SOLIDE = new DimensionBuilder(Chars.constant("angle solide"),
                Quantities.SOLID_ANGLE)
                .addFactor(SUPERFICIE)
                .addFactor(SUPERFICIE, -1).build();

        /*================================================================================================================*/
        // Tableau 3
        /*================================================================================================================*/

        public static final Dimension ACTIVITE_CATALITIQUE = new DimensionBuilder(Chars.constant("activité catalitique"),
                Quantities.CATALYTIC_ACTIVITY)
                .addFactor(QUANTITE_DE_MATIERE)
                .addFactor(TEMPS, -1).build();

        public static final Dimension ACTIVITE = new DimensionBuilder(Chars.constant("activité"),
                Quantities.RADIOACTIVE_DECAY)
                .addFactor(TEMPS, -1).build();

        public static final Dimension CHARGE_ELECTRIQUE = new DimensionBuilder(Chars.constant("charge électrique"),
                Quantities.ELECTRIC_CHARGE)
                .addFactor(COURANT_ELECTRIQUE)
                .addFactor(TEMPS).build();

        public static final Dimension QUANTITE_D_ELECTRICITE = new DimensionBuilder(Chars.constant("quantité d'électricité"),
                Quantities.AMOUNT_OF_ELECTRICITY)
                .addFactor(COURANT_ELECTRIQUE)
                .addFactor(TEMPS).build();

        public static final Dimension CAPACITE_ELECTRIQUE = new DimensionBuilder(Chars.constant("capacité électrique"),
                Quantities.CAPACITANCE)
                .addFactor(CHARGE_ELECTRIQUE)
                .addFactor(TEMPS).build();

        public static final Dimension POTENTIEL_ELECTRIQUE
                = new DimensionBuilder(Chars.constant("différence de potentiel"), Quantities.ELECTRIC_POTIENTIAL)
                .addFactor(MASSE)
                .addFactor(LONGUEUR, 2)
                .addFactor(TEMPS, -3)
                .addFactor(COURANT_ELECTRIQUE, -1).build();

        public static final Dimension TENSION_ELECTRIQUE = new DimensionBuilder(Chars.constant("tension électrique"),
                Quantities.VOLTAGE)
                .addFactor(MASSE)
                .addFactor(LONGUEUR, 2)
                .addFactor(TEMPS, -3)
                .addFactor(COURANT_ELECTRIQUE, -1).build();

        public static final Dimension FORCE_ELECTROMOTRICE = new DimensionBuilder(Chars.constant("force électromotrice"),
                Quantities.ELECTROMOTIVE_FORCE)
                .addFactor(MASSE)
                .addFactor(LONGUEUR, 2)
                .addFactor(TEMPS, -3)
                .addFactor(COURANT_ELECTRIQUE, -1).build();

        public static final Dimension CONDUCTANCE_ELECTRIQUE = new DimensionBuilder(Chars.constant("conductance électrique"),
                Quantities.ELECTRIC_CONDUCTANCE)
                .addFactor(COURANT_ELECTRIQUE)
                .addFactor(TENSION_ELECTRIQUE, -1).build();

        public static final Dimension FORCE = new DimensionBuilder(Chars.constant("force"), Quantities.FORCE)
                .addFactor(LONGUEUR)
                .addFactor(MASSE)
                .addFactor(TEMPS, -2).build();

        public static final Dimension ENERGIE = new DimensionBuilder(Chars.constant("énergie"), Quantities.ENERGY)
                .addFactor(LONGUEUR)
                .addFactor(FORCE).build();

        public static final Dimension DOSE_ABSORBEE = new DimensionBuilder(Chars.constant("dose absorbée"),
                Quantities.ABSORBED_DOSE)
                .addFactor(ENERGIE)
                .addFactor(MASSE).build();

        public static final Dimension DOSE_EQUIVALENTE = new DimensionBuilder(Chars.constant("dose équivalente"),
                Quantities.EQUIVALENT_DOSE)
                .addFactor(ENERGIE)
                .addFactor(MASSE).build();

        public static final Dimension FLUX_LUMINEUX = new DimensionBuilder(Chars.constant("flux lumineux"),
                Quantities.LUMINOUS_FLUX)
                .addFactor(INTENSITE_LUMINEUSE)
                .addFactor(ANGLE_SOLIDE).build();

        public static final Dimension ECLAIREMENT_LUMINEUX = new DimensionBuilder(Chars.constant("éclairement lumineux"),
                Quantities.ILLUMINANCE)
                .addFactor(FLUX_LUMINEUX)
                .addFactor(SUPERFICIE, -1).build();

        public static final Dimension FLUX_D_INDUCTION_MAGNETIQUE
                = new DimensionBuilder(Chars.constant("flux d'induction magnétique"), Quantities.MAGNETIC_FLUX)
                .addFactor(TENSION_ELECTRIQUE)
                .addFactor(TEMPS).build();

        public static final Dimension FREQUENCE = new DimensionBuilder(Chars.constant("fréquence"),
                Quantities.FREQUENCY)
                .addFactor(TEMPS, -1).build();

        public static final Dimension INDUCTANCE = new DimensionBuilder(Chars.constant("inductance"),
                Quantities.INDUCTANCE)
                .addFactor(FLUX_D_INDUCTION_MAGNETIQUE)
                .addFactor(COURANT_ELECTRIQUE, -1).build();

        public static final Dimension INDUCTION_MAGNETIQUE = new DimensionBuilder(Chars.constant("induction magnétique"),
                Quantities.MAGNETIC_FLUX_DENSITY)
                .addFactor(FLUX_D_INDUCTION_MAGNETIQUE)
                .addFactor(SUPERFICIE, -1).build();

        public static final Dimension PRESSION = new DimensionBuilder(Chars.constant("pression"), Quantities.PRESSURE)
                .addFactor(FORCE)
                .addFactor(SUPERFICIE, -1).build();

        public static final Dimension PUISSANCE = new DimensionBuilder(Chars.constant("puissance"), Quantities.POWER)
                .addFactor(ENERGIE)
                .addFactor(TEMPS, -1).build();

        public static final Dimension RESISTANCE_ELECTRIQUE = new DimensionBuilder(Chars.constant("résistance électrique"),
                Quantities.ELECTRIC_RESISTANCE)
                .addFactor(TENSION_ELECTRIQUE)
                .addFactor(COURANT_ELECTRIQUE, -1).build();

        /*================================================================================================================*/
        // tableau 4


        public static final Dimension CHAMP_ELECTRIQUE
                = new DimensionBuilder(Chars.constant("champ électrique"), Quantities.ELECTRIC_FIELD)
                .addFactor(POTENTIEL_ELECTRIQUE)
                .addFactor(LONGUEUR, -1).build();

        public static final Dimension CHARGE_ELECTRIQUE_VOLUMIQUE
                = new DimensionBuilder(Chars.constant("charge électrique volumique"), Quantities.CHARGE_DENSITY)
                .addFactor(CHARGE_ELECTRIQUE)
                .addFactor(VOLUME, -1).build();

        public static final Dimension CONDUCTIVITE_THERMIQUE
                = new DimensionBuilder(Chars.constant("conductivité thermique"), Quantities.THERMAL_CONDUCTIVITY)
                .addFactor(PUISSANCE)
                .addFactor(LONGUEUR, -1)
                .addFactor(TEMPERATURE_THERMODYNAMIQUE, -1).build();

        public static final Dimension DEBIT_DE_DOSE_ABSORBEE
                = new DimensionBuilder(Chars.constant("débit de dose absorbée"), Quantities.ABSORBED_DOSE_RATE)
                .addFactor(DOSE_ABSORBEE)
                .addFactor(TEMPS, -1).build();

        public static final Dimension DEBIT_DE_DOSE_EQUIVALENTE
                = new DimensionBuilder(Chars.constant("débit de dose équivalente"), Quantities.EQUIVALENT_DOSE_RATE)
                .addFactor(DOSE_EQUIVALENTE)
                .addFactor(TEMPS, -1).build();

        public static final Dimension DENSITE_DE_FLUX_THERMIQUE
                = new DimensionBuilder(Chars.constant("dentité de flux thermique"), Quantities.THERMAL_FLUX)
                .addFactor(PUISSANCE)
                .addFactor(SUPERFICIE, -1).build();

        public static final Dimension ECLAIREMENT_ENERGETIQUE
                = new DimensionBuilder(Chars.constant("éclairement énergétique"), Quantities.IRRADIANCE)
                .addFactor(PUISSANCE)
                .addFactor(SUPERFICIE, -1).build();

        public static final Dimension ENERGIE_MASSIQUE
                = new DimensionBuilder(Chars.constant("énergie massique"), Quantities.SPECIFIC_ENERGY)
                .addFactor(ENERGIE)
                .addFactor(MASSE, -1).build();

        public static final Dimension ENERGIE_MOLAIRE
                = new DimensionBuilder(Chars.constant("énergie molaire"), Quantities.MOLAR_ENERGY)
                .addFactor(ENERGIE)
                .addFactor(QUANTITE_DE_MATIERE, -1).build();

        public static final Dimension ENERGIE_VOLUMIQUE
                = new DimensionBuilder(Chars.constant("énergie volumique"), Quantities.ENERGY_DENSITY)
                .addFactor(ENERGIE)
                .addFactor(VOLUME, -1).build();

        public static final Dimension ENTROPIE
                = new DimensionBuilder(Chars.constant("entropie"), Quantities.ENTROPY)
                .addFactor(ENERGIE)
                .addFactor(TEMPERATURE_THERMODYNAMIQUE, -1).build();

        public static final Dimension CAPACITE_THERMIQUE
                = new DimensionBuilder(Chars.constant("capacité thermique"), Quantities.THERMAL_CAPACITY)
                .addFactor(ENERGIE)
                .addFactor(TEMPERATURE_THERMODYNAMIQUE, -1).build();

        public static final Dimension ENTROPIE_MASSIQUE
                = new DimensionBuilder(Chars.constant("entropie massique"), Quantities.SPECIFIC_ENTROPY)
                .addFactor(ENTROPIE)
                .addFactor(MASSE, -1).build();

        public static final Dimension CAPACITE_THERMIQUE_MASSIQUE
                = new DimensionBuilder(Chars.constant("capacité thermique massique"), Quantities.SPECIFIC_THERMAL_CAPACITY)
                .addFactor(CAPACITE_THERMIQUE)
                .addFactor(MASSE, -1).build();

        public static final Dimension ENTROPIE_MOLAIRE
                = new DimensionBuilder(Chars.constant("entropie molaire"), Quantities.MOLAR_ENTROPY)
                .addFactor(ENTROPIE)
                .addFactor(QUANTITE_DE_MATIERE, -1).build();

        public static final Dimension CAPACITE_THERMIQUE_MOLAIRE
                = new DimensionBuilder(Chars.constant("capacité thermique molaire"), Quantities.MOLAR_THERMAL_CAPACITY)
                .addFactor(CAPACITE_THERMIQUE)
                .addFactor(QUANTITE_DE_MATIERE, -1).build();

        public static final Dimension EXPOSITION_RAYONNEMENT_IONISANT
                = new DimensionBuilder(Chars.constant("exposition au rayonnement ionisant"), Quantities.RADIATION_EXPOSURE)
                .addFactor(CHARGE_ELECTRIQUE)
                .addFactor(MASSE, -1).build();

        public static final Dimension INDUCTION_ELECTRIQUE
                = new DimensionBuilder(Chars.constant("induction électrique"), Quantities.ELECTRIC_DISPLACEMENT_FIELD)
                .addFactor(CHARGE_ELECTRIQUE)
                .addFactor(SUPERFICIE, -1).build();

        public static final Dimension LUMINANCE_ENERGETIQUE
                = new DimensionBuilder(Chars.constant("luminance énergétique"), Quantities.RADIANCE)
                .addFactor(PUISSANCE)
                .addFactor(ANGLE_SOLIDE, -1)
                .addFactor(SUPERFICIE, -1).build();

        public static final Dimension MOMENT_D_UNE_FORCE
                = new DimensionBuilder(Chars.constant("moment d'une force"), Quantities.TORQUE)
                .addFactor(FORCE)
                .addFactor(LONGUEUR).build();

        public static final Dimension PERMEABILITE_MAGNETIQUE
                = new DimensionBuilder(Chars.constant("perméabilité magnétique"), Quantities.MAGNETIC_PERMEABILITY)
                .addFactor(INDUCTANCE)
                .addFactor(LONGUEUR, -1).build();

        public static final Dimension PERMITTIVITE_ELECTRIQUE
                = new DimensionBuilder(Chars.constant("permittivité électrique"), Quantities.ELECTRIC_PERMITTIVITY)
                .addFactor(CAPACITE_ELECTRIQUE)
                .addFactor(LONGUEUR, -1).build();

        public static final Dimension TENSION_SUPERFICIELLE
                = new DimensionBuilder(Chars.constant("tension superficielle"), Quantities.SURFACE_TENSION)
                .addFactor(FORCE)
                .addFactor(LONGUEUR, -1).build();

        public static final Dimension VISCOSITE_DYNAMIQUE
                = new DimensionBuilder(Chars.constant("viscosité dynamique"), Quantities.VISCOSITY)
                .addFactor(PRESSION)
                .addFactor(TEMPS).build();

        public static final Dimension SUSCEPTIBILITE_MAGNETIQUE = new DimensionBuilder(
                Chars.constant("susceptibilité magnétique"), Quantities.MAGNETIC_SUSCEPTIBILITY)
                .addFactor(ADIMENSION).build();

        public static final Dimension PRESSION_ACOUSTIQUE = new DimensionBuilder(
                Chars.constant("pression accoustique"), null)
                .addFactor(MASSE)
                .addFactor(LONGUEUR, -1)
                .addFactor(TEMPS, -2).build();

        public static final Dimension NIVEAU_DE_PRESSION_ACOUSTIQUE = new DimensionBuilder(
                Chars.constant("niveau de pression accoustique"), null)
                .addFactor(ADIMENSION).build();

        public static final Dimension INTENSITE_ACOUSTIQUE = new DimensionBuilder(
                Chars.constant("intensité accoustique"), null)
                .addFactor(MASSE)
                .addFactor(TEMPS, -3).build();

        public static final Dimension NIVEAU_D_INTENSITE_ACOUSTIQUE = new DimensionBuilder(
                Chars.constant("niveau d'intensité accoustique"), null)
                .addFactor(ADIMENSION).build();

        private DIMENSION(){}
    }

    public static final class PREFIX {

        public static final Prefix YOTTA = new Prefix(1e24, Chars.constant("Y"));
        public static final Prefix ZETTA = new Prefix(1e21, Chars.constant("Z"));
        public static final Prefix EXA = new Prefix(1e18, Chars.constant("E"));
        public static final Prefix PETA = new Prefix(1e15, Chars.constant("P"));
        public static final Prefix TERA = new Prefix(1e12, Chars.constant("T"));
        public static final Prefix GIGA = new Prefix(1e9, Chars.constant("G"));
        public static final Prefix MEGA = new Prefix(1e6, Chars.constant("M"));

        public static final Prefix KILO = new Prefix(1e3, Chars.constant("k"));
        public static final Prefix HECTO = new Prefix(1e2, Chars.constant("h"));
        public static final Prefix DECA = new Prefix(1e1, Chars.constant("da"));
        public static final Prefix DECI = new Prefix(1e-1, Chars.constant("d"));
        public static final Prefix CENTI = new Prefix(1e-2, Chars.constant("c"));
        public static final Prefix MILLI = new Prefix(1e-3, Chars.constant("m"));

        public static final Prefix MICRO = new Prefix(1e-6, Chars.constant("µ"));
        public static final Prefix NANO = new Prefix(1e-9, Chars.constant("n"));
        public static final Prefix PICO = new Prefix(1e-12, Chars.constant("p"));
        public static final Prefix FEMTO = new Prefix(1e-15, Chars.constant("f"));
        public static final Prefix ATTO = new Prefix(1e-18, Chars.constant("a"));
        public static final Prefix ZEPTO = new Prefix(1e-21, Chars.constant("z"));
        public static final Prefix YOCTO = new Prefix(1e-24, Chars.constant("y"));

        private PREFIX(){}
    }

    public static final Affine1 SEXA = new Affine1(60.0, 0.0);

    public static final FundamentalUnit UN = new UnitBuilder()
            .addName(Chars.constant("un"))
            .buildFundamental(DIMENSION.ADIMENSION);

    /**
     * <div class="fr">
     * Le gramme n'est pas une unité de base ni une unité dérivée d'une unité de base.
     * C'est une unité transformée à partir d'une unité de base (le kilogramme), mais qui n'est pas préfixée.
     *
     * Le kilogramme, unité de base des masses, a néanmoins également besoin du gramme car il est une unité préfixée à
     * partir du gramme.
     *
     * Ceci fait que le gramme et le kilogramme sont interdépendants, le premier ayant besoin de son unité de base de
     * définition et le second de l'unité à partir de laquelle il est préfixé.
     * </div>
     */
    public static final Unit GRAMME = new PrebaseTransformedUnit(
            Chars.constant("gramme"),
            Chars.constant("g"),
            new Affine1(1.0 / 1000.0, 0.0)) {
        @Override
        public BaseDimension[] dimension() {
            return new BaseDimension[]{DIMENSION.MASSE};
        }

        @Override
        public Unit referenceUnit() {
            return KILOGRAMME;
        }
    };

    // unités de base
    public static final Unit KILOGRAMME = new UnitBuilder()
            .buildPrefixed(SI.PREFIX.KILO, GRAMME, true);

    public static final Unit METRE = new UnitBuilder()
            .addName(Chars.constant("mètre"))
            .addSymbol(Chars.constant("m"))
            .buildFundamental(DIMENSION.LONGUEUR);

    public static final Unit SECONDE = new UnitBuilder()
            .addName(Chars.constant("seconde"))
            .addSymbol('s')
            .buildFundamental(DIMENSION.TEMPS);

    public static final Unit AMPERE = new UnitBuilder()
            .addName(Chars.constant("ampère"))
            .addSymbol('A')
            .buildFundamental(DIMENSION.COURANT_ELECTRIQUE);

    public static final Unit KELVIN = new UnitBuilder()
            .addName(Chars.constant("kelvin"))
            .addSymbol('K')
            .buildFundamental(DIMENSION.TEMPERATURE_THERMODYNAMIQUE);

    public static final Unit MOLE = new UnitBuilder()
            .addName(Chars.constant("mole"))
            .addSymbol(Chars.constant("mol"))
            .buildFundamental(DIMENSION.QUANTITE_DE_MATIERE);

    public static final Unit CANDELA = new UnitBuilder()
            .addName(Chars.constant("candela"))
            .addSymbol(Chars.constant("cd"))
            .buildFundamental(DIMENSION.INTENSITE_LUMINEUSE);

    public static final Unit METRE_CARRE = new UnitBuilder()
            .addName(Chars.constant("mètre carré"))
            .addFactor(METRE, 2)
            .buildDerived(DIMENSION.SUPERFICIE);

    public static final Unit METRE_CUBE = new UnitBuilder()
            .addName(Chars.constant("mètre cube"))
            .addFactor(METRE, 3)
            .buildDerived(DIMENSION.VOLUME);

    public static final Unit METRE_PAR_SECONDE = new UnitBuilder()
            .addName(Chars.constant("mètre par seconde"))
            .addFactor(METRE)
            .addFactor(SECONDE, -1)
            .buildDerived(DIMENSION.VITESSE);

    public static final Unit METRE_PAR_SECONDE_CARREE = new UnitBuilder()
            .addName(Chars.constant("mètre par seconde carrée"))
            .addFactor(METRE)
            .addFactor(SECONDE, -2)
            .buildDerived(DIMENSION.ACCELERATION);

    public static final Unit METRE_PAR_SECONDE_CUBE = new UnitBuilder()
            .addName(Chars.constant("mètre par seconde cube"))
            .addFactor(METRE)
            .addFactor(SECONDE, -3)
            .buildDerived(DIMENSION.A_COUP);

    public static final Unit UN_PAR_METRE = new UnitBuilder()
            .addName(Chars.constant("un par mètre"))
            .addFactor(METRE, -1)
            .buildDerived(DIMENSION.NOMBRE_D_ONDE);

    public static final Unit KILOGRAMME_PAR_METRE_CUBE = new UnitBuilder()
            .addName(Chars.constant("kilogramme par mètre cube"))
            .addFactor(KILOGRAMME)
            .addFactor(METRE, -3)
            .buildDerived(DIMENSION.MASSE_VOLUMIQUE);

    public static final Unit METRE_CUBE_PAR_KILOGRAMME = new UnitBuilder()
            .addName(Chars.constant("mètre cube par kilogramme"))
            .addFactor(METRE_CUBE)
            .addFactor(KILOGRAMME, -1)
            .buildDerived(DIMENSION.VOLUME_MASSIQUE);

    public static final Unit AMPERE_PAR_METRE = new UnitBuilder()
            .addName(Chars.constant("ampère par mètre"))
            .addFactor(AMPERE)
            .addFactor(METRE, -1)
            .buildDerived(DIMENSION.EXCITATION_MAGNETIQUE);

    public static final Unit AMPERE_PAR_METRE_CARRE = new UnitBuilder()
            .addName(Chars.constant("ampère par mètre carré"))
            .addFactor(AMPERE)
            .addFactor(METRE_CARRE, -1)
            .buildDerived(DIMENSION.DENSITE_DE_COURANT);

    public static final Unit MOLE_PAR_METRE_CUBE = new UnitBuilder()
            .addName(Chars.constant("mole par mètre cube"))
            .addFactor(MOLE)
            .addFactor(METRE_CUBE, -1)
            .buildDerived(DIMENSION.CONCENTRATION_MOLAIRE);

    public static final Unit CANDELA_PAR_METRE_CARRE = new UnitBuilder()
            .addName(Chars.constant("candela par mètre carré"))
            .addFactor(CANDELA)
            .addFactor(METRE_CARRE, -1)
            .buildDerived(DIMENSION.LUMINANCE_LUMINEUSE);

    // angles
    public static final Unit RADIAN = new UnitBuilder()
            .addName(Chars.constant("radian"))
            .addSymbol(Chars.constant("rad"))
            .addFactor(UN)
            .buildBaseDerived(DIMENSION.ANGLE_PLAN);

    public static final Unit STERADIAN = new UnitBuilder()
            .addName(Chars.constant("stéradian"))
            .addSymbol(Chars.constant("sr"))
            .addFactor(UN)
            .buildBaseDerived(DIMENSION.ANGLE_SOLIDE);

    // unités dérivées dotées de noms spéciaux
    public static final Unit KATAL = new UnitBuilder()
            .addName(Chars.constant("katal"))
            .addSymbol(Chars.constant("kat"))
            .addFactor(MOLE)
            .addFactor(SECONDE, -1)
            .buildDerived(DIMENSION.ACTIVITE_CATALITIQUE);

    public static final Unit BECQUEREL = new UnitBuilder()
            .addName(Chars.constant("becquerel"))
            .addSymbol(Chars.constant("Bq"))
            .addFactor(SECONDE, -1)
            .buildDerived(DIMENSION.ACTIVITE);

    public static final Unit VOLT = new UnitBuilder()
            .addName(Chars.constant("volt"))
            .addSymbol(Chars.constant("V"))
            .addFactor(KILOGRAMME)
            .addFactor(METRE, 2)
            .addFactor(SECONDE, -3)
            .addFactor(AMPERE, -1)
            .buildDerived(DIMENSION.TENSION_ELECTRIQUE,
                          DIMENSION.POTENTIEL_ELECTRIQUE,
                          DIMENSION.FORCE_ELECTROMOTRICE);

    public static final Unit COULOMB = new UnitBuilder()
            .addName(Chars.constant("coulomb"))
            .addSymbol(Chars.constant("C"))
            .addFactor(AMPERE)
            .addFactor(SECONDE)
            .buildDerived(DIMENSION.CHARGE_ELECTRIQUE,
                          DIMENSION.QUANTITE_D_ELECTRICITE);

    public static final Unit FARAD = new UnitBuilder()
            .addName(Chars.constant("farad"))
            .addSymbol(Chars.constant("F"))
            .addFactor(COULOMB)
            .addFactor(VOLT, -1)
            .buildDerived(DIMENSION.CAPACITE_ELECTRIQUE);

    public static final Unit SIEMENS = new UnitBuilder()
            .addName(Chars.constant("siemens"))
            .addSymbol(Chars.constant("S"))
            .addFactor(AMPERE)
            .addFactor(VOLT, -1)
            .buildDerived(DIMENSION.CONDUCTANCE_ELECTRIQUE);

    public static final Unit NEWTON = new UnitBuilder()
            .addName(Chars.constant("newton"))
            .addSymbol(Chars.constant("N"))
            .addFactor(METRE)
            .addFactor(KILOGRAMME)
            .addFactor(SECONDE, -2)
            .buildDerived(DIMENSION.FORCE);

    public static final Unit JOULE = new UnitBuilder()
            .addName(Chars.constant("joule"))
            .addSymbol(Chars.constant("J"))
            .addFactor(NEWTON)
            .addFactor(METRE)
            .buildDerived(DIMENSION.ENERGIE);

    public static final Unit GRAY = new UnitBuilder()
            .addName(Chars.constant("gray"))
            .addSymbol(Chars.constant("Gy"))
            .addFactor(JOULE)
            .addFactor(KILOGRAMME)
            .buildDerived(DIMENSION.DOSE_ABSORBEE);

    public static final Unit SIEVERT = new UnitBuilder()
            .addName(Chars.constant("gray"))
            .addSymbol(Chars.constant("Sv"))
            .addFactor(JOULE)
            .addFactor(KILOGRAMME)
            .buildDerived(DIMENSION.DOSE_EQUIVALENTE);

    public static final Unit LUMEN = new UnitBuilder()
            .addName(Chars.constant("lumen"))
            .addSymbol(Chars.constant("lm"))
            .addFactor(CANDELA)
            .addFactor(STERADIAN)
            .buildDerived(DIMENSION.FLUX_LUMINEUX);

    public static final Unit LUX = new UnitBuilder()
            .addName(Chars.constant("lux"))
            .addSymbol(Chars.constant("lux"))
            .addFactor(LUMEN)
            .addFactor(METRE_CARRE, -2)
            .buildDerived(DIMENSION.ECLAIREMENT_LUMINEUX);

    public static final Unit WEBER = new UnitBuilder()
            .addName(Chars.constant("weber"))
            .addSymbol(Chars.constant("Wb"))
            .addFactor(VOLT)
            .addFactor(SECONDE)
            .buildDerived(DIMENSION.FLUX_D_INDUCTION_MAGNETIQUE);

    public static final Unit HERTZ = new UnitBuilder()
            .addName(Chars.constant("hertz"))
            .addSymbol(Chars.constant("Hz"))
            .addFactor(SECONDE, -1)
            .buildDerived(DIMENSION.FREQUENCE);

    public static final Unit HENRY = new UnitBuilder()
            .addName(Chars.constant("henry"))
            .addSymbol(Chars.constant("H"))
            .addFactor(WEBER)
            .addFactor(AMPERE, -1)
            .buildDerived(DIMENSION.INDUCTANCE);

    public static final Unit TESLA = new UnitBuilder()
            .addName(Chars.constant("tesla"))
            .addSymbol(Chars.constant("T"))
            .addFactor(WEBER)
            .addFactor(METRE_CARRE, -1)
            .buildDerived(DIMENSION.INDUCTION_MAGNETIQUE);

    public static final Unit PASCAL = new UnitBuilder()
            .addName(Chars.constant("pascal"))
            .addSymbol(Chars.constant("Pa"))
            .addFactor(NEWTON)
            .addFactor(METRE_CARRE, -1)
            .buildDerived(DIMENSION.PRESSION);

    public static final Unit WATT = new UnitBuilder()
            .addName(Chars.constant("watt"))
            .addSymbol(Chars.constant("W"))
            .addFactor(JOULE)
            .addFactor(SECONDE, -1)
            .buildDerived(DIMENSION.PUISSANCE);

    public static final Unit OHM = new UnitBuilder()
            .addName(Chars.constant("ohm"))
            .addSymbol(Chars.constant("\u03A9"))
            .addFactor(VOLT)
            .addFactor(AMPERE, -1)
            .buildDerived(DIMENSION.RESISTANCE_ELECTRIQUE);

    public static final Unit VOLT_PAR_METRE = new UnitBuilder()
            .addName(Chars.constant("volt par mètre"))
            .addFactor(VOLT)
            .addFactor(METRE, -1)
            .buildDerived(DIMENSION.CHARGE_ELECTRIQUE);

    public static final Unit NEWTON_PAR_COULOMB = new UnitBuilder()
            .addName(Chars.constant("newton par coulomb"))
            .addFactor(NEWTON)
            .addFactor(COULOMB, -1)
            .buildDerived(DIMENSION.CHARGE_ELECTRIQUE);

    public static final Unit COULOMB_PAR_METRE_CUBE = new UnitBuilder()
            .addName(Chars.constant("coulomb par mètre cube"))
            .addFactor(COULOMB)
            .addFactor(METRE_CUBE, -1)
            .buildDerived(DIMENSION.CHARGE_ELECTRIQUE_VOLUMIQUE);

    public static final Unit WATT_PAR_METRE_KELVIN = new UnitBuilder()
            .addName(Chars.constant("watt par mètre-kelvin"))
            .addFactor(WATT)
            .addFactor(METRE, -1)
            .addFactor(KELVIN, -1)
            .buildDerived(DIMENSION.CONDUCTIVITE_THERMIQUE);

    public static final Unit GRAY_PAR_SECONDE = new UnitBuilder()
            .addName(Chars.constant("gray par seconde"))
            .addFactor(GRAY)
            .addFactor(SECONDE, -1)
            .buildDerived(DIMENSION.DEBIT_DE_DOSE_ABSORBEE);

    public static final Unit SIVERT_PAR_SECONDE = new UnitBuilder()
            .addName(Chars.constant("sievert par seconde"))
            .addFactor(SIEVERT)
            .addFactor(SECONDE, -1)
            .buildDerived(DIMENSION.DEBIT_DE_DOSE_EQUIVALENTE);

    public static final Unit WATT_PAR_METRE_CARRE = new UnitBuilder()
            .addName(Chars.constant("watt par mètre carré"))
            .addFactor(WATT)
            .addFactor(METRE_CARRE, -1)
            .buildDerived(DIMENSION.DENSITE_DE_FLUX_THERMIQUE,
                          DIMENSION.ECLAIREMENT_ENERGETIQUE,
                          DIMENSION.INTENSITE_ACOUSTIQUE);

    public static final Unit JOULE_PAR_KILOGRAMME = new UnitBuilder()
            .addName(Chars.constant("joule par kilogramme"))
            .addFactor(JOULE)
            .addFactor(KILOGRAMME, -1)
            .buildDerived(DIMENSION.ENERGIE_MASSIQUE);

    public static final Unit JOULE_PAR_MOLE = new UnitBuilder()
            .addName(Chars.constant("joule par mole"))
            .addFactor(JOULE)
            .addFactor(MOLE, -1)
            .buildDerived(DIMENSION.ENERGIE_MOLAIRE);

    public static final Unit JOULE_PAR_METRE_CUBE = new UnitBuilder()
            .addName(Chars.constant("joule par mètre cube"))
            .addFactor(JOULE)
            .addFactor(METRE_CUBE, -1)
            .buildDerived(DIMENSION.ENERGIE_VOLUMIQUE);

    public static final Unit JOULE_PAR_KELVIN = new UnitBuilder()
            .addName(Chars.constant("joule par kelvin"))
            .addFactor(JOULE)
            .addFactor(KELVIN, -1)
            .buildDerived(DIMENSION.ENTROPIE, DIMENSION.CAPACITE_THERMIQUE);

    public static final Unit JOULE_PAR_KILOGRAMME_KELVIN = new UnitBuilder()
            .addName(Chars.constant("joule par kilogramme-kelvin"))
            .addFactor(JOULE)
            .addFactor(KILOGRAMME, -1)
            .addFactor(KELVIN, -1)
            .buildDerived(DIMENSION.ENTROPIE_MASSIQUE, DIMENSION.CAPACITE_THERMIQUE_MASSIQUE);

    public static final Unit JOULE_PAR_MOLE_KELVIN = new UnitBuilder()
            .addName(Chars.constant("joule par mole-kelvin"))
            .addFactor(JOULE)
            .addFactor(MOLE, -1)
            .addFactor(KELVIN, -1)
            .buildDerived(DIMENSION.ENTROPIE_MOLAIRE,
                          DIMENSION.CAPACITE_THERMIQUE_MOLAIRE);

    public static final Unit COULOMB_PAR_KILOGRAMME = new UnitBuilder()
            .addName(Chars.constant("coulomb par kilogramme"))
            .addFactor(COULOMB)
            .addFactor(KILOGRAMME, -1)
            .buildDerived(DIMENSION.EXPOSITION_RAYONNEMENT_IONISANT);

    public static final Unit COULOMB_PAR_METRE_CARRE = new UnitBuilder()
            .addName(Chars.constant("coulomb par mètre carré"))
            .addFactor(COULOMB)
            .addFactor(METRE_CARRE, -1)
            .buildDerived(DIMENSION.INDUCTION_ELECTRIQUE);

    public static final Unit WATT_PAR_STERADIAN_METRE_CARRE = new UnitBuilder()
            .addName(Chars.constant("watt par stéradian mètre carré"))
            .addFactor(WATT)
            .addFactor(STERADIAN, -1)
            .addFactor(METRE_CARRE, -1)
            .buildDerived(DIMENSION.LUMINANCE_ENERGETIQUE);

    public static final Unit NEWTON_METRE = new UnitBuilder()
            .addName(Chars.constant("newton-mètre"))
            .addFactor(NEWTON)
            .addFactor(METRE)
            .buildDerived(DIMENSION.MOMENT_D_UNE_FORCE);

    public static final Unit HENRY_PAR_METRE = new UnitBuilder()
            .addName(Chars.constant("henry par mètre"))
            .addFactor(HENRY)
            .addFactor(METRE, -1)
            .buildDerived(DIMENSION.PERMEABILITE_MAGNETIQUE);

    public static final Unit FARAD_PAR_METRE = new UnitBuilder()
            .addName(Chars.constant("farad par mètre"))
            .addFactor(FARAD)
            .addFactor(METRE, -1)
            .buildDerived(DIMENSION.PERMITTIVITE_ELECTRIQUE);

    public static final Unit NEWTON_PAR_METRE = new UnitBuilder()
            .addName(Chars.constant("newton par mètre"))
            .addFactor(NEWTON)
            .addFactor(METRE, -1)
            .buildDerived(DIMENSION.TENSION_SUPERFICIELLE);

    public static final Unit PASCAL_SECONDE = new UnitBuilder()
            .addName(Chars.constant("pascal-seconde"))
            .addFactor(PASCAL)
            .addFactor(SECONDE)
            .buildDerived(DIMENSION.VISCOSITE_DYNAMIQUE);


    // unités transformées

    public static final Unit MINUTE = new UnitBuilder()
            .addName(Chars.constant("minute"))
            .addSymbol(Chars.constant("min"))
            .buildTransformed(SECONDE, SEXA);
    public static final Unit HEURE = new UnitBuilder()
            .addName(Chars.constant("heure"))
            .addSymbol('h')
            .buildTransformed(MINUTE, SEXA);
    public static final Unit JOUR = new UnitBuilder()
            .addName(Chars.constant("jour"))
            .addSymbol('d')
            .addSymbol('j')
            .buildTransformed(HEURE, new Affine1(24.0, 0.0));



    public static final Unit CELSIUS = new UnitBuilder()
            .addName(Chars.constant("degré celsius"))
            .addSymbol(Chars.constant("°C"))
            .buildTransformed(KELVIN, new Affine1(1.0, 273.15));

    public static final Unit MILLIVOLT = new UnitBuilder()
            .buildPrefixed(SI.PREFIX.MILLI, VOLT);
    public static final Unit KILOVOLT = new UnitBuilder()
            .buildPrefixed(SI.PREFIX.KILO, VOLT);

    public static final Unit G = new UnitBuilder()
            .addName(Chars.constant("G"))
            .addSymbol('g')
            .buildTransformed(METRE_PAR_SECONDE_CARREE, new Affine1(9.80665, 0.0));

    public static final Unit WATTHEURE = new UnitBuilder()
            .addName(Chars.constant("watt-heure"))
            .addSymbol(Chars.constant("Wh"))
            .buildTransformed(JOULE, new Affine1(3600.0, 0.0));

    public static final Unit CALORIE_IT = new UnitBuilder()
            .addName(Chars.constant("calorie it"))
            .addSymbol(Chars.constant("cal"))
            .buildTransformed(JOULE, new Affine1(4.1868, 0.0));



    private static final Unit SUB_BEL = new UnitBuilder()
            .buildPrefixed(SI.PREFIX.PICO, WATT_PAR_METRE_CARRE);
    public static final Unit BEL = new LevelUnit(Chars.constant("bel"), new Chars[]{}, Chars.constant("B"), new Chars[]{},
            new DefaultRatioLog(new Measure(1, SUB_BEL)),
            DIMENSION.NIVEAU_D_INTENSITE_ACOUSTIQUE);

    public static final Unit DECIBEL = new UnitBuilder()
            .buildPrefixed(SI.PREFIX.DECI, BEL);


    public static final class CONSTANT {

        public static final Measure C = new Measure(299_792_458., SI.METRE_PAR_SECONDE);

        public static final Measure MU_0 = new Measure(4 * Math.PI * 1e-7, SI.HENRY_PAR_METRE);

        // ici, il serait bien de faire un calcul simplement avec les mesures, sans passer par les valeurs ni les unités
        public static final Measure EPSILON_0 = new Measure(
                    1 / (MU_0.to(SI.HENRY_PAR_METRE).getValue() * Math.pow(C.to(SI.METRE_PAR_SECONDE).getValue(), 2)),
                    SI.FARAD_PAR_METRE);

        private CONSTANT(){}
    }

    public static final class ANGLE {

        public static final Unit RADIAN_PAR_JOUR = new UnitBuilder()
                .addName(Chars.constant("radian par jour"))
                .buildDerived(DIMENSION.ANGLE_PLAN, DIMENSION.TEMPS);

        public static final Unit RADIAN_PAR_KILOMETRE = new UnitBuilder()
                .addName(Chars.constant("radian par kilomètre"))
                .buildDerived(DIMENSION.ANGLE_PLAN, DIMENSION.LONGUEUR);

        public static final Unit DEGRE = new UnitBuilder()
                .addName(Chars.constant("degré"))
                .addSymbol('°')
                .buildTransformed(SI.RADIAN, new Affine1(2 * Math.PI / 360.0, 0.0));

        public static final Unit ARC_MINUTE = new UnitBuilder()
                .addName(Chars.constant("minute d'arc"))
                .addSymbol('\'')
                .buildTransformed(DEGRE, new Affine1(1.0 / 60.0, 0.0));

        public static final Unit ARC_SECONDE = new UnitBuilder()
                .addName(Chars.constant("seconde d'arc"))
                .addSymbol(Chars.constant("''"))
                .buildTransformed(DEGRE, new Affine1(1.0 / (60.0 * 60.0), 0.0));

        private ANGLE(){}
    }

    public static final class MASS {

        public static final Unit MILLIGRAMME = new UnitBuilder()
                .buildPrefixed(SI.PREFIX.MILLI, SI.GRAMME);

        private MASS(){}
    }

    public static final class LENGTH {

        public static final Unit KILOMETRE = new UnitBuilder()
                .buildPrefixed(SI.PREFIX.KILO, SI.METRE);
        public static final Unit DECIMETRE = new UnitBuilder()
                .buildPrefixed(SI.PREFIX.DECI, SI.METRE);

        private LENGTH(){}

    }

    public static final class VOLUME {

        public static final Unit DECIMETRE_CUBE = new UnitBuilder()
                .addName(Chars.constant("décimètre cube"))
                .addFactor(LENGTH.DECIMETRE, 3)
                .buildDerived(DIMENSION.VOLUME);

        public static final Unit LITRE = new UnitBuilder()
                .addName(Chars.constant("litre"))
                .addSymbol('L')
                .buildTransformed(DECIMETRE_CUBE, UnitUtil.IDENTITY);

        public static final Unit MEGALITRE = new UnitBuilder()
                .buildPrefixed(SI.PREFIX.MEGA, LITRE);
        public static final Unit MILLILITRE = new UnitBuilder()
                .buildPrefixed(SI.PREFIX.MILLI, LITRE);
        public static final Unit MICROLITRE = new UnitBuilder()
                .buildPrefixed(SI.PREFIX.MICRO, LITRE);

        private VOLUME(){}
    }


    private SI(){}

}
