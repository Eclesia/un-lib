package science.unlicense.unit.impl.base;

import java.util.Comparator;
import java.util.Set;
import science.unlicense.math.api.unit.Dimensional;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.math.api.unit.BaseUnit;
import science.unlicense.unit.api.unit.DerivedUnit;
import science.unlicense.unit.api.unit.TransformedUnit;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public class PseudoUnitBaseBuilder extends AbstractBaseBuilder<Unit> {

    private final Set<? extends Dimensional<BaseUnit>> pseudoBaseUnits;

    public PseudoUnitBaseBuilder(final Dimensional<BaseUnit> unit, final int pNumerator, final int pDenominator,
            final Set<? extends Dimensional<BaseUnit>> pseudoBaseUnits) {
        this.pseudoBaseUnits = pseudoBaseUnits;
        base(unit, pNumerator, pDenominator);
    }

    private void base(final Dimensional<BaseUnit> unit, final int pNumerator, final int pDenominator) {
        if (unit instanceof BaseUnit || this.pseudoBaseUnits.contains(unit)) {
            append((Unit) unit, pNumerator, pDenominator);
        } else if (unit instanceof TransformedUnit) {
            base(((TransformedUnit) unit).referenceUnit(), pNumerator, pDenominator);
        } else if (unit instanceof DerivedUnit) {
            extractUnitBase(((DerivedUnit) unit).definitionUnits(), pNumerator, pDenominator);
        } else {
            throw new UnsupportedOperationException("unsupported dimentional " + unit);
        }
    }

    private void extractUnitBase(final Factor<Unit>[] definition, final int pNumerator, final int pDenominator){

        // parcours des dimensions de la définition
        for (final Factor<Unit> factor : definition) {

            final Unit unit = factor.dim();

            if (unit instanceof BaseUnit || this.pseudoBaseUnits.contains(unit)) {
                append((Unit) unit, pNumerator * factor.powerNumerator(), pDenominator * factor.powerDenominator());
            } else if (unit instanceof TransformedUnit) {
                base(((TransformedUnit) unit).referenceUnit(), pNumerator * factor.powerNumerator(),
                        pDenominator * factor.powerDenominator());
            } else if (unit instanceof DerivedUnit) {
                extractUnitBase(((DerivedUnit) unit).definitionUnits(), pNumerator * factor.powerNumerator(),
                        pDenominator * factor.powerDenominator());
            }
        }
    }

    @Override
    protected Comparator<Factor<Unit>> comparator() {
        return (Factor<Unit> o1, Factor<Unit> o2)
                -> o1.dim().symbol().order(o2.dim().symbol());
    }
}
