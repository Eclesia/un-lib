package science.unlicense.unit.impl.conversion;

import java.util.Set;
import science.unlicense.math.api.transform.AbstractTransform1;
import science.unlicense.math.api.transform.Transform1;
import science.unlicense.unit.api.UnitMapper;
import science.unlicense.math.api.unit.Base;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.unit.api.unit.DerivedUnit;
import science.unlicense.unit.api.unit.FundamentalUnit;
import science.unlicense.unit.api.unit.LevelUnit;
import science.unlicense.unit.api.unit.TransformedUnit;
import science.unlicense.math.api.unit.Unit;
import science.unlicense.unit.impl.base.DefaultBase;
import science.unlicense.unit.impl.base.PseudoUnitBaseBuilder;

/**
 *
 * @author Samuel Andrés
 */
public class PseudoBaseSwitchConverter extends AbstractConverter {

    private final UnitMapper mapper;
    private final Transform1 transform;

    public PseudoBaseSwitchConverter(final Unit source, final Unit target, final UnitMapper mapper) {
        super(source, target);
        this.mapper = mapper;

        transform = new AbstractTransform1() {
            @Override
            public double transform(double value) {
                // valeur dans l'unité de définition => valeur dans l'unité de la pseudo-base du mapper
                final double valueInSourcePseudoBase = valueToPseudoBase(value, source, mapper.source());
                final Base<Unit> sourcePseudoBase = getPseudoBase(source, mapper.source());

                // valeur dans la pseudo-base du mapper => valeur dans la base du système cible
                final double valueInTargetBase
                        = switchPseudoBaseValue(valueInSourcePseudoBase, sourcePseudoBase, mapper);

                // valeur dans l'unité de la base cible => valeur dans l'unité cible
                return SingleBaseConverter.valueFromBase(valueInTargetBase, target);
            }

            @Override
            public Transform1 invert() {
                return new PseudoBaseSwitchConverter(target, source, mapper).getTransform();
            }
        };
    }

    @Override
    public Transform1 getTransform() {
        return transform;
    }

    public static Base<Unit> getPseudoBase(final Unit unit, final Set<Unit> pseudoBaseUnits) {
        return new DefaultBase<>(new PseudoUnitBaseBuilder(unit, 1, 1, pseudoBaseUnits).build());
    }

    private static double switchPseudoBaseValue(final double valueInThisBase, final Base<Unit> thisBase,
            final UnitMapper mapper) {

        // conversion dans la base fournie par le mapper

        if (thisBase.size() == 1) {
            /*
            si on a une seule unité dans la base, il se peut qu'il faille appliquer une transformation avec translation
            */
            for (final Factor<Unit> factor : thisBase.factors()) {
                if (factor.power() == 1) {
                    return BaseSwitchConverter.mapAndTransformValue(valueInThisBase, factor.dim(), mapper);
                }
                else {
                    return BaseSwitchConverter.mapAndScaleValue(valueInThisBase, factor, mapper);
                }
            }
        } else {
            // dans le cas général, on applique les ratios d'échelles successivement à la valeur
            double valueInOtherBase = valueInThisBase;
            for (final Factor<Unit> thisEntry : thisBase.factors()) {
                valueInOtherBase = BaseSwitchConverter.mapAndScaleValue(valueInOtherBase, thisEntry, mapper);
            }
            return valueInOtherBase;
        }
        throw new IllegalStateException();
    }

    private static double valueToPseudoBase(final double value, final Unit unit, final Set<Unit> pseudoBase) {
        if (unit instanceof FundamentalUnit || pseudoBase.contains(unit)) {
            return value;
        } else if (unit instanceof TransformedUnit) {
            return valueToPseudoBase(((TransformedUnit) unit).transformation().transform(value),
                            ((TransformedUnit) unit).referenceUnit(), pseudoBase);
        } else if (unit instanceof LevelUnit) {
            return valueToPseudoBase(((LevelUnit) unit).level().toReferenceUnit(value),
                    ((LevelUnit) unit).referenceUnit(), pseudoBase);
        } else if (unit instanceof DerivedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            double prod = value;
            for (final Factor<Unit> factor : ((DerivedUnit) unit).definitionUnits()) {
                prod *= Math.pow(scaleValueToPseudoBase(1., factor.dim(), pseudoBase), factor.power());
            }
            return prod;
        }
        throw new UnsupportedOperationException(unit.symbol().toString());
    }

    private static double scaleValueToPseudoBase(final double value, final Unit unit, final Set<Unit> pseudoBase) {
        if (unit instanceof FundamentalUnit || pseudoBase.contains(unit)) {
            return value;
        } else if (unit instanceof TransformedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            return scaleValueToPseudoBase(((TransformedUnit) unit).transformation().getScale() * value,
                            ((TransformedUnit) unit).referenceUnit(), pseudoBase);
        } else if (unit instanceof LevelUnit) {
            throw new IllegalStateException("unsuported unit type");
        } else if (unit instanceof DerivedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            double prod = value;
            for (final Factor<Unit> factor : ((DerivedUnit) unit).definitionUnits()) {
                prod *= Math.pow(scaleValueToPseudoBase(1., factor.dim(), pseudoBase), factor.power());
            }
            return prod;
        }
        throw new UnsupportedOperationException(unit.symbol().toString());
    }
}
