package science.unlicense.unit.impl.base;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import science.unlicense.math.api.unit.Base;
import science.unlicense.math.api.unit.Dimensional;
import science.unlicense.math.api.unit.Factor;

/**
 *
 * @author Samuel Andrés
 * @param <D>
 */
public class DefaultBase<D extends Dimensional<?>> implements Base<D> {

    private final Set<Factor<D>> base;

    public DefaultBase(final Set<Factor<D>> base) {
        this.base = base;
    }

    @Override
    public void remove0s(){
        final Set<Factor<D>> nullPower = new HashSet<>();
        for (final Factor<D> entry : base) {
            if (entry.power()== 0) {
                nullPower.add(entry);
            }
        }
        this.base.removeAll(nullPower);
    }

    @Override
    public int size() {
        return base.size();
    }

    @Override
    public Set<Factor<D>> factors() {
        return this.base;
    }

    @Override
    public boolean sameBase(final Base other){
        return Objects.equals(this.factors(), other.factors());
    }

    @Override
    public D[] dimensionals(){
        final Set<Factor<D>> factors = factors();
        final D[] result = (D[]) new Object[factors.size()];
        int i = 0;
        for (final Factor<D> factor : factors) {
            result[i++] = factor.dim();
        }
        return result;
    }

    @Override
    public String toString() {
        return "DefaultBase{" + "base=" + base + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.base);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultBase<?> other = (DefaultBase<?>) obj;
        if (!Objects.equals(this.base, other.base)) {
            return false;
        }
        return true;
    }
}
