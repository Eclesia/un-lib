package science.unlicense.unit.impl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.api.unit.Quantities;
import science.unlicense.unit.api.UnitBuilder;
import science.unlicense.math.api.unit.BaseDimension;
import science.unlicense.math.api.unit.DefinedDimension;
import science.unlicense.math.api.unit.DimensionBuilder;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public class Uscs {

    public static final class DIMENSION {

        public static final BaseDimension MASS = new BaseDimension(Chars.constant("mass"), Quantities.MASS, Chars.constant("M"));

        public static final BaseDimension TIME = new BaseDimension(Chars.constant("time"), Quantities.TIME, Chars.constant("T"));

        public static final BaseDimension LENGTH = new BaseDimension(Chars.constant("length"), Quantities.LENGTH, Chars.constant("L"));

        public static final BaseDimension TEMPERATURE = new BaseDimension(Chars.constant("thermodynamic temperature"), Quantities.THERMODYNAMIC_TEMPERATURE, Chars.constant("\u0398"));

        public static final DefinedDimension SURFACE = new DimensionBuilder(Chars.constant("surface"), Quantities.AREA)
                .addFactor(LENGTH, 2).build();

        public static final DefinedDimension VOLUME = new DimensionBuilder(Chars.constant("volume"), Quantities.VOLUME)
                .addFactor(LENGTH, 3).build();

        private DIMENSION() {}

    }

    public static final Unit INTERNATIONAL_FOOT = new UnitBuilder()
            .addName(Chars.constant("foot"))
            .addSymbol(Chars.constant("ft"))
            .buildFundamental(DIMENSION.LENGTH);

    public static final Unit FAHRENHEIT = new UnitBuilder()
            .addName(Chars.constant("degré fahrenheit"))
            .addSymbol(Chars.constant("°F"))
            .buildFundamental(DIMENSION.TEMPERATURE);

    public static final Unit GRAIN = new UnitBuilder()
            .addName(Chars.constant("grain"))
            .addSymbol(Chars.constant("gr"))
            .buildFundamental(DIMENSION.MASS);

    // length -- international

    public static final Unit INTERNATIONAL_INCH = new UnitBuilder()
            .addName(Chars.constant("inch"))
            .addSymbol(Chars.constant("in"))
            .buildTransformed(INTERNATIONAL_FOOT, new Affine1(1.0 / 12.0, 0.0));

    public static final Unit INTERNATIONAL_YARD = new UnitBuilder()
            .addName(Chars.constant("yard"))
            .addSymbol(Chars.constant("yd"))
            .buildTransformed(INTERNATIONAL_FOOT, new Affine1(3.0, 0.0));

    public static final Unit INTERNATIONAL_MILE = new UnitBuilder()
            .addName(Chars.constant("mile"))
            .addSymbol(Chars.constant("mi"))
            .buildTransformed(INTERNATIONAL_FOOT, new Affine1(5280.0, 0.0));

    // length -- survey

    /**
     * <div class="fr">La définition du pied d'arpentage repose sur le mètre (1200/3937 m) qui vaut lui-même 1/0.3048
     * pied international.</div>
     */
    public static final Unit SURVEY_FOOT = new UnitBuilder()
            .addName(Chars.constant("foot"))
            .addSymbol(Chars.constant("ft"))
            .buildTransformed(INTERNATIONAL_FOOT, new Affine1(1200.0 / 0.3048 / 3937.0, 0.0));

    public static final Unit SURVEY_LINK = new UnitBuilder()
            .addName(Chars.constant("link"))
            .addSymbol(Chars.constant("li"))
            .buildTransformed(SURVEY_FOOT, new Affine1(33.0 / 50.0, 0.0));

    public static final Unit SURVEY_ROD = new UnitBuilder()
            .addName(Chars.constant("rod"))
            .addSymbol(Chars.constant("rd"))
            .buildTransformed(SURVEY_FOOT, new Affine1(16.5, 0.0));

    public static final Unit SURVEY_CHAIN = new UnitBuilder()
            .addName(Chars.constant("chain"))
            .addSymbol(Chars.constant("ch"))
            .buildTransformed(SURVEY_ROD, new Affine1(4.0, 0.0));

    public static final Unit SURVEY_FURLONG = new UnitBuilder()
            .addName(Chars.constant("furlong"))
            .addSymbol(Chars.constant("fur"))
            .buildTransformed(SURVEY_CHAIN, new Affine1(10.0, 0.0));

    public static final Unit SURVEY_MILE = new UnitBuilder()
            .addName(Chars.constant("mile"))
            .addSymbol(Chars.constant("mi"))
            .buildTransformed(SURVEY_FURLONG, new Affine1(8.0, 0.0));

    public static final Unit SURVEY_LEAGUE = new UnitBuilder()
            .addName(Chars.constant("league"))
            .addSymbol(Chars.constant("lea"))
            .buildTransformed(SURVEY_MILE, new Affine1(3.0, 0.0));

    // length -- nautical

    public static final Unit FATHOM = new UnitBuilder()
            .addName(Chars.constant("fathom"))
            .addSymbol(Chars.constant("ftm"))
            .buildTransformed(INTERNATIONAL_YARD, new Affine1(2.0, 0.0));

    public static final Unit CABLE = new UnitBuilder()
            .addName(Chars.constant("cable"))
            .addSymbol(Chars.constant("cb"))
            .buildTransformed(FATHOM, new Affine1(120.0, 0.0));

    public static final Unit NAUTICAL_MILE = new UnitBuilder()
            .addName(Chars.constant("nautical mile"))
            .addSymbol(Chars.constant("nmi"))
            .buildTransformed(INTERNATIONAL_FOOT, new Affine1(1_852.0 / 0.3048, 0.0));

    // surface

    public static final Unit SQUARE_SURVEY_FOOT = new UnitBuilder()
            .addName(Chars.constant("square survey foot"))
            .addSymbol(Chars.constant("sq ft"))
            .addFactor(SURVEY_FOOT, 2)
            .buildDerived(DIMENSION.SURFACE);

    public static final Unit SQUARE_INTERNATIONAL_FOOT = new UnitBuilder()
            .addName(Chars.constant("square survey foot"))
            .addSymbol(Chars.constant("sq ft"))
            .addFactor(INTERNATIONAL_FOOT, 2)
            .buildDerived(DIMENSION.SURFACE);

    public static final Unit SQUARE_SURVEY_INCH = new UnitBuilder()
            .addName(Chars.constant("square inch"))
            .addSymbol(Chars.constant("sq in"))
            .buildTransformed(SQUARE_SURVEY_FOOT, new Affine1(1.0 / 144.0, 0.0));

    public static final Unit SQUARE_SURVEY_CHAIN = new UnitBuilder()
            .addName(Chars.constant("square chain"))
            .addSymbol(Chars.constant("sq ch"))
            .buildTransformed(SQUARE_SURVEY_FOOT, new Affine1(4_356.0, 0.0));

    public static final Unit SURVEY_ACRE = new UnitBuilder()
            .addName(Chars.constant("acre"))
            .addSymbol(Chars.constant("ac"))
            .buildTransformed(SQUARE_SURVEY_FOOT, new Affine1(43_560.0, 0.0));

    public static final Unit INTERNATIONAL_ACRE = new UnitBuilder()
            .addName(Chars.constant("acre"))
            .addSymbol(Chars.constant("ac"))
            .buildTransformed(SQUARE_INTERNATIONAL_FOOT, new Affine1(43_560.0, 0.0));

    public static final Unit SURVEY_SECTION = new UnitBuilder()
            .addName(Chars.constant("section"))
            .addSymbol(Chars.constant("sq ch"))
            .buildTransformed(SURVEY_ACRE, new Affine1(640.0, 0.0));

    public static final Unit SQUARE_SURVEY_MILE = new UnitBuilder()
            .addName(Chars.constant("square mile"))
            .addSymbol(Chars.constant("sq mi"))
            .addFactor(SURVEY_MILE, 2)
            .buildDerived(DIMENSION.SURFACE);

    public static final Unit SQUARE_SURVEY_LEAGUE = new UnitBuilder()
            .addName(Chars.constant("square league"))
            .addSymbol(Chars.constant("sq lea"))
            .addFactor(SURVEY_LEAGUE, 2)
            .buildDerived(DIMENSION.SURFACE);

    public static final Unit SURVEY_TOWNSHIP = new UnitBuilder()
            .addName(Chars.constant("survey township"))
            .addSymbol(Chars.constant("twp"))
            .buildTransformed(SURVEY_SECTION, new Affine1(36.0, 0.0));

    public static final Unit CUBIC_FOOT = new UnitBuilder()
            .addName(Chars.constant("cubic foot"))
            .addSymbol(Chars.constant("cu ft"))
            .addFactor(INTERNATIONAL_FOOT, 3)
            .buildDerived(DIMENSION.VOLUME);

    public static final Unit CUBIC_INCH = new UnitBuilder()
            .addName(Chars.constant("cubic inch"))
            .addSymbol(Chars.constant("cu in"))
            .addFactor(INTERNATIONAL_INCH, 3)
            .buildDerived(DIMENSION.VOLUME);

    public static final Unit CUBIC_YARD = new UnitBuilder()
            .addName(Chars.constant("cubic inch"))
            .addSymbol(Chars.constant("cu yd"))
            .addFactor(INTERNATIONAL_YARD, 3)
            .buildDerived(DIMENSION.VOLUME);

    public static final Unit INTERNATIONAL_ACRE_FOOT = new UnitBuilder()
            .addName(Chars.constant("acre-foot"))
            .addSymbol(Chars.constant("acre ft"))
            .addFactor(INTERNATIONAL_ACRE)
            .addFactor(INTERNATIONAL_FOOT)
            .buildDerived(DIMENSION.VOLUME);

    public static final Unit SURVEY_ACRE_FOOT = new UnitBuilder()
            .addName(Chars.constant("acre-foot"))
            .addSymbol(Chars.constant("acre ft"))
            .addFactor(SURVEY_ACRE)
            .addFactor(SURVEY_FOOT)
            .buildDerived(DIMENSION.VOLUME);

    public static final Unit MINIM = new UnitBuilder()
            .addName(Chars.constant("minim"))
            .addSymbol(Chars.constant("min"))
            .buildTransformed(CUBIC_INCH, new Affine1(0.003759765625, 0.0));

    public static final Unit FLUID_DRAM = new UnitBuilder()
            .addName(Chars.constant("fluid dram"))
            .addSymbol(Chars.constant("fl dr"))
            .buildTransformed(MINIM, new Affine1(60.0, 0.0));

    public static final Unit TEASPOON = new UnitBuilder()
            .addName(Chars.constant("teaspoon"))
            .addSymbol(Chars.constant("tsp"))
            .buildTransformed(MINIM, new Affine1(80.0, 0.0));

    public static final Unit TABLESPOON = new UnitBuilder()
            .addName(Chars.constant("tablespoon"))
            .addSymbol(Chars.constant("tsp"))
            .buildTransformed(TEASPOON, new Affine1(3.0, 0.0));

    public static final Unit FLUID_OUNCE = new UnitBuilder()
            .addName(Chars.constant("fluid ounce"))
            .addSymbol(Chars.constant("fl oz"))
            .buildTransformed(TABLESPOON, new Affine1(2.0, 0.0));

    public static final Unit SHOT = new UnitBuilder()
            .addName(Chars.constant("shot"))
            .addSymbol(Chars.constant("jig"))
            .buildTransformed(TABLESPOON, new Affine1(3.0, 0.0));

    public static final Unit GILL = new UnitBuilder()
            .addName(Chars.constant("gill"))
            .addSymbol(Chars.constant("gi"))
            .buildTransformed(FLUID_OUNCE, new Affine1(4.0, 0.0));

    public static final Unit CUP = new UnitBuilder()
            .addName(Chars.constant("cup"))
            .addSymbol(Chars.constant("cp"))
            .buildTransformed(GILL, new Affine1(2.0, 0.0));

    public static final Unit LIQUID_PINT = new UnitBuilder()
            .addName(Chars.constant("pint"))
            .addSymbol(Chars.constant("pt"))
            .buildTransformed(CUP, new Affine1(2.0, 0.0));

    public static final Unit LIQUID_QUART = new UnitBuilder()
            .addName(Chars.constant("quart"))
            .addSymbol(Chars.constant("qt"))
            .buildTransformed(LIQUID_PINT, new Affine1(2.0, 0.0));

    public static final Unit LIQUID_GALLON = new UnitBuilder()
            .addName(Chars.constant("gallon"))
            .addSymbol(Chars.constant("gal"))
            .buildTransformed(CUBIC_INCH, new Affine1(231.0, 0.0));

    public static final Unit LIQUID_BARREL = new UnitBuilder()
            .addName(Chars.constant("barrel"))
            .addSymbol(Chars.constant("bbl"))
            .buildTransformed(LIQUID_GALLON, new Affine1(31.5, 0.0));

    public static final Unit OIL_BARREL = new UnitBuilder()
            .addName(Chars.constant("oil barrel"))
            .addSymbol(Chars.constant("bbl"))
            .buildTransformed(LIQUID_GALLON, new Affine1(42.0, 0.0));

    public static final Unit HOGSHEAD = new UnitBuilder()
            .addName(Chars.constant("hogshead"))
            .addSymbol(Chars.constant("Hhd"))
            .buildTransformed(LIQUID_GALLON, new Affine1(63.0, 0.0));

    public static final Unit DRY_PINT = new UnitBuilder()
            .addName(Chars.constant("pint"))
            .addSymbol(Chars.constant("pt"))
            .buildTransformed(CUBIC_INCH, new Affine1(33.6003125, 0.0));

    public static final Unit DRY_QUART = new UnitBuilder()
            .addName(Chars.constant("quart"))
            .addSymbol(Chars.constant("qt"))
            .buildTransformed(DRY_PINT, new Affine1(2.0, 0.0));

    public static final Unit DRY_GALLON = new UnitBuilder()
            .addName(Chars.constant("gallon"))
            .addSymbol(Chars.constant("gal"))
            .buildTransformed(DRY_QUART, new Affine1(4.0, 0.0));

    public static final Unit PECK = new UnitBuilder()
            .addName(Chars.constant("peck"))
            .addSymbol(Chars.constant("pk"))
            .buildTransformed(DRY_GALLON, new Affine1(2.0, 0.0));

    public static final Unit BUSHEL = new UnitBuilder()
            .addName(Chars.constant("bushel"))
            .addSymbol(Chars.constant("bu"))
            .buildTransformed(PECK, new Affine1(4.0, 0.0));

    public static final Unit DRY_BARREL = new UnitBuilder()
            .addName(Chars.constant("barrel"))
            .addSymbol(Chars.constant("bbl"))
            .buildTransformed(CUBIC_INCH, new Affine1(7056.0, 0.0));

    // masse -- avoirdupois

    public static final Unit AVOIRDUPOIS_POUND = new UnitBuilder()
            .addName(Chars.constant("pound"))
            .addSymbol(Chars.constant("lb"))
            .buildTransformed(GRAIN, new Affine1(7000.0, 0.0));

    public static final Unit AVOIRDUPOIS_OUNCE = new UnitBuilder()
            .addName(Chars.constant("ounce"))
            .addSymbol(Chars.constant("oz"))
            .buildTransformed(AVOIRDUPOIS_POUND, new Affine1(1.0 / 16.0, 0.0));

    public static final Unit AVOIRDUPOIS_DRAM = new UnitBuilder()
            .addName(Chars.constant("dram"))
            .addSymbol(Chars.constant("dr"))
            .buildTransformed(AVOIRDUPOIS_OUNCE, new Affine1(1.0 / 16.0, 0.0));

    public static final Unit AVOIRDUPOIS_GRAIN = new UnitBuilder()
            .addName(Chars.constant("dram"))
            .addSymbol(Chars.constant("Gr"))
            .buildTransformed(AVOIRDUPOIS_DRAM, new Affine1(1.0 / 7000.0, 0.0));

    public static final Unit AVOIRDUPOIS_SHORT_HUNDREDWEIGHT = new UnitBuilder()
            .addName(Chars.constant("short hundredweight"))
            .addSymbol(Chars.constant("cwt"))
            .buildTransformed(AVOIRDUPOIS_POUND, new Affine1(100.0, 0.0));

    public static final Unit AVOIRDUPOIS_LONG_HUNDREDWEIGHT = new UnitBuilder()
            .addName(Chars.constant("long hundredweight"))
            .addSymbol(Chars.constant("cwt"))
            .buildTransformed(AVOIRDUPOIS_POUND, new Affine1(112.0, 0.0));

    public static final Unit AVOIRDUPOIS_SHORT_TON = new UnitBuilder()
            .addName(Chars.constant("short ton"))
            .addSymbol(Chars.constant("cwt"))
            .buildTransformed(AVOIRDUPOIS_SHORT_HUNDREDWEIGHT, new Affine1(20.0, 0.0));

    public static final Unit AVOIRDUPOIS_LONG_TON = new UnitBuilder()
            .addName(Chars.constant("long ton"))
            .addSymbol(Chars.constant("cwt"))
            .buildTransformed(AVOIRDUPOIS_LONG_HUNDREDWEIGHT, new Affine1(20.0, 0.0));

    public static final Unit TROY_PENNYWEIGHT = new UnitBuilder()
            .addName(Chars.constant("pennyweight"))
            .addSymbol(Chars.constant("cwt"))
            .buildTransformed(GRAIN, new Affine1(24.0, 0.0));

    public static final Unit TROY_OUNCE = new UnitBuilder()
            .addName(Chars.constant("troy ounce"))
            .addSymbol(Chars.constant("oz t"))
            .buildTransformed(TROY_PENNYWEIGHT, new Affine1(20.0, 0.0));

    public static final Unit TROY_POUND = new UnitBuilder()
            .addName(Chars.constant("troy pound"))
            .addSymbol(Chars.constant("lb t"))
            .buildTransformed(TROY_OUNCE, new Affine1(12.0, 0.0));

    private Uscs() {}

    private static final Uscs INSTANCE = new Uscs();

    public static Uscs getInstance(){
        return INSTANCE;
    }
}
