package science.unlicense.unit.impl.conversion;

import java.util.Map;
import science.unlicense.math.api.transform.AbstractTransform1;
import science.unlicense.math.api.transform.Transform1;
import science.unlicense.math.impl.Affine1;
import science.unlicense.unit.api.UnitMapper;
import science.unlicense.math.api.unit.Base;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.math.api.unit.BaseUnit;
import science.unlicense.math.api.unit.Unit;
import science.unlicense.unit.impl.UnitUtil;

/**
 *
 * @author Samuel Andrés
 */
public class BaseSwitchConverter extends AbstractConverter {

    private final UnitMapper mapper;
    private final Transform1 transform;

    public BaseSwitchConverter(final Unit source, final Unit target, final UnitMapper mapper) {
        super(source, target);
        this.mapper = mapper;

        transform = new AbstractTransform1() {
            @Override
            public double transform(double value) {
                // valeur dans l'unité de définition => valeur dans l'unité de la base de l'unité de définition
                final double valueInBase = SingleBaseConverter.valueToBase(value, source);
                final Base<BaseUnit> thisBase = UnitUtil.getBase(source);

                // valeur dans l'unité de la base de l'unité de saisie => valeur dans un système cible
                final double valueInOtherBase = switchBaseValue(valueInBase, thisBase, mapper);

                // valeur dans l'unité de la base cible => valeur dans l'unité cible
                return SingleBaseConverter.valueFromBase(valueInOtherBase, target);
            }

            @Override
            public Transform1 invert() {
                return new BaseSwitchConverter(target, source, mapper).getTransform();
            }
        };
    }

    @Override
    public Transform1 getTransform() {
        return transform;
    }

    private static double switchBaseValue(final double valueInThisBase, final Base<BaseUnit> thisBase,
            final UnitMapper mapper) {

        // conversion dans la base fournie par le mapper

        if (thisBase.size() == 1) {
            /*
            si on a une seule unité dans la base, il se peut qu'il faille appliquer une transformation avec translation
            */
            for (final Factor<BaseUnit> factor : thisBase.factors()) {
                if (factor.power() == 1) {
                    return mapAndTransformValue(valueInThisBase, factor.dim(), mapper);
                }
                else {
                    return mapAndScaleValue(valueInThisBase, factor, mapper);
                }
            }
        } else {
            // dans le cas général, on applique les ratios d'échelles successivement à la valeur
            double valueInOtherBase = valueInThisBase;
            for (final Factor<BaseUnit> factor : thisBase.factors()) {
                valueInOtherBase = mapAndScaleValue(valueInOtherBase, factor, mapper);
            }
            return valueInOtherBase;
        }
        throw new IllegalStateException();
    }

    public static double mapAndTransformValue(double valueInOtherBase, final Unit unit, final UnitMapper mapper) {

        final Map.Entry<Affine1, Unit> transformParam = mapper.transformation(unit);

        // si on trouve une transformation connaissant l'unité de base de départ
        if (transformParam != null) {

            // on applique la transformation
            valueInOtherBase = transformParam.getKey().transform(valueInOtherBase);

            /*
            puis on exprime la valeur dans la base de l'unité cible à partir de l'unité résultant de la transformation
            */
            return SingleBaseConverter.valueToBase(valueInOtherBase, transformParam.getValue());
        }
        throw new IllegalArgumentException("no mapping found for " + unit);
    }

    public static double mapAndScaleValue(double valueInOtherBase, final Factor<? extends Unit> factor,
            final UnitMapper mapper) {

        final Map.Entry<Affine1, Unit> transformParam = mapper.transformation(factor.dim());

        // si on trouve une transformation connaissant l'unité de base de départ
        if (transformParam != null) {

            // on applique la transformation
            valueInOtherBase *= Math.pow(transformParam.getKey().getScale(), factor.power());

            /*
            puis on exprime la valeur dans la base de l'unité cible à partir de l'unité résultant de la transformation
            */
            return SingleBaseConverter.scaleValueToBase(valueInOtherBase, transformParam.getValue());
        }
        throw new IllegalArgumentException("no mapping found for " + factor.dim());
    }
}
