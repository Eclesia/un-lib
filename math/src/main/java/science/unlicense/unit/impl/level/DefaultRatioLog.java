package science.unlicense.unit.impl.level;

import science.unlicense.unit.api.Measure;

/**
 *
 * @author Samuel Andrés
 */
public class DefaultRatioLog extends AbstractLevel {

    public DefaultRatioLog(final Measure reference) {
        super(reference);
    }

    @Override
    public double toLevel(final double value) {
        return Math.log10(value / this.referenceMeasure().getValue());
    }

    @Override
    public double toReferenceUnit(final double value) {
        return this.referenceMeasure().getValue() * Math.pow(10., value);
    }
}
