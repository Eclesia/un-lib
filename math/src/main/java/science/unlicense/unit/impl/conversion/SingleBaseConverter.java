package science.unlicense.unit.impl.conversion;

import science.unlicense.math.api.transform.AbstractTransform1;
import science.unlicense.math.api.transform.Transform1;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.unit.api.unit.DerivedUnit;
import science.unlicense.unit.api.unit.FundamentalUnit;
import science.unlicense.unit.api.unit.LevelUnit;
import science.unlicense.unit.api.unit.TransformedUnit;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public class SingleBaseConverter extends AbstractConverter {

    private final Transform1 transform;

    public SingleBaseConverter(final Unit source, final Unit target) {
        super(source, target);
        transform = new AbstractTransform1() {
            @Override
            public double transform(double value) {
                // valeur dans l'unité de définition => valeur dans l'unité de la base de l'unité de définition
                final double valueInBase = valueToBase(value, source);

                // valeur dans l'unité de la base => valeur dans l'unité cible
                return valueFromBase(valueInBase, target);
            }

            @Override
            public Transform1 invert() {
                return new SingleBaseConverter(target, source).getTransform();
            }
        };
    }

    @Override
    public Transform1 getTransform() {
        return transform;
    }

    /**
     * <div class="fr">Expression d'une valeur dans la base de l'unité en paramètre.</div>
     *
     * @param value
     * @param unit
     * @return
     */
    public static double valueToBase(final double value, final Unit unit) {
        if (unit instanceof FundamentalUnit) {
            return value;
        } else if (unit instanceof TransformedUnit) {
            return valueToBase(((TransformedUnit) unit).transformation().transform(value),
                            ((TransformedUnit) unit).referenceUnit());
        } else if (unit instanceof LevelUnit) {
            return valueToBase(((LevelUnit) unit).level().toReferenceUnit(value), ((LevelUnit) unit).referenceUnit());
        } else if (unit instanceof DerivedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            double prod = value;
            for (final Factor<Unit> factor : ((DerivedUnit) unit).definitionUnits()) {
                prod *= Math.pow(scaleValueToBase(1., factor.dim()), factor.power());
            }
            return prod;
        }
        throw new UnsupportedOperationException(unit.symbol().toString());
    }

    public static double scaleValueToBase(final double value, final Unit unit) {
        if (unit instanceof FundamentalUnit) {
            return value;
        } else if (unit instanceof TransformedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            return scaleValueToBase(((TransformedUnit) unit).transformation().getScale() * value,
                            ((TransformedUnit) unit).referenceUnit());
        } else if (unit instanceof LevelUnit) {
            throw new IllegalStateException("unsuported unit type");
        } else if (unit instanceof DerivedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            double prod = value;
            for (final Factor<Unit> factor : ((DerivedUnit) unit).definitionUnits()) {
                prod *= Math.pow(scaleValueToBase(1., factor.dim()), factor.power());
            }
            return prod;
        }
        throw new UnsupportedOperationException(unit.symbol().toString());
    }

    /**
     * <div class="fr">Expression d'une valeur dans l'unité en paramètre à partir de la valeur exprimée dans sa base.
     * </div>
     *
     * @param value <span class="fr">valeur à exprimer en unités de base</span>
     * @param unit <span class="fr">unité dans laquelle la valeur à transformer est exprimée</span>
     * @return
     */
    public static double valueFromBase(final double value, final Unit unit) {
        if (unit instanceof FundamentalUnit) {
            return value;
        } else if (unit instanceof TransformedUnit) {
            return ((TransformedUnit) unit).transformation()
                    .invert()
                    .transform(valueFromBase(value, ((TransformedUnit) unit).referenceUnit()));
        } else if (unit instanceof LevelUnit) {
            return ((LevelUnit) unit).level().toLevel(valueFromBase(value, ((LevelUnit) unit).referenceUnit()));
        } else if (unit instanceof DerivedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            double prod = value;
            for (final Factor<Unit> factor : ((DerivedUnit) unit).definitionUnits()) {
                prod *= Math.pow(scaleValueFromBase(1., factor.dim()), factor.power());
            }
            return prod;
        }
        throw new UnsupportedOperationException(unit.symbol().toString());
    }

    /**
     * <div class="fr">Expression d'une valeur dans l'unité en paramètre à partir de la valeur exprimée dans sa base en
     * applicant les seules transformations d'échelles d'unité.
     * </div>
     *
     * @param value <span class="fr">valeur à exprimer en unités de base</span>
     * @param unit <span class="fr">unité dans laquelle la valeur à transformer est exprimée</span>
     * @return
     */
    private static double scaleValueFromBase(final double value, final Unit unit) {
        if (unit instanceof FundamentalUnit) {
            return value;
        } else if (unit instanceof TransformedUnit) {
            double v = scaleValueFromBase(value, ((TransformedUnit) unit).referenceUnit());
            return ((TransformedUnit) unit).transformation()
                    .invert().getScale() * v;
        } else if (unit instanceof LevelUnit) {
            throw new IllegalStateException("unsuported unit type");
        } else if (unit instanceof DerivedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            double prod = value;
            for (final Factor<Unit> factor : ((DerivedUnit) unit).definitionUnits()) {
                prod *= Math.pow(scaleValueFromBase(1., factor.dim()), factor.power());
            }
            return prod;
        }
        throw new UnsupportedOperationException(unit.symbol().toString());
    }
}
