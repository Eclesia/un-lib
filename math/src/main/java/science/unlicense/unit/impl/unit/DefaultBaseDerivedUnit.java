package science.unlicense.unit.impl.unit;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.math.api.unit.Dimension;
import science.unlicense.unit.api.unit.BaseDerivedUnit;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public class DefaultBaseDerivedUnit extends DefaultDerivedUnit implements BaseDerivedUnit {

    public DefaultBaseDerivedUnit(final Chars name, final Chars[] alternativeNames, final Chars symbol,
            final Chars[] alternativeSymbols, final Dimension[] dimension,
            final Factor<Unit>... definitionUnits) {
        super(name, alternativeNames, symbol, alternativeSymbols, dimension, definitionUnits);
    }
}
