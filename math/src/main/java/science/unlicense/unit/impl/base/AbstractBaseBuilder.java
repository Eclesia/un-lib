package science.unlicense.unit.impl.base;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import science.unlicense.math.api.unit.Dimensional;
import science.unlicense.math.api.unit.Factor;

/**
 *
 * @author Samuel Andrés
 * @param <D>
 */
public abstract class AbstractBaseBuilder<D extends Dimensional<?>> {

    private final Map<D, int[]> base = new HashMap<>();

    protected AbstractBaseBuilder<D> append(final D dim, final int numerator, final int denominator) {
        if (!base.containsKey(dim)) {
            base.put(dim, new int[]{numerator, denominator});
        } else {
            base.put(dim, addQuotients(base.get(dim)[0], base.get(dim)[1], numerator, denominator));
        }
        return this;
    }

    private static int[] addQuotients(final int num1, final int denom1, final int num2, final int denom2) {
        if (denom1 == denom2) {
            return new int[]{num1 + num2, denom1};
        } else {
            return new int[]{num1 * denom2 + num2 * denom1, denom1 *  denom2};
        }
    }

    protected abstract Comparator<Factor<D>> comparator();

    public Set<Factor<D>> build() {
        final NavigableSet<Factor<D>> treeSet = new TreeSet<>(comparator());
        treeSet.addAll(base.entrySet().stream()
                .map(e -> new DefaultFactor<>(e.getKey(), e.getValue()[0], e.getValue()[1]))
                .collect(Collectors.toSet()));
        return treeSet;
    }
}
