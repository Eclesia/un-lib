
package science.unlicense.unit.impl;

import java.util.Map;
import java.util.Set;
import science.unlicense.math.api.transform.ConcatenateTransform;
import science.unlicense.math.api.transform.Transform1;
import science.unlicense.math.impl.Affine1;
import science.unlicense.unit.api.UnitMapper;
import science.unlicense.math.api.unit.Base;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.math.api.unit.BaseUnit;
import science.unlicense.unit.api.unit.DerivedUnit;
import science.unlicense.unit.api.unit.FundamentalUnit;
import science.unlicense.unit.api.unit.LevelUnit;
import science.unlicense.unit.api.unit.TransformedUnit;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public class Conversions {

    public static Affine1 paramsToBase(final Unit unit) {
        if (unit instanceof FundamentalUnit) {
            return new Affine1(1.0, 0.0);
        } else if (unit instanceof TransformedUnit) {

            final Affine1 trParams = paramsToBase(((TransformedUnit) unit).referenceUnit());
            final Affine1 tr = ((TransformedUnit) unit).transformation();
            return (Affine1) ConcatenateTransform.create(trParams, tr);
        } else if (unit instanceof LevelUnit) {
            throw new IllegalStateException("unsuported unit type");
        } else if (unit instanceof DerivedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            double transform = 1.;
            for (final Factor<Unit> factor : ((DerivedUnit) unit).definitionUnits()) {
                final Affine1 paramsToBase = paramsToBase(factor.dim());
                transform *= Math.pow(paramsToBase.getM00(), factor.power());
            }
            return new Affine1(transform, 0.0);
        }
        throw new UnsupportedOperationException(unit.symbol().toString());
    }

    public static Affine1 paramsFromBase(final Unit unit) {
        if (unit instanceof FundamentalUnit) {
            return new Affine1(1.0, 0.0);
        } else if (unit instanceof TransformedUnit) {

            final Affine1 trParams = paramsFromBase(((TransformedUnit) unit).referenceUnit());
            final Affine1 tr = ((TransformedUnit) unit).transformation().invert();
            return (Affine1) ConcatenateTransform.create(trParams, tr);
        } else if (unit instanceof LevelUnit) {
            throw new IllegalStateException("unsuported unit type");
        } else if (unit instanceof DerivedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            double transform = 1.;
            for (final Factor<Unit> factor : ((DerivedUnit) unit).definitionUnits()) {
                final Affine1 paramsFromBase = paramsFromBase(factor.dim());
                transform *= Math.pow(paramsFromBase.getM00(), factor.power());
            }
            return new Affine1(transform, 0.0);
        }
        throw new UnsupportedOperationException(unit.symbol().toString());
    }


    public static Affine1 switchBaseParams(final Base<BaseUnit> thisBase, final UnitMapper mapper) {

        // conversion dans la base fournie par le mapper

        if (thisBase.size() == 1) {
            /*
            si on a une seule unité dans la base, il se peut qu'il faille appliquer une transformation avec translation
            */
            for (final Factor<BaseUnit> factor : thisBase.factors()) {
                if (factor.power() == 1) {
                    return mapAndScaleParams(factor.dim(), mapper);
                }
                else {
                    return new Affine1(mapAndScaleParams(factor, mapper), 0.0);
                }
            }
        } else {
            // dans le cas général, on applique les ratios d'échelles successivement à la valeur
            double valueInOtherBase = 1.;
            for (final Factor<BaseUnit> factor : thisBase.factors()) {
                valueInOtherBase *= mapAndScaleParams(factor, mapper);
            }
            return new Affine1(valueInOtherBase, 0.0);
        }
        throw new IllegalStateException();
    }

    public static Affine1 mapAndScaleParams(final Unit unit, final UnitMapper mapper) {

        final Map.Entry<Affine1, Unit> transformParam = mapper.transformation(unit);

        // si on trouve une transformation connaissant l'unité de base de départ
        if (transformParam != null) {

            // on applique la transformation
            final Transform1 transformation = transformParam.getKey();

            //puis on exprime la valeur dans la base de l'unité cible à partir de l'unité résultant de la transformation
            final Affine1 paramsToBase = Conversions.paramsToBase(transformParam.getValue());

            return (Affine1) ConcatenateTransform.create(transformation, paramsToBase);
        }
        throw new IllegalArgumentException("no mapping found for " + unit);
    }

    public static double mapAndScaleParams(final Factor<? extends Unit> thisEntry, final UnitMapper mapper) {

        final Map.Entry<Affine1, Unit> transformParam = mapper.transformation(thisEntry.dim());

        // si on trouve une transformation connaissant l'unité de base de départ
        if (transformParam != null) {

            double scaling = 1.;

            // on applique la transformation
            scaling *= Math.pow(transformParam.getKey().getScale(), thisEntry.power());

            /*
            puis on exprime la valeur dans la base de l'unité cible à partir de l'unité résultant de la transformation
             */
            final Affine1 paramsFromBase = Conversions.paramsFromBase(transformParam.getValue());
            return scaling * paramsFromBase.getM00();
        }
        throw new IllegalArgumentException("no mapping found for " + thisEntry.dim());
    }

    public static Affine1 switchPseudoBaseParams(final Base<Unit> thisBase, final UnitMapper mapper) {

        // conversion dans la base fournie par le mapper

        if (thisBase.size() == 1) {
            /*
            si on a une seule unité dans la base, il se peut qu'il faille appliquer une transformation avec translation
            */
            for (final Factor<Unit> factor : thisBase.factors()) {
                if (factor.power() == 1) {
                    return Conversions.mapAndScaleParams(factor.dim(), mapper);
                }
                else {
                    return new Affine1(Conversions.mapAndScaleParams(factor, mapper), 0.0);
                }
            }
        } else {
            // dans le cas général, on applique les ratios d'échelles successivement à la valeur
            double valueInOtherBase = 1.;
            for (final Factor<Unit> thisEntry : thisBase.factors()) {
                valueInOtherBase *= Conversions.mapAndScaleParams(thisEntry, mapper);
            }
            return new Affine1(valueInOtherBase, 0.0);
        }
        throw new IllegalStateException();
    }

    public static Affine1 paramsToPseudoBase(final Unit unit, final Set<Unit> pseudoBase) {
        if (unit instanceof FundamentalUnit || pseudoBase.contains(unit)) {
            return new Affine1(1.0, 0.0);
        } else if (unit instanceof TransformedUnit) {

            final Affine1 trParams = paramsToPseudoBase(((TransformedUnit) unit).referenceUnit(), pseudoBase);
            final Affine1 tr = ((TransformedUnit) unit).transformation();
            return (Affine1) ConcatenateTransform.create(trParams, tr);
        } else if (unit instanceof LevelUnit) {
            throw new IllegalStateException("unsuported unit type");
        } else if (unit instanceof DerivedUnit) {
            /*
            En combinaison avec d'autres unités, il ne faut plus appliquer de décalage d'origine d'échelle (température)
            mais uniquement appliquer le facteur d'échelle.
            */
            double transform = 1.;
            for (final Factor<Unit> factor : ((DerivedUnit) unit).definitionUnits()) {
                final Affine1 paramsToPseudoBase = paramsToPseudoBase(factor.dim(), pseudoBase);
                transform *= Math.pow(paramsToPseudoBase.getM00(), factor.power());
            }
            return new Affine1(transform, 0.0);
        }
        throw new UnsupportedOperationException(unit.symbol().toString());
    }
}
