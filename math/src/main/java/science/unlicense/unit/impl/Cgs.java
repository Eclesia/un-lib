package science.unlicense.unit.impl;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.api.unit.Prefix;
import science.unlicense.math.api.unit.Quantities;
import science.unlicense.unit.api.UnitBuilder;
import science.unlicense.math.api.unit.BaseDimension;
import science.unlicense.math.api.unit.DefinedDimension;
import science.unlicense.math.api.unit.DimensionBuilder;
import science.unlicense.math.api.unit.Unit;
import science.unlicense.unit.impl.unit.PrebaseTransformedUnit;

/**
 *
 * @author Samuel Andrés
 */
public class Cgs {

    public static final class PREFIX {

        public static final Prefix KILO = new Prefix(1e3, Chars.constant("k"));
        public static final Prefix DECI = new Prefix(1e-1, Chars.constant("d"));
        public static final Prefix CENTI = new Prefix(1e-2, Chars.constant("c"));
        public static final Prefix MILLI = new Prefix(1e-3, Chars.constant("m"));

        private PREFIX(){}
    }

    public static class DIMENSION {

        public static final BaseDimension ADIMENSION = new BaseDimension(Chars.constant("adimension"), null, Chars.constant("1"));

        public static final BaseDimension MASSE = new BaseDimension(Chars.constant("masse"), Quantities.MASS, Chars.constant("M"));

        public static final BaseDimension TEMPS = new BaseDimension(Chars.constant("temps"), Quantities.TIME, Chars.constant("T"));

        public static final BaseDimension LONGUEUR = new BaseDimension(Chars.constant("longueur"), Quantities.LENGTH, Chars.constant("L"));

        public static final DefinedDimension FORCE = new DimensionBuilder(Chars.constant("force"), Quantities.FORCE)
                .addFactor(LONGUEUR)
                .addFactor(MASSE)
                .addFactor(TEMPS, -2).build();

        public static final DefinedDimension ENERGIE = new DimensionBuilder(Chars.constant("énergie"), Quantities.ENERGY)
                .addFactor(LONGUEUR)
                .addFactor(FORCE).build();

        DIMENSION() {}
    }


    public static final Unit UN = new UnitBuilder()
            .addName(Chars.constant("un"))
            .buildFundamental(DIMENSION.ADIMENSION);

    /**
     * <div class="fr">
     * Le mètre n'est pas une unité de base ni une unité dérivée d'une unité de base.
     * C'est une unité transformée à partir d'une unité de base (le centimètre), mais qui n'est pas préfixée.
     *
     * Le centimètre, unité de base des longueurs, a néanmoins également besoin du mètre car il est une unité préfixée à
     * partir du mètre.
     *
     * Ceci fait que le mètre et le centimètre sont interdépendants, le premier ayant besoin de son unité de base de
     * définition et le second de l'unité à partir de laquelle il est préfixé.
     * </div>
     */
    public static final Unit METRE = new PrebaseTransformedUnit(Chars.constant("mètre"), Chars.constant("m"), new Affine1(100.0, 0.0)) {
        @Override
        public BaseDimension[] dimension() {
            return new BaseDimension[]{DIMENSION.LONGUEUR};
        }

        @Override
        public Unit referenceUnit() {
            return CENTIMETRE;
        }
    };

    // unités de base
    public static final Unit CENTIMETRE = new UnitBuilder()
            .buildPrefixed(SI.PREFIX.CENTI, METRE, true);

    public static final Unit GRAMME = new UnitBuilder()
            .addName(Chars.constant("gramme"))
            .addSymbol('g')
            .buildFundamental(DIMENSION.MASSE);

    public static final Unit SECONDE = new UnitBuilder()
            .addName(Chars.constant("seconde"))
            .addSymbol('s')
            .buildFundamental(DIMENSION.TEMPS);

    public static final Unit DYNE = new UnitBuilder()
            .addName(Chars.constant("dyne"))
            .addSymbol(Chars.constant("dyn"))
            .addFactor(CENTIMETRE)
            .addFactor(GRAMME)
            .addFactor(SECONDE, -2)
            .buildDerived(DIMENSION.FORCE);

    public static final Unit ERG = new UnitBuilder()
            .addName(Chars.constant("erg"))
            .addSymbol(Chars.constant("erg"))
            .addFactor(DYNE)
            .addFactor(CENTIMETRE)
            .buildDerived(DIMENSION.ENERGIE);

    private static final Cgs INSTANCE = new Cgs();

    protected Cgs(){}

    public static Cgs getInstance(){
        return INSTANCE;
    }
}
