package science.unlicense.unit.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import science.unlicense.math.impl.Affine1;
import science.unlicense.unit.api.UnitMapper;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public class DefaultUnitMapper implements UnitMapper {

    private final Map<Unit, Map.Entry<Affine1, Unit>> mapping = new HashMap<>();

    @Override
    public Map.Entry<Affine1, Unit> transformation(Unit u1) {
        return mapping.get(u1);
    }

    protected final void addToMap(final Unit source, final Affine1 transformation, final Unit unit) {
        mapping.put(source, new HashMap.SimpleImmutableEntry<>(transformation, unit));
    }

    @Override
    public Set<Unit> source() {
        return mapping.keySet();
    }
}
