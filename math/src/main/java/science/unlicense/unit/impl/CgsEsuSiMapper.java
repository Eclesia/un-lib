package science.unlicense.unit.impl;

import science.unlicense.math.impl.Affine1;

/**
 *
 * @author Samuel Andrés
 */
public class CgsEsuSiMapper extends CgsSiMapper {

    private static final CgsEsuSiMapper INSTANCE = new CgsEsuSiMapper();

    public static final double C = 299_792_458.;

    protected CgsEsuSiMapper() {
        addToMap(CgsEsu.STATCOULOMB, new Affine1(0.1 / C, 0.0), SI.COULOMB);
    }

    public static CgsEsuSiMapper getInstance() {
        return INSTANCE;
    }
}
