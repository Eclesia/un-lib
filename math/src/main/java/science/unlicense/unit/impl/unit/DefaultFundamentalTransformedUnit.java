package science.unlicense.unit.impl.unit;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.Affine1;
import science.unlicense.unit.api.unit.FundamentalUnit;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public class DefaultFundamentalTransformedUnit extends DefaultTransformedUnit implements FundamentalUnit {


    public DefaultFundamentalTransformedUnit(final Chars name, final Chars[] alternativeNames, final Chars symbol,
            final Chars[] alternativeSymbols, final Affine1 transformation, final Unit definitionUnit)
    {
        super(name, alternativeNames, symbol, alternativeSymbols, transformation, definitionUnit);
    }

}
