package science.unlicense.unit.impl.unit;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.api.unit.BaseDimension;
import science.unlicense.unit.api.unit.TransformedUnit;

/**
 * <div class="fr">Amorce d'implémentation d'unité transformée spécifique aux unités servant à définir une unité
 * fondamentale à partir de laquelle elles sont elles-mêmes construites par transformation.</div>
 *
 * @author Samuel Andrés
 */
public abstract class PrebaseTransformedUnit implements TransformedUnit {

    private final Chars name;
    private final Chars symbol;
    private final Affine1 transformation;
    private final Chars[] alternativeNames;
    private final Chars[] alternativeSymbols;

    public PrebaseTransformedUnit(final Chars name, final Chars symbol, final Affine1 transformation) {
        this(name, symbol, transformation, new Chars[]{}, new Chars[]{});
    }

    public PrebaseTransformedUnit(final Chars name, final Chars symbol, final Affine1 transformation,
            final Chars[] alternativeNames, final Chars[] alternativeSymbols) {
        this.name = name;
        this.symbol = symbol;
        this.transformation = transformation;
        this.alternativeNames = alternativeNames;
        this.alternativeSymbols = alternativeSymbols;
    }

    @Override
    public Affine1 transformation() {
        return this.transformation;
    }

    @Override
    public Chars name() {
        return this.name;
    }

    @Override
    public Chars[] alternativeNames() {
        return this.alternativeNames;
    }

    @Override
    public Chars symbol() {
        return symbol;
    }

    @Override
    public Chars[] alternativeSymbols() {
        return alternativeSymbols;
    }

    @Override
    public abstract BaseDimension[] dimension();
}
