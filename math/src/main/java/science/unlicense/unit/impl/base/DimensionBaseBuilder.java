package science.unlicense.unit.impl.base;

import java.util.Comparator;
import science.unlicense.math.api.unit.Dimensional;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.math.api.unit.BaseDimension;
import science.unlicense.math.api.unit.DefinedDimension;
import science.unlicense.math.api.unit.Dimension;

/**
 *
 * @author Samuel Andrés
 */
public class DimensionBaseBuilder extends AbstractBaseBuilder<BaseDimension> {

    public DimensionBaseBuilder(final Dimensional<BaseDimension> dim, final int pNumerator, final int pDenominator) {
        base(dim, pNumerator, pDenominator);
    }

    private void base(final Dimensional<BaseDimension> dim, final int pNumerator, final int pDenominator) {
        if (dim instanceof BaseDimension) {
            append((BaseDimension) dim, pNumerator, pDenominator);
        } else if (dim instanceof DefinedDimension) {
            extractDimensionBase(((DefinedDimension) dim).definition(), pNumerator, pDenominator);
        }
    }

    private void extractDimensionBase(final Factor<Dimension>[] definition, final int pNumerator,
            final int pDenominator){

        // parcours des dimensions de la définition
        for (final Factor<Dimension> factor : definition) {

            final Dimension dim = factor.dim();

            if (dim instanceof BaseDimension) {
                append((BaseDimension) dim, pNumerator * factor.powerNumerator(),
                        pDenominator * factor.powerDenominator());
            } else if (dim instanceof DefinedDimension) {
                extractDimensionBase(((DefinedDimension) dim).definition(), pNumerator * factor.powerNumerator(),
                        pDenominator * factor.powerDenominator());
            }
        }
    }

    @Override
    protected Comparator<Factor<BaseDimension>> comparator() {
        return (Factor<BaseDimension> o1, Factor<BaseDimension> o2)
                -> o1.dim().symbol().order(o2.dim().symbol());
    }
}
