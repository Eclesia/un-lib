package science.unlicense.unit.impl;

import science.unlicense.math.impl.Affine1;


/**
 *
 * @author Samuel Andrés
 */
public class UscsSiMapper extends DefaultUnitMapper {

    private static final UscsSiMapper INSTANCE = new UscsSiMapper();

    private UscsSiMapper() {
        addToMap(Uscs.INTERNATIONAL_FOOT, new Affine1(0.3048, 0.0), SI.METRE);
        addToMap(Uscs.FAHRENHEIT, new Affine1(5.0 / 9.0, - 5.0 / 9.0 * 32.0), SI.CELSIUS);
        addToMap(Uscs.GRAIN, new Affine1(64.798_91, 0.0), SI.MASS.MILLIGRAMME);
    }

    public static UscsSiMapper getInstance() {
        return INSTANCE;
    }
}
