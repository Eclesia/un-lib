package science.unlicense.unit.impl.base;

import java.util.Comparator;
import science.unlicense.math.api.unit.Dimensional;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.math.api.unit.BaseUnit;
import science.unlicense.unit.api.unit.DerivedUnit;
import science.unlicense.unit.api.unit.LevelUnit;
import science.unlicense.unit.api.unit.TransformedUnit;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public class UnitBaseBuilder extends AbstractBaseBuilder<BaseUnit> {

    public UnitBaseBuilder(final Dimensional<BaseUnit> unit, final int pNumerator, final int pDenominator) {
        base(unit, pNumerator, pDenominator);
    }

    private void base(final Dimensional<BaseUnit> unit, final int pNumerator, final int pDenominator) {
        if (unit instanceof BaseUnit) {
            append((BaseUnit) unit, pNumerator, pDenominator);
        } else if (unit instanceof TransformedUnit) {
            base(((TransformedUnit) unit).referenceUnit(), pNumerator, pDenominator);
        } else if (unit instanceof DerivedUnit) {
            extractUnitBase(((DerivedUnit) unit).definitionUnits(), pNumerator, pDenominator);
        } else if (unit instanceof LevelUnit) {
            base(((LevelUnit) unit).referenceUnit(), pNumerator, pDenominator); // on devrait garder la levelUnit ?
        } else {
            throw new UnsupportedOperationException("unsupported dimentional " + unit);
        }
    }

    private void extractUnitBase(final Factor<Unit>[] definition, final int pNumerator, final int pDenominator){

        // parcours des dimensions de la définition
        for (final Factor<Unit> factor : definition) {

            final Unit unit = factor.dim();

            if (unit instanceof BaseUnit) {
                append((BaseUnit) unit, pNumerator * factor.powerNumerator(), pDenominator * factor.powerDenominator());
            } else if (unit instanceof TransformedUnit) {
                base(((TransformedUnit) unit).referenceUnit(), pNumerator * factor.powerNumerator(),
                        pDenominator * factor.powerDenominator());
            } else if (unit instanceof LevelUnit) {
                base(((LevelUnit) unit).referenceUnit(), pNumerator, pDenominator); // on devrait garder la levelUnit ?
            } else if (unit instanceof DerivedUnit) {
                extractUnitBase(((DerivedUnit) unit).definitionUnits(), pNumerator * factor.powerNumerator(),
                        pDenominator * factor.powerDenominator());
            }
        }
    }

    @Override
    protected Comparator<Factor<BaseUnit>> comparator() {
        return (Factor<BaseUnit> o1, Factor<BaseUnit> o2)
                -> o1.dim().symbol().order(o2.dim().symbol());
    }
}
