package science.unlicense.unit.api.unit;

import science.unlicense.math.api.unit.Unit;
import science.unlicense.math.api.unit.Factor;

/**
 *
 * @author Samuel Andrés
 */
public interface DerivedUnit extends Unit {

    Factor<Unit>[] definitionUnits();

    boolean hasProperSymbol();
}
