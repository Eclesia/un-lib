package science.unlicense.unit.api.unit;

import science.unlicense.math.api.unit.BaseUnit;

/**
 * <div class="fr">Une unité dérivée que l'on peut vouloir traiter un unité de base (faire figurer dans la base etc.).
 * Contrairement aux unités fondamentales préfixées ({@link PrefixedFundamentalUnit}) qui sont des unités de base, les
 * unités dérivées de base ne sont pas des unités fondamentales. C'est par exemple le cas des unités d'angle plan et d'
 * angle solide dans le système international d'unités.</div>
 *
 * @author Samuel Andrés
 */
public interface BaseDerivedUnit extends DerivedUnit, BaseUnit {
}
