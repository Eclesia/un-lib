package science.unlicense.unit.api;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;
import science.unlicense.math.api.unit.Unit;
import science.unlicense.unit.impl.UnitUtil;
import science.unlicense.unit.impl.conversion.BaseSwitchConverter;
import science.unlicense.unit.impl.conversion.PseudoBaseSwitchConverter;
import science.unlicense.unit.impl.conversion.SingleBaseConverter;

/**
 * <div class="fr">La mesure d'un phénomène physique, composée d'une valeur et d'une unité.</div>
 *
 * <div class="fr">Une mesure peut être convertie dans une autre unité du même système d'unités
 * ou d'un système d'unités différent de celui de l'unité de définition.</div>
 *
 * @author Samuel Andrés
 */
public final class Measure extends CObject {

    private final double value;
    private final Unit unit;

    public Measure(final double value, final Unit unit) {
        this.value = value;
        this.unit = unit;
    }

    /**
     * <div class="fr">Valeur dans laquelle est réalisée la mesure (ou valeur de référence).</div>
     * @return
     */
    public double getValue() {
        return this.value;
    }

    /**
     * <div class="fr">Unité dans laquelle est réalisée la mesure (ou unité de référence).</div>
     * @return
     */
    public Unit getUnit() {
        return this.unit;
    }

    @Override
    public Chars toChars() {
        return Float64.encode(value).concat(unit.symbol());
    }

    /**
     * <div class="fr">Une nouvelle mesure construite à partir de la conversion de la mesure courante dans l'unité en
     * paramètre.</div>
     * @param target <span class="fr">unité de référence de la nouvelle mesure</span>
     * @return
     */
    public Measure to(final Unit target) {
        final Unit source = getUnit();
        final UnitConverter converter;
        if (UnitUtil.getBase(source).sameBase(UnitUtil.getBase(target))) {
            converter = new SingleBaseConverter(source, target);
        } else {
            throw new IllegalStateException();
        }

        final double value = converter.convert(getValue());
        return new Measure(value, target);
    }

    /**
     * <div class="fr">Une nouvelle mesure construite à partir de la conversion de la mesure courante dans l'unité en
     * paramètre.</div>
     * @param target <span class="fr">unité de référence de la nouvelle mesure</span>
     * @param mapper
     * @return
     */
    public Measure to(final Unit target, final UnitMapper mapper) {
        final Unit source = getUnit();
        final UnitConverter converter;
        if (mapper.isBaseSource()) {
            converter = new BaseSwitchConverter(source, target, mapper);
        } else {
            converter = new PseudoBaseSwitchConverter(source, target, mapper);
        }

        final double value = converter.convert(getValue());
        return new Measure(value, target);
    }

}
