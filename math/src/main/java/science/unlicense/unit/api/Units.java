
package science.unlicense.unit.api;

import science.unlicense.math.api.transform.ConcatenateTransform;
import science.unlicense.math.api.transform.Transform1;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.api.unit.Base;
import science.unlicense.math.api.unit.BaseUnit;
import science.unlicense.math.api.unit.Unit;
import science.unlicense.unit.impl.Conversions;
import science.unlicense.unit.impl.DefaultUnitConverter;
import science.unlicense.unit.impl.UnitUtil;
import science.unlicense.unit.impl.conversion.PseudoBaseSwitchConverter;

/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel
 */
public class Units {

    private Units(){}

    /**
     * <div class="fr">Obtention d'un convertisseur de l'unité de définition de la mesure vers l'unité en paramètre.
     * </div>
     * @param source
     * @param target
     * @return
     */
    public static UnitConverter converter(final Unit source, final Unit target) {
        final Affine1 toBase = Conversions.paramsToBase(source);
        final Affine1 fromBase = Conversions.paramsFromBase(target);
        final Transform1 transform = (Transform1) ConcatenateTransform.create(toBase, fromBase);
        return new DefaultUnitConverter(source, target, transform);
    }

    /**
     * <div class="fr">Obtention d'un convertisseur de l'unité de définition de la mesure vers l'unité en paramètre.
     * </div>
     * @param source
     * @param target
     * @param mapper
     * @return
     */
    public static UnitConverter converter(final Unit source, final Unit target, final UnitMapper mapper) {
        if (mapper.isBaseSource()) {
            final Affine1 toBase = Conversions.paramsToBase(source);
            final Base<BaseUnit> thisBase = UnitUtil.getBase(source);
            final Affine1 paramsInOtherBase = Conversions.switchBaseParams(thisBase, mapper);
            final Affine1 fromBase = Conversions.paramsFromBase(target);
            final Affine1 transform = (Affine1) ConcatenateTransform.create(toBase, paramsInOtherBase, fromBase);
            return new DefaultUnitConverter(source, target, transform);
        } else {
            final Affine1 toBase = Conversions.paramsToPseudoBase(source, mapper.source());
            final Base<Unit> thisBase = PseudoBaseSwitchConverter.getPseudoBase(source, mapper.source());
            final Affine1 paramsInOtherBase = Conversions.switchPseudoBaseParams(thisBase, mapper);
            final Affine1 fromBase = Conversions.paramsFromBase(target);
            final Affine1 transform = (Affine1) ConcatenateTransform.create(toBase, paramsInOtherBase, fromBase);
            return new DefaultUnitConverter(source, target, transform);
        }
    }
}
