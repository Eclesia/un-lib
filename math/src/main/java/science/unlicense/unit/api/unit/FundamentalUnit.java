package science.unlicense.unit.api.unit;

import science.unlicense.math.api.unit.BaseUnit;
import science.unlicense.math.api.unit.Dimension;

/**
 * <div class="fr">Dans un système d'unités donné, une unité fondamentale est la définition d'une quantité élémentaire
 * d'une dimension de base qui ne peut être réduite à la combinaison d'autres unités. Lorsqu'elle peut être appliquée à
 * plusieurs dimensions, ces dernières doivent être mutuellement homogènes, c'est à dire reposer sur la même base
 * dimensionnelle.</div>
 *
 * @author Samuel Andrés
 */
public interface FundamentalUnit extends BaseUnit {

    /**
     * Array of BaseDimension
     * @return
     */
    @Override
    Dimension[] dimension();
}
