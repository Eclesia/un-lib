package science.unlicense.unit.api;

import java.util.Map;
import java.util.Set;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.api.unit.BaseUnit;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public interface UnitMapper {

    /**
     * <div class="fr">Indique, à partir d'une unité source, l'unité cible et la transformation à appliquer à une
     * valeur exprimée dans l'unité source pour l'exprimer dans l'unité cible.</div>
     * @param u1 <span class="fr">unité source</span>
     * @return
     */
    Map.Entry<Affine1, Unit> transformation(Unit u1);

    /**
     * <div class="fr">Ensemble des unités sources.</div>
     * @return
     */
    Set<Unit> source();

    /**
     * <div class="fr">Indique si toutes les unités sources sont des unités de base.</div>
     * @return
     */
    default boolean isBaseSource() {
        for (final Unit u : source()) {
            if (!(u instanceof BaseUnit)) {
                return false;
            }
        }
        return true;
    }
}
