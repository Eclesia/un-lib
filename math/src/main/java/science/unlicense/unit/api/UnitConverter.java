package science.unlicense.unit.api;

import science.unlicense.math.api.transform.Transform1;
import science.unlicense.math.api.unit.Unit;

/**
 *
 * @author Samuel Andrés
 */
public interface UnitConverter {

    Unit getSource();

    Unit getTarget();

    Transform1 getTransform();

    double convert(double value);
}
