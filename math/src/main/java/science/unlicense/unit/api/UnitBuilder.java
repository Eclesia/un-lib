
package science.unlicense.unit.api;

import science.unlicense.math.api.unit.Prefix;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.math.api.unit.BaseDimension;
import science.unlicense.math.api.unit.Dimension;
import science.unlicense.unit.api.unit.BaseDerivedUnit;
import science.unlicense.unit.api.unit.DerivedUnit;
import science.unlicense.unit.api.unit.FundamentalUnit;
import science.unlicense.unit.api.unit.TransformedUnit;
import science.unlicense.math.api.unit.Unit;
import science.unlicense.unit.impl.base.DefaultFactor;
import science.unlicense.unit.impl.unit.DefaultBaseDerivedUnit;
import science.unlicense.unit.impl.unit.DefaultDerivedUnit;
import science.unlicense.unit.impl.unit.DefaultFundamentalTransformedUnit;
import science.unlicense.unit.impl.unit.DefaultFundamentalUnit;
import science.unlicense.unit.impl.unit.DefaultTransformedUnit;

/**
 *
 * @author Johann Sorel
 */
public class UnitBuilder {

    private static final Chars[] CHARS_EMPTY = new Chars[0];

    private final Sequence names = new ArraySequence();
    private final Sequence symbols = new ArraySequence();
    private final Sequence factors = new ArraySequence();

    public UnitBuilder addName(final Chars name) {
        names.add(name);
        return this;
    }

    public UnitBuilder addSymbol(final int symbolCodePoint) {
        symbols.add(new Chars(new int[]{symbolCodePoint}));
        return this;
    }

    public UnitBuilder addSymbol(final Chars symbol) {
        symbols.add(symbol);
        return this;
    }

    public UnitBuilder addFactor(final Unit unit) {
        return addFactor(unit, 1);
    }

    public UnitBuilder addFactor(final Unit unit, final int power) {
        return addFactor(unit, power, 1);
    }

    public UnitBuilder addFactor(final Unit unit, final int pNumerator, final int pDenominator) {
        this.factors.add(new DefaultFactor(unit, pNumerator, pDenominator));
        return this;
    }

    private Chars getName() {
        return names.isEmpty() ? Chars.EMPTY : (Chars) names.get(0);
    }

    private Chars[] getAliasNames() {
        int size = names.getSize();
        if (size < 2) {
            return CHARS_EMPTY;
        } else {
            final Chars[] array = new Chars[size-1];
            for (int i=0;i<array.length;i++) {
                array[i] = (Chars) names.get(i+1);
            }
            return array;
        }
    }

    private Chars getSymbol() {
        return symbols.isEmpty() ? Chars.EMPTY : (Chars) symbols.get(0);
    }

    private Chars[] getAliasSymbols() {
        int size = symbols.getSize();
        if (size < 2) {
            return CHARS_EMPTY;
        } else {
            final Chars[] array = new Chars[size-1];
            for (int i=0;i<array.length;i++) {
                array[i] = (Chars) symbols.get(i+1);
            }
            return array;
        }
    }

    private Factor[] getFactors() {
        return (Factor[]) factors.toArray(Factor.class);
    }

    public FundamentalUnit buildFundamental(BaseDimension... dimensions) {
        return new DefaultFundamentalUnit(
                getName(),
                getAliasNames(),
                getSymbol(),
                getAliasSymbols(),
                dimensions);
    }

    public TransformedUnit buildTransformed(final Unit definitionUnit, final Affine1 transformation) {
        return new DefaultTransformedUnit(
                getName(),
                getAliasNames(),
                getSymbol(),
                getAliasSymbols(),
                transformation,
                definitionUnit);
    }

    public DerivedUnit buildDerived(final Dimension... dimensions) {
        return new DefaultDerivedUnit(
                getName(),
                getAliasNames(),
                getSymbol(),
                getAliasSymbols(),
                dimensions,
                getFactors());
    }

    public BaseDerivedUnit buildBaseDerived(final Dimension... dimensions) {
        return new DefaultBaseDerivedUnit(
                getName(),
                getAliasNames(),
                getSymbol(),
                getAliasSymbols(),
                dimensions,
                getFactors());
    }

    public TransformedUnit buildPrefixed(final Prefix prefix, final Unit referenceUnit) {
        return buildPrefixed(prefix, referenceUnit, false);
    }

    public TransformedUnit buildPrefixed(final Prefix prefix, final Unit referenceUnit, boolean fundamental) {
        Chars name = getName();
        Chars symbol = getSymbol();
        if (name.isEmpty()) {
            name = prefix.getPrefix().concat(referenceUnit.name());
        }
        if (symbol.isEmpty()) {
            symbol = prefix.getPrefix().concat(referenceUnit.symbol());
        }

        if (fundamental) {
            return new DefaultFundamentalTransformedUnit(
                name,
                getAliasNames(),
                symbol,
                getAliasSymbols(),
                prefix.getTransformation(),
                referenceUnit);
        } else {
            return new DefaultTransformedUnit(
                name,
                getAliasNames(),
                symbol,
                getAliasSymbols(),
                prefix.getTransformation(),
                referenceUnit);
        }
    }
}
