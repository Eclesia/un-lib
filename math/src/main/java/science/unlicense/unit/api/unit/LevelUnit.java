package science.unlicense.unit.api.unit;

import science.unlicense.math.impl.unit.AbstractUnit;
import science.unlicense.math.api.unit.Unit;
import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.Dimension;
import science.unlicense.unit.api.level.Level;

/**
 * <div class="fr">Unité de niveau, telle que le Bel par exemple, ou le ratio. L'unité de référence d'une unité de
 * niveau est l'unité de la mesure de référence du niveau.</div>
 *
 * @author Samuel Andrés
 */
public class LevelUnit extends AbstractUnit {

    private final Level level;

    public LevelUnit(final Chars name, final Chars[] alternativeNames, final Chars symbol,
            final Chars[] alternativeSymbols, final Level level, final Dimension... dimensions)
    {
        super(name, symbol, alternativeNames, alternativeSymbols, dimensions);
        this.level = level;
    }

    /**
     * <div class="fr">Le niveau permet de transformer une valeur exprimer dans l'unité de niveau en valeur exprimée
     * dans l'unité de référence et vice-versa.</div>
     * @return
     */
    public Level level() {
        return level;
    }

    public Unit referenceUnit() {
        return level.referenceMeasure().getUnit();
    }

}
