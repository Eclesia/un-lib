package science.unlicense.unit.api.unit;

import science.unlicense.math.api.unit.Unit;
import science.unlicense.math.impl.Affine1;

/**
 * <div class="fr">
 * Unité de même dimension qu'une autre unité et pour laquelle la valeur d'une mesure peut être déduite de la valeur
 * d'une mesure dans l'unité de référence (et réciproquement).
 * </div>
 *
 * @author Samuel Andrés
 */
public interface TransformedUnit extends Unit {

    /**
     * <div class="fr">
     * L'unité de définition n'est pas forcément une unité de base. Par exemple, on peut définir le gramme comme un
     * millième du kilogramme (unité de base), mais également comme mille milligrammes (unité elle-même dérivée).
     * </div>
     * @return
     */
    Unit referenceUnit();

    /**
     * <div class="fr">
     * Transformation à appliquer pour exprimer une quantité exprimée en unité courante en quantité exprimée en
     * unité de référence.
     * </div>
     * @return
     */
    Affine1 transformation();

}
