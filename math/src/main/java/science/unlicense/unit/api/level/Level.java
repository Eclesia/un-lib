package science.unlicense.unit.api.level;

import science.unlicense.unit.api.Measure;

/**
 *
 * @author Samuel Andrés
 */
public interface Level {

    Measure referenceMeasure();

    double toLevel(double value);

    double toReferenceUnit(double value);
}
