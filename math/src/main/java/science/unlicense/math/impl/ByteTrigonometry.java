
package science.unlicense.math.impl;

/**
 * Base trigonometry operands using only byte array types.
 * This can be used as reference algorithms to emulate trigo operations when
 * the cpu does not handle them.
 *
 * @author Johann Sorel
 */
public final class ByteTrigonometry {

    private ByteTrigonometry(){}

    // ...etc... TODO ... find algos

}
