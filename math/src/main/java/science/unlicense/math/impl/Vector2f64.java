
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.api.AbstractVectorRW;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class Vector2f64 extends AbstractVectorRW {

    public double x;
    public double y;

    public Vector2f64() {
    }

    public Vector2f64(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public NumberType getNumericType() {
        return Float64.TYPE;
    }

    @Override
    public int getSampleCount() {
        return 2;
    }

    @Override
    public double get(int indice) {
        switch (indice) {
            case 0 : return x;
            case 1 : return y;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public Number getNumber(int indice) {
        switch (indice) {
            case 0 : return new Float64(x);
            case 1 : return new Float64(y);
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        throw new InvalidIndexException();
    }

    @Override
    public double getW() {
        throw new InvalidIndexException();
    }

    @Override
    public VectorRW getXY() {
        return new Vector2f64(x, y);
    }

    @Override
    public VectorRW getXYZ() {
        throw new InvalidIndexException();
    }

    @Override
    public VectorRW getXYZW() {
        throw new InvalidIndexException();
    }

    @Override
    public boolean isAll(double value) {
        return x==value && y==value;
    }

    @Override
    public boolean isValid() {
        return !( Double.isInfinite(x) || Double.isNaN(x)
               || Double.isInfinite(y) || Double.isNaN(y) );
    }

    @Override
    public double[] toDouble() {
        return new double[]{x,y};
    }

    @Override
    public void toDouble(double[] buffer, int offset) {
        buffer[offset+0] = x;
        buffer[offset+1] = y;
    }

    @Override
    public float[] toFloat() {
        return new float[]{(float) x,(float) y};
    }

    @Override
    public void toFloat(float[] buffer, int offset) {
        buffer[0] = (float) x;
        buffer[1] = (float) y;
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        buffer[0] = new Float64(x);
        buffer[1] = new Float64(y);
    }

    @Override
    public Vector2f64 copy() {
        return new Vector2f64(x, y);
    }

    @Override
    public VectorRW create(int size) {
        return VectorNf64.createDouble(size);
    }

    @Override
    public void setX(double x) {
        this.x = x;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public void setZ(double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setW(double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXY(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void setXYZ(double x, double y, double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXYZW(double x, double y, double z, double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setAll(double v) {
        x = v;
        y = v;
    }

    @Override
    public void set(int indice, double value) {
        switch (indice) {
            case 0 : x = value; break;
            case 1 : y = value; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(int indice, Arithmetic value) {
        double v = ((Number) value).toDouble();
        switch (indice) {
            case 0 : x = v; break;
            case 1 : y = v; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(Tuple toCopy) {
        x = toCopy.get(0);
        y = toCopy.get(1);
    }

    @Override
    public void set(double[] values) {
        x = values[0];
        y = values[1];
    }

    @Override
    public void set(float[] values) {
        x = values[0];
        y = values[1];
    }

    @Override
    public boolean equals(Tuple obj, double tolerance) {
        if (obj == null) {
            return false;
        }
        if (obj.getSampleCount() != 2) {
            return false;
        }

        return Float64.equals(x, obj.get(0), tolerance)
            && Float64.equals(y, obj.get(1), tolerance);
    }

    @Override
    public VectorRW localAdd(Tuple other) {
        if (other.getSampleCount() != 2) {
            throw new InvalidArgumentException("Both vectors must have same length.");
        }
        this.x += other.get(0);
        this.y += other.get(1);
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y) {
        this.x += x;
        this.y += y;
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y, double z) {
        throw new InvalidIndexException();
    }

    @Override
    public VectorRW localAdd(double x, double y, double z, double w) {
        throw new InvalidIndexException();
    }

    @Override
    public VectorRW localSubtract(Tuple other) {
        if (other.getSampleCount() != 2) {
            throw new InvalidArgumentException("Both vectors must have same length.");
        }
        this.x -= other.get(0);
        this.y -= other.get(1);
        return this;
    }

    @Override
    public VectorRW localCross(Tuple other) {
        throw new InvalidArgumentException("vector and v2 size must be 3.");
    }

    @Override
    public VectorRW localDivide(Tuple other) {
        if (other.getSampleCount() != 2) {
            throw new InvalidArgumentException("Both vectors must have same length.");
        }
        this.x /= other.get(0);
        this.y /= other.get(1);
        return this;
    }

    @Override
    public VectorRW localLerp(Tuple other, double ratio) {
        if (other.getSampleCount() != 2) {
            throw new InvalidArgumentException("Both vectors must have same length.");
        }
        x = Maths.lerp(x, other.get(0), ratio);
        y = Maths.lerp(y, other.get(1), ratio);
        return this;
    }

    @Override
    public VectorRW localMultiply(Tuple other) {
        if (other.getSampleCount() != 2) {
            throw new InvalidArgumentException("Both vectors must have same length.");
        }
        this.x *= other.get(0);
        this.y *= other.get(1);
        return this;
    }

    @Override
    public VectorRW localNegate() {
        this.x = -x;
        this.y = -y;
        return this;
    }

    @Override
    public VectorRW localNormalize() {
        return localScale(1.0 / length());
    }

    @Override
    public VectorRW localScale(double scale) {
        this.x *= scale;
        this.y *= scale;
        return this;
    }

    @Override
    public VectorRW subtract(Tuple other, VectorRW buffer){
        if (buffer == null) {
            return new Vector2f64(x - other.get(0), y - other.get(1) );
        } else {
            buffer.setXY(x - other.get(0), y - other.get(1));
            return buffer;
        }
    }

    @Override
    public Vector2f64 reflect(Tuple normal) {
        double d = 2 * dot(normal);
        return new Vector2f64(
            x - normal.get(0)*d,
            y - normal.get(1)*d);
    }
}
