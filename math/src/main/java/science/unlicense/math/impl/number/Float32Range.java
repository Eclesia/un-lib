
package science.unlicense.math.impl.number;

import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Number;

/**
 *
 * @author Johann Sorel
 */
public class Float32Range extends AbstractNumberRange {

    protected final float min;
    protected final float max;

    public Float32Range(float min, float max, boolean minIncluded, boolean maxIncluded) {
        super(minIncluded, maxIncluded);
        this.min = min;
        this.max = max;
    }

    public Number getMinimum() {
        return new Float32(min);
    }

    public Number getMaximum() {
        return new Float32(max);
    }

}
