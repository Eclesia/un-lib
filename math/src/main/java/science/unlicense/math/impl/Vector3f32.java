
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.api.AbstractVectorRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class Vector3f32 extends AbstractVectorRW {

    public float x;
    public float y;
    public float z;

    public Vector3f32() {
    }

    public Vector3f32(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public NumberType getNumericType() {
        return Float32.TYPE;
    }

    @Override
    public int getSampleCount() {
        return 3;
    }

    @Override
    public double get(int indice) throws InvalidIndexException {
        switch (indice) {
            case 0 : return x;
            case 1 : return y;
            case 2 : return z;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public Number getNumber(int indice) throws InvalidIndexException {
        switch (indice) {
            case 0 : return new Float32(x);
            case 1 : return new Float32(y);
            case 2 : return new Float32(z);
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        return z;
    }

    @Override
    public double getW() {
        throw new InvalidIndexException();
    }

    @Override
    public Vector2f64 getXY() {
        return new Vector2f64(x, y);
    }

    @Override
    public Vector3f32 getXYZ() {
        return new Vector3f32(x, y, z);
    }

    @Override
    public VectorRW getXYZW() {
        throw new InvalidIndexException();
    }

    @Override
    public boolean isAll(double value) {
        return x==value && y==value && z==value;
    }

    @Override
    public boolean isValid() {
        return !( Float.isInfinite(x) || Float.isNaN(x)
               || Float.isInfinite(y) || Float.isNaN(y)
               || Float.isInfinite(z) || Float.isNaN(z) );
    }

    @Override
    public double length() {
        return Math.sqrt(x*x + y*y + z*z);
    }

    @Override
    public double lengthSquare() {
        return x*x + y*y + z*z;
    }

    @Override
    public Vector3f32 negate() {
        return new Vector3f32(-x,-y,-z);
    }

    @Override
    public double dot(Tuple other) {
        if (other instanceof Vector3f32) {
            final Vector3f32 v = (Vector3f32) other;
            return x*v.x + y*v.y + z*v.z;
        } else {
            return x*other.get(0) + y*other.get(1) + z*other.get(2);
        }
    }

    @Override
    public Vector4f64 extend(double value) {
        return new Vector4f64(x, y, z, value);
    }

    @Override
    public Vector3f32 reflect(Tuple normal) {
        double d = 2 * dot(normal);
        return new Vector3f32(
            (float) (x - normal.get(0)*d),
            (float) (y - normal.get(1)*d),
            (float) (z - normal.get(2)*d));
    }

    @Override
    public double[] toDouble() {
        return new double[]{x,y,z};
    }

    @Override
    public void toDouble(double[] buffer, int offset) {
        buffer[offset+0] = x;
        buffer[offset+1] = y;
        buffer[offset+2] = z;
    }

    @Override
    public float[] toFloat() {
        return new float[]{(float) x,(float) y,(float) z};
    }

    @Override
    public void toFloat(float[] buffer, int offset) {
        buffer[0] = (float) x;
        buffer[1] = (float) y;
        buffer[2] = (float) z;
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        buffer[0] = new Float32(x);
        buffer[1] = new Float32(y);
        buffer[2] = new Float32(z);
    }

    @Override
    public Vector3f32 copy() {
        return new Vector3f32(x, y, z);
    }

    @Override
    public VectorRW create(int size) {
        return VectorNf32.createFloat(size);
    }

    @Override
    public void setX(double x) {
        this.x = (float) x;
    }

    @Override
    public void setY(double y) {
        this.y = (float) y;
    }

    @Override
    public void setZ(double z) {
        this.z = (float) z;
    }

    @Override
    public void setW(double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXY(double x, double y) {
        this.x = (float) x;
        this.y = (float) y;
    }

    @Override
    public void setXYZ(double x, double y, double z) {
        this.x = (float) x;
        this.y = (float) y;
        this.z = (float) z;
    }

    @Override
    public void setXYZW(double x, double y, double z, double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setAll(double v) {
        x = (float) v;
        y = (float) v;
        z = (float) v;
    }

    @Override
    public void set(int indice, double value) {
        switch (indice) {
            case 0 : x = (float) value; break;
            case 1 : y = (float) value; break;
            case 2 : z = (float) value; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(int indice, Arithmetic value) {
        float v = ((Number) value).toFloat();
        switch (indice) {
            case 0 : x = v; break;
            case 1 : y = v; break;
            case 2 : z = v; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(Tuple toCopy) {
        x = (float) toCopy.get(0);
        y = (float) toCopy.get(1);
        z = (float) toCopy.get(2);
    }

    @Override
    public void set(double[] values) {
        x = (float) values[0];
        y = (float) values[1];
        z = (float) values[2];
    }

    @Override
    public void set(float[] values) {
        x = values[0];
        y = values[1];
        z = values[2];
    }

    @Override
    public VectorRW localAdd(Tuple other) {
        if (other instanceof Vector3f32) {
            final Vector3f32 o = (Vector3f32) other;
            this.x += o.x;
            this.y += o.y;
            this.z += o.z;

        } else {
            this.x += other.get(0);
            this.y += other.get(1);
            this.z += other.get(2);
        }
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y) {
        this.x += x;
        this.y += y;
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y, double z) {
        this.x += x;
        this.y += y;
        this.z += z;
        return this;
    }

    @Override
    public VectorRW localMultiply(Tuple other) {

        if (other instanceof Vector3f32) {
            final Vector3f32 o = (Vector3f32) other;
            x *= o.x;
            y *= o.y;
            z *= o.z;
        } else {
            x *= other.get(0);
            y *= other.get(1);
            z *= other.get(2);
        }
        return this;
    }

    @Override
    public VectorRW localScale(double scale) {
        x *= scale;
        y *= scale;
        z *= scale;
        return this;
    }

    @Override
    public VectorRW localNormalize() {
        final double s = 1.0 / Math.sqrt(x*x + y*y + z*z);
        x *= s;
        y *= s;
        z *= s;
        return this;
    }

    @Override
    public VectorRW localNegate() {
        x = -x;
        y = -y;
        z = -z;
        return this;
    }

    @Override
    public VectorRW scale(double scale, VectorRW buffer) {
        if (buffer == null) buffer = new Vector3f32();
        if (buffer instanceof Vector3f32) {
            final Vector3f32 o = (Vector3f32) buffer;
            o.x = (float) (x*scale);
            o.y = (float) (y*scale);
            o.z = (float) (z*scale);
        } else {
            buffer.set(0, x*scale);
            buffer.set(1, y*scale);
            buffer.set(2, z*scale);
        }
        return buffer;
    }

    @Override
    public VectorRW subtract(Tuple other, VectorRW buffer) {
        if (buffer == null) buffer = new Vector3f32();

        double rx,ry,rz;

        if (other instanceof Vector3f32) {
            final Vector3f32 o = (Vector3f32) other;
            rx = x-o.x;
            ry = y-o.y;
            rz = z-o.z;
        } else {
            rx = x-other.get(0);
            ry = y-other.get(1);
            rz = z-other.get(2);
        }

        if (buffer instanceof Vector3f32) {
            final Vector3f32 o = (Vector3f32) buffer;
            o.x = (float) rx;
            o.y = (float) ry;
            o.z = (float) rz;
        } else {
            buffer.set(0, rx);
            buffer.set(1, ry);
            buffer.set(2, rz);
        }
        return buffer;
    }

    @Override
    public VectorRW multiply(Tuple other, VectorRW buffer) {
        if (buffer == null) buffer = new Vector3f32();

        double rx,ry,rz;

        if (other instanceof Vector3f32) {
            final Vector3f32 o = (Vector3f32) other;
            rx = x*o.x;
            ry = y*o.y;
            rz = z*o.z;
        } else {
            rx = x*other.get(0);
            ry = y*other.get(1);
            rz = z*other.get(2);
        }

        if (buffer instanceof Vector3f32) {
            final Vector3f32 o = (Vector3f32) buffer;
            o.x = (float) rx;
            o.y = (float) ry;
            o.z = (float) rz;
        } else {
            buffer.set(0, rx);
            buffer.set(1, ry);
            buffer.set(2, rz);
        }
        return buffer;
    }

}
