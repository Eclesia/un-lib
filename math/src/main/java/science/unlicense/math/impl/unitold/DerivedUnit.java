
package science.unlicense.math.impl.unitold;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Affine1;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.unitold.Unit;

/**
 *
 * @author Johann Sorel
 */
public class DerivedUnit extends Unit {

    private final BaseUnit baseUnit;
    private final Transform toBase;
    private final Transform fromBase;

    public DerivedUnit(BaseUnit baseUnit, double scale, Chars name) {
        this(baseUnit, new Affine1(scale, 0),
            name, name, name, name);
    }

    public DerivedUnit(BaseUnit baseUnit, Affine toBase,
            Chars unitName, Chars physicName, Chars symbol, Chars expression) {
        this(baseUnit,toBase, toBase.invert(),
            unitName, physicName, symbol, expression);
    }

    public DerivedUnit(BaseUnit baseUnit, Transform toBase, Transform fromBase,
            Chars unitName, Chars physicName, Chars symbol, Chars expression) {
        super(unitName, physicName, symbol, expression);
        this.baseUnit = baseUnit;
        this.toBase = toBase;
        this.fromBase = fromBase;
    }

    /**
     * {@inheritDoc }
     */
    public BaseUnit getBaseUnit() {
        return baseUnit;
    }

    /**
     * {@inheritDoc }
     */
    public Transform getToBase() {
        return toBase;
    }

    /**
     * {@inheritDoc }
     */
    public Transform getFromBase() {
        return fromBase;
    }

}
