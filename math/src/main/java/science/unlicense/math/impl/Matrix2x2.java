
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public class Matrix2x2 extends AbstractMatrix {

    //package private for fast access by other classes
    double m00,m01;
    double m10,m11;

    public Matrix2x2() {
        super(2, 2);
    }

    public Matrix2x2(Matrix m) {
        super(2, 2);
        m00 = m.get(0, 0);
        m01 = m.get(0, 1);
        m10 = m.get(1, 0);
        m11 = m.get(1, 1);
    }

    public Matrix2x2(double[][] values) {
        super(2, 2);
        if (values[0].length != 2 || values.length != 2) {
            throw new InvalidArgumentException("Size must be 2x2");
        }
        m00 = values[0][0];
        m01 = values[0][1];
        m10 = values[1][0];
        m11 = values[1][1];
    }

    public Matrix2x2(double m00, double m01,
                     double m10, double m11) {
        super(2, 2);
        this.m00 = m00;
        this.m01 = m01;
        this.m10 = m10;
        this.m11 = m11;
    }

    @Override
    public double get(int row, int col) {
        switch(row){
            case 0 : switch(col){case 0:return m00;case 1:return m01;}
            case 1 : switch(col){case 0:return m10;case 1:return m11;}
        }
        throw new InvalidArgumentException("Invalid row/col index "+row+":"+col);
    }

    @Override
    public void set(int row, int col, double value) {
        switch(row){
            case 0 : switch(col){case 0:m00=value;break;case 1:m01=value;break;} return;
            case 1 : switch(col){case 0:m10=value;break;case 1:m11=value;break;} return;
        }
        throw new InvalidArgumentException("Invalid row/col index "+row+":"+col);
    }

    @Override
    public void set(final Matrix toCopy){
        if (toCopy instanceof Matrix2x2){
            Matrix2x2 o = (Matrix2x2) toCopy;
            m00 = o.m00;m01 = o.m01;
            m10 = o.m10;m11 = o.m11;
        } else {
            super.set(toCopy);
        }
    }

    public void setAll(double value) {
        m00 = value;
        m01 = value;
        m10 = value;
        m11 = value;
    }

    @Override
    public Matrix2x2 setToIdentity() {
        m00=1.0; m01=0.0;
        m10=0.0; m11=1.0;
        return this;
    }

    @Override
    public boolean isIdentity() {
        return m00==1.0 && m01==0.0
            && m10==0.0 && m11==1.0;
    }

    @Override
    public boolean isFinite() {
        return !(
                Double.isNaN(m00) || Double.isInfinite(m00) ||
                Double.isNaN(m01) || Double.isInfinite(m01) ||
                Double.isNaN(m10) || Double.isInfinite(m10) ||
                Double.isNaN(m11) || Double.isInfinite(m11)
                );
    }

    @Override
    public MatrixRW add(Matrix other, MatrixRW buffer) {
        if (buffer==null) buffer = new Matrix2x2();

        double b00,b01;
        double b10,b11;

        if (other instanceof Matrix2x2){
            //usual case
            Matrix2x2 o = (Matrix2x2) other;
            b00 = this.m00 + o.m00;
            b01 = this.m01 + o.m01;
            b10 = this.m10 + o.m10;
            b11 = this.m11 + o.m11;
        } else {
            b00 = this.m00 + other.get(0,0);
            b01 = this.m01 + other.get(0,1);
            b10 = this.m10 + other.get(1,0);
            b11 = this.m11 + other.get(1,1);
        }

        if (buffer instanceof Matrix2x2) {
            Matrix2x2 o = (Matrix2x2) buffer;
            o.m00 = b00;o.m01 = b01;
            o.m10 = b10;o.m11 = b11;
        } else {
            buffer.set(0,0,b00); buffer.set(0,1,b01);
            buffer.set(1,0,b10); buffer.set(1,1,b11);
        }
        return buffer;

    }

    @Override
    public MatrixRW scale(double scale, MatrixRW buffer) {
        if (buffer == null) buffer = new Matrix2x2();

        if (buffer instanceof Matrix2x2) {
            final Matrix2x2 o = (Matrix2x2) buffer;
            o.m00 = m00 * scale;
            o.m10 = m10 * scale;
            o.m01 = m01 * scale;
            o.m11 = m11 * scale;
        } else {
            buffer.set(0,0, m00 * scale);
            buffer.set(1,0, m10 * scale);
            buffer.set(0,1, m01 * scale);
            buffer.set(1,1, m11 * scale);
        }
        return buffer;
    }

    @Override
    public MatrixRW invert() {
        return invert(new Matrix2x2());
    }

    @Override
    public MatrixRW multiply(Matrix other) {
        return multiply(other, new Matrix2x2());
    }

    @Override
    public MatrixRW multiply(Matrix other, MatrixRW buffer) {
        if (buffer==null) buffer = new Matrix2x2();

        double b00,b01;
        double b10,b11;

        if (other instanceof Matrix2x2){
            //usual case
            Matrix2x2 o = (Matrix2x2) other;
            b00 = this.m00 * o.m00 + this.m01 * o.m10;
            b01 = this.m00 * o.m01 + this.m01 * o.m11;
            b10 = this.m10 * o.m00 + this.m11 * o.m10;
            b11 = this.m10 * o.m01 + this.m11 * o.m11;
        } else {
            b00 = this.m00 * other.get(0,0) + this.m01 * other.get(1,0);
            b01 = this.m00 * other.get(0,1) + this.m01 * other.get(1,1);
            b10 = this.m10 * other.get(0,0) + this.m11 * other.get(1,0);
            b11 = this.m10 * other.get(0,1) + this.m11 * other.get(1,1);
        }

        if (buffer instanceof Matrix2x2) {
            Matrix2x2 o = (Matrix2x2) buffer;
            o.m00 = b00;o.m01 = b01;
            o.m10 = b10;o.m11 = b11;
        } else {
            buffer.set(0,0,b00); buffer.set(0,1,b01);
            buffer.set(1,0,b10); buffer.set(1,1,b11);
        }
        return buffer;
    }

    @Override
    public Matrix2x2 localScale(double scale) {
        m00 *= scale;m01 *= scale;
        m10 *= scale;m11 *= scale;
        return this;
    }

    @Override
    public Matrix2x2 localScale(double[] scale) {
        m00 *= scale[0];m01 *= scale[1];
        m10 *= scale[0];m11 *= scale[1];
        return this;
    }

    @Override
    public Matrix2x2 localMultiply(Matrix other) {
        if (other instanceof Matrix2x2){
            //usual case
            Matrix2x2 o = (Matrix2x2) other;
            double b00 = this.m00 * o.m00 + this.m01 * o.m10;
            double b01 = this.m00 * o.m01 + this.m01 * o.m11;
            double b10 = this.m10 * o.m00 + this.m11 * o.m10;
            double b11 = this.m10 * o.m01 + this.m11 * o.m11;
            m00 = b00;m01 = b01;
            m10 = b10;m11 = b11;
            return this;
        } else {
            return (Matrix2x2) super.localMultiply(other);
        }
    }

    @Override
    protected void transform1(double[] source, int sourceOffset, double[] dest, int destOffset) {
        double d0 = m00*source[sourceOffset] + m01*source[sourceOffset+1];
        double d1 = m10*source[sourceOffset] + m11*source[sourceOffset+1];
        dest[destOffset  ] = d0;
        dest[destOffset+1] = d1;
    }

    @Override
    protected void transform1(float[] source, int sourceOffset, float[] dest, int destOffset) {
        double d0 = m00*source[sourceOffset] + m01*source[sourceOffset+1];
        double d1 = m10*source[sourceOffset] + m11*source[sourceOffset+1];
        dest[destOffset  ] = (float) d0;
        dest[destOffset+1] = (float) d1;
    }

    @Override
    public TupleRW transform(Tuple vector, TupleRW buffer) {
        if (buffer == null) buffer = new Vector2f64();

        if (vector instanceof Vector2f64 && buffer instanceof Vector2f64) {
            final Vector2f64 v = (Vector2f64) vector;
            final Vector2f64 b = (Vector2f64) buffer;
            double rx = m00*v.x + m01*v.y ;
            double ry = m10*v.x + m11*v.y;
            b.x = rx;
            b.y = ry;
            return b;
        }

        return super.transform(vector, buffer);
    }

    @Override
    public MatrixRW copy() {
        return new Matrix2x2(m00, m01, m10, m11);
    }

    /**
     * Create rotation matrix from angle.
     *
     * @param angle in radians
     * @return Matrix2
     */
    public static Matrix2x2 fromAngle(double angle) {
        final Matrix2x2 m = new Matrix2x2();
        m.set(0, 0, Math.cos(angle));
        m.set(0, 1, -Math.sin(angle));
        m.set(1, 0, Math.sin(angle));
        m.set(1, 1, Math.cos(angle));
        return m;
    }

}
