package science.unlicense.math.impl;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.math.api.Maths;

/**
 * résolution du système d'equations par la methode des moindres carrés
 *
 * @author Xavier PHILIPPEAU
 * @author Johann Sorel (UN project adaptation)
 */
public final class LeastSquare {

    private LeastSquare(){}

    /**
     * Resolution d'un systeme lineaire
     *
     * @param systemsize Nombre d'inconnues dans le systeme
     * @param system Sequence of LinearEquation, Le systeme d'equations lineaires
     * @return le vecteur solution du système
     */
    public static double[] linearFit(int systemsize, Sequence system) {

        // taille du systeme a resoudre (= nombre d'inconnues)
        final int n = systemsize;

        // On cherche a trouver X qui resoud le systeme:
        //
        // J.X = R
        //
        // on multiplie par Jt, la transposée de J:
        //
        // Jt.J.X = Jt.R
        //
        // on pose:
        //                    n
        // A[c][l] = Jt.J = Somme[ coefficient[c][k] * coefficient[l][k] ]
        //                   k=0
        //
        //                 n
        // Y[l] = Jt.R = Somme[ residual[k] * coefficient[l][k] ]
        //                k=0
        //
        // et "n", le nombre d'equation dans le systeme.
        //
        // Le systeme a resoudre devient ainsi:
        //
        // A.X = Y

        //matrix is set to identity by default, do not use the row/col constructor
        final MatrixNxN A = new MatrixNxN(new double[n][n]);

        final double Y[] = new double[n];
        for (int i=0,k=system.getSize();i<k; i++) {
            final LinearEquation equation = (LinearEquation) system.get(i);
            for (int l = 0; l < n; l++) {
                for (int c = 0; c < n; c++) {
                    A.values[l][c] += equation.coefficents[c] * equation.coefficents[l];
                }
                Y[l] += equation.residual * equation.coefficents[l];
            }
        }

        // Resolution par inversion:
        //
        // A . X = Y  ==>  X = A^-1 . Y

        A.localInvert();
        final double X[] = A.transform(Y);
        return X;
    }

    /**
     * Trouver la "meilleure ligne" au sens des moindres carrés.
     * La solution s'ecrit: Y = C[0] + C[1]*X
     *
     * @param data Sequence of double[]
     * @return double[]
     */
    public static double[] bestLine(Sequence data) {

        final Sequence system = new ArraySequence();

        for (int i=0,n=data.getSize();i<n;i++) {
            final double[] d = (double[]) data.get(i);
            final double x = d[0];
            final double y = d[1];

            // a.1 + b.X = Y
            //
            // (X,Y) sont connus (données du probleme)
            // (a,b) sont les 2 inconnues

            final LinearEquation equation = new LinearEquation(2);
            equation.coefficents = new double[]{1, x};
            equation.residual = y;
            system.add(equation);
        }

        // resolution du systeme lineaire
        return LeastSquare.linearFit(2, system);
    }

    /**
     * Trouver la "meilleure parabole" au sens des moindres carrés.
     * La solution s'ecrit: Y = C[0] + C[1]*X + C[2]*X^2
     *
     * @param data Sequence of double[]
     * @return double[]
     */
    public static double[] bestParabol(Sequence data) {

        final Sequence system = new ArraySequence();

        for (int i=0,n=data.getSize();i<n;i++) {
            final double[] d = (double[]) data.get(i);
            final double x = d[0];
            final double y = d[1];

            // a.1 + b.X + c.X^2 = Y
            //
            // (X,Y) sont connus (données du probleme)
            // (a,b,c) sont les 3 inconnues

            final LinearEquation equation = new LinearEquation(3);
            equation.coefficents = new double[]{1, x, x * x};
            equation.residual = y;

            system.add(equation);
        }

        // resolution du systeme lineaire
        return LeastSquare.linearFit(3, system);
    }

    /**
     * Trouver le "meilleure polynome de degré n" au sens des moindres carrés.
     * La solution s'ecrit: Y = C[0] + C[1]*X + C[2]*X^2 + ... + C[n]*X^n
     *
     * @param order, int polynom order
     * @param data Sequence of double[]
     * @return double[]
     */
    public static double[] bestPolynom(int order, Sequence data) {

        final Sequence system = new ArraySequence();

        for (int i=0,k=data.getSize();i<k;i++) {
            final double[] d = (double[]) data.get(i);
            final double x = d[0];
            final double y = d[1];

            // a.1 + b.X + c.X^2 + ... + z.X^ordre = Y
            //
            // (X,Y) sont connus (données du probleme)
            // (a,b,c,...z) sont les inconnues

            final LinearEquation equation = new LinearEquation(order + 1);

            equation.residual = y;
            for (int n = 0; n <= order; n++) {
                equation.coefficents[n] = Math.pow(x, n);
            }

            system.add(equation);
        }

        // resolution du systeme lineaire
        return LeastSquare.linearFit(order + 1, system);
    }

    /**
     * Trouver la "meilleure ellipse" au sens des moindres carrés,
     * en utilisant les formules de mathworld.wolfram.com.
     *
     * @param data Sequence of double[]
     * @return double[]{xcentre, ycentre, angle, xhalflength, yhalflength}
     */
    public static double[] bestEllipsoid(Sequence data) {

        final Sequence system = new ArraySequence();

        for (int i=0,k=data.getSize();i<k;i++) {
            final double[] d = (double[]) data.get(i);
            final double x = d[0];
            final double y = d[1];

            // a.x^2 + 2b.xy + c.y^2 + 2d.x + 2f.y + g = 0
            //
            // (X,Y) sont connus (données du probleme)
            // on pose g = -1
            // (a,b,c,d,f) sont les 5 inconnues

            final LinearEquation equation = new LinearEquation(5);
            equation.coefficents = new double[]{x * x, x * y, y * y, x, y};
            equation.residual = 1.0;

            system.add(equation);
        }

        // resolution du systeme lineaire
        final double[] C = LeastSquare.linearFit(5, system);

        // evaluation des 5 inconnues
        final double a = C[0];
        final double b = C[1] / 2;
        final double c = C[2];
        final double d = C[3] / 2;
        final double f = C[4] / 2;
        final double g = -1;

        // calculs des caracteristiques de l'ellipse:
        //
        // d'après http://mathworld.wolfram.com/Ellipse.html
        //
        // formules (15), (16), ..., (23)

        // verifie que l'ellipse est valide
        final double delta = a * (c * g - f * f) - b * (b * g - d * f) + d * (b * f - d * c);
        final double J = a * c - b * b;
        final double I = a + c;
        if (delta == 0) {
            return null;
        }
        if (J <= 0) {
            return null;
        }
        if ((delta / I) >= 0) {
            return null;
        }

        /* calcul du centre de l'ellipse */
        final double xcentre = (c * d - b * f) / (b * b - a * c);
        final double ycentre = (a * f - b * d) / (b * b - a * c);

        /* angle de rotation (en radian) */
        double angle = 0.5 * Math.atan(2 * b / (c - a));

        /* longueurs des 2 demi-axes */
        final double num = 2 * (a * f * f + c * d * d + g * b * b - 2 * b * d * f - a * c * g);
        final double sq = Math.sqrt(1 + 4 * b * b / ((a - c) * (a - c)));

        final double denom1 = (b * b - a * c) * ((c - a) * sq - (c + a));
        double xhalflength = Math.sqrt(num / denom1);

        final double denom2 = (b * b - a * c) * ((a - c) * sq - (c + a));
        double yhalflength = Math.sqrt(num / denom2);

        /* retourne toujours un angle positif  */
        if (angle < 0) {
            angle += Maths.PI / 2;
            // echange les longueurs des demi-axes
            final double tmp = xhalflength;
            xhalflength = yhalflength;
            yhalflength = tmp;
        }

        return new double[]{xcentre, ycentre, angle, xhalflength, yhalflength};
    }

}
