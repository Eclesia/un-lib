
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.math.api.AbstractVectorRW;
import science.unlicense.math.api.ScalarRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class Scalara extends AbstractVectorRW implements ScalarRW {

    public Arithmetic x;

    public Scalara() {
    }

    public Scalara(Arithmetic x) {
        this.x = x;
    }

    @Override
    public ArithmeticType getNumericType() {
        return x.getType();
    }

    @Override
    public int getSampleCount() {
        return 1;
    }

    @Override
    public double get(int indice) {
        return arithmeticAsNumber(x).toDouble();
    }

    @Override
    public Arithmetic getNumber(int indice) {
        switch (indice) {
            case 0 : return x;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public double getX() {
        return arithmeticAsNumber(x).toDouble();
    }

    @Override
    public double getY() {
        throw new InvalidIndexException();
    }

    @Override
    public double getZ() {
        throw new InvalidIndexException();
    }

    @Override
    public double getW() {
        throw new InvalidIndexException();
    }

    @Override
    public VectorRW getXY() {
        return new Scalara(x);
    }

    @Override
    public VectorRW getXYZ() {
        throw new InvalidIndexException();
    }

    @Override
    public VectorRW getXYZW() {
        throw new InvalidIndexException();
    }

    @Override
    public boolean isAll(double value) {
        return arithmeticAsNumber(x).toDouble() == value;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public double[] toDouble() {
        return new double[]{arithmeticAsNumber(x).toDouble()};
    }

    @Override
    public void toDouble(double[] buffer, int offset) {
        buffer[offset+0] = arithmeticAsNumber(x).toDouble();
    }

    @Override
    public float[] toFloat() {
        return new float[]{arithmeticAsNumber(x).toFloat()};
    }

    @Override
    public void toFloat(float[] buffer, int offset) {
        buffer[0] = arithmeticAsNumber(x).toFloat();
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        buffer[0] = x;
    }

    @Override
    public Scalara copy() {
        return new Scalara(x);
    }

    @Override
    public VectorRW create(int size) {
        return VectorNi64.createLong(size);
    }

    @Override
    public void setX(double x) {
        throw new UnimplementedException();
    }

    @Override
    public void setY(double y) {
        throw new InvalidIndexException();
    }

    @Override
    public void setZ(double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setW(double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXY(double x, double y) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXYZ(double x, double y, double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXYZW(double x, double y, double z, double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setAll(double v) {
        throw new UnimplementedException();
    }

    @Override
    public void set(int indice, double value) {
        throw new UnimplementedException();
    }

    @Override
    public void set(int indice, Arithmetic value) {
        throw new UnimplementedException();
    }

    @Override
    public void set(Tuple toCopy) {
        throw new UnimplementedException();
    }

    @Override
    public void set(double[] values) {
        throw new UnimplementedException();
    }

    @Override
    public void set(float[] values) {
        throw new UnimplementedException();
    }

    @Override
    public boolean equals(Tuple obj, double tolerance) {
        if (obj == null) {
            return false;
        }
        if (obj.getSampleCount() != 1) {
            return false;
        }

        return obj.getNumber(0).equals(x);
    }

    // scalar methods //////////////////////////////////////////////////////////

    @Override
    public void set(double value) {
        x = new Float64(value);
    }

    @Override
    public void set(float value) {
        x = new Float32(value);
    }

    @Override
    public void set(Arithmetic value) {
        x = value;
    }

    @Override
    public double get() {
        return arithmeticAsNumber(x).toDouble();
    }

    @Override
    public Arithmetic getNumber() {
        return x;
    }

    @Override
    public boolean getBoolean() {
        return arithmeticAsNumber(x).toInteger() != 0;
    }

    @Override
    public byte getByte() {
        return (byte) arithmeticAsNumber(x).toInteger();
    }

    @Override
    public short getShort() {
        return (short) arithmeticAsNumber(x).toInteger();
    }

    @Override
    public int getInt() {
        return arithmeticAsNumber(x).toInteger();
    }

    @Override
    public long getLong() {
        return arithmeticAsNumber(x).toLong();
    }

    @Override
    public float getFloat() {
        return arithmeticAsNumber(x).toFloat();
    }

    @Override
    public double getDouble() {
        return arithmeticAsNumber(x).toDouble();
    }

}
