
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.api.AbstractVectorRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class Vector2f32 extends AbstractVectorRW {

    public float x;
    public float y;

    public Vector2f32() {
    }

    public Vector2f32(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public NumberType getNumericType() {
        return Float32.TYPE;
    }

    @Override
    public int getSampleCount() {
        return 2;
    }

    @Override
    public double get(int indice) {
        switch (indice) {
            case 0 : return x;
            case 1 : return y;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public Number getNumber(int indice) {
        switch (indice) {
            case 0 : return new Float32(x);
            case 1 : return new Float32(y);
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        throw new InvalidIndexException();
    }

    @Override
    public double getW() {
        throw new InvalidIndexException();
    }

    @Override
    public VectorRW getXY() {
        return new Vector2f32(x, y);
    }

    @Override
    public VectorRW getXYZ() {
        throw new InvalidIndexException();
    }

    @Override
    public VectorRW getXYZW() {
        throw new InvalidIndexException();
    }

    @Override
    public boolean isAll(double value) {
        return x==value && y==value;
    }

    @Override
    public boolean isValid() {
        return !( Float.isInfinite(x) || Float.isNaN(x)
               || Float.isInfinite(y) || Float.isNaN(y) );
    }

    @Override
    public double[] toDouble() {
        return new double[]{x,y};
    }

    @Override
    public void toDouble(double[] buffer, int offset) {
        buffer[offset+0] = x;
        buffer[offset+1] = y;
    }

    @Override
    public float[] toFloat() {
        return new float[]{x, y};
    }

    @Override
    public void toFloat(float[] buffer, int offset) {
        buffer[0] = x;
        buffer[1] = y;
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        buffer[0] = new Float32(x);
        buffer[1] = new Float32(y);
    }

    @Override
    public Vector2f32 copy() {
        return new Vector2f32(x, y);
    }

    @Override
    public VectorRW create(int size) {
        return VectorNf32.createFloat(size);
    }

    @Override
    public void setX(double x) {
        this.x = (float) x;
    }

    @Override
    public void setY(double y) {
        this.y = (float) y;
    }

    @Override
    public void setZ(double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setW(double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXY(double x, double y) {
        this.x = (float) x;
        this.y = (float) y;
    }

    @Override
    public void setXYZ(double x, double y, double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXYZW(double x, double y, double z, double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setAll(double v) {
        x = (float) v;
        y = (float) v;
    }

    @Override
    public void set(int indice, double value) {
        switch (indice) {
            case 0 : x = (float) value; break;
            case 1 : y = (float) value; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(int indice, Arithmetic value) {
        float v = ((Number) value).toFloat();
        switch (indice) {
            case 0 : x = v; break;
            case 1 : y = v; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(Tuple toCopy) {
        x = (float) toCopy.get(0);
        y = (float) toCopy.get(1);
    }

    @Override
    public void set(double[] values) {
        x = (float) values[0];
        y = (float) values[1];
    }

    @Override
    public void set(float[] values) {
        x = values[0];
        y = values[1];
    }

    @Override
    public boolean equals(Tuple obj, double tolerance) {
        if (obj == null) {
            return false;
        }
        if (obj.getSampleCount() != 2) {
            return false;
        }

        return Float64.equals(x, obj.get(0), tolerance)
            && Float64.equals(y, obj.get(1), tolerance);
    }

}
