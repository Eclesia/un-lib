
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.api.AbstractVectorRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class Vector4f32 extends AbstractVectorRW {

    public float x;
    public float y;
    public float z;
    public float w;

    public Vector4f32() {
    }

    public Vector4f32(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    @Override
    public NumberType getNumericType() {
        return Float32.TYPE;
    }

    @Override
    public int getSampleCount() {
        return 4;
    }

    @Override
    public double get(int indice) throws InvalidIndexException {
        switch (indice) {
            case 0 : return x;
            case 1 : return y;
            case 2 : return z;
            case 3 : return w;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public Number getNumber(int indice) throws InvalidIndexException {
        switch (indice) {
            case 0 : return new Float32(x);
            case 1 : return new Float32(y);
            case 2 : return new Float32(z);
            case 3 : return new Float32(w);
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        return z;
    }

    @Override
    public double getW() {
        return w;
    }

    @Override
    public Vector2f64 getXY() {
        return new Vector2f64(x, y);
    }

    @Override
    public Vector3f64 getXYZ() {
        return new Vector3f64(x, y, z);
    }

    @Override
    public Vector4f32 getXYZW() {
        return new Vector4f32(x, y, z, w);
    }

    @Override
    public boolean isAll(double value) {
        return x==value && y==value && z==value && w==value;
    }

    @Override
    public boolean isValid() {
        return !( Float.isInfinite(x) || Float.isNaN(x)
               || Float.isInfinite(y) || Float.isNaN(y)
               || Float.isInfinite(z) || Float.isNaN(z)
               || Float.isInfinite(w) || Float.isNaN(w));
    }

    @Override
    public double[] toDouble() {
        return new double[]{x,y,z,w};
    }

    @Override
    public void toDouble(double[] buffer, int offset) {
        buffer[offset+0] = x;
        buffer[offset+1] = y;
        buffer[offset+2] = z;
        buffer[offset+3] = w;
    }

    @Override
    public float[] toFloat() {
        return new float[]{(float) x,(float) y,(float) z, (float) w};
    }

    @Override
    public void toFloat(float[] buffer, int offset) {
        buffer[0] = (float) x;
        buffer[1] = (float) y;
        buffer[2] = (float) z;
        buffer[3] = (float) w;
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        buffer[0] = new Float32(x);
        buffer[1] = new Float32(y);
        buffer[2] = new Float32(z);
        buffer[3] = new Float32(w);
    }

    @Override
    public Vector4f32 copy() {
        return new Vector4f32(x, y, z, w);
    }

    @Override
    public VectorRW create(int size) {
        return VectorNf32.createFloat(size);
    }

    @Override
    public void setX(double x) {
        this.x = (float) x;
    }

    @Override
    public void setY(double y) {
        this.y = (float) y;
    }

    @Override
    public void setZ(double z) {
        this.z = (float) z;
    }

    @Override
    public void setW(double w) {
        this.w = (float) w;
    }

    @Override
    public void setXY(double x, double y) {
        this.x = (float) x;
        this.y = (float) y;
    }

    @Override
    public void setXYZ(double x, double y, double z) {
        this.x = (float) x;
        this.y = (float) y;
        this.z = (float) z;
    }

    @Override
    public void setXYZW(double x, double y, double z, double w) {
        this.x = (float) x;
        this.y = (float) y;
        this.z = (float) z;
        this.w = (float) w;
    }

    @Override
    public void setAll(double v) {
        x = (float) v;
        y = (float) v;
        z = (float) v;
        w = (float) v;
    }

    @Override
    public void set(int indice, double value) {
        switch (indice) {
            case 0 : x = (float) value; break;
            case 1 : y = (float) value; break;
            case 2 : z = (float) value; break;
            case 3 : w = (float) value; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(int indice, Arithmetic value) {
        float v = ((Number) value).toFloat();
        switch (indice) {
            case 0 : x = v; break;
            case 1 : y = v; break;
            case 2 : z = v; break;
            case 3 : w = v; break;
            default : throw new InvalidIndexException();
        }
    }
    @Override
    public void set(Tuple toCopy) {
        x = (float) toCopy.get(0);
        y = (float) toCopy.get(1);
        z = (float) toCopy.get(2);
        w = (float) toCopy.get(3);
    }

    @Override
    public void set(double[] values) {
        x = (float) values[0];
        y = (float) values[1];
        z = (float) values[2];
        w = (float) values[3];
    }

    @Override
    public void set(float[] values) {
        x = values[0];
        y = values[1];
        z = values[2];
        w = values[3];
    }

    @Override
    public VectorRW localAdd(Tuple other) {
        if (other instanceof Vector4f32) {
            final Vector4f32 o = (Vector4f32) other;
            this.x += o.x;
            this.y += o.y;
            this.z += o.z;
            this.w += o.w;

        } else {
            this.x += other.get(0);
            this.y += other.get(1);
            this.z += other.get(2);
            this.w += other.get(3);
        }
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y) {
        this.x += x;
        this.y += y;
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y, double z) {
        this.x += x;
        this.y += y;
        this.z += z;
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y, double z, double w) {
        this.x += x;
        this.y += y;
        this.z += z;
        this.w += w;
        return this;
    }

    @Override
    public VectorRW scale(double scale, VectorRW buffer) {
        if (buffer == null) buffer = new Vector4f32();
        if (buffer instanceof Vector4f32) {
            final Vector4f32 o = (Vector4f32) buffer;
            o.x = (float) (x*scale);
            o.y = (float) (y*scale);
            o.z = (float) (z*scale);
            o.w = (float) (w*scale);
        } else {
            buffer.set(0, x*scale);
            buffer.set(1, y*scale);
            buffer.set(2, z*scale);
            buffer.set(3, w*scale);
        }
        return buffer;
    }

    @Override
    public VectorRW multiply(Tuple other, VectorRW buffer) {
        if (buffer == null) buffer = new Vector4f32();

        double rx,ry,rz,rw;

        if (other instanceof Vector4f32) {
            final Vector4f32 o = (Vector4f32) other;
            rx = x*o.x;
            ry = y*o.y;
            rz = z*o.z;
            rw = w*o.w;
        } else {
            rx = x*other.get(0);
            ry = y*other.get(1);
            rz = z*other.get(2);
            rw = w*other.get(3);
        }

        if (buffer instanceof Vector4f32) {
            final Vector4f32 o = (Vector4f32) buffer;
            o.x = (float) rx;
            o.y = (float) ry;
            o.z = (float) rz;
            o.w = (float) rw;
        } else {
            buffer.set(0, rx);
            buffer.set(1, ry);
            buffer.set(2, rz);
            buffer.set(3, rw);
        }
        return buffer;
    }

}
