
package science.unlicense.math.impl.unitold;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;

/**
 * A physical quantity.
 * Only 7 base quantity exist (http://fr.wikipedia.org/wiki/Unité_de_base_du_système_international)
 * An additional one called UNSET has been added for convinience.
 *
 * @author Johann Sorel
 */
public final class PhysicalQuantity extends CObject{

    public static final PhysicalQuantity LENGTH             = new PhysicalQuantity(new Chars("metre"),              new Chars("L"), new Chars("L"));
    public static final PhysicalQuantity MASS               = new PhysicalQuantity(new Chars("mass"),               new Chars("m"), new Chars("M"));
    public static final PhysicalQuantity TIME               = new PhysicalQuantity(new Chars("time"),               new Chars("t"), new Chars("T"));
    public static final PhysicalQuantity ELECTRIC_CURRENT   = new PhysicalQuantity(new Chars("electric current"),   new Chars("I"), new Chars("I"));
    public static final PhysicalQuantity TEMPERATURE        = new PhysicalQuantity(new Chars("temperature"),        new Chars("T"), new Chars("Θ"));
    public static final PhysicalQuantity MATERIAL_QUANTITY  = new PhysicalQuantity(new Chars("amount of substance"),new Chars("n"), new Chars("N"));
    public static final PhysicalQuantity LUMINOUS_INTENSITY = new PhysicalQuantity(new Chars("luminous intensity"), new Chars("IV"),new Chars("J"));
    public static final PhysicalQuantity UNSET              = new PhysicalQuantity(new Chars("unset"),              new Chars("?"), new Chars("?"));


    private final Chars name;
    private final Chars usualSymbol;
    private final Chars dimensionSymbol;

    private PhysicalQuantity(Chars name, Chars usualSymbol, Chars dimensionSymbol) {
        this.name = name;
        this.usualSymbol = usualSymbol;
        this.dimensionSymbol = dimensionSymbol;
    }

    /**
     * Quantity name.
     * @return Chars, never null
     */
    public Chars getName() {
        return name;
    }

    /**
     * Quantity common symbol.
     * @return Chars, never null
     */
    public Chars getUsualSymbol() {
        return usualSymbol;
    }

    /**
     * Quantity dimension symbol.
     * @return Chars, never null
     */
    public Chars getDimensionSymbol() {
        return dimensionSymbol;
    }

    public Chars toChars() {
        return name.concat('(').concat(dimensionSymbol).concat(')');
    }

}
