
package science.unlicense.math.impl;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractAffine extends SimplifiedTransform implements Affine {

    public AbstractAffine(int size) {
        super(UndefinedSystem.create(size));
    }

    @Override
    public double[] getCol(int col) {
        final double[] array = new double[getInputDimensions()];
        for (int i=0;i<array.length;i++) array[i] = get(i,col);
        return array;
    }

    @Override
    public double[] getRow(int row) {
        final double[] array = new double[getInputDimensions()+1];
        for (int i=0;i<array.length;i++) array[i] = get(row, i);
        return array;
    }

    @Override
    public boolean isIdentity() {
        final int size = getInputDimensions();

        for ( int x=0; x<=size; x++) {
            for ( int y=0; y<size; y++) {
                if (x==y){
                    if ( get(y,x) != 1.0 ) return false;
                } else {
                    if ( get(y,x) != 0.0 ) return false;
                }
            }
        }
        return true;
    }

    @Override
    public AffineRW multiply(Affine affine) {
        return multiply(affine, null);
    }

    @Override
    public AffineRW multiply(Affine affine, AffineRW buffer) {
        if (buffer == null) buffer = AffineN.create(this.getInputDimensions());
        final MatrixRW res = this.toMatrix().multiply(affine.toMatrix());
        buffer.fromMatrix(res);
        return buffer;
    }

    @Override
    public Affine invert() {
        final AffineRW affine = AffineN.create(this);
        affine.localInvert();
        return affine;
    }

    @Override
    public AffineRW invert(AffineRW buffer) {
        if (buffer==null) buffer = AffineN.create(getInputDimensions());
        buffer.fromMatrix(toMatrix().invert());
        return buffer;
    }

    @Override
    public Chars toChars() {
        return CObjects.toChars(toMatrix().getMatrix(0, getInputDimensions()-1, 0, getInputDimensions()));
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Affine)) return false;

        final Affine aff = (Affine) obj;
        final int dim = aff.getInputDimensions();
        if (aff.getInputDimensions()!= getInputDimensions()) {
            return false;
        }

        for (int x=0;x<=dim;x++) {
            for (int y=0;y<dim;y++) {
                if (get(y, x) != aff.get(y, x)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public int getHash() {
        return getInputDimensions();
    }

    public abstract static class RW extends AbstractAffine implements AffineRW {

        public RW(int size) {
            super(size);
        }

        public void set(final Affine toCopy) {
            final int dim = toCopy.getInputDimensions();
            for (int x=0;x<=dim;x++) {
                for (int y=0;y<dim;y++) {
                    set(y, x, toCopy.get(y, x));
                }
            }
        }

        public void setRow(int row, double[] values) {
            if (values.length != getInputDimensions()+1) throw new InvalidArgumentException("Unvalid array size");
            for (int x=0;x<values.length;x++) {
                set(row, x, values[x]);
            }

        }

        public void setCol(int col, double[] values) {
            if (values.length != getInputDimensions()) throw new InvalidArgumentException("Unvalid array size");
            for (int y=0;y<values.length;y++) {
                set(y, col, values[y]);
            }
        }

        /**
         * Set affine values to identity transform.
         *
         * @return this affine
         */
        public AffineRW setToIdentity() {
            final int dim = getInputDimensions();
            for (int x=0;x<=dim;x++) {
                for (int y=0;y<dim;y++) {
                    set(y, x, x==y ? 1:0);
                }
            }
            return this;
        }

        @Override
        public AffineRW localScale(double scale) {
            fromMatrix(this.toMatrix().localScale(scale));
            return this;
        }

        @Override
        public AffineRW localScale(double[] tuple) {
            fromMatrix(this.toMatrix().localScale(tuple));
            return this;
        }

        public void localMultiply(Affine affine) {
            fromMatrix(this.toMatrix().multiply(affine.toMatrix()));
        }

        @Override
        public AffineRW localInvert() {
            fromMatrix(toMatrix().invert());
            return this;
        }

    }

}
