
package science.unlicense.math.impl;

/**
 * Lanczos interpolation.
 * https://en.wikipedia.org/wiki/Lanczos_resampling
 *
 * TODO : clean interpolation api
 *
 * @author Johann Sorel
 */
public final class Lanczos {

    public static void main(String[] args) {

        double[] serie = new double[]{0,1,2,3,4,5,6,7,8,9};

        System.out.println(interpolate(serie, 3.5, 3));


    }

    private Lanczos(){}

    public static double interpolate(double[] array, double index, int a) {
        double sum = 0.0;

        int low = (int) Math.floor(index) - a + 1;
        int high = (int) Math.floor(index) + a;

        for (int i=low; i <= high; i++) {
            sum += array[i] * lanczos(index-i, a);
        }
        return sum;
    }

    public static double lanczos(double x, int a) {
        if (x < 0) {
            x = -x;
        }
        if (x < a) {
            return sincNor(x) * sincNor(x / a);
        }
        return 0.0;
    }

    public static double sinc(double x) {
        if (x != 0.0) {
            return Math.sin(x) / x;
        }
        return 1.0;
    }

    public static double sincNor(double x) {
        return sinc(x * Math.PI);
    }
}
