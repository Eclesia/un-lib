
package science.unlicense.math.impl;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventManager;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.math.api.*;

/**
 * A similarity is the equivalent of a affine transform but preserving angles by avoiding
 * shearing value not rotations.
 * 3 different elements are stored.
 * - rotation matrix
 * - translation vector
 * - scale vector
 *
 * History :
 * At first the project used Matrix for 3d scenes, but this approach starts to raise problems
 * when node are moved around very often (like billboards), the mathematic inaccuracy makes the
 * objects start to distord over time.
 *
 * A good description of the problem can be found here :
 * http://www.altdevblogaday.com/2012/07/03/matrices-rotation-scale-and-drifting/
 *
 *
 * @author Johann Sorel
 */
public class SimilarityNd extends AbstractAffine implements SimilarityRW {

    //keep track of the matrix state
    private final Object lock = new Object();
    private EventManager eventManager;
    private final MatrixRW oldMatrix;

    private final int dimension;
    private final MatrixRW rotation;
    private final VectorRW scale;
    private final VectorRW translation;

    //store a view of the global matrix of size dimension+1
    //which group rotation,scale and translation
    private final Inverse inverseAff = new Inverse();
    private boolean dirty = true;
    private boolean inverseDirty = true;
    private boolean affineDirty = true;
    private final MatrixRW matrix;
    private final MatrixRW inverseMatrix;
    private final MatrixRW inverseRotation;
    private final AffineRW affine;

    public static SimilarityRW create(int dimension) {
        return new SimilarityNd(dimension);
    }

    public static SimilarityRW create(MatrixRW rotation, VectorRW scale, VectorRW translation) {
        return new SimilarityNd(rotation, scale, translation);
    }

    public static SimilarityRW create(Quaternion rotation, VectorRW scale, VectorRW translation) {
        return new SimilarityNd(rotation, scale, translation);
    }

    protected SimilarityNd(int dimension){
        super(dimension);
        this.dimension = dimension;
        this.rotation = MatrixNxN.create(dimension, dimension).setToIdentity();
        this.inverseRotation = rotation.copy();
        this.scale = VectorNf64.createDouble(dimension);
        //set scale to 1 by default
        this.scale.setAll(1d);
        this.translation = VectorNf64.createDouble(dimension);
        this.dirty = false;
        final int msize = dimension+1;
        this.matrix = MatrixNxN.create(msize, msize).setToIdentity();
        this.oldMatrix = this.matrix.copy();
        this.inverseMatrix = MatrixNxN.create(msize, msize);
        this.affine = AffineN.create(dimension);
    }

    protected SimilarityNd(MatrixRW rotation, VectorRW scale, VectorRW translation) {
        super(scale.getSampleCount());
        this.dimension = scale.getSampleCount();
        this.rotation = rotation;
        this.inverseRotation = this.rotation.copy();
        this.scale = scale;
        this.translation = translation;
        final int msize = dimension+1;
        this.matrix = MatrixNxN.create(msize, msize).setToIdentity();
        this.oldMatrix = this.matrix.copy();
        this.inverseMatrix = MatrixNxN.create(msize, msize);
        this.affine = AffineN.create(dimension);
    }

    protected SimilarityNd(Quaternion rotation, VectorRW scale, VectorRW translation) {
        super(scale.getSampleCount());
        this.dimension = scale.getSampleCount();
        this.rotation = rotation.toMatrix3();
        this.inverseRotation = this.rotation.copy();
        this.scale = scale;
        this.translation = translation;
        final int msize = dimension+1;
        this.matrix = MatrixNxN.create(msize, msize).setToIdentity();
        this.oldMatrix = this.matrix.copy();
        this.inverseMatrix = MatrixNxN.create(msize, msize);
        this.affine = AffineN.create(dimension);
    }

    /**
     * Copy values from given transform.
     * @param trs
     */
    @Override
    public void set(Similarity trs){
        if (rotation.equals(trs.getRotation()) && scale.equals(trs.getScale()) && translation.equals(trs.getTranslation())){
            //nothing changes
            return;
        }
        rotation.set(trs.getRotation());
        scale.set(trs.getScale());
        translation.set(trs.getTranslation());
        notifyChanged();
    }

    /**
     * Set transform from given matrix.
     * Matrix must be orthogonal of size dimension+1.
     * @param trs
     */
    @Override
    public void set(Matrix trs){
        Matrices.decomposeMatrix(trs, rotation, scale, translation);
        notifyChanged();
    }

    /**
     * Set transform from given matrix.
     * Matrix must be orthogonal of size dimension+1.
     * @param trs
     */
    @Override
    public void set(Affine trs){
        set(trs.toMatrix());
    }

    /**
     * Set to identity.
     * This method will send a change event if values have changed.
     */
    @Override
    public void setToIdentity(){
        boolean change = false;
        if (!rotation.isIdentity()){
            change = true;
            rotation.setToIdentity();
        }
        if (!scale.isAll(1.0)){
            change = true;
            scale.setAll(1.0);
        }
        if (!translation.isAll(0.0)){
            change = true;
            translation.setAll(0.0);
        }

        if (change) notifyChanged();
    }

    /**
     * Set this transform to given translation.
     * This will reset rotation and scale values.
     *
     * This method will send a change event if values have changed.
     */
    @Override
    public void setToTranslation(double[] trs){
        boolean change = false;
        if (!rotation.isIdentity()){
            change = true;
            rotation.setToIdentity();
        }
        if (!scale.isAll(1.0)){
            change = true;
            scale.setAll(1.0);
        }
        if (!Arrays.equals(trs, translation.toDouble())){
            change = true;
            translation.set(trs);
        }

        if (change) notifyChanged();
    }

    @Override
    public double get(int row, int col) {
        return viewMatrix().get(row, col);
    }

    /**
     * Dimension of the transform.
     * @return int
     */
    @Override
    public int getDimension() {
        return dimension;
    }

    /**
     * Get transform rotation.
     * Call notifyChanged after if you modified the values.
     *
     * @return Matrix
     */
    @Override
    public MatrixRW getRotation() {
        return rotation;
    }

    /**
     * Get transform scale.
     * Call notifyChanged after if you modified the values.
     *
     * @return Vector
     */
    @Override
    public VectorRW getScale() {
        return scale;
    }

    /**
     * Get transform translation.
     * Call notifyChanged after if you modified the values.
     *
     * @return Vector
     */
    @Override
    public VectorRW getTranslation() {
        return translation;
    }

    /**
     * Flag to indicate the transform parameters has changed.
     * This is used to recalculate the general matrix when needed.
     */
    @Override
    public void notifyChanged(){
        dirty=true;
        inverseDirty=true;
        affineDirty=true;

        if (eventManager!=null && eventManager.hasListeners()){
            //we have listeners, we need to recalculate the transform now
            eventManager.sendPropertyEvent(this, PROPERTY_MATRIX, oldMatrix.copy(), viewMatrix().copy());
        }
    }

    /**
     * Get a general matrix view of size : dimension+1
     * This matrix combine rotation, scale and translation
     *
     * [R*S, R*S, R*S, T]
     * [R*S, R*S, R*S, T]
     * [R*S, R*S, R*S, T]
     * [  0,   0,   0, 1]
     *
     * @return Matrix, never null
     */
    @Override
    public Matrix viewMatrix(){
        if (dirty){
            dirty = false;
            //update matrix
            matrix.setToIdentity();
            matrix.set(rotation);
            matrix.localScale(scale.extend(1).toDouble());
            for (int i=0;i<dimension;i++){
                matrix.set(i, dimension, translation.get(i));
            }
            oldMatrix.set(matrix);
            if (!matrix.isFinite()){
                throw new RuntimeException("Matrix is not finite :\nRotation\n"+rotation+"Scale "+scale+"\nTranslate "+translation);
            }
        }
        return matrix;
    }

    /**
     * Get a general inverse matrix view of size : dimension+1
     * DO NOT MODIFY THIS MATRIX.
     *
     * @return Matrix, never null
     */
    @Override
    public Matrix viewMatrixInverse(){
        if (dirty){
            viewMatrix();
        }
        if (inverseDirty){
            inverseDirty = false;
            rotation.invert(inverseRotation);
            matrix.invert(inverseMatrix);
        }
        return inverseMatrix;
    }

    private Affine asAffine() {
        if (dirty || affineDirty) {
            affine.fromMatrix(viewMatrix());
            affineDirty = false;
        }
        return affine;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int getInputDimensions() {
        return dimension;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int getOutputDimensions() {
        return dimension;
    }

    @Override
    public TupleRW transform(Tuple source, TupleRW out) {
        return asAffine().transform(source, out);

        //under code uses less memory but is much slower
//        if (out == null) out = DefaultVector.create(dimension);
//
//        if (out instanceof VectorRW) {
//            //scale
//            out.set(source);
//            ((VectorRW) out).localMultiply(scale);
//            //rotate
//            rotation.transform(out, out);
//            //translate
//            ((VectorRW) out).localAdd(translation);
//        } else {
//            final VectorRW array = DefaultVector.create(out);
//            //scale
//            array.set(source);
//            array.localMultiply(scale);
//            //rotate
//            rotation.transform(array, array);
//            //translate
//            array.localAdd(translation);
//            out.set(array);
//        }
//
//        return out;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void transform(double[] in, int sourceOffset, double[] out, int destOffset, int nbTuple) {
        asAffine().transform(in, sourceOffset, out, destOffset, nbTuple);

        //we could use this, but it is slower.
        //asMatrix().transform(in, sourceOffset, out, destOffset, nbTuple);

        //this is also slower
//        //scale
//        Vectors.multiplyRegular(in, out, sourceOffset, destOffset, scale.toDouble(), nbTuple);
//        //rotate
//        rotation.transform(out, destOffset, out, destOffset, nbTuple);
//        //translate
//        Vectors.addRegular(out, out, destOffset, destOffset, translation.toDouble(), nbTuple);
//
//        return out;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void transform(float[] in, int sourceOffset, float[] out, int destOffset, int nbTuple) {
        asAffine().transform(in, sourceOffset, out, destOffset, nbTuple);

        //we could use this, but it is slower.
        //asMatrix().transform(source, sourceOffset, dest, destOffset, nbTuple);

        //this is also slower
//        //scale
//        Vectors.multiplyRegular(in, out, sourceOffset, destOffset, scale.toFloat(), nbTuple);
//        //rotate
//        rotation.transform(out, destOffset, out, destOffset, nbTuple);
//        //translate
//        Vectors.addRegular(out, out, destOffset, destOffset, translation.toFloat(), nbTuple);
//
//        return out;
    }

    @Override
    protected void transform1(double[] source, int sourceOffset, double[] dest, int destOffset) {
        asAffine().transform(source, sourceOffset, dest, destOffset, 1);
    }

    @Override
    protected void transform1(float[] source, int sourceOffset, float[] dest, int destOffset) {
        asAffine().transform(source, sourceOffset, dest, destOffset, 1);
    }

    /**
     * Inverse transform a single tuple.
     *
     * @param source tuple, can not be null.
     * @param dest tuple, can be null.
     * @return destination tuple.
     */
    public TupleRW inverseTransform(Tuple source, TupleRW dest){
        return inverseAff.transform(source, dest);
    }

    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

    protected EventManager getEventManager() {
        return getEventManager(true);
    }

    protected EventManager getEventManager(boolean create) {
        synchronized (lock){
            if (eventManager==null && create) eventManager = new EventManager();
        }
        return eventManager;
    }

    public void addEventListener(Predicate predicate, EventListener listener) {
        getEventManager().addEventListener(predicate, listener);
    }

    public void removeEventListener(Predicate predicate, EventListener listener) {
        getEventManager().removeEventListener(predicate, listener);
    }

    public EventListener[] getListeners(Predicate predicate) {
        final EventManager manager = getEventManager(false);
        if (manager==null) return EventManager.EMPTY_EVENTLISTENER_ARRAY;
        return manager.getListeners(predicate);
    }

    /**
     * Inverse view of this transform.
     * The returned affine is no modifiable.
     * The returned affine reflects any change made to this NodeTransform
     *
     * @return inverse transform view
     */
    public Affine inverse() {
        return inverseAff;
    }

    @Override
    public AffineRW invert() {
        final AffineRW affine = AffineN.create(this);
        affine.localInvert();
        return affine;
    }

    @Override
    public void invert(SimilarityRW buffer) {
        buffer.set(this);
        buffer.localInvert();
    }

    public SimilarityRW localInvert() {
        final AffineRW affine = AffineN.create(this);
        affine.localInvert();
        set(affine);
        return this;
    }

    @Override
    public AffineRW multiply(Affine other) {
        final MatrixRW res = viewMatrix().multiply(other.toMatrix());
        final AffineRW trs = AffineN.create(dimension);
        trs.fromMatrix(res);
        return trs;
    }

    @Override
    public SimilarityRW multiply(Similarity other) {
        return multiply(other, (SimilarityRW) null);
    }

    @Override
    public SimilarityRW multiply(Similarity other, SimilarityRW buffer) {
        /*
        b00 = A.r00*A.s0 * B.r00*B.s0 + A.r01*A.s0 * B.r10*B.s1 + A.r02*A.s0 * B.r20*B.s2;
        b01 = A.r00*A.s0 * B.r01*B.s0 + A.r01*A.s0 * B.r11*B.s1 + A.r02*A.s0 * B.r21*B.s2;
        b02 = A.r00*A.s0 * B.r02*B.s0 + A.r01*A.s0 * B.r12*B.s1 + A.r02*A.s0 * B.r22*B.s2;
        b10 = A.r10*A.s1 * B.r00*B.s0 + A.r11*A.s1 * B.r10*B.s1 + A.r12*A.s1 * B.r20*B.s2;
        b11 = A.r10*A.s1 * B.r01*B.s0 + A.r11*A.s1 * B.r11*B.s1 + A.r12*A.s1 * B.r21*B.s2;
        b12 = A.r10*A.s1 * B.r02*B.s0 + A.r11*A.s1 * B.r12*B.s1 + A.r12*A.s1 * B.r22*B.s2;
        b20 = A.r20*A.s2 * B.r00*B.s0 + A.r21*A.s2 * B.r10*B.s1 + A.r22*A.s2 * B.r20*B.s2;
        b21 = A.r20*A.s2 * B.r01*B.s0 + A.r21*A.s2 * B.r11*B.s1 + A.r22*A.s2 * B.r21*B.s2;
        b22 = A.r20*A.s2 * B.r02*B.s0 + A.r21*A.s2 * B.r12*B.s1 + A.r22*A.s2 * B.r22*B.s2;

        b03 = A.r00*A.s0 * B.t0       + A.r01*A.s0 * B.t1       + A.r02*A.s0 * B.t2         + A.t0;
        b13 = A.r10*A.s1 * B.t0       + A.r11*A.s1 * B.t1       + A.r12*A.s1 * B.t2         + A.t1;
        b23 = A.r20*A.s2 * B.t0       + A.r21*A.s2 * B.t1       + A.r22*A.s2 * B.t2         + A.t2;
        */
        if (buffer == null) buffer = SimilarityNd.create(dimension);

        rotation.multiply(other.getRotation(), buffer.getRotation());
        scale.multiply(other.getScale(), buffer.getScale());

        final VectorRW resTrans = buffer.getTranslation();
        rotation.transform(other.getTranslation(), resTrans);
        scale.multiply(resTrans, resTrans);
        resTrans.localAdd(translation);

        buffer.notifyChanged();
        return buffer;
    }

    @Override
    public SimilarityNd localMultiply(Affine other) {
        if (other instanceof Similarity) {
            return localMultiply((Similarity) other);
        } else {
            final SimilarityNd s = new SimilarityNd(dimension);
            s.set(other);
            return localMultiply(s);
        }
    }

    @Override
    public SimilarityNd localMultiply(Similarity other) {
        /*
        b00 = A.r00*A.s0 * B.r00*B.s0 + A.r01*A.s0 * B.r10*B.s1 + A.r02*A.s0 * B.r20*B.s2;
        b01 = A.r00*A.s0 * B.r01*B.s0 + A.r01*A.s0 * B.r11*B.s1 + A.r02*A.s0 * B.r21*B.s2;
        b02 = A.r00*A.s0 * B.r02*B.s0 + A.r01*A.s0 * B.r12*B.s1 + A.r02*A.s0 * B.r22*B.s2;
        b10 = A.r10*A.s1 * B.r00*B.s0 + A.r11*A.s1 * B.r10*B.s1 + A.r12*A.s1 * B.r20*B.s2;
        b11 = A.r10*A.s1 * B.r01*B.s0 + A.r11*A.s1 * B.r11*B.s1 + A.r12*A.s1 * B.r21*B.s2;
        b12 = A.r10*A.s1 * B.r02*B.s0 + A.r11*A.s1 * B.r12*B.s1 + A.r12*A.s1 * B.r22*B.s2;
        b20 = A.r20*A.s2 * B.r00*B.s0 + A.r21*A.s2 * B.r10*B.s1 + A.r22*A.s2 * B.r20*B.s2;
        b21 = A.r20*A.s2 * B.r01*B.s0 + A.r21*A.s2 * B.r11*B.s1 + A.r22*A.s2 * B.r21*B.s2;
        b22 = A.r20*A.s2 * B.r02*B.s0 + A.r21*A.s2 * B.r12*B.s1 + A.r22*A.s2 * B.r22*B.s2;

        b03 = A.r00*A.s0 * B.t0       + A.r01*A.s0 * B.t1       + A.r02*A.s0 * B.t2         + A.t0;
        b13 = A.r10*A.s1 * B.t0       + A.r11*A.s1 * B.t1       + A.r12*A.s1 * B.t2         + A.t1;
        b23 = A.r20*A.s2 * B.t0       + A.r21*A.s2 * B.t1       + A.r22*A.s2 * B.t2         + A.t2;
        */

        final VectorRW resTrans = VectorNf64.createDouble(dimension);
        rotation.transform(other.getTranslation(), resTrans);
        scale.multiply(resTrans, resTrans);
        resTrans.localAdd(translation);

        rotation.localMultiply(other.getRotation());
        scale.localMultiply(other.getScale());

        translation.set(resTrans);
        notifyChanged();
        return this;
    }

    @Override
    public MatrixRW toMatrix() {
        return viewMatrix().copy();
    }

    @Override
    public MatrixRW toMatrix(MatrixRW buffer) {
        if (buffer==null) return toMatrix();
        buffer.set(viewMatrix());
        return buffer;
    }

    private class Inverse extends AbstractAffine {

        public Inverse() {
            super(dimension);
        }

        @Override
        public int getInputDimensions() {
            return dimension;
        }

        @Override
        public int getOutputDimensions() {
            return dimension;
        }

        @Override
        public TupleRW transform(Tuple source, TupleRW dest) {
            if (dest == null) dest = VectorNf64.createDouble(dimension);
            viewMatrixInverse();
            Vector vs = Vectors.castOrWrap(source);
            VectorRW vd = Vectors.castOrWrap(dest);
            //inverse translate
            vs.subtract(translation, vd);
            //inverse rotate
            inverseRotation.transform(vd, vd);
            //invert scale
            vd.localDivide(scale);
            return vd;
        }

        @Override
        public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
            viewMatrixInverse();
            //inverse translate
            Vectors.subtractRegular(source, dest, sourceOffset, destOffset, translation.toDouble(), nbTuple);
            //inverse rotate
            inverseRotation.transform(dest, destOffset, dest, destOffset, nbTuple);
            //invert scale
            Vectors.divideRegular(dest, dest, destOffset, destOffset, scale.toDouble(), nbTuple);
        }

        @Override
        public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
            viewMatrixInverse();
            //inverse translate
            Vectors.subtractRegular(source, dest, sourceOffset, destOffset, translation.toFloat(), nbTuple);
            //inverse rotate
            inverseRotation.transform(dest, destOffset, dest, destOffset, nbTuple);
            //invert scale
            Vectors.divideRegular(dest, dest, destOffset, destOffset, scale.toFloat(), nbTuple);
        }

        @Override
        protected void transform1(double[] source, int sourceOffset, double[] dest, int destOffset) {
            viewMatrixInverse();
            //inverse translate
            Vectors.subtractRegular(source, dest, sourceOffset, destOffset, translation.toDouble(), 1);
            //inverse rotate
            inverseRotation.transform(dest, destOffset, dest, destOffset, 1);
            //invert scale
            Vectors.divideRegular(dest, dest, destOffset, destOffset, scale.toDouble(), 1);
        }

        @Override
        protected void transform1(float[] source, int sourceOffset, float[] dest, int destOffset) {
            viewMatrixInverse();
            //inverse translate
            Vectors.subtractRegular(source, dest, sourceOffset, destOffset, translation.toFloat(), 1);
            //inverse rotate
            inverseRotation.transform(dest, destOffset, dest, destOffset, 1);
            //invert scale
            Vectors.divideRegular(dest, dest, destOffset, destOffset, scale.toFloat(), 1);
        }

        @Override
        public double get(int row, int col) {
            return viewMatrixInverse().get(row, col);
        }

        @Override
        public MatrixRW toMatrix() {
            return viewMatrixInverse().copy();
        }

        @Override
        public MatrixRW toMatrix(MatrixRW buffer) {
            if (buffer==null) {
                return viewMatrixInverse().copy();
            } else {
                buffer.set(viewMatrixInverse());
                return buffer;
            }
        }
    }
}
