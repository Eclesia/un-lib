
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Int64;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.api.AbstractVectorRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class Vector2i64 extends AbstractVectorRW {

    public long x;
    public long y;

    public Vector2i64() {
    }

    public Vector2i64(long x, long y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public NumberType getNumericType() {
        return Int64.TYPE;
    }

    @Override
    public int getSampleCount() {
        return 2;
    }

    @Override
    public double get(int indice) {
        switch (indice) {
            case 0 : return x;
            case 1 : return y;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public Number getNumber(int indice) {
        switch (indice) {
            case 0 : return new Int64(x);
            case 1 : return new Int64(y);
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        throw new InvalidIndexException();
    }

    @Override
    public double getW() {
        throw new InvalidIndexException();
    }

    @Override
    public Vector2i64 getXY() {
        return new Vector2i64(x, y);
    }

    @Override
    public VectorRW getXYZ() {
        throw new InvalidIndexException();
    }

    @Override
    public VectorRW getXYZW() {
        throw new InvalidIndexException();
    }

    @Override
    public boolean isAll(double value) {
        return x==value && y==value;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public double[] toDouble() {
        return new double[]{x,y};
    }

    @Override
    public void toDouble(double[] buffer, int offset) {
        buffer[offset+0] = x;
        buffer[offset+1] = y;
    }

    @Override
    public float[] toFloat() {
        return new float[]{(float) x,(float) y};
    }

    @Override
    public void toFloat(float[] buffer, int offset) {
        buffer[0] = (float) x;
        buffer[1] = (float) y;
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        buffer[0] = new Int64(x);
        buffer[1] = new Int64(y);
    }

    @Override
    public Vector2i64 copy() {
        return new Vector2i64(x, y);
    }

    @Override
    public VectorRW create(int size) {
        return VectorNi64.createLong(size);
    }

    @Override
    public void setX(double x) {
        this.x = (long) x;
    }

    @Override
    public void setY(double y) {
        this.y = (long) y;
    }

    @Override
    public void setZ(double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setW(double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXY(double x, double y) {
        this.x = (long) x;
        this.y = (long) y;
    }

    @Override
    public void setXYZ(double x, double y, double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXYZW(double x, double y, double z, double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setAll(double v) {
        x = (long) v;
        y = (long) v;
    }

    @Override
    public void set(int indice, double value) {
        switch (indice) {
            case 0 : x = (long) value; break;
            case 1 : y = (long) value; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(int indice, Arithmetic value) {
        int v = ((Number) value).toInteger();
        switch (indice) {
            case 0 : x = v; break;
            case 1 : y = v; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(Tuple toCopy) {
        x = (long) toCopy.get(0);
        y = (long) toCopy.get(1);
    }

    @Override
    public void set(double[] values) {
        x = (long) values[0];
        y = (long) values[1];
    }

    @Override
    public void set(float[] values) {
        x = (long) values[0];
        y = (long) values[1];
    }

    @Override
    public boolean equals(Tuple obj, double tolerance) {
        if (obj == null) {
            return false;
        }
        if (obj.getSampleCount() != 2) {
            return false;
        }

        final double diffx = x - obj.get(0);
        if (diffx < -tolerance || diffx > tolerance) {
            return false;
        }

        final double diffy = y - obj.get(1);
        if (diffy < -tolerance || diffy > tolerance) {
            return false;
        }

        return true;
    }

}
