
package science.unlicense.math.impl.number;

import science.unlicense.common.api.number.Int64;
import science.unlicense.common.api.number.Number;

/**
 *
 * @author Johann Sorel
 */
public class Int64Range extends AbstractNumberRange {

    protected final long min;
    protected final long max;

    public Int64Range(long min, long max, boolean minIncluded, boolean maxIncluded) {
        super(minIncluded, maxIncluded);
        this.min = min;
        this.max = max;
    }

    public Number getMinimum() {
        return new Int64(min);
    }

    public Number getMaximum() {
        return new Int64(max);
    }

}
