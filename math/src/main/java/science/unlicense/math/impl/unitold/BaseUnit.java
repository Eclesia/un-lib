
package science.unlicense.math.impl.unitold;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.api.transform.Transforms;
import science.unlicense.math.api.unitold.Unit;


/**
 * International base unit.
 * There is one base unit for each physical quantity.
 *
 * @author Johann Sorel
 */
public final class BaseUnit extends Unit{

    public static final BaseUnit METER      = new BaseUnit(new Chars("metre"),      new Chars("m"),  PhysicalQuantity.LENGTH);
    public static final BaseUnit KILOGRAM   = new BaseUnit(new Chars("kilogram"),   new Chars("kg"), PhysicalQuantity.MASS);
    public static final BaseUnit SECOND     = new BaseUnit(new Chars("second"),     new Chars("s"),  PhysicalQuantity.TIME);
    public static final BaseUnit AMPERE     = new BaseUnit(new Chars("ampere"),     new Chars("A"),  PhysicalQuantity.ELECTRIC_CURRENT);
    public static final BaseUnit KELVIN     = new BaseUnit(new Chars("kelvin"),     new Chars("K"),  PhysicalQuantity.TEMPERATURE);
    public static final BaseUnit MOLE       = new BaseUnit(new Chars("mole"),       new Chars("mol"),PhysicalQuantity.MATERIAL_QUANTITY);
    public static final BaseUnit CANDELA    = new BaseUnit(new Chars("candela"),    new Chars("cd"), PhysicalQuantity.LUMINOUS_INTENSITY);
    public static final BaseUnit UNDEFINED  = new BaseUnit(new Chars("undefined"),  new Chars("?"),  PhysicalQuantity.UNSET);


    private final PhysicalQuantity quantity;

    private BaseUnit(Chars unitName, Chars symbol, PhysicalQuantity quantity) {
        super(unitName, unitName, symbol, symbol);
        this.quantity = quantity;
    }

    /**
     * PhysicalQuantity of this unit.
     * @return PhysicalQuantity, never null
     */
    public PhysicalQuantity getQuantity() {
        return quantity;
    }

    /**
     * {@inheritDoc }
     */
    public Unit getBaseUnit() {
        return this;
    }

    /**
     * Identity transform.
     *
     * @return Identity transform
     */
    public Transform getToBase() {
        return Transforms.IDENTITY_1D;
    }

    /**
     * Identity transform.
     *
     * @return Identity transform
     */
    public Transform getFromBase() {
        return Transforms.IDENTITY_1D;
    }

}
