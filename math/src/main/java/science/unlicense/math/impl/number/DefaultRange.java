
package science.unlicense.math.impl.number;

import science.unlicense.common.api.number.Number;

/**
 *
 * @author Johann Sorel
 */
public class DefaultRange extends AbstractNumberRange {

    protected final Number min;
    protected final Number max;

    public DefaultRange(Number min, Number max, boolean minIncluded, boolean maxIncluded) {
        super(minIncluded, maxIncluded);
        this.min = min;
        this.max = max;
    }

    public Number getMinimum() {
        return min;
    }

    public Number getMaximum() {
        return max;
    }

}
