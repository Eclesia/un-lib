package science.unlicense.math.impl;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.number.Int64;
import science.unlicense.math.api.Maths;

/**
 * Magnitude is an helper class used in LargeInteger and LargeDecimal.
 *
 * It uses long arrays to make basic computations on very long integers. The
 * least significant long is at index 0 and the most significant long is at
 * index magnitude.length-1.
 *
 * @author Bertrand COTE
 */
public final class Magnitude {

    private static final long  LOW_INT_MASK = 0x00000000ffffffffL;
    private static final long HALF_OVERFLOW = 0x100000000L;
    private static final int INT_BITS_LENGTH = 32;
    private static final int LONG_BITS_LENGTH = 64;
    private static final int LONG_BYTE_LENGTH = 8;

    private static final boolean IS_SIGNED_LONG = true;
    private static final boolean IS_UNSIGNED_LONG = false;

    private static final boolean ADDITION = true;
    private static final boolean SUBTRACTION = false;

    private Magnitude(){}

    /**
     * convert mag to its String representation.
     * @param mag
     * @return
     */
    public static Chars toChars( long[] mag ) {

        CharBuffer cb = new CharBuffer();
        cb.append("{ 1, ");
        for ( long n : mag ) {
            cb.append("0x");
            cb.append(Int64.encodeHexa(n));
            cb.append("L, ");
        }
        cb.append("},");

        return cb.toChars();
    }

    /**
     * Performs a comparison between two unsigned long values.
     *
     * @param a an unsigned long value.
     * @param b an unsigned long value.
     * @return 1 if a>b, 0 if a==b and -1 if a<b.
     */
    private static int unsignedLongCompare( long a, long b ) {
        if ( a == b ) {
            return 0;
        }
        if ( (a^b) < 0 ) { // differents signs
            if ( b < 0 ) {
                return -1;
            } else {
                return 1;
            }
        } else { // same signs
            return a>b ? 1 : -1;
        }
    }

    // =========================================================================
    // =========================================================================
    // =========================================================================

    /**
     * Remove all consecutive 0 elements in the end of the given array.
     * (If all elements are zeros, it returns the array { 0x0L })
     *
     * @param mag
     * @return
     */
    public static long[] reductMagnitude( long[] mag ) {
        if ( mag == null || mag.length == 0 ) {
            return new long[1];
        }

        if ( mag[mag.length-1] != 0 ) { // The last element is not zero.
            return mag;
        }

        int i;
        for ( i=mag.length-1; i>0; i-- ) {
            if ( mag[i] != 0 ) break;
        }
        return Arrays.copy(mag, 0, i+1);
    }

    /**
     * Computes n such that n is the higher integer such that mag can be
     * written as magResult*2.pow(n).(n is the count of 0 bits beginning mag)<br>
     * (Used in simplifications in LargeDecimal)
     *
     * @param mag
     * @return n the count of 0 bits beginning mag.
     */
    public static int simplifyMagnitude( long[] mag ) {

        if ( mag == null || mag.length == 0 ) {
            return 0;
        }

        int factor = 0;
        int i = 0;
        for ( ; i<mag.length; i++ ) {
            if ( mag[i]==0 ) {
                factor += LONG_BITS_LENGTH;
            } else {
                break;
            }
        }

        if ( i == mag.length ) return 0;

        if ( i<mag.length ) {
            long current = mag[i];
            for ( int j=0; j<LONG_BITS_LENGTH; j++ ) {
                if ( (current&0x1L) == 0 ) {
                    factor++;
                } else {
                    break;
                }
                current >>>= 1;
            }
        }

        return factor;
    }

    // =========================================================================
    // =========================================================================
    // =========================================================================

    /**
     * Compares the two given magnitudes. (mag1 is shifted left by offset zeros longs)
     *
     * @param mag0
     * @param mag1
     * @param offset
     * @return  0 if mag0 == mag1
     *          1 if mag0 > mag1
     *         -1 if mag0 < mag1
     */
    public static int compareMagnitudes( long[] mag0, long[] mag1, int offset ) {

        if ( offset < 0 ) throw new InvalidArgumentException("The offset must be positive or zero.");

        // Attention: les magnitudes ne sont pas forcement réduites (les MSLongs peuvent être nuls)!!!

        int i;
        if ( mag0.length > mag1.length + offset ) {
            // MSB in mag0:
            for ( i=mag0.length-1; i>=mag1.length+offset; i-- ) {
                if ( mag0[i] != 0 ) return 1;
            }
        } else {
            // MSB in mag1:
            for ( i=mag1.length-1; i>=mag0.length-offset; i-- ) {
                if ( mag1[i] != 0 ) return -1;
            }
            i=i+offset;
        }

        // commons:
        for ( ; i>=offset; i-- ) {
            if ( mag0[i] != mag1[i-offset] ) {
                return Magnitude.unsignedLongCompare(mag0[i], mag1[i-offset]);
            }
        }

        // LSB in mag0:
        for ( ; i>-1; i-- ) {
            if (mag0[i] != 0L) {
                return 1;
            }
        }

        return 0;
    }

    /**
     * Compare the two given magnitudes.
     * The elements at index 0 in the arrays mag0 and mag1 mustn't be zero.
     *
     * @param mag0 a magnitude.
     * @param mag1 a magnitude.
     * @return
     */
    public static int compareMagnitudes( long[] mag0, long[]mag1 ) {
        return compareMagnitudes( mag0, mag1, 0 );
    }

    /**
     * Computes and returns the sum of the two given magnitudes.
     * If add is  true, mag1[0] is added       to  mag0[offset]...
     * If add is false, mag1[0] is subtracted from mag0[offset]...
     * <br>
     * WARNING: mag0 must be >= mag1*2.pow( 64*offset ) if add is false.
     *
     * @param mag0
     * @param mag1
     * @param offset
     * @param makeAdd true for addition and false for subtraction.
     * @return
     */
    public static long[] addMagnitudes( long[] mag0, long[] mag1, int offset, boolean makeAdd ) {

        long[] result = Arrays.copy(mag0, 0, Maths.max(mag0.length, offset+mag1.length) );
        if ( addMagnitudesBuffered( mag1, result, offset, makeAdd ) ) {
            result = Arrays.copy(result, 0, result.length+1 );
            result[result.length-1] = 1;
            return result;
        }
        return reductMagnitude(result);
    }

    /**
     * Computes and returns the sum of the two given magnitudes.
     *
     * If add is  true, magSource[0] is added to  magBuffer[offset]. If add is false, magSource[0] is subtracted from magBuffer[offset].<br>
     * WARNING: magBuffer must be >= magSource*2.pow( 64*offset ) if add is false.
     * WARNING: magBuffer.length   must be   Maths.max(magBuffer.length, offset+magSource.length)+1
     *
     * @param magBuffer
     * @param magSource
     * @param offset
     * @param makeAdd true for addition and false for subtraction.
     * @return the carry.
     */
    public static boolean addMagnitudesBuffered( long[] magSource, long[] magBuffer, int offset, boolean makeAdd ) {

        int i=0;
        boolean carry = false;
        long current, previous;
        while ( ( i<magSource.length || carry ) && (i+offset<magBuffer.length) ) {

            previous = magBuffer[i+offset];
            if ( i<magSource.length ) {
                current = carry ? magSource[i] + 1: magSource[i];
            } else {
                current = carry ? 1: 0;
            }

            if ( makeAdd ) {
                // addition
                magBuffer[i+offset] += current;
                carry = Magnitude.unsignedLongCompare(magBuffer[i+offset], previous) < 0;

            } else {
                // subtraction
                magBuffer[i+offset] -= current;
                carry = Magnitude.unsignedLongCompare(magBuffer[i+offset], previous) > 0;
            }

            i++;
        }
        return carry;
    }

    /**
     *
     * @param m0LowHalf
     * @param m0HighHalf
     * @param m1Half >=0 and < 2³²
     * @return
     */
    private static long[] prodLocal( long m0LowHalf, long m0HighHalf, long m1Half ) {

        long prod0LowHalf, prod0HighHalf, carry;

        // ********** lowestLSB product **********
        prod0LowHalf = m0LowHalf * m1Half;

        // ********** lowestMSB product **********
        prod0HighHalf = (prod0LowHalf >>> INT_BITS_LENGTH);
        prod0LowHalf = prod0LowHalf & LOW_INT_MASK;

        prod0HighHalf += m0HighHalf * m1Half;

        // ********** get carry **********
        carry = (prod0HighHalf >>> INT_BITS_LENGTH);
        prod0HighHalf = prod0HighHalf & LOW_INT_MASK;

        return new long[]{ prod0LowHalf, prod0HighHalf, carry };
    }

    /**
     *
     * @param m0
     * @param m1HighHalf the 4 m1's MSBs
     * @param m1LowHalf the 4 m1's LSBs
     * @param buffer m0*m1 result.
     */
    private static void prodTwoLongBuffered( long m0, long m1HighHalf, long m1LowHalf, long[] buffer ) {

        final long m0LowHalf, m0HighHalf;//, m1LowHalf, m1HighHalf;
        long[] tempoL, tempoH;

        // ========== split values in half long ==========

        m0LowHalf = m0 & LOW_INT_MASK;
        m0HighHalf = m0 >>> INT_BITS_LENGTH; //  & LOW_INT_MASK;

        // ========== local products ==========

        tempoL = prodLocal( m0LowHalf, m0HighHalf, m1LowHalf );
        tempoH = prodLocal( m0LowHalf, m0HighHalf, m1HighHalf );

        // ========== sum of local products ==========

        tempoL[1] += tempoH[0];
        if ( tempoL[1] > LOW_INT_MASK ) {
            tempoL[1] -= HALF_OVERFLOW;
            tempoL[2]++;
        }

        tempoL[2] += tempoH[1];
        if ( tempoL[2] > LOW_INT_MASK ) {
            tempoL[2] -= HALF_OVERFLOW;
            tempoH[2]++;
        }

        buffer[0] = tempoL[0] | (tempoL[1]<<INT_BITS_LENGTH);
        buffer[1] = tempoL[2] | (tempoH[2]<<INT_BITS_LENGTH);
    }

    /**
     * Multiply the given magnitude with the given long multiplicator.
     * If the boolean isSignedLong is:
     * <ul>
     * <li>true: multiplicator is considered as a signed long.</li>
     * <li>false: multiplicator is considered as unsigned long.</li>
     * </ul>
     *
     * @param mag
     * @param multiplicator
     * @param offset
     * @param isSignedLong true if multiplicator must be considered as a signed long and false if multiplicator must be considered as unsigned long.
     * @return mag*multiplicator
     */
    public static long[] multMagnitudeByLong( long[] mag, long multiplicator, int offset, boolean isSignedLong ) {

        if ( isSignedLong && multiplicator<0 ) {
            multiplicator *= -1L;
        }

        long[] resultMagnitude = new long[mag.length+offset+1];

        long[] prod = new long[2];
        long m1LowHalf = multiplicator & LOW_INT_MASK;
        long m1HighHalf = multiplicator >>> INT_BITS_LENGTH; // & LOW_INT_MASK;

        for (int i = 0; i < mag.length; i++) { // i: index in the highest integer
            prodTwoLongBuffered(mag[i], m1HighHalf, m1LowHalf, prod);
            addMagnitudesBuffered(prod, resultMagnitude, i+offset, ADDITION);
        }

        return resultMagnitude;
    }

    /**
     * Computes mag*multiplicator and adds it to the buffer beginning at the given offset.
     * If the boolean isSignedLong is:
     * <ul>
     * <li>true: multiplicator is considered as a signed long.</li>
     * <li>false: multiplicator is considered as unsigned long.</li>
     * </ul>
     *
     * @param mag
     * @param multiplicator
     * @param buffer
     * @param offset
     * @param isSignedLong
     * @return
     */
    public static long[] multMagnitudeByLongBuffered( long[] mag, long multiplicator, long[] buffer, int offset, boolean isSignedLong ) {

        if ( isSignedLong && multiplicator<0 ) {
            multiplicator *= -1L;
        }

        long[] prod = new long[2];
        long m1LowHalf = multiplicator & LOW_INT_MASK;
        long m1HighHalf = multiplicator >>> INT_BITS_LENGTH; // & LOW_INT_MASK;

        for (int i = 0; i < mag.length; i++) { // i: index in the highest integer
            prodTwoLongBuffered(mag[i], m1HighHalf, m1LowHalf, prod);
            addMagnitudesBuffered(prod, buffer, i+offset, ADDITION);
        }

        return buffer;
    }

    /**
     * Multiply mag0 with mag1.
     *
     * @param mag0
     * @param mag1
     * @return mag0*mag1.
     */
    public static long[] multMagnitude( long[] mag0, long[] mag1 ) {

        long[] resultMagnitude = new long[mag0.length + mag1.length];

        for (int i = 0; i < mag1.length; i++) {
            multMagnitudeByLongBuffered( mag0, mag1[i], resultMagnitude, i, IS_UNSIGNED_LONG );
        }

        return Magnitude.reductMagnitude(resultMagnitude);
    }

//    /**
//     * Multiply magSource with magBuffer.
//     *
//     * @param magSource
//     * @param magBuffer
//     * @param bufferBegin the index of the beginning of zeros in the end of buffer.
//     */
//    public static void multMagnitudeBuffered( long[] magSource, long[] magBuffer, int bufferBegin ) {
//
//        for ( int i=bufferBegin-1; i>-1; i-- ) {
//
//            if ( magBuffer[i] != 0L ) {
//
//                long[] tempo = multMagnitudeByLong( magSource, magBuffer[i], 0, false );
//                magBuffer[i] = 0L;
//                addMagnitudesBuffered( tempo, magBuffer, i, true );
//            }
//        }
//    }

    /**
     * Divides numerator by denominator.
     * <br>
     * <b>WARNING:</b>
     * numerator/denominator must be >=0 and <2⁶⁴ (ensure that the result
     * can be stored in only one unsigned long)
     *
     * @param numerator
     * @param denominator
     * @return
     */
    public static long divideSameOrderMagnitude( long[] numerator, long[] denominator ) {

        long quotient = 0x8000000000000000L;
        long delta    = 0x4000000000000000L;

        int compare_DenominatorMultQuotient_And_Numerator = 1;
        long[] denominatorMultQuotient;

        denominatorMultQuotient = Magnitude.shiftLeft(denominator, 63);
        long[] shiftedDenominator = Magnitude.shiftLeft(denominator, 62);

        while ( compare_DenominatorMultQuotient_And_Numerator!=0 && delta!=0 ) {

            compare_DenominatorMultQuotient_And_Numerator = compareMagnitudes( denominatorMultQuotient, numerator);

            if ( compare_DenominatorMultQuotient_And_Numerator < 0 ) {
                quotient += delta;
                if ( delta!=0 ) Magnitude.addMagnitudesBuffered(shiftedDenominator, denominatorMultQuotient, 0, ADDITION);
            } else if ( compare_DenominatorMultQuotient_And_Numerator > 0 ) {
                quotient -= delta;
                if ( delta!=0 ) Magnitude.addMagnitudesBuffered(shiftedDenominator, denominatorMultQuotient, 0, SUBTRACTION);
            }

            delta = delta >>> 1;
            Magnitude.shiftRightBuffered(shiftedDenominator, 1);
        }

        compare_DenominatorMultQuotient_And_Numerator = compareMagnitudes( denominatorMultQuotient, numerator);
        if (compare_DenominatorMultQuotient_And_Numerator > 0) {
            quotient -= 1;
        }

        return quotient;
    }

    /**
     * Divides numerator by denominator and returns the quotient and the
     * remainder (Euclid algorithm).
     *
     * @param numerator
     * @param denominator
     * @return { quotient, remainder }
     */
    public static long[][] divideAndRemainderMagnitude( long[] numerator, long[] denominator ) {

        if (denominator.length == 1 && denominator[0] == 0L) {
            throw new InvalidArgumentException("Division by 0 exception.");
        }

        long[] remainder = Arrays.copy(numerator); // abs(remainder) is always <= abs(numerator)
        long[] quotient;

        if (numerator.length >= denominator.length) {

            quotient = new long[numerator.length - denominator.length + 1];
            long[] num, tempo;
            long quo = 1L; // init to any value != 0

            for (int i=quotient.length-1; i>-1; i--) {

                if ( ( i+denominator.length<remainder.length && remainder[i+denominator.length]!=0L) || quo==0L ) {
                    num = Arrays.copy(remainder, i, denominator.length+1);
                } else {
                    num = Arrays.copy(remainder, i, denominator.length);
                }

                quo = divideSameOrderMagnitude(num, denominator);
                quotient[i] = quo;

                if (quo != 0) {
                    tempo = multMagnitudeByLong(denominator, quo, 0, false);
                    remainder = addMagnitudes(remainder, tempo, i, false);
                }
            }
            quotient = reductMagnitude(quotient);

        } else {
            quotient = new long[]{ 0x0L };
        }

        remainder = reductMagnitude(remainder);

        return new long[][]{ quotient, remainder };

        // *********************************************************************
        // *********************************************************************
        // *********************************************************************

//        long[] quotient;
//        long[] remainder = null;
//
//        int cmp = Magnitude.compareMagnitudes(numerator, denominator);
//
//        if ( cmp < 0 ) {
//
//            quotient = new long[]{0x0L};
//            remainder = Arrays.copy(numerator);
//
//        } else if ( cmp == 0 ) {
//
//            quotient = new long[]{0x1L};
//            remainder = new long[]{0x0L};
//
//        } else {
//
//            // =================================================================
//            // ========== Initializations ======================================
//            // =================================================================
//
//            long[] denomShifted = null;
//
//            final int numeratorBitLength = Magnitude.getBitLength(numerator);
//            final int denominatorBitLength = Magnitude.getBitLength(denominator);
//
//            int cpt = numeratorBitLength - denominatorBitLength;
//
//            final int qLongLength = (cpt+1) / 64 + 1;
//            quotient = new long[qLongLength];
//            quotient[0] = 1L;
//
//            long[] mult = Arrays.copy(denominator, 0, (numeratorBitLength+1) / 64 + 1);
//            Magnitude.shiftLeftBuffered(mult, cpt);
//            if ( numerator.length != mult.length ) {
//                numerator = Arrays.copy(numerator, 0, mult.length);
//            }
//
//            cmp = Magnitude.compareMagnitudes(mult, numerator);
//
//            if ( cmp == 0 ) {
//                Magnitude.shiftLeftBuffered(quotient, cpt);
//                cpt = 0;
//            } else {
//                if ( cmp > 0 ) {
//                    Magnitude.shiftRightBuffered(mult, 1);
//                    cmp = -1;
//                    cpt--;
//                }
//                denomShifted = Arrays.copy(mult);
//            }
//
//            // =================================================================
//            // ========== Process ==============================================
//            // =================================================================
//
//            while ( cpt > 0 ) {
//
//                Magnitude.shiftLeftBuffered(quotient, 1);
//                Magnitude.shiftRightBuffered(denomShifted, 1);
//
//                if ( cmp < 0 ) {
//                    Magnitude.addMagnitudesBuffered(denomShifted, mult, 0, true);
//                } else { // Here cmp > 0
//                    Magnitude.addMagnitudesBuffered(denomShifted, mult, 0, false);
//                }
//
//                cmp = Magnitude.compareMagnitudes(mult, numerator);
//                if ( cmp < 1 ) {
//                    Magnitude.addMagnitudesBuffered(new long[]{ 0x01L }, quotient, 0, true); // Q = Q + 1
//                    if ( cmp == 0 ) {
//                        Magnitude.shiftLeftBuffered(quotient, cpt-1);
//                        cpt = 0;
//                    }
//                }
//
//                cpt--;
//            }
//
//            // =================================================================
//            // ========== End ==================================================
//            // =================================================================
//
//            if ( cmp == 0 ) {
//                remainder = new long[]{ 0x0L };
//            } else {
//                if ( cmp > 0 ) {
//                    Magnitude.addMagnitudesBuffered(denominator, mult, 0, false); // M = M - D
//                }
//                remainder = Magnitude.addMagnitudes(numerator, mult, 0, false);
//            }
//        }
//
//        return new long[][]{ Magnitude.reductMagnitude(quotient), Magnitude.reductMagnitude(remainder) };
    }

    public static long[] divide( long[] numerator, long[] denominator ) {
        return Magnitude.divideAndRemainderMagnitude(numerator, denominator)[0];
    }

    // =========================================================================
    // ========== Bitwise operators ============================================
    // =========================================================================

    /**
     * Complements all values in mag.
     *
     * @param mag
     * @return
     */
    public static long[] not( long[] mag ) {
        long[] result = new long[mag.length];
        for ( int i=0; i<mag.length; i++ ) {
            result[i] = ~mag[i];
        }
        return result;
    }

    /**
     * Bitwise and between all values with the same index in mag0 and mag1.
     *
     * @param mag0
     * @param mag1
     * @return
     */
    public static long[] and( long[] mag0, long[] mag1 ) {

        long[] result = new long[Maths.max(mag0.length, mag1.length)];
        for ( int i=0; i<Maths.min(mag0.length, mag1.length); i++ ) {
            result[i] = mag0[i] & mag1[i];
        }
        return result;
    }

    public static long[] andNot( long[] mag0, long[] mag1 ) {

        long[] result = new long[Maths.max(mag0.length, mag1.length)];
        for ( int i=0; i<Maths.min(mag0.length, mag1.length); i++ ) {
            result[i] = mag0[i] & ~mag1[i];
        }
        return result;
    }

    public static long[] or( long[] mag0, long[] mag1 ) {

        final int maxLength = Maths.max(mag0.length, mag1.length);
        final int minLength = Maths.min(mag0.length, mag1.length);
        long[] result = new long[maxLength];

        for ( int i=0; i<minLength; i++ ) {
            result[i] = mag0[i] | mag1[i];
        }
        for ( int i=Maths.min(mag0.length, mag1.length); i<Maths.max(mag0.length, mag1.length); i++) {
            if ( mag0.length > mag1.length) {
                result[i] = mag0[i];
            } else  {
                result[i] = mag1[i];
            }
        }
        return result;
    }

    public static long[] orNot( long[] mag0, long[] mag1 ) {

        final int maxLength = Maths.max(mag0.length, mag1.length);
        final int minLength = Maths.min(mag0.length, mag1.length);
        long[] result = new long[maxLength];

        for ( int i=0; i<minLength; i++ ) {
            result[i] = mag0[i] | ~mag1[i];
        }
        for ( int i=Maths.min(mag0.length, mag1.length); i<Maths.max(mag0.length, mag1.length); i++) {
            if ( mag0.length > mag1.length) {
                result[i] = mag0[i];
            } else  {
                result[i] = ~mag1[i];
            }
        }
        return result;
    }

    public static long[] xor( long[] mag0, long[] mag1 ) {

        final int maxLength = Maths.max(mag0.length, mag1.length);
        final int minLength = Maths.min(mag0.length, mag1.length);
        long[] result = new long[maxLength];

        for ( int i=0; i<minLength; i++ ) {
            result[i] = mag0[i] ^ mag1[i];
        }
        for ( int i=Maths.min(mag0.length, mag1.length); i<Maths.max(mag0.length, mag1.length); i++) {
            if ( mag0.length > mag1.length) {
                result[i] = mag0[i];
            } else  {
                result[i] = mag1[i];
            }
        }
        return result;
    }

    public static long[] xorNot( long[] mag0, long[] mag1 ) {

        final int maxLength = Maths.max(mag0.length, mag1.length);
        final int minLength = Maths.min(mag0.length, mag1.length);
        long[] result = new long[maxLength];

        for ( int i=0; i<minLength; i++ ) {
            result[i] = mag0[i] ^ ~mag1[i];
        }
        for ( int i=Maths.min(mag0.length, mag1.length); i<Maths.max(mag0.length, mag1.length); i++) {
            if ( mag0.length > mag1.length) {
                result[i] = mag0[i];
            } else  {
                result[i] = ~mag1[i];
            }
        }
        return result;
    }

    /**
     * Shifts mag n bits to the right. (unsigned shift).
     *
     * @param mag
     * @param n shift in bits.
     * @return mag >>> n.
     */
    public static long[] shiftRight( long[] mag, int n ) {

        final int nLongsShift = n/LONG_BITS_LENGTH;

        if ( nLongsShift >= mag.length ) {
            return new long[]{ 0x0L };
        }

        final int nBitsShift = n%LONG_BITS_LENGTH;

        long[] result = Arrays.copy(mag, nLongsShift, mag.length-nLongsShift);
        shiftRightBuffered( result, nBitsShift );

        return reductMagnitude(result);
    }

    /**
     * Shifts buffer n bits to the right. (unsigned shift).
     *
     * @param buffer
     * @param n shift in bits.
     */
    public static void shiftRightBuffered( long[] buffer, int n ) {

        if ( n == 0 ) {
            return;
        }

        final int nLongsShift = n/LONG_BITS_LENGTH;
        final int nBitsShift = n%LONG_BITS_LENGTH;

        // if n > buffer bit length: set all to 0.
        if ( n >= buffer.length*LONG_BITS_LENGTH ) {
            for ( int i=0; i<buffer.length; i++ ) buffer[i] = 0;
            return;
        }

        for ( int i=0; i<buffer.length-nLongsShift-1; i++ ) {
            buffer[i] = (buffer[i+nLongsShift+1] << ( LONG_BITS_LENGTH-nBitsShift ) ) | ( buffer[i+nLongsShift] >>> nBitsShift );
        }

        buffer[buffer.length-nLongsShift-1] = buffer[buffer.length-1] >>> nBitsShift;

        for ( int i=buffer.length-nLongsShift; i<buffer.length; i++ ) buffer[i] = 0;
    }

    /**
     * Shifts mag n bits to the left. If necessary mag length is increased.
     *
     * @param mag
     * @param n shift in bits.
     * @return mag << n.
     */
    public static long[] shiftLeft( long[] mag, int n ) {

        if ( n == 0 ) {
            return Arrays.copy(mag);
        }

        int nLongsShift = n/LONG_BITS_LENGTH;
        int nBitsShift = n%LONG_BITS_LENGTH;

        long[] result = Arrays.copy(mag, 0, mag.length, new long[mag.length+nLongsShift+1], nLongsShift);
        Magnitude.shiftLeftBuffered(result, nBitsShift);

        return Magnitude.reductMagnitude(result);
    }

    /**
     * Shifts buffer n bits to the left. buffer is never resized.
     *
     * @param buffer
     * @param n shift in bits.
     */
    public static void shiftLeftBuffered( long[] buffer, int n ) {

        if ( n == 0 ) {
            return;
        }

        // if n > buffer bit length: set all buffer to 0.
        if ( n >= buffer.length*LONG_BITS_LENGTH ) {
            for ( int i=0; i<buffer.length; i++ ) buffer[i] = 0;
            return;
        }

        final int nLongsShift = n/LONG_BITS_LENGTH;
        final int nBitsShift = n%LONG_BITS_LENGTH;

        for ( int i=buffer.length-1; i>nLongsShift; i-- ) {

            if ( nBitsShift == 0 ) {
                buffer[i] = buffer[i-nLongsShift];
            } else {
                buffer[i] = ( buffer[i-nLongsShift] << nBitsShift ) | ( buffer[i-nLongsShift-1] >>> (LONG_BITS_LENGTH-nBitsShift) );
            }
        }
        buffer[nLongsShift] = ( buffer[0] << nBitsShift );

        for ( int i=0; i<nLongsShift; i++ ) buffer[i] = 0;
    }

    /**
     * Computes the two's complemented value of mag.
     *
     * @param mag
     * @param newLength
     * @return the two's complemented value of mag.
     */
    public static long[] toNegative( long[] mag, int newLength ) {

        long[] result = Arrays.copy(mag, 0, newLength);
        // first complement:
        result = not( result );
        // then add 1
        return addMagnitudes( result, new long[] { 1 }, 0, true );
    }

    /**
     * Returns the state of the bit at index nBit.
     *
     * @param sign used to compute the signed magnitude ( < 0 for negative ).
     * @param mag
     * @param index
     * @return false for 0 and true for 1.
     */
    public static boolean getBit( int sign, long[] mag, int index ) {

        final int magIndex = index/LONG_BITS_LENGTH;

        if ( magIndex >= mag.length ) {

            return sign < 0;

        } else {

            final int longIndex = index%LONG_BITS_LENGTH;
            long[] signedMag;

            if ( sign < 0 ) {
                signedMag = Magnitude.toNegative( mag, mag.length );
            } else {
                signedMag = mag;
            }

            return ( ( signedMag[magIndex] >> longIndex ) & 0x1 ) == 0x1;
        }
    }

    /**
     * Computes the number of bytes needed to encode mag.
     *
     * @param mag
     * @return the number of bytes needed to encode mag.
     */
    public static int getByteLength( long[] mag ) {

        int byteLength = (mag.length-1)*LONG_BYTE_LENGTH;

        long lastLongByteHalf = mag[mag.length-1];
        while ( lastLongByteHalf != 0 ) {
            byteLength++;
            lastLongByteHalf >>>= 8;
        }

        if ( byteLength == 0 ) return 1;
        return byteLength;
    }

    /**
     * Computes the number of bits needed to encode mag.
     *
     * @param mag
     * @return the number of bits needed to encode mag.
     */
    public static int getBitLength( long[] mag ) {

        int bitLength = (mag.length-1)*LONG_BITS_LENGTH;

        long lastLongByteHalf = mag[mag.length-1];
        while ( lastLongByteHalf != 0 ) {
            bitLength++;
            lastLongByteHalf >>>= 1;
        }

        if ( bitLength == 0 ) return 1;
        return bitLength;
    }
}
