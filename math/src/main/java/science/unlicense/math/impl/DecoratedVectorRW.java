
package science.unlicense.math.impl;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.math.api.AbstractVectorRW;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class DecoratedVectorRW extends AbstractVectorRW {

    private final TupleRW tuple;

    public DecoratedVectorRW(TupleRW tuple) {
        CObjects.ensureNotNull(tuple);
        this.tuple = tuple;
    }

    @Override
    public ArithmeticType getNumericType() {
        return tuple.getNumericType();
    }

    @Override
    public int getSampleCount() {
        return tuple.getSampleCount();
    }

    @Override
    public double get(int indice) throws InvalidIndexException {
        return tuple.get(indice);
    }

    @Override
    public Arithmetic getNumber(int indice) throws InvalidIndexException {
        return tuple.getNumber(indice);
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        tuple.toNumber(buffer, offset);
    }

    @Override
    public void toFloat(float[] buffer, int offset) {
        tuple.toFloat(buffer, offset);
    }

    @Override
    public void toDouble(double[] buffer, int offset) {
        tuple.toDouble(buffer, offset);
    }

    @Override
    public void set(int indice, double value) throws InvalidIndexException {
        tuple.set(indice, value);
    }

    @Override
    public void set(int indice, Arithmetic value) throws InvalidIndexException {
        tuple.set(indice, value);
    }

    @Override
    public double getX() throws InvalidIndexException {
        return tuple.get(0);
    }

    @Override
    public double getY() throws InvalidIndexException {
        return tuple.get(1);
    }

    @Override
    public double getZ() throws InvalidIndexException {
        return tuple.get(2);
    }

    @Override
    public double getW() throws InvalidIndexException {
        return tuple.get(3);
    }

    @Override
    public VectorRW create(int size) {
        return VectorNf64.createDouble(size);
    }
}
