
package science.unlicense.math.impl.number;

import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Number;

/**
 *
 * @author Johann Sorel
 */
public class Float64Range extends AbstractNumberRange {

    protected final double min;
    protected final double max;

    public Float64Range(double min, double max, boolean minIncluded, boolean maxIncluded) {
        super(minIncluded, maxIncluded);
        this.min = min;
        this.max = max;
    }

    public Number getMinimum() {
        return new Float64(min);
    }

    public Number getMaximum() {
        return new Float64(max);
    }

}
