

package science.unlicense.math.impl.transform;

/**
 * ZigZag transform.
 *
 * Often used with DCT and quantization in image and video compression.
 *
 * @author Johann Sorel
 */
public class ZigZag {

    private final int[] indices;

    public ZigZag(int[] indices) {
        this.indices = indices;
    }

    /**
     * Un-Zigzag int array and store result in float array.
     * @param source
     * @param target
     */
    public void unZigZag(int[] source, float[] target) {
        for (int i=0; i<indices.length; i++){
            target[i] = source[indices[i]];
        }
    }

    /**
     * Un-Zigzag source array and store result in target array.
     * @param source
     * @param target
     */
    public void unZigZag(final int[] source, final int[] target) {
        for (int i=0; i<indices.length; i++) {
            target[i] = source[indices[i]];
        }
    }

    /**
     * Zizag float array and store result in int array.
     * @param source
     * @param target
     */
    public void zigZag(float[] source, int[] target) {
        for (int i=0; i<indices.length; i++) {
            target[indices[i]] = (int) source[i];
        }
    }

}
