package science.unlicense.math.impl.number;

import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.math.api.Maths;

/**
 * Interval arithmetic.
 *
 * A method developed by mathematicians since the 1950s and 1960s as an approach
 * to putting bounds on rounding errors and measurement errors in mathematical
 * computation and thus developing numerical methods that yield reliable results.
 * ( https://en.wikipedia.org/wiki/Interval_arithmetic )
 *
 * @author Bertrand COTE
 */

public class Interval implements Arithmetic {

    private static final Type TYPE = new Type();
    /**
     * Margin of error on isZero and isOne methods
     */
    private static double epsilon;
    private static boolean epsilonIsSet;
    static {
        Interval.resetEpsilon();
    }

    /**
     * Interval's minimum and maximum bounds.
     * The property this.min <= this.max is always true.
     */
    private final double min, max;

    /**
     * Constructor for the [0., 0.] interval.
     */
    public Interval() {
        this(0., 0.);
    }

    /**
     * Constructor for the [val, val] interval.
     *
     * @param val min and max values
     */
    public Interval( double val ) {
        this(val, val);
    }

    /**
     * Main constructor.
     *
     * @param min one bound of the interval
     * @param max the other bound of the interval
     */
    public Interval( double min, double max ) {
        // the condition this.min <= this.max must be true.
        if ( max < min ) {
            this.min = max;
            this.max = min;
        } else {
            this.min = min;
            this.max = max;
        }
    }

    @Override
    public ArithmeticType getType() {
        return TYPE;
    }

    /**
     * String representation of the interval.
     * @return the string representation of the interval.
     */
    @Override
    public String toString() {
        return "[ " + this.min + "; " + this.max + " ]";
    }

    /**
     * @return the interval's minimum bound
     */
    public double getMin() {
        return min;
    }

    /**
     * @return the interval's maximum bound
     */
    public double getMax() {
        return max;
    }

    /**
     * Check if zero is not element of the interval.
     *
     * @return true if zero not in the interval
     */
    public boolean zeroNotElementOfThis() {
        return !(this.min<0. && this.max>0.);
    }

    @Override
    public Interval zero() {
        return new Interval();
    }

    @Override
    public boolean isZero() { // Both min and max must be zero.
        if ( Interval.epsilonIsSet ) {
            return     (this.min>-Interval.epsilon && this.min<Interval.epsilon)
                    && (this.max>-Interval.epsilon && this.max<Interval.epsilon);
        } else {
            return (this.min==0.) && (this.max==0.);
        }
    }

    @Override
    public Interval one() {
        return new Interval(1., 1.);
    }

    @Override
    public boolean isOne() { // Both min and max must be one.
        if ( Interval.epsilonIsSet ) {
            return     (this.min>1.-Interval.epsilon && this.min<1.+Interval.epsilon)
                    && (this.max>1.-Interval.epsilon && this.max<1.+Interval.epsilon);
        } else {
            return (this.min==1.) && (this.max==1.);
        }
    }

    /**
     * Compute the width of the interval.
     *
     * @return the width of the interval
     */
    public double width() {
        return this.max - this.min;
    }

    /**
     * Compute the radius of the interval. (distance between de middle and bounds)
     *
     * @return the radius of the interval
     */
    public double rad() {
        return this.width()/2.;
    }

    /**
     * Compute the middle of the interval.
     *
     * @return the middle of the interval.
     */
    public double mid() {
        return (this.min+this.max)/2.;
    }

    /**
     * Compute the mignitude.
     * (The smallest absolute value of the elements of the interval. There is no
     * typo, this is the correct spelling: M-I-G-N-I-T-U-D-E)
     *
     * @return the smallest absolute value of the elements of the interval
     */
    public double mig() {
        if ( this.zeroNotElementOfThis() ) {
            return Math.min( Math.abs(this.min), Math.abs(this.max) );
        }
        return 0.;

    }

    /**
     * Addition.
     * [a, b] + [c, d] = [a + c, b + d]
     * The addition and multiplication operations are commutative,
     * associative and sub-distributive: the set X ( Y + Z ) is a subset
     * of XY + XZ.
     * @param other addition
     * @return added interval
     */
    @Override
    public Interval add( Arithmetic other ) {
        Interval otherInterval = (Interval) other;
        return new Interval( this.getMin()+otherInterval.getMin(), this.getMax()+otherInterval.getMax() );
    }

    /**
     * Addition with simple real.
     * @param d addition
     * @return added interval
     */
    public Interval add( double d ) {
        return new Interval( this.getMin()+d, this.getMax()+d );
    }

    /**
     * Subtraction.
     * [a, b] − [c, d] = [a − d, b − c]
     * @param other subtract
     * @return subtracted interval
     */
    @Override
    public Interval subtract( Arithmetic other ) {
        Interval otherInterval = (Interval) other;
        return new Interval( this.getMin()-otherInterval.getMax(), this.getMax()-otherInterval.getMin() );
    }

    /**
     * Subtraction with simple real.
     * @param d subtract
     * @return subtracted interval
     */
    public Interval subtract( double d ) {
        return new Interval( this.getMin()-d, this.getMax()-d );
    }

    /**
     * Multiplication.
     * [a, b] × [c, d] = [min (a × c, a × d, b × c, b × d), max (a × c, a × d, b × c, b × d)]
     * The addition and multiplication operations are commutative,
     * associative and sub-distributive: the set X ( Y + Z ) is a subset
     * of XY + XZ.
     * @param other multiplier
     * @return multiplied interval
     */
    @Override
    public Interval mult( Arithmetic other ) {
        Interval otherInterval = (Interval) other;
        double[] bounds = new double[]{ this.getMin()*otherInterval.getMin(),
                                        this.getMin()*otherInterval.getMax(),
                                        this.getMax()*otherInterval.getMin(),
                                        this.getMax()*otherInterval.getMax() };
        return new Interval( Maths.min( bounds ), Maths.max( bounds ) );
    }

    /**
     * Multiplication by simple real.
     * @param d multiplier
     * @return multiplied interval
     */
    public Interval mult( double d ) {
        return new Interval( this.getMin()*d, this.getMax()*d );
    }

    /**
     * Division.
     * [a, b] ÷ [c, d] = [min (a ÷ c, a ÷ d, b ÷ c, b ÷ d), max (a ÷ c, a ÷ d, b ÷ c, b ÷ d)]
     * when 0 is not in [c, d].
     * @param other division
     * @return divided interval
     */
    @Override
    public Interval divide( Arithmetic other ) {
        Interval otherInterval = (Interval) other;
        if ( otherInterval.zeroNotElementOfThis() ) {
            double[] bounds = new double[]{ this.getMin()/otherInterval.getMin(),
                                            this.getMin()/otherInterval.getMax(),
                                            this.getMax()/otherInterval.getMin(),
                                            this.getMax()/otherInterval.getMax() };
            return new Interval( Maths.min( bounds ), Maths.max( bounds ) );
        } else {
            throw new InvalidArgumentException( "Divide by zero exception.(zero is included in the interval)");
        }
    }

    /**
     * Division by simple real.
     * @param d division
     * @return divided interval
     */
    public Interval divide( double d ) {
        return new Interval( this.getMin()/d, this.getMax()/d );
    }

    /**
     * Square function
     *
     * WARNING: In intervals arithmetic: this.sq() != this.mult(this)
     *
     * @return squared interval
     */
    public Interval sq() {

        if ( zeroNotElementOfThis() ) {
            return new Interval( this.min*this.min, this.max*this.max );
        } else {
            return new Interval( 0., Math.max(this.min*this.min, this.max*this.max));
        }
    }

    /**
     * Square root function
     * @return square interval
     */
    public Interval sqrt() {
        return new Interval( Math.sqrt(this.min), Math.sqrt(this.max) );
    }

    /**
     * Compute this^n with n integer.
     * @param n power
     * @return pow
     */
    @Override
    public Interval pow( int n ) {

        if ( n<0 && !this.zeroNotElementOfThis() ) {
            throw new InvalidArgumentException( "Divide by zero exception.(zero is included in the interval)");
        }

        if ( n==0 ) {
            return this.one();
        }

        Interval res;
        if ( ((n&1) == 0) && !this.zeroNotElementOfThis() ) { // (n is even) and (0 ∈ this)
            res = new Interval( 0., Maths.max(Maths.pow(min, n), Maths.pow(max, n)));
        } else {
            res = new Interval( Maths.pow(min, n), Maths.pow(max, n) );
        }

        return res;
    }

    @Override
    public Arithmetic op(int opCode) {
        throw new UnimplementedException("Unsupported operand "+ opCode);
    }

    /**
     * Check if the interval is a subset of other.
     * @param other compare interval
     * @return true if this interval is a subset
     */
    public boolean isSubsetOf( Interval other ) {
        return other.min<=this.min && other.max>=this.max;
    }

    /**
     * Sets the margin of error on isZero and isOne methods
     *
     * @param epsilon tolerance
     */
    public static void setEpsilon( double epsilon ) {
        if ( epsilon>=0.) {
            Interval.epsilon = epsilon;
        } else {
            Interval.epsilon = -epsilon;
        }
        Interval.epsilonIsSet = true;
    }

    public static void resetEpsilon() {
        Interval.epsilon = 0.;
        Interval.epsilonIsSet = false;
    }

    private static final class Type implements ArithmeticType {

        @Override
        public Arithmetic create(Arithmetic value) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Class getValueClass() {
            return Interval.class;
        }

        @Override
        public int getPrimitiveCode() {
            return Primitive.UNKNOWNED;
        }
    }

}
