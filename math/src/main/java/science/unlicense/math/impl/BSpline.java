
package science.unlicense.math.impl;

/**
 *
 * Resource :
 * Appendix C: Spline Interpolation
 *
 * p(t) = (2t³ - 3t² + 1)p0 + (t³ - 2t² + t)m0 + (-2t³ + 3t²)p1 + (t³ - t²)m1
 *
 * TODO merge somehow with InterpolationMethod and Interpolator
 *
 * @author Johann Sorel
 */
public class BSpline {

    public double interpolate(double m0, double m1, double p0, double p1, double t) {
        final double t2 = t*t;
        final double t3 = t2*t;
        final double _2t3 = 2*t3;
        final double _3t2 = 3*t2;
        double res = ( _2t3 - _3t2 + 1) * p0
                   + (   t3 - 2*t2 + t) * m0
                   + (-_2t3 + _3t2    ) * p1
                   + (   t3 -   t2    ) * m1;
        return res;
    }

}
