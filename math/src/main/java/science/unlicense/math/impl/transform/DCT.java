

package science.unlicense.math.impl.transform;

import science.unlicense.math.api.Maths;

/**
 * Discrete Cosinus Transform
 *
 * http://fr.wikipedia.org/wiki/Compression_JPEG
 *
 * This implementation is not thread safe.
 *
 * @author Johann Sorel
 */
public class DCT {

    private final double[] C;
    private final int N ;
    private final double N2;
    private final double NS2;

    private final double[][] workmatrix1;
    private final double[][] workmatrix2;

    //cache all values possible to speed up performance.
    //Johann Sorel : there must be a better way to simplify all that
    //but I'm not a not good with math ... so bulk method :)
    //and I'm sure this does not introduce any rounding/calculation error.
    private final double[][][][] DCTVALCACHE;
    private final double[][][][] IDCTVALCACHE;

    public DCT(final int size) {
        N = size;
        N2 = 2.0*N;
        NS2 = 2.0/N;
        C = new double[N];

        //init constants
        for (int i=1; i<N; i++){
            C[i] = 1;
        }
        C[0] = 1 / Math.sqrt(2.0);

        //prepare cache
        workmatrix1 = new double[N][N];
        workmatrix2 = new double[N][N];
        DCTVALCACHE = new double[N][N][N][N];
        IDCTVALCACHE = new double[N][N][N][N];
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                for (int x=0; x<N; x++){
                    for (int y=0; y<N; y++){
                        DCTVALCACHE[i][j][x][y] =
                               Math.cos( ((2*x+1) * i * Maths.PI) / N2)
                             * Math.cos( ((2*y+1) * j * Maths.PI) / N2);
                        IDCTVALCACHE[i][j][x][y] =
                                DCTVALCACHE[i][j][x][y] * C[i] * C[j];
                    }
                }
            }
        }
    }

    /**
     * Apply DCT transform to given pixel matrix.
     *
     * @param pixel
     * @param dct cache . size N x N, not null
     * @return transformed matrix
     */
    public double[][] dct(final double[][] pixel, double[][] dct) {
        double sum;
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                sum=0;
                for (int x=0; x<N; x++){
                    for (int y=0; y<N; y++){
                        sum += pixel[x][y] * DCTVALCACHE[i][j][x][y];
                    }
                }
                sum *= (2.0/N) * C[i] * C[j];

                dct[i][j] = sum;
            }
        }
        return dct;
    }

    /**
     * Apply IDCT transform to given array.
     * @param dct
     */
    public void idct(final float[] dct) {
        int i=0;
        for (int y=0;y<N;y++){
            for (int x=0;x<N;x++){
                workmatrix1[x][y] = dct[i];
                i++;
            }
        }
        idct(workmatrix1,workmatrix2);
        i=0;
        for (int y=0;y<N;y++){
            for (int x=0;x<N;x++){
                dct[i] = (float) workmatrix2[x][y];
                i++;
            }
        }
    }

    /**
     * Apply IDCT transform to given matrix.
     * @param dct
     * @param pixel
     */
    public void idct(final double[][] dct,final double[][] pixel) {

        double sum;
        for (int x=0; x<N; x++){
            for (int y=0; y<N; y++){
                sum = 0;
                for (int i=0; i<N; i++){
                    for (int j=0; j<N; j++){
                        sum += dct[i][j] * IDCTVALCACHE[i][j][x][y];
                    }
                }
                pixel[x][y] = NS2 * sum;
            }
        }
    }

}

