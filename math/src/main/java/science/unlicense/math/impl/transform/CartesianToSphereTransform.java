
package science.unlicense.math.impl.transform;

import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * Transform cartesian centric x/y/z coordinate to spherique latitude/longitude radian coordinate.
 *
 * @author Johann Sorel
 */
public class CartesianToSphereTransform extends SimplifiedTransform {

    private final double radius;

    public CartesianToSphereTransform(double radius) {
        super(3, 2);
        this.radius = radius;
    }

    @Override
    public TupleRW transform(Tuple source, TupleRW dest) {
        if (dest == null) dest = new Vector2f64();
        dest.set(0, Math.atan2(source.get(1), source.get(0))); //longitude
        dest.set(1, Math.asin(source.get(2) / radius)); //latitude
        return dest;
    }

    @Override
    protected void transform1(double[] source, int sourceOffset, double[] dest, int destOffset) {
        double x = source[sourceOffset  ];
        double y = source[sourceOffset+1];
        double z = source[sourceOffset+2];
        dest[destOffset  ] = Math.atan2(y, x); //longitude
        dest[destOffset+1] = Math.asin(z / radius); //latitude
    }

    @Override
    protected void transform1(float[] source, int sourceOffset, float[] dest, int destOffset) {
        double x = source[sourceOffset  ];
        double y = source[sourceOffset+1];
        double z = source[sourceOffset+2];
        dest[destOffset  ] = (float) Math.atan2(y, x); //longitude
        dest[destOffset+1] = (float) Math.asin(z / radius); //latitude
    }

    @Override
    public int getInputDimensions() {
        return 3;
    }

    @Override
    public int getOutputDimensions() {
        return 2;
    }

    @Override
    public Transform invert() {
        return new SphereToCartesianTransform(radius);
    }

}
