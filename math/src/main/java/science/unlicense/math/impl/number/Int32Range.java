
package science.unlicense.math.impl.number;

import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Number;

/**
 *
 * @author Johann Sorel
 */
public class Int32Range extends AbstractNumberRange {

    protected final int min;
    protected final int max;

    public Int32Range(int min, int max, boolean minIncluded, boolean maxIncluded) {
        super(minIncluded, maxIncluded);
        this.min = min;
        this.max = max;
    }

    public Number getMinimum() {
        return new Int32(min);
    }

    public Number getMaximum() {
        return new Int32(max);
    }

}
