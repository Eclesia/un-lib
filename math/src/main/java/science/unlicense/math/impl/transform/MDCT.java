

package science.unlicense.math.impl.transform;

import science.unlicense.math.api.Maths;

/**
 * TODO MDCT and IMDCT
 *
 * Resource :
 * http://en.wikipedia.org/wiki/Modified_discrete_cosine_transform
 *
 * Used in audio and video encoding like :
 * MP3, AC-3, Vorbis, WMA, ATRAC, Cook, AAC
 *
 */
public class MDCT {

    private final int N;
    private final int N2;

    /**
     *
     * @param N frame size, input is 2N, output is 1N
     */
    public MDCT(int N){
        this.N = N;
        this.N2 = N+N;
    }

    public void mdct(float[] data, float[] buffer){

        for (int k=0;k<N;k++){
            float sum = 0;
            for (int n=0;n<N2;n++){
                float v = (float) (Maths.PI/N) * (n+0.5f+N/2)*(k+0.5f);
                sum += data[n] * Math.cos(v);
            }
            buffer[k] = sum;
        }
    }

    public void idct(float[] data, float[] buffer){

        for (int n=0;n<N;n++){
            float sum = 0;
            for (int k=0;k<N;k++){
                float v = (float) (Maths.PI/N) * (n+0.5f+N/2)*(n+0.5f);
                sum += data[k] * Math.cos(v);
            }
            buffer[n] = 1/N * sum;
        }

    }

}
