package science.unlicense.math.impl;

import java.lang.reflect.Array;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.Vector;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.system.SampleSystem;

/**
 *
 * @author Johann Sorel
 */
public class VectorNf64 extends TupleNf64 implements VectorRW {

    public static VectorRW create(SampleSystem system) {
        return createDouble(system.getNumComponents());
    }

    /**
     * Create a new tuple of given size.
     * This methods create the most efficient implementation available.
     *
     * @param size
     * @return
     */
    public static VectorRW createDouble(int size){
        switch (size) {
            case 1 : return new Scalarf64();
            case 2 : return new Vector2f64();
            case 3 : return new Vector3f64();
            case 4 : return new Vector4f64();
            default : return new VectorNf64(size);
        }
    }

    public static VectorRW create(Tuple tuple){
        VectorRW v = createDouble(tuple.getSampleCount());
        v.set(tuple);
        return v;
    }

    public static VectorRW create(double[] array){
        VectorRW v = createDouble(array.length);
        v.set(array);
        return v;
    }

    public static VectorRW create(float[] array){
        VectorRW v = createDouble(array.length);
        v.set(array);
        return v;
    }

    public static VectorRW create(int[] array){
        VectorRW v = createDouble(array.length);
        for (int i=0;i<array.length;i++) v.set(i, array[i]);
        return v;
    }

    public static VectorRW create(Object array){
        final int l = Array.getLength(array);
        VectorRW v = createDouble(l);
        for (int i=0;i<l;i++) v.set(i, Array.getDouble(array, i));
        return v;
    }

    public VectorNf64(int size){
        super(size);
    }

    /**
     * Construct a vector adding one dimension.
     * @param tuple
     * @param value1
     */
    public VectorNf64(Tuple tuple, double value1){
        this(expand(tuple.toDouble(), value1));
    }

    public VectorNf64(final double[] values){
        super(values);
    }

    public VectorNf64(final float[] values){
        super(values);
    }

    public VectorNf64(final Tuple v){
        super(v);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public double getX(){
        return values[0];
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public double getY(){
        return values[1];
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public double getZ(){
        return values[2];
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public double getW(){
        return values[3];
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Vector2f64 getXY(){
        return new Vector2f64(values[0], values[1]);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Vector3f64 getXYZ(){
        return new Vector3f64(values[0], values[1], values[2]);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Vector4f64 getXYZW(){
        return new Vector4f64(values[0], values[1], values[2], values[3]);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setX(double x){
        values[0] = x;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setY(double y){
        values[1] = y;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setZ(double z){
        values[2] = z;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setW(double w){
        values[3] = w;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setXY(double x, double y) {
        values[0] = x;
        values[1] = y;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setXYZ(double x, double y, double z) {
        values[0] = x;
        values[1] = y;
        values[2] = z;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setXYZW(double x, double y, double z, double w) {
        values[0] = x;
        values[1] = y;
        values[2] = z;
        values[3] = w;
    }

    @Override
    public double length(){
        return Vectors.length(values);
    }

    @Override
    public double lengthSquare(){
        return Vectors.lengthSquare(values);
    }

    @Override
    public double dot(Tuple other){
        return Vectors.dot(values, other.toDouble());
    }

    /**
     * Returns true if vector do not contains any NaN or Infinite values.
     *
     * @return true is vector is finite
     */
    @Override
    public boolean isFinite(){
        for (int i=0;i<values.length;i++){
            if (Double.isNaN(values[0]) || Double.isInfinite(values[1])){
                return false;
            }
        }
        return true;
    }

    /**
     * Test if two vectors are orthogonal.
     *
     * @param other second vector
     * @param epsilon tolerance value if not exactly zero
     * @return
     */
    @Override
    public boolean isOrthogonal(Tuple other, double epsilon) {
        final double d = dot(other);
        final double l = length()*Vectors.length(other.toDouble());
        return Math.abs(d/l) < epsilon;
    }

    /**
     * Test if two vectors are parallel
     *
     * @param other second vector
     * @param epsilon tolerance value if not exactly zero
     * @return
     */
    @Override
    public boolean isParallel(Tuple other, double epsilon) {
        final double d = cross(other).length();
        return Math.abs(d) <= epsilon;
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods without buffer //////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public VectorRW add(Tuple other){
        return add(other,null);
    }

    @Override
    public VectorRW subtract(Tuple other){
        return subtract(other,null);
    }

    @Override
    public VectorRW multiply(Tuple other){
        return multiply(other,null);
    }

    @Override
    public VectorRW divide(Tuple other){
        return divide(other,null);
    }

    @Override
    public VectorRW scale(double scale){
        return scale(scale,null);
    }

    @Override
    public VectorRW cross(Tuple other){
        return cross(other,null);
    }

    @Override
    public VectorRW lerp(Tuple other, double ratio){
        return lerp(other,ratio,null);
    }

    @Override
    public VectorRW normalize(){
        return normalize(null);
    }

    @Override
    public VectorRW negate(){
        return negate(null);
    }

    /**
     * Calculate shortest angle to given vector.
     * formula : acos(dot(vec1,vec2))
     *
     * @param second
     * @return shortest angle in radian
     */
    @Override
    public double shortestAngle(Vector second){
        return Vectors.shortestAngle(values, second.toDouble());
    }

    /**
     * Calculate the projected vector of given vector on this vector.
     *
     * formula :
     * v = this vector
     * u = other vector
     * ((u dot v) / |v|^2 ) * v
     *
     * @param candidate vector to project
     * @return projected vector of candidate on this vector.
     */
    @Override
    public VectorRW project(Tuple candidate){
        return scale(dot(candidate) / lengthSquare());
    }

    /**
     * Calculate reflected vector.
     *
     * @param normal
     * @return
     */
    @Override
    public VectorRW reflect(Tuple normal){
        return new VectorNf64(Vectors.reflect(values, normal.toDouble()));
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods with buffer /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////


    @Override
    public VectorRW add(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(values.length);
        final double[] array = buffer.toDouble();
        Vectors.add(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW subtract(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(values.length);
        final double[] array = buffer.toDouble();
        Vectors.subtract(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW multiply(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(values.length);
        final double[] array = buffer.toDouble();
        Vectors.multiply(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW divide(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(values.length);
        final double[] array = buffer.toDouble();
        Vectors.divide(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW scale(double scale, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(values.length);
        final double[] array = buffer.toDouble();
        Vectors.scale(toDouble(), scale, array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW cross(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(values.length);
        final double[] array = buffer.toDouble();
        Vectors.cross(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW lerp(Tuple other, double ratio, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(values.length);
        final double[] array = buffer.toDouble();
        Vectors.lerp(toDouble(), other.toDouble(), ratio, array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW normalize(VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(values.length);
        final double[] array = buffer.toDouble();
        Vectors.normalize(toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW negate(VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(values.length);
        final double[] array = buffer.toDouble();
        Vectors.negate(toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods local  //////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public VectorNf64 localAdd(double x, double y){
        values[0] += x;
        values[1] += y;
        return this;
    }

    @Override
    public VectorNf64 localAdd(double x, double y, double z){
        values[0] += x;
        values[1] += y;
        values[2] += z;
        return this;
    }

    @Override
    public VectorNf64 localAdd(double x, double y, double z, double w){
        values[0] += x;
        values[1] += y;
        values[2] += z;
        values[3] += w;
        return this;
    }

    @Override
    public VectorNf64 localAdd(Tuple other){
        Vectors.add(values, other.toDouble(), values);
        return this;
    }

    @Override
    public VectorNf64 localSubtract(Tuple other){
        Vectors.subtract(values, other.toDouble(), values);
        return this;
    }

    @Override
    public VectorNf64 localMultiply(Tuple other){
        Vectors.multiply(values, other.toDouble(), values);
        return this;
    }

    @Override
    public VectorNf64 localDivide(Tuple other){
        Vectors.divide(values, other.toDouble(), values);
        return this;
    }

    @Override
    public VectorNf64 localScale(double scale){
        Vectors.scale(values, scale, values);
        return this;
    }

    @Override
    public VectorNf64 localCross(Tuple other){
        Vectors.cross(values, other.toDouble(), values);
        return this;
    }

    @Override
    public VectorNf64 localNormalize(){
        Vectors.normalize(values, values);
        return this;
    }

    @Override
    public VectorNf64 localNegate(){
        Vectors.negate(values, values);
        return this;
    }

    @Override
    public VectorNf64 localLerp(Tuple other, double ratio){
        Vectors.lerp(values, other.toDouble(), ratio, values);
        return this;
    }

    @Override
    public VectorNf64 copy() {
        return new VectorNf64(this);
    }

    @Override
    public VectorRW create(int size) {
        return VectorNf64.createDouble(size);
    }

    /**
     * Create a new vector with current vector values and extend it with
     * given value.
     * @param value, value to add at the end of Vector
     * @return Vector
     */
    public VectorNf64 extend(double value){
        return new VectorNf64(expand(values, value));
    }

}
