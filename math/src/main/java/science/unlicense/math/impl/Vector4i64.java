
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Int64;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.api.AbstractVectorRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.common.api.number.NumberType;

/**
 *
 * @author Johann Sorel
 */
public class Vector4i64 extends AbstractVectorRW {

    public long x;
    public long y;
    public long z;
    public long w;

    public Vector4i64() {
    }

    public Vector4i64(long x, long y, long z, long w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    @Override
    public NumberType getNumericType() {
        return Int64.TYPE;
    }

    @Override
    public int getSampleCount() {
        return 4;
    }

    @Override
    public double get(int indice) throws InvalidIndexException {
        switch (indice) {
            case 0 : return x;
            case 1 : return y;
            case 2 : return z;
            case 3 : return w;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public Number getNumber(int indice) throws InvalidIndexException {
        switch (indice) {
            case 0 : return new Int64(x);
            case 1 : return new Int64(y);
            case 2 : return new Int64(z);
            case 3 : return new Int64(w);
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        return z;
    }

    @Override
    public double getW() {
        return w;
    }

    @Override
    public Vector2i64 getXY() {
        return new Vector2i64(x, y);
    }

    @Override
    public Vector3i64 getXYZ() {
        return new Vector3i64(x, y, z);
    }

    @Override
    public Vector4i64 getXYZW() {
        return new Vector4i64(x, y, z, w);
    }

    @Override
    public boolean isAll(double value) {
        return x==value && y==value && z==value && w==value;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public double[] toDouble() {
        return new double[]{x,y,z,w};
    }

    @Override
    public void toDouble(double[] buffer, int offset) {
        buffer[offset+0] = x;
        buffer[offset+1] = y;
        buffer[offset+2] = z;
        buffer[offset+3] = w;
    }

    @Override
    public float[] toFloat() {
        return new float[]{(float) x,(float) y,(float) z, (float) w};
    }

    @Override
    public void toFloat(float[] buffer, int offset) {
        buffer[0] = (float) x;
        buffer[1] = (float) y;
        buffer[2] = (float) z;
        buffer[3] = (float) w;
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        buffer[0] = new Int64(x);
        buffer[1] = new Int64(y);
        buffer[2] = new Int64(z);
        buffer[3] = new Int64(w);
    }

    @Override
    public Vector4i64 copy() {
        return new Vector4i64(x, y, z, w);
    }

    @Override
    public VectorRW create(int size) {
        return VectorNi64.createLong(size);
    }

    @Override
    public void setX(double x) {
        this.x = (long) x;
    }

    @Override
    public void setY(double y) {
        this.y = (long) y;
    }

    @Override
    public void setZ(double z) {
        this.z = (long) z;
    }

    @Override
    public void setW(double w) {
        this.w = (long) w;
    }

    @Override
    public void setXY(double x, double y) {
        this.x = (long) x;
        this.y = (long) y;
    }

    @Override
    public void setXYZ(double x, double y, double z) {
        this.x = (long) x;
        this.y = (long) y;
        this.z = (long) z;
    }

    @Override
    public void setXYZW(double x, double y, double z, double w) {
        this.x = (long) x;
        this.y = (long) y;
        this.z = (long) z;
        this.w = (long) w;
    }

    @Override
    public void setAll(double v) {
        x = (long) v;
        y = (long) v;
        z = (long) v;
        w = (long) v;
    }

    @Override
    public void set(int indice, double value) {
        switch (indice) {
            case 0 : x = (int) value; break;
            case 1 : y = (int) value; break;
            case 2 : z = (int) value; break;
            case 3 : w = (int) value; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(int indice, Arithmetic value) {
        int v = ((Number) value).toInteger();
        switch (indice) {
            case 0 : x = v; break;
            case 1 : y = v; break;
            case 2 : z = v; break;
            case 3 : w = v; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(Tuple toCopy) {
        x = (long) toCopy.get(0);
        y = (long) toCopy.get(1);
        z = (long) toCopy.get(2);
        w = (long) toCopy.get(3);
    }

    @Override
    public void set(double[] values) {
        x = (long) values[0];
        y = (long) values[1];
        z = (long) values[2];
        w = (long) values[3];
    }

    @Override
    public void set(float[] values) {
        x = (long) values[0];
        y = (long) values[1];
        z = (long) values[2];
        w = (long) values[3];
    }

    @Override
    public VectorRW localAdd(Tuple other) {
        if (other instanceof Vector4i64) {
            final Vector4i64 o = (Vector4i64) other;
            this.x += o.x;
            this.y += o.y;
            this.z += o.z;
            this.w += o.w;

        } else {
            this.x += other.get(0);
            this.y += other.get(1);
            this.z += other.get(2);
            this.w += other.get(3);
        }
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y) {
        this.x += x;
        this.y += y;
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y, double z) {
        this.x += x;
        this.y += y;
        this.z += z;
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y, double z, double w) {
        this.x += x;
        this.y += y;
        this.z += z;
        this.w += w;
        return this;
    }

    @Override
    public VectorRW scale(double scale, VectorRW buffer) {
        if (buffer == null) buffer = new Vector4i64();
        if (buffer instanceof Vector4i64) {
            final Vector4i64 o = (Vector4i64) buffer;
            o.x = (int) (x*scale);
            o.y = (int) (y*scale);
            o.z = (int) (z*scale);
            o.w = (int) (w*scale);
        } else {
            buffer.set(0, x*scale);
            buffer.set(1, y*scale);
            buffer.set(2, z*scale);
            buffer.set(3, w*scale);
        }
        return buffer;
    }

    @Override
    public VectorRW multiply(Tuple other, VectorRW buffer) {
        if (buffer == null) buffer = new Vector4i64();

        double rx,ry,rz,rw;

        if (other instanceof Vector4i64) {
            final Vector4i64 o = (Vector4i64) other;
            rx = x*o.x;
            ry = y*o.y;
            rz = z*o.z;
            rw = w*o.w;
        } else {
            rx = x*other.get(0);
            ry = y*other.get(1);
            rz = z*other.get(2);
            rw = w*other.get(3);
        }

        if (buffer instanceof Vector4i64) {
            final Vector4i64 o = (Vector4i64) buffer;
            o.x = (int) rx;
            o.y = (int) ry;
            o.z = (int) rz;
            o.w = (int) rw;
        } else {
            buffer.set(0, rx);
            buffer.set(1, ry);
            buffer.set(2, rz);
            buffer.set(3, rw);
        }
        return buffer;
    }

}
