
package science.unlicense.math.impl.transform;

import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * Transform spherique latitude/longitude radian coordinate to cartesian centric x/y/z coordinate.
 *
 * @author Johann Sorel
 */
public class SphereToCartesianTransform extends SimplifiedTransform {

    private final double radius;

    public SphereToCartesianTransform(double radius) {
        super(2, 3);
        this.radius = radius;
    }

    @Override
    public TupleRW transform(Tuple source, TupleRW dest) {
        if (dest == null) dest = new Vector3f64();
        double lon = source.get(0);
        double lat = source.get(1);
        double cosLat = Math.cos(lat);
        dest.set(0, radius * cosLat * Math.cos(lon));
        dest.set(1, radius * cosLat * Math.sin(lon));
        dest.set(2, radius * Math.sin(lat));
        return dest;
    }

    @Override
    protected void transform1(double[] source, int sourceOffset, double[] dest, int destOffset) {
        double lon = source[sourceOffset  ];
        double lat = source[sourceOffset+1];
        double cosLat = Math.cos(lat);
        dest[destOffset  ] = radius * cosLat * Math.cos(lon);
        dest[destOffset+1] = radius * cosLat * Math.sin(lon);
        dest[destOffset+2] = radius * Math.sin(lat);
    }

    @Override
    protected void transform1(float[] source, int sourceOffset, float[] dest, int destOffset) {
        double lon = source[sourceOffset  ];
        double lat = source[sourceOffset+1];
        double cosLat = Math.cos(lat);
        dest[destOffset  ] = (float) (radius * cosLat * Math.cos(lon));
        dest[destOffset+1] = (float) (radius * cosLat * Math.sin(lon));
        dest[destOffset+2] = (float) (radius * Math.sin(lat));
    }

    @Override
    public int getInputDimensions() {
        return 2;
    }

    @Override
    public int getOutputDimensions() {
        return 3;
    }

    @Override
    public Transform invert() {
        return new CartesianToSphereTransform(radius);
    }

}
