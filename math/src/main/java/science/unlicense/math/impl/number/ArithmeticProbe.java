
package science.unlicense.math.impl.number;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Number;
import science.unlicense.common.api.number.Primitive;

/**
 * Special kind of arithmetic implementation which record all operation used.
 *
 * @author Johann Sorel
 */
public class ArithmeticProbe extends CObject implements Arithmetic {

    private static final ArithmeticType TYPE = new Type();
    private static final Constant ZERO = new Constant(new Float64(0.0));
    private static final Constant ONE = new Constant(new Float64(1.0));


    private ArithmeticProbe() {

    }

    @Override
    public ArithmeticType getType() {
        return TYPE;
    }

    @Override
    public Arithmetic add(Arithmetic other) {
        return new Add(this, other);
    }

    @Override
    public Arithmetic subtract(Arithmetic other) {
        return new Subtract(this, other);
    }

    @Override
    public Arithmetic mult(Arithmetic other) {
        return new Multiply(this, other);
    }

    @Override
    public Arithmetic divide(Arithmetic other) {
        return new Divide(this, other);
    }

    @Override
    public Arithmetic zero() {
        return ZERO;
    }

    @Override
    public boolean isZero() {
        return this instanceof Constant && ((Constant) this).num.isZero();
    }

    @Override
    public Arithmetic one() {
        return ONE;
    }

    @Override
    public boolean isOne() {
        return this instanceof Constant && ((Constant) this).num.isOne();
    }

    @Override
    public Arithmetic pow(int n) {
        return new Pow(this, new Constant(new Int32(n)));
    }

    @Override
    public Arithmetic op(int opCode) {
        return new Op(this, opCode);
    }

    private abstract static class Binary extends ArithmeticProbe {
        private final Arithmetic first;
        private final Arithmetic second;

        public Binary(Arithmetic first, Arithmetic second) {
            this.first = first;
            this.second = second;
        }

        protected abstract Char getSymbol();

        @Override
        public Chars toChars() {
            final CharBuffer cb = new CharBuffer();
            cb.append(first);
            cb.append(' ');
            cb.append(getSymbol());
            cb.append(' ');
            cb.append(second);
            return cb.toChars();
        }
    }

    public static class Add extends Binary {

        public Add(Arithmetic first, Arithmetic second) {
            super(first, second);
        }

        @Override
        protected Char getSymbol() {
            return new Char('+');
        }
    }

    public static class Subtract extends Binary {

        public Subtract(Arithmetic first, Arithmetic second) {
            super(first, second);
        }

        @Override
        protected Char getSymbol() {
            return new Char('-');
        }
    }

    public static class Multiply extends Binary {

        public Multiply(Arithmetic first, Arithmetic second) {
            super(first, second);
        }

        @Override
        protected Char getSymbol() {
            return new Char('*');
        }
    }

    public static class Divide extends Binary {

        public Divide(Arithmetic first, Arithmetic second) {
            super(first, second);
        }

        @Override
        protected Char getSymbol() {
            return new Char('/');
        }
    }

    public static class Pow extends Binary {

        public Pow(Arithmetic first, Arithmetic second) {
            super(first, second);
        }

        @Override
        protected Char getSymbol() {
            return new Char('^');
        }
    }

    public static class Op extends ArithmeticProbe {

        private final Arithmetic first;
        private final int op;

        public Op(Arithmetic first, int op) {
            this.first = first;
            this.op = op;
        }

        @Override
        public Chars toChars() {
            Chars opName = new Chars("op"+op);
            switch (op) {
                case OP_SIN : opName = new Chars("sin"); break;
                case OP_COS : opName = new Chars("cos"); break;
                case OP_TAN : opName = new Chars("tan"); break;
                case OP_ASIN : opName = new Chars("asin"); break;
                case OP_ACOS : opName = new Chars("acos"); break;
                case OP_ATAN : opName = new Chars("atan"); break;
                case OP_LOG : opName = new Chars("log"); break;
                case OP_LOG10 : opName = new Chars("log10"); break;
                case OP_SQRT : opName = new Chars("sqrt"); break;
                case OP_CQRT : opName = new Chars("cqrt"); break;
            }

            final CharBuffer cb = new CharBuffer();
            cb.append(opName);
            cb.append('(');
            cb.append(first);
            cb.append(')');
            return cb.toChars();
        }
    }

    public static class Constant extends ArithmeticProbe {

        private final Number num;

        public Constant(Number num) {
            this.num = num;
        }

        @Override
        public Chars toChars() {
            return CObjects.toChars(num);
        }
    }

    public static class Var extends ArithmeticProbe {

        private final Chars name;

        public Var(Chars name) {
            this.name = name;
        }

        @Override
        public Chars toChars() {
            return name;
        }
    }

    private static class Type implements ArithmeticType {

        @Override
        public Arithmetic create(Arithmetic value) {
            if (value instanceof ArithmeticProbe) {
                return value;
            } else if (value instanceof Number) {
                return new Constant( (Number) value);
            } else {
                throw new UnsupportedOperationException("Unexpectec type "+ value);
            }
        }

        @Override
        public Class getValueClass() {
            return ArithmeticProbe.class;
        }

        @Override
        public int getPrimitiveCode() {
            return Primitive.UNKNOWNED;
        }

    }

}
