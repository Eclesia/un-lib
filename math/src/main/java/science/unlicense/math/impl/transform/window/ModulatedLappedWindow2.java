

package science.unlicense.math.impl.transform.window;

import science.unlicense.math.api.Maths;

/**
 * Window transofm used by : Vorbis
 *
 * resource : window function part
 * http://en.wikipedia.org/wiki/Modified_discrete_cosine_transform
 *
 * @author Johann Sorel
 */
public class ModulatedLappedWindow2 {

    private final int N;
    private final float[] window;

    public ModulatedLappedWindow2(int N){
        this.N = N;
        window = new float[N];

        for (int n=0;n<N;n++){
            final float val = (float) Math.sin( (Maths.PI/2*N) * (n+0.5f) );
            window[n] = (float) Math.sin( (Maths.PI/2) * val*val );
        }
    }

    public void transform(float[] data){
        for (int n=0;n<N;n++){
            data[n] *= window[n];
        }
    }

}
