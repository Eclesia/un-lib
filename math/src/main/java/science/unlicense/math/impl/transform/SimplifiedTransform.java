
package science.unlicense.math.impl.transform;

import science.unlicense.common.api.CObject;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.api.transform.*;
import science.unlicense.math.impl.VectorNf64;

/**
 * Abstract transformation, all calls fallback on N tuple transform methods.
 *
 * @author Johann Sorel
 */
public abstract class SimplifiedTransform extends CObject implements Transform{

    protected final SampleSystem input;
    protected final SampleSystem output;

    public SimplifiedTransform(SampleSystem inout) {
        this(inout, inout);
    }

    public SimplifiedTransform(int inOutSize) {
        this(UndefinedSystem.create(inOutSize),
             UndefinedSystem.create(inOutSize));
    }

    public SimplifiedTransform(int inSize, int outSize) {
        this(UndefinedSystem.create(inSize),
             UndefinedSystem.create(outSize));
    }

    public SimplifiedTransform(SampleSystem input, SampleSystem output) {
        this.input = input;
        this.output = output;
    }

    @Override
    public SampleSystem getInputSystem() {
        return input;
    }

    @Override
    public SampleSystem getOutputSystem() {
        return output;
    }

    @Override
    public TupleRW transform(Tuple source, TupleRW dest) {
        final double[] array = new double[getOutputDimensions()];
        transform(source.toDouble(), 0, array, 0, 1);
        if (dest == null) {
            return VectorNf64.create(array);
        } else {
            dest.set(array);
            return dest;
        }
    }

    @Override
    public final double[] transform(double[] source, double[] dest) {
        if (dest == null) dest = new double[getOutputDimensions()];
        transform1(source, 0, dest, 0);
        return dest;
    }

    @Override
    public final float[] transform(float[] source, float[] dest) {
        if (dest == null) dest = new float[getOutputDimensions()];
        transform1(source, 0, dest, 0);
        return dest;
    }

    @Override
    public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        if (nbTuple == 1) {
            transform1(source, sourceOffset, dest, destOffset);
            return;
        }

        final int inDim = getInputDimensions();
        final int outDim = getOutputDimensions();
        for (int n=0; n<nbTuple; n++,sourceOffset+=inDim,destOffset+=outDim){
            transform1(source, sourceOffset, dest, destOffset);
        }
    }

    @Override
    public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        if (nbTuple == 1) {
            transform1(source, sourceOffset, dest, destOffset);
            return;
        }

        final int inDim = getInputDimensions();
        final int outDim = getOutputDimensions();
        for (int n=0; n<nbTuple; n++,sourceOffset+=inDim,destOffset+=outDim){
            transform1(source, sourceOffset, dest, destOffset);
        }
    }

    protected abstract void transform1(double[] source, int sourceOffset, double[] dest, int destOffset);

    protected abstract void transform1(float[] source, int sourceOffset, float[] dest, int destOffset);

}
