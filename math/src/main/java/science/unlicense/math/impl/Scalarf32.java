
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Number;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.math.api.AbstractVectorRW;
import science.unlicense.math.api.ScalarRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class Scalarf32 extends AbstractVectorRW implements ScalarRW {

    public float x;

    public Scalarf32() {
    }

    public Scalarf32(float x) {
        this.x = x;
    }

    @Override
    public NumberType getNumericType() {
        return Float32.TYPE;
    }

    @Override
    public int getSampleCount() {
        return 1;
    }

    @Override
    public double get(int indice) {
        switch (indice) {
            case 0 : return x;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public Number getNumber(int indice) {
        switch (indice) {
            case 0 : return new Float32(x);
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        throw new InvalidIndexException();
    }

    @Override
    public double getZ() {
        throw new InvalidIndexException();
    }

    @Override
    public double getW() {
        throw new InvalidIndexException();
    }

    @Override
    public VectorRW getXY() {
        return new Scalarf32(x);
    }

    @Override
    public VectorRW getXYZ() {
        throw new InvalidIndexException();
    }

    @Override
    public VectorRW getXYZW() {
        throw new InvalidIndexException();
    }

    @Override
    public boolean isAll(double value) {
        return x==value;
    }

    @Override
    public boolean isValid() {
        return !( Float.isInfinite(x) || Float.isNaN(x) );
    }

    @Override
    public double[] toDouble() {
        return new double[]{x};
    }

    @Override
    public void toDouble(double[] buffer, int offset) {
        buffer[offset+0] = x;
    }

    @Override
    public float[] toFloat() {
        return new float[]{x};
    }

    @Override
    public void toFloat(float[] buffer, int offset) {
        buffer[0] = x;
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        buffer[0] = new Float32(x);
    }

    @Override
    public Scalarf32 copy() {
        return new Scalarf32(x);
    }

    @Override
    public VectorRW create(int size) {
        return VectorNf32.createFloat(size);
    }

    @Override
    public void setX(double x) {
        this.x = (float) x;
    }

    @Override
    public void setY(double y) {
        throw new InvalidIndexException();
    }

    @Override
    public void setZ(double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setW(double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXY(double x, double y) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXYZ(double x, double y, double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXYZW(double x, double y, double z, double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setAll(double v) {
        x = (float) v;
    }

    @Override
    public void set(int indice, double value) {
        switch (indice) {
            case 0 : x = (float) value; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(int indice, Arithmetic value) {
        switch (indice) {
            case 0 : x = ((Number) value).toFloat(); break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(Tuple toCopy) {
        x = (float) toCopy.get(0);
    }

    @Override
    public void set(double[] values) {
        x = (float) values[0];
    }

    @Override
    public void set(float[] values) {
        x = values[0];
    }

    @Override
    public boolean equals(Tuple obj, double tolerance) {
        if (obj == null) {
            return false;
        }
        if (obj.getSampleCount() != 1) {
            return false;
        }

        return Float64.equals(x, obj.get(0), tolerance);
    }

    // scalar methods //////////////////////////////////////////////////////////

    @Override
    public void set(double value) {
        x = (float) value;
    }

    @Override
    public void set(float value) {
        x = value;
    }

    @Override
    public void set(Arithmetic value) {
        x = arithmeticAsNumber(value).toFloat();
    }

    @Override
    public double get() {
        return x;
    }

    @Override
    public Arithmetic getNumber() {
        return new Float32(x);
    }

    @Override
    public boolean getBoolean() {
        return x != 0;
    }

    @Override
    public byte getByte() {
        return (byte) x;
    }

    @Override
    public short getShort() {
        return (short) x;
    }

    @Override
    public int getInt() {
        return (int) x;
    }

    @Override
    public long getLong() {
        return (long) x;
    }

    @Override
    public float getFloat() {
        return x;
    }

    @Override
    public double getDouble() {
        return x;
    }
}
