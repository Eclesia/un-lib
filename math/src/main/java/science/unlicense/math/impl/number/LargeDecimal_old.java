package science.unlicense.math.impl.number;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Orderable;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.common.api.number.Primitive;

/**
 *
 * @author Bertrand COTE
 */
public class LargeDecimal_old implements Orderable, Arithmetic {

    private static final Type TYPE = new Type();

    public static final LargeDecimal_old ZERO;
    public static final LargeDecimal_old  ONE;
    public static final LargeDecimal_old  TWO;

    static {
        ZERO = new LargeDecimal_old( LargeInteger.ZERO, LargeInteger.ZERO );
        ONE  = new LargeDecimal_old(  LargeInteger.ONE, LargeInteger.ZERO );
        TWO  = new LargeDecimal_old(  LargeInteger.TWO, LargeInteger.ZERO );
    }

    /**
     * The mantissa.
     */
    private LargeInteger mantissa;
    /**
     * The maximum mantissa's length (mantissa's maximum precision).<br>
     * Used to limit computing precision in functions like divide, for example.
     * Default value is 128 longs ( = 1KB )
     */
    private long mantissaPrecision = 128;
    /**
     * The exponent.
     */
    private LargeInteger exponent;


    public LargeDecimal_old() {
        this.mantissa = LargeInteger.ZERO;
        this.exponent = LargeInteger.ZERO;
    }

    public LargeDecimal_old( LargeDecimal_old other ) {
        this(other.mantissa, other.exponent );
    }

    public LargeDecimal_old( LargeInteger mantissa, LargeInteger exponent ) {

        this.mantissa = mantissa;

        if ( this.mantissa.isZero() ) {
            this.exponent = LargeInteger.ZERO;
        } else {
            this.exponent = exponent;
        }
    }

    /**
     *
     * @param mantissaSign
     * @param mantissa
     * @param exponentSign
     * @param exponent
     */
    public LargeDecimal_old( int mantissaSign, long[] mantissa, int exponentSign, long[] exponent ) {

        this.mantissa = new LargeInteger( mantissaSign, mantissa );

        if ( this.mantissa.isZero() ) {
            this.exponent = LargeInteger.ZERO;
        } else {
            this.exponent = new LargeInteger( exponentSign, exponent );
        }

    }

    @Override
    public ArithmeticType getType() {
        return TYPE;
    }

    // =========================================================================
    // ========== getters ======================================================
    // =========================================================================

    /**
     * @return the mantissa
     */
    public long[] getMantissa() {
        return Arrays.copy(this.mantissa.getMagnitude());
    }

    /**
     * @return the mantissaSign
     */
    public int getMantissaSign() {
        return this.mantissa.getSign();
    }

    /**
     * @return the exponent
     */
    public long[] getExponent() {
        return Arrays.copy(this.exponent.getMagnitude());
    }

    /**
     * @return the exponentSign
     */
    public int getExponentSign() {
        return this.exponent.getSign();
    }

    // =========================================================================
    // ========== Orderable ====================================================
    // =========================================================================

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (!(otherObject instanceof LargeDecimal_old)) {
            return false;
        }

        LargeDecimal_old other = (LargeDecimal_old) otherObject;
        return
                this.exponent.equals(other.exponent)
                &&
                this.mantissa.equals(other.mantissa);
    }

    @Override
    public int hashCode() {
        return this.mantissa.hashCode() ^ this.exponent.hashCode();
    }

    @Override
    public int order(Object otherObject) {

        LargeDecimal_old other = (LargeDecimal_old) otherObject;

        int compareExponents = this.exponent.order(other.exponent);
        if ( compareExponents == 0 ) {
            return this.mantissa.order(other.mantissa);
        } else {
            return compareExponents;
        }
    }

    // =========================================================================
    // ========== Arithmetic interface methods =================================
    // =========================================================================

    @Override
    public Arithmetic add(Arithmetic otherArithmetic) {

        LargeDecimal_old other = (LargeDecimal_old) otherArithmetic;

        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Arithmetic subtract(Arithmetic otherArithmetic) {

        LargeDecimal_old other = (LargeDecimal_old) otherArithmetic;

        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Arithmetic mult(Arithmetic otherArithmetic) {

        LargeDecimal_old other = (LargeDecimal_old) otherArithmetic;

        // resultMantissa = this.mantissa * other.mantissa
        LargeInteger resultMantissa = this.mantissa.mult(other.mantissa);
        // resultExponent = this.exponent + other.exponent
        LargeInteger resultExponent = this.exponent.add(other.exponent);

        return LargeDecimal_old.simplify(resultMantissa, resultExponent);
    }

    @Override
    public Arithmetic divide(Arithmetic otherArithmetic) {

        LargeDecimal_old other = (LargeDecimal_old) otherArithmetic;

        // resultMantissa = this.mantissa / other.mantissa


        // resultExponent = this.exponent - other.exponent
        LargeInteger resultExponent = this.exponent.subtract(other.exponent);

        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Arithmetic zero() {
        return LargeDecimal_old.ZERO;
    }

    @Override
    public boolean isZero() {
        return this.equals(LargeDecimal_old.ZERO);
    }

    @Override
    public Arithmetic one() {
        return LargeDecimal_old.ONE;
    }

    @Override
    public boolean isOne() {
        return this.equals(LargeDecimal_old.ONE);
    }

    @Override
    public Arithmetic pow(int n) {

        LargeInteger resultMantissa = this.mantissa.pow(n);
        LargeInteger resultExponent = this.exponent.mult(new LargeInteger(n));

        return LargeDecimal_old.simplify(resultMantissa, resultExponent);
    }

    @Override
    public Arithmetic op(int opCode) {
        throw new UnimplementedException("Unsupported operand "+ opCode);
    }

    // =========================================================================
    // =========================================================================
    // =========================================================================

    private static LargeDecimal_old simplify( LargeInteger m, LargeInteger e ) {

        int factor = m.simplify();

        LargeInteger resultM = m.shiftRight(factor);
        LargeInteger resultE = e.add(new LargeInteger(factor));

        return new LargeDecimal_old( resultM, resultE );
    }

    private static final class Type implements ArithmeticType {

        @Override
        public Arithmetic create(Arithmetic value) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Class getValueClass() {
            return LargeDecimal_old.class;
        }

        @Override
        public int getPrimitiveCode() {
            return Primitive.UNKNOWNED;
        }
    }
}
