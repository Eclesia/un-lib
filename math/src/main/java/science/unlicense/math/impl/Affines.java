
package science.unlicense.math.impl;

import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public final class Affines {

    private Affines(){}

    public static Tuple transformNormal(Affine affine, Tuple t, TupleRW buffer) {
        final VectorNf64 v = new VectorNf64(t, 0);
        affine.toMatrix().transform(v, v);
        if (buffer == null) {
            return v.getXYZ();
        } else {
            buffer.set(v);
            return buffer;
        }
    }

}
