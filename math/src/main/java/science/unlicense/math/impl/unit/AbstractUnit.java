
package science.unlicense.math.impl.unit;

import science.unlicense.math.api.unit.Unit;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.Dimension;

/**
 *
 * @author Johann Sorel
 */
public class AbstractUnit extends CObject implements Unit {

    protected final Chars name;
    protected final Chars symbol;
    protected final Chars[] alternativeNames;
    protected final Chars[] alternativeSymbols;
    protected final Dimension[] dimensions;

    public AbstractUnit(Chars name, Chars symbol,
            Chars[] alternativeNames,
            Chars[] alternativeSymbols,
            Dimension[] dimensions) {
        this.name = name;
        this.symbol = symbol;
        this.alternativeNames = alternativeNames;
        this.alternativeSymbols = alternativeSymbols;
        this.dimensions = dimensions;
    }

    @Override
    public Chars name() {
        return name;
    }

    @Override
    public Chars symbol() {
        return symbol;
    }

    @Override
    public Chars[] alternativeNames() {
        return alternativeNames.clone();
    }

    @Override
    public Chars[] alternativeSymbols() {
        return alternativeSymbols.clone();
    }

    @Override
    public Dimension[] dimension() {
        return dimensions;
    }

    @Override
    public Chars toChars() {
        return name.concat(' ').concat(symbol);
    }

}
