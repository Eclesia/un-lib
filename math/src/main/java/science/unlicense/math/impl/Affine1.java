
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.transform.Transform1;

/**
 *
 * @author Johann Sorel
 */
public class Affine1 extends AbstractAffine.RW implements Transform1 {

    private double m00;
    private double m01;

    public Affine1() {
        super(1);
        this.m00 = 1.0;
        this.m01 = 0.0;
    }

    /**
     *
     * @param m00 also called scale
     * @param m01 also called translate
     */
    public Affine1(double m00, double m01) {
        super(1);
        this.m00 = m00;
        this.m01 = m01;
    }

    public Affine1(Affine affine) {
        super(1);
        this.m00 = affine.get(0, 0);
        this.m01 = affine.get(0, 1);
    }

    public Affine1(Matrix m) {
        super(1);
        fromMatrix(m);
    }

    public void setM00(double m00) {
        this.m00 = m00;
    }

    public void setM01(double m01) {
        this.m01 = m01;
    }

    public double getM00() {
        return m00;
    }

    public double getM01() {
        return m01;
    }

    public double getScale() {
        return m00;
    }

    public double getTranslation() {
        return m01;
    }

    @Override
    public int getInputDimensions() {
        return 1;
    }

    @Override
    public int getOutputDimensions() {
        return 1;
    }

    @Override
    public double get(int row, int col) {
        switch(row){
            case 0 : switch(col){case 0:return m00;case 1:return m01;}
        }
        throw new InvalidArgumentException("Invalid row/col index "+row+":"+col);
    }

    @Override
    public void set(int row, int col, double value) {
        switch(row){
            case 0 : switch(col){case 0:m00=value;break;case 1:m01=value;break;} return;
        }
        throw new InvalidArgumentException("Invalid row/col index "+row+":"+col);
    }

    @Override
    public float transform(float source) {
        return (float) (m00*source + m01);
    }

    @Override
    public double transform(double source) {
        return m00*source + m01;
    }

    @Override
    public TupleRW transform(Tuple source, TupleRW dest) {
        if (dest == null) dest = new Scalarf64();
        dest.set(0, m00*source.get(0) + m01);
        return dest;
    }

    @Override
    protected void transform1(double[] source, int sourceOffset, double[] dest, int destOffset) {
        dest[destOffset] = m00*source[sourceOffset] + m01;
    }

    @Override
    protected void transform1(float[] source, int sourceOffset, float[] dest, int destOffset) {
        dest[destOffset] = (float) (m00*source[sourceOffset] + m01);
    }

    @Override
    public AffineRW localInvert() {
        m01 = -(m01/m00);
        m00 = 1.0/m00;
        return this;
    }

    @Override
    public Affine1 invert() {
        return new Affine1(1.0/m00, -(m01/m00));
    }

    @Override
    public void fromMatrix(Matrix m) {
        m00 = m.get(0, 0);
        m01 = m.get(0, 1);
    }

    @Override
    public MatrixRW toMatrix() {
        return new Matrix2x2(m00, m01, 0, 1);
    }

    @Override
    public MatrixRW toMatrix(MatrixRW buffer) {
        if (buffer==null) return toMatrix();
        buffer.set(0, 0, m00);
        buffer.set(0, 1, m01);
        buffer.set(1, 0, 0);
        buffer.set(1, 1, 1);
        return buffer;
    }

}
