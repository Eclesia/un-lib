
package science.unlicense.math.impl.transform;

import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.math.api.Maths;

/**
 * Discrete Fourrier Transform and Inverse Discrete Fourrier Transform.
 *
 * @author Bertrand COTE
 */
public final class DFT {

    private DFT(){}

    /**
     * Discrete Fourrier Transform. (O(n²))
     *
     * Implementation of H = W*h
     * With:
     * <ul>
     *  <li>H: The Discrete Fourrier transform of h (size: 1*n)</li>
     *  <li>W: (size: n*n)</li>
     *  <li>h:The sampled values vector (size: 1*n)</li>
     * </ul>
     *
     * @param dataReal
     * @param dataImag
     */
    public static void dft(double[] dataReal, double[] dataImag) {

        if ( dataReal.length != dataImag.length ) {
            throw new InvalidArgumentException("dataReal.length and dataImag.length must be equals.");
        }

        int n = dataReal.length;
        final double twoPiOverN = 2 * Maths.PI / n;
        double angle, cosAngle, sinAngle;

        double[] dataOutReal = new double[n];
        double[] dataOutImag = new double[n];

        for (int k = 0; k < n; k++) {  // For each dataOut
            for (int t = 0; t < n; t++) {  // For each data
                angle = t * k * twoPiOverN;
                cosAngle = Math.cos(angle);
                sinAngle = Math.sin(angle);
                dataOutReal[k] +=  dataReal[t] * cosAngle + dataImag[t] * sinAngle;
                dataOutImag[k] += -dataReal[t] * sinAngle + dataImag[t] * cosAngle;
            }
        }

        for ( int i=0; i<n; i++ ) {
            dataReal[i] = dataOutReal[i];
            dataImag[i] = dataOutImag[i];
        }
    }

    /**
     * Inverse Discrete Fourrier Transform. (O(n²))
     *
     * @param dataReal data real part array
     * @param dataImag data imaginary part array
     */
    public static void idft(double[] dataReal, double[] dataImag) {
        // Conjugate input (inverse sign of dataImag values).
        for ( int i=0; i<dataImag.length; i++ ) {
            dataImag[i] = -dataImag[i];
        }

        // forward fft
        DFT.dft( dataReal, dataImag );

        // Conjugate again and divide by data length.
        for ( int i=0; i<dataImag.length; i++ ) {
            dataReal[i] = dataReal[i]/dataReal.length;
            dataImag[i] = -dataImag[i]/dataImag.length;
        }
    }

}
