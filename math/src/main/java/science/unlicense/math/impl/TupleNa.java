
package science.unlicense.math.impl;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.common.api.number.Float64;
import science.unlicense.math.api.AbstractTupleRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public class TupleNa extends AbstractTupleRW {

    protected final Arithmetic[] values;

    /**
     * Use given array for values.
     * Warning : no copy made of the array.
     *
     * @param values no copy made of the array.
     */
    public TupleNa(final Arithmetic[] values){
        this.values = values;
    }

    /**
     * Use given array for values.
     * Warning : copy is made of the array.
     *
     * @param values copy is made of the array.
     */
    public TupleNa(final double[] values){
        this.values = new Arithmetic[values.length];
        for (int i=0;i<this.values.length;i++){
            this.values[i] = new Float64(values[i]);
        }
    }

    /**
     * Use given tuple's array for values.
     * Warning : copy is made of the array.
     *
     * @param tuple no copy made of tuple.values.
     */
    public TupleNa(final Tuple tuple){
        values = tuple.toNumber();
    }

    @Override
    public ArithmeticType getNumericType() {
        return values[0].getType();
    }

    /**
     * Get the back array used by this tuple.
     * This is a direct access, modify with care.
     *
     * @return back array
     */
    public Arithmetic[] getValues() {
        return values;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int getSampleCount() {
        return values.length;
    }

    /**
     * {@inheritDoc }
     */
    public Tuple getXY(){
        throw new UnimplementedException();
    }

    /**
     * {@inheritDoc }
     */
    public Tuple getXYZ(){
        throw new UnimplementedException();
    }

    /**
     * {@inheritDoc }
     */
    public Tuple getXYZW(){
        throw new UnimplementedException();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setAll(double v){
        Arrays.fill(values, (int) v);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isAll(double value){
        throw new UnimplementedException();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isValid() {
        return true;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public double get(final int index) {
        return arithmeticAsNumber(values[index]).toDouble();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Arithmetic getNumber(final int index) {
        return values[index];
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void set(final int indice, final double value) {
        throw new UnimplementedException();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void set(final int indice, final Arithmetic value) {
        throw new UnimplementedException();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void set(Tuple toCopy){
        throw new UnimplementedException();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void set(float[] values){
        throw new UnimplementedException();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void set(double[] values){
        throw new UnimplementedException();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public double[] toDouble(){
        double[] array = new double[this.values.length];
        toDouble(array,0);
        return array;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void toDouble(double[] buffer, int offset){
        throw new UnimplementedException();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public float[] toFloat(){
        float[] array = new float[this.values.length];
        toFloat(array,0);
        return array;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void toFloat(float[] buffer, int offset){
        throw new UnimplementedException();
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        throw new UnimplementedException();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public TupleNa copy() {
        return new TupleNa(this);
    }

    @Override
    public TupleRW create(int size) {
        return VectorNi64.createLong(size);
    }

    /**
     * Creates a new tuple with current tuple values and extends it with the
     * given value.
     *
     * @param value to add at the end of tuple.
     * @return new tuple
     */
    public TupleNa extend(float value){
        throw new UnimplementedException();
    }

    /**
     *
     * @return the Chars representation of this.
     */
    @Override
    public Chars toChars() {
        final CharBuffer sb = new CharBuffer();
        sb.append("[");
        for (int i=0;i<this.values.length-1;i++){
            sb.append(Float64.encode(get(i)));
            sb.append(',');
        }
        sb.append(Float64.encode(get(this.values.length-1)));
        sb.append(']');
        return sb.toChars();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Tuple)) {
            return false;
        }
        final Tuple other = (Tuple) obj;
        if (values.length != other.getSampleCount()) {
            return false;
        }
        throw new UnimplementedException();
    }

    @Override
    public boolean equals(Tuple obj, double tolerance) {
        if (obj == null) {
            return false;
        }
        if (values.length != obj.getSampleCount()) {
            return false;
        }
        throw new UnimplementedException();
    }

}
