
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.InvalidArgumentException;

/**
 * Base mathematic operands using only byte array types.
 * This can be used as reference algorithms to emulate number operations when
 * the cpu does not handle them.
 *
 * @author Johann Sorel
 */
public final class ByteMath {

    private ByteMath(){}

    public static byte[] add_UByte_UByte(byte[] a1, byte[] a2){
        throw new InvalidArgumentException("Not implemented yet");
    }

    public static byte[] add_UShort_UShort(byte[] a1, byte[] a2){
        throw new InvalidArgumentException("Not implemented yet");
    }

    public static byte[] add_UInt_UInt(byte[] a1, byte[] a2){
        throw new InvalidArgumentException("Not implemented yet");
    }

    public static byte[] add_ULong_ULong(byte[] a1, byte[] a2){
        throw new InvalidArgumentException("Not implemented yet");
    }

    public static byte[] add_Half_Half(byte[] a1, byte[] a2){
        throw new InvalidArgumentException("Not implemented yet");
    }

    public static byte[] add_Float_Float(byte[] a1, byte[] a2){
        throw new InvalidArgumentException("Not implemented yet");
    }

    public static byte[] add_Double_Double(byte[] a1, byte[] a2){
        throw new InvalidArgumentException("Not implemented yet");
    }

    // ...etc... TODO ... find algos

}
