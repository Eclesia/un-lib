
package science.unlicense.math.impl.number;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.api.number.NumberRange;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractNumberRange extends CObject implements NumberRange{

    protected final boolean minIncluded;
    protected final boolean maxIncluded;

    public AbstractNumberRange(boolean minIncluded, boolean maxIncluded) {
        this.minIncluded = minIncluded;
        this.maxIncluded = maxIncluded;
    }

    public boolean isMinimumInclusive() {
        return maxIncluded;
    }

    public boolean isMaximumInclusive() {
        return maxIncluded;
    }

    public boolean contains(Number candidate) {
        final Number min = getMinimum();
        int order = candidate.order(min);
        if (order<0 || (order==0 && !minIncluded)) return false;
        final Number max = getMaximum();
        order = candidate.order(max);
        return order<0 || (order==0 && maxIncluded);
    }

    public boolean contains(NumberRange candidate) {
        return contains(candidate.getMinimum())
            && contains(candidate.getMaximum());
    }

    public boolean intersects(NumberRange candidate) {
        Number max = getMaximum();
        Number cmin = candidate.getMinimum();
        int order = max.order(cmin);
        if (order<0) {
            return false;
        } else if (order==0) {
            return maxIncluded && candidate.isMinimumInclusive();
        }

        Number min = getMinimum();
        Number cmax = candidate.getMaximum();
        order = min.order(cmax);
        if (order>0) {
            return false;
        } else if (order==0) {
            return minIncluded && candidate.isMaximumInclusive();
        }
        return true;
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("Range ");
        cb.append(minIncluded ? '[' : ']');
        cb.append(getMinimum());
        cb.append(" ... ");
        cb.append(getMaximum());
        cb.append(maxIncluded ? '[' : ']');
        return cb.toChars();
    }

}
