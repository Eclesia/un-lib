
package science.unlicense.math.impl.util;

import java.util.ArrayList;
import java.util.List;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.IntSet;
import science.unlicense.common.api.exception.AlgorithmException;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.impl.number.AbstractNumberRange;

/**
 * Detect linear transform in number series.
 *
 * @author Johann Sorel
 */
public class LinearTransformDetector {

    private double tolerance;

    /**
     * Default detector with a 0 tolerance value.
     */
    public LinearTransformDetector() {
    }

    /**
     *
     * @param tolerance gradiant tolerance
     */
    public LinearTransformDetector(double tolerance) {
        this.tolerance = tolerance;
    }

    /**
     *
     * @param values
     * @param offset starting offset in values
     * @param length number of values to process from starting offset
     * @return
     */
    public Range1S[] detect(double[] values, int offset, int length) {

        final List<Range1S> ranges = new ArrayList<>();

        final Range1S temp = new Range1S();

        int end = offset+length;
        while (offset < end) {
            final Range1S after = new Range1S();
            after.search(values, offset, end, tolerance);

            if (!ranges.isEmpty()) {
                //check if the previous point are more appropriate for this range then the previous one
                //we promote the best linear fitting and lowest A factor
                final Range1S before = ranges.get(ranges.size()-1);
                final double distAfter = after.distance(-1, values[before.end-1]);
                if (distAfter <= tolerance) {
                    //possible better match on the next linear range
                    temp.compute(values, before.start, before.end-1);
                    final double distBefore = temp.distance(before.end-1, values[before.end-1]);
                    if (distAfter < distBefore) {
                        //reduce before range
                        before.a = temp.a;
                        before.end = temp.end;
                        //expand after range
                        after.search(values, after.start-1, end, tolerance);
                    }
                }

                if (before.end != after.start) throw new AlgorithmException();
            }

            ranges.add(after);
            offset = after.end;
        }

        return ranges.toArray(new Range1S[0]);
    }

    /**
     * Compute the intersection of all ranges.
     *
     * @param ranges [sample][ranges]
     * @return
     */
    public static RangeNS[] combine(Range1S[][] ranges) {

        //find all steps
        final IntSet steps = new IntSet();
        final int nbSample = ranges.length;
        for (int i=0; i<nbSample; i++) {
            for (int k=0;k<ranges[i].length;k++) {
                steps.add(ranges[i][k].start);
            }
        }

        //build all ranges
        final int[] starts = steps.toArrayInt();
        final int[] inc = new int[nbSample];
        final int end = ranges[0][ranges[0].length-1].end;
        final RangeNS[] all = new RangeNS[starts.length];
        for (int r=0; r<all.length; r++) {
            all[r] = new RangeNS();
            all[r].start = starts[r];
            all[r].end = (r==all.length-1) ? end : starts[r+1];
            all[r].sampleRanges = new Range1S[nbSample];

            //find each intersecting range
            for (int i=0; i<nbSample; i++) {
                while (!ranges[i][inc[i]].intersects(all[r])) {
                    inc[i]++;
                }
                all[r].sampleRanges[i] = ranges[i][inc[i]];
            }
        }

        return all;

    }

    /**
     * interval in data array which match a same linear transform.
     */
    public static class Range1S extends AbstractNumberRange {
        /** start index inclusive */
        public int start;
        /** end index exclusive */
        public int end;

        /**
         * A value in linear transform y = Ax + b
         */
        public double a;
        /**
         * B value in linear transform y = Ax + b
         */
        public double b;

        public Range1S() {
            super(true, false);
        }

        public Number getMinimum() {
            return new Int32(start);
        }

        public Number getMaximum() {
            return new Int32(end);
        }

        /**
         *
         * @param values
         * @param start index inclusive
         * @param end index exclusive
         */
        void compute(double[] values, int start, int end) {
            this.start = start;
            this.end = end;
            b = values[start];
            a = (values[end-1]-b) / (end-start-1);
        }

        void search(double[] values, int start, int end, double tolerance) {
            this.start = start;
            b = values[start];

            int length = 0;
            search:
            for (int i=start+1;i<end;i++,length++) {
                double oldA = a;
                a = (values[i]-b) / (length+1);

                for (int k=start;k<i;k++) {
                    if (!contains(k-start, values[k], tolerance)) {
                        a = oldA;
                        break search;
                    }
                }
            }
            this.end = start+length+1;
        }

        boolean contains(double x, double y, double tolerance) {
            return distance(x, y) <= tolerance;
        }

        double distance(double x, double y) {
            final double expected = (a * x) + b;
            return Math.abs(expected - y);
        }

        @Override
        public Chars toChars() {
            final CharBuffer cb = new CharBuffer(super.toChars());
            cb.append(" a=").appendNumber(a);
            cb.append(" b=").appendNumber(b);
            return cb.toChars();
        }

    }

    public static class RangeNS extends AbstractNumberRange {
        /** start index inclusive */
        public int start;
        /** end index exclusive */
        public int end;

        public Range1S[] sampleRanges;

        public RangeNS() {
            super(true, false);
        }

        public Number getMinimum() {
            return new Int32(start);
        }

        public Number getMaximum() {
            return new Int32(end);
        }

    }

}
