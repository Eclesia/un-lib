
package science.unlicense.math.impl.diff;

import science.unlicense.common.api.Orderable;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;


/**
 *
 * Adapted from : http://www.codeproject.com/Articles/6943/A-Generic-Reusable-Diff-Algorithm-in-C-II
 * quote : Michael Potter, 10 Jun 2004 Public Domain
 *
 * @author Michael Potter (Original author)
 * @author Johann Sorel (adapted to unlicense and java, code cleaning)
 */
public class DiffEngine {

    public static enum DiffEngineLevel{
        FastImperfect,
        Medium,
        SlowPerfect
    }

    public static enum DiffResultSpanStatus{
        NoChange,
        Replace,
        DeleteSource,
        AddDestination
    }

    private static enum DiffStatus {
        Matched(1),
        NoMatch(-1),
        Unknown(-2);

        public final int val;

        private DiffStatus(int val) {
            this.val = val;
        }

    }

    private Sequence _source;
    private Sequence _dest;
    private ArraySequence _matchList;
    private DiffEngineLevel _level;
    private DiffStateList _stateList;

    public DiffEngine() {
        _source = null;
        _dest = null;
        _matchList = null;
        _stateList = null;
        _level = DiffEngineLevel.FastImperfect;
    }

    private int getSourceMatchLength(int destIndex, int sourceIndex, int maxLength) {
        int matchCount;
        Object dstObject;
        Object srcObject;
        for (matchCount = 0; matchCount < maxLength; matchCount++) {
            dstObject = _dest.get(destIndex + matchCount);
            srcObject = _source.get(sourceIndex + matchCount);
            if (!dstObject.equals(srcObject)) {
                break;
            }
        }
        return matchCount;
    }

    private void getLongestSourceMatch(DiffState curItem, int destIndex, int destEnd, int sourceStart, int sourceEnd) {

        int maxDestLength = (destEnd - destIndex) + 1;
        int curLength = 0;
        int curBestLength = 0;
        int curBestIndex = -1;
        int maxLength = 0;
        for (int sourceIndex = sourceStart; sourceIndex <= sourceEnd; sourceIndex++) {
            maxLength = Math.min(maxDestLength, (sourceEnd - sourceIndex) + 1);
            if (maxLength <= curBestLength) {
                //No chance to find a longer one any more
                break;
            }
            curLength = getSourceMatchLength(destIndex, sourceIndex, maxLength);
            if (curLength > curBestLength) {
                //This is the best match so far
                curBestIndex = sourceIndex;
                curBestLength = curLength;
            }
            //jump over the match
            sourceIndex += curBestLength;
        }
        //DiffState cur = _stateList.GetByIndex(destIndex);
        if (curBestIndex == -1) {
            curItem.setNoMatch();
        } else {
            curItem.setMatch(curBestIndex, curBestLength);
        }

    }

    private void processRange(int destStart, int destEnd, int sourceStart, int sourceEnd) {
        int curBestIndex = -1;
        int curBestLength = -1;
        int maxPossibleDestLength = 0;
        DiffState curItem = null;
        DiffState bestItem = null;
        for (int destIndex = destStart; destIndex <= destEnd; destIndex++) {
            maxPossibleDestLength = (destEnd - destIndex) + 1;
            if (maxPossibleDestLength <= curBestLength) {
                //we won't find a longer one even if we looked
                break;
            }
            curItem = _stateList.GetByIndex(destIndex);

            if (!curItem.hasValidLength(sourceStart, sourceEnd, maxPossibleDestLength)) {
                //recalc new best length since it isn't valid or has never been done.
                getLongestSourceMatch(curItem, destIndex, destEnd, sourceStart, sourceEnd);
            }
            if (curItem.getStatus() == DiffStatus.Matched) {
                switch (_level) {
                    case FastImperfect:
                        if (curItem.getLength() > curBestLength) {
                            //this is longest match so far
                            curBestIndex = destIndex;
                            curBestLength = curItem.getLength();
                            bestItem = curItem;
                        }
                        //Jump over the match
                        destIndex += curItem.getLength() - 1;
                        break;
                    case Medium:
                        if (curItem.getLength() > curBestLength) {
                            //this is longest match so far
                            curBestIndex = destIndex;
                            curBestLength = curItem.getLength();
                            bestItem = curItem;
                            //Jump over the match
                            destIndex += curItem.getLength() - 1;
                        }
                        break;
                    default:
                        if (curItem.getLength() > curBestLength) {
                            //this is longest match so far
                            curBestIndex = destIndex;
                            curBestLength = curItem.getLength();
                            bestItem = curItem;
                        }
                        break;
                }
            }
        }
        if (curBestIndex < 0) {
            //we are done - there are no matches in this span
        } else {

            int sourceIndex = bestItem.getStartIndex();
            _matchList.add(DiffResultSpan.createNoChange(curBestIndex, sourceIndex, curBestLength));
            if (destStart < curBestIndex) {
                //Still have more lower destination data
                if (sourceStart < sourceIndex) {
                    //Still have more lower source data
                    // Recursive call to process lower indexes
                    processRange(destStart, curBestIndex - 1, sourceStart, sourceIndex - 1);
                }
            }
            int upperDestStart = curBestIndex + curBestLength;
            int upperSourceStart = sourceIndex + curBestLength;
            if (destEnd > upperDestStart) {
                //we still have more upper dest data
                if (sourceEnd > upperSourceStart) {
                    //set still have more upper source data
                    // Recursive call to process upper indexes
                    processRange(upperDestStart, destEnd, upperSourceStart, sourceEnd);
                }
            }
        }
    }

    public Sequence processDiff(Sequence source, Sequence destination) {
        return processDiff(source, destination, DiffEngineLevel.FastImperfect);
    }

    public Sequence processDiff(Sequence source, Sequence destination, DiffEngineLevel level) {
        _level = level;
        _source = source;
        _dest = destination;
        _matchList = new ArraySequence();

        int dcount = _dest.getSize();
        int scount = _source.getSize();

        if ((dcount > 0) && (scount > 0)) {
            _stateList = new DiffStateList(dcount);
            processRange(0, dcount - 1, 0, scount - 1);
        }
        return diffReport();
    }

    private boolean addChanges(Sequence report,int curDest,int nextDest,
            int curSource,int nextSource) {

        boolean retval = false;
        int diffDest = nextDest - curDest;
        int diffSource = nextSource - curSource;
        int minDiff = 0;
        if (diffDest > 0) {
            if (diffSource > 0) {
                minDiff = Math.min(diffDest, diffSource);
                report.add(DiffResultSpan.createReplace(curDest, curSource, minDiff));
                if (diffDest > diffSource) {
                    curDest += minDiff;
                    report.add(DiffResultSpan.createAddDestination(curDest, diffDest - diffSource));
                } else if (diffSource > diffDest) {
                    curSource += minDiff;
                    report.add(DiffResultSpan.createDeleteSource(curSource, diffSource - diffDest));
                }
            } else {
                report.add(DiffResultSpan.createAddDestination(curDest, diffDest));
            }
            retval = true;
        } else if (diffSource > 0) {
            report.add(DiffResultSpan.createDeleteSource(curSource, diffSource));
            retval = true;
        }
        return retval;
    }

    private Sequence diffReport() {
        Sequence retval = new ArraySequence();
        int dcount = _dest.getSize();
        int scount = _source.getSize();

        //Deal with the special case of empty files
        if (dcount == 0) {
            if (scount > 0) {
                retval.add(DiffResultSpan.createDeleteSource(0, scount));
            }
            return retval;
        } else {
            if (scount == 0) {
                retval.add(DiffResultSpan.createAddDestination(0, dcount));
                return retval;
            }
        }

        Collections.sort(_matchList);
        int curDest = 0;
        int curSource = 0;
        DiffResultSpan last = null;

        //Process each match record
        for (int i=0,n=_matchList.getSize();i<n;i++){
            DiffResultSpan drs = (DiffResultSpan) _matchList.get(i);
            if ((!addChanges(retval, curDest, drs.getDestIndex(), curSource, drs.getSourceIndex())) && (last != null)) {
                last.addLength(drs.getLength());
            } else {
                retval.add(drs);
            }
            curDest = drs.getDestIndex() + drs.getLength();
            curSource = drs.getSourceIndex() + drs.getLength();
            last = drs;
        }

        //Process any tail end data
        addChanges(retval, curDest, dcount, curSource, scount);

        return retval;
    }

    private static class DiffState {

        private static final int BAD_INDEX = -1;
        private int _startIndex;
        private int _length;

        public DiffState() {
            setToUnkown();
        }

        public int getStartIndex() {
            return _startIndex;
        }

        public int getEndIndex() {
            return ((_startIndex + _length) - 1);
        }

        public int getLength() {
            int len;
            if (_length > 0) {
                len = _length;
            } else {
                if (_length == 0) {
                    len = 1;
                } else {
                    len = 0;
                }
            }
            return len;
        }

        public DiffStatus getStatus() {
            if (_length > 0) {
                return DiffStatus.Matched;
            } else if (_length==-1){
                return DiffStatus.NoMatch;
            } else if (_length==-2){
                throw new InvalidArgumentException("Invalid status: _length < -2");
            } else {
                return DiffStatus.Unknown;
            }
        }

        protected void setToUnkown() {
            _startIndex = BAD_INDEX;
            _length = DiffStatus.Unknown.val;
        }

        public void setMatch(int start, int length) {
            if (length <= 0) {
                throw new InvalidArgumentException("Length must be greater than zero");
            }
            if (start < 0) {
                throw new InvalidArgumentException("Start must be greater than or equal to zero");
            }
            _startIndex = start;
            _length = length;
        }

        public void setNoMatch() {
            _startIndex = BAD_INDEX;
            _length = DiffStatus.NoMatch.val;
        }

        public boolean hasValidLength(int newStart, int newEnd, int maxPossibleDestLength) {
            if (_length > 0) {
                //have unlocked match
                if ((maxPossibleDestLength < _length)
                     || ((_startIndex < newStart)
                     || (getEndIndex() > newEnd))) {
                    setToUnkown();
                }
            }
            return _length != DiffStatus.Unknown.val;
        }
    }

    private static final class DiffStateList {

        private final DiffState[] _array;

        public DiffStateList(int destCount) {
            _array = new DiffState[destCount];
        }

        public DiffState GetByIndex(int index) {
            DiffState retval = _array[index];
            if (retval == null) {
                retval = new DiffState();
                _array[index] = retval;
            }
            return retval;
        }
    }

    public static final class DiffResultSpan implements Orderable {

        private static final int BAD_INDEX = -1;
        private final int _destIndex;
        private final int _sourceIndex;
        private final DiffResultSpanStatus _status;
        private int _length;

        protected DiffResultSpan(
                DiffResultSpanStatus status,
                int destIndex,
                int sourceIndex,
                int length) {
            _status = status;
            _destIndex = destIndex;
            _sourceIndex = sourceIndex;
            _length = length;
        }

        public int getDestIndex() {
            return _destIndex;
        }

        public int getSourceIndex() {
            return _sourceIndex;
        }

        public int getLength() {
            return _length;
        }

        public DiffResultSpanStatus getStatus() {
            return _status;
        }

        void addLength(int i) {
            _length += i;
        }

        public int order(Object obj) {
            return Integer.compare(_destIndex, ((DiffResultSpan) obj)._destIndex);
        }

        public static DiffResultSpan createNoChange(int destIndex, int sourceIndex, int length) {
            return new DiffResultSpan(DiffResultSpanStatus.NoChange, destIndex, sourceIndex, length);
        }

        public static DiffResultSpan createReplace(int destIndex, int sourceIndex, int length) {
            return new DiffResultSpan(DiffResultSpanStatus.Replace, destIndex, sourceIndex, length);
        }

        public static DiffResultSpan createDeleteSource(int sourceIndex, int length) {
            return new DiffResultSpan(DiffResultSpanStatus.DeleteSource, BAD_INDEX, sourceIndex, length);
        }

        public static DiffResultSpan createAddDestination(int destIndex, int length) {
            return new DiffResultSpan(DiffResultSpanStatus.AddDestination, destIndex, BAD_INDEX, length);
        }

        public String toString() {
            return _status +" src:"+_sourceIndex+" dst:"+_destIndex+" lght:"+_length;
        }

    }


}
