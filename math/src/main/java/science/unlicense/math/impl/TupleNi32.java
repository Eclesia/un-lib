
package science.unlicense.math.impl;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Number;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.math.api.AbstractTupleRW;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;

/**
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public class TupleNi32 extends AbstractTupleRW {

    protected final int[] values;

    public TupleNi32(int size){
        values = new int[size];
    }

    /**
     * Use given array for values.
     * Warning : no copy made of the array.
     *
     * @param values no copy made of the array.
     */
    public TupleNi32(final int[] values){
        this.values = values;
    }

    /**
     * Use given array for values.
     * Warning : copy is made of the array.
     *
     * @param values copy is made of the array.
     */
    public TupleNi32(final double[] values){
        this.values = new int[values.length];
        for (int i=0;i<this.values.length;i++){
            this.values[i] = (int) values[i];
        }
    }

    /**
     * Use given tuple's array for values.
     * Warning : copy is made of the array.
     *
     * @param tuple no copy made of tuple.values.
     */
    public TupleNi32(final Tuple tuple){
        values = tuple.toInt();
    }

    @Override
    public NumberType getNumericType() {
        return Int32.TYPE;
    }

    /**
     * Get the back array used by this tuple.
     * This is a direct access, modify with care.
     *
     * @return back array
     */
    public int[] getValues() {
        return values;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int getSampleCount() {
        return values.length;
    }

    /**
     * {@inheritDoc }
     */
    public Tuple getXY(){
        return new Vector2i32(values[0], values[1]);
    }

    /**
     * {@inheritDoc }
     */
    public Tuple getXYZ(){
        return new Vector3i32(values[0], values[1], values[2]);
    }

    /**
     * {@inheritDoc }
     */
    public Tuple getXYZW(){
        return new Vector4i32(values[0], values[1], values[2], values[3]);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setAll(double v){
        Arrays.fill(values, (int) v);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isAll(double value){
        if (values.length == 0)  return false;
        for (int i = 0; i < values.length; i++) {
            if (values[i] != value) return false;
        }
        return true;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isValid() {
        return true;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public double get(final int index) {
        return values[index];
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Number getNumber(final int index) {
        return new Int32(values[index]);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void set(final int indice, final double value) {
        if (indice < 0 || indice >= values.length) throw new InvalidIndexException();
        values[indice] = (int) value;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void set(final int indice, final Arithmetic value) {
        if (indice < 0 || indice >= values.length) throw new InvalidIndexException();
        values[indice] = ((Number) value).toInteger();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void set(Tuple toCopy){
        set( toCopy.toDouble() );
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void set(float[] values){
        for (int i=0,n=Maths.min(this.values.length, values.length);i<n;i++){
            this.values[i] = (int) values[i];
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void set(double[] values){
        for (int i=0,n=Maths.min(this.values.length, values.length);i<n;i++){
            this.values[i] = (int) values[i];
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public double[] toDouble(){
        double[] array = new double[this.values.length];
        toDouble(array,0);
        return array;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void toDouble(double[] buffer, int offset){
        for (int i=0;i<this.values.length;i++){
            buffer[offset+i] = values[i];
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public float[] toFloat(){
        float[] array = new float[this.values.length];
        toFloat(array,0);
        return array;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void toFloat(float[] buffer, int offset){
        for (int i=0;i<this.values.length;i++){
            buffer[offset+i] = values[i];
        }
    }

    @Override
    public void toNumber(Arithmetic[] buffer, int offset) {
        for (int i=0;i<this.values.length;i++){
            buffer[offset+i] = new Int32(values[i]);
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public TupleNi32 copy() {
        return new TupleNi32(this);
    }

    @Override
    public TupleRW create(int size) {
        return VectorNi32.createInt(size);
    }

    /**
     * Creates a new tuple with current tuple values and extends it with the
     * given value.
     *
     * @param value to add at the end of tuple.
     * @return new tuple
     */
    public TupleNi32 extend(float value){
        return new TupleNi32(expand(values, (int) value));
    }

    /**
     *
     * @return the Chars representation of this.
     */
    @Override
    public Chars toChars() {
        final CharBuffer sb = new CharBuffer();
        sb.append("[");
        for (int i=0;i<this.values.length-1;i++){
            sb.append(Float64.encode(get(i)));
            sb.append(',');
        }
        sb.append(Float64.encode(get(this.values.length-1)));
        sb.append(']');
        return sb.toChars();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Tuple)) {
            return false;
        }
        final Tuple other = (Tuple) obj;
        if (values.length != other.getSampleCount()) {
            return false;
        }
        return Arrays.equals(values, other.toInt());
    }

    @Override
    public boolean equals(Tuple obj, double tolerance) {
        if (obj == null) {
            return false;
        }
        if (values.length != obj.getSampleCount()) {
            return false;
        }
        return Arrays.equals(values, obj.toInt());
    }

}
