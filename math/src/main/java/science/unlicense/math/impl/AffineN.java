
package science.unlicense.math.impl;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.MatrixRW;

/**
 *
 * @author Johann Sorel
 */
public class AffineN extends AbstractAffine.RW {

    protected final double[][] values;
    private final int dim;

    public static AffineRW create(int dimension) {
        switch (dimension) {
            case 1 : return new Affine1();
            case 2 : return new Affine2();
            case 3 : return new Affine3();
            case 4 : return new Affine4();
            default : return new AffineN(dimension);
        }
    }

    public static AffineRW create(Affine toCopy) {
        final AffineRW cp = create(toCopy.getInputDimensions());
        cp.set(toCopy);
        return cp;
    }

    public AffineN(int dimension) {
        super(dimension);
        this.dim = dimension;
        this.values = new double[dimension][dimension];
    }

    @Override
    public MatrixRW toMatrix() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void fromMatrix(Matrix m) {
        for (int y=0;y<dim;y++) {
            for (int x=0;x<=dim;x++) {
                values[y][x] = m.get(y, x);
            }
        }
    }

    @Override
    public int getInputDimensions() {
        return dim;
    }

    @Override
    public int getOutputDimensions() {
        return dim;
    }

    @Override
    public double get(int row, int col) {
        return values[row][col];
    }

    @Override
    public void set(int row, int col, double value) {
        values[row][col] = value;
    }

    @Override
    protected void transform1(double[] source, int sourceOffset, double[] dest, int destOffset) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void transform1(float[] source, int sourceOffset, float[] dest, int destOffset) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MatrixRW toMatrix(MatrixRW buffer) {
        if (buffer == null) buffer = MatrixNxN.create(dim+1, +1);
        for (int y=0;y<dim;y++) {
            for (int x=0;x<=dim;x++) {
                buffer.set(y, x, values[y][x]);
            }
        }
        return buffer;
    }

}
