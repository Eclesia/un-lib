
package science.unlicense.math.api;

import science.unlicense.common.api.number.Arithmetic;

/**
 *
 * @author Johann Sorel
 */
public interface ScalarRW extends Scalar, TupleRW {

    /**
     * Set scalar value.
     *
     * @param value new value
     */
    void set(double value);

    /**
     * Set scalar value.
     *
     * @param value new value
     */
    void set(float value);

    /**
     * Set scalar value.
     *
     * @param value new value
     */
    void set(Arithmetic value);
}
