
package science.unlicense.math.api.unitold;

import science.unlicense.math.impl.unitold.BaseUnit;
import science.unlicense.math.impl.unitold.DerivedUnit;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.math.api.transform.ConcatenateTransform;
import science.unlicense.math.api.transform.Transform;

/**
 * List of usual units.
 * TODO
 *
 * @author Johann Sorel
 */
public final class Units {

    //copy of base units
    public static final Unit METER      = BaseUnit.METER;
    public static final Unit KILOGRAM   = BaseUnit.KILOGRAM;
    public static final Unit SECOND     = BaseUnit.SECOND;
    public static final Unit AMPERE     = BaseUnit.AMPERE;
    public static final Unit KELVIN     = BaseUnit.KELVIN;
    public static final Unit MOLE       = BaseUnit.MOLE;
    public static final Unit CANDELA    = BaseUnit.CANDELA;
    public static final Unit UNDEFINED  = BaseUnit.UNDEFINED;

    //length
    public static final Unit DECAMETER      = new DerivedUnit(BaseUnit.METER, 1e1, new Chars("dam"));
    public static final Unit HECTOMETER     = new DerivedUnit(BaseUnit.METER, 1e2, new Chars("hm"));
    public static final Unit KILOMETER      = new DerivedUnit(BaseUnit.METER, 1e3, new Chars("km"));
    public static final Unit MEGAMETER      = new DerivedUnit(BaseUnit.METER, 1e6, new Chars("Mm"));
    public static final Unit GIGAMETER      = new DerivedUnit(BaseUnit.METER, 1e9, new Chars("Gm"));
    public static final Unit TERAMETER      = new DerivedUnit(BaseUnit.METER, 1e12, new Chars("Tm"));
    public static final Unit PETAMETER      = new DerivedUnit(BaseUnit.METER, 1e15, new Chars("Pm"));
    public static final Unit EXAMETER       = new DerivedUnit(BaseUnit.METER, 1e18, new Chars("Em"));
    public static final Unit ZETTAMETER     = new DerivedUnit(BaseUnit.METER, 1e21, new Chars("Zm"));
    public static final Unit YOTTAMETER     = new DerivedUnit(BaseUnit.METER, 1e24, new Chars("Ym"));

    public static final Unit DECIMETER      = new DerivedUnit(BaseUnit.METER, 1e-1, new Chars("dm"));
    public static final Unit CENTIMETER     = new DerivedUnit(BaseUnit.METER, 1e-2, new Chars("cm"));
    public static final Unit MILLIMETER     = new DerivedUnit(BaseUnit.METER, 1e-3, new Chars("mm"));
    public static final Unit MICROMETER     = new DerivedUnit(BaseUnit.METER, 1e-6, new Chars("μm"));
    public static final Unit NANOMETER      = new DerivedUnit(BaseUnit.METER, 1e-9, new Chars("nm"));
    public static final Unit PICOMETER      = new DerivedUnit(BaseUnit.METER, 1e-12, new Chars("pm"));
    public static final Unit FEMTOMETER     = new DerivedUnit(BaseUnit.METER, 1e-15, new Chars("fm"));
    public static final Unit ATTOMETER      = new DerivedUnit(BaseUnit.METER, 1e-18, new Chars("am"));
    public static final Unit ZEPTOMETER     = new DerivedUnit(BaseUnit.METER, 1e-21, new Chars("zm"));
    public static final Unit YOCTOMETER     = new DerivedUnit(BaseUnit.METER, 1e-24, new Chars("ym"));

    //time
    public static final Unit MILLISECOND    = new DerivedUnit(BaseUnit.SECOND, 1e-3, new Chars("ms"));
    public static final Unit NANOSECOND     = new DerivedUnit(BaseUnit.SECOND, 1e-9, new Chars("ns"));

    private Units(){}

    public static Transform getTransform(Unit source, Unit target){
        if (!source.isCompatible(target)){
            throw new InvalidArgumentException("Units are not compatible");
        }
        final Transform sourceToBase = source.getToBase();
        final Transform baseToTarget = target.getFromBase();
        return ConcatenateTransform.create(sourceToBase, baseToTarget);
    }

}
