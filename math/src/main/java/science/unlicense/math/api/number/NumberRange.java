
package science.unlicense.math.api.number;

import science.unlicense.common.api.number.Number;


/**
 * Numeric range.
 *
 * @author Johann Sorel
 */
public interface NumberRange {

    /**
     *
     * @return true is minimum value is included in range
     */
    boolean isMinimumInclusive();

    /**
     *
     * @return true is maximum value is included in range
     */
    boolean isMaximumInclusive();

    /**
     *
     * @return range minimum value
     */
    Number getMinimum();

    /**
     *
     * @return range maximum value
     */
    Number getMaximum();

    /**
     * Test if a value is within this range.
     *
     * @param candidate tested value
     * @return true if value is inside range.
     */
    boolean contains(Number candidate);

    /**
     * Test if a range is within this range.
     *
     * @param candidate tested range
     * @return true if range is inside range.
     */
    boolean contains(NumberRange candidate);

    /**
     * Test if a range intersects this range.
     *
     * @param candidate tested range
     * @return true if ranges intersects
     */
    boolean intersects(NumberRange candidate);

}
