
package science.unlicense.math.api.unit;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.system.SampleSystem;

/**
 * A measured system define units associated to tuple components.
 * A Measure is a one dimension Tuple(Scalar) with a MeasuredSystem.
 *
 * @author Johann Sorel
 */
public class MeasuredSystem extends CObject implements SampleSystem {

    private final MeasuredAxis[] dimensions;

    public MeasuredSystem(MeasuredAxis[] dimensions) {
        this.dimensions = dimensions;
    }

    @Override
    public int getNumComponents() {
        return dimensions.length;
    }

    public MeasuredAxis[] getAxis() {
        return dimensions.clone();
    }

    @Override
    public Chars toChars() {
        final MeasuredAxis[] axis = getAxis();
        final CharBuffer cb = new CharBuffer();
        cb.append(new Chars("CS["));
        cb.append(axis[0].toChars());
        for (int i=1;i<axis.length;i++){
            cb.append(',').append(axis[i].toChars());
        }
        cb.append(new Chars("]"));
        return cb.toChars();
    }

    @Override
    public int getHash() {
        int hash = 5;
        hash = 41 * hash + Arrays.computeHash(this.getAxis());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MeasuredSystem other = (MeasuredSystem) obj;
        return Arrays.equals(this.getAxis(), other.getAxis());
    }

}
