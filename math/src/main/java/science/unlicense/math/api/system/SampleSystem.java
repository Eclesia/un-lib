
package science.unlicense.math.api.system;

/**
 *
 * @author Johann Sorel
 */
public interface SampleSystem {

    /**
     * Get number of samples defining this system.
     *
     * @return number of sample values in the system.
     */
    int getNumComponents();


}
