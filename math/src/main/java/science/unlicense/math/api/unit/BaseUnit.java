package science.unlicense.math.api.unit;

/**
 * <div class="fr">Une unité, qui n'est pas forcément fondamentale dans sa définition, mais que l'on ne souhaite pas
 * décomposer dans le cadre d'une base donnée. Par exemple, le radian est une unité dérivée du rapport de deux
 * longueurs, et peut être considérée comme une unité de base (c'est à dire ne pas disparaître dans l'expression d'une
 * base) sans être pour autant une unité fondamentale.</div>
 *
 * @author Samuel Andrés
 */
public interface BaseUnit extends Unit {
}
