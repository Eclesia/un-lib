
package science.unlicense.math.api.transform;

import science.unlicense.math.api.Affine;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.impl.AffineN;
import science.unlicense.math.impl.MatrixNxN;

/**
 *
 * @author Johann Sorel
 */
public class DimStackTransform extends AbstractTransform {

    private final Transform[] stack;
    private final int[] inSizes;
    private final int[] outSizes;
    private final int[] inOffset;
    private final int[] outOffset;

    private DimStackTransform(Transform[] stack) {
        super(getInSize(stack), getOutSize(stack));
        this.stack = stack;

        inSizes = new int[stack.length];
        outSizes = new int[stack.length];
        for (int i=0;i<stack.length;i++){
            inSizes[i] = stack[i].getInputDimensions();
            outSizes[i] = stack[i].getInputDimensions();
        }

        inOffset = new int[stack.length];
        outOffset = new int[stack.length];
        for (int i=1;i<stack.length;i++){
            inOffset[i] = inOffset[i-1] + inSizes[i-1];
            outOffset[i] = outOffset[i-1] + outSizes[i-1];
        }
    }

    private static int getInSize(Transform[] stack) {
        int s = 0;
        for (Transform t : stack) {
            s += t.getInputDimensions();
        }
        return s;
    }

    private static int getOutSize(Transform[] stack) {
        int s = 0;
        for (Transform t : stack) {
            s += t.getOutputDimensions();
        }
        return s;
    }

    @Override
    public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        int tinInc = sourceOffset;
        int toutInc = destOffset;
        for (int t=0;t<nbTuple;t++){
            for (int u=0;u<stack.length;u++){
                stack[u].transform(
                        source, tinInc + inOffset[u],
                        dest,   toutInc + outOffset[u],
                        1);
            }
            tinInc += inSize;
            toutInc += outSize;
        }
    }

    @Override
    public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        int tinInc = sourceOffset;
        int toutInc = destOffset;
        for (int t=0;t<nbTuple;t++){
            for (int u=0;u<stack.length;u++){
                stack[u].transform(
                        source, tinInc + inOffset[u],
                        dest,   toutInc + outOffset[u],
                        1);
            }
            tinInc += inSize;
            toutInc += outSize;
        }
    }

    @Override
    public Transform invert() {
        throw new RuntimeException("Uninvertible transform");
    }

    public static Transform create(Transform[] stack){

        final Matrix[] m = new Matrix[stack.length];
        for (int i=0;i<stack.length;i++){
            if (stack[i] instanceof Affine && stack[i].getInputDimensions()==1){
                m[i] = ((Affine) stack[i]).toMatrix();
            } else {
                //can not merge transform
                return new DimStackTransform(stack);
            }
        }

        //merge matrices
        final MatrixRW result = MatrixNxN.create(m.length+1, m.length+1);
        for (int i=0;i<stack.length;i++){
            result.set(i, i,        m[i].get(0, 0));
            result.set(i, m.length, m[i].get(0, 1));
        }
        result.set(m.length, m.length, 1);

        final AffineRW trs = AffineN.create(m.length);
        trs.fromMatrix(result);
        return trs;
    }

}
