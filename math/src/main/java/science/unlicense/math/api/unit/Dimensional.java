package science.unlicense.math.api.unit;

/**
 * Type of entity that can be factorized in the sense of dimensional analysis, in association with a power.
 *
 * <div class="fr">Type d'entité pouvant être mise en facteur au sens de l'analyse dimensionnelle, en association avec
 * une puissance.</div>
 *
 * @author Samuel Andrés
 * @param <D>
 * @see Factor
 */
public interface Dimensional<D extends Dimensional<D>> {
}
