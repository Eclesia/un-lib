
package science.unlicense.math.api;

import science.unlicense.common.api.number.Arithmetic;

/**
 * A scalar is a single dimension Tuple.
 *
 * @author Johann Sorel
 */
public interface Scalar extends Tuple {

    /**
     * Get value at ordinate zero.
     *
     * @return ordinate value
     */
    double get();

    /**
     * Get value at ordinate zero.
     *
     * @return ordinate value
     */
    Arithmetic getNumber();

    /**
     * Get a copy of the scalar value as boolean.
     *
     * @return boolean
     */
    boolean getBoolean();

    /**
     * Get a copy of the scalar value as byte.
     *
     * @return byte
     */
    byte getByte();

    /**
     * Get a copy of the scalar value as short.
     *
     * @return short
     */
    short getShort();

    /**
     * Get a copy of the scalar value as int.
     *
     * @return int
     */
    int getInt();

    /**
     * Get a copy of the scalar value as long.
     *
     * @return long
     */
    long getLong();

    /**
     * Get a copy of the scalar value as float.
     *
     * @return float
     */
    float getFloat();

    /**
     * Get a copy of the scalar value as double.
     *
     * @return double
     */
    double getDouble();

}
