
package science.unlicense.math.api;

/**
 *
 * @author Johann Sorel
 */
public interface SimilarityRW extends Similarity {

    /**
     * Copy values from given transform.
     * @param trs
     */
    void set(Similarity trs);

    /**
     * Set transform from given matrix.
     * Matrix must be orthogonal of size dimension+1.
     * @param trs
     */
    void set(Matrix trs);

    /**
     * Set transform from given matrix.
     * Matrix must be orthogonal of size dimension+1.
     * @param trs
     */
    void set(Affine trs);

    /**
     * Set to identity.
     * This method will send a change event if values have changed.
     */
    void setToIdentity();

    /**
     * Set this transform to given translation.
     * This will reset rotation and scale values.
     *
     * This method will send a change event if values have changed.
     */
    void setToTranslation(double[] trs);


    /**
     * Get transform rotation.
     * Call notifyChanged after if you modified the values.
     *
     * @return Matrix
     */
    @Override
    MatrixRW getRotation();

    /**
     * Get transform scale.
     * Call notifyChanged after if you modified the values.
     *
     * @return Vector
     */
    @Override
    VectorRW getScale();

    /**
     * Get transform translation.
     * Call notifyChanged after if you modified the values.
     *
     * @return Vector
     */
    @Override
    VectorRW getTranslation();

    /**
     * Flag to indicate the transform parameters has changed.
     * This is used to recalculate the general matrix when needed.
     */
    void notifyChanged();

    @Override
    AffineRW invert();

    void invert(SimilarityRW buffer);

    SimilarityRW localInvert();

    SimilarityRW localMultiply(Affine other);

    SimilarityRW localMultiply(Similarity other);

}
