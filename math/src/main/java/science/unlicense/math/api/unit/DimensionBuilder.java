package science.unlicense.math.api.unit;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.math.api.unit.Quantity;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.unit.impl.base.DefaultFactor;

/**
 *
 * @author Samuel Andrés
 */
public class DimensionBuilder {

    private final Chars name;
    private final Quantity quantity;
    private final Sequence factors = new ArraySequence();

    public DimensionBuilder(final Chars name, final Quantity quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public DimensionBuilder addFactor(final Dimension unit, final int pNumerator, final int pDenominator) {
        this.factors.add(new DefaultFactor(unit, pNumerator, pDenominator));
        return this;
    }

    public DimensionBuilder addFactor(Dimension unit, int power) {
        return DimensionBuilder.this.addFactor(unit, power, 1);
    }

    public DimensionBuilder addFactor(final Dimension unit) {
        return DimensionBuilder.this.addFactor(unit, 1);
    }

    public DefinedDimension build() {
        return new DefinedDimension(name, quantity,
                (Factor<Dimension>[]) this.factors.toArray(Factor.class));
    }
}
