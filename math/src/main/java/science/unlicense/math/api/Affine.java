

package science.unlicense.math.api;

import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.math.api.transform.Transform;

/**
 * An affine is a transform which preserve points, straight lines, planes
 * and parallel lines.
 *
 * @author Johann Sorel
 */
public interface Affine extends Transform {

    /**
     * Returns true if matrix is identity.
     *
     * @return true if matrix is identity
     */
    boolean isIdentity();

    /**
     * Get value at coordinate.
     *
     * @param row value row index
     * @param col value column index
     * @return affine cell value
     * @throws InvalidArgumentException if coordinate is out of affine range
     */
    double get(int row, int col) throws InvalidArgumentException;

    /**
     * Get affine row values.
     *
     * @param row row index
     * @return row values
     * @throws InvalidArgumentException if coordinate is out of affine range
     */
    double[] getRow(int row) throws InvalidArgumentException;

    /**
     * Get affine column values.
     *
     * @param col column index
     * @return column values
     * @throws InvalidArgumentException if coordinate is out of affine range
     */
    double[] getCol(int col) throws InvalidArgumentException;

    /**
     * Create inverse affine transform.
     *
     * @return new transform inverse
     */
    Affine invert();

    /**
     * Create inverse affine transform and store the result in given buffer.
     *
     * @param buffer affine to store inverted values
     * @return inverted affine
     */
    AffineRW invert(AffineRW buffer);

    /**
     * Create multiplied affine transform.
     *
     * @param other
     * @return new multiplied transform
     */
    AffineRW multiply(Affine other);

    /**
     * Create multiplied affine transform.
     *
     * @param other
     * @param buffer
     * @return new multiplied transform
     */
    AffineRW multiply(Affine other, AffineRW buffer);

    /**
     * Create a square matrix of size dimentions+1
     * The last matrix line will be [0,...,1]
     *
     * @return matrix
     */
    MatrixRW toMatrix();

    /**
     * Create a square matrix of size dimentions+1
     * The last matrix line will be [0,...,1]
     *
     * @param buffer
     * @return matrix
     */
    MatrixRW toMatrix(MatrixRW buffer);

}
