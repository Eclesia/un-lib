
package science.unlicense.math.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.EventSource;

/**
 * A similarity is the equivalent of a affine transform but preserving angles by avoiding
 * shearing value not rotations.
 * 3 different elements are stored.
 * - rotation matrix
 * - translation vector
 * - scale vector
 *
 * History :
 * At first the project used Matrix for 3d scenes, but this approach starts to raise problems
 * when node are moved around very often (like billboards), the mathematic inaccuracy makes the
 * objects start to distord over time.
 *
 * A good description of the problem can be found here :
 * http://www.altdevblogaday.com/2012/07/03/matrices-rotation-scale-and-drifting/
 *
 *
 * @author Johann Sorel
 */
public interface Similarity extends Affine,EventSource {

    public static final Chars PROPERTY_MATRIX = Chars.constant("Matrix");

    /**
     *
     * @param row
     * @param col
     * @return
     */
    @Override
    double get(int row, int col);

    /**
     * Dimension of the transform.
     * @return int
     */
    int getDimension();

    /**
     * Get transform rotation.
     * Call notifyChanged after if you modified the values.
     *
     * @return Matrix
     */
    Matrix getRotation();

    /**
     * Get transform scale.
     * Call notifyChanged after if you modified the values.
     *
     * @return Vector
     */
    Vector getScale();

    /**
     * Get transform translation.
     * Call notifyChanged after if you modified the values.
     *
     * @return Vector
     */
    Vector getTranslation();

    /**
     * Get a general matrix view of size : dimension+1
     * This matrix combine rotation, scale and translation
     *
     * [R*S, R*S, R*S, T]
     * [R*S, R*S, R*S, T]
     * [R*S, R*S, R*S, T]
     * [  0,   0,   0, 1]
     *
     * @return Matrix, never null
     */
    Matrix viewMatrix();

    /**
     * Get a general inverse matrix view of size : dimension+1
     *
     * @return Matrix, never null
     */
    Matrix viewMatrixInverse();

    /**
     * {@inheritDoc }
     */
    @Override
    int getInputDimensions();

    /**
     * {@inheritDoc }
     */
    @Override
    int getOutputDimensions();

    /**
     * {@inheritDoc }
     */
    @Override
    void transform(double[] in, int sourceOffset, double[] out, int destOffset, int nbTuple);

    /**
     * {@inheritDoc }
     */
    @Override
    void transform(float[] in, int sourceOffset, float[] out, int destOffset, int nbTuple);

    /**
     * Inverse transform a single tuple.
     *
     * @param source tuple, can not be null.
     * @param dest tuple, can be null.
     * @return destination tuple.
     */
    TupleRW inverseTransform(Tuple source, TupleRW dest);

    /**
     * Inverse view of this transform.
     * The returned affine is no modifiable.
     * The returned affine reflects any change made to this NodeTransform
     *
     * @return inverse transform view
     */
    Affine inverse();

    @Override
    AffineRW invert();

    void invert(SimilarityRW buffer);

    @Override
    AffineRW multiply(Affine other);

    SimilarityRW multiply(Similarity other);

    SimilarityRW multiply(Similarity other, SimilarityRW buffer);

    @Override
    MatrixRW toMatrix();

    @Override
    MatrixRW toMatrix(MatrixRW buffer);

}
