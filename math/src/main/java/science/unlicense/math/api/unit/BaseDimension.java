package science.unlicense.math.api.unit;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.Quantity;

/**
 * <div class="fr">
 * <p>
 * Une dimension de base est une dimension élémentaire dans un système d'unités donné.
 * </p>
 * <p>
 * Une dimension de base est caractérisée par un symbole.
 * </p>
 * </div>
 *
 * @author Samuel Andrés
 */
public class BaseDimension extends AbstractDimension {

    private final Chars symbol;

    public BaseDimension(final Chars name, final Quantity quantity, final Chars symbol) {
        super(name, quantity);
        this.symbol = symbol;
    }

    public Chars symbol() {
        return this.symbol;
    }

    @Override
    public Chars toChars() {
        return symbol();
    }
}
