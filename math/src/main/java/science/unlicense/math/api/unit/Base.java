package science.unlicense.math.api.unit;

import java.util.Set;

/**
 * Set of dimensional entity factors.
 *
 * <div class="fr">Ensemble de facteurs d'entités dimensionnelles.</div>
 *
 * @author Samuel Andrés
 * @param <D>
 * @see Factor
 * @see Dimensional
 */
public interface Base<D extends Dimensional<?>> {

    Set<Factor<D>> factors();

    int size();

    /**
     * Remove from base all 0 factors.
     * <div class="fr">Élimine de la base les facteurs de puissance 0.</div>
     */
    void remove0s();

    /**
     * Check if given Base parameters are considered equivalent to this base.
     *
     * <div class="fr">Indique si la base en paramètre peut être considérée comme équivalente à la base courante.</div>
     * @param other
     * @return
     */
    boolean sameBase(final Base other);

    D[] dimensionals();

}
