
package science.unlicense.math.api.unit;


/**
 *
 * @author Samuel Andrés
 * @author Johann Sorel
 */
public final class Quantities {

    // Tableau 1
    public static final Quantity MASS                           = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity TIME                           = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity LENGTH                         = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity THERMODYNAMIC_TEMPERATURE      = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity ELECTRIC_CURRENT               = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity AMOUNT_OF_SUBSTANCE            = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity LUMINOUS_INTENSITY             = new Quantity(Quantity.Type.EXTENSIVE);
    // Tableau 2
    public static final Quantity AREA                           = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity VOLUME                         = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity SPEED                          = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity ANGULAR_SPEED                  = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity UNIT_SPEED                     = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity ACCELERATION                   = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity JERK                           = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity WAVENUMBER                     = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity DENSITY                        = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity SPECIFIC_VOLUME                = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity MAGNETIC_EXCITATION            = new Quantity(Quantity.Type.INTENSIVE); // EXCITATION MAGNÉTIQUE
    public static final Quantity CURRENT_DENSITY                = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity MOLAR_CONCENTRATION            = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity LUMINANCE                      = new Quantity(Quantity.Type.INTENSIVE);
    // Tableau 5
    public static final Quantity ANGLE                          = new Quantity(null);
    public static final Quantity SOLID_ANGLE                    = new Quantity(null);
    // Tableau 3
    public static final Quantity CATALYTIC_ACTIVITY             = new Quantity(null);
    public static final Quantity RADIOACTIVE_DECAY              = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity ELECTRIC_CHARGE                = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity AMOUNT_OF_ELECTRICITY          = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity CAPACITANCE                    = new Quantity(null);
    public static final Quantity ELECTRIC_POTIENTIAL            = new Quantity(null);
    public static final Quantity VOLTAGE                        = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity ELECTROMOTIVE_FORCE            = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity ELECTRIC_CONDUCTANCE           = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity FORCE                          = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity ENERGY                         = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity ABSORBED_DOSE                  = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity EQUIVALENT_DOSE                = new Quantity(null);
    public static final Quantity LUMINOUS_FLUX                  = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity ILLUMINANCE                    = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity MAGNETIC_FLUX                  = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity FREQUENCY                      = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity INDUCTANCE                     = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity MAGNETIC_FLUX_DENSITY          = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity PRESSURE                       = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity POWER                          = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity ELECTRIC_RESISTANCE            = new Quantity(Quantity.Type.EXTENSIVE);
    // Tableau 4
    public static final Quantity ELECTRIC_FIELD                 = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity CHARGE_DENSITY                 = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity THERMAL_CONDUCTIVITY           = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity ABSORBED_DOSE_RATE             = new Quantity(null);
    public static final Quantity EQUIVALENT_DOSE_RATE           = new Quantity(null);
    public static final Quantity THERMAL_FLUX                   = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity IRRADIANCE                     = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity SPECIFIC_ENERGY                = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity MOLAR_ENERGY                   = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity ENERGY_DENSITY                 = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity ENTROPY                        = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity THERMAL_CAPACITY               = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity SPECIFIC_ENTROPY               = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity SPECIFIC_THERMAL_CAPACITY      = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity MOLAR_ENTROPY                  = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity MOLAR_THERMAL_CAPACITY         = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity RADIATION_EXPOSURE             = new Quantity(null);
    public static final Quantity ELECTRIC_DISPLACEMENT_FIELD    = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity RADIANCE                       = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity TORQUE                         = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity MAGNETIC_PERMEABILITY          = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity ELECTRIC_PERMITTIVITY          = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity SURFACE_TENSION                = new Quantity(null);
    public static final Quantity VISCOSITY                      = new Quantity(Quantity.Type.INTENSIVE);
    public static final Quantity VOLUMIC_CAPACITY               = new Quantity(Quantity.Type.EXTENSIVE);
    public static final Quantity MAGNETIC_SUSCEPTIBILITY        = new Quantity(null);
    public static final Quantity ELECTRIC_SUSCEPTIBILITY        = new Quantity(null);

    private Quantities(){};

}
