
package science.unlicense.math.api.system;

/**
 *
 * @author Johann Sorel
 */
public class UndefinedSystem implements SampleSystem {

    private static final SampleSystem[] INSTANCES = new SampleSystem[10];
    static {
        for (int i=0;i<INSTANCES.length;i++) INSTANCES[i] = new UndefinedSystem(i);
    }

    private final int nbComponents;

    public UndefinedSystem(int nbComponents) {
        this.nbComponents = nbComponents;
    }

    @Override
    public int getNumComponents() {
        return nbComponents;
    }

    /**
     * Create a sample system.
     * The 10 first systems are in cache.
     *
     * @param nbComponents
     * @return SampleSystem never null
     */
    public static SampleSystem create(int nbComponents) {
        if (nbComponents<11) return INSTANCES[nbComponents];
        return new UndefinedSystem(nbComponents);
    }

}
