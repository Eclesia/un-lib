package science.unlicense.math.api.unit;

/**
 * Association of a dimensional entity to a power.
 *
 * <div class="fr">Association d'une entité dimentionnelle à une puissance entière.</div>
 *
 * @author Samuel Andrés
 * @param <D>
 */
public interface Factor<D extends Dimensional<?>> {

    D dim();

    double power();

    int powerNumerator();

    int powerDenominator();
}
