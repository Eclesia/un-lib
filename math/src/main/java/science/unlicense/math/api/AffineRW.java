
package science.unlicense.math.api;

import science.unlicense.common.api.exception.InvalidArgumentException;

/**
 * Writable affine transform.
 *
 * @author Johann Sorel
 */
public interface AffineRW extends Affine {

    /**
     * Copy all values from given transform.
     *
     * @param toCopy transform to copy from
     */
    void set(final Affine toCopy);

    /**
     * Set one affine value.
     *
     * @param row value row index
     * @param col value column index
     * @param value new cell value
     * @throws InvalidArgumentException if coordinate is out of affine range
     */
    void set(int row, int col, double value) throws InvalidArgumentException;

    /**
     * Set affine row values.
     *
     * @param row row index
     * @param values new row values
     * @throws InvalidArgumentException if coordinate is out of affine range
     */
    void setRow(int row, double[] values) throws InvalidArgumentException;

    /**
     * Set affine column values.
     *
     * @param col column index
     * @param values new column values
     * @throws InvalidArgumentException if coordinate is out of affine range
     */
    void setCol(int col, double[] values) throws InvalidArgumentException;

    /**
     * Set affine values to identity .
     *
     * @return this affine instance
     */
    AffineRW setToIdentity();

    /**
     * Copy all values from given transform.
     * Given matrix must be affine.
     *
     * @param m matrix to copy from
     * @throws InvalidArgumentException if matrix is not affine
     */
    void fromMatrix(Matrix m) throws InvalidArgumentException;

    /**
     * Inverse this affine transform.
     *
     * @return this affine instance
     */
    AffineRW localInvert();

    /**
     * Scale this affine.
     *
     * @param tuple scaling factor to apply, (one by columns).
     * @return this affine instance
     * @throws InvalidArgumentException if coordinate is out of affine range
     */
    AffineRW localScale(double[] tuple) throws InvalidArgumentException;

    /**
     * Scale this affine.
     *
     * @param scale scaling factor to apply
     * @return this affine instance
     */
    AffineRW localScale(double scale);

    /**
     * Multiply this affine by given affine and store the result in this affine.
     *
     * @param affine multiplying affine
     */
    void localMultiply(Affine affine);

}
