package science.unlicense.math.api.unit;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.Quantity;
import science.unlicense.math.api.unit.Dimensional;

/**
 * <div class="fr">
 * Une dimension est une définition formelle d'une grandeur physique mesurable, propre à un système d'unités. Elle est
 * dotée d'un nom et peut être élémentaire ou définie à partir d'autres dimensions. Dans certains cas particuliers, une
 * dimension peut être à la fois élémentaire et composée : c'est le cas par exemple des angles qui peuvent être vus à la
 * fois en tant que dimension élémentaire ou homogène au rapport de deux longueurs.
 * </div>
 *
 * @author Samuel Andrés
 */
public interface Dimension extends Dimensional<BaseDimension> {

    Chars name();

    Quantity quantity();

}
