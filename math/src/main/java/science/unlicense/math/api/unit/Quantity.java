package science.unlicense.math.api.unit;

/**
 * A Quantity is an abstract magnitude, unrelated to a unit system.
 * <div class="fr">
 * Une quantité est une grandeur abstraite, conceptualisée de manière indépendante d'un système d'unités.
 * </div>
 *
 * @author Samuel Andrés
 * @author Johann Sorel
 */
public final class Quantity {

    public static enum Type {
        EXTENSIVE, INTENSIVE;
    }

    private final Type type;

    Quantity(final Type type) {
        this.type = type;
    }

    public Type type() {
        return this.type;
    }

    public boolean sameQuantity(final Quantity other) {
        return this == other;
    }
}
