
package science.unlicense.math.api.transform;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public class ConcatenateTransform extends AbstractTransform {

    private final Transform trs1;
    private final Transform trs2;
    private final int bufferSize;

    private ConcatenateTransform(Transform trs1, Transform trs2) {
        super(trs1.getInputSystem(), trs2.getOutputSystem());
        CObjects.ensureNotNull(trs1);
        CObjects.ensureNotNull(trs2);
        if (trs1.getOutputDimensions()!= trs2.getInputDimensions()){
            throw new InvalidArgumentException("Transform 1 output size differ from transform 2 input size");
        }

        this.trs1 = trs1;
        this.trs2 = trs2;
        bufferSize = trs1.getOutputDimensions();
    }

    @Override
    public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        final double[] buffer = new double[bufferSize];
        for (int i=0;i<nbTuple;i++){
            trs1.transform(source, sourceOffset+i*inSize, buffer, 0, 1);
            trs2.transform(buffer, 0, dest, destOffset+i*outSize, 1);
        }
    }

    @Override
    public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        final float[] buffer = new float[bufferSize];
        for (int i=0;i<nbTuple;i++){
            trs1.transform(source, sourceOffset+i*inSize, buffer, 0, 1);
            trs2.transform(buffer, 0, dest, destOffset+i*outSize, 1);
        }
    }

    @Override
    public TupleRW transform(Tuple source, TupleRW dest) {
        final VectorRW temp = Vectors.create(trs1.getOutputSystem(), source.getNumericType());
        trs1.transform(source, temp);
        return trs2.transform(temp, dest);
    }

    @Override
    public Transform invert() {
        final Transform itrs1 = trs1.invert();
        final Transform itrs2 = trs2.invert();
        return create(itrs2, itrs1);
    }

    public static Transform create(Transform trs1, Transform trs2){

        if (trs1 instanceof IdentityTransform) {
            return trs2;
        } else if (trs2 instanceof IdentityTransform) {
            return trs1;
        }

        if (trs1 instanceof Affine && trs2 instanceof Affine) {
            final Affine aff1 = (Affine) trs1;
            final Affine aff2 = (Affine) trs2;
            return aff2.multiply(aff1);
        } else if (trs1 instanceof Matrix && trs2 instanceof Matrix) {
            return ((Matrix) trs2).multiply(((Matrix) trs1));
        } else {
            return new ConcatenateTransform(trs1, trs2);
        }

    }

    public static Transform create(Transform trs1, Transform trs2, Transform trs3){
        return create(create(trs1, trs2), trs3);
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("Concatenate"), new Object[]{trs1,trs2});
    }

}
