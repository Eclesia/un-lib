package science.unlicense.math.api.unit;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.Quantity;
import science.unlicense.math.api.unit.Factor;
import science.unlicense.unit.impl.UnitUtil;
import science.unlicense.unit.impl.base.DimensionBaseBuilder;

/**
 * <div class="fr">
 * <p>
 * Une dimension définie est une grandeur physique mesurable définie à partir d'autres grandeurs physiques mesurables.
 * </p>
 * <p>
 * Par exemple, dans le système international, la dimension "tension électrique" peut être définie comme :
 * M·L^(2)·T^(-3)·I^(-1)
 * </p>
 * <p>
 * Une dimension définie est caractérisée par la combinaison des facteurs de ses dimensions de définition qui peuvent
 * être elles-mêmes de base ou définies, ainsi que par la combinaison des facteurs des dimensions de base qui peut en
 * être implicitement déduite.
 * </p>
 * </div>
 *
 * @author Samuel Andrés
 */
public class DefinedDimension extends AbstractDimension {

    private final Factor<Dimension>[] definition;

    public DefinedDimension(final Chars name, final Quantity quantity, final Factor<Dimension>... definition) {
        super(name, quantity);
        this.definition = definition;
    }

    /**
     * Dimensions à partir desquelles la dimension est définie.
     * @return
     */
    public Factor<Dimension>[] definition() {
        return definition;
    }

    @Override
    public Chars toChars() {
        final CharBuffer sb = new CharBuffer();
        boolean first = true;
        for (final Factor<BaseDimension> b : new DimensionBaseBuilder(this, 1, 1).build()) {
            if (first == true){
                first = false;
            } else {
                sb.append('.');
            }
            sb.append(b.dim());
            if (b.power() != 1) {
                sb.append(UnitUtil.exponent(b.powerNumerator()));
            }
        }
        return sb.toChars();
    }
}


