
package science.unlicense.math.api.transform;

import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;

/**
 * A Transformation is a mathematic operation which transform a tuple values.
 * Example : Affine transformation, Matrices, Geographic projections.
 *
 * The number of dimensions may change between input and output.
 *
 * @author Johann Sorel
 */
public interface Transform {

    SampleSystem getInputSystem();

    SampleSystem getOutputSystem();

    /**
     * @return number of dimension expected in input tuple.
     */
    int getInputDimensions();

    /**
     * @return number of dimension expected in output tuple.
     */
    int getOutputDimensions();

    /**
     * Transform a single tuple.
     *
     * @param source tuple array, can not be null.
     * @param dest array, can be null.
     * @return destination tuple.
     */
    double[] transform(double[] source, double[] dest);

    /**
     * Transform N tuples.
     *
     * @param source tuple array, can not be null.
     * @param sourceOffset index where to start.
     * @param dest array, can not be null.
     * @param destOffset index where start inserting converted values.
     * @param nbTuple number of tuples to transform.
     * @return destination array.
     */
    void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple);

    /**
     * Transform a single tuple.
     *
     * @param source tuple array, can not be null.
     * @param dest array, can be null.
     * @return destination tuple.
     */
    float[] transform(float[] source, float[] dest);

    /**
     * Transform N tuples.
     *
     * @param source tuple array, can not be null.
     * @param sourceOffset index where to start.
     * @param dest array, can not be null.
     * @param destOffset index where start inserting converted values.
     * @param nbTuple number of tuples to transform.
     * @return destination array.
     */
    void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple);

    /**
     * Transform a single tuple.
     *
     * @param source tuple, can not be null.
     * @param dest tuple, can be null.
     * @return destination tuple.
     */
    TupleRW transform(Tuple source, TupleRW dest);

    /**
     * Create the inverse transform.
     */
    Transform invert();

}
