
package science.unlicense.math.api.transform;

import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.AffineN;
import science.unlicense.math.impl.MatrixNxN;

/**
 *
 * @author Johann Sorel
 */
public class RearrangeTransform extends AbstractTransform {

    protected final int[] mapping;
    protected int nbOutDim;

    /**
     *
     * @param mapping source to target mapping, tablea has the size of the source tuple.
     */
    private RearrangeTransform(int[] mapping) {
        super(mapping.length, getOutSize(mapping));
        this.mapping = mapping;

        for (int i=0;i<mapping.length;i++){
            if (mapping[i]>=0) nbOutDim++;
        }
    }

    private static int getOutSize(int[] mapping) {
        int n = 0;
        for (int i=0;i<mapping.length;i++){
            if (mapping[i]>=0) n++;
        }
        return n;
    }

    @Override
    public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        //rearrange values
        int srco = sourceOffset;
        int dsto = destOffset;
        for (int i = 0;i < nbTuple; i++) {
            for (int k = 0; k < mapping.length; k++) {
                if (mapping[k] >= 0) dest[dsto + mapping[k]] = source[srco +k];
            }
            srco += mapping.length;
            dsto += nbOutDim;
        }
    }

    @Override
    public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        //rearrange values
        int srco = sourceOffset;
        int dsto = destOffset;
        for (int i = 0; i < nbTuple; i++) {
            for (int k = 0;k < mapping.length; k++) {
                if (mapping[k] >= 0) dest[dsto + mapping[k]] = source[srco + k];
            }
            srco += mapping.length;
            dsto += nbOutDim;
        }
    }

    @Override
    public TupleRW transform(Tuple source, TupleRW dest) {
        if (dest == null) dest = source.create(nbOutDim);
        //rearrange values
        for (int k = 0; k < mapping.length; k++) {
            if (mapping[k] >= 0) {
                dest.set(mapping[k], source.getNumber(k));
            }
        }
        return dest;
    }

    @Override
    public Transform invert() {
        throw new RuntimeException("Uninvertible transform");
    }

    public static Transform create(int[] mapping){
        if (sameSize(mapping)){
            final MatrixRW matrix = MatrixNxN.create(mapping.length+1, mapping.length+1);
            for (int i=0;i<mapping.length;i++){
                if (mapping[i]>=0){
                    matrix.set(mapping[i], i, 1);
                } else {
                    matrix.set(i, i, 1);
                }
            }
            matrix.set(mapping.length, mapping.length, 1);
            AffineRW trs = AffineN.create(mapping.length);
            trs.fromMatrix(matrix);
            return trs;
        } else {
            return new RearrangeTransform(mapping);
        }
    }

    private static boolean sameSize(int[] mapping){
        for (int i=0;i<mapping.length;i++){
            if (mapping[i]<0) return false;
        }
        return true;
    }

}
