package science.unlicense.math.api;

import science.unlicense.common.api.exception.InvalidIndexException;

/**
 *
 * @author Johann Sorel
 */
public interface VectorRW extends Vector, TupleRW{

    /**
     * Convenient method to get values at ordinate [0,1].
     * May throw an exception if ordinates do not exist
     * @return double
     */
    VectorRW getXY();

    /**
     * Convenient method to get values at ordinate [0,1,2].
     * May throw an exception if ordinates do not exist
     * @return double
     */
    VectorRW getXYZ();

    /**
     * Convenient method to get values at ordinate [0,1,2,3].
     * May throw an exception if ordinates do not exist
     * @return double
     */
    VectorRW getXYZW();

    /**
     * Convenient method to set value at ordinate 0.
     *
     * @param x first ordinate value
     * @throws InvalidIndexException if indice is out of range
     */
    void setX(double x) throws InvalidIndexException;

    /**
     * Convenient method to set value at ordinate 1.
     *
     * @param y second ordinate value
     * @throws InvalidIndexException if indice is out of range
     */
    void setY(double y) throws InvalidIndexException;

    /**
     * Convenient method to set value at ordinate 2.
     *
     * @param z third ordinate value
     * @throws InvalidIndexException if indice is out of range
     */
    void setZ(double z) throws InvalidIndexException;

    /**
     * Convenient method to set value at ordinate 3.
     *
     * @param w fourth ordinate value
     * @throws InvalidIndexException if indice is out of range
     */
    void setW(double w) throws InvalidIndexException;

    /**
     * Convenient method to set value at ordinate 0,1.
     *
     * @param x first ordinate value
     * @param y second ordinate value
     * @throws InvalidIndexException if indice is out of range
     */
    void setXY(double x, double y) throws InvalidIndexException;

    /**
     * Convenient method to set value at ordinate 0,1,3.
     *
     * @param x first ordinate value
     * @param y second ordinate value
     * @param z third ordinate value
     * @throws InvalidIndexException if indice is out of range
     */
    void setXYZ(double x, double y, double z) throws InvalidIndexException;

    /**
     * Convenient method to set value at ordinate 0,1,2,3.
     *
     * @param x first ordinate value
     * @param y second ordinate value
     * @param z third ordinate value
     * @param w fourth ordinate value
     * @throws InvalidIndexException if indice is out of range
     */
    void setXYZW(double x, double y, double z, double w) throws InvalidIndexException;

    VectorRW add(Tuple other);

    VectorRW subtract(Tuple other);

    VectorRW multiply(Tuple other);

    VectorRW divide(Tuple other);

    VectorRW scale(double scale);

    VectorRW cross(Tuple other);

    VectorRW lerp(Tuple other, double ratio);

    VectorRW normalize();

    VectorRW negate();

    /**
     * Calculate shortest angle to given vector.
     * formula : acos(dot(vec1,vec2))
     *
     * @param second
     * @return shortest angle in radian
     */
    double shortestAngle(Vector second);

    /**
     * Calculate the projected vector of given vector on this vector.
     *
     * formula :
     * v = this vector
     * u = other vector
     * ((u dot v) / |v|^2 ) * v
     *
     * @param candidate vector to project
     * @return projected vector of candidate on this vector.
     */
    VectorRW project(Tuple candidate);

    /**
     * Calculate reflected vector.
     *
     * @param normal
     * @return
     */
    VectorRW reflect(Tuple normal);

    VectorRW localAdd(double x, double y);

    VectorRW localAdd(double x, double y, double z);

    VectorRW localAdd(double x, double y, double z, double w);

    VectorRW localAdd(Tuple other);

    VectorRW localSubtract(Tuple other);

    VectorRW localMultiply(Tuple other);

    VectorRW localDivide(Tuple other);

    VectorRW localScale(double scale);

    VectorRW localCross(Tuple other);

    VectorRW localNormalize();

    VectorRW localNegate();

    VectorRW localLerp(Tuple other, double ratio);

    VectorRW copy();

}
