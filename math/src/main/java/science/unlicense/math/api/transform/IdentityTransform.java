
package science.unlicense.math.api.transform;

import science.unlicense.common.api.Arrays;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.impl.AbstractAffine;
import science.unlicense.math.impl.MatrixNxN;

/**
 * Transformation doing nothing
 *
 * @author Johann Sorel
 */
public class IdentityTransform extends AbstractAffine {

    private final int dim;

    public IdentityTransform(int dim) {
        super(dim);
        this.dim = dim;
    }

    @Override
    public boolean isIdentity() {
        return true;
    }

    @Override
    public int getInputDimensions() {
        return dim;
    }

    @Override
    public int getOutputDimensions() {
        return dim;
    }

    @Override
    protected void transform1(double[] source, int sourceOffset, double[] dest, int destOffset) {
        Arrays.copy(source, sourceOffset, dim, dest, destOffset);
    }

    @Override
    protected void transform1(float[] source, int sourceOffset, float[] dest, int destOffset) {
        Arrays.copy(source, sourceOffset, dim, dest, destOffset);
    }

    @Override
    public MatrixRW toMatrix() {
        return MatrixNxN.create(dim+1, dim+1).setToIdentity();
    }

    @Override
    public double get(int row, int col) {
        return row==col ? 1 : 0;
    }

    @Override
    public MatrixRW toMatrix(MatrixRW buffer) {
        if (buffer==null) return toMatrix();
        buffer.setToIdentity();
        return buffer;
    }

    @Override
    public IdentityTransform invert() {
        return this;
    }

}
