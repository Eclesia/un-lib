package science.unlicense.math.api.unit;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.unit.Quantity;

/**
 *
 * @author Samuel Andrés
 */
public abstract class AbstractDimension extends CObject implements Dimension {

    private final Chars name;
    private final Quantity quantity;

    public AbstractDimension(final Chars name, final Quantity quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    @Override
    public Quantity quantity() {
        return this.quantity;
    }

    @Override
    public Chars name() {
        return this.name;
    }

    @Override
    public Chars toChars() {
        return name();
    }
}
