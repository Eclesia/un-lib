package science.unlicense.math.api;

import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractVector extends AbstractTuple implements Vector {

    /**
     * {@inheritDoc }
     */
    @Override
    public double getX(){
        return get(0);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public double getY(){
        return get(1);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public double getZ(){
        return get(2);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public double getW(){
        return get(3);
    }

    public Vector getXY() {
        return new Vector2f64(get(0), get(1));
    }

    public Vector getXYZ() {
        return new Vector3f64(get(0), get(1), get(2));
    }

    public Vector getXYZW() {
        return new Vector4f64(get(0), get(1), get(2), get(3));
    }

    public double length(){
        return Vectors.length(toDouble());
    }

    public double lengthSquare(){
        return Vectors.lengthSquare(toDouble());
    }

    public double dot(Tuple other){
        return Vectors.dot(toDouble(), other.toDouble());
    }

    /**
     * Returns true if vector do not contains any NaN or Infinite values.
     *
     * @return true is vector is finite
     */
    public boolean isFinite(){
        final double[] values = toDouble();
        for (int i=0;i<values.length;i++){
            if (Double.isNaN(values[0]) || Double.isInfinite(values[1])){
                return false;
            }
        }
        return true;
    }

    /**
     * Test if two vectors are orthogonal.
     *
     * @param other second vector
     * @param epsilon tolerance value if not exactly zero
     * @return
     */
    public boolean isOrthogonal(Tuple other, double epsilon) {
        final double d = dot(other);
        final double l = length()*Vectors.length(other.toDouble());
        return Math.abs(d/l) < epsilon;
    }

    /**
     * Test if two vectors are parallel
     *
     * @param other second vector
     * @param epsilon tolerance value if not exactly zero
     * @return
     */
    public boolean isParallel(Tuple other, double epsilon) {
        final double d = cross(other).length();
        return Math.abs(d) <= epsilon;
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods without buffer //////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public VectorRW add(Tuple other){
        return add(other,null);
    }

    public VectorRW subtract(Tuple other){
        return subtract(other,null);
    }

    public VectorRW multiply(Tuple other){
        return multiply(other,null);
    }

    public VectorRW divide(Tuple other){
        return divide(other,null);
    }

    public VectorRW scale(double scale){
        return scale(scale,null);
    }

    public VectorRW cross(Tuple other){
        return cross(other,null);
    }

    public VectorRW lerp(Tuple other, double ratio){
        return lerp(other,ratio,null);
    }

    public VectorRW normalize(){
        return normalize(null);
    }

    public VectorRW negate(){
        return negate(null);
    }

    /**
     * Calculate shortest angle to given vector.
     * formula : acos(dot(vec1,vec2))
     *
     * @param second
     * @return shortest angle in radian
     */
    public double shortestAngle(Vector second){
        return Vectors.shortestAngle(toDouble(), second.toDouble());
    }

    /**
     * Calculate the projected vector of given vector on this vector.
     *
     * formula :
     * v = this vector
     * u = other vector
     * ((u dot v) / |v|^2 ) * v
     *
     * @param candidate vector to project
     * @return projected vector of candidate on this vector.
     */
    public Vector project(Tuple candidate){
        return scale(dot(candidate) / lengthSquare());
    }

    /**
     * Calculate reflected vector.
     *
     * @param normal
     * @return
     */
    public Vector reflect(Tuple normal){
        final double[] reflect = Vectors.reflect(toDouble(), normal.toDouble());
        final VectorRW v = VectorNf64.createDouble(reflect.length);
        v.set(reflect);
        return v;
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods with buffer /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public VectorRW add(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.add(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    public VectorRW subtract(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.subtract(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    public VectorRW multiply(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.multiply(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    public VectorRW divide(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.divide(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    public VectorRW scale(double scale, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.scale(toDouble(), scale, array);
        buffer.set(array);
        return buffer;
    }

    public VectorRW cross(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.cross(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    public VectorRW lerp(Tuple other, double ratio, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.lerp(toDouble(), other.toDouble(), ratio, array);
        buffer.set(array);
        return buffer;
    }

    public VectorRW normalize(VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.normalize(toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    public VectorRW negate(VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.negate(toDouble(), array);
        buffer.set(array);
        return buffer;
    }

}
