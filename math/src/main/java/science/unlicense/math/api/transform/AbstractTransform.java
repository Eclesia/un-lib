
package science.unlicense.math.api.transform;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 * Abstract transformation, all calls fallback on N tuple transform methods.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTransform extends CObject implements Transform {

    protected final SampleSystem inputSystem;
    protected final SampleSystem outputSystem;
    protected final int inSize;
    protected final int outSize;

    public AbstractTransform(int inoutSize) {
        this(inoutSize,inoutSize);
    }

    public AbstractTransform(int inSize, int outSize) {
        this(UndefinedSystem.create(inSize), UndefinedSystem.create(outSize));
    }

    public AbstractTransform(SampleSystem inoutSystem) {
        this(inoutSystem, inoutSystem);
    }

    public AbstractTransform(SampleSystem inputSystem, SampleSystem outputSystem) {
        this.inputSystem = inputSystem;
        this.outputSystem = outputSystem;
        inSize = inputSystem.getNumComponents();
        outSize = outputSystem.getNumComponents();
    }

    @Override
    public SampleSystem getInputSystem() {
        return inputSystem;
    }

    @Override
    public SampleSystem getOutputSystem() {
        return outputSystem;
    }

    @Override
    public final int getInputDimensions() {
        return inSize;
    }

    @Override
    public final int getOutputDimensions() {
        return outSize;
    }

    @Override
    public double[] transform(double[] source, double[] dest) {
        if (dest == null) {
            dest = new double[getOutputDimensions()];
        }
        transform(source, 0, dest, 0, 1);
        return dest;
    }

    @Override
    public float[] transform(float[] source, float[] dest) {
        if (dest == null) {
            dest = new float[getOutputDimensions()];
        }
        transform(source, 0, dest, 0, 1);
        return dest;
    }

    @Override
    public TupleRW transform(Tuple source, TupleRW dest) {
        final int outSize = getOutputDimensions();
        if (dest == null) dest = source.create(outSize);

        final int numericType = dest.getNumericType().getPrimitiveCode();
        switch (numericType) {
            case Primitive.BITS1 :
            case Primitive.BITS2 :
            case Primitive.BITS4 :
            case Primitive.INT8 :
            case Primitive.UINT8 :
            case Primitive.INT16 :
            case Primitive.UINT16 :
            case Primitive.INT32 :
            case Primitive.UINT32 :
            case Primitive.INT64 :
            case Primitive.UINT64 :
            case Primitive.FLOAT32 :
                final float[] arrayf = new float[outSize];
                transform(source.toFloat(), 0, arrayf, 0, 1);
                dest.set(arrayf);
                return dest;
            case Primitive.FLOAT64 :
                final double[] arrayd = new double[outSize];
                transform(source.toDouble(), 0, arrayd, 0, 1);
                dest.set(arrayd);
                return dest;
            default :
                throw new UnimplementedException("specific numeric type, transform must be implemented by each implementation.");
        }
    }

}
