
package science.unlicense.math.api;

import science.unlicense.math.impl.VectorNf64;

/**
 * Abstract tuple.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTupleRW extends AbstractTuple implements TupleRW {

    @Override
    public void setAll(double v) {
        final int size = getSampleCount();
        for (int i=0;i<size;i++){
            set(i, 0);
        }
    }

    @Override
    public void set(Tuple toCopy) {
        set(toCopy.toDouble());
    }

    @Override
    public void set(double[] values) {
        final int size = getSampleCount();
        for (int i=0,n=Maths.min(size, values.length);i<n;i++){
            set(i, values[i]);
        }
    }

    @Override
    public void set(float[] values) {
        final int size = getSampleCount();
        for (int i=0,n=Maths.min(size, values.length);i<n;i++){
            set(i, values[i]);
        }
    }

    @Override
    public TupleRW copy() {
        return new VectorNf64(this);
    }

}
