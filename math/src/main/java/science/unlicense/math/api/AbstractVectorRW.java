package science.unlicense.math.api;

import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractVectorRW extends AbstractTupleRW implements VectorRW {

    @Override
    public void setX(double x) {
        set(0, x);
    }

    @Override
    public void setY(double y) {
        set(1, y);
    }

    @Override
    public void setZ(double z) {
        set(2, z);
    }

    @Override
    public void setW(double w) {
        set(3, w);
    }

    @Override
    public void setXY(double x, double y) {
        set(0, x);
        set(1, y);
    }

    @Override
    public void setXYZ(double x, double y, double z) {
        set(0, x);
        set(1, y);
        set(2, z);
    }

    @Override
    public void setXYZW(double x, double y, double z, double w) {
        set(0, x);
        set(1, y);
        set(2, z);
        set(3, w);
    }

    @Override
    public VectorRW getXY() {
        return new Vector2f64(get(0), get(1));
    }

    @Override
    public VectorRW getXYZ() {
        return new Vector3f64(get(0), get(1), get(2));
    }

    @Override
    public VectorRW getXYZW() {
        return new Vector4f64(get(0), get(1), get(2), get(3));
    }

    @Override
    public VectorRW localAdd(double x, double y){
        setX(getX()+x);
        setY(getY()+y);
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y, double z){
        setX(getX()+x);
        setY(getY()+y);
        setZ(getZ()+z);
        return this;
    }

    @Override
    public VectorRW localAdd(double x, double y, double z, double w){
        setX(getX()+x);
        setY(getY()+y);
        setZ(getZ()+z);
        setW(getW()+w);
        return this;
    }

    @Override
    public VectorRW localAdd(Tuple other){
        final double[] values = toDouble();
        Vectors.add(values, other.toDouble(), values);
        set(values);
        return this;
    }

    @Override
    public VectorRW localSubtract(Tuple other){
        final double[] values = toDouble();
        Vectors.subtract(values, other.toDouble(), values);
        set(values);
        return this;
    }

    @Override
    public VectorRW localMultiply(Tuple other){
        final double[] values = toDouble();
        Vectors.multiply(values, other.toDouble(), values);
        set(values);
        return this;
    }

    @Override
    public VectorRW localDivide(Tuple other){
        final double[] values = toDouble();
        Vectors.divide(values, other.toDouble(), values);
        set(values);
        return this;
    }

    @Override
    public VectorRW localScale(double scale){
        final double[] values = toDouble();
        Vectors.scale(values, scale, values);
        set(values);
        return this;
    }

    @Override
    public VectorRW localCross(Tuple other){
        final double[] values = toDouble();
        Vectors.cross(values, other.toDouble(), values);
        set(values);
        return this;
    }

    @Override
    public VectorRW localNormalize(){
        final double[] values = toDouble();
        Vectors.normalize(values, values);
        set(values);
        return this;
    }

    @Override
    public VectorRW localNegate(){
        final double[] values = toDouble();
        Vectors.negate(values, values);
        set(values);
        return this;
    }

    @Override
    public VectorRW localLerp(Tuple other, double ratio){
        final double[] values = toDouble();
        Vectors.lerp(values, other.toDouble(), ratio, values);
        set(values);
        return this;
    }

    @Override
    public VectorRW copy() {
        final VectorRW v = VectorNf64.createDouble(getSampleCount());
        v.set(this);
        return v;
    }

    /**
     * Create a new vector with current vector values and extend it with
     * given value.
     * @param value, value to add at the end of Vector
     * @return Vector
     */
    @Override
    public VectorRW extend(double value){
        int size = getSampleCount();
        final VectorRW v = VectorNf64.createDouble(size+1);
        for (int i=0;i<size;i++) {
            v.set(i, get(i));
        }
        v.set(size, value);
        return v;
    }

    @Override
    public double length(){
        return Vectors.length(toDouble());
    }

    @Override
    public double lengthSquare(){
        return Vectors.lengthSquare(toDouble());
    }

    @Override
    public double dot(Tuple other){
        return Vectors.dot(toDouble(), other.toDouble());
    }

    /**
     * Returns true if vector do not contains any NaN or Infinite values.
     *
     * @return true is vector is finite
     */
    @Override
    public boolean isFinite(){
        final double[] values = toDouble();
        for (int i=0;i<values.length;i++){
            if (Double.isNaN(values[0]) || Double.isInfinite(values[1])){
                return false;
            }
        }
        return true;
    }

    /**
     * Test if two vectors are orthogonal.
     *
     * @param other second vector
     * @param epsilon tolerance value if not exactly zero
     * @return
     */
    @Override
    public boolean isOrthogonal(Tuple other, double epsilon) {
        final double d = dot(other);
        final double l = length()*Vectors.length(other.toDouble());
        return Math.abs(d/l) < epsilon;
    }

    /**
     * Test if two vectors are parallel
     *
     * @param other second vector
     * @param epsilon tolerance value if not exactly zero
     * @return
     */
    @Override
    public boolean isParallel(Tuple other, double epsilon) {
        final double d = cross(other).length();
        return Math.abs(d) <= epsilon;
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods without buffer //////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public VectorRW add(Tuple other){
        return add(other,null);
    }

    @Override
    public VectorRW subtract(Tuple other){
        return subtract(other,null);
    }

    @Override
    public VectorRW multiply(Tuple other){
        return multiply(other,null);
    }

    @Override
    public VectorRW divide(Tuple other){
        return divide(other,null);
    }

    @Override
    public VectorRW scale(double scale){
        return scale(scale,null);
    }

    @Override
    public VectorRW cross(Tuple other){
        return cross(other,null);
    }

    @Override
    public VectorRW lerp(Tuple other, double ratio){
        return lerp(other,ratio,null);
    }

    @Override
    public VectorRW normalize(){
        return normalize(null);
    }

    @Override
    public VectorRW negate(){
        return negate(null);
    }

    /**
     * Calculate shortest angle to given vector.
     * formula : acos(dot(vec1,vec2))
     *
     * @param second
     * @return shortest angle in radian
     */
    @Override
    public double shortestAngle(Vector second){
        return Vectors.shortestAngle(toDouble(), second.toDouble());
    }

    /**
     * Calculate the projected vector of given vector on this vector.
     *
     * formula :
     * v = this vector
     * u = other vector
     * ((u dot v) / |v|^2 ) * v
     *
     * @param candidate vector to project
     * @return projected vector of candidate on this vector.
     */
    @Override
    public VectorRW project(Tuple candidate){
        return scale(dot(candidate) / lengthSquare());
    }

    /**
     * Calculate reflected vector.
     *
     * @param normal
     * @return
     */
    @Override
    public VectorRW reflect(Tuple normal){
        final double[] reflect = Vectors.reflect(toDouble(), normal.toDouble());
        final VectorRW v = VectorNf64.createDouble(reflect.length);
        v.set(reflect);
        return v;
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods with buffer /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    @Override
    public VectorRW add(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.add(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW subtract(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.subtract(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW multiply(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.multiply(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW divide(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.divide(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW scale(double scale, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.scale(toDouble(), scale, array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW cross(Tuple other, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.cross(toDouble(), other.toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW lerp(Tuple other, double ratio, VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.lerp(toDouble(), other.toDouble(), ratio, array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW normalize(VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.normalize(toDouble(), array);
        buffer.set(array);
        return buffer;
    }

    @Override
    public VectorRW negate(VectorRW buffer){
        if (buffer == null) buffer = VectorNf64.createDouble(getSampleCount());
        final double[] array = buffer.toDouble();
        Vectors.negate(toDouble(), array);
        buffer.set(array);
        return buffer;
    }


}
