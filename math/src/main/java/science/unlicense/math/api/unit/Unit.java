package science.unlicense.math.api.unit;

import science.unlicense.common.api.character.Chars;

/**
 * <div class="fr">
 * <p>
 * Une unité est une quantité élémentaire de dimension (ou un étalon de dimension), c'est à dire une quantité
 * élementaire de grandeur physique mesurable d'un système d'unités.
 * </p>
 * <p>
 * Exemples dans le système international d'unités :
 * </p>
 * <ol>
 * <li>le kilogramme est une quantité élementaire de masse</li>
 * <li>la seconde est une quantité élémentaire de temps</li>
 * <li>le mètre est une quantité élémentaire de longueur</li>
 * <li>le kelvin est une quantité élémenaire de température thermodinamique</li>
 * <li>l'ampère est une quantité élémentaire d'intensité électrique</li>
 * <li>le mole est une quantité élémentaire de quantité de matière</li>
 * <li>la candela est une quantité élémentaire d'intensité lumineuse</li>
 * </ol>
 * <p>Une unité est composée d'un symbole et s'applique à un certain nombre de grandeurs physiques mesurables d'un
 * système d'unités.</p>
 * </div>
 *
 * @author Samuel Andrés
 */
public interface Unit extends Dimensional<BaseUnit> {

    /**
     *
     * @return <span class="fr">le nom de l'unité</span>
     */
    Chars name();

    /**
     *
     * @return <span class="fr">le symbole de l'unité</span>
     */
    Chars symbol();

    /**
     *
     * @return <span class="fr">des noms secondaires de l'unité</span>
     */
    Chars[] alternativeNames();

    /**
     *
     * @return <span class="fr">des symboles secondaires de l'unité</span>
     */
    Chars[] alternativeSymbols();

    /**
     *
     * @return  <span class="fr">les grandeurs physiques du système d'unités auxquelles l'unité s'applique</span>
     */
    Dimension[] dimension();

}
