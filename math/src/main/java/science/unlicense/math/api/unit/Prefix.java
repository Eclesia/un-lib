package science.unlicense.math.api.unit;

import science.unlicense.common.api.character.Chars;
import science.unlicense.math.impl.Affine1;

/**
 *
 * @author Samuel Andrés
 */
public class Prefix {

    private final Chars prefix;
    private final Affine1 trs;

    public Prefix(final double a, final Chars prefix) {
        this.prefix = prefix;
        this.trs = new Affine1(a, 0.0);
    }

    public Chars getPrefix() {
        return this.prefix;
    }

    public Affine1 getTransformation() {
        return trs;
    }
}
