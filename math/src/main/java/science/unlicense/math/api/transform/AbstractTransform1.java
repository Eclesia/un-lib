
package science.unlicense.math.api.transform;

/**
 * Abstract transformation, all calls fallback on N tuple transform methods.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTransform1 extends AbstractTransform implements Transform1 {

    public AbstractTransform1() {
        super(1);
    }

    @Override
    public void transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        for (int i=0;i<nbTuple;i++) {
            dest[destOffset+i] = transform(source[sourceOffset+i]);
        }
    }

    @Override
    public void transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        for (int i=0;i<nbTuple;i++) {
            dest[destOffset+i] = transform(source[sourceOffset+i]);
        }
    }

    @Override
    public float transform(float number) {
        return (float) transform((double) number);
    }

}
