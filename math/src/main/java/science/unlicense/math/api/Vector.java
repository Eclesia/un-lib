package science.unlicense.math.api;

import science.unlicense.common.api.exception.InvalidIndexException;

/**
 *
 * @author Johann Sorel
 */
public interface Vector extends Tuple{

    /**
     * Convenient method to get value at ordinate 0.
     *
     * @throws InvalidIndexException if indice is out of range
     * @return double
     */
    double getX() throws InvalidIndexException;

    /**
     * Convenient method to get value at ordinate 1.
     *
     * @throws InvalidIndexException if indice is out of range
     * @return double
     */
    double getY() throws InvalidIndexException;

    /**
     * Convenient method to get value at ordinate 2.
     *
     * @throws InvalidIndexException if indice is out of range
     * @return double
     */
    double getZ() throws InvalidIndexException;

    /**
     * Convenient method to get value at ordinate 3.
     *
     * @throws InvalidIndexException if indice is out of range
     * @return double
     */
    double getW() throws InvalidIndexException;

    /**
     * Convenient method to get values at ordinate [0,1].
     *
     * @return Vector
     * @throws InvalidIndexException if indice is out of range
     */
    Vector getXY() throws InvalidIndexException;

    /**
     * Convenient method to get values at ordinate [0,1,2].
     *
     * @return Vector
     * @throws InvalidIndexException if indice is out of range
     */
    Vector getXYZ() throws InvalidIndexException;

    /**
     * Convenient method to get values at ordinate [0,1,2,3].
     *
     * @return Vector
     * @throws InvalidIndexException if indice is out of range
     */
    Vector getXYZW() throws InvalidIndexException;

    double length();

    double lengthSquare();

    double dot(Tuple other);

    /**
     * Returns true if vector do not contains any NaN or Infinite values.
     *
     * @return true is vector is finite
     */
    boolean isFinite();

    /**
     * Test if two vectors are orthogonal.
     *
     * @param other second vector
     * @param epsilon tolerance value if not exactly zero
     * @return
     */
    boolean isOrthogonal(Tuple other, double epsilon);

    /**
     * Test if two vectors are parallel
     *
     * @param other second vector
     * @param epsilon tolerance value if not exactly zero
     * @return
     */
    boolean isParallel(Tuple other, double epsilon);

    ////////////////////////////////////////////////////////////////////////////
    // methods without buffer //////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    VectorRW add(Tuple other);

    VectorRW subtract(Tuple other);

    VectorRW multiply(Tuple other);

    VectorRW divide(Tuple other);

    VectorRW scale(double scale);

    VectorRW cross(Tuple other);

    VectorRW lerp(Tuple other, double ratio);

    VectorRW normalize();

    VectorRW negate();

    /**
     * Calculate shortest angle to given vector.
     * formula : acos(dot(vec1,vec2))
     *
     * @param second
     * @return shortest angle in radian
     */
    double shortestAngle(Vector second);

    /**
     * Calculate the projected vector of given vector on this vector.
     *
     * formula :
     * v = this vector
     * u = other vector
     * ((u dot v) / |v|^2 ) * v
     *
     * @param candidate vector to project
     * @return projected vector of candidate on this vector.
     */
    Vector project(Tuple candidate);

    /**
     * Calculate reflected vector.
     *
     * @param normal
     * @return
     */
    Vector reflect(Tuple normal);

    ////////////////////////////////////////////////////////////////////////////
    // methods with buffer /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    VectorRW add(Tuple other, VectorRW buffer);

    VectorRW subtract(Tuple other, VectorRW buffer);

    VectorRW multiply(Tuple other, VectorRW buffer);

    VectorRW divide(Tuple other, VectorRW buffer);

    VectorRW scale(double scale, VectorRW buffer);

    VectorRW cross(Tuple other, VectorRW buffer);

    VectorRW lerp(Tuple other, double ratio, VectorRW buffer);

    VectorRW normalize(VectorRW buffer);

    VectorRW negate(VectorRW buffer);

    /**
     * Create a new vector with current vector values and extend it with
     * given value.
     * @param value, value to add at the end of Vector
     * @return Vector
     */
    VectorRW extend(double value);

    /**
     * Create a new Vector of a different size but similar storage.
     * The returned vector must have at least the same NumericType.
     *
     * @param size
     * @return
     */
    @Override
    VectorRW create(int size);
}
