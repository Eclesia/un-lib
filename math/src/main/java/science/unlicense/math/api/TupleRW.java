
package science.unlicense.math.api;

import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;

/**
 * A tuple is an ordered list of numeric values.
 *
 * @author Johann Sorel
 */
public interface TupleRW extends Tuple {

    /**
     * Set all values to given value.
     *
     * @param v new value for all elements.
     */
    void setAll(double v);

    /**
     * Set value at given ordinate.
     *
     * @param indice ordinate
     * @param value new value
     * @throws InvalidIndexException if indice is out of range
     */
    void set(final int indice, final double value) throws InvalidIndexException;

    /**
     * Set value at given ordinate.
     *
     * @param indice ordinate
     * @param value new value
     * @throws InvalidIndexException if indice is out of range
     */
    void set(final int indice, final Arithmetic value) throws InvalidIndexException;

    /**
     * Copy values from tuple.
     * If toCopy.values.length superior to this.values.length, then the only
     * this.values.length values of toCopy are copied.
     *
     * @param toCopy tuple to copy
     */
    void set(Tuple toCopy);

    /**
     * Copy values from array.
     * If values.length superior to this.values.length, then the only
     * this.values.length values of values are copied.
     *
     * @param values array to copy
     */
    void set(double[] values);

    /**
     * Copy values from array.
     * If values.length superior to this.values.length, then the only
     * this.values.length values of values are copied.
     *
     * @param values array to copy
     */
    void set(float[] values);

    /**
     * Copy this Tuple.
     *
     * @return new tuple copy
     */
    @Override
    TupleRW copy();

}
