
package science.unlicense.math.api;

import science.unlicense.common.api.Obj;
import science.unlicense.common.api.exception.InvalidIndexException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.ArithmeticType;
import science.unlicense.math.api.system.SampleSystem;

/**
 * A tuple is an ordered list of numeric values.
 *
 * @author Johann Sorel
 */
public interface Tuple extends Obj {

    /**
     * Get description of tuple samples.
     *
     * @return Sample sysmte, never null
     */
    SampleSystem getSampleSystem();

    /**
     * Natural numeric type stored in this tuple.
     *
     * @return NumericType never null
     */
    ArithmeticType getNumericType();

    /**
     * Get number of samples.
     *
     * @return number of samples
     */
    int getSampleCount();

    /**
     * Get value at ordinate.
     *
     * @param indice ordinate
     * @throws InvalidIndexException if indice is out of range
     * @return ordinate value
     */
    double get(final int indice) throws InvalidIndexException;

    /**
     * Get value at ordinate.
     *
     * @param indice ordinate
     * @throws InvalidIndexException if indice is out of range
     * @return ordinate value
     */
    Arithmetic getNumber(final int indice) throws InvalidIndexException;

    /**
     * Copy a range of samples to a new tuple.
     *
     * @param start inclusive
     * @param end exclusive
     * @throws InvalidIndexException if an indice is out of range
     * @return double
     */
    Tuple getRange(int start, int end) throws InvalidIndexException;

    /**
     * @return true if all values are set to given value.
     */
    boolean isAll(double value);

    /**
     * Check that all values are not NaN or Infinites.
     * @return true if valid
     */
    boolean isValid();

    /**
     * Get a copy of the tuple values as booleans..
     *
     * @return boolean array
     */
    boolean[] toBoolean();

    /**
     * Get a copy of the tuple values as booleans..
     *
     * @param buffer buffer to write into, not null
     * @param offset offset in the buffer where to start writing
     */
    void toBoolean(boolean[] buffer, int offset);

    /**
     * Get a copy of the tuple values as bytes..
     *
     * @return byte array
     */
    byte[] toByte();

    /**
     * Get a copy of the tuple values as bytes..
     *
     * @param buffer buffer to write into, not null
     * @param offset offset in the buffer where to start writing
     */
    void toByte(byte[] buffer, int offset);

    /**
     * Get a copy of the tuple values as shorts.
     *
     * @return short array
     */
    short[] toShort();

    /**
     * Get a copy of the tuple values as shorts.
     *
     * @param buffer buffer to write into, not null
     * @param offset offset in the buffer where to start writing
     */
    void toShort(short[] buffer, int offset);

    /**
     * Get a copy of the tuple values as ints.
     *
     * @return int array
     */
    int[] toInt();

    /**
     * Get a copy of the tuple values as ints.
     *
     * @param buffer buffer to write into, not null
     * @param offset offset in the buffer where to start writing
     */
    void toInt(int[] buffer, int offset);

    /**
     * Get a copy of the tuple values as longs.
     *
     * @return long array
     */
    long[] toLong();

    /**
     * Get a copy of the tuple values as longs.
     *
     * @param buffer buffer to write into, not null
     * @param offset offset in the buffer where to start writing
     */
    void toLong(long[] buffer, int offset);

    /**
     * Get a copy of the tuple values as floats.
     *
     * @return float array
     */
    float[] toFloat();

    /**
     * Returns a copy of the array values casted to float.
     *
     * @param buffer buffer to write into, not null
     * @param offset offset in the buffer where to start writing
     */
    void toFloat(float[] buffer, int offset);

    /**
     * Get a copy of the tuple values.
     *
     * @return double array
     */
    double[] toDouble();

    /**
     * Copies and returns the array values.
     *
     * @param buffer buffer to write into, not null
     * @param offset offset in the buffer where to start writing
     */
    void toDouble(double[] buffer, int offset);

    /**
     * Get a copy of the tuple values.
     *
     * @return number array
     */
    Arithmetic[] toNumber();

    /**
     * Copies and returns the array values.
     *
     * @param buffer buffer to write into, not null
     * @param offset offset in the buffer where to start writing
     */
    void toNumber(Arithmetic[] buffer, int offset);

    /**
     * Copy this Tuple.
     *
     * @return new tuple copy
     */
    Tuple copy();

    /**
     * Test equality with a tolerance margin.
     *
     * @param obj
     * @param tolerance
     * @return true if tuple are equal
     */
    boolean equals(Tuple obj, double tolerance);

    /**
     * Create a new Tuple of a different size but similar storage.
     * The returned tuple must have at least the same NumericType.
     *
     * @param size
     * @return
     */
    TupleRW create(int size);

}
