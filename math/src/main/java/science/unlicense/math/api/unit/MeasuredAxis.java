
package science.unlicense.math.api.unit;

import science.unlicense.math.api.unitold.Units;
import science.unlicense.math.api.unitold.Unit;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class MeasuredAxis extends CObject {

    public static final MeasuredAxis UNDEFINED = new MeasuredAxis(Units.UNDEFINED);

    protected final Unit unit;

    public MeasuredAxis(Unit unit) {
        this.unit = unit;
    }

    public Unit getUnit() {
        return unit;
    }

    @Override
    public Chars toChars() {
        return new Chars("Axis[").concat(unit.toChars()).concat(']');
    }

    @Override
    public int getHash() {
        int hash = 7;
        hash = 67 * hash + (this.unit != null ? this.unit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MeasuredAxis other = (MeasuredAxis) obj;
        return !(this.unit != other.unit && (this.unit == null || !this.unit.equals(other.unit)));
    }

}
