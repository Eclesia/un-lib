
package science.unlicense.math.api;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.MishandleException;
import science.unlicense.common.api.number.Arithmetic;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Number;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Vectors;

/**
 * Abstract tuple.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTuple extends CObject implements Tuple {

    @Override
    public SampleSystem getSampleSystem() {
        return UndefinedSystem.create(getSampleCount());
    }

    @Override
    public Tuple getRange(int start, int end) {
        final VectorRW v = Vectors.create(UndefinedSystem.create(end-start), getNumericType());
        for (int i=start;i<end;i++) v.set(i, get(i));
        return v;
    }

    @Override
    public boolean isAll(double value) {
        final int size = getSampleCount();
        for (int i=0;i<size;i++){
            if (get(i)!=value) return false;
        }
        return true;
    }

    @Override
    public boolean isValid() {
        final int size = getSampleCount();
        for (int i=0;i<size;i++){
            double v = get(i);
            if (Double.isInfinite(v) || Double.isNaN(v)){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean[] toBoolean() {
        final boolean[] buffer = new boolean[getSampleCount()];
        toBoolean(buffer, 0);
        return buffer;
    }

    @Override
    public void toBoolean(boolean[] buffer, int offset) {
        for (int i=0,n=getSampleCount();i<n;i++) {
            buffer[i+offset] = (byte) getArithmeticAsNumber(i).toInteger() != 0;
        }
    }

    @Override
    public byte[] toByte() {
        final byte[] buffer = new byte[getSampleCount()];
        toByte(buffer, 0);
        return buffer;
    }

    @Override
    public void toByte(byte[] buffer, int offset) {
        for (int i=0,n=getSampleCount();i<n;i++) {
            buffer[i+offset] = (byte) getArithmeticAsNumber(i).toInteger();
        }
    }

    @Override
    public short[] toShort() {
        final short[] buffer = new short[getSampleCount()];
        toShort(buffer, 0);
        return buffer;
    }

    @Override
    public void toShort(short[] buffer, int offset) {
        for (int i=0,n=getSampleCount();i<n;i++) {
            buffer[i+offset] = (short) getArithmeticAsNumber(i).toInteger();
        }
    }

    @Override
    public int[] toInt() {
        final int[] buffer = new int[getSampleCount()];
        toInt(buffer, 0);
        return buffer;
    }

    @Override
    public void toInt(int[] buffer, int offset) {
        for (int i=0,n=getSampleCount();i<n;i++) {
            buffer[i+offset] = getArithmeticAsNumber(i).toInteger();
        }
    }

    @Override
    public long[] toLong() {
        final long[] buffer = new long[getSampleCount()];
        toLong(buffer, 0);
        return buffer;
    }

    @Override
    public void toLong(long[] buffer, int offset) {
        for (int i=0,n=getSampleCount();i<n;i++) {
            buffer[i+offset] = getArithmeticAsNumber(i).toLong();
        }
    }

    @Override
    public float[] toFloat() {
        final float[] buffer = new float[getSampleCount()];
        toFloat(buffer, 0);
        return buffer;
    }

    @Override
    public void toFloat(float[] buffer, int offset) {
        for (int i=0,n=getSampleCount();i<n;i++) {
            buffer[i+offset] = getArithmeticAsNumber(i).toFloat();
        }
    }

    @Override
    public double[] toDouble() {
        final double[] buffer = new double[getSampleCount()];
        toDouble(buffer, 0);
        return buffer;
    }

    @Override
    public void toDouble(double[] buffer, int offset) {
        for (int i=0,n=getSampleCount();i<n;i++) {
            buffer[i+offset] = getArithmeticAsNumber(i).toDouble();
        }
    }

    @Override
    public Arithmetic[] toNumber() {
        final Arithmetic[] buffer = new Arithmetic[getSampleCount()];
        toNumber(buffer, 0);
        return buffer;
    }

    @Override
    public TupleRW copy() {
        VectorRW v = Vectors.create(getSampleSystem(), getNumericType());
        v.set(this);
        return v;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Tuple) {
            return equals((Tuple) obj, 0);
        }
        return false;
    }

    @Override
    public boolean equals(Tuple obj, double tolerance) {
        if (obj == null) {
            return false;
        }
        final int size = getSampleCount();
        if (size != obj.getSampleCount()) {
            return false;
        }

        for (int i=0;i<size;i++) {
            if (!Float64.equals(get(i), obj.get(i), tolerance)){
                return false;
            }
        }
        return true;
    }

    protected final Number getArithmeticAsNumber(int index) {
        return arithmeticAsNumber(getNumber(index));
    }

    protected final Number arithmeticAsNumber(Arithmetic art) {
        if (art instanceof Number) {
            return (Number) art;
        }
        throw new MishandleException("Tuple is not composed of numbers " +art.getClass());
    }

    protected static int[] expand(final int[] values, int value){
        final int[] exp = Arrays.copy(values, 0, values.length+1);
        exp[values.length] = value;
        return exp;
    }

    protected static long[] expand(final long[] values, long value){
        final long[] exp = Arrays.copy(values, 0, values.length+1);
        exp[values.length] = value;
        return exp;
    }

    protected static double[] expand(final double[] values, double value){
        final double[] exp = Arrays.copy(values, 0, values.length+1);
        exp[values.length] = value;
        return exp;
    }

    protected static float[] expand(final float[] values, float value){
        final float[] exp = Arrays.copy(values, 0, values.length+1);
        exp[values.length] = value;
        return exp;
    }

    /**
     *
     * @return the Chars representation of this.
     */
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(getClass().getSimpleName());
        cb.append('[');
        for (int i=0,n=getSampleCount();i<n;i++) {
            if (i!=0) cb.append(',');
            cb.append(Float64.encode(get(i)));
        }
        cb.append(']');
        return cb.toChars();
    }

}
