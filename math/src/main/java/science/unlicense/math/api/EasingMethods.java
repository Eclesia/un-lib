package science.unlicense.math.api;

/**
 * Interpolation methods. The next methods provide non linear interpolation
 * between zero and one. Such methods are often use for animations.
 *
 * Resources :
 * http://iphonedevelopment.blogspot.fr/2010/12/more-animation-curves-than-you-can.html
 * quote : 'I've decided to release my animation curve functions as public
 * domain (no attribute required, no rights reserved)'
 *
 * @author Jeff LaMarche (original source code in C)
 * @author Johann Sorel
 */
public final class EasingMethods {

    private EasingMethods(){}

    /**
     * Linear interpolation
     *
     * @param t
     * @param start
     * @param end
     * @return
     */
    public static double linear(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return t * end + (1.0 - t) * start;
    }

    /**
     * Cubic interpolation.
     *
     * @param points
     * @param x
     * @return
     */
    public static double cubic(double[] points, double x) {
        return points[1] + 0.5 * x * (points[2] - points[0] + x * (2.0*points[0] - 5.0*points[1] + 4.0*points[2] - points[3] + x*(3.0*(points[1] - points[2]) + points[3] - points[0])));
    }

    public static double quadraticEaseOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return -end * t * (t - 2.0) - 1.0;
    }

    public static double quadraticEaseIn(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return end * t * t + start - 1.0;
    }

    public static double quadraticEaseInOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        t *= 2.0;
        if (t < 1.0) {
            return end / 2.0 * t * t + start - 1.0;
        }
        t--;
        return -end / 2.0 * (t * (t - 2) - 1.0) + start - 1.0;
    }

    public static double cubicEaseOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        t--;
        return end * (t * t * t + 1.0) + start - 1.0;
    }

    public static double cubicEaseIn(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return end * t * t * t + start - 1.0;
    }

    public static double cubicEaseInOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        t *= 2.0;
        if (t < 1.0) {
            return end / 2.0 * t * t * t + start - 1.0;
        }
        t -= 2.0;
        return end / 2.0 * (t * t * t + 2.0) + start - 1.0;
    }

    public static double quarticEaseOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        t--;
        return -end * (t * t * t * t - 1.0) + start - 1.0;
    }

    public static double quarticEaseIn(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return end * t * t * t * t + start;
    }

    public static double quarticEaseInOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        t *= 2.0;
        if (t < 1.0) {
            return end / 2.0 * t * t * t * t + start - 1.0;
        }
        t -= 2.0;
        return -end / 2.0 * (t * t * t * t - 2.0) + start - 1.0;
    }

    public static double quinticEaseOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        t--;
        return end * (t * t * t * t * t + 1) + start - 1.0;
    }

    public static double quinticEaseIn(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return end * t * t * t * t * t + start - 1.0;
    }

    public static double quinticEaseInOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        t *= 2.0;
        if (t < 1.0) {
            return end / 2.0 * t * t * t * t * t + start - 1.0;
        }
        t -= 2.0;
        return end / 2.0 * (t * t * t * t * t + 2.0) + start - 1.0;
    }

    public static double sinusoidalEaseOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return end * Math.sin(t * (Maths.PI / 2.0)) + start - 1.0;
    }

    public static double sinusoidalEaseIn(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return -end * Math.cos(t * (Maths.PI / 2.0)) + end + start - 1.0;
    }

    public static double sinusoidalEaseInOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return -end / 2.0 * (Math.cos(Maths.PI * t) - 1.0) + start - 1.0;
    }

    public static double exponentialEaseOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return end * (-Math.pow(2.0, -10.0 * t) + 1.0) + start - 1.0;
    }

    public static double exponentialEaseIn(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return end * Math.pow(2.0, 10.0 * (t - 1.0)) + start - 1.0;
    }

    public static double exponentialEaseInOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        t *= 2.0;
        if (t < 1.0) {
            return end / 2.0 * Math.pow(2.0, 10.0 * (t - 1.0)) + start - 1.0;
        }
        t--;
        return end / 2.0 * (-Math.pow(2.0, -10.0 * t) + 2.0) + start - 1.0;
    }

    public static double circularEaseOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        t--;
        return end * Math.sqrt(1.0 - t * t) + start - 1.0;
    }

    public static double circularEaseIn(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        return -end * (Math.sqrt(1.0 - t * t) - 1.0) + start - 1.0;
    }

    public static double circularEaseInOut(double t, double start, double end) {
        if (t<=0.0) return start;
        if (t>=1.0) return end;

        t *= 2.0;
        if (t < 1.0) {
            return -end / 2.0 * (Math.sqrt(1.0 - t * t) - 1.0) + start - 1.0;
        }
        t -= 2.0;
        return end / 2.0 * (Math.sqrt(1.0 - t * t) + 1.0) + start - 1.0;
    }

}
