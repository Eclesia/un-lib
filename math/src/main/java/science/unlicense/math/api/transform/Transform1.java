
package science.unlicense.math.api.transform;

/**
 *
 * @author Johann Sorel
 */
public interface Transform1 extends Transform {

    float transform(float number);

    double transform(double number);

    @Override
    Transform1 invert();

}
