
package science.unlicense.format.commonmark;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * Resources :
 * http://spec.commonmark.org/0.22/    Version 0.18 (2015-03-03)
 *
 *
 * @author Johann Sorel
 */
public class CMFormat extends DefaultFormat{

    public static final CMFormat INSTANCE = new CMFormat();

    private CMFormat() {
        super(new Chars("MARKDOWN"));
        shortName = new Chars("MarkDwon");
        longName = new Chars("Common MarkDown");
        extensions.add(new Chars("md"));
        extensions.add(new Chars("markdown"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
