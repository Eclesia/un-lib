
package science.unlicense.format.commonmark;

import science.unlicense.common.api.character.Chars;

/**
 * Common Mark constants
 * http://commonmark.org/
 *
 * @author Johann Sorel
 */
public class CMConstants {

    public static final Chars RULE_SETEXHEADER = Chars.constant("setextheader");
    public static final Chars RULE_HRUL = Chars.constant("hrul");
    public static final Chars RULE_ATXHEADER = Chars.constant("atxheader");
    public static final Chars RULE_CODEBLOCK = Chars.constant("codeblock");
    public static final Chars RULE_LINK = Chars.constant("link");
    public static final Chars RULE_LISTITEM = Chars.constant("listitem");
    public static final Chars RULE_LINE = Chars.constant("line");

    public static final Chars RULE_DIA16 = Chars.constant("dia16");
    public static final Chars RULE_INLINE = Chars.constant("inline");

    public static final Chars TOKEN_DIA = Chars.constant("DIA");

    private CMConstants() {}

}
