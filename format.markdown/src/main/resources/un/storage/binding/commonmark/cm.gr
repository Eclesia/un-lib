
# CommonMark documentation grammar
# author : Johann Sorel

# PREPROCESS token part
$D : [0-9] ;
$L : ([a-z]|[A-Z]|[_$]) ;
$S : (-|\+)? ;
# NOTE : @ should be in the list but it's just for emails
$C : [^' '\t\n\r\!\"\#\$\%\&\'\(\)\*\+\-\.\/\:\;\<\=\>\?\[\\\]\^\_\`\{\|\}\~];
$P : [\t\!\"\#\$\%\&\'\(\)\*\+\-\.\/\:\;\<\=\>\?\@\[\\\]\^\_\`\{\|\}\~];

# TOKENS used in lexer
DEFAULT : {
    ENDL    : \n|\r|(\r\n) ;
    SP      : ' ' ;
    UDL     : _ ;
    AST     : \* ;
    MIN     : - ;
    PLUS    : + ;
    DIA     : \#;
    EQUAL   : \=;
    FENCE1  : \`;
    FENCE2  : \~;
    LA      : \[;
    RA      : \];
    LP      : \(;
    RP      : \);
    DDOT    : \:;
    WORD    : ($C|$D)+ ;
    ESCWORD : (\"([^\\\"]|(\\.))*\")|\'([^\\\"]|(\\.))*\' ;
    PONCT   : $P;
}

CODE : {
    CHARS   : [^\n\r]* ;
}


# RULES used in parser
ws      : SP ;
threesp : SP (SP SP?)? ;
foursp  : SP SP SP SP ;
ows     : (SP+ (ENDL SP*)?) | (ENDL SP*) ;

fulludl : UDL ws* UDL ws* UDL ws* (UDL ws*)* ;
fullast : AST ws* AST ws* AST ws* (AST ws*)* ;
fullmin : MIN ws* MIN ws* MIN ws* (MIN ws*)* ;
fulleql : EQUAL ws* EQUAL ws* EQUAL ws* (EQUAL ws*)* ;

literal : WORD|PONCT|UDL|AST|MIN|PLUS|DIA|EQUAL|LA|RA|LP|RP|DDOT ;
inline  : literal (SP* literal)*;

# 4 Leaf blocks
# 4.1 Horizontal rules
##TODO : we should have fullmin also here but it conflicts with Setext
hrul    : threesp? (fulludl|fullast) ;


# 4.2 ATX headers
dia16     : DIA (DIA (DIA (DIA (DIA DIA?)?)?)?)? ;
atxheader : threesp? dia16 ws+ inline (DIA|ws)* ;


# 4.3 Setext headers
setextheader : threesp? inline ENDL threesp? (fullmin|fulleql) ;


# 4.4 Indented code blocks
nomd/CODE : CHARS ;
codeblock : foursp nomd ;

# 4.5 Fenced code blocks
# 4.6 HTML blocks
# 4.7 Link reference definitions
linklabel : LA inline RA ;
link : threesp? linklabel ows? LP ows? inline ows? RP ws*;

# 4.8 Paragraphs
# 4.9 Blank lines
# 5.1 Block quotes
# 5.2 List items
listitem : (AST|MIN|PLUS) SP+ inline SP*;

# 6 Inlines


line : (WORD SP* inline?);
block : setextheader
          | hrul
          | atxheader
          | codeblock
          | link
          | listitem
          | line;

file : (block? ENDL)* block? ;