
package science.unlicense.format.commonmark;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class CMtoHTMLTest {

    @Test
    public void testCM() throws IOException{

        final Chars md = new Chars("- one\n- two\n- three");

        Chars html = new CMtoHTML().writeToChars(md);
        html = html.replaceAll(new Chars(" "), Chars.EMPTY);
        html = html.replaceAll(new Chars("\n"), Chars.EMPTY);
        html = html.replaceAll(new Chars("\t"), Chars.EMPTY);

        Assert.assertEquals(new Chars("<ul><li>one</li><li>two</li><li>three</li></ul>"), html);
    }

}
