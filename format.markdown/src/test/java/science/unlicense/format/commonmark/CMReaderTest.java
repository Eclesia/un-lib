
package science.unlicense.format.commonmark;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.parser.SyntaxNode;

/**
 *
 * @author Johann Sorel
 */
public class CMReaderTest {

    @Test
    public void testPlainText() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/format/commonmark/PlainText.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(sn);

    }

    @Test
    public void testHorizontalRule() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/format/commonmark/HorizontalRule.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(Nodes.toCharsTree(sn, 20));

    }

    @Test
    public void testATXHeader() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/format/commonmark/ATXHeader.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(Nodes.toCharsTree(sn, 20));

    }

    @Test
    public void testCodeBlock() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/format/commonmark/CodeBlock.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(Nodes.toCharsTree(sn, 20));

    }

    @Ignore
    @Test
    public void testFencedCodeBlock() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/format/commonmark/FencedCodeBlock.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(Nodes.toCharsTree(sn, 20));

    }

    @Test
    public void testLink() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/format/commonmark/Link.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(Nodes.toCharsTree(sn, 20));

    }

    @Test
    public void testList() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/format/commonmark/List.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(Nodes.toCharsTree(sn, 20));

    }

}
