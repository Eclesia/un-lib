
package science.unlicense.format.png.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.png.PNGMetadata;

/**
 *
 * @author Johann Sorel
 */
public class tRNS extends Chunk {

    public byte[] datas;

    public tRNS() {
        super(PNGMetadata.MD_tRNS);
    }

    public void read(DataInputStream ds, int length) throws IOException {
        datas = ds.readFully(new byte[length]);
    }

}
