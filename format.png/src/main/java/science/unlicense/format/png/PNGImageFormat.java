
package science.unlicense.format.png;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;
import static science.unlicense.format.png.PNGConstants.SIGNATURE;

/**
 *
 * @author Johann Sorel
 */
public class PNGImageFormat extends AbstractImageFormat {

    public static final PNGImageFormat INSTANCE = new PNGImageFormat();

    private PNGImageFormat() {
        super(new Chars("png"));
        shortName = new Chars("PNG");
        longName = new Chars("Portable Network Graphics");
        mimeTypes.add(new Chars("image/png"));
        extensions.add(new Chars("png"));
        signatures.add(SIGNATURE);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return true;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new PNGImageStore(this, source);
    }

}
