
package science.unlicense.format.png;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.ImageResource;
import science.unlicense.image.api.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class PNGImageStore extends AbstractStore implements ImageResource {

    public PNGImageStore(ImageFormat format, Object input) {
        super(format, input);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return true;
    }

    @Override
    public ImageReader createReader() throws IOException {
        final PNGImageReader reader = new PNGImageReader();
        reader.setInput(source);
        return reader;
    }

    @Override
    public ImageWriter createWriter() throws IOException {
        final PNGImageWriter writer = new PNGImageWriter();
        writer.setOutput(source);
        return writer;
    }

    @Override
    public Chars getId() {
        return null;
    }

}
