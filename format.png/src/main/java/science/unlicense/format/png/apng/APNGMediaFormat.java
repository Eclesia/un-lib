
package science.unlicense.format.png.apng;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import static science.unlicense.format.png.PNGConstants.CHUNK_IEND;
import static science.unlicense.format.png.PNGConstants.SIGNATURE;
import science.unlicense.media.api.AbstractMediaCapabilities;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.MediaCapabilities;

/**
 * PNG extension for animated images.
 *
 * @author Johann Sorel
 */
public class APNGMediaFormat extends AbstractMediaFormat{

    public static final APNGMediaFormat INSTANCE = new APNGMediaFormat();

    public APNGMediaFormat() {
        super(new Chars("apng"));
        shortName = new Chars("Animated-PNG");
        longName = new Chars("Animated-PNG");
        mimeTypes.add(new Chars("image/png"));
        extensions.add(new Chars("png"));
        extensions.add(new Chars("apng"));
        signatures.add(SIGNATURE);
        isSubset = true;
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    public Store open(Object input) throws IOException {
        return new APNGMediaStore((Path) input);
    }

    public boolean canDecode(Object input) throws IOException {
        if (super.canDecode(input)){
            //check if this png contains an acTL chunk
            boolean isApng = false;

            final boolean[] closeStream = new boolean[1];
            final ByteInputStream is = IOUtilities.toInputStream(input, closeStream);
            final DataInputStream ds = new DataInputStream(is, Endianness.BIG_ENDIAN);
            //skip signature
            ds.skipFully(8);
            //quick loop on chunks
            while (true){
                //read chunk header
                final int length = ds.readInt();
                final byte[] btype = ds.readFully(new byte[4]);
                final Chars type = new Chars(btype);
                //skip chunk data + crc
                ds.skipFully(length + 4);

                if (APNGMetaModel.CHUNK_acTL.equals(type)){
                    isApng = true;
                    break;
                } else if (CHUNK_IEND.equals(type)){
                    //we have finish reading file
                    break;
                }
            }
            if (closeStream[0]) is.dispose();

            return isApng;
        }
        return false;
    }



}
