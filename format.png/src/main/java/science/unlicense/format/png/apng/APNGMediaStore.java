
package science.unlicense.format.png.apng;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.image.api.ImageSetMetadata;
import science.unlicense.format.png.PNGImageReader;
import science.unlicense.format.png.apng.model.acTL;
import science.unlicense.format.png.apng.model.fcTL;
import science.unlicense.format.png.apng.model.fdAT;
import science.unlicense.format.png.model.IDAT;
import science.unlicense.format.png.model.IHDR;
import science.unlicense.media.api.ImageTrack;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;

/**
 *
 * @author Johann Sorel
 */
public class APNGMediaStore extends AbstractStore implements Media {

    private final Path path;

    //caches
    private ImageTrack videoMeta = null;
    private IHDR imageInfo = null;
    private acTL videoInfo = null;
    private Sequence frames = null;

    public APNGMediaStore(Path input) {
        super(APNGMediaFormat.INSTANCE,input);
        this.path = input;
    }

    /**
     * Create a PNG reader with extended chunks support.
     *
     * @return PNGImageReader
     * @throws IOException
     */
    private PNGImageReader createReader() throws IOException{
        final PNGImageReader reader = new PNGImageReader();
        reader.getChunkTable().add(APNGMetaModel.CHUNK_acTL, acTL.class);
        reader.getChunkTable().add(APNGMetaModel.CHUNK_fcTL, fcTL.class);
        reader.getChunkTable().add(APNGMetaModel.CHUNK_fdAT, fdAT.class);
        reader.setInput(path);
        return reader;
    }

    @Override
    public Track[] getTracks() throws IOException {
        if (videoMeta == null){
            final PNGImageReader reader = createReader();
            final ImageSetMetadata metas = (ImageSetMetadata) reader.getMetadata(new Chars("imageset"));
            final TypedNode imageMeta = metas.getChild(ImageSetMetadata.MD_IMAGE.getId());
            final TypedNode pngMeta = (TypedNode) reader.getMetadata(new Chars("png"));
            buildFrames(pngMeta);

            final int nbFrame;
            if (videoInfo==null){
                nbFrame = 1;
            } else {
                nbFrame = videoInfo.getNumFrames();
            }

            videoMeta = new ImageTrack() {

                public TypedNode getImageMetadata() {
                    return imageMeta;
                }

                public int getNbFrames() {
                    return nbFrame;
                }

                public int getFrameRate() {
                    return 0;
                }

                public Chars getName() {
                    return new Chars("video");
                }
            };

        }
        return new Track[]{videoMeta};
    }

    private void buildFrames(TypedNode mdPNG){
        frames = new ArraySequence();

        final Iterator chunks = mdPNG.getChildren().createIterator();
        APNGFrame frame = null;
        while (chunks.hasNext()) {
            final Object chunk = chunks.next();
            if (chunk instanceof IHDR){
                imageInfo = (IHDR) chunk;
            } else if (chunk instanceof acTL){
                videoInfo = (acTL) chunk;
            } else if (chunk instanceof fcTL){
                frame = new APNGFrame();
                frame.fcTL = (fcTL) chunk;
                frames.add(frame);
            } else if (chunk instanceof IDAT && frame != null){
                frame.datas.add(chunk);
            }
        }

    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        //ensure metas are loaded
        getTracks();
        //create reader
        final PNGImageReader reader = createReader();
        //ensure metas are loaded
        reader.getMetadataNames();

        return new APNGVideoReader(reader, frames, imageInfo);
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
