
package science.unlicense.format.png.model;

import science.unlicense.common.api.model.tree.NodeCardinality;
import science.unlicense.format.png.PNGMetadata;

/**
 *
 * @author Johann Sorel
 */
public class IDAT extends Chunk {

    public IDAT() {
        super(PNGMetadata.MD_IDAT);
    }

    /**
     * Size of the header before image datas.
     * @return
     */
    public int getHeaderSize(){
        return 0;
    }

    protected IDAT(NodeCardinality card) {
        super(card);
    }

}
