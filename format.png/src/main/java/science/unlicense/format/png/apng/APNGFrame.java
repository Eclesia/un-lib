
package science.unlicense.format.png.apng;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.format.png.apng.model.fcTL;

/**
 * Group fcTL and fDat/Idat which makes a video frame.
 *
 * @author Johann Sorel
 */
public class APNGFrame {

    public fcTL fcTL;
    /** sequence of IDAT */
    public final Sequence datas = new ArraySequence();

}
