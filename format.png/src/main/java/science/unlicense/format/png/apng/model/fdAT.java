
package science.unlicense.format.png.apng.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.png.apng.APNGMetaModel;
import static science.unlicense.format.png.apng.APNGMetaModel.*;
import science.unlicense.format.png.model.IDAT;

/**
 *
 * @author Johann Sorel
 */
public class fdAT extends IDAT {

    public fdAT() {
        super(APNGMetaModel.MD_fdAT);
    }

    public int getHeaderSize(){
        return 4;
    }

    public void read(DataInputStream ds, int length) throws IOException {
        addChild(MD_fcTL_SEQUENCENUMBER, ds.readInt());
        ds.skipFully(length-4);
    }

}
