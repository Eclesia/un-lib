
package science.unlicense.format.png;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class PNGConstants {

    /** first bytes of all PNG file */
    public static final byte[] SIGNATURE = new byte[]{(byte) 137,(byte) 80,(byte) 78,(byte) 71,(byte) 13,(byte) 10,(byte) 26,(byte) 10};
    /** last bytes of all PNG file : IEND chunk, size+chunkName+crc32 */
    public static final byte[] IENDCHUNK = new byte[]{0,0,0,0,73,69,78,68,-82,66,96,-126};

    public static final Chars CHUNK_IHDR = Chars.constant("IHDR");
    public static final Chars CHUNK_PLTE = Chars.constant("PLTE");
    public static final Chars CHUNK_IDAT = Chars.constant("IDAT");
    public static final Chars CHUNK_IEND = Chars.constant("IEND");
    public static final Chars CHUNK_cHRM = Chars.constant("cHRM");
    public static final Chars CHUNK_gAMA = Chars.constant("gAMA");
    public static final Chars CHUNK_iCCP = Chars.constant("iCCP");
    public static final Chars CHUNK_sBIT = Chars.constant("sBIT");
    public static final Chars CHUNK_sRGB = Chars.constant("sRGB");
    public static final Chars CHUNK_bKGD = Chars.constant("bKGD");
    public static final Chars CHUNK_hIST = Chars.constant("hIST");
    public static final Chars CHUNK_tRNS = Chars.constant("tRNS");
    public static final Chars CHUNK_pHYs = Chars.constant("pHYs");
    public static final Chars CHUNK_sPLT = Chars.constant("sPLT");
    public static final Chars CHUNK_tIME = Chars.constant("tIME");
    public static final Chars CHUNK_iTXt = Chars.constant("iTXt");
    public static final Chars CHUNK_tEXt = Chars.constant("tEXt");
    public static final Chars CHUNK_zTXt = Chars.constant("zTXt");
    public static final Chars CHUNK_UNKNOWNED = Chars.constant("uwkd");

    public static final int COLOR_GREYSCALE = 0;        //allowed bit depth : 1, 2, 4, 8, 16
    public static final int COLOR_TRUECOLOUR = 2;       //allowed bit depth : 8, 16
    public static final int COLOR_INDEXED = 3;          //allowed bit depth : 1, 2, 4, 8
    public static final int COLOR_GREYSCALE_ALPHA = 4;  //allowed bit depth : 8, 16
    public static final int COLOR_TRUECOLOUR_ALPHA = 6; //allowed bit depth : 8, 16

    public static final int COMPRESSION_DEFAULT = 0;    // compression method 0, deflate
    public static final int FILTER_DEFAULT = 0;         // filter method 0, only type reconized.
    public static final int INTERLACE_NONE = 0;         // No interlace
    public static final int INTERLACE_ADAM7 = 1;        // Adam7 interlaced
    public static final int FILTER_TYPE_NONE = 0;       // None
    public static final int FILTER_TYPE_SUB = 1;        // Sub
    public static final int FILTER_TYPE_UP = 2;         // Up
    public static final int FILTER_TYPE_AVERAGE = 3;    // Average
    public static final int FILTER_TYPE_PAETH = 4;      // Paeth

    private PNGConstants(){}

}
