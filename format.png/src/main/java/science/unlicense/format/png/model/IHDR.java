
package science.unlicense.format.png.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.png.PNGMetadata;
import static science.unlicense.format.png.PNGMetadata.MD_IHDR_BITDEPTH;
import static science.unlicense.format.png.PNGMetadata.MD_IHDR_COLORTYPE;
import static science.unlicense.format.png.PNGMetadata.MD_IHDR_COMPRESSION;
import static science.unlicense.format.png.PNGMetadata.MD_IHDR_FILTER;
import static science.unlicense.format.png.PNGMetadata.MD_IHDR_HEIGHT;
import static science.unlicense.format.png.PNGMetadata.MD_IHDR_INTERLACE;
import static science.unlicense.format.png.PNGMetadata.MD_IHDR_WIDTH;

/**
 *
 * @author Johann Sorel
 */
public class IHDR extends Chunk {

    public IHDR() {
        super(PNGMetadata.MD_IHDR);
    }

    public void read(DataInputStream ds, int length) throws IOException {
        if (length != 13) throw new IOException(ds, "Invalid IHDR chunk.");
        addChild(MD_IHDR_WIDTH,        ds.readInt());
        addChild(MD_IHDR_HEIGHT,       ds.readInt());
        addChild(MD_IHDR_BITDEPTH,     ds.readUByte());
        addChild(MD_IHDR_COLORTYPE,    ds.readUByte());
        addChild(MD_IHDR_COMPRESSION,  ds.readUByte());
        addChild(MD_IHDR_FILTER,       ds.readUByte());
        addChild(MD_IHDR_INTERLACE,    ds.readUByte());
    }

    public int getWidth(){
        return (Integer) getChild(MD_IHDR_WIDTH).getValue();
    }

    public int getHeight(){
        return (Integer) getChild(MD_IHDR_HEIGHT).getValue();
    }

    public int getBitDepth(){
        return (Integer) getChild(MD_IHDR_BITDEPTH).getValue();
    }

    public int getColorType(){
        return (Integer) getChild(MD_IHDR_COLORTYPE).getValue();
    }

    public int getCompression(){
        return (Integer) getChild(MD_IHDR_COMPRESSION).getValue();
    }

    public int getFilter(){
        return (Integer) getChild(MD_IHDR_FILTER).getValue();
    }

    public int getInterlace(){
        return (Integer) getChild(MD_IHDR_INTERLACE).getValue();
    }


}
