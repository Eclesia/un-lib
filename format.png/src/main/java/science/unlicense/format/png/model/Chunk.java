
package science.unlicense.format.png.model;

import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.NodeCardinality;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.png.PNGMetadata;
import static science.unlicense.format.png.PNGMetadata.*;

/**
 *
 * @author Johann Sorel
 */
public class Chunk extends DefaultTypedNode{

    public Chunk() {
        super(PNGMetadata.MD_uwkd);
    }

    public Chunk(NodeCardinality card) {
        super(card);
    }

    public void read(DataInputStream ds, int length) throws IOException{
        ds.skipFully(length);
    }

    public int getChunkOffset(){
        return (Integer) getChild(MD_CHUNK_DATA_OFFSET).getValue();
    }

    public int getChunkLength(){
        return (Integer) getChild(MD_CHUNK_DATA_LENGTH).getValue();
    }

}
