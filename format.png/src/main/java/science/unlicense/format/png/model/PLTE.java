
package science.unlicense.format.png.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.png.PNGMetadata;
import static science.unlicense.format.png.PNGMetadata.MD_PLTE_TABLE;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;

/**
 *
 * @author Johann Sorel
 */
public class PLTE extends Chunk {

    public Color[] palette = null;

    public PLTE() {
        super(PNGMetadata.MD_PLTE);
    }

    public void read(DataInputStream ds, int length) throws IOException {
        //rebuild palette
        palette = new Color[length/3];
        for (int i=0;i<palette.length;i++){
            palette[i] = new ColorRGB(
                    ds.readUByte(),
                    ds.readUByte(),
                    ds.readUByte());
        }
        addChild(MD_PLTE_TABLE, palette);
    }

    /**
     * Adjust pallete colors with given alphas.
     * @param alphas
     */
    public void fix(tRNS alphas, int colorType) throws IOException{
        if (alphas == null) return;

        if (colorType == 0){
            throw new IOException(null, "Not supported yet.");
        } else if (colorType == 2){
            throw new IOException(null, "Not supported yet.");
        } else if (colorType == 3){
            for (int i=0;i<alphas.datas.length;i++){
                final int alpha = alphas.datas[i] &0xFF;
                final float[] rgba = palette[i].toRGBA();
                rgba[3] = (float) alpha/255f;
                palette[i] = new ColorRGB(rgba);
            }
        } else {
            throw new IOException(null, "Not supported yet.");
        }

    }

}
