
package science.unlicense.format.png.io;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.AbstractInputStream;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Unscan the stream.
 */
public class UnScanInputStream extends AbstractInputStream {

    private final DataInputStream stream;
    private final int nbLine;
    private final int bytePerPixel;
    private final int bitsPerPixel;
    private final int linelength;
    //current and previous scanline :
    // previous byte[0][.]
    // current byte[1][.]
    private final byte[][] lines;
    private int colIndex = 0;
    private int rowIndex = 0;

    public UnScanInputStream(ByteInputStream stream, int scanline, int nbLine, int bitsPerPixel) {
        this.stream = new DataInputStream(stream);
        //if bits are 1,2or4 we fill the line with 0 bits.
        this.bytePerPixel = (bitsPerPixel < 8) ? 1 : bitsPerPixel / 8;
        this.linelength = (scanline * bitsPerPixel) / 8 + ((bitsPerPixel < 8) ? 1 : 0);
        //+1 for filter type
        //+bytePerPixel for dummy left pixel
        this.lines = new byte[2][1 + bytePerPixel + linelength];
        this.bitsPerPixel = bitsPerPixel;
        this.nbLine = nbLine;
        this.colIndex = lines[0].length + 1; //will make sure we read the first scan
    }

    @Override
    public int read() throws IOException {
        if (colIndex >= lines[0].length) {
            if (rowIndex >= nbLine) {
                //we have finish
                return -1;
            }
            rowIndex++;
            //read line, roll scanlines
            byte[] old = lines[0];
            lines[0] = lines[1];
            lines[1] = old;
            stream.readFully(lines[1], bytePerPixel, linelength + 1);
            final int type = lines[1][bytePerPixel];
            //set it to zero, simplifies not having to care about left pixel
            // when reversing scan.
            lines[1][bytePerPixel] = 0;
            //reverse the filter
            for (int i = bytePerPixel + 1; i < lines[1].length; i++) {
                switch (type) {
                    case 0:
                        break; //nothing to do
                    case 1:
                        lines[1][i] = (byte) (lines[1][i] + lines[1][i - bytePerPixel]);
                        break;
                    case 2:
                        lines[1][i] = (byte) (lines[1][i] + lines[0][i]);
                        break;
                    case 3:
                        lines[1][i] = (byte) (lines[1][i] + Math.floor(((lines[1][i - bytePerPixel] & 0xFF) + (lines[0][i] & 0xFF)) / 2));
                        break;
                    case 4:
                        lines[1][i] = (byte) (lines[1][i] + paeth(lines[1][i - bytePerPixel], lines[0][i], lines[0][i - bytePerPixel]));
                        break;
                    default:
                        throw new IOException(this, "Unexpected filter type : " + type);
                }
            }
            colIndex = bytePerPixel + 1;
        }
        final int b = lines[1][colIndex] & 0xff; //unsign it
        colIndex++;
        return b;
    }

    @Override
    public void dispose() throws IOException {
    }

    private static byte paeth(byte ba, byte bb, byte bc) {
        int a = ba & 0xFF;
        int b = bb & 0xFF;
        int c = bc & 0xFF;

        int p = a + b - c;
        int pa = Math.abs(p - a);
        int pb = Math.abs(p - b);
        int pc = Math.abs(p - c);
        if (pa <= pb && pa <= pc) {
            return (byte) a;
        }
        if (pb <= pc) {
            return (byte) b;
        }
        return (byte) c;
    }

    @Override
    public Chars toChars() {
        return new Chars("UnScanInputStream, ").concat(CObjects.toChars(stream));
    }

}
