
package science.unlicense.format.png;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.cryptography.hash.CRC32;
import science.unlicense.encoding.impl.io.zlib.ZlibOutputStream;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.AbstractImageWriter;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageWriteParameters;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 * PNG image writer, images are written as RGBA.
 *
 * @author Johann Sorel
 */
public class PNGImageWriter extends AbstractImageWriter{

    protected void write(Image image, ImageWriteParameters params, ByteOutputStream stream) throws IOException {

        final DataOutputStream ds = new DataOutputStream(stream);

        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGrid tb = cm.asTupleBuffer(image);

        //compression
        final ArrayOutputStream stemp = new ArrayOutputStream();
        final DataOutputStream dstemp = new DataOutputStream(stemp);
        final CRC32 crc = new CRC32();


        //write signature ------------------------------------------------------
        ds.write(PNGConstants.SIGNATURE);

        //write IHDR -----------------------------------------------------------
        stemp.getBuffer().removeAll();

        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final byte bitdepth = 8;
        final byte colortype = PNGConstants.COLOR_TRUECOLOUR_ALPHA;
        final byte compression = PNGConstants.COMPRESSION_DEFAULT;
        final byte filter = PNGConstants.FILTER_DEFAULT;
        final byte interlace = PNGConstants.INTERLACE_NONE;

        dstemp.write(PNGConstants.CHUNK_IHDR.toBytes(CharEncodings.US_ASCII));
        dstemp.writeInt(width);
        dstemp.writeInt(height);
        dstemp.writeByte(bitdepth);
        dstemp.writeByte(colortype);
        dstemp.writeByte(compression);
        dstemp.writeByte(filter);
        dstemp.writeByte(interlace);
        dstemp.flush();
        byte[] array = stemp.getBuffer().toArrayByte();
        crc.reset();
        crc.update(array);
        ds.writeInt(array.length-4);
        ds.write(array);
        ds.writeInt((int) crc.getResultLong());


        //write IDAT -----------------------------------------------------------
        stemp.getBuffer().removeAll();
        stemp.write(PNGConstants.CHUNK_IDAT.toBytes(CharEncodings.US_ASCII));
        final ByteOutputStream zlib = new ZlibOutputStream(stemp);

        //write each color
        final Vector2i32 coord = new Vector2i32();
        final TupleRW tuple = tb.createTuple();
        for (coord.y = 0; coord.y <height; coord.y++) {
            //normaly we should calculate the best scanline,but for now lets just use NONE
            zlib.write((byte) PNGConstants.FILTER_TYPE_NONE);

            for (coord.x = 0; coord.x < width; coord.x++) {
                tb.getTuple(coord, tuple);
                final Color color = new ColorRGB(tuple,cs);
                zlib.write((byte) (color.getRed() * 255));
                zlib.write((byte) (color.getGreen() * 255));
                zlib.write((byte) (color.getBlue() * 255));
                zlib.write((byte) (color.getAlpha() * 255));
            }
        }


        zlib.flush();
        zlib.close();
        stemp.flush();
        array = stemp.getBuffer().toArrayByte();
        crc.reset();
        crc.update(array);
        ds.writeInt(array.length-4);
        ds.write(array);
        ds.writeInt((int) crc.getResultLong());


        //write IEND -----------------------------------------------------------
        ds.write(PNGConstants.IENDCHUNK);

    }

}
