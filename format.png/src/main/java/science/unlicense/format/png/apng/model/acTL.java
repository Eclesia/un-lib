
package science.unlicense.format.png.apng.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.png.apng.APNGMetaModel.*;
import science.unlicense.format.png.model.Chunk;

/**
 *
 * @author Johann Sorel
 */
public class acTL extends Chunk {

    public acTL() {
        super(MD_acTL);
    }

    public void read(DataInputStream ds, int length) throws IOException {
        addChild(MD_acTL_NUMFRAMES, ds.readInt());
        addChild(MD_acTL_NUMPLAYS,  ds.readInt());
    }

    public int getNumFrames(){
        return (Integer) getChild(MD_acTL_NUMFRAMES).getValue();
    }

    public int getNumPlays(){
        return (Integer) getChild(MD_acTL_NUMPLAYS).getValue();
    }

}
