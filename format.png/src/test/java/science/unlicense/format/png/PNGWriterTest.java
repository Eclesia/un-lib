package science.unlicense.format.png;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.ImageWriter;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class PNGWriterTest {

    @Ignore
    @Test
    public void debug() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/png/LongScanLine.png")).createInputStream();
        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);
        final Image image = reader.read(reader.createParameters());

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();
        final TupleRW samples = sm.createTuple();
        sm.getTuple(new Vector2i32(365,1), samples);

        final Chars f = new Chars("/home/eclesia/image.png");
        final ImageWriter writer = new PNGImageWriter();
        writer.setOutput(Paths.resolve(f).createOutputStream());
        writer.write(image, null);

        final ImageReader reader2 = new PNGImageReader();
        reader2.setInput(Paths.resolve(f).createInputStream());
        final Image img2 = reader2.read(reader2.createParameters());


    }

}
