package science.unlicense.format.png;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.IndexedColorModel;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class PNGReaderTest {

    @Test
    public void testTrueColorUncompressed() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/png/TrueColorUncompressed.png")).createInputStream();

        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleGridCursor rawCursor = image.getRawModel().asTupleBuffer(image).cursor();
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor colorCursor = cm.asTupleBuffer(image).cursor();

        final Vector2i32 coord = new Vector2i32();

        //values are in RGB
        coord.x = 0; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{255,0,0}, rawCursor.samples().toInt()); //RED
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 1; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{255,255,0}, rawCursor.samples().toInt()); //YELLOW
        Assert.assertEquals(new ColorRGB(255, 255, 0, 255), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 0; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,255,255}, rawCursor.samples().toInt()); //CYAN
        Assert.assertEquals(new ColorRGB(0, 255, 255, 255), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 1; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,0,255}, rawCursor.samples().toInt()); //BLUE
        Assert.assertEquals(new ColorRGB(0,   0, 255, 255),new ColorRGB(colorCursor.samples(),cs));

    }

    @Test
    public void testTrueColorAlphaUncompressed() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/png/TrueColorAlphaUncompressed.png")).createInputStream();

        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleGridCursor rawCursor = image.getRawModel().asTupleBuffer(image).cursor();
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor colorCursor = cm.asTupleBuffer(image).cursor();

        final Vector2i32 coord = new Vector2i32();

        //values are in RGB
        coord.x = 0; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{255,0,0,255}, rawCursor.samples().toInt()); //RED
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 1; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{255,255,0,206}, rawCursor.samples().toInt()); //YELLOW
        Assert.assertEquals(new ColorRGB(255, 255, 0, 206), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 0; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,255,255,0}, rawCursor.samples().toInt()); //TRANSLUCENT
        Assert.assertEquals(new ColorRGB(0, 255, 255, 0), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 1; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,0,255,255}, rawCursor.samples().toInt()); //BLUE
        Assert.assertEquals(new ColorRGB(0,   0, 255, 255), new ColorRGB(colorCursor.samples(),cs));

    }

    @Test
    public void testTrueColorAlphaCompressed() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/png/TrueColorAlphaCompressed.png")).createInputStream();

        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleGridCursor rawCursor = image.getRawModel().asTupleBuffer(image).cursor();
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor colorCursor = cm.asTupleBuffer(image).cursor();
        final Vector2i32 coord = new Vector2i32();


        //values are in RGB
        coord.x = 0; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{255,0,0,255}, rawCursor.samples().toInt()); //RED
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 1; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{255,255,0,206}, rawCursor.samples().toInt()); //YELLOW
        Assert.assertEquals(new ColorRGB(255, 255, 0, 206), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 0; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,255,255,0}, rawCursor.samples().toInt()); //TRANSLUCENT
        Assert.assertEquals(new ColorRGB(0, 255, 255, 0), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 1; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,0,255,255}, rawCursor.samples().toInt()); //BLUE
        Assert.assertEquals(new ColorRGB(0,   0, 255, 255), new ColorRGB(colorCursor.samples(),cs));

    }

    @Test
    public void testTrueColorCompressedLarge() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/png/TrueColorCompressedLarge.png")).createInputStream();

        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(4, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleGridCursor rawCursor = image.getRawModel().asTupleBuffer(image).cursor();
        final TupleGridCursor colorCursor = image.getColorModel().asTupleBuffer(image).cursor();
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();

        final Vector2i32 coord = new Vector2i32();

        //values are in RGB

        //BLUE
        coord.x = 0; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,0,255}, rawCursor.samples().toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 255, 255), new ColorRGB(colorCursor.samples(),cs));
        //BLACK
        coord.x = 1; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,0,0}, rawCursor.samples().toInt());
        Assert.assertEquals(new ColorRGB(0, 0, 0, 255), new ColorRGB(colorCursor.samples(),cs));
        //WHITE
        coord.x = 2; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{255,255,255}, rawCursor.samples().toInt());
        Assert.assertEquals(new ColorRGB(255, 255, 255, 255), new ColorRGB(colorCursor.samples(),cs));
        //YELLOW
        coord.x = 3; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{255,255,0}, rawCursor.samples().toInt());
        Assert.assertEquals(new ColorRGB(255, 255, 0, 255), new ColorRGB(colorCursor.samples(),cs));
        //RED
        coord.x = 0; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{255,0,0}, rawCursor.samples().toInt());
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), new ColorRGB(colorCursor.samples(),cs));
        //GREEN
        coord.x = 1; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,255,0}, rawCursor.samples().toInt());
        Assert.assertEquals(new ColorRGB(0, 255, 0, 255), new ColorRGB(colorCursor.samples(),cs));
        //CYAN
        coord.x = 2; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0,255,255}, rawCursor.samples().toInt());
        Assert.assertEquals(new ColorRGB(0, 255, 255, 255), new ColorRGB(colorCursor.samples(),cs));
        //GRAY
        coord.x = 3; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{100,100,100}, rawCursor.samples().toInt());
        Assert.assertEquals(new ColorRGB(100, 100, 100, 255), new ColorRGB(colorCursor.samples(),cs));

    }

    /**
     * There was a bug when encountering a 128 byte value in the unscanline.
     * Because of byte values not properly unsigned.
     * this test ensure it won't happen anymore.
     */
    @Test
    public void testTrueColorUncompressed128() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/png/LongScanLine.png")).createInputStream();
        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);
        final Image image = reader.read(reader.createParameters());

        final TupleGrid sm = image.getRawModel().asTupleBuffer(image);
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleRW samples = sm.createTuple();
        sm.getTuple(new Vector2i32(365,1), samples);
        final Color c = new ColorRGB(samples,cs);

    }

    //still bugs, unscan line issue
    @Ignore
    @Test
    public void testIndexed() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/format/png/Indexed.png")).createInputStream();

        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleGridCursor rawCursor = image.getRawModel().asTupleBuffer(image).cursor();
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor colorCursor = cm.asTupleBuffer(image).cursor();
        Assert.assertTrue(cm instanceof IndexedColorModel);

        final Vector2i32 coord = new Vector2i32();

        //values are indexed
        coord.x = 0; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{2}, rawCursor.samples().toInt()); //RED
        Assert.assertEquals(new ColorRGB(255, 0, 0, 255), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 1; coord.y = 0;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{3}, rawCursor.samples().toInt()); //YELLOW
        Assert.assertEquals(new ColorRGB(255, 255, 0, 255), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 0; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{0}, rawCursor.samples().toInt()); //GRAY-LIKE
        Assert.assertEquals(new ColorRGB(101, 45, 103, 255), new ColorRGB(colorCursor.samples(),cs));

        coord.x = 1; coord.y = 1;
        rawCursor.moveTo(coord); colorCursor.moveTo(coord);
        Assert.assertArrayEquals(new int[]{1}, rawCursor.samples().toInt()); //BLUE
        Assert.assertEquals(new ColorRGB(0,   0, 255, 255), new ColorRGB(colorCursor.samples(),cs));

    }
}
