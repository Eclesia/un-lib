
package science.unlicense.format.png;

import science.unlicense.format.png.PNGImageFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class PNGImageFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final ImageFormat[] formats = Images.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof PNGImageFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("Image format not found.");
    }

}
