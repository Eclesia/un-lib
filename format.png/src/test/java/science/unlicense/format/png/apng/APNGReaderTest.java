package science.unlicense.format.png.apng;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageSetMetadata;
import science.unlicense.media.api.ImagePacket;
import science.unlicense.media.api.ImageTrack;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.Medias;
import science.unlicense.media.api.Track;

/**
 *
 * @author Johann Sorel
 */
public class APNGReaderTest {

    @Test
    public void testMediaReader() throws Exception {

        final Path path = Paths.resolve(new Chars("mod:/un/storage/format/apng/clock.png"));

        //check store creation
        final Store store = Medias.open(path);
        Assert.assertNotNull(store);

        //check metadatas
        Media media = (Media) store;
        final Track[] metas = media.getTracks();
        Assert.assertNotNull(metas);
        Assert.assertEquals(1, metas.length);

        final ImageTrack meta = (ImageTrack) metas[0];
        Assert.assertEquals(40, meta.getNbFrames());

        final TypedNode n =  meta.getImageMetadata();
        final TypedNode[] dimensions = n.getChildren(ImageSetMetadata.MD_IMAGE_DIMENSION.getId());
        Assert.assertEquals(2,dimensions.length);
        Assert.assertEquals(new Integer(150), dimensions[0].getChild(ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND.getId()).getValue());
        Assert.assertEquals(new Integer(150), dimensions[1].getChild(ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND.getId()).getValue());

        //check reading
        final MediaReadStream reader = media.createReader(null);
        Assert.assertNotNull(reader);

        //check images are generated
        for (int i=0;i<40;i++){
            final MediaPacket pack = reader.next();
            Assert.assertNotNull(pack);
            Assert.assertTrue(pack instanceof ImagePacket);
            final Image image = ((ImagePacket) pack).getImage();
        }

        //ensure nothing left
        Assert.assertNull(reader.next());

    }

}
