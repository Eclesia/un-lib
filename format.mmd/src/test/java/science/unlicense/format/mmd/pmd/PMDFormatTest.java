
package science.unlicense.format.mmd.pmd;

import science.unlicense.format.mmd.pmd.PMDFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.model3d.api.Model3Ds;

/**
 *
 * @author Johann Sorel
 */
public class PMDFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final Format[] formats = Model3Ds.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof PMDFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("PMD format not found.");
    }

}
