
package science.unlicense.format.mmd.vpd;

import science.unlicense.format.mmd.vpd.VPDFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.model3d.api.Model3Ds;

/**
 *
 * @author Johann Sorel
 */
public class VPDFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final Format[] formats = Model3Ds.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof VPDFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("VPD format not found.");
    }

}
