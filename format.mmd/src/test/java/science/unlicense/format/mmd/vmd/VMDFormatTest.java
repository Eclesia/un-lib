
package science.unlicense.format.mmd.vmd;

import science.unlicense.format.mmd.vmd.VMDFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.model3d.api.Model3Ds;

/**
 *
 * @author Johann Sorel
 */
public class VMDFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final Format[] formats = Model3Ds.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof VMDFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("VMD format not found.");
    }

}
