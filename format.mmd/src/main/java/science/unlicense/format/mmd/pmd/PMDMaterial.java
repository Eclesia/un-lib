package science.unlicense.format.mmd.pmd;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mmd.MMDUtilities;

/**
 * @author Johann Sorel
 */
public class PMDMaterial {

    public float[] diffuse_RGBA;
    public float shinness;
    public float[] specular_RGB;
    public float[] ambiant_RGB;

    /** 255 : no toon effect, otherwise should be 0-9 : index in toon textures */
    public int toon;
    /** draw edges */
    public int edgeFlags;
    /**
     * Texture name, may contain multiple texture separate by '*' .
     */
    public Chars textureName;

    /** face indexes affect by this material. */
    public int[] indices;

    public void setToDefault(){
        diffuse_RGBA    = new float[]{1,0,0,1};
        shinness        = 1f;
        specular_RGB    = new float[]{1,1,1};
        ambiant_RGB     = new float[]{1,1,1};
        toon            = 255;
        edgeFlags       = 0;
        textureName     = Chars.EMPTY;
        indices         = new int[0];
    }

    public void read(DataInputStream ds) throws IOException {
        diffuse_RGBA = ds.readFloat(4);
        shinness     = ds.readFloat();
        specular_RGB = ds.readFloat(3);
        ambiant_RGB  = ds.readFloat(3);
        toon         = ds.readUByte();
        edgeFlags    = ds.readUByte();
        //affected vertices/faces
        final int ulNumIndices = ds.readInt();
        textureName  = ds.readBlockZeroTerminatedChars(20, CharEncodings.SHIFT_JIS);
        indices      = new int[ulNumIndices];
    }

    public void write(DataOutputStream ds) throws IOException{
        ds.writeFloat(diffuse_RGBA);
        ds.writeFloat(shinness);
        ds.writeFloat(specular_RGB);
        ds.writeFloat(ambiant_RGB);
        ds.writeUByte(toon);
        ds.writeUByte(edgeFlags);
        ds.writeInt(indices.length);
        ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(textureName,CharEncodings.SHIFT_JIS,20), 20, CharEncodings.SHIFT_JIS);
    }

}
