package science.unlicense.format.mmd.pmd;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.character.DefaultLChars;
import science.unlicense.common.api.character.LChars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.country.Country;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.mmd.MMDUtilities;
import science.unlicense.format.mmd.pmx.PMXStore;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.MorphTarget;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.model3d.impl.technique.RenderState;
import science.unlicense.model3d.impl.technique.SilhouetteBackFaceTechnique;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.Technique;
import science.unlicense.physics.api.body.BodyGroup;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.constraint.AngleLimitConstraint;
import science.unlicense.physics.api.constraint.Constraint;
import science.unlicense.physics.api.force.Force;
import science.unlicense.physics.api.force.Spring;
import science.unlicense.physics.api.skeleton.IKSolver;
import science.unlicense.physics.api.skeleton.IKSolverCCD;
import science.unlicense.physics.api.skeleton.InverseKinematic;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * @author Johann Sorel
 */
public class PMDStore extends AbstractModel3DStore {

    // special limit for knees IK
    // values are arbitrary, after looking at multiple PMX files (where the limits is expressed)
    // those values seems to come up the most.
    private static final Chars KNEES = new Chars("ひざ");
    private static final double KNEES_ANGLE_MIN = -Maths.PI;
    private static final double KNEES_ANGLE_MAX = -0.008726646;

    private final Path pmdPath;
    //base parent
    private Path basePath;

    private PMDModel model;


    //cache loaded images
    private final Dictionary imageCache = new HashDictionary();

    public PMDStore(Object input) {
        super(PMDFormat.INSTANCE,input);
        pmdPath = ((Path) getInput());
        basePath = pmdPath.getParent();
    }

    public Collection getElements() throws StoreException {
        if (model==null){
            try{
                model = new PMDModel();
                model.read(getSourceAsInputStream());
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }

        final MotionModel node = new DefaultMotionModel(MMDUtilities.COORDSYS);

        //rebuild skeleton
        final Skeleton skeleton = new Skeleton(MMDUtilities.COORDSYS);
        final Joint[] joints = new Joint[model.bones.length];
        for (int i = 0; i < model.bones.length; i++) {
            final PMDBone bone = model.bones[i];

            final Dictionary trs = new HashDictionary();
            trs.add(Country.JPN.asLanguage(), bone.name);
            trs.add(Country.GBR.asLanguage(), bone.englishName);
            final LChars name = new DefaultLChars(trs, Country.JPN.asLanguage());
            final Joint jt = new Joint(3,name);
            jt.setLocalCoordinateSystem(MMDUtilities.COORDSYS);
            jt.getNodeTransform().getTranslation().set(bone.position);
            jt.getNodeTransform().notifyChanged();
            jt.setId(i);
            joints[i] = jt;
        }

        //Second pass to build joints hierarchy
        for (int i=joints.length-1;i>=0;i--){
            final PMDBone bone = model.bones[i];
            if (bone.parentBone >=0){
                joints[bone.parentBone].getChildren().add(joints[i]);
            } else {
                skeleton.getChildren().add(joints[i]);
            }
        }

        //rebuild inverse kinematics
        Arrays.sort(model.kinematics);
        for (int i=0;i<model.kinematics.length;i++){
            final PMDIK pmdik = model.kinematics[i];
            final Joint[] targetJoints = new Joint[pmdik.links.length];
            for (int k=0;k<pmdik.links.length;k++){
                targetJoints[k] = (Joint) skeleton.getJointById(pmdik.links[k]);
                if (targetJoints[k].getTitle().getFirstOccurence(KNEES)>=0){
                    //joint is in the IK chain, PMD more or less expect ik joint to move
                    //only on the X axis for knees joints
                    final BBox limits = new BBox(3);
                    limits.setRange(0, KNEES_ANGLE_MIN, KNEES_ANGLE_MAX);
                    limits.setRange(1, 0, 0);
                    limits.setRange(2, 0, 0);
                    final Constraint cst = new AngleLimitConstraint(targetJoints[k], limits);
                    targetJoints[k].getConstraints().add(cst);
                }
            }
            final IKSolver solver = new IKSolverCCD(pmdik.maxLoop, pmdik.maxAngle, 3);
            final Joint target = (Joint) skeleton.getJointById(pmdik.targetBone);
            final Joint effector = (Joint) skeleton.getJointById(pmdik.effectBone);
            final InverseKinematic ik = new InverseKinematic(target,effector,targetJoints,solver);

            skeleton.getIks().add(ik);
        }

        //we do it before reading part because silhouette will requiere bone positions
        skeleton.reverseWorldPose();
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        //rebuild parts
        for (PMDMaterial material : model.materials){
            final DefaultModel part = buildPartMesh(material);
            node.getChildren().add(part);

            //rebuild skin
            //each vertex is affected by two joints
            final Float32Cursor weightCursor = DefaultBufferFactory.INSTANCE.createFloat32(material.indices.length*2).cursor();
            final Int32Cursor jointCursor = DefaultBufferFactory.INSTANCE.createInt32(material.indices.length*2).cursor();

            for (int i=0;i<material.indices.length;i++) {
                final PMDVertex vertex = model.vertices[material.indices[i]];
                jointCursor.write(vertex.boneIndexes);
                weightCursor.write(vertex.boneWeights);
            }

            final DefaultMesh shell = (DefaultMesh) part.getShape();
            final Skin skin = new Skin();
            skin.setSkeleton(skeleton);
            skin.setMaxWeightPerVertex(2);
            shell.setPositions(shell.getPositions());
            shell.setNormals(shell.getNormals());
            shell.setIndex(shell.getIndex());
            shell.setRanges(shell.getRanges());
            shell.getAttributes().add(Skin.ATT_JOINTS_0, new VBO(jointCursor.getBuffer(),2));
            shell.getAttributes().add(Skin.ATT_WEIGHTS_0, new VBO(weightCursor.getBuffer(),2));
            shell.setUVs(shell.getUVs());
            shell.getMorphs().addAll(shell.getMorphs());
            part.setShape(shell);
            part.setSkin(skin);
            part.updateBoundingBox();

            if (material.edgeFlags>0){
                //models are in decimeter units, edges size seems to be in millimeter : TODO need to confirm this
                //so we divide by 100 on have it in decimeter
                //which color ? which size ?
                part.getTechniques().add(new SilhouetteBackFaceTechnique(Color.BLACK, 2.0f/100));
            }
        }

        node.setSkeleton(skeleton);


        //rebuild physics
        final RigidBody[] bodies = new RigidBody[model.rigidBodies.length];
        if (model.rigidBodies.length>0){
            //create all body groups
            final Dictionary groups = new HashDictionary();
            for (int i=0;i<32;i++){
                groups.add(i, new BodyGroup(new Chars(""+i)));
            }

            for (int i=0;i<model.rigidBodies.length;i++){
                final PMDRigidBody body = model.rigidBodies[i];

                final Geometry shape;
                if (body.shapeType == PMDConstants.SHAPE_SPHERE){
                    shape = new Sphere(body.shapeSize[0]);
                } else if (body.shapeType == PMDConstants.SHAPE_CAPSULE){
                    shape = new Capsule(body.shapeSize[1], body.shapeSize[0]);
                } else if (body.shapeType == PMDConstants.SHAPE_BOX){
                    final double[] lower = VectorNf64.create(body.shapeSize).scale(-1).toDouble();
                    final double[] upper = Arrays.reformatDouble(body.shapeSize);
                    shape = new BBox(lower,upper);

                } else {
                    throw new StoreException("unknowed body type : "+ body.shapeType);
                }
                final RigidBody physicbody = new RigidBody(shape, body.mass);
                physicbody.setLocalCoordinateSystem(MMDUtilities.COORDSYS);
                physicbody.setUpdateMode(RigidBody.UPDATE_PARENT);
                physicbody.setFixed(body.bodyType==0);

                bodies[i] = physicbody;

                //place the physic object
                physicbody.getNodeTransform().getTranslation().set(body.position);
                final Matrix m = Matrix3x3.createFromAngles(body.rotation[0],body.rotation[1],body.rotation[2]);

                physicbody.getNodeTransform().getRotation().set(m);
                physicbody.getNodeTransform().notifyChanged();

                //set material informations
                physicbody.getMaterial().setDensity(1);
                physicbody.getMaterial().setRestitution(body.restitution);
                physicbody.getMaterial().setLinearAttenuation(body.linearAttenuation);
                physicbody.getMaterial().setAngularAttenuation(body.angularAttenuation);
                physicbody.getMaterial().setDynamicFriction(body.friction);
                physicbody.getMaterial().setStaticFriction(body.friction);
                //rebuild groups
                if (body.collisionGroupId!=0){
                    final BodyGroup group = (BodyGroup) groups.getValue(body.collisionGroupId);
                    physicbody.setGroup(group);
                    //rebuild no collision groups
                    for (int k=0;k<32;k++){
                        final boolean nocollide = ((1<<k) & body.collisionGroupMask) == 0;
                        if (nocollide){
                            final BodyGroup no = (BodyGroup) groups.getValue(k);
                            if (no==null){
                                throw new NullPointerException("group should not be null : "+k);
                            }
                            physicbody.addNoCollisionGroup(no);
                        }
                    }
                }

                //attach the related joint
                Joint joint = skeleton.getJointById(body.boneId);
                if (joint!=null){
                    joint.getChildren().add(physicbody);
                    //the physic body is in joint space already
                } else {
                    System.out.println("Physic body is not attached to any bone : "+body);
                }
            }
        }

        //rebuild physic constraints
        for (int i=0;i<model.physicConstraints.length;i++){
            final PMDPhysicConstraint cst = model.physicConstraints[i];
            final RigidBody rbA = bodies[cst.rigidBodyId1];
            final RigidBody rbB = bodies[cst.rigidBodyId2];
            final Tuple centerA = rbA.getWorldPosition(null);
            final Tuple centerB = rbB.getWorldPosition(null);
            final Force force = new Spring(rbA, rbB, 5, 0.5, DistanceOp.distance(centerA, centerB));

            node.getConstraints().add(force);
        }


        final Collection col = new ArraySequence();
        col.add(node);
        return col;
    }

    private DefaultModel buildPartMesh(final PMDMaterial material) {
        final DefaultModel mesh = new DefaultModel();
        mesh.setLocalCoordinateSystem(MMDUtilities.COORDSYS);
        mesh.getTechniques().add(new SimpleBlinnPhong());

        //TODO seems like we are missing an information,
        //some texture should obviously be back culled, yet we don't know which ones
        ((Technique) mesh.getTechniques().get(0)).getState().setCulling(RenderState.CULLING_NONE);

        if (material.textureName != null){
            mesh.setTitle(Paths.stripExtension(material.textureName));
        }

        final Float32Cursor vertexBuffer = DefaultBufferFactory.INSTANCE.createFloat32(material.indices.length*3).cursor();
        final Float32Cursor normalBuffer = DefaultBufferFactory.INSTANCE.createFloat32(material.indices.length*3).cursor();
        final Int32Cursor indexBuffer = DefaultBufferFactory.INSTANCE.createInt32(material.indices.length).cursor();

        for (int i=0;i<material.indices.length;i++) {
            vertexBuffer.write(model.vertices[material.indices[i]].position);
            normalBuffer.write(model.vertices[material.indices[i]].normal);
            indexBuffer.write(i);
        }

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.CARTESIAN3D_METRIC_RIGH_HANDED);
        shell.setPositions(new VBO(vertexBuffer.getBuffer(),3));
        shell.setNormals(new VBO(normalBuffer.getBuffer(),3));
        shell.setIndex(new IBO(indexBuffer.getBuffer()));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) indexBuffer.getBuffer().getNumbersSize())});
        mesh.setShape(shell);


        //set default colors
        final SimpleBlinnPhong.Material meshmat = SimpleBlinnPhong.newMaterial();
        mesh.getMaterials().add(meshmat);
        meshmat.setAmbient(new ColorRGB(material.ambiant_RGB));
        meshmat.setDiffuse(new ColorRGB(material.diffuse_RGBA));
        meshmat.setSpecular(new ColorRGB(material.specular_RGB));

        Path p = null;
        try {
            textured:
            if (material.textureName != null && !material.textureName.isEmpty()) {
                Chars baseName = material.textureName;

                final Sequence imageFiles = PMXStore.splitTextures(basePath, baseName);
                if (imageFiles.isEmpty()){
                    getLogger().info(new Chars("No image files found for : "+baseName));
                    break textured;
                }
                final Sequence images = new ArraySequence();
                for (int i=0,n=imageFiles.getSize();i<n;i++){
                    p = (Path) imageFiles.get(i);
                    Texture2D texture = (Texture2D) imageCache.getValue(p);
                    if (texture==null){
                        //load image
                        Image img = PMXStore.loadImage(p);
                        texture = new Texture2D(img);
                        imageCache.add(p, texture);
                    }
                    images.add(texture);
                }
                if (images.isEmpty()) throw new IOException(getInput(), "No image files could be read.");

                final Float32Cursor uvCursor = DefaultBufferFactory.INSTANCE.createFloat32(material.indices.length*2).cursor();
                for (int i=0;i<material.indices.length;i++) {
                    final int m = material.indices[i];
                    uvCursor.write(model.vertices[m].uv);
                }
                shell.setUVs(new VBO(uvCursor.getBuffer(), 2));

                final TextureMapping[] materialLayers = PMXStore.buildTexture(imageFiles,images);
                meshmat.properties().setPropertyValue(SimpleBlinnPhong.MATERIAL_DIFFUSETEXTURE, materialLayers[0]);
                if (materialLayers.length>1) {
                    meshmat.properties().setPropertyValue(SimpleBlinnPhong.MATERIAL_ALPHATEXTURE, materialLayers[1]);
                }
            }
        } catch (IOException ex) {
            getLogger().warning(new Chars("Failed to load image for path" + CObjects.toChars(p)+""+ex.getMessage()),ex);
        }

        //rebuild morph frames
        rebuildMorphFrames(mesh, material, vertexBuffer.getBuffer());

        mesh.updateBoundingBox();
        return mesh;
    }


    private void rebuildMorphFrames(DefaultModel mesh, PMDMaterial material, Buffer vertexBuffer){
        if (model.morphs.length==0) return;

        //cache vertex indexes from material for faster morph reconstruction
        int minIdx = Integer.MAX_VALUE;
        int maxIdx = Integer.MIN_VALUE;
        for (int i=0,idx,n=material.indices.length;i<n;i++) {
            idx = material.indices[i];
            if (idx<minIdx) minIdx = idx;
            if (idx>maxIdx) maxIdx = idx;
        }
        final int[][] dico = new int[maxIdx-minIdx+1][0];
        for (int i=0,idx,did,n=material.indices.length;i<n;i++) {
            idx = material.indices[i];
            did = idx-minIdx;
            dico[did] = Arrays.insert(dico[did], dico[did].length, i);
        }


        final Sequence ms = new ArraySequence();

        for (int k=0;k<model.morphs.length;k++){
            final PMDMorph morph = model.morphs[k];

            final Float32Cursor copy = DefaultBufferFactory.INSTANCE.createFloat32(material.indices.length*3).cursor();
            copy.offset(0);

            boolean modified = false;
            for (PMDMorph.VertexPosition ele : morph.movements){
                //indexes after the first morph are indirected from the base morph
                final int trueIdx;
                if (k==0){
                    trueIdx = ele.index;
                } else {
                    trueIdx = model.morphs[0].movements[ele.index].index;
                }

                if (trueIdx<minIdx || trueIdx>maxIdx) continue;
                final int[] ref = dico[trueIdx-minIdx];
                if (ref.length>0){
                    modified = true;
                    //replace coords
                    for (int i=0;i<ref.length;i++) {
                        int ri = ref[i]*3;
                        copy.offset(ri);
                        copy.write(ele.position_3f);
                    }
                }
            }
            if (modified){
                final MorphTarget mf = new MorphTarget();
                mf.setName(morph.name);
                mf.setVertices(new VBO(copy.getBuffer(), 3));
                ms.add(mf);
            }
        }

        ((Mesh) mesh.getShape()).getMorphs().addAll(ms);
    }

    /**
     * Writing will search and write only the first MultiPartMesh found in the collection.
     *
     * @param elements
     * @throws IOException
     */
    @Override
    public void writeElements(Collection elements) throws StoreException {
        MotionModel mpm = null;
        for (Iterator ite=elements.createIterator();ite.hasNext();){
            final Object obj = ite.next();
            if (obj instanceof MotionModel){
                mpm = (MotionModel) obj;
                break;
            }
        }

        if (mpm!=null){
            try{
                PMDModel model = toModel(mpm);
                final ByteOutputStream out = getSourceAsOutputStream();
                model.write(out);
                out.close();
            }catch(IOException ex){
                throw new StoreException(ex);
            }
        }
    }

    private PMDModel toModel(MotionModel mpm) throws IOException{
        final PMDModel pmdModel = new PMDModel();
        pmdModel.setToDefault();

        final Vector3f64 tuple = new Vector3f64();

        //counters for dynamic names
        int texturecounter = 0;

        //transform the skeleton and rigid bodies
        final Skeleton skeleton = mpm.getSkeleton();
        if (skeleton!=null){
            final Dictionary buildDico = new HashDictionary();
            final Sequence roots = skeleton.getChildren();
            final Sequence result = new ArraySequence();
            for (int i=0;i<roots.getSize();i++){
                buildSkeleton((Joint) roots.get(i), buildDico, result);
            }

            pmdModel.bones = new PMDBone[result.getSize()];
            Collections.copy(result, pmdModel.bones, 0);
        }

        //transform the mesh
        final Iterator children = mpm.getChildren().createIterator();
        while (children.hasNext()) {
            final Object next = children.next();
            if (next instanceof Model){
                final Model model = (Model) next;
                final Mesh mesh = (Mesh) model.getShape();
                final Skin skin = model.getSkin();

                final int vertexBaseOffset = pmdModel.vertices.length;

                //rebuild vertices
                final VBO vertices = (VBO) mesh.getPositions();
                final VBO normals = (VBO) mesh.getNormals();
                final VBO uvs = (VBO) mesh.getUVs();
                final VBO jointIndexes = (VBO) mesh.getAttributes().getValue(Skin.ATT_JOINTS_0);
                final VBO jointWeights = (VBO) mesh.getAttributes().getValue(Skin.ATT_WEIGHTS_0);
                final VectorRW temp = VectorNf64.createDouble(2);

                final int vn = (int) vertices.getExtent().getL(0);
                pmdModel.vertices = (PMDVertex[]) Arrays.resize(pmdModel.vertices, vertexBaseOffset+vn);

                for (int v=0;v<vn;v++){
                    final PMDVertex vertex = new PMDVertex();
                    vertex.setToDefault();
                    pmdModel.vertices[vertexBaseOffset+v] = vertex;
                    vertices.getTuple(v, tuple);
                    tuple.toFloat(vertex.position, 0);
                    if (normals!=null) {
                        normals.getTuple(v, tuple);
                        tuple.toFloat(vertex.normal, 0);
                    }
                    if (uvs!=null) {
                        uvs.getTuple(v, tuple);
                        vertex.uv[0] = (float) tuple.getX();
                        vertex.uv[1] = (float) tuple.getY();
                    }
                    if (jointIndexes!=null){
                        jointWeights.getTuple(v, temp);
                        final float[] weights = temp.toFloat();
                        jointIndexes.getTuple(v, temp);
                        final int[] indexes = temp.toInt();
                        Arrays.copy(weights, 0, Maths.min(weights.length, 2),vertex.boneWeights, 0);
                        Arrays.copy(indexes, 0, Maths.min(indexes.length, 2),vertex.boneIndexes, 0);
                    }
                }

                //rebuild material
                final SimpleBlinnPhong.Material material = SimpleBlinnPhong.view((Material) model.getMaterials().get(0));
                final PMDMaterial pmdMaterial = new PMDMaterial();
                pmdMaterial.setToDefault();
                pmdModel.materials = (PMDMaterial[]) Arrays.insert(pmdModel.materials, pmdModel.materials.length, pmdMaterial);

                //texture informations
                pmdMaterial.ambiant_RGB = material.getAmbient().toRGB();
                pmdMaterial.diffuse_RGBA = material.getDiffuse().toRGBA();
                pmdMaterial.shinness = (float) material.getShininess();
                pmdMaterial.specular_RGB = material.getSpecular().toRGB();

                final TextureMapping diffuseTexture = material.getDiffuseTexture();
                if (diffuseTexture != null) {
                    final Image texture = diffuseTexture.getTexture().getImage();
                    pmdMaterial.textureName = new Chars("texture"+(texturecounter++)+".bmp");
                    //save texture aside pmd file
                    final Path texturePath = basePath.resolve(pmdMaterial.textureName);
                    Images.write(texture, new Chars("bmp"), texturePath);
                }

                //face indices
                final int[] indexes = Geometries.toInt32(mesh.getIndex());
                pmdMaterial.indices = new int[indexes.length];
                for (int t=0;t<indexes.length;t++){
                    pmdMaterial.indices[t] = vertexBaseOffset + indexes[t];
                }
                pmdModel.faceIndex = Arrays.concatenate(new int[][]{pmdModel.faceIndex,pmdMaterial.indices});
            }
        }

        return pmdModel;
    }

    private void buildSkeleton(Joint joint, Dictionary all, Sequence result){
        final PMDBone bone = new PMDBone();
        bone.setToDefault();
        bone.name = joint.getTitle().toChars();
        bone.englishName = bone.name;
        bone.ik = 0;

        final Affine matrix = joint.getNodeToRootSpace();
        final double[] pos = new double[3];
        matrix.transform(pos,0,pos,0,1);
        bone.position = Arrays.reformatToFloat(pos);

        final Integer parentIndex = (Integer) all.getValue(joint.getParent());
        if (parentIndex!=null){
            bone.parentBone = parentIndex.shortValue();
        }

        all.add(result.getSize(), bone);
        result.add(bone);

        //loop on children
        final Iterator ite = joint.getChildren().createIterator();
        while (ite.hasNext()) {
            final Object next = ite.next();
            if (next instanceof Joint){
                buildSkeleton((Joint) next, all, result);
            } else if (next instanceof RigidBody){
                //TODO
            }
        }

    }

}
