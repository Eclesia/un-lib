package science.unlicense.format.mmd.mvd;

import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.model3d.api.AbstractModel3DStore;


/**
 * @author Johann Sorel
 */
public class MVDStore extends AbstractModel3DStore{


    public MVDStore(Object input) {
        super(MVDFormat.INSTANCE, input);
    }

    public Collection getElements() throws StoreException {
        final ArraySequence col = new ArraySequence();

        return col;
    }

    private void read() throws IOException{

        final ByteInputStream in = getSourceAsInputStream();
        final DataInputStream ds = new DataInputStream(in,Endianness.LITTLE_ENDIAN);

        final Chars signature = ds.readBlockZeroTerminatedChars(30, CharEncodings.UTF_8);
        final float version = ds.readFloat();
        final CharEncoding enc = ds.read() == 0 ?
                                CharEncodings.UTF_16LE :
                                CharEncodings.UTF_8;
        final int jpNameLength = ds.readInt();
        final Chars jpName = ds.readBlockZeroTerminatedChars(jpNameLength, enc);
        final int compatibleNameLength = ds.readInt();
        final Chars compatibleName = ds.readBlockZeroTerminatedChars(compatibleNameLength, enc);
        //unknown
        ds.skipFully(18);
        final int nbBone = ds.readInt();
        final int nbUnknown = ds.readInt();

        final MVDBone[] bones = new MVDBone[nbBone];
        for (int i=0;i<nbBone;i++) {
            bones[i] = new MVDBone();
            bones[i].index = ds.readInt();
            final int length = ds.readInt();
            bones[i].name = ds.readBlockZeroTerminatedChars(length, enc);
        }

        for (int i=0;i<nbBone;i++) {
            final int unknown1 = ds.readUShort();
            final int boneIndex = ds.readInt();
            final int frameDataLength = ds.readInt();
            final int frameCount = ds.readInt();
            final int extraLength = ds.readInt();
            //extra data
            ds.skipFully(extraLength);

            for (int k=0;k<frameCount;k++) {
                if (frameDataLength == 16) {
                    float rotX = ds.readFloat();
                    float weight = ds.readFloat();
                    float rotZ = ds.readFloat();
                    float rotW = ds.readFloat();
                } else if (frameDataLength == 56) {
                    final int unknown2 = ds.readInt();
                    final int frameIndex = ds.readInt();
                    final int unknown3 = ds.readInt();

                    float posX = ds.readFloat();
                    float posY = ds.readFloat();
                    float posZ = ds.readFloat();

                    float rotX = ds.readFloat();
                    float rotY = ds.readFloat();
                    float rotZ = ds.readFloat();
                    float rotW = ds.readFloat();
                    float rrotX = ds.readFloat();
                    float rrotY = ds.readFloat();
                    float rrotZ = ds.readFloat();
                    float rrotW = ds.readFloat();
                } else {
                    throw new IOException(ds, "Unexpected size");
                }
            }
        }
    }

}
