package science.unlicense.format.mmd.pmx;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;


/**
 * Used in editor user interface.
 *
 * @author Johann Sorel
 */
public class PMXDisplaySlot {

    public Chars name;
    public Chars englishName;
    public int flag;
    public PMXSlotElement[] elements;

    public static class PMXSlotElement{
        public int type;
        public int index;
    }

    public void setToDefault(){
        name        = null;
        englishName = null;
        flag        = 0;
        elements    = new PMXSlotElement[0];
    }

    public void read(PMXInputStream ds) throws IOException, StoreException{
        name          = ds.readText();
        englishName   = ds.readText();
        flag          = ds.readUByte();
        elements      = new PMXDisplaySlot.PMXSlotElement[ds.readInt()];
        for (int k=0;k<elements.length;k++){
            elements[k] = new PMXDisplaySlot.PMXSlotElement();
            elements[k].type = ds.readUByte();
            if (elements[k].type==0){
                elements[k].index = ds.readBoneIndex();
            } else if (elements[k].type==1){
                elements[k].index = ds.readMorphIndex();
            } else {
                throw new IOException(ds, "Unknowned slot type : "+elements[k].type);
            }
        }
    }

    public void write(PMXOutputStream ds) throws IOException, StoreException{
        ds.writeText(name);
        ds.writeText(englishName);
        ds.writeUByte(flag);
        ds.writeInt(elements.length);
        for (int k=0;k<elements.length;k++){
            ds.writeUByte(elements[k].type);
            if (elements[k].type==0){
                ds.writeBoneIndex(elements[k].index);
            } else if (elements[k].type==1){
                ds.writeMorphIndex(elements[k].index);
            } else {
                throw new IOException(ds, "Unknowned slot type : "+elements[k].type);
            }
        }
    }

}
