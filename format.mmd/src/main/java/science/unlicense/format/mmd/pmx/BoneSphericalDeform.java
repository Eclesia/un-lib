package science.unlicense.format.mmd.pmx;

import science.unlicense.common.api.Arrays;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;

/**
 * Bone spherical deform.
 * two weights and spherical parameters
 *
 * @author Johann Sorel
 */
public class BoneSphericalDeform extends BoneWeightDeform{

    public float[] sdef_c;
    public float[] sdef_r0;
    public float[] sdef_r1;

    public BoneSphericalDeform() {
        super(2);
    }

    public void setToDefault(){
        Arrays.fill(indexes, 0);
        Arrays.fill(weights, 0);
        sdef_c = new float[3];
        sdef_r0 = new float[3];
        sdef_r1 = new float[3];
    }

    public void read(PMXInputStream ds) throws IOException, StoreException{
        indexes[0] = ds.readBoneIndex();
        indexes[1] = ds.readBoneIndex();
        weights[0] = ds.readFloat();
        weights[1] = 1f-weights[0];
        sdef_c     = ds.readFloat(3);
        sdef_r0    = ds.readFloat(3);
        sdef_r1    = ds.readFloat(3);
    }

    public void write(PMXOutputStream ds) throws IOException, StoreException{
        ds.writeBoneIndex(indexes[0]);
        ds.writeBoneIndex(indexes[1]);
        ds.writeFloat(weights[0]);
        ds.writeFloat(sdef_c);
        ds.writeFloat(sdef_r0);
        ds.writeFloat(sdef_r1);
    }

}
