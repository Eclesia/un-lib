package science.unlicense.format.mmd.vmd;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;
import static science.unlicense.format.mmd.MMDUtilities.*;

/**
 * This is a bone/joint frame.
 *
 * boneName : name of the bone to move.
 * frameNumber : time when to apply it ( 100.0/3 of second)
 * position : bone local position . do not apply when 0,0,0
 * rotation : bone rotation. do not apply when 0,0,0,0
 *
 * @author Johann Sorel
 */
public class VMDBoneFrame extends CObject implements Comparable<VMDBoneFrame> {

    public Chars boneName = Chars.EMPTY;
    public int frameNumber = 0;

    public VectorRW position_3f;
    //quaternion
    public Quaternion rotation_4f;

    //not sure yet if it's 4*16 bytes or 4*4 int
//    public final byte[] interpolation1 = new byte[16];
//    public final byte[] interpolation2 = new byte[16];
//    public final byte[] interpolation3 = new byte[16];
//    public final byte[] interpolation4 = new byte[16];
    public float[] interpolation_16f;

    /**
     * Get the bone frame time in millisecond.
     * @return time offset from animation start in millisecond.
     */
    public int getTime(){
        return (int) (frameNumber * (100f/3f));
    }

    public void read(DataInputStream ds) throws IOException{
        boneName = ds.readBlockZeroTerminatedChars(15, CharEncodings.SHIFT_JIS);
        frameNumber = ds.readInt();
        position_3f = VectorNf64.create(ds.readFloat(3));
        rotation_4f = new Quaternion(ds.readFloat(4));
        //frame.rotation_4f.normalize();
        interpolation_16f = ds.readFloat(16);
    }

    @Override
    public Chars toChars() {
        return new Chars(frameNumber +" - "+ toEnglish(boneName)+"("+boneName +")  "+ position_3f +" "+rotation_4f);
    }

    @Override
    public int compareTo(VMDBoneFrame t) {
        return this.frameNumber - t.frameNumber;
    }

}
