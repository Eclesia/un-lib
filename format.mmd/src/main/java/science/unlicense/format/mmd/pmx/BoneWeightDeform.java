package science.unlicense.format.mmd.pmx;

import science.unlicense.common.api.Arrays;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;

/**
 * bone deform, using weights.
 * @author Johann Sorel
 */
public class BoneWeightDeform implements BoneDeform {

    /** Bone indexes. */
    public final int[] indexes;
    /** Bone weights */
    public final float[] weights;

    public BoneWeightDeform(int size) {
        indexes = new int[size];
        weights = new float[size];
    }

    public void setToDefault(){
        Arrays.fill(indexes, 0);
        Arrays.fill(weights, 0);
    }

    public int getNbJoint() {
        return indexes.length;
    }

    public void read(PMXInputStream ds) throws IOException, StoreException{
        if (indexes.length == 1){
            indexes[0] = ds.readBoneIndex();
            weights[0] = 1f;
        } else if (indexes.length == 2){
            indexes[0] = ds.readBoneIndex();
            indexes[1] = ds.readBoneIndex();
            weights[0] = ds.readFloat();
            weights[1] = 1f-weights[0];
        } else if (indexes.length == 4){
            indexes[0] = ds.readBoneIndex();
            indexes[1] = ds.readBoneIndex();
            indexes[2] = ds.readBoneIndex();
            indexes[3] = ds.readBoneIndex();
            weights[0] = ds.readFloat();
            weights[1] = ds.readFloat();
            weights[2] = ds.readFloat();
            weights[3] = ds.readFloat();
        }

    }

    public void write(PMXOutputStream ds) throws IOException, StoreException{
        if (indexes.length==1){
            ds.writeBoneIndex(indexes[0]);
        } else if (indexes.length == 2){
            ds.writeBoneIndex(indexes[0]);
            ds.writeBoneIndex(indexes[1]);
            ds.writeFloat(weights[0]);
        } else if (indexes.length == 4){
            ds.writeBoneIndex(indexes[0]);
            ds.writeBoneIndex(indexes[1]);
            ds.writeBoneIndex(indexes[2]);
            ds.writeBoneIndex(indexes[3]);
            ds.writeFloat(weights);
        }
    }

}
