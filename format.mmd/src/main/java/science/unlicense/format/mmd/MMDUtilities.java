package science.unlicense.format.mmd;

import java.util.ResourceBundle;
import science.unlicense.common.api.character.CharEncoding;
import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.system.Axis;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.DefaultCoordinateSystem;
import science.unlicense.geometry.api.system.Direction;
import science.unlicense.math.api.unitold.Units;

/**
 * Various methods useful for MMD parsing.
 * @author Johann Sorel
 */
public class MMDUtilities {

    public static final CoordinateSystem COORDSYS = new DefaultCoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.DECIMETER),
                new Axis(Direction.UP, Units.DECIMETER),
                new Axis(Direction.FORWARD, Units.DECIMETER)
            }
    );

    private static final ResourceBundle JP_TO_EN = ResourceBundle.getBundle("science/unlicense/format/mmd/translation");

    private MMDUtilities() {}

    public static Chars truncate(Chars text, CharEncoding encoding, int byteLength){
        if (text==null) return text;
        text = text.recode(encoding);
        //TODO we can write this more efficient
        while (text.toBytes().length>byteLength){
            text = text.truncate(0, text.getCharLength()-1);
        }
        return text;
    }

    public static Chars toEnglish(Chars jpString){
        return new Chars(MMDUtilities.toEnglish(jpString.toString()));
    }

    private static String toEnglish(String jpString){

        if (JP_TO_EN.containsKey(jpString)){
            return JP_TO_EN.getString(jpString);
        }

        for (String key : JP_TO_EN.keySet()){
            jpString = jpString.replaceAll(key, " "+JP_TO_EN.getString(key));
        }
        return jpString.trim();
    }

}
