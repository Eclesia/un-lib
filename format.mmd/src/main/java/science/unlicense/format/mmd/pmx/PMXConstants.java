
package science.unlicense.format.mmd.pmx;


/**
 * PMX constants.
 *
 * @author Johann Sorel
 */
public final class PMXConstants {

    public static final byte[] SIGNATURE = new byte[]{'P','M','X'};

    /** Bone constants, flag masks */
    public static final int FLAG_TAILPOS_IS_BONE            = 1 <<  0;
    public static final int FLAG_CAN_ROTATE                 = 1 <<  1;
    public static final int FLAG_CAN_TRANSLATE              = 1 <<  2;
    public static final int FLAG_IS_VISIBLE                 = 1 <<  3;
    public static final int FLAG_CAN_MANIPULATE             = 1 <<  4;
    public static final int FLAG_IS_IK                      = 1 <<  5;
    public static final int FLAG_IS_EXTERNAL_ROTATION       = 1 <<  8;
    public static final int FLAG_IS_EXTERNAL_TRANSLATION    = 1 <<  9;
    public static final int FLAG_HAS_FIXED_AXIS             = 1 << 10;
    public static final int FLAG_HAS_LOCAL_COORDINATE       = 1 << 11;
    public static final int FLAG_IS_AFTER_PHYSICS_DEFORM    = 1 << 12;
    public static final int FLAG_IS_EXTERNAL_PARENT_DEFORM  = 1 << 13;

    /** Material constants, flag masks */
    public static final int MATERIAL_BOTHFACE               = 1 << 0;
    public static final int MATERIAL_GROUNDSHADOW           = 1 << 1;
    public static final int MATERIAL_SELFSHADOWMAP          = 1 << 2;
    public static final int MATERIAL_SELFSHADOW             = 1 << 4;
    public static final int MATERIAL_EDGE                   = 1 << 5;
    public static final int MATERIAL_SPHERE_NONE            = 0;
    public static final int MATERIAL_SPHERE_SPH             = 1 << 0;
    public static final int MATERIAL_SPHERE_SPA             = 1 << 1;

    /** Rigid body constants, flag masks */
    public static final int SHAPE_SPHERE                    = 0;
    public static final int SHAPE_BOX                       = 1 << 0;
    public static final int SHAPE_CAPSULE                   = 1 << 1;

    private PMXConstants(){}

}
