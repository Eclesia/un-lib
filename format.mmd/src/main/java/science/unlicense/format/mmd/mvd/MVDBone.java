
package science.unlicense.format.mmd.mvd;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class MVDBone {

    public int index;
    public Chars name;

}
