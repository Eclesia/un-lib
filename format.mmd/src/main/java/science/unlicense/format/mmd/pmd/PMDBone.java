package science.unlicense.format.mmd.pmd;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mmd.MMDUtilities;
import static science.unlicense.format.mmd.MMDUtilities.*;

/**
 * @author Johann Sorel
 */
public class PMDBone {

    public Chars name;
    /** translated value, if possible */
    public Chars englishName;
    public short parentBone;
    public short nbChildNo;
    public byte boneType;
    public short ik;
    public float[] position;

    public void setToDefault(){
        name = null;
        englishName = null;
        parentBone = -1;
        nbChildNo = 0;
        boneType = 0;
        ik = 0;
        position = new float[3];
    }

    public void read(DataInputStream ds) throws IOException{
        name        = ds.readBlockZeroTerminatedChars(20, CharEncodings.SHIFT_JIS);
        englishName = toEnglish(name);
        parentBone  = ds.readShort();
        nbChildNo   = ds.readShort();
        boneType    = ds.readByte();
        ik          = ds.readShort();
        position    = ds.readFloat(3);
    }

    public void write(DataOutputStream ds) throws IOException{
        ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(name,CharEncodings.SHIFT_JIS,20), 20, CharEncodings.SHIFT_JIS);
        ds.writeShort(parentBone);
        ds.writeShort(nbChildNo);
        ds.writeByte(boneType);
        ds.writeShort(ik);
        ds.writeFloat(position);
    }

}
