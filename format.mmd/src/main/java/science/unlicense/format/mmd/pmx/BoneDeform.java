
package science.unlicense.format.mmd.pmx;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;

/**
 *
 * @author Johann Sorel
 */
public interface BoneDeform {

    int getNbJoint();

    void read(PMXInputStream ds) throws IOException, StoreException;

    void write(PMXOutputStream ds) throws IOException, StoreException;

}
