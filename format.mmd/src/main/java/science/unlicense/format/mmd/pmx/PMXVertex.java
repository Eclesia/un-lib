package science.unlicense.format.mmd.pmx;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;

/**
 * PMX Vertex, contains, position,normal,uv and skinning informations.
 *
 * @author Johann Sorel
 */
public class PMXVertex {

    /** Vertex position */
    public float[] position;
    /** Vertex normal */
    public float[] normal;
    /** Vertex texture uv */
    public float[] uv;
    /** skinning deformation */
    public BoneDeform deform;
    public float edgeFactor;

    public void setToDefault(){
        position    = new float[3];
        normal      = new float[3];
        uv          = new float[2];
        deform      = null;
        edgeFactor  = 0;
    }

    public void read(PMXInputStream ds) throws IOException, StoreException{
        position     = ds.readFloat(3);
        normal       = ds.readFloat(3);
        uv           = ds.readFloat(2);

        final  byte deform_type = ds.readByte();

        if (deform_type == 0){
            deform = new BoneWeightDeform(1);
        } else if (deform_type == 1){
            deform = new BoneWeightDeform(2);
        } else if (deform_type == 2){
            deform = new BoneWeightDeform(4);
        } else if (deform_type == 3){
            deform = new BoneSphericalDeform();
        } else {
            throw new StoreException("unknowned deform type : " + deform_type);
        }
        deform.read(ds);
        edgeFactor   = ds.readFloat();
    }

    public void write(PMXOutputStream ds) throws IOException, StoreException{
        ds.writeFloat(position);
        ds.writeFloat(normal);
        ds.writeFloat(uv);

        if (deform instanceof BoneWeightDeform){
            final int nbJoint = deform.getNbJoint();
            if (nbJoint==1){
                ds.writeByte((byte) 0);
            } else if (nbJoint==2){
                ds.writeByte((byte) 1);
            } else if (nbJoint==4){
                ds.writeByte((byte) 2);
            }
        } else if (deform instanceof BoneSphericalDeform){
            ds.writeByte((byte) 3);
        } else {
            throw new StoreException("unknowned deform type : " + deform);
        }
        deform.write(ds);
        ds.writeFloat(edgeFactor);

    }

}
