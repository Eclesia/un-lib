
package science.unlicense.format.mmd.vpd;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class VPDConstants {

    public static final Chars HEADER = Chars.constant("Vocaloid Pose Data file");
    public static final Chars BONE = Chars.constant("Bone");
    public static final Chars COMMENT = Chars.constant("//");

    private VPDConstants() {}



}
