
package science.unlicense.format.mmd.vpd;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class VPDFormat extends AbstractModel3DFormat {

    public static final VPDFormat INSTANCE = new VPDFormat();

    private VPDFormat() {
        super(new Chars("mmd_vpd"));
        shortName = new Chars("MMD-VPD");
        longName = new Chars("MikuMikuDance Pose file");
        extensions.add(new Chars("vpd"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new VPDStore(input);
    }

}
