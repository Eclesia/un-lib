
package science.unlicense.format.mmd.pmx;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class PMXFormat extends AbstractModel3DFormat {

    public static final PMXFormat INSTANCE = new PMXFormat();

    private PMXFormat() {
        super(new Chars("mmd_pmx"));
        shortName = new Chars("MMD-PMX");
        longName = new Chars("MikuMikuDance New Model file");
        extensions.add(new Chars("pmx"));
        signatures.add(PMXConstants.SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new PMXStore(input);
    }

}
