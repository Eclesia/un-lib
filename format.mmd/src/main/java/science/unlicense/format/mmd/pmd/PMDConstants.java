
package science.unlicense.format.mmd.pmd;

/**
 * PMD constants.
 *
 * @author Johann Sorel
 */
public final class PMDConstants {

    public static final byte[] SIGNATURE = new byte[]{'P','m','d'};

    /** Rigid body constants, flag masks */
    public static final int SHAPE_SPHERE                    = 0;
    public static final int SHAPE_BOX                       = 1 << 0;
    public static final int SHAPE_CAPSULE                   = 1 << 1;

    private PMDConstants(){}

}
