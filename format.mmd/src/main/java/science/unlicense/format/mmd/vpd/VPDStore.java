
package science.unlicense.format.mmd.vpd;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.impl.SimilarityNd;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.model3d.impl.physic.RelativeSkeletonPose;

/**
 * Vocaloid Pose Data file store.
 * Contains positions/rotation for skeleton joints.
 *
 * @author Johann Sorel
 */
public class VPDStore extends AbstractModel3DStore{

    private RelativeSkeletonPose poseSkeleton;
    private Chars origin;

    public VPDStore(Object input) {
        super(VPDFormat.INSTANCE, input);
    }

    public Chars getOrigin() {
        return origin;
    }

    public Collection getElements() throws StoreException {
        final ArraySequence col = new ArraySequence();

        try {
            read();
            col.add(poseSkeleton);
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        return col;
    }

    public void read() throws IOException{
        final ByteInputStream bs = getSourceAsInputStream();
        final CharInputStream cs = new CharInputStream(bs,CharEncodings.SHIFT_JIS, new Char('\n'));

        Chars line = cs.readLine();
        if (!line.startsWith(VPDConstants.HEADER)){
            //it's not a vpd file
            throw new IOException(cs, "File is not a Vocaloid Pose Data file.");
        }

        poseSkeleton = new RelativeSkeletonPose();

        for (line=cs.readLine();line!=null;line=cs.readLine()){
            line = line.trim();
            if (line.isEmpty()) continue;
            if (line.startsWith(VPDConstants.COMMENT)) continue;

            if (line.startsWith(VPDConstants.BONE)){
                poseSkeleton.getJointPoses().add(readBone(line, cs));
            } else if (origin==null){
                origin = line.split(';')[0];
            }
        }
    }

    private JointKeyFrame readBone(Chars line, CharInputStream cs) throws IOException{
        final JointKeyFrame bone = new JointKeyFrame();
        bone.setValue(SimilarityNd.create(3));
        final SimilarityRW boneTrs = bone.getValue();
        //set them to null to check they have been properly define in the bone definition
        boneTrs.getTranslation().setAll(Double.NaN);
        boneTrs.getRotation().set(0, 0, Double.NaN);
        bone.setFrom(JointKeyFrame.FROM_BASE);

        final Chars[] parts = line.trim().split('{');
        if (parts.length>1){
            //there is a name set
            //convert to proper encoding
            bone.setJoint(new Chars(parts[1].toBytes(),CharEncodings.SHIFT_JIS).recode(CharEncodings.UTF_8));
        }

        for (line=cs.readLine();line!=null;line=cs.readLine()){
            line = line.trim();
            if (line.isEmpty()) continue;
            if (line.startsWith(VPDConstants.COMMENT)) continue;

            if (!boneTrs.getTranslation().isValid()){
                final Chars[] elements = line.split(';')[0].split(',');
                boneTrs.getTranslation().set(new Vector3f64(
                        Float64.decode(elements[0]),
                        Float64.decode(elements[1]),
                        Float64.decode(elements[2])));
            } else if (Double.isNaN(boneTrs.getRotation().get(0, 0))){
                final Chars[] elements = line.split(';')[0].split(',');
                final Quaternion q = new Quaternion(
                        Float64.decode(elements[0]),
                        Float64.decode(elements[1]),
                        Float64.decode(elements[2]),
                        Float64.decode(elements[3]));
                boneTrs.getRotation().set(q.toMatrix3());
            } else {
                //should be a bone end
                if (line.startsWith(new Char('}'))){
                    //check translation and rotation have been read
                    if (!boneTrs.getTranslation().isValid()) throw new IOException(cs, "Invalid bone definition, translation missing.");
                    if (Double.isNaN(boneTrs.getRotation().get(0, 0))) throw new IOException(cs, "Invalid bone definition, rotation missing.");
                    break;
                }
            }

        }

        boneTrs.notifyChanged();
        return bone;
    }

    public void writeElements(Collection elements) throws StoreException {
        final Iterator ite = elements.createIterator();
        while (ite.hasNext()){
            final Object candidate = ite.next();
            if (candidate instanceof RelativeSkeletonPose){
                try {
                    write((RelativeSkeletonPose) candidate);
                } catch (IOException ex) {
                    throw new StoreException(ex);
                }
            } else {
                throw new StoreException("Unsupported object type : "+candidate);
            }
        }
    }

    private void write(final RelativeSkeletonPose pose) throws IOException {

        final ByteOutputStream out = getSourceAsOutputStream();
        final CharOutputStream cs = new CharOutputStream(out, CharEncodings.UTF_8);
        cs.writeLine(VPDConstants.HEADER);
        cs.writeLine(null);

        //write metas
        final Sequence joints = pose.getJointPoses();
        cs.writeLine(new Chars("unknowned.osm;"));
        cs.write(Int32.encode(joints.getSize())).write(';').endLine();
        cs.writeLine(null);

        for (int b=0,n=joints.getSize();b<n;b++){
            final JointKeyFrame jp = (JointKeyFrame) joints.get(b);
            final Chars name = CObjects.toChars(jp.getJoint());
            cs.write(VPDConstants.BONE).write(Int32.encode(b)).write('{');
            final byte[] nameShiftJS = name.toBytes(CharEncodings.SHIFT_JIS);
            cs.write(nameShiftJS);
            cs.endLine();

            cs.write(' ').write(' ');
            final VectorRW position = jp.getValue().getTranslation();
            cs.write(Float64.encode(position.get(0)));
            cs.write(',');
            cs.write(Float64.encode(position.get(1)));
            cs.write(',');
            cs.write(Float64.encode(position.get(2)));
            cs.write(';').endLine();

            cs.write(' ').write(' ');
            final Quaternion quaternion = new Matrix3x3(jp.getValue().getRotation()).toQuaternion();
            cs.write(Float64.encode(quaternion.get(0)));
            cs.write(',');
            cs.write(Float64.encode(quaternion.get(1)));
            cs.write(',');
            cs.write(Float64.encode(quaternion.get(2)));
            cs.write(',');
            cs.write(Float64.encode(quaternion.get(3)));
            cs.write(';').endLine();

            cs.write('}').endLine();
            cs.writeLine(null);
        }

    }

}
