package science.unlicense.format.mmd.pmx;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;

/**
 * @author Johann Sorel
 */
public class PMXMaterial {

    public Chars name;
    public Chars englishName;

    public float[] diffuse_RGB;
    public float alpha;
    public float[] specular_RGB;
    public float specularFactor;
    public float[] ambiant_RGB;
    public int flag;
    public float[] edgeColor_RGBA;
    public float edgeSize;
    public int textureIndex;
    public int sphereTextureIndex;
    public byte sphereMode;
    public byte toonSharingFlag;
    public int toonTextureIndex = 0;
    public Chars comment = Chars.EMPTY;
    public int vertexCount = 0;

    /** face indexes affect by this material */
    public int[] indices;

    public void setToDefault(){
        name                = null;
        englishName         = null;
        diffuse_RGB         = new float[3];
        alpha               = 1;
        specular_RGB        = new float[3];
        specularFactor      = 1;
        ambiant_RGB         = new float[3];
        flag                = 0;
        edgeColor_RGBA      = new float[4];
        edgeSize            = 0;
        textureIndex        = 0;
        sphereTextureIndex  = 0;
        sphereMode          = 0;
        toonSharingFlag     = 1;
        toonTextureIndex    = 0;
        comment             = null;
        vertexCount         = 0;
    }

    public void read(PMXInputStream ds) throws IOException, StoreException{
        name               = ds.readText();
        englishName        = ds.readText();
        diffuse_RGB        = ds.readFloat(3);
        alpha              = ds.readFloat();
        specular_RGB       = ds.readFloat(3);
        specularFactor     = ds.readFloat();
        ambiant_RGB        = ds.readFloat(3);
        flag               = ds.readByte();
        edgeColor_RGBA     = ds.readFloat(4);
        edgeSize           = ds.readFloat();
        textureIndex       = ds.readTextureIndex();
        sphereTextureIndex = ds.readTextureIndex();
        sphereMode         = ds.readByte();
        toonSharingFlag    = ds.readByte();

        if (toonSharingFlag==0){
            toonTextureIndex = ds.readTextureIndex();
        } else if (toonSharingFlag==1){
            toonTextureIndex= ds.readByte();
        } else {
            throw new StoreException("unknowned toon flag : " +toonSharingFlag);
        }
        comment     = ds.readText();
        vertexCount = ds.readInt();
    }

    public void write(PMXOutputStream ds) throws IOException, StoreException{
        ds.writeText(name);
        ds.writeText(englishName);
        ds.writeFloat(diffuse_RGB);
        ds.writeFloat(alpha);
        ds.writeFloat(specular_RGB);
        ds.writeFloat(specularFactor);
        ds.writeFloat(ambiant_RGB);
        ds.writeFloat(flag);
        ds.writeFloat(edgeColor_RGBA);
        ds.writeFloat(edgeSize);
        ds.writeTextureIndex(textureIndex);
        ds.writeTextureIndex(sphereTextureIndex);
        ds.writeByte(sphereMode);
        ds.writeByte(toonSharingFlag);

        if (toonSharingFlag==0){
            ds.writeTextureIndex(toonTextureIndex);
        } else if (toonSharingFlag==1){
            ds.writeByte((byte) toonTextureIndex);
        } else {
            throw new StoreException("unknowned toon flag : " +toonSharingFlag);
        }
        ds.writeText(comment);
        ds.writeInt(vertexCount);
    }

}
