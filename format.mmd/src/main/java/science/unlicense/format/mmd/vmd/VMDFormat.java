
package science.unlicense.format.mmd.vmd;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class VMDFormat extends AbstractModel3DFormat {

    public static final Chars SIGNATURE = Chars.constant("Vocaloid Motion Data 0002",CharEncodings.US_ASCII);

    public static final VMDFormat INSTANCE = new VMDFormat();

    private VMDFormat() {
        super(new Chars("mmd_vmd"));
        shortName = new Chars("MMD-VMD");
        longName = new Chars("MikuMikuDance Motion Data");
        extensions.add(new Chars("vmd"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new VMDStore(input);
    }

}
