package science.unlicense.format.mmd.pmx;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;

/**
 * @author Johann Sorel
 */
public class PMXPhysicConstraint {

    public Chars name;
    public Chars englishName;

    public int joinType;
    public int rigidbodyIndexA;
    public int rigidbodyIndexB;
    /** size 3 */
    public float[] position;
    public float[] rotation;
    public float[] translationLimitMin;
    public float[] translationLimitMax;
    public float[] rotationLimitMin;
    public float[] rotationLimitMax;
    public float[] springConstantTranslation;
    public float[] springConstantRotation;

    public void setToDefault(){
        name                        = null;
        englishName                 = null;
        joinType                    = 0;
        rigidbodyIndexA             = 0;
        rigidbodyIndexB             = 0;
        position                    = new float[3];
        rotation                    = new float[3];
        translationLimitMin         = new float[3];
        translationLimitMax         = new float[3];
        rotationLimitMin            = new float[3];
        rotationLimitMax            = new float[3];
        springConstantTranslation   = new float[3];
        springConstantRotation      = new float[3];
    }

    public void read(PMXInputStream ds) throws IOException, StoreException{
        name                      = ds.readText();
        englishName               = ds.readText();
        joinType                  = ds.readUByte();
        rigidbodyIndexA           = ds.readRigidBodyIndex();
        rigidbodyIndexB           = ds.readRigidBodyIndex();
        position                  = ds.readFloat(3);
        rotation                  = ds.readFloat(3);
        translationLimitMin       = ds.readFloat(3);
        translationLimitMax       = ds.readFloat(3);
        rotationLimitMin          = ds.readFloat(3);
        rotationLimitMax          = ds.readFloat(3);
        springConstantTranslation = ds.readFloat(3);
        springConstantRotation    = ds.readFloat(3);
    }

    public void write(PMXOutputStream ds) throws IOException, StoreException{
        ds.writeText(name);
        ds.writeText(englishName);
        ds.writeUByte(joinType);
        ds.writeRigidBodyIndex(rigidbodyIndexA);
        ds.writeRigidBodyIndex(rigidbodyIndexB);
        ds.writeFloat(position);
        ds.writeFloat(rotation);
        ds.writeFloat(translationLimitMin);
        ds.writeFloat(translationLimitMax);
        ds.writeFloat(rotationLimitMin);
        ds.writeFloat(rotationLimitMax);
        ds.writeFloat(springConstantTranslation);
        ds.writeFloat(springConstantRotation);
    }

}
