
package science.unlicense.format.mmd.mvd;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Motion format used by MikuMikuMoving.
 *
 *
 * Resources :
 * http://mikumikudance.wikia.com/wiki/MMD:Motion_Vector_Data
 *
 * @author Johann Sorel
 */
public class MVDFormat extends AbstractModel3DFormat {

    public static final Chars SIGNATURE = Chars.constant("Motion Vector Data",CharEncodings.US_ASCII);

    public static final MVDFormat INSTANCE = new MVDFormat();

    private MVDFormat() {
        super(new Chars("mmd_mvd"));
        shortName = new Chars("MMD-MVD");
        longName = new Chars("MikuMikuMoving Motion Vector Data");
        extensions.add(new Chars("mvd"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new MVDStore(input);
    }

}
