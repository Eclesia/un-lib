package science.unlicense.format.mmd.pmx;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.character.DefaultLChars;
import science.unlicense.common.api.character.LChars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.country.Country;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.display.api.scene.SceneUtils;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.mmd.MMDUtilities;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.MorphTarget;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Scalari32;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vectors;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.model3d.impl.technique.RenderState;
import science.unlicense.model3d.impl.technique.SilhouetteBackFaceTechnique;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.model3d.impl.technique.Technique;
import science.unlicense.physics.api.body.BodyGroup;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.constraint.AngleLimitConstraint;
import science.unlicense.physics.api.constraint.Constraint;
import science.unlicense.physics.api.constraint.CopyRotationConstraint;
import science.unlicense.physics.api.constraint.CopyTransformConstraint;
import science.unlicense.physics.api.constraint.CopyTranslationConstraint;
import science.unlicense.physics.api.force.Force;
import science.unlicense.physics.api.force.Spring;
import science.unlicense.physics.api.skeleton.IKSolver;
import science.unlicense.physics.api.skeleton.IKSolverCCD;
import science.unlicense.physics.api.skeleton.InverseKinematic;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * @author Johann Sorel
 */
public class PMXStore extends AbstractModel3DStore{

    private Path basePath;
    private PMXModel model;

    //cache loaded images
    private final Dictionary imageCache = new HashDictionary();

    public PMXStore(Object input) {
        super(PMXFormat.INSTANCE,input);
        basePath = ((Path) getInput()).getParent();
    }

    public Collection getElements() throws StoreException {
        if (model== null){
            model = new PMXModel();
            try {
                model.read(getSourceAsInputStream());
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }

        final MotionModel node = new DefaultMotionModel(MMDUtilities.COORDSYS);

        //rebuild skeleton
        final Skeleton skeleton = new Skeleton(MMDUtilities.COORDSYS);
        final Joint[] joints = new Joint[model.bones.length];
        for (int i=0;i<model.bones.length;i++){
            final PMXBone bone = model.bones[i];
            final Joint joint = new Joint(3);
            joint.setLocalCoordinateSystem(MMDUtilities.COORDSYS);

            final Dictionary trs = new HashDictionary();
            trs.add(Country.JPN.asLanguage(), bone.name);
            trs.add(Country.GBR.asLanguage(), bone.englishName);
            final LChars name = new DefaultLChars(trs, Country.JPN.asLanguage());
            joint.setTitle(name);
            joint.getNodeTransform().getTranslation().set(bone.position);
            joint.getNodeTransform().notifyChanged();
            joint.setId(i);
            joints[i] = joint;
        }

        //rebuild node constraints
        for (int i=0;i<model.bones.length;i++){
            final PMXBone bone = model.bones[i];
            final Joint joint = (Joint) skeleton.getJointById(i);

            if ( (bone.flag & PMXConstants.FLAG_IS_EXTERNAL_ROTATION)!=0 ){
                final Joint toCopy = (Joint) skeleton.getJointById(bone.effectIndex);
                if (toCopy!=null){
                    //some files have invalide indexes, ensure we don't have an index error
                    final CopyRotationConstraint cst = new CopyRotationConstraint(joint, toCopy, bone.effectFactor);
                    joint.getConstraints().add(cst);
                }
            }
            if ( (bone.flag & PMXConstants.FLAG_IS_EXTERNAL_TRANSLATION)!=0 ){
                final Joint toCopy = (Joint) skeleton.getJointById(bone.effectIndex);
                if (toCopy!=null){
                    final CopyTranslationConstraint cst = new CopyTranslationConstraint(joint, toCopy, bone.effectFactor);
                    joint.getConstraints().add(cst);
                }
            }
//
//            if ((bone.flag & PMXConstants.FLAG_HAS_FIXED_AXIS)!=0){
//                final FixedAxisConstraint cst = new FixedAxisConstraint(joint, bone.fixedAxis);
//                joint.getConstraints().add(cst);
//            }
//
//            if ((bone.flag & PMXConstants.FLAG_HAS_LOCAL_COORDINATE)!=0){
//                //TODO need to find a model with such thing to rebuild it correctly
//                //System.out.println("todo");
//            }
//
            if ((bone.flag & PMXConstants.FLAG_IS_EXTERNAL_PARENT_DEFORM)!=0){
                final Joint toCopy = (Joint) skeleton.getJointById(bone.externalKey);
                if (toCopy!=null){
                    final CopyTransformConstraint cst = new CopyTransformConstraint(joint, toCopy, 1f);
                    joint.getConstraints().add(cst);
                }
            }

        }

        //rebuild hierarchy
        for (int i=0;i<model.bones.length;i++){
            if (model.bones[i].parentIndex < 255){
                final Joint child = (Joint) joints[i];
                final Joint parent = (Joint) joints[model.bones[i].parentIndex];
                parent.getChildren().add(child);
            } else {
                //root joint
                skeleton.getChildren().add(joints[i]);
            }
        }
        node.setSkeleton(skeleton);

        //rebuild inverse kinematic
        for (int i=0;i<model.bones.length;i++){
            final PMXBone bone = model.bones[i];
            if (bone.ik != null){
                final Joint effector = (Joint) skeleton.getJointById(bone.ik.effectorBone);
                final Joint target = (Joint) skeleton.getJointById(i);
                final Joint[] chain = new Joint[bone.ik.links.length];
                for (int k=0;k<chain.length;k++){
                    PMXIK.PMXIKLink link = bone.ik.links[k];
                    chain[k] = (Joint) skeleton.getJointById(link.boneIndex);
                    //PMXIK contains bone angle limitations and a float array minX,minY,minZ,maxX,maxY,maxZ
                    if (link.hasLimit){
                        final BBox limits = new BBox(3);
                        limits.setRange(0, link.limits[0], link.limits[3]);
                        limits.setRange(1, link.limits[1], link.limits[4]);
                        limits.setRange(2, link.limits[2], link.limits[5]);
                        final Constraint cst = new AngleLimitConstraint(chain[k], limits);
                        chain[k].getConstraints().add(cst);
                    }
                }
                final IKSolver solver = new IKSolverCCD(bone.ik.maxLoop, bone.ik.maxAngle, 3);
                final InverseKinematic ik = new InverseKinematic(target, effector, chain, solver);
                skeleton.getIks().add(ik);
           }
        }

        //invert skeleton and physics world transforms
        //build bind and invert bind pose.
        //we do it before reading part because silhouette will requiere bone positions
        skeleton.reverseWorldPose();
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        for (int i=0;i<model.materials.length;i++){
            node.getChildren().add(buildPart(model.materials[i],skeleton));
        }

        //rebuild rigid bodies
        final RigidBody[] bodies = new RigidBody[model.rigidBodies.length];
        if (model.rigidBodies.length>0){
            //create all body groups
            final Dictionary groups = new HashDictionary();
            for (int i=0;i<32;i++){
                groups.add(i, new BodyGroup(new Chars(""+i)));
            }

            for (int i=0;i<model.rigidBodies.length;i++){
                final PMXRigidBody body = model.rigidBodies[i];

                //rigid body shape
                final Geometry shape;
                if (body.shapeType == PMXConstants.SHAPE_SPHERE){
                    shape = new Sphere(body.shapeSize[0]);
                } else if (body.shapeType == PMXConstants.SHAPE_CAPSULE){
                    shape = new Capsule(body.shapeSize[1],body.shapeSize[0]);
                } else if (body.shapeType == PMXConstants.SHAPE_BOX){
                    final double[] lower = VectorNf64.create(body.shapeSize).scale(-1).toDouble();
                    final double[] upper = Arrays.reformatDouble(body.shapeSize);
                    shape = new BBox(lower,upper);

                } else {
                    throw new StoreException("unknowed body type : "+ body.shapeType);
                }
                final RigidBody physicbody = new RigidBody(shape, body.mass);
                physicbody.setLocalCoordinateSystem(MMDUtilities.COORDSYS);
                physicbody.setUpdateMode(RigidBody.UPDATE_PARENT);
                physicbody.setFixed(body.mode==0);

                bodies[i] = physicbody;

                //place the physic object
                physicbody.getNodeTransform().getTranslation().set(body.shapePosition);
                final Matrix m;
                if (Vectors.isFinite(body.shapeRotation)){//some files contains NaN values
                    m = Matrix3x3.createFromAngles(body.shapeRotation[0],body.shapeRotation[1],body.shapeRotation[2]);
                    physicbody.getNodeTransform().getRotation().set(m);
                }

                physicbody.getNodeTransform().notifyChanged();

                //set material informations
                physicbody.getMaterial().setDensity(1);
                physicbody.getMaterial().setRestitution(body.restitution);
                physicbody.getMaterial().setLinearAttenuation(body.linearAttenuation);
                physicbody.getMaterial().setAngularAttenuation(body.angularAttenuation);
                physicbody.getMaterial().setDynamicFriction(body.friction);
                physicbody.getMaterial().setStaticFriction(body.friction);
                //rebuild groups
                if (body.collisionGroup!=0){
                    final BodyGroup group = (BodyGroup) groups.getValue(body.collisionGroup);
                    physicbody.setGroup(group);
                    //rebuild no collision groups
                    for (int k=0;k<32;k++){
                        final boolean nocollide = ((1<<k) & body.noCollisionGroup) == 0;
                        if (nocollide){
                            final BodyGroup no = (BodyGroup) groups.getValue(k);
                            if (no==null){
                                throw new NullPointerException("group should not be null : "+k);
                            }
                            physicbody.addNoCollisionGroup(no);
                        }
                    }
                }

                //attach the related joint
                final Joint joint = skeleton.getJointById(body.boneIndex);
                if (joint != null){
                    joint.getChildren().add(physicbody);
                    //the physic body is in world space, we must convert it to joint space
                    SceneUtils.reverseWorldToNodeTrs(physicbody,false);
                } else {
                    System.out.println("Physic body is not attached to any bone : "+body);
                }
            }
        }

        //rebuild physic constraints
        for (int i=0;i<model.constraints.length;i++){
            final PMXPhysicConstraint cst = model.constraints[i];
            if (cst.rigidbodyIndexA>=bodies.length || cst.rigidbodyIndexB>=bodies.length){
                System.out.println("Physic constraints using body index "+cst.rigidbodyIndexA+":"+cst.rigidbodyIndexB+" but there are only "+bodies.length+" rigid bodies.");
                continue;
            }

            final RigidBody rbA = bodies[cst.rigidbodyIndexA];
            final RigidBody rbB = bodies[cst.rigidbodyIndexB];
            final Tuple centerA = rbA.getWorldPosition(null);
            final Tuple centerB = rbB.getWorldPosition(null);
            final Force force = new Spring(rbA, rbB, 5, 0.5, DistanceOp.distance(centerA, centerB));

            node.getConstraints().add(force);
        }

        final Collection col = new ArraySequence();
        col.add(node);
        return col;
    }

    private DefaultModel buildPart(final PMXMaterial material, Skeleton skeleton) {
        final DefaultModel mesh = new DefaultModel();
        mesh.setLocalCoordinateSystem(MMDUtilities.COORDSYS);
        mesh.getTechniques().add(new SimpleBlinnPhong());

        if ((material.flag & PMXConstants.MATERIAL_BOTHFACE)==1){
            ((Technique) mesh.getTechniques().get(0)).getState().setCulling(RenderState.CULLING_NONE);
        } else {
            ((Technique) mesh.getTechniques().get(0)).getState().setCulling(RenderState.CULLING_BACK);
        }

        //find max number of joint per vertex
        int maxJoint = 1; //at least one
        for (int i=0;i<material.indices.length;i++) {
            final BoneDeform deform = model.vertices[material.indices[i]].deform;
            maxJoint = Maths.max(maxJoint, deform.getNbJoint());
        }

        final int nbVertice = material.indices.length*3;
        final int nbJoint = material.indices.length*maxJoint;
        final Float32Cursor vertexCursor = DefaultBufferFactory.INSTANCE.createFloat32(nbVertice).cursor();
        final Float32Cursor normalCursor = DefaultBufferFactory.INSTANCE.createFloat32(nbVertice).cursor();
        final Int32Cursor jointCursor  = DefaultBufferFactory.INSTANCE.createInt32(nbJoint).cursor();
        final Float32Cursor weightCursor = DefaultBufferFactory.INSTANCE.createFloat32(nbJoint).cursor();
        final Float32Cursor deformCursor = DefaultBufferFactory.INSTANCE.createFloat32(nbJoint*4).cursor();
        final Int32Cursor indexCursor  = DefaultBufferFactory.INSTANCE.createInt32(material.indices.length).cursor();

        for (int i=0;i<material.indices.length;i++) {
            final PMXVertex vertex = model.vertices[material.indices[i]];
            vertexCursor.write(vertex.position);
            normalCursor.write(vertex.normal);
            indexCursor.write(i);

            final BoneDeform deform = vertex.deform;
            if (deform instanceof BoneSphericalDeform){
                final BoneSphericalDeform def = (BoneSphericalDeform) deform;
                jointCursor.write(def.indexes);
                weightCursor.write(def.weights);
                deformCursor.write(1.0f).write(def.sdef_c);
            } else if (deform instanceof BoneWeightDeform){
                final BoneWeightDeform def = (BoneWeightDeform) deform;
                jointCursor.write(def.indexes);
                weightCursor.write(def.weights);
                deformCursor.write(0).write(0).write(0).write(0);
            } else {
                throw new InvalidArgumentException("Should not happen");
            }
            //fill remaining weigths with zeros
            for (int w=deform.getNbJoint();w<maxJoint;w++){
                jointCursor.write(0);
                weightCursor.write(0f);
            }
        }

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.CARTESIAN3D_METRIC_RIGH_HANDED);
        final Skin skin = new Skin();
        skin.setMaxWeightPerVertex(maxJoint);
        skin.setSkeleton(skeleton);
        shell.setPositions(InterleavedTupleGrid1D.create(vertexCursor.getBuffer(),3));
        shell.setNormals(InterleavedTupleGrid1D.create(normalCursor.getBuffer(),3));
        shell.setIndex(InterleavedTupleGrid1D.create(indexCursor.getBuffer(),1));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) indexCursor.getBuffer().getNumbersSize())});
        shell.getAttributes().add(Skin.ATT_JOINTS_0, InterleavedTupleGrid1D.create(jointCursor.getBuffer(),maxJoint));
        shell.getAttributes().add(Skin.ATT_WEIGHTS_0, InterleavedTupleGrid1D.create(weightCursor.getBuffer(),maxJoint));
        shell.getAttributes().add(Skin.ATT_JPARAMS_0, InterleavedTupleGrid1D.create(deformCursor.getBuffer(),4));
        mesh.setShape(shell);
        mesh.setSkin(skin);

        //set default colors
        final SimpleBlinnPhong.Material meshmat = SimpleBlinnPhong.newMaterial();
        mesh.getMaterials().add(meshmat);
        meshmat.setAmbient(new ColorRGB(material.ambiant_RGB));
        meshmat.setDiffuse(new ColorRGB(material.diffuse_RGB));
        meshmat.setSpecular(new ColorRGB(material.specular_RGB));

        if (!material.englishName.isEmpty()){
            mesh.setTitle(material.englishName);
        } else if (!material.name.isEmpty()){
            mesh.setTitle(material.name);
        } else {
            mesh.setTitle(Chars.EMPTY);
        }

        try {
            textured:
            if (material.textureIndex >= 0 && material.textureIndex<255) {
                final Chars textureName = model.textures[material.textureIndex];
                Chars baseName = textureName;

                mesh.setTitle(mesh.getTitle().concat(' ').concat(baseName));

                final Sequence imageFiles = splitTextures(basePath, baseName);
                if (imageFiles.isEmpty()){
                    getLogger().info(new Chars("No image files found for : "+textureName));
                    break textured;
                }
                final Sequence images = new ArraySequence();
                for (int i=0,n=imageFiles.getSize();i<n;i++){
                    Path p = (Path) imageFiles.get(i);
                    Texture2D texture = (Texture2D) imageCache.getValue(p);
                    if (texture==null){
                        //load image
                        Image img = loadImage(p);
                        texture = new Texture2D(img);
                        imageCache.add(p, texture);
                    }
                    images.add(texture);
                }
                if (images.isEmpty()) throw new IOException(getInput(), "No image files could be read.");

                final Float32Cursor uvBuffer = DefaultBufferFactory.INSTANCE.createFloat32(material.indices.length*2).cursor();
                for (int i=0;i<material.indices.length;i++) {
                    final int m = material.indices[i];
                    uvBuffer.write(model.vertices[m].uv);
                }
                shell.setUVs(InterleavedTupleGrid1D.create(uvBuffer.getBuffer(), 2));

                final TextureMapping[] materialLayers = PMXStore.buildTexture(imageFiles,images);
                meshmat.properties().setPropertyValue(SimpleBlinnPhong.MATERIAL_DIFFUSETEXTURE, materialLayers[0]);

                if (materialLayers.length>1) {
                    meshmat.properties().setPropertyValue(SimpleBlinnPhong.MATERIAL_ALPHATEXTURE, materialLayers[1]);
                }
            }
        } catch (IOException ex) {
            getLogger().warning(ex);
        }

        //rebuild morph frames
        rebuildMorphFrames(mesh, material, vertexCursor.getBuffer());

        if (material.edgeSize>0 && (material.flag & PMXConstants.MATERIAL_EDGE) !=0){
            //models are in decimeter units, edges size seems to be in millimeter : TODO need to confirm this
            //so we divide by 100 on have it in decimeter
            //mesh.getTechniques().add(new GLSilhouetteBackFaceTechnique(mesh,
            //        new ColorRGB(material.edgeColor_RGBA), material.edgeSize/100));
            mesh.getTechniques().add(new SilhouetteBackFaceTechnique(
                    new ColorRGB(material.edgeColor_RGBA), material.edgeSize/100));
        }

        mesh.updateBoundingBox();
        return mesh;
    }

    private void rebuildMorphFrames(DefaultModel mesh, PMXMaterial material, Buffer vertexBuffer){
        if (model.morphs.length==0) return;

        //cache vertex indexes from material for faster morph reconstruction
        int minIdx = Integer.MAX_VALUE;
        int maxIdx = Integer.MIN_VALUE;
        for (int i=0,idx,n=material.indices.length;i<n;i++) {
            idx = material.indices[i];
            if (idx<minIdx) minIdx = idx;
            if (idx>maxIdx) maxIdx = idx;
        }
        final int[][] dico = new int[maxIdx-minIdx+1][0];
        for (int i=0,idx,did,n=material.indices.length;i<n;i++) {
            idx = material.indices[i];
            did = idx-minIdx;
            dico[did] = Arrays.insert(dico[did], dico[did].length, i);
        }


        final Sequence ms = new ArraySequence();
        for (int k=0;k<model.morphs.length;k++){
            final PMXMorph morph = model.morphs[k];

            final Float32Cursor copy = DefaultBufferFactory.INSTANCE.createFloat32(material.indices.length*3).cursor();
            copy.offset(0);

            //TODO only support vertex morph for now
            boolean modified = false;
            for (PMXMorph.PMXMorphElement ele : morph.elements){
                if (ele instanceof PMXMorph.PMXMorphVertex){
                    final PMXMorph.PMXMorphVertex vm = (PMXMorph.PMXMorphVertex) ele;
                    if (vm.index<minIdx || vm.index>maxIdx) continue;
                    final int[] ref = dico[vm.index-minIdx];
                    if (ref.length>0){
                        modified = true;
                        //replace coords
                        for (int i=0;i<ref.length;i++) {
                            int ri = ref[i]*3;
                            copy.offset(ri);
                            copy.write(vm.position);
                        }
                    }
                }
            }
            if (modified){
                final MorphTarget mf = new MorphTarget();
                mf.setName(morph.name);
                mf.setVertices(InterleavedTupleGrid1D.create(copy.getBuffer(), 3));
                ms.add(mf);
            }
        }
        ((Mesh) mesh.getShape()).getMorphs().addAll(ms);
    }

    /**
     * Writing will search and write only the first MultiPartMesh found in the collection.
     *
     * @param elements
     * @throws IOException
     */
    @Override
    public void writeElements(Collection elements) throws StoreException {
        MotionModel mpm = null;
        for (Iterator ite=elements.createIterator();ite.hasNext();){
            final Object obj = ite.next();
            if (obj instanceof MotionModel){
                mpm = (MotionModel) obj;
                break;
            }
        }

        if (mpm!=null){
            try{
                final PMXModel model = toModel(mpm);
                final ByteOutputStream out = getSourceAsOutputStream();
                model.write(out);
                out.close();
            }catch(IOException ex){
                throw new StoreException(ex);
            }
        }
    }


    private PMXModel toModel(MotionModel mpm) throws IOException{
        final PMXModel pmxmodel = new PMXModel();
        pmxmodel.setToDefault();

        final Vector3f64 tuple = new Vector3f64();

        //counters for dynamic names
        int texturecounter = 0;

        //transform the skeleton and rigid bodies
        final Skeleton skeleton = mpm.getSkeleton();
        if (skeleton!=null){
            final Dictionary buildDico = new HashDictionary();
            final Sequence roots = skeleton.getChildren();
            final Sequence result = new ArraySequence();
            for (int i=0;i<roots.getSize();i++){
                buildSkeleton((Joint) roots.get(i), buildDico, result);
            }

            pmxmodel.bones = new PMXBone[result.getSize()];
            Collections.copy(result, pmxmodel.bones, 0);
        }

        //transform the mesh
        final Iterator children = mpm.getChildren().createIterator();
        while (children.hasNext()) {
            final Object next = children.next();
            if (next instanceof Model){
                final Model model = (Model) next;
                final Mesh mesh = (Mesh) model.getShape();
                final Skin skin = model.getSkin();

                final int vertexBaseOffset = pmxmodel.vertices.length;

                //rebuild vertices
                final TupleGrid vertices = mesh.getPositions();
                final TupleGrid normals = mesh.getNormals();
                final TupleGrid uvs = mesh.getUVs();
                final TupleGrid jointIndexes = (TupleGrid) mesh.getAttributes().getValue(Skin.ATT_JOINTS_0);
                final TupleGrid jointWeights = (TupleGrid) mesh.getAttributes().getValue(Skin.ATT_WEIGHTS_0);
                final int maxWeitgh = (skin != null) ? skin.getMaxWeightPerVertex() : 0;
                final int bestNbWeight = maxWeitgh>=3 ? 4 : Maths.max(maxWeitgh,1);
                final VectorRW temp = VectorNf64.createDouble(bestNbWeight);

                final int vn = (int) vertices.getExtent().getL(0);
                pmxmodel.vertices = (PMXVertex[]) Arrays.resize(pmxmodel.vertices, vertexBaseOffset+vn);

                Scalari32 v = new Scalari32();
                for (; v.x < vn; v.x++) {
                    final PMXVertex vertex = new PMXVertex();
                    vertex.setToDefault();
                    pmxmodel.vertices[vertexBaseOffset+v.x] = vertex;
                    vertices.getTuple(v, tuple);
                    tuple.toFloat(vertex.position, 0);
                    if (normals!=null) {
                        normals.getTuple(v, tuple);
                        tuple.toFloat(vertex.normal, 0);
                    }
                    if (uvs!=null) {
                        uvs.getTuple(v, tuple);
                        vertex.uv[0] = (float) tuple.getX();
                        vertex.uv[1] = (float) tuple.getY();
                    }
                    if (jointIndexes!=null) {
                        jointWeights.getTuple(v, temp);
                        final float[] weights = temp.toFloat();
                        jointIndexes.getTuple(v, temp);
                        final int[] indexes = temp.toInt();
                        final BoneWeightDeform deform = new BoneWeightDeform(bestNbWeight);
                        vertex.deform = deform;
                        Arrays.copy(weights, 0, Maths.min(weights.length, bestNbWeight),deform.weights, 0);
                        Arrays.copy(indexes, 0, Maths.min(indexes.length, bestNbWeight),deform.indexes, 0);
                    }
                }

                //rebuild material
                final SimpleBlinnPhong.Material material = SimpleBlinnPhong.view((Material) model.getMaterials().get(0));
                final PMXMaterial pmxMaterial = new PMXMaterial();
                pmxMaterial.setToDefault();
                pmxmodel.materials = (PMXMaterial[]) Arrays.insert(pmxmodel.materials, pmxmodel.materials.length, pmxMaterial);

                //texture informations
                pmxMaterial.alpha = (float) material.getAlpha();
                pmxMaterial.ambiant_RGB = material.getAmbient().toRGB();
                pmxMaterial.diffuse_RGB = material.getDiffuse().toRGB();
                pmxMaterial.specularFactor = (float) material.getShininess();
                pmxMaterial.specular_RGB = material.getSpecular().toRGB();

                final TextureMapping diffuseTexture = material.getDiffuseTexture();
                if (diffuseTexture != null) {

                    final int texIndex = pmxmodel.textures.length;
                    final Chars texName = new Chars("texture"+(texturecounter++)+".bmp");
                    pmxmodel.textures = (Chars[]) Arrays.insert(pmxmodel.textures,texIndex,texName);

                    final Image texture = diffuseTexture.getTexture().getImage();
                    pmxMaterial.textureIndex = texIndex ;
                    //save texture aside pmd file
                    final Path texturePath = basePath.resolve(texName);
                    Images.write(texture, new Chars("bmp"), texturePath);
                }

                //face indices
                final int[] indexes = Geometries.toInt32(mesh.getIndex());
                pmxMaterial.indices = new int[indexes.length];
                for (int t=0;t<indexes.length;t++){
                    pmxMaterial.indices[t] = vertexBaseOffset + indexes[t];
                }
                pmxmodel.faceIndices = Arrays.concatenate(new int[][]{pmxmodel.faceIndices,pmxMaterial.indices});
            }
        }

        return pmxmodel;
    }

    private void buildSkeleton(Joint joint, Dictionary all, Sequence result){
        final PMXBone bone = new PMXBone();
        bone.setToDefault();
        bone.name = joint.getTitle().toChars();
        bone.englishName = bone.name;
        bone.ik = null;

        final Affine matrix = joint.getNodeToRootSpace();
        final double[] pos = new double[3];
        matrix.transform(pos,0,pos,0,1);
        bone.position = Arrays.reformatToFloat(pos);

        final Integer parentIndex = (Integer) all.getValue(joint.getParent());
        if (parentIndex!=null){
            bone.parentIndex = parentIndex.shortValue();
        }

        all.add(result.getSize(), bone);
        result.add(bone);

        //loop on children
        final Iterator ite = joint.getChildren().createIterator();
        while (ite.hasNext()) {
            final Object next = ite.next();
            if (next instanceof Joint){
                buildSkeleton((Joint) next, all, result);
            } else if (next instanceof RigidBody){
                //TODO
            }
        }

    }

    /**
     * Split textures from name.
     *
     * MMD textures are often combined.
     * Handle names such as : ttttt*uuuuu
     * where tttt is diffuse map and uuuu the specular map.
     *
     * @param parentPath
     * @param baseName
     * @return
     */
    public static Sequence splitTextures(Path parentPath, Chars baseName){
        final Sequence valids = new ArraySequence();

        //pmd/pmx paths are case insensitive
        Chars str = baseName.replaceAll('\\', '/');
        // slash in japanese encoding, normaly we should not have those, but
        // some files mix encodings, so it may happen
        str = str.replaceAll('¥', '/');
        Chars[] fileparts = str.split('\\');
        loop:
        for (Chars filepart : fileparts){
            //remove relative path
            if (filepart.startsWith(new Chars("./"))) filepart = filepart.truncate(2,-1);
            if (filepart.startsWith('/')) filepart = filepart.truncate(1,-1);
            final Chars[] pathParts = filepart.split('/');

            //split multitextures
            final Chars[] multiTex = pathParts[pathParts.length-1].split('*');


            for (int k=0;k<multiTex.length;k++) {
                Path imageFile = parentPath;
                nextPart:
                for (int i=0;i<pathParts.length;i++){
                    final Chars part = (i == pathParts.length-1) ? multiTex[k] : pathParts[i];
                    final Iterator ite = imageFile.getChildren().createIterator();
                    while (ite.hasNext()){
                        Path p = (Path) ite.next();
                        if (p.getName().equals(part,true,true)){
                            imageFile = p;
                            continue nextPart;
                        }
                    }
                    //not found
                    imageFile = null;
                    continue loop;
                }
                valids.add(imageFile);
            }

        }
        return valids;
    }

    public static Image loadImage(Path path) throws IOException{
        return Images.read(path);
    }

    public static TextureMapping[] buildTexture(Sequence paths, Sequence textures){
        final TextureMapping[] layers = new TextureMapping[paths.getSize()];

        for (int i=0;i<layers.length;i++){
            final Texture2D texture = (Texture2D) textures.get(i);
            layers[i] = new TextureMapping();
            layers[i].setMethod(TextureMapping.METHOD_UV);
            layers[i].setMapping(Mesh.ATT_TEXCOORD_0);
            layers[i].setTexture(texture);
        }
        return layers;
    }

}
