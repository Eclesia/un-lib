

package science.unlicense.format.mmd.pmd;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class PMDBoneGroup {

    public Chars name;
    public Chars englishName;
    /** indexes of bones associated to this group */
    public int[] boneIndexes = new int[0];

}
