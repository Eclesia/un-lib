package science.unlicense.format.mmd.pmx;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;

/**
 * Rigid body for physics.
 * PMX bodies are attached to bones.
 *
 * @author Johann Sorel
 */
public class PMXRigidBody {

    public Chars name;
    public Chars englishName;
    public int boneIndex;

    /**
     * group id, bodies in the same group do not collide.
     * At most 32 groups.(to fit in no collision group range)
     */
    public int collisionGroup;
    /**
     * no collision group mask. bit 1 for first group ...
     */
    public int noCollisionGroup;

    /**
     * geometry type, one of SHAPE_X
     */
    public int shapeType;
    /**
     * Geometry parmeters.
     * if SPHERE :
     *  x = radius
     * if BOX :
     *  x = width
     *  y = height
     *  z = depth
     * if CAPSULE :
     *  x = radius
     *  y = height
     */
    public float[] shapeSize;
    public float[] shapePosition;
    /**
     * Rotation is the concatenate rotation matrices on x,y,z.
     * [0] : rotation angle on X
     * [1] : rotation angle on Y
     * [2] : rotation angle on Z
     */
    public float[] shapeRotation;

    /** Body mass */
    public float mass;
    public float linearAttenuation;
    public float angularAttenuation;
    public float restitution;
    public float friction;
    /**
     * How body must interact in the physic engine.
     * 0 = kinematic (moves with bones, no physic applied)
     * 1 = simulated (not moved by bones, physics apply)
     * 2 = aligned (both)
     */
    public int mode;

    public void setToDefault(){
        name                = null;
        englishName         = null;
        boneIndex           = 0;
        collisionGroup      = 0;
        noCollisionGroup    = 0;
        shapeType           = 0;
        shapeSize           = new float[3];
        shapePosition       = new float[3];
        shapeRotation       = new float[3];
        mass                = 0;
        linearAttenuation   = 0;
        angularAttenuation  = 0;
        restitution         = 0;
        friction            = 0;
        mode                = 0;
    }

    public void read(PMXInputStream ds) throws IOException, StoreException{
        name               = ds.readText();
        englishName        = ds.readText();
        boneIndex          = ds.readBoneIndex();
        collisionGroup     = ds.readUByte();
        noCollisionGroup   = ds.readUShort();
        shapeType          = ds.readUByte();
        shapeSize          = ds.readFloat(3);
        shapePosition      = ds.readFloat(3);
        shapeRotation      = ds.readFloat(3);
        mass               = ds.readFloat();
        linearAttenuation  = ds.readFloat();
        angularAttenuation = ds.readFloat();
        restitution        = ds.readFloat();
        friction           = ds.readFloat();
        mode               = ds.readUByte();
    }

    public void write(PMXOutputStream ds) throws IOException, StoreException{
        ds.writeText(name);
        ds.writeText(englishName);
        ds.writeBoneIndex(boneIndex);
        ds.writeUByte(collisionGroup);
        ds.writeUShort(noCollisionGroup);
        ds.writeUByte(shapeType);
        ds.writeFloat(shapeSize);
        ds.writeFloat(shapePosition);
        ds.writeFloat(shapeRotation);
        ds.writeFloat(mass);
        ds.writeFloat(linearAttenuation);
        ds.writeFloat(angularAttenuation);
        ds.writeFloat(restitution);
        ds.writeFloat(friction);
        ds.writeUByte(mode);
    }

}
