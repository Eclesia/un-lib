

package science.unlicense.format.mmd.vmd;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class VMDLightFrame {

    public int frameNumber;
    public float[] color;
    public VectorRW position;

    public void read(DataInputStream ds) throws IOException{
        frameNumber = ds.readInt();
        color       = ds.readFloat(3);
        position    = VectorNf64.create(ds.readFloat(3));
    }

}
