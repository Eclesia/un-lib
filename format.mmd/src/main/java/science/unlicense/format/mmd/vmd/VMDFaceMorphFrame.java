package science.unlicense.format.mmd.vmd;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.mmd.MMDUtilities.*;

/**
 * This is a skin (face vertex animation) frame.
 *
 * name : name of the skin to use
 * frameNumber : time when to apply it (in 100.0/3 of second)
 * factor : global scale to apply to the vertices
 *
 * @author Johann Sorel
 */
public class VMDFaceMorphFrame extends CObject {

    public Chars name;
    public long  frameNumber;
    public float factor;

    /**
     * Get the frame time in millisecond.
     * @return time offset from animation start in millisecond.
     */
    public int getTime(){
        return (int) (frameNumber * (100f/3f));
    }

    public void read(DataInputStream ds) throws IOException{
        name = ds.readBlockZeroTerminatedChars(15, CharEncodings.SHIFT_JIS);
        frameNumber = ds.readInt();
        factor = ds.readFloat();
    }

    public Chars toChars() {
        return new Chars(frameNumber +" - "+ toEnglish(name)+"("+name +")  "+ factor);
    }

}
