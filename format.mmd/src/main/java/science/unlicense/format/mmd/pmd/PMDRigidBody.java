

package science.unlicense.format.mmd.pmd;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mmd.MMDUtilities;

/**
 *
 * @author Johann Sorel
 */
public class PMDRigidBody {

    /** 20bytes */
    public Chars name;
    /** bone affected*/
    public int boneId;
    /** collision group id */
    public int collisionGroupId;
    /** collision group mask : indicate which groups can collide */
    public int collisionGroupMask;
    /** shape type :
     * 0 = sphere
     * 1 = bbox
     * 2 = capsule
     */
    public int shapeType;
    /**
     * sphere : width, radius
     * capsule : width, radius
     * bbox : width, height, depth
     */
    public float[] shapeSize;
    /** shpae position */
    public float[] position;
    /** shpae rotation : euler*/
    public float[] rotation;
    /** body mass */
    public float mass;
    /** material linear damping */
    public float linearAttenuation;
    /** material angular damping */
    public float angularAttenuation;
    /** material restitution */
    public float restitution;
    /** material friction */
    public float friction;
    /** body type :
     * 0 = kinematic (moves with bones, no physic applied)
     * 1 = simulated (not moved by bones, physics apply)
     * 2 = aligned (both)
     */
    public int bodyType;

    public void setToDefault(){
        name                = null;
        boneId              = -1;
        collisionGroupId    = 0;
        collisionGroupMask  = 0;
        shapeType           = 0;
        shapeSize           = new float[3];
        position            = new float[3];
        rotation            = new float[3];
        mass                = 0;
        linearAttenuation   = 0;
        angularAttenuation  = 0;
        restitution         = 0;
        friction            = 0;
        bodyType            = 0;
    }

    public void read(DataInputStream ds) throws IOException{
        name                = ds.readBlockZeroTerminatedChars(20, CharEncodings.US_ASCII);
        boneId              = ds.readUShort();
        collisionGroupId    = ds.readUByte();
        collisionGroupMask  = ds.readUShort();
        shapeType           = ds.readUByte();
        shapeSize           = ds.readFloat(3);
        position            = ds.readFloat(3);
        rotation            = ds.readFloat(3);
        mass                = ds.readFloat();
        linearAttenuation   = ds.readFloat();
        angularAttenuation  = ds.readFloat();
        restitution         = ds.readFloat();
        friction            = ds.readFloat();
        bodyType            = ds.readUByte();
    }

    public void write(DataOutputStream ds) throws IOException{
        ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(name,CharEncodings.SHIFT_JIS,20), 20, CharEncodings.SHIFT_JIS);
        ds.writeUShort(boneId);
        ds.writeUByte(collisionGroupId);
        ds.writeUShort(collisionGroupMask);
        ds.writeUByte(shapeType);
        ds.writeFloat(shapeSize);
        ds.writeFloat(position);
        ds.writeFloat(rotation);
        ds.writeFloat(mass);
        ds.writeFloat(linearAttenuation);
        ds.writeFloat(angularAttenuation);
        ds.writeFloat(restitution);
        ds.writeFloat(friction);
        ds.writeUByte(bodyType);
    }

}
