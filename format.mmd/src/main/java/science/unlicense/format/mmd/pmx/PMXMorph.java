package science.unlicense.format.mmd.pmx;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.math.impl.Quaternion;

/**
 * PMX Morph are used for face expressions.
 *
 * @author Johann Sorel
 */
public class PMXMorph {

    public Chars name;
    public Chars englishName;
    /**
     * UI panel index in editor.
     */
    public int panel;
    /**
     * Type of morphs in 'elements' array.
     */
    public int type;
    public PMXMorphElement[] elements;

    public void setToDefault(){
        name        = null;
        englishName = null;
        panel       = 0;
        type        = 0;
        elements    = new PMXMorphElement[0];
    }

    public void read(PMXInputStream ds) throws IOException, StoreException{
        name          = ds.readText();
        englishName   = ds.readText();
        panel         = ds.readUByte();
        type          = ds.readUByte();
        elements      = new PMXMorph.PMXMorphElement[ds.readInt()];

        if (type == 0){
            for (int k=0;k<elements.length;k++){
                final PMXMorph.PMXMorphGroup md = new PMXMorph.PMXMorphGroup();
                elements[k] = md;
                md.index = ds.readMorphIndex();
                md.ratio = ds.readFloat();
            }
        } else if (type == 1){
            for (int k=0;k<elements.length;k++){
                final PMXMorph.PMXMorphVertex md = new PMXMorph.PMXMorphVertex();
                elements[k] = md;
                md.index    = ds.readVertexIndex();
                md.position = ds.readFloat(3);
            }
        } else if (type == 2){
            for (int k=0;k<elements.length;k++){
                final PMXMorph.PMXMorphBone md = new PMXMorph.PMXMorphBone();
                elements[k] = md;
                md.index    = ds.readBoneIndex();
                md.offset   = ds.readFloat(3);
                md.quaternion = new Quaternion(ds.readFloat(4));
            }
        } else if (type == 3 || type == 4 || type == 5 || type == 6 || type == 7){
            //TODO find difference between those UV types
            for (int k=0;k<elements.length;k++){
                final PMXMorph.PMXMorphUV md = new PMXMorph.PMXMorphUV();
                elements[k] = md;
                md.index    = ds.readVertexIndex();
                md.offset   = ds.readFloat(4);
            }
        } else if (type == 8){
            for (int k=0;k<elements.length;k++){
                final PMXMorph.PMXMorphTexture md = new PMXMorph.PMXMorphTexture();
                elements[k] = md;
                md.index    = ds.readTextureIndex();
                md.data     = ds.readFully(new byte[113]);
            }
        } else {
            throw new IOException(ds, "Unknowned morph type : " + type);
        }
    }

    public void write(PMXOutputStream ds) throws IOException, StoreException{
        ds.writeText(name);
        ds.writeText(englishName);
        ds.writeUByte(panel);
        ds.writeUByte(type);
        ds.writeInt(elements.length);

        if (type == 0){
            for (int k=0;k<elements.length;k++){
                final PMXMorph.PMXMorphGroup md = (PMXMorph.PMXMorphGroup) elements[k];
                ds.writeMorphIndex(md.index);
                ds.writeFloat(md.ratio);
            }
        } else if (type == 1){
            for (int k=0;k<elements.length;k++){
                final PMXMorph.PMXMorphVertex md = (PMXMorph.PMXMorphVertex) elements[k];
                ds.writeVertexIndex(md.index);
                ds.writeFloat(md.position);
            }
        } else if (type == 2){
            for (int k=0;k<elements.length;k++){
                final PMXMorph.PMXMorphBone md = (PMXMorph.PMXMorphBone) elements[k];
                ds.writeBoneIndex(md.index);
                ds.writeFloat(md.offset);
                ds.writeFloat(md.quaternion.toFloat());
            }
        } else if (type == 3 || type == 4 || type == 5 || type == 6 || type == 7){
            //TODO find difference between those UV types
            for (int k=0;k<elements.length;k++){
                final PMXMorph.PMXMorphUV md = (PMXMorph.PMXMorphUV) elements[k];
                ds.writeVertexIndex(md.index);
                ds.writeFloat(md.offset);
            }
        } else if (type == 8){
            for (int k=0;k<elements.length;k++){
                final PMXMorph.PMXMorphTexture md = (PMXMorph.PMXMorphTexture) elements[k];
                ds.writeTextureIndex(md.index);
                ds.write(md.data);
            }
        } else {
            throw new IOException(ds, "Unknowned morph type : " + type);
        }
    }

    public static interface PMXMorphElement{}

    /**
     * Morph group, combine multiple morphs with given ratio.
     */
    public static class PMXMorphGroup implements PMXMorphElement {
        public int index;
        public float ratio;
    }

    /**
     * Morph moving a vertex position.
     */
    public static class PMXMorphVertex implements PMXMorphElement {
        public int index;
        public float[] position;
    }

    /**
     * Morph Bone.
     */
    public static class PMXMorphBone implements PMXMorphElement {
        public int index;
        /** Vector3 , offset */
        public float[] offset;
        /** Bone rotation */
        public Quaternion quaternion;
    }

    /**
     * Morph texture UV.
     */
    public static class PMXMorphUV implements PMXMorphElement {
        public int index;
        /** only for 2 values are used, but 4 values stored */
        public float[] offset;
    }

    /**
     * Morph changing texture.
     */
    public static class PMXMorphTexture implements PMXMorphElement {
        public int index;
        //TODO to decode, seems like 1 byte + 28 float = 113bytes
        public byte[] data;
    }

}
