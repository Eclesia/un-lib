

package science.unlicense.format.mmd.vmd;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class VMDCameraFrame {

    public int frameNumber;
    public VectorRW position;
    public VectorRW rotation;
    //TODO : find more infos on this
    public byte[] interpolation;

    public void read(DataInputStream ds) throws IOException{
        frameNumber = ds.readInt();
        position = VectorNf64.create(ds.readFloat(3));
        rotation = VectorNf64.create(ds.readFloat(3));
        interpolation = ds.readFully(new byte[33]);
    }

}

