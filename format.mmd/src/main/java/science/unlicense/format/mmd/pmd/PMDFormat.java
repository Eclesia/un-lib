
package science.unlicense.format.mmd.pmd;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * Some resource :
 * http://mikumikudance.wikia.com/wiki/PMD_file_format
 *
 * @author Johann Sorel
 */
public class PMDFormat extends AbstractModel3DFormat {

    public static final PMDFormat INSTANCE = new PMDFormat();

    private PMDFormat() {
        super(new Chars("mmd_pmd"));
        shortName = new Chars("MMD-PMD");
        longName = new Chars("MikuMikuDance Model file");
        extensions.add(new Chars("pmd"));
        signatures.add(PMDConstants.SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new PMDStore(input);
    }

}
