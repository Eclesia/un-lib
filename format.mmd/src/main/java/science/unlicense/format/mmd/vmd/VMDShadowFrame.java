

package science.unlicense.format.mmd.vmd;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class VMDShadowFrame {

    public int frameNumber;
    public int type;
    public float distance;

    public void read(DataInputStream ds) throws IOException{
        frameNumber = ds.readInt();
        type        = ds.readUByte();
        distance    = ds.readFloat();
    }

}
