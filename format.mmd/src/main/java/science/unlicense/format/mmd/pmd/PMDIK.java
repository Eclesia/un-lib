package science.unlicense.format.mmd.pmd;

import science.unlicense.common.api.Orderable;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.Maths;


/**
 * PMD IK is an inverse kinematic using CCD method.
 *
 * @author Johann Sorel
 */
public class PMDIK implements Orderable{

    /** Target bone for CCD solver. */
    public int targetBone;
    /** Effector bone. */
    public int effectBone;
    /** Maximum number of loops for the CCD solver. */
    public int maxLoop;
    /** Max angle in each loop for CCD solver. */
    public float maxAngle;
    /** Joints in the kinematic chain. */
    public int[] links;

    /**
     * Order ik by their first chain joint.
     */
    public int order(Object other) {
        final PMDIK oik = (PMDIK) other;
        if (links.length==0 && oik.links.length==0) return 0;
        else if (links.length==0) return -1;
        else if (oik.links.length==0) return +1;
        return links[0] - oik.links[0];
    }

    public void setToDefault(){
        targetBone = -1;
        effectBone = -1;
        maxLoop = 10;
        maxAngle = (float) Maths.TWO_PI;
        links = new int[0];
    }

    public void read(DataInputStream ds) throws IOException{
        targetBone    = ds.readUShort();
        effectBone    = ds.readUShort();
        final int nbLink = ds.readByte();
        maxLoop       = ds.readUShort();
        maxAngle      = ds.readFloat();
        links         = ds.readUShort(nbLink);
    }

    public void write(DataOutputStream ds) throws IOException{
        ds.writeUShort(targetBone);
        ds.writeUShort(effectBone);
        ds.writeByte((byte) links.length);
        ds.writeUShort(maxLoop);
        ds.writeFloat(maxAngle);
        ds.writeUByte(links);
    }

}
