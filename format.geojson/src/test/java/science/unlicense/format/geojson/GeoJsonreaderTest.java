
package science.unlicense.format.geojson;

import science.unlicense.format.geojson.GeoJsonReader;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.impl.DefaultPoint;

/**
 *
 * @author Johann Sorel
 */
public class GeoJsonreaderTest {

    /**
     * Read a collection test.
     */
    @Test
    public void testFeatureCollection() throws IOException {

        final Chars text = new Chars(
                "{\n" +
"       \"type\": \"FeatureCollection\",\n" +
"       \"features\": [{\n" +
"           \"type\": \"Feature\",\n" +
"           \"geometry\": {\n" +
"               \"type\": \"Point\",\n" +
"               \"coordinates\": [102.0, 0.5]\n" +
"           },\n" +
"           \"properties\": {\n" +
"               \"prop0\": \"value0\"\n" +
"           }\n" +
"       }, {\n" +
"           \"type\": \"Feature\",\n" +
"           \"geometry\": {\n" +
"               \"type\": \"LineString\",\n" +
"               \"coordinates\": [\n" +
"                   [102.0, 0.0],\n" +
"                   [103.0, 1.0],\n" +
"                   [104.0, 0.0],\n" +
"                   [105.0, 1.0]\n" +
"               ]\n" +
"           },\n" +
"           \"properties\": {\n" +
"               \"prop0\": \"value0\",\n" +
"               \"prop1\": 0.0\n" +
"           }\n" +
"       }, {\n" +
"           \"type\": \"Feature\",\n" +
"           \"geometry\": {\n" +
"               \"type\": \"Polygon\",\n" +
"               \"coordinates\": [\n" +
"                   [\n" +
"                       [100.0, 0.0],\n" +
"                       [101.0, 0.0],\n" +
"                       [101.0, 1.0],\n" +
"                       [100.0, 1.0],\n" +
"                       [100.0, 0.0]\n" +
"                   ]\n" +
"               ]\n" +
"           },\n" +
"           \"properties\": {\n" +
"               \"prop0\": \"value0\",\n" +
"               \"prop1\": {\n" +
"                   \"this\": \"that\"\n" +
"               }\n" +
"           }\n" +
"       }]\n" +
"   }"
        );

        GeoJsonReader reader = new GeoJsonReader();
        reader.setInput(text.toBytes());

        Assert.assertTrue(reader.hasNext());
        Document doc1 = reader.next();
        Assert.assertTrue(reader.hasNext());
        Document doc2 = reader.next();
        Assert.assertTrue(reader.hasNext());
        Document doc3 = reader.next();
        Assert.assertFalse(reader.hasNext());


    }

    /**
     * Read a single feature test.
     */
    @Test
    public void testFeature() throws IOException {

        final Chars text = new Chars(
            "{\n" +
"           \"type\": \"Feature\",\n" +
"           \"id\": \"FID-123\",\n" +
"           \"bbox\": [-10.0, -20.0, 10.0, 20.0],\n" +
"           \"geometry\": {\n" +
"               \"type\": \"Point\",\n" +
"               \"coordinates\": [102.0, 0.5]\n" +
"           },\n" +
"           \"properties\": {\n" +
"               \"prop0\": \"value0\"\n" +
"           },\n" +
"           \"extra\": \"something\"\n" +
"       }"
        );

        GeoJsonReader reader = new GeoJsonReader();
        reader.setInput(text.toBytes());

        Assert.assertTrue(reader.hasNext());
        Document doc = reader.next();
        Assert.assertEquals(new Chars("FID-123"), doc.getPropertyValue(new Chars("id")));
        Assert.assertEquals(new BBox(new double[]{-10,-20}, new double[]{10,20}), doc.getPropertyValue(new Chars("bbox")));
        Assert.assertEquals(new DefaultPoint(102, 0.5), doc.getPropertyValue(new Chars("geometry")));
        Assert.assertEquals(new Chars("value0"), doc.getPropertyValue(new Chars("prop0")));
        Assert.assertEquals(new Chars("something"), doc.getPropertyValue(new Chars("extra")));

        Assert.assertFalse(reader.hasNext());

    }

}
