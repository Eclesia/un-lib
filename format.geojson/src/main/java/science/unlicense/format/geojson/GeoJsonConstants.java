

package science.unlicense.format.geojson;

import science.unlicense.common.api.character.Chars;

/**
 * GeoJSON constants.
 *
 * @author Johann Sorel
 */
public final class GeoJsonConstants {

    public static final Chars PROP_TYPE         = Chars.constant("type");
    public static final Chars PROP_ID           = Chars.constant("id");
    public static final Chars PROP_BBOX         = Chars.constant("bbox");
    public static final Chars PROP_GEOMETRY     = Chars.constant("geometry");
    public static final Chars PROP_PROPERTIES   = Chars.constant("properties");
    public static final Chars PROP_FEATURES     = Chars.constant("features");
    public static final Chars PROP_COORDINATES  = Chars.constant("coordinates");
    public static final Chars PROP_GEOMETRIES   = Chars.constant("geometries");

    public static final Chars TYPE_FEATURECOLLECTION    = Chars.constant("FeatureCollection");
    public static final Chars TYPE_FEATURE              = Chars.constant("Feature");

    public static final Chars GEOM_POINT                = Chars.constant("Point");
    public static final Chars GEOM_MULTIPOINT           = Chars.constant("MultiPoint");
    public static final Chars GEOM_LINESTRING           = Chars.constant("LineString");
    public static final Chars GEOM_MULTILINESTRING      = Chars.constant("MultiLineString");
    public static final Chars GEOM_POLYGON              = Chars.constant("Polygon");
    public static final Chars GEOM_MULTIPOLYGON         = Chars.constant("MultiPolygon");
    public static final Chars GEOM_GEOMETRYCOLLECTION   = Chars.constant("GeometryCollection");

    private GeoJsonConstants(){}

}
