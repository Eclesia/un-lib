
package science.unlicense.format.geojson;

import java.lang.reflect.Array;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.Documents;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.Reader;
import static science.unlicense.format.geojson.GeoJsonConstants.*;
import science.unlicense.format.json.JSONReader;
import science.unlicense.format.json.JSONUtilities;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.impl.ArrayMultiGeometry;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.MultiPoint;
import science.unlicense.geometry.impl.MultiPolygon;
import science.unlicense.geometry.impl.MultiPolyline;
import science.unlicense.geometry.impl.Polygon;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class GeoJsonReader implements Reader {

    private final JSONReader reader;
    private Iterator features;

    public GeoJsonReader() {
        this.reader = new JSONReader();
    }

    @Override
    public void setInput(Object input) throws IOException {
        reader.setInput(input);
    }

    @Override
    public void setConfiguration(Document configuration) {
        reader.setConfiguration(configuration);
    }

    @Override
    public Document getConfiguration() {
        return reader.getConfiguration();
    }

    @Override
    public Object getInput() {
        return reader.getInput();
    }

    public boolean hasNext() throws IOException {
        init();
        return features.hasNext();
    }

    public Document next() throws IOException {
        init();
        return (Document) features.next();
    }

    private void init() throws IOException {
        if (features!=null) return;

        final Sequence all = new ArraySequence();
        //TODO make a progressive reader
        Document fullDoc = JSONUtilities.readAsDocument(reader, null);
        Object type = fullDoc.getPropertyValue(PROP_TYPE);
        if (TYPE_FEATURECOLLECTION.equals(type)) {
            Object[] values = (Object[]) fullDoc.getPropertyValue(PROP_FEATURES);
            for (Object o : values) {
                all.add(toFeature((Document) o));
            }
        } else if (TYPE_FEATURE.equals(type)) {
            all.add(toFeature(fullDoc));
        }

        this.features = all.createIterator();
    }

    private Document toFeature(Document jsonDoc) throws IOException {

        jsonDoc.setPropertyValue(PROP_TYPE, null);

        //convert bbox
        Object jsonBbox = jsonDoc.getPropertyValue(PROP_BBOX);
        if (jsonBbox != null) {
            final Object[] values = (Object[]) jsonBbox;
            if (values.length==4) {
                BBox bbox = new BBox(2);
                bbox.setRange(0, ((Number) values[0]).doubleValue(), ((Number) values[2]).doubleValue());
                bbox.setRange(1, ((Number) values[1]).doubleValue(), ((Number) values[3]).doubleValue());
                jsonDoc.setPropertyValue(PROP_BBOX, bbox);
            } else if (values.length==6) {
                BBox bbox = new BBox(3);
                bbox.setRange(0, ((Number) values[0]).doubleValue(), ((Number) values[3]).doubleValue());
                bbox.setRange(1, ((Number) values[1]).doubleValue(), ((Number) values[4]).doubleValue());
                bbox.setRange(2, ((Number) values[2]).doubleValue(), ((Number) values[5]).doubleValue());
                jsonDoc.setPropertyValue(PROP_BBOX, bbox);
            } else {
                throw new IOException(null, "Unvalid bbox size "+Arrays.toChars(values));
            }
        }

        //convert geometry
        Document geometry = (Document) jsonDoc.getPropertyValue(PROP_GEOMETRY);
        if (geometry!=null) {
            jsonDoc.setPropertyValue(PROP_GEOMETRY, readGeometry(geometry));
        }

        //flatten properties
        Document properties = (Document) jsonDoc.getPropertyValue(PROP_PROPERTIES);
        if (properties != null) {
            jsonDoc.setPropertyValue(PROP_PROPERTIES, null);
            Documents.overrideFields(jsonDoc, properties);
        }

        return jsonDoc;
    }

    private Geometry readGeometry(Document geometry) throws IOException {
        Object geomType = geometry.getPropertyValue(PROP_TYPE);
        Object coordinates = geometry.getPropertyValue(PROP_COORDINATES);

        final PlanarGeometry geom;
        if (GEOM_POINT.equals(geomType)) {
            geom = readPoint(coordinates);
        } else if (GEOM_MULTIPOINT.equals(geomType)) {
            geom = readMultiPoint(coordinates);
        } else if (GEOM_LINESTRING.equals(geomType)) {
            geom = readLineString(coordinates);
        } else if (GEOM_MULTILINESTRING.equals(geomType)) {
            geom = readMultiLineString(coordinates);
        } else if (GEOM_POLYGON.equals(geomType)) {
            geom = readPolygon(coordinates);
        } else if (GEOM_MULTIPOLYGON.equals(geomType)) {
            geom = readMultiPolygon(coordinates);
        } else if (GEOM_GEOMETRYCOLLECTION.equals(geomType)) {
            Object subs = geometry.getPropertyValue(PROP_GEOMETRIES);
            final int nbGeom = Array.getLength(subs);
            final Sequence geometries = new ArraySequence(nbGeom);
            for (int i=0;i<nbGeom;i++) {
                geometries.add(readGeometry((Document) Array.get(subs, i)));
            }
            geom = new ArrayMultiGeometry(geometries);

        } else {
            throw new IOException(null, "Unexpected geometry type "+geomType);
        }

        return geom;
    }

    private Point readPoint(Object coordinates) {
        return new DefaultPoint(readTuple(coordinates));
    }

    private MultiPoint readMultiPoint(Object coordinates) {
        final Sequence points = readTuples(coordinates);
        for (int i=0,n=points.getSize();i<n;i++) {
            points.replace(i, new DefaultPoint((TupleRW) points.get(i)));
        }
        return new MultiPoint(points);
    }

    private Polyline readLineString(Object coordinates) {
        final double[] coords = readTuplesArray(coordinates);
        return new Polyline(InterleavedTupleGrid1D.create(coords,tupleSize));
    }

    private MultiPolyline readMultiLineString(Object coordinates) {
        final int nbGeom = Array.getLength(coordinates);
        final Sequence geometries = new ArraySequence(nbGeom);
        for (int i=0;i<nbGeom;i++) {
            final double[] coords = readTuplesArray(Array.get(coordinates, i));
            geometries.add(new Polyline(InterleavedTupleGrid1D.create(coords,tupleSize)));
        }
        return new MultiPolyline(geometries);
    }

    private Polygon readPolygon(Object coordinates) {
        final int nbGeom = Array.getLength(coordinates);
        double[] coords = readTuplesArray(Array.get(coordinates, 0));
        final Polyline outer = new Polyline(InterleavedTupleGrid1D.create(coords, tupleSize));
        final Sequence geometries = new ArraySequence(nbGeom-1);
        for (int i=1;i<nbGeom;i++) {
            coords = readTuplesArray(Array.get(coordinates, i));
            geometries.add(new Polyline(InterleavedTupleGrid1D.create(coords,tupleSize)));
        }
        return new Polygon(outer, geometries);
    }

    private MultiPolygon readMultiPolygon(Object coordinates) {
        final int nbGeom = Array.getLength(coordinates);
        final Sequence geometries = new ArraySequence(nbGeom);
        for (int i=0;i<nbGeom;i++) {
            geometries.add(readPolygon(Array.get(coordinates, i)));
        }
        return new MultiPolygon(geometries);
    }


    private static Sequence readTuples(Object array) {
        final int length = Array.getLength(array);
        final Sequence tuples = new ArraySequence(length);
        for (int i=0;i<length;i++) {
            tuples.add(readTuple(Array.get(array, i)));
        }
        return tuples;
    }

    private int tupleSize;
    private double[] readTuplesArray(Object array) {
        final int length = Array.getLength(array);
        double[] tuples = null;
        for (int i=0;i<length;i++) {
            Object tuple = Array.get(array, i);
            if (tuples==null) {
                tupleSize = Array.getLength(tuple);
                tuples = new double[length*tupleSize];
            }
            for (int k=0;k<tupleSize;k++) {
                tuples[i*tupleSize+k] = ((Number) Array.get(tuple, k)).doubleValue();
            }
        }

        return tuples;
    }

    private static VectorRW readTuple(Object array) {
        final int length = Array.getLength(array);
        final VectorRW t = VectorNf64.createDouble(length);
        t.setXY(((Number) Array.get(array, 0)).doubleValue(), ((Number) Array.get(array, 1)).doubleValue());
        if (length>=3) t.setZ(((Number) Array.get(array, 2)).doubleValue());
        return t;
    }

    @Override
    public void dispose() throws IOException {
        reader.dispose();
    }

}
