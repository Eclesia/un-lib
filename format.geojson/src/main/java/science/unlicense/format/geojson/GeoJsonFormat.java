

package science.unlicense.format.geojson;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * Graphtec Vector Graphic Data format.
 *
 * Resource :
 * https://tools.ietf.org/html/rfc7946
 * http://geojson.org/geojson-spec.html
 * https://en.wikipedia.org/wiki/GeoJSON
 *
 * @author Johann Sorel
 */
public class GeoJsonFormat extends DefaultFormat {

    public GeoJsonFormat() {
        super(new Chars("geojson"));
        shortName = new Chars("GeoJSON");
        longName = new Chars("Geographic data structures in JSON");
        mimeTypes.add(new Chars("application/vnd.geo+json"));
        extensions.add(new Chars("geojson"));
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
