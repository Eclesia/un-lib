
package science.unlicense.format.etc;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * resources :
 * http://www.khronos.org/opengles/sdk/tools/KTX/file_format_spec/
 * http://en.wikipedia.org/wiki/Ericsson_Texture_Compression
 *
 * @author Johann Sorel
 */
public class ETCFormat extends AbstractImageFormat {

    public static final ETCFormat INSTANCE = new ETCFormat();

    private ETCFormat() {
        super(new Chars("etc"));
        shortName = new Chars("ETC");
        longName = new Chars("Ericsson Texture Compression");
        extensions.add(new Chars("etc"));
        extensions.add(new Chars("ftx"));
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new ETCStore(this, source);
    }

}
