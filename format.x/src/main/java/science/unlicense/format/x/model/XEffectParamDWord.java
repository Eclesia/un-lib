
package science.unlicense.format.x.model;

import science.unlicense.format.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XEffectParamDWord extends XObject{

    public XEffectParamDWord() {
        super(XConstants.TYPE_EFFECT_PARAM_DWORD);
    }



}
