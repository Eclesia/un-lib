
package science.unlicense.format.x;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.Reader;

/**
 *
 * @author Johann Sorel
 */
public interface XReader extends Reader {

    public Sequence read() throws IOException;

}
