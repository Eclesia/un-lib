
package science.unlicense.format.x.model;

import science.unlicense.format.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XMeshMaterialList extends XObject{

    public XMeshMaterialList() {
        super(XConstants.TYPE_MESH_MATERIAL_LIST);
    }



}
