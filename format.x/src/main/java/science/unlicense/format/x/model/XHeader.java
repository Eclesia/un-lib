
package science.unlicense.format.x.model;

import science.unlicense.format.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XHeader extends XObject{

    public XHeader() {
        super(XConstants.TYPE_HEADER);
    }

}
