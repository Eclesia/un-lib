
package science.unlicense.format.x.model;

import science.unlicense.format.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XCompressedAnimationSet extends XObject{

    public XCompressedAnimationSet() {
        super(XConstants.TYPE_COMPRESSED_ANIMATION_SET);
    }



}
