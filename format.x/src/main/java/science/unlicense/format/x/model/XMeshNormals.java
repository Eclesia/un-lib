
package science.unlicense.format.x.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.format.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XMeshNormals extends XObject{

    public XMeshNormals() {
        super(XConstants.TYPE_MESH_NORMALS);
    }

    public float[] getNormals(){
         final Object[] xnormals = (Object[]) getPropertyValue(new Chars("normals"));

         float[] normals = new float[xnormals.length*3];
         for (int i=0,k=0;i<xnormals.length;i++,k+=3){
             normals[k+0] = ((Number) ((XVector) xnormals[i]).getPropertyValue(new Chars("x"))).floatValue();
             normals[k+1] = ((Number) ((XVector) xnormals[i]).getPropertyValue(new Chars("y"))).floatValue();
             normals[k+2] = ((Number) ((XVector) xnormals[i]).getPropertyValue(new Chars("z"))).floatValue();
         }

         return normals;
    }

}
