
package science.unlicense.format.x.model;

import science.unlicense.format.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XBoolean extends XObject{

    public XBoolean() {
        super(XConstants.TYPE_BOOLEAN);
    }



}
