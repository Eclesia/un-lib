
package science.unlicense.format.x.model;

import science.unlicense.format.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XVertexDuplicationIndices extends XObject{

    public XVertexDuplicationIndices() {
        super(XConstants.TYPE_VERTEX_DUPLICATION_INDICES);
    }


}
