
package science.unlicense.format.x;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * Reference :
 * http://msdn.microsoft.com/en-us/library/windows/desktop/bb173014(v=vs.85).aspx
 * http://msdn.microsoft.com/en-us/library/windows/desktop/bb174837(v=vs.85).aspx
 * http://paulbourke.net/dataformats/directx/
 * http://www.xbdev.net/3dformats/x/xfileformat.php
 * http://www.cgdev.net/axe/x-file.html
 *
 * @author Johann Sorel
 */
public class XFormat extends AbstractModel3DFormat {

    static final byte[] SIGNATURE = new byte[]{'x','o','f',' '};
    public static final XFormat INSTANCE = new XFormat();

    private XFormat() {
        super(new Chars("X"));
        shortName = new Chars("DirectX");
        longName = new Chars("DirectX");
        extensions.add(new Chars("x"));
        extensions.add(new Chars("xch"));
        signatures.add(SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new XStore((Path) input);
    }

}
