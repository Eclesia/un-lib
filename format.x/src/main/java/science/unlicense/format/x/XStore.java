
package science.unlicense.format.x;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.encoding.impl.io.deflate.DeflateInputStream;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.format.x.model.XFileHeader;
import science.unlicense.format.x.model.XFrame;
import science.unlicense.format.x.model.XMesh;


/**
 * DirectX 3D Format store.
 *
 * @author Johann Sorel
 */
public class XStore extends AbstractModel3DStore {


    public XStore(Object input) {
        super(XFormat.INSTANCE, input);
    }


    public Collection getElements() throws StoreException {
        final Sequence all = new ArraySequence();

        //parse fil to docs
        final Sequence docs;
        try {
            docs = read();
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        //convert to models
        for (int i=0,n=docs.getSize();i<n;i++){
            Object candidate = docs.get(i);

            if (candidate instanceof XFrame){
                final Object[] children = (Object[]) ((XFrame) candidate).getPropertyValue(new Chars("children"));
                for (Object o : children){
                    if (o instanceof XMesh){
                        all.add(toMesh((XMesh) o));
                    }
                }
            } else if (candidate instanceof XMesh){
                all.add(toMesh((XMesh) candidate));
            }
        }

        return all;
    }

    private DefaultModel toMesh(XMesh xmesh){
        final float[] vertices = xmesh.getVertices();
        final float[] normals = xmesh.getNormals();
        final int[] faces = xmesh.getFaces();

        final DefaultModel mesh = new DefaultModel();
        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(new VBO(vertices, 3));
        shell.setIndex(new IBO(faces));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, faces.length)});
        mesh.setShape(shell);

        if (normals==null){
            Mesh.calculateNormals(shell);
        } else {
            shell.setNormals(new VBO(normals, 3));
        }

        mesh.updateBoundingBox();

        final SimpleBlinnPhong.Material mat = SimpleBlinnPhong.newMaterial();
        mat.setDiffuse(Color.GRAY_NORMAL);
        mesh.getMaterials().add(mat);
        return mesh;
    }

    /**
     * Parse file to DocumentType and Document.
     *
     * @return
     * @throws IOException
     */
    Sequence read() throws IOException{

        //read file header
        ByteInputStream in = getSourceAsInputStream();
        final XFileHeader header = new XFileHeader();
        header.read(new DataInputStream(in));

        final XReader reader;
        if (XConstants.FORMAT_TXT.equals(header.format)){
            reader = new XTxtReader();
        } else if (XConstants.FORMAT_TZIP.equals(header.format)){
            in = new DeflateInputStream(in);
            reader = new XTxtReader();
        } else if (XConstants.FORMAT_BIN.equals(header.format)){
            reader = new XBinReader();
        } else if (XConstants.FORMAT_BZIP.equals(header.format)){
            in = new DeflateInputStream(in);
            reader = new XBinReader();
        } else {
            throw new IOException("Unknowned format "+header.format);
        }
        reader.setInput(in);
        return reader.read();
    }

}
