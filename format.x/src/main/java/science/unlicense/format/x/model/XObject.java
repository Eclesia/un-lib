
package science.unlicense.format.x.model;

import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.DocumentType;

/**
 *
 * @author Johann Sorel
 */
public class XObject extends DefaultDocument{

    public XObject(DocumentType type) {
        super(true,type);
    }

}
