
package science.unlicense.format.x.model;

import science.unlicense.format.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XAnimTicksPerSecond extends XObject{

    public XAnimTicksPerSecond() {
        super(XConstants.TYPE_ANIM_TICK_PER_SECOND);
    }



}
