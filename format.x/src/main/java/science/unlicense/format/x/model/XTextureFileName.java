
package science.unlicense.format.x.model;

import science.unlicense.format.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XTextureFileName extends XObject{

    public XTextureFileName() {
        super(XConstants.TYPE_TEXTURE_FILENAME);
    }



}
