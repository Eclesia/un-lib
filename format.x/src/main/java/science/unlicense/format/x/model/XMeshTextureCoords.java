
package science.unlicense.format.x.model;

import science.unlicense.format.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XMeshTextureCoords extends XObject{

    public XMeshTextureCoords() {
        super(XConstants.TYPE_MESH_TEXTURE_COORDS);
    }



}
