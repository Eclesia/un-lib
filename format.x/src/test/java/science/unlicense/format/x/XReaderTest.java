
package science.unlicense.format.x;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.store.StoreException;

/**
 *
 * @author Johann Sorel
 */
public class XReaderTest {

    @Test
    public void testRead() throws StoreException{

        final XStore store = new XStore(Paths.resolve(new Chars("mod:/science/unlicense/format/x/testFile.x")));

        final Collection res = store.getElements();

    }

}
