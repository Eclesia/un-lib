
package science.unlicense.format.pic;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * @author Johann Sorel
 */
public class PICFormat extends AbstractImageFormat {

    public static final PICFormat INSTANCE = new PICFormat();

    private PICFormat() {
        super(new Chars("pic"));
        shortName = new Chars("PIC");
        longName = new Chars("PICtor");
        mimeTypes.add(new Chars("image/pic"));
        extensions.add(new Chars("pic"));
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new PICStore(this, source);
    }

}
