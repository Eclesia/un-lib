
package science.unlicense.code.gcode.model;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class GBlock {

    public GCommand[] commands;
    /**
     * Optional comment.
     */
    public Chars comment;

}
