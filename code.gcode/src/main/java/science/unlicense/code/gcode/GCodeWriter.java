
package science.unlicense.code.gcode;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.code.gcode.model.GBlock;

/**
 *
 * @author Johann Sorel
 */
public class GCodeWriter extends AbstractWriter {

    public GCodeWriter() {
    }

    public void writer(GBlock block) throws IOException {
        final CharOutputStream cs = getOutputAsCharStream(CharEncodings.UTF_8);

        if (block.commands.length==0) {
            if (block.comment!= null) {
                cs.write(';').write(' ').write(block.comment);
            }
        } else {

        }


    }

}
