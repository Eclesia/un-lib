
package science.unlicense.code.gcode;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class GConstants {

    /** Default commands */
    public final Chars PREFIX_G = new Chars("G");
    /** Select tool */
    public final Chars PREFIX_T = new Chars("T");
    /** Command parameter, like time(seconds) */
    public final Chars PREFIX_S = new Chars("S");
    /** Command parameter, like time(milliseconds) */
    public final Chars PREFIX_P = new Chars("P");
    /** X axis coordinate */
    public final Chars PREFIX_X = new Chars("X");
    /** Y axis coordinate */
    public final Chars PREFIX_Y = new Chars("Y");
    /** Z axis coordinate */
    public final Chars PREFIX_Z = new Chars("Z");
    /** U axis coordinate */
    public final Chars PREFIX_U = new Chars("U");
    /** V axis coordinate */
    public final Chars PREFIX_V = new Chars("V");
    /** W axis coordinate */
    public final Chars PREFIX_W = new Chars("W");
    /** X-offset for arc movement */
    public final Chars PREFIX_I = new Chars("I");
    /** Y-offset for arc movement*/
    public final Chars PREFIX_J = new Chars("J");
    /** Diameter parameter */
    public final Chars PREFIX_D = new Chars("D");
    /** Heater number parameter */
    public final Chars PREFIX_H = new Chars("H");
    /** Feedrate in mm/min , speed of the head*/
    public final Chars PREFIX_F = new Chars("F");
    /** Temperature parameter */
    public final Chars PREFIX_R = new Chars("R");
    /** Length of filament to consume */
    public final Chars PREFIX_E = new Chars("E");
    /** Line number, for errors ?*/
    public final Chars PREFIX_N = new Chars("N");


    private GConstants() {}


}
