
package science.unlicense.code.gcode;

import science.unlicense.code.api.CodeFileReader;
import science.unlicense.code.api.CodeFormat;
import science.unlicense.code.api.CodeProducer;
import science.unlicense.code.impl.GrammarCodeFileReader;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 * G-Code numerical command language.
 *
 * References :
 * https://en.wikipedia.org/wiki/G-code
 * http://reprap.org/wiki/G-code
 * http://wiki.solidoodle.com/gcode-guide
 *
 * @author Johann Sorel
 */
public class GFormat extends DefaultFormat implements CodeFormat {

    public static final GFormat INSTANCE = new GFormat();

    private GFormat() {
        super(new Chars("gcode"));
        shortName = new Chars("G-Code");
        longName = new Chars("Numerical command G-Code language");
        extensions.add(new Chars("mpt"));
        extensions.add(new Chars("mpf"));
        extensions.add(new Chars("nc"));
        extensions.add(new Chars("g"));
        extensions.add(new Chars("gco"));
        extensions.add(new Chars("gcode"));
    }

    @Override
    public CodeFileReader createReader() {
        return new GrammarCodeFileReader(null);
    }

    @Override
    public CodeProducer createProducer() {
        return null;
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
