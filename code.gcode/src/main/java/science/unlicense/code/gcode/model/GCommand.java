
package science.unlicense.code.gcode.model;

import science.unlicense.common.api.character.Chars;


/**
 *
 * @author Johann Sorel
 */
public class GCommand {

    /**
     * Unicode letter code
     */
    public int code;
    /**
     * Value associated to code
     */
    public Number value;
    /**
     * Optional comment.
     */
    public Chars comment;

}
