
package science.unlicense.code.gcode;

import org.junit.Test;
import science.unlicense.code.gcode.model.GBlock;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class GReaderTest {

    @Test
    public void testRead() throws IOException{
        final String code =
                "; program header\n" +
                "G0 X2 (do something)\n" +
                "(block comment) G0 X5 (X is 5) Y3.14 (Y is 3) (and .14)\n" +
                "S4 ; end";

        final GReader reader = new GReader();
        reader.setInput(new Chars(code).toBytes());

        final GBlock block1 = reader.nexBlock();
        Assert.assertEquals(0, block1.commands.length);
        Assert.assertEquals(new Chars("program header"), block1.comment);

        final GBlock block2 = reader.nexBlock();
        Assert.assertEquals(2, block2.commands.length);
        Assert.assertEquals(null, block2.comment);
        Assert.assertEquals('G', block2.commands[0].code);
        Assert.assertEquals(0d, block2.commands[0].value);
        Assert.assertEquals(null, block2.commands[0].comment);
        Assert.assertEquals('X', block2.commands[1].code);
        Assert.assertEquals(2d, block2.commands[1].value);
        Assert.assertEquals(new Chars("do something"), block2.commands[1].comment);

        final GBlock block3 = reader.nexBlock();
        Assert.assertEquals(3, block3.commands.length);
        Assert.assertEquals(new Chars("block comment"), block3.comment);
        Assert.assertEquals('G', block3.commands[0].code);
        Assert.assertEquals(0d, block3.commands[0].value);
        Assert.assertEquals(null, block3.commands[0].comment);
        Assert.assertEquals('X', block3.commands[1].code);
        Assert.assertEquals(5d, block3.commands[1].value);
        Assert.assertEquals(new Chars("X is 5"), block3.commands[1].comment);
        Assert.assertEquals('Y', block3.commands[2].code);
        Assert.assertEquals(3.14d, block3.commands[2].value);
        Assert.assertEquals(new Chars("Y is 3,and .14"), block3.commands[2].comment);

        final GBlock block4 = reader.nexBlock();
        Assert.assertEquals(1, block4.commands.length);
        Assert.assertEquals(new Chars("end"), block4.comment);
        Assert.assertEquals('S', block4.commands[0].code);
        Assert.assertEquals(4d, block4.commands[0].value);
        Assert.assertEquals(null, block4.commands[0].comment);

        Assert.assertNull(reader.nexBlock());
    }

}
