
package science.unlicense.protocol.irc;

import java.util.Scanner;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Aurélien Lombardo
 */
public final class Main {

    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            IRCClient client = new IRCClient(new Chars("barjavel.freenode.net"), 6667);
            boolean isOK = false;
            while (!isOK) {
                System.out.println("Renseigner un nickname :");
                String nick = sc.nextLine();
                isOK = client.connect(new Chars(nick));
            }

            client.join(new Chars("#botwar"));

            Chars line = null;
            while ((line = client.receive()) != null) {
                System.out.println(line);
                if (line.getFirstOccurence(new Chars("PING")) >= 0) {
                    client.pong();
                }
            }

            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Main(){}

}
