
package science.unlicense.protocol.irc;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.IPAddress;

/**
 *
 * @author Aurélien Lombardo
 */
public class IRCClient {
    private ClientSocket socket;
    private CharInputStream input;
    private CharOutputStream output;

    /**
     * Create an irc client on default port (7).
     *
     * @param host
     * @throws IOException
     */
    public IRCClient(Chars host) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host));
    }

    /**
     * Create an irc client on default port (7).
     *
     * @param address
     * @throws IOException
     */
    public IRCClient(IPAddress address) throws IOException {
        this(address, 194);
    }

    /**
     * Create an irc client.
     *
     * @param host
     * @param port
     * @throws IOException
     */
    public IRCClient(Chars host, int port) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host), port);
    }

    /**
     * Create an irc client.
     *
     * @param address
     * @param port
     * @throws IOException
     */
    public IRCClient(IPAddress address, int port) throws IOException {
        socket = science.unlicense.system.System.get().getSocketManager().createClientSocket(address, port);
        input = new CharInputStream(socket.getInputStream(), CharEncodings.US_ASCII);
        output = new CharOutputStream(socket.getOutputStream());
    }

    /**
     * Connect to the server with the nick parameter
     *
     * @param nick
     * @return
     * @throws IOException
     */
    public boolean connect(Chars nick) throws IOException {
        send(new Chars("NICK " + nick + "\r\nUSER " + nick + " 8 * :test\r\n"));
        Chars line = null;
        while ((line = receive()) != null) {
            if (line.getFirstOccurence(new Chars("004")) >= 0) {
                return true;
            } else if (line.getFirstOccurence(new Chars("433")) >= 0) {
                return false;
            }
        }
        return false;
    }

    /**
     * Join the channel
     *
     * @param channel
     * @throws IOException
     */
    public void join(Chars channel) throws IOException {
        send(new Chars("JOIN " + channel + "\r\n"));
    }

    /**
     * Answer a pong after a ping request
     *
     * @throws IOException
     */
    public void pong() throws IOException {
        send(new Chars("PONG\r\n"));
    }

    /**
     * Send datas and wait for echo response.
     *
     * @param data byte array to send.
     * @return byte[] should contain the same values that was send.
     * @throws IOException
     */
    public void send(Chars data) throws IOException{
        output.write(data);
        output.flush();
    }

    /**
     * Get message from the server
     *
     * @return
     * @throws IOException
     */
    public Chars receive() throws IOException {
        return input.readLine();
    }

    /**
     * Close IRC client.
     *
     * @throws IOException
     */
    public void close() throws IOException{
        output.close();
        input.dispose();
        socket.close();
    }
}
