

package science.unlicense.model3d.impl;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.buffer.DefaultBufferFactory;

/**
 *
 * @author Johann Sorel
 */
public class MeshUtilitiesTest {

    @Test
    public void testAdjency(){

        // 2---3---4
        //  \  |\  |
        //   \ | \ |
        //     1---5
        //      \  |
        //       \ |
        //        6
        final int[] base = {
            1,2,3,
            1,3,5,
            5,3,4,
            6,1,5
        };

        final int[] expected = {
            1, 2,2, 3,3, 5,
            1, 2,3, 4,5, 6,
            5, 1,3, 4,4, 5,
            6, 1,1, 3,5, 6
        };

        final int[] res = MeshUtilities.createAdjencyIndex(DefaultBufferFactory.wrap(base)).toIntArray();
        Assert.assertArrayEquals(expected, res);

    }

}
