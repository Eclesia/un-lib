
package science.unlicense.model3d.impl.operation;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public class MeshBufferOpTest {

    //TODO
    @Ignore
    @Test
    public void testOneJoint(){

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        final Skin skin = new Skin();

        shell.setPositions(InterleavedTupleGrid1D.create(new float[]{0,1,2, 2,3,4, 4,5,6}, 3));
        shell.setIndex(InterleavedTupleGrid1D.create(new int[]{0,1,2},1));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 3)});
        shell.getAttributes().add(Skin.ATT_JOINTS_0, InterleavedTupleGrid1D.create(new int[]{0,0,0}, 1));
        shell.getAttributes().add(Skin.ATT_WEIGHTS_0, InterleavedTupleGrid1D.create(new float[]{1,1,1}, 1));
        skin.setMaxWeightPerVertex(1);

        final Joint joint = new Joint(3);
        joint.setId(0);
        final Skeleton skeleton = new Skeleton();
        skeleton.getChildren().add(joint);
        skin.setSkeleton(skeleton);

        final DefaultModel mesh = new DefaultModel();
        mesh.setShape(shell);
        mesh.setSkin(skin);

        new MeshBufferOp(mesh, 2, 0);

        float[] array = Geometries.toFloat32(shell.getPositions());

        Assert.assertArrayEquals(new float[]{0,2,4, 4,6,8, 8,10,12}, array, 0.00001f);

    }

    @Test
    public void testOneJointConstant(){

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        final Skin skin = new Skin();

        shell.setPositions(InterleavedTupleGrid1D.create(new float[]{0,1,0, 2,0,0, 0,0,6}, 3));
        shell.setIndex(InterleavedTupleGrid1D.create(new int[]{0,1,2},1));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 3)});
        shell.getAttributes().add(Skin.ATT_JOINTS_0, InterleavedTupleGrid1D.create(new int[]{0,0,0}, 1));
        shell.getAttributes().add(Skin.ATT_WEIGHTS_0, InterleavedTupleGrid1D.create(new float[]{1,1,1}, 1));
        skin.setMaxWeightPerVertex(1);

        final Joint joint = new Joint(3);
        joint.setId(0);
        final Skeleton skeleton = new Skeleton();
        skeleton.getChildren().add(joint);
        skin.setSkeleton(skeleton);

        final DefaultModel mesh = new DefaultModel();
        mesh.setShape(shell);
        mesh.setSkin(skin);

        new MeshBufferOp(mesh, 1, 0.1);

        float[] array = Geometries.toFloat32(shell.getPositions());

        Assert.assertArrayEquals(new float[]{0,1.1f,0, 2.1f,0,0, 0,0,6.1f}, array, 0.00001f);

    }

    //TODO
    @Ignore
    @Test
    public void testTwoJoint(){

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        final Skin skin = new Skin();

        shell.setPositions(InterleavedTupleGrid1D.create(new float[]{
            4,15,0,  //will be pushed on -x
            8,10,0,  //will be pushed on +x
            5,22,0   //will be pushed on +y
            }, 3));
        shell.setIndex(InterleavedTupleGrid1D.create(new int[]{0,1,2},1));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 3)});
        shell.getAttributes().add(Skin.ATT_JOINTS_0, InterleavedTupleGrid1D.create(new int[]{0,1, 0,1, 0,1}, 2));
        shell.getAttributes().add(Skin.ATT_WEIGHTS_0, InterleavedTupleGrid1D.create(new float[]{0.5f,0.5f, 0.5f,0.5f, 0.5f,0.5f}, 2));
        skin.setMaxWeightPerVertex(2);

        final Joint joint0 = new Joint(3);
        joint0.setId(0);
        joint0.getNodeTransform().set(new Matrix4x4(
                1, 0, 0, 5,
                0, 1, 0, 10,
                0, 0, 1, 0,
                0, 0, 0, 1));
        final Joint joint1 = new Joint(3);
        joint1.setId(1);
        joint1.getNodeTransform().set(new Matrix4x4(
                1, 0, 0, 5,
                0, 1, 0, 20,
                0, 0, 1, 0,
                0, 0, 0, 1));
        final Skeleton skeleton = new Skeleton();
        skeleton.getChildren().add(joint0);
        skeleton.getChildren().add(joint1);
        skin.setSkeleton(skeleton);


        final DefaultModel mesh = new DefaultModel();
        mesh.setShape(shell);
        mesh.setSkin(skin);

        new MeshBufferOp(mesh, 2, 0);

        float[] array = Geometries.toFloat32(shell.getPositions());

        Assert.assertArrayEquals(new float[]{
            3,15,0,
            11,10,0,
            5,24,0
            }, array, 0.00001f);

    }

}
