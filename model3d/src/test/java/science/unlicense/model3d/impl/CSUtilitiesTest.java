

package science.unlicense.model3d.impl;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.system.Axis;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.system.DefaultCoordinateSystem;
import science.unlicense.geometry.api.system.Direction;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.unitold.Units;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public class CSUtilitiesTest {

    private static final float DELTA = 0.00000001f;

    @Test
    public void testInvertHandVector(){
        final VectorRW v = new Vector3f64(5, 2, 3);
        CSUtilities.invertHandCS(v);
        Assert.assertEquals(new Vector3f64(5, 2, -3), v);
    }

    @Test
    public void testInvertHandVBO(){
        final TupleGrid1D vbo = InterleavedTupleGrid1D.create(new float[]{1,2,3,4,5,6,7,8,9}, 3);
        CSUtilities.invertHandCS(vbo);
        Assert.assertArrayEquals(new float[]{1,2,-3,4,5,-6,7,8,-9}, Geometries.toFloat32(vbo), DELTA);
    }

    @Test
    public void testInvertHandMatrix3(){
        Matrix3x3 m = new Matrix3x3(
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        CSUtilities.invertHandCS(m);
        Assert.assertEquals(new Matrix3x3(
                1, 2,-3,
                4, 5,-6,
               -7,-8, 9), m);
    }

    @Test
    public void testInvertHandMatrix4(){
        final Matrix4x4 m = new Matrix4x4(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9,10,11,12,
               13,14,15,16);
        CSUtilities.invertHandCS(m);
        Assert.assertEquals(new Matrix4x4(
                1,  2, -3,  4,
                5,  6, -7,  8,
               -9,-10, 11,-12,
               13, 14,-15, 16), m);
    }

    @Test
    public void testInvertHandMatrix(){
        MatrixRW ma = new Matrix3x3(
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        CSUtilities.invertHandCS((MatrixRW) ma  );
        Assert.assertEquals(new Matrix3x3(
                1, 2,-3,
                4, 5,-6,
               -7,-8, 9), ma );

        ma = new Matrix4x4(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9,10,11,12,
               13,14,15,16);
        CSUtilities.invertHandCS(ma);
        Assert.assertEquals(new Matrix4x4(
                1,  2, -3,  4,
                5,  6, -7,  8,
               -9,-10, 11,-12,
               13, 14,-15, 16), ma);
    }

    @Test
    public void testTransformNode(){

        final CoordinateSystem sourceCs = new DefaultCoordinateSystem(
            new Axis[]{
                new Axis(Direction.BACKWARD, Units.METER),
                new Axis(Direction.LEFT, Units.METER),
                new Axis(Direction.UP, Units.METER),
            }
        );
        final CoordinateSystem targetCs = new DefaultCoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.CENTIMETER),
                new Axis(Direction.UP, Units.CENTIMETER),
                new Axis(Direction.FORWARD, Units.CENTIMETER)
            }
        );


        //create a small scene
        final SceneNode root = new DefaultSceneNode(sourceCs);
        final SceneNode branch = new DefaultSceneNode(sourceCs);
        final MotionModel mpm = new DefaultMotionModel(sourceCs);
        final DefaultModel mesh = new DefaultModel(sourceCs);

        root.getChildren().add(branch);
        branch.getChildren().add(mpm);
        mpm.getChildren().add(mesh);

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(InterleavedTupleGrid1D.create(new float[]{1,2,3, 4,5,6, 7,8,9, 10,11,12}, 3));
        shell.setNormals(InterleavedTupleGrid1D.create(new float[]{1,0,0, 0,1,0, 0,0,1,  0, 1, 0}, 3));
        shell.setUVs(InterleavedTupleGrid1D.create(new float[]{0,0, 1,0, 1,1, 0,1}, 2));
        shell.setIndex(InterleavedTupleGrid1D.create(new int[]{0,1,2, 2,3,0},1));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 6)});
        mesh.setShape(shell);


        //transform
        CSUtilities.transform(root, targetCs);

        //check result
        Assert.assertTrue(root.getCoordinateSystem() == targetCs);
        Assert.assertTrue(branch.getCoordinateSystem() == targetCs);
        Assert.assertTrue(mpm.getCoordinateSystem() == targetCs);
        Assert.assertTrue(mesh.getCoordinateSystem() == targetCs);

        //check node transform changed

        //TODO

//        Assert.assertEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 1, 0, 0,
//                0, 0, 1, 0,
//                0, 0, 0, 1),
//                root.getNodeTransform().asMatrix());


        final float[] resVertices = Geometries.toFloat32(shell.getPositions());
        Assert.assertArrayEquals(new float[]{
             -200,  300,  -100,
             -500,  600,  -400,
             -800,  900,  -700,
            -1100, 1200, -1000
            }, resVertices, DELTA);

    }

}
