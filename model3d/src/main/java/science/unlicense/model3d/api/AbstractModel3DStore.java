
package science.unlicense.model3d.api;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.encoding.api.store.StoreException;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractModel3DStore extends AbstractStore implements SceneResource {

    public AbstractModel3DStore(Format format, Object input) {
        super(format,input);
    }

    @Override
    public Collection searchElements(Class type) throws StoreException{
        final Collection col = new ArraySequence();
        final Collection ele = getElements();
        for (Iterator ite = ele.createIterator();ite.hasNext();){
            final Object candidate = ite.next();
            if (type.isInstance(candidate)){
                col.add(candidate);
            }
        }
        return col;
    }

    public ByteInputStream getSourceAsInputStream() throws IOException {
        return IOUtilities.toInputStream(source, new boolean[1]);
    }

    public ByteOutputStream getSourceAsOutputStream() throws IOException {
        return IOUtilities.toOutputStream(source, new boolean[1]);
    }

    @Override
    public void writeElements(Collection elements) throws StoreException {
        throw new StoreException("Not supported.");
    }

}
