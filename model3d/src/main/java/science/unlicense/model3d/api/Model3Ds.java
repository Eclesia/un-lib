
package science.unlicense.model3d.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.ViewNode;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.concurrent.api.Formats;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public final class Model3Ds {

    private Model3Ds(){}

    /**
     * Lists available 3d models formats.
     * 3D formats are formats which declare to support SceneResource.
     *
     * @return array of Format, never null but can be empty.
     */
    public static Format[] getFormats(){
        return (Format[]) Formats.getFormatsForResourceType(SceneResource.class);
    }

    public static boolean canDecode(Object input) throws IOException {
        return Formats.canDecode(input, new ArraySequence(getFormats()));
    }

    /**
     * Convinient method to open a store of unknowned format.
     * The method will loop on available formats until one can decode the source.
     *
     * @param input
     * @return MediaStore, never null
     * @throws IOException if not format could support the source.
     */
    public static Store open(Object input) throws IOException {
        return Formats.open(input, new ArraySequence(getFormats()));
    }

    public static Format getFormat(Chars name) throws InvalidArgumentException{
        final Format[] formats = getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i].getIdentifier().equals(name)) {
                return formats[i];
            }
        }
        throw new InvalidArgumentException("Format "+name+" not found.");
    }

    public static Predicate createPredicate(){
        return new Predicate() {
            @Override
            public Boolean evaluate(Object candidate) {
                try {
                    if (candidate instanceof Path && ((Path) candidate).isContainer()){
                        return true;
                    }

                    candidate = ViewNode.unWrap((Node) candidate);
                    return canDecode(candidate);
                } catch (IOException ex) {
                    Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                }
                return false;
            }
        };
    }

}
