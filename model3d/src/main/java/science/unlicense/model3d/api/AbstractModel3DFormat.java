
package science.unlicense.model3d.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.store.DefaultFormat;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractModel3DFormat extends DefaultFormat {

    public AbstractModel3DFormat(Chars identifier) {
        super(identifier);
        this.resourceTypes.add(SceneResource.class);
    }

}
