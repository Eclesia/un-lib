
package science.unlicense.model3d.api;

import science.unlicense.common.api.collection.Collection;
import science.unlicense.encoding.api.store.Resource;
import science.unlicense.encoding.api.store.StoreException;

/**
 * 3D stores may contain various informations.
 * Most contains a single Mesh like OBJ,MD5,PLY,STL.
 * Some have a wide range of elements like Collada or Blend.
 * Or at the opposite can be very specialized like VPD,VMD, XNA POSE.
 *
 * Since there are so many variation in the 3d file formats this interface
 * provide a general approach to list the content of the store or search for
 * a specific type of objects.
 *
 * @author Johann Sorel
 */
public interface SceneResource extends Resource {

    /**
     * List all objects stored.
     * In the store contains a full Scene, only the root node
     * can be returned, it is the caller work to loop in the scene tree.
     *
     * @return collection of all objects stored, never null, can be empty
     * @throws science.unlicense.encoding.api.store.StoreException
     */
    Collection getElements() throws StoreException;

    /**
     * The store may contain multiple objects.
     * Since those can be long to create, this method allows to search
     * for a specific type of objects and avoid creating others.
     *
     * @param type class of objects searched
     * @return collection of all objects of given type, never null, can be empty
     * @throws science.unlicense.encoding.api.store.StoreException
     */
    Collection searchElements(Class type) throws StoreException;

    /**
     * Erase and write given elements.
     *
     * @param elements
     * @throws science.unlicense.encoding.api.store.StoreException
     */
    void writeElements(Collection elements) throws StoreException;

}
