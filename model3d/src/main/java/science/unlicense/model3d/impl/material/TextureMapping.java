
package science.unlicense.model3d.impl.material;

import science.unlicense.common.api.character.Chars;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.impl.SimilarityNd;
import science.unlicense.geometry.impl.Mesh;

/**
 * TODO review, find a propert way to define :
 * - image (Image or Texture or Else)
 * - sampling (Sampler or parameter list or sampler definition)
 * - mapping (UV, Spherique, ....)
 *
 * @author Johann Sorel
 */
public class TextureMapping {

    public static final int METHOD_UV = 0;
    public static final int METHOD_SPHERICAL = 1;
    public static final int METHOD_CUBE = 2;
    public static final int METHOD_CYLINDRE = 3;
    public static final int METHOD_DISTANCE = 4;
    public static final int METHOD_DUALPARABOLOID = 5;
    public static final int METHOD_VOLUMETRIC = 6;

    private Texture2D texture;
    private Chars mapping = Mesh.ATT_TEXCOORD_0;
    private int method = METHOD_UV;
    private SimilarityRW transform = SimilarityNd.create(2);

    public TextureMapping() {
        this(null);
    }

    public TextureMapping(Texture2D texture) {
        this(texture, Mesh.ATT_TEXCOORD_0, METHOD_UV);
    }

    public TextureMapping(Texture2D texture, Chars mapping, int method) {
        this.texture = texture;
        this.mapping = mapping;
        this.method = method;
        transform.setToIdentity();
    }

    public Texture2D getTexture() {
        return texture;
    }

    public void setTexture(Texture2D texture) {
        this.texture = texture;
    }

    /**
     * Get name of the UV property, default is IndexedGeometry.ATT_TEXCOORD_0
     * @return
     */
    public Chars getMapping() {
        return mapping;
    }

    public void setMapping(Chars mapping) {
        this.mapping = mapping;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public SimilarityRW getTransform() {
        return transform;
    }

}
