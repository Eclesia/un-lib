
package science.unlicense.model3d.impl.technique;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.Attenuation;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.light.SpotLight;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Cone;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Vector;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 * Technique to display light range sphere or cone.
 *
 * @author Johann Sorel
 */
public class DebugLightBoxTechnique extends AbstractTechnique implements SubSceneTechnique {

    public static final Chars PROPERTY_COLOR = Chars.constant("color");

    public DebugLightBoxTechnique() {
        properties.getDefaults().add(PROPERTY_COLOR, new ColorRGB(0.0f, 1.0f, 0.0f, 1.0f));
    }

    @Override
    public SceneNode derivateScene(SceneNode base) {

        Color color = (Color) properties.getPropertyValue(PROPERTY_COLOR);

        final DefaultSceneNode root = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        root.setRootToNodeSpace(base.getRootToNodeSpace());
        root.setLocalCoordinateSystem(base.getLocalCoordinateSystem());

        SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(color);
        SimpleBlinnPhong technique = new SimpleBlinnPhong();
        technique.getState().setPolygonMode(RenderState.MODE_LINE);
        technique.getState().setCulling(RenderState.CULLING_NONE);
        technique.setLightEnable(false);

        if (base instanceof SpotLight) {
            final SpotLight light = (SpotLight) base;
            final Attenuation att = light.getAttenuation();

            float fallOffAngle = light.getFallOffAngle();
            float fallOffExponent = light.getFallOffExponent();
            Vector forward = new Vector3f64(0, 0, -1);

            float maxDistance = (float) att.getMaxDistance();
            if (Float.isNaN(maxDistance)) maxDistance = 1.0f;

            final Cone geometry = new Cone(1, maxDistance);

            final Matrix3x3 rotation = Matrix3x3.createFromAngles(Math.PI/2.0, 0.0, 0.0);

            final DefaultMesh mesh;
            try {
                mesh = DefaultMesh.createFromGeometry(geometry);
            } catch (OperationException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }

            final DefaultModel model = new DefaultModel();
            model.setShape(mesh);
            model.getMaterials().add(material);
            model.getTechniques().add(technique);
            model.getNodeTransform().getRotation().set(rotation);
            model.getNodeTransform().notifyChanged();

            root.getChildren().add(model);
        } else if (base instanceof PointLight) {
            final PointLight light = (PointLight) base;
            final Attenuation att = light.getAttenuation();

            float maxDistance = (float) att.getMaxDistance();
            if (Float.isNaN(maxDistance)) maxDistance = 1.0f;

            final Geometry geometry = new Sphere(maxDistance);
            final DefaultMesh mesh;
            try {
                mesh = DefaultMesh.createFromGeometry(geometry);
            } catch (OperationException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }

            final DefaultModel model = new DefaultModel();
            model.setShape(mesh);
            model.getMaterials().add(material);
            model.getTechniques().add(technique);

            root.getChildren().add(model);
        } else if (base instanceof DirectionalLight) {
            //TODO
        } else if (base instanceof AmbientLight) {
            //TODO
        }

        return root;
    }

}
