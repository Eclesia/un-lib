

package science.unlicense.model3d.impl.scene;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.constraint.ConstraintUpdater;
import science.unlicense.physics.api.constraint.CopyTransformConstraint;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * A scene node which display the mesh skeleton and rigid bodies.
 *
 * @author Johann Sorel
 */
public class DebugSkeletonNode extends DefaultGraphicNode {

    public static final Chars PROPERTY_JOINT = Chars.constant("joint");
    public static final Chars PROPERTY_SPHERE = Chars.constant("sphere");
    public static final Chars PROPERTY_STATE = Chars.constant("state");
    public static final Chars PROPERTY_RIGIDBODY = Chars.constant("rigidbody");
    public static final Chars PROPERTY_CS = Chars.constant("cs");


    private static final NodeVisitor RESIZE_JOINT = new NodeVisitor() {
        public Object visit(Node node, Object context) {
            if (node instanceof Model && ((Model) node).getUserProperties().getValue(PROPERTY_SPHERE) != null){
                final double sphereSize = (Float) context;
                final Model glNode = ((Model) node);
                //resize only spheres, not lines
                final SimilarityRW trs = glNode.getNodeTransform();
                trs.getScale().setXYZ(sphereSize, sphereSize, sphereSize);
                trs.notifyChanged();
            }
            return super.visit(node, context);
        }
    };
    private static final NodeVisitor SHOW_JOINT = new NodeVisitor() {
        public Object visit(Node node, Object context) {
            if (node instanceof DefaultGraphicNode && ((DefaultGraphicNode) node).getUserProperties().getValue(PROPERTY_JOINT) != null){
                final DefaultGraphicNode glNode = ((DefaultGraphicNode) node);
                glNode.setVisible((Boolean) context);
            }
            return super.visit(node, context);
        }
    };
    private static final NodeVisitor SHOW_CS = new NodeVisitor() {
        public Object visit(Node node, Object context) {
            if (node instanceof DefaultGraphicNode && ((DefaultGraphicNode) node).getUserProperties().getValue(PROPERTY_CS) != null){
                final DefaultGraphicNode glNode = ((DefaultGraphicNode) node);
                glNode.setVisible((Boolean) context);
            }
            return super.visit(node, context);
        }
    };

//    public static final NodeVisitor SHOW_CONSTRAINTS = new DefaultNodeVisitor() {
//        public Object visit(Node node, Object context) {
//            if (node instanceof GLNode && ((GLNode) node).getProperties().getValue(PROPERTY_JOINT) != null){
//                final GLNode glNode = ((GLNode) node);
//                glNode.setVisible((Boolean) context);
//            }
//            return super.visit(node, context);
//        }
//    };

    public static final NodeVisitor SHOW_PHYSIC = new NodeVisitor() {
        public Object visit(Node node, Object context) {
            if (node instanceof DefaultGraphicNode && ((DefaultGraphicNode) node).getUserProperties().getValue(PROPERTY_RIGIDBODY) != null){
                final DefaultGraphicNode glNode = ((DefaultGraphicNode) node);
                glNode.setVisible((Boolean) context);
            }
            return super.visit(node, context);
        }
    };


    private final Skeleton skeleton;
    private float jointSize = 0.1f;
    private float csSize = 0.02f;
    private boolean jointVisible = true;
    private boolean csVisible = false;
    private boolean rigidBodyVisible = false;

    public DebugSkeletonNode(MotionModel mpm) throws OperationException{
        super(mpm.getCoordinateSystem());
        this.skeleton = mpm.getSkeleton();

        setTitle(new Chars("SKELETON DEBUG"));

        final Sequence workroots = skeleton.getChildren();
        for (int i=0,n=workroots.getSize();i<n;i++){
            final SceneNode gljoint = debugJoint((Joint) workroots.get(i));
            getChildren().add(gljoint);
        }

        setJointSize(jointSize);
        setJointVisible(jointVisible);
        setRigidBodyVisible(rigidBodyVisible);
    }

    public void setJointSize(float size){
        this.jointSize = size;
        RESIZE_JOINT.visit(this, size);
    }

    public float getJointSize(){
        return jointSize;
    }

    public void setJointVisible(boolean jointVisible) {
        this.jointVisible = jointVisible;
        SHOW_JOINT.visit(this, jointVisible);
    }

    public boolean isJointVisible() {
        return jointVisible;
    }

    public void setCsVisible(boolean csVisible) {
        this.csVisible = csVisible;
        SHOW_CS.visit(this, csVisible);
    }

    public boolean isCsVisible() {
        return csVisible;
    }

    public void setRigidBodyVisible(boolean rigidBodyVisible) {
        this.rigidBodyVisible = rigidBodyVisible;
        SHOW_PHYSIC.visit(this, rigidBodyVisible);
    }

    public boolean isRigidBodyVisible() {
        return rigidBodyVisible;
    }

    private SceneNode debugJoint(final Joint joint) throws OperationException {
        final DefaultGraphicNode node = new DefaultGraphicNode(coordinateSystem);

        node.getUpdaters().add(new ConstraintUpdater(new CopyTransformConstraint(node, joint, 1)));

        final Color color = getColor(joint, skeleton);

        for (int i=joint.getChildren().getSize()-1;i>=0;i--){
            final Node nc = (Node) joint.getChildren().get(i);

            if (nc instanceof Joint){
                final Joint child = (Joint) nc;

                //add a line toward the child
                final Affine childmatrix = child.getNodeTransform();
                final Tuple t = childmatrix.transform(new Vector3f64(0, 0, 0),null);
                float[] end = t.toFloat();
                final DefaultModel line = new DefaultModel();

                final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
                material.setDiffuse(color);
                line.getMaterials().add(material);

                line.setLocalCoordinateSystem(coordinateSystem);
                line.getUserProperties().add(PROPERTY_JOINT, joint);
                final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
                final Float32Cursor fl = DefaultBufferFactory.INSTANCE.createFloat32(2*3).cursor();
                fl.write(0).write(0).write(0);
                fl.write(end);
                shell.setPositions(new VBO(fl.getBuffer(),3));
                shell.setNormals(new VBO(fl.getBuffer(),3));
                shell.setIndex(new IBO(new int[]{0,1}));
                shell.setRanges(new IndexedRange[]{IndexedRange.LINES(0, 2)});
                line.setShape(shell);
                node.getChildren().add(line);

                //build the child tree
                final SceneNode childdj = debugJoint(child);
                node.getChildren().add(childdj);
            } else if (nc instanceof RigidBody){
                final RigidBody body = (RigidBody) nc;
                final Geometry geom = body.getGeometry();

                final DefaultModel ele = new DefaultModel();
                ele.setShape(DefaultMesh.createFromGeometry(geom, 10, 10));

                if (ele != null){
                    ele.setLocalCoordinateSystem(coordinateSystem);
                    ele.getUserProperties().add(PROPERTY_RIGIDBODY, body);
                    //TODO ((MeshRenderer) ele.getRenderers().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
                    ele.getNodeTransform().set(body.getNodeTransform());
                    ele.getNodeTransform().notifyChanged();
                    final Color rbColor = getColor(body);

                    final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
                    material.setDiffuse(rbColor);
                    ele.getMaterials().add(material);

                    node.getChildren().add(ele);
                }

            }
        }

        //small sphere at joint root

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(color);

        final DefaultModel sphere = new DefaultModel();
        sphere.setShape(DefaultMesh.createFromGeometry(new Sphere(2), 10, 10));
        sphere.setLocalCoordinateSystem(coordinateSystem);
        sphere.getMaterials().add(material);
        //TODO ((MeshRenderer) sphere.getRenderers().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
        sphere.getUserProperties().add(PROPERTY_JOINT, joint);
        sphere.getUserProperties().add(PROPERTY_SPHERE, Boolean.TRUE);
        sphere.getNodeTransform().getScale().setXYZ(jointSize, jointSize, jointSize);
        sphere.getNodeTransform().notifyChanged();
        node.getChildren().add(sphere);

        final MotionModel cs = DefaultMotionModel.createXYZMarker();
        cs.getUserProperties().add(PROPERTY_CS, Boolean.TRUE);
        cs.getNodeTransform().getScale().setXYZ(csSize, csSize, csSize);
        cs.getNodeTransform().notifyChanged();
        node.getChildren().add(cs);

        //constraints
//        final Sequence csts = joint.getConstraints();
//        for (int i=0,n=csts.getSize();i<n;i++){
//            final Constraint cst = (Constraint) csts.get(i);
//            if (cst instanceof AngleLimitConstraint){
//                final AngleLimitConstraint alc = (AngleLimitConstraint) cst;
//                final double min = alc.getLimits().getMin(0);
//                final double max = alc.getLimits().getMax(0);
//                final Mesh cone = new GeometryMesh(new Cone(1,0.5), 10, 10);
//                cone.getMaterial().putLayer(new Layer(new ColorMapping(color)));
//                cone.getMaterial().setLightVulnerable(false);
//                cone.setPickable(false);
//                //cone.getProperties().add(PROPERTY_JOINT, joint);
//                //cone.getModelTransform().getScale().setXYZ(shpereSize, shpereSize, shpereSize);
//                cone.getModelTransform().notifyChanged();
//                node.addChild(cone);
//            } else {
//                System.out.println("can not display joint constraint of type : "+cst.getClass().getSimpleName());
//            }
//        }

        return node;
    }

    public static Color getColor(Joint jt, Skeleton skeleton){
        final Color color;
        if (jt.isKinematicTarget(skeleton)){
            color = Color.RED;
        } else if (jt.isKinematicEffector(skeleton)){
            color = Color.GREEN;
        } else if (jt.isKinematic(skeleton)){
            color = Color.BLUE;
        } else {
            color = Color.GRAY_LIGHT;
        }
        return color;
    }

    public static Color getColor(RigidBody body){
        return  body.isFree() ? Color.BLUE : Color.RED;
    }

}
