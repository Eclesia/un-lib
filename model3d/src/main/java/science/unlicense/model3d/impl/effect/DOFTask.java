
package science.unlicense.model3d.impl.effect;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultFieldType;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.number.Float32;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.image.impl.process.ConvolutionMatrices;
import science.unlicense.image.impl.process.ConvolutionMatrix;
import science.unlicense.task.api.AbstractTask;

/**
 *
 * The blur factor calculation was from :
 * "Depth of Field" demo for Ogre
 * Copyright (C) 2006  Christian Lindequist Larsen
 * This code is in the public domain. You may do whatever you want with it.
 *
 * @author Christian Lindequist Larsen (original blur factor calculation)
 * @author Johann Sorel (new blur factor math and texture merge)
 */
public class DOFTask extends AbstractTask {

    public static final AttachmentType IN_COLOR = new AttachmentType(AfterEffect.LAYER_COLOR, ColorSystem.RGB_FLOAT, Float32.TYPE);
    public static final AttachmentType IN_POSITION_VIEW = new AttachmentType(AfterEffect.LAYER_POSITION_VIEW, ColorSystem.RGB_FLOAT, Float32.TYPE);
    public static final AttachmentType OUT_COLOR = new AttachmentType(AfterEffect.LAYER_COLOR, ColorSystem.RGBA_FLOAT, Float32.TYPE);

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("dof"), new Chars("dof"), Chars.EMPTY, DOFTask.class,
                new FieldType[]{
                    new DefaultFieldType(new Chars("nearBlurDepth"), null, null, 1, 1, Float.class, 1f, null, null),
                    new DefaultFieldType(new Chars("focalPlaneDepthMin"), null, null, 1, 1, Float.class, 5f, null, null),
                    new DefaultFieldType(new Chars("focalPlaneDepthMax"), null, null, 1, 1, Float.class, 15f, null, null),
                    new DefaultFieldType(new Chars("farBlurDepth"), null, null, 1, 1, Float.class, 10f, null, null),
                    new DefaultFieldType(new Chars("cutoff"), null, null, 1, 1, Float.class, 0f, null, null),
                    new DefaultFieldType(new Chars("dofBlurRadius"), null, null, 1, 1, Integer.class, 4, null, null),
                    IN_COLOR,
                    IN_POSITION_VIEW,},
                new FieldType[]{OUT_COLOR});

    public DOFTask() {
        super(DESCRIPTOR);
    }

    @Override
    public Document perform() {

        final int dofBlurRadius = (int) inputParameters.getPropertyValue(new Chars("dofBlurRadius"));
        final ConvolutionMatrix gauss = ConvolutionMatrices.createGaussian(dofBlurRadius, 6);


        return outputParameters;
    }

}
