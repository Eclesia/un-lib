

package science.unlicense.model3d.impl.physic;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.impl.anim.KeyFrameNumberTimeSerie;

/**
 *
 * @author Johann Sorel
 */
public class MorphTimeSerie extends KeyFrameNumberTimeSerie {

    private Chars morphName;

    public MorphTimeSerie() {
    }

    public MorphTimeSerie(Chars morphName) {
        this.morphName = morphName;
    }

    public Chars getMorphName() {
        return morphName;
    }

    public void setMorphName(Chars morphName) {
        this.morphName = morphName;
    }

//
//    @Override
//    public MorphKeyFrame getOrInterpolate(double time, KeyFrame buffer) {
//
//        final KeyFrame[] nearest = getNearest(time, null);
//        final MorphKeyFrame underFrame = (MorphKeyFrame) nearest[0];
//        final MorphKeyFrame aboveFrame = (MorphKeyFrame) nearest[1];
//
//
//        if (underFrame == aboveFrame){
//            //we are at the begin, the end, or exactly on a frame
//            return underFrame;
//        } else {
//            //interpolate
//            final MorphKeyFrame frame = buffer==null ? new MorphKeyFrame() : (MorphKeyFrame) buffer;
//            frame.setTime(time);
//
//            final double extent = aboveFrame.getTime() - underFrame.getTime();
//            final double ratio = (time-underFrame.getTime()) / extent;
//
//            final Dictionary above = aboveFrame.getMorphRatios();
//            final Iterator underIte = underFrame.getMorphRatios().getPairs().createIterator();
//            while (underIte.hasNext()){
//                final Pair underPair = (Pair) underIte.next();
//                final double underVal = (Double) underPair.getValue2();
//                final double aboveVal = (Double) above.getValue(underPair.getValue1());
//                frame.getMorphRatios().add(underPair.getValue1(), underVal*(1-ratio) + aboveVal*ratio);
//            }
//
//            return frame;
//        }
//
//    }


}
