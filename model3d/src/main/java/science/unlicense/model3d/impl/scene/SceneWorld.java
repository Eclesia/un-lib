
package science.unlicense.model3d.impl.scene;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeSequence;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.common.api.predicate.ClassPredicate;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.physics.api.AbstractWorld;
import science.unlicense.physics.api.body.Body;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.force.Force;
import science.unlicense.physics.api.force.Singularity;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * A GL Scene world mapping the scene content.
 *
 * @author Johann Sorel
 */
public class SceneWorld extends AbstractWorld {

    /**
     * Visitor to find all rigid bodies in the scene.
     * returns a sequence of rigid bodies.
     */
    public static final NodeVisitor FORCE_VISITOR = new NodeVisitor(){

        @Override
        public Sequence visit(Node node, Object context) {
            if (node instanceof MotionModel){
                final MotionModel mpm = (MotionModel) node;
                ((Sequence) context).addAll(mpm.getConstraints());
            }
            super.visit(node, context);
            return (Sequence) context;
        }
    };


    private final Sequence singularities = new ArraySequence();
    private final Sequence forces = new ArraySequence();
    private Sequence bodies;
    private Sequence skeletons;
    private SceneNode scene;

    public SceneWorld(SceneNode scene) {
        super(3);
        this.scene = scene;
        bodies = new NodeSequence(scene, true, new ClassPredicate(RigidBody.class));
        skeletons = new NodeSequence(scene, true, new ClassPredicate(Skeleton.class));
    }

    public void setScene(SceneNode scene){
        this.scene = scene;
        bodies = new NodeSequence(scene, true, new ClassPredicate(RigidBody.class));
        skeletons = new NodeSequence(scene, true, new ClassPredicate(Skeleton.class));
    }

    public SceneNode getScene() {
        return scene;
    }

    public Sequence getSkeletons() {
        return skeletons;
    }

    @Override
    public Sequence getSingularities() {
        return singularities;
    }

    @Override
    public Sequence getBodies() {
        return bodies;
    }

    @Override
    public void addSingularity(Singularity singularity) {
        singularities.add(singularity);
    }

    @Override
    public void removeSingularity(Singularity singularity) {
        singularities.remove(singularity);
    }

    @Override
    public void addBody(Body body) {
        throw new UnimplementedException("Not supported.");
    }

    @Override
    public void removeBody(Body body) {
        throw new UnimplementedException("Not supported.");
    }

    @Override
    public Sequence getForces() {
        final Sequence seq = new ArraySequence();
        //defensive copy
        SceneNode cp = scene;
        if (cp!=null){
            FORCE_VISITOR.visit(cp, seq);
            seq.addAll(forces);
        }
        return seq;
    }

    @Override
    public void addForce(Force force) {
        forces.add(force);
    }

    @Override
    public void removeForce(Force force) {
        forces.remove(force);
    }

    @Override
    public void removeAll() {
        throw new UnimplementedException("Not supported.");
    }

}
