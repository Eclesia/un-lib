
package science.unlicense.model3d.impl.effect;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.number.Float32;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.light.Light;
import science.unlicense.display.impl.light.Lights;
import science.unlicense.display.impl.light.PixelFragment;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.light.SpotLight;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.impl.DefaultFragment;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.DefaultColorRW;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Vector2i32;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.system.Work;
import science.unlicense.system.WorkUnits;
import science.unlicense.task.api.AbstractTask;

/**
 *
 * @author Johann Sorel
 */
public class BlinnPhongLightsTask extends AbstractTask {

    public static final AttachmentType IN_AMBIENT = new AttachmentType(Chars.constant("AMBIENT"), ColorSystem.RGB_FLOAT, Float32.TYPE);
    public static final AttachmentType IN_DIFFUSE = new AttachmentType(Chars.constant("DIFFUSE"), ColorSystem.RGB_FLOAT, Float32.TYPE);
    public static final AttachmentType IN_SPECULAR = new AttachmentType(Chars.constant("SPECULAR"), ColorSystem.RGB_FLOAT, Float32.TYPE);
    public static final AttachmentType IN_POSITION_VIEW = new AttachmentType(Chars.constant("POSITION_VIEW"), new UndefinedSystem(3), Float32.TYPE);
    public static final AttachmentType IN_NORMAL_VIEW = new AttachmentType(Chars.constant("NORMAL_VIEW"), new UndefinedSystem(3), Float32.TYPE);
    public static final AttachmentType OUT_COLOR = new AttachmentType(Chars.constant("COLOR"), ColorSystem.RGBA_FLOAT, Float32.TYPE);

    //scene variables
    private Sequence lights;
    private LightState[] lightstates;

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("blinnlights"), new Chars("blinnlights"), Chars.EMPTY, BlinnPhongLightsTask.class,
                new FieldType[]{IN_AMBIENT, IN_DIFFUSE, IN_SPECULAR, IN_POSITION_VIEW, IN_NORMAL_VIEW},
                new FieldType[]{OUT_COLOR});

    public BlinnPhongLightsTask() {
        super(DESCRIPTOR);
    }

    @Override
    public Document perform() {

        final TupleGrid ambients = (TupleGrid) inputParameters.getPropertyValue(IN_AMBIENT.getId());
        final TupleGrid diffuses = (TupleGrid) inputParameters.getPropertyValue(IN_DIFFUSE.getId());
        final TupleGrid speculars = (TupleGrid) inputParameters.getPropertyValue(IN_SPECULAR.getId());
        final TupleGrid positions = (TupleGrid) inputParameters.getPropertyValue(IN_POSITION_VIEW.getId());
        final TupleGrid normals = (TupleGrid) inputParameters.getPropertyValue(IN_NORMAL_VIEW.getId());
        final TupleGrid colors = (TupleGrid) outputParameters.getPropertyValue(OUT_COLOR.getId());

        final Node scene = null;
        final MonoCamera camera = null;
        final Similarity V = camera.getRootToNodeSpace();
        updateLights(scene, V);

        final WorkUnits workers = science.unlicense.system.System.get().createWorkUnits(50);

        final Extent.Long extent = diffuses.getExtent();
        final long width = extent.getL(0);
        final long height = extent.getL(1);

        workers.submit(width * height, new Work() {
            @Override
            public void run(long index) {
                final Vector2i32 xy = new Vector2i32((int) (index % width), (int) (index / width));

                //extract fragment informations
                final Vector4f64 color = new Vector4f64();
                final Vector3f64 accAmbient = new Vector3f64();
                final Vector3f64 accDiffuse = new Vector3f64();
                final Vector3f64 accSpecular = new Vector3f64();
                final DefaultFragment frag = new DefaultFragment(CoordinateSystems.UNDEFINED_3D);
                final SimpleBlinnPhong.Fragment bfrag = SimpleBlinnPhong.view(frag);
                final DefaultColorRW ambient = new DefaultColorRW((ColorSystem) IN_AMBIENT.getSampleSystem());
                final DefaultColorRW diffuse = new DefaultColorRW((ColorSystem) IN_DIFFUSE.getSampleSystem());
                final DefaultColorRW specular = new DefaultColorRW((ColorSystem) IN_SPECULAR.getSampleSystem());
                bfrag.setBaseAmbient((Color) ambients.getTuple(xy, ambient));
                bfrag.setBaseDiffuse((Color) diffuses.getTuple(xy, diffuse));
                bfrag.setBaseSpecular((Color) speculars.getTuple(xy, specular));
                bfrag.setAccAmbient(accAmbient);
                bfrag.setAccDiffuse(accDiffuse);
                bfrag.setAccSpecular(accSpecular);
                bfrag.setAlpha(1.0f); //TODO

                final PixelFragment pxf = PixelFragment.view(frag);
                TupleRW positionV = positions.getTuple(xy, positions.createTuple());
                pxf.setPositionView(positionV);
                TupleRW normalV = normals.getTuple(xy, normals.createTuple());
                pxf.setNormalView(normalV);
                pxf.setEyeDirection(new Vector3f64(positionV).localNormalize().localNegate());


                //apply lights
                for (int i=0,n=lights.getSize(); i<n; i++) {
                    final Light light = (Light) lights.get(i);

                    double shadowFactor = 1.0;
        //            if (lightstates[i].shadowMap != null) {
        //                //convert fragment position to light space
        //                Vector3d fragPosLightSpace = new Vector3d();
        //                M.transform(positionM, fragPosLightSpace);
        //                Similarity lightM = light.getRootToNodeSpace();
        //                lightM.transform(fragPosLightSpace, fragPosLightSpace);
        //
        //                Vector4d fragPosLightProj = new Vector4d();
        //                fragPosLightProj.setXYZ(fragPosLightSpace.x,fragPosLightSpace.y,fragPosLightSpace.z);
        //                lightstates[i].camera.getProjectionMatrix().transform(fragPosLightProj, fragPosLightProj);
        //                double distToLight = -fragPosLightSpace.z;
        //
        //                //calculate shadow map coordinate
        //                Vector4d shadowCoord = fragPosLightProj;
        //                shadowCoord.x /= shadowCoord.w;
        //                shadowCoord.y /= shadowCoord.w;
        //                shadowCoord.z /= shadowCoord.w;
        //                shadowCoord.x += 1.0;
        //                shadowCoord.y += 1.0;
        //                shadowCoord.x *= 0.5;
        //                shadowCoord.y *= 0.5;
        //
        //                final Scalard d = new Scalard();
        //                lightstates[i].shadowMap.getTuple(shadowCoord.getXY(), d);
        //
        //                System.out.println(d +"  "+distToLight);
        //            }

                    if (light instanceof SpotLight) {
                        final SpotLight dl = (SpotLight) light;
                        final VectorRW lightPose = lightstates[i].position;
                        SimpleBlinnPhong.evaluate(dl, bfrag, lightPose, V, shadowFactor);

                    } else if (light instanceof PointLight) {
                        final PointLight pl = (PointLight) light;
                        final VectorRW lightPose = lightstates[i].position;
                        SimpleBlinnPhong.evaluate(pl, bfrag, lightPose, null, shadowFactor);

                    } else if (light instanceof DirectionalLight) {
                        final DirectionalLight dl = (DirectionalLight) light;
                        final VectorRW spotDirection = VectorNf64.create(dl.getWorldSpaceDirection());
                        V.getRotation().transform(spotDirection.localNegate(), spotDirection);
                        SimpleBlinnPhong.evaluate(dl, bfrag, null, spotDirection, shadowFactor);

                    } else  if (light instanceof AmbientLight) {
                        final AmbientLight dl = (AmbientLight) light;
                        final Color baseAmbient = bfrag.getBaseAmbient();
                        final Color baseDiffuse = bfrag.getBaseDiffuse();
                        accAmbient.x = dl.getDiffuse().getRed() * baseAmbient.getRed() * baseDiffuse.getRed();
                        accAmbient.y = dl.getDiffuse().getGreen() * baseAmbient.getGreen() * baseDiffuse.getGreen();
                        accAmbient.z = dl.getDiffuse().getBlue() * baseAmbient.getBlue() * baseDiffuse.getBlue();
                    }
                }

                color.x = Maths.clamp(accAmbient.x + accDiffuse.x + accSpecular.x, 0, 1);
                color.y = Maths.clamp(accAmbient.y + accDiffuse.y + accSpecular.y, 0, 1);
                color.z = Maths.clamp(accAmbient.z + accDiffuse.z + accSpecular.z, 0, 1);
                color.w = bfrag.getAlpha();

                colors.setTuple(xy, color);
            }
        });

        return outputParameters;
    }

    private void updateLights(Node scene, Transform V) {
        if (lights != null) return;

        lights = Lights.getLights(scene);
        lightstates = new LightState[lights.getSize()];
        for (int i=0;i<lightstates.length;i++) {
            lightstates[i] = new LightState();
            final Light pl = (Light) lights.get(i);
            if (pl instanceof PointLight) {
                final Vector3f64 lightPose = (Vector3f64) ((PointLight) pl).getPosition();
                V.transform(lightPose, lightPose);
                lightstates[i].position = lightPose;
                //if (enableShadow) buildShadowMap(camera, pl, shadowMapSize, lightstates[i]);
            } else if (pl instanceof SpotLight) {
                final Vector3f64 lightPose = (Vector3f64) ((SpotLight) pl).getPosition();
                V.transform(lightPose, lightPose);
                lightstates[i].position = lightPose;
                //if (enableShadow) buildShadowMap(camera, pl, shadowMapSize, lightstates[i]);
            }
        }
    }

    private static class LightState {
        Light light;
        MonoCamera camera;
        Vector3f64 position;
        TupleSpace shadowMap;
    }

}
