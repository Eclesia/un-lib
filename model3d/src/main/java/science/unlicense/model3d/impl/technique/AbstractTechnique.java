
package science.unlicense.model3d.impl.technique;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.event.Properties;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.model3d.impl.material.TextureMapping;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractTechnique implements Technique {

    /**
     * A tangent space normal map.
     * Used by many techniques.
     */
    public static final Chars NORMAL = Chars.constant(new Chars("normal"));

    protected final Properties properties = new Properties();

    protected final RenderState state = new RenderState();
    protected Chars materialName;

    @Override
    public Chars getMaterialName() {
        return materialName;
    }

    @Override
    public void setMaterialName(Chars name) {
        this.materialName = name;
    }

    @Override
    public RenderState getState() {
        return state;
    }

    @Override
    public Properties properties() {
        return properties;
    }

    protected static VectorRW evaluate(TextureMapping tm, final Dictionary stack) {
        final Texture2D tex = tm.getTexture();
        final Tuple uv = (Tuple) stack.getValue(tm.getMapping());

        final TupleSpace space = tex.asTupleSpace();
        final Extent.Long extent = tex.getExtent();

        final Vector2f64 uvi = new Vector2f64(
            uv.get(0) * extent.get(0),
            uv.get(1) * extent.get(1)
        );

        final SampleSystem ss = space.getSampleSystem();
        final VectorRW buffer = VectorNf64.createDouble(ss.getNumComponents());
        space.getTuple(uvi, buffer);
        return buffer;
    }

    protected static Color evaluateColor(TextureMapping tm, final Dictionary stack) {
        final Texture2D tex = tm.getTexture();
        final Tuple uv = (Tuple) stack.getValue(tm.getMapping());

        final TupleSpace space = tex.asTupleSpace();
        final Extent.Long extent = tex.getExtent();

        final Vector2f64 uvi = new Vector2f64(
            uv.get(0) * extent.get(0),
            uv.get(1) * extent.get(1)
        );

        final SampleSystem ss = space.getSampleSystem();
        final TupleRW buffer = space.createTuple();
        space.getTuple(uvi, buffer);

        return Colors.castOrWrap((Tuple) buffer, (ColorSystem) ss);
    }

}
