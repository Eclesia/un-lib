
package science.unlicense.model3d.impl.scene;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Int32;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public class DefaultMotionModel extends DefaultGraphicNode implements MotionModel {

    private Skeleton skeleton;
    private final Dictionary animations = new HashDictionary();
    private final Sequence constraints = new ArraySequence();

    public DefaultMotionModel(CoordinateSystem cs) {
        super(cs);
    }

    @Override
    public Skeleton getSkeleton() {
        return skeleton;
    }

    @Override
    public void setSkeleton(Skeleton skeleton) {
        if (this.skeleton != null) {
            children.remove(this.skeleton);
        }
        this.skeleton = skeleton;
        if (this.skeleton!=null) {
            children.add(skeleton);
        }
    }

    @Override
    public Sequence getConstraints() {
        return constraints;
    }

    @Override
    public Dictionary getAnimations() {
        return animations;
    }


    public static MotionModel createXYZMarker() {
        return createXYZMarker(CoordinateSystems.undefined(3), 5, 0.1);
    }

    /**
     * TODO somewhere else.
     *
     * @return XYZ marker.
     */
    public static MotionModel createXYZMarker(CoordinateSystem cs, double length, double width) {

        final MotionModel group = new DefaultMotionModel(cs);

        int nbRing = 10;
        int nbSector = 10;
        final DefaultMesh sx;
        final DefaultMesh sy;
        final DefaultMesh sz;
        try {
            sx = DefaultMesh.createFromGeometry(new BBox(new double[]{0,-width,-width},new double[]{length,width,width}), nbRing, nbSector);
            sy = DefaultMesh.createFromGeometry(new BBox(new double[]{-width,0,-width},new double[]{width,length,width}), nbRing, nbSector);
            sz = DefaultMesh.createFromGeometry(new BBox(new double[]{-width,-width,0},new double[]{width,width,length}), nbRing, nbSector);
        } catch (OperationException ex) {
            //won't happen with BBox
            throw new RuntimeException(ex.getMessage(), ex);
        }

        final DefaultModel x = new DefaultModel(cs);
        x.setShape(sx);
        final DefaultModel y = new DefaultModel(cs);
        y.setShape(sy);
        final DefaultModel z = new DefaultModel(cs);
        z.setShape(sz);

        final SimpleBlinnPhong.Material mx = SimpleBlinnPhong.newMaterial();
        final SimpleBlinnPhong.Material my = SimpleBlinnPhong.newMaterial();
        final SimpleBlinnPhong.Material mz = SimpleBlinnPhong.newMaterial();
        mx.setDiffuse(Color.RED);
        my.setDiffuse(Color.GREEN);
        mz.setDiffuse(Color.BLUE);

        x.getMaterials().add(mx);
        y.getMaterials().add(my);
        z.getMaterials().add(mz);

        group.getChildren().add(x);
        group.getChildren().add(y);
        group.getChildren().add(z);

        return group;
    }

    public static MotionModel createCrosshair3D() {
        return createCrosshair3D(true);
    }

    public static MotionModel createCrosshair3D(boolean colored) {
        return createCrosshair3D(colored,false);
    }

    public static MotionModel createCrosshair3D(boolean colored, boolean showPositive) {

        final MotionModel group = new DefaultMotionModel(CoordinateSystems.undefined(3));

        final float[] vertex = {
            -1, 0, 0,+1, 0, 0,
             0,-1, 0, 0,+1, 0,
             0, 0,-1, 0, 0,+1
        };
        final float[] normal = {
             0, 1, 0, 0, 1, 0,
             0, 1, 0, 0, 1, 0,
             0, 1, 0, 0, 1, 0
        };
        final int[] index = {
            0,1,
            2,3,
            4,5
        };

        final DefaultModel model = new DefaultModel();

        final DefaultMesh mesh = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        mesh.setPositions(new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(vertex), Float32.TYPE, UndefinedSystem.create(3), 6));
        mesh.setNormals(new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(normal), Float32.TYPE, UndefinedSystem.create(3), 6));
        mesh.setIndex(new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(index), Int32.TYPE, UndefinedSystem.create(1), 6));
        mesh.setRanges(new IndexedRange[]{IndexedRange.LINES(0, 6)});
        model.setShape(mesh);
        group.getChildren().add(model);



//        if (colored) {
//            final ByVertexColorMapping paint = new ByVertexColorMapping();
//            paint.setHasAlpha(false);
//            final Buffer colors = GLBufferFactory.INSTANCE.createFloat(18);
//            colors.cursorFloat().write(new float[]{
//                1,0,0,1,0,0,
//                0,1,0,0,1,0,
//                0,0,1,0,0,1});
//            paint.setColors(colors);
//            getMaterial().putOrReplaceLayer(new Layer(paint));
//        } else {
        final SimpleBlinnPhong.Material mat = SimpleBlinnPhong.newMaterial();
        mat.setDiffuse(Color.WHITE);
        model.getMaterials().add(mat);
//        }

        if (showPositive) {
            final DefaultModel xdir;
            final DefaultModel ydir;
            final DefaultModel zdir;
            xdir = DefaultModel.createFromGeometry(new Sphere(0.04));
            ydir = DefaultModel.createFromGeometry(new Sphere(0.04));
            zdir = DefaultModel.createFromGeometry(new Sphere(0.04));

            xdir.getNodeTransform().getTranslation().setXYZ(1, 0, 0);
            ydir.getNodeTransform().getTranslation().setXYZ(0, 1, 0);
            zdir.getNodeTransform().getTranslation().setXYZ(0, 0, 1);
            xdir.getNodeTransform().notifyChanged();
            ydir.getNodeTransform().notifyChanged();
            zdir.getNodeTransform().notifyChanged();

            final SimpleBlinnPhong.Material mx = SimpleBlinnPhong.newMaterial();
            final SimpleBlinnPhong.Material my = SimpleBlinnPhong.newMaterial();
            final SimpleBlinnPhong.Material mz = SimpleBlinnPhong.newMaterial();

            if (colored) {
                mx.setDiffuse(Color.RED);
                my.setDiffuse(Color.GREEN);
                mz.setDiffuse(Color.BLUE);
            } else {
                mx.setDiffuse(Color.WHITE);
                my.setDiffuse(Color.WHITE);
                mz.setDiffuse(Color.WHITE);
            }
            xdir.getMaterials().add(mx);
            ydir.getMaterials().add(my);
            zdir.getMaterials().add(mz);

            group.getChildren().add(xdir);
            group.getChildren().add(ydir);
            group.getChildren().add(zdir);
        }

        return group;
    }

}
