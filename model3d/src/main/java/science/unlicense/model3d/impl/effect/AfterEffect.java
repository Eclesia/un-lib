
package science.unlicense.model3d.impl.effect;

import science.unlicense.common.api.character.Chars;

/**
 * Also called PostProcess, after effects are processes which work on one or
 * several image inputs and produce one or several outputs.
 *
 * @author Johann Sorel
 */
public final class AfterEffect {

    public static final Chars LAYER_COLOR = Chars.constant("COLOR");
    public static final Chars LAYER_NORMAL_MODEL = Chars.constant("NORMAL_MODEL");
    public static final Chars LAYER_NORMAL_WORLD = Chars.constant("NORMAL_WORLD");
    public static final Chars LAYER_NORMAL_VIEW = Chars.constant("NORMAL_VIEW");
    public static final Chars LAYER_NORMAL_PROJECTION = Chars.constant("NORMAL_PROJECTION");
    public static final Chars LAYER_POSITION_MODEL = Chars.constant("POSITION_MODEL");
    public static final Chars LAYER_POSITION_WORLD = Chars.constant("POSITION_WORLD");
    public static final Chars LAYER_POSITION_VIEW = Chars.constant("POSITION_VIEW");
    public static final Chars LAYER_POSITION_PROJECTION = Chars.constant("POSITION_PROJECTION");

    private AfterEffect(){}

}
