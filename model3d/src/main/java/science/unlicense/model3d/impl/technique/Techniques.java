
package science.unlicense.model3d.impl.technique;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public final class Techniques {

    private Techniques() {
    }

    /**
     * Create a default rendering style used display edges and backfaces in a CAD way.
     * This configuration is appropriate for formats with only mesh definitions and
     * no proper style, like STL and PLY.
     *
     * The style is composed of two techniques :
     * - one drawing edges in light gray for the back faces
     * - one drawing edges in dark gray for the front faces
     *
     * @param node
     */
    public static void styleCAD(SceneNode node) {

        if (node instanceof Model) {
            final Model model = (Model) node;

            final SimpleBlinnPhong.Material materialB = SimpleBlinnPhong.newMaterial();
            materialB.setName(new Chars("BACK"));
            materialB.setDiffuse(Color.GRAY_LIGHT);
            final SimpleBlinnPhong techniqueB = new SimpleBlinnPhong();
            techniqueB.setMaterialName(materialB.getName().toChars());
            techniqueB.getState().setPolygonMode(RenderState.MODE_LINE);
            techniqueB.getState().setCulling(RenderState.CULLING_FRONT);

            final SimpleBlinnPhong.Material materialF = SimpleBlinnPhong.newMaterial();
            materialF.setName(new Chars("FRONT"));
            materialF.setDiffuse(Color.GRAY_DARK);
            final SimpleBlinnPhong techniqueF = new SimpleBlinnPhong();
            techniqueF.setMaterialName(materialF.getName().toChars());
            techniqueF.getState().setPolygonMode(RenderState.MODE_LINE);
            techniqueF.getState().setCulling(RenderState.CULLING_BACK);

            model.getTechniques().removeAll();
            model.getMaterials().removeAll();

            model.getMaterials().add(materialF);
            model.getMaterials().add(materialB);
            model.getTechniques().add(techniqueF);
            model.getTechniques().add(techniqueB);
        }

        final Iterator ite = node.getChildren().createIterator();
        while (ite.hasNext()) {
            Object child = ite.next();
            if (child instanceof SceneNode) {
                styleCAD((SceneNode) child);
            }
        }

    }

}
