
package science.unlicense.model3d.impl.technique;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Properties;

/**
 * Techniques combine the properties from the material and it's own configuration
 * with properties from the geometry.
 *
 * @author Johann Sorel
 */
public interface Technique {

    /**
     * Returns the rendering state configuration.
     *
     * @return RenderState, never null
     */
    RenderState getState();

    /**
     * Get the material name to use with this technique.
     * If name is null, default material should be used.
     *
     * @return name of the material to use.
     */
    Chars getMaterialName();

    void setMaterialName(Chars name);

    Properties properties();

}
