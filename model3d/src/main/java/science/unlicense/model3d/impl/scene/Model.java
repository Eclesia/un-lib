
package science.unlicense.model3d.impl.scene;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.geometry.api.Geometry;

/**
 * A Model is geometrical shape with associated material definitions.
 *
 * The mesh is the smallest piece of information which can be rendered.
 *
 * @author Johann Sorel
 */
public interface Model extends GraphicNode {

    Geometry getShape();

    Sequence getMaterials();

    Skin getSkin();

    /**
     * Returns the weight to apply on each morph target.
     *
     * @return morph target weights, null or matching shape morph target length
     */
    float[] getMorphTargetWeights();

    void setMorphTargetWeights(float[] weights);

}
