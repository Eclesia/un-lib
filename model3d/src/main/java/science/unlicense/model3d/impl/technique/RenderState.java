
package science.unlicense.model3d.impl.technique;

/**
 *
 * @author Johann Sorel (Geomatys)
 */
public class RenderState {

    public static final int CULLING_NONE = -1;
    public static final int CULLING_FRONT = 0;
    public static final int CULLING_BACK = 1;
    public static final int CULLING_BOTH = 2;

    public static final int MODE_FILL = 0;
    public static final int MODE_LINE = 1;
    public static final int MODE_POINT = 2;

    /**
     * Inherited from OpenGL rendering, called culling.
     */
    protected int culling = CULLING_NONE;
    /**
     * Inherited from OpenGL rendering, called polygon mode.
     */
    protected int polygonMode = MODE_FILL;
    /**
     * Inherited from OpenGL rendering, called depthMask.
     */
    protected boolean writeToDepth = true;
    /**
     * Inherited from OpenGL rendering state, called DEPTH_TEST (code 2929).
     */
    protected boolean depthTest = true;

    /**
     * Set culling state.
     * One of CULLING_*
     *
     * @param culling
     */
    public void setCulling(int culling) {
        this.culling = culling;
    }

    /**
     * Returns culling face.
     *
     * @return One of CULLING_*
     */
    public int getCulling() {
        return culling;
    }

    /**
     * Returns polygon mode.
     *
     * @return One of MODE_*
     */
    public int getPolygonMode() {
        return polygonMode;
    }

    /**
     * Set polygon mode.
     * One of MODE_*
     *
     * @param mode
     */
    public void setPolygonMode(int mode) {
        this.polygonMode = mode;
    }

    /**
     * Returns true if fragment depths must be written.
     *
     * @return true if depth is written
     */
    public boolean isWriteToDepth() {
        return writeToDepth;
    }

    /**
     * Set depth writting.
     * @param writeToDepth
     */
    public void setWriteToDepth(boolean writeToDepth) {
        this.writeToDepth = writeToDepth;
    }

    public void setDepthTest(boolean depthTest) {
        this.depthTest = depthTest;
    }

    public boolean isDepthTest() {
        return depthTest;
    }

    public void set(RenderState state) {
        this.culling = state.culling;
        this.writeToDepth = state.writeToDepth;
        this.polygonMode = state.polygonMode;
        this.depthTest = state.depthTest;
    }

}
