
package science.unlicense.model3d.impl.scene;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * A motion model is a group of nodes sharing common resources. often
 * vertexbuffers, skeleton, physics or animations.
 *
 * TODO think about a proper organisation of models divided in plenty of parts
 * and materials.
 *
 * @author Johann Sorel
 */
public interface MotionModel extends GraphicNode {

    /**
     * Shortcut to find the skeleton node.
     *
     * @return skeleton
     */
    Skeleton getSkeleton();

    /**
     * Replace skeleton.
     * The skeleton is a children node.
     *
     * @param skeleton
     */
    void setSkeleton(Skeleton skeleton);

    /**
     * Physic constraints associate to this model.
     * @return Sequence, never null
     */
    Sequence getConstraints();

    /**
     * Get collection of animations possible for this group.
     * Dictionary keys are animation names.
     *
     * @return available animations dictionary, never null
     */
    Dictionary getAnimations();

}
