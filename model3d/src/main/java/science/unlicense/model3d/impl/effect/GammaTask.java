
package science.unlicense.model3d.impl.effect;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.task.api.AbstractTask;

/**
 *
 * @author Johann Sorel
 */
public class GammaTask extends AbstractTask {

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("gamma"), new Chars("gamma"), Chars.EMPTY, GammaTask.class,
                new FieldType[]{},
                new FieldType[]{});

    public GammaTask() {
        super(DESCRIPTOR);
    }

    @Override
    public Document perform() {

        return outputParameters;
    }

}
