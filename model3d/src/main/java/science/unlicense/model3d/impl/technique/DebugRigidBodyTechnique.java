
package science.unlicense.model3d.impl.technique;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.NodeSequence;
import science.unlicense.common.api.predicate.ClassPredicate;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Affine;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.physics.api.body.Body;
import science.unlicense.physics.api.body.BodyGroup;
import science.unlicense.physics.api.body.RigidBody;

/**
 *
 * @author Johann Sorel
 */
public class DebugRigidBodyTechnique extends AbstractTechnique implements SubSceneTechnique {

    private static final Color[] GROUP_PALETTE = new Color[]{
        new ColorRGB(0x00,0x00,0x00),
        new ColorRGB(0x80,0x00,0x00),
        new ColorRGB(0x00,0x80,0x00),
        new ColorRGB(0x80,0x80,0x00),
        new ColorRGB(0x00,0x00,0x80),
        new ColorRGB(0x80,0x00,0x80),
        new ColorRGB(0x00,0x80,0x80),
        new ColorRGB(0xc0,0xc0,0xc0),
        new ColorRGB(0x80,0x80,0x80),
        new ColorRGB(0xff,0x00,0x00),
        new ColorRGB(0x00,0xff,0x00),
        new ColorRGB(0xff,0xff,0x00),
        new ColorRGB(0x00,0x00,0xff),
        new ColorRGB(0xff,0x00,0xff),
        new ColorRGB(0x00,0xff,0xff),
        new ColorRGB(0xff,0xff,0xff)
    };

    private boolean viewBody = true;
    private boolean viewBodyVelocity = true;
    private boolean viewBodyForce = true;

    public DebugRigidBodyTechnique() {
    }

    public boolean isBodyVelocityVisible() {
        return viewBodyVelocity;
    }

    public void setBodyVisible(boolean visible) {
        this.viewBody = visible;
    }

    public boolean isBodyVisible() {
        return viewBody;
    }

    public void setBodyVelocityVisible(boolean visible) {
        this.viewBodyVelocity = visible;
    }

    public void setBodyForceVisible(boolean visible) {
        this.viewBodyForce = visible;
    }

    public boolean isBodyForceVisible() {
        return viewBodyForce;
    }

    @Override
    public SceneNode derivateScene(SceneNode base) {

        final DefaultSceneNode root = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        root.setRootToNodeSpace(base.getRootToNodeSpace());
        root.setLocalCoordinateSystem(base.getLocalCoordinateSystem());

        final Sequence bodies = new NodeSequence(base, true, new ClassPredicate(RigidBody.class));

        final SimpleBlinnPhong rendererFront = new SimpleBlinnPhong();
        rendererFront.setMaterialName(new Chars("f"));
        rendererFront.getState().setDepthTest(false);
        rendererFront.getState().setCulling(RenderState.CULLING_BACK);
        rendererFront.getState().setWriteToDepth(false);
        rendererFront.getState().setPolygonMode(RenderState.MODE_LINE);

        final SimpleBlinnPhong rendererBack = new SimpleBlinnPhong();
        rendererBack.setMaterialName(new Chars("b"));
        rendererBack.getState().setDepthTest(false);
        rendererBack.getState().setCulling(RenderState.CULLING_FRONT);
        rendererBack.getState().setWriteToDepth(false);
        rendererBack.getState().setPolygonMode(RenderState.MODE_LINE);

        //render rigid body geometry
        if (viewBody){
            for (int i=0,n=bodies.getSize();i<n;i++){
                final Body body = (Body) bodies.get(i);
                if (body instanceof RigidBody){
                    final RigidBody rb = (RigidBody) body;
                    final Geometry geometry = rb.getGeometry();
                    final Affine trs = rb.getNodeToRootSpace();
                    final BodyGroup group = body.getGroup();

                    final SimpleBlinnPhong.Material matBack = SimpleBlinnPhong.newMaterial();
                    matBack.setName(rendererBack.getMaterialName());
                    matBack.setDiffuse(Color.GRAY_DARK);

                    final SimpleBlinnPhong.Material matFront = SimpleBlinnPhong.newMaterial();
                    matFront.setName(rendererFront.getMaterialName());
                    if (group != null) {
                        int index = System.identityHashCode(group) % GROUP_PALETTE.length;
                        matFront.setDiffuse(GROUP_PALETTE[index]);
                    } else {
                        matFront.setDiffuse(Color.GRAY_LIGHT);
                    }

                    final DefaultModel mesh = DefaultModel.createFromGeometry(geometry);
                    mesh.getNodeTransform().set(trs);
                    mesh.getMaterials().add(matFront);
                    mesh.getMaterials().add(matBack);
                    mesh.getTechniques().add(rendererFront);
                    mesh.getTechniques().add(rendererBack);

                    root.getChildren().add(mesh);
                }
            }
        }

//        //render velocities and forces
//        if (viewBodyVelocity || viewBodyForce){
//            gl.glLineWidth(1f);
//            gl.glBegin(GL.GL_LINES);
//            for (int i=0,n=bodies.getSize();i<n;i++){
//                final Body body = (Body) bodies.get(i);
//
//                final Matrix M = body.getNodeToRootSpace();
//                final MatrixRW MVP = P.multiply(V.multiply(M));
//
//                p0.setXYZW(0, 0, 0, 1);
//                p1.set(body.getMotion().getVelocity());p1.setW(1);
//                p2.set(body.getMotion().getForce().scale(0.1));p2.setW(1);
//                MVP.transform(p0,p0);
//                MVP.transform(p1,p1);
//                MVP.transform(p2,p2);
//                if (p0.getZ()<0) return; //check not outside screen
//
//                if (viewBodyVelocity){
//                    gl.glColor3f(0f, 1f, 0f);
//                    gl.glVertex2f((float) (p0.getX()/p0.getZ()),(float) (p0.getY()/p0.getZ()));
//                    gl.glVertex2f((float) (p1.getX()/p1.getZ()),(float) (p1.getY()/p1.getZ()));
//                }
//                if (viewBodyForce){
//                    gl.glColor3f(0f, 0f, 1f);
//                    gl.glVertex2f((float) (p0.getX()/p0.getZ()),(float) (p0.getY()/p0.getZ()));
//                    gl.glVertex2f((float) (p2.getX()/p2.getZ()),(float) (p2.getY()/p2.getZ()));
//                }
//            }
//            gl.glEnd();
//        }

        return root;
    }

}
