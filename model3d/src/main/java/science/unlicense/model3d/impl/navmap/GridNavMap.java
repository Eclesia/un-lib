package science.unlicense.model3d.impl.navmap;

import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.impl.s3d.Grid;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3i32;
import science.unlicense.math.impl.VectorNf64;

/**
 * Navigation map based on a grid.
 *
 * @author Johann Sorel
 */
public class GridNavMap implements NavMap {

    private final Grid navMap;
    private final TupleGrid navMapSm;
    private final TupleGridCursor cursor;
    private final Affine sceneToNavMap;
    private final Affine navMapToScene;
    private double groundDistance = -1;

    public GridNavMap(Grid navMap) {
        this.navMap = navMap;
        this.navMapSm = navMap.getTuples();
        this.sceneToNavMap = navMap.getGeomToGrid();
        this.navMapToScene = sceneToNavMap.invert();
        this.cursor = navMapSm.cursor();
    }

    public void setGroundDistance(double groundDistance) {
        this.groundDistance = groundDistance;
    }

    public double getGroundDistance() {
        return groundDistance;
    }

    private double getHeightAtGrid(final Vector3i32 gridCoord) {
        int inc = 0;
        cursor.moveTo(gridCoord);
        if (cursor.samples().get(0) != 0) {
            //search above
            while (cursor.samples().get(0) != 0 && inc < 10) {
                gridCoord.y++;
                clampCoord(gridCoord);
                inc++;
                cursor.moveTo(gridCoord);
            }
        } else {
            //search under
            while (cursor.samples().get(0) == 0 && inc < 10) {
                gridCoord.y--;
                clampCoord(gridCoord);
                inc++;
                cursor.moveTo(gridCoord);
            }
        }

        final double[] geoCoord = new double[]{gridCoord.x, gridCoord.y, gridCoord.z};
        navMapToScene.transform(geoCoord,0,geoCoord,0,1);
        return geoCoord[1];
    }

    private Vector3i32 toNavCoord(Tuple position) {
        position = sceneToNavMap.transform(position, null);
        Vector3i32 navMapCoord = new Vector3i32((int) position.get(0), (int) position.get(1), (int) position.get(2));
        return clampCoord(navMapCoord);
    }

    private Vector3i32 clampCoord(Vector3i32 navMapCoord) {
        for (int i = 0; i < 3; i++) {
            navMapCoord.set(i, Maths.clamp(navMapCoord.get(i), 0, (int) navMapSm.getExtent().get(i) - 1));
        }
        return navMapCoord;
    }

    private double getValue(double[][] matrix, double x, double z) {
        final double[] seq = new double[4];
        seq[0] = getValue(matrix[0], z);
        seq[1] = getValue(matrix[1], z);
        seq[2] = getValue(matrix[2], z);
        seq[3] = getValue(matrix[3], z);
        return getValue(seq, x);
    }

    private static double getValue(double[] points, double x) {
        return points[1] + 0.5 * x * (points[2] - points[0] + x * (2.0 * points[0] - 5.0 * points[1] + 4.0 * points[2] - points[3] + x * (3.0 * (points[1] - points[2]) + points[3] - points[0])));
    }

    @Override
    public VectorRW navigate(VectorRW start, VectorRW direction, VectorRW buffer) {
        if (buffer==null) buffer = start.copy();

        final VectorRW position = VectorNf64.create(start).localAdd(direction);
        buffer.set(position);
        Vector3i32 navMapCoord = toNavCoord(position);

        cursor.moveTo(navMapCoord);

        if (cursor.samples().get(0) != 0) {
            //invalid position, collision
            final VectorRW lastPosition = VectorNf64.create(start);
            //try to calculate a movement anyway, until we reach the collision
            //or at least the movement on the other axis
            buffer.set(lastPosition);

            for (int i = 0; i < 3; i++) {
                buffer.set(i, position.get(i));
                navMapCoord = toNavCoord(buffer);
                cursor.moveTo(navMapCoord);
                if (cursor.samples().get(0) != 0) {
                    //can't fix this axis translation
                    buffer.set(i, lastPosition.get(i));
                }
            }
        }

        if (groundDistance >= 0) {
            //find the first collision under current location

            while (cursor.samples().get(0) == 0 && navMapCoord.y > 0) {
                navMapCoord.y--;
                cursor.moveTo(navMapCoord);
            }

            final double[][] nieghbor = new double[4][4];
            for (int x = 0; x < nieghbor[0].length; x++) {
                for (int z = 0; z < nieghbor[1].length; z++) {
                    nieghbor[x][z] = getHeightAtGrid(new Vector3i32(
                        navMapCoord.x - 2 + x,
                        navMapCoord.y,
                        navMapCoord.z - 2 + z)
                    );
                }
            }

            final TupleRW navCoordD = sceneToNavMap.transform(buffer, null);
            navCoordD.set(0,(navCoordD.get(0) - (navMapCoord.x - 2)) / 4.0);
            navCoordD.set(1,(navCoordD.get(2) - (navMapCoord.z - 2)) / 4.0);

            final double z = getValue(nieghbor, navCoordD.get(0), navCoordD.get(2)) + groundDistance;
            buffer.setZ(z);
        }

        return buffer;
    }

}
