
package science.unlicense.model3d.impl.technique;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Affine;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public class DebugSkeletonTechnique extends AbstractTechnique implements SubSceneTechnique {

    private boolean viewBody = true;
    private boolean viewBodyVelocity = true;
    private boolean viewBodyForce = true;

    public DebugSkeletonTechnique() {
    }

    public boolean isBodyVelocityVisible() {
        return viewBodyVelocity;
    }

    public void setBodyVisible(boolean visible) {
        this.viewBody = visible;
    }

    public boolean isBodyVisible() {
        return viewBody;
    }

    public void setBodyVelocityVisible(boolean visible) {
        this.viewBodyVelocity = visible;
    }

    public void setBodyForceVisible(boolean visible) {
        this.viewBodyForce = visible;
    }

    public boolean isBodyForceVisible() {
        return viewBodyForce;
    }

    @Override
    public SceneNode derivateScene(SceneNode base) {

        if (!(base instanceof MotionModel)) return null;

        final DefaultSceneNode root = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        root.setRootToNodeSpace(base.getRootToNodeSpace());
        root.setLocalCoordinateSystem(base.getLocalCoordinateSystem());

        final Skeleton skeleton = ((MotionModel) base).getSkeleton();
        final Sequence joints = skeleton.getAllJoints();

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.RED);

        final SimpleBlinnPhong technique = new SimpleBlinnPhong();
        technique.getState().setDepthTest(false);
        technique.getState().setWriteToDepth(false);
        technique.getState().setCulling(RenderState.CULLING_NONE);
        technique.getState().setPolygonMode(RenderState.MODE_LINE);

        for (int i=0,n=joints.getSize();i<n;i++) {
            final Joint joint = (Joint) joints.get(i);
            Affine trs = joint.getNodeToRootSpace();
            final DefaultModel mesh = DefaultModel.createFromGeometry(new BBox(new double[]{-0.1,-0.1,-0.1}, new double[]{0.1,0.1,0.1}));
            mesh.getMaterials().add(material);
            mesh.getTechniques().add(technique);
            mesh.getNodeTransform().set(trs);
            root.getChildren().add(mesh);
        }

        return root;
    }

}
