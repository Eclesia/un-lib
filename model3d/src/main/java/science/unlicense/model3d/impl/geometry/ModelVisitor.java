
package science.unlicense.model3d.impl.geometry;

import science.unlicense.common.api.collection.AbstractDictionary;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.collection.primitive.IntSet;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.path.PathIterator;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.AbstractGeometry;
import science.unlicense.geometry.impl.AbstractMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.MorphTarget;
import science.unlicense.geometry.impl.Vertex;
import science.unlicense.geometry.impl.path.CoordinateTuplePathIterator;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.Vector;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Scalari32;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * Loop on all shell vertex informations.
 *
 * @author Johann Sorel
 */
public abstract class ModelVisitor {

    private final IntSet visited = new IntSet();

    private Model model;
    private Mesh shell;
    private TupleGrid ibo;
    private IndexedRange[] modes;

    private Skeleton skeleton = null;
    private boolean applyJointTransform = false;
    private boolean applyMorphTargets = false;

    //cache of joints by index
    private Joint[] sortedJoints;
    private Similarity[] jointSimilarities;

    public ModelVisitor() {
    }

    public ModelVisitor(Model model) {
        reset(model);
    }

    public void reset(Model model) {
        this.model = model;
        this.shell = (Mesh) model.getShape();
        ibo = shell.getIndex();
        modes = shell.getRanges();

        final Skin skin = model.getSkin();

        if (skin != null) {
            int nbBone = skin.getMaxWeightPerVertex();
            skeleton = skin.getSkeleton();
            sortedJoints = new Joint[skin.getSkeleton().getAllJoints().getSize()];
            jointSimilarities = new Similarity[sortedJoints.length];
        } else {
            int nbBone = 0;
            skeleton = null;
            sortedJoints = null;
            jointSimilarities = null;
        }

        visited.removeAll();
    }

    public void setApplyJointTransform(boolean applyJointTransform) {
        this.applyJointTransform = applyJointTransform;
    }

    public boolean isApplyJointTransform() {
        return applyJointTransform;
    }

    public void setApplyMorphTargets(boolean applyMorphTargets) {
        this.applyMorphTargets = applyMorphTargets;
    }

    public boolean isApplyMorphTargets() {
        return applyMorphTargets;
    }

    public void visit() {
        visited.removeAll();

        final TupleGridCursor cursor = ibo.cursor();
        final MeshTriangle t = new MeshTriangle();
//        final MeshLine l = new MeshLine();
        final MeshPoint p = new MeshPoint();

        int idx0;
        int idx1;
        int idx2;
        IndexedRange range;
        int mode;
        int offset;
        int cnt;
        int i;
        int n;
        for (int m=0; m<modes.length; m++) {
            range  = modes[m];
            mode   = range.getMode();
            offset = range.getIndexOffset();
            cnt    = range.getCount();

            switch (mode) {
                case GLC.PRIMITIVE.POINTS:
                    for (i=0; i<cnt; i++) {
                        cursor.moveTo(i+offset+0);
                        idx0 = (int) cursor.samples().get(0);
                        readPoint(p, idx0);
                        visit(p);
                    }   break;
                case GLC.PRIMITIVE.LINES:
                    for (i=0; i<cnt; i+=2) {
                        cursor.moveTo(i+offset+0);
                        idx0 = (int) cursor.samples().get(0);
                        cursor.moveTo(i+offset+1);
                        idx1 = (int) cursor.samples().get(0);
                        MeshLine l = new MeshLine(shell, idx0, idx1);
                        readLine(l, idx0, idx1);
                        visit(l);
                    }   break;
                case GLC.PRIMITIVE.LINES_ADJACENCY:
                    throw new UnimplementedException("TODO");
                case GLC.PRIMITIVE.LINE_LOOP:
                    throw new UnimplementedException("TODO");
                case GLC.PRIMITIVE.LINE_STRIP:
                    throw new UnimplementedException("TODO");
                case GLC.PRIMITIVE.LINE_STRIP_ADJACENCY:
                    throw new UnimplementedException("TODO");
                case GLC.PRIMITIVE.TRIANGLES:
                    for (n=offset+cnt; offset<n;) {
                        cursor.moveTo(offset);
                        idx0 = (int) cursor.samples().get(0);
                        offset++;
                        cursor.moveTo(offset);
                        idx1 = (int) cursor.samples().get(0);
                        offset++;
                        cursor.moveTo(offset);
                        idx2 = (int) cursor.samples().get(0);
                        offset++;
                        readTriangle(t, idx0, idx1, idx2);
                        visit(t);
                    }   break;
                case GLC.PRIMITIVE.TRIANGLES_ADJACENCY:
                    //TODO make a real type triangleadjency, same for fan and strip
                    for (i=0; i<cnt; i+=6) {
                        cursor.moveTo(i+offset+0);
                        idx0 = (int) cursor.samples().get(0);
                        cursor.moveTo(i+offset+2);
                        idx1 = (int) cursor.samples().get(0);
                        cursor.moveTo(i+offset+4);
                        idx2 = (int) cursor.samples().get(0);
                        readTriangle(t, idx0, idx1, idx2);
                        visit(t);
                    }   break;
                case GLC.PRIMITIVE.TRIANGLE_FAN:
                    cursor.moveTo(offset+0);
                    idx0 = (int) cursor.samples().get(0);
                    for (i=1,n=cnt-1; i<n; i++) {
                        cursor.moveTo(offset+i);
                        idx1 = (int) cursor.samples().get(0);
                        cursor.moveTo(offset+i+1);
                        idx2 = (int) cursor.samples().get(0);
                        readTriangle(t, idx0, idx1, idx2);
                        visit(t);
                    }   break;
                case GLC.PRIMITIVE.TRIANGLE_STRIP:
                    cursor.moveTo(offset+0);
                    idx0 = (int) cursor.samples().get(0);
                    cursor.moveTo(offset+1);
                    idx1 = (int) cursor.samples().get(0);
                    for (i=2,n=cnt-1; i<n; i++) {
                        cursor.moveTo(offset+i);
                        idx2 = (int) cursor.samples().get(0);
                        readTriangle(t, idx0, idx1, idx2);
                        visit(t);
                        idx0 = idx1;
                        idx1 = idx2;
                    }   break;
                case GLC.PRIMITIVE.TRIANGLE_STRIP_ADJACENCY:
                    throw new UnimplementedException("TODO");
                case GLC.PRIMITIVE.PATCHES:
                    throw new UnimplementedException("TODO");
                default:
                    throw new UnimplementedException("TODO");
            }
        }
    }

    private void readPoint(MeshPoint p, int idx0) {
        p.v0 = readVertex(idx0);
    }

    private void readTriangle(MeshTriangle p, int idx0, int idx1, int idx2) {
        p.v0 = readVertex(idx0);
        p.v1 = readVertex(idx1);
        p.v2 = readVertex(idx2);
    }

    private void readLine(MeshLine p, int idx0, int idx1) {
        p.v0 = readVertex(idx0);
        p.v1 = readVertex(idx1);
    }

    public MeshVertex readVertex(int idx) {
        MeshVertex v = new VVertex(shell, idx);
        ((VVertex) v).visited = visited.contains(idx);
        if (!v.isVisited()) visited.add(idx);

        if (applyMorphTargets && model.getMorphTargetWeights() != null) {
            v = applyMorphTransform(v);
        }
        if (applyJointTransform && skeleton != null) {
            v = applyJointTransform(v);
        }
        return v;
    }

    protected void visit(MeshTriangle candidate) {
        visit(candidate.v0);
        visit(candidate.v1);
        visit(candidate.v2);
    }

    protected void visit(MeshLine candidate) {
        visit(candidate.v0);
        visit(candidate.v1);
    }

    protected void visit(MeshPoint candidate) {
        visit(candidate.v0);
    }

    protected abstract void visit(MeshVertex vertex);

    private Joint getJoint(int index) {
        if (sortedJoints[index] == null) {
            sortedJoints[index] = (Joint) skeleton.getJointById(index);
        }
        return sortedJoints[index];
    }

    private Similarity getJointSimilarity(int index) {
        if (jointSimilarities[index] == null) {
            final Joint joint = getJoint(index);
            jointSimilarities[index] = joint.getBindPose()
                    .multiply(joint.getInvertBindPose(), (SimilarityRW) null);
        }
        return jointSimilarities[index];
    }

    private MeshVertex applyJointTransform(MeshVertex v) {
        Tuple jointIndexes = (Tuple) v.properties().getValue(Skin.ATT_JOINTS_0);
        if (jointIndexes == null) return v;
        return new SkinVertex(v, skeleton);
    }

    private MeshVertex applyMorphTransform(MeshVertex v) {
        return new MorphVertex(v, model);
    }

    public static final class MeshTriangle extends AbstractGeometry implements Triangle {
        public MeshVertex v0;
        public MeshVertex v1;
        public MeshVertex v2;

        private MeshTriangle() {
            super(3);
        }

        @Override
        public TupleRW getFirstCoord() {
            return v0.getCoordinate();
        }

        @Override
        public TupleRW getSecondCoord() {
            return v1.getCoordinate();
        }

        @Override
        public TupleRW getThirdCoord() {
            return v2.getCoordinate();
        }
    }

    public static final class MeshLine extends Mesh.Line {
        public MeshVertex v0;
        public MeshVertex v1;

        private MeshLine(Mesh shell, int idx0, int idx1) {
            super(shell, new int[]{idx0,idx1});
        }

        @Override
        public TupleRW getStart() {
            return v0.getCoordinate();
        }

        @Override
        public TupleRW getEnd() {
            return v1.getCoordinate();
        }

        @Override
        public PathIterator createPathIterator() {
            return new CoordinateTuplePathIterator(new Tuple[]{getStart(),getEnd()});
        }
    }

    public static final class MeshPoint {
        public MeshVertex v0;

        private MeshPoint() {
        }
    }

    public static interface MeshVertex extends Vertex {

        Model getModel();

        boolean isVisited();

    }

    public final class VVertex extends AbstractMesh.IndexedVertex implements MeshVertex {
        /** True if this vertex has already been visited as part of another primitive */
        public boolean visited;

        private VVertex(Mesh parent, int index) {
            super(parent, index);
        }

        @Override
        public Model getModel() {
            return model;
        }

        @Override
        public boolean isVisited() {
            return visited;
        }
    }

    private class SkinVertex extends AbstractDictionary implements MeshVertex {

        private final MeshVertex parent;
        private final Skeleton skeleton;
        private final Vector3f64 temp_v_model = new Vector3f64();
        private final Vector3f64 temp_v_view = new Vector3f64();
        private final Vector3f64 temp_n_model = new Vector3f64();
        private final Vector3f64 temp_n_view = new Vector3f64();
        private final Vector3f64 temp3 = new Vector3f64();
        private final Vector3f64 temp4 = new Vector3f64();
        private final Matrix3x3 tempRot1 = new Matrix3x3();
        private final Matrix3x3 tempRot2 = new Matrix3x3();

        private VectorRW vertice;
        private VectorRW normal;

        public SkinVertex(MeshVertex parent, Skeleton skeleton) {
            this.parent = parent;
            this.skeleton = skeleton;
        }

        @Override
        public Model getModel() {
            return model;
        }

        @Override
        public boolean isVisited() {
            return parent.isVisited();
        }

        @Override
        public int getIndex() {
            return parent.getIndex();
        }

        @Override
        public TupleRW getCoordinate() {
            return (TupleRW) getValue(Mesh.ATT_POSITION);
        }

        @Override
        public Dictionary properties() {
            return this;
        }

        @Override
        public PathIterator createPathIterator() {
            return new CoordinateTuplePathIterator(new Tuple[]{getCoordinate()});
        }

        @Override
        public CoordinateSystem getCoordinateSystem() {
            return parent.getCoordinateSystem();
        }

        @Override
        public void setCoordinateSystem(CoordinateSystem cs) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public int getDimension() {
            return parent.getDimension();
        }

        @Override
        public BBox getBoundingBox() {
            final TupleRW pos = getCoordinate();
            final BBox bbox = new BBox(getCoordinateSystem());
            bbox.getLower().set(pos);
            bbox.getUpper().set(pos);
            return bbox;
        }

        @Override
        public Dictionary getUserProperties() {
            return parent.getUserProperties();
        }

        @Override
        public int getSize() {
            return parent.properties().getSize();
        }

        @Override
        public Set getKeys() {
            return parent.properties().getKeys();
        }

        @Override
        public Collection getValues() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Collection getPairs() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Object getValue(Object key) {
            Object value = parent.properties().getValue(key);

            if (vertice == null) {
                final Tuple indexes = (Tuple) parent.properties().getValue(Skin.ATT_JOINTS_0);
                final Tuple weights = (Tuple) parent.properties().getValue(Skin.ATT_WEIGHTS_0);
                final Tuple deforms = (Tuple) parent.properties().getValue(Skin.ATT_JPARAMS_0);
                final boolean linear = deforms == null || deforms.get(0) == 0;

                vertice = VectorNf64.create((Tuple) parent.properties().getValue(Mesh.ATT_POSITION));
                Tuple pn = (Tuple) parent.properties().getValue(Mesh.ATT_NORMAL);
                if (pn != null) normal = VectorNf64.create(pn);

                if (linear) {
                    temp_v_model.set(vertice);
                    temp_v_view.setAll(0.0);
                    if (normal != null) {
                        temp_n_model.set(normal);
                        temp_n_view.setAll(0.0);
                    }
                    for (int i=0,n=indexes.getSampleCount(); i<n; i++) {
                        final Similarity simi = getJointSimilarity((int) indexes.get(i));
                        simi.transform(temp_v_model,temp3);
                        temp3.localScale(weights.get(i));
                        temp_v_view.localAdd(temp3);

                        if (normal != null) {
                            simi.getRotation().transform(temp_n_model,temp3);
                            temp3.localScale(weights.get(i));
                            temp_n_view.localAdd(temp3);
                        }
                    }
                    vertice.set(temp_v_view);

                    if (normal != null) {
                        normal.set(temp_n_view);
                    }
                } else {
                    //position and rotation and calculated separately with this method
                    //references :
                    // - http://www.seas.upenn.edu/~ladislav/kavan05spherical/kavan05spherical.pdf
                    // - http://image.diku.dk/projects/media/kasper.amstrup.andersen.07.pdf

                    temp4.setXYZ(deforms.get(1),deforms.get(2),deforms.get(3));
                    tempRot1.setAll(0.0);
                    temp_v_view.setAll(0.0);

                    if (normal != null) {
                        temp_n_model.set(normal);
                        temp_n_view.setAll(0.0);
                    }

                    for (int i=0,n=indexes.getSampleCount(); i<n; i++) {
                        final double w = weights.get(i);
                        final Similarity simi = getJointSimilarity((int) indexes.get(i));

                        final Matrix jointRot = simi.getRotation();
                        final Vector jointTrs = simi.getTranslation();

                        jointRot.transform(temp4,temp3);
                        temp3.localAdd(jointTrs);
                        temp3.localScale(w);
                        temp_v_view.localAdd(temp3);
                        jointRot.scale(w,tempRot2);
                        tempRot1.localAdd(tempRot2);

                        if (normal != null) {
                            simi.getRotation().transform(temp_n_model,temp3);
                            temp3.localScale(weights.get(i));
                            temp_n_view.localAdd(temp3);
                        }
                    }

                    temp3.set(vertice);
                    temp3.localSubtract(temp4);
                    tempRot1.transform(temp3,temp3);
                    temp_v_view.localAdd(temp3);

                    vertice.set(temp_v_view);

                    if (normal != null) {
                        normal.set(temp_n_view);
                    }
                }

    //            verticeTransformed[v.index] = new Vector3d();
    //            verticeTransformed[v.index].set(v.vertice);
    //            if (v.normal!=null) normalTransformed[v.index] = new Vector3d(v.normal[0], v.normal[1], v.normal[2]);

            }

            if (Mesh.ATT_POSITION.equals(key)) {
                value = vertice;
            } else if (Mesh.ATT_NORMAL.equals(key)) {
                value = normal;
            }

            return value;
        }

        @Override
        public void add(Object key, Object value) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Object remove(Object key) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public void removeAll() {
            throw new UnsupportedOperationException("Not supported.");
        }

    }

    private static class MorphVertex extends AbstractDictionary implements MeshVertex {

        private final MeshVertex parent;
        private final Model model;
        private final Sequence morphs;

        public MorphVertex(MeshVertex parent, Model mesh) {
            this.parent = parent;
            this.model = mesh;
            morphs = ((Mesh) mesh.getShape()).getMorphs();
        }

        @Override
        public Model getModel() {
            return model;
        }

        @Override
        public boolean isVisited() {
            return parent.isVisited();
        }

        @Override
        public int getIndex() {
            return parent.getIndex();
        }

        @Override
        public TupleRW getCoordinate() {
            return (TupleRW) getValue(Mesh.ATT_POSITION);
        }

        @Override
        public Dictionary properties() {
            return this;
        }

        @Override
        public PathIterator createPathIterator() {
            return new CoordinateTuplePathIterator(new Tuple[]{getCoordinate()});
        }

        @Override
        public CoordinateSystem getCoordinateSystem() {
            return parent.getCoordinateSystem();
        }

        @Override
        public void setCoordinateSystem(CoordinateSystem cs) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public int getDimension() {
            return parent.getDimension();
        }

        @Override
        public BBox getBoundingBox() {
            final TupleRW pos = getCoordinate();
            final BBox bbox = new BBox(getCoordinateSystem());
            bbox.getLower().set(pos);
            bbox.getUpper().set(pos);
            return bbox;
        }

        @Override
        public Dictionary getUserProperties() {
            return parent.getUserProperties();
        }

        @Override
        public int getSize() {
            return parent.properties().getSize();
        }

        @Override
        public Set getKeys() {
            return parent.properties().getKeys();
        }

        @Override
        public Collection getValues() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Collection getPairs() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Object getValue(Object key) {
            Object value = parent.properties().getValue(key);

            if (Mesh.ATT_POSITION.equals(key)) {
                final VectorRW tuple = VectorNf64.create(parent.getCoordinate());
                final VectorRW cp = tuple.copy();

                final Scalari32 idx = new Scalari32(getIndex());
                final float[] weights = model.getMorphTargetWeights();
                for (int i=0;i<weights.length;i++) {
                    if (weights[i] <= 0) continue;

                    final TupleGrid vertices = ((MorphTarget) morphs.get(i)).getVertices();
                    vertices.getTuple(idx, cp);
                    cp.localScale(weights[i]);
                    tuple.localAdd(cp);
                }
                return tuple;
            }
            //TODO other properties may be morphed

            return value;
        }

        @Override
        public void add(Object key, Object value) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Object remove(Object key) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public void removeAll() {
            throw new UnsupportedOperationException("Not supported.");
        }

    }

}
