
package science.unlicense.model3d.impl.navmap;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.index.IndexRecord;
import science.unlicense.geometry.api.index.QuadTree;
import science.unlicense.geometry.api.operation.CentroidOp;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.geometry.api.operation.IntersectionOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.Axis;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.system.DefaultCoordinateSystem;
import science.unlicense.geometry.api.system.Direction;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.model3d.impl.geometry.ModelVisitor;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class MeshNavMap implements NavMap {

    private final CoordinateSystem quadCs;
    private final QuadTree quadtree;
    private final Transform transform;
    private final VectorRW tempv = VectorNf64.createDouble(2);

    public MeshNavMap(Model model) {
        this(model.getCoordinateSystem(), (Mesh) model.getShape());
    }

    public MeshNavMap(CoordinateSystem cs, Mesh mesh) {

        //we build a coordinate system with only the right and forward axis
        final Axis[] axis = cs.getAxis();
        final Axis[] quadAxis = new Axis[2];
        for (int i=0;i<axis.length;i++) {
            if (Direction.FORWARD.isCompatible(axis[i].getDirection())!=0) {
                quadAxis[0] = axis[i];
            } else if (Direction.RIGHT.isCompatible(axis[i].getDirection())!=0) {
                quadAxis[1] = axis[i];
            }
        }
        this.quadCs = new DefaultCoordinateSystem(quadAxis);
        this.transform = CoordinateSystems.createTransform(cs, quadCs);

        final BBox bbox = mesh.getBoundingBox();
        final BBox b = new BBox(2);
        transform.transform(bbox.getLower(), b.getLower());
        b.getUpper().set(b.getLower());
        b.expand(transform.transform(bbox.getUpper(),null));

        quadtree = new QuadTree(2, 8, b);


        final ModelVisitor shellVisitor = new ModelVisitor() {
            @Override
            protected void visit(ModelVisitor.MeshTriangle candidate) {
                //copy triangle, the same one is always returned by the visitor
                final Triangle triangle = new DefaultTriangle(candidate.getCoordinateSystem());
                triangle.getFirstCoord().set(candidate.getFirstCoord());
                triangle.getSecondCoord().set(candidate.getSecondCoord());
                triangle.getThirdCoord().set(candidate.getThirdCoord());

                final IndexRecord rec = new IndexRecord(2, triangle);
                transform.transform(triangle.getFirstCoord(), rec.getLower());
                rec.getUpper().set(rec.getLower());
                rec.expand(transform.transform(triangle.getSecondCoord(), tempv));
                rec.expand(transform.transform(triangle.getThirdCoord(), tempv));

                quadtree.add(rec);
            }
            @Override
            protected void visit(ModelVisitor.MeshPoint candidate) {
            }
            @Override
            protected void visit(ModelVisitor.MeshLine candidate) {
            }
            @Override
            protected void visit(ModelVisitor.MeshVertex vertex) {
            }
        };

        shellVisitor.reset(new DefaultModel(mesh));
        shellVisitor.visit();
    }

    @Override
    public VectorRW navigate(VectorRW start, VectorRW direction, VectorRW buffer) {
        buffer.set(start);
        buffer.localAdd(direction);

        final Ray ray = new Ray(buffer.add(new Vector3f64(0, +1, 0)), new Vector3f64(0, -1, 0));

        final BBox search = new BBox(2);
        transform.transform(buffer, search.getLower());
        search.getUpper().set(search.getLower());

        boolean match = false;
        final Sequence result = quadtree.search(search);
        final IntersectionOp iop = new IntersectionOp(ray, null);
        for (int i=0,n=result.getSize();i<n;i++) {
            final IndexRecord rec = (IndexRecord) result.get(i);
            final Triangle triangle = (Triangle) rec.getValue();
            final Geometry intersection;
            try {
                iop.setSecond(triangle);
                intersection = iop.execute();
                if (intersection!=null && new DistanceOp(new CentroidOp(intersection).execute(), new DefaultPoint(buffer)).execute() < 0.5) {
                    buffer.set(new CentroidOp(intersection).execute().getCoordinate());
                    match = true;
                    break;
                }
            } catch (OperationException ex) {
                //TODO : raise exception in api ? or log ? or null return value ?
                ex.printStackTrace();
            }
        }

        if (!match) {
            buffer.set(start);
        }

        return buffer;
    }

}
