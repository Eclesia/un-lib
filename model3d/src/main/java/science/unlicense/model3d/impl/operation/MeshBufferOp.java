
package science.unlicense.model3d.impl.operation;

import science.unlicense.geometry.impl.Mesh;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.model3d.impl.geometry.ModelVisitor;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.physics.api.skeleton.Skeleton;


/**
 * Shrink or expand mesh skin around it's armature.
 *
 * @author Johann Sorel
 */
public class MeshBufferOp extends ModelVisitor{

    private final VectorRW v0 = VectorNf64.createDouble(3);
    private final VectorRW vNew = VectorNf64.createDouble(3);
    private final Vector3f64 direction = new Vector3f64();
    private final double a;
    private final double b;

    public MeshBufferOp(Model model, double a, double b) {
        super(model);
        this.a = a;
        this.b = b;
        visit();
    }

    @Override
    protected void visit(MeshVertex vertex) {
        if (vertex.isVisited()) return;

        final Tuple indexes = (Tuple) vertex.properties().getValue(Mesh.ATT_JOINTS_0);
        final Tuple weights = (Tuple) vertex.properties().getValue(Mesh.ATT_WEIGHTS_0);
        final TupleRW vertice = vertex.getCoordinate();

        final Skeleton skeleton = vertex.getModel().getSkin().getSkeleton();

        direction.setAll(0);
        for (int i=0,n=weights.getSampleCount(); i<n; i++) {
            double w = weights.get(i);
            if (w > 0) {
                int idx = (int) indexes.get(i);
                v0.setAll(0);
                skeleton.getJointById(idx).getNodeToRootSpace().transform(v0, v0);
                vNew.set(vertice);
                vNew.localSubtract(v0);
                vNew.localScale(w);
                direction.localAdd(vNew);
            }
        }
        direction.localNormalize();
        direction.localScale(b);

        vNew.set(vertice);
        vNew.localAdd(direction);
        vertice.set(vNew);

    }

}
