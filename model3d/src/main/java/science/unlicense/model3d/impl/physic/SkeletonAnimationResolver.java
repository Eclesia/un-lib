

package science.unlicense.model3d.impl.physic;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.OrderedSet;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.Similarity;
import static science.unlicense.model3d.impl.physic.SkeletonPoseResolver.findJoint;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * Attach a skeleton to an animation.
 *
 * @author Johann Sorel
 */
public final class SkeletonAnimationResolver {

    private SkeletonAnimationResolver(){}

    /**
     *
     * @param skeleton
     * @param animation
     * @param nameAliases models may have different names for bones
     *        when a bone is not found the aliases are searched
     */
    public static void map(Skeleton skeleton, SkeletonAnimation animation, Dictionary nameAliases){

        final Set resolved = new HashSet();

        final Sequence series = animation.getSeries();
        for (int i=0,n=series.getSize();i<n;i++){
            final JointTimeSerie serie = (JointTimeSerie) series.get(i);

            CharArray serieId;
            final Joint rj = serie.getJoint();
            if (rj!=null){
                serieId = rj.getTitle();
            } else {
                serieId = CObjects.toChars(serie.getJointIdentifier());
            }

            final Joint joint = findJoint(skeleton, serieId, nameAliases);

            if (joint!=null){
                if (resolved.contains(joint.getTitle())){
                    System.out.println("Joint : "+joint.getTitle()+" is mapped multiple times");
                    continue;
                }
                resolved.add(joint.getTitle());

                serie.setJointIdentifier(joint.getTitle());
                serie.setJoint(joint);
                final OrderedSet set = serie.getFrames();
                final Iterator ite = set.createIterator();
                while (ite.hasNext()){
                    final JointKeyFrame jf = (JointKeyFrame) ite.next();
                    final Similarity trs = joint.getNodeTransform();
                    final Similarity ptrs = jf.getValue();
                    final MatrixRW res = trs.viewMatrix().multiply(ptrs.viewMatrix());
                    jf.getValue().set(res);
                }
            } else {
                System.out.println("No bone for joint time serie : "+serieId);
            }
        }

        animation.setSkeleton(skeleton);
    }

}
