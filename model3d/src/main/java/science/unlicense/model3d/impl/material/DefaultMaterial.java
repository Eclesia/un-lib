
package science.unlicense.model3d.impl.material;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Properties;

/**
 *
 * @author Johann Sorel
 */
public class DefaultMaterial implements Material {

    protected CharArray name = Chars.EMPTY;
    protected final Properties properties = new Properties();

    public DefaultMaterial() {

    }

    public DefaultMaterial(Material toCopy) {
        if (toCopy != null) {
            properties.set(toCopy.properties());
        }
    }

    @Override
    public CharArray getName() {
        return name;
    }

    @Override
    public void setName(CharArray name) {
        CObjects.ensureNotNull(name, "name");
        this.name = name;
    }

    @Override
    public Properties properties() {
        return properties;
    }

}
