
package science.unlicense.model3d.impl.material;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.event.Properties;

/**
 *
 * @author Johann Sorel
 */
public interface Material {

    /**
     * Get material name.
     *
     * @return Chars, not null
     */
    CharArray getName();

    /**
     * Set material name.
     *
     * @param name not null.
     */
    void setName(CharArray name);

    /**
     * Get material properties.
     *
     * @return Properties
     */
    Properties properties();

}
