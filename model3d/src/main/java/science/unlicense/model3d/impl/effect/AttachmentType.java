
package science.unlicense.model3d.impl.effect;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultFieldType;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.common.api.number.NumberType;

/**
 * Named derived from OpenGL FBO.
 * @author Johann Sorel
 */
public class AttachmentType extends DefaultFieldType {

    private final SampleSystem sampleSystem;
    private final NumberType numericType;

    public AttachmentType(Chars id, SampleSystem sampleSystem, NumberType numericType) {
        super(id, id, id, 1, 1, TupleGrid.class, null, null, null);
        this.sampleSystem = sampleSystem;
        this.numericType = numericType;
    }

    public SampleSystem getSampleSystem() {
        return sampleSystem;
    }

    public NumberType getNumericType() {
        return numericType;
    }

}
