
package science.unlicense.model3d.impl.effect;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.task.api.AbstractTask;

/**
 *
 * @author Johann Sorel
 */
public class BloomTask extends AbstractTask {

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("bloom"), new Chars("bloom"), Chars.EMPTY, BloomTask.class,
                new FieldType[]{},
                new FieldType[]{});

    public BloomTask() {
        super(DESCRIPTOR);
    }

    @Override
    public Document perform() {

        return outputParameters;
    }

}
