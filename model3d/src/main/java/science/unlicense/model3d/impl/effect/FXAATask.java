
package science.unlicense.model3d.impl.effect;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.task.api.AbstractTask;

/**
 *
 * @author Johann Sorel
 */
public class FXAATask extends AbstractTask {

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("fxaa"), new Chars("fxaa"), Chars.EMPTY, FXAATask.class,
                new FieldType[]{},
                new FieldType[]{});

    public FXAATask() {
        super(DESCRIPTOR);
    }

    @Override
    public Document perform() {

        return outputParameters;
    }

}
