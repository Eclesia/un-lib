
package science.unlicense.model3d.impl.physic;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;

/**
 * An unresolved skeleton pose.
 * Stores informations about multiple unresolved joint poses.
 *
 * @author Johann Sorel
 */
public class RelativeSkeletonPose {

    private final Sequence jointPoses = new ArraySequence();

    public Sequence getJointPoses() {
        return jointPoses;
    }

}
