

package science.unlicense.model3d.impl.technique;

import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.impl.operation.MeshBufferOp;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.scene.Skin;

/**
 * Silhouette/outline rendering using backface method.
 * this consist by enlarging the mesh, and render only the backfaces.
 *
 * @author Johann Sorel
 */
public class SilhouetteBackFaceTechnique extends AbstractTechnique implements SubSceneTechnique {

    private Color color;
    private float width;

    /**
     *
     * @param color silhouette border color
     * @param width silhouette border width
     */
    public SilhouetteBackFaceTechnique(Color color, float width) {
        this.color = color;
        this.width = width;
    }

    /**
     * Set silhouette border color.
     *
     * @param c
     */
    public void setColor(Color c){
        color = c;
    }

    public Color getColor() {
        return color;
    }

    /**
     * Set silhouette border width.
     *
     * @param borderWidth
     */
    public void setBorderWidth(float borderWidth) {
        this.width = borderWidth;
    }

    public float getBorderWidth() {
        return width;
    }

    @Override
    public SceneNode derivateScene(SceneNode base) {

        final Model model = (Model) base;
        final Skin skin = model.getSkin();
        final DummyModel resizedMesh = new DummyModel(model);

        if (skin != null) {
            resizedMesh.getNodeTransform().set(model.getNodeTransform());
            resizedMesh.setLocalCoordinateSystem(model.getLocalCoordinateSystem());
            //resizedMesh.getMaterial().properties().set(mesh.getMaterial().properties());
            resizedMesh.setSkin(skin);

            //make a buffer expanding around bones
            final Mesh orig = (Mesh) model.getShape();
            final DefaultMesh copy = new DefaultMesh( orig.getCoordinateSystem() );
            copy.getAttributes().addAll(orig.getAttributes());
            copy.setIndex(orig.getIndex());
            copy.setRanges(orig.getRanges());

            //copy the vertices vbo, we are going to modify it
            TupleGrid vertices = copy.getPositions();
            vertices = new VBO(Geometries.toFloat32(vertices), vertices.getSampleSystem().getNumComponents());
            copy.setPositions(vertices);
            resizedMesh.setShape(copy);

            new MeshBufferOp(resizedMesh, 1, width);

            final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
            material.setDiffuse(color);

            final SimpleBlinnPhong p = new SimpleBlinnPhong();
            p.getState().setDepthTest(true);
            p.getState().setWriteToDepth(true);
            p.getState().setCulling(RenderState.CULLING_FRONT);
            //p.getState().setPolygonMode(RenderState.MODE_LINE);

            resizedMesh.getMaterials().add(material);
            resizedMesh.getTechniques().add(p);
        }

        return resizedMesh;
    }

    private class DummyModel extends DefaultModel {

        private final Model base;

        public DummyModel(Model base) {
            this.base = base;
        }

        @Override
        public SceneNode getParent() {
            return base.getParent();
        }
    }

}
