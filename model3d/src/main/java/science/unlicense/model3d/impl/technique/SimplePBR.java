
package science.unlicense.model3d.impl.technique;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.StackDictionary;
import science.unlicense.common.api.event.Properties;
import science.unlicense.display.impl.light.Light;
import science.unlicense.geometry.impl.Fragment;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.Vector;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.model3d.impl.material.DefaultMaterial;
import science.unlicense.model3d.impl.material.TextureMapping;

/**
 * Simple Metallic-Roughness Technique.
 *
 * Physical based rendering ay use a wide range of properties, this class
 * defines a possible composition.
 *
 * Specification :
 * GLTF 2.0 :
 * https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#metallic-roughness-material
 * https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#reference-pbrmetallicroughness
 *
 * @author Johann Sorel
 */
public class SimplePBR extends AbstractTechnique {

    public static final Chars BASECOLOR = Chars.constant("baseColor");
    public static final Chars METALLIC = Chars.constant("metallic");
    public static final Chars ROUGHNESS = Chars.constant("roughness");


    /**
     * In linear RGBA space.
     */
    public static final Chars BASECOLORFACTOR = Chars.constant("baseColorFactor");
    /**
     * Between 0 and 1.
     */
    public static final Chars METALLICFACTOR = Chars.constant("metallicFactor");
    /**
     * Between 0 and 1.
     */
    public static final Chars ROUGHNESSFACTOR = Chars.constant("roughnessFactor");
    /**
     * Texture in sRGB.
     */
    public static final Chars BASECOLORTEXTURE = Chars.constant("baseColorTexture");
    /**
     * Texture (RGB).
     * Metalness in on channel B.
     * Roughness in on channel G
     */
    public static final Chars METALLICROUGHNESSTEXTURE = Chars.constant("metallicRoughnessTexture");
    /**
     * A tangent space normal map.
     */
    public static final Chars NORMAL = AbstractTechnique.NORMAL;
    /**
     * The occlusion map indicating areas of indirect lighting.
     */
    public static final Chars OCCLUSION = Chars.constant(new Chars("occlusion"));
    /**
     * The emissive map controls the color and intensity of the light being emitted by the material.
     */
    public static final Chars EMISSIVE = Chars.constant(new Chars("emissive"));

    private static final Vector3f64 DIELECTRIC_SPECULAR = new Vector3f64(0.04, 0.04, 0.04);
    private static final Vector3f64 BLACK = new Vector3f64(0, 0, 0);
    private static final Vector3f64 ONE = new Vector3f64(1, 1, 1);

    private static Dictionary MATERIAL_DEFAULT;
    static {
        final Dictionary defs = new HashDictionary();
        defs.add(BASECOLORFACTOR, new ColorRGB(1f, 1f, 1f, 1f));
        defs.add(METALLICFACTOR, 1.0);
        defs.add(ROUGHNESSFACTOR, 1.0);
        MATERIAL_DEFAULT = Collections.readOnlyDictionary(defs);
    }

    public void process(Properties context, Material material, Fragment fragment, Fragment result) {

        final Dictionary stack = new StackDictionary(new Dictionary[]{
            fragment.properties(), material.properties().asDictionary(), properties.asDictionary(), context.asDictionary()
        });

        // EXTRACT VALUES //////////////////////////////////////////////////////
        VectorRW baseColor = VectorNf64.create((Tuple) material.properties().getPropertyValue(BASECOLORFACTOR));
        double metallic = ((Number) material.properties().getPropertyValue(METALLICFACTOR)).doubleValue();
        double roughness = ((Number) material.properties().getPropertyValue(ROUGHNESSFACTOR)).doubleValue();

        final TextureMapping baseColorTexture = (TextureMapping) material.properties().getPropertyValue(BASECOLORTEXTURE);
        final TextureMapping metallicRoughnessTexture = (TextureMapping) material.properties().getPropertyValue(METALLICROUGHNESSTEXTURE);

        if (baseColorTexture != null) {
            Color color = evaluateColor(baseColorTexture, stack);
            VectorRW rgb = VectorNf64.create(color.toRGBA());
            baseColor = rgb.localMultiply(baseColor);
        }

        if (metallicRoughnessTexture != null) {
            Color color = evaluateColor(metallicRoughnessTexture, stack);
            VectorRW rgb = VectorNf64.create(color.toRGBA());
            metallic *= rgb.get(2);
            roughness *= rgb.get(1);
        }

        result.properties().add(BASECOLOR, baseColor);
        result.properties().add(METALLIC, metallic);
        result.properties().add(ROUGHNESS, roughness);
    }

    public void applyLight(Fragment fragment, Light light, Vector eyeToFrag, Vector lightToFrag, Vector fragNormal) {
        VectorRW baseColor = (VectorRW) fragment.properties().getValue(BASECOLOR);
        double metallic = (double) fragment.properties().getValue(METALLIC);
        double roughness = (double) fragment.properties().getValue(ROUGHNESS);

        baseColor.localScale(1 - DIELECTRIC_SPECULAR.x);
        VectorRW C_diff = baseColor.localLerp(BLACK, metallic);
        VectorRW F_0 = DIELECTRIC_SPECULAR.lerp(baseColor, metallic);
        double α = roughness*roughness;

        //V is the eye vector to the shading location
        //L is the vector from the light to the shading location
        //N is the surface normal in the same space as the above values
        //H is the half vector, where H = normalize(L+V)
        Vector V = eyeToFrag;
        Vector L = lightToFrag;
        Vector N = fragNormal;
        Vector H = L.add(V).localNormalize();

        // Surface reflection ratio (F)
        VectorRW t = ONE.subtract(F_0);
        t.localScale(Math.pow(1.0 - V.dot(H), 5));
        Vector F = F_0.add(t);

        // Geometric occlusion (G)
        double k = roughness * Math.sqrt(2.0/Math.PI);
        double G = geometricOcclusion(k, L, H) * geometricOcclusion(k, N, H);

        // Microfaced Distribution (D)
        double a2 = α * α;
        double nh2 = Math.pow(N.dot(H), 2);
        double D = a2 / (Math.PI * Math.pow(nh2 * (a2-1) + 1, 2));

        // Diffuse Term (diffuse)
        Vector diffuse = C_diff.scale( 1.0/Math.PI);

        Vector f_diffuse = ONE.subtract(F).localMultiply(diffuse);
        Vector f_specular = (F.scale(G).scale(D)).scale(
                1.0 / (4.0 * (N.dot(L)) * (N.dot(V)))
                );
        Vector f = f_diffuse.add(f_specular);

        fragment.properties().add(BASECOLOR, f);
    }

    private static double geometricOcclusion(double k, Vector v, Vector H) {
        return v.dot(H) / (v.dot(H) * (1-k) + k);
    }

    public static Material newMaterial() {
        final DefaultMaterial material = new DefaultMaterial();
        material.properties().setDefaults(MATERIAL_DEFAULT);
        return view(material);
    }

    public static Material view(science.unlicense.model3d.impl.material.Material material) {
        if (material instanceof Material) {
            return (Material) material;
        }
        return new DecoratedMaterial(material);
    }


    public static interface Material extends science.unlicense.model3d.impl.material.Material {

        Color getBaseColorFactor();

        void setBaseColorFactor(Color color);

        double getMetallicFactor();

        void setMetallicFactor(double factor);

        double getRoughnessFactor();

        void setRoughnessFactor(double factor);

        TextureMapping getBaseColorTexture();

        void setBaseColorTexture(TextureMapping img);

        TextureMapping getMetallicRoughnessTexture();

        void setMetallicRoughnessTexture(TextureMapping img);
    }

    private static class DecoratedMaterial implements Material {

        private final science.unlicense.model3d.impl.material.Material parent;

        public DecoratedMaterial(science.unlicense.model3d.impl.material.Material parent) {
            this.parent = parent;
        }

        @Override
        public CharArray getName() {
            return parent.getName();
        }

        @Override
        public void setName(CharArray name) {
            parent.setName(name);
        }

        @Override
        public Properties properties() {
            return parent.properties();
        }

        @Override
        public Color getBaseColorFactor() {
            return (Color) properties().getPropertyValue(BASECOLORFACTOR);
        }

        @Override
        public void setBaseColorFactor(Color color) {
            properties().setPropertyValue(BASECOLORFACTOR, color);
        }

        @Override
        public double getMetallicFactor() {
            return (Double) properties().getPropertyValue(METALLICFACTOR);
        }

        @Override
        public void setMetallicFactor(double factor) {
            properties().setPropertyValue(METALLICFACTOR, factor);
        }

        @Override
        public double getRoughnessFactor() {
            return (Double) properties().getPropertyValue(ROUGHNESSFACTOR);
        }

        @Override
        public void setRoughnessFactor(double factor) {
            properties().setPropertyValue(ROUGHNESSFACTOR, factor);
        }

        @Override
        public TextureMapping getBaseColorTexture() {
            return (TextureMapping) properties().getPropertyValue(BASECOLORTEXTURE);
        }

        @Override
        public void setBaseColorTexture(TextureMapping img) {
            properties().setPropertyValue(BASECOLORTEXTURE, img);
        }

        @Override
        public TextureMapping getMetallicRoughnessTexture() {
            return (TextureMapping) properties().getPropertyValue(METALLICROUGHNESSTEXTURE);
        }

        @Override
        public void setMetallicRoughnessTexture(TextureMapping img) {
            properties().setPropertyValue(METALLICROUGHNESSTEXTURE, img);
        }

    }

}
