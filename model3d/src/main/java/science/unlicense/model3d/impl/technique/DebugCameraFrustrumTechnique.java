
package science.unlicense.model3d.impl.technique;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.DoubleSequence;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.tuple.TupleGrid1D;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 * Technique to display camera frustrum.
 *
 * @author Johann Sorel
 */
public class DebugCameraFrustrumTechnique extends AbstractTechnique implements SubSceneTechnique {

    public static final Chars PROPERTY_COLOR = Chars.constant("color");

    public DebugCameraFrustrumTechnique() {
        properties.getDefaults().add(PROPERTY_COLOR, new ColorRGB(1.0f, 0.0f, 0.0f, 1.0f));
    }

    @Override
    public SceneNode derivateScene(SceneNode base) {

        Color color = (Color) properties.getPropertyValue(PROPERTY_COLOR);

        final DefaultSceneNode root = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        root.setRootToNodeSpace(base.getRootToNodeSpace());
        root.setLocalCoordinateSystem(base.getLocalCoordinateSystem());

        if (base instanceof MonoCamera){
            final MonoCamera cam = (MonoCamera) base;
            final double near = cam.getNearPlane();
            final double far = near + 1; //cam.getFarPlane();

            final Ray ray1 = cam.calculateRayCameraCS(0,  0);
            final Ray ray2 = cam.calculateRayCameraCS(0,  512);
            final Ray ray3 = cam.calculateRayCameraCS(512,0);
            final Ray ray4 = cam.calculateRayCameraCS(512,512);

            final VectorRW n1 = ray1.getDirection().scale(near).localAdd(ray1.getPosition());
            final VectorRW n2 = ray2.getDirection().scale(near).localAdd(ray2.getPosition());
            final VectorRW n3 = ray3.getDirection().scale(near).localAdd(ray3.getPosition());
            final VectorRW n4 = ray4.getDirection().scale(near).localAdd(ray4.getPosition());
            final VectorRW f1 = ray1.getDirection().scale(far).localAdd(ray1.getPosition());
            final VectorRW f2 = ray2.getDirection().scale(far).localAdd(ray2.getPosition());
            final VectorRW f3 = ray3.getDirection().scale(far).localAdd(ray3.getPosition());
            final VectorRW f4 = ray4.getDirection().scale(far).localAdd(ray4.getPosition());

            final double[][] vertices = new double[8][3];
            vertices[0] = n1.toDouble();
            vertices[1] = n3.toDouble();
            vertices[2] = n4.toDouble();
            vertices[3] = n2.toDouble();

            vertices[4] = f3.toDouble();
            vertices[5] = f1.toDouble();
            vertices[6] = f4.toDouble();
            vertices[7] = f2.toDouble();

            final DoubleSequence vertexCursor = new DoubleSequence();
            //back
            vertexCursor.put(vertices[0]);
            vertexCursor.put(vertices[1]);
            vertexCursor.put(vertices[2]);
            vertexCursor.put(vertices[3]);

            //front
            vertexCursor.put(vertices[4]);
            vertexCursor.put(vertices[5]);
            vertexCursor.put(vertices[7]);
            vertexCursor.put(vertices[6]);

            //rigth
            vertexCursor.put(vertices[1]);
            vertexCursor.put(vertices[4]);
            vertexCursor.put(vertices[6]);
            vertexCursor.put(vertices[2]);

            //left
            vertexCursor.put(vertices[5]);
            vertexCursor.put(vertices[0]);
            vertexCursor.put(vertices[3]);
            vertexCursor.put(vertices[7]);

            //up
            vertexCursor.put(vertices[2]);
            vertexCursor.put(vertices[6]);
            vertexCursor.put(vertices[7]);
            vertexCursor.put(vertices[3]);

            //down
            vertexCursor.put(vertices[0]);
            vertexCursor.put(vertices[5]);
            vertexCursor.put(vertices[4]);
            vertexCursor.put(vertices[1]);

            final int[] indices = {
                2, 1, 0, //back
                3, 2, 0,
                6, 5, 4, //front
                7, 6, 4,
                10, 9, 8, //sides
                11, 10, 8,
                14, 13, 12,
                15, 14, 12,
                18, 17, 16,
                19, 18, 16,
                22, 21, 20,
                23, 22, 20 };

            final TupleGrid1D positions = new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(vertexCursor.toArrayDouble()),
                    Float64.TYPE, UndefinedSystem.create(3), 24);

            final TupleGrid1D index = new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(indices),
                    Int32.TYPE, UndefinedSystem.create(1), 36);
            final IndexedRange range = IndexedRange.TRIANGLES(0, 36);

            final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
            shell.getAttributes().add(DefaultMesh.ATT_POSITION, positions);

            shell.setIndex(index);
            shell.setRanges(new IndexedRange[]{range});

            final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
            material.setDiffuse(color);

            final SimpleBlinnPhong technique = new SimpleBlinnPhong();
            technique.getState().setPolygonMode(RenderState.MODE_LINE);
            technique.getState().setCulling(RenderState.CULLING_NONE);
            technique.setLightEnable(false);

            final DefaultModel mesh = new DefaultModel();
            mesh.setShape(shell);
            mesh.getMaterials().add(material);
            mesh.getTechniques().add(technique);
            mesh.getNodeTransform().notifyChanged();

            root.getChildren().add(mesh);
        }

        return root;
    }

}
