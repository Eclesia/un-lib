

package science.unlicense.model3d.impl.scene;

import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.impl.s3d.Grid;
import science.unlicense.math.api.Affine;
import science.unlicense.math.impl.Vector3i32;
import science.unlicense.model3d.impl.technique.RenderState;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Display a Grid as red cube where voxel is fill.
 *
 * @author Johann Sorel
 */
public class DebugGridNode extends DefaultGraphicNode {

    public DebugGridNode(Grid grid, Extent ext) {
        super(CoordinateSystems.UNDEFINED_3D);

        final TupleGridCursor cursor = grid.getTuples().cursor();
        final Affine geomToGrid = grid.getGeomToGrid();
        final Affine gridToGeom = geomToGrid.invert();

        final BBox voxel = new BBox(
                new double[]{0, 0, 0},
                new double[]{+1, +1, +1});
        final DefaultModel model = DefaultModel.createFromGeometry(voxel);

        final SimpleBlinnPhong.Material materialHit = SimpleBlinnPhong.newMaterial();
        materialHit.setDiffuse(Color.RED);

        final SimpleBlinnPhong.Material materialNoHit = SimpleBlinnPhong.newMaterial();
        materialNoHit.setDiffuse(Color.BLUE);

        final SimpleBlinnPhong technique = new SimpleBlinnPhong();
        technique.getState().setPolygonMode(RenderState.MODE_LINE);

        final Vector3i32 coord = new Vector3i32();
        final GraphicNode cells = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        cells.getNodeTransform().set(gridToGeom);
        for (int nx=(int) ext.get(0);coord.x<nx;coord.x+=1){
            coord.y=0;
            for (int ny=(int) ext.get(1);coord.y<ny;coord.y+=1){
                coord.z=0;
                for (int nz=(int) ext.get(2);coord.z<nz;coord.z+=1){
                    cursor.moveTo(coord);
                    if (cursor.samples().get(0) != 0) {
                        final DefaultModel cell = new DefaultModel();
                        cell.setShape(model.getShape());
                        cell.getMaterials().add((cursor.samples().get(0) != 0) ? materialHit : materialNoHit);
                        cell.getTechniques().add(technique);
                        cell.getNodeTransform().getTranslation().set(coord);
                        cell.getNodeTransform().notifyChanged();
                        cells.getChildren().add(cell);
                    }
                }
            }
        }
        children.add(cells);
    }

}
