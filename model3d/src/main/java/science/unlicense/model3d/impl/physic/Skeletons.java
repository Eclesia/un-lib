

package science.unlicense.model3d.impl.physic;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.anim.Animation;
import science.unlicense.display.api.anim.CompoundAnimation;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.SimilarityNd;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.model3d.impl.geometry.ModelVisitor;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public final class Skeletons {

    /**
     * Calculate vertice position, taking in account skeleton pose.
     *
     * @param model
     * @param vertexId
     */
    public static VectorRW evaluatePosition(Model model, int vertexId) {
        final ModelVisitor visitor = new ModelVisitor(model) {
            @Override
            protected void visit(ModelVisitor.MeshVertex vertex) {
            }
        };
        visitor.setApplyJointTransform(true);

        final ModelVisitor.MeshVertex vertex = visitor.readVertex(vertexId);
        return VectorNf64.create(vertex.getCoordinate());
    }

    private Skeletons(){}

    /**
     * Build relative skeleton pose from skeleton bind pose.
     *
     * @param jointRelativeFrom
     * @return RelativeSkeletonPose
     */
    public static RelativeSkeletonPose toRelative(final Skeleton skePose, final int jointRelativeFrom){

        final RelativeSkeletonPose pose = new RelativeSkeletonPose();

        if (jointRelativeFrom == JointKeyFrame.FROM_BASE){

            //rebuild the base skeleton to calculate the joint differences
            final Skeleton skeBase = skePose.copy();
            skeBase.resetToBase();

            final Sequence bindJoints = skePose.getChildren();
            final Sequence baseJoints = skeBase.getChildren();
            for (int i=0,n=bindJoints.getSize();i<n;i++){
                toRelative((Joint) bindJoints.get(i), (Joint) baseJoints.get(i), pose);
            }

        } else {
            throw new RuntimeException("Not supported yet");
        }
        return pose;
    }

    private static void toRelative(Joint bind, Joint base, RelativeSkeletonPose pose){
        final Similarity bindTrs = bind.getNodeTransform();
        final Similarity baseTrs = base.getNodeTransform();
        final JointKeyFrame jp = new JointKeyFrame();
        jp.setValue(SimilarityNd.create(base.getDimension()));
        final SimilarityRW trs = jp.getValue();
        jp.setFrom(JointKeyFrame.FROM_BASE);
        jp.setJoint(bind.getTitle());
        trs.getTranslation().set(bindTrs.getTranslation().subtract(baseTrs.getTranslation()));
        trs.getRotation().set(bindTrs.getRotation().multiply(baseTrs.getRotation().invert()));
        trs.notifyChanged();
        pose.getJointPoses().add(jp);

        //loop on childrens
        final Object[] bindChildren = bind.getChildren().toArray();
        final Object[] baseChildren = base.getChildren().toArray();
        for (int i=0;i<bindChildren.length;i++){
            toRelative((Joint) bindChildren[i], (Joint) baseChildren[i], pose);
        }

    }

    public static JointKeyFrame toRelative(Joint jt, final int jointRelativeFrom){
        throw new RuntimeException("TODO");
//        final Matrix bindPose = jt.getBindPose();
//        bindPose.set(jt.getInvertBindPose());
//        bindPose.localInvert();
//        //at this stage, the bind pose contains the root to joint matrix
//
//        final SceneNode parent = jt.getParent();
//        if (parent instanceof Joint){
//            final Joint jpa = (Joint) parent;
//            //we remove the root to parent transform
//            final Matrix parentToRoot = jpa.getBindPose().invert();
//            final Matrix parentToJoint = bindPose.multiply(parentToRoot);
//            //convert the matrix back to a NodeTransform
//            jt.getModelTransform().set(parentToJoint);
//        } else {
//            //no parent, the bind pose is the same as parent to joint matrix
//            jt.getModelTransform().set(bindPose);
//        }
    }

    /**
     * Attach an animation to a mesh and skeleton.
     *
     * @param anim Animation to map
     * @param skeleton skeleton to link : used by skeleton animations
     * @param node node to link : used by morph animations
     * @param nameAliases : possible aliases for name mapping
     */
    public static void mapAnimation(Animation anim, Skeleton skeleton, GraphicNode node, Dictionary nameAliases){
        if (anim instanceof SkeletonAnimation){
            final SkeletonAnimation skeAnim = (SkeletonAnimation) anim;
            SkeletonAnimationResolver.map(skeleton, skeAnim, nameAliases);

        } else if (anim instanceof MorphAnimation){
            final MorphAnimation morphAnim = (MorphAnimation) anim;
            morphAnim.setNode(node);

        } else if (anim instanceof CompoundAnimation){
            final Sequence subs = ((CompoundAnimation) anim).getElements();
            for (int i=0,n=subs.getSize();i<n;i++){
                mapAnimation((Animation) subs.get(i), skeleton, node, nameAliases);
            }
        }
    }

}
