
package science.unlicense.model3d.impl.technique;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 * Technique to display given node box.
 *
 * @author Johann Sorel
 */
public class DebugNodeBoxTechnique extends AbstractTechnique implements SubSceneTechnique {

    public static final Chars PROPERTY_COLOR = Chars.constant("color");

    public DebugNodeBoxTechnique() {
        properties.getDefaults().add(PROPERTY_COLOR, new ColorRGB(0.0f, 0.0f, 1.0f, 1.0f));
    }

    @Override
    public SceneNode derivateScene(SceneNode base) {

        Color color = (Color) properties.getPropertyValue(PROPERTY_COLOR);

        final DefaultSceneNode root = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        root.setRootToNodeSpace(base.getRootToNodeSpace());
        root.setLocalCoordinateSystem(base.getLocalCoordinateSystem());

        if (base instanceof GraphicNode){
            final GraphicNode cam = (GraphicNode) base;
            final BBox bbox = cam.getBBox();

            final DefaultMesh mesh;
            try {
                mesh = DefaultMesh.createFromGeometry(bbox);
            } catch (OperationException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }

            final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
            material.setDiffuse(color);

            final SimpleBlinnPhong technique = new SimpleBlinnPhong();
            technique.getState().setPolygonMode(RenderState.MODE_LINE);
            technique.getState().setCulling(RenderState.CULLING_NONE);
            technique.setLightEnable(false);

            final DefaultModel model = new DefaultModel();
            model.setShape(mesh);
            model.getMaterials().add(material);
            model.getTechniques().add(technique);
            model.getNodeTransform().notifyChanged();

            root.getChildren().add(model);
        }

        return root;
    }

}
