
package science.unlicense.model3d.impl;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.OrderedSet;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.api.anim.Animation;
import science.unlicense.display.api.anim.CompoundAnimation;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.MorphTarget;
import static science.unlicense.gpu.api.opengl.GLC.*;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.model3d.impl.physic.JointTimeSerie;
import science.unlicense.model3d.impl.physic.RelativeSkeletonPose;
import science.unlicense.model3d.impl.physic.SkeletonAnimation;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.constraint.AngleLimitConstraint;
import science.unlicense.physics.api.constraint.Constraint;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * Coordinate System utilities.
 *
 * @author Johann Sorel
 */
public final class CSUtilities {

    private static final Matrix3x3 INVERT_HAND = new Matrix3x3(
            1,  0,  0,
            0,  1,  0,
            0,  0, -1);

    private CSUtilities() {}

    /**
     * Invert coordinates : left hand CS <> right hand CS.
     * Z value is inverted.
     *
     * @param vbo : expect a float buffer, tuple size 3
     */
    public static void invertHandCS(TupleGrid vbo) {
        if (vbo==null) return;

        final TupleGridCursor cursor = vbo.cursor();
        while (cursor.next()) {
            cursor.samples().set(2, -cursor.samples().get(2));
        }
    }

    /**
     * Invert index : left hand CS <> right hand CS.
     * triangles clockwise direction are inverted.
     *
     * @param ibo : expect an int buffer, tuple size 3
     */
    public static void invertHandCS(TupleGrid ibo, IndexedRange[] ranges) {
        if (ibo==null) return;

        for (int k=0;k<ranges.length;k++) {
            final IndexedRange range = ranges[k];
            final int mode = range.getMode();
            final int nb = range.getCount();
            final int ofs = range.getIndexOffset();

            TupleGridCursor ib = ibo.cursor();
            ib.moveTo(ofs);

            if (mode==GL_POINTS || mode==GL_LINES || mode==GL_LINE_LOOP
              || mode==GL_LINE_STRIP || mode==GL_LINE_STRIP_ADJACENCY
              || mode==GL_LINES_ADJACENCY) {
                //nothing to do
            } else if (mode==GL_TRIANGLES) {
                for (int i=0;i<nb;i+=3) {
                    ib.moveTo(ofs+i+1);
                    int v1 = (int) ib.samples().get(0);
                    ib.moveTo(ofs+i+2);
                    int v2 = (int) ib.samples().get(0);
                    ib.moveTo(ofs+i+1);
                    ib.samples().set(0, v2);
                    ib.moveTo(ofs+i+2);
                    ib.samples().set(0, v1);
                }
            } else if (mode==GL_TRIANGLE_STRIP) {
                throw new InvalidArgumentException("Triangle strip inversion not supported yet.");
            } else if (mode==GL_TRIANGLE_FAN) {
                throw new InvalidArgumentException("Triangle fan inversion not supported yet.");
            } else if (mode==GL_TRIANGLE_STRIP_ADJACENCY) {
                throw new InvalidArgumentException("Triangle strip adjency inversion not supported yet.");
            } else if (mode==GL_TRIANGLES_ADJACENCY) {
                for (int i=0;i<nb;i+=6) {
                    ib.moveTo(ofs+i);
                    int v0 = (int) ib.samples().get(0);
                    ib.next();
                    int v1 = (int) ib.samples().get(0);
                    ib.next();
                    int v2 = (int) ib.samples().get(0);
                    ib.next();
                    int v3 = (int) ib.samples().get(0);
                    ib.next();
                    int v4 = (int) ib.samples().get(0);
                    ib.next();
                    int v5 = (int) ib.samples().get(0);

                    ib.moveTo(ofs+i+0); ib.samples().set(0, v0);
                    ib.moveTo(ofs+i+1); ib.samples().set(0, v5);
                    ib.moveTo(ofs+i+2); ib.samples().set(0, v4);
                    ib.moveTo(ofs+i+3); ib.samples().set(0, v3);
                    ib.moveTo(ofs+i+4); ib.samples().set(0, v2);
                    ib.moveTo(ofs+i+5); ib.samples().set(0, v1);
                }
            } else if (mode==GL_PATCHES) {
                throw new InvalidArgumentException("Patche inversion not supported yet.");
            }
        }

    }

    public static void invertHandCS(TupleRW tuple) {
        if (tuple==null) return;
        tuple.set(2,-tuple.get(2));
    }

    public static void invertHandCS(Quaternion q) {
        if (q==null) return;
        final Matrix3x3 m = q.toMatrix3();
        invertHandCS(m);
        q.fromMatrix(m);
    }

    public static void invertHandCS(MatrixRW matrix) {
        if (matrix==null) return;
        if (matrix.getNbCol()==3 && matrix.getNbRow()==3) {
            matrix.set(0, 2, -matrix.get(0, 2));
            matrix.set(1, 2, -matrix.get(1, 2));
            matrix.set(2, 0, -matrix.get(2, 0));
            matrix.set(2, 1, -matrix.get(2, 1));
        } else if (matrix.getNbCol()==4 && matrix.getNbRow()==4) {
            matrix.set(0, 2, -matrix.get(0, 2));
            matrix.set(1, 2, -matrix.get(1, 2));
            matrix.set(3, 2, -matrix.get(3, 2));
            matrix.set(2, 0, -matrix.get(2, 0));
            matrix.set(2, 1, -matrix.get(2, 1));
            matrix.set(2, 3, -matrix.get(2, 3));
        } else {
            throw new RuntimeException("Invalid matrix, must be 3x3 or 4x4");
        }
    }

    public static void invertHandCS(SimilarityRW q) {
        if (q==null) return;
        invertHandCS(q.getTranslation());
        invertHandCS(q.getRotation());
        q.notifyChanged();
    }

    public static void invertHandCS(MorphTarget mf) {
        if (mf==null) return;
        invertHandCS(mf.getVertices());
    }

    public static void invertHandCS(Mesh shell) {
        if (shell==null) return;
        invertHandCS(shell.getPositions());
        invertHandCS(shell.getNormals());
        invertHandCS(shell.getIndex(),shell.getRanges());
        invertHandCS(shell.getTangents());

        final Iterator ite = shell.getMorphs().createIterator();
        while (ite.hasNext()) {
            final MorphTarget morphTarget = (MorphTarget) ite.next();
            invertHandCS(morphTarget);
        }

        //recalculate shell bbox
        shell.getBoundingBox();
    }

    public static void invertHandCS(SceneNode node) {
        final SimilarityRW trs = node.getNodeTransform();
        invertHandCS(trs.getTranslation());
        invertHandCS(trs.getRotation());
        trs.notifyChanged();

        if (node instanceof Joint) {
            final Joint joint = (Joint) node;
            final Sequence seq = joint.getConstraints();
            for (int i=0,n=seq.getSize();i<n;i++) {
                final Constraint cst = (Constraint) seq.get(i);
                if (cst instanceof AngleLimitConstraint) {
                    final AngleLimitConstraint alc = (AngleLimitConstraint) cst;
                    final BBox limit = alc.getLimits();

                    //the z axis direction change, so it's the angle around X which must be fixed
                    double min = -limit.getMin(0);
                    double max = -limit.getMax(0);
                    limit.setRange(0, Math.min(min, max), Math.max(min, max));
                }
            }
        } else if (node instanceof RigidBody) {
        }

        //loop on children
        for (Iterator ite=node.getChildren().createIterator();ite.hasNext();) {
            final Object next = ite.next();
            if (next instanceof SceneNode) {
                invertHandCS((SceneNode) next);
            }
        }
    }

    public static void invertHandCS(Skeleton skeleton) {
        final Sequence roots = skeleton.getChildren();
        for (int i=0,n=roots.getSize();i<n;i++) {
            invertHandCS((SceneNode) roots.get(i));
        }
    }

    public static void invertHandCS(RelativeSkeletonPose skeleton) {
        final Sequence poses = skeleton.getJointPoses();
        for (int i=0,n=poses.getSize();i<n;i++) {
            final JointKeyFrame jp = (JointKeyFrame) poses.get(i);
            invertHandCS(jp.getValue());
        }
    }

    public static void invertHandCS(SkeletonAnimation animation) {
        //TODO
    }

    public static void invertHandCS(Model mesh) {
        invertHandCS((Node) mesh);
    }

    public static void invertHandCS(MotionModel pmesh) {
        invertHandCS((Node) pmesh);
    }

    private static void invertHandCS(Node node) {
        if (node instanceof Model) {
            final Mesh mesh = (Mesh) ((Model) node).getShape();
            invertHandCS(mesh);
        } else if (node instanceof MotionModel) {
            final Skeleton skeleton = ((MotionModel) node).getSkeleton();
            if (skeleton!=null) {
                invertHandCS(skeleton);
            }
        }

        for (Iterator ite=node.getChildren().createIterator();ite.hasNext();) {
            invertHandCS((Node) ite.next());
        }
    }


    ////////////////////////////////////////////////////////////////////////////

    public static void transform(TupleGrid vbo, Transform trs) {
        if (vbo==null) return;

        final TupleGridCursor cursor = vbo.cursor();
        while (cursor.next()) {
            trs.transform(cursor.samples(), cursor.samples());
        }
    }

    public static void transform(TupleGrid ibo, IndexedRange[] ranges, Transform trs) {
        if (ibo==null) return;

//        for (int k=0;k<ranges.length;k++) {
//            final IBO.Range range = ranges[k];
//            final int mode = range.getMode();
//            final int nb = range.getCount();
//            final int ofs = range.getIndexOffset();
//
//            final IntBuffer ib = ibo.getBuffer();
//            ib.position(ofs);
//
//
//            if (mode==GL.GL_POINTS || mode==GL.GL_LINES || mode==GL.GL_LINE_LOOP
//              || mode==GL.GL_LINE_STRIP || mode==GL3.GL_LINE_STRIP_ADJACENCY
//              || mode==GL3.GL_LINES_ADJACENCY) {
//                //nothing to do
//            } else if (mode==GL.GL_TRIANGLES) {
//                for (int i=0;i<nb;i+=3) {
//                    int v1 = ib.get(ofs+i+1);
//                    int v2 = ib.get(ofs+i+2);
//                    ib.put(ofs+i+1, v2);
//                    ib.put(ofs+i+2, v1);
//                }
//            } else if (mode==GL.GL_TRIANGLE_STRIP) {
//                throw new InvalidArgumentException("Triangle strip inversion not supported yet.");
//            } else if (mode==GL.GL_TRIANGLE_FAN) {
//                throw new InvalidArgumentException("Triangle fan inversion not supported yet.");
//            } else if (mode==GL3.GL_TRIANGLE_STRIP_ADJACENCY) {
//                throw new InvalidArgumentException("Triangle strip adjency inversion not supported yet.");
//            } else if (mode==GL3.GL_TRIANGLES_ADJACENCY) {
//                for (int i=0;i<nb;i+=6) {
//                    int v0 = ib.get(ofs+i+0);
//                    int v1 = ib.get(ofs+i+1);
//                    int v2 = ib.get(ofs+i+2);
//                    int v3 = ib.get(ofs+i+3);
//                    int v4 = ib.get(ofs+i+4);
//                    int v5 = ib.get(ofs+i+5);
//                    ib.put(ofs+i+0, v0);
//                    ib.put(ofs+i+1, v5);
//                    ib.put(ofs+i+2, v4);
//                    ib.put(ofs+i+3, v3);
//                    ib.put(ofs+i+4, v2);
//                    ib.put(ofs+i+5, v1);
//                }
//            } else if (mode==GL3.GL_PATCHES) {
//                throw new InvalidArgumentException("Patche inversion not supported yet.");
//            }
//        }

    }

    public static void transform(TupleRW tuple, Transform trs) {
        if (tuple==null) return;
        trs.transform(tuple, tuple);
    }

    public static void transform(Quaternion q, Transform trs) {
        if (q==null) return;
        final Matrix3x3 m = q.toMatrix3();
        transform(m,trs);
        q.fromMatrix(m);
    }

    public static void transform(Matrix3x3 matrix, Transform trs) {
        if (matrix==null) return;
        final Matrix m2 = ((Affine) trs).toMatrix();
        matrix.set(m2.multiply(matrix));
    }

    public static void transform(Matrix4x4 matrix, Transform trs) {
        if (matrix==null) return;
        final Matrix m2 = ((Affine) trs).toMatrix();
        matrix.set(m2.multiply(matrix));
    }

    public static void transform(MatrixRW matrix, Transform trs) {
        if (matrix==null) return;
        final Matrix m2 = ((Affine) trs).toMatrix();
        matrix.set(m2.multiply(matrix));
    }

    public static void transform(SimilarityRW q, Transform trs) {
        if (q==null) return;
        final MatrixRW m = q.viewMatrix().copy();
        transform(m, trs);
        q.set(m);
    }

    public static void transform(MorphTarget mf, Transform trs) {
        if (mf==null) return;
        transform(mf.getVertices(), trs);
    }

    public static void transform(Mesh shell, Transform trs) {
        if (shell==null) return;
        transform(shell.getPositions(), trs);
        transform(shell.getNormals(), trs);
        transform(shell.getIndex(),shell.getRanges(), trs);
        transform(shell.getTangents(), trs);

        final Iterator ite = shell.getMorphs().createIterator();
        while (ite.hasNext()) {
            final MorphTarget morphTarget = (MorphTarget) ite.next();
            transform(morphTarget, trs);
        }

        //recalculate shell bbox
        shell.getBoundingBox();
    }

    public static void transform(SceneNode node, Transform cstrs) {
        final SimilarityRW trs = node.getNodeTransform();
        transform(trs, cstrs);

//        if (node instanceof Joint) {
//            final Joint joint = (Joint) node;
//            final Sequence seq = joint.getConstraints();
//            for (int i=0,n=seq.getSize();i<n;i++) {
//                final Constraint cst = (Constraint) seq.get(i);
//                if (cst instanceof AngleLimitConstraint) {
//                    final AngleLimitConstraint alc = (AngleLimitConstraint) cst;
//                    final BBox limit = alc.getLimits();
//
//                    //the z axis direction change, so it's the angle around X which must be fixed
//                    double min = -limit.getMin(0);
//                    double max = -limit.getMax(0);
//                    limit.setRange(0, Math.min(min, max), Math.max(min, max));
//                }
//            }
//        } else if (node instanceof RigidBody) {
//        }

        //loop on children
        for (Iterator ite=node.getChildren().createIterator();ite.hasNext();) {
            Object next = ite.next();
            if (next instanceof SceneNode) {
                transform((SceneNode) next, cstrs);
            }
        }
    }

    public static void transform(Skeleton skeleton, Transform trs) {

        //TODO still not good, but better
        skeleton.resetToWorldPose();

        final Sequence joints = skeleton.getAllJoints();
        for (int i=0,n=joints.getSize();i<n;i++) {
            transform( ((SceneNode) joints.get(i)).getNodeTransform(), trs);
        }

        skeleton.reverseWorldPose();
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();
    }

    public static void transform(RelativeSkeletonPose skeleton, Transform trs) {
        final Sequence poses = skeleton.getJointPoses();
        for (int i=0,n=poses.getSize();i<n;i++) {
            final JointKeyFrame jp = (JointKeyFrame) poses.get(i);
            transform(jp.getValue(), trs);
        }
    }

    public static void transform(JointKeyFrame pose, Transform trs) {
        transform(pose.getValue(), trs);
    }

    public static void transform(JointTimeSerie serie, Transform trs) {
        final OrderedSet frames = serie.getFrames();
        final Iterator ite = frames.createIterator();
        while (ite.hasNext()) {
            final JointKeyFrame frame = (JointKeyFrame) ite.next();
            transform(frame, trs);
        }
    }

    public static void transform(SkeletonAnimation animation, Transform trs) {
        final Sequence series = animation.getSeries();
        for (int i=0,n=series.getSize();i<n;i++) {
            final JointTimeSerie serie = (JointTimeSerie) series.get(i);
            transform(serie, trs);
        }
    }

    public static void transform(Animation animation, Transform trs) {
        if (animation instanceof SkeletonAnimation) {
            transform((SkeletonAnimation) animation, trs);
        } else if (animation instanceof CompoundAnimation) {
            final CompoundAnimation ca = (CompoundAnimation) animation;
            final Sequence elements = ca.getElements();
            for (int i=0,n=elements.getSize();i<n;i++) {
                transform((Animation) elements.get(i), trs);
            }
        }
    }

    public static void transform(Model mesh, Transform trs) {
        transform((Node) mesh, trs);
    }

    public static void transform(MotionModel pmesh, Transform trs) {
        transform((Node) pmesh, trs);
    }

    public static void transform(Node node, Transform trs) {
        if (node instanceof Model) {
            final Mesh mesh = (Mesh) ((Model) node).getShape();
            transform(mesh, trs);
        } else if (node instanceof MotionModel) {
            final Skeleton skeleton = ((MotionModel) node).getSkeleton();
            if (skeleton!=null) {
                transform(skeleton, trs);
            }
        }

        for (Iterator ite=node.getChildren().createIterator();ite.hasNext();) {
            transform((Node) ite.next(), trs);
        }
    }

    public static void transform(Node node, CoordinateSystem target) {
        if (node instanceof SceneNode) {
            final SceneNode glnode = (SceneNode) node;
            final CoordinateSystem nodeCs = glnode.getCoordinateSystem();
            if (!nodeCs.equals(target)) {
                final Transform trs = CoordinateSystems.createTransform(nodeCs, target);
//                transform(glnode.getNodeTransform(), trs);

                if (node instanceof Model) {
                    final Mesh mesh = (Mesh) ((Model) node).getShape();
                    transform(mesh, trs);
                } else if (node instanceof MotionModel) {
                    final Skeleton skeleton = ((MotionModel) node).getSkeleton();
                    if (skeleton!=null) {
                        transform(skeleton, trs);
                    }
                }
                glnode.setLocalCoordinateSystem(target);
            }
        }

        for (Iterator ite=node.getChildren().createIterator();ite.hasNext();) {
            transform((Node) ite.next(), target);
        }
    }

}
