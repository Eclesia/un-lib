
package science.unlicense.model3d.impl.navmap;

import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public interface NavMap {

    VectorRW navigate(VectorRW start, VectorRW direction, VectorRW buffer);

}
