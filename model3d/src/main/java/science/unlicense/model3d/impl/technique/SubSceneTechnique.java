
package science.unlicense.model3d.impl.technique;

import science.unlicense.display.api.scene.SceneNode;

/**
 *
 * @author Johann Sorel
 */
public interface SubSceneTechnique extends Technique {

    SceneNode derivateScene(SceneNode base);

}
