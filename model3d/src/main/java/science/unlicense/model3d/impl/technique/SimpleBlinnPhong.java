
package science.unlicense.model3d.impl.technique;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.StackDictionary;
import science.unlicense.common.api.event.Properties;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.display.impl.light.Attenuation;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.light.PixelFragment;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.light.ProjectiveLight;
import science.unlicense.display.impl.light.SpotLight;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.impl.AbstractFragment;
import science.unlicense.gpu.impl.opengl.resource.TextureUtils;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.DecoratedColor;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.Vector;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector3f32;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.model3d.impl.material.DefaultMaterial;
import science.unlicense.model3d.impl.material.TextureMapping;

/**
 *
 * @author Johann Sorel
 */
public class SimpleBlinnPhong extends AbstractTechnique {

    public static final Chars MATERIAL_AMBIANT = Chars.constant("AMBIANT");
    public static final Chars MATERIAL_DIFFUSE = Chars.constant("DIFFUSE");
    public static final Chars MATERIAL_SPECULAR = Chars.constant("SPECULAR");
    public static final Chars MATERIAL_ALPHA = Chars.constant("ALPHA");
    public static final Chars MATERIAL_SHININESS = Chars.constant("SHININESS");
    public static final Chars MATERIAL_AMBIANTTEXTURE = Chars.constant("AMBIANT_TEXTURE");
    public static final Chars MATERIAL_DIFFUSETEXTURE = Chars.constant("DIFFUSE_TEXTURE");
    public static final Chars MATERIAL_SPECULARTEXTURE = Chars.constant("SPECULAR_TEXTURE");
    public static final Chars MATERIAL_ALPHATEXTURE = Chars.constant("ALPHA_TEXTURE");
    public static final Chars MATERIAL_NORMALTEXTURE = Chars.constant("NORMAL_TEXTURE");

    public static final Chars FRAGMENT_BASE_AMBIANT = Chars.constant("BASE_AMBIENT");
    public static final Chars FRAGMENT_BASE_DIFFUSE = Chars.constant("BASE_DIFFUSE");
    public static final Chars FRAGMENT_BASE_SPECULAR = Chars.constant("BASE_SPECULAR");
    public static final Chars FRAGMENT_ALPHA = Chars.constant("ALPHA");
    public static final Chars FRAGMENT_ACC_AMBIENT = Chars.constant("ACC_AMBIENT");
    public static final Chars FRAGMENT_ACC_DIFFUSE = Chars.constant("ACC_DIFFUSE");
    public static final Chars FRAGMENT_ACC_SPECULAR = Chars.constant("ACC_SPECULAR");

    //In result fragment
    public static final Chars NORMAL = Chars.constant("NORMAL");

    private static final Dictionary MATERIAL_DEFAULT;
    static {
        Dictionary defs = new HashDictionary();
        defs.add(MATERIAL_AMBIANT, ColorRGB.WHITE);
        defs.add(MATERIAL_DIFFUSE, ColorRGB.WHITE);
        defs.add(MATERIAL_SPECULAR, ColorRGB.WHITE);
        defs.add(MATERIAL_ALPHA, 1.0);
        defs.add(MATERIAL_SHININESS, 5.0);
        MATERIAL_DEFAULT = Collections.readOnlyDictionary(defs);
    }

    private boolean lightEnable = true;

    public void setLightEnable(boolean lightEnable) {
        this.lightEnable = lightEnable;
    }

    public boolean isLightEnable() {
        return lightEnable;
    }

    public void process(Properties context, science.unlicense.model3d.impl.material.Material material,
            science.unlicense.geometry.impl.Fragment fragment, Fragment frag) {

        final Dictionary stack = new StackDictionary(new Dictionary[]{
            fragment.properties(), material.properties().asDictionary(), properties.asDictionary(), context.asDictionary()
        });

        //frag.pixel = pixFrag;
        Color ambiant = (Color) material.properties().getPropertyValue(MATERIAL_AMBIANT);
        Color diffuse = (Color) material.properties().getPropertyValue(MATERIAL_DIFFUSE);
        Color specular = (Color) material.properties().getPropertyValue(MATERIAL_SPECULAR);
        float alpha = ((Number) material.properties().getPropertyValue(MATERIAL_ALPHA)).floatValue()
                      * diffuse.getAlpha();
        Tuple normal = null;

        TextureMapping alphaTexture = (TextureMapping) material.properties().getPropertyValue(MATERIAL_ALPHATEXTURE);
        TextureMapping ambiantTexture = (TextureMapping) material.properties().getPropertyValue(MATERIAL_AMBIANTTEXTURE);
        TextureMapping diffuseTexture = (TextureMapping) material.properties().getPropertyValue(MATERIAL_DIFFUSETEXTURE);
        TextureMapping specularTexture = (TextureMapping) material.properties().getPropertyValue(MATERIAL_SPECULARTEXTURE);
        TextureMapping normalTexture = (TextureMapping) material.properties().getPropertyValue(MATERIAL_NORMALTEXTURE);

        if (ambiantTexture != null) {
            ambiant = multiplyRGB(ambiant, evaluateColor(ambiantTexture, stack));
        }
        if (specularTexture != null) {
            specular = multiplyRGB(specular, evaluateColor(specularTexture, stack));
        }
        if (diffuseTexture != null) {
            Color texDiffuse = evaluateColor(diffuseTexture, stack);
            diffuse = multiplyRGB(diffuse, texDiffuse);
            alpha *= texDiffuse.getAlpha();
        }
        if (alphaTexture != null) {
            Color c = evaluateColor(alphaTexture, stack);
            alpha *= c.getRed();
        }

        frag.setBaseAmbient(ambiant);
        frag.setBaseDiffuse(diffuse);
        frag.setBaseSpecular(specular);
        frag.setAlpha(alpha);
    }

    private static Color multiplyRGB(Color c1, Color c2) {
        Vector3f32 v = new Vector3f32(
                c1.getRed()*c2.getRed(),
                c1.getGreen()*c2.getGreen(),
                c1.getBlue()*c2.getBlue());
        return new DecoratedColor(v, ColorSystem.RGB_FLOAT);
    }

//    /**
//     *
//     * @param light
//     * @param fragment
//     * @param lightCameraSpace
//     * @param fragToLightDirection
//     * @param shadowFactor 0 fragment is in the shadow, 1 fragment is in bright light
//     */
//    public static void evaluate(Light light, Fragment fragment, Vector lightCameraSpace, Vector fragToLightDirection, double shadowFactor) {
//
//        if (light instanceof DirectionalLight) {
//            evaluate( (DirectionalLight) light, fragment, lightCameraSpace, fragToLightDirection, shadowFactor);
//        } else if (light instanceof SpotLight) {
//            evaluate( (SpotLight) light, fragment, lightCameraSpace, fragToLightDirection, shadowFactor);
//        } else if (light instanceof PointLight) {
//            evaluate( (PointLight) light, fragment, lightCameraSpace, fragToLightDirection, shadowFactor);
//        } else if (light instanceof ProjectiveLight) {
//            evaluate( (ProjectiveLight) light, fragment, lightCameraSpace, fragToLightDirection, shadowFactor);
//        } else {
//            throw new UnimplementedException();
//        }
//    }

    public static void evaluate(DirectionalLight light, Fragment frag, Vector lightCameraSpace, Vector fragToLight, double shadowFactor) {

        final PixelFragment pxf = PixelFragment.view(frag);

        final Color diffuse = light.getDiffuse();
        final Color specular = light.getSpecular();

        double diffuseFactor = Maths.clamp(fragToLight.dot(pxf.getNormalView()), 0.0, 1.0);
        diffuseFactor = cellShading(diffuseFactor) ;

        double specularFactor = 0.0;
        if (diffuseFactor > 0.0) {
            specularFactor = calcSpecularFactor(pxf.getNormalView(), fragToLight, pxf.getEyeDirection());
        }

        final Color baseSpecular = frag.getBaseSpecular();
        final Color baseDiffuse = frag.getBaseDiffuse();
        TupleRW spec = frag.getAccSpecular();
        TupleRW diff = frag.getAccDiffuse();

        double ratioSpec = specularFactor * shadowFactor;
        spec.set(0, spec.get(0) + specular.get(0) * baseSpecular.get(0) * ratioSpec);
        spec.set(1, spec.get(1) + specular.get(1) * baseSpecular.get(1) * ratioSpec);
        spec.set(2, spec.get(2) + specular.get(2) * baseSpecular.get(2) * ratioSpec);

        double ratioDiff = diffuseFactor * shadowFactor;
        diff.set(0, diff.get(0) + diffuse.get(0) * baseDiffuse.get(0) * ratioDiff);
        diff.set(1, diff.get(1) + diffuse.get(1) * baseDiffuse.get(1) * ratioDiff);
        diff.set(2, diff.get(2) + diffuse.get(2) * baseDiffuse.get(2) * ratioDiff);

    }

    public static void evaluate(PointLight light, Fragment frag, Vector lightCameraSpace, Vector lightDirection, double shadowFactor) {

        final PixelFragment pxf = PixelFragment.view(frag);

        Color diffuse = light.getDiffuse();
        Color specular = light.getSpecular();
        Attenuation att = light.getAttenuation();

        // point light or spotlight (or other kind of light)
        final Vector3f64 fragToLight = new Vector3f64();
        lightCameraSpace.subtract(pxf.getPositionView(),fragToLight);
        double fragToLightDistance = fragToLight.length();
        double attenuation = att.computeAttenuation(fragToLightDistance);

        double diffuseFactor = Maths.max( fragToLight.dot(pxf.getNormalView()), 0.0);
        diffuseFactor = cellShading(diffuseFactor);

        double specularFactor = 0.0;
        if (diffuseFactor > 0.0) {
            fragToLight.localNormalize();
            specularFactor = calcSpecularFactor(pxf.getNormalView(), fragToLight, pxf.getEyeDirection());
        }

        Color baseSpecular = frag.getBaseSpecular();
        Color baseDiffuse = frag.getBaseDiffuse();
        TupleRW spec = frag.getAccSpecular();
        TupleRW diff = frag.getAccDiffuse();

        double ratioSpec = specularFactor * attenuation * shadowFactor;
        spec.set(0, spec.get(0) + specular.get(0) * baseSpecular.get(0) * ratioSpec);
        spec.set(1, spec.get(1) + specular.get(1) * baseSpecular.get(1) * ratioSpec);
        spec.set(2, spec.get(2) + specular.get(2) * baseSpecular.get(2) * ratioSpec);

        double ratioDiff = diffuseFactor * attenuation * shadowFactor;
        diff.set(0, diff.get(0) + diffuse.get(0) * baseDiffuse.get(0) * ratioDiff);
        diff.set(1, diff.get(1) + diffuse.get(1) * baseDiffuse.get(1) * ratioDiff);
        diff.set(2, diff.get(2) + diffuse.get(2) * baseDiffuse.get(2) * ratioDiff);
    }

    public static void evaluate(ProjectiveLight light, Fragment frag, Vector lightCameraSpace, Vector lightDirection, double shadowFactor) {
        throw new UnimplementedException();
    }

    public static void evaluate(SpotLight light, Fragment frag, Vector lightCameraSpace, Similarity V, double shadowFactor) {

        final PixelFragment pxf = PixelFragment.view(frag);

        final Color diffuse = light.getDiffuse();
        final Color specular = light.getSpecular();
        final Attenuation att = light.getAttenuation();

        // point light or spotlight (or other kind of light)
        final Vector3f64 fragToLight = new Vector3f64();
        lightCameraSpace.subtract(pxf.getPositionView(), fragToLight);
        double fragToLightDistance = fragToLight.length();
        double attenuation = att.computeAttenuation(fragToLightDistance);

        Vector dir3d = light.getWorldSpaceDirection();
        Vector4f64 dir4d = new Vector4f64(dir3d.getX(), dir3d.getY(), dir3d.getZ(), 0.0);
        V.toMatrix().transform(dir4d, dir4d);
        Vector3f64 spotDirection = new Vector3f64(dir4d);
        spotDirection.localNormalize();

        double clampedCosine = Maths.max(0.0, fragToLight.normalize().negate().dot(spotDirection));
        if (clampedCosine < Math.cos(Math.toRadians(light.getFallOffAngle()))) {
            // outside of spotlight cone
            attenuation = 0.0;
        } else {
            attenuation = attenuation * Math.pow(clampedCosine, light.getFallOffExponent());
        }

        double diffuseFactor = Maths.max( fragToLight.dot(pxf.getNormalView()), 0.0);
        diffuseFactor = cellShading(diffuseFactor);

        double specularFactor = 0.0;
        if (diffuseFactor > 0.0) {
            fragToLight.localNormalize();
            specularFactor = calcSpecularFactor(pxf.getNormalView(), fragToLight, pxf.getEyeDirection());
        }

        Color baseSpecular = frag.getBaseSpecular();
        Color baseDiffuse = frag.getBaseDiffuse();
        TupleRW spec = frag.getAccSpecular();
        TupleRW diff = frag.getAccDiffuse();

        double ratioSpec = specularFactor * attenuation * shadowFactor;
        spec.set(0, spec.get(0) + specular.get(0) * baseSpecular.get(0) * ratioSpec);
        spec.set(1, spec.get(1) + specular.get(1) * baseSpecular.get(1) * ratioSpec);
        spec.set(2, spec.get(2) + specular.get(2) * baseSpecular.get(2) * ratioSpec);

        double ratioDiff = diffuseFactor * attenuation * shadowFactor;
        diff.set(0, diff.get(0) + diffuse.get(0) * baseDiffuse.get(0) * ratioDiff);
        diff.set(1, diff.get(1) + diffuse.get(1) * baseDiffuse.get(1) * ratioDiff);
        diff.set(2, diff.get(2) + diffuse.get(2) * baseDiffuse.get(2) * ratioDiff);

    }

    private static double cellShading(double v) {
        return v;
    }


    private static double calcSpecularFactor(Tuple fragNormal, Vector toLight, Tuple toEye){

        // Standard reflection method, slower but more accurate
        final Vector v = toLight.negate();

        Vector reflection = v.reflect(fragNormal);
        double specularFactor = Maths.clamp(reflection.dot(toEye),0.0,1.0);
        specularFactor = Math.pow(specularFactor, 5.0);

        // Blinn-Phong method, faster but less accurate
        //vec3 halfWay = normalize(toEye + toLight);
        //float specularFactor = max(dot(halfWay, fragNormal),0.0);
        //specularFactor = pow(specularFactor, material.shininess);

//        if(CELLSHADINGSTEPS>0){
//            //cell shading, also called Toon shading
//            specularFactor = specularFactor < 0.5 ? 0.0 : 1.0;
//        }

        return specularFactor;
    }


    /**
     * Phong light model.
     *
     * @param ambient color
     * @param diffuse color
     * @param specular color
     * @param lightPosition in camera space
     * @param lightShininess
     * @param fragmentPosition in camera space
     * @param fragmentNormal in camera space, expected to be normalized
     * @param buffer to store result, can be null
     * @return Tuple, same as buffer is not null, new tuple otherwise
     */
    public static float[] phong(float[] ambient, float[] diffuse, float[] specular,
                                     VectorRW lightPosition, float lightShininess,
                                     VectorRW fragmentPosition, VectorRW fragmentNormal,
                                     float[] buffer) {

        // fragment to light direction
        final VectorRW fragToLight = lightPosition.subtract(fragmentPosition).localNormalize();

        final float lambertian = Maths.max( (float) fragToLight.dot(fragmentNormal), 0f);

        float specFactor = 0;
        if (lambertian > 0f) {
            // Eye direction
            final VectorRW eyeToFrag = fragmentPosition.negate().localNormalize();
            final VectorRW reflect = fragToLight.negate().reflect(fragmentNormal);
            final float angle = (float) Maths.max(reflect.dot(eyeToFrag), 0f);
            specFactor = (float) Math.pow(angle, lightShininess/4.0);
        }

        if (buffer==null) buffer = new float[ambient.length];

        buffer[0] = ambient[0] + (diffuse[0]*lambertian) + (specular[0]*specFactor);
        buffer[1] = ambient[1] + (diffuse[1]*lambertian) + (specular[1]*specFactor);
        buffer[2] = ambient[2] + (diffuse[2]*lambertian) + (specular[2]*specFactor);
        return buffer;
    }

    /**
     * Blinn-Phong light model.
     *
     * @param ambient color
     * @param diffuse color
     * @param specular color
     * @param lightPosition in camera space
     * @param lightShininess
     * @param fragmentPosition in camera space
     * @param fragmentNormal in camera space, expected to be normalized
     * @param buffer to store result, can be null
     * @return Tuple, same as buffer is not null, new tuple otherwise
     */
    public static float[] blinnPhong(float[] ambient, float[] diffuse, float[] specular,
                                     VectorRW lightPosition, float lightShininess,
                                     VectorRW fragmentPosition, VectorRW fragmentNormal,
                                     float[] buffer) {

        // fragment to light direction
        final VectorRW fragToLight = lightPosition.subtract(fragmentPosition).localNormalize();

        final float lambertian = Maths.max( (float) fragToLight.dot(fragmentNormal), 0f);

        float specFactor = 0;
        if (lambertian > 0f) {
            // Eye direction
            final VectorRW eyeToFrag = fragmentPosition.negate().localNormalize();
            //Blinn simplification
            final VectorRW halfDir = fragToLight.add(eyeToFrag).localNormalize();
            final float specAngle = Maths.max( (float) halfDir.dot(fragmentNormal), 0f);
            specFactor = (float) Math.pow(specAngle, lightShininess);
        }

        if (buffer==null) buffer = new float[ambient.length];

        buffer[0] = ambient[0] + (diffuse[0]*lambertian) + (specular[0]*specFactor);
        buffer[1] = ambient[1] + (diffuse[1]*lambertian) + (specular[1]*specFactor);
        buffer[2] = ambient[2] + (diffuse[2]*lambertian) + (specular[2]*specFactor);
        return buffer;
    }

    public static Material newMaterial() {
        final DefaultMaterial material = new DefaultMaterial();
        material.properties().setDefaults(MATERIAL_DEFAULT);
        return view(material);
    }

    public static Fragment newFragment(CoordinateSystem cs) {
        throw new UnimplementedException();
//        final DefaultFragment fragment = new DefaultFragment(cs);
//        fragment.properties().setDefaults(FRAGMENT_DEFAULT);
//        return view(fragment);
    }

    public static Material view(science.unlicense.model3d.impl.material.Material material) {
        if (material instanceof Material) {
            return (Material) material;
        }
        return new DecoratedMaterial(material);
    }


    public static Fragment view(science.unlicense.geometry.impl.Fragment fragment) {
        return new DecoratedFragment(fragment);
    }

    public static interface Material extends science.unlicense.model3d.impl.material.Material {

        Color getAmbient();

        void setAmbient(Color color);

        Color getDiffuse();

        void setDiffuse(Color color);

        Color getSpecular();

        void setSpecular(Color color);

        double getAlpha();

        void setAlpha(double alpha);

        double getShininess();

        void setShininess(double shininess);

        TextureMapping getAmbiantTexture();

        void setAmbiantTexture(TextureMapping img);

        TextureMapping getDiffuseTexture();

        void setDiffuseTexture(TextureMapping img);

        TextureMapping getSpecularTexture();

        void setSpecularTexture(TextureMapping img);

        TextureMapping getAlphaTexture();

        void setAlphaTexture(TextureMapping img);

        TextureMapping getNormalTexture();

        void setNormalTexture(TextureMapping img);

        /**
         * Indicate if the material will generate an opaque objets.
         * This can be used to sort meshes for better rendering of translucent
         * objects.
         *
         * @return true if final material color is fully opaque.
         */
        boolean isOpaque();

    }

    public static interface Fragment extends science.unlicense.geometry.impl.Fragment {

        Color getBaseAmbient();

        void setBaseAmbient(Color value);

        Color getBaseDiffuse();

        void setBaseDiffuse(Color value);

        Color getBaseSpecular();

        void setBaseSpecular(Color value);

        TupleRW getAccAmbient();

        void setAccAmbient(TupleRW value);

        TupleRW getAccDiffuse();

        void setAccDiffuse(TupleRW value);

        TupleRW getAccSpecular();

        void setAccSpecular(TupleRW value);

        float getAlpha();

        void setAlpha(float alpha);

    }

    private static class DecoratedMaterial implements Material {

        private final science.unlicense.model3d.impl.material.Material parent;

        public DecoratedMaterial(science.unlicense.model3d.impl.material.Material parent) {
            this.parent = parent;
        }

        @Override
        public CharArray getName() {
            return parent.getName();
        }

        @Override
        public void setName(CharArray name) {
            parent.setName(name);
        }

        @Override
        public Properties properties() {
            return parent.properties();
        }

        @Override
        public Color getAmbient() {
            return (Color) properties().getPropertyValue(MATERIAL_AMBIANT);
        }

        @Override
        public void setAmbient(Color color) {
            properties().setPropertyValue(MATERIAL_AMBIANT, color);
        }

        @Override
        public Color getDiffuse() {
            return (Color) properties().getPropertyValue(MATERIAL_DIFFUSE);
        }

        @Override
        public void setDiffuse(Color color) {
            properties().setPropertyValue(MATERIAL_DIFFUSE, color);
        }

        @Override
        public Color getSpecular(){
            return (Color) properties().getPropertyValue(MATERIAL_SPECULAR);
        }

        @Override
        public void setSpecular(Color color) {
            properties().setPropertyValue(MATERIAL_SPECULAR, color);
        }

        @Override
        public double getAlpha(){
            return (Double) properties().getPropertyValue(MATERIAL_ALPHA);
        }

        @Override
        public void setAlpha(double alpha) {
            properties().setPropertyValue(MATERIAL_ALPHA, alpha);
        }

        @Override
        public double getShininess() {
            return (Double) properties().getPropertyValue(MATERIAL_SHININESS);
        }

        @Override
        public void setShininess(double shininess) {
            properties().setPropertyValue(MATERIAL_SHININESS, shininess);
        }

        @Override
        public TextureMapping getAmbiantTexture() {
            return (TextureMapping) properties().getPropertyValue(MATERIAL_AMBIANTTEXTURE);
        }

        @Override
        public void setAmbiantTexture(TextureMapping img) {
            properties().setPropertyValue(MATERIAL_AMBIANTTEXTURE, img);
        }

        @Override
        public TextureMapping getDiffuseTexture() {
            return (TextureMapping) properties().getPropertyValue(MATERIAL_DIFFUSETEXTURE);
        }

        @Override
        public void setDiffuseTexture(TextureMapping img) {
            properties().setPropertyValue(MATERIAL_DIFFUSETEXTURE, img);
        }

        @Override
        public TextureMapping getSpecularTexture() {
            return (TextureMapping) properties().getPropertyValue(MATERIAL_SPECULARTEXTURE);
        }

        @Override
        public void setSpecularTexture(TextureMapping img) {
            properties().setPropertyValue(MATERIAL_SPECULARTEXTURE, img);
        }

        @Override
        public TextureMapping getAlphaTexture() {
            return (TextureMapping) properties().getPropertyValue(MATERIAL_ALPHATEXTURE);
        }

        @Override
        public void setAlphaTexture(TextureMapping img) {
            properties().setPropertyValue(MATERIAL_ALPHATEXTURE, img);
        }

        @Override
        public TextureMapping getNormalTexture() {
            return (TextureMapping) properties().getPropertyValue(MATERIAL_NORMALTEXTURE);
        }

        @Override
        public void setNormalTexture(TextureMapping img) {
            properties().setPropertyValue(MATERIAL_NORMALTEXTURE, img);
        }

        @Override
        public boolean isOpaque() {
            double alpha = getAlpha();
            if (alpha < 1.0) return false;
            Color diffuse = getDiffuse();
            if (diffuse.getAlpha() < 1.0f) return false;
            TextureMapping alphaTexture = getAlphaTexture();
            if (alphaTexture != null) return false;
            TextureMapping diffuseTexture = getDiffuseTexture();
            if (diffuseTexture != null) {
                final Image image = diffuseTexture.getTexture().getImage();
                if (image != null && !TextureUtils.isOpaque(image)) return false;
            }
            return true;
        }
    }

    private static class DecoratedFragment extends AbstractFragment implements Fragment {

        private final science.unlicense.geometry.impl.Fragment parent;

        public DecoratedFragment(science.unlicense.geometry.impl.Fragment parent) {
            this.parent = parent;
        }

        @Override
        public TupleRW getCoordinate() {
            return parent.getCoordinate();
        }

        @Override
        public Dictionary properties() {
            return parent.properties();
        }

        @Override
        public Color getBaseAmbient() {
            return (Color) properties().getValue(FRAGMENT_BASE_AMBIANT);
        }

        @Override
        public void setBaseAmbient(Color value) {
            properties().add(FRAGMENT_BASE_AMBIANT, value);
        }

        @Override
        public Color getBaseDiffuse() {
            return (Color) properties().getValue(FRAGMENT_BASE_DIFFUSE);
        }

        @Override
        public void setBaseDiffuse(Color value) {
            properties().add(FRAGMENT_BASE_DIFFUSE, value);
        }

        @Override
        public Color getBaseSpecular() {
            return (Color) properties().getValue(FRAGMENT_BASE_SPECULAR);
        }

        @Override
        public void setBaseSpecular(Color value) {
            properties().add(FRAGMENT_BASE_SPECULAR, value);
        }

        @Override
        public TupleRW getAccAmbient() {
            return (TupleRW) properties().getValue(FRAGMENT_ACC_AMBIENT);
        }

        @Override
        public void setAccAmbient(TupleRW value) {
            properties().add(FRAGMENT_ACC_AMBIENT, value);
        }

        @Override
        public TupleRW getAccDiffuse() {
            return (TupleRW) properties().getValue(FRAGMENT_ACC_DIFFUSE);
        }

        @Override
        public void setAccDiffuse(TupleRW value) {
            properties().add(FRAGMENT_ACC_DIFFUSE, value);
        }

        @Override
        public TupleRW getAccSpecular() {
            return (TupleRW) properties().getValue(FRAGMENT_ACC_SPECULAR);
        }

        @Override
        public void setAccSpecular(TupleRW value) {
            properties().add(FRAGMENT_ACC_SPECULAR, value);
        }

        @Override
        public float getAlpha() {
            return (Float) properties().getValue(FRAGMENT_ALPHA);
        }

        @Override
        public void setAlpha(float value) {
            properties().add(FRAGMENT_ALPHA, value);
        }
    }
}
