
package science.unlicense.model3d.impl.scene;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.transform.BBoxCalculator;
import science.unlicense.math.api.transform.Transform;

/**
 *
 * @author Johann Sorel
 */
public class DefaultModel extends DefaultGraphicNode implements Model {

    //BoundingBox cache
    private BBox bbox;

    protected Geometry shape;
    protected final Sequence materials = new ArraySequence();
    protected Skin skin;
    protected float[] morphWeights;

    public DefaultModel() {
        this(CoordinateSystems.UNDEFINED_3D);
    }

    public DefaultModel(Geometry geometry) {
        this(geometry.getCoordinateSystem());
        setShape(geometry);
    }

    public DefaultModel(CoordinateSystem cs) {
        super(cs);
    }

    public void setBoundingBox(BBox bbox) {
        this.bbox = bbox;
    }

    public void updateBoundingBox() {
        bbox = new BBoxCalculator().computeBBox(shape);
    }

    public BBox getBoundingBox() {
        if (bbox == null) {
            bbox = new BBoxCalculator().computeBBox(shape);
        }
        return (bbox != null) ? new BBox(bbox) : null;
    }

    @Override
    public Geometry getShape() {
        return shape;
    }

    public void setShape(Geometry geometry) {
        this.shape = geometry;
    }

    @Override
    public Sequence getMaterials() {
        return materials;
    }

    public Skin getSkin() {
        return skin;
    }

    public void setSkin(Skin skin) {
        this.skin = skin;
    }

    @Override
    public BBox getBBox(CoordinateSystem cs) {
        BBox bbox = shape.getBoundingBox();
        if (bbox == null) return null;

        if (this.coordinateSystem.equals(cs)) {
            return bbox;
        } else if (CoordinateSystems.isUndefined(cs)) {
            bbox = new BBox(bbox);
            bbox.setCoordinateSystem(cs);
            return bbox;
        } else {
            final Transform m = CoordinateSystems.createTransform(coordinateSystem, cs);
            bbox = Geometries.transform(bbox, m, null);
            bbox.setCoordinateSystem(cs);
            return bbox;
        }
    }

    @Override
    public float[] getMorphTargetWeights() {
        return morphWeights;
    }

    @Override
    public void setMorphTargetWeights(float[] weights) {
        this.morphWeights = weights;
    }

    public static DefaultModel createFromGeometry(Geometry geometry) {
        final DefaultMesh mesh;
        try {
            mesh = DefaultMesh.createFromGeometry(geometry);
        } catch (OperationException ex) {
            throw new RuntimeException(ex);
        }
        final DefaultModel model = new DefaultModel(mesh.getCoordinateSystem());
        model.setShape(mesh);
        return model;
    }
}
