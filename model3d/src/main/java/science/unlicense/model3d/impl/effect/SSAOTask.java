
package science.unlicense.model3d.impl.effect;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.number.Float32;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.process.DefaultImageTaskDescriptor;
import science.unlicense.image.impl.process.paint.SSAO;
import science.unlicense.task.api.AbstractTask;

/**
 *
 * @author Johann Sorel
 */
public class SSAOTask extends AbstractTask {

    public static final AttachmentType IN_COLOR = new AttachmentType(AfterEffect.LAYER_COLOR, ColorSystem.RGB_FLOAT, Float32.TYPE);
    public static final AttachmentType IN_POSITION_VIEW = new AttachmentType(AfterEffect.LAYER_POSITION_VIEW, ColorSystem.RGB_FLOAT, Float32.TYPE);
    public static final AttachmentType IN_NORMAL_VIEW = new AttachmentType(AfterEffect.LAYER_NORMAL_VIEW, ColorSystem.RGB_FLOAT, Float32.TYPE);
    public static final AttachmentType OUT_COLOR = new AttachmentType(AfterEffect.LAYER_COLOR, ColorSystem.RGBA_FLOAT, Float32.TYPE);

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("ssao"), new Chars("ssao"), Chars.EMPTY, SSAOTask.class,
                new FieldType[]{IN_COLOR, IN_POSITION_VIEW, IN_NORMAL_VIEW},
                new FieldType[]{OUT_COLOR});

    public SSAOTask() {
        super(DESCRIPTOR);
    }

    @Override
    public Document perform() {
        final TupleGrid incolors  = (TupleGrid) inputParameters.getPropertyValue(IN_COLOR.getId());
        final TupleGrid positions = (TupleGrid) inputParameters.getPropertyValue(IN_POSITION_VIEW.getId());
        final TupleGrid normals   = (TupleGrid) inputParameters.getPropertyValue(IN_NORMAL_VIEW.getId());
        final TupleGrid outcolors = (TupleGrid) outputParameters.getPropertyValue(OUT_COLOR.getId());

        final SSAO ssao = new SSAO();
        ssao.setRgb(incolors);
        ssao.setPositionsView(positions);
        ssao.setNormalsView(normals);
        Images.sample(ssao, outcolors);

        return outputParameters;
    }

}
