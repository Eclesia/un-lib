

package science.unlicense.model3d.impl.physic;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public final class SkeletonPoseResolver {

    private SkeletonPoseResolver(){}

    /**
     *
     * @param skeleton
     * @param pose
     * @param nameAliases
     */
    public static void resolve1(Skeleton skeleton, RelativeSkeletonPose pose, Dictionary nameAliases){

        //restore the base
        skeleton.resetToBase();

        final Sequence jointpose = pose.getJointPoses();
        for (int i=0,n=jointpose.getSize();i<n;i++){
            final JointKeyFrame jp = (JointKeyFrame) jointpose.get(i);
            final Joint joint = findJoint(skeleton, jp.getJoint(), nameAliases);
            if (joint==null) continue;

            joint.getNodeTransform().getTranslation().localAdd(jp.getValue().getTranslation());
            joint.getNodeTransform().getRotation().localMultiply(jp.getValue().getRotation());
            joint.getNodeTransform().notifyChanged();
        }

        skeleton.updateBindPose();

    }

    /**
     * TODO experimental.
     * try to properly restore XNA pose using cumulative transform.
     *
     * @param skeleton
     * @param pose
     */
    public static void resolve2(Skeleton skeleton, RelativeSkeletonPose pose){

        //restore the base
        skeleton.resetToBase();

        //create a map of all Joint > RelativePoseJoint
        final Dictionary dico = new HashDictionary();
        final Sequence jointpose = pose.getJointPoses();
        for (int i=0,n=jointpose.getSize();i<n;i++){
            final JointKeyFrame jp = (JointKeyFrame) jointpose.get(i);
            final Object id = jp.getJoint();

            final Joint jt = skeleton.getJointById(id);
            if (jt==null) continue;

            dico.add(jt, jp);
        }

        final VectorRW globalLocation = VectorNf64.createDouble(3);
        final Quaternion globalRotation = new Quaternion(0,0,0,1);

        final Sequence roots = skeleton.getChildren();
        for (int i=0,n=roots.getSize();i<n;i++){
            resolveJoint2((Joint) roots.get(i), dico, globalLocation, globalRotation);
        }

        skeleton.updateBindPose();
    }

    private static void resolveJoint2(Joint jt, Dictionary dico,
            VectorRW globalLocation, Quaternion globalRotation){

        final JointKeyFrame jp = (JointKeyFrame) dico.getValue(jt);

        if (jp!=null){
            VectorRW ptjLoc = globalLocation.subtract(jp.getValue().getTranslation());
            Matrix ptjRot = jp.getValue().getRotation().copy().localMultiply(globalRotation.toMatrix3().localInvert());

            jt.getNodeTransform().getTranslation().localAdd(ptjLoc);
            jt.getNodeTransform().getRotation().localMultiply(ptjRot);
            jt.getNodeTransform().notifyChanged();

            globalLocation.set(jp.getValue().getTranslation());
            globalRotation.set(new Quaternion().fromMatrix(jp.getValue().getRotation()));
        }

        for (Iterator ite=jt.getChildren().createIterator();ite.hasNext();){
            resolveJoint2((Joint) ite.next(), dico, globalLocation.copy(), globalRotation.copy());
        }

    }

    public static Joint findJoint(Skeleton skeleton, Object id, Dictionary nameAliases){
        final CharArray cid = CObjects.toChars(id);

        Joint joint;
        if (id instanceof Number){
            joint = (Joint) skeleton.getJointById(id);
        } else {
            joint = (Joint) skeleton.getJointByTitle(cid);
        }

        if (joint==null && nameAliases!=null){
            //search aliases
            Object value = nameAliases.getValue(cid);
            if (value instanceof CharArray){
                joint = skeleton.getJointByTitle((CharArray) value);
            } else if (value instanceof Collection){
                final Iterator ite = ((Collection) value).createIterator();
                while (ite.hasNext() && joint==null){
                    joint = skeleton.getJointByTitle((CharArray) ite.next());
                }
            }
        }

        return joint;
    }

}
