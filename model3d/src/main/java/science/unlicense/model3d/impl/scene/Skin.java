
package science.unlicense.model3d.impl.scene;

import science.unlicense.common.api.character.Chars;
import science.unlicense.physics.api.skeleton.Skeleton;
import science.unlicense.geometry.impl.Mesh;

/**
 * A skin holds the relation between a mesh vertices and
 * the skeleton joints.
 *
 * *Bone weights*
 *  Bone weight for each vertex,
 *  size is nbVertex * nbWeightPerVertex
 *
 * *Bone indexes*
 *  Bones for each vertex,
 *  size is nbVertex * nbWeightPerVertex
 *
 * *Bone deform*
 *  Deform type for each vertex,
 *  1 float : 0=linear,1=spherical
 *  3 float : spherical rotation center
 *
 * @author Johann Sorel
 */
public class Skin {

    /**
     * Identifier of the deforms parameters
     */
    public static final Chars ATT_JPARAMS_0 = Chars.constant("JPARAMS_0");
    public static final Chars ATT_JOINTS_0 = Mesh.ATT_JOINTS_0;
    public static final Chars ATT_WEIGHTS_0 = Mesh.ATT_WEIGHTS_0;

    private Skeleton skeleton;
    private int maxWeightPerVertex;

    public Skin() {
    }

    public void setSkeleton(Skeleton skeleton) {
        this.skeleton = skeleton;
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public void setMaxWeightPerVertex(int maxWeightPerVertex) {
        this.maxWeightPerVertex = maxWeightPerVertex;
    }

    public int getMaxWeightPerVertex() {
        return maxWeightPerVertex;
    }

}
