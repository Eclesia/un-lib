

package science.unlicense.model3d.impl.physic;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.api.anim.AbstractAnimation;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.MorphTarget;
import science.unlicense.model3d.impl.scene.Model;

/**
 * Mesh morph animation.
 *
 * @author Johann Sorel
 */
public class MorphAnimation extends AbstractAnimation {

    private static final NodeVisitor visitor = new NodeVisitor(){

        public Object visit(Node node, Object context) {

            if (node instanceof Model){
                final Model model = (Model) node;
                final Geometry shape = model.getShape();
                if (shape instanceof Mesh){
                    final Sequence ms = ((Mesh) shape).getMorphs();
                    final Dictionary morphState = (Dictionary) context;
                    for (Iterator ite = ms.createIterator();ite.hasNext();){
                        final MorphTarget mf = (MorphTarget) ite.next();
                        final Double def = (Double) morphState.getValue(mf.getName());
                        if (def != null) {
                            mf.setFactor(def.floatValue());
                        } else {
                            mf.setFactor(0);
                        }
                    }
                }
            }
            return super.visit(node, context);
        }
    };

    private final Dictionary series = new HashDictionary();
    private final Dictionary frame = new HashDictionary();
    private GraphicNode node;

    public Dictionary getSeries() {
        return series;
    }

    public GraphicNode getNode() {
        return node;
    }

    public void setNode(GraphicNode node) {
        this.node = node;
    }

    public double getLength() {
        double length = 0;
        final Iterator ite = series.getPairs().createIterator();
        while (ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final MorphTimeSerie serie = (MorphTimeSerie) pair.getValue2();
            final double l = serie.getLength();
            if (l>length) length = l;
        }
        return length;
    }

    public void update() {
        if (node==null) return;

        final double time = getTime();
        final Iterator ite = series.getPairs().createIterator();
        while (ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final MorphTimeSerie serie = (MorphTimeSerie) pair.getValue2();
            frame.add(serie.getMorphName(), serie.interpolate(time));
        }

        visitor.visit(node, frame);
    }

}
