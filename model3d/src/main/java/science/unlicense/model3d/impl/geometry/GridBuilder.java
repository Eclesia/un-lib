

package science.unlicense.model3d.impl.geometry;

import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.operation.IntersectsOp;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.operation.IntersectsExecutors;
import science.unlicense.image.impl.s3d.Grid;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector3i32;
import science.unlicense.model3d.impl.scene.Model;

/**
 * Calculate a 3D image cube of 1 bit.
 * A bit incidate a triangle intersects the voxel.
 *
 * @author Johann Sorel
 */
public class GridBuilder {

    private final NodeVisitor visitor = new NodeVisitor() {
        public Object visit(Node node, Object context) {

            if (node instanceof Model) {
                try {
                    append((Model) node);
                } catch (OperationException ex) {
                    throw new RuntimeException(ex);
                }
            }

            return super.visit(node, context);
        }
    };

    private final Grid grid;
    private final Extent imageSize;
    private final TupleGrid tb;
    private final TupleRW sample;
    private final Affine sceneToImage;

    //cache for voxel intersection
    private final Triangle triangle = new DefaultTriangle(CoordinateSystems.UNDEFINED_3D);
    private final TupleRW v0 = triangle.getFirstCoord();
    private final TupleRW v1 = triangle.getSecondCoord();
    private final TupleRW v2 = triangle.getThirdCoord();
    private final BBox voxel = new BBox(3);
    private final IntersectsOp intersect = new IntersectsOp(triangle, voxel);
    private final int[] lower = new int[3];
    private final int[] upper = new int[3];

    public GridBuilder(BBox bbox, Extent.Long imageSize) {
        this.grid = new Grid(bbox, imageSize);
        this.imageSize = imageSize;
        this.tb = grid.getTuples();
        this.sceneToImage = grid.getGeomToGrid();
        this.sample = tb.createTuple();
    }

    public Grid getResult() {
        return grid;
    }

    public void append(SceneNode node) {
        visitor.visit(node, null);
    }

    public void append(final Model model) throws OperationException{
        final Geometry shape = model.getShape();
        if (!(shape instanceof Mesh)) return;

        ModelVisitor visitor = new ModelVisitor(model) {
            @Override
            protected void visit(ModelVisitor.MeshVertex vertex) {}

            @Override
            protected void visit(ModelVisitor.MeshLine candidate) {}

            @Override
            protected void visit(ModelVisitor.MeshTriangle candidate) {
                try {
                    v0.set(candidate.getFirstCoord());
                    v1.set(candidate.getSecondCoord());
                    v2.set(candidate.getThirdCoord());
                    sceneToImage.transform(v0, v0);
                    sceneToImage.transform(v1, v1);
                    sceneToImage.transform(v2, v2);
                    testTriangle();
                } catch (OperationException ex) {
                    throw new RuntimeException(ex.getMessage(), ex);
                }
            }
        };
        visitor.visit();
    }

    public void appendTriangle(VectorRW v0, VectorRW v1, VectorRW v2) throws OperationException{
        this.v0.set(v0);
        this.v1.set(v1);
        this.v2.set(v2);
        sceneToImage.transform(this.v0, this.v0);
        sceneToImage.transform(this.v1, this.v1);
        sceneToImage.transform(this.v2, this.v2);
        testTriangle();
    }

    private void testTriangle() throws OperationException {

        //compute the possible intersection voxels
        for (int i=0;i<3;i++) {
            lower[i] = (int) Math.floor( Math.min(Math.min(v0.get(i), v1.get(i)), v2.get(i)) );
            upper[i] = (int) Math.ceil( Math.max(Math.max(v0.get(i), v1.get(i)), v2.get(i)) );

            //ensure we don't go out of image
            if (lower[i]<0) lower[i]=0;
            if (upper[i]>imageSize.get(i)) upper[i]=(int) imageSize.get(i);

        }

        //loop on each voxel
        final Vector3i32 coord = new Vector3i32();
        for (coord.x=lower[0];coord.x<upper[0];coord.x++) {
            for (coord.y=lower[1];coord.y<upper[1];coord.y++) {
                for (coord.z=lower[2];coord.z<upper[2];coord.z++) {
                    tb.getTuple(coord, sample);

                    //no need to test voxel if it is already intersecting
                    if (sample.get(0) == 0) {
                        //check for intersection
                        voxel.setRange(0, coord.x, coord.x+1);
                        voxel.setRange(1, coord.y, coord.y+1);
                        voxel.setRange(2, coord.z, coord.z+1);
                        sample.set(0, ((Boolean) IntersectsExecutors.TRIANGLE_BBOX.execute(intersect)) ? 1 : 0);
                        if (sample.get(0) != 0) tb.setTuple(coord, sample);
                    }
                }
            }
        }
    }

}
