
package science.unlicense.format.flac;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.media.api.AudioPacket;
import science.unlicense.media.api.AudioSamples;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.media.api.MediaReadStream;

/**
 *
 * @author Johann Sorel
 */
public class FLACReaderTest {

    @Test
    public void testMediaReader() throws IOException{

        FLACMediaStore store = new FLACMediaStore(Paths.resolve(new Chars("mod:/science/unlicense/format/flac/sample.flac")));
        final MediaReadStream reader = store.createReader(null);

        final int[] expectedSamples = new int[]{1,4095,8195,12284,16387,20478,21505,17409};

        int nbSample = 0;
        for (MediaPacket pack=reader.next();pack!=null;pack=reader.next()){
            //check the 8 first samples
            AudioSamples as = ((AudioPacket) pack).getSamples();
            int[] samples = as.asPCM(null, 16);
            if (nbSample<8){
                Assert.assertEquals(expectedSamples[nbSample], samples[0]);
            }
            nbSample++;
        }

        Assert.assertEquals(5962, nbSample);
    }

}
