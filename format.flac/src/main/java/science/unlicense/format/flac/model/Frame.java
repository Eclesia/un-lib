

package science.unlicense.format.flac.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.flac.FLACUtils;
import science.unlicense.math.api.Maths;
import static science.unlicense.media.api.AudioTrack.*;

/**
 *
 * @author Johann Sorel
 */
public class Frame {

    //Frame header

    /** 14bits : sync code : 3FFE */
    public int sync;
    /** 1bit */
    public int reserved1;
    /** 1bit : blocking strategy
     * 0 : fixed size , frame header contains the frame number
     * 1 : variable size , frame header contains the sample number
     */
    public int strategy;
    /** 4bits : block size */
    public int blockSize;
    /** 4bits : sample rate */
    public int sampleRate;
    /** 4bits : Channel assignment */
    public int channelAssignment;
    /** 3bits : Sample size, in bits */
    public int sampleSize;
    /** 1bit */
    public int reserved2;
    /** ?bits : UTF-8 encoding
     * sample or frame number
     */
    public long number;
    /** 8/16bits */
    public int bitblocksize;
    /** 8/16bits */
    public int bitsamplerate;
    /** 8bits */
    public int crc8;

    //sub frames
    public int[] channels;
    public SubFrame[] subs;

    //calculated values
    /** sample size in bits */
    public int trueNbBitsPerSample;
    /** number of interleaved samples stored in one subframe(block) */
    public int trueNbSample;


    //Frame footer
    /** 16bits */
    public int crc16;

    public void read(MetadataStreamInfo info, DataInputStream ds) throws IOException{
        //header
        reserved1 = ds.readBits(1);
        strategy = ds.readBits(1);
        blockSize = ds.readBits(4);
        sampleRate = ds.readBits(4);
        channelAssignment = ds.readBits(4);
        sampleSize = ds.readBits(3);
        reserved2 = ds.readBits(1);

        byte[] array = new byte[4];
        array[0] = ds.readByte();
        for (int i=1;!CharEncodings.UTF_8.isComplete(array, 0, i);i++){
            array[i] = ds.readByte();
        }
        number = CharEncodings.UTF_8.toUnicode(array);

        if (blockSize == 6){ // 0110
            bitblocksize = ds.readUByte();
        } else if (blockSize == 7){ // 0111
            bitblocksize = ds.readUShort();
        }
        if (bitsamplerate == 12){ // 1100
            bitsamplerate = ds.readUByte();
        } else if (bitsamplerate == 13 || bitsamplerate == 14 || bitsamplerate == 15){ // 11xx
            bitsamplerate = ds.readUShort();
        }
        crc8 = ds.readUByte();

        //calculate true size
        if     (sampleSize==0) trueNbBitsPerSample = info.bitsPerSample;
        else if (sampleSize==1) trueNbBitsPerSample = 8;
        else if (sampleSize==2) trueNbBitsPerSample = 12;
        else if (sampleSize==4) trueNbBitsPerSample = 16;
        else if (sampleSize==5) trueNbBitsPerSample = 20;
        else if (sampleSize==6) trueNbBitsPerSample = 24;
        else throw new IOException(ds, "Invalid sample size : "+ sampleSize);

        if     (blockSize==1)                 trueNbSample = 192;
        else if (blockSize>=2 && blockSize<=5) trueNbSample =  576 * (int) Maths.pow(2,(blockSize-2));
        else if (blockSize == 6)               trueNbSample = bitblocksize+1;
        else if (blockSize == 7)               trueNbSample = bitblocksize+1;
        else if ((blockSize & 0x8)!=0)         trueNbSample = 256 * (int) Maths.pow(2,(blockSize-8));
        else throw new IOException(ds, "Invalid block size : "+ blockSize);

        //subframes = number of channels
        channels = toChannels(channelAssignment);
        subs = new SubFrame[channels.length];
        for (int i=0;i<subs.length;i++){
            final int extraBit = (channels[i]==CHANNEL_SIDE || channels[i]==CHANNEL_DIFF) ? 1 : 0;
            subs[i] = new SubFrame();
            subs[i].read(ds,extraBit);
        }
        //realign
        ds.skipToByteEnd();

        //footer
        crc16 = ds.readUShort();
    }

    public static int[] toChannels(int channelAssignment) throws IOException{
        if (channelAssignment == 0){
            return new int[]{CHANNEL_MONO};
        } else if (channelAssignment == 1){
            return new int[]{CHANNEL_LEFT, CHANNEL_RIGHT};
        } else if (channelAssignment == 2){
            return new int[]{CHANNEL_LEFT, CHANNEL_RIGHT, CHANNEL_CENTER};
        } else if (channelAssignment == 3){
            return new int[]{CHANNEL_FRONT_LEFT, CHANNEL_FRONT_RIGHT, CHANNEL_REAR_LEFT, CHANNEL_REAR_RIGHT};
        } else if (channelAssignment == 4){
            return new int[]{CHANNEL_FRONT_LEFT, CHANNEL_FRONT_RIGHT, CHANNEL_CENTER, CHANNEL_REAR_LEFT, CHANNEL_REAR_RIGHT};
        } else if (channelAssignment == 5){
            return new int[]{CHANNEL_FRONT_LEFT, CHANNEL_FRONT_RIGHT, CHANNEL_CENTER, CHANNEL_SURROUND, CHANNEL_REAR_LEFT, CHANNEL_REAR_RIGHT};
        } else if (channelAssignment == 6){
            return new int[]{CHANNEL_FRONT_LEFT, CHANNEL_FRONT_RIGHT, CHANNEL_CENTER, CHANNEL_SURROUND, CHANNEL_REAR, CHANNEL_LEFT, CHANNEL_RIGHT};
        } else if (channelAssignment == 7){
            return new int[]{CHANNEL_FRONT_LEFT, CHANNEL_FRONT_RIGHT, CHANNEL_CENTER, CHANNEL_SURROUND, CHANNEL_REAR_LEFT, CHANNEL_REAR_RIGHT, CHANNEL_LEFT, CHANNEL_RIGHT};
        } else if (channelAssignment == 8){
            //TODO not exact, true value is left/side
            return new int[]{CHANNEL_LEFT, CHANNEL_DIFF};
        } else if (channelAssignment == 9){
            //TODO not exact, true value is right/side
            return new int[]{CHANNEL_DIFF, CHANNEL_RIGHT};
            //TODO not exact, true value is mid/side
        } else if (channelAssignment == 10){
            //TODO not exact, true value is mid/side
            return new int[]{CHANNEL_CENTER, CHANNEL_SIDE};
        } else {
            throw new IOException(null, "Invalid channel assignment : "+ channelAssignment);
        }

    }

    public final class SubFrame{

        /** 1bit : always 0 */
        public int bitPadding;
        /** 6bits : type */
        public int type;
        /** (k) bits : wasted number of bits */
        public int nbWastedBits;

        //recalculate values for decoding samples
        public int nbBitPerSample;

        //samples in PCM signed format
        public int[] samples;

        /**
         *
         * @param ds
         * @param extraBitPerSample set to 1 when subframe is Side or Difference
         * @throws IOException
         */
        private void read(DataInputStream ds, int extraBitPerSample) throws IOException{
            bitPadding = ds.readBits(1);
            if (bitPadding!=0){
                throw new IOException(ds, "Invalid subframe sync padding");
            }
            type     = ds.readBits(6);
            nbWastedBits = ds.readBits(1);
            if (nbWastedBits!=0){
                nbWastedBits = FLACUtils.readUnary(ds,1)+1;
            }

            nbBitPerSample = Frame.this.trueNbBitsPerSample+extraBitPerSample - nbWastedBits;

            if (type == 0x00){
                // 000000 : constant
                samples = readConstant(ds, nbBitPerSample, trueNbSample);
            } else if (type == 0x01){
                // 000001 : verbatim
                samples = readVerbatim(ds, nbBitPerSample, trueNbSample);

            } else if (type >= 0x08 && type <= 0x0F){
                // 001xxx : if (xxx <= 4) SUBFRAME_FIXED, xxx=order
                if ((type & 0x07) > 4){
                    throw new IOException(ds, "invalid subframe type : "+ type);
                }
                final int order = type & 0x07;
                samples = readFixed(ds, nbBitPerSample, trueNbSample, order);

            } else if ( (type & 0x20) != 0){
                // 1xxxxx SUBFRAME_LPC, xxxxx=order-1
                final int order = (type & 0x1F)+1;
                samples = readLPC(ds, nbBitPerSample, trueNbSample, order);

            } else {
                throw new IOException(ds, "Invalid subframe type : "+ type);
            }

            //fix wasted bits
            if (nbWastedBits>0){
                for (int i=0;i<samples.length;i++){
                    samples[i] <<= nbWastedBits;
                }
            }
        }
    }

    /**
     * Same value is used for all samples in the block.
     *
     * @param ds
     * @param bitPerSample
     * @param nbSamples
     * @return
     * @throws IOException
     */
    private static int[] readConstant(DataInputStream ds, int bitPerSample, int nbSamples) throws IOException{
        final int[] samples = new int[nbSamples];
        int constant = ds.readSignedBits(bitPerSample);
        Arrays.fill(samples, constant);
        return samples;
    }

    /**
     * Sample values are written without any compression.
     *
     * @param ds
     * @param bitPerSample
     * @param nbSamples
     * @return
     * @throws IOException
     */
    private static int[] readVerbatim(DataInputStream ds, int bitPerSample, int nbSamples) throws IOException{
        final int[] samples = new int[nbSamples];
        for (int i=0;i<nbSamples;i++){
            samples[i] = ds.readSignedBits(bitPerSample);
        }
        return samples;
    }

    /**
     * Sample values are divided in smaller groups (RICE?)
     *
     * @param ds
     * @param bitPerSample
     * @param nbSamples
     * @param order
     * @return
     * @throws IOException
     */
    private static int[] readFixed(DataInputStream ds, int bitPerSample, int nbSamples, int order) throws IOException{

        int sampleOffset = 0;
        final int[] samples = new int[nbSamples];

        //warmup samples
        for (sampleOffset=0;sampleOffset<order;sampleOffset++){
            samples[sampleOffset] = ds.readSignedBits(bitPerSample);
        }

        final int[] residuals = readResiduals(ds, nbSamples, order);

        //rebuild samples from residuals
        for (;sampleOffset<nbSamples;sampleOffset++){
            if (order==0){
                samples[sampleOffset] = residuals[sampleOffset];
            } else if (order==1){
                samples[sampleOffset] =   samples[sampleOffset-1]
                                      + residuals[sampleOffset-1];
            } else if (order==2){
                samples[sampleOffset] = 2*samples[sampleOffset-1]
                                      -   samples[sampleOffset-2]
                                      + residuals[sampleOffset-2];
            } else if (order==3){
                samples[sampleOffset] = 3*samples[sampleOffset-1]
                                      - 3*samples[sampleOffset-2]
                                      +   samples[sampleOffset-3]
                                      + residuals[sampleOffset-3];
            } else if (order==4){
                samples[sampleOffset] = 4*samples[sampleOffset-1]
                                      - 6*samples[sampleOffset-2]
                                      + 4*samples[sampleOffset-3]
                                      -   samples[sampleOffset-4]
                                      + residuals[sampleOffset-4];
            }
        }

        return samples;
    }

    private static int[] readLPC(DataInputStream ds, int bitPerSample, int nbSamples, int order) throws IOException{
        int sampleOffset = 0;
        final int[] samples = new int[nbSamples];

        //warmup samples
        for (sampleOffset=0;sampleOffset<order;sampleOffset++){
            samples[sampleOffset] = ds.readSignedBits(bitPerSample);
        }

        final int quantizedLinearPredictorCoeffPrecisionNbBits = ds.readBits(4)+1;
        //negative shifts are ignored
        final int shiftsInBits = Maths.max(ds.readSignedBits(5),0);

        final int[] coeffs = new int[order];
        for (int i=0;i<order;i++){
            coeffs[i] = ds.readSignedBits(quantizedLinearPredictorCoeffPrecisionNbBits);
        }

        final int[] residuals = readResiduals(ds, nbSamples, order);

        //QLC transform
        final long div = (int) Math.pow(2, shiftsInBits);
        for (int i=order;i<nbSamples;i++){
            long sum =0;
            for (int j=0;j<order;j++){
                sum += (long) coeffs[j] * (long) samples[i-j-1];
            }
            sum /= div;
            sum += residuals[i-order];
            samples[i] = (int) sum;
        }

        return samples;
    }

    private static int[] readResiduals(DataInputStream ds, int nbSamples, int order) throws IOException{

        int residualOffset = 0;
        final int[] residuals = new int[nbSamples];

        //read residual
        final int method = ds.readBits(2);
        final int partitionOrder = ds.readBits(4);

        //rice parameter differ in method 0 or 1
        final int paramNbBits;
        final int paramEscapeValue;
        if (method==0){
            paramNbBits = 4;
            paramEscapeValue = 15; // 1111 escape code
        } else if (method==1){
            paramNbBits = 5;
            paramEscapeValue = 31; // 11111 escape code
        } else {
            throw new IOException(ds, "Unknowned residual method "+method);
        }

        //decode partitions
        for (int p=0,pn=(int) Maths.pow(2,partitionOrder);p<pn;p++){
            final int param = ds.readBits(paramNbBits);

            //calculate number of residual in this partition
            final int nbResidual;
            if (partitionOrder==0){
                nbResidual = nbSamples-order;
            } else if (p!=0){
                nbResidual = nbSamples/pn;
            } else {
                nbResidual = (nbSamples/pn)-order;
            }

            //decode residuals
            if (param!=paramEscapeValue){
                for (int i=0;i<nbResidual;i++){
                    final int msb = FLACUtils.readUnary(ds,1);
                    final int lsb = ds.readBits(param);
                    int res = (msb*(int) Maths.pow(2,param)) + lsb;
                    if (res%2==0){
                        res = res/2;
                    } else {
                        res = -(res/2)-1;
                    }
                    residuals[residualOffset++] = res;
                }
            } else {
                final int nbBits = ds.readBits(5);
                for (int i=0;i<nbResidual;i++){
                    residuals[residualOffset++] = ds.readSignedBits(nbBits);
                }
            }
        }

        return residuals;
    }

}
