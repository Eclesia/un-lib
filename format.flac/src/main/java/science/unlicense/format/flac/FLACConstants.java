
package science.unlicense.format.flac;

/**
 *
 * @author Johann Sorel
 */
public final class FLACConstants {

    /**
     * Flac stream signature, locaed at the begining of the stream.
     */
    public static final byte[] SIGNATURE = new byte[]{'f','L','a','C'};

    // FLAC BLOCK TYPES ////////////////////////////////////////////////////////
    public static final int BLOCK_TYPE_STREAMINFO       = 0;
    public static final int BLOCK_TYPE_PADDING          = 1;
    public static final int BLOCK_TYPE_APPLICATION      = 2;
    public static final int BLOCK_TYPE_SEEKTABLE        = 3;
    public static final int BLOCK_TYPE_VORBIS_COMMENT   = 4;
    public static final int BLOCK_TYPE_CUESHEET         = 5;
    public static final int BLOCK_TYPE_PICTURE          = 6;

    // FLAC APPLICATION IDS ////////////////////////////////////////////////////
    /** "ATCH" FlacFile */
    public static final int APPLICATION_FLACFILE            = 0x41544348;
    /** "BSOL" beSolo */
    public static final int APPLICATION_BESOLO              = 0x42534F4C;
    /** "BUGS" Bugs Player */
    public static final int APPLICATION_BUGS_PLAYER         = 0x42554753;
    /** "Cues" GoldWave cue points (specification) */
    public static final int APPLICATION_GOLDWAVE            = 0x43756573;
    /** "Fica" CUE Splitter */
    public static final int APPLICATION_CUE_SPLITTER        = 0x46696361;
    /** "Ftol" flac-tools */
    public static final int APPLICATION_FLAC_TOOLS          = 0x46746F6C;
    /** "MOTB" MOTB MetaCzar */
    public static final int APPLICATION_MOTB                = 0x4D4F5442;
    /** "MPSE" MP3 Stream Editor */
    public static final int APPLICATION_MP3_STREAM_EDITOR   = 0x4D505345;
    /** "MuML" MusicML: Music Metadata Language */
    public static final int APPLICATION_MUSICML             = 0x4D754D4C;
    /** "RIFF" Sound Devices RIFF chunk storage */
    public static final int APPLICATION_SOUND_DEVICE_RIFF   = 0x52494646;
    /** "SFFL" Sound Font FLAC */
    public static final int APPLICATION_SOUND_FONT_FLAC     = 0x5346464C;
    /** "SONY" Sony Creative Software */
    public static final int APPLICATION_SONY                = 0x534F4E59;
    /** "SQEZ" flacsqueeze */
    public static final int APPLICATION_FLACSQUEEZE         = 0x5351455A;
    /** "TtWv" TwistedWave */
    public static final int APPLICATION_TWISTEDWAVE         = 0x54745776;
    /** "UITS" UITS Embedding tools */
    public static final int APPLICATION_UITS                = 0x55495453;
    /** "aiff" FLAC AIFF chunk storage */
    public static final int APPLICATION_AIFF                = 0x61696666;
    /** "imag" flac-image application for storing arbitrary files in APPLICATION metadata blocks */
    public static final int APPLICATION_FLAC_IMAGE          = 0x696D6167;
    /** "peem" Parseable Embedded Extensible Metadata (specification) */
    public static final int APPLICATION_PEEM                = 0x7065656D;
    /** "qfst" QFLAC Studio */
    public static final int APPLICATION_QFLAC               = 0x71667374;
    /** "riff" FLAC RIFF chunk storage */
    public static final int APPLICATION_FLAC_RIFF_CHUNK     = 0x72696666;
    /** "tune" TagTuner */
    public static final int APPLICATION_TAGTUNER            = 0x74756E65;
    /** "xbat" XBAT */
    public static final int APPLICATION_XBAT                = 0x78626174;
    /** "xmcd" xmcd*/
    public static final int APPLICATION_XMCD                = 0x786D6364;

    // FLAC PICTURE TYPES //////////////////////////////////////////////////////
    /** Other */
    public static final int PICTURE_OTHER = 0;
    /** 32x32 pixels 'file icon' (PNG only) */
    public static final int PICTURE_FILEICON_PNG = 1;
    /** Other file icon */
    public static final int PICTURE_FILEICON_OTHER = 2;
    /** Cover (front) */
    public static final int PICTURE_COVER_FRONT = 3;
    /** Cover (back) */
    public static final int PICTURE_COVER_BACK = 4;
    /** Leaflet page */
    public static final int PICTURE_LEAFLET = 5;
    /** Media (e.g. label side of CD) */
    public static final int PICTURE_MEDIA = 6;
    /** Lead artist/lead performer/soloist */
    public static final int PICTURE_LEAD_ARTIST = 7;
    /** Artist/performer */
    public static final int PICTURE_ARTIST = 8;
    /** Conductor */
    public static final int PICTURE_CONDUCTOR = 9;
    /** Band/Orchestra */
    public static final int PICTURE_ORCHESTRA = 10;
    /** Composer */
    public static final int PICTURE_COMPOSER = 11;
    /** Lyricist/text writer */
    public static final int PICTURE_TEXT_WRITER = 12;
    /** Recording Location */
    public static final int PICTURE_RECORDING_LOCATION = 13;
    /** During recording */
    public static final int PICTURE_DURING_RECORD = 14;
    /** During performance */
    public static final int PICTURE_DURING_PERFORMANCE = 15;
    /** Movie/video screen capture */
    public static final int PICTURE_SCREEN_CAPTURE = 16;
    /** A bright coloured fish */
    public static final int PICTURE_BRIGHT_COLOURED_FISH = 17;
    /** Illustration */
    public static final int PICTURE_ILLUSTRATION = 18;
    /** Band/artist logotype */
    public static final int PICTURE_ARTIST_LOGO = 19;
    /** Publisher/Studio logotype */
    public static final int PICTURE_STUDIO_LOGO = 20;

    // FRAME ///////////////////////////////////////////////////////////////////
    public static final int FRAME_SYNC = 0x3FFE;

    // FRAME SAMPLE RATES //////////////////////////////////////////////////////

    /** 0000 : get from STREAMINFO metadata block  */
    public static final int FRAME_SAMPLERATE_STREAMINFO = 0x0;
    /** 0001 : 88.2kHz  */
    public static final int FRAME_SAMPLERATE_88_2 = 0x1;
    /** 0010 : 176.4kHz  */
    public static final int FRAME_SAMPLERATE_176_4 = 0x2;
    /** 0011 : 192kHz  */
    public static final int FRAME_SAMPLERATE_192 = 0x3;
    /** 0100 : 8kHz  */
    public static final int FRAME_SAMPLERATE_8 = 0x4;
    /** 0101 : 16kHz  */
    public static final int FRAME_SAMPLERATE_16 = 0x5;
    /** 0110 : 22.05kHz  */
    public static final int FRAME_SAMPLERATE_22_05 = 0x6;
    /** 0111 : 24kHz  */
    public static final int FRAME_SAMPLERATE_24 = 0x7;
    /** 1000 : 32kHz  */
    public static final int FRAME_SAMPLERATE_32 = 0x8;
    /** 1001 : 44.1kHz  */
    public static final int FRAME_SAMPLERATE_44_1 = 0x9;
    /** 1010 : 48kHz  */
    public static final int FRAME_SAMPLERATE_48 = 0xA;
    /** 1011 : 96kHz  */
    public static final int FRAME_SAMPLERATE_96 = 0xB;
    /** 1100 : get 8 bit sample rate (in kHz) from end of header  */
    public static final int FRAME_SAMPLERATE_8BITS_KHZ = 0xC;
    /** 1101 : get 16 bit sample rate (in Hz) from end of header  */
    public static final int FRAME_SAMPLERATE_16BITS_HZ = 0xD;
    /** 1110 : get 16 bit sample rate (in tens of Hz) from end of header  */
    public static final int FRAME_SAMPLERATE_16BITS_10HZ = 0xE;
    /** 1111 : invalid, to prevent sync-fooling string of 1s */
    public static final int FRAME_SAMPLERATE_INVALID = 0xF;

    // FRAME CHANNELS //////////////////////////////////////////////////////////

    // FRAME SAMPLE SIZES //////////////////////////////////////////////////////

    /** 000 : get from STREAMINFO metadata block  */
    public static final int FRAME_SAMPLESIZE_STREAMINFO = 0x0;
    /** 001 : 8 bits per sample  */
    public static final int FRAME_SAMPLESIZE_8 = 0x1;
    /** 010 : 12 bits per sample  */
    public static final int FRAME_SAMPLESIZE_12 = 0x2;
    /** 011 : reserved  */
    public static final int FRAME_SAMPLESIZE_RESERVED1 = 0x3;
    /** 100 : 16 bits per sample  */
    public static final int FRAME_SAMPLESIZE_16 = 0x4;
    /** 101 : 20 bits per sample  */
    public static final int FRAME_SAMPLESIZE_20 = 0x5;
    /** 110 : 24 bits per sample  */
    public static final int FRAME_SAMPLESIZE_24 = 0x6;
    /** 111 : reserved */
    public static final int FRAME_SAMPLESIZE_RESERVED2 = 0x7;

    private FLACConstants() {
    }

}
