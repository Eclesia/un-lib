
package science.unlicense.format.flac.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class MetadataBlock {

    /**
     * 1bit : last metablock flag
     */
    public boolean isLast;
    /**
     * 7 bits : One of FLACConstants.BLOCK_TYPE_X
     */
    public int type;
    /**
     * Block size in bytes.
     */
    public int length;

    public void read(DataInputStream ds) throws IOException {
        ds.skipFully(length);
    }

}
