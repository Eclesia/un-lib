
package science.unlicense.format.flac;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaCapabilities;

/**
 * http://xiph.org/flac/format.html
 *
 * @author Johann Sorel
 */
public class FLACFormat extends AbstractMediaFormat{

    public static final FLACFormat INSTANCE = new FLACFormat();

    public FLACFormat() {
        super(new Chars("flac"));
        shortName = new Chars("FLAC");
        longName = new Chars("Free Lossless Audio Codec");
        mimeTypes.add(new Chars("audio/x-flac"));
        extensions.add(new Chars("flac"));
        resourceTypes.add(Media.class);
    }

    @Override
    public MediaCapabilities getCapabilities() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object input) {
        return new FLACMediaStore((Path) input);
    }

}
