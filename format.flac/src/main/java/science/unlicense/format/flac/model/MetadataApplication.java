
package science.unlicense.format.flac.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class MetadataApplication extends MetadataBlock{

    /**
     * 32bits : application id, see FLACConstants.APPLICATION_X
     */
    public int appId;
    /**
     * Application specification data.
     */
    public byte[] appData;

    public void read(DataInputStream ds) throws IOException {
        appId = ds.readInt();
        appData = ds.readFully(new byte[length-4]);
    }

}
