

package science.unlicense.format.flac;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.media.api.AudioTrack;
import science.unlicense.media.api.DefaultAudioTrack;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;
import science.unlicense.format.flac.model.MetadataBlock;
import science.unlicense.format.flac.model.MetadataStreamInfo;

/**
 * Flac file store.
 *
 * @author Johann Sorel
 */
public class FLACMediaStore extends AbstractStore implements Media {

    private Sequence frames;
    private Sequence blocks;
    private MetadataStreamInfo info;
    private int[] channels;


    public FLACMediaStore(Path input) {
        super(FLACFormat.INSTANCE,input);
    }

    private void analyze() throws IOException{
        if (frames!=null) return;

        //todo, we should split in 2 calls
        //only read the meta blocks and the frames in the media reader only.
        final FLACReader reader = new FLACReader();
        reader.setInput(source);
        reader.read();
        blocks = reader.getMetadataBlocks();
        frames = reader.getFrames();
    }

    @Override
    public Track[] getTracks() throws IOException {
        analyze();

        Track[] metas = new Track[0];
        for (int i=0;i<blocks.getSize();i++){
            final MetadataBlock block = (MetadataBlock) blocks.get(i);
            if (!(block instanceof MetadataStreamInfo)) continue;
            info = (MetadataStreamInfo) block;
            channels = new int[info.nbChannels+1];
            final int[] bitsPerSample = new int[info.nbChannels+1];
            Arrays.fill(bitsPerSample, info.bitsPerSample+1);

            //todo improve this
            if (channels.length==1){
                channels[0] = AudioTrack.CHANNEL_CENTER;
            } else if (channels.length==2){
                channels[0] = AudioTrack.CHANNEL_LEFT;
                channels[1] = AudioTrack.CHANNEL_RIGHT;
            }

            Track meta = new DefaultAudioTrack(Chars.EMPTY, channels, bitsPerSample, info.sampleRate);
            metas = new Track[]{meta};
        }

        return metas;
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        getTracks();
        return new FLACMediaReader(info, frames, channels);
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
