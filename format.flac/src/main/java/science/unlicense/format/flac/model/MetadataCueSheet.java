
package science.unlicense.format.flac.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class MetadataCueSheet extends MetadataBlock{

    /** 128*8 : media catalog number */
    public Chars catalogNumber;
    /** 64bits : number of lead-in samples */
    public long nbLeadInSamples;
    /**
     * 1bit :
     * 1 : compact disc cue sheet
     * 0 : else
     */
    public int isCompactDisc;
    /** tracks */
    public CueSheetTrack[] tracks;

    public void read(DataInputStream ds) throws IOException {
        catalogNumber = new Chars(ds.readFully(new byte[128]));
        nbLeadInSamples = ds.readLong();
        isCompactDisc = ds.readBits(1);
        //skip unused bits : 7 + 258*8
        ds.readBits(7);
        ds.skipFully(258);
        tracks = new CueSheetTrack[ds.readUByte()];
        for (int i=0;i<tracks.length;i++){
            tracks[i] = new CueSheetTrack();
            tracks[i].read(ds);
        }
    }

    public static final class CueSheetTrack{

        /** 64bits : track offset in samples */
        public long offset;
        /** 8bits : track number */
        public int trackNum;
        /** 12*8bits : ISRC alphanumeric code */
        public byte[] isrc;
        /**
         * 1bit : track type.
         * - 0 : audio
         * - 1 : non-audio
         */
        public int type;
        /** 1bit : pre-emphasis */
        public int preEmphasis;
        /** track indexes */
        public CueSheetTrackIndex[] indexes;

        public void read(DataInputStream ds) throws IOException{
            offset      = ds.readLong();
            trackNum    = ds.readUByte();
            isrc        = ds.readFully(new byte[12]);
            type        = ds.readBits(1);
            preEmphasis = ds.readBits(1);
            //skip unused bits : 6 + 13*8
            ds.readBits(6);
            ds.skipFully(13);
            indexes = new CueSheetTrackIndex[ds.readUByte()];
            for (int i=0;i<indexes.length;i++){
                indexes[i] = new CueSheetTrackIndex();
                indexes[i].read(ds);
            }
        }
    }

    public static final class CueSheetTrackIndex{
        /** 64bits : offset, in samples */
        public long offset;
        /** 8bits : index point number */
        public int indexPoint;

        public void read(DataInputStream ds) throws IOException{
            offset = ds.readLong();
            indexPoint = ds.readUByte();
            //3 unused bytes
            ds.skipFully(3);
        }
    }

}
