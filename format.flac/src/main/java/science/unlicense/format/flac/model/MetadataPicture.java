
package science.unlicense.format.flac.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class MetadataPicture extends MetadataBlock{

    /** 32bits : picture type */
    public int type;
    /** 32bits : mime type length in bytes */
    public int mimeLength;
    /** Mime type */
    public Chars mimeType;
    /** 32bits : description length in bytes */
    public int descLength;
    /** Description in UTF-8 */
    public Chars desc;
    /** 32bits : image width in pixel */
    public int width;
    /** 32bits : image height in pixel */
    public int height;
    /** 32bits : image color depth, bits per pixel */
    public int bitsPerPixel;
    /** 32bits : nb color for indexed image type, 0 = non indexed */
    public int nbColor;
    /** 32bits : image data length in bytes */
    public int imageLength;
    /** image data */
    public byte[] imageData;

    public void read(DataInputStream ds) throws IOException {
        type        = ds.readInt();
        mimeLength  = ds.readInt();
        mimeType    = new Chars(ds.readFully(new byte[mimeLength]));
        descLength  = ds.readInt();
        desc        = new Chars(ds.readFully(new byte[descLength]));
        width       = ds.readInt();
        height      = ds.readInt();
        bitsPerPixel= ds.readInt();
        nbColor     = ds.readInt();
        imageLength = ds.readInt();
        imageData   = ds.readFully(new byte[imageLength]);
    }

}
