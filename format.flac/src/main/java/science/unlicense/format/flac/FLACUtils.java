

package science.unlicense.format.flac;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public final class FLACUtils {

    private FLACUtils() {
    }

    public static int readUnary(DataInputStream ds, int endBitValue) throws IOException{
        int nb=0;
        while (ds.readBits(1)!=endBitValue){
            nb++;
        }
        return nb;
    }


}
