
package science.unlicense.code.java;

import org.junit.Test;
import science.unlicense.code.api.CodeFileReader;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.syntax.api.parser.SyntaxNode;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class JavaReaderTest {

    @Test
    public void testReadImports() throws IOException{

        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();

        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Imports.java")));

        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);

    }

    @Test
    public void testReadExpressions() throws IOException{

        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();

        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Expressions.java")));

        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);

    }

    @Test
    public void testReadBranching() throws IOException{

        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();

        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Branching.java")));

        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);

    }

    @Test
    public void testReadClasses() throws IOException{

        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();

        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Classes.java")));

        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);

    }

    @Test
    public void testReadEnum() throws IOException{

        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();

        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Enumeration.java")));

        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);

    }

    @Test
    public void testReadInterface() throws IOException{

        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();

        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Interface.java")));

        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);

    }

    @Test
    public void testReadGenerics() throws IOException{

        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();

        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Generics.java")));

        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);

    }

}
