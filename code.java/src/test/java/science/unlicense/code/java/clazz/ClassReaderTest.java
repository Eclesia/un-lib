
package science.unlicense.code.java.clazz;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class ClassReaderTest {

    @Test
    public void testRead() throws IOException{

        final ClassReader reader = new ClassReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/class/HelloWorld.class")));
        final ClassFile clazz = reader.read();

    }
}
