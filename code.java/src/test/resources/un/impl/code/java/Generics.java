
package un.language.java;

import java.time.chrono.Era;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


class Generics {

    void test() {

        List<?> lst1 = new ArrayList<Object>();
        List<? extends Object> lst2 = new ArrayList<java.lang.Object>();
        List<? super String> lst3 = new ArrayList<Object>();

        Map<?,?> map1 = new HashMap<Object, String>();
        Map<? extends Object,? super String> map2 = new HashMap<Object, Object>();

        Arrays.<Integer>asList((Integer) 0);

    }

    public static <S extends List & Runnable, E extends Runnable & List> long between(S startInclusive, E endExclusive) {
        return 0;
    }

}
