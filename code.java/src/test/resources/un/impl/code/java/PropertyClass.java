package un.test;

public class PropertyClass {

    /**
     * attribute 1
     */
    public science.unlicense.api.character.Chars att1;
    /**
     * attribute 2
     */
    protected science.unlicense.api.character.Chars att2;
    /**
     * attribute 3
     */
    science.unlicense.api.character.Chars att3;
    /**
     * attribute 4
     */
    private science.unlicense.api.character.Chars att4;

}
