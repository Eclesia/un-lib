/**
 * Some doc
 */
package un.language;

import java.util.concurrent.Callable;

public interface Interface extends Callable, Comparable {

    public static final int VAR_ONE     = 1;
    public static final double VAR_TWO  = 56.123;

    /**
     * Get the array.
     *
     * @return int[]
     */
    int[] getValues();

    /**
     * Awesome function doing things.
     *
     * @return double
     */
    double doThing();



}
