
# Java .java text grammar
# author : Johann Sorel
# converted from : https://docs.oracle.com/javase/specs/jls/se10/html/jls-19.html

# PREPROCESS token part
$D : [0-9] ;
# any none digit and non surrogate
$L : ([a-z]|[A-Z]|[_$]|[\u0101-\uFFFF]) ;
$S : (-|\+)? ;
$WS: ( |\t|\n|\r)+ ;

# TOKENS used in lexer

DEFAULT : {
    BYTE            : byte ;
    SHORT           : short ;
    INT             : int ;
    LONG            : long ;
    CHAR            : char ;
    FLOAT           : float ;
    DOUBLE          : double ;
    BOOLEAN         : boolean ;

    DOT             : \. ;
    AND1            : & ;
    AND2            : && ;
    LOWER           : < ;
    GREAT           : > ;
    COM             : , ;
    CHOICE          : ? ;
    NOT             : \! ;
    SEMI            : ; ;
    OR1             : \| ;
    OR2             : \|\| ;
    ANNOTE          : @ ;
    DDOTS           : : ;
    FCTREF          : :: ;
    MUL             : \* ;
    DIV             : \/ ;
    PLUS            : \+ ;
    MIN             : \- ;
    MOD             : % ;
    EQUAL           : = ;
    EQUALEQUAL      : == ;
    NOTEQUAL        : \!= ;
    POW             : ^ ;
    NEG             : \~ ;
    ANYNUM          : \.\.\. ;
    LAMBDA          : \-\> ;
    LSHIFT2         : << ;
    RSHIFT2         : >> ;
    RSHIFT3         : >>> ;
    INC             : \+\+ ;
    DEC             : \-\- ;

    MULEQUAL        : \*= ;
    DIVEQUAL        : \/= ;
    MODEQUAL        : %= ;
    PLUSEQUAL       : \+= ;
    MINEQUAL        : \-= ;
    LSHIFT2EQUAL    : <<= ;
    RSHIFT2EQUAL    : >>= ;
    RSHIFT3EQUAL    : >>>= ;
    ANDEQUAL        : \&= ;
    POWEQUAL        : ^= ;
    OREQUAL         : \|= ;
    LOWEREQUAL      : <= ;
    GREATEQUAL      : >= ;

    LP              : \( ;
    RP              : \) ;
    LA              : { ;
    RA              : } ;
    LB              : \[ ;
    RB              : \] ;


    ABSTRACT        : abstract ;
    ASSERT          : assert ;
    BREAK           : break ;
    CASE            : case ;
    CATCH           : catch ;
    CONTINUE        : continue ;
    CLASS           : class ;
    DEFAULT         : default ;
    DO              : do ;
    ELSE            : else ;
    ENUM            : enum ;
    EXTENDS         : extends ;
    EXPORTS         : exports ;
    FINAL           : final ;
    FINALLY         : finally ;
    FOR             : for ;
    IF              : if ;
    IMPLEMENTS      : implements ;
    IMPORT          : import ;
    INSTANCEOF      : instanceof ;
    INTERFACE       : interface ;
    MODULE          : module ;
    NATIVE          : native ;
    NEW             : new ;
    OPEN            : open ;
    OPENS           : opens ;
    PACKAGE         : package ;
    PRIVATE         : private ;
    PROTECTED       : protected ;
    PROVIDES        : provides ;
    PUBLIC          : public ;
    RETURN          : return ;
    REQUIRES        : requires ;
    STATIC          : static ;
    STRICTFP        : strictfp ;
    SUPER           : super ;
    SWITCH          : switch ;
    SYNCHRONIZED    : synchronized ;
    THIS            : this ;
    THROW           : throw ;
    THROWS          : throws ;
    TRANSIENT       : transient ;
    TRANSITIVE      : transitive ;
    TO              : to ;
    TRY             : try ;
    USES            : uses ;
    VAR             : var ;
    VOID            : void ;
    VOLATILE        : volatile ;
    WHILE           : while ;
    WITH            : with ;

    FALSE        : false ;
    NULL         : null ;
    TRUE         : true ;

    # not usable
    CONST        : const ;
    GOTO         : goto ;

    WS              : $WS ;
    COMMENT1        : //[^\n]* ;
    COMMENT2        : '/*'([^\*]|(\*[^/]))*'*/' ;
    WORD            : $L($L|$D)* ;
    NUMBER          : (($D+(\.($D)*)?)|(\.($D)+))((e|E)$S$D+)?(l|f|d|L|F|D)? ;
    STRING          : \"([^\\\"]|(\\.))*\" ;
    CHARVAL         : \'([^\\\']|(\\.)|(\\u([a-z]|[A-Z]|[0-9])+))\' ;
    HEXA            : 0x([a-f]|[A-F]|[0-9])*(l|f|d|L|F|D)? ;
}


# RULES used in parser


################################################################################
#
################################################################################

numberliteral : NUMBER | HEXA ;

booleanliteral : TRUE | FALSE ;

characterliteral : CHARVAL ;

stringliteral : STRING ;

nullliteral : NULL ;


################################################################################
# productions from §3 (lexical structure)
################################################################################

identifier : identifierchars ;

identifierchars : WORD ;

typeidentifier : identifier ;

literal : numberliteral
        | booleanliteral
        | characterliteral
        | stringliteral
        | nullliteral
        ;

################################################################################
# productions from §4 (types, values, and variables)
################################################################################

type : primitivetype
     | referencetype
     ;

primitivetype : (annotation* numerictype)
              | (annotation* BOOLEAN)
              ;

numerictype : integraltype
            | floatingpointtype
            ;

integraltype : BYTE
             | SHORT
             | INT
             | LONG
             | CHAR
             ;

floatingpointtype : FLOAT
                  | DOUBLE
                  ;

referencetype : classorinterfacetype
              | typevariable
              | arraytype
              ;

classorinterfacetype : classtype
                     | interfacetype
                     ;

classtype : (annotation* typeidentifier typearguments?)
          | (packagename DOT annotation* typeidentifier typearguments?)
          | (classorinterfacetype DOT annotation* typeidentifier typearguments?)
          ;

interfacetype : classtype ;

typevariable : annotation* typeidentifier ;

arraytype : primitivetype dims
          | classorinterfacetype dims
          | typevariable dims
          ;

dims : annotation* RB LB (annotation* RB LB)* ;

typeparameter : typeparametermodifier* typeidentifier typebound? ;

typeparametermodifier : annotation ;

typebound : (EXTENDS typevariable)
          | (EXTENDS classorinterfacetype additionalbound*)
          ;

additionalbound : AND1 interfacetype ;

typearguments : LOWER typeargumentlist GREAT ;

typeargumentlist : typeargument (COM typeargument)* ;

typeargument : referencetype
             | wildcard
             ;

wildcard : annotation* CHOICE wildcardbounds? ;

wildcardbounds : (EXTENDS referencetype)
               | (SUPER referencetype)
               ;

################################################################################
# productions from §6 (names)
################################################################################

modulename : identifier
           | (modulename DOT identifier)
           ;

packagename : identifier
            | (packagename DOT identifier)
            ;

typename : typeidentifier
         | (packageortypename DOT typeidentifier)
         ;

expressionname : identifier
               | (ambiguousname DOT identifier)
               ;

methodname : identifier ;

packageortypename : identifier
                  | (packageortypename DOT identifier)
                  ;

ambiguousname : identifier
              | (ambiguousname DOT identifier)
              ;

################################################################################
# productions from §7 (packages and modules)
################################################################################

compilationunit : ordinarycompilationunit
                | modularcompilationunit
                ;

ordinarycompilationunit : packagedeclaration? importdeclaration* typedeclaration* ;

modularcompilationunit : importdeclaration* moduledeclaration ;

packagedeclaration : packagemodifier* PACKAGE identifier (DOT identifier)* ;

packagemodifier : annotation ;

importdeclaration : singletypeimportdeclaration
                  | typeimportondemanddeclaration
                  | singlestaticimportdeclaration
                  | staticimportondemanddeclaration
                  ;

singletypeimportdeclaration : IMPORT typename SEMI ;

typeimportondemanddeclaration : IMPORT packageortypename DOT MUL SEMI ;

singlestaticimportdeclaration : IMPORT STATIC typename DOT identifier SEMI ;

staticimportondemanddeclaration : IMPORT STATIC typename DOT MUL SEMI ;

typedeclaration : classdeclaration
                | interfacedeclaration
                ;

moduledeclaration : annotation* OPEN? MODULE identifier (DOT identifier)* LA moduledirective* RA ;

moduledirective : (REQUIRES requiresmodifier* modulename SEMI)
                | (EXPORTS packagename (TO modulename (COM modulename)*)? SEMI)
                | (OPENS packagename (TO modulename (COM modulename)*)? SEMI)
                | (USES typename SEMI)
                | (PROVIDES typename WITH typename (COM typename)* SEMI)
                ;

requiresmodifier : TRANSITIVE
                 | STATIC
                 ;

################################################################################
# productions from §8 (classes)
################################################################################

classdeclaration : normalclassdeclaration
                 | enumdeclaration
                 ;

normalclassdeclaration : classmodifier* CLASS typeidentifier typeparameters? superclass? superinterfaces? classbody ;

classmodifier : annotation
              | PUBLIC
              | PROTECTED
              | PRIVATE
              | ABSTRACT
              | STATIC
              | FINAL
              | STRICTFP
              ;

typeparameters : LOWER typeparameterlist GREAT ;

typeparameterlist : typeparameter (COM typeparameter)* ;

superclass : EXTENDS classtype ;

superinterfaces : IMPLEMENTS interfacetypelist ;

interfacetypelist : interfacetype (COM interfacetype)* ;

classbody : LA (classbodydeclaration)* RA ;

classbodydeclaration : classmemberdeclaration
                     | instanceinitializer
                     | staticinitializer
                     | constructordeclaration
                     ;

classmemberdeclaration : fielddeclaration
                       | methoddeclaration
                       | classdeclaration
                       | interfacedeclaration
                       | SEMI
                       ;

fielddeclaration : fieldmodifier* unanntype variabledeclaratorlist SEMI ;

fieldmodifier : annotation
              | PUBLIC
              | PROTECTED
              | PRIVATE
              | STATIC
              | FINAL
              | TRANSIENT
              | VOLATILE
              ;

variabledeclaratorlist : variabledeclarator (COM variabledeclarator)* ;

variabledeclarator : variabledeclaratorid (EQUAL variableinitializer)? ;

variabledeclaratorid : identifier dims? ;

variableinitializer : expression
                    | arrayinitializer
                    ;

unanntype : unannprimitivetype
          | unannreferencetype
          ;

unannprimitivetype : numerictype
                   | BOOLEAN
                   ;

unannreferencetype : unannclassorinterfacetype
                   | unanntypevariable
                   | unannarraytype
                   ;

unannclassorinterfacetype : unannclasstype
                          | unanninterfacetype
                          ;

unannclasstype : (typeidentifier typearguments?)
               | (packagename DOT annotation* typeidentifier typearguments?)
               | (unannclassorinterfacetype DOT annotation* typeidentifier typearguments?)
               ;

unanninterfacetype : unannclasstype ;

unanntypevariable : typeidentifier ;

unannarraytype : (unannprimitivetype dims)
               | (unannclassorinterfacetype dims)
               | (unanntypevariable dims)
               ;

methoddeclaration : methodmodifier* methodheader methodbody ;

methodmodifier : annotation
               | PUBLIC
               | PROTECTED
               | PRIVATE
               | ABSTRACT
               | STATIC
               | FINAL
               | SYNCHRONIZED
               | NATIVE
               | STRICTFP
               ;

methodheader : (result methoddeclarator throws?)
             | (typeparameters annotation* result methoddeclarator throws?)
             ;

result : unanntype
       | VOID
       ;

methoddeclarator : identifier LP formalparameterlist? RP dims? ;

formalparameterlist : (receiverparameter)
                    | (formalparameters COM lastformalparameter)
                    | (lastformalparameter)
                    ;

formalparameters : (formalparameter (COM formalparameter)*)
                 | (receiverparameter (COM formalparameter)*)
                 ;

formalparameter : variablemodifier* unanntype variabledeclaratorid ;

variablemodifier : annotation
                 | FINAL
                 ;

lastformalparameter : (variablemodifier* unanntype annotation* ANYNUM variabledeclaratorid)
                    | formalparameter
                    ;

receiverparameter : annotation* unanntype (identifier DOT)? THIS ;

throws : THROWS exceptiontypelist ;

exceptiontypelist : exceptiontype (COM exceptiontype)* ;

exceptiontype : classtype
              | typevariable
              ;

methodbody : block SEMI ;

instanceinitializer : block ;

staticinitializer : STATIC block ;

constructordeclaration : constructormodifier* constructordeclarator throws? constructorbody ;

constructormodifier : annotation
                    | PUBLIC
                    | PROTECTED
                    | PRIVATE
                    ;

constructordeclarator : typeparameters? simpletypename LP formalparameterlist? RP ;

simpletypename : typeidentifier ;

constructorbody : LA explicitconstructorinvocation? blockstatements? RA ;

explicitconstructorinvocation : (typearguments? THIS LP argumentlist? RP SEMI)
                              | (typearguments? SUPER LP argumentlist? RP SEMI)
                              | (expressionname DOT typearguments? SUPER LP argumentlist? RP SEMI)
                              | (primary DOT typearguments? SUPER LP argumentlist? RP SEMI)
                              ;

enumdeclaration : classmodifier* ENUM typeidentifier superinterfaces? enumbody ;

enumbody : LA enumconstantlist? COM? enumbodydeclarations? RA ;

enumconstantlist : enumconstant (COM enumconstant)* ;

enumconstant : enumconstantmodifier* identifier (LP argumentlist? RP)? classbody? ;

enumconstantmodifier : annotation ;

enumbodydeclarations : SEMI classbodydeclaration* ;

################################################################################
# productions from §9 (interfaces)
################################################################################

interfacedeclaration : normalinterfacedeclaration
                     | annotationtypedeclaration
                     ;

normalinterfacedeclaration : interfacemodifier* INTERFACE typeidentifier typeparameters? extendsinterfaces? interfacebody ;

interfacemodifier : annotation
                  | PUBLIC
                  | PROTECTED
                  | PRIVATE
                  | ABSTRACT
                  | STATIC
                  | STRICTFP
                  ;

extendsinterfaces : EXTENDS interfacetypelist ;

interfacebody : LA interfacememberdeclaration* RA ;

interfacememberdeclaration : constantdeclaration
                           | interfacemethoddeclaration
                           | classdeclaration
                           | interfacedeclaration
                           | SEMI
                           ;

constantdeclaration : constantmodifier* unanntype variabledeclaratorlist SEMI ;

constantmodifier : annotation
                 | PUBLIC
                 | STATIC
                 | FINAL
                 ;

interfacemethoddeclaration : interfacemethodmodifier* methodheader methodbody ;

interfacemethodmodifier : annotation
                        | PUBLIC
                        | PRIVATE
                        | ABSTRACT
                        | DEFAULT
                        | STATIC
                        | STRICTFP
                        ;

annotationtypedeclaration : interfacemodifier* ANNOTE INTERFACE typeidentifier annotationtypebody ;

annotationtypebody : LA annotationtypememberdeclaration* RA ;

annotationtypememberdeclaration : annotationtypeelementdeclaration
                                | constantdeclaration
                                | classdeclaration
                                | interfacedeclaration
                                | SEMI
                                ;

annotationtypeelementdeclaration : annotationtypeelementmodifier* unanntype identifier LP RP dims? defaultvalue? ;

annotationtypeelementmodifier : annotation
                              | PUBLIC
                              | ABSTRACT
                              ;

defaultvalue : DEFAULT elementvalue ;

annotation : normalannotation
           | markerannotation
           | singleelementannotation
           ;

normalannotation : ANNOTE typename LP elementvaluepairlist? RP ;

elementvaluepairlist : elementvaluepair (COM elementvaluepair)* ;

elementvaluepair : identifier EQUAL elementvalue ;

elementvalue : conditionalexpression
             | elementvaluearrayinitializer
             | annotation
             ;

elementvaluearrayinitializer : LA elementvaluelist? COM? RA ;

elementvaluelist : elementvalue (COM elementvalue)* ;

markerannotation : ANNOTE typename ;

singleelementannotation : ANNOTE typename LP elementvalue RP ;


################################################################################
# productions from §10 (arrays)
################################################################################

arrayinitializer : LA variableinitializerlist? COM? RA ;

variableinitializerlist : variableinitializer (COM variableinitializer)* ;

################################################################################
# productions from §14 (blocks and statements)
################################################################################

block : LA blockstatements? RA ;

blockstatements : blockstatement blockstatement* ;

blockstatement : localvariabledeclarationstatement
               | classdeclaration
               | statement
               ;

localvariabledeclarationstatement : localvariabledeclaration SEMI ;

localvariabledeclaration : variablemodifier* localvariabletype variabledeclaratorlist ;

localvariabletype : unanntype
                  | VAR
                  ;

statement : statementwithouttrailingsubstatement
          | labeledstatement
          | ifthenstatement
          | ifthenelsestatement
          | whilestatement
          | forstatement
          ;

statementnoshortif : statementwithouttrailingsubstatement
                   | labeledstatementnoshortif
                   | ifthenelsestatementnoshortif
                   | whilestatementnoshortif
                   | forstatementnoshortif
                   ;

statementwithouttrailingsubstatement : block
                                     | emptystatement
                                     | expressionstatement
                                     | assertstatement
                                     | switchstatement
                                     | dostatement
                                     | breakstatement
                                     | continuestatement
                                     | returnstatement
                                     | synchronizedstatement
                                     | throwstatement
                                     | trystatement
                                     ;

emptystatement : SEMI ;

labeledstatement : identifier DDOTS statement ;

labeledstatementnoshortif : identifier DDOTS statementnoshortif ;

expressionstatement : statementexpression SEMI ;

statementexpression : assignment
                    | preincrementexpression
                    | predecrementexpression
                    | postincrementexpression
                    | postdecrementexpression
                    | methodinvocation
                    | classinstancecreationexpression
                    ;

ifthenstatement : IF LP expression RP statement ;

ifthenelsestatement : IF LP expression RP statementnoshortif ELSE statement ;

ifthenelsestatementnoshortif : IF LP expression RP statementnoshortif ELSE statementnoshortif ;

assertstatement : (ASSERT expression SEMI)
                | (ASSERT expression DDOTS expression SEMI)
                ;

switchstatement : SWITCH LP expression RP switchblock ;

switchblock : LA switchblockstatementgroup* switchlabel* RA ;

switchblockstatementgroup : switchlabels blockstatements ;

switchlabels : switchlabel switchlabel* ;

switchlabel : (CASE constantexpression DDOTS)
            | (CASE enumconstantname DDOTS)
            | (DEFAULT DDOTS)
            ;

enumconstantname : identifier ;

whilestatement : WHILE LP expression RP statement ;

whilestatementnoshortif : WHILE LP expression RP statementnoshortif ;

dostatement : DO statement WHILE LP expression RP SEMI ;

forstatement : basicforstatement
             | enhancedforstatement
             ;

forstatementnoshortif : basicforstatementnoshortif
                      | enhancedforstatementnoshortif
                      ;

basicforstatement : FOR LP forinit? SEMI expression? SEMI forupdate? RP statement ;

basicforstatementnoshortif : FOR LP forinit? SEMI expression? SEMI forupdate? RP statementnoshortif ;

forinit : statementexpressionlist
        | localvariabledeclaration
        ;

forupdate : statementexpressionlist ;

statementexpressionlist : statementexpression (COM statementexpression)* ;

enhancedforstatement : FOR LP variablemodifier? localvariabletype variabledeclaratorid DDOTS expression RP statement ;

enhancedforstatementnoshortif : FOR LP variablemodifier? localvariabletype variabledeclaratorid DDOTS expression RP statementnoshortif ;

breakstatement : BREAK identifier? SEMI ;

continuestatement : CONTINUE identifier? SEMI ;

returnstatement : RETURN expression? SEMI ;

throwstatement : THROW expression SEMI ;

synchronizedstatement : SYNCHRONIZED LP expression RP block ;

trystatement : TRY block catches
             | TRY block catches? finally
             | trywithresourcesstatement
             ;

catches : catchclause catchclause* ;

catchclause : CATCH LP catchformalparameter RP block ;

catchformalparameter : variablemodifier* catchtype variabledeclaratorid ;

catchtype : unannclasstype (OR1 classtype)* ;

finally : FINALLY block ;

trywithresourcesstatement : TRY resourcespecification block catches? finally? ;

resourcespecification : LP resourcelist SEMI? RP ;

resourcelist : resource (SEMI resource)* ;

resource : (variablemodifier* localvariabletype identifier EQUAL expression)
         | (variableaccess)
         ;

variableaccess : expressionname
               | fieldaccess
               ;

################################################################################
# productions from §15 (expressions)
################################################################################

primary : primarynonewarray
        | arraycreationexpression
        ;

primarynonewarray : literal
                  | classliteral
                  | THIS
                  | (typename DOT THIS)
                  | (LP expression RP)
                  | classinstancecreationexpression
                  | fieldaccess
                  | arrayaccess
                  | methodinvocation
                  | methodreference
                  ;

classliteral : typename (LB RB)* DOT CLASS
             | numerictype (LB RB)* DOT CLASS
             | BOOLEAN (LB RB)* DOT CLASS
             | VOID DOT CLASS
             ;

classinstancecreationexpression : (unqualifiedclassinstancecreationexpression)
                                | (expressionname DOT unqualifiedclassinstancecreationexpression)
                                | (primary DOT unqualifiedclassinstancecreationexpression)
                                ;

unqualifiedclassinstancecreationexpression : NEW typearguments? classorinterfacetypetoinstantiate LP argumentlist? RP classbody? ;

classorinterfacetypetoinstantiate : annotation* identifier (DOT annotation* identifier)* typeargumentsordiamond? ;

typeargumentsordiamond : typearguments
                       | (LOWER GREAT)
                       ;

fieldaccess : (primary DOT identifier)
            | (SUPER DOT identifier)
            | (typename DOT SUPER DOT identifier)
            ;

arrayaccess : (expressionname LB expression RB )
            | (primarynonewarray LB expression RB )
            ;

methodinvocation : (methodname LP argumentlist? RP)
                 | (typename DOT typearguments? identifier LP argumentlist? RP)
                 | (expressionname DOT typearguments? identifier LP argumentlist? RP)
                 | (primary DOT typearguments? identifier LP argumentlist? RP)
                 | (SUPER DOT typearguments? identifier LP argumentlist? RP)
                 | (typename DOT SUPER DOT typearguments? identifier LP argumentlist? RP)
                 ;

argumentlist : expression (COM expression)* ;

methodreference : (expressionname FCTREF typearguments? identifier)
                | (primary FCTREF typearguments? identifier)
                | (referencetype FCTREF typearguments? identifier)
                | (SUPER FCTREF typearguments? identifier)
                | (typename DOT SUPER FCTREF typearguments? identifier)
                | (classtype FCTREF typearguments? NEW)
                | (arraytype FCTREF NEW)
                ;

arraycreationexpression : (NEW primitivetype dimexprs dims?)
                        | (NEW classorinterfacetype dimexprs dims?)
                        | (NEW primitivetype dims arrayinitializer)
                        | (NEW classorinterfacetype dims arrayinitializer)
                        ;

dimexprs : dimexpr dimexpr* ;

dimexpr : annotation* LB expression RB ;

expression : lambdaexpression
           | assignmentexpression
           ;

lambdaexpression : lambdaparameters LAMBDA lambdabody ;

lambdaparameters : identifier
                 | (LP formalparameterlist? RP)
                 | (LP inferredformalparameterlist RP)
                 ;

inferredformalparameterlist : identifier (COM identifier)* ;

lambdabody : expression
           | block
           ;

assignmentexpression : conditionalexpression
                     | assignment
                     ;

assignment : lefthandside assignmentoperator expression ;

lefthandside : expressionname
             | fieldaccess
             | arrayaccess
             ;

assignmentoperator : EQUAL
                   | MULEQUAL
                   | DIVEQUAL
                   | MODEQUAL
                   | PLUSEQUAL
                   | MINEQUAL
                   | LSHIFT2EQUAL
                   | RSHIFT2EQUAL
                   | RSHIFT3EQUAL
                   | ANDEQUAL
                   | POWEQUAL
                   | OREQUAL
                   ;

conditionalexpression : (conditionalorexpression)
                      | (conditionalorexpression CHOICE expression DDOTS conditionalexpression)
                      | (conditionalorexpression CHOICE expression DDOTS lambdaexpression)
                      ;

conditionalorexpression : conditionalandexpression
                        | (conditionalorexpression OR2 conditionalandexpression)
                        ;

conditionalandexpression : inclusiveorexpression
                         | (conditionalandexpression AND2 inclusiveorexpression)
                         ;

inclusiveorexpression : exclusiveorexpression
                      | (inclusiveorexpression OR1 exclusiveorexpression)
                      ;

exclusiveorexpression : andexpression
                      | (exclusiveorexpression POW andexpression)
                      ;

andexpression : equalityexpression
              | (andexpression AND1 equalityexpression)
              ;

equalityexpression : relationalexpression
                   | equalityexpression EQUALEQUAL relationalexpression
                   | equalityexpression NOTEQUAL relationalexpression
                   ;

relationalexpression : shiftexpression
                     | (relationalexpression LOWER shiftexpression)
                     | (relationalexpression GREAT shiftexpression)
                     | (relationalexpression LOWEREQUAL shiftexpression)
                     | (relationalexpression GREATEQUAL shiftexpression)
                     | (relationalexpression INSTANCEOF referencetype)
                     ;

shiftexpression : additiveexpression
                | (shiftexpression LSHIFT2 additiveexpression)
                | (shiftexpression RSHIFT2 additiveexpression)
                | (shiftexpression RSHIFT3 additiveexpression)
                ;

additiveexpression : multiplicativeexpression
                   | (additiveexpression PLUS multiplicativeexpression)
                   | (additiveexpression MIN multiplicativeexpression)
                   ;

multiplicativeexpression : unaryexpression
                         | (multiplicativeexpression MUL unaryexpression)
                         | (multiplicativeexpression DIV unaryexpression)
                         | (multiplicativeexpression MOD unaryexpression)
                         ;

unaryexpression : preincrementexpression
                | predecrementexpression
                | (PLUS unaryexpression)
                | (MIN unaryexpression)
                | unaryexpressionnotplusminus
                ;

preincrementexpression : INC unaryexpression ;

predecrementexpression : DEC unaryexpression ;

unaryexpressionnotplusminus : postfixexpression
                            | (NEG unaryexpression)
                            | (NOT unaryexpression)
                            | castexpression
                            ;

postfixexpression : primary
                  | expressionname
                  | postincrementexpression
                  | postdecrementexpression
                  ;

postincrementexpression : postfixexpression INC ;

postdecrementexpression : postfixexpression DEC ;

castexpression : (LP primitivetype RP unaryexpression)
               | (LP referencetype additionalbound* RP unaryexpressionnotplusminus)
               | (LP referencetype additionalbound* RP lambdaexpression)
               ;

constantexpression : expression ;