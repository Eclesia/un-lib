
package science.unlicense.code.java.model;

import science.unlicense.code.api.meta.DefaultMeta;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class JavaException extends DefaultMeta{

    public static final Chars ID = Chars.constant("java-exception");

    private JavaClass exceptionClass;

    public JavaException() {
        super(ID);
    }

    public JavaException(JavaClass exceptionClass) {
        super(ID);
        this.exceptionClass = exceptionClass;
    }

    public JavaClass getExceptionClass() {
        return exceptionClass;
    }

    public void setExceptionClass(JavaClass exceptionClass) {
        this.exceptionClass = exceptionClass;
    }



}
