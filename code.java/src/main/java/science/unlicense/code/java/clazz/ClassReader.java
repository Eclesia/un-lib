
package science.unlicense.code.java.clazz;

import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class ClassReader extends AbstractReader {

    public ClassFile read() throws IOException{
        final ClassFile file = new ClassFile();
        file.read(getInputAsDataStream(Endianness.BIG_ENDIAN));
        return file;
    }

}
