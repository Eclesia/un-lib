
package science.unlicense.code.java.clazz;

import science.unlicense.code.api.CodeFileReader;
import science.unlicense.code.api.CodeFormat;
import science.unlicense.code.api.CodeProducer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public class ClassFormat extends DefaultFormat implements CodeFormat {

    public static final ClassFormat INSTANCE = new ClassFormat();

    private ClassFormat() {
        super(new Chars("class"));
        shortName = new Chars("class");
        longName = new Chars("Java class file");
        extensions.add(new Chars("class"));
    }

    @Override
    public CodeFileReader createReader() {
        throw new UnimplementedException("not supported yet.");
    }

    @Override
    public CodeProducer createProducer() {
        throw new UnimplementedException("not supported yet.");
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
