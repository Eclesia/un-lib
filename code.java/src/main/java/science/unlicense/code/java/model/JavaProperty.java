
package science.unlicense.code.java.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.code.api.Class;
import science.unlicense.code.api.Property;

/**
 *
 * @author Johann Sorel
 */
public class JavaProperty extends Property {

    public JavaProperty() {
    }

    public JavaProperty(JavaProperty prop) {
        this.id = prop.id;
        this.metas.addAll(prop.metas);
        this.objclass = prop.objclass;
        this.primclass = prop.primclass;
    }

    public JavaProperty(Chars name, Chars description, science.unlicense.code.api.Class objclass, java.lang.Class primclass, int scope) {
        super(name, objclass, primclass);
        metas.add(new JavaDocumentation(description));
        metas.add(new JavaScope(scope));
    }

    public JavaProperty(Chars name, Chars description, science.unlicense.code.api.Class objclass, java.lang.Class primclass, JavaScope scope) {
        super(name, objclass, primclass);
        metas.add(new JavaDocumentation(description));
        metas.add(scope);
    }

}
