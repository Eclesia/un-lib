
package science.unlicense.code.java.model;

import science.unlicense.code.api.Function;
import science.unlicense.common.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class JavaFunction extends Function {

    public Sequence getExceptions() {
        return metas.getAll(JavaException.ID);
    }

}
