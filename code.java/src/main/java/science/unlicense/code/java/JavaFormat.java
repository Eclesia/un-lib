
package science.unlicense.code.java;

import science.unlicense.code.api.CodeFileReader;
import science.unlicense.code.api.CodeFormat;
import science.unlicense.code.api.CodeProducer;
import science.unlicense.code.impl.GrammarCodeFileReader;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public class JavaFormat extends DefaultFormat implements CodeFormat {

    public static final JavaFormat INSTANCE = new JavaFormat();

    private JavaFormat() {
        super(new Chars("java"));
        shortName = new Chars("Java");
        longName = new Chars("Java source file");
        extensions.add(new Chars("java"));
    }

    @Override
    public CodeFileReader createReader() {
        return new GrammarCodeFileReader(JavaConstants.RULE_FILE);
    }

    @Override
    public CodeProducer createProducer() {
        return new JavaProducer();
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
