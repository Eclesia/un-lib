
package science.unlicense.code.java.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.code.api.meta.DefaultAbstract;
import science.unlicense.code.api.meta.DefaultMeta;
import science.unlicense.code.api.meta.Meta;

/**
 *
 * @author Johann Sorel
 */
public final class JavaMetas {

    public static final Meta NATIVE         = new DefaultMeta(new Chars("java-native"));
    public static final Meta SYNCHRONIZED   = new DefaultMeta(new Chars("java-synchronized"));
    public static final Meta TRANSIENT      = new DefaultMeta(new Chars("java-transsient"));
    public static final Meta STATIC         = new DefaultMeta(new Chars("java-static"));
    public static final Meta FINAL          = new DefaultMeta(new Chars("java-final"));
    public static final Meta VOLATILE       = new DefaultMeta(new Chars("java-volatile"));
    public static final Meta ABSTRACT       = new DefaultAbstract();

    public static final Meta INTERFACE      = new DefaultMeta(new Chars("java-interface"));
    public static final Meta ENUM           = new DefaultMeta(new Chars("java-enum"));

    private JavaMetas() {}

}
