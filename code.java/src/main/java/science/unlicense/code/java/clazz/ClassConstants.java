
package science.unlicense.code.java.clazz;

/**
 *
 * @author Johann Sorel
 */
public final class ClassConstants {

    public static final int SIGNATURE = 0xCAFEBABE;

    public static final int CONSTANT_CLASS              = 7;
    public static final int CONSTANT_FIELDREF           = 9;
    public static final int CONSTANT_METHODREF          = 10;
    public static final int CONSTANT_INTERFACEMETHODREF = 11;
    public static final int CONSTANT_STRING             = 8;
    public static final int CONSTANT_INTEGER            = 3;
    public static final int CONSTANT_FLOAT              = 4;
    public static final int CONSTANT_LONG               = 5;
    public static final int CONSTANT_DOUBLE             = 6;
    public static final int CONSTANT_NAMEANDTYPE        = 12;
    public static final int CONSTANT_UTF8               = 1;
    public static final int CONSTANT_METHODHANDLE       = 15;
    public static final int CONSTANT_METHODTYPE         = 16;
    public static final int CONSTANT_INVOKEDYNAMIC      = 18;

    // Spec : Table 4.4 Field access and property flags
    public static final int ACC_PUBLIC      = 0x0001;
    public static final int ACC_PRIVATE     = 0x0002;
    public static final int ACC_PROTECTED   = 0x0004;
    public static final int ACC_STATIC      = 0x0008;
    public static final int ACC_FINAL       = 0x0010;
    public static final int ACC_VOLATILE    = 0x0040;
    public static final int ACC_TRANSIENT   = 0x0080;
    public static final int ACC_SYNTHETIC   = 0x1000;
    public static final int ACC_ENUM        = 0x4000;

    private ClassConstants(){}

}
