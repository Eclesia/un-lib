

package science.unlicense.protocol.imap;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class IMAPConstants {

    /** Used to delimite message and commands parts */
    public static final Chars CRLF = Chars.constant(new byte[]{0x0D,0x0A});
    /** Used to indicate continuation */
    public static final Chars CONTINUATION = Chars.constant(new byte[]{'+'});
    /** Used to indicate untagged response */
    public static final Chars UNTAGGED = Chars.constant(new byte[]{'*'});

    //use as message markers and delimiters
    public static final Chars BRACKET_OPEN = Chars.constant(new byte[]{'{'});
    public static final Chars BRACKET_CLOSE = Chars.constant(new byte[]{'}'});
    public static final Chars QUOTE = Chars.constant(new byte[]{'"'});
    public static final Chars LIST_OPEN = Chars.constant(new byte[]{'('});
    public static final Chars LIST_CLOSE = Chars.constant(new byte[]{')'});
    public static final Chars NIL = Chars.constant("NIL");
    public static final Chars WILDCARD1 = Chars.constant(new byte[]{'%'});
    public static final Chars WILDCARD2 = Chars.constant(new byte[]{'*'});
    public static final Chars AND = Chars.constant(new byte[]{'&'});
    public static final Chars DIESE = Chars.constant(new byte[]{'#'});

    /** Command success */
    public static final Chars RESPONSE_OK = Chars.constant("OK");
    /** Command failure */
    public static final Chars RESPONSE_NO = Chars.constant("NO");
    /** Command error, syntax or parameters */
    public static final Chars RESPONSE_BAD = Chars.constant("BAD");
    /** */
    public static final Chars RESPONSE_PREAUTH = Chars.constant("PREAUTH");
    /** */
    public static final Chars RESPONSE_BYE = Chars.constant("BYE");

    /** Prefix for system flags */
    public static final Chars SYSTEM_FLAG = Chars.constant("\\");
    /** Message flag : seen */
    public static final Chars FLAG_SEEN = Chars.constant("\\Seen");
    /** Message flag : answered */
    public static final Chars FLAG_ANSWERED = Chars.constant("\\Answered");
    /** Message flag : flagged : means urgent or special */
    public static final Chars FLAG_FLAGGED = Chars.constant("\\Flagged");
    /** Message flag : deleted, ready for remove by an expunge command */
    public static final Chars FLAG_DELETED = Chars.constant("\\Deleted");
    /** Message flag : draft : uncomplete, not send yet */
    public static final Chars FLAG_DRAFT = Chars.constant("\\Draft");
    /** Message flag : recent */
    public static final Chars FLAG_RECENT = Chars.constant("\\Recent");


    public static final Chars COMMAND_CAPABILITY    = Chars.constant("CAPABILITY");
    public static final Chars COMMAND_NOOP          = Chars.constant("NOOP");
    public static final Chars COMMAND_LOGOUT        = Chars.constant("LOGOUT");

    public static final Chars COMMAND_STARTTLS      = Chars.constant("STARTTLS");
    public static final Chars COMMAND_AUTHENTICATE  = Chars.constant("AUTHENTICATE");
    public static final Chars COMMAND_LOGIN         = Chars.constant("LOGIN");

    public static final Chars COMMAND_SELECT        = Chars.constant("SELECT");
    public static final Chars COMMAND_EXAMINE       = Chars.constant("EXAMINE");
    public static final Chars COMMAND_CREATE        = Chars.constant("CREATE");
    public static final Chars COMMAND_DELETE        = Chars.constant("DELETE");
    public static final Chars COMMAND_RENAME        = Chars.constant("RENAME");
    public static final Chars COMMAND_SUBSCRIBE     = Chars.constant("SUBSCRIBE");
    public static final Chars COMMAND_UNSUBSCRIBE   = Chars.constant("UNSUBSCRIBE");
    public static final Chars COMMAND_LIST          = Chars.constant("LIST");
    public static final Chars COMMAND_LSUB          = Chars.constant("LSUB");
    public static final Chars COMMAND_STATUS        = Chars.constant("STATUS");
    public static final Chars COMMAND_APPEND        = Chars.constant("APPEND");

    public static final Chars COMMAND_CHECK         = Chars.constant("CHECK");
    public static final Chars COMMAND_CLOSE         = Chars.constant("CLOSE");
    public static final Chars COMMAND_EXPUNGE       = Chars.constant("EXPUNGE");
    public static final Chars COMMAND_SEARCH        = Chars.constant("SEARCH");
    public static final Chars COMMAND_FETCH         = Chars.constant("FETCH");
    public static final Chars COMMAND_STORE         = Chars.constant("STORE");
    public static final Chars COMMAND_COPY          = Chars.constant("COPY");
    public static final Chars COMMAND_UID           = Chars.constant("UID");

    private IMAPConstants(){}

}
