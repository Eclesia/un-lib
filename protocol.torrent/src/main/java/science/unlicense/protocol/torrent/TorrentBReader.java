

package science.unlicense.protocol.torrent;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Int32;
import science.unlicense.common.api.number.Int64;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Torrent file reader.
 *
 * Specification :
 * http://www.bittorrent.org/beps/bep_0003.html
 * https://wiki.theory.org/BitTorrentSpecification
 *
 * @author Johann Sorel
 */
public class TorrentBReader extends AbstractReader{


    public Dictionary read() throws IOException {

        final BacktrackInputStream bs = getInputAsBacktrackStream();
        bs.mark();
        final CharInputStream cs = new CharInputStream(bs, CharEncodings.UTF_8);

        //we expect a dictionary
        Object result = read(cs.readChar().toUnicode(), cs);

        if (result instanceof Dictionary){
            return (Dictionary) result;
        } else {
            throw new IOException("Root file element is not a dictionnary");
        }
    }

    private Object read(int su, CharInputStream cs) throws IOException {
        if (su == TorrentConstants.BENC_DICO_START){
            // key,value pairs
            final Dictionary dico = new HashDictionary();
            su = cs.readChar().toUnicode();
            while (su != TorrentConstants.BENC_DICO_END){
                dico.add(read(su, cs), read(cs.readChar().toUnicode(), cs));
                try{
                    su = cs.readChar().toUnicode();
                }catch(Exception ex){
                    ex.printStackTrace();
                    return dico;
                }
            }
            return dico;
        } else if (su == TorrentConstants.BENC_LIST_START){
            //list of elements
            final Sequence lst = new ArraySequence();
            su = cs.readChar().toUnicode();
            while (su != TorrentConstants.BENC_LIST_END){
                lst.add(read(su, cs));
                su = cs.readChar().toUnicode();
            }
            return lst;
        } else if (su == TorrentConstants.BENC_INTEGER_START){
            //integer value
            final CharBuffer cb = new CharBuffer(CharEncodings.UTF_8);
            su = cs.readChar().toUnicode();
            while (su != TorrentConstants.BENC_INTEGER_END){
                cb.append(su);
                su = cs.readChar().toUnicode();
            }
            return Int64.decode(cb.toChars());
        } else if (Char.isDigit(su)){
            //character string
            final CharBuffer cb = new CharBuffer(CharEncodings.UTF_8);
            cb.append(su);
            su = cs.readChar().toUnicode();
            while (su != TorrentConstants.BENC_STRING_SEPARATOR){
                cb.append(su);
                su = cs.readChar().toUnicode();
            }
            final int length = Int32.decode(cb.toChars());

            final DataInputStream ds = new DataInputStream(cs);
            final Chars str = new Chars(ds.readFully(new byte[length]),CharEncodings.UTF_8);
            return str;
        } else {
            throw new IOException("Unexpected unicode : "+ su);
        }

    }

}
