
package science.unlicense.format.ttf.ttf.glyph;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 * TTF compound glyph.
 *
 * @author Johann Sorel
 */
public class CompoundGlyph extends Glyph {
    /** uint16 : Component flag*/
    public int flags;
    /** uint16 : Glyph index of component*/
    public int glyphIndex;
    /** int16, uint16, int8 or uint8 : X-offset for component or point number;
     * type depends on bits 0 and 1 in component flags*/
    public int argument1;
    /** int16, uint16, int8 or uint8 : Y-offset for component or point number
     * type depends on bits 0 and 1 in component flags*/
    public int argument2;
    /** transformation option One of the transformation options from Table 19*/
    public Matrix3x3 transformation;

    @Override
    public PlanarGeometry getGeometry() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
