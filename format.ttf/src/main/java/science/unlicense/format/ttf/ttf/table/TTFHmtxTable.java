
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'hmtx' table contains metric information for the horizontal
 * layout each of the glyphs in the font.
 *
 * @author Johann Sorel
 */
public class TTFHmtxTable extends TTFTable{

    public static class LongHorMetric{
        /** uint16 */
        public final int advanceWidth;
        /** int16*/
        public final short leftSideBearing;

        public LongHorMetric(int advanceWidth, short leftSideBearing) {
            this.advanceWidth = advanceWidth;
            this.leftSideBearing = leftSideBearing;
        }
    }

    /**
     * The value numOfLongHorMetrics comes from the 'hhea' table.
     * If the font is monospaced, only one entry need be in the array but that entry is required.
     */
    public LongHorMetric[] hMetrics;
    /**
     * FWord table :
     * Here the advanceWidth is assumed to be the same as the advanceWidth
     * for the last entry above. The number of entries in this array is derived
     * from the total number of glyphs minus numOfLongHorMetrics. This generally
     * is used with a run of monospaced glyphs (e.g. Kanji fonts or Courier fonts).
     * Only one run is allowed and it must be at the end.
     */
    public short[] leftSideBearing;

    public TTFHmtxTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_HMTX);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
        final TTFHheaTable hhead = (TTFHheaTable) font.getTable(TTFConstants.TAG_HHEA);
        final TTFMaxPTable maxp = (TTFMaxPTable) font.getTable(TTFConstants.TAG_MAXP);
        hMetrics = new LongHorMetric[hhead.numOfLongHorMetrics];
        for (int i=0;i<hMetrics.length;i++){
            hMetrics[i] = new LongHorMetric(ds.readUShort(),ds.readShort());
        }
        leftSideBearing = new short[maxp.numGlyphs-hhead.numOfLongHorMetrics];
        for (int i=0;i<leftSideBearing.length;i++){
            leftSideBearing[i] = ds.readShort();
        }
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        sb.append("-hMetrics=[...]:").append(hMetrics.length).append('\n');
        sb.append("-leftSideBearing=[...]:").append(leftSideBearing.length).append('\n');
        return sb.toChars();
    }

}
