
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'OS/2' table consists of a set of metrics that are required
 * by OS/2 and Windows. It is not used by the Mac OS.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFOs2Table extends TTFTable{

    public TTFOs2Table(TrueTypeFont font) {
        super(font,TTFConstants.TAG_OS2);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
