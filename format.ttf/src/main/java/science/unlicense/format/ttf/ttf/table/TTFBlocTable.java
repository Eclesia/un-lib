
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The bitmap location table(tag name: 'bloc') provides information about
 * the availability of bitmaps at requested point sizes.
 * If a bitmap is included in the font, it also tells where the data is located
 * in the bitmap data table. The bitmap location and bitmap data tables support various formats.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFBlocTable extends TTFTable{

    public TTFBlocTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_BLOC);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
