
package science.unlicense.format.ttf.otf;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class OTFConstants {

    /** PostScript font program (compact font format) */
    public static final Chars TAG_CFF = Chars.constant(new byte[]{'C','F','F',' '});
    /** Vertical Origin */
    public static final Chars TAG_VORG = Chars.constant(new byte[]{'V','O','R','G'});
    /** Embedded bitmap data */
    public static final Chars TAG_EBDT = Chars.constant(new byte[]{'E','B','D','T'});
    /** Embedded bitmap location data */
    public static final Chars TAG_EBLC = Chars.constant(new byte[]{'E','B','L','C'});
    /** Embedded bitmap scaling data */
    public static final Chars TAG_EBSC = Chars.constant(new byte[]{'E','B','S','C'});
    /** Baseline data */
    public static final Chars TAG_BASE = Chars.constant(new byte[]{'B','A','S','E'});
    /** Glyph definition data */
    public static final Chars TAG_GDEF = Chars.constant(new byte[]{'G','D','E','F'});
    /** Glyph positioning data */
    public static final Chars TAG_GPOS = Chars.constant(new byte[]{'G','P','O','S'});
    /** Glyph substitution data */
    public static final Chars TAG_GSUB = Chars.constant(new byte[]{'G','S','U','B'});
    /** Justification data */
    public static final Chars TAG_JSTF = Chars.constant(new byte[]{'J','S','T','F'});
    /** Digital signature */
    public static final Chars TAG_DSIG = Chars.constant(new byte[]{'D','S','I','G'});
    /** Linear threshold data */
    public static final Chars TAG_LTSH = Chars.constant(new byte[]{'L','T','S','H'});
    /** PCL 5 data */
    public static final Chars TAG_PCLT = Chars.constant(new byte[]{'P','C','L','T'});
    /** Vertical device metrics */
    public static final Chars TAG_VDMX = Chars.constant(new byte[]{'V','D','M','X'});

    private OTFConstants(){}

}
