
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'loca' table stores the offsets to the locations of the glyphs
 * in the font relative to the beginning of the 'glyf' table.
 *
 * @author Johann Sorel
 */
public class TTFLocaTable extends TTFTable{

    public long[] offsets;

    public TTFLocaTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_LOCA);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
        final TTFHeadTable head = (TTFHeadTable) font.getTable(TTFConstants.TAG_HEAD);
        final TTFMaxPTable maxp = (TTFMaxPTable) font.getTable(TTFConstants.TAG_MAXP);
        offsets = new long[maxp.numGlyphs];
        final int type = head.indexToLocFormat;
        if (type == 0){ //short type
            for (int i=0;i<offsets.length;i++){
                offsets[i] = ds.readUShort()*2;
            }
        } else if (type == 1){ //long type
            for (int i=0;i<offsets.length;i++){
                offsets[i] = ds.readUInt();
            }
        } else {
            throw new IOException("Unknowned index storage type : "+type);
        }
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        sb.append("-offsets=[...]:").append(offsets.length).append('\n');
        return sb.toChars();
    }

}
