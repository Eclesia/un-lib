
package science.unlicense.format.ttf.otf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.otf.OTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * OTF Specification :
 * Linear threshold data
 *
 * http://www.microsoft.com/typography/otspec/ltsh.htm
 *
 * @author Johann Sorel
 */
public class OTFLTSHTable extends TTFTable{

    public OTFLTSHTable(TrueTypeFont font) {
        super(font,OTFConstants.TAG_LTSH);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
