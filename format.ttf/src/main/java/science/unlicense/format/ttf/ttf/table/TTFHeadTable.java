
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'head' table contains global information about the font.
 *
 * @author Johann Sorel
 */
public class TTFHeadTable extends TTFTable{

    /** Fixed (1.0) */
    public int version = 0x00010000;
    /** int : set by font manufacturer */
    public int fontRevision;
    /** uint32 : To compute: set it to 0, calculate the checksum for the
     * 'head' table and put it in the table directory, sum the entire
     * font as uint32, then store B1B0AFBA - sum. The checksum for the
     * 'head' table will not be wrong. That is OK.*/
    public int checkSumAdjustment;
    /** uint32 : set to 0x5F0F3CF5*/
    public int magicNumber = 0x5F0F3CF5;
    /** uint16 :
        bit 0 - y value of 0 specifies baseline
        bit 1 - x position of left most black bit is LSB
        bit 2 - scaled point size and actual point size will differ (i.e. 24 point glyph differs from 12 point glyph scaled by factor of 2)
        bit 3 - use integer scaling instead of fractional
        bit 4 - (used by the Microsoft implementation of the TrueType scaler)
        bit 5 - This bit should be set in fonts that are intended to e laid out vertically, and in which the glyphs have been drawn such that an x-coordinate of 0 corresponds to the desired vertical baseline.
        bit 6 - This bit must be set to zero.
        bit 7 - This bit should be set if the font requires layout for correct linguistic rendering (e.g. Arabic fonts).
        bit 8 - This bit should be set for a GX font which has one or more metamorphosis effects designated as happening by default.
        bit 9 - This bit should be set if the font contains any strong right-to-left glyphs.
        bit 10 - This bit should be set if the font contains Indic-style rearrangement effects.
        bits 11-12 - Defined by Adobe.
        */
    public int flags;
    /** uint16 : range from 64 to 16384*/
    public int unitsPerEm;
    /** longDateTime : international date*/
    public long created;
    /** longDateTime : international date*/
    public long modified;
    /** FWord : for all glyph bounding boxes */
    public short xMin;
    /** FWord : for all glyph bounding boxes */
    public short yMin;
    /** FWord : for all glyph bounding boxes */
    public short xMax;
    /** FWord : for all glyph bounding boxes */
    public short yMax;
    /** uint16 :
    bit 0 bold
    bit 1 italic
    bit 2 underline
    bit 3 outline
    bit 4 shadow
    bit 5 condensed (narrow)
    bit 6 extended
    */
    public int macStyle;
    /** uint16 : smallest readable size in pixels */
    public int lowestRecPPEM;
    /** uint16 :
     0 Mixed directional glyphs
     1 Only strongly left to right glyphs
     2 Like 1 but also contains neutrals
    -1 Only strongly right to left glyphs
    -2 Like -1 but also contains neutrals
    */
    public int fontDirectionHint;
    /** uint16 : 0 for short offsets, 1 for long */
    public int indexToLocFormat;
    /** uint16 : 0 for current format */
    public int glyphDataFormat;

    public TTFHeadTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_HEAD);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
        version             = ds.readInt();
        fontRevision        = ds.readInt();
        checkSumAdjustment  = ds.readInt();
        magicNumber         = ds.readInt();
        flags               = ds.readUShort();
        unitsPerEm          = ds.readUShort();
        created             = ds.readLong();
        modified            = ds.readLong();
        xMin                = ds.readShort();
        yMin                = ds.readShort();
        xMax                = ds.readShort();
        yMax                = ds.readShort();
        macStyle            = ds.readUShort();
        lowestRecPPEM       = ds.readUShort();
        fontDirectionHint   = ds.readUShort();
        indexToLocFormat    = ds.readUShort();
        glyphDataFormat     = ds.readUShort();
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        sb.append("-version=")               .append(version).append('\n');
        sb.append("-fontRevision=")          .append(fontRevision).append('\n');
        sb.append("-checkSumAdjustment=")    .append(checkSumAdjustment).append('\n');
        sb.append("-magicNumber=")           .append(magicNumber).append('\n');
        sb.append("-flags=")                 .append(flags).append('\n');
        sb.append("-unitsPerEm=")            .append(unitsPerEm).append('\n');
        sb.append("-created=")               .appendNumber(created).append('\n');
        sb.append("-modified=")              .appendNumber(modified).append('\n');
        sb.append("-xMin=")                  .append(xMin).append('\n');
        sb.append("-yMin=")                  .append(yMin).append('\n');
        sb.append("-xMax=")                  .append(xMax).append('\n');
        sb.append("-yMax=")                  .append(yMax).append('\n');
        sb.append("-macStyle=")              .append(macStyle).append('\n');
        sb.append("-lowestRecPPEM=")         .append(lowestRecPPEM).append('\n');
        sb.append("-fontDirectionHint=")     .append(fontDirectionHint).append('\n');
        sb.append("-indexToLocFormat=")      .append(indexToLocFormat).append('\n');
        sb.append("-glyphDataFormat=")       .append(glyphDataFormat).append('\n');
        return sb.toChars();
    }

}
