
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * Theglyph metamorphosis table(tag name: 'mort') allows you to specify
 * a set of transformations that can apply to the glyphs of your font.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFMortTable extends TTFTable{

    public TTFMortTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_MORT);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
