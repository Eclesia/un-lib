
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The CVT variations table (tag name: 'cvar') allows you to include all
 * of the data required for stylizing the CVT to match the styling done to outlines.
 * This table mirrors the glyph variation table in that it contains the changes needed
 * to create a style for any location in the font's style space.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFCvarTable extends TTFTable{

    public TTFCvarTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_CVAR);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
