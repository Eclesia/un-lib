
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The bitmap data table (tag name: 'bdat') is a very simple structure;
 * a version number followed by data. The data can be in various formats.
 * Some of the formats contain metric information and other formats contain only the image.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFBdatTable extends TTFTable{

    public TTFBdatTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_BDAT);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
