
package science.unlicense.format.ttf.ttf;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.IntSet;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.format.ttf.ttf.glyph.Glyph;
import science.unlicense.format.ttf.ttf.glyph.SimpleGlyph;
import science.unlicense.format.ttf.ttf.table.TTFCmapTable;
import science.unlicense.format.ttf.ttf.table.TTFGlyfTable;
import science.unlicense.format.ttf.ttf.table.TTFLocaTable;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 * A TrueTypeFont.
 * specification : https://developer.apple.com/fonts/TTRefMan/
 *
 * @author Johann Sorel
 */
public class TrueTypeFont extends CObject {

    /** storage location */
    public Object path;

    public int scalerType;
    public int searchRange;
    public int entrySelector;
    public int rangeShift;

    public final Sequence tables = new ArraySequence();

    public PlanarGeometry getGlyph(int cp) {
        try {
            int glyphIndex = findGlyphIndex(cp);
            if (glyphIndex<0){
                //no glyph
                return null;
            }
            final TTFLocaTable loca = (TTFLocaTable) getTable(TTFConstants.TAG_LOCA);
            final TTFGlyfTable glyf = (TTFGlyfTable) getTable(TTFConstants.TAG_GLYF);

            final long offset = loca.offsets[glyphIndex];
            final Glyph g = glyf.readGlyph((int) offset);

            if (g instanceof SimpleGlyph){
                return ((SimpleGlyph) g).getGeometry();
            }
        } catch (IOException ex) {
            Loggers.get().log(ex,Logger.LEVEL_WARNING);
        }

        return null;
    }

    public DataInputStream openNewStream() throws IOException{
        final DataInputStream ds = new DataInputStream(IOUtilities.toInputStream(path, new boolean[1]));
        return ds;
    }

    public TTFTable getTable(Chars name) throws IOException{
        for (int i=0,n=tables.getSize();i<n;i++){
            final TTFTable table = (TTFTable) tables.get(i);
            if (table.name.equals(name)){
                table.load();
                return table;
            }
        }
        return null;
    }

    public int findGlyphIndex(int unicode_codepoint) throws IOException{
        final TTFCmapTable cmap = (TTFCmapTable) getTable(TTFConstants.TAG_CMAP);

        for (TTFCmapTable.Table table : cmap.tables){
            if (table instanceof TTFCmapTable.Format0){
                final TTFCmapTable.Format0 f = (TTFCmapTable.Format0) table;
                if (unicode_codepoint < f.length-6){
                    return f.glyphIndexArray[unicode_codepoint];
                }
            } else if (table instanceof TTFCmapTable.Format4){
                final TTFCmapTable.Format4 f = (TTFCmapTable.Format4) table;
                int segIndex = -1;

                //search first endcode greater or equal to codepoint
                for (int i=0;i<f.endCode.length;i++){
                    if (f.endCode[i]>=unicode_codepoint){
                        //check start code
                        if (f.startCode[i]<=unicode_codepoint){
                            segIndex=i;
                        }
                        break;
                    }
                }

                if (segIndex>=0){
                    if (f.idRangeOffset[segIndex]==0){
                        //If the idRangeOffset is 0, the idDelta value is added
                        //directly to the character code to get the corresponding glyph index
                        //NOTE: All idDelta[i] arithmetic is modulo 65536.
                        return (f.idDelta[segIndex] + unicode_codepoint)%65536;
                    } else {
                        //If the idRangeOffset value for the segment is not 0,
                        //the mapping of the character codes relies on the glyphIndexArray.
                        //The character code offset from startCode is added to the idRangeOffset value.
                        //This sum is used as an offset from the current location within idRangeOffset itself
                        //to index out the correct glyphIdArray value. This indexing method works because
                        //glyphIdArray immediately follows idRangeOffset in the font file.
                        return f.glyphIndexes[segIndex][unicode_codepoint-f.startCode[segIndex]];
                    }

                } else {
                    //character not found
                    return -1;
                }
            }
        }

        return -1;
     }

    public IntSet listCharacters() throws IOException  {
        final TTFCmapTable cmap = (TTFCmapTable) getTable(TTFConstants.TAG_CMAP);

        final IntSet res = new IntSet();

        for (TTFCmapTable.Table table : cmap.tables){
            if (table instanceof TTFCmapTable.Format0){
                final TTFCmapTable.Format0 f = (TTFCmapTable.Format0) table;
                for (int unicode : f.glyphIndexArray){
                    res.add(unicode);
                }
            } else if (table instanceof TTFCmapTable.Format4){
                final TTFCmapTable.Format4 f = (TTFCmapTable.Format4) table;
                int segIndex = -1;

                //search first endcode greater or equal to codepoint
                for (int i=0;i<f.endCode.length;i++){
                    for (int k=f.startCode[i];k<f.endCode[i];k++){
                        res.add(k);
                    }
                }
            }
        }

        return res;
    }


    public Chars toChars() {
        final CharBuffer sb = new CharBuffer();
        sb.append("TrueTypeFont:\n");
        for (int i=0,n=tables.getSize();i<n;i++){
            final TTFTable table = (TTFTable) tables.get(i);
            try {
                table.load();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            sb.append(table.toString());
            sb.append('\n');
        }
        return sb.toChars();
    }

}
