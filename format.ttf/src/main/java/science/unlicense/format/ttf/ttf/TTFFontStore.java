
package science.unlicense.format.ttf.ttf;

import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.display.api.font.AbstractFontStore;
import science.unlicense.display.api.font.Font;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.concurrent.api.Paths;

/**
 * TTF font store.
 *
 * @author Johann Sorel
 */
public class TTFFontStore extends AbstractFontStore {

    public static final TTFFontStore INSTANCE = new TTFFontStore();

    private final Dictionary dico = new HashDictionary();

    public TTFFontStore() {

    }

    private synchronized void init(){
        if (dico.getSize()==0) {
            try {
                registerFont(Paths.resolve(new Chars("mod:/module-res/Tuffy.ttf")));
                registerFont(Paths.resolve(new Chars("mod:/module-res/Mona.ttf")));
                registerFont(Paths.resolve(new Chars("mod:/module-res/Unicon.ttf")));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public Set getFamilies() {
        init();
        final Set s = new HashSet();
        s.addAll(dico.getKeys());
        return s;
    }

    public Font getFont(Chars family) {
        init();
        return (TTFFont) dico.getValue(family);
    }

    public FontMetadata getMetadata(Chars family) {
        init();
        final TTFFont ffi = (TTFFont) dico.getValue(family);
        if (ffi==null) throw new InvalidArgumentException("No font for family name : "+family);
        return ffi.getMetaData();
    }

    @Override
    public void registerFont(Path path) throws IOException {
        final Chars name = Paths.stripExtension(path.getName());
        dico.add(name, new TTFFont(name,path));
    }

}
