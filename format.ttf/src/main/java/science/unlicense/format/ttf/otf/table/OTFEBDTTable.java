
package science.unlicense.format.ttf.otf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.otf.OTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * OTF Specification :
 * Embedded Bitmap Data Table
 *
 * http://www.microsoft.com/typography/otspec/ebdt.htm
 *
 * @author Johann Sorel
 */
public class OTFEBDTTable extends TTFTable{

    public OTFEBDTTable(TrueTypeFont font) {
        super(font,OTFConstants.TAG_EBDT);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
