
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'fpgm' table is optional. It is needed by fonts that are instructed.
 * Like the 'cvt ', it is an ordered list which is stored as an array.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFFpgmTable extends TTFTable{

    public TTFFpgmTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_FPGM);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
