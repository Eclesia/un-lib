
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The font descriptors table (tag name: 'fdsc') allows applications to
 * take an existing run of text and allow the user to specify a new font
 * family for that run of text. A new style run that best preserves the font
 * style information using the new font family will be created by using
 * the style information that you provide.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFFdscTable extends TTFTable{

    public TTFFdscTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_FDSC);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
