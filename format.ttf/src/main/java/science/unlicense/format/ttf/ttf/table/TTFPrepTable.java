
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'prep' table stores the instructions that make up the control value program,
 * a set of TrueType instructions that will be executed once when the font
 * is first accessed and again whenever the font, point size or transformation matrix change
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFPrepTable extends TTFTable{

    public TTFPrepTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_PREP);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
