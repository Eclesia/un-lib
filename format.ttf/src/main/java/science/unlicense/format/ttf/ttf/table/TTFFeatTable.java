
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * Thefeature name table(tag name: 'feat') allows you to include the
 * font's text features, the settings for each text feature, and the
 * name table indices for common (human-readable) names for the features and settings.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFFeatTable extends TTFTable{

    public TTFFeatTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_FEAT);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
