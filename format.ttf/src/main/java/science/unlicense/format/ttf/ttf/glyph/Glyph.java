
package science.unlicense.format.ttf.ttf.glyph;

import science.unlicense.geometry.api.PlanarGeometry;

/**
 * TTF Glyph.
 *
 * @author Johann Sorel
 */
public abstract class Glyph {
    /** int16 :
     * If the number of contours is positive or zero, it is a single glyph;
     * If the number of contours is -1, the glyph is compound
     */
    public short numberOfContours;
    /** FWord : Minimum x for coordinate data */
    public short xMin;
    /** FWord : Minimum y for coordinate data */
    public short yMin;
    /** FWord : Maximum x for coordinate data */
    public short xMax;
    /** FWord : Maximum y for coordinate data */
    public short yMax;

    public abstract PlanarGeometry getGeometry();

}
