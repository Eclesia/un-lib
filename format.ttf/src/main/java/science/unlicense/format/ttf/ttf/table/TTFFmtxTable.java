
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The Font Metrics Table (tag: 'fmtx') identifies a glyph whose points
 * represent various font-wide metrics: ascent, descent, caret angle, caret offset.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFFmtxTable extends TTFTable{

    public TTFFmtxTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_FMTX);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
