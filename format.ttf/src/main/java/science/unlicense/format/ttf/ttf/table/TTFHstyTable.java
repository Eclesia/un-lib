
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'hsty' table provides information about how to synthesize styles
 * such as boldfacing and italics on the Newton OS.
 * It is used to mimic the way in which PostScript will modify fonts when the requested style is not directly available.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFHstyTable extends TTFTable{

    public TTFHstyTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_HSTY);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
