
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * Apple Advanced Typography variations allow the font designer to build
 * high quality styles into the typeface itself.
 * This reduces the dependence on algorithmic styling in the graphics system.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFFvarTable extends TTFTable{

    public TTFFvarTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_FVAR);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
