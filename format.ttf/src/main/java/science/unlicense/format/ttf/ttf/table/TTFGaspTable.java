
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * This table contains information which describes the preferred rasterization
 * techniques for the typeface when it is rendered on grayscale-capable devices.
 * This table also has some use for monochrome devices, which may use the table to turn
 * off hinting at very large or small sizes, to improve performance.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFGaspTable extends TTFTable{

    public TTFGaspTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_GASP);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
