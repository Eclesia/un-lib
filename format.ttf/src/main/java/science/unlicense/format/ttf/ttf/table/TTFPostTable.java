
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'post' table contains information needed to use a TrueType font
 * on a PostScript printer. It contains the data needed for the FontInfo dictionary
 * entry as well as the PostScript names for all of the glyphs in the font.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFPostTable extends TTFTable{

    public TTFPostTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_POST);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
