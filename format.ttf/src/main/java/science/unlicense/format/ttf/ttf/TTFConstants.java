
package science.unlicense.format.ttf.ttf;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class TTFConstants {

    /** accent attachment table */
    public static final Chars TAG_ACNT = Chars.constant(new byte[]{'a','c','n','t'});
    /** axis variation table */
    public static final Chars TAG_AVAR = Chars.constant(new byte[]{'a','v','a','r'});
    /** bitmap data table */
    public static final Chars TAG_BDAT = Chars.constant(new byte[]{'b','d','a','t'});
    /** bitmap font header table */
    public static final Chars TAG_BHED = Chars.constant(new byte[]{'b','h','e','d'});
    /** bitmap location table */
    public static final Chars TAG_BLOC = Chars.constant(new byte[]{'b','l','o','c'});
    /** baseline table */
    public static final Chars TAG_BSLN = Chars.constant(new byte[]{'b','s','l','n'});
    /** character code mapping table */
    public static final Chars TAG_CMAP = Chars.constant(new byte[]{'c','m','a','p'});
    /** CVT variation table */
    public static final Chars TAG_CVAR = Chars.constant(new byte[]{'c','v','a','r'});
    /** control value table */
    public static final Chars TAG_CVT = Chars.constant(new byte[]{'c','v','t',' '});
    /** font descriptor table  */
    public static final Chars TAG_FDSC = Chars.constant(new byte[]{'f','d','s','c'});
    /** layout feature table */
    public static final Chars TAG_FEAT = Chars.constant(new byte[]{'f','e','a','t'});
    /** font metrics table  */
    public static final Chars TAG_FMTX = Chars.constant(new byte[]{'f','m','t','x'});
    /** font program table */
    public static final Chars TAG_FPGM = Chars.constant(new byte[]{'f','p','g','m'});
    /** font variation table */
    public static final Chars TAG_FVAR = Chars.constant(new byte[]{'f','v','a','r'});
    /** grid-fitting and scan-conversion procedure table */
    public static final Chars TAG_GASP = Chars.constant(new byte[]{'g','a','s','p'});
    /** glyph outline table */
    public static final Chars TAG_GLYF = Chars.constant(new byte[]{'g','l','y','f'});
    /** glyph variation table */
    public static final Chars TAG_GVAR = Chars.constant(new byte[]{'g','v','a','r'});
    /** horizontal device metrics table */
    public static final Chars TAG_HDMX = Chars.constant(new byte[]{'h','d','m','x'});
    /** font header table */
    public static final Chars TAG_HEAD = Chars.constant(new byte[]{'h','e','a','d'});
    /** horizontal header table */
    public static final Chars TAG_HHEA = Chars.constant(new byte[]{'h','h','e','a'});
    /** horizontal metrics table */
    public static final Chars TAG_HMTX = Chars.constant(new byte[]{'h','m','t','x'});
    /** horizontal style table */
    public static final Chars TAG_HSTY = Chars.constant(new byte[]{'h','s','t','y'});
    /** justification table */
    public static final Chars TAG_JUST = Chars.constant(new byte[]{'j','u','s','t'});
    /** kerning table */
    public static final Chars TAG_KERN = Chars.constant(new byte[]{'k','e','r','n'});
    /** ligature caret table */
    public static final Chars TAG_LCAR = Chars.constant(new byte[]{'l','c','a','r'});
    /** glyph location table */
    public static final Chars TAG_LOCA = Chars.constant(new byte[]{'l','o','c','a'});
    /** maximum profile table */
    public static final Chars TAG_MAXP = Chars.constant(new byte[]{'m','a','x','p'});
    /** metamorphosis table */
    public static final Chars TAG_MORT = Chars.constant(new byte[]{'m','o','r','t'});
    /** extended metamorphosis table */
    public static final Chars TAG_MORX = Chars.constant(new byte[]{'m','o','r','x'});
    /** name table */
    public static final Chars TAG_NAME = Chars.constant(new byte[]{'n','a','m','e'});
    /** optical bounds table */
    public static final Chars TAG_OPBD = Chars.constant(new byte[]{'o','p','b','d'});
    /** compatibility table */
    public static final Chars TAG_OS2 = Chars.constant(new byte[]{'O','S','/','2'});
    /** glyph name and PostScript compatibility table */
    public static final Chars TAG_POST = Chars.constant(new byte[]{'p','o','s','t'});
    /** control value program table */
    public static final Chars TAG_PREP = Chars.constant(new byte[]{'p','r','e','p'});
    /** properties table */
    public static final Chars TAG_PROP = Chars.constant(new byte[]{'p','r','o','p'});
    /** tracking table */
    public static final Chars TAG_TRAK = Chars.constant(new byte[]{'t','r','a','k'});
    /** vertical header table */
    public static final Chars TAG_VHEA = Chars.constant(new byte[]{'v','h','e','a'});
    /** vertical metrics table */
    public static final Chars TAG_VMTX = Chars.constant(new byte[]{'v','m','t','x'});
    /** glyph reference table*/
    public static final Chars TAG_ZAPF = Chars.constant(new byte[]{'Z','a','p','f'});


    ////////////////////////////////////////////////////////////////////////////
    //// MASKS FOR SYMPLE GLYPH FLAGS //////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * On Curve
     * If set, the point is on the curve;
     * Otherwise, it is off the curve.
     */
    public static final byte SG_ONCURVE = 0x01;
    /**
     * x-Short Vector
     * If set, the corresponding x-coordinate is 1 byte long;
     * Otherwise, the corresponding x-coordinate is 2 bytes long
     */
    public static final byte SG_XSV = 0x02;
    /**
     * y-Short Vector
     * If set, the corresponding y-coordinate is 1 byte long;
     * Otherwise, the corresponding y-coordinate is 2 bytes long
     */
    public static final byte SG_YSV = 0x04;
    /**
     * Repeat
     * If set, the next byte specifies the number of additional times
     * this set of flags is to be repeated. In this way, the number of flags
     * listed can be smaller than the number of points in a character.
     */
    public static final byte SG_REPEAT = 0x08;
    /**
     * This x is same (Positive x-Short vector)
     * This flag has one of two meanings, depending on how the x-Short Vector flag is set.
     * If the x-Short Vector bit is set, this bit describes the sign of the value, with a value of 1 equalling positive and a zero value negative.
     * If the x-short Vector bit is not set, and this bit is set, then the current x-coordinate is the same as the previous x-coordinate.
     * If the x-short Vector bit is not set, and this bit is not set, the current x-coordinate is a signed 16-bit delta vector. In this case, the delta vector is the change in x
     */
    public static final byte SG_XMSK = 0x10;
    /**
     * This y is same (Positive y-Short vector)
     * This flag has one of two meanings, depending on how the y-Short Vector flag is set.
     * If the y-Short Vector bit is set, this bit describes the sign of the value, with a value of 1 equalling positive and a zero value negative.
     * If the y-short Vector bit is not set, and this bit is set, then the current y-coordinate is the same as the previous y-coordinate.
     * If the y-short Vector bit is not set, and this bit is not set, the current y-coordinate is a signed 16-bit delta vector. In this case, the delta vector is the change in y
     */
    public static final byte SG_YMSK = 0x20;

    private TTFConstants(){}

}
