
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The vertical headertable (tag name: 'vhea') contains information needed
 * for vertical fonts. The glyphs of vertical fonts are written either top to bottom or bottom to top.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFVheaTable extends TTFTable{

    public TTFVheaTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_VHEA);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
