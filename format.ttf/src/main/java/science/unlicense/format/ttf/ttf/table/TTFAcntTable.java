
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The accent attachment table (tag name: 'acnt') provides a space-efficient
 * method of combining component glyphs into compound glyphs to form accents.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFAcntTable extends TTFTable{

    public TTFAcntTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_ACNT);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
