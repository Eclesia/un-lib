
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.format.ttf.ttf.glyph.SimpleGlyph;
import science.unlicense.format.ttf.ttf.glyph.Glyph;
import science.unlicense.format.ttf.ttf.glyph.CompoundGlyph;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'glyf' table contains the data that defines the appearance of the glyphs in the font.
 *
 * @author Johann Sorel
 */
public class TTFGlyfTable extends TTFTable{

    /**
     * Cache glyphs.
     */
    public final Dictionary glyphs = new HashDictionary();

    public TTFGlyfTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_GLYF);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
        //reading if donne later.
    }

    public Glyph readGlyph(int glyphOffset) throws IOException {
        //search the cache
        Glyph glyph = (Glyph) glyphs.getValue(glyphOffset);
        if (glyph!=null) return glyph;

        //move to glyph position
        final DataInputStream ds = font.openNewStream();
        try{
            ds.skipFully(this.offset+glyphOffset);

            //read the glyph
            final short nbcontour = ds.readShort();
            if (nbcontour>=0){
                //simple glyph
                final SimpleGlyph sglyph = new SimpleGlyph();
                glyph = sglyph;
                sglyph.numberOfContours = nbcontour;
                sglyph.xMin = ds.readShort();
                sglyph.yMin = ds.readShort();
                sglyph.xMax = ds.readShort();
                sglyph.yMax = ds.readShort();

                sglyph.endPtsOfContours = ds.readUShort(nbcontour);
                sglyph.instructionLength = ds.readUShort();
                sglyph.instructions = ds.readFully(new byte[sglyph.instructionLength]);
                final int nbPts = sglyph.endPtsOfContours[nbcontour-1] + 1;

                sglyph.flags = new byte[nbPts];
                for (int i=0;i<nbPts;i++){
                    sglyph.flags[i] = ds.readByte();
                    if ((sglyph.flags[i] & TTFConstants.SG_REPEAT)==0) continue;
                    //repetition
                    final int nbrepeat = ds.readUByte();
                    Arrays.fill(sglyph.flags, i+1, nbrepeat, sglyph.flags[i]);
                    i+=nbrepeat;
                }

                sglyph.xCoordinates = new short[nbPts];
                short x=0;
                for (int i=0;i<nbPts;i++){
                    final boolean oneByte = ((sglyph.flags[i]&TTFConstants.SG_XSV)!=0);
                    final boolean same = ((sglyph.flags[i]&TTFConstants.SG_XMSK)!=0);
                    if (!oneByte){
                        if (same){
                            //same a previous coordinate, do nothing
                        } else {
                            //delta modification
                            x += ds.readShort();
                        }
                    } else {
                        int val = ds.readUByte();
                        if (!same) val = -val; //flip sign
                        x += val;
                    }
                    sglyph.xCoordinates[i] = x;
                }

                sglyph.yCoordinates = new short[nbPts];
                short y=0;
                for (int i=0;i<nbPts;i++){
                    final boolean oneByte = ((sglyph.flags[i]&TTFConstants.SG_YSV)!=0);
                    final boolean same = ((sglyph.flags[i]&TTFConstants.SG_YMSK)!=0);
                    if (!oneByte){
                        if (same){
                            //same a previous coordinate, do nothing
                        } else {
                            //delta modification
                            y += ds.readShort();
                        }
                    } else {
                        int val = ds.readUByte();
                        if (!same) val = -val; //flip sign
                        y += val;
                    }
                    sglyph.yCoordinates[i] = y;
                }

            } else if (nbcontour==-1){
                //compound glyph
                CompoundGlyph cglyph = new CompoundGlyph();
                glyph = cglyph;
                cglyph.numberOfContours = nbcontour;
                cglyph.xMin = ds.readShort();
                cglyph.yMin = ds.readShort();
                cglyph.xMax = ds.readShort();
                cglyph.yMax = ds.readShort();
            } else {
                throw new IOException("Unknowned glyph type : "+nbcontour);
            }
        }finally{
            ds.dispose();
        }

        glyphs.add(glyphOffset, glyph);
        return glyph;
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
