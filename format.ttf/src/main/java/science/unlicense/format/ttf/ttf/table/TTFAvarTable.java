
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * In drawing a glyph with variations, the coordinates specified by the
 * user must be mapped from the space defined by the axes' minimum, default,
 * and maximum values into a normalized space of -1.0, 0 and 1.0.
 * The Axis Variation table (tag: 'avar') allows the font to modify the mapping
 * between axis values and these normalized values, which are represented as shortFrac values.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFAvarTable extends TTFTable{

    public TTFAvarTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_AVAR);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
