
package science.unlicense.format.ttf.ttf;

import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.number.Endianness;

/**
 * TTF Writer.
 *
 * @author Johann Sorel
 */
public class TTFWriter extends AbstractWriter {

    public void write(TrueTypeFont font) throws IOException{

        final DataOutputStream ds = getOutputAsDataStream(Endianness.BIG_ENDIAN);

        final int numTables = font.tables.getSize();
        ds.writeInt(font.scalerType);
        ds.writeUShort(numTables);
        ds.writeUShort(font.searchRange);
        ds.writeUShort(font.entrySelector);
        ds.writeUShort(font.rangeShift);

        int offset = 12;

        for (int i=0; i<numTables; i++) {
            TTFTable table = (TTFTable) font.tables.get(i);
            offset += table.write(ds,offset);
        }
    }

}
