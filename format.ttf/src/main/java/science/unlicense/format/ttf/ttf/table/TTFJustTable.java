
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * Thejustification table (tag name: 'just') allows you to design your
 * AAT fonts to accommodate special justification functions.
 * Justification is the process of typographically stretching or shrinking a line of text to fit within a given height or width.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFJustTable extends TTFTable{

    public TTFJustTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_JUST);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
