
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The tracking table (tag: 'trak') allows you to design Quickdraw GX
 * fonts which allow adjustment to normal interglyph spacing.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFTrakTable extends TTFTable{

    public TTFTrakTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_TRAK);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
