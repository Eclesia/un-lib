
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'hdmx' table is used only with fonts intended for use on the Macintosh platform.
 * If this table is present in a font, the values in contains will be used where appropriate.
 * If this table is missing, the values in the 'hmtx' table will be scaled and rounded.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFHdmxTable extends TTFTable{

    public TTFHdmxTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_HDMX);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
