
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * By collecting all this information into one placeÑthe 'Zapf' table—named
 * with permission after legendary type designer Hermann Zapf—can make
 * it much easier for the user interface parts of applications to get what
 * they need to present sensible choices to their users.
 * This document proposes a format for this table which represents all this information in a relatively compact form.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFZapfTable extends TTFTable{

    public TTFZapfTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_ZAPF);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
