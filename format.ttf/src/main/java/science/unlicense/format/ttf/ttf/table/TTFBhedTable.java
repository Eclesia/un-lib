
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'bhed' table contains global information about a bitmap font.
 * It records such facts as the font version number, the creation and modification dates,
 * revision number and basic typographic data that applies to the font as a whole.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFBhedTable extends TTFTable{

    public TTFBhedTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_BHED);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
