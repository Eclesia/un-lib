
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'kern' table contains the values that adjust the intercharacter
 * spacing for glyphs in a font. It can have multiple subtables in varied
 * formats that can contain information for vertical or horizontal text.
 * Kerning values are used to adjust intercharacter spacing.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFKernTable extends TTFTable{

    public TTFKernTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_KERN);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
