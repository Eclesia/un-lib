
package science.unlicense.format.ttf.ttf;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.primitive.IntSet;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.display.api.font.Font;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.font.AbstractFontMetadata;
import science.unlicense.display.api.font.DerivateFont;
import science.unlicense.display.api.font.CachedFontMetadata;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class TTFFont implements Font {

    private final Chars name;
    private final Path path;
    private TrueTypeFont font;
    private AbstractFontMetadata metadata;

    /**
     * Cache glyphs.
     */
    public final Dictionary glyphs = new HashDictionary();

    public TTFFont(Chars name, Path path) {
        this.name = name;
        this.path = path;
    }

    public synchronized TrueTypeFont getFont() {
        if (font==null){
            TTFReader reader = new TTFReader();
            try {
                reader.setInput(path);
                font = reader.read();
                reader.dispose();
                metadata = new CachedFontMetadata(new TTFFontMetadata(font));
            } catch (IOException ex) {
                Loggers.get().log(ex,Logger.LEVEL_WARNING);
            }
        }
        return font;
    }

    public AbstractFontMetadata getMetaData() {
        getFont();//ensure it is loaded
        return metadata;
    }

    @Override
    public Chars getFamily() {
        return name;
    }

    @Override
    public int getWeight() {
        //TODO
        return WEIGHT_NONE;
    }

    @Override
    public float getSize() {
        return (float) getMetaData().getHeight();
    }

    @Override
    public FontStore getStore() {
        return TTFFontStore.INSTANCE;
    }

    @Override
    public IntSet listCharacters() {
        try {
            return getFont().listCharacters();
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public PlanarGeometry getGlyph(int unicode) {
        PlanarGeometry g = (PlanarGeometry) glyphs.getValue(unicode);
        if (g==null){
            g = getFont().getGlyph(unicode);
            glyphs.add(unicode, g);
        }
        return g;
    }

    @Override
    public PlanarGeometry getGlyph(Char character) {
        return getGlyph(character.toUnicode());
    }

    @Override
    public Font derivate(float fontSize) {
        if (fontSize==getSize()) return this;
        final double scaling = fontSize/getSize();
        final Affine2 trs = new Affine2(scaling, 0, 0, 0, scaling, 0);
        return new DerivateFont(this, trs, fontSize);
    }

}
