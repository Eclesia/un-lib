
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The baseline table (tag name: 'bsln') allows you to design your AAT
 * fonts to accommodate the automatic alignment of text to different baselines.
 * All fonts have a natural baseline defined by the position of each glyph in the font's em-square.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class TTFBslnTable extends TTFTable{

    public TTFBslnTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_BSLN);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
