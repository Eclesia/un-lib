
package science.unlicense.format.ttf.ttf.table;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ttf.ttf.TTFConstants;
import science.unlicense.format.ttf.ttf.TTFTable;
import science.unlicense.format.ttf.ttf.TrueTypeFont;
import science.unlicense.common.api.number.Endianness;

/**
 * TTF Specification :
 * The 'cmap' table maps character codes to glyph indices.
 *
 * @author Johann Sorel
 */
public class TTFCmapTable extends TTFTable{

    public static class Table{
        /** UInt16 : Platform identifier*/
        public int platformID;
        /** UInt16 : Platform-specific encoding identifier*/
        public int platformSpecificID;
        /** UInt32 : Offset of the mapping table*/
        public long offset;
    }

    //TODO format 0, format 2, format 4, format 6, format 8.0, format 10.0, and format 12.0
    public static class Format0 extends Table{
        /** UInt16 : Set to 0*/
        public int format;
        /** UInt16 : Length in bytes of the subtable (set to 262 for format 0)*/
        public int length;
        /** UInt16 : Language code for this encoding subtable, or zero if language-independent*/
        public int language;
        /** UInt8 : An array that maps character codes to glyph index values*/
        public int[] glyphIndexArray;
    }

    public static class Format4 extends Table{
        /** UInt16 : Set to 4*/
        public int format;
        /** UInt16 : Length in bytes of the subtable*/
        public int length;
        /** UInt16 : Language code for this encoding subtable, or zero if language-independent*/
        public int language;
        /** UInt16 : 2 * segCount*/
        public int segCountX2;
        /** UInt16 : 2 * (2**FLOOR(log2(segCount)))*/
        public int searchRange;
        /** UInt16 : log2(searchRange/2)*/
        public int entrySelector;
        /** UInt16 : (2 * segCount) - searchRange*/
        public int rangeShift;
        /** UInt16 : Ending character code for each segment, last = 0xFFFF.*/
        public int[] endCode;
        /** UInt16 : This value should be zero*/
        public int reservedPad;
        /** UInt16 : Starting character code for each segment*/
        public int[] startCode;
        /** UInt16 : Delta for all character codes in segment*/
        public int[] idDelta;
        /** UInt16 : Offset in bytes to glyph indexArray, or 0*/
        public int[] idRangeOffset;
        /** UInt16 : Glyph index array
         * Table seems to overlaps, allowing table to pick values
         * further in the stream.
         * We rebuild the array here
         */
        public int[][] glyphIndexes;
    }



    /** UInt16 : Version number (Set to zero)*/
    public int version;
    /** UInt16 : Number of encoding subtables*/
    public int numberSubtables;

    public Table[] tables;

    public TTFCmapTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_CMAP);
    }

    public void readData(DataInputStream bds) throws IOException {
        super.readData(bds);

        final byte[] buffer = bds.readFully(new byte[length]);
        final BacktrackInputStream bs = new BacktrackInputStream(new ArrayInputStream(buffer));
        bs.mark();
        final DataInputStream ds = new DataInputStream(bs, Endianness.BIG_ENDIAN);

        version         = ds.readUShort();
        numberSubtables = ds.readUShort();
        tables = new Table[numberSubtables];
        for (int i=0;i<tables.length;i++){
            tables[i] = new Table();
            tables[i].platformID         = ds.readUShort();
            tables[i].platformSpecificID = ds.readUShort();
            tables[i].offset             = ds.readUInt();
        }

        //replace tables by more complete format tables.
        for (int i=0;i<tables.length;i++){
            bs.rewind();
            ds.skipFully(tables[i].offset);
            final int format = ds.readUShort();

            if (format == 0){
                final Format0 f = new Format0();
                f.format                = format;
                f.platformID            = tables[i].platformID;
                f.platformSpecificID    = tables[i].platformSpecificID;
                f.offset                = tables[i].offset;
                f.length                = ds.readUShort();
                f.language              = ds.readUShort();
                f.glyphIndexArray       = ds.readUByte(256);
                tables[i] = f;
            } else if (format == 4){
                final Format4 f = new Format4();
                f.format                = format;
                f.platformID            = tables[i].platformID;
                f.platformSpecificID    = tables[i].platformSpecificID;
                f.offset                = tables[i].offset;
                f.length                = ds.readUShort();
                f.language              = ds.readUShort();
                f.segCountX2            = ds.readUShort();
                final int segCount      = f.segCountX2/2;
                f.searchRange           = ds.readUShort();
                f.entrySelector         = ds.readUShort();
                f.rangeShift            = ds.readUShort();
                f.endCode               = ds.readUShort(segCount);
                f.reservedPad           = ds.readUShort();
                f.startCode             = ds.readUShort(segCount);
                f.idDelta               = ds.readUShort(segCount);
                f.idRangeOffset         = new int[segCount];
                f.glyphIndexes          = new int[segCount][0];
                for (int k=0;k<segCount;k++){
                    f.idRangeOffset[k] = ds.readUShort();

                    if (f.idRangeOffset[k]!=0){
                        final int nbGlyph = f.endCode[k]-f.startCode[k]+1;
                        final DataInputStream gds = new DataInputStream(new ArrayInputStream(buffer), Endianness.BIG_ENDIAN);
                        gds.skipFully(bs.position() - 2 + f.idRangeOffset[k]);
                        f.glyphIndexes[k] = gds.readUShort(nbGlyph);
                    }
                }
                tables[i] = f;
            }

        }
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }

}
