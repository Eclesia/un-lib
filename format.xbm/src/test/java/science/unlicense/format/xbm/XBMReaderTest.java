package science.unlicense.format.xbm;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReader;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public class XBMReaderTest {

    @Test
    public void testReader() throws Exception {

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/science/unlicense/format/xbm/sample.xbm")).createInputStream();

        final ImageReader reader = new XBMReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(11,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleGrid tb = image.getRawModel().asTupleBuffer(image);

        final int[][] expected = new int[][]{
            {1,0,1,0,1,1,1,1,0,1,0},
            {0,1,0,1,0,0,0,0,0,1,0}
        };

        final TupleRW pixel = tb.createTuple();
        for (int y=0;y<2;y++){
            for (int x=0;x<11;x++){
                tb.getTuple(new Vector2i32(x,y),pixel);
                Assert.assertEquals(expected[y][x], pixel.get(0), "at "+y+" "+x);
            }
        }

    }

}
