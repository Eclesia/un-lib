
package science.unlicense.format.xbm;

import science.unlicense.code.c.CWriter;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Bits;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.AbstractImageWriter;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageWriteParameters;
import science.unlicense.image.api.model.ImageModel;

/**
 *
 * @author Johann Sorel
 */
public class XBMWriter extends AbstractImageWriter {

    protected void write(Image image, ImageWriteParameters params, ByteOutputStream stream) throws IOException {

        final ImageModel sm = image.getRawModel();
        if (sm.getNumericType()!= Bits.TYPE_1_BIT){
            throw new IOException("Only 1Bit sample image supported.");
        } else if (sm.getSampleSystem().getNumComponents() != 1){
            throw new IOException("Only one sample supported.");
        }

        final CWriter writer = new CWriter();
        writer.setOutput(getOutput());

        writer.writeDefine(new Chars("width"), Int32.encode((int) image.getExtent().getL(0)));
        writer.writeDefine(new Chars("height"), Int32.encode((int) image.getExtent().getL(1)));
        //TODO
    }

}
