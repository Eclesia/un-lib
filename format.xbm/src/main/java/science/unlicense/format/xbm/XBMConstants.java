

package science.unlicense.format.xbm;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class XBMConstants {

    /**
     * Not a real signature but will do.
     */
    public static final Chars SIGNATURE = Chars.constant("#define");
    public static final Chars DECLARATION = Chars.constant("static unsigned char sample_bits[] = {");

    private XBMConstants() {
    }

}
