

package science.unlicense.format.xbm;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.common.api.number.Bits;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.DefaultImage;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGESET;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND;
import static science.unlicense.image.api.ImageSetMetadata.MD_IMAGE_DIMENSION_ID;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.model.DerivateModel;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.api.model.PlanarModel;
import science.unlicense.math.api.system.UndefinedSystem;

/**
 *
 * @author Johann Sorel
 */
public class XBMReader extends AbstractImageReader {

    private static final Chars SPACE = Chars.constant(" ");
    private static final Chars EMPTY = Chars.constant(" ");

    private TypedNode mdImage;
    private int width;
    private int height;
    private int hotspotx;
    private int hotspoty;
    private Buffer datas;

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        final CharInputStream cs = new CharInputStream(stream, CharEncodings.US_ASCII, new Char('\n'));

        final Chars lineWidth = cs.readLine();
        final Chars lineHeight = cs.readLine();
        width = Int32.decode(lineWidth.createIterator(lineWidth.getLastOccurence(' ')+1, lineWidth.getCharLength()),false);
        height = Int32.decode(lineHeight.createIterator(lineHeight.getLastOccurence(' ')+1, lineHeight.getCharLength()),false);

        //check if there is a hotspot
        Chars candidate = cs.readLine().trim();
        if (candidate.startsWith(new Char('#'))){
            //hotspot informations
            final Chars lineHotSpotX = candidate;
            final Chars lineHotSpotY = cs.readLine();
            hotspotx = Int32.decode(lineHotSpotX.createIterator(
                    lineHotSpotX.getLastOccurence(' ')+1, lineHotSpotX.getCharLength()),false);
            hotspoty = Int32.decode(lineHotSpotY.createIterator(
                    lineHotSpotY.getLastOccurence(' ')+1, lineHotSpotY.getCharLength()),false);

            //skip declaration
            cs.readLine();
        }

        //datas
        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ds = new DataOutputStream(out);
        Chars line = cs.readLine();

        int x=width;
        for (;line!=null;line=cs.readLine()){
            final Chars[] parts = splitValues(line);
            for (int i=0;i<parts.length;i++){
                parts[i] = parts[i].trim();
                byte b = (byte) Int32.decodeHexa(parts[i],2,parts[i].getCharLength());
                for (int k=0;k<8;k++){
                     //bits are stored the opposite way
                    final int bit = (b & (1 << (k))) >>> k;
                    ds.writeBit(bit);

                    //ensuse we dont use unread unused bits at the end of each line
                    x--;
                    if (x==0){
                        x = width;
                        break;
                    }
                }
            }
        }
        ds.flush();
        datas = DefaultBufferFactory.wrap(out.getBuffer().getBackArray());

        //rebuild metas
        mdImage =
        new DefaultTypedNode(MD_IMAGE,new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,width)}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,height)})
        });

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        readMetadatas(stream);

        final ImageModel sm = new PlanarModel(new UndefinedSystem(1), Bits.TYPE_1_BIT);
        final ImageModel cm = DerivateModel.create(sm, new int[]{0,0,0}, null, null, ColorSystem.RGB_8BITS);

        return new DefaultImage(datas, new Extent.Long(width, height), sm, cm);
    }

    private static Chars[] splitValues(Chars candidate){
        candidate = candidate.trim().replaceAll(SPACE,EMPTY);
        final int index = candidate.getLastOccurence('}');
        if (index>0){
            candidate = candidate.truncate(0, index);
        }
        return candidate.split(',');
    }

}
