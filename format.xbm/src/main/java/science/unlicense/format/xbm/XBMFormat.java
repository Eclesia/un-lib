
package science.unlicense.format.xbm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * Resources :
 * http://en.wikipedia.org/wiki/X_BitMap
 *
 * @author Johann Sorel
 */
public class XBMFormat extends AbstractImageFormat {

    public static final XBMFormat INSTANCE = new XBMFormat();

    private XBMFormat() {
        super(new Chars("xbm"));
        shortName = new Chars("XBM");
        longName = new Chars("X BitMap");
        mimeTypes.add(new Chars("image/x-xbitmap"));
        mimeTypes.add(new Chars("image/x-xbm"));
        extensions.add(new Chars("xbm"));
        signatures.add(XBMConstants.SIGNATURE.toBytes());
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return true;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new XBMStore(this, source);
    }

}
