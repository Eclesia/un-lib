
package science.unlicense.format.dbf;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * Specification :
 * http://dbase.com/Knowledgebase/INT/db7_file_fmt.htm
 * http://www.dbf2002.com/dbf-file-format.html
 *
 *
 * @author Johann Sorel
 */
public class DBFFormat extends DefaultFormat {

    public static final DBFFormat INSTANCE = new DBFFormat();

    public DBFFormat() {
        super(new Chars("dbf"));
        shortName = new Chars("DBF");
        longName = new Chars("Database File");
        mimeTypes.add(new Chars("application/dbase"));
        extensions.add(new Chars("sbf"));
        signatures.add(new byte[]{0x03});
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
