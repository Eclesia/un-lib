
package science.unlicense.format.dbf;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public final class DBFHeader {

    private static final int FIELD_SIZE = 32;

    public int year;
    public int month;
    public int day;
    public int nbRecord;
    public int headerSize;
    public int recordSize;
    public DBFField[] fields;

    public void read(DataInputStream ds) throws IOException {

        if (ds.read() != 0x03) {
            throw new IOException(ds, "Unvalid magic number");
        }

        year       = ds.readUByte();
        month      = ds.readUByte();
        day        = ds.readUByte();
        nbRecord   = ds.readInt();
        headerSize = ds.readUShort();
        recordSize = ds.readUShort();
        ds.skipFully(20);
        fields     = new DBFField[(headerSize - FIELD_SIZE - 1) / FIELD_SIZE];

        for (int i=0; i<fields.length; i++) {
            fields[i] = new DBFField();
            fields[i].read(ds);
        }
        ds.skipFully(1);
    }

}
