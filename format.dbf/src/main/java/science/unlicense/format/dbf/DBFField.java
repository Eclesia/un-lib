
package science.unlicense.format.dbf;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.character.Languages;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.time.api.Date;

/**
 *
 * @author Johann Sorel
 */
public class DBFField {

    /**
     * Binary, String : b or B
     */
    public static final int TYPE_BINARY = 'b';
    /**
     * Characters : c or C
     */
    public static final int TYPE_CHAR = 'c';
    /**
     * Date : d or D
     */
    public static final int TYPE_DATE = 'd';
    /**
     * Numeric : n or N
     */
    public static final int TYPE_NUMBER = 'n';
    /**
     * Logical : l or L
     */
    public static final int TYPE_LOGIC = 'l';
    /**
     * Memo, String : m or M
     */
    public static final int TYPE_MEMO = 'm';
    /**
     * TimeStamp : 8 bytes, two longs, first for date, second for time.
     * The date is the number of days since  01/01/4713 BC.
     * Time is hours * 3600000L + minutes * 60000L + Seconds * 1000L
     */
    public static final int TYPE_TIMESTAMP = '@';
    /**
     * Long : i or I on 4 bytes, first bit is the sign, 0 = negative
     */
    public static final int TYPE_LONG = 'i';
    /**
     * Autoincrement : same as Long
     */
    public static final int TYPE_INC = '+';
    /**
     * Floats : f or F
     */
    public static final int TYPE_FLOAT = 'f';
    /**
     * Double : o or O, real double on 8bytes, not string encoded
     */
    public static final int TYPE_DOUBLE = 'o';
    /**
     * OLE : g or G
     */
    public static final int TYPE_OLE = 'g';

    public Chars fieldName;
    public int fieldType;
    public int fieldAddress;
    public int fieldLength;
    public int fieldLDecimals;

    public void read(DataInputStream ds) throws IOException {
        fieldName      = ds.readBlockZeroTerminatedChars(11, CharEncodings.US_ASCII);
        fieldType      = Languages.UNSET.toLowerCase(ds.readUByte());
        fieldAddress   = ds.readInt();
        fieldLength    = ds.readUByte();
        fieldLDecimals = ds.readUByte();
        ds.skipFully(14);
    }

    public Class getValueClass() {
        switch (fieldType) {
            case TYPE_BINARY : return Long.class;
            case TYPE_CHAR : return Chars.class;
            case TYPE_DATE : return Date.class;
            case TYPE_NUMBER : {
                if (fieldLDecimals != 0) return Double.class;
                return fieldLength > 9 ? Long.class : Integer.class;
            }
            case TYPE_LOGIC : return Boolean.class;
            case TYPE_MEMO : return Long.class;
            case TYPE_TIMESTAMP : return Date.class;
            case TYPE_LONG : return Long.class;
            case TYPE_INC : return Long.class;
            case TYPE_FLOAT : return Double.class;
            case TYPE_DOUBLE : return Double.class;
            case TYPE_OLE : return Long.class;
            default: throw new InvalidArgumentException("Unknown field type "+fieldType);
        }
    }

}
