
package science.unlicense.format.dbf;

import science.unlicense.common.api.exception.MishandleException;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class DBFReader extends AbstractReader {

    private DBFHeader header;
    private int recordIndex = 0;
    private Document nextValue = null;

    public boolean hasNext() throws IOException {
        if (nextValue == null) findNext();
        return nextValue != null;
    }

    public Object next() throws IOException {
        if (nextValue == null) findNext();
        if (nextValue == null) {
            throw new MishandleException("No more elements");
        }
        Object temp = nextValue;
        nextValue = null;
        return temp;
    }

    private void findNext() throws IOException {
        final DataInputStream ds = getInputAsDataStream(Endianness.LITTLE_ENDIAN);
        if (header == null) {
            header = new DBFHeader();
            header.read(ds);
        }

        if (recordIndex < header.nbRecord) {
            //deletion marker
            int deleted = ds.read();
            throw new IOException(ds, "todo");
        }

    }

}
