

package science.unlicense.format.webm;

import org.junit.Test;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.mkv.MKVMediaStore;

/**
 *
 * @author Johann Sorel
 */
public class WebmReaderTest {

    @Test
    public void testRead() throws IOException{
        MKVMediaStore store = new MKVMediaStore(Paths.resolve(new Chars("mod:/science/unlicense/format/webm/sample.webm")));
        store.createReader(null);
    }

}
