

package science.unlicense.format.webm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaCapabilities;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaCapabilities;

/**
 *
 * @author Johann Sorel
 */
public class WebmFormat extends AbstractMediaFormat{

    public static final WebmFormat INSTANCE = new WebmFormat();

    public WebmFormat() {
        super(new Chars("webm"));
        shortName = new Chars("Webm");
        longName = new Chars("Webm");
        mimeTypes.add(new Chars("video/webm"));
        mimeTypes.add(new Chars("audio/webm"));
        extensions.add(new Chars("webm"));
        resourceTypes.add(Media.class);
    }

    @Override
    public boolean supportReading() {
        return false;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    @Override
    public Store open(Object input) throws IOException {
        throw new RuntimeException("not yet");
    }

}
