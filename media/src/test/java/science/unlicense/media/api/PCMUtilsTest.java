
package science.unlicense.media.api;

import org.junit.Test;
import science.unlicense.common.api.Assert;

/**
 *
 * @author Johann Sorel
 */
public class PCMUtilsTest {

    @Test
    public void testPcmFloat(){
        Assert.assertEquals(+1f, PCMUtils.PCMSignedToFloat(+32767,16));
        Assert.assertEquals(-1f, PCMUtils.PCMSignedToFloat(-32768,16));
    }

    @Test
    public void testFloatToPcm(){
        Assert.assertEquals(+32767, PCMUtils.floatToSignedPCM(+1,16));
        Assert.assertEquals(-32768, PCMUtils.floatToSignedPCM(-1,16));
    }

}
