
package science.unlicense.media.impl.inmemory;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.image.api.Image;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.media.api.DefaultImagePacket;
import science.unlicense.media.api.ImagePacket;
import science.unlicense.media.api.MediaReadStream;

/**
 *
 * @author Johann Sorel
 */
class MemoryImageReadStream implements MediaReadStream {

    private final MemoryImageSerie mv;
    private int currentImageIndex = -1;

    public MemoryImageReadStream(MemoryImageSerie mv) {
        this.mv = mv;
    }

    public ImagePacket next() {
        currentImageIndex++;
        if (currentImageIndex < mv.getImages().getSize()-1){
            final Image img = (Image) mv.getImages().get(currentImageIndex);
            final long step = mv.getTimeSpacing();
            final long time = currentImageIndex*step;
            final DefaultImagePacket packet = new DefaultImagePacket(null, time, step, img);
        }
        return null;
    }

    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
