
package science.unlicense.media.impl.inmemory;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.media.api.Track;
import science.unlicense.media.api.ImageTrack;

/**
 * An in memory video based on a sequence of images.
 *
 * @author Johann Sorel
 */
public class MemoryImageSerie {

    private final Sequence images = new ArraySequence();
    private int timeSpacing = 1;

    public MemoryImageSerie() {
    }

    /**
     * @return time between each image, in milliseconds.
     */
    public int getTimeSpacing() {
        return timeSpacing;
    }

    public void setTimeSpacing(int timeSpacing) {
        this.timeSpacing = timeSpacing;
    }

    /**
     * Get images stack in this video.
     * @return Sequence, never null, can be empty.
     */
    public Sequence getImages() {
        return images;
    }

    public Track getStreamsMeta() {
        final ImageTrack meta = new ImageTrack() {
            public TypedNode getImageMetadata() {
                throw new UnimplementedException("Not supported yet.");
            }
            public int getNbFrames() {
                return images.getSize();
            }
            public int getFrameRate() {
                return timeSpacing;
            }
            public Chars getName() {
                return Chars.EMPTY;
            }
        };
        return meta;
    }

    public MediaReadStream getReader(){
        return new MemoryImageReadStream(this);
    }

}
