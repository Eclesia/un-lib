
package science.unlicense.media.impl.inmemory;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaStackReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;

/**
 * An in memory video composed of a succesion of images.
 *
 * @author Johann Sorel
 */
public class MemoryMediaStore extends AbstractStore implements Media {

    private final Sequence streams = new ArraySequence();

    public MemoryMediaStore() {
        super(MemoryImageVideoFormat.INSTANCE,null);
    }

    /**
     * Modifiable sequence of all streams in this storage.
     *
     * @return Sequence, never null. can be empty.
     */
    public Sequence getStreams() {
        return streams;
    }

    @Override
    public Track[] getTracks() {
        final Track[] metas = new Track[streams.getSize()];
        for (int i=0;i<metas.length;i++){
            metas[i] = ((MemoryImageSerie) streams.get(i)).getStreamsMeta();
        }

        return metas;
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) {
        final int[] indexes = params.getStreamIndexes();
        final MediaReadStream[] readers = new MediaReadStream[indexes.length];
        for (int i=0;i<indexes.length;i++){
            readers[i] = ((MemoryImageSerie) streams.get(i)).getReader();
        }
        return new MediaStackReadStream(readers);
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) {
        throw new UnimplementedException("Not supported yet.");
    }

}
