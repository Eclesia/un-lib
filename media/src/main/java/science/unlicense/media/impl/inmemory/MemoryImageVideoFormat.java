
package science.unlicense.media.impl.inmemory;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;

/**
 *
 * @author Johann Sorel
 */
public class MemoryImageVideoFormat extends AbstractMediaFormat {
    public static final MemoryImageVideoFormat INSTANCE = new MemoryImageVideoFormat();

    private MemoryImageVideoFormat() {
        super(new Chars("memory"));
        shortName = new Chars("Memory");
        longName = new Chars("In memory video format");
    }

    @Override
    public Store open(Object input) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
