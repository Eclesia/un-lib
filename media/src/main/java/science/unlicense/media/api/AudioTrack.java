
package science.unlicense.media.api;

/**
 *
 * @author Johann Sorel
 */
public interface AudioTrack extends Track {

    public static final int CHANNEL_MONO            = 0;
    public static final int CHANNEL_LEFT            = 1;
    public static final int CHANNEL_RIGHT           = 2;
    public static final int CHANNEL_CENTER          = 3;
    public static final int CHANNEL_CENTER_LEFT     = 4;
    public static final int CHANNEL_CENTER_RIGHT    = 5;
    public static final int CHANNEL_FRONT_LEFT      = 6;
    public static final int CHANNEL_FRONT_RIGHT     = 7;
    public static final int CHANNEL_REAR            = 8;
    public static final int CHANNEL_REAR_LEFT       = 9;
    public static final int CHANNEL_REAR_RIGHT      = 10;
    public static final int CHANNEL_SURROUND        = 11;
    public static final int CHANNEL_DIFF            = 12;
    public static final int CHANNEL_SIDE            = 13;

    /**
     * Indicate the available channels in the stream.
     * Values are from CHANNEL_X constants.
     *
     * @return int[]
     */
    int[] getChannels();

    /**
     * Number of sample per second : frequency (hertz).
     *
     * @return double
     */
    double getSampleRate();

    /**
     * Number of bits used for each sample in each channel.
     *
     * @return int[]
     */
    int[] getBitsPerSample();


}
