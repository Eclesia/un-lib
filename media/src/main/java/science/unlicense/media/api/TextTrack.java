
package science.unlicense.media.api;

/**
 * Stream definition for text like tracks.
 * Used for subtitles.
 *
 * @author Johann Sorel
 */
public interface TextTrack extends Track{

}
