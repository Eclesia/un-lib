

package science.unlicense.media.api;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DefaultAudioTrack implements AudioTrack {

    private final Chars name;
    private final int[] channels;
    private final int[] bitsPerSample;
    private final double sampleRate;

    public DefaultAudioTrack(Chars name, int[] channels, int[] bitsPerSample, double sampleRate) {
        this.name = name;
        this.channels = channels;
        this.sampleRate = sampleRate;
        this.bitsPerSample = bitsPerSample;
    }

    public Chars getName() {
        return name;
    }

    public int[] getChannels() {
        return Arrays.copy(channels, new int[channels.length]);
    }

    public double getSampleRate() {
        return sampleRate;
    }

    public int[] getBitsPerSample() {
        return Arrays.copy(bitsPerSample, new int[bitsPerSample.length]);
    }

}
