
package science.unlicense.media.api;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public interface MediaFormat extends Format {

    /**
     * Encoding capabilities of the format.
     * @return MediaCapabilities
     */
    MediaCapabilities getCapabilities();

    /**
     * @return true if reading is supported.
     */
    boolean supportReading();

    /**
     * @return true if writing is supported.
     */
    boolean supportWriting();

    /**
     * @param input
     * @return new media store
     */
    @Override
    Store open(Object input) throws IOException;

}
