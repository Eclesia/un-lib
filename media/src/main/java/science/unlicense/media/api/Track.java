
package science.unlicense.media.api;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public interface Track {

    /**
     * Get name of this track.
     *
     * @return Chars, never null
     */
    Chars getName();

}
