
package science.unlicense.media.api;

/**
 *
 * @author Johann Sorel
 */
public class AbstractMediaPacket implements MediaPacket{

    protected final Track meta;
    protected final long startTime;
    protected final long timeLength;

    public AbstractMediaPacket(Track meta, long startTime, long timeLength) {
        this.meta = meta;
        this.startTime = startTime;
        this.timeLength = timeLength;
    }

    public Track getTrack() {
        return meta;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getTimeLength() {
        return timeLength;
    }

}
