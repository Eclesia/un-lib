
package science.unlicense.media.api;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DefaultTextPacket extends AbstractMediaPacket implements TextPacket {

    protected final Chars text;

    public DefaultTextPacket(TextTrack meta, long startTime, long timeLength, Chars text) {
        super(meta, startTime, timeLength);
        this.text = text;
    }

    public TextTrack getTrack() {
        return (TextTrack) super.getTrack();
    }

    public Chars getText() {
        return text;
    }

}
