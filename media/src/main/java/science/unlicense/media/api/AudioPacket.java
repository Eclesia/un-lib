
package science.unlicense.media.api;

/**
 * Audio packet.
 *
 * @author Johann Sorel
 */
public interface AudioPacket extends MediaPacket {

    /**
     *
     * @return audio stream metadata.
     */
    AudioTrack getTrack();

    /**
     * @return packet audio samples.
     */
    AudioSamples getSamples();

}
