
package science.unlicense.media.api;

/**
 *
 * @author Johann Sorel
 */
public interface MediaPacket {

    /**
     *
     * @return metadatas of the track
     */
    Track getTrack();

    /**
     *
     * @return packet start time or -1 if unknown
     */
    long getStartTime();

    /**
     *
     * @return packet time length or -1 if unknown
     */
    long getTimeLength();

}
