
package science.unlicense.media.api;

/**
 *
 * @author Johann Sorel
 */
public class DefaultAudioPacket extends AbstractMediaPacket implements AudioPacket {

    protected final AudioSamples samples;

    public DefaultAudioPacket(AudioTrack meta, long startTime, long timeLength, AudioSamples samples) {
        super(meta, startTime, timeLength);
        this.samples = samples;
    }

    public AudioTrack getTrack() {
        return (AudioTrack) super.getTrack();
    }

    public AudioSamples getSamples() {
        return samples;
    }

}
