
package science.unlicense.media.api;

import science.unlicense.encoding.api.io.IOException;

/**
 * Media stream reader.
 *
 * @author Johann Sorel
 */
public interface MediaReadStream {

    /**
     * Move to given time.
     *
     * @param time
     * @throws IOException if the stream does not support random access.
     */
    void moveTo(long time) throws IOException;

    /**
     * Move forward on the next step if the reader support it.
     */
    MediaPacket next() throws IOException;

}
