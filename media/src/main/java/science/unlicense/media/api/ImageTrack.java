
package science.unlicense.media.api;

import science.unlicense.common.api.model.tree.TypedNode;

/**
 *
 * @author Johann Sorel
 */
public interface ImageTrack extends Track {

    /**
     * Get the video image type definition.
     * Expected to follow the image metamodel.
     *
     * @return TypedNode defition of the video images.
     */
    TypedNode getImageMetadata();

    /**
     * Get number of frame in the track.
     *
     * @return total number of frames, -1 for unknowned.
     */
    int getNbFrames();

    /**
     * Get the video frame rate.
     * In milliseconds between frames.
     *
     * @return  video frame rate.
     */
    int getFrameRate();

}
