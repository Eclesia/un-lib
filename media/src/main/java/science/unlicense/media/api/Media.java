
package science.unlicense.media.api;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Resource;

/**
 *
 * @author Johann Sorel
 */
public interface Media extends Resource {

    /**
     * List available streams in this storage.
     *
     * @return MediaStreamMeta array, never null
     */
    Track[] getTracks() throws IOException;

    MediaReadStream createReader(MediaReadParameters params) throws IOException;

    MediaWriteStream createWriter(MediaWriteParameters params) throws IOException;

}
