
package science.unlicense.media.api;

import science.unlicense.image.api.Image;

/**
 *
 * @author Johann Sorel
 */
public interface ImagePacket extends MediaPacket{

    /**
     *
     * @return image stream metadata.
     */
    ImageTrack getTrack();

    /**
     * @return image
     */
    Image getImage();

}
