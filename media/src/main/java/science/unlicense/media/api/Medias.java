
package science.unlicense.media.api;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.concurrent.api.Formats;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;

/**
 * Convinient methods for media manipulation.
 *
 * @author Johann Sorel
 */
public final class Medias {

    private Medias(){}

    /**
     * Lists available media formats.
     * @return array of MediaFormat, never null but can be empty.
     */
    public static MediaFormat[] getFormats(){
        return (MediaFormat[]) Formats.getFormatsOfType(MediaFormat.class);
    }

    public static boolean canDecode(Object input) throws IOException {
        return Formats.canDecode(input, new ArraySequence(getFormats()));
    }

    /**
     * Convinient method to open a media of unknowned format.
     * The method will loop on available formats until one can decode the source.
     *
     * @param input
     * @return MediaStore, never null
     * @throws IOException if not format could support the source.
     */
    public static Store open(Object input) throws IOException {
        return Formats.open(input, new ArraySequence(getFormats()));
    }

}
