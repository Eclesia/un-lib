
package science.unlicense.media.api;

import science.unlicense.common.api.character.Chars;

/**
 * Text packet.
 *
 * @author Johann Sorel
 */
public interface TextPacket extends MediaPacket {

    /**
     *
     * @return text stream metadata.
     */
    TextTrack getTrack();

    /**
     * @return packet text.
     */
    Chars getText();

}
