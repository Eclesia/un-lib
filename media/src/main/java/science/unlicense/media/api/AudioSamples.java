

package science.unlicense.media.api;

import science.unlicense.common.api.Arrays;

/**
 * A group of sample for each channel.
 *
 * @author Johann Sorel
 */
public class AudioSamples {

    public static final int ENCODING_FLOAT  = 0;
    public static final int ENCODING_PCM  = 1;
    public static final int ENCODING_SIGNEDPCM  = 2;

    private final int[] channels;
    private final int encoding;
    private final int nbBits;
    private final Object samples;


    /**
     *
     * @param channels
     * @param encoding FLOAT or PCM
     * @param nbBits used only with PCM type
     * @param samples
     */
    public AudioSamples(int[] channels, int encoding, int nbBits, Object samples) {
        this.channels = channels;
        this.encoding = encoding;
        this.nbBits = nbBits;
        this.samples = samples;
    }

    public int[] getChannels() {
        return channels;
    }

    public int getEncoding() {
        return encoding;
    }

    public int getNbBits() {
        return nbBits;
    }

    public Object getSamples() {
        return samples;
    }

    public int[] asPCM(int[] buffer, int outNbBits){
        if (buffer==null) buffer = new int[channels.length];

        if (encoding == ENCODING_FLOAT){
            final float[] floats = (float[]) samples;
            for (int i=0;i<channels.length;i++){
                buffer[i] = (int) PCMUtils.floatToPCM(floats[i], outNbBits);
            }
        } else if (encoding == ENCODING_PCM){
            final int[] pcm = (int[]) samples;

            if (outNbBits == this.nbBits){
                //no conversion, just copy
                Arrays.copy((int[]) pcm, 0, channels.length, buffer, 0);
            } else {
                //change number of bits
                for (int i=0;i<channels.length;i++){
                    final float v = PCMUtils.PCMToFloat(pcm[i], nbBits);
                    buffer[i] = (int) PCMUtils.floatToPCM(v, outNbBits);
                }
            }
        } else if (encoding == ENCODING_SIGNEDPCM){
            final int[] pcm = (int[]) samples;

            if (outNbBits == this.nbBits){
                //unsign values
                for (int i=0;i<channels.length;i++){
                    buffer[i] = (int) PCMUtils.PCMSignedToPCM(pcm[i], nbBits);
                }
            } else {
                //change number of bits
                for (int i=0;i<channels.length;i++){
                    final float v = PCMUtils.PCMSignedToFloat(pcm[i], nbBits);
                    buffer[i] = (int) PCMUtils.floatToPCM(v, outNbBits);
                }
            }
        } else {
            throw new RuntimeException("Unknowned encoding : "+encoding);
        }

        return buffer;
    }

    public int[] asPCMSigned(int[] buffer, int nbBits){
        if (buffer==null) buffer = new int[channels.length];

        if (encoding == ENCODING_FLOAT){
            final float[] floats = (float[]) samples;
            for (int i=0;i<channels.length;i++){
                buffer[i] = (int) PCMUtils.floatToSignedPCM(floats[i], nbBits);
            }
        } else if (encoding == ENCODING_PCM){
            final int[] pcm = (int[]) samples;

            if (nbBits == this.nbBits){
                //sign values
                for (int i=0;i<channels.length;i++){
                    buffer[i] = (int) PCMUtils.PCMToPCMSigned(pcm[i], nbBits);
                }
            } else {
                //change number of bits
                for (int i=0;i<channels.length;i++){
                    final float v = PCMUtils.PCMToFloat(pcm[i], nbBits);
                    buffer[i] = (int) PCMUtils.floatToSignedPCM(v, nbBits);
                }
            }

        } else if (encoding == ENCODING_SIGNEDPCM){
            final int[] pcm = (int[]) samples;

            if (nbBits == this.nbBits){
                //no conversion, just copy
                Arrays.copy((int[]) samples, 0, channels.length, buffer, 0);
            } else {
                //change number of bits
                for (int i=0;i<channels.length;i++){
                    final float v = PCMUtils.PCMSignedToFloat(pcm[i], nbBits);
                    buffer[i] = (int) PCMUtils.floatToSignedPCM(v, nbBits);
                }
            }

        } else {
            throw new RuntimeException("Unknowned encoding : "+encoding);
        }

        return buffer;
    }

    public float[] asFloat(float[] buffer){
        if (buffer==null) buffer = new float[channels.length];

        if (encoding == ENCODING_FLOAT){
            //no conversion, just copy
            Arrays.copy((float[]) samples, 0, channels.length, buffer, 0);
        } else if (encoding == ENCODING_PCM){
            final int[] pcm = (int[]) samples;
            for (int i=0;i<channels.length;i++){
                buffer[i] = PCMUtils.PCMToFloat(pcm[i], nbBits);
            }
        } else if (encoding == ENCODING_SIGNEDPCM){
            final int[] pcm = (int[]) samples;
            for (int i=0;i<channels.length;i++){
                buffer[i] = PCMUtils.PCMSignedToFloat(pcm[i], nbBits);
            }
        } else {
            throw new RuntimeException("Unknowned encoding : "+encoding);
        }

        return buffer;
    }


}
