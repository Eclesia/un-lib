
package science.unlicense.media.api;

import science.unlicense.image.api.Image;

/**
 *
 * @author Johann Sorel
 */
public class DefaultImagePacket extends AbstractMediaPacket implements ImagePacket {

    protected final Image image;

    public DefaultImagePacket(ImageTrack meta, long startTime, long timeLength, Image image) {
        super(meta, startTime, timeLength);
        this.image = image;
    }

    public ImageTrack getTrack() {
        return (ImageTrack) super.getTrack();
    }

    public Image getImage() {
        return image;
    }

}
