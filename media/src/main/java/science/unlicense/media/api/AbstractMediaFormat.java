
package science.unlicense.media.api;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.store.DefaultFormat;

/**
 * Abstract media format.
 *
 * @author Johann Sorel
 */
public abstract class AbstractMediaFormat extends DefaultFormat implements MediaFormat {

    public AbstractMediaFormat(Chars identifier) {
        super(identifier);
    }

    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

}
