
package science.unlicense.format.threeds.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class M3DSTriangularMesh extends M3DSChunk {

    public void read(DataInputStream ds) throws IOException {
        size = ds.readInt();
        readSubChunks(6, ds);
    }

}
