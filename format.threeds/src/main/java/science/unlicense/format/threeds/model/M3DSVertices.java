
package science.unlicense.format.threeds.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class M3DSVertices extends M3DSChunk {

    /** vertices : x1,y1,z1 , x2,y2,z2, ... */
    public float[] vertices;

    public void read(DataInputStream ds) throws IOException {
        size = ds.readInt();
        final int nbvertices = ds.readUShort();
        vertices = ds.readFloat(nbvertices*3);
    }

}
