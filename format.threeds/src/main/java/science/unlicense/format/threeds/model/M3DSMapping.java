
package science.unlicense.format.threeds.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class M3DSMapping extends M3DSChunk {

    /** Texture mapping : u1,v1,  u2,v2  ... */
    public float[] uvs;
    public int flag;

    public void read(DataInputStream ds) throws IOException {
        size = ds.readInt();

        final int nbUvs = ds.readUShort();
        uvs = ds.readFloat(nbUvs*2);

        final int position = 6 + 2 + nbUvs*2*4;
        readSubChunks(position, ds);
    }

}
