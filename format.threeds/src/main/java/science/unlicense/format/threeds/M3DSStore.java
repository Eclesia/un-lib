
package science.unlicense.format.threeds;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.api.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.format.threeds.model.M3DSChunk;
import science.unlicense.format.threeds.model.M3DSFaces;
import science.unlicense.format.threeds.model.M3DSMain;
import science.unlicense.format.threeds.model.M3DSMapping;
import science.unlicense.format.threeds.model.M3DSObject;
import science.unlicense.format.threeds.model.M3DSVertices;

/**
 * 3DS storage.
 *
 * @author Johann Sorel
 */
public class M3DSStore extends AbstractModel3DStore{

    public M3DSStore(Object input) {
        super(M3DSFormat.INSTANCE,input);
    }

    public Collection getElements() throws StoreException{

        M3DSChunk mainchunk = null;
        try {
            final ByteInputStream stream = getSourceAsInputStream();
            final DataInputStream ds = new DataInputStream(stream, Endianness.LITTLE_ENDIAN);

            final int id = ds.readShort();
            if (id == M3DSConstants.CHUNK_MAIN){
                mainchunk = new M3DSMain();
                mainchunk.read(ds);
            }
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        if (mainchunk==null) return null;

        final MotionModel root = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);

        final M3DSChunk editor = mainchunk.getChunk(M3DSConstants.CHUNK_EDITOR);
        if (editor!=null){
            final Sequence objects = editor.getChunks(M3DSConstants.CHUNK_EDITOR_OBJECT);

            for (int i=0,n=objects.getSize();i<n;i++){
                final M3DSObject object = (M3DSObject) objects.get(i);
                final M3DSChunk tm = object.getChunk(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH);
                final M3DSVertices vertices = (M3DSVertices) tm.getChunk(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_VERTICES);
                final M3DSFaces faces = (M3DSFaces) tm.getChunk(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_FACES);
                final M3DSMapping uvs = (M3DSMapping) tm.getChunk(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_MAPPINGCOODS);

                final VBO vboVertex = new VBO(vertices.vertices, 3);
                final VBO vboNormal = new VBO(vertices.vertices, 3); //TODO, not right fix this
                final IBO iboFaces = new IBO(faces.faces);

                final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
                shell.setPositions(vboVertex);
                shell.setNormals(vboNormal);
                shell.setIndex(iboFaces);
                shell.setRanges(new IndexedRange[]{new IndexedRange(GLC.GL_TRIANGLES, 3, 0, faces.faces.length)});

                final DefaultModel mesh = new DefaultModel();
                mesh.getTechniques().add(new SimpleBlinnPhong());
                mesh.setTitle(object.name);
                mesh.setShape(shell);
                mesh.updateBoundingBox();
                root.getChildren().add(mesh);
            }

        }

        final Collection col = new ArraySequence();
        col.add(root);
        return col;
    }

}
