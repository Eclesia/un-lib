
package science.unlicense.format.threeds;

/**
 * 3DS file format constants.
 *
 * @author Johann Sorel
 */
public class M3DSConstants {

    private M3DSConstants(){}

    public static final byte[] SIGNATURE = new byte[]{0x4D,0x4D};

    public static final int CHUNK_MAIN = 0x4D4D;

    public static final int CHUNK_EDITOR = 0x3D3D;
    public static final int CHUNK_EDITOR_OBJECT = 0x4000;
    public static final int CHUNK_EDITOR_OBJECT_TRIANGLEMESH = 0x4100;
    public static final int CHUNK_EDITOR_OBJECT_TRIANGLEMESH_VERTICES = 0x4110;
    public static final int CHUNK_EDITOR_OBJECT_TRIANGLEMESH_FACES    = 0x4120;
    public static final int CHUNK_EDITOR_OBJECT_TRIANGLEMESH_FACES_MATERIAL  = 0x4130;
    public static final int CHUNK_EDITOR_OBJECT_TRIANGLEMESH_FACES_SMOOTHING = 0x4150;
    public static final int CHUNK_EDITOR_OBJECT_TRIANGLEMESH_MAPPINGCOODS = 0x4140;
    public static final int CHUNK_EDITOR_OBJECT_TRIANGLEMESH_LOCALCOORDS  = 0x4160;
    public static final int CHUNK_EDITOR_OBJECT_LIGHT  = 0x4600;
    public static final int CHUNK_EDITOR_OBJECT_LIGHT_SPOT = 0x4610;
    public static final int CHUNK_EDITOR_OBJECT_CAMERA = 0x4700;
    public static final int CHUNK_EDITOR_MATERIAL = 0xAFFF;
    public static final int CHUNK_EDITOR_MATERIAL_NAME       = 0xA000;
    public static final int CHUNK_EDITOR_MATERIAL_AMBIANT    = 0xA010;
    public static final int CHUNK_EDITOR_MATERIAL_DIFFUSE    = 0xA020;
    public static final int CHUNK_EDITOR_MATERIAL_SPECULAR   = 0xA030;
    public static final int CHUNK_EDITOR_MATERIAL_TEXTURE    = 0xA200;
    public static final int CHUNK_EDITOR_MATERIAL_BUMP       = 0xA230;
    public static final int CHUNK_EDITOR_MATERIAL_REFLECTION = 0xA220;
    public static final int CHUNK_EDITOR_MATERIAL_REFLECTION_FILENAME   = 0xA300;
    public static final int CHUNK_EDITOR_MATERIAL_REFLECTION_PARAMETERS = 0xA351;

    public static final int CHUNK_KEYFRAME = 0xB000;
    public static final int CHUNK_KEYFRAME_INFO  = 0xB002;
    public static final int CHUNK_KEYFRAME_LIGHT = 0xB007;
    public static final int CHUNK_KEYFRAME_FRAME = 0xB008;
    public static final int CHUNK_KEYFRAME_FRAME_NAME      = 0xB010;
    public static final int CHUNK_KEYFRAME_FRAME_PIVOT     = 0xB013;
    public static final int CHUNK_KEYFRAME_FRAME_POSITION  = 0xB020;
    public static final int CHUNK_KEYFRAME_FRAME_ROTATION  = 0xB021;
    public static final int CHUNK_KEYFRAME_FRAME_SCALE     = 0xB022;
    public static final int CHUNK_KEYFRAME_FRAME_HIERARCHY = 0xB030;

}
