
package science.unlicense.format.threeds.model;

import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class M3DSObject extends M3DSChunk {

    public Chars name;

    public void read(DataInputStream ds) throws IOException {
        size = ds.readInt();

        //read name
        final ByteSequence namebuffer = new ByteSequence();
        int b=0;
        boolean prepeek = true;
        for (b=ds.readByte();namebuffer.getSize()<20;b=ds.readByte()){
            if (b==0){
                prepeek = false;
                break;
            }
            namebuffer.put((byte) b);
        }
        name = new Chars(namebuffer.toArrayByte());

        //read sub chunks
        int position = 6 + (prepeek ? namebuffer.getSize() : namebuffer.getSize()+1);
        readSubChunks(position, ds);
    }

}
