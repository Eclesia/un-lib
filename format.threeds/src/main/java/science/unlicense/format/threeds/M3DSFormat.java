
package science.unlicense.format.threeds;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * 3DS format.
 *
 * @author Johann Sorel
 */
public class M3DSFormat extends AbstractModel3DFormat {

    public static final M3DSFormat INSTANCE = new M3DSFormat();

    private M3DSFormat() {
        super(new Chars("3DS"));
        shortName = new Chars("3DS");
        longName = new Chars("Autodesk 3ds Max");
        mimeTypes.add(new Chars("application/x-3ds"));
        mimeTypes.add(new Chars("image/x-3ds"));
        extensions.add(new Chars("3ds"));
        extensions.add(new Chars("max"));
        signatures.add(M3DSConstants.SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new M3DSStore(input);
    }

}
