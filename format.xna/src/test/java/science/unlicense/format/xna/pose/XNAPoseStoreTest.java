

package science.unlicense.format.xna.pose;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.model3d.impl.physic.RelativeSkeletonPose;

/**
 *
 * @author Johann Sorel
 */
public class XNAPoseStoreTest {

    @Ignore
    @Test
    public void testRead() throws IOException, StoreException{

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/xna/pose/sample.pose"));

        final XNAPoseStore store = new XNAPoseStore(path);
        final RelativeSkeletonPose pose = (RelativeSkeletonPose) store.getElements().createIterator().next();
        Assert.assertNotNull(pose);

        final Sequence seq = pose.getJointPoses();
        Assert.assertEquals(5, seq.getSize());
        test((JointKeyFrame) seq.get(0),new Chars("elbow"),0,1,2,3,4,5);
        test((JointKeyFrame) seq.get(1),new Chars("head"),0.1,45,-2.3,40,0,8);
        test((JointKeyFrame) seq.get(2),new Chars("left knee"),-3.8,-3,21,-0.01,0,-5.9);
        test((JointKeyFrame) seq.get(3),new Chars("t-shirt 3"),1,0,4,0,9,0);
        test((JointKeyFrame) seq.get(4),new Chars("$ù[_ĝe-*!"),5,3.12,-8,4,0.008,-1.3);

    }

    private void test(JointKeyFrame pose, Chars name, double rx, double ry,
            double rz, double x, double y, double z){
        Assert.assertEquals(name, pose.getJoint());
        Assert.assertEquals(new Vector3f64(x, y, z), pose.getValue().getTranslation());
        Assert.assertEquals(Matrix3x3.createRotationEuler(new Vector3f64(rx, ry, rz)), pose.getValue().getRotation());
    }

    @Ignore
    @Test
    public void testWrite() throws IOException, StoreException{

        final ArrayOutputStream out = new ArrayOutputStream();

        final Path path = Paths.resolve(new Chars("mod:/science/unlicense/format/xna/pose/sample.pose"));

        final XNAPoseStore store = new XNAPoseStore(out);
        final RelativeSkeletonPose pose = new RelativeSkeletonPose();
        pose.getJointPoses().add(new JointKeyFrame(new Chars("elbow"),
                new Vector3f64(0, 1, 2), Quaternion.createFromEuler(new Vector3f64(4, 5, 6)),
                JointKeyFrame.FROM_BASE));
        pose.getJointPoses().add(new JointKeyFrame(new Chars("head"),
                new Vector3f64(0.1,45,-2), Quaternion.createFromEuler(new Vector4f64(3,40,0,8)),
                JointKeyFrame.FROM_BASE));
        pose.getJointPoses().add(new JointKeyFrame(new Chars("left knee"),
                new Vector3f64(-3.8,-3,21), Quaternion.createFromEuler(new Vector3f64(-0.01,0,-5.9)),
                JointKeyFrame.FROM_BASE));
        pose.getJointPoses().add(new JointKeyFrame(new Chars("t-shirt 3"),
                new Vector3f64(1,0,4), Quaternion.createFromEuler(new Vector3f64(0,9,0)),
                JointKeyFrame.FROM_BASE));
        pose.getJointPoses().add(new JointKeyFrame(new Chars("$ù[_ĝe-*!"),
                new Vector3f64(5,3.12,-8), Quaternion.createFromEuler(new Vector3f64(4,0.008,-1.3)),
                JointKeyFrame.FROM_BASE));

        final Sequence col = new ArraySequence();
        col.add(pose);
        store.writeElements(col);

        final byte[] result = out.getBuffer().toArrayByte();
        final byte[] expected = IOUtilities.readAll(path.createInputStream());
        Assert.assertArrayEquals(expected, result);

    }

}
