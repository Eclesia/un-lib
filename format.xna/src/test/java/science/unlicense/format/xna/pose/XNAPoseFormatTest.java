
package science.unlicense.format.xna.pose;

import science.unlicense.format.xna.pose.XNAPoseFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.model3d.api.Model3Ds;

/**
 *
 * @author Johann Sorel
 */
public class XNAPoseFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final Format[] formats = Model3Ds.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof XNAPoseFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("XNA Pose format not found.");
    }

}
