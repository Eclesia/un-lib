
package science.unlicense.format.xna.mesh;

import science.unlicense.common.api.Arrays;
import science.unlicense.format.xna.XNATexture;
import science.unlicense.format.xna.XNAMesh;
import science.unlicense.format.xna.XNAVertex;
import science.unlicense.format.xna.XNABone;
import science.unlicense.format.xna.XNAFace;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.VectorRW;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;

/**
 * XPS reader.
 * No proper doc for this format found.
 * Most of what is bellow has been made analyzing the file structure.
 *
 * @author Johann Sorel
 */
public class XNAMeshReader {

    private XNAMeshStore storage;
    private DataInputStream ds;
    //usefull for debugging
    private long currentOffset = 0;

    public XNAMeshStore read(XNAMeshStore storage) throws IOException {
        this.storage = storage;


        final BacktrackInputStream inStream = new BacktrackInputStream(storage.getSourceAsInputStream());
        inStream.mark();
        //file is in little endian
        ds = new DataInputStream(inStream, Endianness.LITTLE_ENDIAN);

        //try to find out the version
        final byte[] sign = ds.readFully(new byte[2], 0, 2);
        currentOffset+=2;
        if (Arrays.equals(sign, 0, 2, XNAMeshConstants.SIGNATURE, 0)){
            storage.versionType = XNAMeshConstants.VERSION_TYPE_4;
            storage.versionMajor = ds.readUShort();
            storage.versionMinor = ds.readUShort();
            storage.versionUpdate = ds.readUShort();
            currentOffset+=6;

            //somekind of header
            readHeader();

            //there is something here of size 4* header.nb1
            //don't know yet what this is
            final byte[] somedata = ds.readFully(new byte[4*storage.header.nb1]);
            currentOffset += somedata.length;
        } else {
            storage.versionType = XNAMeshConstants.VERSION_TYPE_0;
            storage.versionMajor = -1;
            storage.versionMinor = -1;
            storage.versionUpdate = -1;
            //starts directly by number of bones
            //get back to start
            inStream.rewind();
        }

        //bones
        readBones();

        //meshes
        readMeshes();

        return storage;
    }

    private void readHeader() throws IOException{
        storage.header = new XNAMeshHeader();
        storage.header.name = readChars();
        storage.header.nb1 = ds.readUShort();
        storage.header.nb2 = ds.readUShort();
        currentOffset+=4;
        storage.header.str1 = readChars();
        storage.header.str2 = readChars();
        storage.header.str3 = readChars();
    }

    private void readBones() throws IOException{
        storage.bones = new XNABone[ds.readInt()];
        currentOffset+=4;
        for (int i=0;i<storage.bones.length;i++){
            storage.bones[i] = new XNABone();
            storage.bones[i].name = readChars();
            storage.bones[i].parentId = ds.readShort();
            storage.bones[i].position = new Vector3f64(ds.readFloat(), ds.readFloat(), ds.readFloat());
            currentOffset+=14;
        }
    }

    private void readMeshes() throws IOException{
        storage.meshes = new XNAMesh[ds.readInt()];
        currentOffset+=4;
        for (int i=0;i<storage.meshes.length;i++){
            final XNAMesh mesh = new XNAMesh();
            storage.meshes[i] = mesh;

            mesh.name = readChars();

            //read textures
            mesh.nbUV = ds.readInt();
            mesh.textures = new XNATexture[ds.readInt()];
            currentOffset+=8;
            for (int k=0; k<mesh.textures.length; k++){
                mesh.textures[k] = readTexture();
            }

            //read vertices
            mesh.vertices = new XNAVertex[ds.readInt()];
            currentOffset+=4;
            for (int k=0; k<mesh.vertices.length; k++){
                mesh.vertices[k] = readVertex(mesh.nbUV);
            }

            //read faces
            mesh.faces = new XNAFace[ds.readInt()];
            currentOffset+=4;
            for (int k=0; k<mesh.faces.length; k++){
                mesh.faces[k] = readFace();
            }
        }
    }

    private XNATexture readTexture() throws IOException{
        final XNATexture t = new XNATexture();
        t.name = readChars();
        t.uvID = ds.readInt();
        currentOffset+=4;
        return t;
    }

    private XNAVertex readVertex(final int nbUV) throws IOException{
        final XNAVertex v = new XNAVertex();
        v.position = new Vector3f64(ds.readFloat(),ds.readFloat(),ds.readFloat());
        v.normal = new Vector3f64(ds.readFloat(),ds.readFloat(),ds.readFloat());
        v.rgba = new int[]{ds.readUByte(),ds.readUByte(),
                           ds.readUByte(),ds.readUByte()};
        currentOffset+=28;

        //read texture coordinates
        v.uvs = new VectorRW[nbUV];
        for (int i=0;i<v.uvs.length;i++){
            v.uvs[i] = new Vector2f64(ds.readFloat(),ds.readFloat());
            currentOffset+=8;
        }

        if (storage.versionType == XNAMeshConstants.VERSION_TYPE_0){
            //read tangents
            v.tangents = new VectorRW[nbUV];
            for (int i=0;i<v.tangents.length;i++){
                v.tangents[i] = new Vector4f64(ds.readFloat(),ds.readFloat(),
                                           ds.readFloat(),ds.readFloat());
                currentOffset+=16;
            }
            //read bone index
            v.boneIndex = new int[]{ds.readUShort(),ds.readUShort(),
                                    ds.readUShort(),ds.readUShort()};
            currentOffset+=8;
            //read bone weights
            v.boneWeight = new float[]{ds.readFloat(),ds.readFloat(),
                                       ds.readFloat(),ds.readFloat()};
            currentOffset+=16;


        } else if (storage.versionType == XNAMeshConstants.VERSION_TYPE_4){

            if (storage.versionMinor==1){
                //read tangents
                v.tangents = new VectorRW[nbUV];
                for (int i=0;i<v.tangents.length;i++){
                    v.tangents[i] = new Vector4f64(ds.readFloat(),ds.readFloat(),
                                               ds.readFloat(),ds.readFloat());
                    currentOffset+=16;
                }
                //read bone index
                v.boneIndex = new int[]{ds.readUShort(),ds.readUShort(),
                                        ds.readUShort(),ds.readUShort()};
                currentOffset+=8;
                //read bone weights
                v.boneWeight = new float[]{ds.readFloat(),ds.readFloat(),
                                           ds.readFloat(),ds.readFloat()};
                currentOffset+=16;
            } else if (storage.versionMinor>=2){
                //TODO rebuild proper tangents
                v.tangents = new VectorRW[nbUV];
                for (int i=0;i<v.tangents.length;i++){
                    v.tangents[i] = new Vector4f64(0,0,1,1);
                }

                //read bone index
                v.boneIndex = new int[]{ds.readUShort(),ds.readUShort(),
                                        ds.readUShort(),ds.readUShort()};
                currentOffset+=8;
                //read bone weights
                v.boneWeight = new float[]{ds.readFloat(),ds.readFloat(),
                                           ds.readFloat(),ds.readFloat()};
                currentOffset+=16;
            }

        }

        return v;
    }

    private XNAFace readFace() throws IOException{
        final XNAFace f = new XNAFace();
        f.v1 = ds.readInt();
        f.v2 = ds.readInt();
        f.v3 = ds.readInt();
        currentOffset+=12;
        return f;
    }

    private Chars readChars() throws IOException{
        int size = ds.readUByte();
        currentOffset+=1;
        if (size>127){
            //size is on 2 bytes
            size = (size & 0x7F) + (ds.readUByte()<<7);
            currentOffset+=1;
        }
        currentOffset+=size;
        return new Chars(ds.readFully(new byte[size]));
    }

}
