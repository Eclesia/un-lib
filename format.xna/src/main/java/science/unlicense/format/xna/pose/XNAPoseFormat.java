
package science.unlicense.format.xna.pose;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class XNAPoseFormat extends AbstractModel3DFormat {

    public static final XNAPoseFormat INSTANCE = new XNAPoseFormat();

    private XNAPoseFormat() {
        super(new Chars("xna_pose"));
        shortName = new Chars("XNA-Pose");
        longName = new Chars("XNA Model Pose file");
        extensions.add(new Chars("pose"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new XNAPoseStore(input);
    }

}
