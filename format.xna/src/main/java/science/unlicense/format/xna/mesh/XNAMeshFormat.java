
package science.unlicense.format.xna.mesh;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class XNAMeshFormat extends AbstractModel3DFormat {

    public static final XNAMeshFormat INSTANCE = new XNAMeshFormat();

    private XNAMeshFormat() {
        super(new Chars("xna_mesh"));
        shortName = new Chars("XNA-Mesh-Binary");
        longName = new Chars("XNA Model Binary file");
        extensions.add(new Chars("mesh"));
        extensions.add(new Chars("xps"));
        signatures.add(XNAMeshConstants.SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new XNAMeshStore(input);
    }

}
