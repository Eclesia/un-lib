
package science.unlicense.format.xna;

import science.unlicense.math.api.VectorRW;


/**
 *
 * @author Johann Sorel
 */
public class XNAVertex {

    /** size 3 */
    public VectorRW position;
    /** size 3 */
    public VectorRW normal;
    /** size 4 */
    public int[] rgba;

    /** one for each uv layer, each element size 2 */
    public VectorRW[] uvs;
    /** one for each uv layer, each element size 4 */
    public VectorRW[] tangents;

    /** rigging infos, only if bones are present, size 4 */
    public int[] boneIndex;
    public float[] boneWeight;

}
