

package science.unlicense.format.xna.meshascii;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class XNAAsciiFormat extends AbstractModel3DFormat {

    public static final XNAAsciiFormat INSTANCE = new XNAAsciiFormat();

    private XNAAsciiFormat() {
        super(new Chars("xna_mesh"));
        shortName = new Chars("XNA-Mesh-ASCII");
        longName = new Chars("XNA Model ASCII file");
        extensions.add(new Chars("mesh.ascii"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new XNAAsciiStore(input);
    }

}
