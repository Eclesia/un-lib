

package science.unlicense.format.xna.meshascii;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.format.xna.AbstractXNAStore;
import science.unlicense.format.xna.XNABone;
import science.unlicense.format.xna.XNAFace;
import science.unlicense.format.xna.XNAMesh;
import science.unlicense.format.xna.XNATexture;
import science.unlicense.format.xna.XNAVertex;

/**
 *
 * @author Johann Sorel
 */
public class XNAAsciiStore extends AbstractXNAStore{

    public XNAAsciiStore(Object input) {
        super(XNAAsciiFormat.INSTANCE,input);
    }

    protected void read() throws IOException{
        final ByteInputStream stream = getSourceAsInputStream();
        final CharInputStream cs = new CharInputStream(stream,CharEncodings.UTF_8,new Char('\n'));
        readBones(cs);
        readMeshes(cs);
    }

    private void readBones(CharInputStream cs) throws IOException{
        Chars line = cs.readLine();
        final int nbBone = Int32.decode(line.createIterator(), false);
        bones = new XNABone[nbBone];

        for (int i=0;i<nbBone;i++){
            bones[i] = new XNABone();
            bones[i].name = cs.readLine().trim();
            bones[i].parentId = Int32.decode(cs.readLine().createIterator(), false);
            final Chars[] coords = cs.readLine().trim().split(' ');
            bones[i].position = new Vector3f64(
                    Float64.decode(coords[0]),
                    Float64.decode(coords[1]),
                    Float64.decode(coords[2]));
        }
    }

    private void readMeshes(CharInputStream cs) throws IOException{
        Chars line = cs.readLine();
        final int nbMeshes = Int32.decode(line.createIterator(), false);
        meshes = new XNAMesh[nbMeshes];

        for (int i=0;i<nbMeshes;i++){
            meshes[i] = new XNAMesh();
            meshes[i].name = cs.readLine();
            final int nbUV = Int32.decode(cs.readLine().createIterator(), false);
            final int nbTexture = Int32.decode(cs.readLine().createIterator(), false);

            //read textures
            meshes[i].textures = new XNATexture[nbTexture];
            for (int t=0;t<nbTexture;t++){
                meshes[i].textures[t] = new XNATexture();
                meshes[i].textures[t].name = cs.readLine().trim();
                meshes[i].textures[t].uvID = Int32.decode(cs.readLine().createIterator(), false);
            }

            //read vertices
            final int nbVertices = Int32.decode(cs.readLine().createIterator(), false);
            meshes[i].vertices = new XNAVertex[nbVertices];
            for (int t=0;t<nbVertices;t++){
                meshes[i].vertices[t] = new XNAVertex();
                Chars[] parts = cs.readLine().trim().split(' ');
                meshes[i].vertices[t].position = new Vector3f64(
                    Float64.decode(parts[0]),
                    Float64.decode(parts[1]),
                    Float64.decode(parts[2]));
                parts = cs.readLine().trim().split(' ');
                meshes[i].vertices[t].normal = new Vector3f64(
                    Float64.decode(parts[0]),
                    Float64.decode(parts[1]),
                    Float64.decode(parts[2]));
                parts = cs.readLine().trim().split(' ');
                meshes[i].vertices[t].rgba = new int[]{
                    Int32.decode(parts[0]),
                    Int32.decode(parts[1]),
                    Int32.decode(parts[2]),
                    Int32.decode(parts[3])};

                //read texture uv, one for each uv layer
                meshes[i].vertices[t].uvs = new VectorRW[nbUV];
                meshes[i].vertices[t].tangents = new VectorRW[nbUV];
                for (int k=0;k<nbUV;k++){
                    parts = cs.readLine().trim().split(' ');
                    meshes[i].vertices[t].uvs[k] = new Vector2f64(
                        Float64.decode(parts[0]),
                        Float64.decode(parts[1]));
                    //TODO calculate a proper tangent
                    meshes[i].vertices[t].tangents[k] = new Vector4f64(0,0,1,1);
                }

                parts = cs.readLine().trim().split(' ');
                meshes[i].vertices[t].boneIndex = new int[parts.length];
                for (int k=0;k<meshes[i].vertices[t].boneIndex.length;k++){
                    meshes[i].vertices[t].boneIndex[k] = Int32.decode(parts[k]);
                }

                parts = cs.readLine().trim().split(' ');
                meshes[i].vertices[t].boneWeight = new float[parts.length];
                for (int k=0;k<meshes[i].vertices[t].boneWeight.length;k++){
                    meshes[i].vertices[t].boneWeight[k] = (float) Float64.decode(parts[k]);
                }
            }

            //read faces
            final int nbFaces = Int32.decode(cs.readLine().createIterator(), false);
            meshes[i].faces = new XNAFace[nbFaces];
            for (int f=0;f<nbFaces;f++){
                meshes[i].faces[f] = new XNAFace();
                Chars[] parts = cs.readLine().trim().split(' ');
                meshes[i].faces[f].v1 = Int32.decode(parts[0]);
                meshes[i].faces[f].v2 = Int32.decode(parts[1]);
                meshes[i].faces[f].v3 = Int32.decode(parts[2]);
            }
        }
    }

}
