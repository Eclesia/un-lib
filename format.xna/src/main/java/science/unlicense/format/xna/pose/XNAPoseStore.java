

package science.unlicense.format.xna.pose;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.number.Float64;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vectors;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.model3d.impl.physic.RelativeSkeletonPose;

/**
 * Read XNA .pose file.
 *
 * Poses are in Right-Handed coordinate system.
 *
 * @author Johann Sorel
 */
public class XNAPoseStore extends AbstractModel3DStore{

    private RelativeSkeletonPose poseSkeleton;

    public XNAPoseStore(Object input) {
        super(XNAPoseFormat.INSTANCE, input);
    }

    public Collection getElements() throws StoreException {
        final ArraySequence col = new ArraySequence();
        try {
            read();
            col.add(poseSkeleton);
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        return col;
    }

    private void read() throws IOException{

        final RelativeSkeletonPose spose = new RelativeSkeletonPose();

        final ByteInputStream bs = getSourceAsInputStream();
        final CharInputStream cs = new CharInputStream(bs,CharEncodings.UTF_8,new Char('\n'));

        Chars line;
        for (line=cs.readLine();line!=null;line=cs.readLine()){
            line = line.trim();
            if (line.isEmpty()) continue;

            final Chars[] parts = line.split(':');
            if (parts.length != 2){
                throw new IOException("Unvalid line pattern, missing ':' for line : "+line);
            }

            final Chars[] coords = parts[1].trim().split(' ');
            if (coords.length < 6){
                //some files contain more then 6 values, perhaps scale, we can ignore them
                throw new IOException("Unvalid line pattern, was expexting 6 values for line : "+line);
            }
            final double[] euler = new double[]{
                    Angles.degreeToRadian(Float64.decode(coords[0])+180.0),
                    Angles.degreeToRadian(Float64.decode(coords[1])+180.0),
                    Angles.degreeToRadian(Float64.decode(coords[2])+180.0)
                };
            Vectors.clampEuler(euler);
            final VectorRW position = new Vector3f64(
                    Float64.decode(coords[3]),
                    Float64.decode(coords[4]),
                    Float64.decode(coords[5]));
            final Quaternion rotation = Matrix3x3.createRotationEuler(VectorNf64.create(euler)).toQuaternion();

            final JointKeyFrame jpose = new JointKeyFrame();
            jpose.setFrom(JointKeyFrame.FROM_ORIGIN);
            jpose.setJoint(parts[0].trimEnd());
            jpose.getValue().getTranslation().set(position);
            jpose.getValue().getRotation().set(rotation.toMatrix3());
            jpose.getValue().notifyChanged();
            spose.getJointPoses().add(jpose);
        }

        poseSkeleton = spose;
    }

    public void writeElements(Collection elements) throws StoreException {
        final Iterator ite = elements.createIterator();
        while (ite.hasNext()){
            final Object candidate = ite.next();
            if (candidate instanceof RelativeSkeletonPose){
                try {
                    write((RelativeSkeletonPose) candidate);
                } catch (IOException ex) {
                    throw new StoreException(ex);
                }
            } else {
                throw new StoreException("Unsupported object type : "+candidate);
            }
        }
    }

    private void write(final RelativeSkeletonPose pose) throws IOException {

        final ByteOutputStream out = getSourceAsOutputStream();
        final CharOutputStream cs = new CharOutputStream(out, CharEncodings.UTF_8);

        final Iterator ite = pose.getJointPoses().createIterator();
        while (ite.hasNext()){
            final JointKeyFrame jp = (JointKeyFrame) ite.next();
            final Chars name = CObjects.toChars(jp.getJoint());
            cs.write(name).write(':');
            final VectorRW position = jp.getValue().getTranslation();
            final VectorRW euler = new Matrix3x3(jp.getValue().getRotation()).toEuler();
            for (int i=0;i<3;i++){
                cs.write(' ').write(Float64.encode(euler.get(i)));
            }
            for (int i=0;i<3;i++){
                cs.write(' ').write(Float64.encode(position.get(i)));
            }
            cs.endLine();
        }

    }

}
