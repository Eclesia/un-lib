
package science.unlicense.format.xna;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class XNATexture {

    public Chars name;
    public int uvID;

}
