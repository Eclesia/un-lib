

package science.unlicense.format.xna;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.CSUtilities;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * Models are in Right-Handed coordinate system.
 *
 * @author Johann Sorel
 */
public abstract class AbstractXNAStore extends AbstractModel3DStore{

    public Path base;
    public XNABone[] bones;
    public XNAMesh[] meshes;

    public AbstractXNAStore(Format format,Object input) {
        super(format,input);
        base = ((Path) input).getParent();
    }

    public Collection getElements() throws StoreException {
        if (meshes==null){
            try {
                read();
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }

        final MotionModel root = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);

        final boolean hasSkeleton = bones.length>0;

        //rebuild skeleton
        Skeleton skeleton = null;
        if (hasSkeleton){
            skeleton = new Skeleton();
            for (int i=0;i<bones.length;i++){
                final Joint joint = new Joint(3);
                joint.setId(i);
                joint.setTitle(bones[i].name);
                joint.getNodeTransform().getTranslation().set(bones[i].position);
                joint.getNodeTransform().notifyChanged();
                skeleton.getChildren().add(joint);
            }
            //rebuild hierarchy
            for (int i=bones.length-1;i>=0;i--){
                final Joint jt = (Joint) skeleton.getChildren().get(i);
                if (bones[i].parentId>=0){
                    final Joint parent = (Joint) skeleton.getChildren().get(bones[i].parentId);
                    parent.getChildren().add(jt);
                }
            }
            root.setSkeleton(skeleton);
        }

        //rebuil meshes
        final Dictionary cache = new HashDictionary();

        for (int i=0;i<meshes.length;i++){
            //rebuild mesh faces
            final XNAMesh xnamesh = meshes[i];

            final DefaultModel glmesh = new DefaultModel();
            glmesh.getTechniques().add(new SimpleBlinnPhong());
            glmesh.setTitle(xnamesh.name);

            //rebuild vertex and normals
            final int maxBonePerVertex = 4;
            final Float32Cursor vertex = DefaultBufferFactory.INSTANCE.createFloat32(xnamesh.vertices.length*3).cursor();
            final Float32Cursor normals = DefaultBufferFactory.INSTANCE.createFloat32(xnamesh.vertices.length*3).cursor();
            final Float32Cursor boneweights = hasSkeleton ? DefaultBufferFactory.INSTANCE.createFloat32(xnamesh.vertices.length*4).cursor() : null;
            final Int32Cursor boneindexes = hasSkeleton ? DefaultBufferFactory.INSTANCE.createInt32(xnamesh.vertices.length*4).cursor() : null;

            for (int k=0;k<xnamesh.vertices.length;k++){
                vertex.write(xnamesh.vertices[k].position.toFloat());
                normals.write(xnamesh.vertices[k].normal.toFloat());
                if (hasSkeleton){
                    boneweights.write(xnamesh.vertices[k].boneWeight);
                    boneindexes.write(xnamesh.vertices[k].boneIndex);
                }
            }

            //rebuil triangle indices
            final Int32Cursor indices = DefaultBufferFactory.INSTANCE.createInt32(xnamesh.faces.length*3).cursor();
            for (int k=0;k<xnamesh.faces.length;k++){
                indices.write(xnamesh.faces[k].v1);
                indices.write(xnamesh.faces[k].v2);
                indices.write(xnamesh.faces[k].v3);
            }

            //rebuild material
            final Float32Cursor uv = DefaultBufferFactory.INSTANCE.createFloat32(xnamesh.vertices.length*2).cursor();
            final Float32Cursor tangents = DefaultBufferFactory.INSTANCE.createFloat32(xnamesh.vertices.length*4).cursor();
            for (int l=0;l<xnamesh.vertices.length;l++){
                uv.write(xnamesh.vertices[l].uvs[0].toFloat());
                final float[] normal = xnamesh.vertices[l].normal.toFloat();
                final float[] tangent = xnamesh.vertices[l].tangents[0].toFloat();
                tangents.write(tangent);
            }

            final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
            shell.setPositions(new VBO(vertex.getBuffer(),3));
            shell.setNormals(new VBO(normals.getBuffer(),3));
            shell.setIndex(new IBO(indices.getBuffer()));
            shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, (int) indices.getBuffer().getNumbersSize())});
            shell.setUVs(new VBO(uv.getBuffer(), 2));
            shell.setTangents(new VBO(tangents.getBuffer(),4));
            Skin skin = null;
            if (hasSkeleton){
                skin = new Skin();
                skin.setMaxWeightPerVertex(maxBonePerVertex);
                skin.setSkeleton(skeleton);
                shell.getAttributes().add(Skin.ATT_WEIGHTS_0, new VBO(boneweights.getBuffer(),maxBonePerVertex));
                shell.getAttributes().add(Skin.ATT_JOINTS_0, new VBO(boneindexes.getBuffer(),maxBonePerVertex));
            }
            glmesh.setShape(shell);
            glmesh.setSkin(skin);
            // TODO : only triangle clockwise seams to be inverted.
            // find a way to confirm this
            CSUtilities.invertHandCS(shell.getIndex(), shell.getRanges());

            final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
            glmesh.getMaterials().add(material);

            for (int k=0;k<xnamesh.textures.length && k<3;k++){
                final XNATexture texture = xnamesh.textures[k];

                try{
                    Texture2D tex = (Texture2D) cache.getValue(texture.name.toString());
                    if (tex == null){
                        final String[] parts = texture.name.toString().split("\\\\");
                        Path imagePath = base.resolve(new Chars(parts[parts.length-1]));
                        imagePath = checkExist(imagePath);
                        Image image = Images.read(imagePath);
                        tex = new Texture2D(image);
                        cache.add(texture.name.toString(), tex);
                    }

                    switch (k) {
                        case 0 : material.setDiffuseTexture(new TextureMapping(tex));
                        case 1 : material.setSpecularTexture(new TextureMapping(tex));
                        case 2 : material.setNormalTexture(new TextureMapping(tex));
                    }

                }catch(IOException ex){
                    throw new StoreException(ex);
                }
            }

            glmesh.updateBoundingBox();
            root.getChildren().add(glmesh);
        }


        if (skeleton!=null){
            skeleton.reverseWorldPose();
            skeleton.updateBindPose();
            skeleton.updateInvBindPose();
        }

        final Collection col = new ArraySequence();
        col.add(root);
        return col;
    }

    protected abstract void read() throws IOException ;

    private Path checkExist(final Path path){
        final Path parent = path.getParent();
        //try to open this path
        try{
            final ByteInputStream stream = path.createInputStream();
            stream.dispose();
            return path;
        }catch(IOException ex){
            //file do not exist, try to find a file with same name but case insensitive.
        }

        final Iterator ite = parent.getChildren().createIterator();
        while (ite.hasNext()) {
            final Path child = (Path) ite.next();
            if (child.getName().equals(path.getName(),true,true)){
                return child;
            }
        }

        return path;
    }

}
