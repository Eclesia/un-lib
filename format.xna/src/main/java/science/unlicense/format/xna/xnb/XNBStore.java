
package science.unlicense.format.xna.xnb;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.xna.xnb.model.XNBHeader;
import science.unlicense.common.api.number.Endianness;

/**
 *
 * @author Johann Sorel
 */
public class XNBStore extends AbstractModel3DStore {

    public XNBStore(Object input) {
        super(XNBFormat.INSTANCE, input);
    }

    @Override
    public Collection getElements() throws StoreException {
        return null;
    }

    private void readAll() throws IOException{

        final ByteInputStream is = getSourceAsInputStream();
        final DataInputStream ds = new DataInputStream(is,Endianness.LITTLE_ENDIAN);

        if (!Arrays.equals(XNBConstants.SIGNATURE,ds.readFully(new byte[3]))){
            throw new IOException("Unvalid XNB file signature.");
        }

        final XNBHeader header = new XNBHeader();
        header.read(ds);

        if ((header.flags & 0x80) != 0){
            throw new IOException("Compressed XNB not supported yet.");
        }

        final int nbTypes = (int) ds.readVarLengthUInt();
        for (int i=0;i<nbTypes;i++){
            final Chars typeReaderName = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
            final int typeReaderVersion = ds.readInt();
        }

        final int nbResource = (int) ds.readVarLengthUInt();
        final Object mainAsset;
        for (int i=0;i<nbResource;i++){
            final Object asset;
        }

    }

}
