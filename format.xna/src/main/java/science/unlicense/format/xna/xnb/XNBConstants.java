
package science.unlicense.format.xna.xnb;

/**
 *
 * @author Johann Sorel
 */
public final class XNBConstants {

    public static final byte[] SIGNATURE = new byte[]{'X','N','B'};

    private XNBConstants(){}

}
