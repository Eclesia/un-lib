
package science.unlicense.format.xna.mesh;

/**
 * XPS/Mesh constants.
 *
 * @author Johann Sorel
 */
public final class XNAMeshConstants {

    /**
     * If version is VERSION_TYPE_1,
     * file will start by this signature.
     * MAGIC = -96,-18
     * followed by :
     * MAJOR VERSION = 2 bytes
     * MINOR VERSION = 2 bytes
     * UPDATE VERSION = 2 bytes
     */
    public static final byte[] SIGNATURE = new byte[]{-96,-18};

    /**
     * XPS/Mesh files without header.
     */
    public static final int VERSION_TYPE_0 = 0;

    /**
     * XPS/Mesh files with header.
     * Use in XNA version 4+
     */
    public static final int VERSION_TYPE_4 = 1;

    private XNAMeshConstants(){}

}
