
package science.unlicense.format.xna.mesh;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.xna.AbstractXNAStore;

/**
 *
 * @author Johann Sorel
 */
public class XNAMeshStore extends AbstractXNAStore{

    public int versionType;
    public int versionMajor;
    public int versionMinor;
    public int versionUpdate;
    public XNAMeshHeader header;

    public XNAMeshStore(Object input) {
        super(XNAMeshFormat.INSTANCE,input);
    }

    @Override
    protected void read() throws IOException {
        final XNAMeshReader reader = new XNAMeshReader();
        reader.read(this);
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(header.toString());
        cb.append("\n");
        cb.append("BONES ").append(bones.length).append("\n");
        for (int i=0;i<bones.length;i++){
            cb.append(i).append(" ").append(bones[i]).append("\n");
        }
        cb.append("MESHES ").append(meshes.length).append("\n");
        for (int i=0;i<meshes.length;i++){
            cb.append(i).append(meshes[i]).append("\n");
        }
        return cb.toChars();
    }

}
