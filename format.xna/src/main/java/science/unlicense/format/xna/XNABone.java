
package science.unlicense.format.xna;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class XNABone extends CObject{

    public Chars name;
    public int parentId;
    public VectorRW position;

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("bone : ");
        cb.append(name).append(" ").append(parentId).append(" ").append(position);
        return cb.toChars();
    }

}
