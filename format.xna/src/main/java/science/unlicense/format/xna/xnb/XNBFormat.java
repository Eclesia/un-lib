
package science.unlicense.format.xna.xnb;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * Specification :
 * http://xbox.create.msdn.com/en-US/sample/xnb_format
 *
 * @author Johann Sorel
 */
public class XNBFormat extends AbstractModel3DFormat{

    public static final XNBFormat INSTANCE = new XNBFormat();

    private XNBFormat() {
        super(new Chars("xnb"));
        shortName = new Chars("XNA-GameStudio-Binary");
        longName = new Chars("XNA Game studio Binary file");
        extensions.add(new Chars("xnb"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new XNBStore(input);
    }

}

