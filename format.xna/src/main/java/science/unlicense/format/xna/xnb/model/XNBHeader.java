
package science.unlicense.format.xna.xnb.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class XNBHeader {

    /**
     * 'w' = Windows
     * 'm' = Windows Phone
     * 'x' = Xbox 360
     */
    public byte targetPlatform;
    /**
     * Should be 5.
     */
    public byte formatVersion;
    /**
     * 0x01 : HiDef profile
     * 0x80 : compressed file
     */
    public byte flags;

    public int compressedSize;
    public int decompressedSize;

    public void read(DataInputStream ds) throws IOException{
        targetPlatform = ds.readByte();
        formatVersion = ds.readByte();
        flags = ds.readByte();
        compressedSize = ds.readInt();
        decompressedSize = ds.readInt();
    }

    public void write(DataOutputStream ds) throws IOException{
        throw new UnsupportedOperationException("TODO");
    }

}
