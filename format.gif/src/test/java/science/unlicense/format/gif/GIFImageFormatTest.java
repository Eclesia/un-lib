
package science.unlicense.format.gif;

import science.unlicense.format.gif.GIFImageFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class GIFImageFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final ImageFormat[] formats = Images.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof GIFImageFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("Image format not found.");
    }

}
