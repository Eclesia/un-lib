
package science.unlicense.format.gif.model;

import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.Maths;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.format.gif.GIFMetaData;
import static science.unlicense.format.gif.GIFMetaData.*;

/**
 *
 * @author Johann Sorel
 */
public class PlainText extends Extension {

    public int glp;
    public int gtp;
    public int gw;
    public int gh;
    public int cw;
    public int ch;
    public int fc;
    public int bc;
    public Chars text;

    public void read(DataInputStream ds) throws IOException{
        size = ds.readUByte();
        glp = ds.readUShort();
        gtp = ds.readUShort();
        gw = ds.readUShort();
        gh = ds.readUShort();
        cw = ds.readUByte();
        ch = ds.readUByte();
        fc = ds.readUByte();
        bc = ds.readUByte();

        //read sub blocks
        final ByteSequence buffer = new ByteSequence();
        for (int bs = ds.readUByte(); bs!=0; bs=ds.readUByte()){
            buffer.put(ds.readFully(new byte[bs]));
        }
        text = new Chars(buffer.toArrayByte());
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(size);
        ds.writeUShort(glp);
        ds.writeUShort(gtp);
        ds.writeUShort(gw);
        ds.writeUShort(gh);
        ds.writeUShort(cw);
        ds.writeUShort(ch);
        ds.writeUShort(fc);
        ds.writeUShort(bc);

        final byte[] dataBytes = text.toBytes();
        int index = 0;
        while (index<dataBytes.length){
            final int nb = Maths.min(dataBytes.length-index,256);
            ds.write(dataBytes, index, nb);
            index+=nb;
        }
        ds.writeUByte(0);
    }

    public TypedNode toNode(){
        final TypedNode node = new DefaultTypedNode(MD_GIF_PLAINTEXT);
        node.getOrCreateChild(MD_GIF_PLAINTEXT_GLP).setValue(glp);
        node.getOrCreateChild(MD_GIF_PLAINTEXT_GTP).setValue(gtp);
        node.getOrCreateChild(MD_GIF_PLAINTEXT_GW).setValue(gw);
        node.getOrCreateChild(MD_GIF_PLAINTEXT_GH).setValue(gh);
        node.getOrCreateChild(MD_GIF_PLAINTEXT_CW).setValue(cw);
        node.getOrCreateChild(MD_GIF_PLAINTEXT_CH).setValue(ch);
        node.getOrCreateChild(MD_GIF_PLAINTEXT_FC).setValue(fc);
        node.getOrCreateChild(MD_GIF_PLAINTEXT_BC).setValue(bc);
        node.getOrCreateChild(GIFMetaData.MD_GIF_PLAINTEXT_TEXT).setValue(text);
        return node;
    }

}
