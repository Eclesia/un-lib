package science.unlicense.format.gif;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.DefaultMetadataGroup;
import science.unlicense.common.api.model.Metadata;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import static science.unlicense.format.gif.GIFMetaData.*;
import science.unlicense.format.gif.model.Application;
import science.unlicense.format.gif.model.Comment;
import science.unlicense.format.gif.model.Extension;
import science.unlicense.format.gif.model.GraphicControl;
import science.unlicense.format.gif.model.Header;
import science.unlicense.format.gif.model.ImageDescriptor;
import science.unlicense.format.gif.model.PlainText;
import science.unlicense.format.gif.model.ScreenDescriptor;
import science.unlicense.image.api.AbstractImageReader;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import science.unlicense.image.api.ImageSetMetadata;

/**
 * http://www.w3.org/Graphics/GIF/spec-gif89a.txt
 *
 * @author Johann Sorel
 */
public class GIFImageReader extends AbstractImageReader{

    /**
     * Gif Header.
     */
    private Header header;
    /**
     * Gif screen descriptor.
     */
    private ScreenDescriptor screenDesc;
    /**
     * Contains the different images available.
     */
    private final Sequence imageDescs = new ArraySequence();

    public GIFImageReader() {
    }

    Header getHeader() {
        return header;
    }

    ScreenDescriptor getScreenDesc() {
        return screenDesc;
    }

    Sequence getImageDescs() {
        return imageDescs;
    }

    BacktrackInputStream getSubStream() throws IOException{
        return getInputAsBacktrackStream();
    }

    @Override
    protected Dictionary readMetadatas(final BacktrackInputStream stream) throws IOException {
        explore(stream);

        final ImageSetMetadata.Image[] stMetas = new ImageSetMetadata.Image[imageDescs.getSize()];
        final Metadata[] gfMetas = new Metadata[imageDescs.getSize()+2];
        gfMetas[0] = header;
        gfMetas[1] = screenDesc;
        for (int i=0;i<stMetas.length;i++){
            final GIFImage gifi = (GIFImage) imageDescs.get(i);
            stMetas[i] = gifi.getStandardImageMeta();
            gfMetas[i+2] = gifi.getGifImageMeta();
        }

        final ImageSetMetadata standardMetas = new ImageSetMetadata();
        standardMetas.getChildren().addAll(stMetas);
        final DefaultMetadataGroup gifMetas = new DefaultMetadataGroup(MD_GIF.getId(),gfMetas);

        final Dictionary metas = new HashDictionary();
        metas.add(standardMetas.getType().getId(), standardMetas);
        metas.add(gifMetas.getName(), gifMetas);
        return metas;
    }

    @Override
    protected Image read(final ImageReadParameters params, final BacktrackInputStream stream) throws IOException {
        explore(stream);
        stream.rewind();

        final GIFImage gifi = (GIFImage) imageDescs.get(params.getImageIndex());

        return gifi.toImage(stream,params.getBufferFactory());
    }

    /**
     * Explore all blocks in the file to find all image definitions.
     * Won't do anything if image has already been explored.
     *
     * @param stream
     * @throws IOException
     */
    private void explore(final BacktrackInputStream stream) throws IOException{
        if (header!=null) return;

        final DataInputStream ds = new DataInputStream(stream, Endianness.LITTLE_ENDIAN);

        //read header
        header = new Header();
        header.offset = stream.position();
        header.read(ds);

        //read image definition
        screenDesc = new ScreenDescriptor();
        screenDesc.offset = stream.position();
        screenDesc.read(ds);

        //prepare container for each image definition
        GIFImage imageDesc = new GIFImage(this,null);

        for (int b=ds.read();b!=-1;b=ds.read()){
            if (b == GIFMetaData.BLOCK_IMAGE_DESC){
                imageDesc.descriptor = new ImageDescriptor();
                imageDesc.descriptor.offset = stream.position()-1;
                imageDesc.descriptor.read(ds);
                imageDescs.add(imageDesc);
                imageDesc.descriptor.dataoffset = stream.position();

                //skip image datas
                for (int bs = ds.readUByte(); bs!=0; bs=ds.readUByte()){
                    ds.skipFully(bs);
                }

                //prepare for next image
                imageDesc = new GIFImage(this, imageDesc);

            } else if (b == GIFMetaData.BLOCK_EXT){
                final int offset = stream.position();
                final int type = ds.readUByte();

                final Extension ext;
                if (type == GIFMetaData.BLOCK_EXT_GRAPHIC_CONTROL){
                    ext = new GraphicControl();
                    ext.read(ds);
                } else if (type == GIFMetaData.BLOCK_EXT_COMMENT){
                    ext = new Comment();
                    ext.read(ds);
                } else if (type == GIFMetaData.BLOCK_EXT_PLAIN_TEXT){
                    ext = new PlainText();
                    ext.read(ds);
                } else if (type == GIFMetaData.BLOCK_EXT_APPLICATION){
                    ext = new Application();
                    ext.read(ds);
                } else {
                    //unknowned extension, skip it
                    ext = new Extension();
                    ext.size = ds.readUByte();
                    ds.skipFully(ext.size);
                    //skip subblocks until terminator
                    for (int bs = ds.readUByte(); bs!=0; bs=ds.readUByte()){
                        ds.skipFully(bs);
                    }
                }
                ext.offset = offset;
                imageDesc.extensions.add(ext);

            } else if (b == GIFMetaData.BLOCK_TRAILER){
                //reached end of gif file
                break;
            } else {
                throw new IOException(ds, "Unexpected block.");
            }
        }
    }

}
