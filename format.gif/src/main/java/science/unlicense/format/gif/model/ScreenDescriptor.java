
package science.unlicense.format.gif.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.TypedNode;
import static science.unlicense.format.gif.GIFMetaData.*;

/**
 *
 * @author Johann Sorel
 */
public class ScreenDescriptor extends Chunk {

    public ColorTable globalColorTable;

    public int screenWidth;
    public int screenHeight;
    public boolean colorTableFlag;
    public int colorResolution;
    public boolean sortFlag;
    public int colorTableSize;
    public int bgColorIndex;
    public int pixelAspectRatio;

    public void read(DataInputStream ds) throws IOException{
        screenWidth = ds.readUShort();
        screenHeight = ds.readUShort();
        colorTableFlag = ds.readBits(1, DataInputStream.MSB) != 0;
        colorResolution = ds.readBits(3, DataInputStream.MSB);
        sortFlag = ds.readBits(1, DataInputStream.MSB) != 0;
        colorTableSize = ds.readBits(3, DataInputStream.MSB);
        bgColorIndex = ds.readUByte();
        pixelAspectRatio = ds.readUByte();

        //read color table
        if (colorTableFlag){
            globalColorTable = new ColorTable();
            globalColorTable.read(ds, colorTableSize);
        }

    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUShort(screenWidth);
        ds.writeUShort(screenHeight);
        ds.writeBit(colorTableFlag?0:1);
        ds.writeBits(colorResolution, 3);
        ds.writeBit(sortFlag?0:1);
        ds.writeBits(colorTableSize, 3);
        ds.writeUByte(bgColorIndex);
        ds.writeUByte(pixelAspectRatio);

        if (colorTableFlag){
            globalColorTable.write(ds);
        }
    }

    public TypedNode toNode(){
        final TypedNode node = new DefaultTypedNode(MD_GIF_SCREEN);
        node.getOrCreateChild(MD_GIF_SCREEN_WIDTH).setValue(screenWidth);
        node.getOrCreateChild(MD_GIF_SCREEN_HEIGHT).setValue(screenHeight);
        node.getOrCreateChild(MD_GIF_SCREEN_GLOBALCOLORTABLE).setValue(colorTableFlag);
        node.getOrCreateChild(MD_GIF_SCREEN_COLORRESOLUTION).setValue(colorResolution);
        node.getOrCreateChild(MD_GIF_SCREEN_SORT).setValue(sortFlag);
        node.getOrCreateChild(MD_GIF_SCREEN_GLOBALCOLORTABLESIZE).setValue(colorTableSize);
        node.getOrCreateChild(MD_GIF_SCREEN_BGCOlORINDEX).setValue(bgColorIndex);
        node.getOrCreateChild(MD_GIF_SCREEN_PIXELASPECTRATIO).setValue(pixelAspectRatio);
        return node;
    }

}
