
package science.unlicense.format.gif.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.tree.TypedNode;

/**
 * An extension block.
 *
 * @author Johann Sorel
 */
public class Extension extends Chunk {

    protected Chars label;

    public Chars getLabel(){
        return label;
    }

    public void setLabel(Chars label){
        this.label = label;
    }

    public void read(DataInputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public TypedNode toNode() {
        throw new UnimplementedException("Not supported yet.");
    }

}
