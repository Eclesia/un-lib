
package science.unlicense.format.gif.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.gif.GIFMetaData.*;

/**
 *
 * @author Johann Sorel
 */
public class Header extends Chunk{

    public Chars version;

    public void read(DataInputStream ds) throws IOException {
        final byte[] signature = ds.readFully(new byte[3]);
        if (!Arrays.equals(signature, SIGNATURE)){
            throw new IOException(ds, "Stream is not a GIF.");
        }
        version = new Chars(ds.readFully(new byte[3]));
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.write(SIGNATURE);
        ds.write(version.toBytes());
    }

    public TypedNode toNode() {
        final TypedNode node = new DefaultTypedNode(MD_GIF_HEADER);
        node.getOrCreateChild(MD_GIF_HEADER_VERSION).setValue(version);
        return node;
    }

}
