
package science.unlicense.format.gif.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.TypedNode;
import static science.unlicense.format.gif.GIFMetaData.*;

/**
 *
 * @author Johann Sorel
 */
public class ImageDescriptor extends Chunk {

    public int lzwmincodesize;
    public int dataoffset;
    public ColorTable localColorTable = null;

    /**
     * Image Left Position - Column number, in pixels, of the left edge
     * of the image, with respect to the left edge of the Logical Screen.
     * Leftmost column of the Logical Screen is 0.
     */
    public int leftPosition;
    /**
     * Image Top Position - Row number, in pixels, of the top edge of
     * the image with respect to the top edge of the Logical Screen. Top
     * row of the Logical Screen is 0.
     */
    public int topPosition;
    /**
     * Image Width - Width of the image in pixels.
     */
    public int width;
    /**
     * Image Height - Height of the image in pixels.
     */
    public int height;

    public boolean localColorTableFlag;
    public boolean interlaceFlag;
    public boolean sortFlag;
    public int colorTableSize;


    public void read(DataInputStream ds) throws IOException{
        leftPosition = ds.readUShort();
        topPosition = ds.readUShort();
        width = ds.readUShort();
        height = ds.readUShort();
        localColorTableFlag = ds.readBits(1, DataInputStream.MSB) != 0;
        interlaceFlag = ds.readBits(1, DataInputStream.MSB) != 0;
        sortFlag = ds.readBits(1, DataInputStream.MSB) != 0;
        ds.readBits(2, DataInputStream.MSB);
        colorTableSize = ds.readBits(3, DataInputStream.MSB);

        if (localColorTableFlag){
            localColorTable = new ColorTable();
            localColorTable.read(ds, colorTableSize);
        }

        lzwmincodesize = ds.readUByte();
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUShort(leftPosition);
        ds.writeUShort(topPosition);
        ds.writeUShort(width);
        ds.writeUShort(height);
        ds.writeBit(localColorTableFlag?0:1);
        ds.writeBit(interlaceFlag?0:1);
        ds.writeBit(sortFlag?0:1);
        ds.writeBits(0, 2);
        ds.writeBits(colorTableSize, 3);
        if (localColorTableFlag){
            localColorTable.write(ds);
        }
        ds.writeUByte(lzwmincodesize);
    }

    public TypedNode toNode(){
        final TypedNode node = new DefaultTypedNode(MD_GIF_IMAGE);
        node.getOrCreateChild(MD_GIF_IMAGE_LEFTPOSITION).setValue(leftPosition);
        node.getOrCreateChild(MD_GIF_IMAGE_TOPPOSITION).setValue(topPosition);
        node.getOrCreateChild(MD_GIF_IMAGE_WIDTH).setValue(width);
        node.getOrCreateChild(MD_GIF_IMAGE_HEIGHT).setValue(height);
        node.getOrCreateChild(MD_GIF_IMAGE_LOCALCOLORTABLE).setValue(localColorTableFlag);
        node.getOrCreateChild(MD_GIF_IMAGE_INTERLACED).setValue(interlaceFlag);
        node.getOrCreateChild(MD_GIF_IMAGE_SORT).setValue(sortFlag);
        node.getOrCreateChild(MD_GIF_IMAGE_LOCALCOLORTABLESIZE).setValue(colorTableSize);
        return node;
    }

}
