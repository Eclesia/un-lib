
package science.unlicense.format.gif;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaCapabilities;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.MediaCapabilities;

/**
 *
 * @author Johann Sorel
 */
public class GIFMediaFormat extends AbstractMediaFormat{

    public static final GIFMediaFormat INSTANCE = new GIFMediaFormat();

    public GIFMediaFormat() {
        super(new Chars("gif"));
        shortName = new Chars("GIF");
        longName = new Chars("Graphics Interchange Format");
        mimeTypes.add(new Chars("image/gif"));
        extensions.add(new Chars("gif"));
        signatures.add(GIFMetaData.SIGNATURE);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    @Override
    public Store open(Object input) throws IOException {
        return new GIFMediaStore((Path) input);
    }

}
