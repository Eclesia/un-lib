
package science.unlicense.format.gif;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.image.api.ImageSetMetadata;
import science.unlicense.media.api.ImageTrack;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;

/**
 * View a GIF as a video media.
 *
 * @author Johann Sorel
 */
public class GIFMediaStore extends AbstractStore implements Media {

    private final Path path;

    //caches
    private ImageTrack videoMeta = null;

    public GIFMediaStore(Path path) {
        super(GIFMediaFormat.INSTANCE,path);
        this.path = path;
    }

    @Override
    public Track[] getTracks() throws IOException {
        if (videoMeta == null){
            final GIFImageReader reader = new GIFImageReader();
            reader.setInput(path);
            final ImageSetMetadata metas = (ImageSetMetadata) reader.getMetadata(new Chars("imageset"));
            final ImageSetMetadata.Image imageMeta = (ImageSetMetadata.Image) metas.getChild(ImageSetMetadata.MD_IMAGE.getId());
            final int nbFrame = metas.getChildren().getSize();
            final int sizeX = ((ImageSetMetadata.Dimension) imageMeta.getDimensions().get(0)).geExtent();
            final int sizeY = ((ImageSetMetadata.Dimension) imageMeta.getDimensions().get(1)).geExtent();

            videoMeta = new ImageTrack() {

                public TypedNode getImageMetadata() {
                    return imageMeta;
                }

                public int getNbFrames() {
                    return nbFrame;
                }

                public int getFrameRate() {
                    //default is 10 Frame per second, so 100 milliseconds per image
                    return 100;
                }

                public Chars getName() {
                    return new Chars("video");
                }
            };

        }
        return new Track[]{videoMeta};
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        getTracks();
        final GIFImageReader reader = new GIFImageReader();
        reader.setInput(path);
        return new GIFVideoReader(reader, videoMeta.getNbFrames());
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
