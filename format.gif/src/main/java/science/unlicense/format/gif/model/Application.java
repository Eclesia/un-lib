
package science.unlicense.format.gif.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.gif.GIFMetaData.*;
import science.unlicense.math.api.Maths;

/**
 *
 * @author Johann Sorel
 */
public class Application extends Extension {

    public Chars identifier;
    public Chars authCode;
    public Chars data;

    public void read(DataInputStream ds) throws IOException{
        size = ds.readUByte();
        identifier = new Chars(ds.readFully(new byte[8]));
        authCode = new Chars(ds.readFully(new byte[3]));

        //read sub blocks
        final ByteSequence buffer = new ByteSequence();
        for (int bs = ds.readUByte(); bs!=0; bs=ds.readUByte()){
            buffer.put(ds.readFully(new byte[bs]));
        }
        data = new Chars(buffer.toArrayByte());
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(size);
        final byte[] idBytes = identifier.toBytes();
        if (idBytes.length!=8) throw new IOException(ds, "Identifier byte size must be 8");
        ds.write(idBytes);
        final byte[] authBytes = identifier.toBytes();
        if (authBytes.length!=3) throw new IOException(ds, "AuthCode byte size must be 3");
        ds.write(authBytes);

        final byte[] dataBytes = data.toBytes();
        int index = 0;
        while (index<dataBytes.length){
            final int nb = Maths.min(dataBytes.length-index,256);
            ds.write(dataBytes, index, nb);
            index+=nb;
        }
        ds.writeUByte(0);
    }

    public TypedNode toNode(){
        final TypedNode node = new DefaultTypedNode(MD_GIF_APP);
        node.getOrCreateChild(MD_GIF_APP_IDENTIFIER).setValue(identifier);
        node.getOrCreateChild(MD_GIF_APP_AUTHCODE).setValue(authCode);
        node.getOrCreateChild(MD_GIF_APP_DATA).setValue(data);
        return node;
    }

}
