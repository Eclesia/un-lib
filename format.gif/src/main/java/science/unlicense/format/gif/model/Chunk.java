
package science.unlicense.format.gif.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.Metadata;

/**
 *
 * @author Johann Sorel
 */
public abstract class Chunk implements Metadata{

    public int offset;
    public int size;

    public Chunk() {
    }

    public abstract void read(DataInputStream ds) throws IOException;

    public abstract void write(DataOutputStream ds) throws IOException;

}
