
package science.unlicense.format.gif;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.image.api.AbstractImageFormat;

/**
 *
 * @author Johann Sorel
 */
public class GIFImageFormat extends AbstractImageFormat {

    public GIFImageFormat() {
        super(new Chars("gif"));
        shortName = new Chars("GIF");
        longName = new Chars("Graphics Interchange Format");
        mimeTypes.add(new Chars("image/gif"));
        extensions.add(new Chars("gif"));
        signatures.add(GIFMetaData.SIGNATURE);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public Store open(Object source) throws IOException {
        return new GIFImageStore(this, source);
    }

}
