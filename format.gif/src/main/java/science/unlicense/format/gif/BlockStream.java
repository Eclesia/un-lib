
package science.unlicense.format.gif;

import science.unlicense.encoding.api.io.AbstractInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Loops on data block until terminator is reached.
 *
 * @author Johann Sorel
 */
public class BlockStream extends AbstractInputStream{

    private final DataInputStream stream;
    private int counter = 0;

    public BlockStream(DataInputStream stream) {
        this.stream = stream;
    }

    @Override
    public int read() throws IOException {
        if (counter==0){
            //begining of a block
            int b = stream.readUByte();
            if (b==0){
                //we have finish
                return -1;
            } else {
                counter = b;
            }
        }

        counter--;
        return stream.readUByte();
    }

    @Override
    public void dispose() throws IOException {}

}
