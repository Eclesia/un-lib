
package science.unlicense.format.gif;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.buffer.BufferFactory;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.model.DefaultMetadataGroup;
import science.unlicense.common.api.model.Metadata;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageSetMetadata;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorRW;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.image.api.model.ImageModel;
import static science.unlicense.format.gif.GIFMetaData.*;
import science.unlicense.format.gif.model.ColorTable;
import science.unlicense.format.gif.model.GraphicControl;
import science.unlicense.format.gif.model.ImageDescriptor;
import science.unlicense.format.gif.model.ScreenDescriptor;
import science.unlicense.math.impl.Vector2i32;

/**
 * Contains a single image definition in the GIFfile.
 *
 * @author Johann Sorel
 */
class GIFImage {

    private final GIFImageReader reader;
    private final GIFImage previousImage;
    public final Sequence extensions = new ArraySequence();
    public ImageDescriptor descriptor;

    //metadatas
    private ImageSetMetadata.Image mdStandard;
    private Metadata mdGif;

    /**
     *
     * @param reader base reader
     * @param previous image, may be needed for image reconstruction.
     */
    public GIFImage(GIFImageReader reader, GIFImage previous) {
        this.reader = reader;
        this.previousImage =previous;
    }

    public ImageSetMetadata.Image getStandardImageMeta(){
        if (mdStandard == null){
            final ScreenDescriptor screenDesc = reader.getScreenDesc();
            mdStandard = new ImageSetMetadata.Image(screenDesc.screenWidth, screenDesc.screenHeight);
        }
        return mdStandard;
    }

    public Metadata getGifImageMeta(){
        if (mdGif == null){
            final Metadata[] subs = new Metadata[extensions.getSize()+1];
            subs[0] = descriptor;
            for (int i=0;i<subs.length-1;i++){
                subs[i+1] = ((Metadata) extensions.get(i));
            }
            mdGif = new DefaultMetadataGroup(MD_GIF_IMAGE.getId(),subs);
        }
        return mdGif;
    }

    /**
     * Get image graphic control, if any.
     * @return
     */
    private GraphicControl getGraphicControl(){
        for (int i=0,n=extensions.getSize();i<n;i++){
            final Object o = extensions.get(i);
            if (o instanceof GraphicControl){
                return (GraphicControl) o;
            }
        }
        return null;
    }

    public Image toImage(final BacktrackInputStream ds, BufferFactory bufferFactory) throws IOException{

        //rebuild image datas
        final Image image = fillImage(ds, null,bufferFactory);
        return image;
    }

    public ColorTable getColorTable(){
        //select the appropriate color table
        final ColorTable colorTable;
        if (descriptor.localColorTableFlag){
            colorTable = descriptor.localColorTable;
        } else {
            final ScreenDescriptor screenDesc = reader.getScreenDesc();
            colorTable = screenDesc.globalColorTable;
        }
        return colorTable;
    }

    /**
     *
     * @param ds
     * @param buffer
     * @return
     * @throws IOException
     */
    public Image fillImage(final BacktrackInputStream ds, Image buffer, BufferFactory bufferFactory) throws IOException{
        final ScreenDescriptor screenDesc = reader.getScreenDesc();
        final int screenWidth = screenDesc.screenWidth;
        final int screenHeight = screenDesc.screenHeight;

        final int offsetx = descriptor.leftPosition;
        final int offsety = descriptor.topPosition;
        final int width = descriptor.width;
        final int height = descriptor.height;
        final boolean partialImage = (width!=screenWidth || height!=screenHeight);
        final boolean hasTransparency;
        final byte transparencyValue;

        final GraphicControl control = getGraphicControl();
        if (control != null){
            hasTransparency = control.transparentColorFlag;
            transparencyValue = (byte) (int) control.transparentColorIndex;
        } else {
            hasTransparency = false;
            transparencyValue = 0;
        }

        /*
         Note : We can't use indexed color models unless there is only a single
         Image. because multiple image or animated gif use overlapping color tables.
         And by checking several gifs color table values do not necessarly overlaps
         to make a proper 265 color palette.
         So we have to use an ARGB color model.
         Todo : we could detect the case of a single image GIF and still use
         an indexed color model.
         */
        if (buffer == null){
            if (previousImage!=null){
                //we need the previous image datas
                if (partialImage || hasTransparency){
                    buffer = previousImage.fillImage(ds, buffer,bufferFactory);
                } else {
                    //create an empty image
                    buffer = Images.create(new Extent.Long(screenWidth, screenHeight),Images.IMAGE_TYPE_RGBA,bufferFactory);
                }
            } else {
                //create an empty buffer
                buffer = Images.create(new Extent.Long(screenWidth, screenHeight),Images.IMAGE_TYPE_RGBA,bufferFactory);
            }
        }


        //decompress image
        final ByteSequence bank;
        if (descriptor.interlaceFlag){
            //we need to decompress in a different buffer
            //then swap lines
            ByteSequence ubank = decompressImageData(ds, new ByteSequence(new byte[width*height]), LZWDecoder.NO_REPLACE);
            final byte[] swaparray = ubank.getBackArray();
            final byte[] unswaparray = new byte[swaparray.length];

            final int end = height*width;
            final int width8 = 8*width;
            final int width4 = 4*width;
            final int width2 = 2*width;
            int y=0;

            //pass 1
            for (int uy=0; uy<end; y+=width,uy+=width8){
                Arrays.copy(swaparray,y,width,unswaparray,uy);
            }
            //pass 2
            for (int uy=width4; uy<end; y+=width,uy+=width8){
                Arrays.copy(swaparray,y,width,unswaparray,uy);
            }
            //pass 3
            for (int uy=width2; uy<end; y+=width,uy+=width4){
                Arrays.copy(swaparray,y,width,unswaparray,uy);
            }
            //pass 4
            for (int uy=width; uy<end; y+=width,uy+=width2){
                Arrays.copy(swaparray,y,width,unswaparray,uy);
            }

            bank = new ByteSequence(unswaparray);

        } else {
            //we need to decompress in a different buffer
            bank = decompressImageData(ds, new ByteSequence(new byte[width*height]), LZWDecoder.NO_REPLACE);
        }

        final ColorTable colorTable = getColorTable();
        final ImageModel cm = buffer.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor cursor = cm.asTupleBuffer(buffer).cursor();
        final ColorRW pixel = Colors.castOrWrap(cursor.samples(), cs);
        final byte[] overlapBytes = bank.getBackArray();
        final Vector2i32 coord = new Vector2i32();

        if (hasTransparency){
            //we need to merge image datas
            for (int y=0;y<height;y++){
                for (int x=0;x<width;x++){
                    final byte val = overlapBytes[y*width+x];
                    int color;
                    if (val == transparencyValue){
                        color = 0;
                    } else {
                        color = colorTable.colors[val&0xFF];
                        //replace pixel value
                        coord.x = x+offsetx;
                        coord.y = y+offsety;
                        cursor.moveTo(coord);
                        pixel.set(new ColorRGB(color));
                    }
                }
            }
        } else {
            //we need to copy a piece of image datas
            for (int y=0;y<height;y++){
                for (int x=0;x<width;x++){
                    final byte val = overlapBytes[y*width+x];
                    final int color = colorTable.colors[val&0xFF];
                    coord.x = x+offsetx;
                    coord.y = y+offsety;
                    cursor.moveTo(coord);
                    pixel.set(new ColorRGB(color));
                }
            }
        }

        return buffer;
    }

    /**
     * Decompress the image data.
     *
     * @param bts
     * @return
     * @throws IOException
     */
    private ByteSequence decompressImageData(final BacktrackInputStream bts, ByteSequence bank, int skipValue) throws IOException{
        //move to image data starting position
        bts.rewind();
        final DataInputStream ds = new DataInputStream(bts,Endianness.LITTLE_ENDIAN);
        ds.skipFully(descriptor.dataoffset);

        //read image datas
        final BlockStream bs = new BlockStream(ds);
        final LZWDecoder decoder = new LZWDecoder(new DataInputStream(bs),
                descriptor.lzwmincodesize, bank, skipValue);
        return decoder.decode();
    }

}
