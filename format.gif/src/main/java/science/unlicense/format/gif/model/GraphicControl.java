
package science.unlicense.format.gif.model;

import science.unlicense.common.api.model.tree.DefaultTypedNode;
import science.unlicense.common.api.model.tree.TypedNode;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.gif.GIFMetaData.*;

/**
 *
 * @author Johann Sorel
 */
public class GraphicControl extends Extension {

    public int disposalMethod;
    public boolean userInputFlag;
    public boolean transparentColorFlag;
    public int delayTime;
    public int transparentColorIndex;

    public void read(DataInputStream ds) throws IOException{
        size = ds.readUByte();
        ds.readBits(3, DataInputStream.MSB);
        disposalMethod = ds.readBits(3, DataInputStream.MSB);
        userInputFlag = ds.readBits(1, DataInputStream.MSB) != 0;
        transparentColorFlag = ds.readBits(1, DataInputStream.MSB) != 0;
        delayTime = ds.readUShort();
        transparentColorIndex = ds.readUByte();

        //read terminator
        final int term = ds.readUByte();
        if (term!= 0){
            throw new IOException(ds, "Was expecting a terminator block");
        }
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(size);
        ds.writeBits(0, 3);
        ds.writeBit(userInputFlag?1:0);
        ds.writeBit(transparentColorFlag?1:0);
        ds.writeUShort(delayTime);
        ds.writeUByte(transparentColorIndex);
        ds.writeUByte(0);
    }

    public TypedNode toNode() {
        final TypedNode node = new DefaultTypedNode(MD_GIF_GRAPHICCONTROL);
        node.getOrCreateChild(MD_GIF_GRAPHICCONTROL_DISPMETHOD).setValue(disposalMethod);
        node.getOrCreateChild(MD_GIF_GRAPHICCONTROL_USERINPUTF).setValue(userInputFlag);
        node.getOrCreateChild(MD_GIF_GRAPHICCONTROL_TRSCOLORF).setValue(transparentColorFlag);
        node.getOrCreateChild(MD_GIF_GRAPHICCONTROL_DELAYTIME).setValue(delayTime);
        node.getOrCreateChild(MD_GIF_GRAPHICCONTROL_TRSCOLOR).setValue(transparentColorIndex);
        return node;
    }

}
