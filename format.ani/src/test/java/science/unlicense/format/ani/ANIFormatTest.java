
package science.unlicense.format.ani;

import science.unlicense.format.ani.ANIFormat;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class ANIFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final ImageFormat[] formats = Images.getFormats();
        for (int i=0;i<formats.length;i++){
            if (formats[i] instanceof ANIFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("Image format not found.");
    }

}
