package science.unlicense.format.ani;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.model.Metadata;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.ani.model.AnihChunk;
import science.unlicense.format.ani.model.IconChunk;
import science.unlicense.format.ani.model.RateChunk;
import science.unlicense.format.ani.model.SeqChunk;
import science.unlicense.format.riff.RIFFReader;
import science.unlicense.image.api.DefaultImageReadParameters;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.ImageReadParameters;
import science.unlicense.image.api.ImageReader;

/**
 *
 * @author Johann Sorel
 */
public class ANIReader extends AbstractReader implements ImageReader{

    @Override
    public ImageReadParameters createParameters() {
        return new DefaultImageReadParameters();
    }

    @Override
    public Chars[] getMetadataNames() throws IOException {
        return new Chars[0];
    }

    @Override
    public Metadata getMetadata(Chars name) throws IOException {
        return null;
    }

    @Override
    public Image read(ImageReadParameters params) throws IOException {

        final RIFFReader reader = createRiffReader();
        while (reader.hasNext()){
            System.out.println(reader.next());
        }

        throw new IOException(getInput(), "TODO");

    }

    public RIFFReader createRiffReader() throws IOException{
        final Dictionary types = new HashDictionary();
        types.add(ANIConstants.TYPE_ANIHEADER, AnihChunk.class);
        types.add(ANIConstants.TYPE_SEQUENCE, SeqChunk.class);
        types.add(ANIConstants.TYPE_RATE, RateChunk.class);
        types.add(ANIConstants.TYPE_ICON, IconChunk.class);
        final RIFFReader reader = new RIFFReader(types);
        reader.setInput(input);
        return reader;
    }

}
