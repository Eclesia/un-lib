
package science.unlicense.format.ani;

import science.unlicense.format.riff.RIFFConstants;
import science.unlicense.common.api.character.Chars;
import science.unlicense.image.api.AbstractImageFormat;

/**
 * Microsoft animated cursor format.
 *
 * Resources :
 * https://en.wikipedia.org/wiki/ANI_(file_format)
 * http://www.gdgsoft.com/anituner/help/aniformat.htm
 * https://www.daubnet.com/en/file-format-ani
 *
 *
 * @author Johann Sorel
 */
public class ANIFormat extends AbstractImageFormat {

    public static final ANIFormat INSTANCE = new ANIFormat();

    private ANIFormat() {
        super(new Chars("ani"));
        shortName = new Chars("ANI");
        longName = new Chars("Animated mouse cursor");
        extensions.add(new Chars("ani"));
        signatures.add(RIFFConstants.CHUNK_RIFF.toBytes());
        signatures.add(RIFFConstants.CHUNK_RIFX.toBytes());
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

}
