
package science.unlicense.format.ani.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.ani.ANIConstants;

/**
 *
 * @author Johann Sorel
 */
public class AnihChunk extends DefaultChunk {

    public int numFrames;
    public int numSteps;
    public int width;
    public int height;
    public int bitCount;
    public int numPlanes;
    public int displayRate;
    public int reserved;
    public boolean sequenceFlag;
    public boolean iconFlag;

    public AnihChunk() {
        super(ANIConstants.TYPE_ANIHEADER);
    }

    public void readInternal(DataInputStream ds) throws IOException {
        numFrames       = ds.readInt();
        numSteps        = ds.readInt();
        width           = ds.readInt();
        height          = ds.readInt();
        bitCount        = ds.readInt();
        numPlanes       = ds.readInt();
        displayRate     = ds.readInt();
        reserved        = ds.readBits(30);
        sequenceFlag    = ds.readBits(1)==1;
        iconFlag        = ds.readBits(1)==1;
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUInt(size);
        ds.writeInt(numFrames);
        ds.writeInt(numSteps);
        ds.writeInt(width);
        ds.writeInt(height);
        ds.writeInt(bitCount);
        ds.writeInt(numPlanes);
        ds.writeInt(displayRate);
        ds.writeBits(reserved,30);
        ds.writeBit(sequenceFlag?1:0);
        ds.writeBit(iconFlag?1:0);
    }

}
