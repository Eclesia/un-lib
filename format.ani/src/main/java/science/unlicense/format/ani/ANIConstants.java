
package science.unlicense.format.ani;

import science.unlicense.common.api.character.Chars;


/**
 *
 * @author Johann Sorel
 */
public final class ANIConstants {

    private ANIConstants(){}

    public static final Chars TYPE_ACON = Chars.constant("ACON");

    public static final Chars TYPE_ANIHEADER = Chars.constant(new byte[]{'a','n','i','h'});
    public static final Chars TYPE_SEQUENCE = Chars.constant(new byte[]{'s','e','q',' '});
    public static final Chars TYPE_RATE = Chars.constant(new byte[]{'r','a','t','e'});
    public static final Chars TYPE_ICON = Chars.constant(new byte[]{'i','c','o','n'});

    public static final Chars FRAME_LIST = Chars.constant("fram");

}
