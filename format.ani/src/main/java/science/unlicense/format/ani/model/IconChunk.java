
package science.unlicense.format.ani.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.ani.ANIConstants;

/**
 *
 * @author Johann Sorel
 */
public class IconChunk extends DefaultChunk {

    public byte[] imgData;

    public IconChunk() {
        super(ANIConstants.TYPE_SEQUENCE);
    }

    public void readInternal(DataInputStream ds) throws IOException {
        imgData = ds.readFully(new byte[(int) size]);
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUInt(size);
        ds.write(imgData);
    }

}
