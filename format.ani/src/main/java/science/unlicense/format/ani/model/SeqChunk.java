
package science.unlicense.format.ani.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.riff.model.DefaultChunk;
import science.unlicense.format.ani.ANIConstants;

/**
 *
 * @author Johann Sorel
 */
public class SeqChunk extends DefaultChunk {

    public int[] frameNumber;

    public SeqChunk() {
        super(ANIConstants.TYPE_SEQUENCE);
    }

    public void readInternal(DataInputStream ds) throws IOException {
        frameNumber = ds.readInt(((int) size-4)/4);
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUInt(size);
        ds.writeInt(frameNumber);
    }

}
