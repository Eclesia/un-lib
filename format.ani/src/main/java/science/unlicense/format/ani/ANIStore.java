
package science.unlicense.format.ani;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.ImageResource;
import science.unlicense.image.api.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class ANIStore extends AbstractStore implements ImageResource {

    public ANIStore(Object input) {
        super(ANIFormat.INSTANCE, input);
    }

    @Override
    public boolean supportReading() {
        return true;
    }

    @Override
    public boolean supportWriting() {
        return false;
    }

    @Override
    public ImageReader createReader() throws IOException {
        final ANIReader reader = new ANIReader();
        reader.setInput(source);
        return reader;
    }

    @Override
    public ImageWriter createWriter() throws IOException {
        throw new IOException(getInput(), "Not supported.");
    }

    @Override
    public Chars getId() {
        return null;
    }

}
