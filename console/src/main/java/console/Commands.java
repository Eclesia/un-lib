
package console;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.encoding.impl.io.x364.X364.FOREGROUND_BLUE;
import static science.unlicense.encoding.impl.io.x364.X364.FOREGROUND_RED;
import static science.unlicense.encoding.impl.io.x364.X364.FOREGROUND_WHITE;
import static science.unlicense.encoding.impl.io.x364.X364.MODE_BOLD;
import static science.unlicense.encoding.impl.io.x364.X364.MODE_RESET;
import science.unlicense.system.jvm.JVMOutputStream;
import science.unlicense.task.api.Task;
import science.unlicense.task.api.TaskDescriptor;
import science.unlicense.task.api.Tasks;

/**
 * Console tools.
 *
 * @author Johann Sorel
 */
public final class Commands {

    private static final Chars PREFIX = Chars.constant("console.");
    private static final Chars HELP = Chars.constant("-help");
    private static final Chars LIST = Chars.constant("-list");

    private Commands(){}

    public static void main(String[] as) throws Exception {
        Chars[] args = toChars(as);

        if (args.length==0 || args[0].equals(HELP)) {
            printHelp();
        } else if (args[0].equals(LIST)) {
            printList();
        } else {

            final Chars toolName = args[0];
            final TaskDescriptor desc = Tasks.getDescriptor(toolName);
            final Task task = desc.create();

            //map parameters
            for (int i=1;i<args.length;i+=2) {
                final Chars name = args[i].truncate(1, args[i].getCharLength());
                task.inputs().setPropertyValue(name,args[i+1]);
            }
            task.perform();
        }
    }

    private static TaskDescriptor[] getConsoleTasks() {
        final TaskDescriptor[] descs = Tasks.getDescriptors();
        final Sequence consoles = new ArraySequence();
        for (int i=0;i<descs.length;i++) {
            if (descs[i].getId().startsWith(PREFIX)) {
                consoles.add(descs[i]);
            }
        }
        final TaskDescriptor[] cdescs = new TaskDescriptor[consoles.getSize()];
        Collections.copy(consoles, cdescs, 0);
        return cdescs;
    }

    private static void printHelp() throws IOException{

        final CharOutputStream out = new CharOutputStream(new JVMOutputStream(System.out),CharEncodings.UTF_8);
        out.writeLine(new Chars("Use -list to list all tools."));
        out.writeLine(new Chars("Usage example : image.reformat -input file:/... -format png"));

    }

    private static void printList() throws IOException{

        final CharOutputStream out = new CharOutputStream(new JVMOutputStream(System.out),CharEncodings.UTF_8);

        final TaskDescriptor[] descs = getConsoleTasks();
        for (TaskDescriptor td : descs) {

            //general infos
            out.write(MODE_BOLD);
            out.write(td.getId());
            out.write(MODE_RESET);
            out.write(' ');
            out.write('(');
            out.write(td.getTitle().toChars());
            out.write(')');
            out.endLine();
            out.write(FOREGROUND_BLUE);
            out.write(td.getDescription().toChars());
            out.endLine();
            out.write(MODE_RESET);


            //input parameters
            out.write(FOREGROUND_RED);
            out.write(new Chars("Inputs"));
            out.endLine();
            out.write(MODE_RESET);

            final FieldType[] incards = td.getInputType().getFields();
            for (FieldType nc : incards) {
                out.write(FOREGROUND_WHITE);
                out.write('-');
                out.write(nc.getId().toLowerCase());
                out.write(' ');
                out.write(':');
                out.write(' ');
                out.write(new Chars(((FieldType) nc).getValueClass().getSimpleName()));
                out.write(MODE_RESET);
                out.endLine();
            }
            out.endLine();


            //output parameters
            out.write(FOREGROUND_RED);
            out.write(new Chars("Outputs"));
            out.endLine();
            out.write(MODE_RESET);

            final FieldType[] outcards = td.getInputType().getFields();
            for (FieldType nc : outcards) {
                out.write(FOREGROUND_WHITE);
                out.write('-');
                out.write(nc.getId().toLowerCase());
                out.write(' ');
                out.write(':');
                out.write(' ');
                out.write(new Chars(((FieldType) nc).getValueClass().getSimpleName()));
                out.write(MODE_RESET);
                out.endLine();
            }
            out.endLine();

        }

    }

    private static Chars[] toChars(String[] args) {
        final Chars[] chars = new Chars[args.length];
        for (int i=0;i<args.length;i++) {
            chars[i] = new Chars(args[i]);
        }
        return chars;
    }

}
