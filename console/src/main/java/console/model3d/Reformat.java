

package console.model3d;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocumentType;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.model3d.api.Model3Ds;
import science.unlicense.model3d.api.SceneResource;
import science.unlicense.task.api.AbstractTask;
import science.unlicense.task.api.AbstractTaskDescriptor;
import science.unlicense.task.api.Task;
import science.unlicense.task.api.TaskDescriptor;

/**
 *
 * @author Johann Sorel
 */
public class Reformat extends AbstractTask{

    public static final TaskDescriptor DESCRIPTOR = new Descriptor();

    public Reformat() {
        super(DESCRIPTOR);
    }


    public Document perform() {
        final Chars input = (Chars) inputParameters.getPropertyValue(new Chars("input"));
        final Chars format = (Chars) inputParameters.getPropertyValue(new Chars("format"));
        Chars output = (Chars) inputParameters.getPropertyValue(new Chars("output"));

        final Format modelformat = Model3Ds.getFormat(format);

        if (output==null) {
            final int index =input.getLastOccurence('.');
        }

        final Path inPath = Paths.resolve(input);
        final Path outPath;
        if (output==null) {
            final Chars inName = new Chars(inPath.getName());
            final int index = new Chars(inPath.getName()).getFirstOccurence('.');
            final Chars name;
            if (index>=0) {
                name = inName.truncate(0, index).concat('.').concat((CharArray) modelformat.getExtensions().createIterator().next());
            } else {
                name = inName.concat('.').concat((CharArray) modelformat.getExtensions().createIterator().next());
            }
            outPath = inPath.getParent().resolve(name);
        } else {
            outPath = Paths.resolve(output);
        }

        try {
            final Store inStore = Model3Ds.open(inPath);
            final Store outStore = modelformat.open(outPath);
            final SceneResource resIn = (SceneResource) inStore;
            final SceneResource resOut = (SceneResource) outStore;
            resOut.writeElements(resIn.getElements());
        } catch(IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        } catch(StoreException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }

        return outputParameters;
    }

    private static class Descriptor extends AbstractTaskDescriptor{

    private Descriptor() {
            super(new Chars("console.model3d.reformat"), new Chars("Reformat model 3d"), Chars.EMPTY,
                    new DefaultDocumentType(new Chars("input"), null,null,false,new FieldType[]{
                        new FieldTypeBuilder(new Chars("input")).valueClass(Chars.class).build(),
                        new FieldTypeBuilder(new Chars("format")).valueClass(Chars.class).build(),
                        new FieldTypeBuilder(new Chars("output")).valueClass(Chars.class).build()
                    },null),
                    new DefaultDocumentType(new Chars("output"),null,null,false,null,null));
        }

        public Task create() {
            return new Reformat();
        }
    }

}
