
package console.archive;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.doc.DefaultDocumentType;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.FieldType;
import science.unlicense.common.api.model.doc.FieldTypeBuilder;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.task.api.AbstractTask;
import science.unlicense.task.api.AbstractTaskDescriptor;
import science.unlicense.task.api.Task;
import science.unlicense.task.api.TaskDescriptor;
import science.unlicense.archive.api.ArchiveFormat;
import science.unlicense.archive.api.Archives;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class Extract extends AbstractTask{

    public static final TaskDescriptor DESCRIPTOR = new Descriptor();

    public Extract() {
        super(DESCRIPTOR);
    }

    public Document perform() {
        final Chars input = (Chars) inputParameters.getPropertyValue(new Chars("input"));
        final Chars encoding = (Chars) inputParameters.getPropertyValue(new Chars("encoding"));
        final Chars output = (Chars) inputParameters.getPropertyValue(new Chars("output"));

        final Path inPath = Paths.resolve(input);

        final Path outPath;
        if (output==null) {
            outPath = inPath.getParent().resolve( Paths.stripExtension(new Chars(inPath.getName())));
        } else {
            outPath = Paths.resolve(input);
        }

        final Dictionary params = new HashDictionary();
        if (encoding!=null) {
            params.add(ArchiveFormat.PARAM_ENCODING.getId(), CharEncodings.find(encoding));
        }

        try {
            Archives.extract(inPath, outPath, params);
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
        return outputParameters;
    }

    private static class Descriptor extends AbstractTaskDescriptor{

    private Descriptor() {
            super(new Chars("console.archive.extract"), new Chars("Extract archive content"), Chars.EMPTY,
                    new DefaultDocumentType(new Chars("input"), null,null,false,new FieldType[]{
                        new FieldTypeBuilder(new Chars("input")).valueClass(Chars.class).build(),
                        new FieldTypeBuilder(new Chars("encoding")).valueClass(Chars.class).build(),
                        new FieldTypeBuilder(new Chars("output")).valueClass(Chars.class).build()
                    },null),
                    new DefaultDocumentType(new Chars("output"),null,null,false,null,null));
        }

        public Task create() {
            return new Extract();
        }
    }
}
