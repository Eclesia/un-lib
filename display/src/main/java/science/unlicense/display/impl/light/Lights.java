
package science.unlicense.display.impl.light;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.common.api.predicate.ClassPredicate;

/**
 * Utils for lights.
 *
 * @author Johann Sorel
 */
public final class Lights {

    private Lights() {}

    /**
     * Visitor to find all lights in the scene.
     * returns a sequence of lights.
     */
    public static Sequence getLights(Node node) {
        return Nodes.flatten(node, true, new ClassPredicate(Light.class));
    }

}
