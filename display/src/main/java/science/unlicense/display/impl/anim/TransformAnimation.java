
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.AbstractAnimation;
import science.unlicense.display.api.scene.SceneNode;

/**
 *
 * @author Johann Sorel
 */
public class TransformAnimation extends AbstractAnimation{

    private SceneNode node;
    private TransformTimeSerie timeSerie;
    private TransformKeyFrame buffer;

    public SceneNode getNode() {
        return node;
    }

    public void setNode(SceneNode node) {
        this.node = node;
    }

    public TransformTimeSerie getTimeSerie() {
        return timeSerie;
    }

    public void setTimeSerie(TransformTimeSerie timeSerie) {
        this.timeSerie = timeSerie;
    }

    @Override
    public double getLength() {
        return timeSerie==null ? 0 : timeSerie.getLength();
    }

    @Override
    public void update() {
        if (timeSerie==null || node==null) return;

        buffer = timeSerie.interpolate(getTime(), buffer);
        if (buffer==null) return;

        node.getNodeTransform().set(buffer.getValue());
    }

}
