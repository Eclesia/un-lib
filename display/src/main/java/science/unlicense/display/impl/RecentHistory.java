
package science.unlicense.display.impl;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.AbstractProperty;
import science.unlicense.common.api.event.Property;

/**
 * A History stores a list of object in order up to a given size.
 * The history provide convenient methods to access next and previous elements
 * and moving backward or forward in the list while sending appropriate events.
 *
 * @author Johann Sorel
 */
public class RecentHistory extends AbstractEventSource {

    private static final Chars PROPERTY_NEXT = Chars.constant("Next");
    private static final Chars PROPERTY_CURRENT = Chars.constant("Current");
    private static final Chars PROPERTY_PREVIOUS = Chars.constant("Previous");
    private static final Chars PROPERTY_NEXTEXIST = Chars.constant("NextExist");
    private static final Chars PROPERTY_CURRENTEXIST = Chars.constant("CurrentExist");
    private static final Chars PROPERTY_PREVIOUSEXIST = Chars.constant("PreviousExist");

    private final Sequence list = new ArraySequence();
    private final int maxElement;
    private int currentIdx = -1;

    private Object next = null;
    private Object current = null;
    private Object previous = null;
    private boolean nextExist = false;
    private boolean currentExist = false;
    private boolean previousExist = false;

    public RecentHistory() {
        this(30);
    }

    /**
     *
     * @param maxElement maximum number of entries stored in the history.
     */
    public RecentHistory(int maxElement) {
        this.maxElement = maxElement;
    }

    public int getSize() {
        return list.getSize();
    }

    public int getCurrentIndex() {
        return currentIdx;
    }

    private Object get(int index) {
        if (index<0 || index>=list.getSize()) throw new IllegalArgumentException("Unvalid index");
        return list.get(index);
    }

    private Object getInternal(int index) {
        if (index<0 || index>=list.getSize()) return null;
        return list.get(index);
    }

    /**
     *
     * @param candidate null values are ignored
     */
    public synchronized void put(Object candidate) {
        if (candidate==null) return;

        if (CObjects.equals(candidate,current)) {
            return;
        } else if (previous!=null && CObjects.equals(candidate,previous)) {
            backward();
        } else if (next!=null && CObjects.equals(candidate,next)) {
            forward();
        } else {
            //remove all forward elements
            while (list.getSize() > currentIdx+1) {
                list.remove(currentIdx+1);
            }
            list.add(candidate);
            //ensure we do not store to much elements
            if (list.getSize() > maxElement) {
                list.remove(0);
            }
            currentIdx = list.getSize()-1;
            update();
        }
    }

    private void update() {
        setNext(currentIdx+1);
        setCurrent(currentIdx);
        setPrevious(currentIdx-1);
    }

    /**
     * Move forward in the history.
     * Next,Current and Previous element will be updated accordingly.
     * This method has not effect if there are not more element forward.
     */
    public void forward() {
        if (currentIdx<list.getSize()-1) {
            currentIdx++;
            update();
        }
    }

    /**
     * Move backward in the history.
     * Next,Current and Previous element will be updated accordingly.
     * This method has not effect if there are not more element backward.
     */
    public void backward() {
        if (currentIdx>=0) {
            currentIdx--;
            update();
        }
    }

    public Object getNext() {
        return next;
    }

    public void setNext(int index) {
        final Object next = getInternal(index);
        if (this.next != next) {
            final Object old = this.next;
            this.next = next;
            sendPropertyEvent(PROPERTY_NEXT, old, next);
            if (nextExist != (index>=0 && index<list.getSize())) {
                nextExist = !nextExist;
                sendPropertyEvent(PROPERTY_NEXTEXIST, !nextExist, nextExist);
            }
        }
    }

    public Object getCurrent() {
        return current;
    }

    public void setCurrent(int index) {
        final Object current = getInternal(index);
        if (this.current != current) {
            final Object old = this.current;
            this.current = current;
            sendPropertyEvent(PROPERTY_CURRENT, old, current);
            if (currentExist != (index>=0 && index<list.getSize())) {
                currentExist = !currentExist;
                sendPropertyEvent(PROPERTY_CURRENTEXIST, !currentExist, currentExist);
            }
        }
    }

    public Object getPrevious() {
        return previous;
    }

    public void setPrevious(int index) {
        final Object previous = getInternal(index);
        if (this.previous != previous) {
            final Object old = this.previous;
            this.previous = previous;
            sendPropertyEvent(PROPERTY_PREVIOUS, old, previous);
            if (previousExist != (index>=0 && index<list.getSize())) {
                previousExist = !previousExist;
                sendPropertyEvent(PROPERTY_PREVIOUSEXIST, !previousExist, previousExist);
            }
        }
    }

    public boolean isNextExist() {
        return nextExist;
    }

    public boolean isCurrentExist() {
        return currentExist;
    }

    public boolean isPreviousExist() {
        return previousExist;
    }

    public Property varNext() {
        return new AbstractProperty(this, PROPERTY_NEXT, Object.class, true, false) {
            @Override
            public Object getValue() throws RuntimeException {
                return getNext();
            }

            @Override
            public void setValue(Object value) throws RuntimeException {
                throw new RuntimeException();
            }
        };
    }

    public Property varCurrent() {
        return new AbstractProperty(this, PROPERTY_CURRENT, Object.class, true, false) {
            @Override
            public Object getValue() throws RuntimeException {
                return getCurrent();
            }

            @Override
            public void setValue(Object value) throws RuntimeException {
                throw new RuntimeException();
            }
        };
    }

    public Property varPrevious() {
        return new AbstractProperty(this, PROPERTY_PREVIOUS, Object.class, true, false) {
            @Override
            public Object getValue() throws RuntimeException {
                return getPrevious();
            }

            @Override
            public void setValue(Object value) throws RuntimeException {
                throw new RuntimeException();
            }
        };
    }

    public Property varNextExist() {
        return new AbstractProperty(this, PROPERTY_NEXTEXIST, Boolean.class, true, false) {
            @Override
            public Object getValue() throws RuntimeException {
                return isNextExist();
            }

            @Override
            public void setValue(Object value) throws RuntimeException {
                throw new RuntimeException();
            }
        };
    }

    public Property varCurrentExist() {
        return new AbstractProperty(this, PROPERTY_CURRENTEXIST, Boolean.class, true, false) {
            @Override
            public Object getValue() throws RuntimeException {
                return isCurrentExist();
            }

            @Override
            public void setValue(Object value) throws RuntimeException {
                throw new RuntimeException();
            }
        };
    }

    public Property varPreviousExist() {
        return new AbstractProperty(this, PROPERTY_PREVIOUSEXIST, Boolean.class, true, false) {
            @Override
            public Object getValue() throws RuntimeException {
                return isPreviousExist();
            }

            @Override
            public void setValue(Object value) throws RuntimeException {
                throw new RuntimeException();
            }
        };
    }

}
