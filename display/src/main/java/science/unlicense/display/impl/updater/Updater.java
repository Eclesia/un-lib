
package science.unlicense.display.impl.updater;

import science.unlicense.display.impl.DisplayTimerState;

/**
 * Before each rendering operation all Updater in a scene should be called.
 *
 * Updaters can be used for various actions, moving nodes, physics, resource loading,
 * and many more.
 *
 * @author Johann Sorel
 */
public interface Updater {

    /**
     *
     * @param timing
     */
    void update(DisplayTimerState timing);

}
