
package science.unlicense.display.impl.scene;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.SceneUtils;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.math.api.Similarity;

/**
 *
 * @author Johann Sorel
 */
public class DefaultGraphicNode extends DefaultSceneNode implements GraphicNode {

    private final Sequence updaters = new ArraySequence();
    private final Sequence renderers = new ArraySequence();

    private boolean visible = true;

    public DefaultGraphicNode(CoordinateSystem cs) {
        super(cs,true);
        this.coordinateSystem = cs;
    }

    /**
     * Get this node and children data bounding box in this node coordinate system.
     * BBox is relative to this node transform.
     *
     * @return BBox , can be null if no data.
     */
    @Override
    public BBox getBBox(){
        return getBBox(coordinateSystem);
    }

    /**
     * Get this node and children data bounding box in given coordinate system.
     * BBox is relative to this node transform.
     *
     * @param cs not null
     * @return BBox , can be null if no data.
     */
    @Override
    public BBox getBBox(CoordinateSystem cs){
        CObjects.ensureNotNull(cs);
        BBox bbox = null;
        final Iterator ite = this.children.createIterator();
        while (ite.hasNext()) {
            final Object next = ite.next();
            if (next instanceof SceneNode) {
                final SceneNode child = (SceneNode) next;
                final BBox cbbox = SceneUtils.calculateBBox(child, cs);
                if (cbbox != null) {
                    //convert it to this node space
                    final Similarity m = child.getNodeTransform();
                    m.inverseTransform(cbbox.getLower(),cbbox.getLower());
                    m.inverseTransform(cbbox.getUpper(),cbbox.getUpper());
                    cbbox.reorder();
                    if (bbox==null){
                        bbox = new BBox(cbbox);
                    } else {
                        bbox.expand(cbbox);
                    }
                }
            }
        }

        return bbox;
    }

    /**
     * Indicate if the mesh is visible.
     * Default is true.
     * @param visible
     */
    @Override
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    /**
     * Get updaters of this node.
     * @see science.unlicense.engine.opengl.animation.Updater
     *
     * @return Sequence of Updater, never null.
     */
    @Override
    public Sequence getUpdaters() {
        return updaters;
    }

    /**
     * Get renderers of this node.
     * @see science.unlicense.engine.opengl.renderer.Renderer
     *
     * @return Sequence of Renderer, never null.
     */
    @Override
    public Sequence getTechniques() {
        return renderers;
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(getClass().getSimpleName());
        cb.append(' ');
        cb.append(getTitle());
        cb.append(' ');
        cb.append('(');
        cb.append(CObjects.toChars(getId()));
        cb.append(')');
        return cb.toChars();
    }

}
