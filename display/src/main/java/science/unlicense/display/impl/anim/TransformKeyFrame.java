
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.KeyFrame;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.impl.SimilarityNd;

/**
 *
 * @author Johann Sorel
 */
public class TransformKeyFrame extends KeyFrame {

    public TransformKeyFrame() {
    }

    public TransformKeyFrame(double time, SimilarityRW transform) {
        super(time,transform);
    }

    public TransformKeyFrame(double time, VectorRW position, Quaternion rotation, VectorRW scale) {
        super(time);
        SimilarityRW transform = SimilarityNd.create(position.getSampleCount());
        transform.getTranslation().set(position);
        transform.getRotation().set(rotation.toMatrix3());
        transform.getScale().set(scale);
        transform.notifyChanged();
        setValue(transform);
    }


    public SimilarityRW getValue() {
        return (SimilarityRW) value;
    }

    public void setValue(SimilarityRW transform) {
        setValue((Object) transform);
    }

}
