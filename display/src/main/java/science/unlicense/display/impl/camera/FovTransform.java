
package science.unlicense.display.impl.camera;

import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.math.impl.Vectors;
import science.unlicense.math.impl.transform.SimplifiedTransform;

/**
 * Transform 2D coordinate inside camera view rectangle to ray direction normalized
 * vector.
 *
 * @author Johann Sorel
 */
public class FovTransform extends SimplifiedTransform {

    private final Matrix4x4 invertProjection;
    private final Rectangle renderRectangle;

    public FovTransform(Matrix4x4 invertProjection, Rectangle renderRectangle) {
        super(2,3);
        this.invertProjection = invertProjection;
        this.renderRectangle = renderRectangle;
    }

    @Override
    protected void transform1(double[] source, int sourceOffset, double[] dest, int destOffset) {
        TupleRW rayEye = transform(source[sourceOffset], source[sourceOffset+1],null);
        rayEye.toDouble(dest, destOffset);
    }

    @Override
    protected void transform1(float[] source, int sourceOffset, float[] dest, int destOffset) {
        TupleRW rayEye = transform(source[sourceOffset], source[sourceOffset+1],null);
        rayEye.toFloat(dest, destOffset);
    }

    @Override
    public TupleRW transform(Tuple source, TupleRW ray_eye) {
        return transform(source.get(0), source.get(1), ray_eye);
    }

    private TupleRW transform(double viewX, double viewY, TupleRW ray_eye) {
        if (ray_eye == null) ray_eye = new Vector3f64();

        //Step 1: 3d Normalised Device Coordinates
        final double x = (2d * viewX) / renderRectangle.getWidth() - 1d;
        final double y = 1d - (2d * viewY) / renderRectangle.getHeight();
        //Step 2: 4d Homogeneous Clip Coordinates
        final Vector4f64 ray_clip = new Vector4f64(x,y, 1.0,1.0);
        //Step 3: 4d Eye (Camera) Coordinates
        invertProjection.transform(ray_clip, ray_eye);
        Vectors.castOrWrap(ray_eye).localNormalize();
        return ray_eye;
    }

    @Override
    public int getInputDimensions() {
        return 2;
    }

    @Override
    public int getOutputDimensions() {
        return 3;
    }

    @Override
    public Transform invert() {
        throw new UnsupportedOperationException("Not supported.");
    }

}
