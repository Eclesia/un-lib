
package science.unlicense.display.impl.light;

import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.image.api.color.Color;

/**
 * Common attributes shared by all lights.
 *
 * Collada 1.5 :
 * A light embodies a source of illumination shining on the visual scene. A light source can be located within
 * the scene or infinitely far away. Light sources have many different properties and radiate light in many
 * different patterns and frequencies.
 *
 * @author Johann Sorel
 */
public abstract class Light extends DefaultGraphicNode {

    // light diffuse
    protected Color diffuse;
    // light specular
    protected Color specular;

    private boolean castShadows = false;

    /**
     * Allow only subclass in this package to extend it.
     */
    protected Light() {
        super(CoordinateSystems.CARTESIAN3D_METRIC_RIGH_HANDED);
    }

    protected Light(final Color diffuse, final Color specular) {
        super(CoordinateSystems.CARTESIAN3D_METRIC_RIGH_HANDED);
        this.diffuse = diffuse;
        this.specular = specular;
    }

    public Color getDiffuse() {
        return diffuse;
    }

    public void setDiffuse(Color diffuse) {
        this.diffuse = diffuse;
    }

    public Color getSpecular() {
        return specular;
    }

    public void setSpecular(Color specular) {
        this.specular = specular;
    }

    public void setCastShadows(boolean castShadows) {
        this.castShadows = castShadows;
    }

    public boolean isCastShadows() {
        return castShadows;
    }

}
