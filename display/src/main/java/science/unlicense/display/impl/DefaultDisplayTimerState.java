
package science.unlicense.display.impl;

/**
 *
 * @author Johann Sorel
 */
public class DefaultDisplayTimerState implements DisplayTimerState {

    private long diffTime;
    private long time;

    @Override
    public long getTimeNano() {
        return time;
    }

    @Override
    public long getDiffTimeNano() {
        return diffTime;
    }

    @Override
    public float getTimeSecond() {
        return time / 1000000000f;
    }

    @Override
    public float getDiffTimeSecond() {
        return diffTime  / 1000000000f;
    }

    public void pulse() {

        long last = this.time;
        this.time = System.nanoTime();
        if (last==0) {
            //first rendering
            last = this.time;
        }
        this.diffTime = this.time - last;
    }

}
