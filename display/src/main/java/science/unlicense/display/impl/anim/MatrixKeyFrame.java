
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.KeyFrame;
import science.unlicense.math.impl.MatrixNxN;

/**
 *
 * @author Johann Sorel
 */
public class MatrixKeyFrame extends KeyFrame{


    public MatrixKeyFrame() {
    }

    public MatrixKeyFrame(double time, MatrixNxN value) {
        super(time,value);
    }

    public MatrixNxN getValue() {
        return (MatrixNxN) value;
    }

    public void setValue(MatrixNxN value) {
        setValue((Object) value);
    }

}
