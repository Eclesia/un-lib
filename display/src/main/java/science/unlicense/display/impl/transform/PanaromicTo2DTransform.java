
package science.unlicense.display.impl.transform;

import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.transform.Projections;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.transform.ConcatenateTransform;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.transform.CartesianToSphereTransform;

/**
 *
 * @author Johann Sorel
 */
public final class PanaromicTo2DTransform {

    private static final CartesianToSphereTransform XYZ_TO_RADIAN = new CartesianToSphereTransform(1);

    private PanaromicTo2DTransform(){}

    public static Transform create(Extent panoramicImageExtent, MonoCamera camera) {

        final AffineRW radianToImage = Projections.stretched(
                new BBox(new double[]{-Maths.TWO_PI,-Maths.PI}, new double[]{Maths.TWO_PI,Maths.PI}),
                new BBox(new double[]{0,0}, new double[]{panoramicImageExtent.get(0),panoramicImageExtent.get(1)}));

        Transform fovToXyz = camera.getFovTransform();
        Transform t = CoordinateSystems.createTransform(CoordinateSystems.CARTESIAN3D_METRIC_RIGH_HANDED, CoordinateSystems.EARTH_CENTERED_ROTATIONAL);
        fovToXyz = ConcatenateTransform.create(fovToXyz, t);
        return ConcatenateTransform.create(fovToXyz, XYZ_TO_RADIAN, radianToImage);
    }

    public static Transform create(Extent panoramicImageExtent, MonoCamera camera, Affine cameraToWorld) {

        final AffineRW radianToImage = Projections.stretched(
                new BBox(new double[]{-Maths.PI,Maths.HALF_PI}, new double[]{Maths.PI,-Maths.HALF_PI}),
                new BBox(new double[]{0,0}, new double[]{panoramicImageExtent.get(0),panoramicImageExtent.get(1)}));

        Transform fovToXyz = camera.getFovTransform();
        Transform t = CoordinateSystems.createTransform(CoordinateSystems.CARTESIAN3D_METRIC_RIGH_HANDED, CoordinateSystems.EARTH_CENTERED_ROTATIONAL);
        fovToXyz = ConcatenateTransform.create(fovToXyz, cameraToWorld, t);
        return ConcatenateTransform.create(fovToXyz, XYZ_TO_RADIAN, radianToImage);
    }

}
