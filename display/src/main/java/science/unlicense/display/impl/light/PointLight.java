
package science.unlicense.display.impl.light;

import science.unlicense.common.api.character.Chars;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;

/**
 * Point light.
 * Position is provided by the GLNode matrix.
 *
 * Colladata 1.5 :
 * A point light source radiates light in all directions from a known location in space.
 * The intensity of a point light source is attenuated as the distance to the light source increases.
 *
 * @author Johann Sorel
 */
public class PointLight extends Light {

    //attenuation
    private Attenuation attenuation;

    public PointLight() {
        this(Color.WHITE,Color.WHITE,new Attenuation(1f, 1f, 1f));
    }

    public PointLight(Color diffuse, Color specular, final Attenuation attenuation) {
        super(diffuse,specular);
        setTitle(new Chars("Point light"));
        this.attenuation = attenuation;
    }

    public VectorRW getPosition() {
        final VectorRW pos = VectorNf64.createDouble(getDimension());
        getNodeToRootSpace().transform(pos,pos);
        return pos;
    }

    public Attenuation getAttenuation() {
        return attenuation;
    }

    public void setAttenuation(Attenuation attenuation) {
        this.attenuation = attenuation;
    }

}
