
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.KeyFrame;
import science.unlicense.display.api.anim.TimeSerie;

/**
 *
 * @author Johann Sorel
 */
public interface TupleTimeSerie extends TimeSerie{

    int getDimension();

    TupleKeyFrame interpolate(double time, KeyFrame buffer);

}
