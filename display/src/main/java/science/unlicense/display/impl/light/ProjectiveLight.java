
package science.unlicense.display.impl.light;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.SceneUtils;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.BBox;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector4f64;

/**
 * Projective light are similar to directional light but have an origin in space.
 *
 *
 * @author Johann Sorel
 */
public class ProjectiveLight extends Light{

    public ProjectiveLight() {
        this(Color.WHITE,Color.WHITE);
    }

    public ProjectiveLight(Color diffuse, Color specular) {
        super(diffuse,specular);
        setTitle(new Chars("Directional light"));
    }

    public Tuple getWorldSpaceDirection() {
        final Affine rootToNode = getNodeToRootSpace();
        VectorRW forward = (VectorRW) rootToNode.toMatrix().transform(new Vector4f64(0, 0, -1, 0), new Vector4f64());
        return forward.getXYZ().localNormalize();
    }

    /**
     * Create the directional light best camera to produce a shadowmap.
     *
     * @param camera
     * @return
     */
    public MonoCamera createFittingCamera(MonoCamera camera) {

        //get frustrum corners
        final VectorRW[] corners = camera.calculateFrustrumCorners();

        //convert them to light space
        final Affine vm = SceneUtils.sourceToTarget(camera,this);
        for (int i=0;i<8;i++) {
            vm.transform(corners[i],corners[i]);
        }

        //get boundingbox
        final BBox bbox = new BBox(corners[0].copy(),corners[0].copy());
        for (int i=1;i<8;i++) {
            bbox.expand(corners[i]);
        }

        final MonoCamera lightCamera = new MonoCamera();
        lightCamera.setNearPlane(0.1);
        lightCamera.setFarPlane(10000);
        lightCamera.setFieldOfView(60);

        return lightCamera;
    }

}
