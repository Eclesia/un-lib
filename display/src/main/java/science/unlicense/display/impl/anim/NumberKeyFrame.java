
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.KeyFrame;

/**
 *
 * @author Johann Sorel
 */
public class NumberKeyFrame extends KeyFrame{


    public NumberKeyFrame() {
    }

    public NumberKeyFrame(double time, double value) {
        super(time,value);
    }

    public Double getValue() {
        return (Double) value;
    }

    public void setValue(double value) {
        setValue((Double) value);
    }

}
