
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.AbstractAnimation;
import science.unlicense.common.api.predicate.Variable;

/**
 *
 * @author Johann Sorel
 */
public class NumberAnimation extends AbstractAnimation{

    private Variable var;
    private NumberTimeSerie timeSerie;
    private NumberKeyFrame buffer;

    public Variable getVariable() {
        return var;
    }

    public void setVariable(Variable var) {
        this.var = var;
    }

    public NumberTimeSerie getTimeSerie() {
        return timeSerie;
    }

    public void setTimeSerie(NumberTimeSerie timeSerie) {
        this.timeSerie = timeSerie;
    }

    @Override
    public double getLength() {
        return timeSerie==null ? 0 : timeSerie.getLength();
    }

    @Override
    public void update() {
        if (timeSerie==null || var==null) return;

        buffer = timeSerie.interpolate(getTime(), buffer);
        if (buffer==null) return;

        var.setValue(buffer.getValue());
    }

}
