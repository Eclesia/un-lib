

package science.unlicense.display.impl.camera;

import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.SceneUtils;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Affines;
import science.unlicense.math.impl.Matrices;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;
import science.unlicense.math.impl.VectorNf64;

/**
 * 3D Camera for a single eye view point.
 *
 * @author Johann Sorel
 */
public class MonoCamera extends DefaultGraphicNode implements Camera {

    public static final int TYPE_FLAT = 0;
    public static final int TYPE_ORTHO = 1;
    public static final int TYPE_PERSPECTIVE = 2;

    //view size in screen space
    protected final Rectangle renderRectangle = new Rectangle();

    //camera and frustrum informations
    protected final VectorRW right = new Vector3f64(1,0,0);
    protected final VectorRW up = new Vector3f64(0,1,0);
    protected final boolean rightHanded;

    //if set to true, an orthogonal projection is used in place of the perspective.
    protected int cameraType = TYPE_PERSPECTIVE;
    protected double fieldOfView = 45f;
    protected double nearPlane = 0.1f;
    protected double farPlane = 100f;
    protected boolean verticalFlip = false;

    //flag indicating matrix must be recalculated.
    private boolean dirty = true;

    /**
     * Camera 3D to Homogeneous coordinates.
     */
    protected final Matrix4x4 projectionMatrix = new Matrix4x4().setToIdentity();

    /**
     * Create a right handed camera.
     */
    public MonoCamera() {
        this(true);
    }

    /**
     *
     * @param rightHanded is scene using a right handed coordinate system.
     */
    public MonoCamera(boolean rightHanded) {
        super(CoordinateSystems.UNDEFINED_3D);
        this.rightHanded = rightHanded;
    }

    public void copy(MonoCamera camera){
        this.cameraType = camera.cameraType;
        this.farPlane = camera.farPlane;
        this.fieldOfView = camera.fieldOfView;
        this.nearPlane = camera.nearPlane;
        this.renderRectangle.set(camera.renderRectangle);
        this.projectionMatrix.set(camera.projectionMatrix);
        this.right.set(camera.right);
        this.up.set(camera.up);
    }

    public Rectangle getRenderArea() {
        return renderRectangle;
    }

    public void setRenderArea(Rectangle paintArea) {
        if (this.renderRectangle.equals(paintArea)) return;
        this.renderRectangle.set(paintArea);
        setDirty(true);
    }

    public VectorRW getForward() {
        final VectorRW forward = getUpAxis().cross(getRightAxis(), null);
        forward.localNormalize();
        return forward;
    }

    protected boolean isDirty() {
        return dirty;
    }

    protected void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public void setVerticalFlip(boolean verticalFlip) {
        if (this.verticalFlip==verticalFlip) return;
        this.verticalFlip = verticalFlip;
        setDirty(true);
    }

    public boolean isVerticalFlip() {
        return verticalFlip;
    }

    /**
     * @return Vector3 copy of current up axis.
     */
    public VectorRW getUpAxis() {
        return up.copy();
    }

    public void setUpAxis(VectorRW value){
        this.up.set(value);
        this.up.localNormalize();
        setDirty(true);
    }

    public VectorRW getRightAxis(){
        return right.copy();
    }

    public void setRightAxis(VectorRW value){
        this.up.set(value);
        this.up.localNormalize();
        setDirty(true);
    }

    /**
     * @return field of view, in degree.
     */
    public double getFieldOfView() {
        return fieldOfView;
    }

    public void setFieldOfView(double fieldOfView) {
        if (this.fieldOfView==fieldOfView) return;
        this.fieldOfView = fieldOfView;
        setDirty(true);
    }

    /**
     * @return frustrum near plane.
     */
    public double getNearPlane() {
        return nearPlane;
    }

    public void setNearPlane(double nearPlane) {
        if (this.nearPlane==nearPlane) return;
        this.nearPlane = nearPlane;
        setDirty(true);
    }

    /**
     * @return frustrum far plane.
     */
    public double getFarPlane() {
        return farPlane;
    }

    public void setFarPlane(double farPlane) {
        if (this.farPlane==farPlane) return;
        this.farPlane = farPlane;
        setDirty(true);
    }

    public void setCameraType(int cameraType) {
        this.cameraType = cameraType;
        setDirty(true);
    }

    /**
     * DO NOT MODIFY MATRIX
     * @return DO NOT MODIFY MATRIX
     */
    public Matrix4x4 getProjectionMatrix() {
        if (isDirty()) calculateMatrices();
        return projectionMatrix;
    }

    /**
     * Calculate a ray from 2D X/Y projection.
     * A mouse click for example.
     *
     * Origin : anton gerdelan
     * http://www.antongerdelan.net/opengl4/raycasting.html
     *
     * @param mouse_x
     * @param mouse_y
     * @return Ray in World Space
     */
    public Ray calculateRayWorldCS(double mouse_x, double mouse_y){
        Ray ray = calculateRayCameraCS(mouse_x, mouse_y);
        //Step 4: 4d World Coordinates
        final Affine invertView = getNodeToRootSpace();
        final VectorRW ray_wor = (VectorRW) Affines.transformNormal(invertView,ray.getDirection(),new Vector3f64());
        // don't forget to normalise the vector at some point
        ray_wor.localNormalize();

        ray.getPosition().set(invertView.getCol(invertView.getInputDimensions()));
        ray.setDirection(ray_wor);
        return ray;
    }

    /**
     * Calculate a ray from 2D X/Y projection.
     * A mouse click for example.
     *
     * Origin : anton gerdelan
     * http://www.antongerdelan.net/opengl4/raycasting.html
     *
     * @param mouse_x
     * @param mouse_y
     * @return Ray in Camera Space
     */
    public Ray calculateRayCameraCS(double mouse_x, double mouse_y){
        //Step 1: 3d Normalised Device Coordinates
        final double x = (2d * mouse_x) / renderRectangle.getWidth() - 1d;
        final double y = 1d - (2d * mouse_y) / renderRectangle.getHeight();
        //Step 2: 4d Homogeneous Clip Coordinates
        final VectorRW ray_clip = new Vector4f64(x,y, 1.0,1.0);
        //Step 3: 4d Eye (Camera) Coordinates
        final Matrix4x4 invertProjection = getProjectionMatrix().invert();
        final VectorRW ray_eye = new Vector3f64();
        invertProjection.transform(ray_clip,ray_eye);
        ray_clip.set(2, 1);

        return new Ray(new Vector3f64(0,0,0), VectorNf64.create(ray_eye.getXYZ()).localNormalize());
    }

    public Transform getFovTransform() {
        return new FovTransform(projectionMatrix.invert(), renderRectangle);
    }

    public VectorRW[] calculateFrustrumCorners(){
        return calculateFrustrumCorners(Double.MAX_VALUE);
    }

    /**
     * Calculate frustrum corners.
     *
     * @param maxDistance maximum far plane
     * @return 8 Vectors [n1,n2,n3,n4,f1,f2,f3,f4]
     */
    public VectorRW[] calculateFrustrumCorners(double maxDistance){
        final Ray ray1 = calculateRayCameraCS(0,  0);
        final Ray ray2 = calculateRayCameraCS(0,  (int) renderRectangle.getHeight());
        final Ray ray3 = calculateRayCameraCS((int) renderRectangle.getWidth(),0);
        final Ray ray4 = calculateRayCameraCS((int) renderRectangle.getWidth(),(int) renderRectangle.getHeight());

        final double far = Maths.min(farPlane,maxDistance);

        return new VectorRW[]{
            new VectorNf64(ray1.getDirection().scale(nearPlane).localAdd(ray1.getPosition()),1),
            new VectorNf64(ray2.getDirection().scale(nearPlane).localAdd(ray2.getPosition()),1),
            new VectorNf64(ray3.getDirection().scale(nearPlane).localAdd(ray3.getPosition()),1),
            new VectorNf64(ray4.getDirection().scale(nearPlane).localAdd(ray4.getPosition()),1),
            new VectorNf64(ray1.getDirection().scale(far).localAdd(ray1.getPosition()),1),
            new VectorNf64(ray2.getDirection().scale(far).localAdd(ray2.getPosition()),1),
            new VectorNf64(ray3.getDirection().scale(far).localAdd(ray3.getPosition()),1),
            new VectorNf64(ray4.getDirection().scale(far).localAdd(ray4.getPosition()),1)
        };
    }

    /**
     * Calculate transformation matrice relative to the camera,
     * including view and projection transforms.
     *
     * @param node
     * @return Matrix
     */
    public Matrix calculateMVP(SceneNode node){
        final Affine mv = SceneUtils.sourceToTarget(node, this);
        return getProjectionMatrix().multiply(mv.toMatrix());
    }

    protected void calculateMatrices() {
        setDirty(false);

        final double width = renderRectangle.getWidth();
        final double height = renderRectangle.getHeight();

        projectionMatrix.setToIdentity();
        final double ratio = width/height;
        //calculate edges
        final double top = nearPlane * Math.tan(fieldOfView * Math.PI / 360.0);
        final double bottom = -top;
        final double left = bottom * ratio;
        final double right = top * ratio;

        if (cameraType == TYPE_ORTHO){
            projectionMatrix.set(Matrices.ortho2(fieldOfView,ratio,nearPlane,farPlane,null));
            //Matrices.orthogonal(left, right, bottom, top, nearPlane, farPlane, projectionMatrix.getValues());
        } else if (cameraType == TYPE_PERSPECTIVE){
            projectionMatrix.set(Matrices.perspective(fieldOfView, width, height,
                nearPlane, farPlane, null, rightHanded));
        }

        if (verticalFlip) {
            Matrix4x4 postMatrix = new Matrix4x4().setToIdentity();
            postMatrix.set(1, 1, -1);
            projectionMatrix.multiply(postMatrix, projectionMatrix);
        }
    }

}
