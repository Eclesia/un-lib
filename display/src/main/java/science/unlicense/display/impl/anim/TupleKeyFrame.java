
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.KeyFrame;
import science.unlicense.math.api.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public class TupleKeyFrame extends KeyFrame{

    public TupleKeyFrame() {
    }

    public TupleKeyFrame(double time, TupleRW value) {
        super(time,value);
    }

    public TupleRW getValue() {
        return (TupleRW) value;
    }

    public void setValue(TupleRW value) {
        this.value = value;
    }

}
