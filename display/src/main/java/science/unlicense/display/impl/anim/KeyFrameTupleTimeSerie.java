
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.KeyFrame;
import science.unlicense.display.api.anim.KeyFrameTimeSerie;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Vectors;

/**
 *
 * @author Johann Sorel
 */
public class KeyFrameTupleTimeSerie extends KeyFrameTimeSerie implements TupleTimeSerie{

    public int getDimension() {
        if (getFrames().isEmpty()) {
            return 0;
        } else {
            TupleKeyFrame frame = (TupleKeyFrame) getFrames().createIterator().next();
            return frame.getValue().getSampleCount();
        }
    }

    public TupleKeyFrame interpolate(double time, KeyFrame buffer) {

        final KeyFrame[] nearest = getNearest(time, null);
        final TupleKeyFrame underFrame = (TupleKeyFrame) nearest[0];
        final TupleKeyFrame aboveFrame = (TupleKeyFrame) nearest[1];

        final TupleKeyFrame frame = buffer==null ? new TupleKeyFrame() : (TupleKeyFrame) buffer;
        if (frame.getValue()==null) frame.setValue(VectorNf64.createDouble(underFrame.getValue().getSampleCount()));

        if (underFrame == aboveFrame) {
            //we are at the begin, the end, or exactly on a frame
            frame.setValue(underFrame.getValue().copy());
        } else {
            //interpolate
            final double extent = aboveFrame.getTime() - underFrame.getTime();
            final double ratio = (time-underFrame.getTime()) / extent;

            final Tuple ub = underFrame.getValue();
            final Tuple ab = aboveFrame.getValue();

            final double[] array = frame.getValue().toDouble();
            Vectors.lerp(ub.toDouble(), ab.toDouble(), ratio, array);
            frame.getValue().set(array);
        }

        frame.setTime(time);
        return frame;
    }

}
