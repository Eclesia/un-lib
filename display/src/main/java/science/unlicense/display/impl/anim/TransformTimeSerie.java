
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.KeyFrame;
import science.unlicense.display.api.anim.TimeSerie;

/**
 *
 * @author Johann Sorel
 */
public interface TransformTimeSerie extends TimeSerie {

    int getDimension();

    TransformKeyFrame interpolate(double time, KeyFrame buffer);

}
