
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.TimeSerie;

/**
 *
 * @author Johann Sorel
 */
public interface RotationTimeSerie extends TimeSerie{

    int getDimension();

    MatrixKeyFrame interpolateAsMatrix(double time, MatrixKeyFrame buffer);

}
