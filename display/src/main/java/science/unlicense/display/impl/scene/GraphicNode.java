
package science.unlicense.display.impl.scene;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystem;

/**
 *
 * @author Johann Sorel
 */
public interface GraphicNode extends SceneNode {

    /**
     * Get this node and children data bounding box in this node coordinate system.
     * BBox is relative to this node transform.
     *
     * @return BBox , can be null if no data.
     */
    BBox getBBox();

    /**
     * Get this node and children data bounding box in given coordinate system.
     * BBox is relative to this node transform.
     *
     * @param cs not null
     * @return BBox , can be null if no data.
     */
    BBox getBBox(CoordinateSystem cs);

    /**
     * Indicate if the node is visible.
     * Default is true.
     * @param visible
     */
    void setVisible(boolean visible);

    boolean isVisible();

    /**
     * Get updaters of this node.
     *
     * @return Sequence of Updater, never null.
     */
    Sequence getUpdaters();

    /**
     * Get renderers of this node.
     *
     * @return Sequence of Renderer, never null.
     */
    Sequence getTechniques();

}
