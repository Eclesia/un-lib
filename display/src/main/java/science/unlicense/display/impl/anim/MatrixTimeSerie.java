
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.KeyFrame;
import science.unlicense.display.api.anim.TimeSerie;

/**
 *
 * @author Johann Sorel
 */
public interface MatrixTimeSerie extends TimeSerie{

    int getDimension();

    MatrixKeyFrame interpolate(double time, KeyFrame buffer);

}
