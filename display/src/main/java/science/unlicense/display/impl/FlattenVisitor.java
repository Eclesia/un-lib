

package science.unlicense.display.impl;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.impl.scene.GraphicNode;

/**
 * flatten a tree structure in a sequence.
 * only Mesh and Scene nodes are stored.
 *
 * @author Johann Sorel
 */
public class FlattenVisitor extends NodeVisitor{

    private final Sequence col = new ArraySequence();

    public void reset(){
        col.removeAll();
    }

    public Sequence getCollection() {
        return col;
    }

    public Object visit(Node node, Object context) {
        if (node instanceof GraphicNode){
            //skip nodes which are not visible
            if (!((GraphicNode) node).isVisible()) return null;
        }
        super.visit(node, context);
        if (node instanceof GraphicNode){
            col.add(node);
        }
        return null;
    }

}
