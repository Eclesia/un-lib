
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.KeyFrame;
import science.unlicense.display.api.anim.TimeSerie;

/**
 *
 * @author Johann Sorel
 */
public interface NumberTimeSerie extends TimeSerie{

    double interpolate(double time);

    NumberKeyFrame interpolate(double time, KeyFrame buffer);

}
