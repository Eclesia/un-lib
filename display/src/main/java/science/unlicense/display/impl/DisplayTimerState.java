
package science.unlicense.display.impl;

/**
 *
 * @author Johann Sorel
 */
public interface DisplayTimerState {

    /**
     * @return time in nanosecond at the moment the display was called
     */
    long getTimeNano();

    /**
     * @return time in seconds at the moment the display was called
     */
    float getTimeSecond();

    /**
     * @return time in nanosecond between the previous render and this one.
     */
    long getDiffTimeNano();

    /**
     * @return time in seconds between the previous render and this one.
     */
    float getDiffTimeSecond();

}
