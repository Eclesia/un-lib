
package science.unlicense.display.impl.camera;

import science.unlicense.display.api.scene.SceneNode;

/**
 * Collada 1.5 :
 * A camera embodies the eye point of the viewer looking at the visual scene. It is a device that captures
 * visual images of a scene. A camera has a position and orientation in the scene. This is the viewpoint of the
 * camera as seen by the camera’s optics or lens.
 *
 * @author Johann Sorel
 */
public interface Camera extends SceneNode {


}
