
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.KeyFrame;
import science.unlicense.display.api.anim.KeyFrameTimeSerie;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.MatrixNxN;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.impl.Quaternions;

/**
 *
 * @author Johann Sorel
 */
public class KeyFrameQuaternionTimeSerie extends KeyFrameTimeSerie implements TupleTimeSerie,RotationTimeSerie{

    public int getDimension() {
        if (getFrames().isEmpty()) {
            return 0;
        } else {
            return 3;
        }
    }

    public TupleKeyFrame interpolate(double time, KeyFrame buffer) {

        final KeyFrame[] nearest = getNearest(time, null);
        final TupleKeyFrame underFrame = (TupleKeyFrame) nearest[0];
        final TupleKeyFrame aboveFrame = (TupleKeyFrame) nearest[1];

        final TupleKeyFrame frame = buffer==null ? new TupleKeyFrame() : (TupleKeyFrame) buffer;
        if (frame.getValue()==null) frame.setValue(new Quaternion());

        if (underFrame == aboveFrame) {
            //we are at the begin, the end, or exactly on a frame
            frame.setValue(underFrame.getValue().copy());
        } else {
            //interpolate
            final double extent = aboveFrame.getTime() - underFrame.getTime();
            final double ratio = (time-underFrame.getTime()) / extent;

            final Tuple ub = underFrame.getValue();
            final Tuple ab = aboveFrame.getValue();

            final double[] array = frame.getValue().toDouble();
            Quaternions.slerp(ub.toDouble(), ab.toDouble(), ratio, array);
            frame.getValue().set(array);
        }

        frame.setTime(time);
        return frame;
    }

    @Override
    public MatrixKeyFrame interpolateAsMatrix(double time, MatrixKeyFrame buffer) {
        final KeyFrame[] nearest = getNearest(time, null);
        final TupleKeyFrame underFrame = (TupleKeyFrame) nearest[0];
        final TupleKeyFrame aboveFrame = (TupleKeyFrame) nearest[1];

        final MatrixKeyFrame frame = buffer==null ? new MatrixKeyFrame() : (MatrixKeyFrame) buffer;
        if (frame.getValue()==null) frame.setValue(new Matrix3x3());

        final MatrixNxN matrix = frame.getValue();
        if (underFrame == aboveFrame) {
            //we are at the begin, the end, or exactly on a frame
            Quaternions.toMatrix(underFrame.getValue().toDouble(),matrix.getValues());
        } else {
            //interpolate
            final double extent = aboveFrame.getTime() - underFrame.getTime();
            final double ratio = (time-underFrame.getTime()) / extent;

            final Tuple ub = underFrame.getValue();
            final Tuple ab = aboveFrame.getValue();

            double[] rot = Quaternions.slerp(ub.toDouble(), ab.toDouble(), ratio, null);
            Quaternions.toMatrix(rot, matrix.getValues());
        }

        frame.setTime(time);
        return frame;
    }

}
