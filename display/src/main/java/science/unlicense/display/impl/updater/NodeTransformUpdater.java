
package science.unlicense.display.impl.updater;

import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.VectorNf64;
import science.unlicense.math.impl.Matrices;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.api.SimilarityRW;

/**
 * Utility class to control a node movements.
 * For example once attached to a camera node it can be linked to mouse
 * and keyboard events to control navigation.
 *
 * @author Johann Sorel
 */
public class NodeTransformUpdater implements Updater {

    private final SceneNode node;
    private final VectorRW up;
    private final VectorRW right;
    private final boolean rightHanded;

    //diff
    private final VectorRW translation = VectorNf64.createDouble(3);
    private final Matrix3x3 rotation = new Matrix3x3().setToIdentity();

    public NodeTransformUpdater(SceneNode node, VectorRW up, VectorRW right) {
        this(node, up, right, true);
    }

    public NodeTransformUpdater(SceneNode node, VectorRW up, VectorRW right, boolean rightHanded) {
        this.node = node;
        this.up = up;
        this.right = right;
        this.rightHanded = rightHanded;
    }

    public VectorRW getUp() {
        return up.copy();
    }

    public VectorRW getRight() {
        return right.copy();
    }

    public VectorRW getForward() {
        final VectorRW forward = getUp().cross(getRight(), null);
        forward.localNormalize();
        return forward;
    }

    public void moveForward(float scale) {
        final VectorRW forward = getForward();
        move(forward, scale);
    }

    public void moveBackward(float scale) {
        final VectorRW backward = getForward();
        backward.localNegate();
        backward.localNormalize();
        move(backward, scale);
    }

    public void moveLeft(float scale) {
        move(getRight().negate(null), scale);
    }

    public void moveRight(float scale) {
        move(getRight(), scale);
    }

    public void moveUp(float scale) {
        move(getUp(), scale);
    }

    public void moveDown(float scale) {
        move(getUp().negate(null), scale);
    }

    public void move(VectorRW direction, float scale) {
        direction = direction.scale(scale);
        translation.localAdd(direction);
    }

    public void rotate(VectorRW axis, float angle) {
        final double[][] rotMatrice = Matrices.createRotation3(angle, axis, null);
        final Matrix3x3 r = new Matrix3x3(rotMatrice);

        r.localMultiply(rotation);
        rotation.set(r);
    }

    @Override
    public void update(DisplayTimerState context) {

        if (translation.isAll(0.0) && rotation.isIdentity()) return;

        final SimilarityRW trs = node.getNodeTransform();
        final MatrixRW nodeRotation = trs.getRotation();

        nodeRotation.transform(translation,translation);
        nodeRotation.localMultiply(rotation);

        trs.getTranslation().localAdd(translation);
        trs.notifyChanged();

        //reset our diffs
        translation.setAll(0.0);
        rotation.setToIdentity();
    }

}
