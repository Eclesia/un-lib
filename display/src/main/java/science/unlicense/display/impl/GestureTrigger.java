

package science.unlicense.display.impl;

import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;


/**
 *
 * @author Johann Sorel
 */
public class GestureTrigger {

    public static final int ANY_BUTTON_OR_KEY = -2;

    private final boolean mousetrigger;

    private final int keyboardCodePoint;
    private final int keyboardEventType;

    private final int mouseButton;
    private final int mouseEventType;

    private final boolean isInstant;

    /**
     *
     * @param mousetrigger
     * @param keyboardCodePoint
     * @param keyboardEventType
     * @param mouseButton
     * @param mouseEventType
     * @param instant indicate the event be executed when it happens only, no repetition
     */
    protected GestureTrigger(boolean mousetrigger, int keyboardCodePoint, int keyboardEventType, int mouseButton, int mouseEventType, boolean instant) {
        this.mousetrigger = mousetrigger;
        this.keyboardCodePoint = keyboardCodePoint;
        this.keyboardEventType = keyboardEventType;
        this.mouseButton = mouseButton;
        this.mouseEventType = mouseEventType;
        this.isInstant = instant;
    }

    public boolean isInstant() {
        return isInstant;
    }

    public boolean evaluate(GestureState state) {

        if (!mousetrigger) {
            if (isInstant) {
                if (!(state.currentEvent.getMessage() instanceof KeyMessage)) return false;
                if (state.keyboardEventType!=keyboardEventType) return false;
                if (keyboardCodePoint==ANY_BUTTON_OR_KEY) return true;
                return state.keyboardCodePoint == keyboardCodePoint;
            } else {
                return state.keyboardPressedKeys.contains(keyboardCodePoint);
            }

        } else if (mousetrigger) {
            if (isInstant) {
                if (!(state.currentEvent.getMessage() instanceof MouseMessage)) return false;
                if (state.mouseEventType!=mouseEventType) return false;
                if (mouseButton==ANY_BUTTON_OR_KEY) return true;
                return state.mouseButton == mouseButton;
            } else {
                return state.mousePressedKeys.contains(mouseButton);
            }
        }

        return false;
    }

    public static GestureTrigger createRepeatKeyTrigger(int keyboardCodePoint) {
        return createKeyTrigger(keyboardCodePoint, -1, false);
    }

    public static GestureTrigger createInstantKeyTrigger(int keyboardCodePoint) {
        return createInstantKeyTrigger(keyboardCodePoint, KeyMessage.TYPE_PRESS);
    }

    public static GestureTrigger createInstantKeyTrigger(int keyboardCodePoint, int keyboardEventType) {
        return createKeyTrigger(keyboardCodePoint, keyboardEventType, true);
    }

    public static GestureTrigger createKeyTrigger(int keyboardCodePoint, int keyboardEventType, boolean instant) {
        return new GestureTrigger(false,keyboardCodePoint, keyboardEventType, -1, -1,instant);
    }

    public static GestureTrigger createRepeatInstantMouseTrigger(int mouseButton) {
        return createMouseTrigger(mouseButton, MouseMessage.TYPE_PRESS, false);
    }

    public static GestureTrigger createInstantMouseTrigger(int mouseButton) {
        return createMouseTrigger(mouseButton, MouseMessage.TYPE_PRESS, true);
    }

    public static GestureTrigger createInstantMouseTrigger(int mouseButton, int mouseEventType) {
        return createMouseTrigger(mouseButton, mouseEventType, true);
    }

    public static GestureTrigger createMouseTrigger(int mouseButton, int mouseEventType, boolean instant) {
        return new GestureTrigger(true, -1,-1,mouseButton,mouseEventType, instant);
    }

}
