
package science.unlicense.display.impl.light;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.geometry.impl.AbstractFragment;
import science.unlicense.geometry.impl.Fragment;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Vector3f64;

/**
 * Set of constants properties names often used by fragments.
 *
 * @author Johann Sorel
 */
public abstract class PixelFragment extends AbstractFragment {

    public static final Chars POSITION_MODEL = Chars.constant("POSITION_MODEL");
    public static final Chars NORMAL_MODEL = Chars.constant("NORMAL_MODEL");
    public static final Chars POSITION_VIEW = Chars.constant("POSITION_VIEW");
    public static final Chars NORMAL_VIEW = Chars.constant("NORMAL_VIEW");
    public static final Chars EYE_RAY = Chars.constant("EYE_RAY");
    public static final Chars FRAGMENT_UV = Chars.constant("UV");

    private PixelFragment(){}

    public abstract TupleRW getPositionModel();

    public abstract void setPositionModel(TupleRW value);

    public abstract TupleRW getNormalModel();

    public abstract void setNormalModel(TupleRW value);

    public abstract TupleRW getPositionView();

    public abstract void setPositionView(TupleRW value);

    public abstract TupleRW getNormalView();

    public abstract void setNormalView(TupleRW value);

    public abstract TupleRW getEyeDirection();

    public abstract void setEyeDirection(TupleRW value);

    public abstract TupleRW getUV();

    public abstract void setUV(TupleRW value);

    public static PixelFragment view(Fragment fragment) {
        return new DecoratedFragment(fragment);
    }

    private static class DecoratedFragment extends PixelFragment {

        private final Fragment parent;

        public DecoratedFragment(Fragment parent) {
            this.parent = parent;
        }

        @Override
        public TupleRW getCoordinate() {
            return parent.getCoordinate();
        }

        @Override
        public Dictionary properties() {
            return parent.properties();
        }

        @Override
        public Vector3f64 getPositionModel() {
            return (Vector3f64) properties().getValue(POSITION_MODEL);
        }

        @Override
        public void setPositionModel(TupleRW value) {
            properties().add(POSITION_MODEL, value);
        }

        @Override
        public TupleRW getNormalModel() {
            return (TupleRW) properties().getValue(NORMAL_MODEL);
        }

        @Override
        public void setNormalModel(TupleRW value) {
            properties().add(NORMAL_MODEL, value);
        }

        @Override
        public TupleRW getPositionView() {
            return (TupleRW) properties().getValue(POSITION_VIEW);
        }

        @Override
        public void setPositionView(TupleRW value) {
            properties().add(POSITION_VIEW, value);
        }

        @Override
        public TupleRW getNormalView() {
            return (TupleRW) properties().getValue(NORMAL_VIEW);
        }

        @Override
        public void setNormalView(TupleRW value) {
            properties().add(NORMAL_VIEW, value);
        }

        @Override
        public TupleRW getEyeDirection() {
            return (TupleRW) properties().getValue(EYE_RAY);
        }

        @Override
        public void setEyeDirection(TupleRW value) {
            properties().add(EYE_RAY, value);
        }

        @Override
        public TupleRW getUV() {
            return (TupleRW) properties().getValue(FRAGMENT_UV);
        }

        @Override
        public void setUV(TupleRW value) {
            properties().add(FRAGMENT_UV, value);
        }

    }
}
