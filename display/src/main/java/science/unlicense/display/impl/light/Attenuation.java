
package science.unlicense.display.impl.light;

import science.unlicense.math.api.Maths;

/**
 * Light attenuation.
 *
 * Collada 1.5 :
 * The <constant_attenuation>, <linear_attenuation>, and <quadratic_attenuation> are
 * used to calculate the total attenuation of this light given a distance.
 *
 * The equation used is :
 * A = constant_attenuation + ( Dist * linear_attenuation ) + (( Dist^2 ) * quadratic_attenuation )
 *
 * @author Johann Sorel
 */
public final class Attenuation {

    private float constant;
    private float linear;
    private float quadratic;

    public Attenuation() {
        constant = 1;
        linear = 0;
        quadratic = 0;
    }

    public Attenuation(float constant, float linear, float quadratic) {
        this.constant = constant;
        this.linear = linear;
        this.quadratic = quadratic;
    }

    public float getConstant() {
        return constant;
    }

    public void setConstant(float constant) {
        this.constant = constant;
    }

    public float getLinear() {
        return linear;
    }

    public void setLinear(float linear) {
        this.linear = linear;
    }

    public float getquadratic() {
        return quadratic;
    }

    public void setQuadratic(float exp) {
        this.quadratic = exp;
    }

    public double computeAttenuation(double distance) {
        return 1.0 / (constant
             + linear * distance
             + quadratic * distance * distance);
    }

    /**
     * Compute maximum distance until attenuation result is zero.
     * @return distance or NaN is no solution.
     */
    public double getMaxDistance() {

        //compute max distance based on attenuation
        // Q x d² + L x d + C = 0
        // ax² + bx + c = 0
        // dis = b² - 4ac
        double dis = linear*linear - 4*quadratic*constant;

        double maxDist;
        if (dis > 0) {
            double v1 = (-linear - Math.sqrt(dis)) / (2*quadratic);
            double v2 = (-linear + Math.sqrt(dis)) / (2*quadratic);
            maxDist = (float) Maths.max(v1, v2);
        } else if (dis == 0) {
            maxDist = (-linear) / (2*quadratic);
        } else {
            //no solution
            maxDist = Double.NaN;
        }
        return maxDist;
    }

}
