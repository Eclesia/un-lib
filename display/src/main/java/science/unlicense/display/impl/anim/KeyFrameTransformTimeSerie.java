
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.KeyFrame;
import science.unlicense.display.api.anim.KeyFrameTimeSerie;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.SimilarityNd;

/**
 *
 * @author Johann Sorel
 */
public class KeyFrameTransformTimeSerie extends KeyFrameTimeSerie implements TransformTimeSerie{

    public int getDimension() {
        if (getFrames().isEmpty()) {
            return 0;
        } else {
            TransformKeyFrame frame = (TransformKeyFrame) getFrames().createIterator().next();
            return frame.getValue().getDimension();
        }
    }

    @Override
    public TransformKeyFrame interpolate(double time, KeyFrame buffer) {

        final KeyFrame[] nearest = getNearest(time, null);
        final TransformKeyFrame underFrame = (TransformKeyFrame) nearest[0];
        final TransformKeyFrame aboveFrame = (TransformKeyFrame) nearest[1];

        final TransformKeyFrame frame = buffer==null ? new TransformKeyFrame() : (TransformKeyFrame) buffer;
        if (frame.getValue()==null) frame.setValue(SimilarityNd.create(underFrame.getValue().getDimension()));

        if (underFrame == aboveFrame) {
            //we are at the begin, the end, or exactly on a frame
            frame.getValue().set(underFrame.getValue());
        } else {
            //interpolate
            final double extent = aboveFrame.getTime() - underFrame.getTime();
            final double ratio = (time-underFrame.getTime()) / extent;

            final Similarity ub = underFrame.getValue();
            final Similarity ab = aboveFrame.getValue();
            final VectorRW translation = ub.getTranslation().lerp(ab.getTranslation(), ratio, null);
            final VectorRW scale = ub.getScale().lerp(ab.getScale(), ratio, null);

            final int dim = scale.getSampleCount();
            final Quaternion rotation;
            if (dim==2) {
                Matrix3x3 mu = new Matrix3x3().setToIdentity();
                mu.set(ub.getRotation());
                Matrix3x3 ma = new Matrix3x3().setToIdentity();
                ma.set(ab.getRotation());
                rotation = mu.toQuaternion().slerp(ma.toQuaternion(), ratio, null);
                rotation.localNormalize();
            } else if (dim==3) {
                rotation = new Matrix3x3(ub.getRotation()).toQuaternion().slerp(new Matrix3x3(ab.getRotation()).toQuaternion(), ratio, null);
                rotation.localNormalize();
            } else {
                throw new UnimplementedException(dim +" dimensions transform interpolation not supported.");
            }

            final SimilarityRW transform = frame.getValue();
            transform.getTranslation().set(translation);
            transform.getRotation().set(rotation.toMatrix3());
            transform.getScale().set(scale);
            transform.notifyChanged();
            frame.setValue(transform);
        }

        frame.setTime(time);
        return frame;
    }

}
