
package science.unlicense.display.impl.updater;

import science.unlicense.display.api.anim.Animation;
import science.unlicense.display.impl.DisplayTimerState;

/**
 *
 * @author Johann Sorel
 */
public class AnimationUpdater implements Updater {

    private final Animation animation;

    public AnimationUpdater(Animation animation) {
        this.animation = animation;
    }

    public Animation getAnimation() {
        return animation;
    }

    public void update(DisplayTimerState context) {
        animation.pulse(context.getTimeNano());
    }

}
