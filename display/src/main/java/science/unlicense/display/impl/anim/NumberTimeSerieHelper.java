
package science.unlicense.display.impl.anim;

import science.unlicense.display.api.anim.Timer;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.math.api.EasingMethod;

/**
 *
 * @author Johann Sorel
 */
public class NumberTimeSerieHelper {

    private final NumberAnimation animation = new NumberAnimation();
    private final KeyFrameNumberTimeSerie timeSerie = new KeyFrameNumberTimeSerie();
    private NumberKeyFrame keyFrame = null;

    public NumberTimeSerieHelper() {
    }

    public NumberTimeSerieHelper reset() {
        animation.setRepeatCount(0);
        animation.setSpeed(1.0f);
        animation.setTime(0.0f);
        animation.setTimer(null);
        timeSerie.setMethod(EasingMethod.create(EasingMethod.METHOD_LINEAR));
        timeSerie.getFrames().removeAll();
        animation.setTimeSerie(timeSerie);
        keyFrame = null;
        return this;
    }

    /**
     * Set interpolation method
     *
     * @param method
     * @return TrsBuilder this
     */
    public NumberTimeSerieHelper method(EasingMethod method) {
        timeSerie.setMethod(method);
        return this;
    }

    /**
     * Set animation timer.
     *
     * @param timer
     * @return TrsBuilder this
     */
    public NumberTimeSerieHelper timer(Timer timer) {
        animation.setTimer(timer);
        return this;
    }

    /**
     * Set animation repeat.
     *
     * @param nbReapeat
     * @return TrsBuilder this
     */
    public NumberTimeSerieHelper repeat(int nbReapeat) {
        animation.setRepeatCount(nbReapeat);
        return this;
    }

    /**
     * Move to give time, reusing or creating a new keyframe.
     *
     * @param timeMs
     * @return TrsBuilder this
     */
    public NumberTimeSerieHelper at(long timeMs) {
        keyFrame = (NumberKeyFrame) timeSerie.getFrame(timeMs);
        if (keyFrame==null) {
            keyFrame = new NumberKeyFrame();
            keyFrame.setTime(timeMs);
            timeSerie.getFrames().add(keyFrame);
        }
        return this;
    }

    public NumberTimeSerieHelper value(double val) {
        keyFrame.setValue(val);
        return this;
    }

    public NumberAnimation build() {
        final NumberAnimation anim = new NumberAnimation();
        anim.setRepeatCount(animation.getNbRepeat());
        anim.setSpeed(animation.getSpeed());
        anim.setTimer(animation.getTimer());

        final KeyFrameNumberTimeSerie cp = new KeyFrameNumberTimeSerie();
        cp.setMethod(timeSerie.getMethod());

        final Iterator ite = timeSerie.getFrames().createIterator();
        while (ite.hasNext()) {
            final NumberKeyFrame kf = (NumberKeyFrame) ite.next();
            final NumberKeyFrame cpkf = new NumberKeyFrame();
            cpkf.setTime(kf.getTime());
            cpkf.setValue(kf.getValue());
            cp.getFrames().add(cpkf);
        }

        return anim;
    }

}
