
package science.unlicense.display.impl.updater;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.GestureState;
import science.unlicense.display.impl.GestureTrigger;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;

/**
 * Local space controller.
 * This can be used to create first person or plane like controllers.
 * Node controller responding to Z,Q,S,D and mouse to move node around.
 *
 * @author Johann Sorel
 */
public class FirstPersonUpdater extends NodeTransformUpdater implements EventListener {

    private final EventSource source;

    //variables
    private boolean enable = true;
    private double keySpeed = 10;
    private double mouseSpeed = 0.005;
    private double deltaTime = -1;
    private Tuple warpPosition = null;

    //current gesture state
    private final GestureState gestureState = new GestureState();
    private final Sequence gestureTasks = new ArraySequence();

    public FirstPersonUpdater(EventSource source, SceneNode node, VectorRW up, VectorRW right) {
        super(node, up, right);
        this.source = source;

        if (source!=null) {
            source.addEventListener(MouseMessage.PREDICATE, this);
            source.addEventListener(KeyMessage.PREDICATE, this);
        }
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public boolean isEnable() {
        return enable;
    }

    /**
     * Get pointer warp position.
     * Works ony if event source is a NewtFrame.
     *
     * On each mouse event the mouse position is reset to this coordinate.
     * This allow to avoid mouse hitting the frame borders.
     *
     * @return Tuple can be null
     */
    public Tuple getWarpPointer() {
        return warpPosition;
    }

    public void setWarpPointer(Tuple autoWarp) {
        this.warpPosition = autoWarp;
    }

    /**
     *
     * @return keyboard speed, in unit/second
     */
    public double getKeySpeed() {
        return keySpeed;
    }

    public void setKeySpeed(double keySpeed) {
        this.keySpeed = keySpeed;
    }

    /**
     *
     * @return mouse move speed, in radian/pixel
     */
    public double getMouseSpeed() {
        return mouseSpeed;
    }

    public void setMouseSpeed(double mouseSpeed) {
        this.mouseSpeed = mouseSpeed;
    }


    /**
     * Create a flying control with default controls.
     * Similar to what would be used for a plane or spaceship game.
     *
     * @return LocalController
     */
    public FirstPersonUpdater configureFlyingControls() {
        gestureTasks.removeAll();
        gestureTasks.add(new MoveForward(new Chars("Forward"),GestureTrigger.createRepeatKeyTrigger('z')));
        gestureTasks.add(new MoveBackward(new Chars("Backward"),GestureTrigger.createRepeatKeyTrigger('s')));
        gestureTasks.add(new MoveLeft(new Chars("Left"),GestureTrigger.createRepeatKeyTrigger('q')));
        gestureTasks.add(new MoveRight(new Chars("Right"),GestureTrigger.createRepeatKeyTrigger('d')));
        gestureTasks.add(new MoveUp(new Chars("Up"),GestureTrigger.createRepeatKeyTrigger('r')));
        gestureTasks.add(new MoveDown(new Chars("Down"),GestureTrigger.createRepeatKeyTrigger('f')));
        gestureTasks.add(new RotateHorizontal(new Chars("Rotate H"),GestureTrigger.createMouseTrigger(MouseMessage.BUTTON_1,MouseMessage.TYPE_MOVE,true)));
        gestureTasks.add(new RotateVertical(new Chars("Rotate V"),GestureTrigger.createMouseTrigger(MouseMessage.BUTTON_1,MouseMessage.TYPE_MOVE,true)));
        gestureTasks.add(new RotateRoll(new Chars("Roll"),GestureTrigger.createMouseTrigger(MouseMessage.BUTTON_3,MouseMessage.TYPE_MOVE,true)));
        return this;
    }

    /**
     * Create a first person control with default controls.
     * Similar to what would be used for a first person shooter game.
     *
     * @return LocalController
     */
    public FirstPersonUpdater configureFirstPersonControls() {
        gestureTasks.removeAll();
        gestureTasks.add(new MoveForward(new Chars("Forward"),GestureTrigger.createRepeatKeyTrigger('z')));
        gestureTasks.add(new MoveBackward(new Chars("Backward"),GestureTrigger.createRepeatKeyTrigger('s')));
        gestureTasks.add(new MoveLeft(new Chars("Left"),GestureTrigger.createRepeatKeyTrigger('q')));
        gestureTasks.add(new MoveRight(new Chars("Right"),GestureTrigger.createRepeatKeyTrigger('d')));
        gestureTasks.add(new RotateHorizontal(new Chars("Rotate H"),GestureTrigger.createMouseTrigger(MouseMessage.BUTTON_1,MouseMessage.TYPE_MOVE,true)));
        gestureTasks.add(new RotateVertical(new Chars("Rotate V"),GestureTrigger.createMouseTrigger(MouseMessage.BUTTON_1,MouseMessage.TYPE_MOVE,true)));
        return this;
    }

    /**
     * Live sequence of GestureTasks.
     *
     * @return Sequence of GestureTask.
     */
    public Sequence getGestureTasks() {
        return gestureTasks;
    }

    /**
     * Difference time with previous event.
     * @returndiff time in seconds
     */
    public double getDeltaTime() {
        return deltaTime;
    }

    @Override
    public void update(DisplayTimerState context) {
        if (!enable) return;
        deltaTime = context.getDiffTimeSecond();

        synchronized (gestureState) {
            gestureState.update(null);
            for (int i=0,n=gestureTasks.getSize();i<n;i++) {
                final GestureTask task = (GestureTask) gestureTasks.get(i);
                //execute controls which do not requiere instantanous operations
                if (!task.getTrigger().isInstant()) {
                    final GestureTrigger trigger = task.getTrigger();
                    if (trigger.evaluate(gestureState)) {
                        task.execute(gestureState, this);
                    }
                }
            }
        }

        super.update(context); //To change body of generated methods, choose Tools | Templates.
    }



    public void receiveEvent(Event event) {
        if (!enable) return;
        if (event.getMessage().isConsumed()) return;

        synchronized (gestureState) {
            gestureState.update(event);
            for (int i=0,n=gestureTasks.getSize();i<n;i++) {
                final GestureTask task = (GestureTask) gestureTasks.get(i);
                //execute controls which requiere instantanous operations
                if (task.getTrigger().isInstant()) {
                    final GestureTrigger trigger = task.getTrigger();
                    if (trigger.evaluate(gestureState)) {
                        task.execute(gestureState, this);
                    }
                }
            }

            //warp pointer if requested
            if (warpPosition!=null) {
//                if (source instanceof NewtFrame) {
//                    ((NewtFrame) source).glWindow.warpPointer((int) warpPosition.getX(), (int) warpPosition.getY());
//                    gestureState.mouseWarped(warpPosition.getX(), warpPosition.getY());
//                }
            }

        }
    }

    public abstract static class GestureTask {

        protected GestureTrigger trigger;
        protected CharArray name = Chars.EMPTY;

        public GestureTask() {}

        public GestureTask(GestureTrigger trigger) {
            this.trigger = trigger;
        }

        public GestureTask(Chars name,GestureTrigger trigger) {
            this.name = name;
            this.trigger = trigger;
        }

        public CharArray getName() {
            return name;
        }

        public void setName(CharArray name) {
            this.name = name;
        }

        public GestureTrigger getTrigger() {
            return trigger;
        }

        public void setTrigger(GestureTrigger trigger) {
            this.trigger = trigger;
        }

        public abstract void execute(GestureState state, FirstPersonUpdater control);

    }

    public static class MoveForward extends GestureTask{
        public MoveForward() {}
        public MoveForward(GestureTrigger trigger) {super(trigger);}
        public MoveForward(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, FirstPersonUpdater control) {
            final float scale = (float) (control.keySpeed*control.deltaTime);
            control.moveForward(scale);
        }
    }

    public static class MoveBackward extends GestureTask{
        public MoveBackward() {}
        public MoveBackward(GestureTrigger trigger) {super(trigger);}
        public MoveBackward(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, FirstPersonUpdater control) {
            final float scale = (float) (control.keySpeed*control.deltaTime);
            control.moveBackward(scale);
        }
    }

    public static class MoveLeft extends GestureTask{
        public MoveLeft() {}
        public MoveLeft(GestureTrigger trigger) {super(trigger);}
        public MoveLeft(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, FirstPersonUpdater control) {
            final float scale = (float) (control.keySpeed*control.deltaTime);
            control.moveLeft(scale);
        }
    }

    public static class MoveRight extends GestureTask{
        public MoveRight() {}
        public MoveRight(GestureTrigger trigger) {super(trigger);}
        public MoveRight(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, FirstPersonUpdater control) {
            final float scale = (float) (control.keySpeed*control.deltaTime);
            control.moveRight(scale);
        }
    }

    public static class MoveUp extends GestureTask{
        public MoveUp() {}
        public MoveUp(GestureTrigger trigger) {super(trigger);}
        public MoveUp(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, FirstPersonUpdater control) {
            final float scale = (float) (control.keySpeed*control.deltaTime);
            control.moveUp(scale);
        }
    }

    public static class MoveDown extends GestureTask{
        public MoveDown() {}
        public MoveDown(GestureTrigger trigger) {super(trigger);}
        public MoveDown(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, FirstPersonUpdater control) {
            final float scale = (float) (control.keySpeed*control.deltaTime);
            control.moveDown(scale);
        }
    }

    public static class RotateHorizontal extends GestureTask{
        public RotateHorizontal() {}
        public RotateHorizontal(GestureTrigger trigger) {super(trigger);}
        public RotateHorizontal(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, FirstPersonUpdater control) {
            final double horizontalAngle = control.mouseSpeed * -state.mouseDiffX;
            control.rotate(control.getUp(), (float) horizontalAngle);
        }
    }

    public static class RotateVertical extends GestureTask{
        public RotateVertical() {}
        public RotateVertical(GestureTrigger trigger) {super(trigger);}
        public RotateVertical(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, FirstPersonUpdater control) {
            final double verticalAngle = control.mouseSpeed * -state.mouseDiffY;
            control.rotate(control.getRight(), (float) verticalAngle);
        }
    }

    public static class RotateRoll extends GestureTask{
        public RotateRoll() {}
        public RotateRoll(GestureTrigger trigger) {super(trigger);}
        public RotateRoll(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, FirstPersonUpdater control) {
            final double horizontalAngle = control.mouseSpeed * -state.mouseDiffX;
            control.rotate(control.getForward(), -(float) horizontalAngle);
        }
    }

}
