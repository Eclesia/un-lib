

package science.unlicense.display.api.layout;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.AbstractProperty;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractPositionableNode extends DefaultSceneNode implements Positionable {

    // layout informations
    protected final Extent effExtent = new Extent.Double(-1,-1);
    private final Extents overrides = new Extents();
    private Extents currentExts = null;

    protected LayoutConstraint layoutConstraint = null;
    protected boolean visible = true;
    protected boolean reserveSpace = true;

    public AbstractPositionableNode(boolean allowChildren) {
        super(CoordinateSystems.UNDEFINED_2D,allowChildren);
        overrides.minX = Double.NaN;
        overrides.minY = Double.NaN;
        overrides.bestX = Double.NaN;
        overrides.bestY = Double.NaN;
        overrides.maxX = Double.NaN;
        overrides.maxY = Double.NaN;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Extents getOverrideExtents() {
        return overrides.copy();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final void setOverrideExtents(Extents extent) {
        if (overrides.equals(extent)) return;
        final Extents old = this.overrides.copy();
        overrides.set(extent);
        Extents newExt = extent.copy();
        sendPropertyEvent(PROPERTY_OVERRIDE_EXTENTS, old, newExt);
        valueChanged(PROPERTY_OVERRIDE_EXTENTS, old, newExt);
        updateExtents();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Extent getEffectiveExtent() {
         return effExtent.copy();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setEffectiveExtent(Extent extent) {
        if (effExtent!=null && effExtent.equals(extent)) return;
        final Extent old = this.effExtent.copy();
        this.effExtent.set(extent);
        sendPropertyEvent(PROPERTY_EFFECTIVE_EXTENT, old, extent);
        valueChanged(PROPERTY_EFFECTIVE_EXTENT, old, extent);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final BBox getBoundingBox(BBox buffer) {
        return centeredBBox(effExtent, buffer);
    }

    public static BBox centeredBBox(Extent extent, BBox buffer) {
        if (buffer==null) buffer = new BBox(2);
        final double hx = extent.get(0) / 2.0;
        final double hy = extent.get(1) / 2.0;
        buffer.setRange(0, -hx, hx);
        buffer.setRange(1, -hy, hy);
        return buffer;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isReserveSpace() {
        return reserveSpace;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setReserveSpace(boolean reserve) {
        if (this.reserveSpace != reserve) {
            this.reserveSpace = reserve;
            sendPropertyEvent(PROPERTY_RESERVE_SPACE, !reserve, reserve);
            valueChanged(PROPERTY_RESERVE_SPACE, !reserve, reserve);
        }
    }

    public Property varReserveSpace() {
        return new AbstractProperty(this, PROPERTY_RESERVE_SPACE, Boolean.class, true, true) {
            @Override
            public Object getValue() throws RuntimeException {
                return isReserveSpace();
            }

            @Override
            public void setValue(Object value) throws RuntimeException {
                setReserveSpace((Boolean) value);
            }
        };
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setLayoutConstraint(LayoutConstraint layoutConstraint) {
        if (this.layoutConstraint != layoutConstraint) {
            final LayoutConstraint oldValue = this.layoutConstraint;
            this.layoutConstraint = layoutConstraint;
            sendPropertyEvent(PROPERTY_LAYOUT_CONSTRAINT, oldValue, layoutConstraint);
            valueChanged(PROPERTY_LAYOUT_CONSTRAINT, oldValue, layoutConstraint);
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public LayoutConstraint getLayoutConstraint() {
        return layoutConstraint;
    }

    public Property varLayoutConstraint() {
        return new AbstractProperty(this, PROPERTY_LAYOUT_CONSTRAINT, LayoutConstraint.class, true, true) {
            @Override
            public Object getValue() throws RuntimeException {
                return getLayoutConstraint();
            }

            @Override
            public void setValue(Object value) throws RuntimeException {
                setLayoutConstraint((LayoutConstraint) value);
            }
        };
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void setVisible(boolean visible) {
        if (this.visible != visible) {
            this.visible = visible;
            sendPropertyEvent(PROPERTY_VISIBLE, !visible, visible);
            valueChanged(PROPERTY_VISIBLE, !visible, visible);
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isVisible() {
        return visible;
    }

    public Property varVisible() {
        return new AbstractProperty(this, PROPERTY_VISIBLE, Boolean.class, true, true) {
            @Override
            public Object getValue() throws RuntimeException {
                return isVisible();
            }

            @Override
            public void setValue(Object value) throws RuntimeException {
                setVisible((Boolean) value);
            }
        };
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Extents getExtents(Extents buffer) {
        //defensive ref copy
        Extents ext = currentExts;

        if (ext == null) {
            ext = new Extents();
            getExtents(ext, effExtent);
            currentExts = ext;
        }
        if (buffer == null) {
            return ext.copy();
        } else {
            buffer.set(ext);
            return buffer;
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Extents getExtents(Extents buffer, Extent constraint) {
        if (buffer == null) buffer = new Extents();
        getExtentsInternal(buffer, constraint);
        //apply overrides
        if (!Double.isNaN(overrides.minX)) buffer.minX = overrides.minX;
        if (!Double.isNaN(overrides.minY)) buffer.minY = overrides.minY;
        if (!Double.isNaN(overrides.bestX)) buffer.bestX = overrides.bestX;
        if (!Double.isNaN(overrides.bestY)) buffer.bestY = overrides.bestY;
        if (!Double.isNaN(overrides.maxX)) buffer.maxX = overrides.maxX;
        if (!Double.isNaN(overrides.maxY)) buffer.maxY = overrides.maxY;
        return buffer;
    }

    /**
     * Calculate extents, ignore overrides, this is handle in AbstractPositionableNode
     */
    protected abstract void getExtentsInternal(Extents buffer, Extent constraint);

    /**
     * Recalculate widget min/max/best extent.Sends event if extent changed.
     *
     * @return true if an extent has been updated
     */
    protected boolean updateExtents() {
        Extents old = currentExts;
        currentExts = null;
        getExtents(null);
        if (old!=null && old.equals(currentExts)) {
            return false;
        } else {
            final Extents newExt = currentExts.copy();
            sendPropertyEvent(PROPERTY_EXTENTS, old, newExt);
            valueChanged(PROPERTY_EXTENTS, old, newExt);
            return true;
        }
    }

    protected void valueChanged(Chars name, Object oldValue, Object value) {
    }

}
