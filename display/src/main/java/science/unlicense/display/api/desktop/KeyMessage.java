
package science.unlicense.display.api.desktop;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.DefaultEventMessage;
import science.unlicense.common.api.event.MessagePredicate;
import science.unlicense.common.api.predicate.Predicate;

/**
 * A keyboard event
 *
 * @author Johann Sorel
 */
public class KeyMessage extends DefaultEventMessage {

    public static final Predicate PREDICATE = new MessagePredicate(KeyMessage.class);

    public static final int TYPE_PRESS = 1;
    public static final int TYPE_RELEASE = 2;

    //TODO full list of codes
    private static int KC = 1;
    public static final int KC_BEGIN    = KC++;
    public static final int KC_END      = KC++;
    public static final int KC_ENTER    = KC++;
    public static final int KC_DELETE   = KC++;

    public static final int KC_TAB      = KC++;
    public static final int KC_CONTROL  = KC++;
    public static final int KC_ALT      = KC++;
    public static final int KC_SHIFT    = KC++;
    public static final int KC_BACKSPACE= KC++;

    public static final int KC_LEFT     = KC++;
    public static final int KC_UP       = KC++;
    public static final int KC_RIGHT    = KC++;
    public static final int KC_DOWN     = KC++;

    private final int type;
    private final int codePoint;
    private final int code;

    /**
     * Create a new key event.
     *
     * @param type one of KeyMessage.TYPE_X
     * @param cp character codepoint
     * @param code
     */
    public KeyMessage(int type, int cp, int code) {
        super(false);
        this.type = type;
        this.codePoint = cp;
        this.code = code;
    }

    /**
     * Keyboard event type.
     *
     * @return int, one of KeyMessage.TYPE_X
     */
    public int getType() {
        return type;
    }

    /**
     * Get the pressed character codepoint.
     *
     * @return int, unicode codepoint of typed character
     */
    public int getCodePoint() {
        return codePoint;
    }

    /**
     * KeyBoard key code.
     * http://help.adobe.com/en_US/AS2LCR/Flash_10.0/help.html?content=00000520.html
     * @return key code
     */
    public int getCode() {
        return code;
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("KeyMessage[");
        cb.append("type:"+type);
        cb.append(", code:"+code);
        cb.append(", codePoint:"+codePoint);
        cb.append(']');
        return cb.toChars();
    }

}
