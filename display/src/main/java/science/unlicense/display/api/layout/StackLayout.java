

package science.unlicense.display.api.layout;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Set;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Maths;

/**
 * A stack layout puts widgets above each other in a stack.
 * This allows to create overlay effects.
 *
 * @author Johann Sorel
 */
public final class StackLayout extends AbstractLayout {

    private static final Set DIRTY_PROPERTIES = Collections.staticSet(new Chars[]{Positionable.PROPERTY_LAYOUT_CONSTRAINT, Positionable.PROPERTY_VISIBLE, Positionable.PROPERTY_RESERVE_SPACE});

    private static final Sorter STACK_SORTER = new Sorter() {
        @Override
        public int sort(Object first, Object second) {
            final StackConstraint cst1 = (StackConstraint) ((Positionable) first).getLayoutConstraint();
            final StackConstraint cst2 = (StackConstraint) ((Positionable) second).getLayoutConstraint();
            final StackConstraint st1 = (StackConstraint) cst1;
            final StackConstraint st2 = (StackConstraint) cst2;
            return Float.compare(st1.getPriority(), st2.getPriority());
        }
    };

    public StackLayout() {
        super(StackConstraint.class);
    }

    @Override
    protected Set getDirtyingPropertyNames() {
        return DIRTY_PROPERTIES;
    }

    @Override
    protected void calculateExtents(Extents extents, Extent constraint) {
        final Positionable[] seq = getPositionableArray();
        extents.setAll(0,0);

        final Extents buffer = new Extents();
        for (int i=0;i<seq.length;i++) {
            seq[i].getExtents(buffer, null);
            extents.minX  = Maths.max(extents.minX,buffer.minX);
            extents.minY  = Maths.max(extents.minY,buffer.minY);
            extents.bestX = Maths.max(extents.bestX,buffer.bestX);
            extents.bestY = Maths.max(extents.bestY,buffer.bestY);
            extents.maxX  = Maths.max(extents.maxX,buffer.maxX);
            extents.maxY  = Maths.max(extents.maxY,buffer.maxY);
        }
    }

    @Override
    public void update() {
        final BBox bbox = getView();
        final Extent ext = bbox.getExtent();
        final double[] trs = bbox.getMiddle().toDouble();
        final Positionable[] seq = getPositionableArray();
        for (int i=0;i<seq.length;i++) {
            seq[i].setEffectiveExtent(ext);
            seq[i].getNodeTransform().setToTranslation(trs);
        }
    }

    @Override
    public Positionable[] getPositionableArray() {
        final Positionable[] poses = super.getPositionableArray();
        //sort them, we need to do this to have sorted when mouse events are processed
        Arrays.sort(poses, STACK_SORTER);
        Arrays.reverse(poses, 0, poses.length);
        return poses;
    }

}
