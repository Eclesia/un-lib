
package science.unlicense.display.api.painter3d;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;

/**
 *
 * @author Johann Sorel
 */
public interface Painter3D {

    public static final Chars LAYER_COLOR = Chars.constant("COLOR");
    public static final Chars LAYER_NORMAL_WORLD = Chars.constant("NORMAL_WORLD");
    public static final Chars LAYER_NORMAL_VIEW = Chars.constant("NORMAL_VIEW");
    public static final Chars LAYER_NORMAL_PROJECTION = Chars.constant("NORMAL_PROJECTION");
    public static final Chars LAYER_POSITION_WORLD = Chars.constant("POSITION_WORLD");
    public static final Chars LAYER_POSITION_VIEW = Chars.constant("POSITION_VIEW");
    public static final Chars LAYER_POSITION_PROJECTION = Chars.constant("POSITION_PROJECTION");

    void setCanvasSize(Extent.Long extent);

    Extent.Long getCanvasSize();

    MonoCamera getCamera();

    void setCamera(MonoCamera camera);

    void setScene(SceneNode scene);

    SceneNode getScene();

    Sequence getAfterEffects();

    Image render();

}
