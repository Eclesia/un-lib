
package science.unlicense.display.api.painter2d;

import science.unlicense.system.ModuleSeeker;


/**
 * Convinient methods to manipulate Painters.
 *
 * @author Johann Sorel
 */
public final class Painters {

    private Painters() {}

    /**
     * Get the first painter manager.
     * @return PainterManager
     */
    public static PainterManager getPainterManager() {
        return getManagers()[0];
    }

    /**
     * Lists available painter managers.
     * @return array of PainterManager, never null but can be empty.
     */
    public static PainterManager[] getManagers() {
        return (PainterManager[]) ModuleSeeker.findServices(PainterManager.class);
    }

}
