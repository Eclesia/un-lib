
package science.unlicense.display.api.desktop;

import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.math.api.transform.Transform;

/**
 *
 * @author Johann Sorel
 */
public class DefaultScreen implements Screen {

    private final double dpm;
    private final Extent size;

    public DefaultScreen(double dpm, Extent size) {
        this.dpm = dpm;
        this.size = size;
    }

    @Override
    public double getDotPerMillimeter() {
        return dpm;
    }

    @Override
    public Extent getExtent() {
        return size;
    }

}
