
package science.unlicense.display.api.scene.s2d;

import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.system.CoordinateSystems;

/**
 *
 * @author Johann Sorel
 */
public class DefaultGraphic2DFactory implements Graphic2DFactory{

    public static final DefaultGraphic2DFactory INSTANCE = new DefaultGraphic2DFactory();

    @Override
    public SceneNode createNode() {
        return new DefaultSceneNode(CoordinateSystems.UNDEFINED_2D);
    }

    @Override
    public GeometryNode2D createGeometryNode() {
        return new GeometryNode2D();
    }

    @Override
    public ImageNode2D createImageNode() {
        return new ImageNode2D();
    }

    @Override
    public TextNode2D createTextNode() {
        return new TextNode2D();
    }

}
