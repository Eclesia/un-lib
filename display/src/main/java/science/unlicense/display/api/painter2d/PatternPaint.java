
package science.unlicense.display.api.painter2d;

import science.unlicense.common.api.CObjects;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.geometry.api.BBox;
import science.unlicense.image.api.Image;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.impl.Affine2;

/**
 * Image pattern paint.
 * Merge of SVG and OpenVG specification.
 *
 * Specifications :
 * - SVG Pattern : https://www.w3.org/TR/SVG11/pservers.html#Patterns
 * - OpenVG Pattern Paint : https://www.khronos.org/registry/OpenVG/specs/openvg-1.1.pdf
 *
 * @author Johann Sorel
 */
public class PatternPaint implements Paint {

    private final Image pattern;
    private final Rectangle box;
    private final Color fillColor;
    private final SpreadMode tilingMode;

    /**
     * New pattern paint with given image.
     *
     * @param pattern image to tile
     * @param box image box for rendering, in current painter coordinate system.
     *          if null, default box will be set to image size
     * @param fillColor filling color used when outsider image box, used only if tilingMode is FILL
     *          if null, no filling will be applied
     * @param tilingMode behavior to use when pixels lies outside the image box
     *          if null, default value is set to FILL
     */
    public PatternPaint(Image pattern, Rectangle box, Color fillColor, SpreadMode tilingMode) {
        CObjects.ensureNotNull(pattern);
        this.pattern = pattern;
        this.box = box;
        this.fillColor = fillColor;
        this.tilingMode = tilingMode == null ? SpreadMode.FILL : tilingMode;
    }

    /**
     * Returns the pattern image repeated in the paint.
     *
     * @return pattern image, never null
     */
    public Image getPattern() {
        return pattern;
    }

    /**
     * Returns the image box.
     * The image box is a rectangle in which in pattern image is scaled.
     *
     * @return image fill box, never null
     */
    public Rectangle getBox() {
        return box;
    }

    /**
     * Returns the fill color.
     * The fill color is used only when tiling mode is FILL.
     *
     * @return filling color, can be null
     */
    public Color getFillColor() {
        return fillColor;
    }

    /**
     * Returns the behavior used when painting pixels outside the image box.
     *
     * @return tiling mode, never null
     */
    public SpreadMode getTilingMode() {
        return tilingMode;
    }

    @Override
    public void fill(Image image, Image flagImage, BBox flagBbox, Affine2 trs, AlphaBlending blending) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
