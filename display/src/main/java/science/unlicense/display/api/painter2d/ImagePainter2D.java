

package science.unlicense.display.api.painter2d;

import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;

/**
 * Painter2D which purpose is to produce an image.
 *
 * @author Johann Sorel
 */
public interface ImagePainter2D extends Painter2D{

    void setSize(Extent.Long size);

    Extent.Long getSize();

    Image getImage();

}
