
package science.unlicense.display.api.desktop;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.AbstractEventSource;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractFrame extends AbstractEventSource implements Frame {

    protected final Sequence childrenFrames = new ArraySequence();
    protected final Frame parentFrame;

    public AbstractFrame(Frame parentFrame) {
        this.parentFrame = parentFrame;
    }

    @Override
    public final Frame getParentFrame() {
        return parentFrame;
    }

    @Override
    public Sequence getChildrenFrames() {
        return Collections.readOnlySequence(childrenFrames);
    }

}
