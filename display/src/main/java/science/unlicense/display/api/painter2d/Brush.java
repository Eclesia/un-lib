
package science.unlicense.display.api.painter2d;

import science.unlicense.image.api.Image;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 * A Brush is used to render the outline of a geometry.
 * It is responsible for giving the shape (area which will be filled)
 * of the geometry outline.
 *
 * @author Johann Sorel
 */
public interface Brush {

    PlanarGeometry outline(PlanarGeometry geom, double[] resolution);

    void bitMask(PlanarGeometry geom, double[] resolution,Image flagImage);

    /**
     * Get the brush max width.
     * This does not have to be accurate, but mostly correct for other classes
     * to properly estimate the space used by the stroke.
     * @return double 0 or positive
     */
    double getMaxWidth();

}
