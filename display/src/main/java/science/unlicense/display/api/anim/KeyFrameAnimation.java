

package science.unlicense.display.api.anim;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.math.api.Maths;

/**
 * Animate multiple key frame time series.
 *
 * @author Johann Sorel
 */
public abstract class KeyFrameAnimation extends AbstractAnimation {

    // all time series
    protected final Sequence timeSeries = new ArraySequence();

    /**
     * Modifiable sequence of time series.
     * @return Sequence
     */
    public Sequence getSeries() {
        return timeSeries;
    }

    /**
     * {@inheritDoc }
     */
    public double getLength() {
        double totalenght = 0;
        for (int i=0,n=timeSeries.getSize();i<n;i++) {
            final KeyFrameTimeSerie serie = (KeyFrameTimeSerie) timeSeries.get(i);
            totalenght = Maths.max(serie.getLength(), totalenght);
        }
        return totalenght;
    }

    public void update() {
        for (Iterator ite=timeSeries.createIterator();ite.hasNext();) {
            final KeyFrameTimeSerie serie = (KeyFrameTimeSerie) ite.next();
            update(serie);
        }
    }

    protected abstract void update(KeyFrameTimeSerie timeSerie);

}
