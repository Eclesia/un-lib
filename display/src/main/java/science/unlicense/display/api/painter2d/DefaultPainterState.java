
package science.unlicense.display.api.painter2d;

import science.unlicense.display.api.font.FontChoice;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.math.impl.Affine2;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 *
 * @author Johann Sorel
 */
public class DefaultPainterState implements PainterState {

    private final Affine2 transform;
    private final PlanarGeometry clip;
    private final AlphaBlending blending;
    private final Paint paint;
    private final Brush brush;
    private final FontChoice font;

    public DefaultPainterState(Affine2 transform, PlanarGeometry clip, AlphaBlending blending, Paint paint, Brush brush, FontChoice font) {
        this.transform = transform;
        this.clip = clip;
        this.blending = blending;
        this.paint = paint;
        this.brush = brush;
        this.font = font;
    }

    @Override
    public Affine2 getTransform() {
        return transform;
    }

    @Override
    public PlanarGeometry getClip() {
        return clip;
    }

    @Override
    public AlphaBlending getAlphaBlending() {
        return blending;
    }

    @Override
    public Paint getPaint() {
        return paint;
    }

    @Override
    public Brush getBrush() {
        return brush;
    }

    @Override
    public FontChoice getFont() {
        return font;
    }

}
