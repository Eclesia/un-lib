

package science.unlicense.display.api.scene;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.AffineRW;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.impl.AffineN;

/**
 * Utilities for scene and scene nodes.
 *
 * @author Johann Sorel
 */
public final class SceneUtils {

    private static final NodeVisitor INVERT_WORLD_TO_NODE = new NodeVisitor() {
            /**
             * @param node
             * @param root world root node
             */
            public Object visit(Node node, Object root) {
                if (node instanceof SceneNode) {
                    //visit child before this node
                    super.visit(node, root);
                    if (node != root) {
                        reverseWorldToNodeTrs((SceneNode) node, true);
                    }
                }
                return null;
            }
        };

    private static final NodeVisitor INVERT_PARENT_TO_NODE = new NodeVisitor() {
            /**
             * @param node
             * @param root world root node
             */
            public Object visit(Node node, Object root) {
                if (node instanceof SceneNode) {
                    if (node != root) {
                        reverseParentToNodeTrs((SceneNode) node);
                    }
                    super.visit(node, root);
                }
                return null;
            }
        };

    private SceneUtils() {}

    /**
     * When all scene node transforms are world root to node transforms.
     * This method will rebuild the parent to node transforms.
     */
    public static void reverseWorldToNodeTrsRecursive(SceneNode worldRoot) {
        INVERT_WORLD_TO_NODE.visit(worldRoot, worldRoot);
    }

    /**
     * reverse a single node world to node transform.
     * @param candidate
     * @param parentInWorldTrs true if the parent is in world space
     */
    public static void reverseWorldToNodeTrs(SceneNode candidate, boolean parentInWorldTrs) {
        final SceneNode parent = candidate.getParent();
        if (parent != null) {
            final Similarity prmatrix;
            if (parentInWorldTrs) {
                prmatrix = parent.getNodeTransform();
            } else {
                prmatrix = parent.getNodeToRootSpace();
            }
            final SimilarityRW jtmatrix = candidate.getNodeTransform();
            final AffineRW m = prmatrix.invert();
            m.localMultiply(jtmatrix);
            candidate.getNodeTransform().set(m);
        }
    }

    /**
     * When all joints transforms are parent to node transforms.
     * This method will rebuild the world to node transforms.
     */
    public static void reverseParentToNodeTrsRecursive(SceneNode worldRoot) {
        INVERT_PARENT_TO_NODE.visit(worldRoot, worldRoot);
    }

    public static void reverseParentToNodeTrs(SceneNode candidate) {
        final SceneNode parent = candidate.getParent();
        if (parent != null) {
            final Affine prmatrix = parent.getNodeTransform();
            final Affine jtmatrix = candidate.getNodeTransform();
            final AffineRW m = AffineN.create(prmatrix);
            m.localMultiply(jtmatrix);
            candidate.getNodeTransform().set(m);
        }
    }

    /**
     * Calculate transform from source to target node.
     *
     * @param source
     * @param target
     * @return Affine never null
     */
    public static Affine sourceToTarget(SceneNode source, SceneNode target){
        return target.getRootToNodeSpace().multiply(source.getNodeToRootSpace());
    }

    public static BBox calculateBBox(SceneNode node, CoordinateSystem cs) {

        if (cs == null) {
            cs = node.getCoordinateSystem();
        }

        if (node instanceof GraphicNode) {
            return ((GraphicNode) node).getBBox(cs);
        }

        BBox bbox = null;
        final Iterator ite = node.getChildren().createIterator();
        while (ite.hasNext()) {
            final Object next = ite.next();
            if (next instanceof SceneNode){
                final SceneNode child = (SceneNode) next;
                final BBox cbbox = calculateBBox(child, cs);
                if (cbbox != null) {
                    //convert it to this node space
                    final Similarity m = child.getNodeTransform();
                    m.inverseTransform(cbbox.getLower(),cbbox.getLower());
                    m.inverseTransform(cbbox.getUpper(),cbbox.getUpper());
                    cbbox.reorder();
                    if (bbox == null) {
                        bbox = new BBox(cbbox);
                    } else {
                        bbox.expand(cbbox);
                    }
                }
            }
        }

        return bbox;
    }

}
