
package science.unlicense.display.api.desktop.cursor;

/**
 * A cursor is a small graphic used to locate the mouse position on screen.
 * Cursor can be of 3 types :
 * - NamedCursor
 * - ImageCursor
 * - AnimatedCursor
 *
 * @author Johann Sorel
 */
public interface Cursor {

}
