
package science.unlicense.display.api.scene.s2d;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.math.api.VectorRW;

/**
 *
 * @author Johann Sorel
 */
public class TextNode2D extends GraphicNode2D{

    private CharArray text;
    private FontChoice font;
    protected Paint[] fills = null;
    private VectorRW anchor;

    public TextNode2D() {
        super();
    }

    public TextNode2D(CoordinateSystem cs) {
        super(cs);
    }

    public CharArray getText() {
        return text;
    }

    public void setText(CharArray text) {
        this.text = text;
    }

    public FontChoice getFont() {
        return font;
    }

    public void setFont(FontChoice font) {
        this.font = font;
    }

    public Paint[] getFills() {
        return fills;
    }

    public void setFills(Paint[] fills) {
        this.fills = fills;
    }

    public VectorRW getAnchor() {
        return anchor;
    }

    public void setAnchor(VectorRW anchor) {
        this.anchor = anchor;
    }

}
