
package science.unlicense.display.api.layout;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.Sorter;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Set;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;

/**
 *
 * Origin : adapted from https://github.com/simonwhitaker/fancypants
 *
 * @author Simon Whitaker (Python code)
 * @author Johann Sorel (ported to java and adapted to layouts)
 */
public final class WeightLayout extends AbstractLayout {

    private static final Set DIRTY_PROPERTIES = Collections.staticSet(new Chars[]{Positionable.PROPERTY_LAYOUT_CONSTRAINT, Positionable.PROPERTY_RESERVE_SPACE, Positionable.PROPERTY_VISIBLE});

    private static final Sorter SORTER = new Sorter() {
        @Override
        public int sort(Object first, Object second) {
            final WeightConstraint c1 = (WeightConstraint) ((Positionable) first).getLayoutConstraint();
            final WeightConstraint c2 = (WeightConstraint) ((Positionable) second).getLayoutConstraint();
            return c2.order(c1);
        }
    };

    //padding between the frames in the treemap (defaults to 0)
    private double padding;
    //threshold if total < threshold, aggregate all remaining values into an "other" frame
    private double threshold;

    public WeightLayout() {
        super(WeightConstraint.class);
    }

    public double getPadding() {
        return padding;
    }

    public void setPadding(double padding) {
        this.padding = padding;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    @Override
    protected void calculateExtents(Extents extents, Extent constraint) {
        //do nothing, positionable have they own transforms
        extents.minX = 0.0;
        extents.minX = 0.0;
        extents.bestX = 100;
        extents.bestY = 100;
        extents.maxX = Double.POSITIVE_INFINITY;
        extents.maxY = Double.POSITIVE_INFINITY;
    }

    @Override
    protected Set getDirtyingPropertyNames() {
        return DIRTY_PROPERTIES;
    }

    @Override
    public void update() {

        final BBox view = getView();
        final Positionable[] positionnables = getPositionableArray();

        double total = 0;
        for (Positionable p : positionnables) {
            WeightConstraint cst = (WeightConstraint) p.getLayoutConstraint();
            total += ((WeightConstraint) cst).getWeight();
        }

        //sort by weight order
        Arrays.sort(positionnables, SORTER);

        double x = view.getMin(0);
        double y = view.getMin(1);
        double w = view.getSpan(0);
        double h = view.getSpan(1);

        for (int i=0;i<positionnables.length;i++) {
            final Positionable child = positionnables[i];
            final WeightConstraint datum = (WeightConstraint) child.getLayoutConstraint();
            boolean isLast = false;

            //If there's a threshold and what's left sums to less than the threshold,
            //aggregate all the remaining data under "Others"
            if (total < threshold) {
                //not enough space left
                isLast = true;
            }

            if (i == positionnables.length - 1) {
                isLast = true;
            }

            // Coords for this panel
            double thisx = x;
            double thisy = y;
            double thisw = w;
            double thish = h;

            // If w > h then we'll fix the height to h and set
            // w to a proportion, else do the reverse
            boolean fixed_height = w > h;

            double proportion = datum.getWeight() / total;
            total -= datum.getWeight();

            if (fixed_height) {
                thisw  = w * proportion;
                w -= thisw;
                x += thisw;
            } else {
                thish = h * proportion;
                h -= thish;
                y += thish;
            }

            // Adding the padding...
            // If we're working on a fixed height, reduce the height by another 1 * padding
            // If we're working on a fixed width, reduce the width likewise
            // Special case: if this is the last frame, reduce neither
            if (fixed_height && !isLast) {
                thisw -= padding;
            }

            if (!fixed_height && !isLast) {
                thish -= padding;
            }

            // Sanity check: did we end up rendering a negative-sized frame?
            if (thisw < 0) thisw = 0;
            if (thish < 0) thish = 0;

            child.setEffectiveExtent(new Extent.Double(thisw, thish));
            child.getNodeTransform().setToTranslation(new double[]{thisx+thisw/2.0,thisy+thish/2.0});

            if (isLast) break;
        }

    }

}
