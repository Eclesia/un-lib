
package science.unlicense.display.api.painter2d;

import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.scene.s2d.Graphic2DFactory;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.math.api.Affine;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class DefaultDerivatePainter2D extends SimplifiedPainter2D {

    private final Painter2D base;
    private final State state = new State();

    public DefaultDerivatePainter2D(Painter2D base) {
        this.base = base;
    }

    @Override
    public Affine2 getTransform() {
        return new Affine2(state.transform);
    }

    @Override
    public void setTransform(Affine trs) {
        state.transform.set(trs);
    }

    @Override
    public FontStore getFontStore() {
        return base.getFontStore();
    }

    @Override
    public AlphaBlending getAlphaBlending() {
        return state.blending;
    }

    @Override
    public void setAlphaBlending(AlphaBlending alphaBlending) {
        state.blending = alphaBlending;
    }

    @Override
    public void setPaint(Paint paint) {
        state.paint = paint;
    }

    @Override
    public Paint getPaint() {
        return state.paint;
    }

    @Override
    public void setBrush(Brush brush) {
        state.brush = brush;
    }

    @Override
    public Brush getBrush() {
        return state.brush;
    }

    @Override
    public void setFont(FontChoice font) {
        state.font = font;
    }

    @Override
    public FontChoice getFont() {
        return state.font;
    }

    @Override
    public void setClip(PlanarGeometry clip) {
        state.clip = clip;
    }

    @Override
    public PlanarGeometry getClip() {
        return state.clip;
    }

    @Override
    public void fill(PlanarGeometry geom) {
        final PainterState saveState = base.saveState();
        base.restoreState(state);
        base.fill(geom);
        base.restoreState(saveState);
    }

    @Override
    public void stroke(PlanarGeometry geom) {
        final PainterState saveState = base.saveState();
        base.restoreState(state);
        base.stroke(geom);
        base.restoreState(saveState);
    }

    @Override
    public void paint(Image image, Affine transform) {
        final PainterState saveState = base.saveState();
        base.restoreState(state);
        base.paint(image, transform);
        base.restoreState(saveState);
    }

    @Override
    public Graphic2DFactory getGraphicFactory() {
        return base.getGraphicFactory();
    }

    @Override
    public Painter2D derivate(boolean copyState) {
        final Painter2D p = new DefaultDerivatePainter2D(this);
        if (copyState) p.restoreState(saveState());
        return p;
    }

    @Override
    public void flush() {
        base.flush();
    }

    @Override
    public void dispose() {
        base.dispose();
    }

    private class State implements PainterState {

        private Affine2 transform = new Affine2();
        private PlanarGeometry clip;
        private AlphaBlending blending = AlphaBlending.DEFAULT;
        private Paint paint;
        private Brush brush;
        private FontChoice font;

        @Override
        public Affine2 getTransform() {
            return transform;
        }

        @Override
        public PlanarGeometry getClip() {
            return clip;
        }

        @Override
        public AlphaBlending getAlphaBlending() {
            return blending;
        }

        @Override
        public Paint getPaint() {
            return paint;
        }

        @Override
        public Brush getBrush() {
            return brush;
        }

        @Override
        public FontChoice getFont() {
            return font;
        }

    }

}
