
package science.unlicense.display.api.model2d;

import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;

/**
 * TODO
 * Draft API for vector type formats :
 * SVG, VML, PS, PDF, ...
 * http://en.wikipedia.org/wiki/List_of_vector_graphics_markup_languages#Vector_formats
 *
 * @author Johann Sorel
 */
public interface Model2DStore extends Store {

    /**
     * Create a painter which will write in the store.
     * @return Painter2D
     */
    Painter2D createPainter() throws IOException;

    /**
     * Render the content of the store on given painter.
     * @param painter
     * @throws IOException
     */
    void paint(Painter2D painter) throws IOException;


}
