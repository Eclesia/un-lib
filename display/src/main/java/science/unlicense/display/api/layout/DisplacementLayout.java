

package science.unlicense.display.api.layout;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.event.Property;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;

/**
 * The displacement layout can handle a single positionable.
 * The positionable is placed at the top left corner and can be displaced
 * using offset values.
 *
 * @author Johann Sorel
 */
public final class DisplacementLayout extends AbstractLayout{

    private static final Set DIRTY_PROPERTIES = Collections.staticSet(new Chars[]{Positionable.PROPERTY_EXTENTS, Positionable.PROPERTY_VISIBLE, Positionable.PROPERTY_RESERVE_SPACE});

    public static final Chars PROPERTY_OFFSETX = Chars.constant("OffsetX");
    public static final Chars PROPERTY_OFFSETY = Chars.constant("OffsetY");

    public static final int HALIGN_LEFT = 0;
    public static final int HALIGN_CENTER = 1;
    public static final int HALIGN_RIGHT = 2;
    public static final int VALIGN_TOP = 0;
    public static final int VALIGN_CENTER = 1;
    public static final int VALIGN_BOTTOM = 2;

    private double offsetX = 0;
    private double offsetY = 0;
    private int halignement = HALIGN_LEFT;
    private int valignement = VALIGN_TOP;
    private boolean fillWidth = false;
    private boolean fillHeight = false;

    public DisplacementLayout() {
        super(null);
    }

    public boolean isFillWidth() {
        return fillWidth;
    }

    public void setFillWidth(boolean fillWidth) {
        if (this.fillWidth==fillWidth) return;
        this.fillWidth = fillWidth;
        setDirty();
    }

    public boolean isFillHeight() {
        return fillHeight;
    }

    public void setFillHeight(boolean fillHeight) {
        if (this.fillHeight==fillHeight) return;
        this.fillHeight = fillHeight;
        setDirty();
    }

    public int getVerticalAlignement() {
        return valignement;
    }

    public void setVerticalAlignement(int valignement) {
        if (this.valignement==valignement) return;
        this.valignement = valignement;
        setDirty();
    }

    public int getHorizontalAlignement() {
        return halignement;
    }

    public void setHorizontalAlignement(int halignement) {
        if (this.halignement==halignement) return;
        this.halignement = halignement;
        setDirty();
    }

    public double getOffsetX() {
        return offsetX;
    }

    public void setOffsetX(double offsetX) {
        if (this.offsetX == offsetX) return;
        final double old = this.offsetX;
        this.offsetX = offsetX;
        setDirty();
        sendPropertyEvent(PROPERTY_OFFSETX, old, offsetX);
    }

    public Property varOffsetX() {
        return getProperty(PROPERTY_OFFSETX);
    }

    public double getOffsetY() {
        return offsetY;
    }

    public void setOffsetY(double offsetY) {
        if (this.offsetY == offsetY) return;
        final double old = this.offsetY;
        this.offsetY = offsetY;
        setDirty();
        sendPropertyEvent(PROPERTY_OFFSETY, old, offsetY);
    }

    public Property varOffsetY() {
        return getProperty(PROPERTY_OFFSETY);
    }

    @Override
    protected Set getDirtyingPropertyNames() {
        return DIRTY_PROPERTIES;
    }

    @Override
    public void update() {

        final BBox extent = getView();

        final Positionable[] children = getPositionableArray();
        if (children.length == 0) return;
        final Positionable child = children[0];

        final Extent childExt;
        if (!fillHeight && !fillWidth) {
            childExt = children[0].getExtents(null, null).getBest(null);
        } else {
            Extent cst = new Extent.Double(extent.getSpan(0), extent.getSpan(1));
            if (!fillWidth) cst.set(0, Double.POSITIVE_INFINITY);
            if (!fillHeight) cst.set(1, Double.POSITIVE_INFINITY);
            childExt = children[0].getExtents(null, cst).getBest(null);
        }

        //calculate offset
        double dispX = offsetX;
        double dispY = offsetY;
        if (halignement==HALIGN_LEFT) {
            dispX -= (extent.getSpan(0)-childExt.get(0)) / 2.0;
        } else if (halignement==HALIGN_RIGHT) {
            dispX += (extent.getSpan(0)-childExt.get(0)) / 2.0;
        }

        if (valignement==VALIGN_TOP) {
            dispY -= (extent.getSpan(1)-childExt.get(1)) / 2.0;
        } else if (valignement==VALIGN_BOTTOM) {
            dispY += (extent.getSpan(1)-childExt.get(1)) / 2.0;
        }


        //fill
        if (fillWidth) {
            final double dx = extent.getSpan(0) - childExt.get(0)+offsetX;
            if (dx>0) {
                childExt.set(0, childExt.get(0)+dx);
                dispX += dx/2;
            }
        }
        if (fillHeight) {
            final double dy = extent.getSpan(1) - childExt.get(1)+offsetY;
            if (dy>0) {
                childExt.set(1, childExt.get(1)+dy);
                dispY += dy/2;
            }
        }


        child.setEffectiveExtent(childExt);
        child.getNodeTransform().setToTranslation(new double[]{dispX,dispY});
    }

    @Override
    protected void calculateExtents(Extents extents, Extent constraint) {
        final Positionable[] children = getPositionableArray();
        if (children.length==0) {
            extents.minX = 0.0;
            extents.minX = 0.0;
            extents.bestX = 0.0;
            extents.bestY = 0.0;
            extents.maxX = Double.POSITIVE_INFINITY;
            extents.maxY = Double.POSITIVE_INFINITY;
        } else {
            if (constraint==null) {
                children[0].getExtents(extents, null);
            } else if (!fillHeight && !fillWidth) {
                children[0].getExtents(extents, constraint);
            } else {
                Extent cst = new Extent.Double(constraint.get(0), constraint.get(1));
                if (!fillWidth) cst.set(0, Double.POSITIVE_INFINITY);
                if (!fillHeight) cst.set(1, Double.POSITIVE_INFINITY);
                children[0].getExtents(extents, cst);
            }
        }
    }

}
