
package science.unlicense.display.api.layout;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.AbstractIterator;
import science.unlicense.common.api.collection.CollectionMessage;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.collection.Triplet;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.tree.NodeMessage;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.math.api.Similarity;

/**
 * Common structure for layout instances.
 *
 * @author Johann Sorel
 */
public abstract class AbstractLayout extends AbstractEventSource implements Layout{

    protected static final Extent EMPTY = new Extent.Double(0, 0);
    protected static final Extents EMPTIES = new Extents();

    private final EventListener childListener = new EventListener() {
        @Override
        public void receiveEvent(Event event) {
            if (event.getMessage() instanceof PropertyMessage) {
                final Chars propertyName =  ((PropertyMessage) event.getMessage()).getPropertyName();
                if (getDirtyingPropertyNames().contains(propertyName)) {
                    //a layout related property has changed
                    setDirty();
                }
            }
        }
    };

    private final EventListener sequenceListener = new EventListener() {
        @Override
        public void receiveEvent(Event event) {
            CollectionMessage ce = (CollectionMessage) event.getMessage();
            final int type = ce.getType();
            switch (type) {
                case NodeMessage.TYPE_ADD:
                    {
                        //attached event listener on new elements
                        final Object[] elements = ce.getNewElements();
                        for (int i=0;i<elements.length;i++) {
                            ((Positionable) elements[i]).addEventListener(PropertyMessage.PREDICATE, childListener);
                        }       setDirty();
                        break;
                    }
                case NodeMessage.TYPE_REMOVE:
                    {
                        //remove event listener on new elements
                        final Object[] elements = ce.getOldElements();
                        for (int i=0;i<elements.length;i++) {
                            ((Positionable) elements[i]).removeEventListener(PropertyMessage.PREDICATE, childListener);
                        }       setDirty();
                        break;
                    }
                case NodeMessage.TYPE_REPLACEALL:
                    //remove event listener on new elements
                    final Object[] olds = ce.getOldElements();
                    for (int i=0;i<olds.length;i++) {
                        ((Positionable) olds[i]).removeEventListener(PropertyMessage.PREDICATE, childListener);
                    }   //attached event listener on new elements
                    final Object[] news = ce.getNewElements();
                    for (int i=0;i<news.length;i++) {
                        ((Positionable) news[i]).addEventListener(PropertyMessage.PREDICATE, childListener);
                    }   setDirty();
                    break;
                default:
                    break;
            }
        }
    };

    private Sequence positionables;
    private final BBox workingExtent = new BBox(2);
    private final Class layoutConstraintClass;

    public AbstractLayout(Class layoutConstraintClass) {
        this.layoutConstraintClass = layoutConstraintClass;
    }

    @Override
    public Class getLayoutConstraintClass() {
        return layoutConstraintClass;
    }

    @Override
    public final BBox getView() {
        synchronized (workingExtent) {
            return new BBox(workingExtent);
        }
    }

    @Override
    public final void setView(BBox bbox) {
        if (!bbox.isValid()) {
            throw new InvalidArgumentException("Unvalid bbox "+bbox);
        }
        final BBox old;
        synchronized (workingExtent) {
            if (this.workingExtent.equals(bbox)) return;
            old = new BBox(workingExtent);
            this.workingExtent.set(bbox);
        }
        sendPropertyEvent(PROPERTY_VIEW, old, new BBox(bbox));
        setDirty();
    }

    @Override
    public Extents getExtents(Extents buffer, Extent constraint) {
        if (buffer==null) buffer = new Extents();
        calculateExtents(buffer, constraint);
        return buffer;
    }

    @Override
    public void setPositionables(Sequence positionables) {
        if (this.positionables == positionables) return;

        //remove listener on previous collection
        if (this.positionables != null) {
            this.positionables.removeEventListener(CollectionMessage.PREDICATE, sequenceListener);
            for (int i=0,n=this.positionables.getSize();i<n;i++) {
                ((Positionable) this.positionables.get(i)).removeEventListener(PropertyMessage.PREDICATE, childListener);
            }
        }
        this.positionables = positionables;

        //listen to positionable list changes
        if (this.positionables != null) {
            this.positionables.addEventListener(CollectionMessage.PREDICATE, sequenceListener);
            for (int i=0,n=this.positionables.getSize();i<n;i++) {
                ((Positionable) this.positionables.get(i)).addEventListener(PropertyMessage.PREDICATE, childListener);
            }
        }
        setDirty();
    }

    @Override
    public Sequence getPositionables() {
        return positionables;
    }

    /**
     * Get positionable to process, only those which are :
     * - visible or has reserved space
     * - have a layout constraint of appropriate class
     *
     * Positionable who do not match the above conditions has their transform
     * scale set to NaN.
     *
     * @return
     */
    public Positionable[] getPositionableArray() {
        if (positionables == null || positionables.isEmpty()) return new Positionable[0];

        final Class layoutConstraintClass = getLayoutConstraintClass();
        Positionable[] array = new Positionable[positionables.getSize()];
        final Iterator ite = positionables.createIterator();
        int offset = 0;
        while (ite.hasNext()) {
            Positionable p = (Positionable) ite.next();
            if ( (p.isVisible() || p.isReserveSpace())
                 && (layoutConstraintClass == null || layoutConstraintClass.isInstance(p.getLayoutConstraint()))
                    ) {
                array[offset] = p;
                offset++;
            } else {
                p.getNodeTransform().getScale().setAll(Double.NaN);
            }
        }
        if (array.length != offset) {
            array = (Positionable[]) Arrays.resize(array, offset);
        }
        return array;
    }

    protected final void setDirty() {
        sendPropertyEvent(PROPERTY_DIRTY, false, true);
    }

    /**
     * Get child properties which cause the layout to be recomputed.
     * @return
     */
    protected abstract Set getDirtyingPropertyNames();

    /**
     * Calculate extend, without border.
     * @param extents
     * @param constraint
     */
    protected abstract void calculateExtents(Extents extents, Extent constraint);

    @Override
    public final Iterator getPositionables(final BBox searchArea) {
        final Positionable[] children = getPositionableArray();
        if (children.length==0) {
            return Collections.emptyIterator();
        }

        return new AbstractIterator() {
            private final BBox childSearchArea = new BBox(2);
            private final BBox childBbox = new BBox(2);
            private int i=0;

            @Override
            protected void findNext() {
                for (;nextValue==null && i<children.length;i++) {
                    final Positionable child = children[i];
                    child.getBoundingBox(childBbox);
                    if (childBbox.isEmpty()) continue;
                    final Similarity trs = child.getNodeTransform();

                    //check intersection with the dirty box
                    Geometries.transform(searchArea, trs.inverse(), childSearchArea);

                    //test intersection
                    if (childSearchArea.intersects(childBbox, false)) {
                        nextValue = new Triplet(child,trs,childSearchArea);
                    }
                }
            }
        };

    }

    @Override
    public final Class[] getEventClasses() {
        return new Class[]{
            PropertyMessage.class
        };
    }

}
