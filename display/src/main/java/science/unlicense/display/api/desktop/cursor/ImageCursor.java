
package science.unlicense.display.api.desktop.cursor;

import science.unlicense.image.api.Image;
import science.unlicense.math.api.Tuple;

/**
 *
 * @author Johann Sorel
 */
public class ImageCursor implements Cursor{

    private final Image image;
    private final Tuple pick;

    public ImageCursor(Image image, Tuple pick) {
        this.image = image;
        this.pick = pick;
    }

    public Image getImage() {
        return image;
    }

    public Tuple getPick() {
        return pick;
    }

}
