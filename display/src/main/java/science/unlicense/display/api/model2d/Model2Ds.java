

package science.unlicense.display.api.model2d;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.ViewNode;
import science.unlicense.concurrent.api.Formats;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public final class Model2Ds {

    private Model2Ds(){}

    /**
     * Lists available model 2d formats.
     * @return array of MediaFormat, never null but can be empty.
     */
    public static Model2DFormat[] getFormats(){
        return (Model2DFormat[]) Formats.getFormatsOfType(Model2DFormat.class);
    }

    public static boolean canDecode(Object input) throws IOException {
        return Formats.canDecode(input, new ArraySequence(getFormats()));
    }

    /**
     * Convinient method to open a store of unknowned format.
     * The method will loop on available formats until one can decode the source.
     *
     * @param input
     * @return MediaStore, never null
     * @throws IOException if not format could support the source.
     */
    public static Model2DStore open(Object input) throws IOException {
        return (Model2DStore) Formats.open(input, new ArraySequence(getFormats()));
    }

    public static Predicate createPredicate() {
        return new Predicate() {
            @Override
            public Boolean evaluate(Object candidate) {
                try {
                    if (candidate instanceof Path && ((Path) candidate).isContainer()) {
                        return true;
                    }

                    candidate = ViewNode.unWrap((Node) candidate);
                    return canDecode(candidate);
                } catch (IOException ex) {
                    Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                }
                return false;
            }
        };
    }
}
