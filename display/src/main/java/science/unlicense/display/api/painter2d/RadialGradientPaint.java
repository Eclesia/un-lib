
package science.unlicense.display.api.painter2d;

import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.tuple.BoxCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorRW;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2f64;

/**
 * RadialGradiant paint.
 * Merge of SVG and OpenVG specification.
 *
 * Specifications :
 * - SVG Gradients : https://www.w3.org/TR/SVG11/pservers.html#Gradients
 * - OpenVG Gradients : https://www.khronos.org/registry/OpenVG/specs/openvg-1.1.pdf
 *
 * @author Johann Sorel
 */
public class RadialGradientPaint implements Paint {

    private final double centerX;
    private final double centerY;
    private final double radius;
    private final double focusX;
    private final double focusY;
    private final double[] stopOffsets;
    private final Color[] stopColors;
    private final SpreadMode spreadMode;

    /**
     *
     * @param centerX
     * @param centerY
     * @param radius
     * @param focusX
     * @param focusY
     * @param stopOffsets
     * @param stopColors
     * @param spreadMode behavior to use when pixels lies outside the gradient
     *          if null, default value is set to PAD
     */
    public RadialGradientPaint(double centerX, double centerY,
                               double radius, double focusX, double focusY,
                               double[] stopOffsets, Color[] stopColors,
                               SpreadMode spreadMode) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.focusX = focusX;
        this.focusY = focusY;
        this.stopOffsets = stopOffsets;
        this.stopColors = stopColors;
        this.spreadMode = spreadMode == null ? SpreadMode.PAD : spreadMode;
    }

    public double getCenterX() {
        return centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public double getRadius() {
        return radius;
    }

    public double getFocusX() {
        return focusX;
    }

    public double getFocusY() {
        return focusY;
    }

    public double[] getStopOffsets() {
        return stopOffsets;
    }

    public Color[] getStopColors() {
        return stopColors;
    }

    public SpreadMode getSpreadMode() {
        return spreadMode;
    }

    public void fill(Image image, Image flagImage, BBox flagBbox, Affine2 trs, AlphaBlending blending) {

        //base image
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor colorCursor = cm.asTupleBuffer(image).cursor();
        final ColorRW pixel = Colors.castOrWrap(colorCursor.samples(), cs);

        //flag image
        final TupleGridCursor maskCursor = new BoxCursor(flagImage.getRawModel().asTupleBuffer(flagImage).cursor(),flagBbox);

        final Vector2f64 tc = new Vector2f64();


        while (maskCursor.next()) {

            if (maskCursor.samples().get(0)!=0) {
                final Tuple crd = maskCursor.coordinate();
                colorCursor.moveTo(crd);
                //calculate and set color
                trs.transform(crd, tc);
                double d = distance(centerX,centerY,tc.x,tc.y) / radius;
                if (d<0) d=0;
                if (d>1) d=1;

                //find interval
                int index=0;
                for (;index<stopOffsets.length-1;index++) {
                    if (d>= stopOffsets[index] && d<=stopOffsets[index+1]) {
                        break;
                    }
                }
                d = (d-stopOffsets[index]) / (stopOffsets[index+1]-stopOffsets[index]);

                final Color color = Colors.interpolate(stopColors[index], stopColors[index+1], (float) d);


                final float alpha = ((int) maskCursor.samples().get(0) & 0xff) / 255f;
                final Color col = new ColorRGB(color.getRed(), color.getGreen(),
                        color.getBlue(), color.getAlpha()*alpha);
                pixel.set(blending.blend(col, pixel));
            }
        }

    }

    static double distance(double startX, double startY, double endX, double endY) {
        final double adj = endX-startX;
        final double opp = endY-startY;
        return Math.sqrt(adj*adj + opp*opp);
    }

}
