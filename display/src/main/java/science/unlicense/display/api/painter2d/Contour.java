
package science.unlicense.display.api.painter2d;

import science.unlicense.common.api.collection.Pair;

/**
 * A contour is a pair of brush and Paint.
 *
 * @author Johann Sorel
 */
public class Contour extends Pair {

    public Contour(Brush value1, Paint value2) {
        super(value1, value2);
    }

    @Override
    public Brush getValue1() {
        return (Brush) super.getValue1();
    }

    @Override
    public Paint getValue2() {
        return (Paint) super.getValue2();
    }

}
