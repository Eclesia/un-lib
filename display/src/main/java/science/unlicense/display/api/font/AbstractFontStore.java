
package science.unlicense.display.api.font;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractFontStore implements FontStore{

    @Override
    public Font getFont(FontChoice font) {
        final Chars[] families = font.getFamilies();
        for (int k=0;k<families.length;k++) {
            final Font fc = getFont(families[k]);
            if (fc==null) continue;
            return fc.derivate(font.getSize());
        }
        return null;
    }

}
