
package science.unlicense.display.api.font;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.IntSet;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 * An existing font.
 *
 * @author Johann Sorel
 */
public interface Font {

    public static int WEIGHT_NONE = 0;
    public static int WEIGHT_BOLD = 1;

    /**
     * Returns the commn family name of this font.
     *
     * @return font family name.
     */
    Chars getFamily();

    /**
     *
     * @return font weight
     */
    int getWeight();

    /**
     * Returns the font size.
     * The font size is the distance between one baseline to the next line baseline.
     * Also called line spacing or leading.
     *
     * @return size in undefined units.
     */
    float getSize();

    /**
     * The store this font comes from.
     *
     * @return FontStore, never null.
     */
    FontStore getStore();

    /**
     * Get font family metadatas.
     *
     * @return FontMetadata
     */
    FontMetadata getMetaData();

    /**
     * List available glyphs.
     *
     * @return unicode sequence, never null
     */
    IntSet listCharacters();

    /**
     * Get a single slyph.
     *
     * @param unicode
     * @return glyph geometry
     */
    PlanarGeometry getGlyph(int unicode);

    /**
     * Get a single slyph.
     *
     * @param character
     * @return glyph geometry
     */
    PlanarGeometry getGlyph(Char character);

    /**
     * Derivate this font for given font size.
     *
     * @param fontSize from baseline to max ascent.
     * @return DefaultFontMetadata
     */
    Font derivate(float fontSize);

    /**
     * Calculate the character X offset in given char array.
     *
     * @param font
     * @param text
     * @param index
     * @return
     */
    public static double calculateOffset(Font font, CharArray text, int index) {
        final BBox bbox = font.getMetaData().getCharsBox(text.truncate(0, index));
        return bbox.getSpan(0);
    }
}
