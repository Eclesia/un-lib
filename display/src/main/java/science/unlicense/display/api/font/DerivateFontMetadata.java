
package science.unlicense.display.api.font;

import science.unlicense.geometry.api.BBox;

/**
 *
 * @author Johann Sorel
 */
public class DerivateFontMetadata extends AbstractFontMetadata{

    private final FontMetadata original;
    private final double scale;

    public DerivateFontMetadata(FontMetadata original, double height) {
        this.original = original;

        final BBox obox = original.getGlyphBox();
        this.scale = height/obox.getMax(1);
        final BBox box = new BBox(2);
        box.setRange(0, obox.getMin(0)*scale, obox.getMax(0)*scale);
        box.setRange(1, obox.getMin(1)*scale, obox.getMax(1)*scale);
        this.setHeight(height);
        this.setGlyphBox(box);
        this.setAdvanceWidthMax(original.getAdvanceWidthMax()*scale);
        this.setAscent(original.getAscent()*scale);
        this.setDescent(original.getDescent()*scale);
        this.setLineGap(original.getLineGap()*scale);
        this.setMinLeftSideBearing(original.getMinLeftSideBearing()*scale);
        this.setMinRightSideBearing(original.getMinRightSideBearing()*scale);
        this.setxMaxExtent(original.getXMaxExtent()*scale);

    }

    public double getAdvanceWidth(int c) {
        return scale*original.getAdvanceWidth(c);
    }

}
