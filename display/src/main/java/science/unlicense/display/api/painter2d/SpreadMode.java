
package science.unlicense.display.api.painter2d;

/**
 * Describes the behavior of a paint outside the gradient or pattern area.
 *
 * Merge of SVG and OpenVG specification.
 *
 * Specifications :
 * - SVG Gradients and Patterns : https://www.w3.org/TR/SVG11/pservers.html#Gradients
 * - OpenVG Paints : https://www.khronos.org/registry/OpenVG/specs/openvg-1.1.pdf
 *
 * @author Johann Sorel
 */
public enum SpreadMode {

    /**
     * Use fill color when outside paint pattern.
     * This property is usable only with Patter paints.
     */
    FILL,
    /**
     * Use the nearest color value from the paint.
     */
    PAD,
    /**
     * Restart the paint element when outside paint box.
     */
    REPEAT,
    /**
     * Restart with reflection the paint element when outside paint box.
     * This allow smoother repetition for gradients.
     */
    REFLECT
}
