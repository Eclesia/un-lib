
package science.unlicense.display.api.scene.s2d;

import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.system.CoordinateSystems;

/**
 * A Graphic is geometry with style rendering informations.
 *
 * @author Johann Sorel
 */
public class GraphicNode2D extends DefaultSceneNode{

    private PlanarGeometry clip;
    private AlphaBlending blending = null;

    public GraphicNode2D() {
        super(CoordinateSystems.UNDEFINED_2D);
    }

    public GraphicNode2D(CoordinateSystem cs) {
        super(cs);
    }

    public PlanarGeometry getClip() {
        return clip;
    }

    public void setClip(PlanarGeometry clip) {
        this.clip = clip;
    }

    public AlphaBlending getBlending() {
        return blending;
    }

    public void setBlending(AlphaBlending blending) {
        this.blending = blending;
    }



}
