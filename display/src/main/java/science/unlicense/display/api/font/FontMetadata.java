
package science.unlicense.display.api.font;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.geometry.api.BBox;

/**
 * Font informations.
 *
 * Simplified mapping of TTF tables : head,hhea,hmtx,vhea,vmtx.
 *
 * @author Johann Sorel
 */
public interface FontMetadata {

    /**
     * Get font height.
     * This value is lower or equal to ascent value.
     * This value is the distance between the baseline and the mean-line.
     * Also called x-height.
     *
     * @return font height
     */
    double getHeight();

    /**
     * Glyph max bounding boxes
     * @return BoundingBox never null
     */
    BBox getGlyphBox();

    /**
     * Distance from baseline of highest ascender.
     * @return double
     */
    double getAscent();

    /**
     * Distance from baseline of lowest descender.
     * @return double
     */
    double getDescent();

    /**
     * typographic line gap.
     * Distance between line ascent and above line descent.
     * This is also called the leading.
     *
     * @return double
     */
    double getLineGap();

    /**
     *
     * @return double
     */
    double getAdvanceWidthMax();

    /**
     *
     * @param unicode unicode code point
     * @return double
     */
    double getAdvanceWidth(int unicode);

    /**
     *
     * @param c
     * @return double
     */
    double getAdvanceWidth(Char c);

    /**
     *
     * @return double
     */
    double getMinLeftSideBearing();

    /**
     *
     * @return double
     */
    double getMinRightSideBearing();

    /**
     * max(lsb + (xMax-xMin))
     * @return double
     */
    double getXMaxExtent();

    /**
     * Calculate the bounding box for given c.
     * @param text
     * @return BoundingBox
     */
    BBox getCharBox(Char c);

    /**
     * Calculate the bounding box for given text.
     * @param text
     * @return BoundingBox
     */
    BBox getCharsBox(CharArray text);

    /**
     * Derivate this metadata for given font size.
     * @param fontSize from baseline to max ascent.
     * @return DefaultFontMetadata
     */
    FontMetadata derivate(double fontSize);

}
