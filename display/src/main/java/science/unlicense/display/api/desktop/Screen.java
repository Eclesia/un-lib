
package science.unlicense.display.api.desktop;

import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public interface Screen {
        /**
     *
     * @return screen size.
     */
    Extent getExtent();

    /**
     * Return the common dot per millimeter of this Screen.
     * Note : we do not used DotPerInch (DPI) because multiple inches exist,
     * it is not a unit based on scientific measures and is not part of the stantard units.
     *
     * @return dot per millimeter.
     */
    double getDotPerMillimeter();

}
