

package science.unlicense.display.api.model2d;

import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Format;

/**
 *
 * @author Johann Sorel
 */
public interface Model2DFormat extends Format{

    Model2DStore open(Object input) throws IOException;

}
