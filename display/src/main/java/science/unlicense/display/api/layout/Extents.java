
package science.unlicense.display.api.layout;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Maths;

/**
 * Positionable objects may be resized with some limitations.
 * This class defines the minimum, best and maximum extents the positionable
 * can use while still preserving a correct rendering.
 * Using an extent smaller or larger then the min/max extent will result in
 * a partial or breaked rendering.
 * The extents are in Dot units.
 *
 * @author Johann Sorel
 */
public final class Extents extends AbstractEventSource {

    public double minX;
    public double minY;
    public double bestX;
    public double bestY;
    public double maxX;
    public double maxY;

    public Extents() {
    }

    public Extents(double minX, double minY, double bestX, double bestY, double maxX, double maxY) {
        this.minX = minX;
        this.minY = minY;
        this.bestX = bestX;
        this.bestY = bestY;
        this.maxX = maxX;
        this.maxY = maxY;
    }

    public Extents(Extent ext) {
        this(ext.get(0),ext.get(1));
    }

    public Extents(double spanX, double spanY) {
        this.minX = spanX;
        this.minY = spanY;
        this.bestX = spanX;
        this.bestY = spanY;
        this.maxX = spanX;
        this.maxY = spanY;
    }

    public void set(Extents ext) {
        minX = ext.minX;
        minY = ext.minY;
        bestX = ext.bestX;
        bestY = ext.bestY;
        maxX = ext.maxX;
        maxY = ext.maxY;
    }

    public void setAll(Extent ext) {
        minX = ext.get(0);
        minY = ext.get(1);
        bestX = minX;
        bestY = minY;
        maxX = minX;
        maxY = minY;
    }

    public void setAll(double spanX, double spanY) {
        minX = spanX;
        minY = spanY;
        bestX = minX;
        bestY = minY;
        maxX = minX;
        maxY = minY;
    }

    public void setMin(Extent ext) {
        minX = ext.get(0);
        minY = ext.get(1);
    }

    public Extent getMin(Extent buffer) {
        if (buffer==null) {
            buffer = new Extent.Double(minX, minY);
        } else {
            buffer.set(0, minX);
            buffer.set(1, minY);
        }
        return buffer;
    }

    public void setBest(Extent ext) {
        bestX = ext.get(0);
        bestY = ext.get(1);
    }

    public Extent getBest(Extent buffer) {
        if (buffer==null) {
            buffer = new Extent.Double(bestX, bestY);
        } else {
            buffer.set(0, bestX);
            buffer.set(1, bestY);
        }
        return buffer;
    }

    public void setMax(Extent ext) {
        maxX = ext.get(0);
        maxY = ext.get(1);
    }

    public Extent getMax(Extent buffer) {
        if (buffer==null) {
            buffer = new Extent.Double(maxX, maxY);
        } else {
            buffer.set(0, maxX);
            buffer.set(1, maxY);
        }
        return buffer;
    }

    public Extents copy() {
        final Extents cp = new Extents();
        cp.minX = minX;
        cp.minY = minY;
        cp.bestX = bestX;
        cp.bestY = bestY;
        cp.maxX = maxX;
        cp.maxY = maxY;
        return cp;
    }

    /**
     * Resize given extent to conform to this extents limits.
     *
     * @param ex
     */
    public void constraint(Extent ex) {
        ex.set(0, Maths.clamp(ex.get(0), minX, maxX));
        ex.set(1, Maths.clamp(ex.get(1), minY, maxY));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Extents other = (Extents) obj;
        if (Double.doubleToLongBits(this.minX) != Double.doubleToLongBits(other.minX)) {
            return false;
        }
        if (Double.doubleToLongBits(this.minY) != Double.doubleToLongBits(other.minY)) {
            return false;
        }
        if (Double.doubleToLongBits(this.bestX) != Double.doubleToLongBits(other.bestX)) {
            return false;
        }
        if (Double.doubleToLongBits(this.bestY) != Double.doubleToLongBits(other.bestY)) {
            return false;
        }
        if (Double.doubleToLongBits(this.maxX) != Double.doubleToLongBits(other.maxX)) {
            return false;
        }
        if (Double.doubleToLongBits(this.maxY) != Double.doubleToLongBits(other.maxY)) {
            return false;
        }
        return true;
    }

    @Override
    public int getHash() {
        return 89;
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("Extents\n");
        cb.append("X(").appendNumber(minX).append(" < ").appendNumber(bestX).append(" < ").appendNumber(maxX).append(")\n");
        cb.append("Y(").appendNumber(minY).append(" < ").appendNumber(bestY).append(" < ").appendNumber(maxY).append(")");
        return cb.toChars();
    }

}
