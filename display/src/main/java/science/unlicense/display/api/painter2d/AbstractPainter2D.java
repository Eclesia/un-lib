
package science.unlicense.display.api.painter2d;

import science.unlicense.common.api.CObjects;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.scene.s2d.DefaultGraphic2DFactory;
import science.unlicense.display.api.scene.s2d.Graphic2DFactory;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.math.api.Affine;
import science.unlicense.math.impl.Affine2;

/**
 * Abstract painter 2d.
 *
 * @author Johann Sorel
 */
public abstract class AbstractPainter2D extends SimplifiedPainter2D{

    protected Paint fillPaint;
    protected Brush brush;
    protected FontChoice font;
    protected PlanarGeometry clip;
    protected boolean antialiazing = false;
    protected AlphaBlending alphaBlending = AlphaBlending.DEFAULT;
    protected final Affine2 transform = new Affine2();

    public AbstractPainter2D() {
    }

    public final Affine2 getTransform() {
        return new Affine2(transform);
    }

    public final void setTransform(Affine trs) {
        transform.set(trs);
    }

    public void setClip(PlanarGeometry geom) {
        this.clip = geom;
    }

    public PlanarGeometry getClip() {
        return clip;
    }

    public void setAntialiazing(boolean antialiazing) {
        this.antialiazing = antialiazing;
    }

    public boolean isAntialiazing() {
        return antialiazing;
    }

    public AlphaBlending getAlphaBlending() {
        return alphaBlending;
    }

    public void setAlphaBlending(AlphaBlending alphaBlending) {
        CObjects.ensureNotNull(alphaBlending);
        this.alphaBlending = alphaBlending;
    }

    public void setFont(FontChoice font) {
        this.font = font;
    }

    public FontChoice getFont() {
        return font;
    }

    public void setPaint(final Paint paint) {
        this.fillPaint = paint;
    }

    public Paint getPaint() {
        return fillPaint;
    }

    public void setBrush(final Brush brush) {
        this.brush = brush;
    }

    public Brush getBrush() {
        return brush;
    }

    public Graphic2DFactory getGraphicFactory() {
        return DefaultGraphic2DFactory.INSTANCE;
    }

    @Override
    public Painter2D derivate(boolean copyState) {
        final Painter2D p = new DefaultDerivatePainter2D(this);
        if (copyState) p.restoreState(saveState());
        return p;
    }

    /**
     * Default implementation, does nothing.
     */
    public void flush() {
    }

}
