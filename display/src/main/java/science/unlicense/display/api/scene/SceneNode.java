
package science.unlicense.display.api.scene;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.SimilarityRW;

/**
 * A scene node is a node located in space and in the tree structure.
 * Scene nodes have a reference to their parent and a transform with
 * rotation, translation and scale informations.
 *
 * @author Johann Sorel
 */
public interface SceneNode extends Node {

    /**
     * Property indicating when the node id changed.
     */
    public static final Chars PROPERTY_ID = Chars.constant("Id");
    /**
     * Property indicating when the node title changed.
     */
    public static final Chars PROPERTY_TITLE = Chars.constant("Title");
    /**
     * Property indicating the node parent.
     */
    public static final Chars PROPERTY_PARENT = Chars.constant("Parent");


    /**
     * Get node id
     *
     * @return should be a simple object such as Integer or Chars, may be null
     */
    Object getId();

    /**
     * Set node id.
     * @param id should be a simple object such as Integer or Chars, can be null
     */
    void setId(Object id);

    /**
     * Get node title.
     * The title is a short text meant to be displayed to the users.
     *
     * @return Chars, can be null.
     */
    CharArray getTitle();

    /**
     * Set node name.
     * @param name , can be null.
     */
    void setTitle(CharArray name);

    /**
     * Scene node children.
     *
     * @return Sequence
     */
    Sequence getChildren();

    /**
     * Get the number of dimensions used in this node.
     * Usualy 2D or 3D.
     * The scene node matrix size will be dimension+1.
     *
     * @return number of dimension.
     */
    int getDimension();

    /**
     * Get parent node.
     * @return parent node, can be null
     */
    SceneNode getParent();

    /**
     * Recursively explore parents until the root node is found.
     * @return root node, never null.
     *   This node is returned if it has no parent
     */
    SceneNode getRoot();

    /**
     * Get the coordinate system in which the values are declared.
     * If this node coordinate system is an UndefinedCS and has a parent, then parent
     * coordinate system will be returned.
     *
     * @return CoordinateSystem never null
     */
    CoordinateSystem getCoordinateSystem();

    /**
     * Get the coordinate system in which the values are declared.
     *
     * @return CoordinateSystem never null
     */
    CoordinateSystem getLocalCoordinateSystem();

    /**
     * Set the coordinate system in which the values are declared.
     *
     * @param coordinateSystem can not be null
     * @throws InvalidArgumentException is coordinate system dimension does not match node dimension
     */
    void setLocalCoordinateSystem(CoordinateSystem coordinateSystem) throws InvalidArgumentException;

    /**
     * Returns the parent to node transform.
     * This should be understood as the node to parent space transform.
     * If there is no parent, then getNodeTransform().asMatrix() and getNodeToRootSpace()
     * should be equal.
     *
     * @return this node 'Parent to Node' transform
     */
    SimilarityRW getNodeTransform();

    /**
     * Get the World to Node coordinate space.
     * Example :
     * If the node is 1 unit on X above the root, then this transform
     * will have -1 on the x axis of the translation to move from world coordinate
     * to Node coordinate.
     *
     * @return an immutable similarity calculate from the node chain.
     */
    Similarity getRootToNodeSpace();

    /**
     * Inverse of RootToNode.
     *
     * @return an immutable similarity calculate from the node chain.
     */
    Similarity getNodeToRootSpace();

    /**
     * Update the node world to node transform.
     * Implementation may update any transform in the world to node chain
     * as long as the world to node transform is correct at the end.
     * @param wtn must be orthogonal of size dimension+1
     */
    void setRootToNodeSpace(Affine wtn);

    /**
     * Update the node world to node transform.
     * Implementation may update any transform in the world to node chain
     * as long as the world to node transform is correct at the end.
     * @param wtn
     */
    void setRootToNodeSpace(Similarity wtn);

    /**
     * Index which may be used to store additional user informations.
     *
     * @return user property dictionnary, never null
     */
    Dictionary getUserProperties();

}
