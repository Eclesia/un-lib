
package science.unlicense.display.api.font;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.system.ModuleSeeker;

/**
 * Provides informations on the available fonts, glyphs and metrics.
 *
 * @author Johann Sorel
 */
public final class FontContext {

    public static final FontContext INSTANCE = new FontContext();

    private FontContext() {}

    /**
     * Lists available font resolvers.
     * @return array of FontResolver, never null but can be empty.
     */
    public static FontStore[] getResolvers() {
        return (FontStore[]) ModuleSeeker.findServices(FontStore.class);
    }


    /**
     * Lists font resolver for given font family.
     *
     * @return FontResolver, can be null
     */
    public static FontStore getResolver(Chars family) {
        final FontStore[] resolvers = getResolvers();
        for (int i=0;i<resolvers.length;i++) {
            FontMetadata fm = resolvers[i].getMetadata(family);
            if (fm!=null) return resolvers[i];
        }
        return null;
    }

    /**
     * List all available font families.
     *
     * @return Sequence of CharSequences
     */
    public Sequence getFamilies() {
        final Sequence families = new ArraySequence();
        final FontStore[] resolvers = getResolvers();
        for (int i=0;i<resolvers.length;i++) {
            families.addAll(resolvers[i].getFamilies());
        }
        return families;
    }

    public FontMetadata getMetadata(Chars family) {
        final FontStore[] resolvers = getResolvers();
        for (int i=0;i<resolvers.length;i++) {
            FontMetadata fm = resolvers[i].getMetadata(family);
            if (fm!=null) return fm;
        }
        return null;
    }

}
