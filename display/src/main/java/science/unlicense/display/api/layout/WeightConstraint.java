
package science.unlicense.display.api.layout;

import science.unlicense.common.api.Orderable;

/**
 * Weight constraint to use with a WeightLayout
 *
 * @author Johann Sorel
 */
public final class WeightConstraint implements LayoutConstraint, Orderable {

    private final double weight;

    public WeightConstraint(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public int order(Object other) {
        //todo replace this
        return Double.compare(weight, ((WeightConstraint) other).weight);
    }

}
