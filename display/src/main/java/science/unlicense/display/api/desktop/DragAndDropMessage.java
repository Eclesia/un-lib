
package science.unlicense.display.api.desktop;

import science.unlicense.common.api.event.DefaultEventMessage;
import science.unlicense.system.api.desktop.TransferBag;

/**
 *
 * @author Johann Sorel
 */
public class DragAndDropMessage extends DefaultEventMessage {

    public static final int TYPE_ENTER = 1;
    public static final int TYPE_EXIT = 2;
    public static final int TYPE_DROP = 3;

    private final int type;

    public DragAndDropMessage(int type) {
        super(true);
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public TransferBag getBag(){
        return science.unlicense.system.System.get().getDragAndDrapBag();
    }

}
