
package science.unlicense.display.api.desktop;

import science.unlicense.common.api.Arrays;
import science.unlicense.system.ModuleSeeker;

/**
 * Convenient methods to manipulate frame managers.
 *
 * @author Johann Sorel
 */
public final class FrameManagers {

    private FrameManagers() {}

    /**
     * Indicate if current system has a framing system.
     * @return true if at least one frame manager is available.
     */
    public static boolean isFrameless() {
        return getManagers().length == 0;
    }

    /**
     * Get the first frame manager.
     * @return FrameManager
     */
    public static FrameManager getFrameManager() {
        return getManagers()[0];
    }

    /**
     * Lists available frame managers.
     * @return array of ImageFormat, never null but can be empty.
     */
    public static FrameManager[] getManagers() {
        FrameManager[] managers = (FrameManager[]) ModuleSeeker.findServices(FrameManager.class);
        int i=0;
        while (i<managers.length) {
            if (managers[i].isAvailable()) {
                i++;
            } else {
                managers = (FrameManager[]) Arrays.remove(managers, i);
            }
        }
        return managers;
    }

}
