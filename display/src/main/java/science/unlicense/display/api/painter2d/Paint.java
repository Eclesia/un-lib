
package science.unlicense.display.api.painter2d;

import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.geometry.api.BBox;
import science.unlicense.image.api.Image;
import science.unlicense.math.impl.Affine2;

/**
 * A Paint defines the inner filling color.
 *
 * @author Johann Sorel
 */
public interface Paint {

    void fill(Image image, Image flagImage, BBox flagBbox, Affine2 trs, AlphaBlending blending);

}
