
package science.unlicense.display.api.painter3d;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractPainter3D implements Painter3D {

    protected Extent.Long extent = new Extent.Long(8, 8);
    protected MonoCamera camera;
    protected SceneNode scene;
    protected final Sequence afterEffects = new ArraySequence();

    @Override
    public void setCanvasSize(Extent.Long extent) {
        this.extent = extent;
    }

    @Override
    public Extent.Long getCanvasSize() {
        return extent;
    }

    @Override
    public MonoCamera getCamera() {
        return camera;
    }

    @Override
    public void setCamera(MonoCamera camera) {
        this.camera = camera;
    }

    @Override
    public void setScene(SceneNode scene) {
        this.scene = scene;
    }

    @Override
    public SceneNode getScene() {
        return scene;
    }

    @Override
    public Sequence getAfterEffects() {
        return afterEffects;
    }
}
