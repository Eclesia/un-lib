

package science.unlicense.display.api.layout;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.SimilarityRW;

/**
 * A positionable is similar to a scene node but holds descriptive informations
 *
 * @author Johann Sorel
 */
public interface Positionable extends EventSource {

    /**
     * Property storing override positionable extents.
     */
    public static final Chars PROPERTY_OVERRIDE_EXTENTS = Chars.constant("OverrideExtents");
    /**
     * Property storing current extents.
     * This extents is derived from effective extent and override extents.
     */
    public static final Chars PROPERTY_EXTENTS = Chars.constant("Extents");
    /**
     * Property indicating when the effective size change.
     * The extent may be set to zero if the positionable is not in the visible
     * area of the layout.
     */
    public static final Chars PROPERTY_EFFECTIVE_EXTENT = Chars.constant("EffectiveExtent");
    /**
     * Property indicating when the layout constraint changed.
     */
    public static final Chars PROPERTY_LAYOUT_CONSTRAINT = Chars.constant("LayoutConstraint");
    /**
     * Property indicating if the positionable is visible.
     */
    public static final Chars PROPERTY_VISIBLE = Chars.constant("Visible");
    /**
     * Property indicating when the reserve space change.
     */
    public static final Chars PROPERTY_RESERVE_SPACE = Chars.constant("ReserveSpace");

    /**
     * Get override extents.
     * Values which are not NaN are consider override.
     *
     * @return copy of the override extents, nerver null
     */
    Extents getOverrideExtents();

    /**
     * Set override extents including min, best and max extents.
     *
     * @param ext, values will be copied
     */
    void setOverrideExtents(Extents ext);

    /**
     * Get the current positionable size.
     *
     * @return size of the positionable
     */
    Extent getEffectiveExtent();

    /**
     * Set current positionable size.
     *
     * @param extent effective size of the positionable
     */
    void setEffectiveExtent(Extent extent);

    /**
     * Get Extents.
     * Minimum,best and maximum extent the layout needs for the current view.
     * Under minimum size or over maximum the layout won't have a proper rendering.
     * This extent is constraint by the effective extent.
     *
     * @param buffer can be null
     * @return Extent, never null
     */
    Extents getExtents(Extents buffer);

    /**
     * Calculate min, best and max extents based on given constraint.
     * - Under minimum size the positionable won't have enough space
     * - Best extent provide the most appropriate spacing.
     * - Over maximum size the positionable will be over-stretched or have wasted space.
     *
     * The constraint can be null or have a fixed value on one or both axis.
     *
     * @param buffer can be null
     * @param constraint can be null
     * @return
     */
    Extents getExtents(Extents buffer, Extent constraint);

    /**
     * Get the current positionable bbox.
     * This bbox is centered version of the effective extent.
     *
     * @param buffer can be null
     * @return bbox of the positionable
     */
    BBox getBoundingBox(BBox buffer);

    /**
     * @return this positionable transform
     */
    SimilarityRW getNodeTransform();

    /**
     * Indicate if the positionable space in the layout should be reserved when the
     * positionable is not visible.
     * Default value is TRUE.
     *
     * @return true if positionable space should be reserved
     */
    boolean isReserveSpace();

    /**
     *
     * @param reserve true to reserve space when positionable is not visible
     */
    void setReserveSpace(boolean reserve);

    /**
     * Set positionable layout constraint.
     *
     * @param layoutConstraint may be null
     */
    void setLayoutConstraint(LayoutConstraint layoutConstraint);

    /**
     * Get positionable layout constraint.
     *
     * @return LayoutConstraint, may be null
     */
    LayoutConstraint getLayoutConstraint();

    /**
     * Set element visibility, visible or unvisible.
     * @param visible
     */
    void setVisible(boolean visible);

    /**
     * @return true if element is visible
     */
    boolean isVisible();

}
