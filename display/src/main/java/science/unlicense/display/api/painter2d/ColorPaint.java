
package science.unlicense.display.api.painter2d;

import science.unlicense.common.api.CObjects;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.impl.tuple.BoxCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.image.api.color.ColorRW;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.math.impl.Affine2;

/**
 * Plain color paint.
 *
 * @author Johann Sorel
 */
public class ColorPaint implements Paint{

    private final Color color;
    private final boolean isOpaque;

    public ColorPaint(Color color) {
        CObjects.ensureNotNull(color);
        this.color = color;
        isOpaque = color.getAlpha() == 1f;
    }

    public Color getColor() {
        return color;
    }

    public void fill(Image image, Image flagImage, BBox bbox, Affine2 trs, AlphaBlending blending) {
        //base image
        final ImageModel cm = image.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor colorCursor = cm.asTupleBuffer(image).cursor();
        final ColorRW pixel = Colors.castOrWrap(colorCursor.samples(), cs);

        final TupleGridCursor maskCursor = new BoxCursor(flagImage.getRawModel().asTupleBuffer(flagImage).cursor(),bbox);

        while (maskCursor.next()) {
            colorCursor.moveTo(maskCursor.coordinate());

            if (maskCursor.samples().get(0) == -1 && isOpaque) {
                //opaque replacement
                pixel.set(color);
            } else if (maskCursor.samples().get(0)!=0) {
                //alpha blending
                final float alpha = ((int) maskCursor.samples().get(0) & 0xff) / 255f;
                final Color col = new ColorRGB(color.getRed(), color.getGreen(),
                        color.getBlue(), color.getAlpha()*alpha);
                pixel.set(blending.blend(col, pixel));
            }
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ColorPaint other = (ColorPaint) obj;
        if (this.color != other.color && (this.color == null || !this.color.equals(other.color))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.color != null ? this.color.hashCode() : 0);
        return hash;
    }

}
