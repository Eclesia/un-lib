
package science.unlicense.display.api.layout;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Maths;

/**
 * Layout ordering elements on successive lines.
 * Lines can be horizontal or vertical.
 *
 * @author Johann Sorel
 */
public final class LineLayout extends AbstractLayout {

    private static final Set DIRTY_PROPERTIES = Collections.staticSet(new Chars[]{Positionable.PROPERTY_EXTENTS, Positionable.PROPERTY_RESERVE_SPACE, Positionable.PROPERTY_VISIBLE});

    public static final int DIRECTION_HORIZONTAL = 0;
    public static final int DIRECTION_VERTICAL = 1;

    public static final int VALIGN_TOP = 0;
    public static final int VALIGN_CENTER = 1;
    public static final int VALIGN_BOTTOM = 2;

    public static final int HALIGN_LEFT = 0;
    public static final int HALIGN_CENTER = 1;
    public static final int HALIGN_RIGHT = 2;

    private int direction = DIRECTION_HORIZONTAL;
    private int halignement = HALIGN_CENTER;
    private int valignement = VALIGN_CENTER;
    private int distance = 4;

    private boolean fitToCell = false;

    public LineLayout() {
        super(null);
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        if (this.direction==direction) return;
        this.direction = direction;
        setDirty();
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        if (this.distance==distance) return;
        this.distance = distance;
        setDirty();
    }

    public int getVerticalAlignement() {
        return valignement;
    }

    public void setVerticalAlignement(int valignement) {
        if (this.valignement==valignement) return;
        this.valignement = valignement;
        setDirty();
    }

    public int getHorizontalAlignement() {
        return halignement;
    }

    public void setHorizontalAlignement(int halignement) {
        if (this.halignement==halignement) return;
        this.halignement = halignement;
        setDirty();
    }

    public void setFitToCell(boolean fitToCell) {
        this.fitToCell = fitToCell;
    }

    public boolean isFitToCell() {
        return fitToCell;
    }

    @Override
    protected Set getDirtyingPropertyNames() {
        return DIRTY_PROPERTIES;
    }

    @Override
    protected void calculateExtents(Extents extents, Extent constraint) {
        if (constraint == null) {
            constraint = new Extent.Double(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        }

        final Positionable[] children = getPositionableArray();
        final Extents buffer = new Extents();

        final BBox viewbbox = new BBox(constraint);
        double viewWidth = viewbbox.getSpan(0);
        double viewHeight = viewbbox.getSpan(1);

        double spanX = 0.0;
        double spanY = 0.0;

        if (direction==DIRECTION_HORIZONTAL) {
            spanX = 0.0;
            double x=0.0;
            double y=0.0;
            double lineHeight=0.0;
            int lineIndex = 0;
            int nbPerLine = 0;

            for (int i=0;i<children.length;i++) {
                children[i].getExtents(buffer,null);
                LayoutConstraint cst = children[i].getLayoutConstraint();
                if (x==0.0) {
                    //first element on the line
                    nbPerLine++;
                    x = buffer.bestX;
                    lineHeight = buffer.bestY;
                    if (lineIndex>0) y+=distance;
                } else {
                    if (x+buffer.bestX+distance > viewWidth || cst==LineLayoutConstraint.TO_NEXT_LINE) {
                        //move to next line
                        spanX = Maths.max(spanX,x);
                        nbPerLine = 0;
                        y += lineHeight;
                        x = 0;
                        lineHeight = 0.0;
                        lineIndex++;
                        i--;
                    } else {
                        nbPerLine++;
                        x += distance + buffer.bestX;
                        lineHeight = Maths.max(buffer.bestY,lineHeight);
                    }
                }
            }
            spanY = y;
            if (nbPerLine>0) {
                //last line
                spanX = Maths.max(spanX,x);
                spanY += lineHeight;
            }

        } else {
            spanY = 0.0;
            double x=0.0;
            double y=0.0;
            double lineWidth=0.0;
            int colIndex = 0;
            int nbPerLine = 0;

            for (int i=0;i<children.length;i++) {
                children[i].getExtents(buffer,null);
                LayoutConstraint cst = children[i].getLayoutConstraint();
                if (y==0.0) {
                    nbPerLine++;
                    y = buffer.bestY;
                    lineWidth = buffer.bestX;
                    if (colIndex>0) x+=distance;
                } else {
                    if (y+buffer.bestY+distance > viewHeight || cst==LineLayoutConstraint.TO_NEXT_LINE) {
                        //move to next col
                        spanY = Maths.max(spanY,y);
                        nbPerLine=0;
                        x += lineWidth;
                        y = 0;
                        lineWidth = 0.0;
                        colIndex++;
                        i--;
                    } else {
                        nbPerLine++;
                        y += distance + buffer.bestY;
                        lineWidth = Maths.max(buffer.bestX,lineWidth);
                    }
                }
            }
            spanX = x;
            if (nbPerLine>0) {
                //last col
                spanX += lineWidth;
                spanY = Maths.max(spanY,y);
            }
        }

        extents.minX = spanX;
        extents.minY = spanY;
        extents.bestX = extents.minX;
        extents.bestY = extents.minY;
        extents.maxX = Double.POSITIVE_INFINITY;
        extents.maxY = Double.POSITIVE_INFINITY;
    }

    @Override
    public void update() {

        final Positionable[] children = getPositionableArray();
        final Extents buffer = new Extents();
        final BBox viewbbox = getView();
        double viewWidth = viewbbox.getSpan(0);
        double viewHeight = viewbbox.getSpan(1);

        if (direction==DIRECTION_HORIZONTAL) {
            double x=0.0;
            double y=0.0;
            double lineHeight=0.0;
            final Sequence line = new ArraySequence();
            int lineIndex = 0;

            for (int i=0;i<children.length;i++) {
                children[i].getExtents(buffer,null);
                children[i].setEffectiveExtent(buffer.getBest(null));
                LayoutConstraint cst = children[i].getLayoutConstraint();

                if (x==0.0) {
                    line.add(children[i]);
                    x = buffer.bestX;
                    lineHeight = buffer.bestY;
                    if (lineIndex>0) y+=distance;
                } else {
                    if (x+buffer.bestX+distance > viewWidth || cst==LineLayoutConstraint.TO_NEXT_LINE) {
                        //render line
                        setupLine(line, y, lineHeight);
                        //move to next line
                        line.removeAll();
                        y += lineHeight;
                        x = 0;
                        lineHeight = 0.0;
                        lineIndex++;
                        i--;
                    } else {
                        line.add(children[i]);
                        x += distance + buffer.bestX;
                        lineHeight = Maths.max(buffer.bestY,lineHeight);
                    }
                }
            }
            setupLine(line, y, lineHeight);

        } else {
            double x=0.0;
            double y=0.0;
            double lineWidth=0.0;
            int colIndex = 0;
            final Sequence line = new ArraySequence();

            for (int i=0;i<children.length;i++) {
                children[i].getExtents(buffer,null);
                LayoutConstraint cst = children[i].getLayoutConstraint();

                if (y==0.0) {
                    line.add(children[i]);
                    y += buffer.bestY;
                    lineWidth = buffer.bestX;
                    if (colIndex>0) x+=distance;
                } else {
                    if (y+buffer.bestY+distance > viewHeight || cst==LineLayoutConstraint.TO_NEXT_LINE) {
                        //render col
                        setupCol(line, x, lineWidth);
                        //move to next col
                        line.removeAll();
                        x += lineWidth;
                        y = 0;
                        lineWidth = 0.0;
                        colIndex++;
                        i--;
                    } else {
                        line.add(children[i]);
                        y += distance + buffer.bestY;
                        lineWidth = Maths.max(buffer.bestX,lineWidth);
                    }
                }
            }
            setupCol(line, x, lineWidth);
        }

    }

    private void setupLine(Sequence positionables, double y, double lineHeight) {
        if (positionables.isEmpty()) return;

        final BBox viewbbox = getView();
        final Extents buffer = new Extents();

        double x=0.0;
        for (int i=0,n=positionables.getSize();i<n;i++) {
            final Positionable p = (Positionable) positionables.get(i);
            p.getExtents(buffer,null);

            if (fitToCell) buffer.bestY = lineHeight;
            p.setEffectiveExtent(buffer.getBest(null));

            double d = 0.0;
            switch (valignement) {
                case VALIGN_CENTER: d = (lineHeight-buffer.bestY)/2.0; break;
                case VALIGN_BOTTOM: d = (lineHeight-buffer.bestY); break;
            }
            p.getNodeTransform().setToTranslation(new double[]{
                    viewbbox.getMin(0)+x+buffer.bestX/2.0,
                    viewbbox.getMin(1)+y+buffer.bestY/2.0+d});
            x+=buffer.bestX+distance;
        }
    }

    private void setupCol(Sequence positionables, double x, double lineWidth) {
        if (positionables.isEmpty()) return;

        final BBox viewbbox = getView();
        final Extents buffer = new Extents();

        double y=0.0;
        for (int i=0,n=positionables.getSize();i<n;i++) {
            final Positionable p = (Positionable) positionables.get(i);
            p.getExtents(buffer,null);

            if (fitToCell) buffer.bestX = lineWidth;
            p.setEffectiveExtent(buffer.getBest(null));

            double d = 0.0;
            switch (halignement) {
                case HALIGN_CENTER: d = (lineWidth-buffer.bestX)/2.0; break;
                case HALIGN_RIGHT: d = (lineWidth-buffer.bestX); break;
            }
            p.getNodeTransform().setToTranslation(new double[]{
                    viewbbox.getMin(0)+x+buffer.bestX/2.0+d,
                    viewbbox.getMin(1)+y+buffer.bestY/2.0});
            y+=buffer.bestY+distance;
        }
    }

}
