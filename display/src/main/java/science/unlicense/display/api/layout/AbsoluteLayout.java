
package science.unlicense.display.api.layout;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Set;
import science.unlicense.geometry.api.Extent;

/**
 * Absolute layout places elements according to specified bounds.
 *
 * @author Johann Sorel
 */
public final class AbsoluteLayout extends AbstractLayout {

    private static final Set DIRTY_PROPERTIES = Collections.staticSet(new Chars[]{Positionable.PROPERTY_VISIBLE, Positionable.PROPERTY_RESERVE_SPACE});

    public AbsoluteLayout() {
        super(null);
    }

    @Override
    protected Set getDirtyingPropertyNames() {
        return DIRTY_PROPERTIES;
    }

    @Override
    protected void calculateExtents(Extents extents, Extent constraint) {
        //do nothing, positionable have they own transforms
        extents.minX = 0.0;
        extents.minX = 0.0;
        extents.bestX = 100;
        extents.bestY = 100;
        extents.maxX = Double.POSITIVE_INFINITY;
        extents.maxY = Double.POSITIVE_INFINITY;
    }

    @Override
    public void update() {
        //do nothing, positionable have they own transforms
        //just set the extent
        final Positionable[] children = getPositionableArray();
        final Extents buffer = new Extents();
        for (int i=0;i<children.length;i++) {
            final Positionable element = (Positionable) children[i];
            element.getExtents(buffer, null);
            element.setEffectiveExtent(buffer.getBest(null));
        }
    }

}
