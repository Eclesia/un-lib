
package science.unlicense.display.api.font;

import science.unlicense.common.api.character.Char;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.IntSet;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class DerivateFont implements Font {

    private final Font parent;
    private final Affine2 trs;
    private final float size;

    public DerivateFont(Font parent, Affine2 trs, float size) {
        this.parent = parent;
        this.trs = trs;
        this.size = size;
    }

    @Override
    public Chars getFamily() {
        return parent.getFamily();
    }

    @Override
    public int getWeight() {
        return parent.getWeight();
    }

    @Override
    public float getSize() {
        return size;
    }

    @Override
    public FontStore getStore() {
        return parent.getStore();
    }

    @Override
    public FontMetadata getMetaData() {
        return parent.getMetaData().derivate(size);
    }

    @Override
    public IntSet listCharacters() {
        return parent.listCharacters();
    }

    @Override
    public PlanarGeometry getGlyph(int unicode) {
        final PlanarGeometry glyph = parent.getGlyph(unicode);
        if (glyph==null) return null;
        return TransformedGeometry.create(glyph,trs);
    }

    @Override
    public PlanarGeometry getGlyph(Char character) {
        return getGlyph(character.toUnicode());
    }

    @Override
    public Font derivate(float fontSize) {
        return parent.derivate(fontSize);
    }

}
