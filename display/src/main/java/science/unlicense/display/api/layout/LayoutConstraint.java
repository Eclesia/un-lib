
package science.unlicense.display.api.layout;

/**
 * Defines a positionnable constraint in the layout.
 *
 * @author Johann Sorel
 */
public interface LayoutConstraint {

}
