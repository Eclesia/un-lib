
package science.unlicense.display.api.layout;

/**
 * Border layout constraint.
 *
 * @author Johann Sorel
 */
public final class BorderConstraint implements LayoutConstraint {

    public static final BorderConstraint TOP    = new BorderConstraint();
    public static final BorderConstraint BOTTOM = new BorderConstraint();
    public static final BorderConstraint LEFT   = new BorderConstraint();
    public static final BorderConstraint RIGHT  = new BorderConstraint();
    public static final BorderConstraint CENTER = new BorderConstraint();

    private BorderConstraint() {
        super();
    }

}
