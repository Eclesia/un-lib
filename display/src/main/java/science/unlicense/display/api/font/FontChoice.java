
package science.unlicense.display.api.font;

import science.unlicense.common.api.character.Chars;

/**
 * A Font holds a description of a text glyphs including
 * size, weight and family.
 *
 * @author Johann Sorel
 */
public class FontChoice {

    public static int WEIGHT_NONE = 0;
    public static int WEIGHT_BOLD = 1;

    //from SVG : 10.12 Text decoration
    public static int DECORATION_NON = 0;
    public static int DECORATION_UNDERLINE = 1;
    public static int DECORATION_OVERLINE = 2;
    public static int DECORATION_LINE_THROUGHT = 3;
    public static int DECORATION_BLINK = 4;

    private Chars[] families;
    private float size = 8;
    private int weight;

    /**
     *
     * @param families, font family by preference order.
     * @param size font size
     * @param weight font weight
     */
    public FontChoice(Chars[] families, float size, int weight) {
        this.families = families;
        this.size = size;
        this.weight = weight;
    }

    public Chars[] getFamilies() {
        return families;
    }

    public float getSize() {
        return size;
    }

    public int getWeight() {
        return weight;
    }

}
