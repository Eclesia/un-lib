
package science.unlicense.display.api.scene.s2d;

import science.unlicense.display.api.painter2d.Contour;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.system.CoordinateSystem;

/**
 *
 * @author Johann Sorel
 */
public class GeometryNode2D extends GraphicNode2D{

    private static final Paint[] EMPTY_PAINTS = new Paint[0];
    private static final Contour[] EMPTY_CONTOURS = new Contour[0];

    protected PlanarGeometry geometry;
    protected Contour[] contours = EMPTY_CONTOURS;
    protected Paint[] fills = EMPTY_PAINTS;

    public GeometryNode2D() {
        super();
    }

    public GeometryNode2D(CoordinateSystem cs) {
        super(cs);
    }

    public PlanarGeometry getGeometry() {
        return geometry;
    }

    public void setGeometry(PlanarGeometry geometry) {
        this.geometry = geometry;
    }

    /**
     *
     * @return contours, never null, can be empty.
     */
    public Contour[] getContours() {
        return contours;
    }

    public void setContours(Contour[] contours) {
        this.contours = (contours == null) ? EMPTY_CONTOURS : contours;
    }

    /**
     *
     * @return paints, never null, can be empty.
     */
    public Paint[] getFills() {
        return fills;
    }

    public void setFills(Paint[] fills) {
        this.fills = (fills == null) ? EMPTY_PAINTS : fills;
    }

}
