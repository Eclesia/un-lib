
package science.unlicense.display.api.font;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Set;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 * A font store list the available families it supports.
 *
 * @author Johann Sorel
 */
public interface FontStore {

    /**
     * List available family names.
     */
    Set getFamilies();

    /**
     * Get font family metadatas.
     * @param family
     * @return FontMetadata
     */
    FontMetadata getMetadata(Chars family);

    /**
     * Get font object.
     *
     * @param family
     * @return FontMetadata
     */
    Font getFont(Chars family);

    Font getFont(FontChoice font);

    /**
     * Read and store the given font.
     *
     * @param path Path to font file.
     */
    void registerFont(Path path) throws IOException;

}
