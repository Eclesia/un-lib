
package science.unlicense.display.api.scene;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.AbstractProperty;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.common.api.model.tree.DefaultNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.geometry.api.system.CoordinateSystem;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.SimilarityRW;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.SimilarityNd;

/**
 * Default scene node implementation.
 *
 * @author Johann Sorel
 */
public class DefaultSceneNode extends DefaultNode implements SceneNode{

    private Object id = null;
    private CharArray title = Chars.EMPTY;
    /**
     * This node 'Parent to Node' Matrix.
     */
    protected final SimilarityRW modelTransform;
    protected CoordinateSystem coordinateSystem;
    protected SceneNode parent = null;

    /**
     * Cache root to node transforms.
     */
    private final SimilarityRW rootToNode;
    private boolean rootToNodeDirty = true;
    private final SimilarityRW nodeToRoot;
    private boolean nodeToRootDirty = true;

    /**
     * Additional properties.
     * created only when needed.
     */
    private Dictionary userProperties = null;

    /**
     * Create a scene node with a defined coordinate system.
     * The dimension is use by the node transform.
     *
     * @param cs
     */
    public DefaultSceneNode(CoordinateSystem cs) {
        this(cs,true);
    }

    /**
     * Create a scene node with a defined coordinate system.
     * The dimension is use by the node transform.
     *
     * @param cs
     * @param allowChildren
     */
    public DefaultSceneNode(CoordinateSystem cs, boolean allowChildren) {
        super(allowChildren);
        this.coordinateSystem = cs;
        final int dimension = cs.getDimension();
        rootToNode = SimilarityNd.create(dimension);
        nodeToRoot = SimilarityNd.create(dimension);
        modelTransform = new NotifiedSimilarity(dimension);
    }


    /**
     * Get node id
     *
     * @return should be a simple object such as Integer or Chars, may be null
     */
    @Override
    public Object getId() {
        return id;
    }

    /**
     * Set node id.
     * @param id should be a simple object such as Integer or Chars, can be null
     */
    @Override
    public void setId(Object id) {
        final boolean changed = !CObjects.equals(this.id, id);
        if (changed) {
            final Object oldVal = this.id;
            this.id = id;
            sendPropertyEvent(PROPERTY_ID, oldVal, id);
        }
    }

    public Property varId() {
        return new AbstractProperty(this, PROPERTY_ID, Object.class, true, true) {
            @Override
            public Object getValue() throws RuntimeException {
                return getId();
            }

            @Override
            public void setValue(Object value) throws RuntimeException {
                setId(value);
            }
        };
    }

    /**
     * Get node title.
     * The title is a short text meant to be displayed to the users.
     *
     * @return Chars, can be null.
     */
    @Override
    public CharArray getTitle() {
        return title;
    }

    /**
     * Set node title.
     *
     * @param title , can be null.
     */
    @Override
    public void setTitle(CharArray title) {
        final boolean changed = !CObjects.equals(this.title, title);
        if (changed) {
            final Object oldVal = this.title;
            this.title = title;
            sendPropertyEvent(PROPERTY_TITLE, oldVal, title);
        }
    }

    public Property varTitle() {
        return new AbstractProperty(this, PROPERTY_TITLE, CharArray.class, true, true) {
            @Override
            public Object getValue() throws RuntimeException {
                return getTitle();
            }

            @Override
            public void setValue(Object value) throws RuntimeException {
                setTitle((CharArray) value);
            }
        };
    }

    /**
     * {@inheritDoc }
     * @return
     */
    @Override
    public int getDimension() {
        return modelTransform.getDimension();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public CoordinateSystem getCoordinateSystem() {
        if (!CoordinateSystems.isUndefined(coordinateSystem)) return coordinateSystem;
        if (parent != null) parent.getCoordinateSystem();
        return coordinateSystem;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public CoordinateSystem getLocalCoordinateSystem() {
        return coordinateSystem;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public synchronized void setLocalCoordinateSystem(CoordinateSystem coordinateSystem) {
        CObjects.ensureNotNull(coordinateSystem);
        if (this.coordinateSystem==coordinateSystem) return;
        if (coordinateSystem!=null && coordinateSystem.getDimension()!=getDimension()) {
            throw new InvalidArgumentException("Coordinate system dimension ("+coordinateSystem.getDimension()
                                                +") differ from node dimension ("+getDimension()+").");
        }
        this.coordinateSystem = coordinateSystem;
        rootToNodeDirty = true;
    }

    /**
     * {@inheritDoc }
     * @return
     */
    @Override
    public SimilarityRW getNodeTransform() {
        return modelTransform;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public synchronized Similarity getRootToNodeSpace() {
        if (rootToNodeDirty) {
            rootToNodeDirty = false;
            rootToNode.set(getNodeTransform().invert());
            SceneNode parent = getParent();
            if (parent != null) {

                //adjust transform if coordinate system are different
                if (!CoordinateSystems.isUndefined(coordinateSystem)) {
                    CoordinateSystem parentCs = parent.getCoordinateSystem();
                    if (!CoordinateSystems.isUndefined(parentCs) && !coordinateSystem.equals(parentCs)) {
                        final Affine trs = (Affine) CoordinateSystems.createTransform(parentCs, coordinateSystem);
                        rootToNode.localMultiply(trs);
                    }
                }

                final Affine pm = parent.getRootToNodeSpace();
                rootToNode.localMultiply(pm);
            }
        }
        return rootToNode;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public synchronized Similarity getNodeToRootSpace() {
        if (nodeToRootDirty) {
            nodeToRootDirty = false;
            getRootToNodeSpace().invert(nodeToRoot);
        }
        return nodeToRoot;
    }

    protected synchronized void setCacheTransformsDirty() {
        if (rootToNodeDirty && nodeToRootDirty) return;
        rootToNodeDirty=true;
        nodeToRootDirty=true;
        for (Object n : getChildren().toArray()) {
            if (n instanceof DefaultSceneNode) {
                ((DefaultSceneNode) n).setCacheTransformsDirty();
            }
        }
    }

    /**
     * {@inheritDoc }
     * @param wtn
     */
    @Override
    public void setRootToNodeSpace(Similarity wtn) {
        setRootToNodeSpace((Affine) wtn);
    }

    /**
     * {@inheritDoc }
     * @param wtn
     */
    @Override
    public void setRootToNodeSpace(Affine wtn) {
        if (parent == null) {
            //update the local transform
            getNodeTransform().set(wtn.invert());
        } else {
            //calculate difference from parent
            Affine m = parent.getNodeToRootSpace();
            m = wtn.multiply(m);
            getNodeTransform().set(m.invert());
        }
        setCacheTransformsDirty();
    }

    /**
     * {@inheritDoc }
     * @return
     */
    @Override
    public SceneNode getParent() {
        return (SceneNode) parent;
    }

    /**
     *  {@inheritDoc }
     * @param parent
     */
    protected void setParent(SceneNode parent) {
        final boolean changed = !CObjects.equals(this.parent, parent);
        if (changed) {
            if (parent==this) {
                throw new InvalidArgumentException("Parent can not be this same node");
            }
            final Object oldVal = this.parent;
            this.parent = parent;

            if (parent!=null && parent.getDimension()!=getDimension() ) {
                throw new InvalidArgumentException("Parent dimension differ.");
            }

            setCacheTransformsDirty();
            sendPropertyEvent(PROPERTY_PARENT, oldVal, parent);
        }
    }

    /**
     * {@inheritDoc }
     * @return root node
     */
    @Override
    public SceneNode getRoot() {
        if (parent != null) {
            return parent.getRoot();
        }
        //this is the root
        return this;
    }

    @Override
    public synchronized Dictionary getUserProperties() {
        if (userProperties==null) userProperties = new HashDictionary();
        return userProperties;
    }

    /**
     * {@inheritDoc }
     * Set parent reference on new children nodes.
     *
     * @param array
     * @param index
     */
    @Override
    protected void addChildren(int index, Object[] array) {
        for (Object n : array) {
            //search in the parents if we did not create a loop
            SceneNode p = getParent();
            while (p!=null) {
                if (p==n) {
                    throw new RuntimeException("Loop in scene node hierarchy");
                }
                p = p.getParent();
            }
            //check the node is not already in the list
            if (getChildren().contains(n)) {
                throw new InvalidArgumentException("Object is already a children of this node : "+n);
            }

            childAdded((Node) n);
        }
        super.addChildren(index, array);
    }

    @Override
    protected void replaceChildren(Object[] children) {
        final Object[] oldChildren = getChildren().toArray();
        for (Object child : oldChildren) {
            if (!Arrays.containsIdentity(children, 0, children.length, child)) {
                childRemoved((Node) child);
            }
        }
        for (Object n : children) {
            //search in the parents if we did not create a loop
            SceneNode p = getParent();
            while (p!=null) {
                if (p==n) {
                    throw new RuntimeException("Loop in scene node hierarchy");
                }
                p = p.getParent();
            }
            if (!Arrays.containsIdentity(oldChildren, 0, oldChildren.length, n)) {
                //new children
                childAdded((Node) n);
            }
        }
        super.replaceChildren(children);
    }

    /**
     * {@inheritDoc }
     * Removes parent reference on children node.
     *
     * @param index
     */
    @Override
    protected boolean removeChildNode(int index) {
        childRemoved((Node) children.get(index));
        return super.removeChildNode(index);
    }

    @Override
    protected boolean removeChildren() {
        for (Object child : getChildren().toArray()) {
            childRemoved((Node) child);
        }
        return super.removeChildren();
    }

    /**
     * Removes parent reference.
     *
     * @param child
     */
    protected void childRemoved(Node child) {
        if (child instanceof DefaultSceneNode) {
            ((DefaultSceneNode) child).setParent(null);
        }
    }

    /**
     * Sets parent reference.
     *
     * @param child
     */
    protected void childAdded(Node child) {
        if (child instanceof SceneNode) {
            final SceneNode previous = ((SceneNode) child).getParent();
            if (previous!=null) previous.getChildren().remove(child);
            ((DefaultSceneNode) child).setParent(this);
        }
    }

    /**
     * Catch transform change to update world transform.
     */
    private final class NotifiedSimilarity extends SimilarityNd {

        public NotifiedSimilarity(int dimension) {
            super(dimension);
        }

        public NotifiedSimilarity(MatrixRW rotation, VectorRW scale, VectorRW translation) {
            super(rotation, scale, translation);
        }

        @Override
        public void notifyChanged() {
            setCacheTransformsDirty();
            super.notifyChanged();
        }

    }

}
