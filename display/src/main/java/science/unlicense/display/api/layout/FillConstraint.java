
package science.unlicense.display.api.layout;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public final class FillConstraint extends CObject implements LayoutConstraint {

    public static final float HALIGN_LEFT = 0.0f;
    public static final float HALIGN_CENTER = 0.5f;
    public static final float HALIGN_RIGHT = 1.0f;

    public static final float VALIGN_TOP = 1.0f;
    public static final float VALIGN_CENTER = 0.5f;
    public static final float VALIGN_BOTTOM = 0.0f;


    private final int x;
    private final int y;
    private final int spanx;
    private final int spany;
    private final boolean fillx;
    private final boolean filly;
    private final float alignx;
    private final float aligny;

    public FillConstraint() {
        this(0,0,1,1,false,false,HALIGN_LEFT,VALIGN_CENTER);
    }

    public FillConstraint(int x, int y, int spanx, int spany, boolean fillx, boolean filly, float alignx, float aligny) {
        this.y = y;
        this.x = x;
        this.spanx = spanx;
        this.spany = spany;
        this.fillx = fillx;
        this.filly = filly;
        this.alignx = alignx;
        this.aligny = aligny;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getSpanX() {
        return spanx;
    }

    public int getSpanY() {
        return spany;
    }

    public boolean isFillX() {
        return fillx;
    }

    public boolean isFillY() {
        return filly;
    }

    public float getAlignX() {
        return alignx;
    }

    public float getAlignY() {
        return aligny;
    }

    @Override
    public int getHash() {
        int hash = 7;
        hash = 71 * hash + this.x;
        hash = 71 * hash + this.y;
        hash = 71 * hash + this.spanx;
        hash = 71 * hash + this.spany;
        hash = 71 * hash + (this.fillx ? 1 : 0);
        hash = 71 * hash + (this.filly ? 1 : 0);
        hash = 71 * hash + Float.floatToIntBits(this.alignx);
        hash = 71 * hash + Float.floatToIntBits(this.aligny);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FillConstraint other = (FillConstraint) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        if (this.spanx != other.spanx) {
            return false;
        }
        if (this.spany != other.spany) {
            return false;
        }
        if (this.fillx != other.fillx) {
            return false;
        }
        if (this.filly != other.filly) {
            return false;
        }
        if (Float.floatToIntBits(this.alignx) != Float.floatToIntBits(other.alignx)) {
            return false;
        }
        if (Float.floatToIntBits(this.aligny) != Float.floatToIntBits(other.aligny)) {
            return false;
        }
        return true;
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("FillConstraint");
        cb.append("X/Y ("+x+","+y+") ");
        cb.append("Span ("+spanx+","+spany+") ");
        cb.append("Fill ("+fillx+","+filly+") ");
        cb.append("Align ("+alignx+","+aligny+") ");
        return cb.toChars();
    }

    /**
     * Apply fill and alignment constraints on positionable.
     *
     * @param pos
     * @param bbox allocate rectangle for the positionable
     */
    public void applyOn(Positionable pos, BBox bbox) {

        //calculate extent
        double spanx = bbox.getSpan(0);
        double spany = bbox.getSpan(1);
        double sx = spanx;
        double sy = spany;
        if (!fillx || !filly) {
            final Extents extents = pos.getExtents(null);
            if (!fillx && extents.bestX < sx) sx = extents.bestX;
            if (!filly && extents.bestY < sy) sy = extents.bestY;
        }
        pos.setEffectiveExtent(new Extent.Double(sx,sy));

        //adjust transform
        final double[] trs = {
            bbox.getMin(0) + (spanx-sx)*alignx + sx/2.0,
            bbox.getMin(1) + (spany-sy)*aligny + sy/2.0,
        };

        pos.getNodeTransform().setToTranslation(trs);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private int x = 0;
        private int y = 0;
        private int spanx = 1;
        private int spany = 1;
        private boolean fillx = false;
        private boolean filly = false;
        private float alignx = HALIGN_LEFT;
        private float aligny = VALIGN_CENTER;

        public Builder coord(int x, int y) {
            this.x = x;
            this.y = y;
            return this;
        }

        public Builder span(int spanx, int spany) {
            this.spanx = spanx;
            this.spany = spany;
            return this;
        }

        public Builder fill() {
            return this.fill(true, true);
        }

        public Builder fill(boolean fillx, boolean filly) {
            this.fillx = fillx;
            this.filly = filly;
            return this;
        }

        public Builder align(float alignx, float aligny) {
            this.alignx = alignx;
            this.aligny = aligny;
            return this;
        }

        public FillConstraint build() {
            return new FillConstraint(x, y, spanx, spany, fillx, filly, alignx, aligny);
        }

    }

}
