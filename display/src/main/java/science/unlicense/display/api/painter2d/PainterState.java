
package science.unlicense.display.api.painter2d;

import science.unlicense.display.api.font.FontChoice;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.math.impl.Affine2;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 *
 * @author Johann Sorel
 */
public interface PainterState {

    Affine2 getTransform();

    PlanarGeometry getClip();

    AlphaBlending getAlphaBlending();

    Paint getPaint();

    Brush getBrush();

    FontChoice getFont();

}
