
package science.unlicense.display.api.painter2d;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.CharIterator;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.s2d.GeometryNode2D;
import science.unlicense.display.api.scene.s2d.GraphicNode2D;
import science.unlicense.display.api.scene.s2d.ImageNode2D;
import science.unlicense.display.api.scene.s2d.TextNode2D;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Affine2;
import science.unlicense.task.api.Task;

/**
 *
 * @author Johann Sorel
 */
abstract class SimplifiedPainter2D implements Painter2D {

    private final DefaultPainterVisitor visitor = new DefaultPainterVisitor();


    public void fill(CharArray text, float x, float y) {
        text(text,x,y,true);
    }

    public void stroke(CharArray text, float x, float y) {
        text(text,x,y,false);
    }

    private void text(CharArray text, float x, float y, boolean fill) {
        final Paint paint = getPaint();
        final FontChoice font = getFont();
        final Brush brush = getBrush();
        if (text == null || paint == null || font == null) return;
        if (!fill && brush==null) return;

        final FontMetadata metadata = getFontStore().getFont(font).getMetaData();
        final CharIterator ite = text.createIterator();
        final Affine2 trs = new Affine2(1, 0, x,
                                        0, -1, y);

        final double spacing = metadata.getAdvanceWidth(' ');

        double offset = 0.0;
        while (ite.hasNext()) {
            final int c = ite.nextToUnicode();

            if (c == ' ') {
                //TODO handle control caracters
                offset += spacing;
            } else {
                character(c, trs, fill);
                offset += metadata.getAdvanceWidth(c);
            }

            trs.set(0, 2, x+offset);
        }
    }

    protected void character(int c, Affine2 trs, boolean fill) {
        final FontChoice fontChoice = getFont();
        if (fontChoice==null) return;
        PlanarGeometry geom = getFontStore().getFont(fontChoice).getGlyph(c);

        if (geom==null) return;

        geom = TransformedGeometry.create(geom, trs);

        if (fill) {
            fill(geom);
        } else {
            stroke(geom);
        }
    }

    public void execute(Task task, PlanarGeometry area) {
        if (!(this instanceof ImagePainter2D)) {
            throw new UnimplementedException("Not supported yet.");
        }

        //TODO special kind of image task
        this.flush();
        final Image inImage = ((ImagePainter2D) this).getImage();
        task.inputs().setPropertyValue(new Chars("Image"),inImage);
        final Document execute = task.perform();
        final Image outImage = (Image) execute.getPropertyValue(new Chars("Image"));
        if (outImage!=null) {
            paint(outImage, new Affine2());
        }
    }

    public void render(SceneNode node) {
        visitor.visit(node, null);
    }

    public PainterState saveState() {
        return new DefaultPainterState(
                getTransform(),
                getClip(),
                getAlphaBlending(),
                getPaint(),
                getBrush(),
                getFont());
    }

    public void restoreState(PainterState state) {
        setTransform(state.getTransform());
        setClip(state.getClip());
        setAlphaBlending(state.getAlphaBlending());
        setPaint(state.getPaint());
        setBrush(state.getBrush());
        setFont(state.getFont());
    }

    protected class DefaultPainterVisitor extends NodeVisitor{
        public Object visit(Node node, Object context) {
            if (node instanceof GraphicNode2D) {
                final Affine2 cp = getTransform();

                final GraphicNode2D gn = (GraphicNode2D) node;
                final AlphaBlending blending = gn.getBlending();
                if (blending!=null) {
                    setAlphaBlending(blending);
                }

                Affine2 deriv = (Affine2) cp.multiply(gn.getNodeToRootSpace());
                setTransform(deriv);

                if (node instanceof GeometryNode2D) {

                    final GeometryNode2D cdt = (GeometryNode2D) node;
                    //fills
                    final Paint[] fills = cdt.getFills();
                    if (fills!=null) {
                        for (int i=0;i<fills.length;i++) {
                            setPaint(fills[i]);
                            fill(cdt.getGeometry());
                        }
                    }

                    //contours
                    final Contour[] contours = cdt.getContours();
                    if (contours!=null) {
                        for (int i=0;i<contours.length;i++) {
                            setBrush(contours[i].getValue1());
                            setPaint(contours[i].getValue2());
                            stroke(cdt.getGeometry());
                        }
                    }

                } else if (node instanceof ImageNode2D) {
                    final ImageNode2D gln = (ImageNode2D) node;
                    paint(gln.getImage(), null);


                } else if (node instanceof TextNode2D) {

                    final TextNode2D cdt = (TextNode2D) node;
                    VectorRW anchor = cdt.getAnchor();

                    setFont(cdt.getFont());
                    //fills
                    final Paint[] fills = cdt.getFills();
                    for (int i=0;i<fills.length;i++) {
                        setPaint(fills[i]);
                        fill(cdt.getText(),(float) anchor.getX(),(float) anchor.getY());
                    }
                }

                setTransform(cp);
            }

            return super.visit(node, context);
        }
    }

}
