
package science.unlicense.display.api.layout;

/**
 * Split layout constraint.
 *
 * @author Johann Sorel
 */
public final class SplitConstraint implements LayoutConstraint {

    public static final SplitConstraint TOP    = new SplitConstraint();
    public static final SplitConstraint BOTTOM = new SplitConstraint();
    public static final SplitConstraint LEFT   = TOP;
    public static final SplitConstraint RIGHT  = BOTTOM;
    public static final SplitConstraint SPLITTER = new SplitConstraint();

    private SplitConstraint() {
    }

}
