
package science.unlicense.display.api.scene.s2d;

import science.unlicense.display.api.scene.SceneNode;

/**
 *
 * @author Johann Sorel
 */
public interface Graphic2DFactory {

    SceneNode createNode();

    GeometryNode2D createGeometryNode();

    ImageNode2D createImageNode();

    TextNode2D createTextNode();

}
