

package science.unlicense.display.impl.updater;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class OrbitUpdaterTest {

    private static final double EPSILON = 1e-12;

    @Test
    public void testAtOrigin(){

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode target = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode satellite = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);

        scene.getChildren().add(target);
        scene.getChildren().add(satellite);

        final OrbitUpdater controller = new OrbitUpdater(null, new Vector3f64(0, 1, 0), new Vector3f64(0, 0, 1), satellite, target);
        controller.setDistance(10);
        controller.update(null);

        Assert.assertArrayEquals(new double[]{
            1, 0, 0, 10,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1,
            },satellite.getNodeToRootSpace().toMatrix().toArrayDouble(),EPSILON);


    }

    @Test
    public void testTargetOffset(){

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode target = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode satellite = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);

        target.getNodeTransform().getTranslation().setXYZ(1, 2, 3);
        target.getNodeTransform().notifyChanged();

        scene.getChildren().add(target);
        scene.getChildren().add(satellite);

        final OrbitUpdater controller = new OrbitUpdater(null, new Vector3f64(0, 1, 0), new Vector3f64(0, 0, 1),satellite, target);
        controller.setDistance(10);
        controller.update(null);

        Assert.assertArrayEquals(new double[]{
            1, 0, 0, 11,
            0, 1, 0, 2,
            0, 0, 1, 3,
            0, 0, 0, 1,
            },satellite.getNodeToRootSpace().toMatrix().toArrayDouble(),EPSILON);
    }

    @Test
    public void testParentOffset(){

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode target = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode satellite = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);

        scene.getNodeTransform().getTranslation().setXYZ(1, 2, 3);
        scene.getNodeTransform().notifyChanged();

        scene.getChildren().add(target);
        scene.getChildren().add(satellite);

        final OrbitUpdater controller = new OrbitUpdater(null, new Vector3f64(0, 1, 0), new Vector3f64(0, 0, 1), satellite, target);
        controller.setDistance(10);
        controller.update(null);

        Assert.assertArrayEquals(new double[]{
            1, 0, 0, 11,
            0, 1, 0, 2,
            0, 0, 1, 3,
            0, 0, 0, 1,
            },satellite.getNodeToRootSpace().toMatrix().toArrayDouble(),EPSILON);
    }

}
