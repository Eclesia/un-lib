
package science.unlicense.display.impl.anim;

import science.unlicense.common.api.Assert;
import org.junit.Test;
import science.unlicense.display.api.anim.ManualTimer;
import science.unlicense.math.api.Maths;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.geometry.api.system.CoordinateSystems;

/**
 *
 * @author Johann Sorel
 */
public class TransformTimeSerieHelperTest {

    private static final float DELTA = 0.00001f;

    @Test
    public void testRotation() {

        final ManualTimer timer = new ManualTimer();
        final DefaultSceneNode node = new DefaultSceneNode(CoordinateSystems.UNDEFINED_2D);

        final TransformTimeSerieHelper helper = new TransformTimeSerieHelper(2);
        helper.timer(timer).on(node);
        helper.at(0).rotation(0);
        helper.at(1000).rotation(Maths.PI);
        helper.at(2000).rotation(Maths.PI+Maths.HALF_PI);
        final TransformAnimation anim = helper.repeat(0).build();
        anim.start();

        timer.setUpdateTime(0);
        Assert.assertArrayEquals(new float[]{1,0,0,1},
                node.getNodeTransform().getRotation().toArrayFloat(),DELTA);

        timer.setUpdateTime(1000 * 1000000);
        Assert.assertArrayEquals(new float[]{-1, 0, 0,-1},
                node.getNodeTransform().getRotation().toArrayFloat(),DELTA);

        timer.setUpdateTime(2000 * 1000000);
        Assert.assertArrayEquals(new float[]{ 0, 1,-1, 0},
                node.getNodeTransform().getRotation().toArrayFloat(),DELTA);

    }

}
