
package science.unlicense.display.impl;

import org.junit.Test;
import static science.unlicense.common.api.Assert.*;

/**
 *
 * @author Johann Sorel
 */
public class RecentHistoryTest {

    @Test
    public void testHistory() {

        final RecentHistory history = new RecentHistory(4);
        assertEquals(null, history.getPrevious());
        assertEquals(null, history.getCurrent());
        assertEquals(null, history.getNext());
        assertEquals(false, history.isPreviousExist());
        assertEquals(false, history.isCurrentExist());
        assertEquals(false, history.isNextExist());
        assertEquals(-1, history.getCurrentIndex());

        history.put(1);
        assertEquals(null, history.getPrevious());
        assertEquals(1, history.getCurrent());
        assertEquals(null, history.getNext());
        assertEquals(false, history.isPreviousExist());
        assertEquals(true, history.isCurrentExist());
        assertEquals(false, history.isNextExist());
        assertEquals(0, history.getCurrentIndex());

        history.put(2);
        assertEquals(1, history.getPrevious());
        assertEquals(2, history.getCurrent());
        assertEquals(null, history.getNext());
        assertEquals(true, history.isPreviousExist());
        assertEquals(true, history.isCurrentExist());
        assertEquals(false, history.isNextExist());
        assertEquals(1, history.getCurrentIndex());

        history.put(3);
        assertEquals(2, history.getPrevious());
        assertEquals(3, history.getCurrent());
        assertEquals(null, history.getNext());
        assertEquals(true, history.isPreviousExist());
        assertEquals(true, history.isCurrentExist());
        assertEquals(false, history.isNextExist());
        assertEquals(2, history.getCurrentIndex());

        //should have no effect
        history.forward();
        assertEquals(2, history.getPrevious());
        assertEquals(3, history.getCurrent());
        assertEquals(null, history.getNext());
        assertEquals(true, history.isPreviousExist());
        assertEquals(true, history.isCurrentExist());
        assertEquals(false, history.isNextExist());
        assertEquals(2, history.getCurrentIndex());

        history.backward();
        assertEquals(1, history.getPrevious());
        assertEquals(2, history.getCurrent());
        assertEquals(3, history.getNext());
        assertEquals(true, history.isPreviousExist());
        assertEquals(true, history.isCurrentExist());
        assertEquals(true, history.isNextExist());
        assertEquals(1, history.getCurrentIndex());

        //should move us back to top
        history.forward();
        assertEquals(2, history.getPrevious());
        assertEquals(3, history.getCurrent());
        assertEquals(null, history.getNext());
        assertEquals(true, history.isPreviousExist());
        assertEquals(true, history.isCurrentExist());
        assertEquals(false, history.isNextExist());
        assertEquals(2, history.getCurrentIndex());

        //4 should replace 3
        history.backward();
        history.put(4);
        assertEquals(2, history.getPrevious());
        assertEquals(4, history.getCurrent());
        assertEquals(null, history.getNext());
        assertEquals(true, history.isPreviousExist());
        assertEquals(true, history.isCurrentExist());
        assertEquals(false, history.isNextExist());
        assertEquals(2, history.getCurrentIndex());

    }

}
