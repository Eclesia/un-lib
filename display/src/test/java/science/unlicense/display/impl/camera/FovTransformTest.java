
package science.unlicense.display.impl.camera;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class FovTransformTest {

    @Ignore
    @Test
    public void testTransform() {

        final MonoCamera camera = new MonoCamera();
        camera.setFieldOfView(50);
        camera.setRenderArea(new Rectangle(0, 0, 200, 200));
        Assert.assertEquals(new Vector3f64(0, 0, -1.0), camera.getForward());
        final Transform trs = camera.getFovTransform();

        final Vector2f64 screenPos = new Vector2f64();
        final Vector3f64 screenRay = new Vector3f64();

        //center point
        screenPos.setXY(100, 50);
        trs.transform(screenPos, screenRay);
//        Assert.assertEquals(new Vector3d(0, 0, -1.0), screenRay);

        // left border
        screenPos.setXY(0, 50);
        trs.transform(screenPos, screenRay);
        System.out.println(screenRay);

        // right border
        screenPos.setXY(200, 50);
        trs.transform(screenPos, screenRay);
        System.out.println(screenRay);

        screenPos.setXY(200, 0);
        trs.transform(screenPos, screenRay);
        System.out.println(screenRay);

        screenPos.setXY(200, 200);
        trs.transform(screenPos, screenRay);
        System.out.println(screenRay);
    }

}
