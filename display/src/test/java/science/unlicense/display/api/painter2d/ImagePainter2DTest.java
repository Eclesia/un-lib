
package science.unlicense.display.api.painter2d;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.Path;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector2i32;

/**
 * General tests for Painter2D.
 *
 * @author Johann Sorel
 */
public abstract class ImagePainter2DTest extends Painter2DTest{

    private static final Color TRS = new ColorRGB(0, 0, 0, 0);

    /**
     * Painter disposed after each test method
     */
    private ImagePainter2D painter;

    protected abstract ImagePainter2D createPainter(int width,int height);

    @After
    public void afterMethod() {
        if (painter !=null) {
            painter.dispose();
        }
    }

    @Test
    public void testFillRectangle() {

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 3
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 4
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 5
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 6
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 7
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 8
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 9
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //10
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //11
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //12
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //13
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };


        painter = createPainter(20, 20);
        final Rectangle rect = new Rectangle(2, 3, 10, 12);
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(rect);
        painter.flush();

        final Image image = painter.getImage();

        for (int y=0;y<20;y++) {
            for (int x=0;x<20;x++) {
                final Color c = image.getColor(new Vector2i32(x,y));
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertTrue(exp.equals(c, null, 0), " at coord "+y+" "+x+" : "+c+" / "+exp);
            }
        }

    }

    @Ignore //this has changed, bbox of geometry area clips the fill
    @Test
    public void testFillLine() {

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 2
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 3
            {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 4
            {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 5
            {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 6
            {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 7
            {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 8
            {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1}, // 9
            {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1}, //10
            {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1}, //11
            {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1}, //12
            {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1}, //13
            {0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1}, //14
            {0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1}, //15
            {0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        painter = createPainter(20, 20);
        final Line line = new DefaultLine(9, 17, 6, 2);
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(line);
        painter.flush();

        final Image image = painter.getImage();

        for (int y=0;y<20;y++) {
            for (int x=0;x<20;x++) {
                final Color c = image.getColor(new Vector2i32(x,y));
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertTrue(exp.equals(c, null, 0), " at coord "+y+" "+x+" : "+c+" / "+exp);
            }
        }

    }

    @Test
    @Ignore
    public void testFillPolyline() {

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 3
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 4
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 5
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 6
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 7
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 8
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 9
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //10
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //11
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //12
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //13
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        painter = createPainter(20, 20);
        final Polyline poly = new Polyline(Geometries.toTupleBuffer(new Tuple[]{
            new Vector2f64(10, 5),
            new Vector2f64(15, 15),
            new Vector2f64(5, 15),
            new Vector2f64(10, 5)}));
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(poly);
        painter.flush();

        final Image image = painter.getImage();

        for (int y=0;y<20;y++) {
            for (int x=0;x<20;x++) {
                final Color c = image.getColor(new Vector2i32(x,y));
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertTrue(exp.equals(c, null, 0), " at coord "+y+" "+x+" : "+c+" / "+exp);
            }
        }

    }

    /**
     * Test the close element of path iterator is well interpreted.
     */
    @Test
    @Ignore
    public void testFillPath1() {

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 3
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 4
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 5
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 6
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 7
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 8
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 9
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //10
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //11
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //12
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //13
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        painter = createPainter(20, 20);
        final Path path = new Path();
        path.appendMoveTo(10, 5);
        path.appendLineTo(15, 15);
        path.appendLineTo(5, 15);
        path.appendClose();
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(path);

        final Image image = painter.getImage();
        for (int y=0;y<20;y++) {
            for (int x=0;x<20;x++) {
                final Color c = image.getColor(new Vector2i32(x,y));
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertTrue(exp.equals(c, null, 0), " at coord "+y+" "+x+" : "+c+" / "+exp);
            }
        }

    }

    /**
     * Test for bug : uncorrectly closed fill area
     */
    @Test
    @Ignore
    public void testFillPath2() {

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 3
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 4
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 5
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 6
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 7
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 8
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 9
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //10
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //11
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //12
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //13
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        painter = createPainter(20, 20);

        final Path path = new Path();
        path.appendMoveTo(10, 5);
        path.appendLineTo(15, 15);
        path.appendLineTo(5, 15);
        path.appendLineTo(10, 5);
        path.appendClose();
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(path);
        painter.flush();

        final Image image = painter.getImage();
        for (int y=0;y<20;y++) {
            for (int x=0;x<20;x++) {
                final Color c = image.getColor(new Vector2i32(x,y));
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertTrue(exp.equals(c, null, 0), " at coord "+y+" "+x+" : "+c+" / "+exp);
            }
        }

    }

    @Test
    public void testOverlapFill() {

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 3
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 4
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 5
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 6
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 7
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 8
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 9
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //10
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //11
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //12
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //13
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        painter = createPainter(20, 20);
        Rectangle rect = new Rectangle(0, 0, 20, 20);
        painter.setPaint(new ColorPaint(Color.BLUE));
        painter.fill(rect);
        painter.flush();

        Image image = painter.getImage();
        for (int y=0;y<20;y++) {
            for (int x=0;x<20;x++) {
                final Color c = image.getColor(new Vector2i32(x,y));
                Assert.assertTrue(Color.BLUE.equals(c, null, 0), " at coord "+y+" "+x+" : "+c+" / "+Color.BLUE);
            }
        }

        rect = new Rectangle(5, 3, 10, 12);
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(rect);
        painter.flush();

        image = painter.getImage();

        for (int y=0;y<20;y++) {
            for (int x=0;x<20;x++) {
                final Color c = image.getColor(new Vector2i32(x,y));
                final Color exp = (expected[y][x] == 0) ? Color.BLUE : Color.RED;
                Assert.assertTrue(exp.equals(c, null, 0), " at coord "+y+" "+x+" : "+c+" / "+exp);
            }
        }

    }

    @Test
    public void testImagePaint() {

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 3
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 4
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 5
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 6
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 7
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 8
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 9
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //10
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //11
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //12
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //13
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        final Image subImage = Images.create(new Extent.Long(10, 12), Images.IMAGE_TYPE_RGBA_FLOAT);
        final BBox all = new BBox(2);
        all.setRange(0, 0, 10);
        all.setRange(1, 0, 12);
        subImage.getColorModel().asTupleBuffer(subImage).setTuple(all, Color.RED);
        final Affine2 trs = new Affine2(
                1, 0, 5,
                0, 1, 3);


        painter = createPainter(20, 20);
        painter.paint(subImage, trs);
        painter.flush();

        final Image image = painter.getImage();

        for (int y=0;y<20;y++) {
            for (int x=0;x<20;x++) {
                final Color c = image.getColor(new Vector2i32(x,y));
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertTrue(exp.equals(c, null, 0), " at coord "+y+" "+x+" : "+c+" / "+exp);
            }
        }

    }

    @Test
    public void testImageTransformPaint() {

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 3
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 4
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 5
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 6
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 7
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 8
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 9
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //10
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //11
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //12
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //13
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        final Image subImage = Images.create(new Extent.Long(10, 12), Images.IMAGE_TYPE_RGBA_FLOAT);
        final BBox all = new BBox(2);
        all.setRange(0, 0, 10);
        all.setRange(1, 0, 12);
        subImage.getColorModel().asTupleBuffer(subImage).setTuple(all, Color.RED);
        final Affine2 trs = new Affine2(
                1, 0, 5,
                0, 1, 3);



        final ImagePainter2D imgPainter = createPainter(20, 20);
        Painter2D painter = imgPainter;
        final Affine2 steptrs = new Affine2(
                1, 0, -3,
                0, 1,  0);
        painter = painter.derivate(true);
        painter.setTransform(steptrs);
        painter.paint(subImage, trs);
        painter.flush();

        final Image image = imgPainter.getImage();

        for (int y=0;y<20;y++) {
            for (int x=0;x<20;x++) {
                final Color c = image.getColor(new Vector2i32(x,y));
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertTrue(exp.equals(c, null, 0), " at coord "+y+" "+x+" : "+c+" / "+exp);
            }
        }

    }

    @Test
    public void testLinearGradientPaint() {

        painter = createPainter(20, 20);
        final Rectangle rect = new Rectangle(0, 0, 20, 20);

        final LinearGradientPaint paint = new LinearGradientPaint(0, 1, 0, 19, new double[]{0,1}, new Color[]{Color.RED,Color.GREEN},SpreadMode.PAD);
        painter.setPaint(paint);
        painter.fill(rect);
        painter.flush();

        final Image image = painter.getImage();

        Color c = image.getColor(new Vector2i32(0,0));
        Assert.assertTrue(Color.RED.equals(c, null, 0));
        c = image.getColor(new Vector2i32(0,19));
        Assert.assertTrue(Color.GREEN.equals(c, null, 0));
    }

}
