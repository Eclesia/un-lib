
package science.unlicense.display.api.scene;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.SceneUtils;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.NodeTransformUpdater;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.math.impl.Vector4f64;

/**
 *
 * @author Johann Sorel
 */
public class SceneUtilsTest {

    private static final double DELTA = 0.0000001;

    @Test
    public void testWorldToNodeTransform(){

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode n1 = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        n1.getNodeTransform().getTranslation().setXYZ(2, 1, 3);
        n1.getNodeTransform().notifyChanged();
        final SceneNode n2 = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        n2.getNodeTransform().getTranslation().setXYZ(3, -5, 7);
        n2.getNodeTransform().notifyChanged();
        scene.getChildren().add(n1);
        n1.getChildren().add(n2);

        //calculate our reference vector
        final VectorRW reference = new Vector3f64(0, 0, 0);
        n1.getNodeTransform().transform(reference,reference);
        n2.getNodeTransform().transform(reference,reference);

        Assert.assertEquals(5, reference.get(0), DELTA);
        Assert.assertEquals(-4, reference.get(1), DELTA);
        Assert.assertEquals(10, reference.get(2), DELTA);

        final Matrix worldToNode = n2.getRootToNodeSpace().toMatrix();
        final VectorRW candidate = new Vector4f64(0, 0, 0, 1);
        worldToNode.transform(candidate,candidate);

        //we should be back to 0
        Assert.assertEquals(new Vector4f64(-5, 4, -10, 1), candidate);

    }

    @Test
    public void testScale(){

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();

        final SceneNode n0 = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        n0.getNodeTransform().getTranslation().setXYZ(0, 7, 0);
        n0.getNodeTransform().notifyChanged();
        scene.getChildren().add(n0);

        final SceneNode n1 = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        n1.getNodeTransform().getScale().setXYZ(3, 3, 3);
        n1.getNodeTransform().notifyChanged();
        n0.getChildren().add(n1);

        final SceneNode n2 = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        n2.getNodeTransform().getTranslation().setXYZ(4, 0, 0);
        n2.getNodeTransform().notifyChanged();
        n1.getChildren().add(n2);


        camera.getNodeTransform().getTranslation().setXYZ(0, 0, -5);
        camera.getNodeTransform().notifyChanged();
        scene.getChildren().add(camera);

        final VectorRW reference = new Vector4f64(1, 1, 1, 1);

        Matrix matrix = n1.getNodeToRootSpace().toMatrix();
        VectorRW res = new Vector4f64();
        matrix.transform(reference,res);
        Assert.assertEquals(3, res.get(0), DELTA);
        Assert.assertEquals(3+7, res.get(1), DELTA);
        Assert.assertEquals(3, res.get(2), DELTA);

        matrix = SceneUtils.sourceToTarget(n1, camera).toMatrix();
        matrix.transform(reference,res);
        Assert.assertEquals(3, res.get(0), DELTA);
        Assert.assertEquals(3+7, res.get(1), DELTA);
        Assert.assertEquals(3+5, res.get(2), DELTA);

        matrix = n2.getNodeToRootSpace().toMatrix();
        matrix.transform(reference,res);
        Assert.assertEquals(3*(4+1), res.get(0), DELTA);
        Assert.assertEquals(3+7, res.get(1), DELTA);
        Assert.assertEquals(3, res.get(2), DELTA);

        matrix = SceneUtils.sourceToTarget(n2, camera).toMatrix();
        matrix.transform(reference,res);
        Assert.assertEquals(3*(4+1), res.get(0), DELTA);
        Assert.assertEquals(3+7, res.get(1), DELTA);
        Assert.assertEquals(3+5, res.get(2), DELTA);


    }

    @Test
    public void testNodeToViewTransform(){

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();


        final SceneNode node = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        node.getNodeTransform().getTranslation().setXYZ(2, 1, 3);
        node.getNodeTransform().notifyChanged();
        scene.getChildren().add(node);

        camera.getNodeTransform().getTranslation().setXYZ(3, 5, 7);
        camera.getNodeTransform().notifyChanged();
        scene.getChildren().add(camera);

        final Matrix4x4 expected = new Matrix4x4(
                1, 0, 0, -1,
                0, 1, 0, -4,
                0, 0, 1, -4,
                0, 0, 0, 1
        );

        final Matrix nodeToCamera = SceneUtils.sourceToTarget(node, camera).toMatrix();
        Assert.assertEquals(expected, nodeToCamera);
    }

    /**
     * Verify points alignement.
     */
    @Test
    public void testAlignment(){
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();

        final Tuple t1 = new Vector4f64(5, 0, 0, 1);
        final Tuple t2 = new Vector4f64(-5, 0, 0, 1);

        final SceneNode node1 = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        node1.getNodeTransform().getTranslation().setXYZ(-5, 0, 0);
        node1.getNodeTransform().notifyChanged();
        scene.getChildren().add(node1);

        final SceneNode node2 = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        node2.getNodeTransform().getTranslation().setXYZ(5, 0, 0);
        node2.getNodeTransform().notifyChanged();
        scene.getChildren().add(node2);

        camera.getNodeTransform().getTranslation().setXYZ(0, 0, -10);
        camera.getNodeTransform().notifyChanged();
        scene.getChildren().add(camera);

        Matrix mv1 = SceneUtils.sourceToTarget(node1, camera).toMatrix();
        Matrix mv2 = SceneUtils.sourceToTarget(node2, camera).toMatrix();
        Matrix mvp1 = camera.calculateMVP(node1);
        Matrix mvp2 = camera.calculateMVP(node2);
        //we should obtain the same point
        Tuple tmv1 = mv1.transform(t1);
        Tuple tmv2 = mv2.transform(t2);
        assertTEquals(tmv1, tmv2);
        Tuple tmvp1 = mvp1.transform(t1);
        Tuple tmvp2 = mvp2.transform(t2);
        assertTEquals(tmvp1, tmvp2);

        NodeTransformUpdater control = new NodeTransformUpdater(camera, new Vector4f64(0, 1, 0, 0), new Vector4f64(1, 0, 0, 0));
        control.rotate(control.getUp(), (float) Math.toRadians(45));
        control.update(null);

        mv1 = SceneUtils.sourceToTarget(node1, camera).toMatrix();
        mv2 = SceneUtils.sourceToTarget(node2, camera).toMatrix();
        mvp1 = camera.calculateMVP(node1);
        mvp2 = camera.calculateMVP(node2);
        //we should obtain the same point
        tmv1 = mv1.transform(t1);
        tmv2 = mv2.transform(t2);
        assertTEquals(tmv1, tmv2);
        tmvp1 = mvp1.transform(t1);
        tmvp2 = mvp2.transform(t2);
        assertTEquals(tmvp1, tmvp2);
    }

    private static void assertTEquals(Tuple t1, Tuple t2){
        Assert.assertEquals(t1.getSampleCount(), t2.getSampleCount());
        for (int i=0;i<t1.getSampleCount();i++){
            Assert.assertEquals(t1.get(i),t2.get(i),DELTA);
        }

    }

}
