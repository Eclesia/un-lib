
package science.unlicense.display.api.layout;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public class FillConstraintTest {

    private static final float DELTA = 0.00001f;

    @Test
    public void testBuilder() {
        FillConstraint cst;

        cst = FillConstraint.builder().build();
        Assert.assertEquals(0,cst.getX());
        Assert.assertEquals(0,cst.getY());
        Assert.assertEquals(1,cst.getSpanX());
        Assert.assertEquals(1,cst.getSpanY());
        Assert.assertEquals(0.0f,cst.getAlignX(),DELTA);
        Assert.assertEquals(0.5f,cst.getAlignY(),DELTA);
        Assert.assertEquals(false,cst.isFillX());
        Assert.assertEquals(false,cst.isFillY());

        cst = FillConstraint.builder().coord(10, 20).build();
        Assert.assertEquals(10,cst.getX());
        Assert.assertEquals(20,cst.getY());
        Assert.assertEquals(1,cst.getSpanX());
        Assert.assertEquals(1,cst.getSpanY());
        Assert.assertEquals(0.0f,cst.getAlignX(),DELTA);
        Assert.assertEquals(0.5f,cst.getAlignY(),DELTA);
        Assert.assertEquals(false,cst.isFillX());
        Assert.assertEquals(false,cst.isFillY());

        cst = FillConstraint.builder().span(2, 3).build();
        Assert.assertEquals(0,cst.getX());
        Assert.assertEquals(0,cst.getY());
        Assert.assertEquals(2,cst.getSpanX());
        Assert.assertEquals(3,cst.getSpanY());
        Assert.assertEquals(0.0f,cst.getAlignX(),DELTA);
        Assert.assertEquals(0.5f,cst.getAlignY(),DELTA);
        Assert.assertEquals(false,cst.isFillX());
        Assert.assertEquals(false,cst.isFillY());

        cst = FillConstraint.builder().align(0.8f,0.9f).build();
        Assert.assertEquals(0,cst.getX());
        Assert.assertEquals(0,cst.getY());
        Assert.assertEquals(1,cst.getSpanX());
        Assert.assertEquals(1,cst.getSpanY());
        Assert.assertEquals(0.8f,cst.getAlignX(),DELTA);
        Assert.assertEquals(0.9f,cst.getAlignY(),DELTA);
        Assert.assertEquals(false,cst.isFillX());
        Assert.assertEquals(false,cst.isFillY());

        cst = FillConstraint.builder().fill(true,true).build();
        Assert.assertEquals(0,cst.getX());
        Assert.assertEquals(0,cst.getY());
        Assert.assertEquals(1,cst.getSpanX());
        Assert.assertEquals(1,cst.getSpanY());
        Assert.assertEquals(0.0f,cst.getAlignX(),DELTA);
        Assert.assertEquals(0.5f,cst.getAlignY(),DELTA);
        Assert.assertEquals(true,cst.isFillX());
        Assert.assertEquals(true,cst.isFillY());

    }

    @Test
    public void testAlignement() {
        FillConstraint cst;
        final Space pos = new Space(new Extent.Double(10,10));
        final BBox bbox = new BBox(new double[]{100,200}, new double[]{150,300});

        //bottom left
        cst = FillConstraint.builder().align(FillConstraint.HALIGN_LEFT,FillConstraint.VALIGN_BOTTOM).build();
        cst.applyOn(pos, bbox);
        Assert.assertEquals(new Extent.Double(10, 10), pos.getEffectiveExtent());
        Assert.assertArrayEquals(new double[]{
            1,0,100+5,
            0,1,200+5,
            0,0,1}, pos.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);


        //top right
        cst = FillConstraint.builder().align(FillConstraint.HALIGN_RIGHT,FillConstraint.VALIGN_TOP).build();
        cst.applyOn(pos, bbox);
        Assert.assertEquals(new Extent.Double(10, 10), pos.getEffectiveExtent());
        Assert.assertArrayEquals(new double[]{
            1,0,150-5,
            0,1,300-5,
            0,0,1}, pos.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);

        //center
        cst = FillConstraint.builder().align(FillConstraint.HALIGN_CENTER,FillConstraint.VALIGN_CENTER).build();
        cst.applyOn(pos, bbox);
        Assert.assertEquals(new Extent.Double(10, 10), pos.getEffectiveExtent());
        Assert.assertArrayEquals(new double[]{
            1,0,125,
            0,1,250,
            0,0,1}, pos.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);

    }

    @Test
    public void testFill() {
        FillConstraint cst;
        final Space pos = new Space(new Extent.Double(10,10));
        final BBox bbox = new BBox(new double[]{100,200}, new double[]{150,300});

        //no fill
        cst = FillConstraint.builder().align(FillConstraint.HALIGN_CENTER,FillConstraint.VALIGN_CENTER).build();
        cst.applyOn(pos, bbox);
        Assert.assertEquals(new Extent.Double(10, 10), pos.getEffectiveExtent());
        Assert.assertArrayEquals(new double[]{
            1,0,125,
            0,1,250,
            0,0,1}, pos.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);


        //fill x
        cst = FillConstraint.builder().align(FillConstraint.HALIGN_CENTER,FillConstraint.VALIGN_CENTER).fill(true, false).build();
        cst.applyOn(pos, bbox);
        Assert.assertEquals(new Extent.Double(50, 10), pos.getEffectiveExtent());
        Assert.assertArrayEquals(new double[]{
            1,0,125,
            0,1,250,
            0,0,1}, pos.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);

        //fill y
        cst = FillConstraint.builder().align(FillConstraint.HALIGN_CENTER,FillConstraint.VALIGN_CENTER).fill(false, true).build();
        cst.applyOn(pos, bbox);
        Assert.assertEquals(new Extent.Double(10, 100), pos.getEffectiveExtent());
        Assert.assertArrayEquals(new double[]{
            1,0,125,
            0,1,250,
            0,0,1}, pos.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);

        //fill x and y
        cst = FillConstraint.builder().align(FillConstraint.HALIGN_CENTER,FillConstraint.VALIGN_CENTER).fill(true, true).build();
        cst.applyOn(pos, bbox);
        Assert.assertEquals(new Extent.Double(50, 100), pos.getEffectiveExtent());
        Assert.assertArrayEquals(new double[]{
            1,0,125,
            0,1,250,
            0,0,1}, pos.getNodeTransform().viewMatrix().toArrayDouble(), DELTA);

    }

}
