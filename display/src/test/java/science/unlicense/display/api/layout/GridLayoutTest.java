
package science.unlicense.display.api.layout;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class GridLayoutTest {

    @Test
    public void testVertical() {

        final Space but1 = new Space(new Extent.Double(2, 2));
        final Space but2 = new Space(new Extent.Double(2, 2));
        final Space but3 = new Space(new Extent.Double(2, 2));

        final GridLayout layout = new GridLayout(-1, 1);
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2,but3}));


        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 300)));
        layout.update();
        Assert.assertEquals(new Extent.Double(100d, 100d),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 50,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(100d, 100d),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 150,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(100d, 100d),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 250,
                0, 0, 1),
                but3.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(2d, 6d),layout.getExtents(null, null).getBest(null));

        //change size ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(20, 30)));
        layout.update();
        Assert.assertEquals(new Extent.Double(20d, 10d), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10,
                0, 1, 5,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(20d, 10d),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10,
                0, 1, 15,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(20d, 10d),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10,
                0, 1, 25,
                0, 0, 1),
                but3.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(2d, 6d),layout.getExtents(null, null).getBest(null));

    }

    @Test
    public void testHorizontal() {

        final Space but1 = new Space(new Extent.Double(2, 2));
        final Space but2 = new Space(new Extent.Double(2, 2));
        final Space but3 = new Space(new Extent.Double(2, 2));

        final GridLayout layout = new GridLayout(1, -1);
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2,but3}));

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(300, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(100d, 100d),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 50,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(100d, 100d),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 150,
                0, 1, 50,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(100d, 100d),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 250,
                0, 1, 50,
                0, 0, 1),
                but3.getNodeTransform().viewMatrix());

        //change size ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(60, 5)));
        layout.update();
        Assert.assertEquals(new Extent.Double(20d, 5d), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10,
                0, 1, 2.5,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(20d, 5d),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 30,
                0, 1, 2.5,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(20d, 5d),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 2.5,
                0, 0, 1),
                but3.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(6d, 2d),layout.getExtents(null, null).getBest(null));

    }

    @Test
    public void testSpacing1x1() {

        final Space ele1 = new Space(new Extent.Double(10, 10));

        final GridLayout layout = new GridLayout(1, 1, 2, 2);
        layout.setPositionables(new ArraySequence(new Object[]{ele1}));

        Assert.assertEquals(new Extent.Double(10, 10), layout.getExtents(null, null).getBest(null));

        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(10, 10)));
        layout.update();
        Assert.assertEquals(new Extent.Double(10, 10), ele1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 5,
                0, 0, 1),
                ele1.getNodeTransform().viewMatrix());
    }

    @Test
    public void testSpacing2x2() {

        final Space ele00 = new Space(new Extent.Double(10, 10));
        final Space ele01 = new Space(new Extent.Double(10, 10));
        final Space ele10 = new Space(new Extent.Double(10, 10));
        final Space ele11 = new Space(new Extent.Double(10, 10));

        final GridLayout layout = new GridLayout(2, 2, 2, 2);
        layout.setPositionables(new ArraySequence(new Object[]{ele00,ele01,ele10,ele11}));

        Assert.assertEquals(new Extent.Double(22, 22), layout.getExtents(null, null).getBest(null));

        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(22, 22)));
        layout.update();
        Assert.assertEquals(new Extent.Double(10, 10), ele00.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 5,
                0, 0, 1),
                ele00.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(10, 10), ele01.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 17,
                0, 1, 5,
                0, 0, 1),
                ele01.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(10, 10), ele10.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 17,
                0, 0, 1),
                ele10.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(10, 10), ele11.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 17,
                0, 1, 17,
                0, 0, 1),
                ele11.getNodeTransform().viewMatrix());
    }

}
